local GameStateRecruit = GameStateManager.GameStateRecruit
local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameUIRecruitMain = LuaObjectManager:GetLuaObject("GameUIRecruitMain")
local GameUIRecruitWarpgate = LuaObjectManager:GetLuaObject("GameUIRecruitWarpgate")
local QuestTutorialStarCharge = TutorialQuestManager.QuestTutorialStarCharge
local QuestTutorialRecruit = TutorialQuestManager.QuestTutorialRecruit
local GameUICommonDialog = LuaObjectManager:GetLuaObject("GameUICommonDialog")
local GameUIRecruitTopbar = LuaObjectManager:GetLuaObject("GameUIRecruitTopbar")
local GameUICollect = LuaObjectManager:GetLuaObject("GameUICollect")
local GameVipDetailInfoPanel = LuaObjectManager:GetLuaObject("GameVipDetailInfoPanel")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
function GameUIRecruitWarpgate:OnInitGame()
end
function GameUIRecruitWarpgate:InitFlashObject()
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "lua2fs_setLocalText", "collect_energy", GameLoader:GetGameText("LC_MENU_GATHER_BUTTON"))
end
function GameUIRecruitWarpgate:OnAddToGameState()
  self:LoadFlashObject()
  local canConvert = not QuestTutorialStarCharge:IsActive() and not QuestTutorialRecruit:IsActive()
  self:GetFlashObject():SetBtnEnable("_warpgate._left.btnConvert", canConvert)
  self:GetFlashObject():InvokeASCallback("_root", "showConvertTool")
  self:GetFlashObject():InvokeASCallback("_root", "showBulletinPanel")
  self.RefreshResourceCount()
  GameGlobalData:RegisterDataChangeCallback("item_count", self.RefreshResourceCount)
end
function GameUIRecruitWarpgate:OnEraseFromGameState()
  if QuestTutorialStarCharge:IsActive() then
    self:GetFlashObject():InvokeASCallback("_root", "HideTutorialWarpGateSelect")
    self:GetFlashObject():InvokeASCallback("_root", "HideTutorialWarpGateCharge")
    self:GetFlashObject():InvokeASCallback("_root", "HideTutorialWarpGateMax")
    self:GetFlashObject():InvokeASCallback("_root", "HideTutorialWarpGateConfirm")
  end
  self:UnloadFlashObject()
end
function GameUIRecruitWarpgate:OnFSCommand(cmd, arg)
  if cmd == "seed_energy" then
    AddFlurryEvent("TutorialStart_StartCharge", {}, 2)
    if arg == "quark" then
      local warpgate_info = self._WarpgateInfo
      local info = string.gsub(GameLoader:GetGameText("LC_MENU_STAR_PORTAL_CREDITS_COLLECT"), "<number1>", tostring(warpgate_info.charge_quark_cost))
      GameUtils:CreditCostConfirm(info, function()
        GameUIRecruitWarpgate.quarkClicked()
      end)
    else
      GameUIRecruitWarpgate.SeedEnergyBalls(arg)
    end
    self:GetFlashObject():InvokeASCallback("_root", "SetPlaySeedTip", false)
    self:RefreshUI()
  elseif cmd == "charge_energy" then
    AddFlurryEvent("TutorialStart_Charge", {}, 2)
    local isquark = true
    for _, boltData in ipairs(self._WarpgateInfo.bolts) do
      if boltData.bolt_type == 1 then
        isquark = false
        break
      end
    end
    if arg == "max" then
      local info = string.gsub(GameLoader:GetGameText("LC_MENU_STAR_PORTAL_CREDITS_COLLECT"), "<number1>", tostring(self._WarpgateInfo.max_upgrade_cost))
      GameUtils:CreditCostConfirm(info, function()
        GameUIRecruitWarpgate.maxClicked()
      end)
    elseif isquark then
      local info = string.gsub(GameLoader:GetGameText("LC_MENU_STAR_PORTAL_CREDITS_COLLECT"), "<number1>", tostring(self._WarpgateInfo.normal_upgrade_cost))
      GameUtils:CreditCostConfirm(info, function()
        GameUIRecruitWarpgate:UpgradeEnergyBalls(arg)
      end)
    else
      self:UpgradeEnergyBalls(arg)
    end
  elseif cmd == "collect_energy" then
    AddFlurryEvent("TutorialStart_Collect", {}, 2)
    self:CollectEnergyBalls()
  elseif cmd == "collect_animation_over" then
    self:MoveoutWarpgate()
  elseif cmd == "convert_energy" then
    if arg == "item" then
      GameUIRecruitWarpgate:ConvertEnergy(1, "item2514")
    else
      GameUIRecruitWarpgate:ConvertEnergy(1, "credit")
    end
  elseif cmd == "convert_start" then
    self:UpdateConvertRes()
  elseif cmd == "convert_close" then
    self:GetFlashObject():InvokeASCallback("_root", "HideConvertRequest")
    self.IsShowConvertWindow = false
  elseif cmd == "moveout_warpgate_animation_over" or cmd == "moveout_select_animation_over" then
    self:RefreshUI()
    self:GetFlashObject():InvokeASCallback("_root", "SetPlaySeedTip", true)
    self:UpdateResourceData()
  elseif cmd == "help" then
    GameUIRecruitTopbar:PlayWarpGateHelp()
  elseif cmd == "ReloadMultLangText" then
  end
end
function GameUIRecruitWarpgate:UpdateGameData()
  if not self._WarpgateInfo then
    local function updateWarpgateRequest()
      NetMessageMgr:SendMsg(NetAPIList.warpgate_status_req.Code, nil, self.serverCallback, true, updateWarpgateRequest)
    end
    NetMessageMgr:SendMsg(NetAPIList.warpgate_status_req.Code, nil, self.serverCallback, true, updateWarpgateRequest)
  else
    GameUIRecruitWarpgate:UpdateEnergyBalls()
    GameUIRecruitWarpgate:UpdateResourceData()
    GameUIRecruitWarpgate:RefreshUI()
    if QuestTutorialStarCharge:IsActive() and LuaUtils:table_empty(GameUIRecruitWarpgate._WarpgateInfo.bolts) then
      GameUIRecruitWarpgate:GetFlashObject():InvokeASCallback("_root", "ShowTutorialWarpGateSelect")
    elseif QuestTutorialStarCharge:IsActive() then
      if immanentversion == 2 then
        GameUICommonDialog:PlayStory({51315}, nil)
      end
      GameUIRecruitWarpgate:GetFlashObject():InvokeASCallback("_root", "ShowTutorialWarpGateCharge")
    else
      GameUIRecruitWarpgate:GetFlashObject():InvokeASCallback("_root", "HideTutorialWarpGateCharge")
    end
  end
end
function GameUIRecruitWarpgate:RefreshUI()
  local warpgate_info = self._WarpgateInfo
  local flash_obj = self:GetFlashObject()
  if LuaUtils:table_empty(warpgate_info.bolts) then
    DebugOut("GameUIRecruitWarpgate:RefreshUI DisplaySelectPower")
    self:DisplaySelectPower()
  else
    local seedType = "quark"
    for _, boltData in ipairs(warpgate_info.bolts) do
      if boltData.bolt_type == 1 then
        seedType = "lepton"
        break
      end
    end
    local isFull = true
    for _, boltData in ipairs(warpgate_info.bolts) do
      if boltData.is_full == false then
        isFull = false
        break
      end
    end
    DebugOut("GameUIRecruitWarpgate:RefreshUI DisplayPowerup")
    self:DisplayPowerup(seedType, isFull)
  end
end
function GameUIRecruitWarpgate:DisplaySelectPower()
  local warpgate_info = self._WarpgateInfo
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "SetShowSeedEnergy", true, true)
  flash_obj:InvokeASCallback("_root", "SetShowCollect", false)
  flash_obj:InvokeASCallback("_root", "SetShowCharge", false)
  flash_obj:InvokeASCallback("_root", "lua2fs_updateSeedEnergyRequire", "lepton", tonumber(warpgate_info.charge_lepton_cost) == 0, warpgate_info.charge_lepton_cost)
  flash_obj:InvokeASCallback("_root", "lua2fs_updateSeedEnergyRequire", "quark", tonumber(warpgate_info.charge_quark_cost) == 0, warpgate_info.charge_quark_cost)
  if immanentversion == 1 then
    local vipInfo = GameGlobalData:GetData("vipinfo")
    local curLevel = GameVipDetailInfoPanel:GetVipLevel()
    if curLevel == 0 then
      self:GetFlashObject():InvokeASCallback("_root", "lua2fs_setVisible", "btnQuarkCharge", false)
      self:GetFlashObject():InvokeASCallback("_root", "lua2fs_setVisible", "iconQuarkCharge", false)
    end
  end
  if QuestTutorialStarCharge:IsActive() then
    local function callback()
      self:GetFlashObject():InvokeASCallback("_root", "lua2fs_moveInSelect")
    end
    if immanentversion == 1 then
      GameUICommonDialog:PlayStory({100025}, callback)
    elseif immanentversion == 2 then
      GameUICommonDialog:PlayStory({51316}, callback)
    end
  else
    self:GetFlashObject():InvokeASCallback("_root", "lua2fs_moveInSelect")
  end
end
function GameUIRecruitWarpgate:UpdateUpgradeRequireData(seedType)
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "updateChargeEnergyRequire", seedType, self._WarpgateInfo.normal_upgrade_cost, self._WarpgateInfo.max_upgrade_cost)
end
function GameUIRecruitWarpgate:MoveoutSelect()
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "SetShowCollect", true)
  flash_obj:InvokeASCallback("_root", "SetShowSeedEnergy", false, false)
  flash_obj:InvokeASCallback("_root", "SetShowCharge", true)
end
function GameUIRecruitWarpgate:DisplayPowerup(seedType, isFull)
  local flash_obj = self:GetFlashObject()
  self:UpdateUpgradeRequireData(seedType)
  self:UpdateCurrentBallNumber()
  self:EnableBtnChargeNormal(not isFull)
  self:EnableBtnChargeMax(not isFull)
  flash_obj:InvokeASCallback("_root", "lua2fs_animationWarpgateMovein", seedType)
end
function GameUIRecruitWarpgate:MoveoutWarpgate()
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "lua2fs_moveOutWarpgate")
end
function GameUIRecruitWarpgate:UpdateEnergyBalls()
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "lua2fs_resetEnergyBalls")
  local warpgate_info = self._WarpgateInfo
  for i, v in ipairs(warpgate_info.bolts) do
    local type_energy = ""
    if v.bolt_type == 1 then
      type_energy = "lepton"
    elseif v.bolt_type == 2 then
      type_energy = "quark"
    else
      assert(false)
    end
    flash_obj:InvokeASCallback("_root", "lua2fs_updateEnergyBall", i, type_energy, v.level, v.val)
  end
  GameUIRecruitWarpgate:UpdateCurrentBallNumber()
end
function GameUIRecruitWarpgate:UpgradeEnergyBalls(type_upgrade)
  local packet = {}
  local netCallback = 0
  if type_upgrade == "normal" then
    packet.upgrade_type = 1
    netCallback = self.NetCallbackUpgradeNormal
  elseif type_upgrade == "max" then
    packet.upgrade_type = 2
    netCallback = self.NetCallbackUpgradeMax
  else
    assert(false)
  end
  self.upgrade_type = packet.upgrade_type
  NetMessageMgr:SendMsg(NetAPIList.warpgate_upgrade_req.Code, packet, netCallback, true, nil)
end
function GameUIRecruitWarpgate.maxClicked()
  GameUIRecruitWarpgate:UpgradeEnergyBalls("max")
end
function GameUIRecruitWarpgate.SeedEnergyBalls(type_seed)
  local packet = {}
  if type_seed == "lepton" then
    packet.charge_type = 1
  elseif type_seed == "quark" then
    packet.charge_type = 2
  else
    assert(false)
  end
  GameUIRecruitWarpgate.seed_type = type_seed
  NetMessageMgr:SendMsg(NetAPIList.warpgate_charge_req.Code, packet, GameUIRecruitWarpgate.serverCallback, true, nil)
end
function GameUIRecruitWarpgate.quarkClicked()
  GameUIRecruitWarpgate.SeedEnergyBalls("quark")
end
function GameUIRecruitWarpgate:CollectEnergyBalls()
  NetMessageMgr:SendMsg(NetAPIList.warpgate_collect_req.Code, nil, self.NetCallbackCollect, true, nil)
end
function GameUIRecruitWarpgate:ConvertEnergy(num, type)
  local content = {cnt = num, use_type = type}
  DebugOut("GameUIRecruitWarpgate:ConvertEnergy")
  DebugTable(content)
  NetMessageMgr:SendMsg(NetAPIList.quark_exchange_req.Code, content, self.serverCallback, true)
end
function GameUIRecruitWarpgate:UpdateResourceData()
  local data_resource = GameGlobalData:GetData("resource")
  local text_lepton = GameUtils.numberConversion(data_resource.lepton)
  local text_quark = GameUtils.numberConversion(data_resource.quark)
  self:GetFlashObject():InvokeASCallback("_root", "lua2fs_updateResourceData", text_lepton, text_quark, -1)
  local num_cubit = GameUtils.numberConversion(data_resource.money)
  local num_credit = GameUtils.numberConversion(data_resource.credit)
  self:GetFlashObject():InvokeASCallback("_root", "setCubitAndCredit", num_cubit, num_credit)
end
function GameUIRecruitWarpgate:UpdateConvertRes()
  local data_resource = GameGlobalData:GetData("resource")
  local text_lepton = GameUtils.numberConversion(data_resource.lepton)
  local text_quark = GameUtils.numberConversion(data_resource.quark)
  local item_2514_count = self:GetItem2514Count()
  local redit_cost = self._WarpgateInfo.convert_cost_credits
  self:GetFlashObject():InvokeASCallback("_root", "ShowConvertRequest", text_lepton, text_quark, item_2514_count, 1000, 100, 1, redit_cost)
  self.IsShowConvertWindow = true
end
function GameUIRecruitWarpgate:UpdatePanelData()
end
function GameUIRecruitWarpgate:SetVisible(mcPath, is_visible)
  mcPath = mcPath or -1
  self:GetFlashObject():InvokeASCallback("_root", "lua2fs_setVisible", mcPath, is_visible)
end
function GameUIRecruitWarpgate:PlayCollectAnimation()
  local flash_obj = self:GetFlashObject()
  DebugOut("GameUIRecruitWarpgate:PlayCollectAnimation")
  flash_obj:InvokeASCallback("_root", "lua2fs_playCollectAnimation")
end
function GameUIRecruitWarpgate:UpdateCurrentBallNumber()
  DebugOut("GameUIRecruitWarpgate:UpdateCurrentBallNumber:", self._WarpgateInfo.lepton, self._WarpgateInfo.quark)
  self:GetFlashObject():InvokeASCallback("_root", "setCurrentCollectCount", self._WarpgateInfo.lepton, self._WarpgateInfo.quark)
end
function GameUIRecruitWarpgate:SetCollectInfo()
  self:GetFlashObject():InvokeASCallback("_root", "setCollectInfo", self._WarpgateInfo.lepton, self._WarpgateInfo.quark)
end
function GameUIRecruitWarpgate:EnableBtnChargeNormal(isEnable)
  self:GetFlashObject():InvokeASCallback("_root", "enableChargeNormal", isEnable)
end
function GameUIRecruitWarpgate:EnableBtnChargeMax(isEnable)
  self:GetFlashObject():InvokeASCallback("_root", "enableChargeMax", isEnable)
end
function GameUIRecruitWarpgate.resourceListener()
  if GameStateManager:GetCurrentGameState() == GameStateRecruit then
    GameUIRecruitWarpgate:UpdateResourceData()
  end
end
function GameUIRecruitWarpgate:ShowAlerttTip(content)
  if not GameUICollect:CheckTutorialCollect(content.code) and (content.code == 709 or content.code == 130) then
    local text = AlertDataList:GetTextFromErrorCode(130)
    local GameUIKrypton = LuaObjectManager:GetLuaObject("GameUIKrypton")
    GameUIKrypton.NeedMoreMoney(text)
  else
    local GameVip = LuaObjectManager:GetLuaObject("GameVip")
    GameVip:CheckIsNeedShowVip(content.code)
  end
end
function GameUIRecruitWarpgate.serverCallback(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and (content.api == NetAPIList.warpgate_status_req.Code or content.api == NetAPIList.warpgate_charge_req.Code or content.api == NetAPIList.warpgate_upgrade_req.Code or content.api == NetAPIList.quark_exchange_req.Code or content.api == NetAPIList.system_configuration_req.Code) then
    GameUIRecruitWarpgate:ShowAlerttTip(content)
    return true
  end
  if msgtype == NetAPIList.warpgate_status_ack.Code then
    DebugOut("GameUIRecruitWarpgate.serverCallback warpgate_status_ack")
    DebugTable(content)
    GameUIRecruitWarpgate:GetFlashObject():InvokeASCallback("_root", "ShowAfterLoading")
    GameUIRecruitWarpgate._WarpgateInfo = content
    GameUIRecruitWarpgate:UpdateEnergyBalls()
    GameUIRecruitWarpgate:UpdateResourceData()
    GameUIRecruitWarpgate:RefreshUI()
    if LuaUtils:table_empty(content.bolts) then
      GameUIRecruitWarpgate:GetFlashObject():InvokeASCallback("_root", "SetPlaySeedTip", true)
    else
      GameUIRecruitWarpgate:GetFlashObject():InvokeASCallback("_root", "SetShowSeedTip", false)
    end
    if QuestTutorialStarCharge:IsActive() and LuaUtils:table_empty(GameUIRecruitWarpgate._WarpgateInfo.bolts) then
      GameUIRecruitWarpgate:GetFlashObject():InvokeASCallback("_root", "ShowTutorialWarpGateSelect")
    elseif QuestTutorialStarCharge:IsActive() then
      GameUIRecruitWarpgate:GetFlashObject():InvokeASCallback("_root", "ShowTutorialWarpGateCharge")
    else
      GameUIRecruitWarpgate:GetFlashObject():InvokeASCallback("_root", "HideTutorialWarpGateCharge")
    end
    return true
  end
  if msgtype == NetAPIList.warpgate_charge_ack.Code then
    if content.code ~= 0 then
      GameUIRecruitWarpgate:ShowAlerttTip(content)
      return true
    end
    DebugOut("GameUIRecruitWarpgate.serverCallback warpgate_charge_ack")
    DebugTable(content)
    if QuestTutorialStarCharge:IsActive() then
      GameUIRecruitWarpgate:GetFlashObject():InvokeASCallback("_root", "HideTutorialWarpGateSelect")
      local function callback()
        GameUIRecruitWarpgate:GetFlashObject():InvokeASCallback("_root", "ShowTutorialWarpGateCharge")
      end
      if immanentversion == 1 then
        GameUICommonDialog:PlayStory({100026}, callback)
      elseif immanentversion == 2 then
        GameUICommonDialog:PlayStory({51317}, callback)
      end
    end
    GameUIRecruitWarpgate._WarpgateInfo = content.info
    GameUIRecruitWarpgate:UpdateEnergyBalls()
    GameUIRecruitWarpgate:UpdateResourceData()
    GameUIRecruitWarpgate:MoveoutSelect()
    GameUIRecruitWarpgate:RefreshUI()
    return true
  end
  if msgtype == NetAPIList.warpgate_upgrade_ack.Code then
    if content.code ~= 0 then
      GameUIRecruitWarpgate:ShowAlerttTip(content)
      return true
    end
    DebugOut("GameUIRecruitWarpgate.serverCallback warpgate_upgrade_ack")
    DebugTable(content)
    if QuestTutorialStarCharge:IsActive() then
      GameUIRecruitWarpgate:GetFlashObject():InvokeASCallback("_root", "HideTutorialWarpGateCharge")
      GameUIRecruitWarpgate:GetFlashObject():InvokeASCallback("_root", "ShowTutorialWarpGateConfirm")
      GameUICommonDialog:PlayStory({100028}, nil)
    end
    GameUIRecruitWarpgate._WarpgateInfo = content.info
    GameUIRecruitWarpgate:UpdateEnergyBalls()
    GameUIRecruitWarpgate:UpdateResourceData()
    return true
  end
  if msgtype == NetAPIList.quark_exchange_ack.Code then
    if content.code ~= 0 then
      local GameVip = LuaObjectManager:GetLuaObject("GameVip")
      GameVip:CheckIsNeedShowVip(content.code)
      return true
    end
    GameUIRecruitWarpgate:UpdateResourceData()
    GameUIRecruitWarpgate:UpdateConvertRes()
    return true
  end
  if msgtype == NetAPIList.system_configuration_ack.Code then
    GameUIRecruitWarpgate.configs = {}
    for _, config_info in ipairs(content.configs) do
      GameUIRecruitWarpgate.configs[config_info.config_attr] = tonumber(config_info.config_value)
    end
    return true
  end
  return false
end
function GameUIRecruitWarpgate.NetCallbackCollect(msgtype, content)
  DebugOut("GameUIRecruitWarpgate.NetCallbackCollect")
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.warpgate_collect_req.Code then
    if content.code ~= 0 then
      GameUIRecruitWarpgate:ShowAlerttTip(content)
    end
    return true
  end
  if msgtype == NetAPIList.warpgate_collect_ack.Code then
    if content.code ~= 0 then
      GameUIRecruitWarpgate:ShowAlerttTip(content)
      return true
    end
    DebugOut("QuestTutorialStarCharge:IsActive : " .. tostring(QuestTutorialStarCharge:IsActive()))
    if QuestTutorialStarCharge:IsActive() then
      GameUIRecruitWarpgate:GetFlashObject():InvokeASCallback("_root", "HideTutorialWarpGateConfirm")
      GameUIRecruitWarpgate:GetFlashObject():InvokeASCallback("_root", "ActiveAllBtn")
      QuestTutorialStarCharge:SetFinish(true)
      DebugOut("QuestTutorialRecruit:IsActive : " .. tostring(QuestTutorialRecruit:IsActive()))
      if GameHelper:IsHaveFleetById(2) then
        QuestTutorialRecruit:SetFinish(true, true)
        QuestTutorialRecruit:SetActive(false, true)
        TutorialQuestManager:Save()
      else
        QuestTutorialRecruit:SetActive(true)
      end
      DebugOut("QuestTutorialRecruit:IsActive : " .. tostring(QuestTutorialRecruit:IsActive()))
      GameStateRecruit:AddObject(GameUIRecruitTopbar)
    end
    DebugOut("QuestTutorialStarCharge:IsActive : " .. tostring(QuestTutorialStarCharge:IsActive()))
    GameUIRecruitWarpgate:SetCollectInfo()
    GameUIRecruitWarpgate._WarpgateInfo = content.info
    GameUIRecruitWarpgate:UpdateEnergyBalls()
    GameUIRecruitWarpgate:PlayCollectAnimation()
    GameUIRecruitWarpgate:EnableBtnChargeNormal(true)
    GameUIRecruitWarpgate:EnableBtnChargeMax(true)
    return true
  end
  return false
end
function GameUIRecruitWarpgate.NetCallbackUpgradeMax(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.warpgate_upgrade_req.Code then
    if content.code ~= 0 then
      local GameVip = LuaObjectManager:GetLuaObject("GameVip")
      GameVip:CheckIsNeedShowVip(content.code)
    end
    return true
  end
  if msgtype == NetAPIList.warpgate_upgrade_ack.Code then
    if content.code ~= 0 then
      GameUIRecruitWarpgate:ShowAlerttTip(content)
      return true
    end
    if QuestTutorialStarCharge:IsActive() then
      GameUIRecruitWarpgate:GetFlashObject():InvokeASCallback("_root", "HideTutorialWarpGateCharge")
      GameUIRecruitWarpgate:GetFlashObject():InvokeASCallback("_root", "ShowTutorialWarpGateConfirm")
      if immanentversion == 1 then
        GameUICommonDialog:PlayStory({100028}, nil)
      elseif immanentversion == 2 then
        GameUICommonDialog:PlayStory({51318}, nil)
      end
    end
    GameUIRecruitWarpgate._WarpgateInfo = content.info
    GameUIRecruitWarpgate:UpdateEnergyBalls()
    GameUIRecruitWarpgate:UpdateResourceData()
    GameUIRecruitWarpgate:EnableBtnChargeNormal(false)
    GameUIRecruitWarpgate:EnableBtnChargeMax(false)
    return true
  end
  return false
end
function GameUIRecruitWarpgate.NetCallbackUpgradeNormal(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.warpgate_upgrade_req.Code then
    if content.code ~= 0 then
      GameUIRecruitWarpgate:ShowAlerttTip(content)
    end
    return true
  end
  if msgtype == NetAPIList.warpgate_upgrade_ack.Code then
    if content.code ~= 0 then
      GameUIRecruitWarpgate:ShowAlerttTip(content)
      return true
    end
    if QuestTutorialStarCharge:IsActive() then
      GameUIRecruitWarpgate:GetFlashObject():InvokeASCallback("_root", "HideTutorialWarpGateCharge")
      GameUIRecruitWarpgate:GetFlashObject():InvokeASCallback("_root", "ShowTutorialWarpGateConfirm")
      if immanentversion == 1 then
        GameUICommonDialog:PlayStory({100028}, nil)
      elseif immanentversion == 2 then
        GameUICommonDialog:PlayStory({51318}, nil)
      end
    end
    GameUIRecruitWarpgate._WarpgateInfo = content.info
    GameUIRecruitWarpgate:UpdateEnergyBalls()
    GameUIRecruitWarpgate:UpdateResourceData()
    return true
  end
  return false
end
function GameUIRecruitWarpgate:ClearLocalData()
  self._WarpgateInfo = nil
end
function GameUIRecruitWarpgate:Update(dt)
  local flashObject = self:GetFlashObject()
  if flashObject and dt then
    flashObject:InvokeASCallback("_root", "onUpdateFrame", dt)
    flashObject:Update(dt)
  end
end
function GameUIRecruitWarpgate:GetItem2514Count()
  return ...
end
function GameUIRecruitWarpgate.RefreshResourceCount()
  local resource = GameGlobalData:GetData("item_count")
  local flash_obj = GameUIRecruitWarpgate:GetFlashObject()
  if flash_obj then
    local item_2514_count = GameUIRecruitWarpgate:GetItem2514Count()
    flash_obj:InvokeASCallback("_root", "lua2fs_updateResourceData", -1, -1, item_2514_count)
  end
end
