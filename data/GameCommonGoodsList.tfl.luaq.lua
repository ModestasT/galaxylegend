local GameCommonGoodsList = LuaObjectManager:GetLuaObject("GameCommonGoodsList")
local GameSetting = LuaObjectManager:GetLuaObject("GameSetting")
local StorePicDownloader = StorePicDownloader
local GameCommonBuyBox = LuaObjectManager:GetLuaObject("GameCommonBuyBox")
local GameUIKrypton = LuaObjectManager:GetLuaObject("GameUIKrypton")
local GameUICommonEvent = LuaObjectManager:GetLuaObject("GameUICommonEvent")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local GameUIAdvancedArenaLayer = require("data1/GameUIAdvancedArenaLayer.tfl")
GameCommonGoodsList.initList = false
GameCommonGoodsList.goodsList = nil
GameCommonGoodsList.waitDownloadInfo = {}
GameCommonGoodsList.CurItemDetail = nil
GameCommonGoodsList.TYPE = {wdc = 4}
GameCommonGoodsList.storeType = -1
GameCommonGoodsList.isSelectHero = false
GameCommonGoodsList.isShowCurItemDatail = false
function GameCommonGoodsList:OnInitGame()
end
function GameCommonGoodsList:SetID(exchange)
  GameCommonGoodsList.id1 = -1
  GameCommonGoodsList.id2 = -1
  GameCommonGoodsList.id3 = -1
  for i = 1, #exchange.items do
    GameCommonGoodsList["id" .. i] = exchange.items[i]
  end
end
GameCommonGoodsList.id1 = -1
GameCommonGoodsList.id2 = -1
GameCommonGoodsList.id3 = -1
GameCommonGoodsList.frame1 = "empty"
GameCommonGoodsList.frame2 = "empty"
GameCommonGoodsList.frame3 = "empty"
function GameCommonGoodsList:Init()
  GameCommonGoodsList.frame1 = "empty"
  GameCommonGoodsList.frame2 = "empty"
  GameCommonGoodsList.frame3 = "empty"
  self:GetIcons()
  self:RefreshResource()
  self:RefreshGoodsList()
  self:SetIcon()
  GameGlobalData:RegisterDataChangeCallback("item_count", self.RefreshResource)
  GameGlobalData:RegisterDataChangeCallback("resource", self.RefreshResource)
end
function GameCommonGoodsList:GetIcons()
  for i = 1, 3 do
    local id = "id" .. i
    local frame = "frame" .. i
    if self[id] ~= -1 and self[frame] == "empty" then
      if GameHelper:IsResource(self[id]) then
        self[frame] = GameHelper:GetAwardTypeIconFrameName("silver", nil, nil)
      elseif DynamicResDownloader:IsDynamicStuff(self[id], DynamicResDownloader.resType.PIC) then
        local fullFileName = DynamicResDownloader:GetFullName(self[id] .. ".png", DynamicResDownloader.resType.PIC)
        local localPath = "data2/" .. fullFileName
        if DynamicResDownloader:IfResExsit(localPath) then
          ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
          self[frame] = "item_" .. self[id]
        else
          self[frame] = "temp"
          self:AddDownloadPathWithIcon(self[id], id, frame)
        end
      else
        self[frame] = "item_" .. self[id]
      end
    end
  end
end
function GameCommonGoodsList:SetIcon()
  local flash_obj = GameCommonGoodsList:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "SetIcon", self.frame1, self.frame2, self.frame3)
end
function GameCommonGoodsList.RefreshResource()
  local resource = GameGlobalData:GetData("item_count")
  local resourceCount = GameGlobalData:GetData("resource")
  local flash_obj = GameCommonGoodsList:GetFlashObject()
  if flash_obj then
    local item_1 = 0
    local item_2 = 0
    local item_3 = 0
    DebugTable(resource.items)
    if GameHelper:IsResource(GameCommonGoodsList.id1) then
      item_1 = resourceCount[GameCommonGoodsList.id1]
    end
    if GameHelper:IsResource(GameCommonGoodsList.id2) then
      item_2 = resourceCount[GameCommonGoodsList.id2]
    end
    if GameHelper:IsResource(GameCommonGoodsList.id3) then
      item_3 = resourceCount[GameCommonGoodsList.id3]
    end
    for i, v in ipairs(resource.items) do
      if v.item_id == GameCommonGoodsList.id1 then
        item_1 = item_1 + v.item_no
      elseif v.item_id == GameCommonGoodsList.id2 then
        item_2 = item_2 + v.item_no
      elseif v.item_id == GameCommonGoodsList.id3 then
        item_3 = item_3 + v.item_no
      end
    end
    flash_obj:InvokeASCallback("_root", "RefreshResource", item_1, item_2, item_3)
  end
end
function GameCommonGoodsList:HideBuyConfirmWin()
  GameStateManager:GetCurrentGameState():EraseObject(GameCommonBuyBox)
  GameCommonGoodsList.CurItemDetail = nil
end
function GameCommonGoodsList:ShowBuyConfirmWin(item)
  GameCommonGoodsList.CurItemDetail = item
  if GameCommonGoodsList.CurItemDetail == nil then
    return
  end
  if GameCommonGoodsList.CurItemDetail.sp_type == 4 then
    DebugOut("zm test: is hero")
    GameCommonGoodsList.isSelectHero = true
  else
    DebugOut("zm test: not hero")
    GameCommonGoodsList.isSelectHero = false
  end
  local function callback(msgType, content)
    if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.choosable_item_req.Code then
      GameStateManager:GetCurrentGameState():AddObject(GameCommonBuyBox)
      return true
    elseif msgType == NetAPIList.choosable_item_ack.Code then
      GameCommonGoodsList.choosable_items = content.choosable_items
      if GameCommonGoodsList.choosable_items and #GameCommonGoodsList.choosable_items > 1 then
        for i, v in ipairs(GameCommonGoodsList.choosable_items) do
          v.cnt = GameUtils.numberConversion(v.no)
          v.iconframe = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(v)
          local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
          v.info = ItemBox:getItemInfo(v, v.ItemBox)
        end
        GameCommonGoodsList.choosable_items[1].txt_showbelow = GameLoader:GetGameText("LC_MENU_CHOOSE_ONE_BELOW")
        DebugOut("xxpp callback choosable_items", DebugCommand.t_tostring(GameCommonGoodsList.choosable_items, 8))
      else
        GameCommonGoodsList.choosable_items = nil
      end
      GameStateManager:GetCurrentGameState():AddObject(GameCommonBuyBox)
      return true
    end
    return false
  end
  local param = {
    id = GameCommonGoodsList.CurItemDetail.item_id
  }
  GameCommonGoodsList.choosable_items = nil
  NetMessageMgr:SendMsg(NetAPIList.choosable_item_req.Code, param, callback, true, nil)
end
function GameCommonGoodsList:OnAddToGameState()
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  self:Init()
  self:MoveIn()
  GameCommonGoodsList.goodsList = nil
  self:TrySendGoodsListReq()
end
function GameCommonGoodsList:OnEraseFromGameState()
  self:UnloadFlashObject()
end
function GameCommonGoodsList:IsDownloadPNGExsit(filename)
  return ...
end
function GameCommonGoodsList:MoveIn()
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "animationMoveIn")
end
function GameCommonGoodsList:MoveOut()
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "animationMoveOut")
end
function GameCommonGoodsList:Update(dt)
  self:GetFlashObject():InvokeASCallback("_root", "OnUpdate")
  self:GetFlashObject():Update(dt)
end
function GameCommonGoodsList:OnFSCommand(cmd, arg)
  DebugOut("store command: ", cmd)
  if cmd == "close_menu" then
    GameUIAdvancedArenaLayer.isShowStore = false
    GameCommonGoodsList.storeType = -1
    GameStateManager:GetCurrentGameState():EraseObject(GameCommonGoodsList)
    GameCommonGoodsList:UnloadFlashObject()
  elseif cmd == "needUpdateShopItem" then
    if GameCommonGoodsList.storeType == GameCommonGoodsList.TYPE.wdc then
      self:UpdateGoodsItemWdc(arg)
    else
      self:UpdateGoodsItem(arg)
    end
  elseif cmd == "shopItemReleased" then
    local itemIndex = tonumber(arg)
    self:QueryBuyItem(itemIndex)
    self.isShowCurItemDatail = true
  end
end
function GameCommonGoodsList:QueryBuyItem(itemIndex)
  DebugStore("item released: ", itemIndex)
  GameCommonGoodsList:ShowBuyConfirmWin(GameCommonGoodsList.goodsList.items[itemIndex])
end
function GameCommonGoodsList:updateCommonGoodsListInfo(id, count)
  DebugOut("store_id = " .. id)
  for k, v in ipairs(GameCommonGoodsList.goodsList.items) do
    if v.id == id then
      v.day_buy_left = v.day_buy_left - count
      v.all_buy_left = v.all_buy_left - count
    end
  end
end
function GameCommonGoodsList:UpdateGoodsItemWdc(Id)
  local itemKey = tonumber(Id)
  local name, frame = "", ""
  local item_number = ""
  local price1, price2, price3 = "", "", ""
  local v1, v2
  for k, v in ipairs(GameCommonGoodsList.goodsList.items) do
    if math.ceil(k / 2) == itemKey then
      local p1, p2, p3 = "0", "0", "0"
      for k1, v1 in ipairs(v.items) do
        DebugOut("=====item count", v1.item_no)
        p1 = "" .. v1.item_no
      end
      price1 = price1 .. p1 .. "\001"
      price2 = price2 .. p2 .. "\001"
      price3 = price3 .. p3 .. "\001"
      v.price1 = p1
      v.price2 = p2
      v.price3 = p3
      v.name = ""
      v.desc = ""
      item_number = item_number .. v.item_no .. "\001"
      local isDynamic = false
      if DynamicResDownloader:IsDynamicStuff(v.item_id, DynamicResDownloader.resType.PIC) then
        isDynamic = true
        local fullFileName = DynamicResDownloader:GetFullName(v.item_id .. ".png", DynamicResDownloader.resType.PIC)
        local localPath = "data2/" .. fullFileName
        if DynamicResDownloader:IfResExsit(localPath) then
          ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
          frame = frame .. "item_" .. v.item_id .. "\001"
        else
          frame = frame .. "temp" .. "\001"
          if v1 == nil then
            self:AddDownloadPath(v.item_id, itemKey, 1)
          else
            self:AddDownloadPath(v.item_id, itemKey, 2)
          end
        end
      else
        frame = frame .. "item_" .. v.item_id .. "\001"
      end
      v.name = GameLoader:GetGameText("LC_ITEM_ITEM_NAME_" .. v.item_id)
      if v.item_type == "krypton" then
        local detail = GameUIKrypton:TryQueryKryptonDetail(v.item_id, 1, function(msgType, content)
          local ret = GameUIKrypton.QueryKryptonDetailCallBack(msgType, content)
          if ret then
            local tmp = GameGlobalData:GetKryptonDetail(v.item_id, 1)
            GameCommonGoodsList:SetKryptonDetail(v, tmp)
          end
          return ret
        end)
        if detail == nil then
        else
          GameCommonGoodsList:SetKryptonDetail(v, detail)
        end
      else
        v.desc = GameLoader:GetGameText("LC_ITEM_ITEM_DESC_" .. v.item_id)
      end
      name = name .. v.name .. "\001"
      if v1 == nil then
        v1 = v
      else
        v2 = v
      end
    end
  end
  self:GetFlashObject():InvokeASCallback("_root", "setItem", itemKey, name, frame, item_number, price1, price2, price3, self.frame1, self.frame2, self.frame3)
end
function GameCommonGoodsList:UpdateGoodsItem(Id)
  local itemKey = tonumber(Id)
  local name, frame = "", ""
  local price1, price2, price3 = "", "", ""
  local item_number = ""
  local v1, v2
  for k, v in ipairs(GameCommonGoodsList.goodsList.items) do
    if math.ceil(k / 2) == itemKey then
      local p1, p2, p3 = "0", "0", "0"
      for k1, v1 in ipairs(v.items) do
        DebugOut("=====item id", v1.item_id)
        DebugOut("=====item count", v1.item_no)
        if v1.item_id == self.id1 then
          p1 = "" .. v1.item_no
        elseif v1.item_id == self.id2 then
          p2 = "" .. v1.item_no
        elseif v1.item_id == self.id3 then
          p3 = "" .. v1.item_no
        end
      end
      price1 = price1 .. p1 .. "\001"
      price2 = price2 .. p2 .. "\001"
      price3 = price3 .. p3 .. "\001"
      v.price1 = p1
      v.price2 = p2
      v.price3 = p3
      v.name = ""
      v.desc = ""
      item_number = item_number .. v.item_no .. "\001"
      local isDynamic = false
      if DynamicResDownloader:IsDynamicStuff(v.item_id, DynamicResDownloader.resType.PIC) then
        isDynamic = true
        local fullFileName = DynamicResDownloader:GetFullName(v.item_id .. ".png", DynamicResDownloader.resType.PIC)
        local localPath = "data2/" .. fullFileName
        if DynamicResDownloader:IfResExsit(localPath) then
          ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
          frame = frame .. "item_" .. v.item_id .. "\001"
        else
          frame = frame .. "temp" .. "\001"
          if v1 == nil then
            self:AddDownloadPath(v.item_id, itemKey, 1)
          else
            self:AddDownloadPath(v.item_id, itemKey, 2)
          end
        end
      else
        frame = frame .. "item_" .. v.item_id .. "\001"
      end
      v.name = GameLoader:GetGameText("LC_ITEM_ITEM_NAME_" .. v.item_id)
      if v.item_type == "krypton" then
        local detail = GameUIKrypton:TryQueryKryptonDetail(v.item_id, 1, function(msgType, content)
          local ret = GameUIKrypton.QueryKryptonDetailCallBack(msgType, content)
          if ret then
            local tmp = GameGlobalData:GetKryptonDetail(v.item_id, 1)
            GameCommonGoodsList:SetKryptonDetail(v, tmp)
          end
          return ret
        end)
        if detail == nil then
        else
          GameCommonGoodsList:SetKryptonDetail(v, detail)
        end
      else
        v.desc = GameLoader:GetGameText("LC_ITEM_ITEM_DESC_" .. v.item_id)
      end
      name = name .. v.name .. "\001"
      if v1 == nil then
        v1 = v
      else
        v2 = v
      end
    end
  end
  self:GetFlashObject():InvokeASCallback("_root", "setItem", itemKey, name, frame, item_number, price1, price2, price3, self.frame1, self.frame2, self.frame3)
end
function GameCommonGoodsList:SetKryptonDetail(item, krypton)
  local addon1 = krypton.addon[1]
  local addon2 = krypton.addon[2]
  if addon1.type == 20 then
    item.desc = GameLoader:GetGameText("LC_MENU_Equip_param_" .. addon1.type)
  else
    local paramNum, paramText = "", ""
    paramNum = addon1.value
    paramText = GameLoader:GetGameText("LC_MENU_Equip_param_" .. addon1.type)
    if addon1.type >= 9 and addon1.type <= 14 or addon1.type == 17 then
      paramNum = paramNum / 10 .. "%"
    end
    item.desc = paramText .. "+" .. paramNum
    if addon2 ~= nil then
      item.desc = item.desc .. "\n"
      paramNum = addon2.value
      paramText = GameLoader:GetGameText("LC_MENU_Equip_param_" .. addon2.type)
      if addon2.type >= 9 and addon2.type <= 14 or addon2.type == 17 then
        paramNum = paramNum / 10 .. "%"
      end
      item.desc = item.desc .. paramText .. "+" .. paramNum
    end
  end
end
function GameCommonGoodsList:RefreshGoodsList()
  self:GetFlashObject():InvokeASCallback("_root", "clearListItem")
  if GameCommonGoodsList.goodsList ~= nil then
    local itemCount = math.ceil(#GameCommonGoodsList.goodsList.items / 2)
    self:GetFlashObject():InvokeASCallback("_root", "initListItem", itemCount)
    for i = 1, itemCount do
      self:GetFlashObject():InvokeASCallback("_root", "addListItem", i)
    end
    self:GetFlashObject():InvokeASCallback("_root", "SetArrowInitVisible")
  end
end
function GameCommonGoodsList:TrySendGoodsListReq()
  local reqType = self:GetStoreType()
  local stores_req_content = {
    store_type = reqType,
    locale = GameSettingData.Save_Lang
  }
  NetMessageMgr:SendMsg(NetAPIList.activity_stores_req.Code, stores_req_content, GameCommonGoodsList.GoodsListCallback, true, nil)
end
function GameCommonGoodsList:GetStoreType()
  local reqType = GameUICommonEvent.NetMegType.FESTIVAL
  if GameUICommonEvent.curDetailType == GameUICommonEvent.DetailType.WD_EVENT then
    reqType = GameUICommonEvent.NetMegType.WD_EVENT
  end
  if GameCommonGoodsList.storeType == GameCommonGoodsList.TYPE.wdc then
    reqType = GameCommonGoodsList.TYPE.wdc
  end
  return reqType
end
function GameCommonGoodsList:SetStoreType(mtype)
  self.storeType = mtype
end
function GameCommonGoodsList.GoodsListCallback(msgType, content)
  if msgType == NetAPIList.activity_stores_ack.Code then
    local initList = GameCommonGoodsList.goodsList == nil
    for i = #content.items, 1, -1 do
      if content.items[i].item_type == "fleet" then
        table.remove(content.items, i)
      end
    end
    GameCommonGoodsList.goodsList = content
    if initList then
      GameCommonGoodsList:RefreshGoodsList()
    elseif GameCommonGoodsList:GetFlashObject() then
      GameCommonGoodsList:GetFlashObject():InvokeASCallback("_root", "RefreshListBox")
    end
    return true
  end
  return false
end
function GameCommonGoodsList:AddDownloadPathWithIcon(itemID, id, frame)
  local resName = itemID .. ".png"
  local extInfo = {}
  extInfo.item_id = itemID
  extInfo.localPath = "data2/LAZY_LOAD_dynamic_" .. resName
  extInfo.id = id
  extInfo.frame = frame
  DynamicResDownloader:AddDynamicRes(resName, DynamicResDownloader.resType.PIC, extInfo, GameCommonGoodsList.donamicDownloadFinishCallback2)
end
function GameCommonGoodsList.donamicDownloadFinishCallback2(extInfo)
  if DynamicResDownloader:IfResExsit(extInfo.localPath) and GameCommonGoodsList:GetFlashObject() then
    GameCommonGoodsList[extInfo.frame] = "item_" .. GameCommonGoodsList[extInfo.id]
    GameCommonGoodsList:SetIcon()
    GameCommonGoodsList:RefreshGoodsList()
  end
  if GameStateManager:GetCurrentGameState():IsObjectInState(GameCommonBuyBox) then
    GameCommonBuyBox:DisplayDetail()
  end
  return true
end
function GameCommonGoodsList:AddDownloadPath(itemID, itemKey, index)
  local resName = itemID .. ".png"
  local extInfo = {}
  extInfo.itemKey = itemKey
  extInfo.index = index
  extInfo.item_id = itemID
  extInfo.localPath = "data2/LAZY_LOAD_dynamic_" .. resName
  DynamicResDownloader:AddDynamicRes(resName, DynamicResDownloader.resType.PIC, extInfo, GameCommonGoodsList.donamicDownloadFinishCallback)
end
function GameCommonGoodsList.donamicDownloadFinishCallback(extInfo)
  if DynamicResDownloader:IfResExsit(extInfo.localPath) and GameCommonGoodsList:GetFlashObject() then
    local frameName = "item_" .. extInfo.item_id
    GameCommonGoodsList:GetFlashObject():InvokeASCallback("_root", "SetItemFrame", extInfo.itemKey, extInfo.index, frameName)
  end
  if GameStateManager:GetCurrentGameState():IsObjectInState(GameCommonBuyBox) then
    GameCommonBuyBox:UpdateDisplayIcon()
  end
  return true
end
function GameCommonGoodsList:AndroidBack()
  if self.isShowCurItemDatail then
    GameCommonBuyBox:MoveOut("close_menu")
  else
    GameCommonGoodsList:MoveOut()
  end
end
