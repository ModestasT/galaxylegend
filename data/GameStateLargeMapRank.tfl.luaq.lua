local GameStateLargeMapRank = GameStateManager.GameStateLargeMapRank
local GameUILargeMapRank = LuaObjectManager:GetLuaObject("GameUILargeMapRank")
function GameStateLargeMapRank:InitGameState()
  self.m_preState = nil
end
function GameStateLargeMapRank:OnFocusGain(previousState)
  self.m_preState = previousState
  self:AddObject(GameUILargeMapRank)
end
function GameStateLargeMapRank:OnFocusLost(newState)
  DebugOutBattlePlay("GameStateLargeMapRank:OnFocusLost")
  self:EraseObject(GameUILargeMapRank)
  self.m_preState = nil
end
