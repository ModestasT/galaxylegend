local GameStateAlliance = GameStateManager.GameStateAlliance
local GameObjectCosmicExpedition = LuaObjectManager:GetLuaObject("GameObjectCosmicExpedition")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
local GameUIUserAlliance = LuaObjectManager:GetLuaObject("GameUIUserAlliance")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local NetMessageMgr = NetMessageMgr
GameObjectCosmicExpedition.ExistNum = 0
function GameObjectCosmicExpedition:OnAddToGameState()
  self:LoadFlashObject()
  GameObjectCosmicExpedition:Init()
end
function GameObjectCosmicExpedition:OnEraseFromGameState()
  self:UnloadFlashObject()
end
function GameObjectCosmicExpedition:Init()
  GameObjectCosmicExpedition:GetFlashObject():InvokeASCallback("_root", "Init")
  GameGlobalData:RegisterDataChangeCallback("explore_players", GameObjectCosmicExpedition.RefreshPlayerInfo)
  GameObjectCosmicExpedition.isPlay = false
  local data_alliance = GameGlobalData:GetData("alliance")
  local content = {
    id = data_alliance.id
  }
  DebugOut("\230\137\147\229\141\176----GameObjectCosmicExpedition")
  NetMessageMgr:SendMsg(NetAPIList.block_devil_info_req.Code, content, GameObjectCosmicExpedition.ServerCallBack, true, nil)
  local price_content = {price_type = "explore", type = 0}
  NetMessageMgr:SendMsg(NetAPIList.price_req.Code, price_content, GameObjectCosmicExpedition.RequestPriceCallBack, true, nil)
  self:GetFlashObject():InvokeASCallback("_root", "setRollTextInfo", GameUIUserAlliance:GetRollTextString())
end
function GameObjectCosmicExpedition:MoveIn()
  self:GetFlashObject():InvokeASCallback("_root", "moveInAll")
end
function GameObjectCosmicExpedition.JoinCosmicExpedition()
  local content = {
    type = GameObjectCosmicExpedition.join_type
  }
  if tonumber(GameObjectCosmicExpedition.join_type) == 1 then
    content.type = 1
  end
  NetMessageMgr:SendMsg(NetAPIList.block_devil_req.Code, content, GameObjectCosmicExpedition.JoinInCallback, true, nil)
end
function GameObjectCosmicExpedition.ServerCallBack(msgType, content)
  DebugTable(content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.block_devil_info_req.Code then
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.block_devil_ntf.Code then
    DebugOut("ServerCallBack")
    DebugTable(content)
    GameObjectCosmicExpedition.playerDate = content.players
    GameObjectCosmicExpedition.prestige = content.prestige
    GameObjectCosmicExpedition.award = content.award
    GameObjectCosmicExpedition:UpdateCosmicExpeditionInfo()
    GameObjectCosmicExpedition:MoveIn()
    return true
  end
  return false
end
GameObjectCosmicExpedition.isPlay = false
function GameObjectCosmicExpedition.JoinInCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.block_devil_req.Code then
    if content.code ~= 0 then
      local GameVip = LuaObjectManager:GetLuaObject("GameVip")
      GameVip:CheckIsNeedShowVip(content.code)
    elseif #GameObjectCosmicExpedition.playerDate >= 5 then
      GameObjectCosmicExpedition.isPlay = true
      GameObjectCosmicExpedition:GetFlashObject():InvokeASCallback("_root", "playTeamFull")
    else
      local GameVip = LuaObjectManager:GetLuaObject("GameVip")
      GameVip:CheckIsNeedShowVip(content.code)
    end
    return true
  end
  return false
end
function GameObjectCosmicExpedition.RequestPriceCallBack(msgType, content)
  if msgType == NetAPIList.price_ack.Code and content.price_type == "explore" then
    GameObjectCosmicExpedition.price = content.price
    GameObjectCosmicExpedition:GetFlashObject():InvokeASCallback("_root", "setPrice", content.price)
    return true
  end
  return false
end
GameObjectCosmicExpedition.LastReCallTime = 0
GameObjectCosmicExpedition.ReCallInterval = 600
function GameObjectCosmicExpedition:OnFSCommand(cmd, arg)
  if cmd == "onClose" then
    GameUIUserAlliance:SetVisible(nil, true)
  elseif cmd == "playOver" then
    GameObjectCosmicExpedition.isPlay = false
    GameObjectCosmicExpedition:GetFlashObject():InvokeASCallback("_root", "playTeamOver")
    GameObjectCosmicExpedition:UpdateCosmicExpeditionInfo()
    local GameVip = LuaObjectManager:GetLuaObject("GameVip")
    GameVip:CheckIsNeedShowVip(0)
  elseif cmd == "onErase" then
    GameStateManager:GetCurrentGameState():EraseObject(GameObjectCosmicExpedition)
  elseif cmd == "btn_join" then
    if not GameObjectCosmicExpedition.isPlay then
      GameObjectCosmicExpedition.join_type = arg
      if tonumber(arg) == 2 then
        GameObjectCosmicExpedition.JoinCosmicExpedition()
      elseif tonumber(arg) == 1 and GameObjectCosmicExpedition.price and 0 < GameObjectCosmicExpedition.price then
        local info = string.format(GameLoader:GetGameText("LC_MENU_COST_CREDIT_ALERT_CHAR"), GameObjectCosmicExpedition.price)
        GameUtils:CreditCostConfirm(info, function()
          GameObjectCosmicExpedition.JoinCosmicExpedition()
        end)
      end
    end
  elseif cmd == "onMoveInOver" then
    GameUIUserAlliance:SetVisible(nil, false)
  elseif cmd == "exploreCall" and not GameObjectCosmicExpedition.isPlay then
    if os.time() - GameObjectCosmicExpedition.LastReCallTime < GameObjectCosmicExpedition.ReCallInterval then
      local timeString = GameUtils:formatTimeStringAsPartion(GameObjectCosmicExpedition.ReCallInterval - (os.time() - GameObjectCosmicExpedition.LastReCallTime))
      local msg = GameLoader:GetGameText("LC_MENU_COSMIC_EXPEDITON_CALL_CD_ALERT")
      msg = string.format(msg, timeString)
      GameTip:Show(msg, 2000)
    else
      local data_alliance = GameGlobalData:GetData("alliance")
      local chat_req = {
        content = GameLoader:GetGameText("LC_MENU_COSMIC_EXPEDITON_CALL_CHAT"),
        receiver = "",
        channel = "alchat_" .. data_alliance.id,
        show_effect = GameSettingData.EnableChatEffect and 1 or 0
      }
      NetMessageMgr:SendMsg(NetAPIList.chat_req.Code, chat_req, self.NetCallbackSendMessage, true, nil)
    end
  end
end
function GameObjectCosmicExpedition:UpdateCosmicExpeditionInfo()
  if not GameObjectCosmicExpedition.playerDate then
    return
  end
  if not GameObjectCosmicExpedition:GetFlashObject() then
    return
  end
  local name = ""
  local fleetIcon = ""
  local state = ""
  for i = 1, 6 do
    if GameObjectCosmicExpedition.playerDate[i] then
      if tonumber(GameObjectCosmicExpedition.playerDate[i].state) == 1 then
        fleetIcon = fleetIcon .. "ship17" .. "\001"
        name = name .. GameLoader:GetGameText("LC_MENU_COSMIC_EXPEDITON_NPC_NAME_CHAR") .. "\001"
      else
        name = name .. GameUtils:GetUserDisplayName(GameObjectCosmicExpedition.playerDate[i].name) .. "\001"
        fleetIcon = fleetIcon .. "ship17" .. "\001"
      end
      state = state .. GameObjectCosmicExpedition.playerDate[i].state .. "\001"
    else
      name = name .. " " .. "\001"
      fleetIcon = fleetIcon .. "empty" .. "\001"
      state = state .. 4 .. "\001"
    end
  end
  local prestige = tonumber(GameObjectCosmicExpedition.prestige)
  local desc = string.format(GameLoader:GetGameText("LC_MENU_COSMIC_EXPEDITON_DES_CHAR"), prestige)
  local lang = "en"
  if GameSettingData and GameSettingData.Save_Lang then
    lang = GameSettingData.Save_Lang
    if string.find(lang, "ru") == 1 then
      lang = "ru"
    end
  end
  GameObjectCosmicExpedition:GetFlashObject():InvokeASCallback("_root", "initFlashObject", name, fleetIcon, state, desc, lang)
  local awardIcon = {}
  local awardText = {}
  DebugWD("UpdateCosmicExpeditionInfo")
  DebugWDTable(GameObjectCosmicExpedition.award)
  for key, v in pairs(GameObjectCosmicExpedition.award) do
    awardText[key] = GameHelper:GetAwardNameText(v.item_type, v.number)
    awardIcon[key] = GameHelper:GetAwardTypeIconFrameName(v.item_type, v.number, v.no)
  end
  DebugWD(awardText[1], awardText[2], awardText[3], awardIcon[1], awardIcon[2], awardIcon[3])
  GameObjectCosmicExpedition:GetFlashObject():InvokeASCallback("_root", "setAward", awardText[1], awardText[2], awardText[3], awardIcon[1], awardIcon[2], awardIcon[3])
end
GameObjectCosmicExpedition.lastPlayer = 0
function GameObjectCosmicExpedition.RefreshPlayerInfo(content)
  if not content then
    return
  end
  GameObjectCosmicExpedition.playerDate = content.players
  GameObjectCosmicExpedition.prestige = content.prestige
  GameObjectCosmicExpedition.award = content.award
  if not GameObjectCosmicExpedition.isPlay then
    GameObjectCosmicExpedition:UpdateCosmicExpeditionInfo()
    if GameObjectCosmicExpedition.lastPlayer == 5 and #GameObjectCosmicExpedition.playerDate == 0 and GameObjectCosmicExpedition:GetFlashObject() then
      local GameVip = LuaObjectManager:GetLuaObject("GameVip")
      GameVip:CheckIsNeedShowVip(0)
    end
  end
  GameObjectCosmicExpedition.lastPlayer = #GameObjectCosmicExpedition.playerDate
end
function GameObjectCosmicExpedition.GetExploreReward(content)
  local crditText = GameLoader:GetGameText("LC_MENU_CREDITS_COLON")
  local crditNum = content.credit
  local prestigeText = GameLoader:GetGameText("LC_MENU_LOOT_PRESTIGE")
  local prestigeNum = content.prestige
  local eventText = GameLoader:GetGameText("LC_MENU_COSMIC_EXPEDITON_TITLE")
  if prestigeNum and tonumber(prestigeNum) > 0 then
    local title_pretige = GameHelper:GetRewardInfoAndEvent(prestigeText, prestigeNum .. "", eventText)
    GameTip:Show(title_pretige)
  end
  if crditNum and tonumber(crditNum) > 0 then
    local title_credit = GameHelper:GetRewardInfoAndEvent(crditText, crditNum .. "", eventText)
    GameTip:Show(title_credit)
  end
end
function GameObjectCosmicExpedition:Update(dt)
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    flash_obj:Update(dt)
    flash_obj:InvokeASCallback("_root", "updateRollTextPos", dt)
  end
end
function GameObjectCosmicExpedition.NetCallbackSendMessage(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.chat_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    else
      GameTip:Show(GameLoader:GetGameText("LC_MENU_COSMIC_EXPEDITON_CALL_SUCCESS_ALERT"))
      GameObjectCosmicExpedition.LastReCallTime = os.time()
    end
    return true
  end
  return false
end
if AutoUpdate.isAndroidDevice then
  function GameObjectCosmicExpedition.OnAndroidBack()
    GameObjectCosmicExpedition:GetFlashObject():InvokeASCallback("_root", "moveOutAll")
  end
end
