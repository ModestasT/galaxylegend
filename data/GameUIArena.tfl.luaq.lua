local NetMessageMgr = NetMessageMgr
local NetAPIList = NetAPIList
local GameTimer = GameTimer
local AlertDataList = AlertDataList
local GameGlobalData = GameGlobalData
local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
local GameUIArena = LuaObjectManager:GetLuaObject("GameUIArena")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
local GameUIPrestigeRankUp = LuaObjectManager:GetLuaObject("GameUIPrestigeRankUp")
local GameUIBarRight = LuaObjectManager:GetLuaObject("GameUIBarRight")
local GameStateArena = GameStateManager.GameStateArena
local GameStateBattlePlay = GameStateManager.GameStateBattlePlay
local GameStatePlayerMatrix = GameStateManager.GameStatePlayerMatrix
local QuestTutorialArena = TutorialQuestManager.QuestTutorialArena
local QuestTutorialArenaReward = TutorialQuestManager.QuestTutorialArenaReward
local GameUICommonDialog = LuaObjectManager:GetLuaObject("GameUICommonDialog")
local GameUIBarRight = LuaObjectManager:GetLuaObject("GameUIBarRight")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local GameObjectFormationBackground = LuaObjectManager:GetLuaObject("GameObjectFormationBackground")
local GameUIMaskLayer = LuaObjectManager:GetLuaObject("GameUIMaskLayer")
local GameUIAdvancedArenaLayer = require("data1/GameUIAdvancedArenaLayer.tfl")
require("FleetMatrix.tfl")
require("GameUIGalaxyArena.tfl")
require("GameUITeamLeagueCup.tfl")
local FightCDTime = 0
local AwardCDTime = 0
local CanGetAward = false
local MessageDisplayTime = 0
GameUIArena.AWARD_TYPE = {RANK = "rank", SUCCESSIVE = "successive"}
GameUIArena.enemyInfo = nil
GameUIArena.mShareArenaBreakThroughChecked = true
GameUIArena.mShareArenaDayRewardChecked = true
local BattleResultType = {}
BattleResultType.you_challenge_opponent_win = 11
BattleResultType.you_challenge_opponent_failed = 12
BattleResultType.opponent_challenge_you_win = 21
BattleResultType.opponent_challenge_you_failed = 22
GameUIArena.rankRewardList = {
  1,
  5,
  10,
  30,
  50,
  100,
  200,
  300,
  400,
  500,
  1000,
  2000
}
GameUIArena.mShareArenaDayRewardChecked = true
GameUIArena.TLCUnlockLevel = 60
GameUIArena.ARENA_TYPE = {
  enterLayer = 0,
  normalArena = 1,
  wdc = 2,
  tlc = 3
}
GameUIArena.wdc_windowType = {
  noWindow = 0,
  reward = 1,
  leaderboard = 2,
  honor = 3,
  report = 4
}
GameUIArena.tlc_windowType = {
  noWindow = 0,
  reward = 1,
  leaderboard = 2,
  honor = 3,
  report = 4
}
GameUIArena.windowType = GameUIArena.wdc_windowType.noWindow
GameUIArena.mCurrentArenaType = GameUIArena.ARENA_TYPE.enterLayer
GameUIArena.mPreArenaType = nil
GameUIArena.hasCheckBattle = false
GameUIArena.isShowMatrix = false
GameUIArena.nextChallengeTime = 0
GameUIArena.nextChallengeTime_tlc = 0
function GameUIArena:OnInitGame()
  GameGlobalData:RegisterDataChangeCallback("pvp_supply", GameUIArena.PVPSupplyNotifyCallback)
  GameGlobalData:RegisterDataChangeCallback("wdc_supply", GameUIArena.updateWdcSupplyCallback)
  GameGlobalData:RegisterDataChangeCallback("tlc_supply", GameUIArena.updateTlcSupplyCallback)
  GameGlobalData:RegisterDataChangeCallback("resource", self.updateResourceData)
  GameGlobalData:RegisterDataChangeCallback("fleetinfo", self.UpdateFightingCapacityText)
  GameGlobalData:RegisterDataChangeCallback("matrix", self.UpdateFightingCapacityText)
end
function GameUIArena:ConfirmRestFightCDTime()
  NetMessageMgr:SendMsg(NetAPIList.champion_reset_cd_req.Code, null, self.resetChampionCallback, true, nil)
end
function GameUIArena:OnAddToGameState(parent_state)
  if GameUIArena.mCurrentArenaType == GameUIArena.ARENA_TYPE.enterLayer then
  elseif GameUIArena.mCurrentArenaType == GameUIArena.ARENA_TYPE.normalArena then
    GameUIArena:EnterNormalArena()
  elseif GameUIArena.mCurrentArenaType == GameUIArena.ARENA_TYPE.wdc then
    GameUIArena:EnterAdvancedArena()
    if GameUIArena.currentMenu == "GalaxyArenaReport" then
      GameUIArena:RequestGalaxyArenaReport()
    end
  elseif GameUIArena.mCurrentArenaType == GameUIArena.ARENA_TYPE.tlc then
    GameUIArena:EnterTeamLeagueCup()
  end
end
function GameUIArena.wdcEndNtf(content)
  if GameUIArena.mCurrentArenaType == GameUIArena.ARENA_TYPE.wdc and GameStateManager:GetCurrentGameState() == GameStateManager.GameStateArena and (content.status == 0 or content.status == 3) then
    GameTip:Show(GameLoader:GetGameText("LC_MENU_GLC_SEASON_END_TIP"))
    GameStateArena:Quit()
  end
end
function GameUIArena.tlcEndNtf(content)
  if GameUIArena.mCurrentArenaType == GameUIArena.ARENA_TYPE.tlc and GameStateManager:GetCurrentGameState() == GameStateManager.GameStateArena and (content.status == 0 or content.status == 3) then
    GameTip:Show(GameLoader:GetGameText("LC_MENU_TLC_SEASON_END_TIP"))
    GameStateArena:Quit()
  end
end
function GameUIArena:EnterNormalArena()
  if not GameUIArena:GetFlashObject() then
    GameUIArena:LoadFlashObject()
  end
  self:SetArenaType(GameUIArena.ARENA_TYPE.normalArena)
  self.is_active = true
  self:Visible(false)
  NetMessageMgr:SendMsg(NetAPIList.champion_list_req.Code, nil, self.championListCallback, true)
  NetMessageMgr:SendMsg(NetAPIList.champion_top_req.Code, nil, self.TopPlayerCallback, true, nil)
  GameUIArena:getTutorialHelpPos()
end
function GameUIArena:EnterAdvancedArena()
  if not GameUIArena:GetFlashObject() then
    GameUIArena:LoadFlashObject()
  end
  self:SetArenaType(GameUIArena.ARENA_TYPE.wdc)
  GameUIAdvancedArenaLayer:OnEnter()
end
function GameUIArena:EnterTeamLeagueCup()
  if not GameUIArena:GetFlashObject() then
    GameUIArena:LoadFlashObject()
  end
  self:SetArenaType(GameUIArena.ARENA_TYPE.tlc)
  TeamLeagueCup:OnEnter()
end
function GameUIArena:SetArenaType(mtype)
  GameUIArena.mCurrentArenaType = mtype
end
function GameUIArena:SetPreArenaType(mtype)
  GameUIArena.mPreArenaType = mtype
end
function GameUIArena:CleanPreArenaType()
  GameUIArena.mPreArenaType = nil
end
function GameUIArena:OnEraseFromGameState(old_parent)
  self:UnloadFlashObject()
  self:ClearLocalData()
  self.is_active = nil
  ItemBox.currentBox = nil
  GameUIArena.isOnShowReward = nil
  GameUIArena.isOnShowHistory = nil
  GameUIArena.isOnShowTopRank = nil
  if QuestTutorialArena:IsActive() and immanentversion == 1 and GameUIArena.mCurrentArenaType == GameUIArena.ARENA_TYPE.normalArena then
    QuestTutorialArena:SetFinish(true)
    GameUIBarRight:GetFlashObject():InvokeASCallback("_root", "HideTutorialArena")
  end
end
function GameUIArena:IsActive()
  return self.is_active
end
function GameUIArena:Visible(is_visible, object_name)
  object_name = object_name or -1
  self:GetFlashObject():InvokeASCallback("_root", "setVisible", object_name, is_visible)
end
function GameUIArena:AnimationMoveInMain()
  self:GetFlashObject():InvokeASCallback("_root", "animationMoveInMain")
  GameUIArena:ChecQuestDialog()
end
function GameUIArena:ChecQuestDialog()
  if immanentversion == 2 then
    if QuestTutorialArenaReward:IsActive() and not GameUIArena.hasCheckBattle then
      local function callback()
        self:GetFlashObject():InvokeASCallback("_root", "ShowRewardTip")
      end
      GameUICommonDialog:PlayStory({51228}, callback)
    else
      self:GetFlashObject():InvokeASCallback("_root", "HideRewardTip")
    end
  end
end
function GameUIArena:AnimationMoveInToprank()
  assert(self._toprankUsers)
  self:GetFlashObject():InvokeASCallback("_root", "animationMoveInToprank", #self._toprankUsers)
end
function GameUIArena:AnimationMoveInHistory()
  assert(self._battleHistory)
  local item_count = #self._battleHistory > 6 and #self._battleHistory or 6
  DebugOut("item_count", item_count)
  self:GetFlashObject():InvokeASCallback("_root", "animationMoveInReportBox", item_count)
end
function GameUIArena:ShowTopPlayerReport()
  if not self.history_topplayer_data then
  end
  local count = 0
  for k, v in pairs(self.history_topplayer_data) do
    count = count + 1
    DebugOut("\230\137\147\229\141\176", k)
  end
  DebugOut("count = ", count)
  DebugOut("self.history_topplayer_data", #self.history_topplayer_data)
  self:GetFlashObject():InvokeASCallback("_root", "animationMoveInReportBox", count)
end
function GameUIArena:AnimationMoveInAward(player_rank, award, champion_rewards)
  local player_rank_str = GameUIArena:GetRankNumber(tonumber(player_rank))
  local next_award_text = GameLoader:GetGameText("LC_MENU_NEXT_REWARDS")
  local info_text = ""
  local facebookEnabled = false
  if award then
    if FacebookGraphStorySharer:IsStoryEnabled(FacebookGraphStorySharer.StoryType.STORY_TYPE_ARENA_DAY_AWARD) then
      facebookEnabled = true
    end
    info_text = GameLoader:GetGameText("LC_MENU_REWARD_RANK_CHAR")
  else
    info_text = GameLoader:GetGameText("LC_MENU_YOUR_RANK_CHAR")
  end
  self:GetFlashObject():InvokeASCallback("_root", "AnimationMoveInAwardBox", player_rank, player_rank_str, award, next_award_text, info_text)
  local money, prestige, credit = "", "", ""
  if champion_rewards then
    money, prestige, credit = champion_rewards.money, champion_rewards.prestige, champion_rewards.credit
  end
  local money_text = GameLoader:GetGameText("LC_MENU_LOOT_CUBIT")
  local prestige_text = GameLoader:GetGameText("LC_MENU_LOOT_PRESTIGE")
  local credit_text = GameLoader:GetGameText("LC_MENU_LOOT_CREDIT")
  self:GetFlashObject():InvokeASCallback("_root", "setPlayerReward", money_text, money, prestige_text, prestige, credit_text, credit, GameUIArena:GetRankType(player_rank), facebookEnabled, self.mShareArenaDayRewardChecked)
end
function GameUIArena:GetRankType(rank)
  local rank = tonumber(rank)
  local RankType = 1
  for i = 1, 12 do
    if rank <= GameUIArena.rankRewardList[i] then
      RankType = i
      break
    end
  end
  return RankType
end
function GameUIArena:GetRankReward(rank)
  local money = GameUIArena.champion_reward[rank + 1].money
  local prestige = GameUIArena.champion_reward[rank + 1].prestige
  local credit = GameUIArena.champion_reward[rank + 1].credit
  return math.floor(money), math.floor(prestige), math.floor(credit)
end
function GameUIArena:CheckRewardsIsCanGet()
  if not self:GetFlashObject() then
    return
  end
  if not GameUIArena.brief_info then
    return
  end
  local name_text = GameLoader:GetGameText("LC_MENU_REWARDS_CHAR")
  local canGet = GameUIArena.brief_info.award or GameUIArena.brief_info.straight_award
  GameUIArena:GetFlashObject():InvokeASCallback("_root", "updateRewardsIsTrue", canGet, name_text)
end
function GameUIArena:UpdateMainInfo(content)
  GameUIArena.brief_info = content.brief_info
  DebugOut("------UpdateMainInfo----")
  DebugTable(content.brief_info)
  local brief_info = GameUIArena.brief_info
  local user_info = GameGlobalData:GetUserInfo()
  local level_info = GameGlobalData:GetData("levelinfo")
  local resource = GameGlobalData:GetData("resource")
  local currentForce = GameHelper:GetFleetsForce()
  local player_main_fleet = GameGlobalData:GetFleetInfo(1)
  local player_avatar = ""
  local leaderlist = GameGlobalData:GetData("leaderlist")
  local curmatrixIndex = GameGlobalData.GlobalData.curMatrixIndex
  if leaderlist and curmatrixIndex then
    player_avatar = GameDataAccessHelper:GetFleetAvatar(leaderlist.leader_ids[curmatrixIndex], player_main_fleet.level)
  else
    player_avatar = GameUtils:GetPlayerAvatar(nil, player_main_fleet.level)
  end
  GameUIArena:UpdateUserInfo(GameUtils:GetUserDisplayName(user_info.name), level_info.level, brief_info.rank, player_avatar, currentForce, resource.credit)
  GameUIArena:SetSuccessiveCount()
  GameUIArena:CheckRewardsIsCanGet()
  GameUIArena:UpdateChallengeTimesSupply(brief_info.challenge_cnt, brief_info.max_challenge_cnt)
  local next_award_time
  if brief_info.next_award_time > 0 then
    next_award_time = os.time() + brief_info.next_award_time
  end
  GameUIArena:UpdateAwardCDTime(next_award_time)
  local next_challenge_time
  if 0 < brief_info.next_fight_time then
    next_challenge_time = os.time() + brief_info.next_fight_time
    DebugOut("next_challenge_time - ", next_challenge_time)
  end
  GameUIArena:UpdateChallengeCDTime(next_challenge_time)
  for _, opponentInfo in ipairs(content.users) do
    table.sort(opponentInfo.cached_fleets, function(a, b)
      return a.fleet_identity < b.fleet_identity
    end)
  end
  GameUIArena:UpdateOpponentList(content.users)
end
function GameUIArena:UpdateChallengeChargeMoney(creditNum)
  local flashObject = self:GetFlashObject()
  if flashObject then
    flashObject:InvokeASCallback("_root", "updateChallengeChargeMoney", tostring(creditNum))
  end
end
function GameUIArena:UpdateTopPlayer()
  local top_player_data = self._toprankUsers[1]
  self:GetFlashObject():InvokeASCallback("_root", "updateTopPlayer", GameUtils:GetUserDisplayName(top_player_data.name))
end
function GameUIArena:UpdateAwardCDTime(cd_time)
  self.award_cd_time = cd_time
  self:_UpdateAwardCDTime()
end
function GameUIArena:_UpdateAwardCDTime()
  local flashObject = self:GetFlashObject()
  if flashObject then
    local left_time = -1
    if self.award_cd_time then
      left_time = self.award_cd_time - os.time()
      if left_time < 0 then
        left_time = -1
        self.award_cd_time = nil
      end
    end
    flashObject:InvokeASCallback("_root", "updateAwardCDInfo", left_time)
  end
end
function GameUIArena:UpdateChallengeCDTime(cd_time)
  self.challenge_cd_time = cd_time
  self:_UpdateChallengeCDTime()
end
function GameUIArena:UpdateChallengeCDTime_tlc(cd_time)
  self.challenge_cd_time_tlc = cd_time
  self:_UpdateChallengeCDTime_tlc()
end
function GameUIArena:_UpdateChallengeCDTime()
  local flashObject = self:GetFlashObject()
  if flashObject then
    local left_time = -1
    local max_cd_time = 720
    if self.challenge_cd_time then
      left_time = self.challenge_cd_time - os.time()
      if left_time < 0 then
        left_time = -1
        self.challenge_cd_time = nil
      end
    end
    GameUIArena.nextChallengeTime = left_time
    if GameStateManager.GameStateArena:IsObjectInState(ItemBox) then
      ItemBox:UpdateChallegeTime(left_time)
    end
    flashObject:InvokeASCallback("_root", "updateChallengeCDInfo", left_time, max_cd_time)
  end
end
function GameUIArena:_UpdateChallengeCDTime_tlc()
  local flashObject = self:GetFlashObject()
  if flashObject then
    local left_time = -1
    local max_cd_time = 720
    if self.challenge_cd_time_tlc then
      left_time = self.challenge_cd_time_tlc - os.time()
      if left_time < 0 then
        left_time = -1
        self.challenge_cd_time_tlc = nil
      end
    end
    GameUIArena.nextChallengeTime_tlc = left_time
    if GameStateManager.GameStateArena:IsObjectInState(ItemBox) then
      ItemBox:UpdateChallegeTime_tlc(left_time)
    end
    flashObject:InvokeASCallback("_root", "updateChallengeCDInfo_tlc", left_time, max_cd_time)
  end
end
function GameUIArena:UpdateChallengeTimesSupply(current_times, max_times)
  if self:GetFlashObject() and GameUIArena.brief_info then
    GameUIArena:UpdateChallengeChargeMoney(GameUIArena.brief_info.exchange_cost)
    self:GetFlashObject():InvokeASCallback("_root", "updateChallengeTimesSupply", current_times, max_times)
  end
end
function GameUIArena:UpdateUserInfo(user_name, user_level, user_rank, user_avatar, user_fight_value, user_credit)
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    flash_obj:InvokeASCallback("_root", "updateUserInfo", user_name, user_level, tonumber(user_rank), user_avatar, GameUIArena:GetRankNumber(user_rank), GameUtils.numberConversion(user_fight_value), GameUtils.numberConversion(user_credit))
  end
end
function GameUIArena:GetRankNumber(rank)
  local rank = tonumber(rank)
  if rank < 1 then
    return
  end
  if rank > 9999 then
    rank = 9999
  end
  local rank_1000, rank_100, rank_10, rank_1 = 1, 1, 1, 1
  rank_1000 = tonumber(string.sub(rank / 1000, 1, 1)) + rank_1000
  rank = rank - (rank_1000 - 1) * 1000
  rank_100 = tonumber(string.sub(rank / 100, 1, 1)) + rank_100
  rank = rank - (rank_100 - 1) * 100
  rank_10 = tonumber(string.sub(rank / 10, 1, 1)) + rank_10
  rank_1 = tonumber(rank - (rank_10 - 1) * 10) + 1
  local rand_str = rank_1000 .. "\001" .. rank_100 .. "\001" .. rank_10 .. "\001" .. rank_1 .. "\001"
  return rand_str
end
function GameUIArena:UpdateOpponentList(opponent_list)
  local user_list = opponent_list
  DebugOut("user_list")
  DebugTable(user_list)
  local last_index = 0
  for index_opponent = 1, 6 do
    local data_opponent = user_list[index_opponent]
    if data_opponent then
      local commanders_data = ""
      for index_commander = 1, 5 do
        if #commanders_data > 0 then
          commanders_data = commanders_data .. ","
        end
        local commander_info = data_opponent.cached_fleets[index_commander]
        if commander_info then
          local fleetID = commander_info.fleet_identity
          local commander_avatar = ""
          if commander_info.fleet_identity == 1 then
            if data_opponent.icon == 0 or data_opponent.icon == 1 then
              commander_avatar = GameUtils:GetPlayerAvatarWithSex(data_opponent.icon, commander_info.level)
            else
              commander_avatar = GameDataAccessHelper:GetFleetAvatar(data_opponent.icon, commander_info.level)
              fleetID = data_opponent.icon
            end
          else
            commander_avatar = GameDataAccessHelper:GetFleetAvatar(commander_info.fleet_identity, commander_info.level)
          end
          local commander_type = GameDataAccessHelper:GetCommanderColorFrame(fleetID, commander_info.level)
          commanders_data = commanders_data .. commander_type .. "_" .. commander_avatar
        else
          commanders_data = commanders_data .. "empty"
        end
      end
      local force = data_opponent.force
      GameUIArena:UpdateOpponentItem(index_opponent, data_opponent.rank, GameUtils:GetUserDisplayName(data_opponent.name), data_opponent.level, commanders_data, force)
      last_index = last_index + 1
    else
      GameUIArena:UpdateOpponentItem(index_opponent, -1, -1, -1, -1, 0)
    end
  end
  if immanentversion == 2 then
    if QuestTutorialArena:IsActive() then
      local flash_obj = self:GetFlashObject()
      if flash_obj then
        flash_obj:InvokeASCallback("_root", "ShowPvpTip", last_index)
        AddFlurryEvent("TutorialArena_Enter", {}, 2)
      end
    end
    DebugOut("ameUIArena:UpdateOpponentList last_index", last_index)
  end
  self._opponentTable = opponent_list
end
function GameUIArena:updateTopPlayerAvatar(content)
  if not content then
    return
  end
  local commander_avatar = ""
  local commander_type = ""
  for i = 1, 5 do
    if content.cached_fleets[i] then
      if content.cached_fleets[i].fleet_identity == 1 then
        if content.icon ~= 0 and content.icon ~= 1 then
          commander_avatar = commander_avatar .. GameDataAccessHelper:GetFleetAvatar(content.icon, content.cached_fleets[i].level) .. "\001"
          commander_type = commander_type .. GameDataAccessHelper:GetCommanderColorFrame(content.icon, content.cached_fleets[i].level) .. "\001"
        else
          commander_avatar = commander_avatar .. GameUtils:GetPlayerAvatarWithSex(content.icon, content.cached_fleets[i].level) .. "\001"
          commander_type = commander_type .. GameDataAccessHelper:GetCommanderColorFrame(content.cached_fleets[i].fleet_identity, content.cached_fleets[i].level) .. "\001"
        end
      else
        commander_avatar = commander_avatar .. GameDataAccessHelper:GetFleetAvatar(content.cached_fleets[i].fleet_identity, content.cached_fleets[i].level) .. "\001"
        commander_type = commander_type .. GameDataAccessHelper:GetCommanderColorFrame(content.cached_fleets[i].fleet_identity, content.cached_fleets[i].level) .. "\001"
      end
      DebugOut("hasdjasd:", commander_type)
    else
      commander_avatar = commander_avatar .. "empty" .. "\001"
      commander_type = commander_type .. "blue" .. "\001"
    end
  end
  self:GetFlashObject():InvokeASCallback("_root", "updateTopPlayerAvatar", commander_avatar, commander_type)
end
function GameUIArena:UpdateOpponentItem(index_opponent, opponent_rank, opponent_name, opponent_level, opponent_commanders_data, force)
  local rankStr = GameUIArena:GetRankNumber(tonumber(opponent_rank))
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "updateOpponentInfo", index_opponent, tonumber(opponent_rank), opponent_name, GameLoader:GetGameText("LC_MENU_Level") .. opponent_level, opponent_commanders_data, rankStr, GameUtils.numberConversion(force))
end
function GameUIArena:UpdateToprankItem(index_item)
  local player_data = self._toprankUsers[index_item]
  local player_rank = player_data.rank
  local player_name = GameUtils:GetUserDisplayName(player_data.name)
  local player_level = player_data.level
  local player_force = player_data.force
  self:GetFlashObject():InvokeASCallback("_root", "updateToprankItem", index_item, player_rank, player_name, player_level, GameUtils.numberAddComma(player_force))
end
function GameUIArena:UpdateHistoryItem(index_item)
  local history_data = self._battleHistory[index_item]
  local text_timeago = -1
  local text_result = -1
  local rank_change = 0
  if history_data then
    text_timeago = history_data.fight_time
    local battle_result = self:CheckBattleResult(history_data)
    if battle_result == BattleResultType.you_challenge_opponent_win then
      text_result = GameLoader:GetGameText("LC_MENU_YOU_VS_OPPONENT_WIN")
      text_result = string.format(text_result, GameUtils:GetUserDisplayName(history_data.player2_name))
      rank_change = history_data.player1_rank or 0
    elseif battle_result == BattleResultType.you_challenge_opponent_failed then
      text_result = GameLoader:GetGameText("LC_MENU_YOU_VS_OPPONENT_LOSE")
      text_result = string.format(text_result, GameUtils:GetUserDisplayName(history_data.player2_name))
    elseif battle_result == BattleResultType.opponent_challenge_you_win then
      text_result = GameLoader:GetGameText("LC_MENU_OPPONENT_VS_YOU_WIN")
      text_result = string.format(text_result, GameUtils:GetUserDisplayName(history_data.player1_name))
      rank_change = history_data.player2_rank and -1 * history_data.player2_rank or 0
    elseif battle_result == BattleResultType.opponent_challenge_you_failed then
      text_result = GameLoader:GetGameText("LC_MENU_OPPONENT_VS_YOU_LOSE")
      text_result = string.format(text_result, GameUtils:GetUserDisplayName(history_data.player1_name))
    else
      assert(false)
    end
  else
    text_timeago = GameLoader:GetGameText("LC_MENU_NO_DATA_CHAR")
  end
  DebugOut("UpdateHistoryItem text_result - ", text_result)
  DebugOut("UpdateHistoryItem text_timeago - ", text_timeago)
  self:GetFlashObject():InvokeASCallback("_root", "updateHistoryItem", index_item, text_result, text_timeago, rank_change)
end
function GameUIArena:UpdateTopHistoryItem(index_item)
  DebugOut("self.history_topplayer_data[index_item]", self.history_topplayer_data[index_item])
  DebugTable(self.history_topplayer_data)
  local history_data = self.history_topplayer_data[index_item]
  local text_timeago = -1
  local text_result = -1
  if history_data then
    text_timeago = history_data.fight_time
    local player1 = GameUtils:GetUserDisplayName(history_data.player1_name)
    local player2 = GameUtils:GetUserDisplayName(history_data.player2_name)
    text_result = GameLoader:GetGameText("LC_MENU_ARENA_FIRST_REPORT_CHAR")
    text_result = string.gsub(text_result, "<playerA_char>", player1)
    text_result = string.gsub(text_result, "<playerB_char>", player2)
  end
  DebugOut("text_result - ", text_result)
  DebugOut("text_timeago - ", text_timeago)
  self:GetFlashObject():InvokeASCallback("_root", "updateHistoryItem", index_item, text_result, text_timeago, 0)
end
function GameUIArena:SelectOpponent(opponent_index)
  local opponent_data = self._opponentTable[opponent_index]
  if opponent_data then
    local opponent_info = {}
    opponent_main_level = 0
    for k, v in pairs(opponent_data.cached_fleets) do
      if v.fleet_identity == 1 then
        opponent_main_level = 0
      end
    end
    opponent_info.name = GameUtils:GetUserDisplayName(opponent_data.name)
    opponent_info.avatar = ""
    if opponent_data.icon == 0 or opponent_data.icon == 1 then
      opponent_info.avatar = GameUtils:GetPlayerAvatarWithSex(opponent_data.icon, opponent_main_level)
    else
      opponent_info.avatar = GameDataAccessHelper:GetFleetAvatar(opponent_data.icon, opponent_main_level)
    end
    opponent_info.sex = opponent_data.icon
    opponent_info.level = opponent_data.level
    opponent_info.rank = opponent_data.rank
    opponent_info.force = GameUtils.numberAddCommaWithBackslash(opponent_data.force, true)
    opponent_info.user_id = opponent_data.user_id
    opponent_info.commander_array = {}
    for i = 1, 5 do
      local commander_data = {}
      if opponent_data.cached_fleets[i] and opponent_data.cached_fleets[i].fleet_identity ~= 1 then
        local commander_identity = opponent_data.cached_fleets[i].fleet_identity
        commander_data.commander_type = GameDataAccessHelper:GetCommanderColorFrame(commander_identity, opponent_data.cached_fleets[i].level)
        if commander_identity == 1 then
          commander_data.commander_avatar = GameUtils:GetPlayerAvatarWithSex(opponent_data.icon, opponent_data.cached_fleets[i].level)
        else
          commander_data.commander_avatar = GameDataAccessHelper:GetFleetAvatar(commander_identity, opponent_data.cached_fleets[i].level)
        end
      else
        commander_data.commander_type = -1
        commander_data.commander_avatar = -1
      end
      table.insert(opponent_info.commander_array, commander_data)
    end
    local pos_info = self:GetFlashObject():InvokeASCallback("_root", "getLastTouchPoint")
    pos_info = LuaUtils:string_split(pos_info, ",")
    opponent_info.pos_x = pos_info[1]
    opponent_info.pos_y = pos_info[2]
    opponent_info.nextChallengeTime = GameUIArena.nextChallengeTime
    DebugOutPutTable(opponent_info, "opponent_info")
    GameStateManager:GetCurrentGameState():AddObject(ItemBox)
    ItemBox:ShowArenaOpponentInfo(opponent_info)
    GameUIArena.LastSelectOpponentInfo = opponent_info
  end
  self:GetFlashObject():InvokeASCallback("_root", "selectOpponent", opponent_index)
end
function GameUIArena:SelectOpponent_tlc(opponent_index)
  local opponent_data = self._opponentTable_tlc[opponent_index]
  if opponent_data then
    local opponent_info = {}
    opponent_main_level = 0
    for k, v in pairs(opponent_data.cached_fleets) do
      if v.fleet_identity == 1 then
        opponent_main_level = 0
      end
    end
    opponent_info.name = GameUtils:GetUserDisplayName(opponent_data.name)
    opponent_info.avatar = ""
    if opponent_data.icon == 0 or opponent_data.icon == 1 then
      opponent_info.avatar = GameUtils:GetPlayerAvatarWithSex(opponent_data.icon, opponent_main_level)
    else
      opponent_info.avatar = GameDataAccessHelper:GetFleetAvatar(opponent_data.icon, opponent_main_level)
    end
    opponent_info.sex = opponent_data.icon
    opponent_info.level = opponent_data.level
    opponent_info.rank = opponent_data.rank
    opponent_info.force = GameUtils.numberAddCommaWithBackslash(opponent_data.force, true)
    opponent_info.user_id = opponent_data.user_id
    opponent_info.commander_array = {}
    for i = 1, 5 do
      local commander_data = {}
      if opponent_data.cached_fleets[i] and opponent_data.cached_fleets[i].fleet_identity ~= 1 then
        local commander_identity = opponent_data.cached_fleets[i].fleet_identity
        commander_data.commander_type = GameDataAccessHelper:GetCommanderColorFrame(commander_identity, opponent_data.cached_fleets[i].level)
        if commander_identity == 1 then
          commander_data.commander_avatar = GameUtils:GetPlayerAvatarWithSex(opponent_data.icon, opponent_data.cached_fleets[i].level)
        else
          commander_data.commander_avatar = GameDataAccessHelper:GetFleetAvatar(commander_identity, opponent_data.cached_fleets[i].level)
        end
      else
        commander_data.commander_type = -1
        commander_data.commander_avatar = -1
      end
      table.insert(opponent_info.commander_array, commander_data)
    end
    local pos_info = self:GetFlashObject():InvokeASCallback("_root", "getLastTouchPoint")
    pos_info = LuaUtils:string_split(pos_info, ",")
    opponent_info.pos_x = pos_info[1]
    opponent_info.pos_y = pos_info[2]
    opponent_info.nextChallengeTime = GameUIArena.nextChallengeTime_tlc
    DebugOutPutTable(opponent_info, "opponent_info")
    GameStateManager:GetCurrentGameState():AddObject(ItemBox)
    ItemBox:ShowArenaOpponentInfo_tlc(opponent_info)
    GameUIArena.LastSelectOpponentInfo_tlc = opponent_info
  end
  self:GetFlashObject():InvokeASCallback("_root", "selectOpponent_tlc", opponent_index)
end
function GameUIArena:ChallengeOpponentWithRank()
  GameUIArena.mBreakData = nil
  local champion_challenge_req_data = {
    rank = GameUIArena.LastSelectOpponentInfo.rank
  }
  if GameStateManager:GetCurrentGameState().fightRoundData then
    GameStateManager:GetCurrentGameState().fightRoundData = nil
  end
  NetMessageMgr:SendMsg(NetAPIList.champion_challenge_req.Code, champion_challenge_req_data, self.challengeWithRankCallback, true, nil)
end
function GameUIArena:RequireEnemyMatrixWithRank(opponent_rank)
  DebugOut("RequireEnemyMatrixWithRank", opponent_rank)
  local param = {rank = opponent_rank}
  NetMessageMgr:SendMsg(NetAPIList.champion_matrix_req.Code, param, self.RequireEnemyMatrixWithRankCallback, true, nil)
end
function GameUIArena:RequireEnemyMatrixWithRank_tlc(opponent_rank)
  DebugOut("RequireEnemyMatrixWithRank", opponent_rank)
  local param = {rank = opponent_rank}
  NetMessageMgr:SendMsg(NetAPIList.champion_matrix_req.Code, param, self.RequireEnemyMatrixWithRankCallback_tlc, true, nil)
end
function GameUIArena.RequireEnemyMatrixWithRankCallback(msgType, content)
  if msgType == NetAPIList.champion_matrix_ack.Code then
    DebugOut("RequireEnemyMatrixWithRankCallback")
    DebugTable(content)
    GameUIArena.LastSelectOpponentInfo.matrix = content.matrix
    GameUIArena.BuildEnemyInfoByopponetInfo()
    GameStateManager.GameStateFormation.enemyForce = GameUIArena.LastSelectOpponentInfo.force
    GameStateManager.GameStateFormation.isNeedRequestMatrixInfo = false
    GameStateManager.GameStateFormation.enemyMatrixData = content.matrix
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateFormation)
    return true
  elseif msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.champion_matrix_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  return false
end
function GameUIArena.RequireEnemyMatrixWithRankCallback_tlc(msgType, content)
  if msgType == NetAPIList.champion_matrix_ack.Code then
    DebugOut("RequireEnemyMatrixWithRankCallback_tlc")
    DebugTable(content)
    GameUIArena.LastSelectOpponentInfo_tlc.matrix = content.matrix
    GameUIArena.BuildEnemyInfoByopponetInfo()
    GameStateManager.GameStateFormation.enemyForce = GameUIArena.LastSelectOpponentInfo_tlc.force
    GameStateManager.GameStateFormation.isNeedRequestMatrixInfo = false
    GameStateManager.GameStateFormation.enemyMatrixData = content.matrix
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateFormation)
    return true
  elseif msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.champion_matrix_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  return false
end
function GameUIArena:CleanPVPCD()
  local cost = GameGlobalData:GetGameConfig("clear_pvp_cd_cost")
  if cost and cost ~= -99 then
    local info_content = GameLoader:GetGameText("LC_MENU_RESET_CD_ASK")
    info_content = string.format(info_content, cost)
    local function callback()
      NetMessageMgr:SendMsg(NetAPIList.champion_reset_cd_req.Code, nil, self.resetChampionCallback, true, nil)
    end
    GameUtils:CreditCostConfirm(info_content, callback, true)
  else
    do
      local function netCallback(msgtype, content)
        if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.price_req.Code then
          if content.code ~= 0 then
            GameUIGlobalScreen:ShowAlert("error", content.code, nil)
          end
          return true
        end
        if msgtype == NetAPIList.price_ack.Code then
          GameGlobalData:UpdateGameConfig("clear_pvp_cd_cost", content.price)
          GameUIArena:CleanPVPCD()
          return true
        end
        return false
      end
      local function netCall()
        NetMessageMgr:SendMsg(NetAPIList.price_req.Code, {
          price_type = "clear_pvp_cd",
          type = 0
        }, netCallback, true, netCall)
      end
      netCall()
    end
  end
end
function GameUIArena:CleanPVPCD_tlc()
  local cost = GameGlobalData:GetGameConfig("clear_pvp_tlc_cd_cost")
  if cost and cost ~= -99 then
    local info_content = GameLoader:GetGameText("LC_MENU_RESET_CD_ASK")
    info_content = string.format(info_content, cost)
    local function callback()
      NetMessageMgr:SendMsg(NetAPIList.champion_reset_cd_req.Code, nil, self.resetChampionCallback_tlc, true, nil)
    end
    GameUtils:CreditCostConfirm(info_content, callback, true)
  else
    do
      local function netCallback(msgtype, content)
        if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.price_req.Code then
          if content.code ~= 0 then
            GameUIGlobalScreen:ShowAlert("error", content.code, nil)
          end
          return true
        end
        if msgtype == NetAPIList.price_ack.Code then
          GameGlobalData:UpdateGameConfig("clear_pvp_tlc_cd_cost", content.price)
          GameUIArena:CleanPVPCD()
          return true
        end
        return false
      end
      local function netCall()
        NetMessageMgr:SendMsg(NetAPIList.price_req.Code, {
          price_type = "clear_pvp_cd",
          type = 0
        }, netCallback, true, netCall)
      end
      netCall()
    end
  end
end
function GameUIArena:ReplayBattle(index_item)
  local history_data = self._battleHistory[index_item]
  local reportID = history_data.report_id
  GameUIArena:RequestReplayBattle(reportID)
end
function GameUIArena:RequestReplayBattle(battle_id)
  if GameStateManager:GetCurrentGameState().fightRoundData then
    GameStateManager:GetCurrentGameState().fightRoundData = nil
  end
  GameStateBattlePlay.report_id = battle_id
  NetMessageMgr:SendMsg(NetAPIList.battle_fight_report_req.Code, {battle_report_id = battle_id}, GameUIArena.RequestReplayBattleCallback, true, nil)
end
function GameUIArena.RequestReplayBattleCallback(msgtype, content)
  if msgtype == NetAPIList.battle_fight_report_ack.Code then
    DebugOut("@content.histories = ", #content.histories)
    if content.code == 0 and #content.histories > 0 then
      if #content.histories[1].report.rounds == 0 and GameStateManager:GetCurrentGameState().fightRoundData then
        content.histories[1].report.rounds = GameStateManager:GetCurrentGameState().fightRoundData
      end
      GameStateBattlePlay.curBattleType = "arena"
      GameStateManager.GameStateBattlePlay:InitBattle(GameStateBattlePlay.MODE_HISTORY, content.histories[1].report, nil, nil)
      GameStateBattlePlay:RegisterOverCallback(function()
        GameStateManager:SetCurrentGameState(GameStateArena)
      end, nil)
      GameStateManager:SetCurrentGameState(GameStateBattlePlay)
    else
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  return false
end
function GameUIArena:OnFSCommand(cmd, arg)
  if "Close_Arena_Window" == cmd then
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateMainPlanet)
  elseif "select_opponent" == cmd then
    self:SelectOpponent(tonumber(arg))
    if immanentversion == 2 and QuestTutorialArena:IsActive() then
      QuestTutorialArena:SetFinish(true)
      if not QuestTutorialArenaReward:IsFinished() then
        QuestTutorialArenaReward:SetActive(true)
      end
      GameUIArena.hasCheckBattle = true
      self:GetFlashObject():InvokeASCallback("_root", "HideAllPvpTip")
    end
    if GameUtils:GetTutorialHelp() then
      GameUtils:HideTutorialHelp()
    end
  elseif "select_opponent_tlc" == cmd then
    self:SelectOpponent_tlc(tonumber(arg))
    if immanentversion == 2 and QuestTutorialArena:IsActive() then
      QuestTutorialArena:SetFinish(true)
      if not QuestTutorialArenaReward:IsFinished() then
        QuestTutorialArenaReward:SetActive(true)
      end
      GameUIArena.hasCheckBattle = true
      self:GetFlashObject():InvokeASCallback("_root", "HideAllPvpTip")
    end
    if GameUtils:GetTutorialHelp() then
      GameUtils:HideTutorialHelp()
    end
  elseif "clickedReportItem" == cmd then
    if self.is_show_topplayer then
      GameUIArena:ShowTopPlayerBattleReplay(tonumber(arg))
    else
      self:ReplayBattle(tonumber(arg))
    end
  elseif cmd == "setAndroidBackState" then
    self.isOnShowTopRank = nil
    self.isOnShowHistory = nil
    self.isOnShowReward = nil
  elseif "update_history_item" == cmd then
    if self.is_show_topplayer then
      GameUIArena:UpdateTopHistoryItem(tonumber(arg))
    else
      self:UpdateHistoryItem(tonumber(arg))
    end
  elseif "clickeTopRankItem" == cmd then
    GameUIArena:ShowPlayerDetail(tonumber(arg))
  elseif "update_toprank_item" == cmd then
    self:UpdateToprankItem(tonumber(arg))
  elseif "clean_challenge_cd" == cmd then
    GameUIArena:CleanPVPCD()
  elseif "show_history" == cmd then
    self.is_show_topplayer = false
    NetMessageMgr:SendMsg(NetAPIList.user_fight_history_req.Code, null, self.challengeHistoryCallback, true, nil)
  elseif "show_toprank" == cmd then
    NetMessageMgr:SendMsg(NetAPIList.champion_top_req.Code, null, self.championTopCallback, true, nil)
  elseif "show_formation" == cmd then
    FleetMatrix:GetMatrixsReq(GameUIArena.RequestMultiMatrixCallBack, FleetMatrix.MATRIX_TYPE_CHAMPION)
    self.isShowMatrix = true
  elseif "get_award" == cmd then
    if GameUIArena.brief_info then
      GameUIArena.mSuccessiveAwardList = nil
      NetMessageMgr:SendMsg(NetAPIList.champion_rank_reward_req.Code, null, self.getChampionRewardCallBack, true, nil)
    end
    AddFlurryEvent("TutorialArena_RewardCheck", {}, 2)
  elseif "getEveryDayReward" == cmd then
    NetMessageMgr:SendMsg(NetAPIList.champion_get_award_req.Code, null, self.getAwardCallback, true, nil)
  elseif "OnMoveInAnimOver" == cmd then
  elseif "close_all" == cmd then
    GameStateArena:Quit()
  elseif "close_enterlayer" == cmd then
    GameStateArena:Quit()
  elseif "top_player_replay" == cmd then
    GameUIArena.ReqTopPlayerReplay()
    self.is_show_topplayer = true
  elseif "top_player_show" == cmd then
    GameUIArena:ShowPlayerDetail(1)
  elseif "NeedUpdataRewardID" == cmd then
    self:GetFlashObject():InvokeASCallback("_root", "updataRewardItem", arg)
  elseif "RewardItemCliced" == cmd then
    self.m_showRewardShow = true
    local money, prestige, credit = self:GetRankReward(tonumber(arg))
    local moneyText = GameLoader:GetGameText("LC_MENU_MONEY_CHAR")
    local prestigeText = GameLoader:GetGameText("LC_MENU_PRESTIGE_CHAR")
    local creditText = GameLoader:GetGameText("LC_MENU_LOOT_CREDIT")
    self:GetFlashObject():InvokeASCallback("_root", "ShowRewardDetail", moneyText, money, prestigeText, prestige, creditText, credit, arg)
  elseif "recover_challenge_supply" == cmd then
    GameUIArena.BuySupply()
  elseif "NeedUpdataRrportItem" == cmd then
    self:GetFlashObject():InvokeASCallback("_root", "updataReportItem", arg)
  elseif "close_reward" == cmd then
    self:GetFlashObject():InvokeASCallback("_root", "closeReward")
    GameUIArena:CheckRewardsIsCanGet()
  elseif "close_reward_tlc" == cmd then
    self:GetFlashObject():InvokeASCallback("_root", "closeReward_tlc")
    GameUIArena:CheckRewardsIsCanGet()
  elseif "breakthrough_info_confirm" == cmd then
    local countryStr = ext.GetDeviceCountry and ext.GetDeviceCountry() or "unknow"
    if self.mShareArenaBreakThroughChecked then
      local extraInfo = {}
      if GameUIArena.mBestRank then
        extraInfo.bestRank = GameUIArena.mBestRank
        GameUIArena.mBestRank = nil
      end
      if FacebookGraphStorySharer:ShareStory(FacebookGraphStorySharer.StoryType.STORY_TYPE_ARENA_BEST_RANK, extraInfo) then
      end
    else
    end
    GameUIArena:HideBreakthroughPanel()
  elseif "breakthrough_showed" == cmd then
    GameUIArena:PlayRankUpAnimation()
  elseif "change_tab" == cmd then
    if GameUIArena.AWARD_TYPE.SUCCESSIVE == arg then
      if nil == GameUIArena.mSuccessiveAwardList then
        GameUIArena:RequestBreakthroughAwardList()
      else
        GameUIArena:SetCurAwardType(arg)
      end
    else
      GameUIArena:SetCurAwardType(arg)
    end
  elseif "NeedUpdateSuccessiveAwardListItem" == cmd then
    GameUIArena:UpdateSuccessiveAwardListItem(tonumber(arg))
  elseif "SuccessiveRecieve" == cmd then
    GameUIArena:RequestGetBreakthroughAward(tonumber(arg), false)
  elseif "successive_receive_all" == cmd then
    GameUIArena:RequestGetBreakthroughAward(1, true)
  elseif cmd == "shareArenaBreakThroughCheckboxClicked" then
    if self.mShareArenaBreakThroughChecked then
      self.mShareArenaBreakThroughChecked = false
    else
      self.mShareArenaBreakThroughChecked = true
    end
    local flash_obj = self:GetFlashObject()
    if flash_obj then
      flash_obj:InvokeASCallback("_root", "setCheckBox", self.mShareArenaBreakThroughChecked, "arenaBreakThrough")
    end
  elseif cmd == "shareArenaDayRewardCheckboxClicked" then
    if self.mShareArenaDayRewardChecked then
      self.mShareArenaDayRewardChecked = false
    else
      self.mShareArenaDayRewardChecked = true
    end
    local flash_obj = self:GetFlashObject()
    if flash_obj then
      flash_obj:InvokeASCallback("_root", "setCheckBox", self.mShareArenaDayRewardChecked, "arenaDayReward")
    end
  elseif cmd == "Pvp_pos_arena" then
    if GameUtils:GetTutorialHelp() then
      DebugOut("xxx")
      local param = LuaUtils:string_split(arg, "\001")
      GameUtils:ShowMaskLayer(tonumber(param[1]), tonumber(param[2]), GameUIMaskLayer.ButtonPos.right, "empty", GameUIMaskLayer.MaskStyle.small)
    end
  elseif cmd == "Pvp_pos_enterlayer" then
    if GameUtils:GetTutorialHelp() then
      local param = LuaUtils:string_split(arg, "\001")
      GameUtils:ShowMaskLayer(tonumber(param[1]), tonumber(param[2]), GameUIMaskLayer.ButtonPos.right, "empty", GameUIMaskLayer.MaskStyle.big)
    end
  elseif cmd == "closeRewardShow" then
    self.m_showRewardShow = false
    local flash_obj = self:GetFlashObject()
    if flash_obj then
      flash_obj:InvokeASCallback("_root", "closeRewardShow")
    end
  elseif "UpdateMarix" == cmd then
    local index = tonumber(arg)
    DebugOut("UpdateMarix_tlc " .. arg)
    FleetMatrix:SwitchMatrixClient(index, self.UpdateMatrixFleetAvatar)
  elseif "choose_matrix" == cmd then
    FleetMatrix:SetTypeMatrix(FleetMatrix.MATRIX_TYPE_CHAMPION)
  elseif "onEnterNormalArena" == cmd then
    GameUIArena:SetPreArenaType(GameUIArena.ARENA_TYPE.enterLayer)
    GameUIArena:EnterNormalArena()
    if QuestTutorialArena:IsActive() then
      QuestTutorialArena:SetFinish(true)
      GameUIBarRight:GetFlashObject():InvokeASCallback("_root", "HideTutorialArena")
    end
  elseif "onEnterAdancedArena" == cmd then
    GameUIArena:SetPreArenaType(GameUIArena.ARENA_TYPE.enterLayer)
    GameUIArena:EnterAdvancedArena()
  elseif "onEnterTeamLeagueCup" == cmd then
    if TutorialQuestManager.QuestTutorialTlcArenaEntry:IsActive() then
      TutorialQuestManager.QuestTutorialTlcArenaEntry:SetFinish(true)
      TutorialQuestManager.QuestTutorialTlcSearch:SetActive(true)
      GameUIBarRight:GetFlashObject():InvokeASCallback("_root", "hideTutorialTlcArena")
    end
    GameUIArena:SetPreArenaType(GameUIArena.ARENA_TYPE.enterLayer)
    GameUIArena:EnterTeamLeagueCup()
  elseif "showGradeRewardList_tlc" == cmd then
    GameUIArena:showGradeRewardList_tlc()
  elseif "showRankRewardList_tlc" == cmd then
    GameUIArena:showRankRewardList_tlc()
  elseif "showGradeRewardList" == cmd then
    GameUIArena:showGradeRewardList()
  elseif "showRankRewardList" == cmd then
    GameUIArena:showRankRewardList()
  elseif "UpdateRankRewardItem" == cmd then
    GameUIArena:UpdateRankRewardItem(tonumber(arg))
  elseif "UpdateRankRewardItem_tlc" == cmd then
    GameUIArena:UpdateRankRewardItem_tlc(tonumber(arg))
  elseif "showRankList" == cmd then
    GameUIArena.nowRankListType = arg
    GameUIArena:showRankList(arg)
  elseif "showRankList_tlc" == cmd then
    GameUIArena.nowRankListType_tlc = arg
    GameUIArena:showRankList_tlc(arg)
  elseif "UpdateRankItem" == cmd then
    GameUIArena:UpdateRankItem(tonumber(arg))
  elseif "UpdateRankItem_tlc" == cmd then
    GameUIArena:UpdateRankItem_tlc(tonumber(arg))
  elseif "clickeGalaxyArenaRankItem" == cmd then
    GameUIArena:showRankPlayerInfo(tonumber(arg))
  elseif "clickeGalaxyArenaRankItem_tlc" == cmd then
    GameUIArena:showRankPlayerInfo_tlc(tonumber(arg))
  elseif "showSelfHonorWall" == cmd then
    GameUIArena:showSelfHonorWall()
  elseif "showSelfHonorWall_tlc" == cmd then
    GameUIArena:showSelfHonorWall_tlc()
  elseif "showAllHonorWall" == cmd then
    GameUIArena:showHonorWallAll()
  elseif "showAllHonorWall_tlc" == cmd then
    GameUIArena:showHonorWallAll_tlc()
  elseif "reward" == cmd then
    GameUIArena:RequestGalaxyArenaRewards()
    GameUIArena.windowType = GameUIArena.wdc_windowType.reward
  elseif "reward_tlc" == cmd then
    GameUIArena:RequestTeamLeagueCupRewards()
    GameUIArena.windowType = GameUIArena.tlc_windowType.reward
  elseif "leaderboard" == cmd then
    GameUIArena:RequestGalaxyArenaRankList()
    GameUIArena.windowType = GameUIArena.wdc_windowType.leaderboard
  elseif "leaderboard_tlc" == cmd then
    GameUIArena:RequestGalaxyArenaRankList_tlc()
    GameUIArena.windowType = GameUIArena.tlc_windowType.leaderboard
  elseif "honor" == cmd then
    GameUIArena:RequestHonorWallList()
    GameUIArena.windowType = GameUIArena.wdc_windowType.honor
  elseif "honor_tlc" == cmd then
    GameUIArena:RequestHonorWallList_tlc()
    GameUIArena.windowType = GameUIArena.tlc_windowType.honor
  elseif "report" == cmd then
    self.is_show_wdc_topplayer = false
    GameUIArena:RequestGalaxyArenaReport()
    GameUIArena.windowType = GameUIArena.wdc_windowType.report
  elseif "report_tlc" == cmd then
    self.is_show_wdc_topplayer = false
    TeamLeagueCup:RequestGalaxyArenaReport()
    GameUIArena.windowType = GameUIArena.tlc_windowType.report
  elseif "update_report_item" == cmd then
    GameUIArena:UpdateReportItem(tonumber(arg))
  elseif "update_report_item_tlc" == cmd then
    TeamLeagueCup:UpdateReportItem(tonumber(arg))
  elseif "showReportList" == cmd then
    GameUIArena.nowReportType = arg
    GameUIArena:AnimationMoveInGalaxyArenaReport()
  elseif "showReportList_tlc" == cmd then
    GameUIArena.nowReportTypeTlc = arg
    TeamLeagueCup:AnimationMoveInGalaxyArenaReport()
  elseif "revengeFromReport" == cmd then
    GameUIArena.revengeIndex = tonumber(arg)
    GameUIArena:checkStat()
  elseif "revengeFromReport_tlc" == cmd then
    GameUIArena.revengeIndex_tlc = tonumber(arg)
    GameUIArena:checkStat_tlc()
  elseif "clickedReportItemGalaxyArena" == cmd then
    GameUIArena:replayGalaxyArenaBattle(tonumber(arg))
  elseif "clickedReportItemGalaxyArena_tlc" == cmd then
    TeamLeagueCup:replayGalaxyArenaBattle(tonumber(arg))
  elseif "clearCurrentMenu" == cmd then
    GameUIArena.currentMenu = nil
    GameUIArena.windowType = GameUIArena.wdc_windowType.noWindow
  elseif "clearCurrentMenu_tlc" == cmd then
    GameUIArena.currentMenu = nil
    GameUIArena.windowType = GameUIArena.tlc_windowType.noWindow
  elseif "showGradeDetail" == cmd then
    GameUIArena:showGradeDetail(tonumber(arg))
  elseif "showGradeDetail_tlc" == cmd then
    GameUIArena:showGradeDetail_tlc(tonumber(arg))
  elseif "collectReward" == cmd then
    GameUIArena:collectReward(tonumber(arg))
  elseif "collectReward_tlc" == cmd then
    GameUIArena:collectReward_tlc(tonumber(arg))
  elseif "closeRewardMenu" == cmd then
    GameUIArena.windowType = GameUIArena.wdc_windowType.noWindow
  elseif "closeRewardMenu_tlc" == cmd then
    GameUIArena.windowType = GameUIArena.tlc_windowType.noWindow
  elseif "closeHonorWallMenu" == cmd then
    GameUIArena.windowType = GameUIArena.wdc_windowType.noWindow
  elseif "closeHonorWallMenu_tlc" == cmd then
    GameUIArena.windowType = GameUIArena.tlc_windowType.noWindow
  elseif "closeRanklistMenu" == cmd then
    GameUIArena.windowType = GameUIArena.wdc_windowType.noWindow
  elseif "closeRanklistMenu_tlc" == cmd then
    GameUIArena.windowType = GameUIArena.tlc_windowType.noWindow
  end
  GameUIAdvancedArenaLayer:OnFSCommand(cmd, arg)
  TeamLeagueCup:OnFSCommand(cmd, arg)
end
function GameUIArena.BuySupply()
  local packet = {type = "pvp_supply"}
  NetMessageMgr:SendMsg(NetAPIList.supply_info_req.Code, packet, GameUIArena.SupplyInfoNetCallback, true, nil)
end
function GameUIArena:ClearLocalData()
  self._battleHistory = nil
  self._toprankUsers = nil
end
function GameUIArena:Update(dt)
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    flash_obj:Update(dt)
    flash_obj:InvokeASCallback("_root", "onUpdateFrame", dt)
  end
  if self.award_cd_time then
    GameUIArena:_UpdateAwardCDTime()
  end
  if self.challenge_cd_time then
    self:_UpdateChallengeCDTime()
  end
  if self.challenge_cd_time_tlc then
    self:_UpdateChallengeCDTime_tlc()
  end
  if self.NormalChallenge_cd_time then
    self:UpdateNormalCDTime()
  end
  if self.AdvancedChallenge_cd_time then
    self:UpdateAdvancedCDTime()
  end
  if self.TeamLeaqueCup_cd_time then
    self:UpdateTeamLeagueCupCDTime()
  end
  GameUIAdvancedArenaLayer:Update(dt)
  TeamLeagueCup:Update(dt)
end
function GameUIArena:ShowPlayerDetail(index_item)
  local top_player_data = self._toprankUsers[index_item]
  local GameUIPlayerDetailInfo = LuaObjectManager:GetLuaObject("GameUIPlayerDetailInfo")
  GameUIPlayerDetailInfo:Show(top_player_data.user_id, 320, 480)
end
function GameUIArena.ReqTopPlayerReplay()
  NetMessageMgr:SendMsg(NetAPIList.champion_top_record_req.Code, nil, GameUIArena.ReqTopPlayerCallback, true, nil)
end
function GameUIArena.ReqTopPlayerCallback(msgType, content)
  DebugTable(content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.champion_top_record_req.Code then
    assert(content.code ~= 0)
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.champion_top_record_ack.Code then
    GameUIArena.history_topplayer_data = content.histories
    GameUIArena:ShowTopPlayerReport()
    return true
  end
  return false
end
function GameUIArena:ShowTopPlayerBattleReplay(index_item)
  if not index_item then
    return
  end
  if tonumber(index_item) < 0 then
    return
  end
  if not GameUIArena.history_topplayer_data[tonumber(index_item)] then
  end
  local battleReportID = GameUIArena.history_topplayer_data[tonumber(index_item)].report_id
  GameUIArena:RequestReplayBattle(battleReportID)
end
function GameUIArena.championListCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.champion_list_req.Code then
    assert(content.code ~= 0)
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  end
  if msgType == NetAPIList.champion_list_ack.Code then
    DebugOut("++++++championListCallback++++++++")
    DebugTable(content)
    GameUIArena:UpdateMainInfo(content)
    GameUIArena:Visible(true)
    GameUIArena:AnimationMoveInMain()
    GameUIArena:updateTopPlayerAvatar(content.top_user)
    return true
  end
  return false
end
function GameUIArena.fight_timecomps(Datatable)
  for i = 2, #Datatable do
    for j = 1, #Datatable - 1 do
      local time_i = Datatable[i].fight_time
      local time_j = Datatable[j].fight_time
      if tonumber(time_i) < tonumber(time_j) then
        local tempdata = Datatable[i]
        Datatable[i] = Datatable[j]
        Datatable[j] = tempdata
      end
    end
  end
  return Datatable
end
function GameUIArena.challengeHistoryCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.user_fight_history_ack.Code then
    assert(content.code ~= 0)
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.user_fight_history_ack.Code then
    local DataArray = content.histories
    GameUIArena._battleHistory = GameUIArena.fight_timecomps(DataArray)
    GameUIArena:AnimationMoveInHistory()
    GameUIArena.isOnShowHistory = true
    return true
  end
  return false
end
function GameUIArena.getChampionRewardCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.champion_rank_reward_req.Code then
    assert(content.code ~= 0)
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.champion_rank_reward_ack.Code then
    if immanentversion == 2 and QuestTutorialArenaReward:IsActive() then
      GameUIArena:GetFlashObject():InvokeASCallback("_root", "HideRewardTip")
      local function callback()
        QuestTutorialArenaReward:SetFinish(true)
        GameUIArena.hasCheckBattle = false
        GameUIArena:GetFlashObject():InvokeASCallback("_root", "setCloseArwadAnimVisible", true)
        GameStateManager.GameStateMainPlanet:SetStoryWhenFocusGain({
          51229,
          51230,
          51231
        })
        GameUIBarRight:GetFlashObject():InvokeASCallback("_root", "HideTutorialArena")
        GameUIBarRight:GetFlashObject():InvokeASCallback("_root", "ShowTutorialBattleMapBtn")
      end
      GameUICommonDialog:PlayStory({512281}, callback)
    end
    GameUIArena.isOnShowReward = true
    local facebookEnabled = false
    if FacebookGraphStorySharer:IsStoryEnabled(FacebookGraphStorySharer.StoryType.STORY_TYPE_ARENA_DAY_AWARD) then
      facebookEnabled = true
    end
    GameUIArena:GetFlashObject():InvokeASCallback("_root", "InitRewardList", facebookEnabled, GameUIArena.mShareArenaDayRewardChecked)
    GameUIArena.champion_reward = content.champion_reward
    DebugOut("====")
    DebugOut("reward=====")
    DebugTable(content)
    if GameUIArena.brief_info.award then
      DebugOut("GameUIArena.brief_info.award == true")
      GameUIArena:AnimationMoveInAward(content.last_rank, GameUIArena.brief_info.award, content.last_loot)
    else
      DebugOut("GameUIArena.brief_info.award == false")
      DebugTable(GameUIArena.brief_info)
      GameUIArena:AnimationMoveInAward(GameUIArena.brief_info.rank, GameUIArena.brief_info.award, content.champion_reward[1])
    end
    for i = 1, 12 do
      GameUIArena:GetFlashObject():InvokeASCallback("_root", "addRewardListItem", i)
    end
    GameUIArena.mCurAwardType = GameUIArena.AWARD_TYPE.RANK
    if not GameUIArena.brief_info.award and GameUIArena.brief_info.straight_award then
      GameUIArena.mCurAwardType = GameUIArena.AWARD_TYPE.SUCCESSIVE
      GameUIArena:RequestBreakthroughAwardList()
    else
      GameUIArena:SetCurAwardType(GameUIArena.AWARD_TYPE.RANK)
    end
    GameUIArena:RefreshTabBtnStatus()
    return true
  end
  return false
end
function GameUIArena.TopPlayerCallback(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.champion_top_req.Code then
    assert(content.code ~= 0)
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgtype == NetAPIList.champion_top_ack.Code then
    GameUIArena._toprankUsers = content.users
    GameUIArena:UpdateTopPlayer()
    return true
  end
  return false
end
function GameUIArena.championTopCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.champion_top_req.Code then
    assert(content.code ~= 0)
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.champion_top_ack.Code then
    GameUIArena.isOnShowTopRank = true
    GameUIArena._toprankUsers = content.users
    GameUIArena:AnimationMoveInToprank()
    return true
  end
  return false
end
function GameUIArena.getAwardCallback(msgType, content)
  if msgType == NetAPIList.champion_get_award_ack.Code and content.code ~= 0 then
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.champion_get_award_req.Code and content.code == 0 then
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.champion_get_award_ack.Code then
    GameUIArena.brief_info = content.brief_info
    local countryStr = ext.GetDeviceCountry and ext.GetDeviceCountry() or "unknow"
    if GameUIArena.mShareArenaDayRewardChecked then
      FacebookGraphStorySharer:ShareStory(FacebookGraphStorySharer.StoryType.STORY_TYPE_ARENA_DAY_AWARD)
    elseif FacebookGraphStorySharer:IsStoryEnabled(FacebookGraphStorySharer.StoryType.STORY_TYPE_ARENA_DAY_AWARD) then
    end
    local brief_info = content.brief_info
    DebugOut("---------getAwardCallback")
    DebugTable(content)
    if GameUIArena.brief_info then
      GameUIArena:AnimationMoveInAward(GameUIArena.brief_info.rank, GameUIArena.brief_info.award, GameUIArena.champion_reward[1])
    end
    local award_cd_time
    if 0 < brief_info.next_award_time then
      award_cd_time = os.time() + brief_info.next_award_time
    end
    GameUIArena:UpdateAwardCDTime(award_cd_time)
    for index_award = 1, 3 do
      local item_type = -1
      local item_text = -1
      local award_data = content.awards[index_award]
      if award_data then
        if award_data.item_text == "equip" then
          item_type = award_data.item_type
          item_text = ""
        else
          item_type = award_data.item_type
          item_text = tostring(award_data.number)
        end
      end
    end
    return true
  end
  return false
end
function GameUIArena.resetChampionCallback(msgType, content)
  if msgType == NetAPIList.champion_reset_cd_ack.Code then
    if content.code ~= 0 then
      local GameVip = LuaObjectManager:GetLuaObject("GameVip")
      GameVip:CheckIsNeedShowVip(content.code)
      return true
    end
    local brief_info = content.brief_info
    local challenge_cd_time
    if 0 < brief_info.next_fight_time then
      challenge_cd_time = os.time() + brief_info.next_fight_time
    end
    GameGlobalData:UpdateGameConfig("clear_pvp_cd_cost", -99)
    GameUIArena:UpdateChallengeCDTime(challenge_cd_time)
    return true
  elseif msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.champion_reset_cd_req.Code then
    local GameVip = LuaObjectManager:GetLuaObject("GameVip")
    GameVip:CheckIsNeedShowVip(content.code)
    return true
  end
  return false
end
function GameUIArena.resetChampionCallback_tlc(msgType, content)
  if msgType == NetAPIList.champion_reset_cd_ack.Code then
    if content.code ~= 0 then
      local GameVip = LuaObjectManager:GetLuaObject("GameVip")
      GameVip:CheckIsNeedShowVip(content.code)
      return true
    end
    local brief_info = content.brief_info
    local challenge_cd_time
    if 0 < brief_info.next_fight_time then
      challenge_cd_time = os.time() + brief_info.next_fight_time
    end
    GameGlobalData:UpdateGameConfig("clear_pvp_tlc_cd_cost", -99)
    GameUIArena:UpdateChallengeCDTime_tlc(challenge_cd_time)
    return true
  elseif msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.champion_reset_cd_req.Code then
    local GameVip = LuaObjectManager:GetLuaObject("GameVip")
    GameVip:CheckIsNeedShowVip(content.code)
    return true
  end
  return false
end
function GameUIArena:CheckCallbackResult(code)
  if code == 1004 then
    local text = AlertDataList:GetTextFromErrorCode(code)
    GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.Green)
    local cancel = GameLoader:GetGameText("LC_MENU_BUTTON_CAPTION_CANCEL")
    local affairs = GameLoader:GetGameText("LC_MENU_GET_MORE_BUTTON")
    GameUIMessageDialog:SetRightTextButton(cancel)
    GameUIMessageDialog:SetLeftGreenButton(affairs, function()
      GameUIArena.BuySupply()
    end)
    GameUIMessageDialog:Display("", text)
  else
    GameUIGlobalScreen:ShowAlert("error", code, nil)
  end
end
function GameUIArena.challengeWithRankCallback(msgType, content)
  DebugOut("challengeWithRankCallback 1")
  if msgType == NetAPIList.champion_challenge_ack.Code then
    if content.code ~= 0 then
      GameUIArena:CheckCallbackResult(tonumber(content.code))
      return true
    end
    DebugOut("challengeWithRankCallback 2")
    if content.brief_info ~= nil and GameUIArena.AddArenaNotification ~= nil then
      GameUIArena.AddArenaNotification(content.brief_info.max_challenge_cnt - content.brief_info.challenge_cnt)
    end
    if content.reports then
      local battle_report_data = content.reports[1]
      if battle_report_data then
        local quest = GameGlobalData:GetData("user_quest")
        if quest.quest_id <= 18 then
          AddFlurryEvent("TutorialArena_Battle", {}, 2)
        end
        local lastGameState = GameStateManager:GetCurrentGameState()
        if #battle_report_data.rounds == 0 and lastGameState.fightRoundData then
          battle_report_data.rounds = lastGameState.fightRoundData
        end
        GameStateBattlePlay:InitBattle(GameStateBattlePlay.MODE_PLAY, battle_report_data)
        GameStateBattlePlay.curBattleType = "arena"
        GameStateBattlePlay:RegisterOverCallback(function()
          local GameUIBattleResult = LuaObjectManager:GetLuaObject("GameUIBattleResult")
          GameUIBattleResult:LoadFlashObject()
          GameUIBattleResult:SetFightReport(content.reports[1], nil, nil)
          if content.result == 1 then
            local awardTable = {}
            for index_award = 1, 2 do
              local award_data = content.awards[index_award]
              if award_data then
                local itemName, icon = GameHelper:GetAwardTypeTextAndIcon(award_data.item_type, award_data.number)
                table.insert(awardTable, {
                  itemType = award_data.item_type,
                  itemDesc = itemName,
                  itemNumber = tostring(award_data.number)
                })
              end
            end
            local dataTable = {}
            local userinfo = GameGlobalData:GetData("userinfo")
            local levelinfo = GameGlobalData:GetData("levelinfo")
            dataTable.winnerRank = content.brief_info.rank
            dataTable.winnerName = GameUtils:GetUserDisplayName(userinfo.name)
            dataTable.winnerLevel = levelinfo.level
            if GameUIArena.LastSelectOpponentInfo.rank == content.brief_info.rank then
              dataTable.loserRank = GameUIArena.brief_info.rank
            else
              dataTable.loserRank = GameUIArena.LastSelectOpponentInfo.rank
            end
            dataTable.loserName = GameUIArena.LastSelectOpponentInfo.name
            dataTable.loserLevel = GameUIArena.LastSelectOpponentInfo.level
            dataTable.awardTable = awardTable
            GameUIBattleResult:ShowArenaWin(dataTable)
          else
            GameUIBattleResult:RandomizeImprovement()
            GameUIBattleResult:AnimationMoveIn("challenge_lose", false)
          end
          GameStateManager:SetCurrentGameState(GameStateArena)
          GameStateArena:AddObject(GameUIBattleResult)
        end, nil)
        DebugOut("women = ")
        GameStateManager:SetCurrentGameState(GameStateBattlePlay)
      end
    end
    return true
  elseif msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.champion_challenge_req.Code then
    GameUIArena:CheckCallbackResult(tonumber(content.code))
    return true
  end
  return false
end
function GameUIArena.RecoverSupplyNetCallback(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.supply_exchange_req.Code then
    if content.code ~= 0 then
      local GameVip = LuaObjectManager:GetLuaObject("GameVip")
      GameVip:CheckIsNeedShowVip(content.code)
    else
      GameUIArena.brief_info.exchange_cost = GameUIArena.nextExChangeCost
      GameUIArena:UpdateChallengeChargeMoney(GameUIArena.brief_info.exchange_cost)
    end
    return true
  end
  return false
end
function GameUIArena.SupplyInfoNetCallback(msgtype, content)
  DebugTable(content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.supply_info_req.Code then
    local GameVip = LuaObjectManager:GetLuaObject("GameVip")
    GameVip:CheckIsNeedShowVip(content.code)
    return true
  end
  if msgtype == NetAPIList.supply_info_ack.Code then
    local price = GameUIArena.brief_info.exchange_cost
    GameUIArena.nextExChangeCost = content.exchange_cost
    local count = content.count
    local caption_left = GameLoader:GetGameText("LC_MENU_BUTTON_CAPTION_YES")
    local caption_right = GameLoader:GetGameText("LC_MENU_BUTTON_CAPTION_CANCEL")
    local text_content = GameLoader:GetGameText("LC_MENU_BUY_BATTLE_SUPPLY_ASK")
    text_content = string.gsub(text_content, "<credits_num>", price)
    text_content = string.gsub(text_content, "<supply_num>", count)
    local function buy_supply_callback()
      local packet = {type = "pvp_supply"}
      NetMessageMgr:SendMsg(NetAPIList.supply_exchange_req.Code, packet, GameUIArena.RecoverSupplyNetCallback, false, nil)
    end
    GameUtils:CreditCostConfirm(text_content, buy_supply_callback)
    return true
  end
  return false
end
function GameUIArena:CheckBattleResult(report_data)
  local battle_result = -1
  local user_info = GameGlobalData:GetData("userinfo")
  if report_data.player1_name == user_info.name then
    if report_data.result == 1 then
      battle_result = BattleResultType.you_challenge_opponent_win
    else
      battle_result = BattleResultType.you_challenge_opponent_failed
    end
  elseif report_data.result == 1 then
    battle_result = BattleResultType.opponent_challenge_you_win
  else
    battle_result = BattleResultType.opponent_challenge_you_failed
  end
  return battle_result
end
function GameUIArena.PVPSupplyNotifyCallback()
  if GameStateManager:GetCurrentGameState():IsObjectInState(GameUIArena) then
    local pvp_supply = GameGlobalData:GetData("pvp_supply")
    GameUIArena:UpdateChallengeTimesSupply(pvp_supply.current, pvp_supply.max)
  end
end
function GameUIArena.updateWdcSupplyCallback()
  if GameUIArena:GetFlashObject() then
    local resourceCount = GameGlobalData:GetData("resource")
    if GameUIArena.advancedArenaEnterData then
      local energyText = GameUIArena:GetEnergyColor(resourceCount.wdc_supply) .. "/" .. GameUIArena.advancedArenaEnterData.max_supply
      DebugOut("energyText == " .. energyText)
      GameUIArena:GetFlashObject():InvokeASCallback("_root", "updateWdcSupply", energyText)
    end
  end
end
function GameUIArena.updateTlcSupplyCallback()
  if GameUIArena:GetFlashObject() then
    local resourceCount = GameGlobalData:GetData("resource")
    if GameUIArena.teamLeaqueCupEnterData and TeamLeagueCup.teamLeagueCupInfo ~= nil then
      local energyText = GameUIArena:GetEnergyColor(resourceCount.tlc_supply) .. "/" .. TeamLeagueCup.teamLeagueCupInfo.max_supply
      DebugOut("energyText == " .. energyText)
      if GameUIArena.teamLeaqueCupEnterData.status ~= 2 and resourceCount.tlc_supply >= TeamLeagueCup.teamLeagueCupInfo.max_supply and GameUIArena:GetFlashObject() then
        GameUIArena.TeamLeaqueCup_cd_time = nil
        GameUIArena:GetFlashObject():InvokeASCallback("_root", "SetEnterTeamLeagueCupButtonTime", "", false, GameUIArena.teamLeaqueCupEnterData.status == 2)
      end
      GameUIArena:GetFlashObject():InvokeASCallback("_root", "updateTlcSupply", energyText)
    end
  end
end
if AutoUpdate.isAndroidDevice then
  function GameUIArena.OnAndroidBack()
    if GameUIArena.mCurrentArenaType == GameUIArena.ARENA_TYPE.normalArena then
      if GameUIArena.m_showRewardShow then
        GameUIArena:OnFSCommand("closeRewardShow")
        return
      end
      if GameUIArena.isOnShowReward then
        GameUIArena:GetFlashObject():InvokeASCallback("_root", "awareBoxMoveOut")
      elseif GameUIArena.isOnShowTopRank then
        GameUIArena:GetFlashObject():InvokeASCallback("_root", "animationMoveOutToprank")
      elseif GameUIArena.isOnShowHistory then
        GameUIArena:GetFlashObject():InvokeASCallback("_root", "reportBoxMoveOut")
      elseif GameUIArena.isShowMatrix then
        GameUIArena:GetFlashObject():InvokeASCallback("_root", "hideMatrixLayer")
        GameUIArena.isShowMatrix = false
      elseif GameUIArena:GetFlashObject() then
        GameUIArena:GetFlashObject():InvokeASCallback("_root", "mainMoveOut")
      end
    elseif GameUIArena.mCurrentArenaType == GameUIArena.ARENA_TYPE.enterLayer then
      GameUIArena:GetFlashObject():InvokeASCallback("_root", "MoveOutEnterLayer")
    elseif GameUIArena.mCurrentArenaType == GameUIArena.ARENA_TYPE.wdc then
      if GameUIArena.windowType == GameUIArena.wdc_windowType.noWindow then
        GameUIAdvancedArenaLayer:AndroidBack()
      else
        GameUIArena:AndroidBack()
      end
    end
  end
end
function GameUIArena:BuildEnemyInfoByopponetInfo()
  DebugOut("BuildEnemyInfoByCompponetInfo")
  local enemyInfo = {}
  local fleetLevel = 0
  for k, v in pairs(GameUIArena.LastSelectOpponentInfo.matrix) do
    if v.identity == 1 then
      fleetLevel = v.level
    end
  end
  enemyInfo.avatar = GameUIArena.LastSelectOpponentInfo.avatar
  enemyInfo.name = GameUIArena.LastSelectOpponentInfo.name
  enemyInfo.mSex = GameUIArena.LastSelectOpponentInfo.sex
  GameUIArena.enemyInfo = enemyInfo
end
GameUIArena.mCurAwardType = GameUIArena.AWARD_TYPE.RANK
function GameUIArena:RefreshTabBtnStatus()
  local flashObj = GameUIArena:GetFlashObject()
  if flashObj then
    local leftFr = "Idle"
    local rightFr = "Idle"
    if GameUIArena.mCurAwardType == GameUIArena.AWARD_TYPE.RANK then
      leftFr = "released"
      rightFr = "Idle"
    elseif GameUIArena.mCurAwardType == GameUIArena.AWARD_TYPE.SUCCESSIVE then
      leftFr = "Idle"
      rightFr = "released"
    end
    flashObj:InvokeASCallback("_root", "SetTabBtns", leftFr, rightFr)
  end
end
function GameUIArena:SetCurAwardType(curType)
  GameUIArena.mCurAwardType = curType
  local flashObj = GameUIArena:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "SetCurAwardType", GameUIArena.mCurAwardType)
    if GameUIArena.AWARD_TYPE.SUCCESSIVE == GameUIArena.mCurAwardType then
      local cnt = 0
      if GameUIArena.mSuccessiveAwardList then
        cnt = #GameUIArena.mSuccessiveAwardList
      end
      flashObj:InvokeASCallback("_root", "SetSuccessiveAwardList", cnt)
      local btnEnable = GameUIArena.brief_info and GameUIArena.brief_info.straight_award or false
      flashObj:InvokeASCallback("_root", "SetReceiveAllBtnStatus", btnEnable)
    end
  end
  GameUIArena:RefreshTabBtnStatus()
end
function GameUIArena:UpdateSuccessiveAwardListItem(itemId)
  local flashObj = GameUIArena:GetFlashObject()
  if flashObj then
    local item = GameUIArena.mSuccessiveAwardList[itemId]
    local awardArray = {}
    for _, v in pairs(item.award) do
      local iconFrame = GameHelper:GetCommonIconFrame(v)
      local awardName = GameHelper:GetAwardTypeText(v.item_type, v.number)
      local awardNum = ""
      if v.item_type == "krypton" or v.item_type == "item" then
        awardNum = v.no
      else
        awardNum = v.number
      end
      local award = {
        awardName = awardName,
        awardNum = "x" .. tostring(awardNum),
        fr = iconFrame
      }
      table.insert(awardArray, award)
    end
    local btnFr = "blue"
    local btnEnable = false
    if 1 == item.flag then
      btnFr = "blue"
    elseif 2 == item.flag then
      btnFr = "red"
    elseif 3 == item.flag then
      btnFr = "orange"
      btnEnable = true
    end
    local btnInfo = {
      receivedText = GameLoader:GetGameText("LC_MENU_FACEBOOK_REWARDS_HISTORY"),
      receiveText = GameLoader:GetGameText("LC_MENU_TC_MISSION_AWARD_RECEIVE"),
      incompleteText = GameLoader:GetGameText("LC_MENU_TC_MISSION_AWARD_INCOMPLETE"),
      fr = btnFr,
      enable = btnEnable
    }
    DebugOut("GameUIArena:UpdateSuccessiveAwardListItem")
    local successiveText = GameLoader:GetGameText("LC_MENU_SURMOUNT_SUCCESSIVE")
    flashObj:InvokeASCallback("_root", "UpdateSuccessiveAwardListItem", itemId, successiveText, item.num, awardArray[1], awardArray[2], awardArray[3], awardArray[4], btnInfo)
  end
end
function GameUIArena:SetBreakthoughPanelInfo()
  if GameUIArena.mBreakData then
    local flashObj = GameUIArena:GetFlashObject()
    if flashObj then
      local awardArray = {}
      for _, v in pairs(GameUIArena.mBreakData.award) do
        local iconFrame = GameHelper:GetCommonIconFrame(v)
        local awardName = GameHelper:GetAwardTypeText(v.item_type, v.number)
        local awardNum = ""
        if v.item_type == "krypton" or v.item_type == "item" then
          awardNum = v.no
        else
          awardNum = v.number
        end
        local award = {
          awardName = awardName,
          awardNum = "x" .. tostring(awardNum),
          fr = iconFrame
        }
        table.insert(awardArray, award)
      end
      flashObj:InvokeASCallback("_root", "SetRank", GameUIArena.mBreakData.old)
      flashObj:InvokeASCallback("_root", "SetBreakthroughAward", awardArray[1], awardArray[2], awardArray[3])
    end
  end
end
function GameUIArena:ShowBreakthroughPanel()
  if GameUIArena.mBreakData then
    local facebookEnabled = false
    if FacebookGraphStorySharer:IsStoryEnabled(FacebookGraphStorySharer.StoryType.STORY_TYPE_ARENA_BEST_RANK) then
      facebookEnabled = true
    end
    local flashObj = GameUIArena:GetFlashObject()
    if flashObj then
      flashObj:InvokeASCallback("_root", "ShowBreakthroughPanel", facebookEnabled, self.mShareArenaBreakThroughChecked)
    end
    GameUIArena.mBestRank = GameUIArena.mBreakData.current
  end
end
function GameUIArena:HideBreakthroughPanel()
  local flashObj = GameUIArena:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "HideBreakthroughPanel")
  end
end
function GameUIArena:PlayRankUpAnimation()
  if GameUIArena.mBreakData then
    local flashObj = GameUIArena:GetFlashObject()
    if flashObj then
      flashObj:InvokeASCallback("_root", "PlayRankTo", GameUIArena.mBreakData.old, GameUIArena.mBreakData.current)
    end
    GameUIArena.mBreakData = nil
  end
end
function GameUIArena:ShowUpNum()
end
function GameUIArena:SetSuccessiveCount()
  local cnt = 0
  if GameUIArena.brief_info then
    cnt = GameUIArena.brief_info.straight_times
  end
  local flashObj = GameUIArena:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "SetSuccessiveCount", cnt)
  end
end
GameUIArena.mSuccessiveAwardList = nil
function GameUIArena:UpdateSuccessiveCanGetAward()
  if GameUIArena.mSuccessiveAwardList then
    local canGet = false
    for _, v in pairs(GameUIArena.mSuccessiveAwardList) do
      if 3 == v.flag then
        canGet = true
        break
      end
    end
    if GameUIArena.brief_info then
      GameUIArena.brief_info.straight_award = canGet
    end
  end
end
function GameUIArena:RequestBreakthroughAwardList()
  NetMessageMgr:SendMsg(NetAPIList.champion_straight_req.Code, nil, self.RequestBreakthroughAwardListCallback, true, nil)
end
function GameUIArena.RequestBreakthroughAwardListCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.champion_straight_req.Code then
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.champion_straight_ack.Code then
    DebugOut("GameUIArena.RequestBreakthroughAwardListCallback")
    DebugTable(content)
    GameUIArena.mSuccessiveAwardList = content.ack
    GameUIArena:UpdateSuccessiveCanGetAward()
    GameUIArena:SetCurAwardType(GameUIArena.AWARD_TYPE.SUCCESSIVE)
    return true
  end
  return false
end
function GameUIArena:RequestGetBreakthroughAward(idx, isGetAll)
  if GameUIArena.mSuccessiveAwardList then
    local ids = {}
    if isGetAll then
      for _, v in pairs(GameUIArena.mSuccessiveAwardList) do
        if 3 == v.flag then
          table.insert(ids, v.num)
        end
      end
    else
      table.insert(ids, GameUIArena.mSuccessiveAwardList[idx].num)
    end
    if #ids > 0 then
      local req = {ids = ids}
      DebugOut("RequestGetBreakthroughAward")
      DebugTable(ids)
      NetMessageMgr:SendMsg(NetAPIList.champion_straight_award_req.Code, req, self.RequestGetBreakthroughAwardCallback, true, nil)
    end
  end
end
function GameUIArena.RequestGetBreakthroughAwardCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.champion_straight_award_req.Code then
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    if 0 == content.code then
      GameUIArena:RequestBreakthroughAwardList()
    end
    return true
  end
  return false
end
function GameUIArena:getTutorialHelpPos(...)
  if self:GetFlashObject() then
    DebugOut("uuuu")
    self:GetFlashObject():InvokeASCallback("_root", "getTutorialHelpPos")
  end
end
function GameUIArena:getTutorialHelpEnterLayerPos()
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "getTutorialHelpEnterLayerPos")
  end
end
function GameUIArena.RequestMultiMatrixCallBack(...)
  if GameUIArena:GetFlashObject() then
    local index = FleetMatrix:getCurrentMartixIndex(FleetMatrix.MATRIX_TYPE_CHAMPION)
    DebugOut("RequestMultiMatrixCallBack_index = " .. index)
    DebugTable(FleetMatrix.matrixs_in_as)
    DebugOut(FleetMatrix:getCurrentMartixIndex(FleetMatrix.MATRIX_TYPE_CHAMPION))
    GameUIArena:GetFlashObject():InvokeASCallback("_root", "initMartix", FleetMatrix.matrixs_in_as, FleetMatrix:getCurrentMartixIndex(FleetMatrix.MATRIX_TYPE_CHAMPION))
    GameUIArena:UpdateMatrixFleetAvatar()
    local plyerText = GameLoader:GetGameText("LC_MENU_ARENA_PLAYER_MEMBER_CHAR")
    GameUIArena:GetFlashObject():InvokeASCallback("_root", "showMatrixLayer", plyerText)
  end
end
function GameUIArena:UpdateMatrixFleetAvatar(...)
  local curMatrixAvatarS = GameHelper:GetCurrentMatrixFleetAvatarAndRank(FleetMatrix.system_index)
  if GameUIArena:GetFlashObject() then
    GameUIArena:GetFlashObject():InvokeASCallback("_root", "ShowDialogChangeFormation", curMatrixAvatarS)
  end
end
GameUIArena.mBreakData = nil
function GameUIArena.BreakthroughNtfHandler(content)
  GameUIArena.mBreakData = content
end
GameUIArena.OnEnterButtonStates = {
  locked = 0,
  has_timelimt = 1,
  no_timelimt = 2,
  end_time = 3
}
function GameUIArena:OnEnterLayer()
  NetMessageMgr:SendMsg(NetAPIList.enter_champion_req.Code, nil, GameUIArena.OnEnterLayerCallback, true)
end
function GameUIArena:HideTutorial()
  GameUIArena:GetFlashObject():InvokeASCallback("_root", "hideTutorialArena")
  GameUIArena:GetFlashObject():InvokeASCallback("_root", "hideTutorialWdcArena")
  GameUIArena:GetFlashObject():InvokeASCallback("_root", "hideTutorialTlcArena")
  GameUIArena:GetFlashObject():InvokeASCallback("_root", "hideTutorialTlcArenaSearch")
  GameUIArena:GetFlashObject():InvokeASCallback("_root", "hideTutorialTlcArenaChallenge")
end
function GameUIArena.OnEnterLayerCallback(msgType, content)
  if msgType == NetAPIList.enter_champion_ack.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    GameUIBarRight:ForceMoveOutRightMenu()
    GameUIArena:SetArenaType(GameUIArena.ARENA_TYPE.enterLayer)
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateArena)
    if not GameUIArena:GetFlashObject() then
      GameUIArena:LoadFlashObject()
    end
    GameUIArena:getTutorialHelpEnterLayerPos()
    GameUIArena:HideTutorial()
    if QuestTutorialArena:IsActive() then
      GameUIArena:GetFlashObject():InvokeASCallback("_root", "showTutorialArena")
    end
    if TutorialQuestManager.QuestTutorialTlcArenaEntry:IsActive() then
      GameUIArena:GetFlashObject():InvokeASCallback("_root", "showTutorialTlcArena")
      GameUICommonDialog:ForcePlayStory({9901})
    end
    GameUIArena:OnEnterLayerMidData(content)
    return true
  end
  return false
end
function GameUIArena:GetEnergyColor(energy)
  local text = ""
  if 0 == tonumber(energy) then
    text = GameHelper:GetFoatColor("FF0000", energy)
  else
    text = tostring(energy)
  end
  return text
end
function GameUIArena:OnEnterLayerMidData(content)
  DebugOut("OnEnterLayerMidData = ")
  DebugTable(content)
  GameUIArena.normalArenaEnterData = content.local_champion
  GameUIArena.advancedArenaEnterData = content.world_champion
  GameUIArena.teamLeaqueCupEnterData = content.tlc_champion
  self.NormalChallenge_cd_time = nil
  self.AdvancedChallenge_cd_time = nil
  self.TeamLeaqueCup_cd_time = nil
  local normalArenaTable = {}
  normalArenaTable.states = GameUIArena.normalArenaEnterData.status
  if GameUIArena.normalArenaEnterData.status == 0 then
  elseif GameUIArena.normalArenaEnterData.status == 1 then
    if 0 < GameUIArena.normalArenaEnterData.cdtime then
      normalArenaTable.states = GameUIArena.OnEnterButtonStates.has_timelimt
    else
      normalArenaTable.states = GameUIArena.OnEnterButtonStates.no_timelimt
    end
  end
  normalArenaTable.left_time = GameUtils:formatTimeString(GameUIArena.normalArenaEnterData.cdtime)
  normalArenaTable.energyText = self:GetEnergyColor(GameUIArena.normalArenaEnterData.supply) .. "/" .. GameUIArena.normalArenaEnterData.max_supply
  normalArenaTable.titleText = GameLoader:GetGameText("LC_MENU_ARENA_BUTTON")
  local advancedArenaTable = {}
  if GameUIArena.advancedArenaEnterData.status == 0 or GameUIArena.advancedArenaEnterData.status == 3 then
    advancedArenaTable.states = GameUIArena.OnEnterButtonStates.locked
  elseif GameUIArena.advancedArenaEnterData.status == 1 then
    if 0 < GameUIArena.advancedArenaEnterData.cdtime then
      advancedArenaTable.states = GameUIArena.OnEnterButtonStates.has_timelimt
    else
      advancedArenaTable.states = GameUIArena.OnEnterButtonStates.no_timelimt
    end
  elseif GameUIArena.advancedArenaEnterData.status == 2 then
    DebugOut("xxff")
    advancedArenaTable.states = GameUIArena.OnEnterButtonStates.end_time
  end
  if GameUIArena.advancedArenaEnterData.status == 3 then
    advancedArenaTable.lockedText = GameLoader:GetGameText("LC_MENU_LEAGUE_NOT_OPEN")
  else
    advancedArenaTable.lockedText = string.gsub(GameLoader:GetGameText("LC_MENU_LEAGUE_OPEN_LEVEL"), "<number>", 50)
  end
  advancedArenaTable.left_time = GameUtils:formatTimeString(GameUIArena.advancedArenaEnterData.cdtime)
  advancedArenaTable.energyText = self:GetEnergyColor(GameUIArena.advancedArenaEnterData.supply) .. "/" .. GameUIArena.advancedArenaEnterData.max_supply
  advancedArenaTable.endText = GameLoader:GetGameText("LC_MENU_LEAGUE_SETTLEMENT")
  advancedArenaTable.titleText = GameLoader:GetGameText("LC_MENU_LEAGUE_TITLE")
  DebugTable(advancedArenaTable)
  local teamLeaqueCupTable = {}
  if GameUIArena.teamLeaqueCupEnterData.status == 0 or GameUIArena.teamLeaqueCupEnterData.status == 3 then
    teamLeaqueCupTable.states = GameUIArena.OnEnterButtonStates.locked
  elseif GameUIArena.teamLeaqueCupEnterData.status == 1 then
    if 0 < GameUIArena.teamLeaqueCupEnterData.cdtime then
      teamLeaqueCupTable.states = GameUIArena.OnEnterButtonStates.has_timelimt
    else
      teamLeaqueCupTable.states = GameUIArena.OnEnterButtonStates.no_timelimt
    end
  elseif GameUIArena.teamLeaqueCupEnterData.status == 2 then
    DebugOut("xxff")
    teamLeaqueCupTable.states = GameUIArena.OnEnterButtonStates.end_time
  end
  if GameUIArena.teamLeaqueCupEnterData.status == 3 then
    teamLeaqueCupTable.lockedText = GameLoader:GetGameText("LC_MENU_LEAGUE_NOT_OPEN")
  else
    teamLeaqueCupTable.lockedText = string.gsub(GameLoader:GetGameText("LC_MENU_LEAGUE_OPEN_LEVEL"), "<number>", GameUIArena.TLCUnlockLevel)
  end
  teamLeaqueCupTable.left_time = GameUtils:formatTimeString(GameUIArena.teamLeaqueCupEnterData.cdtime)
  teamLeaqueCupTable.energyText = self:GetEnergyColor(GameUIArena.teamLeaqueCupEnterData.supply) .. "/" .. GameUIArena.teamLeaqueCupEnterData.max_supply
  teamLeaqueCupTable.endText = GameLoader:GetGameText("LC_MENU_LEAGUE_SETTLEMENT")
  teamLeaqueCupTable.titleText = GameLoader:GetGameText("LC_MENU_LEAGUE_TITLE")
  DebugTable(teamLeaqueCupTable)
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "InitEnterLayerData", normalArenaTable, advancedArenaTable, teamLeaqueCupTable)
    self:GetFlashObject():InvokeASCallback("_root", "MoveInEnterLayer")
  end
  self:setNormalCDTime(GameUIArena.normalArenaEnterData.cdtime)
  self:setAdvanceArenaCDTime(GameUIArena.advancedArenaEnterData.cdtime)
  self:setTeamLeagueCupCDTime(GameUIArena.teamLeaqueCupEnterData.cdtime)
end
function GameUIArena:setNormalCDTime(time)
  self.NormalChallenge_cd_time = time + os.time()
  self:UpdateNormalCDTime()
end
function GameUIArena:setAdvanceArenaCDTime(time)
  self.AdvancedChallenge_cd_time = time + os.time()
  self:UpdateAdvancedCDTime()
end
function GameUIArena:setTeamLeagueCupCDTime(time)
  self.TeamLeaqueCup_cd_time = time + os.time()
  self:UpdateTeamLeagueCupCDTime()
end
function GameUIArena:UpdateNormalCDTime()
  local flashObject = self:GetFlashObject()
  if flashObject then
    local left_time = -1
    if self.NormalChallenge_cd_time then
      left_time = self.NormalChallenge_cd_time - os.time()
      if left_time < 0 then
        left_time = -1
        self.NormalChallenge_cd_time = nil
        local function callback(msgType, content)
          if msgType == NetAPIList.champion_cdtime_ack.Code then
            if content.local_champion_cd.cdtime and content.local_champion_cd.cdtime > 0 then
              GameUIArena:setTeamLeagueCupCDTime(content.local_champion_cd.cdtime)
            elseif content.local_champion_cd.supply and content.local_champion_cd.max_supply then
              local energyText = self:GetEnergyColor(content.local_champion_cd.supply) .. "/" .. content.local_champion_cd.max_supply
              local titleText = GameLoader:GetGameText("LC_MENU_ARENA_BUTTON")
              DebugOut("SetEnterLayerNormalArenaButtonTxt " .. energyText .. " " .. titleText)
              flashObject:InvokeASCallback("_root", "SetEnterLayerNormalArenaButtonTxt", energyText, titleText)
            end
            return true
          end
          return false
        end
        NetMessageMgr:SendMsg(NetAPIList.champion_cdtime_req.Code, nil, callback, false)
      end
    end
    flashObject:InvokeASCallback("_root", "SetEnterLayerNormalArenaButtonTime", GameUtils:formatTimeString(left_time))
  end
end
function GameUIArena:UpdateAdvancedCDTime()
  local flashObject = self:GetFlashObject()
  if flashObject then
    local left_time = -1
    if self.AdvancedChallenge_cd_time then
      left_time = self.AdvancedChallenge_cd_time - os.time()
      if left_time < 0 then
        left_time = -1
        self.AdvancedChallenge_cd_time = nil
        local function callback(msgType, content)
          if msgType == NetAPIList.champion_cdtime_ack.Code then
            if content.world_champion_cd.cdtime and content.world_champion_cd.cdtime > 0 then
              GameUIArena:setTeamLeagueCupCDTime(content.world_champion_cd.cdtime)
            elseif content.world_champion_cd.supply and content.world_champion_cd.max_supply and content.world_champion_cd.supply >= content.world_champion_cd.max_supply then
              flashObject:InvokeASCallback("_root", "SetEnterLayerAdvancedArenaButtonTime", "")
            end
            return true
          end
          return false
        end
        if GameUIArena.advancedArenaEnterData.status ~= 2 then
          NetMessageMgr:SendMsg(NetAPIList.champion_cdtime_req.Code, nil, callback, false)
        end
      end
    end
    flashObject:InvokeASCallback("_root", "SetEnterLayerAdvancedArenaButtonTime", GameUtils:formatTimeString(left_time))
  end
end
function GameUIArena:UpdateTeamLeagueCupCDTime(needSendMsg)
  local function callback(msgType, content)
    print("UpdateTeamLeagueCupCDTime_1 code=" .. tostring(content.code))
    if msgType == NetAPIList.champion_cdtime_ack.Code then
      local resourceContent = GameGlobalData:GetData("resource")
      resourceContent.tlc_supply = content.tlc_champion_cd.supply
      print("UpdateTeamLeagueCupCDTime_9")
      DebugTable(resourceContent)
      GameGlobalData:UpdateGameData("resource", resourceContent)
      resourceContent = GameGlobalData:GetData("resource")
      print("UpdateTeamLeagueCupCDTime_10")
      DebugTable(resourceContent)
      if content.tlc_champion_cd.cdtime and content.tlc_champion_cd.cdtime > 0 then
        GameUIArena:setTeamLeagueCupCDTime(content.tlc_champion_cd.cdtime)
      elseif content.tlc_champion_cd.supply and content.tlc_champion_cd.max_supply and content.tlc_champion_cd.supply >= content.tlc_champion_cd.max_supply then
        self.TeamLeaqueCup_cd_time = nil
        local energyText = GameUIArena:GetEnergyColor(content.tlc_champion_cd.supply) .. "/" .. content.tlc_champion_cd.max_supply
        if GameUIArena:GetFlashObject() ~= nil then
          GameUIArena:GetFlashObject():InvokeASCallback("_root", "SetEnterTeamLeagueCupButtonTime", "", false, GameUIArena.teamLeaqueCupEnterData.status == 2)
          GameUIArena:GetFlashObject():InvokeASCallback("_root", "updateTlcSupply", energyText)
        end
      end
      return true
    end
    return false
  end
  if needSendMsg and GameUIArena.teamLeaqueCupEnterData and GameUIArena.teamLeaqueCupEnterData.status ~= 2 then
    NetMessageMgr:SendMsg(NetAPIList.champion_cdtime_req.Code, nil, callback, false)
    return
  end
  local flashObject = self:GetFlashObject()
  if flashObject then
    local left_time = -1
    if self.TeamLeaqueCup_cd_time then
      left_time = self.TeamLeaqueCup_cd_time - os.time()
      if left_time < 0 then
        left_time = -1
        self.TeamLeaqueCup_cd_time = nil
        print(tostring(NetAPIList.champion_cdtime_req.Code))
        print("UpdateTeamLeagueCupCDTime " .. debug.traceback())
        if GameUIArena.teamLeaqueCupEnterData and GameUIArena.teamLeaqueCupEnterData.status ~= 2 then
          NetMessageMgr:SendMsg(NetAPIList.champion_cdtime_req.Code, nil, callback, false)
        end
      end
    end
    flashObject:InvokeASCallback("_root", "SetEnterTeamLeagueCupButtonTime", GameUtils:formatTimeString(left_time), left_time > 0, GameUIArena.teamLeaqueCupEnterData.status == 2)
    if GameUIArena.teamLeaqueCupEnterData and GameUIArena.teamLeaqueCupEnterData.status ~= 2 then
      GameUIArena:updateTlcSupply()
    end
  end
end
function GameUIArena.updateResourceData()
  local flashObject = GameUIArena:GetFlashObject()
  if flashObject then
    local resource = GameGlobalData:GetData("resource")
    flashObject:InvokeASCallback("_root", "updateCreditValue", GameUtils.numberConversion(resource.credit))
  end
end
function GameUIArena.UpdateFightingCapacityText()
  local flashObject = GameUIArena:GetFlashObject()
  if flashObject then
    local currentForce = GameHelper:GetFleetsForce()
    flashObject:InvokeASCallback("_root", "updateFightValue", GameUtils.numberConversion(currentForce))
  end
end
