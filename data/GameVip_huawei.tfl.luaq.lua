local GameVip = LuaObjectManager:GetLuaObject("GameVip")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
local GameWaiting = LuaObjectManager:GetLuaObject("GameWaiting")
local GameStateSetting = GameStateManager.GameStateSetting
local GameSetting = LuaObjectManager:GetLuaObject("GameSetting")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
local GameUIRechargePush = LuaObjectManager:GetLuaObject("GameUIRechargePush")
local GameVipDetailInfoPanel = LuaObjectManager:GetLuaObject("GameVipDetailInfoPanel")
local FacebookPopUI = LuaObjectManager:GetLuaObject("FacebookPopUI")
local GameUIFirstCharge = LuaObjectManager:GetLuaObject("GameUIFirstCharge")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local GameUIKrypton = LuaObjectManager:GetLuaObject("GameUIKrypton")
local GameUIWDStuff = LuaObjectManager:GetLuaObject("GameUIWDStuff")
require("data1/ProductIdentifyList.tfl")
GameVip.PriceMapCoin = {
  ["0.99"] = 120,
  ["4.99"] = 600,
  ["9.99"] = 1250,
  ["19.99"] = 2750,
  ["49.99"] = 7000,
  ["99.99"] = 15000
}
GameVip.strPayType_zfb = "PayType_zfb"
GameVip.strPayType_wechat = "PayType_wechat"
GameVip.strPayType = ""
GameVip.qihooProductPrice = {}
local _getLang = function()
  local localLang = ext.GetGameLocale()
  if GameSettingData and GameSettingData.Save_Lang then
    localLang = GameSettingData.Save_Lang
  end
  return localLang
end
GameVip.productIdentifierList = {}
GameVip.onSaleProductList = {}
GameVip.m_current_vipinfo = 0
GameVip.buyTimes = 10
GameVip.OnSaleLeftTime = 0
GameVip.OnSaleFetchTime = 0
GameVip.ShowOnSaleOldPrice = 0
GameVip.mycardType_price = "mycard_price"
GameVip.mycardType_memberPoint = "mycard_point"
GameVip.mycardType_card = "mycard_card"
GameVip.CarrierProductList = nil
GameVip.gamePriceIdentify = nil
GameVip.gamePurchaseEanble = 1
GameVip.Productions = {}
GameVip.mPayListData = nil
GameVip.first_chage = nil
GameVip.giftData = nil
GameVip.vipReward = nil
GameVip.MonthCardData = nil
GameVip.LastBuyType = "none"
GameVipExtBuyMethodsEnableList = {
  BuyMethodsList = {}
}
function GameVipExtBuyMethodsEnableList:IsMethodEnabled(methodType)
  if GameVipExtBuyMethodsEnableList.BuyMethodsList[methodType] then
    return GameVipExtBuyMethodsEnableList.BuyMethodsList[methodType].is_open == 1
  else
    return false
  end
end
function GameVipExtBuyMethodsEnableList:IsReachMethodLevelLimit(methodType)
  local playerLevel = GameGlobalData:GetData("levelinfo") and GameGlobalData:GetData("levelinfo").level or 1000
  GameUtils:printByAndroid("playerLevel = ")
  GameUtils:printByAndroid(playerLevel)
  GameUtils:printByAndroid(GameVipExtBuyMethodsEnableList.BuyMethodsList[methodType].level)
  if GameVipExtBuyMethodsEnableList.BuyMethodsList[methodType] and playerLevel >= GameVipExtBuyMethodsEnableList.BuyMethodsList[methodType].level then
    return true
  end
  return false
end
function GameVipExtBuyMethodsEnableList.BuyMethodsEnableStateHandler(content)
  for i, v in ipairs(content.pay_limit_infos) do
    GameVipExtBuyMethodsEnableList.BuyMethodsList[v.pay_type] = v
  end
  GameUtils:DebugOutTableInAndroid(GameVipExtBuyMethodsEnableList.BuyMethodsList)
end
function GameVip:IsFastEnhanceUnlocked()
  local curLevel = GameVipDetailInfoPanel:GetVipLevel()
  local player_level = GameGlobalData:GetData("levelinfo").level
  DebugOut("curLevel = ", curLevel)
  if immanentversion == 2 then
    return curLevel >= 2
  else
    return curLevel >= GameDataAccessHelper:GetVIPLimit("one_key_equip_enhance") and player_level >= GameDataAccessHelper:GetLevelLimit("one_key_equip_enhance")
  end
  return false
end
function GameVip:OnInitGame()
  GameGlobalData:RegisterDataChangeCallback("vipinfo", GameVip.RefreshVipInfo)
  GameGlobalData:RegisterDataChangeCallback("resource", GameVip.RefreshVipInfo)
  NetMessageMgr:RegisterMsgHandler(NetAPIList.pay_status_ntf.Code, GameVip.getPurchaseEnableInfo)
  NetMessageMgr:RegisterMsgHandler(NetAPIList.pay_limit_ntf.Code, GameVipExtBuyMethodsEnableList.BuyMethodsEnableStateHandler)
  NetMessageMgr:RegisterMsgHandler(NetAPIList.show_price_ntf.Code, GameVip.ShowOnSaleOldPrice)
end
function GameVip.ShowOnSaleOldPrice(content)
  if content then
    GameVip.ShowOnSaleOldPrice = content.status
  end
end
function GameVip:CheckInitGameData()
  if not self.vipExp then
    self.vipExp = GameDataAccessHelper:GetVipExp()
    self.vipExp = LuaUtils:table_values(self.vipExp)
    table.sort(self.vipExp, function(v1, v2)
      return v1.level < v2.level
    end)
  end
end
function GameVip.RefreshVipInfo()
  if not GameVip:GetFlashObject() then
    return
  end
  if GameStateManager:GetCurrentGameState():IsObjectInState(GameVip) then
    local vipInfo = GameGlobalData:GetData("vipinfo")
    local resource = GameGlobalData:GetData("resource")
    local tipsText = GameLoader:GetGameText("LC_MENU_CREDITS_VIP")
    tipsText = string.gsub(tipsText, "<CREDIT>", tostring(vipInfo.next_level_exp - vipInfo.current_exp))
    tipsText = string.gsub(tipsText, "<VIP>", tostring(vipInfo.level + 1))
    if vipInfo.next_level_exp == vipInfo.current_exp then
      tipsText = ""
    end
    local m, k = 0, 0
    if vipInfo.level >= 10 then
      m = math.floor(vipInfo.level / 10)
      k = vipInfo.level - m * 10
    else
      k = vipInfo.level
    end
    DebugOut("RefreshVipInfo: ", m, k, creditsText, tipsText)
    if vipInfo.level == 12 then
      tipsText = GameLoader:GetGameText("LC_ALERT_vip_level_not_exist")
    end
    GameVip:GetFlashObject():InvokeASCallback("_root", "RefreshVipInfo", m, k, vipInfo.next_level_exp, vipInfo.current_exp, tipsText)
    GameVip:GetFlashObject():InvokeASCallback("_root", "ShowCredit", GameUtils.numberConversion2(resource.credit, false))
    if vipInfo.level == 0 and vipInfo.current_exp == 0 then
      GameVip.first_chage = true
    else
      GameVip.first_chage = false
    end
    local first_chageDecTest = GameLoader:GetGameText("LC_MENU_FIRSTCHARGE_DOUBLE_INFO")
    GameVip:GetFlashObject():InvokeASCallback("_root", "isShowFirstChageFlag", false, first_chageDecTest)
  end
end
function GameVip:OnAddToGameState()
  GameVip.RefreshVipInfo()
  self.entervip = false
  GameVip.hasPaySuccess = false
  local month_card_callback = function(msgType, content)
    if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.month_card_buy_req.Code then
      return true
    end
    return false
  end
  local buy_month_param = {id = 0, gift_user = 0}
  NetMessageMgr:SendMsg(NetAPIList.month_card_buy_req.Code, buy_month_param, month_card_callback, false, nil)
  local localAppid = ext.GetBundleIdentifier()
  if localAppid ~= "com.ts.galaxyempire2_android_global" and localAppid ~= "com.tap4fun.galaxyempire2_android" or IPlatformExt.getConfigValue("GameServerPriceControl") == "True" then
  else
    self.storeObject = self.storeObject or StoreObject:new()
    self.storeObject:RequestProductData(self.productIdentifierList)
  end
  if IPlatformExt.getConfigValue("PlatformName") == "taptap" then
    self.storeObjectWechat = StoreObjectWechat:new()
    NetMessageMgr:RegisterMsgHandler(NetAPIList.wxpay_info_ntf.Code, GameVip.on_wxpay_info_ntf)
  end
  GameVip:ReqGiftData()
  GameVip:ReqMonthCardData()
  GameTimer:Add(self._OnTimerTick, 1000)
end
function GameVip:OnEraseFromGameState()
  GameVip.LastBuyType = "none"
  self:UnloadFlashObject()
end
function GameVip._OnTimerTick()
  if GameVip.giftData then
    for k, v in pairs(GameVip.giftData.list) do
      local left = v.left_time - (os.time() - v.baseTime)
      if left < 0 then
        left = 0
      end
      if v.left_time >= 0 and left == 0 then
        v.isEnd = true
      end
      v.leftTimeStr = GameUtils:formatTimeString(left)
      local itemIndex = math.ceil(k / 4)
      local subIndex = k % 4 == 0 and 4 or k % 4
      if GameVip:GetFlashObject() then
        GameVip:GetFlashObject():InvokeASCallback("_root", "UpdateGiftItemMC", itemIndex, subIndex, v)
      end
    end
  end
  if GameVip.MonthCardData then
    for k, v in pairs(GameVip.MonthCardData.monthList or {}) do
      local left = v.left_time - (os.time() - v.baseTime)
      if left < 0 then
        left = 0
      end
      if v.left_time >= 0 and left == 0 then
        v.isEnd = true
      end
      v.leftTimeStr = GameUtils:formatTimeString(left)
      local itemIndex = math.ceil(k / 4)
      local subIndex = k % 4 == 0 and 4 or k % 4
      if GameVip:GetFlashObject() then
        GameVip:GetFlashObject():InvokeASCallback("_root", "UpdateMonthCardItemMC", itemIndex, subIndex, v, _getLang())
      end
    end
  end
  if GameStateManager:GetCurrentGameState():IsObjectInState(GameVip) then
    return 1000
  end
  return nil
end
function GameVip:ReqVipRewardList()
  NetMessageMgr:SendMsg(NetAPIList.vip_reward_req.Code, param, GameVip.ReqVipRewardCallBack, false, nil)
end
function GameVip.ReqVipRewardCallBack(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.vip_reward_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgtype == NetAPIList.vip_reward_ack.Code then
    GameVip:GenerateVipData(content)
    GameVip:InitVipContent()
    return true
  end
  return false
end
function GameVip:UpdateVipRewardItem(vip, itemIndex, whichList)
  local item = GameVip.vipReward[tonumber(vip)]
  DebugOut("")
  if item then
    local reward = item.rewardList[tonumber(itemIndex)]
    GameVip:GetFlashObject():InvokeASCallback("_root", "UpdateVipRewardItem", whichList, reward, itemIndex)
  end
end
function GameVip:GenerateVipData(content)
  GameVip.vipReward = {}
  for k, v in ipairs(content.vip_items or {}) do
    local item = {}
    item.rewardList = {}
    item.titleName = string.gsub(GameLoader:GetGameText("LC_MENU_PAID_WALL_FUNCTION_VIP_PACKAGE"), "<viplv_num>", "VIP " .. v.vip)
    item.vip = v.vip
    for _, v2 in ipairs(v.rewards or {}) do
      local tmp = {}
      tmp.name = GameHelper:GetAwardTypeText(v2.item_type, v2.number)
      tmp.count = GameHelper:GetAwardCount(v2.item_type, v2.number, v2.no)
      tmp.displayCount = GameUtils.numberConversion(math.floor(tmp.count))
      tmp.frame = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(v2)
      tmp.model = v2
      tmp.vip = v.vip
      item.rewardList[#item.rewardList + 1] = tmp
    end
    GameVip.vipReward[v.vip] = item
  end
end
function GameVip:ReqWishProductBeferBuy(id, productID, count, callback)
  if id == nil or productID == nil then
    return false
  end
  local param = {}
  param.id = tostring(id)
  param.product_id = productID
  param.count = count or 1
  function wishPaycallback(msgtype, content)
    if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.wish_pay_req.Code then
      if content.code ~= 0 then
        GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      elseif callback then
        callback()
      end
      return true
    end
    return false
  end
  NetMessageMgr:SendMsg(NetAPIList.wish_pay_req.Code, param, wishPaycallback, true, nil)
  return true
end
function GameVip:ReqWishProduct(id, productID, count, callback)
  if id == nil or productID == nil then
    return false
  end
  local param = {}
  param.id = tostring(id)
  param.product_id = productID
  param.count = count or 1
  if id ~= productID and GameVip:GetFlashObject() then
    GameVip:GetFlashObject():InvokeASCallback("_root", "ClosePopGift")
  end
  function wishPaycallback(msgtype, content)
    if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.wish_pay_req.Code then
      if content.code ~= 0 then
      end
      if callback then
        callback()
      end
      GameVip.IsReqWish = false
      return true
    end
    return false
  end
  GameVip.IsReqWish = true
  NetMessageMgr:SendMsg(NetAPIList.wish_pay_req.Code, param, wishPaycallback, false, nil)
  if id ~= productID then
    local item = GameVip:GetGiftItemByID(id)
    if item and item.limit_type > 0 and 0 < item.limit_count then
      item.limit_count = item.limit_count - 1
      if 0 >= item.limit_count then
        item.order = item.order + 1000
      end
      if item.limit_type == 1 then
        item.limitText = GameLoader:GetGameText("LC_MENU_SHOP_DAY_BUY_TIME_CHAR") .. item.limit_count
      elseif item.limit_type == 2 then
        item.limitText = GameLoader:GetGameText("LC_MENU_SHOP_BUY_TIME_CHAR") .. item.limit_count
      else
        item.limitText = ""
      end
    end
  end
  GameVip.BuyID = nil
  GameVip.BuyProductID = nil
  GameVip.BuyCount = 1
  return true
end
function GameVip:ReqGiftData()
  DebugOut("GameVip:ReqGiftData")
  if GameVip.LastBuyType == "none" or GameVip.LastBuyType == "gift" then
    NetMessageMgr:SendMsg(NetAPIList.pay_gifts_req.Code, nil, GameVip.ReqGiftDataCallBack, true, nil)
  end
end
function GameVip.ReqGiftDataCallBack(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.pay_gifts_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    GameVip.LastBuyType = "none"
    return true
  elseif msgtype == NetAPIList.pay_gifts_ack.Code then
    GameVip:GenerateGiftData(content)
    GameVip:SetGiftData()
    GameVip.LastBuyType = "none"
    return true
  end
  return false
end
function GameVip:Select_zfb_wechat(startfunc, arg, arg2)
  local function callback_zfb()
    startfunc(arg, arg2)
    print("xxpp zfb")
    GameVip.strPayType = GameVip.strPayType_zfb
  end
  local function callback_wechat()
    startfunc(arg, arg2)
    print("xxpp wechat")
    GameVip.strPayType = GameVip.strPayType_wechat
  end
  local tPayOption = {}
  tPayOption.callback_wechat = callback_wechat
  tPayOption.callback_zfb = callback_zfb
  ItemBox:ShowPayOption(tPayOption)
end
function GameVip:RecovePrice(price, discount_rate)
  if price and discount_rate then
    local priceCurrency, priceStr, priceDotNumber, priceCurrency2 = string.match(price, "([^%d]*)(%d+[^%d]-%d*([^%d]-%d-))([^%d]*)$")
    if priceStr then
      local commaIndex = string.find(priceStr, ",")
      local commaReverseIndex
      local dotIndex = string.find(priceStr, ".", 1, true)
      local dotReverseIndex
      if commaIndex then
        commaReverseIndex = string.len(priceStr) - commaIndex
      end
      if dotIndex then
        dotReverseIndex = string.len(priceStr) - dotIndex
      end
      local priceNum = string.gsub(priceStr, "[^%d]*", "")
      if priceNum then
        DebugOut("priceNum = ", priceNum)
        DebugOut("commaIndex = ", commaIndex)
        DebugOut("dotIndex = ", dotIndex)
        local priceRealNum = tonumber(priceNum)
        if priceRealNum then
          local newPrice = math.floor(priceRealNum / discount_rate) .. ""
          DebugOut("newPrice = ", newPrice)
          local resultStr = newPrice
          if dotReverseIndex then
            if commaReverseIndex then
              if commaReverseIndex > dotReverseIndex then
                local tmpResultStr = LuaUtils:string_insert(newPrice:reverse(), ".", dotReverseIndex)
                DebugOut("tmpResultStr 555555= ", tmpResultStr)
                tmpResultStr = LuaUtils:string_insert(tmpResultStr, ",", commaReverseIndex)
                DebugOut("tmpResultStr 6666666= ", tmpResultStr)
                resultStr = tmpResultStr:reverse()
                DebugOut("tmpResultStr 33333= ", tmpResultStr)
              else
                local tmpResultStr = LuaUtils:string_insert(newPrice:reverse(), ",", commaReverseIndex)
                DebugOut("tmpResultStr 777777= ", tmpResultStr)
                tmpResultStr = LuaUtils:string_insert(tmpResultStr, ".", dotReverseIndex)
                DebugOut("tmpResultStr 888888= ", tmpResultStr)
                resultStr = tmpResultStr:reverse()
                DebugOut("tmpResultStr 44444= ", tmpResultStr)
              end
            else
              local tmpResultStr = LuaUtils:string_insert(newPrice:reverse(), ".", dotReverseIndex)
              DebugOut("tmpResultStr 99999= ", tmpResultStr)
              resultStr = tmpResultStr:reverse()
            end
          elseif commaReverseIndex then
            local tmpResultStr = LuaUtils:string_insert(newPrice:reverse(), ",", commaReverseIndex)
            DebugOut("tmpResultStr 111= ", tmpResultStr)
            resultStr = tmpResultStr:reverse()
            DebugOut("resultStr 11111= ", resultStr)
          end
          if priceCurrency and resultStr then
            resultStr = priceCurrency .. resultStr
          end
          if priceCurrency2 and resultStr then
            resultStr = resultStr .. priceCurrency2
          end
          DebugOut("resultStr 22 = ", resultStr)
          return resultStr
        end
      end
    end
    return ""
  end
  return ""
end
function GameVip:SortVipGiftData()
  if GameVip.giftData and GameVip.giftData.list then
    DebugTable(GameVip.giftData.list)
    table.sort(GameVip.giftData.list, function(a, b)
      return a.order < b.order
    end)
  end
end
function GameVip:GenerateGiftDataItem(v)
  local item = {}
  item = LuaUtils:table_copy(v)
  item.deviceType = GameUtils:isIOSBundle() and "IOS" or "Android"
  item.baseTime = os.time()
  item.rewardList = {}
  item.name = GameLoader:GetGameText("LC_MENU_PAID_WALL_FUNCTION_PACKAGE_" .. item.id)
  item.saleOutText = GameLoader:GetGameText("LC_MENU_SOLD_OUT_ALREADY")
  item.unLimitText = GameLoader:GetGameText("LC_MENU_TIME_UNLIMITED")
  if item.limit_type == 1 then
    item.limitText = GameLoader:GetGameText("LC_MENU_SHOP_DAY_BUY_TIME_CHAR") .. item.limit_count
  elseif item.limit_type == 2 then
    item.limitText = GameLoader:GetGameText("LC_MENU_SHOP_BUY_TIME_CHAR") .. item.limit_count
  else
    item.limitText = ""
  end
  local left = item.left_time - (os.time() - item.baseTime)
  if left < 0 then
    left = 0
  end
  item.leftTimeStr = GameUtils:formatTimeString(left)
  local price = GameVip.GetPriceByProductID(item.product_id)
  item.original_price = ""
  item.discount_price = price
  local discount = 1 - item.discount / 1000 == 0 and 1 or 1 - item.discount / 1000
  item.original_price = GameVip:RecovePrice(price, discount)
  for k, v2 in ipairs(item.goods or {}) do
    local tmp = {}
    tmp.name = GameHelper:GetAwardTypeText(v2.item_type, v2.number, v2.no) .. "x" .. GameUtils.numberConversion(GameHelper:GetAwardCount(v2.item_type, v2.number, v2.no))
    tmp.frame = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(v2)
    tmp.model = v2
    item.rewardList[#item.rewardList + 1] = tmp
  end
  return item
end
function GameVip:GenerateGiftData(content)
  GameVip.giftData = nil
  GameVip.giftData = {}
  GameVip.giftData.list = {}
  GameVip.giftData.hidelist = {}
  for k, v in ipairs(content.gifts or {}) do
    local item = GameVip:GenerateGiftDataItem(v)
    if item.can_show == nil or item.can_show == true then
      GameVip.giftData.list[#GameVip.giftData.list + 1] = item
    else
      GameVip.giftData.hidelist[#GameVip.giftData.hidelist + 1] = item
    end
  end
  GameVip:SortVipGiftData()
end
function GameVip:SetGiftData()
  if GameVip.giftData and GameVip:GetFlashObject() then
    GameVip:GetFlashObject():InvokeASCallback("_root", "SetGiftData", math.ceil(#GameVip.giftData.list / 4))
  end
end
function GameVip:UpdateGiftItem(itemIndex)
  local list = {}
  for i = (itemIndex - 1) * 4 + 1, itemIndex * 4 do
    if GameVip.giftData.list[i] then
      local localPath = "data2/" .. DynamicResDownloader:GetFullName(GameVip.giftData.list[i].bg_pic, DynamicResDownloader.resType.WELCOME_PIC)
      if GameVip.giftData.list[i].bg_pic ~= "undefined" and ext.crc32.crc32(localPath) ~= "" and ext.crc32.crc32(localPath) ~= "0" then
        GameVip.giftData.list[i].bgImg = GameVip.giftData.list[i].bg_pic
      elseif GameVip.giftData.list[i].bg_pic == "undefined" then
        GameVip.giftData.list[i].bgImg = "shop_bg_01.png"
      else
        GameVip.giftData.list[i].bgImg = "shop_bg_01.png"
        local extendInfo = {}
        extendInfo.index = i
        extendInfo.itemIndex = itemIndex
        DynamicResDownloader:AddDynamicRes(GameVip.giftData.list[i].bg_pic, DynamicResDownloader.resType.WELCOME_PIC, extendInfo, GameVip.DynamicBackgroundCallback)
      end
      localPath = "data2/" .. DynamicResDownloader:GetFullName(GameVip.giftData.list[i].bg_circle_pic, DynamicResDownloader.resType.WELCOME_PIC)
      if GameVip.giftData.list[i].bg_circle_pic ~= "undefined" and ext.crc32.crc32(localPath) ~= "" and ext.crc32.crc32(localPath) ~= "0" then
        GameVip.giftData.list[i].bgCircleImg = GameVip.giftData.list[i].bg_circle_pic
      elseif GameVip.giftData.list[i].bg_circle_pic == "undefined" then
        GameVip.giftData.list[i].bgCircleImg = "shop_bg_new1.png"
      else
        GameVip.giftData.list[i].bgCircleImg = "shop_bg_new1.png"
        local extendInfo = {}
        extendInfo.index = i
        extendInfo.itemIndex = itemIndex
        DynamicResDownloader:AddDynamicRes(GameVip.giftData.list[i].bg_circle_pic, DynamicResDownloader.resType.WELCOME_PIC, extendInfo, GameVip.DynamicBGCircleCallback)
      end
      localPath = "data2/" .. DynamicResDownloader:GetFullName(GameVip.giftData.list[i].bg_limit_pic, DynamicResDownloader.resType.WELCOME_PIC)
      if GameVip.giftData.list[i].bg_limit_pic ~= "undefined" and ext.crc32.crc32(localPath) ~= "" and ext.crc32.crc32(localPath) ~= "0" then
        GameVip.giftData.list[i].bgLimitImg = GameVip.giftData.list[i].bg_limit_pic
      elseif GameVip.giftData.list[i].bg_limit_pic == "undefined" then
        GameVip.giftData.list[i].bgLimitImg = "store_sale_bg_01.png"
      else
        GameVip.giftData.list[i].bgLimitImg = "store_sale_bg_01.png"
        local extendInfo = {}
        extendInfo.index = i
        extendInfo.itemIndex = itemIndex
        DynamicResDownloader:AddDynamicRes(GameVip.giftData.list[i].bg_limit_pic, DynamicResDownloader.resType.WELCOME_PIC, extendInfo, GameVip.DynamicBGLimitCallback)
      end
      localPath = "data2/" .. DynamicResDownloader:GetFullName(GameVip.giftData.list[i].icon_pic, DynamicResDownloader.resType.WELCOME_PIC)
      if GameVip.giftData.list[i].icon_pic ~= "undefined" and ext.crc32.crc32(localPath) ~= "" and ext.crc32.crc32(localPath) ~= "0" then
        GameVip.giftData.list[i].bgIconImg = GameVip.giftData.list[i].icon_pic
      elseif GameVip.giftData.list[i].icon_pic ~= "undefined" then
        GameVip.giftData.list[i].bgIconImg = "item_vip.png"
        local extendInfo = {}
        extendInfo.index = i
        extendInfo.itemIndex = itemIndex
        DynamicResDownloader:AddDynamicRes(GameVip.giftData.list[i].icon_pic, DynamicResDownloader.resType.WELCOME_PIC, extendInfo, GameVip.DynamicIconCallback)
      else
        GameVip.giftData.list[i].bgIconImg = "item_vip.png"
      end
      list[#list + 1] = GameVip.giftData.list[i]
    end
  end
  self:GetFlashObject():InvokeASCallback("_root", "UpdateGiftItem", itemIndex, list)
end
function GameVip.DynamicBackgroundCallback(extinfo)
  local item = GameVip.giftData.list[extinfo.index]
  item.bgImg = item.bg_pic
  local subIndex = extinfo.index % 4 == 0 and 4 or extinfo.index % 4
  if GameVip:GetFlashObject() then
    GameVip:GetFlashObject():InvokeASCallback("_root", "UpdateGiftItemMC", extinfo.itemIndex, subIndex, item)
  end
end
function GameVip.DynamicBGCircleCallback(extinfo)
  local item = GameVip.giftData.list[extinfo.index]
  item.bgCircleImg = item.bg_circle_pic
  local subIndex = extinfo.index % 4 == 0 and 4 or extinfo.index % 4
  if GameVip:GetFlashObject() then
    GameVip:GetFlashObject():InvokeASCallback("_root", "UpdateGiftItemMC", extinfo.itemIndex, subIndex, item)
  end
end
function GameVip.DynamicBGLimitCallback(extinfo)
  local item = GameVip.giftData.list[extinfo.index]
  item.bgLimitImg = item.bg_limit_pic
  local subIndex = extinfo.index % 4 == 0 and 4 or extinfo.index % 4
  if GameVip:GetFlashObject() then
    GameVip:GetFlashObject():InvokeASCallback("_root", "UpdateGiftItemMC", extinfo.itemIndex, subIndex, item)
  end
end
function GameVip.DynamicIconCallback(extinfo)
  local item = GameVip.giftData.list[extinfo.index]
  item.bgIconImg = item.icon_pic
  local subIndex = extinfo.index % 4 == 0 and 4 or extinfo.index % 4
  if GameVip:GetFlashObject() then
    GameVip:GetFlashObject():InvokeASCallback("_root", "UpdateGiftItemMC", extinfo.itemIndex, subIndex, item)
  end
end
function GameVip.DynamicBGBottomCallback(extinfo)
  local item = GameVip.giftData.list[extinfo.index]
  item.bgBottomImg = item.icon_pic
  local subIndex = extinfo.index % 4 == 0 and 4 or extinfo.index % 4
  if GameVip:GetFlashObject() then
    GameVip:GetFlashObject():InvokeASCallback("_root", "UpdateGiftItemMC", extinfo.itemIndex, subIndex, item)
  end
end
function GameVip:UpdateGiftIncludeItem(itemIndex, index)
  local item = GameVip:GetGiftItemByID(index)
  if item then
    DebugOut("GameVip:UpdateGiftIncludeItem:", item.rewardList[itemIndex])
    GameVip:GetFlashObject():InvokeASCallback("_root", "UpdateGiftInclude", itemIndex, item.rewardList[itemIndex])
  end
end
function GameVip:ReqMonthCardData()
  DebugOut("GameVip:ReqMonthCardData:", GameVip.LastBuyType)
  if GameVip.LastBuyType == "none" or GameVip.LastBuyType == "month" then
    NetMessageMgr:SendMsg(NetAPIList.month_card_req.Code, nil, GameVip.ReqMonthCardCallBack, true, nil)
  end
end
function GameVip.ReqMonthCardCallBack(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.month_card_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    GameVip.LastBuyType = "none"
    return true
  elseif msgtype == NetAPIList.month_card_ack.Code then
    GameVip:GenerateMonthCardData(content)
    GameVip:SetMonthCardData()
    GameVip.LastBuyType = "none"
    return true
  end
  return false
end
function GameVip:GenerateMonthCardData(content)
  GameVip.MonthCardData = {}
  GameVip.MonthCardData.monthList = {}
  for k, v in ipairs(content.month_cards or {}) do
    local item = {}
    item = LuaUtils:table_copy(v)
    item.titleText = GameLoader:GetGameText(item.title_key)
    item.descText = GameLoader:GetGameText(item.desc_key)
    item.baseTime = os.time()
    local left = item.left_time - (os.time() - item.baseTime)
    if left < 0 then
      left = 0
    end
    if item.left_time >= 0 and left == 0 then
      item.isEnd = true
    end
    item.leftTimeStr = GameUtils:formatTimeString(left)
    local price = ""
    if item.product_id ~= "undefined" then
      price = GameVip.GetPriceByProductID(item.product_id)
    end
    item.count = GameUtils.numberConversion(GameHelper:GetAwardCount(item.reward.item_type, item.reward.number, item.reward.no))
    item.name = GameHelper:GetAwardTypeText(item.reward.item_type, item.reward.number, item.reward.no)
    item.frame = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(item.reward)
    item.perDayText = GameLoader:GetGameText("LC_MENU_EVENT_TIMING_DAY")
    if item.status == 0 then
      item.btnText = price
      item.tipText = string.gsub(GameLoader:GetGameText("LC_MENU_BENEFITS_validity_time"), "<number1>", item.state_day)
    elseif item.status == 1 or item.status == 2 then
      item.btnText = GameLoader:GetGameText("LC_MENU_SURMOUNT_COLLECT_BUTTON")
      item.tipText = string.gsub(GameLoader:GetGameText("LC_MENU_BENEFITS_countdown"), "<number1>", item.state_day)
    end
    GameVip.MonthCardData.monthList[#GameVip.MonthCardData.monthList + 1] = item
  end
  GameVip:SortMonthCards()
  GameVip:RefreshMonthRedPoint()
end
function GameVip:SortMonthCards()
  if GameVip.MonthCardData and GameVip.MonthCardData.monthList then
    table.sort(GameVip.MonthCardData.monthList, function(a, b)
      return a.order < b.order
    end)
  end
end
function GameVip:RefreshMonthRedPoint()
  if GameVip.MonthCardData and GameVip.MonthCardData.monthList then
    local isShow = false
    for k, v in pairs(GameVip.MonthCardData.monthList) do
      if v.status == 1 then
        isShow = true
        break
      end
    end
    if GameVip:GetFlashObject() then
      GameVip:GetFlashObject():InvokeASCallback("_root", "SetMonthCardRedPoint", isShow)
    end
  end
end
function GameVip:SetMonthCardData()
  DebugOut("GameVip:SetMonthCardData")
  if GameVip.MonthCardData and GameVip.MonthCardData.monthList then
    local itemCount = math.ceil(#GameVip.MonthCardData.monthList / 4)
    if GameVip:GetFlashObject() then
      GameVip:GetFlashObject():InvokeASCallback("_root", "SetMonthCardData", itemCount)
    end
  end
end
function GameVip:UpdateMonthCardItem(itemIndex)
  local list = {}
  DebugOut("GameVip:UpdateMonthCardItem:", itemIndex)
  for i = (itemIndex - 1) * 4 + 1, itemIndex * 4 do
    if GameVip.MonthCardData.monthList[i] then
      local localPath = "data2/" .. DynamicResDownloader:GetFullName(GameVip.MonthCardData.monthList[i].bg_pic, DynamicResDownloader.resType.WELCOME_PIC)
      if GameVip.MonthCardData.monthList[i].bg_pic ~= "undefined" and ext.crc32.crc32(localPath) ~= "" and ext.crc32.crc32(localPath) ~= "0" then
        GameVip.MonthCardData.monthList[i].bgImg = GameVip.MonthCardData.monthList[i].bg_pic
      elseif GameVip.MonthCardData.monthList[i].bg_pic == "undefined" then
        GameVip.MonthCardData.monthList[i].bgImg = "moren.png"
      else
        GameVip.MonthCardData.monthList[i].bgImg = "moren.png"
        local extendInfo = {}
        extendInfo.index = i
        extendInfo.itemIndex = itemIndex
        DynamicResDownloader:AddDynamicRes(GameVip.MonthCardData.monthList[i].bg_pic, DynamicResDownloader.resType.WELCOME_PIC, extendInfo, GameVip.DynamicMonthBackgroundCallback)
      end
      list[#list + 1] = GameVip.MonthCardData.monthList[i]
    end
  end
  local itemCount = math.ceil(#GameVip.MonthCardData.monthList / 4)
  local lineCount = #GameVip.MonthCardData.monthList % 4 == 0 and 4 or #GameVip.MonthCardData.monthList % 4
  self:GetFlashObject():InvokeASCallback("_root", "UpdateMonthCardItem", itemIndex, list, itemCount, lineCount, _getLang())
end
function GameVip.DynamicMonthBackgroundCallback(extinfo)
  local item = GameVip.MonthCardData.monthList[extinfo.index]
  item.bgImg = item.bg_pic
  local subIndex = extinfo.index % 4 == 0 and 4 or extinfo.index % 4
  if GameVip:GetFlashObject() then
    GameVip:GetFlashObject():InvokeASCallback("_root", "UpdateMonthCardItemMC", extinfo.itemIndex, subIndex, item, _getLang())
  end
end
function GameVip:GetMonthCardItemById(id)
  if GameVip.MonthCardData then
    for k, v in pairs(GameVip.MonthCardData.monthList or {}) do
      if v.id == id then
        return v
      end
    end
  end
  return nil
end
function GameVip:ReceiveMonthCardReward(id)
  local param = {}
  param.id = id
  DebugOut("TP:ID", id)
  NetMessageMgr:SendMsg(NetAPIList.month_card_receive_reward_req.Code, param, GameVip.ReceiveMonthCardRewardCallBack, true, nil)
end
function GameVip.ReceiveMonthCardRewardCallBack(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.month_card_receive_reward_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    GameVip:ReqMonthCardData()
    return true
  elseif msgtype == NetAPIList.month_card_receive_reward_ack.Code then
    GameVip:GenerateMonthCardData(content)
    if GameVip:GetFlashObject() then
      GameVip:GetFlashObject():InvokeASCallback("_root", "RefreshMonthCardData")
    end
    return true
  end
  return false
end
function GameVip:OpenGiftDetail(id)
  local item = GameVip:GetGiftItemByID(id)
  if item then
    if item.limit_type > 0 and 0 >= item.limit_count then
      return
    end
    GameVip:GetFlashObject():InvokeASCallback("_root", "ShowGiftDetail", item)
  end
end
function GameVip:GetGiftItemByID(id)
  if GameVip.giftData == nil then
    return nil
  end
  for k, v in pairs(GameVip.giftData.list or {}) do
    if v.id == id then
      return v
    end
  end
  for k, v in pairs(GameVip.giftData.hidelist or {}) do
    if v.id == id then
      return v
    end
  end
  return nil
end
function GameVip:setVipTitle()
  local temp_title = string.format(GameLoader:GetGameText("LC_MENU_VIP_TITLE"), self.m_current_vipinfo)
  self:GetFlashObject():InvokeASCallback("_root", "vipPanelReset", self.m_current_vipinfo, self.maxId, temp_title)
end
function GameVip:showVip(isShowVip, closeCallbackFunc, tab)
  GameVip.BankMonthCard:Clear()
  GameVip.CloseCallbackFunc = closeCallbackFunc
  if not GameStateManager:GetCurrentGameState():IsObjectInState(self) then
    GameStateManager:GetCurrentGameState():AddObject(self)
    self:LoadFlashObject()
    local lang = "en"
    if GameSettingData and GameSettingData.Save_Lang then
      lang = GameSettingData.Save_Lang
      if string.find(lang, "ru") == 1 then
        lang = "ru"
      else
        lang = "en"
      end
    end
    self:GetFlashObject():InvokeASCallback("_root", "MoveIn", lang)
    local isAndroidChinese = AutoUpdate.isAndroidDevice and ext.GetBundleIdentifier() ~= AutoUpdate.bundleIdentifierType.BUNDLE_IDENTIFIER_NORMAL_ANDROID and ext.GetBundleIdentifier() ~= AutoUpdate.bundleIdentifierType.BUNDLE_IDENTIFIER_NORMAL_ANDROID_LOCAL
    if not isAndroidChinese and ext.GetBundleIdentifier() == AutoUpdate.bundleIdentifierType.BUNDLE_IDENTIFIER_NORMAL_ANDROID_LOCAL then
      isAndroidChinese = AutoUpdate.localAppVersion < 10808
    end
    self:GetFlashObject():InvokeASCallback("_root", "showVip", isShowVip, false, tab, GameLoader:GetGameText("LC_MENU_PAID_WALL_FUNCTION_FIRST_RECHARGE"), isAndroidChinese)
    local QuickBilVisble = false
    if GameVip.CarrierProductList and GameVip.CarrierProductList.productions and #GameVip.CarrierProductList.productions > 0 then
      QuickBilVisble = true
    end
    self:GetFlashObject():InvokeASCallback("_root", "setQuickBilVisble", QuickBilVisble)
    if isShowVip then
      self:initVipData()
    else
      if not self.isPriceValid then
        self.storeObject = self.storeObject or StoreObject:new()
        self.storeObject:RequestProductData(self.productIdentifierList)
        DebugOut("wait for apple store")
        GameWaiting:ShowLoadingScreen()
      else
        DebugOut("wait for our server")
        GameVip:RequestProductIdentifierList()
        GameWaiting:ShowLoadingScreen()
      end
      if QuickBilVisble then
        self:GetFlashObject():InvokeASCallback("_root", "InitAndShowCarrierQuickBil", GameVip.CarrierProductList)
      end
    end
  end
end
function GameVip:initVipData()
  self:CheckInitGameData()
  local vipInfo = GameGlobalData:GetData("vipinfo")
  self.maxId = 0
  for k, v in pairs(self.vipExp) do
    if v.level > self.maxId then
      self.maxId = v.level
    end
  end
  self.curSelect = vipInfo.level
  GameVip:ReqVipRewardList()
end
function GameVip.GotoVip()
  GameVip:showVip(true)
end
function GameVip.GotoPayment()
  GameVip:showVip(false)
end
function GameVip:CheckIsNeedShowVip(_code, checkDuplicate)
  local code = tonumber(_code)
  DebugOut(code)
  if code == 8904 or code == 140 or code == 135 or code == 301 or code == 1200 or code == 1006 or code == 100124 or code == 10113 or code == 1000014 or code == 1000005 or code == 1000004 or code == 900003 or code == 1000021 or code == 1000020 then
    local text = AlertDataList:GetTextFromErrorCode(code)
    GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.Green)
    local cancel = GameLoader:GetGameText("LC_MENU_BUTTON_CAPTION_CANCEL")
    local affairs = GameLoader:GetGameText("LC_MENU_GET_MORE_BUTTON")
    GameUIMessageDialog:SetRightTextButton(cancel)
    GameUIMessageDialog:SetLeftGreenButton(affairs, GameVip.GotoPayment)
    GameUIMessageDialog:Display("", text)
  elseif code == 1201 or code == 100 or code == 1400 then
    local text = AlertDataList:GetTextFromErrorCode(code)
    GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.Green)
    local cancel = GameLoader:GetGameText("LC_MENU_BUTTON_CAPTION_CANCEL")
    local affairs = GameLoader:GetGameText("LC_MENU_GET_MORE_BUTTON")
    GameUIMessageDialog:SetRightTextButton(cancel)
    GameUIMessageDialog:SetLeftGreenButton(affairs, GameVip.GotoVip)
    GameUIMessageDialog:Display("", text)
  elseif code == 716 then
    local msg = GameLoader:GetGameText("LC_ALERT_VIP_NOT_ENOUGH")
    msg = string.format(msg, 4)
    GameTip:Show(msg)
  else
    GameUIGlobalScreen:ShowAlert("error", code, nil, checkDuplicate)
  end
end
function GameVip:InitVipContent()
  local contents = ""
  local contentsTable = {}
  contentsTable.desc = {}
  contentsTable.rewards = {}
  for index = 0, self.maxId do
    local tempStr = ""
    for i = 1, 10 do
      local vipText = GameDataAccessHelper:GetVIPText(index, i)
      DebugOut("vip text = ", vipText)
      if vipText then
        tempStr = tempStr .. vipText .. "\n"
      end
    end
    contentsTable.desc[index + 1] = tempStr
    contentsTable.rewards[index + 1] = GameVip.vipReward[index + 1]
  end
  DebugOut("contentsTable = ")
  DebugTable(contentsTable)
  self.gameVipDate = contentsTable
  self.m_current_vipinfo = 1
  self:RefreshVipData(1, 1)
  self:setVipTitle()
end
function GameVip:RefreshVipData(whichlist, whichVipInfo)
  local temp_text = ""
  local temp_str = self.gameVipDate.desc[tonumber(whichVipInfo)]
  local rewardData = self.gameVipDate.rewards[tonumber(whichVipInfo)]
  DebugOut("temp_str = ", temp_str)
  DebugTable(self.gameVipDate)
  self:GetFlashObject():InvokeASCallback("_root", "initVipListData", whichlist, temp_str, rewardData)
end
function GameVip:hideVip()
  self:GetFlashObject():InvokeASCallback("_root", "hideVip")
end
function GameVip:Update(dt)
  self:GetFlashObject():InvokeASCallback("_root", "OnUpdate")
  self:GetFlashObject():InvokeASCallback("_root", "UpdateRead")
  self:GetFlashObject():Update(dt)
  self:UpdateOnSaleTime()
  if GameUtils:IsMycardAPP() and GameVip:IsMycardEnabled() then
    GameVip.checkMycardOrder()
  end
end
function GameVip:IsMycardEnabled()
  if GameUtils:IsMycardAPP() then
    local isReallyMycardEnable = 0
    if StoreObject.IsMycardReallyEnabled ~= nil then
      isReallyMycardEnable = self.storeObject:IsMycardReallyEnabled()
    end
    if isReallyMycardEnable > 0 then
      return true
    end
  end
  return false
end
function GameVip.checkMycardOrder()
  if GameVip.MycardOrderReq and GameVip.MycardOrderReq[GameVip.ordersIndex] and GameVip.MycardOrderReq[GameVip.ordersIndex].MycardOrderReqSatates and GameVip.MycardOrderReq[1].waiting_seconds < os.time() then
    GameUtils:printByAndroid("---asend ---- GameVip.ordersIndex----" .. tostring(GameVip.ordersIndex))
    GameVip.MycardOrderReq[GameVip.ordersIndex].MycardOrderReqSatates = false
    GameVip:MycardOrederConfim(GameVip.MycardOrderReq[GameVip.ordersIndex].id)
  end
end
function GameVip:ShowGift(item)
  if item.item_type == "krypton" then
    ItemBox:SetKryptonBox(item, tonumber(item.id))
    local operationTable = {}
    operationTable.btnUnloadVisible = false
    operationTable.btnUpgradeVisible = false
    operationTable.btnUseVisible = false
    operationTable.btnDecomposeVisible = false
    ItemBox:ShowKryptonBox(320, 240, operationTable)
  end
  if item.item_type == "fleet" then
    GameUIFirstCharge:ShowCommanderInfo(tonumber(item.number))
  end
  if item.item_type == "item" then
    item.cnt = 1
    local strtype = "Item"
    strtype = "ChoosableItem"
    ItemBox:showItemBox(strtype, item, tonumber(item.number), 320, 480, 200, 200, "", nil, "", nil)
  end
end
function GameVip:OnFSCommand(cmd, arg)
  DebugStore(cmd)
  DebugStore(GameVip.buyTimes)
  GameTextEdit:fscommand(self:GetFlashObject(), cmd, arg)
  if cmd == "closeVip" then
    self.curProductID = nil
    GameStateManager:GetCurrentGameState():EraseObject(self)
    if self.CloseCallbackFunc then
      self.CloseCallbackFunc()
      self.CloseCallbackFunc = nil
    elseif self.backToFirstCharge then
      self.backToFirstCharge()
      self.backToFirstCharge = nil
    elseif Facebook:IsFacebookEnabled() and GameStateManager:GetCurrentGameState() == GameStateManager.GameStateMainPlanet then
      if not Facebook:IsBindFacebook() then
        if FacebookPopUI.mFeatureSwitch[FACEBOOKFEATURESWITCH.SWITCH_PAY_BIND] and FacebookPopUI.mFeatureSwitch[FACEBOOKFEATURESWITCH.SWITCH_PAY_BIND] == 1 then
          DebugOut("FacebookPopUI111")
          FacebookPopUI:ShowFacebookMenu(FACEBOOKMENUTYPE.TYPE_MENU_LOGIN)
          FacebookPopUI:SetBindAwardType(FACEBOOKBINDAWARDTYPE.AWARD_VIP_CREDIT)
          if not ext.GetDeviceCountry or not ext.GetDeviceCountry() then
            local countryStr = "unknow"
          end
        end
      else
        if not Facebook:GetCanFetchInvitableFriendList() then
          return
        end
        if GameVip.hasPaySuccess then
          return
        end
        if not Facebook:IsLoginedIn() then
          if FacebookPopUI.mFeatureSwitch[FACEBOOKFEATURESWITCH.SWITCH_PAY_INVITE] and FacebookPopUI.mFeatureSwitch[FACEBOOKFEATURESWITCH.SWITCH_PAY_INVITE] == 1 then
            DebugOut("FacebookPopUI222")
            FacebookPopUI:SetBindAwardType(FACEBOOKBINDAWARDTYPE.AWARD_NO_AWARD_VIP)
            FacebookPopUI.mLogInAwardCallback = nil
            FacebookPopUI:LoginDirectly()
            if not ext.GetDeviceCountry or not ext.GetDeviceCountry() then
              local countryStr = "unknow"
            end
          end
        elseif FacebookPopUI.mFeatureSwitch[FACEBOOKFEATURESWITCH.SWITCH_PAY_INVITE] and FacebookPopUI.mFeatureSwitch[FACEBOOKFEATURESWITCH.SWITCH_PAY_INVITE] == 1 then
          DebugOut("FacebookPopUI333")
          FacebookPopUI.mInviteTitleText = GameLoader:GetGameText("LC_MENU_NEW_FACEBOOK_LOG_IN_BUTTON_4")
          FacebookPopUI:GetFacebookInvitableFriendList()
          if not ext.GetDeviceCountry or not ext.GetDeviceCountry() then
            local countryStr = "unknow"
          end
        end
      end
    end
  end
  if cmd == "register_released" then
    GameUtils:RestartGame(200, self:GetFlashObject())
  end
  if cmd == "more_purchase_released" then
    GameWaiting:ShowLoadingScreen()
    self.storeObject:BuyWithCount(self.productIdentifierList[#self.productIdentifierList], GameVip.buyTimes)
    self.curProductID = self.productIdentifierList[#self.productIdentifierList]
  end
  if cmd == "show_gift_item" then
    local gift_item = GameVip.onSaleProductList[GameVip.curSelectSaleIndex].gift_items[tonumber(arg)]
    GameVip:ShowGift(gift_item)
  end
  if cmd == "buy_times_reduce" then
    DebugStore("buy_times_reduce", GameVip.buyTimes)
    if GameVip.buyTimes > 1 then
      GameVip.buyTimes = GameVip.buyTimes - 1
      self:GetFlashObject():InvokeASCallback("_root", "AddDisableState", true)
      if GameVip.buyTimes == 1 then
        self:GetFlashObject():InvokeASCallback("_root", "ReduceDisableState", false)
      end
      self:GetFlashObject():InvokeASCallback("_root", "SetBuyTimes", "" .. GameVip.buyTimes)
    end
  elseif cmd == "buy_times_add" then
    DebugStore("buy_times_add", GameVip.buyTimes)
    if GameVip.buyTimes < 10 then
      GameVip.buyTimes = GameVip.buyTimes + 1
      self:GetFlashObject():InvokeASCallback("_root", "ReduceDisableState", true)
      if GameVip.buyTimes == 10 then
        self:GetFlashObject():InvokeASCallback("_root", "AddDisableState", false)
      end
      self:GetFlashObject():InvokeASCallback("_root", "SetBuyTimes", "" .. GameVip.buyTimes)
    end
  end
  if cmd == "vipEnterPurchase" then
    self:GetFlashObject():InvokeASCallback("_root", "showVip", false, false, 2, GameLoader:GetGameText("LC_MENU_PAID_WALL_FUNCTION_FIRST_RECHARGE"), AutoUpdate.isAndroidDevice and ext.GetBundleIdentifier() ~= AutoUpdate.bundleIdentifierType.BUNDLE_IDENTIFIER_NORMAL_ANDROID)
    if not self.isPriceValid then
      self.storeObject = self.storeObject or StoreObject:new()
      self.storeObject:RequestProductData(self.productIdentifierList)
      GameWaiting:ShowLoadingScreen()
    end
    GameVip:RequestProductIdentifierList()
  end
  if cmd == "Entervip" then
    self:GetFlashObject():InvokeASCallback("_root", "showVip", true, GameVip.first_chage, 2, GameLoader:GetGameText("LC_MENU_PAID_WALL_FUNCTION_FIRST_RECHARGE"), AutoUpdate.isAndroidDevice and ext.GetBundleIdentifier() ~= AutoUpdate.bundleIdentifierType.BUNDLE_IDENTIFIER_NORMAL_ANDROID)
    self:initVipData()
  end
  if cmd == "ShowQuickBilling" then
    DebugOut("ShowQuickBilling")
    DebugTable(GameVip.CarrierProductList)
    self:GetFlashObject():InvokeASCallback("_root", "InitAndShowCarrierQuickBil", GameVip.CarrierProductList)
  end
  if cmd == "quick_buy" then
    DebugOut("quick_buy" .. arg)
    GameVip.BuyArg = arg
    self:DoRealQuickPurchase()
  end
  if cmd == "PurchaseItemPress" then
    local id, count, itemname = unpack(LuaUtils:string_split(arg, "|"))
    GameVip.itemName_wechat = itemname
    if GameUtils:IsNeedChinaIDAuth() then
      if GameGlobalData:isIDAuth() then
        if GameGlobalData:GetModuleStatus("identity_confirm") == false then
          GameVip.PurchaseItemPress(arg)
        else
          local price = GameVip.GetPriceByProductID(id)
          if 1 < tonumber(count) then
            price = price .. "x" .. count or price
          end
          local name = "\228\191\161\231\148\168\231\130\185"
          ItemBox:showBuyConfirmBox(self.PurchaseItemPress, arg, price, name)
        end
      else
        ItemBox:showIdComfirmBox()
      end
    else
      GameVip.PurchaseItemPress(arg)
    end
  end
  if cmd == "MultiBuy_MethodChoose" then
    GameVip:DoPurchaseWithChooesedMethod(arg)
  end
  if cmd == "MoreBuy_MethodChoose" then
    GameVip:DoPurchaseWithChooesedMethod(arg)
  end
  if cmd == "showLeft" and 1 < self.m_current_vipinfo then
    self.m_current_vipinfo = self.m_current_vipinfo - 1
    GameVip:RefreshVipData(1, self.m_current_vipinfo + 1)
    GameVip:RefreshVipData(2, self.m_current_vipinfo)
    self:GetFlashObject():InvokeASCallback("_root", "showVipAim", true)
  end
  if cmd == "showRight" and self.m_current_vipinfo < self.maxId then
    self.m_current_vipinfo = self.m_current_vipinfo + 1
    GameVip:RefreshVipData(2, self.m_current_vipinfo - 1)
    GameVip:RefreshVipData(1, self.m_current_vipinfo)
    self:GetFlashObject():InvokeASCallback("_root", "showVipAim", false)
  end
  if cmd == "vipAnimationOver" then
    GameVip:RefreshVipData(1, self.m_current_vipinfo)
    self:setVipTitle()
  end
  if cmd == "btnMoreClciked" then
    GameVip.curSelectSaleIndex = tonumber(arg)
    GameVip:ShowMoreItemsScreen()
  end
  if cmd == "needUpdatemoreItemsItem" then
    GameVip:UpdateMoreItemsItem(tonumber(arg))
  end
  if cmd == "GooglePlayReleased" then
    self:GetFlashObject():InvokeASCallback("_root", "closeMyCardWin")
    GameVip:DoRealPurchase()
  elseif cmd == "MyCardMoneyReleased" then
    if GameVip.BuyArg ~= nil then
      self:GetFlashObject():InvokeASCallback("_root", "closeMyCardWin")
      StoreObject:getMycardUrl(GameVip.mycardType_price, self.productIdentifierList[tonumber(GameVip.BuyArg)])
    end
  elseif cmd == "MyCardNumberReleased" then
    if GameVip.BuyArg ~= nil then
      self:GetFlashObject():InvokeASCallback("_root", "closeMyCardWin")
      StoreObject:getMycardUrl(GameVip.mycardType_memberPoint, self.productIdentifierList[tonumber(GameVip.BuyArg)])
    end
  elseif cmd == "MyCardPointReleased" then
    if GameVip.BuyArg ~= nil then
      if GameVip.MycardType == 1 then
        self.MycardPointCard = ""
        self.MycardPointPassword = ""
        self:GetFlashObject():InvokeASCallback("_root", "showCardNumAndPwdWin")
      elseif GameVip.MycardType == 2 then
        self:GetFlashObject():InvokeASCallback("_root", "closeMyCardWin")
        StoreObject:getMycardUrl(GameVip.mycardType_card, self.productIdentifierList[tonumber(GameVip.BuyArg)])
      end
    end
  elseif cmd == "MyCardConfirmReleased" then
    if self.MycardPointCard == "" then
      GameTip:Show(GameLoader:GetGameText("LC_MENU_MYCARD_INPURT_CARD_NUMBER"))
    else
      StoreObject:VerifyPointAndPWD(self.productIdentifierList[tonumber(GameVip.BuyArg)], self.MycardPointCard, self.MycardPointPassword)
    end
  elseif cmd == "WriteCardNumber" then
    self.MycardPointCard = arg
  elseif cmd == "WriteCardPassword" then
    self.MycardPointPassword = arg
  elseif cmd == "showItemDetail" then
    DebugOut("---arg:" .. arg)
    local param = LuaUtils:string_split(arg, "\001")
    local gift_item = GameVip.onSaleProductList[tonumber(param[2])].gift_items[tonumber(param[1])]
    GameVip:ShowGift(gift_item)
  elseif cmd == "update_gift_item" then
    self:UpdateGiftItem(tonumber(arg))
  elseif cmd == "first_charge" then
    GameStateManager:GetCurrentGameState():AddObject(GameUIFirstCharge)
  elseif cmd == "touch_gift_item" then
    self:OpenGiftDetail(arg)
  elseif cmd == "Buy_gift" then
    local id, count, itemName = unpack(LuaUtils:string_split(arg, "|"))
    GameVip.itemName_wechat = itemName
    if GameUtils:IsNeedChinaIDAuth() then
      if GameGlobalData:isIDAuth() then
        if GameGlobalData:GetModuleStatus("identity_confirm") == false then
          GameVip.BuyGift(arg)
        else
          local item = GameVip:GetGiftItemByID(id)
          local price = 1 < tonumber(count) and item.discount_price .. "x" .. count or item.discount_price
          local itemName = 1 < tonumber(count) and item.name .. "x" .. count or item.name
          ItemBox:showBuyConfirmBox(self.BuyGift, arg, price, itemName)
        end
      else
        ItemBox:showIdComfirmBox()
      end
    else
      GameVip.BuyGift(arg)
    end
  elseif cmd == "update_include_item" then
    local itemIndex, index = unpack(LuaUtils:string_split(arg, ":"))
    self:UpdateGiftIncludeItem(tonumber(itemIndex), index)
  elseif cmd == "ShowDetail" then
    local id, index = unpack(LuaUtils:string_split(arg, ":"))
    local item = self:GetGiftItemByID(id)
    if item and item.rewardList[tonumber(index)] then
      if item.rewardList[tonumber(index)].model.item_type == "krypton" then
        local function callback(msgType, content)
          if msgType == NetAPIList.krypton_info_ack.Code then
            GameWaiting:HideLoadingScreen()
            content.krypton.item_type = "krypton"
            GameVip:ShowGift(content.krypton)
            return true
          end
          return false
        end
        GameUIKrypton:TryQueryKryptonDetail(item.rewardList[tonumber(index)].model.number, 1, callback)
        return
      end
      GameVip:ShowGift(item.rewardList[tonumber(index)].model)
    end
  elseif cmd == "update_vip_reward_item" then
    local vip, index, whichlist = unpack(LuaUtils:string_split(arg, ":"))
    self:UpdateVipRewardItem(tonumber(vip), tonumber(index), tonumber(whichlist))
  elseif cmd == "touch_vip_reward_item" then
    local index, vip = unpack(LuaUtils:string_split(arg, ":"))
    local reward = GameVip.vipReward[tonumber(vip)].rewardList[tonumber(index)]
    if reward then
      if reward.model.item_type == "krypton" then
        local function callback(msgType, content)
          if msgType == NetAPIList.krypton_info_ack.Code then
            GameWaiting:HideLoadingScreen()
            content.krypton.item_type = "krypton"
            GameVip:ShowGift(content.krypton)
            return true
          end
          return false
        end
        GameUIKrypton:TryQueryKryptonDetail(reward.model.number, 1, callback)
        return
      end
      GameVip:ShowGift(reward.model)
    end
  elseif cmd == "update_month_card_item" then
    self:UpdateMonthCardItem(tonumber(arg))
  elseif cmd == "month_help" then
    GameUIWDStuff:Show(GameUIWDStuff.menuType.menu_type_help)
    GameUIWDStuff:SetHelpText(GameLoader:GetGameText("LC_MENU_MONTH_CARD_DETAIL"))
  elseif cmd == "touch_month_card_item" then
    local buyid, itemname = unpack(LuaUtils:string_split(arg, "|"))
    local item = GameVip:GetMonthCardItemById(buyid)
    GameVip.itemName_wechat = itemname
    print("xxpp itemname", itemname)
    DebugOut("touch_month_card_item")
    DebugTable(item)
    if item then
      if item.status == 0 then
        if GameUtils:IsNeedChinaIDAuth() then
          if not GameGlobalData:isIDAuth() then
            ItemBox:showIdComfirmBox()
          elseif GameGlobalData:GetModuleStatus("identity_confirm") == false then
            GameVip.BuyMonthCard(buyid)
          else
            local price = GameVip.GetPriceByProductID(item.product_id)
            ItemBox:showBuyConfirmBox(self.BuyMonthCard, buyid, price, item.titleText)
          end
        else
          GameVip.BuyMonthCard(buyid)
        end
      else
        GameVip:ReceiveMonthCardReward(buyid)
      end
    end
  end
end
function GameVip.BuyMonthCard(arg)
  if IPlatformExt.getConfigValue("PlatformName") == "taptap" then
    GameVip:Select_zfb_wechat(GameVip.doBuyMonthCard, arg)
  else
    GameVip.doBuyMonthCard(arg)
  end
end
function GameVip.doBuyMonthCard(arg)
  local index = 1
  local item = GameVip:GetMonthCardItemById(arg)
  for k, v in pairs(GameVip.productIdentifierList) do
    if item.product_id == v then
      index = k
      break
    end
  end
  local function netCall()
    GameVip.BuyID = arg
    GameVip.BuyProductID = item.product_id
    GameVip.BuyArg = index
    GameVip.BuyCount = 1
    GameVip.LastBuyType = "month"
    if GameUtils:IsMycardAPP() then
      local isReallyMycardEnable = 0
      if StoreObject.IsMycardReallyEnabled ~= nil then
        isReallyMycardEnable = GameVip.storeObject:IsMycardReallyEnabled()
      end
      if isReallyMycardEnable > 0 then
        GameVip:GetFlashObject():InvokeASCallback("_root", "showMyCardWin")
      else
        GameVip:DoRealPurchase()
      end
    elseif GameUtils:IsQihooApp() then
      GameUtils:printByAndroid("IsQihooApp......pay")
      GameVip.mycardType_qihooPrice = "qh360_price"
      if GameVip.BuyArg ~= nil then
        StoreObject:getMycardUrl(GameVip.mycardType_qihooPrice, GameVip.productIdentifierList[tonumber(GameVip.BuyArg)])
      end
    elseif GameVip:IsEnableExtMultiBuyFeature() then
      GameVip:ShowMultiPop()
    else
      GameVip:DoRealPurchase()
    end
  end
  GameVip:ReqWishProductBeferBuy(arg, item.product_id, 1, netCall)
end
function GameVip.PurchaseItemPress(arg)
  if IPlatformExt.getConfigValue("PlatformName") == "taptap" then
    GameVip:Select_zfb_wechat(GameVip.doPurchaseItemPress, arg)
  else
    GameVip.doPurchaseItemPress(arg)
  end
end
function GameVip.doPurchaseItemPress(arg)
  local id, count = unpack(LuaUtils:string_split(arg, "|"))
  local Ishave = false
  for k, v in pairs(GameVip.productIdentifierList) do
    if id == v then
      Ishave = true
      id = k
      break
    end
  end
  DebugOut("GameVip.PurchaseItemPress", id, Ishave)
  DebugTable(GameVip.productList)
  DebugTable(GameVip.productIdentifierList)
  if not Ishave then
    if GameVip:IsEnableExtMoreFeatire() then
      GameVip:ShowMorePop()
    else
      local priceStr = GameVip.productList[#GameVip.productList]
      GameVip.buyTimes = 10
      GameVip:GetFlashObject():InvokeASCallback("_root", "SetBuyTimes", "" .. GameVip.buyTimes)
      GameVip:GetFlashObject():InvokeASCallback("_root", "AddDisableState", false)
      GameVip:GetFlashObject():InvokeASCallback("_root", "ReduceDisableState", true)
      GameVip:GetFlashObject():InvokeASCallback("_root", "SetCreditsDetail", priceStr, #GameVip.productIdentifierList)
      GameVip:GetFlashObject():InvokeASCallback("_root", "MorePanelMoveIn")
    end
  else
    local function netCall()
      GameVip.BuyArg = id
      GameVip.BuyID = GameVip.productIdentifierList[tonumber(id)]
      GameVip.BuyProductID = GameVip.productIdentifierList[tonumber(id)]
      GameVip.BuyCount = tonumber(count)
      if tonumber(count) > 1 then
        GameWaiting:ShowLoadingScreen()
        GameVip.storeObject:BuyWithCount(GameVip.BuyProductID, tonumber(count))
        GameVip.curProductID = GameVip.BuyProductID
      elseif GameUtils:IsMycardAPP() then
        local isReallyMycardEnable = 0
        if StoreObject.IsMycardReallyEnabled ~= nil then
          isReallyMycardEnable = GameVip.storeObject:IsMycardReallyEnabled()
        end
        if isReallyMycardEnable > 0 then
          GameVip:GetFlashObject():InvokeASCallback("_root", "showMyCardWin")
        else
          GameVip:DoRealPurchase()
        end
      elseif GameUtils:IsQihooApp() then
        GameUtils:printByAndroid("IsQihooApp......pay")
        GameVip.mycardType_qihooPrice = "qh360_price"
        if GameVip.BuyArg ~= nil then
          StoreObject:getMycardUrl(GameVip.mycardType_qihooPrice, GameVip.productIdentifierList[tonumber(GameVip.BuyArg)])
        end
      elseif GameVip:IsEnableExtMultiBuyFeature() then
        GameVip:ShowMultiPop()
      else
        GameVip:DoRealPurchase()
      end
    end
    GameVip:ReqWishProductBeferBuy(GameVip.productIdentifierList[tonumber(id)], GameVip.productIdentifierList[tonumber(id)], tonumber(count), netCall)
  end
end
function GameVip.BuyGift(arg)
  if IPlatformExt.getConfigValue("PlatformName") == "taptap" then
    GameVip:Select_zfb_wechat(GameVip.doBuyGift, arg)
  else
    GameVip.doBuyGift(arg)
  end
end
function GameVip.doBuyGift(arg)
  local id, count = unpack(LuaUtils:string_split(arg, "|"))
  local item = GameVip:GetGiftItemByID(id)
  local index = 1
  for k, v in pairs(GameVip.productIdentifierList) do
    if item.product_id == v then
      index = k
      break
    end
  end
  local function netCall()
    GameVip.BuyID = id
    GameVip.BuyProductID = item.product_id
    GameVip.BuyArg = index
    GameVip.BuyCount = tonumber(count)
    GameVip.LastBuyType = "gift"
    if tonumber(count) > 1 then
      GameWaiting:ShowLoadingScreen()
      GameVip.storeObject:BuyWithCount(GameVip.BuyProductID, tonumber(count))
      GameVip.curProductID = GameVip.BuyProductID
    elseif GameUtils:IsMycardAPP() then
      local isReallyMycardEnable = 0
      if StoreObject.IsMycardReallyEnabled ~= nil then
        isReallyMycardEnable = GameVip.storeObject:IsMycardReallyEnabled()
      end
      if isReallyMycardEnable > 0 then
        GameVip:GetFlashObject():InvokeASCallback("_root", "showMyCardWin")
      else
        GameVip:DoRealPurchase()
      end
    elseif GameUtils:IsQihooApp() then
      GameUtils:printByAndroid("IsQihooApp......pay")
      GameVip.mycardType_qihooPrice = "qh360_price"
      if GameVip.BuyArg ~= nil then
        StoreObject:getMycardUrl(GameVip.mycardType_qihooPrice, GameVip.productIdentifierList[tonumber(GameVip.BuyArg)])
      end
    elseif GameVip:IsEnableExtMultiBuyFeature() then
      GameVip:ShowMultiPop()
    else
      GameVip:DoRealPurchase()
    end
  end
  GameVip:ReqWishProductBeferBuy(id, item.product_id, tonumber(count), netCall)
end
function GameVip.DoRealPurchase_ForPlatformExt(RuningID, OrderID, Notify_url, chargeType)
  GameUtils:printByAndroid("DoRealPurchase_ForPlatformExt")
  GameWaiting:ShowLoadingScreen()
  local ChargeInfo = {}
  ChargeInfo.QuickCharge = GameVip.quick_purchase
  local PurchaseProduct
  if GameVip.quick_purchase == true then
    PurchaseProduct = GameVip.CarrierProductList.productions[tonumber(GameVip.BuyArg)] or {}
  else
    PurchaseProduct = GameVip.ProductionsList[tonumber(GameVip.BuyArg)] or {}
  end
  ChargeInfo.RuningID = RuningID or 0
  ChargeInfo.OrderID = OrderID or ""
  ChargeInfo.sku = PurchaseProduct.id or "error_sku"
  ChargeInfo.Price = PurchaseProduct.price and PurchaseProduct.price / 100 or 0
  ChargeInfo.quantity = PurchaseProduct.credit or 0
  ChargeInfo.PlayerID = GameGlobalData:GetUserInfo().player_id
  ChargeInfo.PlayerName = GameGlobalData:GetUserInfo().name
  ChargeInfo.PlayerLevel = GameGlobalData:GetData("levelinfo").level
  ChargeInfo.ServerID = GameUtils:GetActiveServerInfo().id
  ChargeInfo.ServerLogicID = GameUtils:GetActiveServerInfo().logic_id
  ChargeInfo.ServerName = GameUtils:GetActiveServerInfo().name
  ChargeInfo.NotifyUrl = Notify_url or ""
  ChargeInfo.ChargeType = chargeType or ""
  local accInfo = GameUtils:GetLoginInfo().AccountInfo or {}
  ChargeInfo.Passport = ChargeInfo.passport or "Guest"
  GameUtils:printByAndroid("Charge gogogo!!!")
  GameUtils:DebugOutTableInAndroid(ChargeInfo)
  local function _calback(msgType, content)
    if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.uc_pay_sign_req.Code then
      if content.code ~= 0 then
        GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      end
      return true
    elseif msgType == NetAPIList.uc_pay_sign_ack.Code then
      ChargeInfo.Sign = content.sign
      ChargeInfo.AccountID = content.account_id
      IPlatformExt.RegCallBackFunc(2, GameVip.VerifyPurchase_forPlatformExt)
      IPlatformExt.Charge(ext.json.generate(ChargeInfo))
      GameVip.BuyArg = nil
      return true
    end
    return false
  end
  local param = {}
  param.pay_info = {}
  local item1 = {}
  item1.str_key = "callbackInfo"
  item1.str_value = ""
  param.pay_info[#param.pay_info + 1] = item1
  local item2 = {}
  item2.str_key = "amount"
  item2.str_value = string.format("%.02f", tonumber(ChargeInfo.Price))
  param.pay_info[#param.pay_info + 1] = item2
  local item3 = {}
  item3.str_key = "notifyUrl"
  item3.str_value = ChargeInfo.NotifyUrl
  param.pay_info[#param.pay_info + 1] = item3
  local item4 = {}
  item4.str_key = "cpOrderId"
  item4.str_value = ChargeInfo.OrderID
  param.pay_info[#param.pay_info + 1] = item4
  NetMessageMgr:SendMsg(NetAPIList.uc_pay_sign_req.Code, param, _calback, true, nil)
end
GameVip.BuyArg = nil
GameVip.MycardType = 1
GameVip.MycardPointCard = ""
GameVip.MycardPointPassword = ""
function GameVip:OriginalPurchase()
  if GameVip.BuyArg == nil then
    return
  end
  GameVip.quick_purchase = false
  local arg = GameVip.BuyArg
  if GameUtils:isIOSBundle() and GameVip.gamePurchaseEanble == 0 then
    GameUIGlobalScreen:ShowMessageBox(GameUIGlobalScreen.MessageBoxType_OK, GameLoader:GetGameText("LC_MENU_NOTIFICATION_CHAR"), GameLoader:GetGameText("LC_MENU_UNABLE_TO_BUY_INFO"), nil)
    GameVip.BuyArg = nil
    return
  end
  if GameVip.GetPriceByProductID(self.productIdentifierList[tonumber(arg)]) then
    GameWaiting:ShowLoadingScreen()
    DebugOut("self.p[tonumber(arg)]: ", self.productIdentifierList[tonumber(arg)])
    GameUtils:printByAndroid("GameVip.canShowMycard= " .. tostring(GameVip.canShowMycard) .. ", Wifi = ")
    local canShowMycard, isShowmycardType_1, isShowmycardType_2, isShowmycardType_3 = GameVip:GetMycardPurchageVisible(self.productIdentifierList[tonumber(arg)])
    local mixedData = tostring(isShowmycardType_1) .. tostring(isShowmycardType_2) .. tostring(isShowmycardType_3)
    if GameVip.strPayType == GameVip.strPayType_wechat then
      local sdkshowtxt = GameVip.itemName_wechat
      if tonumber(sdkshowtxt) then
        sdkshowtxt = sdkshowtxt .. GameLoader:GetGameText("LC_MENU_PLAYER_INFO_credit")
      end
      local content = {
        id = self.productIdentifierList[tonumber(arg)],
        payment_type = "wx_price",
        params = {sdkshowtxt}
      }
      NetMessageMgr:SendMsg(NetAPIList.pay_verify2_req.Code, content, GameVip.wechat_onPay_verify2_callback, true, nil)
      IPlatformExt.RegCallBackFunc(10, GameVip.VerifyPurchase_wechat)
    elseif IPlatformExt.getConfigValue("PlatformName") == "Huawei" then
      local param = "<pid>|<ntfurl>|<buyparam>|<sendinfo>"
      param = string.gsub(param, "<pid>", self.productIdentifierList[tonumber(arg)])
      param = string.gsub(param, "<ntfurl>", GameVip.huawei_url)
      param = string.gsub(param, "<buyparam>", "buyparam")
      param = string.gsub(param, "<sendinfo>", "sendinfo")
      self.storeObject:Buy(param, canShowMycard, mixedData)
    else
      self.storeObject:Buy(self.productIdentifierList[tonumber(arg)], canShowMycard, mixedData)
    end
    self.curProductID = self.productIdentifierList[tonumber(arg)]
  else
    DebugOut("Can not get price from app store.")
  end
  GameVip.BuyArg = nil
end
function GameVip.VerifyPurchase_wechat(javaparam)
  local retcode, _, _ = unpack(LuaUtils:string_split(javaparam, "|"))
  if tonumber(retcode) ~= 0 then
    if tonumber(retcode) == -1 then
      local tipMsg = GameUtils:TryGetText("LC_MENU_WECHAT_PAY_ERR")
      local GameTip = LuaObjectManager:GetLuaObject("GameTip")
      GameTip:Show(tipMsg)
      GameSettingData.wechat_refrshtoken = ""
      GameUtils:SaveSettingData()
    end
    return
  end
  local function _callback(msgType, content)
    if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.pay_confirm_req.Code then
      if GameVip:GetFlashObject() then
        GameVip:ReqGiftData()
        GameVip:ReqMonthCardData()
      end
      return true
    end
    return false
  end
  DebugOut("xxpp VerifyPurchase_wechat", GameVip.wechat_id, javaparam)
  GameTimer:Add(function()
    local param = {
      id = GameVip.wechat_id
    }
    NetMessageMgr:SendMsg(NetAPIList.pay_confirm_req.Code, param, _callback, true, nil)
  end, 1000)
end
function GameVip.wechat_onPay_verify2_callback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.pay_verify2_req.Code then
    if content.code ~= 5006 and content.code ~= 5007 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      return true
    end
    GameUIGlobalScreen:ShowAlert("pv2 error ", content.code, nil)
    if GameVip:GetFlashObject() then
      GameVip:ReqGiftData()
      GameVip:ReqMonthCardData()
    end
    return true
  elseif msgType == NetAPIList.pay_verify2_ack.Code then
    GameVip.wechat_id = content.id
    return true
  end
  return false
end
function GameVip.on_wxpay_info_ntf(content)
  GameVip.storeObjectWechat:Buy(ext.json.generate(content))
end
function GameVip.VerifyPurchase_huawei(javaparam)
  DebugOut("xxpp VerifyPurchase_huawei", javaparam)
  if javaparam == "" then
  else
  end
end
function GameVip.huawei_onPay_verify2_callback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.pay_verify2_req.Code then
    if content.code ~= 5006 and content.code ~= 5007 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      return true
    end
    GameUIGlobalScreen:ShowAlert("pv2 error ", content.code, nil)
    if GameVip:GetFlashObject() then
      GameVip:ReqGiftData()
      GameVip:ReqMonthCardData()
    end
    return true
  elseif msgType == NetAPIList.pay_verify2_ack.Code then
    GameVip.huawei_url = content.payment_url
    GameVip.huawei_id = content.id
    DebugOut("xxpp huawei_onPay_verify2_callback", GameVip.huawei_url, GameVip.huawei_id)
    GameVip:OriginalPurchase()
    return true
  end
  return false
end
function GameVip:DoRealPurchase()
  if GameVip.BuyArg == nil then
    return
  end
  GameVip.quick_purchase = false
  local arg = GameVip.BuyArg
  if IPlatformExt and IPlatformExt.getConfigValue("PlatformExtCharge") == "true" then
    if IPlatformExt.getConfigValue("UseOurOrderID") == "True" then
      GameVip:RequestOrderID_forPlatformExt(self.productIdentifierList[tonumber(arg)])
    else
      GameVip.DoRealPurchase_ForPlatformExt(nil, nil, nil)
    end
  elseif GameUtils:IsQihooApp() then
    GameUtils:printByAndroid("qihoo doRealPurchase")
    GameVip.mycardType_qihooPrice = "qh360_price"
    if GameVip.BuyArg ~= nil then
      StoreObject:getMycardUrl(GameVip.mycardType_qihooPrice, GameVip.productIdentifierList[tonumber(GameVip.BuyArg)])
    end
  elseif IPlatformExt.getConfigValue("PlatformName") == "Huawei" then
    local content = {
      id = self.productIdentifierList[tonumber(arg)],
      payment_type = "hw_price",
      params = {}
    }
    NetMessageMgr:SendMsg(NetAPIList.pay_verify2_req.Code, content, GameVip.huawei_onPay_verify2_callback, true, nil)
    IPlatformExt.RegCallBackFunc(10, GameVip.VerifyPurchase_huawei)
  else
    GameVip:OriginalPurchase()
  end
end
function GameVip:DoRealQuickPurchase()
  if GameVip.BuyArg == nil then
    return
  end
  GameVip.quick_purchase = true
  local arg = GameVip.BuyArg
  if IPlatformExt.getConfigValue("UseOurOrderID") == "True" then
    GameVip:RequestQuickBuyOrderID_forPlatformExt(GameVip.CarrierProductList.productions[tonumber(arg)].id)
  else
    GameVip.DoRealPurchase_ForPlatformExt(nil, nil, nil)
  end
end
function GameVip:DoPurchaseWithChooesedMethod(methodType)
  if methodType == "GooglePlay" then
    GameVip:OriginalPurchase()
  else
    GameVip.DoRealPurchase_ForPlatformExt(nil, nil, nil, methodType)
  end
end
function GameVip:SetFirstChageData(param)
end
GameVip.payingRecord = {}
GameVip.payingRecord.price = 0
GameVip.payingRecord.param = {}
function GameVip.payRecordNtf(content)
  GameUtils:printByAndroid(" ========>   GameVip.payRecordNtf")
  GameUtils:DebugOutTableInAndroid(content)
  GameVip.payingRecord.price = content.price
  GameVip.payingRecord.param = content.param
  if GameUtils:IsQihooApp() then
    GameVip.qihooRecords(content)
  end
end
function GameVip.checkReceiptCallback_forPlatformExt(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.pay_verify2_req.Code then
    if content.code == 5008 then
      if IPlatformExt and IPlatformExt.getConfigValue("PlatformExtCharge.Finish") == "true" then
        local payFinishInfo = {}
        payFinishInfo.is_succeeded = true
        payFinishInfo.is_Statistics = false
        payFinishInfo.receiptID = content.payment_url or "null"
        payFinishInfo.currency = ""
        payFinishInfo.price = 0
        payFinishInfo.param = {}
        IPlatformExt.ChargeFinish(ext.json.generate(payFinishInfo))
      end
    else
      GameUIGlobalScreen:ShowAlert("pv2 error ", content.code, nil)
    end
    if GameVip:GetFlashObject() then
      GameVip:ReqGiftData()
      GameVip:ReqMonthCardData()
    end
    return true
  elseif msgType == NetAPIList.pay_verify2_ack.Code then
    GameUtils:printByAndroid("fucking checkReceiptCallback_forPlatformExt")
    if content.code == 0 then
      local staInfo = {}
      staInfo.Type = "ChargeSucess"
      staInfo.Price = GameVip.payingRecord.price
      staInfo.Coin = GameVip.PriceMapCoin[tostring(staInfo.Price)]
      GameUtils:SafeRecordGameInfo(staInfo)
      if IPlatformExt and IPlatformExt.getConfigValue("PlatformExtCharge.Finish") == "true" then
        local payFinishInfo = {}
        payFinishInfo.is_succeeded = true
        payFinishInfo.is_Statistics = true
        payFinishInfo.receiptID = content.payment_url or "null"
        payFinishInfo.currency = "USD"
        payFinishInfo.price = GameVip.payingRecord.price
        payFinishInfo.param = GameVip.payingRecord.param
        GameUtils:DebugOutTableInAndroid(payFinishInfo)
        IPlatformExt.ChargeFinish(ext.json.generate(payFinishInfo))
      end
      GameVip.UpdateAfterPurchaseSucess()
    else
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    if GameVip:GetFlashObject() then
      GameVip:ReqGiftData()
      GameVip:ReqMonthCardData()
    end
    return true
  end
  return false
end
function GameVip.ConfirmPayCallBack_forPlatformExt(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.pay_confirm_req.Code then
    GameUtils:printByAndroid("fucking checkReceiptCallback_forPlatformExt")
    if content.code == 0 then
      local staInfo = {}
      staInfo.Type = "ChargeSucess"
      staInfo.Price = GameVip.payingRecord.price
      staInfo.Coin = GameVip.PriceMapCoin[tostring(staInfo.Price)]
      GameUtils:SafeRecordGameInfo(staInfo)
      if IPlatformExt.getConfigValue("PlatformExtCharge.Finish") == "true" then
        local payFinishInfo = {}
        payFinishInfo.is_succeeded = true
        payFinishInfo.is_Statistics = true
        payFinishInfo.receiptID = content.payment_url or "null"
        payFinishInfo.currency = "USD"
        payFinishInfo.price = GameVip.payingRecord.price
        payFinishInfo.param = GameVip.payingRecord.param
        GameUtils:DebugOutTableInAndroid(payFinishInfo)
        IPlatformExt.ChargeFinish(ext.json.generate(payFinishInfo))
      end
      GameVip.UpdateAfterPurchaseSucess()
    else
      GameTip:Show(GameLoader:GetGameText("LC_MENU_CARRIER_INFO_2"))
    end
    if GameVip:GetFlashObject() then
      GameVip:ReqGiftData()
      GameVip:ReqMonthCardData()
    end
    return true
  end
  return false
end
function GameVip:RequestOrderID_forPlatformExt(productId)
  GameUtils:printByAndroid("----------------RequestOrderID: " .. productId)
  local function netCall()
    local content = {
      id = productId,
      payment_type = IPlatformExt.getConfigValue("PayType"),
      params = {}
    }
    NetMessageMgr:SendMsg(NetAPIList.pay_verify2_req.Code, content, GameVip.RequestOrderIDCallback_forPlatformExt, true, nil)
  end
  if not GameVip:ReqWishProduct(GameVip.BuyID, GameVip.BuyProductID, GameVip.BuyCount, netCall) and not GameVip.IsReqWish then
    netCall()
  end
end
function GameVip:RequestQuickBuyOrderID_forPlatformExt(productId)
  local function netCall()
    local content = {
      id = productId,
      payment_type = IPlatformExt.getConfigValue("quick_buy_paytype"),
      params = {}
    }
    NetMessageMgr:SendMsg(NetAPIList.pay_verify2_req.Code, content, GameVip.RequestOrderIDCallback_forPlatformExt, true, nil)
  end
  if not GameVip:ReqWishProduct(GameVip.BuyID, GameVip.BuyProductID, GameVip.BuyCount, netCall) and not GameVip.IsReqWish then
    netCall()
  end
end
function GameVip.RequestOrderIDCallback_forPlatformExt(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.pay_verify2_req.Code then
    GameUIGlobalScreen:ShowAlert("pv2 error ", content.code, nil)
    if GameVip:GetFlashObject() then
      GameVip:ReqGiftData()
      GameVip:ReqMonthCardData()
    end
    return true
  elseif msgType == NetAPIList.pay_verify2_ack.Code then
    GameVip.DoRealPurchase_ForPlatformExt(content.id, GameVip.payingRecord.param[1] or "errorOrderID", content.payment_url)
    if GameVip:GetFlashObject() then
      GameVip:ReqGiftData()
      GameVip:ReqMonthCardData()
    end
    return true
  end
  return false
end
function GameVip.VerifyPurchase_forPlatformExt(info)
  GameUtils:printByAndroid("-------fucking VerifyPurchase_forPlatformExt")
  GameWaiting:HideLoadingScreen()
  local function netCall()
    if IPlatformExt.getConfigValue("UseOurOrderID") == "True" then
      GameUtils:printByAndroid("-------fucking VerifyPurchase_forPlatformExt UseOurOrderID")
      if string.len(info) == 0 then
        return
      end
      if GameGlobalData.isLogin == false then
        GameUtils:printByAndroid("-----fuck not login!! ----")
        return
      end
      local content = ext.json.parse(info)
      GameUtils:DebugOutTableInAndroid(content)
      NetMessageMgr:SendMsg(NetAPIList.pay_confirm_req.Code, content, GameVip.ConfirmPayCallBack_forPlatformExt, true, nil)
    else
      if string.len(info) == 0 then
        return
      end
      if GameGlobalData.isLogin == false then
        GameUtils:printByAndroid("-----fuck not login!! ----")
        return
      end
      local content = ext.json.parse(info)
      GameUtils:DebugOutTableInAndroid(content)
      NetMessageMgr:SendMsg(NetAPIList.pay_verify2_req.Code, content, GameVip.checkReceiptCallback_forPlatformExt, true, nil)
    end
  end
  if not GameVip:ReqWishProduct(GameVip.BuyID, GameVip.BuyProductID, GameVip.BuyCount, netCall) and not GameVip.IsReqWish then
    netCall()
  end
end
function GameVip:GetMycardPurchageVisible(productId)
  if not GameUtils:IsMycardAPP() then
    return "false", 0, 0, 0
  end
  if ext.IsReachableWifi() then
    return "false", 0, 0, 0
  end
  if not GameVip.canShowMycard then
    return "false", 0, 0, 0
  end
  local mycardType_1 = 0
  local mycardType_2 = 0
  local mycardType_3 = 0
  local canShowMycard = "false"
  for k, v in pairs(GameVip.myCardInfo) do
    if productId == v.id and v.payments then
      if v.payments[1] and v.payments[1].price and 0 < tonumber(v.payments[1].price) then
        canShowMycard = "true"
        mycardType_1 = 1
      end
      if v.payments[2] and v.payments[2].price and 0 < tonumber(v.payments[2].price) then
        canShowMycard = "true"
        mycardType_2 = 1
      end
      if v.payments[3] and v.payments[3].price and 0 < tonumber(v.payments[3].price) then
        canShowMycard = "true"
        mycardType_3 = 1
      end
    end
  end
  return canShowMycard, mycardType_1, mycardType_2, mycardType_3
end
if AutoUpdate.isAndroidDevice then
  function GameVip.OnAndroidBack()
    GameVip:GetFlashObject():InvokeASCallback("_root", "hideVip")
  end
end
function GameVip:ShowAgreementBox()
  if GameSettingData.Save_Lang ~= "cn" and GameSettingData.Save_Lang ~= "zh" then
    return
  end
  local LoginInfo = GameUtils:GetLoginInfo()
  if not LoginInfo.AccountInfo then
    GameVip:GetFlashObject():InvokeASCallback("_root", "showAgreementBox")
  end
end
function GameVip:RequestProductIdentifierList()
  if AutoUpdate.isAndroidDevice then
    local paySection = {}
    if ProductIdentifyList:IsSupportSetProductList() and ProductIdentifyList:IsSetGroupOpen() then
      local productListSection = ProductIdentifyList:GetSectionNumber()
      if productListSection then
        paySection.join_test = 1
        paySection.join_group = productListSection
      else
        paySection.join_test = 0
        paySection.join_group = 0
      end
    else
      paySection.join_test = 0
      paySection.join_group = 0
    end
    NetMessageMgr:SendMsg(NetAPIList.gp_test_pay_group_req.Code, paySection, nil, false, nil)
  end
  local function netCallProcess()
    local platformString = ext.GetPlatform()
    local content = {
      client = tonumber(IPlatformExt.getConfigValue("ProductListReqNum")) or GameUtils:getProductListReqNum()
    }
    GameUtils:DebugOutTableInAndroid(content)
    NetMessageMgr:SendMsg(NetAPIList.pay_list2_req.Code, content, GameVip.ReceiveProductIdentifierList, false, netCallProcess)
  end
  netCallProcess()
end
function GameVip.ReceiveProductIdentifierList(msgType, content)
  if msgType == NetAPIList.pay_list2_ack.Code then
    DebugOut("ReceiveProductIdentifierList")
    DebugTable(content)
    GameVip.mPayListData = content
    GameVip.MycardType = content.mycard_type
    if not GameVip.isPriceValid then
      GameVip.myCardInfo = content.productions
      GameVip.ProductionsList = content.productions
      GameVip.canShowMycard = true
      GameVip.productIdentifierList = {}
      for i, v in ipairs(content.productions) do
        table.insert(GameVip.productIdentifierList, v.id)
        if not v.payments or #v.payments < 1 then
          GameVip.canShowMycard = false
        end
        if GameUtils:IsQihooApp() then
          table.insert(GameVip.qihooProductPrice, v.price)
        end
      end
      GameVip.onSaleProductList = content.activity_items
      GameVip.OnSaleFetchTime = os.time()
      GameVip.canShowMycard = false
      GameVip:Init()
    else
      GameVip.onSaleProductList = content.activity_items
      GameVip.OnSaleFetchTime = os.time()
      GameVip.DoShowPurchase()
    end
    GameUIRechargePush:OnSaleListChange()
    GameUIFirstCharge:OnEnter()
    return true
  elseif msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.pay_list2_req.Code then
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  end
  return false
end
function GameVip.getMycardUrlCallback(msgType, content)
  DebugOut("getMycardUrlCallback")
  GameUtils:DebugOutTableInAndroid(content)
  if msgType == NetAPIList.pay_verify2_ack.Code then
    GameVip.storeObject = GameVip.storeObject or StoreObject:new()
    GameVip.storeObject:ShowMycardUrl(content.payment_url)
    if GameVip:GetFlashObject() then
      GameVip:ReqGiftData()
      GameVip:ReqMonthCardData()
    end
    return true
  elseif msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.pay_verify2_req.Code then
    if content.code ~= 5006 and content.code ~= 5007 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    if GameVip:GetFlashObject() then
      GameVip:ReqGiftData()
      GameVip:ReqMonthCardData()
    end
    return true
  end
  return false
end
function GameVip.VerifyPointAndPWDCallback(msgType, content)
  DebugOut("VerifyPointAndPWDCallback")
  GameUtils:DebugOutTableInAndroid(content)
  if msgType == NetAPIList.pay_verify2_ack.Code then
    GameTip:Show(GameLoader:GetGameText("LC_MENU_TITLE_SUCCESS_INFO"))
    if GameVip:GetFlashObject() then
      GameVip:GetFlashObject():InvokeASCallback("_root", "closeCardNumAndPwdWin")
    end
    if GameVip:GetFlashObject() then
      GameVip:ReqGiftData()
      GameVip:ReqMonthCardData()
    end
    return true
  elseif msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.pay_verify2_req.Code then
    if content.code ~= 5006 and content.code ~= 5007 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    if GameVip:GetFlashObject() then
      GameVip:ReqGiftData()
      GameVip:ReqMonthCardData()
    end
    return true
  end
  return false
end
function GameVip.ordersNtf(content)
  GameUtils:printByAndroid("-------------ordersNtf.msgType-------------------------")
  GameUtils:DebugOutTableInAndroid(content)
  GameUtils:printByAndroid("-----------------------------")
  GameVip.ordersIndex = 1
  GameVip.MycardOrderReq = content.orders
  for k, v in pairs(GameVip.MycardOrderReq) do
    v.MycardOrderReqSatates = true
    v.waiting_seconds = os.time() + v.waiting_seconds
  end
end
function GameVip.qihooRecords(content)
  GameUtils:printByAndroid("--------------GameVip.qihooRecords---------------")
  GameUtils:DebugOutTableInAndroid(content)
  GameUtils:printByAndroid("--------------end   GameVip.qihooRecords---------------")
  if GameVip.MycardOrderReq then
    GameUtils:printByAndroid("------------MycardOrderReq------------qihooRecords")
    GameVip.MycardOrderReq[1].orderId = content.param[1]
    GameVip.MycardOrderReq[1].moneyAmount = content.price
    GameUtils:printByAndroid(GameVip.MycardOrderReq[1].orderId)
    GameUtils:printByAndroid(GameVip.MycardOrderReq[1].moneyAmount)
  end
end
function GameVip:MycardOrederConfim(orderId)
  local function netCall()
    local content = {id = orderId}
    NetMessageMgr:SendMsg(NetAPIList.pay_confirm_req.Code, content, GameVip.MycardOrederConfimCallback, false, nil)
  end
  if not GameVip:ReqWishProduct(GameVip.BuyID, GameVip.BuyProductID, GameVip.BuyCount, netCall) and not GameVip.IsReqWish then
    netCall()
  end
end
function GameVip.MycardOrederConfimCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.pay_confirm_req.Code then
    GameVip.ordersIndex = GameVip.ordersIndex + 1
    GameUtils:printByAndroid("---MycardOrederConfimCallback ---- GameVip.ordersIndex----" .. tostring(GameVip.ordersIndex))
    if GameVip:GetFlashObject() then
      GameVip:ReqGiftData()
      GameVip:ReqMonthCardData()
    end
    return true
  end
  return false
end
function GameVip:Init()
  self.storeObject = self.storeObject or StoreObject:new()
  DebugOut("GameVip:Init: ", self.productIdentifierList, #self.productIdentifierList)
  DebugTable(self.productIdentifierList)
  if IPlatformExt.getConfigValue("GameServerPriceControl") == "True" then
    local priceList = {}
    for i, v in ipairs(GameVip.ProductionsList) do
      if IPlatformExt.getConfigValue("CurrencyUnit_Postposition") == "True" then
        DebugOut("Price:" .. v.price / 100)
        priceList[v.id] = v.price / 100 .. IPlatformExt.getConfigValue("CurrencyUnit")
        DebugOut("endPrice:" .. priceList[v.id])
      else
        DebugOut("Price:" .. v.price / 100)
        priceList[v.id] = IPlatformExt.getConfigValue("CurrencyUnit") .. v.price / 100
        DebugOut("endPrice:" .. priceList[v.id])
      end
    end
    self.storeObject:productDataReceived(priceList)
  else
    self.storeObject:RequestProductData(self.productIdentifierList)
    DebugOut("xxpp RequestProductData ", LuaUtils:serializeTable(self.productIdentifierList))
  end
end
function GameVip.ForceUpdateTransactionStatus()
  DebugStore("call StoreObject:ForceUpdateTransactionStatus")
  GameVip.storeObject = GameVip.storeObject or StoreObject:new()
  if not AutoUpdate.isAndroidDevice and not AutoUpdate.isWPDevice then
    GameVip.storeObject:ForceUpdateTransactionStatus()
  end
end
function GameVip.DoShowPurchase()
  DebugTable(GameVip.productList)
  if GameVip.productList and LuaUtils:table_size(GameVip.productList) > 0 then
    GameVip:RefreshProductList()
  end
end
function GameVip.UpdateFirstCharge()
  if GameVip.productList and LuaUtils:table_size(GameVip.productList) > 0 and GameVip:GetFlashObject() then
    GameVip:RefreshProductList()
  end
end
GameVip.EnabledMoreFeatire = true
function GameVip:IsEnableExtMoreFeatire()
  if not AutoUpdate.isAndroidDevice then
    return false
  end
  if IPlatformExt.getConfigValue("SupportBuyMoreWithPlatformBuy") == "true" then
    local json = IPlatformExt.getConfigValue("MoreBuyMethodsList")
    if not json then
      return false
    end
    local buyMoreMethodsList = ext.json.parse(json)
    local anyMethodenabled = false
    GameUtils:printByAndroid("MoreBuyMethodsList")
    GameUtils:DebugOutTableInAndroid(buyMoreMethodsList)
    GameUtils:printByAndroid("GameVipExtBuyMethodsEnableList.BuyMethodsList")
    GameUtils:DebugOutTableInAndroid(GameVipExtBuyMethodsEnableList.BuyMethodsList)
    for k, v in pairs(buyMoreMethodsList) do
      for k2, v2 in pairs(GameVipExtBuyMethodsEnableList.BuyMethodsList) do
        if v.buyType == v2.pay_type and v2.is_open == 1 and GameVipExtBuyMethodsEnableList:IsReachMethodLevelLimit(v2.pay_type) then
          anyMethodenabled = true
        end
      end
    end
    GameUtils:printByAndroid("anyMethodenabled = ")
    return anyMethodenabled
  end
  return false
end
function GameVip:IsEnableExtMultiBuyFeature()
  if not AutoUpdate.isAndroidDevice then
    return false
  end
  if IPlatformExt.getConfigValue("SupportBuyProductsWithMultiMehtods") == "true" then
    local json = IPlatformExt.getConfigValue("MultiBuyMethodsList")
    if not json then
      return false
    end
    local buyMultiMethodsList = ext.json.parse(json)
    local anyMethodenabled = false
    GameUtils:printByAndroid("MultiBuyMethodsList")
    GameUtils:DebugOutTableInAndroid(buyMultiMethodsList)
    GameUtils:printByAndroid("GameVipExtBuyMethodsEnableList.BuyMethodsList")
    GameUtils:DebugOutTableInAndroid(GameVipExtBuyMethodsEnableList.BuyMethodsList)
    for k, v in pairs(buyMultiMethodsList) do
      for k2, v2 in pairs(GameVipExtBuyMethodsEnableList.BuyMethodsList) do
        if v.buyType == v2.pay_type and v.buyFormat == "ExtMultiBuy" and v2.is_open == 1 and GameVipExtBuyMethodsEnableList:IsReachMethodLevelLimit(v2.pay_type) then
          anyMethodenabled = true
        end
      end
    end
    GameUtils:printByAndroid("anyMethodenabled = ")
    return anyMethodenabled
  end
  return false
end
function GameVip:RefreshProductList()
  if not self:GetFlashObject() then
    return
  end
  GameWaiting:HideLoadingScreen()
  if GameVip.productList and GameVip.ProductionsList then
    local price = ""
    local count = 0
    local haveReocrd = false
    local iconIndex = ""
    local productIds = ""
    local firstbuys = ""
    local hideItemIdList = {}
    GameVip:GetFlashObject():InvokeASCallback("_root", "RemoveAllOnsaleCards")
    if 0 < #GameVip.onSaleProductList then
      local cardIter = 1
      local frIdx = 1
      GameVip.mPushItemIndexList = {}
      local function downloadImgCallback(content)
        local flashObj = GameVip:GetFlashObject()
        if flashObj and GameVip.mPushItemIndexList and #GameVip.mPushItemIndexList > 0 then
          for k, v in pairs(GameVip.mPushItemIndexList) do
            local resName = v.img
            local localPath = "data2/" .. DynamicResDownloader:GetFullName(resName, DynamicResDownloader.resType.PAY_PIC)
            if DynamicResDownloader:IfResExsit(localPath) then
              DebugOut("lua UpdatePushItemIcon " .. localPath)
              local orgTexture = "icon_box_" .. tostring(v.frIdx) .. ".png"
              flashObj:ReplaceTexture(orgTexture, resName)
              flashObj:InvokeASCallback("_root", "UpdatePushItemIcon", v.cardIdx, "icon_box_" .. tostring(v.frIdx))
            end
          end
        end
      end
      for i, v in ipairs(GameVip.onSaleProductList) do
        if not v.timeout then
          local itemNameList = ""
          local itemCountList = ""
          local itemIconList = ""
          local cardName = GameHelper:GetFoatColor(v.word.title_font, GameLoader:GetGameText(v.activity_name)) or ""
          local index = -1
          for iIdentifier, vIdentifier in ipairs(GameVip.productIdentifierList) do
            if vIdentifier == v.production_id then
              index = iIdentifier
              break
            end
          end
          if index > 0 then
            DebugOut("onSaleProductList : " .. tostring(i))
            DebugTable(v)
            local cardPrice = GameVip.productList[index]
            local oldPrice = GameVip:getOldPriceV2(cardPrice, v.discount_rate)
            local discount_rate = GameHelper:GetFoatColor(v.word.discount_font, v.discount_rate .. "%")
            local cardIndexInPruchaseItems = GameVip.ProductionsList[index].credit
            local productId = GameVip.ProductionsList[index].id
            local creditNum = v.credit
            local onsalePercent = v.credit_privilege .. "%"
            hideItemIdList[index] = true
            local extInfo = {}
            extInfo.carIndex = i
            local isPushProduct = false
            isPushProduct = true
            if v.icon and "" ~= v.icon and "undefined" ~= v.icon then
              local resName = v.icon
              local localPath = "data2/" .. DynamicResDownloader:GetFullName(resName, DynamicResDownloader.resType.PAY_PIC)
              DebugOut("localPath " .. localPath)
              if DynamicResDownloader:IfResExsit(localPath) then
                ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
              else
                local downloadInfo = {}
                downloadInfo.localPath = "data2/" .. resName
                DynamicResDownloader:AddDynamicRes(resName, DynamicResDownloader.resType.PAY_PIC, downloadInfo, downloadImgCallback)
              end
              local imgItem = {
                cardIdx = cardIter,
                frIdx = frIdx,
                img = resName
              }
              table.insert(GameVip.mPushItemIndexList, imgItem)
            end
            frIdx = frIdx + 1
            for iItem, vItem in ipairs(v.gift_items) do
              local icon = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(vItem, extInfo, GameVip.DynamicResDowloadFinished)
              itemNameList = itemNameList .. GameHelper:GetAwardTypeText(vItem.item_type, vItem.number) .. "\001"
              itemCountList = itemCountList .. "x " .. GameUtils.numberConversion(GameHelper:GetAwardCount(vItem.item_type, vItem.number, vItem.no)) .. "\001"
              itemIconList = itemIconList .. icon .. "\001"
            end
            DebugOut("v = ")
            DebugTable(v)
            if v.background2 and "" ~= v.background2 and "undefined" ~= v.background2 then
              local localPath = "data2/" .. DynamicResDownloader:GetFullName(v.background2, DynamicResDownloader.resType.FESTIVAL_BANNER)
              if DynamicResDownloader:IfResExsit(localPath) then
                DebugOut("extInfo.localPath = " .. localPath)
                GameVip:GetFlashObject():ReplaceTexture("LAZY_LOAD_FangPIC_100.png", v.background2)
              else
                GameHelper:GetBackgroundImgDynamic(itemId, "LAZY_LOAD_FangPIC_100.png", v.background2, GameVip.GetBackgroundImgDynamicCallBack)
              end
            end
            local btnMoreText = GameLoader:GetGameText("LC_MENU_PAY_SPACIAL_MORE_CHAR")
            local moreText = GameHelper:GetFoatColor(v.word.discount_font, btnMoreText)
            local endTimeLabText = GameLoader:GetGameText("LC_MENU_EVENT_GIVENDAY_END_CHAR")
            GameVip:GetFlashObject():InvokeASCallback("_root", "AddOnsaleCards", cardIter, cardIndexInPruchaseItems, cardName, cardPrice, creditNum, onsalePercent, itemNameList, itemCountList, itemIconList, GameLoader:GetGameText("LC_MENU_PAY_SPACIAL_BUY_BUTTON"), productId, isPushProduct, endTimeLabText, oldPrice, discount_rate, btnMoreText, moreText)
            cardIter = cardIter + 1
          else
          end
        else
        end
      end
      downloadImgCallback()
    end
    DebugOut("GameVip.productList")
    DebugTable(GameVip.productList)
    DebugTable(GameVip.productIdentifierList)
    DebugTable(GameVip.ProductionsList)
    local cnt = #GameVip.ProductionsList
    for k = 1, cnt do
      local v = GameVip.productList[k]
      if v and not hideItemIdList[k] and 0 == GameVip.ProductionsList[k].is_push then
        DebugOut("hello world k = ", k)
        haveReocrd = true
        price = price .. v .. "\001"
        count = count + 1
        iconIndex = iconIndex .. GameVip.ProductionsList[k].credit .. "\001"
        productIds = productIds .. GameVip.ProductionsList[k].id .. "\001"
        firstbuys = firstbuys .. GameUIFirstCharge:GetIsFirstCharge(GameVip.ProductionsList[k].id) .. "\001"
      end
    end
    local hideMoreItem = true
    if GameVip:IsEnableExtMoreFeatire() or not AutoUpdate.isAndroidDevice then
      hideMoreItem = false
      iconIndex = iconIndex .. #self.productIdentifierList + 1 .. "\001"
    end
    local moreBtnTex
    if GameVip:IsEnableExtMoreFeatire() then
      moreBtnTex = GameLoader:GetGameText("LC_MENU_MASSAGES_PAY")
    end
    local first_chageFlag = self.first_chage
    local first_chageFlagText = GameLoader:GetGameText("LC_MENU_FIRSTCHARGE_FIRSTTIME")
    local deviceType = GameUtils:isIOSBundle() and "IOS" or "Android"
    DebugOut("firstbuys = " .. firstbuys)
    GameVip:GetFlashObject():InvokeASCallback("_root", "initPayData", count, price, iconIndex, hideMoreItem, productIds, moreBtnTex, first_chageFlag, first_chageFlagText, firstbuys, deviceType)
  end
end
function GameVip.GetBackgroundImgDynamicCallBack(extInfo)
  DebugOut("extInfo = ")
  DebugTable(extInfo)
  local flashObj = GameVip:GetFlashObject()
  if not flashObj then
    return
  end
  flashObj:ReplaceTexture(extInfo.localImg, extInfo.localPath)
  DebugOut("xxdfsdf")
end
function GameVip.DynamicResDowloadFinished(extInfo)
end
function GameVip:SetItem(itemKey)
  local _status, nameText, priceText, iconFrame = "normal", "itemKey", self.productList[itemKey], "xxx"
  DebugOut("setItem: ", itemKey, _status, nameText, priceText, iconFrame)
  self:GetFlashObject():InvokeASCallback("_root", "setItem", itemKey, _status, nameText, priceText, iconFrame)
end
function GameVip:SetItemPrice(itemKey, price)
  DebugOut("SetItemPrice key = ", itemKey)
  DebugOut("SetItemPrice price = ", price)
  self.productList[itemKey] = price
end
function GameVip.checkReceiptCallback(msgType, content)
  GameWaiting:HideLoadingScreen()
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.pay_verify_req.Code then
    if GameVip:GetFlashObject() then
      GameVip:ReqGiftData()
      GameVip:ReqMonthCardData()
    end
    if not GameVip.storeObject then
      DebugOut("GameTip.object  --nil--")
      GameVip.storeObject = StoreObject:new()
    end
    if content.api == NetAPIList.pay_verify_req.Code then
      if content.code == 0 or content.code == 5005 then
        local success = 1
        if content.code == 5005 then
          success = 0
        else
        end
        GameVip.storeObject:ConfirmTransactionCompleted(success)
        DebugOut("close the pruchase in Success")
        if GameVip.BankMonthCard.isActivation then
          GameVip.BankMonthCard:DoFinish()
        else
          GameVip.UpdateAfterPurchaseSucess()
        end
        return true
      else
        local success = 0
        if content.code ~= 100002 then
          DebugOut("close the pruchase in error")
          GameVip.storeObject:ConfirmTransactionCompleted(success)
        end
        GameUIGlobalScreen:ShowAlert("error", content.code, nil)
        if GameVip.BankMonthCard.isActivation then
          GameVip.BankMonthCard:DoFinish()
        else
          GameVip.UpdateAfterPurchaseSucess()
        end
        return true
      end
    end
  end
  return false
end
function GameVip:RequestCarrierProductList()
  if IPlatformExt.getConfigValue("PlatformExtCharge") ~= "true" then
    return
  end
  local quick_buy_channel = IPlatformExt.getConfigValue("quick_buy_channel")
  local quick_buy_area = IPlatformExt.getConfigValue("quick_buy_area")
  if string.len(quick_buy_channel) < 1 or string.len(quick_buy_area) < 1 then
    return
  end
  local content = {channel = quick_buy_channel, area = quick_buy_area}
  NetMessageMgr:SendMsg(NetAPIList.pay_list_carrier_req.Code, content, GameVip.RequestCarrierProductListCallback, true, nil)
end
function GameVip.RequestCarrierProductListCallback(msgType, content)
  if msgType == NetAPIList.pay_list_carrier_ack.Code then
    GameVip.CarrierProductList = content
    GameVip.CarrierProductList.title_text = GameLoader:GetGameText("LC_MENU_CARRIER")
    GameVip.CarrierProductList.pay_text = GameLoader:GetGameText("LC_MENU_GOOGLE_PAY")
    GameVip.CarrierProductList.desc_text = GameLoader:GetGameText("LC_MENU_CARRIER_INFO_1")
    GameVip.CarrierProductList.CurrencyUnit = IPlatformExt.getConfigValue("quick_buy_CurrencyUnit")
    return true
  elseif msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.pay_list_carrier_req.Code then
    GameVip.CarrierProductList = nil
    return true
  end
  return false
end
function GameVip:UpdateOnSaleTime()
  local flash_obj = self:GetFlashObject()
  if not flash_obj then
    return
  end
  if #GameVip.onSaleProductList > 0 then
    for i, v in ipairs(GameVip.onSaleProductList) do
      local tmpLeftTime = 0
      if 0 < v.end_time and not v.timeout then
        tmpLeftTime = v.end_time - (os.time() - GameVip.OnSaleFetchTime)
        if tmpLeftTime < 0 then
          tmpLeftTime = 0
          v.timeout = true
          GameVip:RequestProductIdentifierList()
        else
          v.timeout = false
        end
        local endTimeLabText = GameHelper:GetFoatColor(v.word.end_font, GameLoader:GetGameText("LC_MENU_EVENT_GIVENDAY_END_CHAR"))
        local timeStr = GameHelper:GetFoatColor(v.word.time_font, GameUtils:formatTimeString(tmpLeftTime))
        local str = endTimeLabText .. " " .. timeStr
        flash_obj:InvokeASCallback("_root", "updateCardLeftTime", i, str)
      end
    end
  end
end
function GameVip:ShowMoreItemsScreen()
  local flash_obj = self:GetFlashObject()
  if not flash_obj then
    return
  end
  GameVip:RefreshMoreItemsList()
  flash_obj:InvokeASCallback("_root", "showMoreItems")
end
function GameVip:HideMoreItemsMenu()
  local flash = self:GetFlashObject()
  if not flash then
    return
  end
  flash:InvokeASCallback("_root", "hideRecoverPanel")
end
function GameVip:RefreshMoreItemsList()
  local flash = self:GetFlashObject()
  if not flash then
    return
  end
  flash:InvokeASCallback("_root", "clearMoreItemsList")
  flash:InvokeASCallback("_root", "initMoreItemsList")
  local saleItem = GameVip.onSaleProductList[GameVip.curSelectSaleIndex]
  local itemNum = #saleItem.gift_items
  for i = 1, itemNum do
    flash:InvokeASCallback("_root", "addMoreItemsItem", i)
  end
  flash:InvokeASCallback("_root", "enableAutoArrowSet", true)
end
function GameVip:UpdateMoreItemsItem(id)
  local flash = self:GetFlashObject()
  if not flash then
    return
  end
  local itemIndex = tonumber(id)
  local saleItem = GameVip.onSaleProductList[GameVip.curSelectSaleIndex]
  local item = saleItem.gift_items[itemIndex]
  local itemName = GameHelper:GetAwardTypeText(item.item_type, item.number)
  local itemNumber = "x" .. GameUtils.numberConversion(GameHelper:GetAwardCount(item.item_type, item.number, item.no))
  local itemIcon
  itemIcon = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(item, nil, nil)
  flash:InvokeASCallback("_root", "setMoreItemsItem", itemIndex, itemName, itemNumber, itemIcon)
end
function GameVip.UpdateAfterPurchaseSucess()
  GameVip:RequestProductIdentifierList()
  GameVip.backToFirstCharge = nil
  GameVip.hasPaySuccess = true
end
function GameVip:SendPriceToServer(price)
  local content = {}
  content.pay_price = price
  NetMessageMgr:SendMsg(NetAPIList.player_pay_price_req.Code, content, nil, false)
end
function GameVip.getPurchaseEnableInfo(content)
  GameVip.gamePurchaseEanble = content.pay_status
end
function GameVip:getOldPrice(price, discount_rate)
  if GameVip.ShowOnSaleOldPrice == 0 then
    return ""
  end
  if price and discount_rate then
    local str1, num1 = string.find(price, "%d")
    local str2, num2 = string.gsub(price, "[%d%.]", "")
    local _price = string.sub(price, str1, str1 + num2 - 1)
    local oldPrice = _price * discount_rate / 100
    local str3, num3 = string.gsub(price, "%d", "")
    local str4, num4 = string.gsub(str3, "%.", oldPrice)
    print("price:" .. price .. " discount_rate:" .. discount_rate .. " oldPrice:" .. oldPrice .. " str4:" .. str4)
    return str4
  end
  return ""
end
function GameVip:getOldPriceV2(price, discount_rate)
  if GameVip.ShowOnSaleOldPrice == 0 then
    return ""
  end
  if price and discount_rate then
    local priceCurrency, priceStr, priceDotNumber, priceCurrency2 = string.match(price, "([^%d]*)(%d+[^%d]-%d*([^%d]-%d-))([^%d]*)$")
    if priceStr then
      DebugOut("priceStr = ", priceStr)
      DebugOut("priceCurrency = ", priceCurrency)
      DebugOut("priceCurrency2 = ", priceCurrency2)
      local commaIndex = string.find(priceStr, ",")
      local commaReverseIndex
      local dotIndex = string.find(priceStr, ".", 1, true)
      local dotReverseIndex
      if commaIndex then
        commaReverseIndex = string.len(priceStr) - commaIndex
      end
      if dotIndex then
        dotReverseIndex = string.len(priceStr) - dotIndex
      end
      local priceNum = string.gsub(priceStr, "[^%d]*", "")
      if priceNum then
        DebugOut("priceNum = ", priceNum)
        DebugOut("commaIndex = ", commaIndex)
        DebugOut("dotIndex = ", dotIndex)
        local priceRealNum = tonumber(priceNum)
        if priceRealNum then
          local newPrice = math.floor(priceRealNum * (discount_rate / 100)) .. ""
          DebugOut("newPrice = ", newPrice)
          local resultStr = newPrice
          if dotReverseIndex then
            if commaReverseIndex then
              if commaReverseIndex > dotReverseIndex then
                local tmpResultStr = LuaUtils:string_insert(newPrice:reverse(), ".", dotReverseIndex)
                DebugOut("tmpResultStr 555555= ", tmpResultStr)
                tmpResultStr = LuaUtils:string_insert(tmpResultStr, ",", commaReverseIndex)
                DebugOut("tmpResultStr 6666666= ", tmpResultStr)
                resultStr = tmpResultStr:reverse()
                DebugOut("tmpResultStr 33333= ", tmpResultStr)
              else
                local tmpResultStr = LuaUtils:string_insert(newPrice:reverse(), ",", commaReverseIndex)
                DebugOut("tmpResultStr 777777= ", tmpResultStr)
                tmpResultStr = LuaUtils:string_insert(tmpResultStr, ".", dotReverseIndex)
                DebugOut("tmpResultStr 888888= ", tmpResultStr)
                resultStr = tmpResultStr:reverse()
                DebugOut("tmpResultStr 44444= ", tmpResultStr)
              end
            else
              local tmpResultStr = LuaUtils:string_insert(newPrice:reverse(), ".", dotReverseIndex)
              DebugOut("tmpResultStr 99999= ", tmpResultStr)
              resultStr = tmpResultStr:reverse()
            end
          elseif commaReverseIndex then
            local tmpResultStr = LuaUtils:string_insert(newPrice:reverse(), ",", commaReverseIndex)
            DebugOut("tmpResultStr 111= ", tmpResultStr)
            resultStr = tmpResultStr:reverse()
            DebugOut("resultStr 11111= ", resultStr)
          end
          if priceCurrency and resultStr then
            resultStr = priceCurrency .. resultStr
          end
          if priceCurrency2 and resultStr then
            resultStr = resultStr .. priceCurrency2
          end
          DebugOut("resultStr 22 = ", resultStr)
          return resultStr
        end
      end
    end
    return ""
  end
  return ""
end
function StoreObject:productDataReceived(productPriceList)
  DebugOut("productDataReceived: ", LuaUtils:table_size(productPriceList))
  GameVip.productList = GameVip.productList or {}
  if LuaUtils:table_size(productPriceList) > 0 then
    GameVip.isPriceValid = true
  end
  for k, v in pairs(productPriceList) do
    DebugOut("----->product:" .. k .. " price:" .. v)
    if not GameVip.gamePriceIdentify then
      GameVip.gamePriceIdentify = v
    end
    for i = 1, #GameVip.productIdentifierList do
      if GameVip.productIdentifierList[i] == k then
        if GameUtils:IsQihooApp() then
          GameVip:SetItemPrice(i, "\239\191\165" .. GameVip.qihooProductPrice[i] / 100)
        else
          GameVip:SetItemPrice(i, v)
        end
        if GameVip.canShowMycard and GameVip.myCardInfo and GameVip.myCardInfo[i] and GameVip.myCardInfo[i].payments[2] and GameVip.myCardInfo[i].payments[2].price and not ext.IsReachableWifi() and 0 < tonumber(GameVip.myCardInfo[i].payments[2].price) then
          GameVip:SetItemPrice(i, "TWD" .. tonumber(GameVip.myCardInfo[i].payments[2].price) / 100)
        end
      end
    end
  end
  DebugOut("GameVip.gamePriceIdentify = ", GameVip.gamePriceIdentify)
  GameVip.gamePriceIdentify = GameVip.gamePriceIdentify or ""
  GameVip:SendPriceToServer(GameVip.gamePriceIdentify)
  GameVip:RefreshProductList()
  GameUIRechargePush:CheckPop()
  GameUIFirstCharge:OnEnter()
end
function StoreObject:multiTransactionCompleted(receipt, productIdentifier, count)
  DebugOut("multiTransactionCompleted receipt: ", receipt, productIdentifier)
  local platformString = ext.GetPlatform()
  GameWaiting:HideLoadingScreen()
  local signatureString = ""
  GameVip.mCurVerifyProduct = productIdentifier
  if GameUtils:IsChinese() and ext.GetPlatform() == "iOS" and GameVip.mCurVerifyProduct then
    local curPrice = GameVip.GetPriceByProductID(GameVip.mCurVerifyProduct)
    GameVip:SendPriceToServer(curPrice or "")
  end
  local function netCall()
    local pay_verify_req_param = {
      platform = platformString == "Android" and 1 or 0,
      pay_type = productIdentifier,
      token = receipt,
      signature = signatureString
    }
    DebugOutPutTable(pay_verify_req_param, "verify_req")
    NetMessageMgr:SendMsg(NetAPIList.pay_verify_req.Code, pay_verify_req_param, GameVip.checkReceiptCallback, true, netCall)
  end
  if not GameVip:ReqWishProduct(GameVip.BuyID, GameVip.BuyProductID, GameVip.BuyCount, netCall) and not GameVip.IsReqWish then
    netCall()
  end
end
function StoreObject:transactionCompleted(receipt, productIdentifier)
  GameVip.mCurVerifyProduct = productIdentifier
  if GameUtils:IsChinese() and ext.GetPlatform() == "iOS" and GameVip.mCurVerifyProduct then
    local curPrice = GameVip.GetPriceByProductID(GameVip.mCurVerifyProduct)
    GameVip:SendPriceToServer(curPrice or "")
  end
  DebugOut("transactionCompleted receipt: ", receipt, productIdentifier)
  local platformString = ext.GetPlatform()
  GameWaiting:HideLoadingScreen()
  local signatureString = ""
  local function netCall()
    local pay_verify_req_param = {
      platform = platformString == "Android" and 1 or 0,
      pay_type = productIdentifier,
      token = receipt,
      signature = signatureString
    }
    DebugOutPutTable(pay_verify_req_param, "verify_req")
    NetMessageMgr:SendMsg(NetAPIList.pay_verify_req.Code, pay_verify_req_param, GameVip.checkReceiptCallback, true, netCall)
  end
  if not GameVip:ReqWishProduct(GameVip.BuyID, GameVip.BuyProductID, GameVip.BuyCount, netCall) and not GameVip.IsReqWish then
    netCall()
  end
end
function GameVip:HideLoadingScreen()
  GameWaiting:HideLoadingScreen()
end
function StoreObject:transactionFailed(canceled)
  DebugOut("StoreObject:transactionFailed")
  GameWaiting:HideLoadingScreen()
end
function StoreObject:initAlipayOrder(productId, productName, productDes)
  GameUtils:printByAndroid("----fuck productId-----" .. productId)
  GameUtils:printByAndroid("----fuck productName-----" .. productName)
  GameUtils:printByAndroid("----fuck productDes-----" .. productDes)
  local requestType = "taobao/order_info?subject=" .. productId
  GameUtils:HTTP_SendRequest(requestType, nil, GameVip.InitAlipayOrderCallback, true, GameUtils.httpRequestFailedCallback, "GET", "notencrypt")
end
function GameVip.InitAlipayOrderCallback(responseContent)
  GameUtils:printByAndroid("----fuck responseContent-----" .. responseContent)
  GameVip.storeObject:OnGetAlipayInitOrderInfo(responseContent)
  GameWaiting:HideLoadingScreen()
  return true
end
function StoreObject:verifyPurchase(signedData, signature, pay_type)
  if not GameGlobalData.isLogin then
    GameUtils:printByAndroid("----fuck , we are not Login -----")
    local thePurchase_tobeVerify = {
      tobeVerify_signedData = signedData,
      tobeVerify_signature = signature,
      tobeVerify_pay_type = pay_type
    }
    table.insert(GameGlobalData.Purchase_tobeVerify, thePurchase_tobeVerify)
    return
  end
  local productID = pay_type
  local signatureDataTemp = signedData
  local signatureTemp = signature
  GameWaiting:HideLoadingScreen()
  local function netCall()
    local pay_verify_req_param = {
      platform = 1,
      pay_type = productID,
      token = signatureDataTemp,
      signature = signatureTemp
    }
    NetMessageMgr:SendMsg(NetAPIList.pay_verify_req.Code, pay_verify_req_param, GameVip.checkReceiptCallback, true, netCall)
  end
  if not GameVip:ReqWishProduct(GameVip.BuyID, GameVip.BuyProductID, GameVip.BuyCount, netCall) and not GameVip.IsReqWish then
    netCall()
  end
end
function StoreObject:getMycardUrl(mycardType, productId)
  GameUtils:printByAndroid("----------------StoreObject:getMycardUrl---mycardType: " .. mycardType .. ", productId: " .. productId)
  local function netCall()
    local content = {
      id = productId,
      payment_type = tostring(mycardType),
      params = {}
    }
    if GameUtils:IsQihooApp() then
      NetMessageMgr:SendMsg(NetAPIList.pay_verify2_req.Code, content, GameVip.getQihooPayCallback, true, nil)
    else
      NetMessageMgr:SendMsg(NetAPIList.pay_verify2_req.Code, content, GameVip.getMycardUrlCallback, true, nil)
    end
  end
  if not GameVip:ReqWishProduct(GameVip.BuyID, GameVip.BuyProductID, GameVip.BuyCount, netCall) and not GameVip.IsReqWish then
    netCall()
  end
end
function StoreObject:VerifyPointAndPWD(productId, mycardPoint, mycardPWD)
  local function netCall()
    local content = {
      id = productId,
      payment_type = tostring(GameVip.mycardType_card),
      params = {mycardPoint, mycardPWD}
    }
    DebugOut("VerifyPointAndPWD")
    DebugTable(content)
    NetMessageMgr:SendMsg(NetAPIList.pay_verify2_req.Code, content, GameVip.VerifyPointAndPWDCallback, true, nil)
  end
  if not GameVip:ReqWishProduct(GameVip.BuyID, GameVip.BuyProductID, GameVip.BuyCount, netCall) and not GameVip.IsReqWish then
    netCall()
  end
end
function GameVip.getQihooPayCallback(msgType, content)
  if msgType == NetAPIList.pay_verify2_ack.Code then
    GameUtils:printByAndroid("GameVip.getQihooPayCallback========msgType=" .. msgType)
    GameUtils:DebugOutTableInAndroid(content)
    GameUtils:printByAndroid("===================end  NetAPIList.pay_verify2_ack.Code========")
    local LoginInfo = GameUtils:GetLoginInfo()
    for k, v in pairs(GameVip.MycardOrderReq) do
      v.exchangeRate = "1"
      v.productName = "\228\191\161\231\148\168\231\130\185-" .. GameVip.myCardInfo[tonumber(GameVip.BuyArg)].credit .. "\231\130\185"
      v.productId = GameVip.productIdentifierList[tonumber(GameVip.BuyArg)]
      v.notify_url = content.payment_url
      v.appUserId = LoginInfo.AccountInfo.passport
    end
    GameVip.MycardOrderReq.RuningID = GameVip.MycardOrderReq[1].id
    GameVip.MycardOrderReq.OrderID = GameVip.MycardOrderReq[1].orderId
    GameVip.MycardOrderReq.sku = GameVip.productIdentifierList[tonumber(GameVip.BuyArg)]
    GameVip.MycardOrderReq.NotifyUrl = content.payment_url
    GameVip.MycardOrderReq.quantity = GameVip.myCardInfo[tonumber(GameVip.BuyArg)].credit
    GameVip.MycardOrderReq.PlayerLevel = GameGlobalData:GetData("levelinfo").level or ""
    GameVip.MycardOrderReq.ServerID = GameUtils:GetActiveServerInfo().id or ""
    GameVip.MycardOrderReq.PlayerID = GameGlobalData:GetUserInfo().player_id or ""
    GameVip.MycardOrderReq.ServerName = GameUtils:GetActiveServerInfo().name or ""
    GameVip.MycardOrderReq.PlayerName = GameGlobalData:GetUserInfo().name or ""
    GameVip.MycardOrderReq.Passport = LoginInfo.AccountInfo.passport
    GameVip.MycardOrderReq.Price = GameVip.MycardOrderReq[1].moneyAmount
    GameVip.MycardOrderReq.Sign = ""
    GameUtils:printByAndroid("--------GameVip.MycardOrderReq----luaQihoo-----")
    GameUtils:DebugOutTableInAndroid(GameVip.MycardOrderReq)
    GameUtils:printByAndroid("------------------end----------------------")
    IPlatformExt.Charge(ext.json.generate(GameVip.MycardOrderReq))
    if GameVip:GetFlashObject() then
      GameVip:ReqGiftData()
      GameVip:ReqMonthCardData()
    end
    return true
  elseif msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.pay_verify2_req.Code then
    GameUtils:printByAndroid("++++GameVip.getQihooPayCallback++++common_ack++++++")
    if content.code ~= 5006 and content.code ~= 5007 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    if GameVip:GetFlashObject() then
      GameVip:ReqGiftData()
      GameVip:ReqMonthCardData()
    end
    return true
  end
  return false
end
function chargeEnd(payInfo)
  local pay_info = {}
  pay_info = ext.json.parse(payInfo)
  GameUtils:printByAndroid("-----------chargeEnd--pay_info-----------------")
  GameUtils:DebugOutTableInAndroid(pay_info)
  if pay_info and tonumber(pay_info.error_code) == 0 then
    GameUtils:printByAndroid("360\230\148\175\228\187\152\230\136\144\229\138\159lua===================" .. pay_info.error_msg)
  end
end
function StoreObject:refeshAccessToken(refreshToken)
  GameUtils:HTTP_SendRequest("qihu_session/refresh_access_token", {refresh_token = refreshToken}, function(response)
    GameVip.storeObject:OnRefreshedAccessToken(response.expires_in, response.access_token, response.refresh_token)
  end, false, function(response)
    GameWaiting:HideLoadingScreen()
  end)
end
function StoreObject:initOrderInfo(productId)
  GameUtils:HTTP_SendRequest("android_orders/create_qihu_order", {product_id = productId}, function(response)
    GameWaiting:HideLoadingScreen()
    GameVip.storeObject:OnGetInitOrderInfo(response.order_id, response.notify_url)
  end, false, function(response)
    GameWaiting:HideLoadingScreen()
  end)
end
function StoreObject:pollingPayingResult(orderId)
  local function netCall()
    local parms = {trade_no = orderId}
    NetMessageMgr:SendMsg(NetAPIList.taobao_trade_req.Code, parms, function(msgType, content)
      if msgType == NetAPIList.common_ack.Code then
        GameVip.RefreshVipInfo()
        if not GameVip.storeObject then
          DebugOut("GameTip.object  --nil--")
          GameVip.storeObject = StoreObject:new()
        end
        if content.api == NetAPIList.taobao_trade_req.Code then
          GameUtils:printByAndroid("FUCK  NetAPIList.taobao_trade_req.Code")
          if content.code == 0 then
            GameVip.storeObject:FinishOrder(orderId, true)
            GameVip.UpdateAfterPurchaseSucess()
            if GameVip:GetFlashObject() then
              GameVip:ReqGiftData()
              GameVip:ReqMonthCardData()
            end
            return true
          else
            GameUIGlobalScreen:ShowAlert("error", content.code, nil)
            return true
          end
        end
      end
      return false
    end, false, nil)
  end
  if not GameVip:ReqWishProduct(GameVip.BuyID, GameVip.BuyProductID, GameVip.BuyCount, netCall) and not GameVip.IsReqWish then
    netCall()
  end
end
function StoreObject:verifyAmazonPayment(orderId, userId, purchaseToken)
  GameUtils:HTTP_SendRequest("android_orders/verify_amazon_order", {user_id = userId, purchase_token = purchaseToken}, function(respons)
    if respons then
      respons.is_succeeded = respons.is_succeeded or false
      GameVip.storeObject:FinishOrder(orderId, respons.is_succeeded)
      if respons.is_succeeded == true then
        local needShowTips = true
        if respons.vip_level_up[1].is_level_up == 1 then
          needShowTips = false
        end
        if respons.tmp_first_buy_pearl == 1 then
          GameVipMenu:ShowVipWindow()
          needShowTips = false
        end
        if not needShowTips or not respons.temp_gems or respons.temp_gems ~= 0 then
        end
      end
    end
  end, false, nil, false, "get")
end
function GameVip.GetPriceByProductID(productID)
  DebugOut("productId = " .. productID)
  DebugOutPutTable(GameVip.productIdentifierList, "list = ")
  if not GameVip.productList then
    return nil
  end
  for i = 1, #GameVip.productIdentifierList do
    if GameVip.productIdentifierList[i] == productID then
      return GameVip.productList[i]
    end
  end
  return "err"
end
function GameVip.PurchaseForACard(card_id, productID, dst_playerID, Finish_callback)
  GameVip.BankMonthCard:setMonthCard(card_id, dst_playerID, Finish_callback)
  GameVip.BuyArg = nil
  for i = 1, #GameVip.productIdentifierList do
    if GameVip.productIdentifierList[i] == productID then
      GameVip.BuyArg = i
      break
    end
  end
  GameVip.BankMonthCard:DoPurchaseForMonthCard(GameVip.DoRealPurchase, GameVip)
end
GameVip.BankMonthCard = {
  isActivation = false,
  MonthCard_ID = nil,
  MonthGiftPlayerID = 0,
  MonthBuyFinish_callback = nil,
  Clear = function(self)
    self.isActivation = false
    self.MonthCard_ID = nil
    self.MonthGiftPlayerID = 0
    self.MonthBuyFinish_callback = nil
  end,
  setMonthCard = function(self, card_id, dst_playerID, finish_callback)
    self.MonthCard_ID = card_id
    self.MonthGiftPlayerID = dst_playerID
    self.MonthBuyFinish_callback = finish_callback
    self.isActivation = true
  end,
  DoPurchaseForMonthCard = function(self, PurchaseNetCall, parame)
    local function month_card_callback(msgType, content)
      if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.month_card_buy_req.Code then
        PurchaseNetCall(parame)
        return true
      end
      return false
    end
    local buy_month_param = {
      id = self.MonthCard_ID,
      gift_user = self.MonthGiftPlayerID
    }
    NetMessageMgr:SendMsg(NetAPIList.month_card_buy_req.Code, buy_month_param, month_card_callback, true, nil)
  end,
  DoFinish = function(self)
    if self.MonthBuyFinish_callback then
      self.MonthBuyFinish_callback()
    end
    self:Clear()
  end
}
function GameVip:ShowMultiPop()
  local flash = self:GetFlashObject()
  if not flash then
    return
  end
  local json = IPlatformExt.getConfigValue("MultiBuyMethodsList")
  if not json then
    return
  end
  local buyMethodsList = ext.json.parse(json)
  GameUtils:printByAndroid("buyMethodsList")
  GameUtils:DebugOutTableInAndroid(buyMethodsList)
  for i, v in ipairs(buyMethodsList) do
    v.buyLocedName = GameLoader:GetGameText(v.buyName)
    if v.buyLocedName == nil or v.buyLocedName == "" then
      v.buyLocedName = v.buyName
    end
  end
  flash:InvokeASCallback("_root", "showMultiBuyMethodsPopUp", buyMethodsList)
end
function GameVip:ShowMorePop()
  local enableMoreMethodCount = 0
  local enableBuyType = ""
  local json = IPlatformExt.getConfigValue("MoreBuyMethodsList")
  if not json then
    return
  end
  local buyMethodsList = ext.json.parse(json)
  for k, v in pairs(GameVipExtBuyMethodsEnableList.BuyMethodsList) do
    for k2, v2 in pairs(buyMethodsList) do
      if v.pay_type == v2.buyType and v.is_open == 1 and GameVipExtBuyMethodsEnableList:IsReachMethodLevelLimit(v.pay_type) then
        enableMoreMethodCount = enableMoreMethodCount + 1
        enableBuyType = v.pay_type
      end
    end
  end
  if enableMoreMethodCount >= 2 then
    local flash = self:GetFlashObject()
    if not flash then
      return
    end
    GameUtils:printByAndroid("buyMethodsList")
    GameUtils:DebugOutTableInAndroid(buyMethodsList)
    flash:InvokeASCallback("_root", "showMoreBuyMethodsPopUp", buyMethodsList)
  elseif enableMoreMethodCount == 1 then
    GameVip.DoRealPurchase_ForPlatformExt(nil, nil, nil, enableBuyType)
  end
end
GameVip.mPushItemIndexList = nil
