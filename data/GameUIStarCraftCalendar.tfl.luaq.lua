require("StarCraftEvent.tfl")
local GameUIStarCraftCalendarRefresher = {}
GameUIStarCraftCalendarRefresher.mCalendar = nil
GameUIStarCraftCalendarRefresher.mNextRefreshDateTime = -1
function GameUIStarCraftCalendarRefresher.RefreshTimelineTimer()
  if GameUIStarCraftCalendarRefresher.mCalendar and GameUIStarCraftCalendarRefresher.mCalendar:IsTimelineShow() then
    GameUIStarCraftCalendarRefresher.mCalendar:ForceCheckCurrentEventState(false)
  end
  return nil
end
function GameUIStarCraftCalendarRefresher.RefreshTimelineCurrentTimeTimer()
  if GameUIStarCraftCalendarRefresher.mCalendar and GameUIStarCraftCalendarRefresher.mCalendar:IsTimelineShow() then
    GameUIStarCraftCalendarRefresher.mCalendar:SetCurrentDateAndTime()
  end
  return nil
end
function GameUIStarCraftCalendarRefresher:SetTargetCalendar(calendar)
  GameUIStarCraftCalendarRefresher.mCalendar = calendar
end
local GameUIBarLeft = LuaObjectManager:GetLuaObject("GameUIBarLeft")
GameUIStarCraftCalendar = luaClass(nil)
GameUIStarCraftCalendar.mMissions = {}
local WAIT_MISSION_REASON = {
  WAIT_REASON_ACTIVE = 1,
  WAIT_REASON_CLICKED = 2,
  WAIT_REASON_RECEIVE = 3,
  WAIT_REASON_RECEIVE_MISSION = 4,
  WAIT_REASON_REFRESH_MISSION = 5,
  WAIT_REASON_DO_NOT_WAITING = 6
}
function GameUIStarCraftCalendar:ctor(flash_obj)
  self.flash_obj = flash_obj
  self.mEventList = {}
  self.mSortedEventList = {}
  self.mStartTime = 0
  self.mEndTime = 0
  self.mCurActiveEvent = {}
  self.mEventBeginCallback = nil
  self.mEventEndCallback = nil
  self.mIsTimelineShow = false
  self.mEventStaredHandler = nil
  self.mEventEndedHandler = nil
end
function GameUIStarCraftCalendar:SetFlashObj(flash_obj)
  self.flash_obj = flash_obj
end
function GameUIStarCraftCalendar:InitWithContent(content, eventStartHandler, eventEndHandler)
  self.mEventStaredHandler = eventStartHandler
  self.mEventEndedHandler = eventEndHandler
  local currentTime = GameUIBarLeft:GetServerTime()
  self.mStartTime = content.begin_time
  self.mEndTime = content.end_time
  for k, v in pairs(content.events) do
    for kTime, vTime in pairs(v.time_span) do
      local event = StarCraftEvent:new()
      event:InitWithContent(v, vTime.begin_time + self.mStartTime, vTime.end_time + self.mStartTime)
      self.mEventList[event.mID] = event
      self.mSortedEventList[event.mID] = event
    end
  end
  self:SortEvents()
end
function GameUIStarCraftCalendar:SortEvents()
  local sortFunc = function(a, b)
    return a.mTimeStart < b.mTimeStart
  end
  table.sort(self.mSortedEventList, sortFunc)
end
function GameUIStarCraftCalendar:GetEventByID(id)
  return self.mEventList[id]
end
function GameUIStarCraftCalendar:HasActiveEvent()
  local has = false
  for k, v in pairs(self.mCurActiveEvent) do
    has = true
  end
  return has
end
function GameUIStarCraftCalendar:InitFlashTimeline()
  local eventIDs = ""
  local eventStartTimes = ""
  local eventEndTimes = ""
  local eventIconFrames = ""
  local currentTime = GameUIBarLeft:GetServerTime()
  local next5MinuteInterval = 300 - currentTime % 300
  DebugOut("refresh time line!!!")
  if -1 ~= GameUIStarCraftCalendarRefresher.mNextRefreshDateTime and currentTime < GameUIStarCraftCalendarRefresher.mNextRefreshDateTime then
    GameTimer:Add(GameUIStarCraftCalendarRefresher.RefreshTimelineTimer, 1000 * next5MinuteInterval)
    DebugOut("refresh time line return !!!")
    return false
  else
    DebugOut("refresh time line really init !!!")
    GameUIStarCraftCalendarRefresher.mNextRefreshDateTime = currentTime + next5MinuteInterval
    for k, v in pairs(self.mSortedEventList) do
      local iconFrame = v:GetIconFrame(currentTime)
      if iconFrame then
        eventIDs = eventIDs .. v.mID .. "\001"
        eventStartTimes = eventStartTimes .. v.mTimeStart .. "\001"
        eventEndTimes = eventEndTimes .. v.mTimeEnd .. "\001"
        eventIconFrames = eventIconFrames .. iconFrame .. "\001"
      end
    end
    if self.flash_obj then
      self.mIsTimelineShow = true
      GameUIStarCraftCalendarRefresher:SetTargetCalendar(self)
      self.flash_obj:InvokeASCallback("_root", "InitTimeLine", self.mStartTime, currentTime, self.mEndTime, eventIDs, eventStartTimes, eventEndTimes, eventIconFrames)
      GameTimer:Add(GameUIStarCraftCalendarRefresher.RefreshTimelineTimer, 1000 * next5MinuteInterval)
      self:SetCurrentDateAndTime()
    end
    self:SetCurrentActiveMission()
    return true
  end
end
function GameUIStarCraftCalendar:SetCurrentDateAndTime()
  local currentTime = GameUIBarLeft:GetServerTime()
  local currentTimeInDay = currentTime % 86400
  local timeStr = GameUtils:formatTimeString(currentTimeInDay)
  local date = ""
  self.flash_obj:InvokeASCallback("_root", "SetCurrentDateAndTime", date, timeStr)
  GameTimer:Add(GameUIStarCraftCalendarRefresher.RefreshTimelineCurrentTimeTimer, 1000)
end
function GameUIStarCraftCalendar:ShowTimeLine()
  self:UpdateCurrentEvents()
  GameUIStarCraftCalendarRefresher.mNextRefreshDateTime = -1
  if not self:UpdateActiveEvent() and self:InitFlashTimeline() and self.flash_obj then
    self.flash_obj:InvokeASCallback("_root", "ShowEventCalendar")
  end
end
function GameUIStarCraftCalendar:ForceCheckCurrentEventState(show)
  self:UpdateCurrentEvents()
  if show then
    GameUIStarCraftCalendarRefresher.mNextRefreshDateTime = -1
  end
  if self:InitFlashTimeline() and show and self.flash_obj then
    self.flash_obj:InvokeASCallback("_root", "ShowEventCalendar")
  end
end
function GameUIStarCraftCalendar:SetCurrentActiveMission()
  local hasActiveMission = false
  local eventIDS = ""
  local desc = ""
  local prgress = ""
  for k, v in pairs(self.mCurActiveEvent) do
    if v:IsMissionEvent() then
      v:GenerateMissionDetails()
      for iSubMission, vSubMission in ipairs(v.mMissionDetails) do
        eventIDS = eventIDS .. v.mID .. "\001"
        desc = desc .. vSubMission:GetMissionDescStr() .. "\001"
        prgress = prgress .. vSubMission:GetMissionStateStr() .. "\001"
      end
      hasActiveMission = true
    end
  end
  DebugOut("prgress = ", prgress)
  DebugOut("desc = ", desc)
  if self.flash_obj and hasActiveMission then
    self.flash_obj:InvokeASCallback("_root", "SetActivityBrief", eventIDS, desc, prgress)
  end
end
function GameUIStarCraftCalendar:UpdateCurrentEvents()
  local curTime = GameUIBarLeft:GetServerTime()
  for k, v in pairs(self.mEventList) do
    if curTime >= v.mTimeStart and curTime <= v.mTimeEnd then
      if not v.mActive then
        v.mActive = true
        self.mCurActiveEvent[v.mID] = v
        self:UpdateEventDetailState(v, false, WAIT_MISSION_REASON.WAIT_REASON_DO_NOT_WAITING)
        if self.mEventStaredHandler then
          self.mEventStaredHandler(v)
        end
      end
    elseif v.mActive then
      v.mActive = false
      self.mCurActiveEvent[v.mID] = nil
      if self.mEventEndedHandler then
        self.mEventEndedHandler(v)
      end
    end
  end
end
function GameUIStarCraftCalendar:UpdateMapEventState(event)
end
function GameUIStarCraftCalendar:UpdateEventDetailState(event, waiting, reason)
  if event.mActive and event:IsMissionEvent() then
    local extInfo = {}
    extInfo.reason = reason
    extInfo.eventId = event.mID
    StarCraftMissionTable:SetMissionStateFetchListner(self, extInfo)
    StarCraftMissionTable:RequestMissionDetails(event.mID, event:GetMissionIdList(), waiting)
  else
  end
end
function GameUIStarCraftCalendar:UpdateActiveEvent()
  local waitingFetchMission = false
  for k, v in pairs(self.mCurActiveEvent) do
    if v:IsMissionEvent() then
      self:UpdateEventDetailState(v, true, WAIT_MISSION_REASON.WAIT_REASON_ACTIVE)
      waitingFetchMission = true
    end
  end
  return waitingFetchMission
end
function GameUIStarCraftCalendar:FoeceCheckEventFinished(extInfo)
  StarCraftMissionTable:SetMissionStateFetchListner(nil, nil)
  if extInfo.reason == WAIT_MISSION_REASON.WAIT_REASON_ACTIVE then
    GameUIStarCraftCalendarRefresher.mNextRefreshDateTime = -1
    self:InitFlashTimeline()
    if self.flash_obj then
      self.flash_obj:InvokeASCallback("_root", "ShowEventCalendar")
    end
  elseif extInfo.reason == WAIT_MISSION_REASON.WAIT_REASON_CLICKED then
    self:SetMissionDetail(extInfo.eventId)
    self:ShowMissionDetail()
  elseif extInfo.reason == WAIT_MISSION_REASON.WAIT_REASON_RECEIVE_MISSION then
    DebugOut("receive finished!!!!")
    DebugTable(extInfo)
    if not extInfo.error then
      local tmpExtInfo = {}
      tmpExtInfo.reason = WAIT_MISSION_REASON.WAIT_REASON_REFRESH_MISSION
      tmpExtInfo.missionIds = {
        extInfo.missionID
      }
      tmpExtInfo.eventId = extInfo.eventId
      StarCraftMissionTable:SetMissionStateFetchListner(self, tmpExtInfo)
      StarCraftMissionTable:RequestMissionDetails(tmpExtInfo.eventId, tmpExtInfo.missionIds, true)
    end
  elseif extInfo.reason == WAIT_MISSION_REASON.WAIT_REASON_REFRESH_MISSION then
    self:SetMissionDetail(extInfo.eventId)
  elseif extInfo.reason == WAIT_MISSION_REASON.WAIT_REASON_DO_NOT_WAITING then
    self:SetCurrentActiveMission()
    local event = self:GetEventByID(extInfo.eventId)
    if self.mEventStaredHandler then
      self.mEventStaredHandler(event)
    end
  end
end
function GameUIStarCraftCalendar:OnClickedMission(eventID)
  local event = self:GetEventByID(eventID)
  local extInfo = {}
  extInfo.reason = WAIT_MISSION_REASON.WAIT_REASON_CLICKED
  extInfo.eventId = event.mID
  StarCraftMissionTable:SetMissionStateFetchListner(self, extInfo)
  StarCraftMissionTable:RequestMissionDetails(eventID, event:GetMissionIdList(), true)
end
function GameUIStarCraftCalendar:SetMissionDetail(eventID)
  local event = self:GetEventByID(eventID)
  local eventTitle = event:GetTitleStr()
  local eventDesc = event:GetDescStr()
  local remainTime = event:GetRemainTime(GameUIBarLeft:GetServerTime())
  event:GenerateMissionDetails()
  local subIDs = ""
  local desc = ""
  local prgress = ""
  local awardIconFrames = ""
  local awardNums = ""
  local awardBtnFrames = ""
  for iSubMission, vSubMission in ipairs(event.mMissionDetails) do
    subIDs = subIDs .. vSubMission.mID .. "\001"
    desc = desc .. vSubMission:GetMissionDescStr() .. "\001"
    prgress = prgress .. vSubMission:GetMissionStateStr() .. "\001"
    awardIconFrames = awardIconFrames .. vSubMission:GetAwardIcon(1) .. "\001"
    awardNums = awardNums .. vSubMission:GetAwardNum(1) .. "\001"
    awardBtnFrames = awardBtnFrames .. vSubMission:GetAwardReceiveFrame() .. "\001"
  end
  if self.flash_obj then
    self.flash_obj:InvokeASCallback("_root", "SetMissionRewardDetail", event.mID, eventTitle, eventDesc, remainTime, subIDs, desc, prgress, awardIconFrames, awardNums, awardBtnFrames)
  end
end
function GameUIStarCraftCalendar:ShowMissionDetail()
  if self.flash_obj then
    self.flash_obj:InvokeASCallback("_root", "ShowMissionRewardDetail")
  end
end
function GameUIStarCraftCalendar:ShowEventDetail(eventID)
  local event = self:GetEventByID(eventID)
  local eventTitle = event:GetTitleStr()
  local eventDesc = event:GetDescStr()
  local remainTime = event:GetRemainTime(GameUIBarLeft:GetServerTime())
  DebugOut("show event details")
  if self.flash_obj then
    self.flash_obj:InvokeASCallback("_root", "ShowMissionDetail", eventTitle, eventDesc, remainTime)
  end
end
function GameUIStarCraftCalendar:ReceiveMissionAwards(eventID, subMissionID)
  local missionInfo = StarCraftMissionTable:GetMissionByID(subMissionID)
  local extInfo = {}
  extInfo.reason = WAIT_MISSION_REASON.WAIT_REASON_RECEIVE_MISSION
  extInfo.eventId = eventID
  extInfo.missionID = subMissionID
  StarCraftMissionTable:SetMissionStateFetchListner(self, extInfo)
  StarCraftMissionTable:TryReceiveMissionAwards(eventID, subMissionID, missionInfo:GetCurrentStep())
end
function GameUIStarCraftCalendar:IsTimelineShow()
  return self.mIsTimelineShow
end
function GameUIStarCraftCalendar:OnFSCommand(cmd, arg)
  if cmd == "ClickEvent" then
    local eventID = tonumber(arg)
    local curEvent = self:GetEventByID(eventID)
    if curEvent:IsInTwoHours(GameUIBarLeft:GetServerTime()) then
      if self.mEventList[eventID]:IsMissionEvent() then
        self:OnClickedMission(tonumber(arg))
      else
        self:ShowEventDetail(tonumber(arg))
      end
    end
  elseif cmd == "CalendarClose" then
    self.mIsTimelineShow = false
  elseif cmd == "GetMission" then
    local eventID, subMissionID = arg:match("(%d+)\001(%d+)")
    self:ReceiveMissionAwards(tonumber(eventID), tonumber(subMissionID))
  end
end
