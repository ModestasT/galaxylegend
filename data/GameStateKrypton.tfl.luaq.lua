local GameStateKrypton = GameStateManager.GameStateKrypton
local GameStateMainPlanet = GameStateManager.GameStateMainPlanet
local GameObjectDragger = LuaObjectManager:GetLuaObject("GameObjectDragger")
local GameObjectDraggerItem = GameObjectDragger:NewInstance("bagItem.tfs", false)
local GameFleetInfoBackground = LuaObjectManager:GetLuaObject("GameFleetInfoBackground")
local GameCommonBackground = LuaObjectManager:GetLuaObject("GameCommonBackground")
local GameUIKrypton = LuaObjectManager:GetLuaObject("GameUIKrypton")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameUIBuilding = LuaObjectManager:GetLuaObject("GameUIBuilding")
GameStateKrypton.KRYPTON = 1
GameStateKrypton.LAB = 2
GameStateKrypton.ADV_LAB = 3
GameStateKrypton.buildingName = "krypton_center"
GameStateKrypton.dragItemOffset = -70
require("FleetMatrix.tfl")
function GameObjectDraggerItem:OnAddToGameState(state)
  if DynamicResDownloader:IsDynamicStuff(GameObjectDraggerItem.DraggedItemTYPE, DynamicResDownloader.resType.PIC) then
    local fullFileName = DynamicResDownloader:GetFullName(GameObjectDraggerItem.DraggedItemTYPE .. ".png", DynamicResDownloader.resType.PIC)
    local localPath = "data2/" .. fullFileName
    if DynamicResDownloader:IfResExsit(localPath) then
      ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
      frame = "item_" .. GameObjectDraggerItem.DraggedItemTYPE
    else
      frame = "temp"
    end
  else
    frame = "item_" .. GameObjectDraggerItem.DraggedItemTYPE
  end
  DebugOut("frame: ", frame)
  self:GetFlashObject():InvokeASCallback("_root", "SetBagItem", frame)
  self:GetFlashObject():InvokeASCallback("_root", "ChangeToBigger")
end
function GameObjectDraggerItem:OnEraseFromGameState(state)
  self.DraggedItemID = -1
  self.dragOnCharge = false
  self.dragOnFleet = false
  self.dragOnBag = false
  self.kryptonDestSlot = nil
end
function GameObjectDraggerItem:OnReachedDestPos()
  if self.DraggedItemID and self.DraggedItemID ~= -1 then
    if self.dragOnCharge then
      GameUIKrypton:SetCurrentChargeKrypton(self.DraggedItemID)
      GameStateKrypton:onEndDragItem()
    elseif self.dragOnFleet then
      GameUIKrypton.dragBagKrypton = false
      GameUIKrypton:RefreshCurFleetKrypton()
      if GameUIKrypton.dragFleetItem then
        GameUIKrypton:SwapFleetKryptonPos(self.DraggedItemID, self.kryptonDestSlot)
      elseif GameUIKrypton.dragBagItem then
        GameUIKrypton:EquipKrypton(self.DraggedItemID, self.kryptonDestSlot)
      end
    elseif self.dragOnBag then
      if GameUIKrypton.dragFleetItem then
        GameUIKrypton:EquipOffKrypton(self.DraggedItemID, self.kryptonDestSlot)
      elseif GameUIKrypton.dragBagItem then
        GameUIKrypton:SwapBagKryptonPos(self.DraggedItemID, self.kryptonDestSlot)
      end
    end
  else
    GameStateKrypton:onEndDragItem()
  end
end
function GameStateKrypton:onEndDragItem()
  GameUIKrypton:onEndDragItem()
  GameStateKrypton:EraseObject(GameObjectDraggerItem)
end
function GameStateKrypton:BeginDragItem(itemId, itemType, initPosX, initPosY, posX, posY)
  DebugOut("BeginDragItem: ", itemId, itemType, posX, posY)
  GameObjectDraggerItem.DraggedItemID = itemId
  GameObjectDraggerItem.DraggedItemTYPE = itemType
  GameObjectDraggerItem:SetDestPosition(initPosX, initPosY)
  GameObjectDraggerItem:BeginDrag(tonumber(self.dragItemOffset), tonumber(self.dragItemOffset), tonumber(posX), tonumber(posY))
  GameStateManager:GetCurrentGameState():AddObject(GameObjectDraggerItem)
  GameUIKrypton:onBeginDragItem(GameObjectDraggerItem)
end
function GameStateKrypton:OnTouchPressed(x, y)
  if GameStateKrypton:IsObjectInState(GameObjectDraggerItem) and GameObjectDraggerItem:IsAutoMove() or GameUIGlobalScreen.IsShowingLoading then
    return
  end
  GameStateBase.OnTouchPressed(self, x, y)
end
function GameStateKrypton:OnTouchMoved(x, y)
  if GameStateKrypton:IsObjectInState(GameObjectDraggerItem) and GameObjectDraggerItem:IsAutoMove() or GameUIGlobalScreen.IsShowingLoading then
    return
  end
  GameStateBase.OnTouchMoved(self, x, y)
  local dragItemId = GameObjectDraggerItem.DraggedItemID
  if dragItemId and dragItemId ~= -1 then
    GameObjectDraggerItem:UpdateDrag()
  end
end
function GameStateKrypton:OnTouchReleased(x, y)
  if GameStateKrypton:IsObjectInState(GameObjectDraggerItem) and GameObjectDraggerItem:IsAutoMove() or GameUIGlobalScreen.IsShowingLoading then
    return
  end
  GameStateBase.OnTouchReleased(self, x, y)
  local dragItemId = GameObjectDraggerItem.DraggedItemID
  if dragItemId and dragItemId ~= -1 then
    if GameUIKrypton.dragFleetItem or GameUIKrypton.dragBagItem then
      GameStateKrypton:DragKryptonRelease()
    end
    if not GameObjectDraggerItem.dragOnCharge and not GameObjectDraggerItem.dragOnFleet and not GameObjectDraggerItem.dragOnBag then
      GameObjectDraggerItem.DraggedItemID = -1
    end
    GameObjectDraggerItem:StartAutoMove()
  end
end
function GameStateKrypton:DragKryptonRelease()
  local posInfo = ""
  if GameUIKrypton:GetFlashObject():InvokeASCallback("_root", "IsOnChargeKrypton") == "true" then
    GameObjectDraggerItem.dragOnCharge = true
    posInfo = GameUIKrypton:GetFlashObject():InvokeASCallback("_root", "GetChargeKryptonPos")
  elseif GameUIKrypton:GetFlashObject():InvokeASCallback("_root", "IsOnFleetKrypton") == "true" then
    if GameUIKrypton.tab == GameStateKrypton.KRYPTON then
      GameObjectDraggerItem.dragOnFleet = true
      posInfo = GameUIKrypton:GetFlashObject():InvokeASCallback("_root", "GetFleetKryptonPos")
    end
  elseif GameUIKrypton:GetFlashObject():InvokeASCallback("_root", "IsOnBagKrypton") == "true" then
    if GameUIKrypton.dragBagItem then
      GameObjectDraggerItem.dragOnBag = false
      posInfo = ""
    else
      GameObjectDraggerItem.dragOnBag = true
      posInfo = GameUIKrypton:GetFlashObject():InvokeASCallback("_root", "GetBagKryptonPos")
    end
  end
  DebugOut("DragKryptonRelease: ", posInfo)
  if GameObjectDraggerItem.dragOnCharge or GameObjectDraggerItem.dragOnFleet or GameObjectDraggerItem.dragOnBag then
    local ptPos = LuaUtils:string_split(posInfo, "\001")
    GameObjectDraggerItem:SetDestPosition(tonumber(ptPos[1]), tonumber(ptPos[2]))
    GameObjectDraggerItem.kryptonDestSlot = tonumber(ptPos[3])
  end
end
function GameStateKrypton:InitGameState()
  self.currentTab = -1
end
function GameStateKrypton:OnFocusGain(previousState)
  self.previousState = previousState
  GameObjectDraggerItem:Init()
  GameObjectDraggerItem:LoadFlashObject()
  GameUIKrypton:LoadFlashObject()
  GameUIKrypton.registerDatacallback()
end
function GameStateKrypton:OnFocusLost(newState)
  GameUIKrypton.removeDatacallback()
  GameObjectDraggerItem.m_renderFx = nil
  self.currentTab = -1
  self:EraseAllObject()
  FleetMatrix.system_index = nil
end
function GameStateKrypton:EraseAllObject()
  self:EraseObject(GameCommonBackground)
  self:EraseObject(GameFleetInfoBackground)
  self:EraseObject(GameUIKrypton)
  GameUIKrypton:Clear()
  GameUIKrypton:UnloadFlashObject()
end
function GameStateKrypton:EnterKrypton(tabId)
  local building = GameGlobalData:GetBuildingInfo(self.buildingName)
  if building and building.level > 0 then
    if GameStateManager:GetCurrentGameState() ~= self then
      GameStateManager:SetCurrentGameState(self)
      tabId = tabId or GameStateKrypton.KRYPTON
      GameUIKrypton.tab = tabId
      self:OnChange(self.currentTab, tabId)
    elseif self.currentTab ~= tabId then
      self:OnChange(self.currentTab, tabId)
    end
  elseif building and 0 < building.status then
    if GameStateManager:GetCurrentGameState() ~= GameStateMainPlanet then
      GameStateManager:SetCurrentGameState(GameStateMainPlanet)
    end
    GameUIBuilding:DisplayUpgradeDialog(self.buildingName)
  end
end
function GameStateKrypton:OnChange(pre, cur)
  self.currentTab = cur
  DebugOut("GameStateKrypton:OnChange: ", pre, cur)
  if pre ~= -1 then
    self:EraseObject(GameCommonBackground)
    if pre == GameStateKrypton.KRYPTON then
      self:EraseObject(GameFleetInfoBackground)
      self:EraseObject(GameUIKrypton)
    elseif pre == GameStateKrypton.LAB or pre == GameStateKrypton.ADV_LAB then
      self:EraseObject(GameUIKrypton)
    end
  end
  if cur ~= -1 then
    self:AddObject(GameCommonBackground)
    if cur == GameStateKrypton.KRYPTON then
      self:AddObject(GameFleetInfoBackground)
      self:AddObject(GameUIKrypton)
    elseif cur == GameStateKrypton.LAB or cur == GameStateKrypton.ADV_LAB then
      self:AddObject(GameUIKrypton)
    end
  end
  self:ForceCompleteCammandList()
end
