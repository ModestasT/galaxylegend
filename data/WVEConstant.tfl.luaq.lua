EWVEPointType = {
  TYPE_PLAYER_BASE = 1,
  TYPE_ENEMY_BASE = 2,
  TYPE_NORMAL = 3
}
EWVEBuffType = {
  Type_Normal = 0,
  Type_Camp = 1,
  Type_Target = 2,
  Type_White_Hole = 3,
  Type_Boat_Yard = 4,
  Type_Missile = 5,
  Type_Point_Level_1 = 10,
  Type_Point_Level_2 = 11,
  Type_Point_Level_3 = 12
}
EWVEGameState = {
  STATE_ENTER_TUTORIAL = 0,
  STATE_ENTER_GAME = 1,
  STATE_BACK_TOGAME = 2,
  STATE_IN_GAME = 3,
  STATE_ROUND_END = 4,
  STATE_GAME_END = 5
}
EWVEPlayerState = {
  STATE_PLAYER_STAYINPOINT = 0,
  STATE_PLAYER_WAITMARCH = 1,
  STATE_PLAYER_MARCH = 2,
  STATE_PLAYER_DIE = 3,
  STATE_PLAYER_UNKNOW = 4
}
EWVEPlayerType = {
  TYPE_PLAYER = 1,
  TYPE_ENEMY = 2,
  TYPE_MYSELF = 3
}
EWVEPlayerMarchStep = {
  STEP_MARCH = 1,
  STEP_INBATTLE = 2,
  STEP_MARCH_END = 3
}
EWVEPlayerTypeFrame = {
  [1] = "green",
  [2] = "red",
  [3] = "blue"
}
EWVEPointOccupyStatus = {
  OCCUPY_STATE_PLAYERS = 1,
  OCCUPY_STATE_ENEMY = 2,
  OCCUPY_STATE_FREE = 3,
  OCCUPY_STATE_MYSELF = 4
}
EWVEPointOccupyStatusFrame = {
  [1] = "blue",
  [2] = "red",
  [3] = "nom",
  [4] = "green"
}
EWVEPointBattleStatus = {POINT_PEACE = 0, POINT_INBATTLE = 1}
EWVEINFOPANEL_TYPE = {
  PANEL_POINT = 1,
  PANEL_ENEMYPOINT = 2,
  PANEL_ENEMYINFO = 3,
  PANEL_POWERUP = 4,
  PANEL_DIE = 5,
  PANEL_LEADERBOARD = 6,
  PANEL_REWARD = 7
}
