require("FleetMatrix.tfl")
require("GameTextEdit.tfl")
local GameStatePlayerMatrix = GameStateManager.GameStatePlayerMatrix
local GameObjectPlayerMatrix = LuaObjectManager:GetLuaObject("GameObjectPlayerMatrix")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameUIAdjutant = LuaObjectManager:GetLuaObject("GameUIAdjutant")
local GameStateTacticsCenter = GameStateManager.GameStateTacticsCenter
local RefineDataManager = LuaObjectManager:GetLuaObject("RefineDataManager")
local QuestTutorialTacticsCenter = TutorialQuestManager.QuestTutorialTacticsCenter
local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
local GameUIWDStuff = LuaObjectManager:GetLuaObject("GameUIWDStuff")
local GameUIMaster = LuaObjectManager:GetLuaObject("GameUIMaster")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
GameObjectPlayerMatrix.battle_matrix = {}
GameObjectPlayerMatrix.rest_matrix = {}
GameObjectPlayerMatrix.m_nextPlayerMatrix = nil
GameObjectPlayerMatrix.m_nextPlayerRestFleets = nil
GameObjectPlayerMatrix.mCurFreeMatrixSubIdx = 0
GameObjectPlayerMatrix.curMainFleetIndex = 1
GameObjectPlayerMatrix.mainFleetList = {}
local currentMatrixIndex = 1
local maxAvailableMatrix = 1
local haveRequestMatrixInfo = false
local lastMatrixIndex = 1
GameObjectPlayerMatrix.curFleetId = 1
local CommanderRanks = {
  [1] = "rank_E",
  [2] = "rank_D",
  [3] = "rank_C",
  [4] = "rank_B",
  [5] = "rank_A",
  [6] = "rank_S",
  [7] = "rank_SS"
}
function GameObjectPlayerMatrix.GetMatrixsAck()
  GameObjectPlayerMatrix.battle_matrix = {}
  for _, v in ipairs(GameGlobalData:GetData("matrix").cells) do
    GameObjectPlayerMatrix.battle_matrix[v.cell_id] = v.fleet_identity
  end
  GameObjectPlayerMatrix.requestMultiMatrixCallBack(NetAPIList.mulmatrix_get_ack.Code, FleetMatrix)
  local flash_obj = GameObjectPlayerMatrix:GetFlashObject()
  if flash_obj then
    flash_obj:InvokeASCallback("_root", "setCurrentMatrix", FleetMatrix:getCurrentMartix().name)
  end
  GameObjectPlayerMatrix:UpdateTotalBattleForce(GameObjectPlayerMatrix:ComputeBattleForce())
end
function GameObjectPlayerMatrix:OnAddToGameState(new_parent)
  DebugOut("OnAddToGameState")
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  GameObjectPlayerMatrix.mSaveFormationToServerSuccessCallback = nil
  GameObjectPlayerMatrix.mCurFreeMatrixSubIdx = 0
  currentMatrixIndex = GameGlobalData.GlobalData.curMatrixIndex
  local leaderdata = GameGlobalData:GetData("leaderlist")
  local curr_id = leaderdata.leader_ids[currentMatrixIndex]
  self.mainFleetList = leaderdata.options
  self.curMainFleetIndex = 1
  for k, v in ipairs(self.mainFleetList) do
    if v == curr_id then
      self.curMainFleetIndex = k
      break
    end
  end
  if not haveRequestMatrixInfo then
    FleetMatrix:GetMatrixsReq(GameObjectPlayerMatrix.GetMatrixsAck)
  end
  if RefineDataManager:GetIsOpenTacticsCenter() == 1 and GameUtils:IsModuleUnlock("centalsystem") then
    if self:GetFlashObject() then
      self:GetFlashObject():InvokeASCallback("_root", "showTacticsCenter")
      self:GetFlashObject():InvokeASCallback("_root", "hideTutoiralTacticsCenter")
      if QuestTutorialTacticsCenter:IsActive() then
        DebugOut("showTutoiralTacticsCenter")
        self:GetFlashObject():InvokeASCallback("_root", "showTutoiralTacticsCenter")
      end
    end
  elseif self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "hideTacticsCenter")
  end
  self:RefreshRedPoint()
end
function GameObjectPlayerMatrix.OnEditHidden()
  if GameObjectPlayerMatrix:GetFlashObject() then
    GameObjectPlayerMatrix:GetFlashObject():InvokeASCallback("_root", "hideInputBox")
  end
end
function GameObjectPlayerMatrix:InitFormation()
  self.battle_matrix = {}
  self.rest_matrix = {}
  local battle_commanders = {}
  for i = 1, 9 do
    self.battle_matrix[i] = 0
  end
  for _, v in ipairs(GameGlobalData:GetData("matrix").cells) do
    self.battle_matrix[v.cell_id] = v.fleet_identity
    if 0 < v.fleet_identity then
      battle_commanders[v.fleet_identity] = true
    end
  end
  for _, commander_data in ipairs(GameGlobalData:GetData("fleetinfo").fleets) do
    if not battle_commanders[commander_data.identity] then
      table.insert(self.rest_matrix, commander_data.identity)
    end
  end
  for i = 1, 9 do
    if not self.rest_matrix[i] then
      self.rest_matrix[i] = -1
    end
  end
end
function GameObjectPlayerMatrix:initUI()
  local levelinfo = GameGlobalData:GetData("levelinfo")
  self:UpdatePlayerInfo(levelinfo.level)
  local buildingTable = GameGlobalData:GetData("buildings").buildings
  for _, buildingInfo in ipairs(buildingTable) do
    if buildingInfo.name == "star_portal" then
      if buildingInfo.level > 0 then
        self:GetFlashObject():InvokeASCallback("_root", "enableBtnRecruit", true)
        break
      end
      self:GetFlashObject():InvokeASCallback("_root", "enableBtnRecruit", false)
      break
    end
  end
  self:AnimationMoveIn()
end
function GameObjectPlayerMatrix:OnEraseFromGameState(old_parent)
  self:UnloadFlashObject()
  GameObjectPlayerMatrix:clearMultiMatrixData()
  FleetMatrix.system_index = nil
end
function GameObjectPlayerMatrix:UpdatePlayerInfo(player_level)
  self:GetFlashObject():InvokeASCallback("_root", "updatePlayerInfo", player_level, GameLoader:GetGameText("LC_MENU_Level"))
end
function GameObjectPlayerMatrix:ComputeBattleForce()
  DebugOutPutTable(GameGlobalData:GetData("fleetinfo").fleets, "all fleets")
  DebugOutPutTable(self.battle_matrix, "all battle_matrix")
  local force_value = 0
  for _, commander_id in pairs(self.battle_matrix) do
    if commander_id > 0 then
      local commander_data = GameGlobalData:GetFleetInfo(commander_id)
      force_value = force_value + commander_data.force
    end
  end
  DebugOut("ComputeBattleForce", force_value)
  return force_value
end
function GameObjectPlayerMatrix:UpdateTotalBattleForce(total_force)
  self:GetFlashObject():InvokeASCallback("_root", "updateBattleCommandersForce", GameUtils.numberAddComma(total_force))
end
function GameObjectPlayerMatrix:GetCommanderGrid(commander_id)
  DebugOut("GetCommanderGrid", commander_id)
  for grid_index, grid_data in pairs(self.battle_matrix) do
    if grid_data == commander_id then
      return "battle", grid_index
    end
  end
  for grid_index, grid_data in pairs(self.rest_matrix) do
    if grid_data == commander_id then
      return "rest", grid_index
    end
  end
  return -1, -1
end
function GameObjectPlayerMatrix:GetCommanderIDWithGrid(grid_type, grid_index)
  local grid_table
  if grid_type == "battle" then
    grid_table = self.battle_matrix
  elseif grid_type == "rest" then
    grid_table = self.rest_matrix
  elseif grid_type == "yours_L" or grid_type == "yours_R" then
    grid_table = self.m_nextPlayerMatrix
  elseif grid_type == "rest_list_L" or grid_type == "rest_list_R" or grid_type == "rest_list_L2" or grid_type == "rest_list_R2" then
    grid_table = self.m_nextPlayerRestFleets
  else
    assert(false)
  end
  local id = -1
  DebugOut("GetCommanderIDWithGrid:", grid_type, grid_index)
  DebugTable(grid_table)
  if grid_table[grid_index] ~= nil then
    id = grid_table[grid_index]
  end
  return id
end
function GameObjectPlayerMatrix:GetCommanderMatrixWithType(matrix_type)
  if matrix_type == "battle" then
    return self.battle_matrix
  elseif matrix_type == "rest" then
    return self.rest_matrix
  else
    assert(false)
  end
end
function GameObjectPlayerMatrix:CountBattleCommander()
  local count = 0
  for index, data in pairs(self.battle_matrix) do
    if data > 0 then
      count = count + 1
    end
  end
  return count
end
function GameObjectPlayerMatrix:UpdateCommanderItem(grid_type, grid_index)
  DebugOut("UpdateCommanderItem:", grid_type, ",", grid_index)
  local commander_id = self:GetCommanderIDWithGrid(grid_type, grid_index)
  local tempid = commander_id
  local commanderType = -1
  local commander_identity = -1
  local commander_avatar = -1
  local commander_vessels = -1
  local commander_vesselstype = -1
  local commander_level = 0
  local commander_color = "blue"
  local commander_sex = "unknown"
  local commander_data = GameGlobalData:GetFleetInfo(commander_id)
  if commander_id == 1 then
    commander_id = GameGlobalData:GetData("leaderlist").leader_ids[currentMatrixIndex] or 1
    DebugOut("1111111111")
    DebugTable(GameGlobalData:GetData("leaderlist").leader_ids)
    DebugOut("currentMatrixIndex", currentMatrixIndex)
  end
  if commander_data == nil then
    if tempid == 1 then
      commander_data = {}
      commander_data.identity = 1
    end
    DebugOut("error:id_commander")
  else
    commander_level = commander_data.level
  end
  DebugOut("GameObjectPlayerMatrix:UpdateCommanderItem")
  DebugOut("commander_id: ", commander_id)
  DebugOut("commander_level: ", commander_level)
  if commander_id > 0 and commander_data ~= nil then
    commander_identity = commander_data.identity
    commanderType = GameGlobalData:GetCommanderTypeWithIdentity(commander_identity)
    commander_avatar = GameDataAccessHelper:GetFleetAvatar(commander_id, commander_level)
    commander_vessels = GameDataAccessHelper:GetCommanderVesselsImage(commander_id, commander_level, false)
    commander_vesselstype = GameDataAccessHelper:GetCommanderVesselsType(commander_id, commander_level)
    commander_color = GameDataAccessHelper:GetCommanderColorFrame(commander_id, commander_level)
    commander_sex = GameDataAccessHelper:GetCommanderSex(commander_id)
    if commander_sex == 1 then
      commander_sex = "man"
    elseif commander_sex == 0 then
      commander_sex = "woman"
    else
      commander_sex = "unknown"
    end
  end
  local dataTable = {}
  dataTable[#dataTable + 1] = grid_type
  dataTable[#dataTable + 1] = grid_index
  dataTable[#dataTable + 1] = commanderType
  dataTable[#dataTable + 1] = tempid
  dataTable[#dataTable + 1] = commander_vessels
  dataTable[#dataTable + 1] = commander_avatar
  dataTable[#dataTable + 1] = commander_vesselstype
  dataTable[#dataTable + 1] = commander_color
  dataTable[#dataTable + 1] = commander_sex
  local hasAdjutant = "false"
  if commander_data and commander_data.cur_adjutant and 0 < commander_data.cur_adjutant then
    hasAdjutant = "true"
  end
  dataTable[#dataTable + 1] = hasAdjutant
  local dataString = table.concat(dataTable, ",")
  DebugOut("datastring:", dataString)
  self:GetFlashObject():InvokeASCallback("_root", "updateCommanderItem", dataString)
  if grid_type == "battle" then
    GameObjectPlayerMatrix:UpdateAdjutantBar()
  end
end
function GameObjectPlayerMatrix:GetCommanderLocatedInfo()
  local located_type, located_pos
  local located_info = self:GetFlashObject():InvokeASCallback("_root", "getCommanderLocatedInfo")
  if located_info then
    local param_list = LuaUtils:string_split(located_info, ",")
    located_type = param_list[1]
    located_pos = param_list[2]
  end
  return located_type, located_pos
end
function GameObjectPlayerMatrix:GetCommanderGridPos(grid_type, grid_index)
  local grid_pos
  local pos_info = self:GetFlashObject():InvokeASCallback("_root", "getCommanderGridPos", grid_type, grid_index)
  DebugOut(pos_info)
  if pos_info then
    local param_list = LuaUtils:string_split(pos_info, ",")
    grid_pos = {}
    grid_pos._x = tonumber(param_list[1])
    grid_pos._y = tonumber(param_list[2])
  end
  return grid_pos
end
function GameObjectPlayerMatrix:OnBeginDragCommander(vessels_dragger)
  local draggedFleedID = vessels_dragger.DraggedFleetID
  local grid_type, grid_index = self:GetCommanderGrid(draggedFleedID)
  if grid_type == "rest" or grid_type == "battle" then
    vessels_dragger.DraggedFleetGridType = grid_type
    vessels_dragger.DraggedFleetGridIndex = grid_index
    local commander_matrix = self:GetCommanderMatrixWithType(grid_type)
    commander_matrix[grid_index] = -1
    self:UpdateCommanderItem(grid_type, grid_index)
  else
    assert(false)
  end
end
function GameObjectPlayerMatrix:UnExpandSkillSelector(commander_id)
  local active_spell = FleetMatrix:GetActiviteSpell(commander_id)
  DebugOut("ReExpandSkillSelector:" .. active_spell)
  self:SelectCommanderSkill(commander_id, active_spell)
end
function GameObjectPlayerMatrix:ExpandSkillSelector(commander_id)
  DebugOut("GameObjectPlayerMatrix ExpandSkillSelector", commander_id)
  local grid_type, index_commander = self:GetCommanderGrid(commander_id)
  if grid_type == "rest" then
    return
  end
  local flash_obj = self:GetFlashObject()
  local commander_data = GameGlobalData:GetFleetInfo(commander_id)
  local active_spell = FleetMatrix:GetActiviteSpell(commander_id)
  local num_max_skill = GameGlobalData:GetNumMaxSkills()
  self:GetFlashObject():InvokeASCallback("_root", "expandSkillSelector", index_commander)
  DebugOut("active_spell: ", commander_data.active_spell)
  for index_skill = 1, num_max_skill do
    local id_skill = commander_data.spells[index_skill]
    DebugOut("id_skill: ", id_skill)
    id_skill = id_skill or -1
    local is_active = id_skill == active_spell
    flash_obj:InvokeASCallback("_root", "updateCommanderSkillItem", index_commander, index_skill, id_skill, false)
    if is_active then
      self:SelectCommanderSkill(commander_id, id_skill)
    end
  end
end
function GameObjectPlayerMatrix:ShowMainFleetSelector(commander_id)
  local grid_type, index_commander = self:GetCommanderGrid(commander_id)
  if grid_type == "rest" then
    return
  end
  local flash_obj = self:GetFlashObject()
  local commander_data = GameGlobalData:GetFleetInfo(commander_id)
  local num_max_skill = GameGlobalData:GetNumMaxSkills()
  self.curMainFleetIndex = 1
  local leaderdata = GameGlobalData:GetData("leaderlist")
  DebugOut("hello")
  DebugTable(leaderdata)
  local curr_id = leaderdata.leader_ids[currentMatrixIndex]
  self.mainFleetList = leaderdata.options
  DebugOut("leaderlist")
  DebugTable(self.mainFleetList)
  local param = {}
  for k, v in ipairs(self.mainFleetList) do
    local param1 = {}
    local player_main_fleet = GameGlobalData:GetFleetInfo(v)
    if v == curr_id then
      self.curMainFleetIndex = k
    end
    param1.avatar = GameDataAccessHelper:GetFleetAvatar(v, 0)
    table.insert(param, param1)
  end
  DebugOut("index: ", self.curMainFleetIndex)
  flash_obj:InvokeASCallback("_root", "ShowMainFleetSelector", index_commander, param, self.curMainFleetIndex)
  DebugOut("hello:tp")
  self:SelectCommanderNew(self.curMainFleetIndex)
  self:showMainFleetSkillSelector(self.curMainFleetIndex)
end
function GameObjectPlayerMatrix:setMainFleet()
  local leaderdata = GameGlobalData:GetData("leaderlist")
  local curr_id = leaderdata.leader_ids[currentMatrixIndex]
  if curr_id ~= nil and (curr_id == self.mainFleetList[self.curMainFleetIndex] or self.mainFleetList[self.curMainFleetIndex] == nil) then
    DebugOut("nil value")
    return
  end
  DebugOut("send:", leaderdata.leader_ids[currentMatrixIndex], "local ", self.mainFleetList[self.curMainFleetIndex])
  local param = {}
  param.id = self.mainFleetList[self.curMainFleetIndex]
  NetMessageMgr:SendMsg(NetAPIList.change_leader_req.Code, param, GameObjectPlayerMatrix.ChangeLeaderCallback, false)
end
function GameObjectPlayerMatrix.ChangeLeaderCallback(msgType, content)
  DebugOut("hangeLeaderCallback")
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.change_leader_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    else
      local fleet_id = GameObjectPlayerMatrix.mainFleetList[GameObjectPlayerMatrix.curMainFleetIndex]
      local grid_type, index_commander = GameObjectPlayerMatrix:GetCommanderGrid(1)
      GameObjectPlayerMatrix:changeMainfleetIcon(index_commander, grid_type, fleet_id)
    end
    return true
  end
  return false
end
function GameObjectPlayerMatrix:changeMainfleetIcon(grid_index, grid_type, id_i)
  DebugOut("changeMainfleetIcon:", grid_type, ",", grid_index)
  local commander_id = id_i
  local commanderType = -1
  local commander_identity = -1
  local commander_avatar = -1
  local commander_vessels = -1
  local commander_vesselstype = -1
  local commander_level = 0
  local commander_color = "blue"
  local commander_sex = "unknown"
  local commander_data = GameGlobalData:GetFleetInfo(1)
  if commander_data == nil then
    DebugOut("error:id_commander")
  else
    commander_level = commander_data.level
  end
  DebugOut("GameObjectPlayerMatrix:UpdateCommanderItem")
  DebugOut("commander_id: ", commander_id)
  DebugOut("commander_level: ", commander_level)
  if commander_id > 0 and commander_data ~= nil then
    commander_identity = id_i
    commanderType = GameGlobalData:GetCommanderTypeWithIdentity(commander_identity)
    commander_avatar = GameDataAccessHelper:GetFleetAvatar(commander_id, commander_level)
    commander_vessels = GameDataAccessHelper:GetCommanderVesselsImage(commander_id, commander_level, false)
    commander_vesselstype = GameDataAccessHelper:GetCommanderVesselsType(commander_id, commander_level)
    commander_color = GameDataAccessHelper:GetCommanderColorFrame(commander_id, commander_level)
    commander_sex = GameDataAccessHelper:GetCommanderSex(commander_id)
    if commander_sex == 1 then
      commander_sex = "man"
    elseif commander_sex == 0 then
      commander_sex = "woman"
    else
      commander_sex = "unknown"
    end
  end
  local dataTable = {}
  dataTable[#dataTable + 1] = grid_type
  dataTable[#dataTable + 1] = grid_index
  dataTable[#dataTable + 1] = commanderType
  dataTable[#dataTable + 1] = 1
  dataTable[#dataTable + 1] = commander_vessels
  dataTable[#dataTable + 1] = commander_avatar
  dataTable[#dataTable + 1] = commander_vesselstype
  dataTable[#dataTable + 1] = commander_color
  dataTable[#dataTable + 1] = commander_sex
  local hasAdjutant = "false"
  if commander_data and commander_data.cur_adjutant and 0 < commander_data.cur_adjutant then
    hasAdjutant = "true"
  end
  dataTable[#dataTable + 1] = hasAdjutant
  local dataString = table.concat(dataTable, ",")
  DebugOut("dataString", dataString)
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "updateCommanderItem", dataString)
  end
  GameObjectPlayerMatrix:SelectCommander(1)
  if grid_type == "battle" then
    GameObjectPlayerMatrix:UpdateAdjutantBar()
  end
end
function GameObjectPlayerMatrix:showMainFleetSkillSelector(fleet_index)
  local iteminfo = self.mainFleetList[fleet_index]
  local grid_type, index_commander = self:GetCommanderGrid(1)
  if grid_type == "rest" then
    return
  end
  local commander_info = GameGlobalData:GetFleetInfo(1)
  local commander_level = 0
  local leaderlist = GameGlobalData:GetData("leaderlist")
  if commander_info then
    commander_level = commander_info.level
  end
  local basic_info = GameDataAccessHelper:GetCommanderBasicInfo(iteminfo, commander_level)
  local flash_obj = self:GetFlashObject()
  DebugOut("GameObjectPlayerMatrix:showMainFleetSkillSelector:", fleet_index, iteminfo)
  if iteminfo == 1 then
    local commander_data = GameGlobalData:GetFleetInfo(iteminfo)
    DebugOut("fleetTable")
    DebugTable(GameGlobalData.GlobalData.fleetinfo.fleets)
    DebugOut("why nil?")
    DebugTable(commander_data)
    local active_spell = FleetMatrix:GetActiviteSpell(iteminfo)
    local num_max_skill = GameGlobalData:GetNumMaxSkills()
    flash_obj:InvokeASCallback("_root", "expandSkillSelectorNew", fleet_index, iteminfo, "skill_name", "skill_desc")
    DebugOut("active_spell: ", commander_data.active_spell)
    local leaderlist = GameGlobalData:GetData("leaderlist")
    local curSpells = leaderlist.active_spells or {}
    for index_skill = 1, num_max_skill do
      local id_skill = curSpells[index_skill]
      DebugOut("id_skill: ", id_skill)
      id_skill = id_skill or -1
      local is_active = id_skill == active_spell
      flash_obj:InvokeASCallback("_root", "updateCommanderSkillItem", index_commander, index_skill, id_skill, false)
      if is_active then
        self:SelectCommanderSkill(iteminfo, id_skill)
      end
    end
  else
    local skill = basic_info.SPELL_ID
    local datatable = {}
    datatable[#datatable + 1] = GameDataAccessHelper:GetSkillNameText(skill)
    datatable[#datatable + 1] = GameDataAccessHelper:GetSkillDesc(skill)
    local rangetype = GameDataAccessHelper:GetSkillRangeType(skill)
    datatable[#datatable + 1] = "TYPE_" .. tostring(rangetype)
    datatable[#datatable + 1] = GameLoader:GetGameText("LC_MENU_SKILL_RANGE_TYPE_" .. tostring(rangetype))
    local datastring = table.concat(datatable, "\001")
    DebugOut("datastring", datastring)
    self:GetFlashObject():InvokeASCallback("_root", "shrinkSkillSelectorNew")
    self:GetFlashObject():InvokeASCallback("_root", "selectCommanderSkillNew", fleet_index, datastring, index_commander)
  end
end
function GameObjectPlayerMatrix:SelectCommanderNew(fleet_index)
  local commander_info = GameGlobalData:GetFleetInfo(1)
  local commander_level = 0
  local leaderlist = GameGlobalData:GetData("leaderlist")
  if commander_info then
    commander_level = commander_info.level
  end
  local commander_force = GameGlobalData:GetFleetInfo(1).force or 0
  local commander_name = ""
  local commander_avatar = ""
  local commander_vessels = ""
  local commander_color = ""
  local commander_vesselstype = ""
  local forceContext = GameLoader:GetGameText("LC_MENU_ARENA_CAPTION_FORCE")
  local basic_info = {}
  local player_info = GameGlobalData:GetData("userinfo")
  commander_name = GameUtils:GetUserDisplayName(player_info.name)
  commander_name = GameDataAccessHelper:CreateLevelDisplayName(commander_name, commander_level)
  local commanderType = -1
  commander_avatar = GameDataAccessHelper:GetFleetAvatar(self.mainFleetList[fleet_index], commander_level)
  commander_vesselstype = GameDataAccessHelper:GetCommanderVesselsType(self.mainFleetList[fleet_index], commander_level)
  commander_vessels = GameDataAccessHelper:GetCommanderVesselsImage(self.mainFleetList[fleet_index], commander_level, false)
  commander_color = GameDataAccessHelper:GetCommanderColorFrame(self.mainFleetList[fleet_index], commander_level)
  basic_info = GameDataAccessHelper:GetCommanderBasicInfo(self.mainFleetList[fleet_index], commander_level)
  local skill = basic_info.SPELL_ID
  local skillIconFram = "TYPE_" .. GameDataAccessHelper:GetSkillRangeType(skill)
  if self.mainFleetList[fleet_index] == 1 then
    local commander_data = GameGlobalData:GetFleetInfo(1)
    local num_max_skill = GameGlobalData:GetNumMaxSkills()
    for index_skill = 1, num_max_skill do
      local id_skill = commander_data.spells[index_skill]
      id_skill = id_skill or -1
      local is_active = id_skill == commander_data.active_spell
      if is_active then
        skillIconFram = "TYPE_" .. GameDataAccessHelper:GetSkillRangeType(id_skill)
      end
    end
  end
  local ability = GameDataAccessHelper:GetCommanderAbility(self.mainFleetList[fleet_index], commander_level)
  local sex = GameDataAccessHelper:GetCommanderSex(self.mainFleetList[fleet_index])
  if commander_id == 1 and leaderlist.leader_ids[currentMatrixIndex] ~= 1 and leaderlist.leader_ids[currentMatrixIndex] ~= nil then
    ability = GameDataAccessHelper:GetCommanderAbility(self.mainFleetList[fleet_index], 0)
    sex = GameDataAccessHelper:GetCommanderSex(self.mainFleetList[fleet_index])
  end
  local fleetRank = GameUtils:GetFleetRankFrame(ability.Rank, GameObjectPlayerMatrix.DownloadRankImageCallback)
  commanderType = GameGlobalData:GetCommanderTypeWithIdentity(self.mainFleetList[fleet_index])
  if sex == 1 then
    sex = "man"
  elseif sex == 0 then
    sex = "woman"
  else
    sex = "unknown"
  end
  local grid_type, grid_index = GameObjectPlayerMatrix:GetCommanderGrid(1)
  local dataTable = {}
  dataTable[#dataTable + 1] = grid_type
  dataTable[#dataTable + 1] = grid_index
  dataTable[#dataTable + 1] = commanderType
  dataTable[#dataTable + 1] = 1
  dataTable[#dataTable + 1] = commander_vessels
  dataTable[#dataTable + 1] = commander_avatar
  dataTable[#dataTable + 1] = commander_vesselstype
  dataTable[#dataTable + 1] = commander_color
  dataTable[#dataTable + 1] = sex
  local hasAdjutant = "false"
  if commander_info and commander_info.cur_adjutant and 0 < commander_info.cur_adjutant then
    hasAdjutant = "true"
  end
  dataTable[#dataTable + 1] = hasAdjutant
  local dataString = table.concat(dataTable, ",")
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "selectCommanderItem", matrix_type, pos_index, commander_name, commander_identity, commander_avatar, commander_vesselstype, GameUtils.numberAddComma(commander_force), commander_color, forceContext, skillIconFram, fleetRank, sex, GameLoader:GetGameText("LC_MENU_Level"))
    self:GetFlashObject():InvokeASCallback("_root", "updateCommanderItem", dataString)
  end
end
function GameObjectPlayerMatrix:SelectCommander(commander_id)
  DebugOut("SelectCommander", commander_id)
  GameObjectPlayerMatrix.curFleetId = commander_id
  local matrix_type, pos_index = self:GetCommanderGrid(commander_id)
  local commander_info = GameGlobalData:GetFleetInfo(commander_id)
  local commander_level = 0
  local leaderlist = GameGlobalData:GetData("leaderlist")
  if commander_info then
    commander_level = commander_info.level
  end
  local commander_name = ""
  local commander_avatar = ""
  if commander_id == 1 then
    local player_info = GameGlobalData:GetData("userinfo")
    commander_name = GameUtils:GetUserDisplayName(player_info.name)
    commander_name = GameDataAccessHelper:CreateLevelDisplayName(commander_name, commander_level)
    commander_avatar = GameUtils:GetPlayerAvatar(nil, commander_level)
    if leaderlist.leader_ids[currentMatrixIndex] ~= 1 and leaderlist.leader_ids[currentMatrixIndex] ~= nil then
      commander_avatar = GameDataAccessHelper:GetFleetAvatar(leaderlist.leader_ids[currentMatrixIndex], commander_level)
    end
  else
    commander_name = GameLoader:GetGameText("LC_NPC_" .. GameDataAccessHelper:GetCommanderName(commander_id, commander_level))
    commander_name = GameDataAccessHelper:CreateLevelDisplayName(commander_name, commander_level)
    commander_avatar = GameDataAccessHelper:GetFleetAvatar(commander_id, commander_level)
  end
  local commander_vesselstype = GameDataAccessHelper:GetCommanderVesselsType(commander_id, commander_level)
  local commander_force = commander_info.force
  local commander_identity = commander_info.identity
  local commander_color = GameDataAccessHelper:GetCommanderColorFrame(commander_id, commander_level)
  local forceContext = GameLoader:GetGameText("LC_MENU_ARENA_CAPTION_FORCE")
  local basic_info = GameDataAccessHelper:GetCommanderBasicInfo(commander_id, commander_level)
  if commander_id == 1 and leaderlist.leader_ids[currentMatrixIndex] ~= 1 and leaderlist.leader_ids[currentMatrixIndex] ~= nil then
    commander_vesselstype = GameDataAccessHelper:GetCommanderVesselsType(leaderlist.leader_ids[currentMatrixIndex], commander_level)
    commander_color = GameDataAccessHelper:GetCommanderColorFrame(leaderlist.leader_ids[currentMatrixIndex], commander_level)
    basic_info = GameDataAccessHelper:GetCommanderBasicInfo(leaderlist.leader_ids[currentMatrixIndex], commander_level)
  end
  local skill = basic_info.SPELL_ID
  local skillIconFram = "TYPE_" .. GameDataAccessHelper:GetSkillRangeType(skill)
  if commander_id == 1 and leaderlist.leader_ids[currentMatrixIndex] ~= nil and leaderlist.leader_ids[currentMatrixIndex] == 1 then
    local commander_data = GameGlobalData:GetFleetInfo(commander_id)
    local num_max_skill = GameGlobalData:GetNumMaxSkills()
    for index_skill = 1, num_max_skill do
      local id_skill = commander_data.spells[index_skill]
      DebugOut("id_skill: ", id_skill)
      id_skill = id_skill or -1
      local is_active = id_skill == commander_data.active_spell
      if is_active then
        skillIconFram = "TYPE_" .. GameDataAccessHelper:GetSkillRangeType(id_skill)
      end
    end
  end
  local ability = GameDataAccessHelper:GetCommanderAbility(commander_id, commander_level)
  local sex = GameDataAccessHelper:GetCommanderSex(commander_id)
  if commander_id == 1 and leaderlist.leader_ids[currentMatrixIndex] ~= 1 and leaderlist.leader_ids[currentMatrixIndex] ~= nil then
    ability = GameDataAccessHelper:GetCommanderAbility(leaderlist.leader_ids[currentMatrixIndex], 0)
    sex = GameDataAccessHelper:GetCommanderSex(leaderlist.leader_ids[currentMatrixIndex])
  end
  local fleetRank = GameUtils:GetFleetRankFrame(ability.Rank, GameObjectPlayerMatrix.DownloadRankImageCallback)
  if sex == 1 then
    sex = "man"
  elseif sex == 0 then
    sex = "woman"
  else
    sex = "unknown"
  end
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "selectCommanderItem", matrix_type, pos_index, commander_name, commander_identity, commander_avatar, commander_vesselstype, GameUtils.numberAddComma(commander_force), commander_color, forceContext, skillIconFram, fleetRank, sex, GameLoader:GetGameText("LC_MENU_Level"))
  end
  self.selected_commander = commander_id
end
function GameObjectPlayerMatrix.DownloadRankImageCallback(extinfo)
  if GameObjectPlayerMatrix:GetFlashObject() then
    GameObjectPlayerMatrix:GetFlashObject():InvokeASCallback("_root", "updateRankImage", extinfo.rank_id)
  end
end
function GameObjectPlayerMatrix:SelectCommanderSkill(commander_id, skill_id)
  if commander_id ~= -1 and skill_id ~= -1 then
    FleetMatrix:SetCurrentSkill(commander_id, skill_id)
    local commander_data = GameGlobalData:GetFleetInfo(commander_id)
    commander_data.active_spell = skill_id
    local leaderlist = GameGlobalData:GetData("leaderlist")
    local curSpells = leaderlist.active_spells
    for indexSkill = 1, GameGlobalData:GetNumMaxSkills() do
      if curSpells[indexSkill] == skill_id then
        local datatable = {}
        datatable[#datatable + 1] = GameDataAccessHelper:GetSkillNameText(skill_id)
        datatable[#datatable + 1] = GameDataAccessHelper:GetSkillDesc(skill_id)
        local rangetype = GameDataAccessHelper:GetSkillRangeType(skill_id)
        datatable[#datatable + 1] = "TYPE_" .. tostring(rangetype)
        datatable[#datatable + 1] = GameLoader:GetGameText("LC_MENU_SKILL_RANGE_TYPE_" .. tostring(rangetype))
        local datastring = table.concat(datatable, "\001")
        self:GetFlashObject():InvokeASCallback("_root", "selectCommanderSkill", indexSkill, datastring)
        break
      end
    end
  else
    local leaderlist = GameGlobalData:GetData("leaderlist")
    if self.mainFleetList[self.curMainFleetIndex] ~= 1 then
      self:GetFlashObject():InvokeASCallback("_root", "shrinkSkillSelectorNew")
    else
      self:GetFlashObject():InvokeASCallback("_root", "shrinkSkillSelector")
    end
  end
end
function GameObjectPlayerMatrix:OnLocatedCommander(object_dragger)
  local located_type = object_dragger.DragToGridType
  local located_index = object_dragger.DragToGridIndex
  local located_src_commander = self:GetCommanderIDWithGrid(located_type, located_index)
  local commander_matrix = self:GetCommanderMatrixWithType(located_type)
  commander_matrix[located_index] = object_dragger.DraggedFleetID
  self:UpdateCommanderItem(located_type, located_index)
  if located_type == "battle" and object_dragger.DraggedFleetGridType == "battle" then
    FleetMatrix:SwapMatrix(object_dragger.DraggedFleetGridIndex, located_index)
  elseif located_type == "battle" then
    FleetMatrix:SetMatrix(located_index, object_dragger.DraggedFleetID)
  elseif object_dragger.DraggedFleetGridType == "battle" then
    FleetMatrix:SetMatrix(object_dragger.DraggedFleetGridIndex, -1)
  end
  if located_src_commander and located_src_commander > 0 then
    local src_matrix = self:GetCommanderMatrixWithType(object_dragger.DraggedFleetGridType)
    src_matrix[object_dragger.DraggedFleetGridIndex] = located_src_commander
    self:UpdateCommanderItem(object_dragger.DraggedFleetGridType, object_dragger.DraggedFleetGridIndex)
  end
  self:GetFlashObject():InvokeASCallback("_root", "onCommanderDragOver")
  self.selected_commander = object_dragger.DraggedFleetID
  self:SelectCommander(self.selected_commander)
  object_dragger.DraggedFleetID = nil
  object_dragger.DragToGridType = nil
  object_dragger.DragToGridIndex = nil
  object_dragger.DraggedFleetGridType = nil
  object_dragger.DraggedFleetGridIndex = nil
  self:UpdateTotalBattleForce(self:ComputeBattleForce())
end
function GameObjectPlayerMatrix.SwitchMatrixAck()
  GameObjectPlayerMatrix.battle_matrix = LuaUtils:table_rcopy(GameObjectPlayerMatrix.m_nextPlayerMatrix)
  GameObjectPlayerMatrix.rest_matrix = LuaUtils:table_rcopy(GameObjectPlayerMatrix.m_nextPlayerRestFleets)
  GameObjectPlayerMatrix:UpdateTotalBattleForce(GameObjectPlayerMatrix:ComputeBattleForce())
  GameObjectPlayerMatrix:SelectCommander(GameObjectPlayerMatrix.selected_commander)
end
function GameObjectPlayerMatrix:OnFSCommand(cmd, arg)
  GameTextEdit:fscommand(self:GetFlashObject(), cmd, arg)
  DebugOut("OnFSCommand", cmd, arg)
  if cmd == "SaveMatrixName" then
    FleetMatrix:RenameMatrix(arg)
    GameTip:Show(GameLoader:GetGameText("LC_MENU_SUGGESTION_SAVE_SUCCESS"))
  elseif cmd == "show_help" then
    GameUIWDStuff:Show(GameUIWDStuff.menuType.menu_type_help)
    GameUIWDStuff:SetHelpText(GameLoader:GetGameText("LC_MENU_SAVE_FORMATION_HELP"))
  elseif cmd == "moveout_over" then
    if arg == "all" then
      if GameStateManager.GameStatePlayerMatrix.basePrveState then
        GameStateManager.GameStatePlayerMatrix.prev_state = GameStateManager.GameStatePlayerMatrix.basePrveState
        GameStateManager.GameStatePlayerMatrix.basePrveState = nil
      end
      GameStatePlayerMatrix:QuitState()
    elseif arg == "goto_recruit" then
      FleetMatrix:SaveMatrixChanged(false)
      local GameStateRecruit = GameStateManager.GameStateRecruit
      GameStateManager:SetCurrentGameState(GameStateRecruit)
    end
  elseif cmd == "begin_drag" then
    DebugOut("begin_drag:" .. arg)
    local param_list = LuaUtils:string_split(arg, ",")
    local commander_id = tonumber(param_list[1])
    local xpos_init = tonumber(param_list[2])
    local ypos_init = tonumber(param_list[3])
    local xpos_mouse = tonumber(param_list[4])
    local ypos_mouse = tonumber(param_list[5])
    local grid_type, grid_index = self:GetCommanderGrid(commander_id)
    if grid_type == "battle" or grid_type == "rest" then
      local leaderlist = GameGlobalData:GetData("leaderlist")
      if commander_id == 1 and leaderlist.leader_ids[currentMatrixIndex] ~= 1 and leaderlist.leader_ids[currentMatrixIndex] ~= nil then
        GameStatePlayerMatrix:BeginDragCommanderVessels(1, xpos_init, ypos_init, xpos_mouse, ypos_mouse, leaderlist.leader_ids[currentMatrixIndex])
      else
        GameStatePlayerMatrix:BeginDragCommanderVessels(commander_id, xpos_init, ypos_init, xpos_mouse, ypos_mouse)
      end
    else
      assert(false)
    end
  elseif cmd == "select_commander" then
    local commander_id = tonumber(arg)
    self:SelectCommander(commander_id)
    if commander_id == 1 then
      self:ShowMainFleetSelector(commander_id)
    end
  elseif cmd == "select_commander_skill" then
    local commander_id, skill_id = unpack(LuaUtils:string_split(arg, ","))
    commander_id = tonumber(commander_id)
    skill_id = tonumber(skill_id)
    GameObjectPlayerMatrix:SelectCommanderSkill(commander_id, skill_id)
  elseif cmd == "clicked_btn_close" then
    GameObjectPlayerMatrix:setMainFleet()
    FleetMatrix:SaveMatrixChanged(false)
    if GameObjectPlayerMatrix:CheckAdjutantBeforeSaveCurrentMatrix() then
      GameObjectPlayerMatrix:AnimationMoveOut()
    else
      DebugOut("GameObjectPlayerMatrix:CheckAdjutantBeforeSaveCurrentMatrix() false")
    end
  elseif cmd == "arrow_L" then
    self:trySaveAndExchangeToMatrix(currentMatrixIndex, true)
  elseif cmd == "arrow_R" then
    self:trySaveAndExchangeToMatrix(currentMatrixIndex, false)
  elseif cmd == "matrixIndexButtonClicked" then
    local isLock = currentMatrixIndex > maxAvailableMatrix
    DebugOut("currentMatrixIndex ", currentMatrixIndex)
    if not isLock then
      lastMatrixIndex = currentMatrixIndex
    end
    local index = tonumber(arg)
    DebugOut("index", index)
    local isMoveLeft = true
    if index == currentMatrixIndex then
      return
    elseif index < currentMatrixIndex then
      isMoveLeft = true
    else
      isMoveLeft = false
    end
    GameObjectPlayerMatrix.mCurFreeMatrixSubIdx = 0
    FleetMatrix:SaveMatrixChanged(true)
    self:GetFlashObject():InvokeASCallback("_root", "closeMainFleetSelector")
    if self.mainFleetList[self.curMainFleetIndex] ~= 1 then
      self:GetFlashObject():InvokeASCallback("_root", "shrinkSkillSelectorNew")
    else
      self:GetFlashObject():InvokeASCallback("_root", "shrinkSkillSelector")
    end
    self:GetFlashObject():InvokeASCallback("_root", "lua2fs_ClickMatrixIndexButton", index)
    currentMatrixIndex = index
    local leaderdata = GameGlobalData:GetData("leaderlist")
    local curr_id = leaderdata.leader_ids[currentMatrixIndex]
    for k, v in ipairs(self.mainFleetList) do
      if v == curr_id then
        self.curMainFleetIndex = k
        break
      end
    end
    if index > maxAvailableMatrix then
      self:loadNextMatrixAndRestFleets(1)
      self:setNextMatrixAndRestFleetsUI(isMoveLeft)
      currentMatrixIndex = index
      GameObjectPlayerMatrix:OnFSCommand("btn_lock")
    else
      self:loadNextMatrixAndRestFleets(index)
      self:setNextMatrixAndRestFleetsUI(isMoveLeft)
      GameGlobalData.GlobalData.curMatrixIndex = currentMatrixIndex
      FleetMatrix:SwitchMatrixReq(currentMatrixIndex, GameObjectPlayerMatrix.SwitchMatrixAck, true)
    end
  elseif cmd == "move_prevRest_over" or cmd == "move_prev_over" or cmd == "move_nextRest_over" or cmd == "move_next_over" then
    self.battle_matrix = LuaUtils:table_rcopy(self.m_nextPlayerMatrix)
    self.rest_matrix = LuaUtils:table_rcopy(self.m_nextPlayerRestFleets)
    for i = 1, 9 do
      self:UpdateCommanderItem("battle", i)
      self:UpdateCommanderItem("rest", i)
    end
    self:GetFlashObject():InvokeASCallback("_root", "lua2fs_updateLockBtn")
    self:SelectCommander(1)
    GameObjectPlayerMatrix:CheckAdjutantBeforeSaveCurrentMatrix()
  elseif cmd == "rest_list_move_prev_over" or cmd == "rest_list_move_next_over" then
    self.rest_matrix = LuaUtils:table_rcopy(self.m_nextPlayerRestFleets)
    for i = 1, 9 do
      local commander_id = self.rest_matrix[i]
      if commander_id == nil then
        commander_id = -1
      end
      self:UpdateCommanderItem("rest", i)
    end
    self:SelectCommander(1)
  elseif cmd == "btn_lock" then
    if currentMatrixIndex > maxAvailableMatrix + 1 then
      GameObjectPlayerMatrix.OnMaxAvailableMatrixChanged(false)
      GameTip:Show(GameLoader:GetGameText("LC_MENU_FORMATION_LIMIT_ARRAY"))
    else
      FleetMatrix:BuyMatrixDialogReq(GameObjectPlayerMatrix.OnMaxAvailableMatrixChanged, lastMatrixIndex)
      if self:GetFlashObject() then
        self:GetFlashObject():InvokeASCallback("_root", "setCurrentMatrix", GameLoader:GetGameText("LC_MENU_FORMATION_BUTTON") .. tostring(currentMatrixIndex))
      end
    end
  elseif cmd == "btn_save" then
    if GameObjectPlayerMatrix:CheckAdjutantBeforeSaveCurrentMatrix() then
      FleetMatrix:SaveMatrixChanged(false)
    end
    GameStateManager:SetCurrentGameState(GameStateTacticsCenter)
  elseif cmd == "adjutantClicked" then
    if GameObjectPlayerMatrix:CheckAdjutantBeforeSaveCurrentMatrix() then
      local function saveSuccessCallback()
        local function exitAdjutantUICallback()
          GameObjectPlayerMatrix:RefreshBattleAndRestGrid()
          GameObjectPlayerMatrix:UpdateAdjutantBar()
          GameObjectPlayerMatrix:CheckAdjutantBeforeSaveCurrentMatrix()
        end
        GameUIAdjutant.mFleetIdBeforeEnter = 1
        GameUIAdjutant:EnterAdjutantUI(exitAdjutantUICallback)
      end
      FleetMatrix:SaveMatrixChanged(false, saveSuccessCallback)
    else
      DebugOut("GameObjectPlayerMatrix:CheckAdjutantBeforeSaveCurrentMatrix() false")
    end
  elseif cmd == "showDetailInfo" then
    local fleet = GameGlobalData:GetFleetInfo(self.curFleetId)
    if self.curFleetId == 1 then
      local leaderlist = GameGlobalData:GetData("leaderlist")
      DebugOut("fleetinfo")
      DebugTable(fleet)
      ItemBox:ShowCommanderDetail2(self.mainFleetList[self.curMainFleetIndex], fleet, 1)
    else
      ItemBox:ShowCommanderDetail2(fleet.identity, fleet, 1)
    end
  elseif cmd == "GoToMasterPage" then
    GameUIMaster.CurrentScope = {
      [1] = -1
    }
    GameUIMaster.CurrentSystemType = GameUIMaster.MasterSystem.FormationMaster
    GameUIMaster.CurrentPageType = GameUIMaster.MasterPage.fleets_augment
    GameUIMaster.CurMatrixId = FleetMatrix.cur_index
    GameStateManager:GetCurrentGameState():AddObject(GameUIMaster)
    FleetMatrix:SaveMatrixChanged(false)
  elseif cmd == "choose_Item" then
    self.curMainFleetIndex = tonumber(arg)
    self:SelectCommanderNew(tonumber(arg))
    self:showMainFleetSkillSelector(tonumber(arg))
  elseif cmd == "setMainFleet" then
    self:setMainFleet()
  elseif cmd == "enterFleets" then
    GameObjectPlayerMatrix:setMainFleet()
    FleetMatrix:SaveMatrixChanged(false)
    local GameStateEquipEnhance = GameStateManager.GameStateEquipEnhance
    if GameStateManager.GameStateEquipEnhance.basePrveState == nil and GameStateManager.GameStatePlayerMatrix.basePrveState == nil then
      GameStateManager.GameStatePlayerMatrix.basePrveState = GameStateManager.GameStatePlayerMatrix.prev_state
    end
    GameStateEquipEnhance:EnterEquipEnhance(-1)
  end
end
function GameObjectPlayerMatrix:AnimationMoveIn()
  self:GetFlashObject():InvokeASCallback("_root", "animationMoveIn")
end
function GameObjectPlayerMatrix:AnimationMoveOut()
  self:GetFlashObject():InvokeASCallback("_root", "animationMoveOut")
end
function GameObjectPlayerMatrix:GetCommanderLocatedInfo()
  local located_type, located_index
  local located_info = self:GetFlashObject():InvokeASCallback("_root", "getCommanderLocatedInfo")
  if located_info then
    local param_list = LuaUtils:string_split(located_info, ",")
    located_type = param_list[1]
    located_index = tonumber(param_list[2])
  end
  return located_type, located_index
end
function GameObjectPlayerMatrix:GetCommanderGridPos(grid_type, grid_index)
  local result_pos
  local pos_info = self:GetFlashObject():InvokeASCallback("_root", "getCommanderGridPos", grid_type, grid_index)
  if pos_info then
    local param_list = LuaUtils:string_split(pos_info, ",")
    local xpos = tonumber(param_list[1])
    local ypos = tonumber(param_list[2])
    result_pos = {_x = xpos, _y = ypos}
  end
  return result_pos
end
local frameOptimize = 0
local frameOptimizeDT = 0
function GameObjectPlayerMatrix:Update(dt)
  local flash_obj = self:GetFlashObject()
  flash_obj:Update(dt)
  flash_obj:InvokeASCallback("_root", "onUpdateFrame", dt)
end
if AutoUpdate.isAndroidDevice then
  function GameObjectPlayerMatrix.OnAndroidBack()
    GameObjectPlayerMatrix:GetFlashObject():InvokeASCallback("_root", "mainMoveOut")
  end
end
function GameObjectPlayerMatrix:requestMultiMatrix(isBlock)
  DebugOut("GameObjectPlayerMatrix requestMultiMatrix")
  NetMessageMgr:SendMsg(NetAPIList.mulmatrix_get_req.Code, {type = 0}, GameObjectPlayerMatrix.requestMultiMatrixCallBack, isBlock)
end
function GameObjectPlayerMatrix.requestMultiMatrixCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.mulmatrix_get_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.mulmatrix_get_ack.Code then
    DebugOut("GameObjectPlayerMatrix.requestMultiMatrixCallBack ", msgType)
    DebugTable(content)
    if content ~= nil and content.blank_num > 1 then
      maxAvailableMatrix = content.blank_num
      GameGlobalData.GlobalData.maxAvailableMatrix = maxAvailableMatrix
      GameObjectPlayerMatrix:GetFlashObject():InvokeASCallback("_root", "initMatrixIndexButton", content.blank_num)
    end
    if not haveRequestMatrixInfo then
      haveRequestMatrixInfo = true
      if 0 < content.cur_index then
        currentMatrixIndex = content.cur_index
        GameGlobalData.GlobalData.curMatrixIndex = currentMatrixIndex
      else
        currentMatrixIndex = 1
        GameObjectPlayerMatrix:InitFormation()
      end
      GameObjectPlayerMatrix:loadNextMatrixAndRestFleets(currentMatrixIndex)
      GameObjectPlayerMatrix:GetFlashObject():InvokeASCallback("_root", "chooseMatrix", currentMatrixIndex)
      GameObjectPlayerMatrix.battle_matrix = LuaUtils:table_rcopy(GameObjectPlayerMatrix.m_nextPlayerMatrix)
      GameObjectPlayerMatrix.rest_matrix = LuaUtils:table_rcopy(GameObjectPlayerMatrix.m_nextPlayerRestFleets)
      for i = 1, 9 do
        GameObjectPlayerMatrix:UpdateCommanderItem("battle", i)
        GameObjectPlayerMatrix:UpdateCommanderItem("rest", i)
      end
      GameObjectPlayerMatrix:GetFlashObject():InvokeASCallback("_root", "lua2fs_updateLockBtn")
      GameObjectPlayerMatrix:SelectCommander(1)
      GameObjectPlayerMatrix:initUI()
    end
    return true
  end
  return false
end
function GameObjectPlayerMatrix:setNextMatrixAndRestFleetsUI(isLeft, isSubRest)
  DebugOut("setNextMatrixAndRestFleetsUI", isLeft)
  DebugTable(self.m_nextPlayerMatrix)
  DebugTable(self.m_nextPlayerRestFleets)
  if isLeft then
    for i = 1, 9 do
      self:UpdateCommanderItem("yours_L", i)
    end
    local listType = "rest_list_L"
    if isSubRest then
      listType = "rest_list_L2"
    end
    for i = 1, 9 do
      self:UpdateCommanderItem(listType, i)
    end
  else
    for i = 1, 9 do
      self:UpdateCommanderItem("yours_R", i)
    end
    local listType = "rest_list_R"
    if isSubRest then
      listType = "rest_list_R2"
    end
    for i = 1, 9 do
      self:UpdateCommanderItem(listType, i)
    end
  end
  self:GetFlashObject():InvokeASCallback("_root", "HideAllSelectRound")
end
function GameObjectPlayerMatrix.CommonNetMessageCallback(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code then
    if content.api == NetAPIList.mulmatrix_buy_req.Code then
      if content.code ~= 0 then
        GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      else
        DebugOut("purchase success")
      end
    end
    return true
  end
  return false
end
function GameObjectPlayerMatrix:clearMultiMatrixData()
  DebugOut("clearMultiMatrixData")
  GameObjectPlayerMatrix.m_nextPlayerMatrix = nil
  GameObjectPlayerMatrix.m_nextPlayerRestFleets = nil
  currentMatrixIndex = 1
  maxAvailableMatrix = 1
  haveRequestMatrixInfo = false
end
function GameObjectPlayerMatrix:CheckMatrixChanged()
  return (...), FleetMatrix
end
function GameObjectPlayerMatrix:loadNextMatrixAndRestFleets(index, subIndex)
  DebugOut("GameObjectPlayerMatrix loadNextMatrixAndRestFleets", index)
  currentMatrixIndex = index
  FleetMatrix.cur_index = currentMatrixIndex
  if self:GetFlashObject() then
    if FleetMatrix:getCurrentMartix() then
      self:GetFlashObject():InvokeASCallback("_root", "setCurrentMatrix", FleetMatrix:getCurrentMartix().name)
    else
      self:GetFlashObject():InvokeASCallback("_root", "setCurrentMatrix", "")
    end
  end
  local currentMatrix = FleetMatrix:getCurrentMartix().cells
  self.m_nextPlayerMatrix = {}
  self.m_nextPlayerRestFleets = {}
  local formationIds = {}
  DebugOutPutTable(currentMatrix, "currentMatrix")
  for _, v in ipairs(currentMatrix) do
    assert(v.fleet_identity ~= nil)
    self.m_nextPlayerMatrix[v.cell_id] = v.fleet_identity
    if v.fleet_identity ~= -1 and v.fleet_identity ~= 0 then
      formationIds[v.fleet_identity] = 1
    end
  end
  for i = 1, 9 do
    if self.battle_matrix[i] == 0 then
      self.m_nextPlayerMatrix[i] = self.battle_matrix[i]
    end
    self.m_nextPlayerRestFleets[i] = -1
  end
  DebugOut("m_nextPlayerMatrix")
  DebugTable(self.m_nextPlayerMatrix)
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  local cell_index = 1
  for i, v in ipairs(fleets) do
    if formationIds[v.identity] == nil then
      self.m_nextPlayerRestFleets[cell_index] = v.identity
      cell_index = cell_index + 1
    end
  end
  if subIndex then
    local pageCnt = math.ceil((#self.m_nextPlayerRestFleets + 1) / 9)
    DebugOut("pageCnt " .. tostring(pageCnt) .. tostring(GameObjectPlayerMatrix.mCurFreeMatrixSubIdx))
    GameObjectPlayerMatrix.mCurFreeMatrixSubIdx = (GameObjectPlayerMatrix.mCurFreeMatrixSubIdx + subIndex + pageCnt) % pageCnt
    local subRestFleets = {}
    for k = 1, 9 do
      if self.m_nextPlayerRestFleets[k + 9 * GameObjectPlayerMatrix.mCurFreeMatrixSubIdx] then
        subRestFleets[k] = self.m_nextPlayerRestFleets[k + 9 * GameObjectPlayerMatrix.mCurFreeMatrixSubIdx]
      else
        subRestFleets[k] = -1
      end
    end
    DebugOut("subIndex = " .. tostring(subIndex) .. " mCurFreeMatrixSubIdx " .. tostring(GameObjectPlayerMatrix.mCurFreeMatrixSubIdx))
    DebugOut("before m_nextPlayerRestFleets")
    DebugTable(self.m_nextPlayerRestFleets)
    self.m_nextPlayerRestFleets = subRestFleets
    DebugOut("after m_nextPlayerRestFleets")
    DebugTable(self.m_nextPlayerRestFleets)
  else
  end
  DebugOut("m_nextPlayerMatrix")
  DebugTable(self.m_nextPlayerRestFleets)
end
function GameObjectPlayerMatrix:trySaveAndExchangeToMatrix(index, isleft)
  DebugOut("trySaveAndExchangeToMatrix", index)
  FleetMatrix:SaveMatrixChanged(true)
  local subIndex = 1
  if isleft then
    subIndex = -1
  end
  self:loadNextMatrixAndRestFleets(index, subIndex)
  self:setNextMatrixAndRestFleetsUI(isleft, true)
  if isleft then
    self:GetFlashObject():InvokeASCallback("_root", "RestFormationMoveToPrev")
  else
    self:GetFlashObject():InvokeASCallback("_root", "RestFormationMoveToNext")
  end
end
function GameObjectPlayerMatrix.OnMaxAvailableMatrixChanged(success)
  DebugOut("OnMaxAvailableMatrixChanged")
  if success then
    maxAvailableMatrix = FleetMatrix.cur_index
    GameObjectPlayerMatrix:GetFlashObject():InvokeASCallback("_root", "lua2fs_resetMaxAvailableMatrix", maxAvailableMatrix)
    GameObjectPlayerMatrix:loadNextMatrixAndRestFleets(maxAvailableMatrix)
  else
    GameObjectPlayerMatrix:OnFSCommand("matrixIndexButtonClicked", lastMatrixIndex)
  end
end
function GameObjectPlayerMatrix:GetMajorIdByAdjutantId(adjutantId)
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  for k, v in pairs(fleets) do
    if v.cur_adjutant == adjutantId then
      return v.identity
    end
  end
  return -1
end
function GameObjectPlayerMatrix:GetAdjutantCount(matrix)
  local cnt = 0
  for k, v in pairs(matrix) do
    local fleet = GameGlobalData:GetFleetInfo(v)
    if fleet and 0 < fleet.cur_adjutant then
      cnt = cnt + 1
    end
  end
  return cnt
end
function GameObjectPlayerMatrix:IsFleetOnMatrix(matrix, fleetId)
  for k, v in pairs(matrix) do
    local fleet = GameGlobalData:GetFleetInfo(v)
    if fleet and fleet.identity == fleetId then
      return true
    end
  end
  return false
end
function GameObjectPlayerMatrix:CheckAdjutant(src, des, draggedFleedId, fromIndex, toIndex)
  DebugOutPutTable(self.battle_matrix, "battle_matrix")
  DebugOutPutTable(self.rest_matrix, "rest_matrix")
  local adjutantMaxCnt = GameGlobalData:GetData("adjutant_max_count")
  DebugOut("adjutantMaxCnt " .. tostring(adjutantMaxCnt))
  local srcMatrix
  if src == "battle" then
    srcMatrix = self.battle_matrix
  else
    srcMatrix = self.rest_matrix
  end
  local destMatrix
  if des == "battle" then
    destMatrix = self.battle_matrix
  else
    destMatrix = self.rest_matrix
  end
  local destFleetId = destMatrix[toIndex]
  if nil == destFleetId then
    destFleetId = -1
  end
  local newBattleMatrix = {}
  for k, v in pairs(self.battle_matrix) do
    newBattleMatrix[k] = v
  end
  if src == des then
    return 0
  elseif src == "battle" and des == "rest" and destFleetId == -1 then
    return 0
  elseif destFleetId > 0 then
    local addFleetId = -1
    if src == "battle" and des == "rest" then
      addFleetId = destFleetId
      newBattleMatrix[fromIndex] = destFleetId
    else
      addFleetId = draggedFleedId
      newBattleMatrix[toIndex] = draggedFleedId
    end
    if adjutantMaxCnt < GameObjectPlayerMatrix:GetAdjutantCount(newBattleMatrix) then
      return -1
    else
      local majorId = GameObjectPlayerMatrix:GetMajorIdByAdjutantId(addFleetId)
      if majorId > 0 then
        return 1, majorId, addFleetId
      else
        local fleetInfo = GameGlobalData:GetFleetInfo(addFleetId)
        if GameObjectPlayerMatrix:IsFleetOnMatrix(newBattleMatrix, fleetInfo.cur_adjutant) then
          return 1, addFleetId, fleetInfo.cur_adjutant
        else
          return 0
        end
      end
    end
  elseif src == "rest" and des == "battle" then
    newBattleMatrix[toIndex] = draggedFleedId
    if adjutantMaxCnt < GameObjectPlayerMatrix:GetAdjutantCount(newBattleMatrix) then
      return -1
    else
      local majorId = GameObjectPlayerMatrix:GetMajorIdByAdjutantId(draggedFleedId)
      if majorId > 0 then
        return 1, majorId, draggedFleedId
      else
        local fleetInfo = GameGlobalData:GetFleetInfo(draggedFleedId)
        if GameObjectPlayerMatrix:IsFleetOnMatrix(newBattleMatrix, fleetInfo.cur_adjutant) then
          return 1, draggedFleedId, fleetInfo.cur_adjutant
        else
          return 0
        end
      end
    end
  end
end
function GameObjectPlayerMatrix:CheckAdjutantBeforeSaveCurrentMatrix(onlyForResult)
  local adjutantMaxCnt = GameGlobalData:GetData("adjutant_max_count")
  if adjutantMaxCnt < GameObjectPlayerMatrix:GetAdjutantCount(self.battle_matrix) then
    DebugOut("Out of max count adjutant can battle.")
    GameTip:Show(GameLoader:GetGameText("LC_MENU_BRIDGE_INFO_4"), 3000)
    return false
  end
  for k, v in pairs(self.battle_matrix) do
    if v > 0 then
      do
        local majorId = GameObjectPlayerMatrix:GetMajorIdByAdjutantId(v)
        if majorId > 0 then
          if nil == onlyForResult or not onlyForResult then
            do
              local tip = GameLoader:GetGameText("LC_MENU_BRIDGE_INFO_3")
              DebugOut("tip " .. tip)
              local adjutantName = GameDataAccessHelper:GetFleetLevelDisplayName(v)
              local majorName = GameDataAccessHelper:GetFleetLevelDisplayName(majorId)
              tip = string.gsub(tip, "<npc2_name>", adjutantName)
              tip = string.gsub(tip, "<npc1_name>", majorName)
              local function releaseAdjutantResultCallback(success)
                DebugOut("releaseAdjutantResultCallback " .. tostring(success))
                if success then
                  GameObjectPlayerMatrix:UpdateAdjutantBar()
                  GameObjectPlayerMatrix:UpdateBattleCommanderByFleetId(majorId)
                  GameObjectPlayerMatrix:CheckAdjutantBeforeSaveCurrentMatrix()
                else
                end
              end
              local function okCallback()
                GameStatePlayerMatrix:RequestReleaseAdjutant(majorId, releaseAdjutantResultCallback)
              end
              local cancelCallback = function()
              end
              GameUIGlobalScreen:ShowMessageBox(2, "", tip, okCallback, cancelCallback)
            end
          end
          return false
        end
      end
    end
  end
  return true
end
function GameObjectPlayerMatrix:UpdateAdjutantBar()
  local flashObj = self:GetFlashObject()
  if flashObj then
    local adjutantCnt = 0
    local adjutantAvatarFrs = ""
    local adjutantbgColor = ""
    for k, v in pairs(self.battle_matrix) do
      local commander_data2 = GameGlobalData:GetFleetInfo(v)
      if v > 0 and commander_data2 ~= nil and commander_data2.cur_adjutant and commander_data2.cur_adjutant > 1 then
        adjutantAvatarFrs = adjutantAvatarFrs .. GameDataAccessHelper:GetFleetAvatar(commander_data2.cur_adjutant) .. "\001"
        adjutantbgColor = adjutantbgColor .. GameDataAccessHelper:GetCommanderColorFrame(commander_data2.cur_adjutant, commander_data2.adjutant_level) .. "\001"
        local ability = GameDataAccessHelper:GetCommanderAbility(commander_data2.cur_adjutant)
        adjutantCnt = adjutantCnt + 1
      end
    end
    local adjutantUnlockCnt = GameGlobalData:GetData("adjutant_max_count")
    DebugOut("SetAdjutantBar " .. tostring(adjutantCnt) .. " " .. adjutantAvatarFrs .. "adjutantbgColor = " .. adjutantbgColor)
    flashObj:InvokeASCallback("_root", "SetAdjutantBar", adjutantUnlockCnt, adjutantCnt, adjutantAvatarFrs, adjutantbgColor)
  end
end
function GameObjectPlayerMatrix:RefreshBattleAndRestGrid()
  for i = 1, 9 do
    self:UpdateCommanderItem("battle", i)
    self:UpdateCommanderItem("rest", i)
  end
end
function GameObjectPlayerMatrix:UpdateBattleCommanderByFleetId(fleetId)
  for k, v in pairs(self.battle_matrix) do
    if fleetId == v then
      GameObjectPlayerMatrix:UpdateCommanderItem("battle", k)
      break
    end
  end
end
function GameObjectPlayerMatrix:RefreshRedPoint()
  local flashObj = self:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "setBtnSaveRedPoint", false)
    if GameGlobalData.redPointStates then
      for k, v in pairs(GameGlobalData.redPointStates.states) do
        if v.key == 100000001 and v.value == 1 then
          flashObj:InvokeASCallback("_root", "setBtnSaveRedPoint", true)
          break
        end
      end
    end
  end
end
