local LABA_TYPE = GameData.Dexter_laba.LABA_TYPE
LABA_TYPE[1] = {
  id = 1,
  name = "money_s",
  size = 0,
  item_id = 0,
  prob = 500
}
LABA_TYPE[2] = {
  id = 2,
  name = "money_m",
  size = 1,
  item_id = 0,
  prob = 100
}
LABA_TYPE[3] = {
  id = 3,
  name = "money",
  size = 2,
  item_id = 0,
  prob = 25
}
LABA_TYPE[4] = {
  id = 4,
  name = "technique_s",
  size = 0,
  item_id = 0,
  prob = 500
}
LABA_TYPE[5] = {
  id = 5,
  name = "technique_m",
  size = 1,
  item_id = 0,
  prob = 100
}
LABA_TYPE[6] = {
  id = 6,
  name = "technique",
  size = 2,
  item_id = 0,
  prob = 25
}
LABA_TYPE[7] = {
  id = 7,
  name = "prestige_s",
  size = 0,
  item_id = 0,
  prob = 250
}
LABA_TYPE[8] = {
  id = 8,
  name = "prestige_m",
  size = 1,
  item_id = 0,
  prob = 50
}
LABA_TYPE[9] = {
  id = 9,
  name = "prestige",
  size = 2,
  item_id = 0,
  prob = 20
}
LABA_TYPE[10] = {
  id = 10,
  name = "credit_s",
  size = 0,
  item_id = 0,
  prob = 0
}
LABA_TYPE[11] = {
  id = 11,
  name = "credit_m",
  size = 1,
  item_id = 0,
  prob = 0
}
LABA_TYPE[12] = {
  id = 12,
  name = "credit",
  size = 2,
  item_id = 0,
  prob = 0
}
LABA_TYPE[13] = {
  id = 13,
  name = "laba_supply_s",
  size = 0,
  item_id = 0,
  prob = 20
}
LABA_TYPE[14] = {
  id = 14,
  name = "laba_supply_m",
  size = 1,
  item_id = 0,
  prob = 10
}
LABA_TYPE[15] = {
  id = 15,
  name = "item_100001",
  size = 0,
  item_id = 100001,
  prob = 5
}
LABA_TYPE[16] = {
  id = 16,
  name = "item_100002",
  size = 0,
  item_id = 100002,
  prob = 5
}
LABA_TYPE[17] = {
  id = 17,
  name = "item_100003",
  size = 0,
  item_id = 100003,
  prob = 5
}
LABA_TYPE[18] = {
  id = 18,
  name = "item_100004",
  size = 0,
  item_id = 100004,
  prob = 5
}
LABA_TYPE[19] = {
  id = 19,
  name = "item_100005",
  size = 0,
  item_id = 100005,
  prob = 5
}
LABA_TYPE[20] = {
  id = 20,
  name = "item_100006",
  size = 0,
  item_id = 100006,
  prob = 5
}
LABA_TYPE[21] = {
  id = 21,
  name = "item_2102",
  size = 0,
  item_id = 2102,
  prob = 150
}
LABA_TYPE[22] = {
  id = 22,
  name = "item_2103",
  size = 0,
  item_id = 2103,
  prob = 50
}
LABA_TYPE[23] = {
  id = 23,
  name = "slot_empty",
  size = 0,
  item_id = 0,
  prob = 200
}
