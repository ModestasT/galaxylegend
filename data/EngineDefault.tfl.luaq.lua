require("IAPPriceList.tfl")
EngineDefault = {
  m_lastRecordTime = os.clock()
}
function EngineDefault.GetVIPLevel(moneyConsumed)
  if moneyConsumed >= 10000 then
    return 4
  elseif moneyConsumed >= 1000 then
    return 3
  elseif moneyConsumed >= 50 then
    return 2
  elseif moneyConsumed >= 1 then
    return 1
  else
    return 0
  end
end
function EngineDefault.GetVIPLevelString(moneyConsumed)
  return "CS:" .. EngineDefault.GetVIPLevel(moneyConsumed)
end
function EngineDefault.GetPriceFromProductID(productID)
  local price = IAPPriceList[productID]
  if price == nil then
    price = 0
    print([[


 ===========ERROR: can not find price for productID 

]] .. productID)
  end
  return price
end
function EngineDefault.RecordUserAction(actionType)
  print("trying RecordUserAction ", actionType)
  local currentTime = os.clock()
  if currentTime - EngineDefault.m_lastRecordTime <= 5 then
    return
  end
  EngineDefault.m_lastRecordTime = os.clock()
  print("record useraction " .. actionType)
  local recordAPI = "http://ba.gateway.tap4fun.com/player_events"
  local param = {}
  table.insert(param, "bundle_id=" .. ext.GetBundleIdentifier())
  table.insert(param, "platform=" .. ext.GetPlatform())
  table.insert(param, "udid=" .. ext.GetIOSOpenUdid())
  table.insert(param, "type=" .. actionType)
  table.insert(param, "user_id=" .. ext.T4FGetUserID())
  table.insert(param, "user_name=" .. ext.T4FGetUserName())
  table.insert(param, "server_id=" .. ext.T4FGetServerID())
  table.insert(param, "server_name=" .. ext.T4FGetServerName())
  table.insert(param, "email=" .. ext.T4FGetAccount())
  local urlBody = LuaUtils:string_combine(param, "&")
  ext.http.requestBasic({
    recordAPI,
    param = "",
    mode = "notencrypt",
    header = {},
    body = urlBody,
    method = "post"
  })
end
