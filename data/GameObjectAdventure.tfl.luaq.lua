local GameObjectAdventure = LuaObjectManager:GetLuaObject("GameObjectAdventure")
local GameUIBattleResult = LuaObjectManager:GetLuaObject("GameUIBattleResult")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameObjectLevelUp = LuaObjectManager:GetLuaObject("GameObjectLevelUp")
local GameUICommonDialog = LuaObjectManager:GetLuaObject("GameUICommonDialog")
local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
local GameRush = LuaObjectManager:GetLuaObject("GameRush")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameUISection = LuaObjectManager:GetLuaObject("GameUISection")
local GameUIArcaneEnemyInfo = LuaObjectManager:GetLuaObject("GameUIArcaneEnemyInfo")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local GameStateConfrontation = GameStateManager.GameStateConfrontation
local GameStateMainPlanet = GameStateManager.GameStateMainPlanet
local GameStateBattleMap = GameStateManager.GameStateBattleMap
local GameStateBattlePlay = GameStateManager.GameStateBattlePlay
local QuestTutorialsFirstKillBoss = TutorialQuestManager.QuestTutorialsFirstKillBoss
local QuestTutorialEquipmentEvolution = TutorialQuestManager.QuestTutorialEquipmentEvolution
local QuestTutorialNewBossArena = TutorialQuestManager.QuestTutorialNewBossArena
local QuestTutorialBattleMapAfterRecruit = TutorialQuestManager.QuestTutorialBattleMapAfterRecruit
local QuestTutorialBattleMap_second = TutorialQuestManager.QuestTutorialBattleMap_second
local GameVipDetailInfoPanel = LuaObjectManager:GetLuaObject("GameVipDetailInfoPanel")
local FacebookPopUI = LuaObjectManager:GetLuaObject("FacebookPopUI")
local GameUIMaskLayer = LuaObjectManager:GetLuaObject("GameUIMaskLayer")
local GameUIEnemyInfo = LuaObjectManager:GetLuaObject("GameUIEnemyInfo")
GameObjectAdventure.currentScreenShot = 1
GameObjectAdventure.totalScreenShot = 1
GameObjectAdventure.m_currentSection = 1
GameObjectAdventure.m_currentBattleID = 0
GameObjectAdventure.mStarDistrictBossStatus = {}
local isGotoRight = false
local preRequestBossInfo = false
local bossWhoIsTobeCheck = 0
GameObjectAdventure.isNeedGotoEnd = true
function GameObjectAdventure:OnInitGame()
  GameGlobalData:RegisterDataChangeCallback("ace_combat_supply", GameObjectAdventure.UpdateSupplyData)
end
function GameObjectAdventure:Init()
  DebugOut("Init()")
  local boss_status_1, boss_status_2, boss_status_3, boss_status_4 = "", "", "", ""
  boss_status_3 = GameLoader:GetGameText("LC_MENU_DEFEATED_CHAR")
  boss_status_1 = GameLoader:GetGameText("LC_MENU_RUSH_CHAR")
  boss_status_2 = GameLoader:GetGameText("LC_MENU_BEAT_IT_CHAR")
  boss_status_4 = GameLoader:GetGameText("LC_MENU_LOCKED_CHAR")
  self:GetFlashObject():InvokeASCallback("_root", "initBoss", boss_status_1, boss_status_2, boss_status_3, boss_status_4)
  local is170 = immanentversion170 == 4 or immanentversion170 == 5
  self:GetFlashObject():InvokeASCallback("_root", "initUI", is170)
  GameObjectAdventure:GetFlashObject():InvokeASCallback("_root", "showBossAndPlayAnimation")
  self.isCanRefresh = false
  self.isCanRush = false
  if not preRequestBossInfo then
    if self.isNeedGotoEnd then
      DebugOut("isNeedGotoEnd", self.isNeedGotoEnd)
      local progress = GameGlobalData:GetData("progress")
      self.m_currentSection = progress.adv_chapter
    end
    self:InitAdventureData()
  else
    DebugOut("use preRequest")
  end
  self:GetTotalCountAndBattleIDList()
  self:GetFlashObject():SetText("_root.main.btn_rush.rush._Text", GameLoader:GetGameText("LC_MENU_RUSH_BUTTON"))
  self:GetFlashObject():SetBtnEnable("_root.main.btn_rush", false)
  GameObjectAdventure:setArerIsLight()
end
function GameObjectAdventure:GetTotalCountAndBattleIDList()
  self.totalBossNum, self.AllBattleID = GameDataAccessHelper:GetAdventureCountAndBattleIDList()
  self.MaxSection = math.floor(self.AllBattleID[self.totalBossNum] / 1000)
  DebugTable(self.AllBattleID)
  self:GetFlashObject():InvokeASCallback("_root", "setChatBarVisible", self.MaxSection)
end
function GameObjectAdventure.UpdateSupplyData()
  local ace_supply = GameGlobalData:GetData("ace_combat_supply")
  DebugTable(ace_supply)
  if GameObjectAdventure:GetFlashObject() and ace_supply then
    GameObjectAdventure:GetFlashObject():InvokeASCallback("_root", "UpdateSupplyData", ace_supply.current)
  end
end
function GameObjectAdventure.GetMapStatesCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.adventure_map_status_req.Code then
    assert(content.code ~= 0)
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  end
  if msgType == NetAPIList.user_map_status_ack.Code then
    if immanentversion == 2 then
      if QuestTutorialsFirstKillBoss:IsActive() and not QuestTutorialsFirstKillBoss:IsFinished() then
        local function callback()
          GameObjectAdventure:GetFlashObject():InvokeASCallback("_root", "isShowTipAnimation", true)
          GameObjectAdventure:AnalyzingData(content)
          GameObjectAdventure.cdTime = content.refresh_time + os.time() + 10
        end
        GameUICommonDialog:PlayStory({51416}, callback)
      else
        GameObjectAdventure:AnalyzingData(content)
        GameObjectAdventure.cdTime = content.refresh_time + os.time() + 10
      end
    elseif immanentversion == 1 then
      GameObjectAdventure:AnalyzingData(content)
      GameObjectAdventure.cdTime = content.refresh_time + os.time() + 10
    end
    return true
  end
  return false
end
function GameObjectAdventure:Update(dt)
  self:GetFlashObject():InvokeASCallback("_root", "UpdateAdventureList", dt)
  self:GetFlashObject():Update(dt)
  if GameObjectAdventure.cdTime then
    GameObjectAdventure:SetRefreshTime()
  end
end
function GameObjectAdventure:SetRefreshTime()
  local haveTime = GameObjectAdventure.cdTime - os.time()
  if haveTime > 0 then
    local hour = math.floor(haveTime / 3600)
    local minute = math.floor((haveTime - hour * 3600) / 60)
    local second = haveTime - hour * 3600 - minute * 60
    self:GetFlashObject():InvokeASCallback("_root", "RefreshCDTime", self:GetExtrenZero(hour), self:GetExtrenZero(minute), self:GetExtrenZero(second))
  else
    GameObjectAdventure:InitAdventureData()
  end
end
function GameObjectAdventure:GetExtrenZero(num)
  local arg = tonumber(num)
  if arg < 10 then
    arg = "0" .. arg
  end
  return arg
end
function GameObjectAdventure:AnalyzingData(content)
  DebugOut("AnalyzingData content = ")
  DebugTable(content)
  GameObjectAdventure.AdventureListData = content.status
  self:setAllIDValue()
  GameObjectAdventure:getCanBattleMaxScreen()
  if GameObjectAdventure.currentScreenShot == 1 then
    if content.finish_count > 0 then
      GameObjectAdventure.currentScreenShot = GameDataAccessHelper:GetAdventureSceneShot(tonumber(content.finish_count))
      DebugOut("get GameObjectAdventure.currentScreenShot", GameObjectAdventure.currentScreenShot)
      local screenShotID = GameObjectAdventure.m_currentSection * 1000 + 1
      local maxScreenShot = GameDataAccessHelper:GetAdventureSceneShot(screenShotID)
      DebugOut("maxScreenShot", maxScreenShot)
    else
      GameObjectAdventure.currentScreenShot = 1
    end
  end
  DebugOut("currentScreenShot", GameObjectAdventure.currentScreenShot)
  if preRequestBossInfo then
    if GameObjectAdventure.AdventureListData[bossWhoIsTobeCheck].status == 4 then
      GameTip:Show(GameLoader:GetGameText("LC_ALERT_can_not_get_core_plans"))
      return
    else
      DebugOut("\229\143\175\228\187\165\232\183\179\232\189\172")
    end
    if GameStateManager:GetCurrentGameState() ~= GameStateConfrontation then
      GameStateManager:SetCurrentGameState(GameStateConfrontation)
    end
    if self:GetFlashObject() == nil then
      self:LoadFlashObject()
    end
    local screenShotID = GameObjectAdventure.m_currentSection * 1000 + 1
    DebugOut("GetAdventureSceneShot", GameObjectAdventure.m_currentSection, screenShotID)
    GameObjectAdventure.currentScreenShot = GameDataAccessHelper:GetAdventureSceneShot(screenShotID)
    if isGotoRight then
      self:InitAdventureList(self.currentScreenShot, 2)
      self.currentScreenShot = self.currentScreenShot + 1
      self:InitAdventureList(self.currentScreenShot, 1)
      self:GetFlashObject():InvokeASCallback("_root", "showRightWithoutAnim")
      isGotoRight = false
    else
      GameObjectAdventure:InitAdventureList(GameObjectAdventure.currentScreenShot, 1)
    end
    local progress = GameGlobalData:GetData("progress")
    for i = 1, progress.adv_chapter do
      self:updataArerItem(i)
    end
    preRequestBossInfo = false
  elseif GameObjectAdventure:checkIfGotoRight(GameObjectAdventure.AdventureListData) and self.isNeedGotoEnd then
    self:InitAdventureList(self.currentScreenShot, 2)
    self.currentScreenShot = self.currentScreenShot + 1
    if self.currentScreenShot > self.CanBattleMxsScreen then
      self.currentScreenShot = self.currentScreenShot - self.CanBattleMxsScreen
    end
    self:InitAdventureList(self.currentScreenShot, 1)
    self:GetFlashObject():InvokeASCallback("_root", "showRightWithoutAnim")
  else
    local firstBossStatus = GameObjectAdventure.AdventureListData[1]
    if firstBossStatus.status == 4 and self.isNeedGotoEnd then
      self.m_currentSection = self.m_currentSection - 1
      self:InitAdventureData()
      return
    else
      self:InitAdventureList(self.currentScreenShot, 1)
    end
  end
  self.isNeedGotoEnd = false
end
function GameObjectAdventure:checkIfGotoRight(status)
  local bossCount = 0
  DebugOutPutTable(status, "checkIfGotoRight status")
  for _, item in ipairs(status) do
    DebugOut("item.status", item.status)
    if item.status ~= 4 then
      bossCount = bossCount + 1
    else
      break
    end
  end
  DebugOut("checkIfGotoRight", bossCount)
  if bossCount > 4 then
    return true
  else
    return false
  end
end
function GameObjectAdventure:ShowAfterDialog()
  if self.m_currentBattleID and self.m_currentBattleID > 0 then
    if self.isFinishBattle then
      local DialogIds = GameDataAccessHelper:GetAdventureWinDiologId(self.m_currentBattleID)
      GameUICommonDialog:PlayStory(DialogIds, nil)
    else
      local DialogIds = GameDataAccessHelper:GetAdventureFailedDialogId(self.m_currentBattleID)
      GameUICommonDialog:PlayStory(DialogIds, nil)
    end
  end
end
GameObjectAdventure.AdventureRewardTable = {
  [1] = {20},
  [2] = {30},
  [3] = {40},
  [4] = {40},
  [5] = {40, 50},
  [6] = {50},
  [7] = {50},
  [8] = {60},
  [9] = {60},
  [10] = {60, 70},
  [11] = {70},
  [12] = {70},
  [13] = {80},
  [14] = {80},
  [15] = {80, 90},
  [16] = {90},
  [17] = {90, 100},
  [18] = {100, 110},
  [19] = {100, 110},
  [20] = {110}
}
function GameObjectAdventure:InitAdventureList(sceneShot, whichboss)
  DebugOut("InitAdventureList", sceneShot, whichboss)
  self.isCanRefresh = false
  self.isCanRush = false
  self:checkTutorial()
  self:checkHelpTutorial()
  local currentCount = 0
  local bossLV, star = "", ""
  local bossStatus, bossFleet, bossName, bossIcon = "", "", "", ""
  local currentBattleIdTable_Index = 1
  local tableTotalCount = self:getCanBattleCount()
  GameObjectAdventure.totalScreenShot = GameDataAccessHelper:GetAdventureSceneShot(self.AllID[tableTotalCount])
  self.currentBattleIdTable = {}
  self.m_currentSection = 0
  for i = 1, tableTotalCount do
    local screen = GameDataAccessHelper:GetAdventureSceneShot(self.AllID[i])
    if screen == sceneShot then
      if math.floor(self.AllID[i] / 1000) > self.m_currentSection then
        self.m_currentSection = math.floor(self.AllID[i] / 1000)
      end
      currentCount = currentCount + 1
      local headName = GameDataAccessHelper:GetAdventureIcon(self.AllID[i])
      bossIcon = bossIcon .. headName .. "\001"
      bossFleet = bossFleet .. GameDataAccessHelper:GetAdventureShip(self.AllID[i]) .. "\001"
      bossLV = bossLV .. GameDataAccessHelper:GetAdventureLevel(self.AllID[i]) .. "\001"
      if tableTotalCount >= i then
        bossStatus = bossStatus .. GameObjectAdventure:GetBossStatus(GameObjectAdventure.AdventureListData[i].status) .. "\001"
        self.currentBattleIdTable[currentBattleIdTable_Index] = self.AdventureListData[tonumber(i)].battle_id
      else
        bossStatus = bossStatus .. 4 .. "\001"
        GameObjectAdventure.currentBattleIdTable[currentBattleIdTable_Index] = 0
      end
      star = star .. GameObjectAdventure:GetBossStar(GameObjectAdventure.AdventureListData[i].status) .. "\001"
      if GameObjectAdventure.AdventureListData[i].status == 4 then
        bossName = bossName .. "????" .. "\001"
      else
        bossName = bossName .. GameDataAccessHelper:GetAreaNameText(51, self.AllID[i]) .. "\001"
      end
      currentBattleIdTable_Index = currentBattleIdTable_Index + 1
    end
  end
  DebugOut("InitAdventureList:bossFleet:")
  DebugOut(bossFleet)
  DebugOut("InitAdventureList:bossStatus:")
  DebugOut(bossStatus)
  self:CheckLeftAndTag(GameObjectAdventure.CanBattleMxsScreen, sceneShot)
  self:GetFlashObject():InvokeASCallback("_root", "InitAdventureList", currentCount, bossName, bossStatus, bossIcon, bossFleet, bossLV, whichboss, self.currentScreenShot, star, GameLoader:GetGameText("LC_MENU_Level"))
  self:GetFlashObject():SetBtnEnable("_root.main.btn_rush", self.isCanRush)
  local currentSectionText = GameLoader:GetGameText("LC_MENU_AREA_NAME_" .. self.m_currentSection)
  self:GetFlashObject():InvokeASCallback("_root", "setChatBarText", currentSectionText)
  DebugOut("sceneShot", sceneShot, (self.m_currentSection - 1) * 2 + sceneShot)
  local rewardsTextParms = GameObjectAdventure.AdventureRewardTable[(self.m_currentSection - 1) * 2 + sceneShot]
  local rewardsTextInfo
  if table.getn(rewardsTextParms) == 1 then
    rewardsTextInfo = string.format(GameLoader:GetGameText("LC_MENU_ACADEMY_REWARDS_PLAN_1_PLAN"), rewardsTextParms[1])
  else
    rewardsTextInfo = GameLoader:GetGameText("LC_MENU_ACADEMY_REWARDS_PLAN_2_PLAN")
    rewardsTextInfo = string.gsub(rewardsTextInfo, "<levelA_num>", rewardsTextParms[1])
    rewardsTextInfo = string.gsub(rewardsTextInfo, "<levelB_num>", rewardsTextParms[2])
  end
  self:GetFlashObject():InvokeASCallback("_root", "setRewardText", rewardsTextInfo)
  self:GetFlashObject():unlockInput()
end
function GameObjectAdventure:GetCurNameFleetHead()
  local bossIndex = GameObjectAdventure.boss_index + (self.currentScreenShot - 1) * 4
  DebugOut("GameObjectAdventure:GetCurNameFleetHead:", bossIndex)
  DebugTable(self.AllID)
  local headName = GameDataAccessHelper:GetAdventureIcon(self.AllID[bossIndex])
  local level = GameDataAccessHelper:GetAdventureLevel(self.AllID[bossIndex])
  local bossName = GameDataAccessHelper:GetAreaNameText(51, self.AllID[bossIndex])
  return bossName, headName, level
end
function GameObjectAdventure:CheckIsCanRefresh()
  local vipinfo = GameGlobalData:GetData("vipinfo")
  local curLevel = GameVipDetailInfoPanel:GetVipLevel()
  local isVip = curLevel >= 2
  if isVip then
    self.isCanRefresh = true
  end
end
function GameObjectAdventure:GetBossStatus(status)
  local statu = tonumber(status)
  if statu > 10 then
    statu = statu - math.floor(statu / 10) * 10
  end
  if statu == 3 then
    self:CheckIsCanRefresh()
  end
  if statu == 1 then
    self.isCanRush = true
  end
  return statu
end
function GameObjectAdventure:GetBossStar(status)
  local star = 0
  if tonumber(status) > 10 then
    star = math.floor(status / 10)
  end
  return (...), star
end
function GameObjectAdventure:getCanBattleCount()
  DebugOut("getCanBattleCount")
  local tableTotalCount = 0
  for i, v in ipairs(self.AdventureListData) do
    tableTotalCount = tableTotalCount + 1
  end
  DebugOut("tableTotalCount", tableTotalCount)
  return tableTotalCount
end
function GameObjectAdventure:getCanRushBossCountAndList()
  local canRushBossList = {}
  local canRushCount = 0
  local tableTotalCount = GameObjectAdventure:getCanBattleCount()
  for i = 1, tableTotalCount do
    local headName = GameDataAccessHelper:GetAdventureIcon(self.AllID[i])
    local bossName = GameDataAccessHelper:GetAreaNameText(51, self.AllID[i])
    local battleID = self.AdventureListData[tonumber(i)].battle_id
    local status = self.AdventureListData[i].status % 10
    if status == 1 then
      canRushCount = canRushCount + 1
      canRushBossList[canRushCount] = {
        icon = headName,
        name = bossName,
        battle_id = battleID
      }
    end
  end
  return canRushCount, canRushBossList
end
function GameObjectAdventure:setAllIDValue()
  self.AllID = {}
  for k, v in pairs(self.AdventureListData) do
    self.AllID[k] = v.battle_id - 51000000
  end
end
function GameObjectAdventure:getCanBattleMaxScreen()
  local max = 1
  local min = 100000
  DebugOut("getCanBattleMaxScreen:")
  DebugTable(self.AllID)
  for i, v in ipairs(self.AdventureListData) do
    if v.status ~= 4 then
      local screen = GameDataAccessHelper:GetAdventureSceneShot(self.AllID[i])
      if screen ~= nil and max < screen then
        max = screen
      end
      if screen and min >= screen then
        min = screen
      end
    else
      break
    end
  end
  GameObjectAdventure.CanBattleMinScreen = min
  GameObjectAdventure.CanBattleMxsScreen = max
  DebugOut("CanBattleMinScreen", GameObjectAdventure.CanBattleMinScreen)
  DebugOut("CanBattleMxsScreen", GameObjectAdventure.CanBattleMxsScreen)
end
function GameObjectAdventure:CheckLeftAndTag(total, current)
  local tag = 0
  if total == 1 then
    tag = 3
  elseif current < total and current == 1 then
    tag = 2
  elseif current < total and current > 1 then
    tag = 0
  elseif current == total then
    tag = 1
  end
  self:GetFlashObject():InvokeASCallback("_root", "checkLeftAndRightTag", tag)
end
function GameObjectAdventure.userBattleResultCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.adventure_battle_req.Code then
    assert(content.code ~= 0)
    if tonumber(content.code) == 165 then
      do
        local GameUIEnemyInfo = LuaObjectManager:GetLuaObject("GameUIEnemyInfo")
        local function netCall()
          local packet = {
            type = "battle_supply"
          }
          NetMessageMgr:SendMsg(NetAPIList.supply_info_req.Code, packet, GameUIEnemyInfo.NetCallbackSupplyInfo, true, nil)
        end
        netCall()
      end
    else
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  if msgType == NetAPIList.battle_result_ack.Code then
    GameStateConfrontation.m_IsActive = true
    do
      local m_combinedBattleID = GameObjectAdventure.currentBattleIdTable[GameObjectAdventure.boss_index]
      GameGlobalData:UpdateFleetInfoAfterBattle(content.fleets)
      GameGlobalData:RefreshData("fleetinfo")
      local GameStateConfrontation = GameStateManager.GameStateConfrontation
      GameObjectAdventure.isFinishBattle = false
      if content.result.accomplish_level ~= 0 then
        GameObjectAdventure.isFinishBattle = true
      end
      local function storyCallback()
        DebugOut(GameObjectAdventure:GetFlashObject())
        if QuestTutorialsFirstKillBoss:IsActive() then
          QuestTutorialsFirstKillBoss:SetFinish(true)
          if not QuestTutorialEquipmentEvolution:IsActive() and not QuestTutorialEquipmentEvolution:IsFinished() then
            DebugOut("Active:QuestTutorialEquipmentEvolution")
            QuestTutorialEquipmentEvolution:SetActive(true, false)
          end
          if GameObjectAdventure:GetFlashObject() then
            GameObjectAdventure:GetFlashObject():InvokeASCallback("_root", "SetTipAnimation", false)
          end
        end
      end
      local function callback()
        local window_type = ""
        GameUIBattleResult:LoadFlashObject()
        GameUIBattleResult:SetFightReport(content.report, 51, GameObjectAdventure.m_currentBattleID)
        if content.result.accomplish_level > 0 then
          GameUIBattleResult:UpdateExperiencePercent()
          GameUIBattleResult:UpdateAccomplishLevel(content.result.accomplish_level)
          local awardDataTable = {}
          for _, award_data in ipairs(content.result.rewards) do
            local itemName, icon = GameHelper:GetAwardTypeTextAndIcon(award_data.base.item_type, award_data.base.number)
            if award_data.base.item_type ~= "exp" then
              table.insert(awardDataTable, {
                itemType = award_data.base.item_type,
                itemDesc = itemName,
                itemNumber = award_data.base.number
              })
            end
          end
          GameUIBattleResult:UpdateAwardList("battle_win", awardDataTable)
          window_type = "battle_win"
        else
          GameUIBattleResult:RandomizeImprovement()
          window_type = "battle_lose"
        end
        local function enterCallback()
          DebugOut("enterCallback")
          if storyCallback then
            storyCallback()
          end
          if QuestTutorialsFirstKillBoss:IsActive() then
            GameUICommonDialog:PlayStory({1100053}, nil)
          end
        end
        GameUIBattleResult:RegisterOverCallback(enterCallback, nil)
        GameStateConfrontation:EnterConfrontation()
        GameUIBattleResult:AnimationMoveIn(window_type, false)
        GameStateConfrontation:AddObject(GameUIBattleResult)
        GameStateConfrontation.m_IsActive = false
        DebugOut("GameObjectAdventure:battlePlay:callback", storyCallback)
      end
      GameStateBattlePlay:RegisterOverCallback(callback, nil)
      GameStateBattlePlay.curBattleType = "adventure"
      GameStateBattlePlay:InitBattle(GameStateBattlePlay.MODE_PLAY, content.report, 51, GameObjectAdventure.m_currentBattleID)
      GameStateManager:SetCurrentGameState(GameStateManager.GameStateBattlePlay)
      return true
    end
  end
  return false
end
function GameObjectAdventure.getCostCreditCallback(msgType, content)
  DebugOut("getCostCreditCallback:", msgType)
  DebugTable(content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.price_req.Code then
    assert(content.code ~= 0)
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  end
  if msgType == NetAPIList.price_ack.Code then
    if 0 <= content.price then
      local info = string.format(GameLoader:GetGameText("LC_MENU_BOSS_REFRESH_ASK"), content.price)
      GameUtils:CreditCostConfirm(info, function()
        GameObjectAdventure.RefreshBossReques()
      end)
    end
    return true
  end
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.adventure_cd_price_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  return false
end
function GameObjectAdventure.RefreshBossRequesCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.adventure_cd_clear_req.Code then
    if content.code == 0 then
      GameObjectAdventure:InitAdventureData()
      local tipText = GameLoader:GetGameText("LC_MENU_REFRESH_SUCCESS")
      GameTip:Show(tipText)
    else
      local GameVip = LuaObjectManager:GetLuaObject("GameVip")
      GameVip:CheckIsNeedShowVip(content.code)
    end
    return true
  end
  return false
end
function GameObjectAdventure.RefreshBossReques()
  local adv_chapter
  if GameObjectAdventure.m_currentSection > 9 then
    adv_chapter = {
      chapter_id = "510" .. GameObjectAdventure.m_currentSection
    }
  else
    adv_chapter = {
      chapter_id = "5100" .. GameObjectAdventure.m_currentSection
    }
  end
  NetMessageMgr:SendMsg(NetAPIList.adventure_cd_clear_req.Code, adv_chapter, GameObjectAdventure.RefreshBossRequesCallback, false, nil)
end
function GameObjectAdventure:InitAdventureData()
  DebugOut("InitAdventureData", self.m_currentSection)
  self:Clear()
  local combinedChapterID = 5100
  local map_status_req = {
    chapter_id = combinedChapterID * 10 + self.m_currentSection
  }
  NetMessageMgr:SendMsg(NetAPIList.adventure_map_status_req.Code, map_status_req, self.GetMapStatesCallback, true)
end
function GameObjectAdventure:GetCostCredit()
  local vipInfo = GameGlobalData:GetData("vipinfo")
  local curLevel = GameVipDetailInfoPanel:GetVipLevel()
  if curLevel and curLevel >= 0 then
    local adv_chapter
    if self.m_currentSection > 9 then
      adv_chapter = {
        chapter_id = "510" .. self.m_currentSection
      }
    else
      adv_chapter = {
        chapter_id = "5100" .. self.m_currentSection
      }
    end
    NetMessageMgr:SendMsg(NetAPIList.adventure_cd_price_req.Code, adv_chapter, self.getCostCreditCallback, true, nil)
  else
    GameTip:Show(string.format(GameLoader:GetGameText("LC_MENU_COMMANDER_ACADEMY_REFRESH"), 2))
  end
end
function GameObjectAdventure:Clear()
  self.AdventureListData = {}
end
function GameObjectAdventure:OnAddToGameState()
  self:Init()
end
function GameObjectAdventure:OnEraseFromGameState(state)
  self.isCanRush = false
  self.isCanRefresh = false
  GameAndroidBackManager:RemoveCallback(GameObjectAdventure.AndAndroidBackEvent)
end
function GameObjectAdventure:checkTutorial()
  if immanentversion == 2 then
    if GameObjectAdventure.afterBattleShowDialog then
      GameObjectAdventure.afterBattleShowDialog = nil
      local function callback()
        GameUISection:GetFlashObject():InvokeASCallback("_root", "ShowTutorialClose")
      end
      GameUICommonDialog:PlayStory({51417}, callback)
    end
  elseif immanentversion == 1 then
    if QuestTutorialsFirstKillBoss:IsActive() and not QuestTutorialsFirstKillBoss:IsFinished() then
      GameObjectAdventure:GetFlashObject():InvokeASCallback("_root", "isShowTipAnimation", true)
    else
      GameObjectAdventure:GetFlashObject():InvokeASCallback("_root", "isShowTipAnimation", false)
    end
  end
end
function GameObjectAdventure:checkHelpTutorial(...)
  if GameUtils:GetTutorialHelp() and GameUtils:GetTutorialName() == "collect" then
    GameObjectAdventure:GetFlashObject():InvokeASCallback("_root", "isShowHelpTutorial", true)
  else
    GameObjectAdventure:GetFlashObject():InvokeASCallback("_root", "isShowHelpTutorial", false)
  end
end
function GameObjectAdventure:GetBattleInfo(id_value)
  for i, v in ipairs(GameObjectAdventure.AdventureListData) do
    if id_value + 51001000 == v.battle_id then
      return v
    end
  end
end
function GameObjectAdventure:BattleNow()
  local battle_id_value = self.currentBattleIdTable[self.boss_index]
  local battle_request = {battle_id = battle_id_value, action = 0}
  if battle_id_value ~= nil then
    NetMessageMgr:SendMsg(NetAPIList.adventure_battle_req.Code, battle_request, self.userBattleResultCallback, true, nil)
  end
end
function GameObjectAdventure:CheckIsCanBattle()
  DebugOut("GameObjectAdventure:CheckIsCanBattle__")
  local left_times, total_times = 0, 0
  local battle_supply = GameGlobalData:GetData("battle_supply")
  if battle_supply then
    left_times = battle_supply.current
    thenotal_times = battle_supply.max
  else
    return false
  end
  if left_times > 0 then
    return true
  elseif Facebook:IsFacebookEnabled() then
    if not Facebook:IsBindFacebook() then
      if FacebookPopUI.mFeatureSwitch[FACEBOOKFEATURESWITCH.SWITCH_SUPPLY] and FacebookPopUI.mFeatureSwitch[FACEBOOKFEATURESWITCH.SWITCH_SUPPLY] == 1 then
        FacebookPopUI.mManullyCloseCallback = GameObjectAdventure.netCallSupplyInfo
        FacebookPopUI:ShowFacebookMenu(FACEBOOKMENUTYPE.TYPE_MENU_LOGIN)
        FacebookPopUI:SetBindAwardType(FACEBOOKBINDAWARDTYPE.AWARD_PVESUPPLY)
        if not ext.GetDeviceCountry or not ext.GetDeviceCountry() then
          local countryStr = "unknow"
        end
      else
        GameObjectAdventure.netCallSupplyInfo()
      end
    else
      GameObjectAdventure.netCallSupplyInfo()
    end
  else
    GameObjectAdventure.netCallSupplyInfo()
  end
  return false
end
function GameObjectAdventure.netCallSupplyInfo()
  local GameUIEnemyInfo = LuaObjectManager:GetLuaObject("GameUIEnemyInfo")
  local function netCall()
    local packet = {
      type = "battle_supply"
    }
    NetMessageMgr:SendMsg(NetAPIList.supply_info_req.Code, packet, GameUIEnemyInfo.NetCallbackSupplyInfo, true, nil)
  end
  netCall()
end
function GameObjectAdventure:OnFSCommand(cmd, arg)
  if "Fight_Adventure" == cmd then
    local data_index = tonumber(arg)
    local battle_id_value = GameObjectAdventure:GetBattleInfo(data_index).battle_id
    local battle_request = {battle_id = battle_id_value, action = 0}
    if battle_id_value ~= nil then
      NetMessageMgr:SendMsg(NetAPIList.user_battle_req.Code, battle_request, self.userBattleResultCallback, true, nil)
    end
    return
  end
  if "NeedUpdateAdventureData" == cmd then
    local data_index = tonumber(arg)
    local name = GameDataAccessHelper:GetAdventureIcon(data_index)
    local level = GameLoader:GetGameText("LC_MENU_Level") .. tostring(GameDataAccessHelper:GetAdventureLevel(data_index))
    local force = "Force: " .. tostring(GameDataAccessHelper:GetAdventureForce(data_index))
    local action = "Locked"
    local isLocked = true
    local dataInfo = GameObjectAdventure:GetBattleInfo(data_index)
    if dataInfo ~= nil then
      if 1 == dataInfo.status then
        action = "Beat"
        isLocked = false
      elseif 2 == dataInfo.status then
        action = "Unlocked"
        isLocked = false
      elseif 3 == dataInfo.status then
        action = "Defeated"
        isLocked = false
      else
        isLocked = true
      end
    end
    self:GetFlashObject():InvokeASCallback("_root", "UpdateAdventureData", data_index, name, level, force, action, isLocked)
    return
  end
  if cmd == "showLeftBoss" and GameObjectAdventure.currentScreenShot > GameObjectAdventure.CanBattleMinScreen then
    GameObjectAdventure.currentScreenShot = GameObjectAdventure.currentScreenShot - 1
    self:InitAdventureList(self.currentScreenShot, 2)
    self:GetFlashObject():InvokeASCallback("_root", "showLeftAnim")
  end
  if cmd == "showRightBoss" and self.currentScreenShot < self.CanBattleMxsScreen then
    self:InitAdventureList(self.currentScreenShot, 2)
    self.currentScreenShot = self.currentScreenShot + 1
    self:InitAdventureList(self.currentScreenShot, 1)
    self:GetFlashObject():InvokeASCallback("_root", "showRightAnim")
  end
  if cmd == "clickBoss" then
    if GameUtils:GetTutorialHelp() and GameUtils:GetTutorialName() == "collect" then
      GameUtils:HideTutorialHelp()
    end
    if not self:CheckIsCanBattle() then
      return
    end
    if immanentversion == 2 and QuestTutorialsFirstKillBoss:IsActive() then
      QuestTutorialsFirstKillBoss:SetFinish(true)
      self:GetFlashObject():InvokeASCallback("_root", "isShowTipAnimation", false)
      GameObjectAdventure.afterBattleShowDialog = true
      AddFlurryEvent("FinishBattle_51001001", {}, 2)
    end
    if QuestTutorialsFirstKillBoss:IsActive() then
      QuestTutorialsFirstKillBoss:SetFinish(true)
      if not QuestTutorialEquipmentEvolution:IsActive() and not QuestTutorialEquipmentEvolution:IsFinished() then
        DebugOut("Active:QuestTutorialEquipmentEvolution")
        QuestTutorialEquipmentEvolution:SetActive(true, false)
      end
      if GameObjectAdventure:GetFlashObject() then
        GameObjectAdventure:GetFlashObject():InvokeASCallback("_root", "SetTipAnimation", false)
      end
    end
    if immanentversion170 == 4 or immanentversion170 == 5 then
      GameObjectAdventure.boss_index = tonumber(arg)
      local battle_id = GameObjectAdventure.currentBattleIdTable[self.boss_index]
      self.m_currentBattleID = battle_id - 51000000
      local param_battle_id
      if self.m_currentBattleID > 10000 then
        param_battle_id = "510" .. self.m_currentBattleID
      else
        param_battle_id = "5100" .. self.m_currentBattleID
      end
      GameUIEnemyInfo:ShowEliteEnemyInfo(param_battle_id, GameObjectAdventure.AdventureListData[self.boss_index].status)
    else
      GameObjectAdventure.boss_index = tonumber(arg)
      local battle_id = GameObjectAdventure.currentBattleIdTable[self.boss_index]
      local GameStateFormation = GameStateManager.GameStateFormation
      GameStateFormation:SetBattleID(51, battle_id - 51000000)
      self.m_currentBattleID = battle_id - 51000000
      GameStateManager:SetCurrentGameState(GameStateFormation)
    end
  end
  if cmd == "OnMoveOutRestAnimOver" then
    self:GetFlashObject():InvokeASCallback("_root", "backToFirst")
  end
  if cmd == "OnMoveInRestAnimOver" then
    DebugOut("OnMoveInRestAnimOver")
    self:GetFlashObject():InvokeASCallback("_root", "backToFirst")
    self:InitAdventureList(GameObjectAdventure.currentScreenShot, 1)
  end
  if cmd == "showArerMoveIn" then
    self:InitArerListData()
    GameAndroidBackManager:AddCallback(GameObjectAdventure.AndAndroidBackEvent)
  end
  if cmd == "NeedUpdataFeeltID" then
    GameObjectAdventure:updataArerItem(arg)
  end
  if cmd == "AreaItemClick" then
    local clickSection = tonumber(arg)
    GameObjectAdventure.currentScreenShot = GameDataAccessHelper:GetAdventureSceneShot(clickSection * 1000 + 1)
    if self.m_currentSection ~= clickSection then
      self.m_currentSection = clickSection
      self:InitAdventureData()
    end
    local progress = GameGlobalData:GetData("progress")
    for i = 1, progress.adv_chapter do
      self:updataArerItem(i)
    end
  end
  if cmd == "clickRefreshBtn" then
    DebugOut("clickRefreshBtn")
    GameObjectAdventure:GetCostCredit()
  end
  if cmd == "Boss_Rush" then
    local canRushBossCount = GameObjectAdventure:getCanRushBossCountAndList()
    if canRushBossCount > 0 then
      GameRush:ShowRush()
    else
      GameTip:Show(GameLoader:GetGameText("LC_ALERT_no_boss_rush"))
    end
  end
  if cmd == "eraseSectionMenu" then
    GameAndroidBackManager:RemoveCallback(GameObjectAdventure.AndAndroidBackEvent)
  end
  if cmd == "helpTutorial_pos" and GameUtils:GetTutorialHelp() and GameUtils:GetTutorialName() == "collect" then
    local param = LuaUtils:string_split(arg, "\001")
    GameUtils:ShowMaskLayer(tonumber(param[1]), tonumber(param[2]), GameUIMaskLayer.ButtonPos.right, "empty", GameUIMaskLayer.MaskStyle.big)
  end
  if cmd == "helpTutorial_pos_no" and GameUtils:GetTutorialHelp() and GameUtils:GetTutorialName() == "collect" then
    GameUtils:MaskLayerMidTutorial(GameLoader:GetGameText("LC_MENU_ASSISTANT_INFO_18"))
  end
end
function GameObjectAdventure:setArerIsLight()
  local progress = GameGlobalData:GetData("progress")
  if progress.adv_chapter > 1 and not QuestTutorialNewBossArena:IsFinished() then
    QuestTutorialNewBossArena:SetFinish(true)
    self:GetFlashObject():InvokeASCallback("_root", "setChatBarLight")
  end
end
function GameObjectAdventure:InitArerListData()
  local progress = GameGlobalData:GetData("progress")
  for i = 1, progress.adv_chapter do
    self:GetFlashObject():InvokeASCallback("_root", "addArerItem", i)
  end
end
function GameObjectAdventure:updataArerItem(arg)
  local fleetID = tonumber(arg)
  local progress = GameGlobalData:GetData("progress")
  local frme = 1
  if fleetID == self.m_currentSection then
    frme = 2
  elseif fleetID > progress.adv_chapter then
    frme = 3
  end
  local fleetIDName = GameLoader:GetGameText("LC_MENU_AREA_NAME_" .. fleetID)
  local hasNewBoss = false
  if GameObjectAdventure.mStarDistrictBossStatus["5100" .. fleetID] and 1 == tonumber(GameObjectAdventure.mStarDistrictBossStatus["5100" .. fleetID]) then
    hasNewBoss = true
  end
  self:GetFlashObject():InvokeASCallback("_root", "updataArerItem", fleetID, frme, fleetIDName, hasNewBoss)
end
function GameObjectAdventure.AndAndroidBackEvent()
  if AutoUpdate.isAndroidDevice then
    GameObjectAdventure:GetFlashObject():InvokeASCallback("_root", "arerMoveOut")
  end
end
function GameObjectAdventure.AdvChapterStatusNtfHandler(content)
  if nil ~= content then
    DebugOut("receive adventure_chapter_status_ntf")
    GameObjectAdventure:UpdateStarDistrictBoss(content)
  else
    DebugOut("adventure_chapter_status_ntf content is nil.")
  end
end
function GameObjectAdventure:RefreshAllArerItem()
  local progress = GameGlobalData:GetData("progress")
  for fleetID = 1, progress.adv_chapter do
    local frme = 1
    if fleetID == self.m_currentSection then
      frme = 2
    elseif fleetID > progress.adv_chapter then
      frme = 3
    end
    local fleetIDName = GameLoader:GetGameText("LC_MENU_AREA_NAME_" .. fleetID)
    local hasNewBoss = false
    if GameObjectAdventure.mStarDistrictBossStatus["5100" .. fleetID] and 1 == tonumber(GameObjectAdventure.mStarDistrictBossStatus["5100" .. fleetID]) then
      hasNewBoss = true
    end
    if flash_obj then
      flash_obj:InvokeASCallback("_root", "updataArerItem", fleetID, frme, fleetIDName, hasNewBoss)
    end
  end
end
function GameObjectAdventure:UpdateStarDistrictBoss(content)
  for k, v in pairs(content.statuses) do
    GameObjectAdventure.mStarDistrictBossStatus[tostring(v.chapter_id)] = v.status
  end
  DebugOut("GameObjectAdventure:UpdateStarDistrictBoss")
  DebugTable(GameObjectAdventure.mStarDistrictBossStatus)
  GameObjectAdventure:RefreshAllArerItem()
  local progress = GameGlobalData:GetData("progress")
  local newBossInOther = false
  if progress.adv_chapter > 1 then
    for fleetID = 2, progress.adv_chapter do
      if GameObjectAdventure.mStarDistrictBossStatus["5100" .. fleetID] and 1 == tonumber(GameObjectAdventure.mStarDistrictBossStatus["5100" .. fleetID]) then
        newBossInOther = true
        break
      end
    end
  end
  GameObjectAdventure:SetChatBtnColorHighlight(newBossInOther)
end
function GameObjectAdventure:SetChatBtnColorHighlight(isHighlight)
  DebugOut("SetChatBtnColorHighlight", isHighlight)
  if self:GetFlashObject() == nil then
    self:LoadFlashObject()
  end
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    flash_obj:InvokeASCallback("_root", "SetChatBtnOrangeColor", isHighlight)
  end
end
function GameObjectAdventure:gotoBossAreaDirectly(buleprintLevel)
  DebugOut("GameObjectAdventure:gotoAreaDirectly", buleprintLevel)
  local areaID
  areaID, isGotoRight, bossWhoIsTobeCheck = self:checkBossAreaByBlueprintLevel(buleprintLevel)
  DebugOut(areaID, isGotoRight)
  local progress = GameGlobalData:GetData("progress")
  assert(progress ~= nil)
  DebugOut("progress.adv_chapter", progress.adv_chapter)
  local QuestTutorialBuildAcademy = TutorialQuestManager.QuestTutorialBuildAcademy
  if not QuestTutorialBuildAcademy:IsFinished() then
    GameTip:Show(GameLoader:GetGameText("LC_ALERT_not_build_commander_academy"))
    return
  end
  if areaID > progress.adv_chapter then
    GameTip:Show(GameLoader:GetGameText("LC_ALERT_can_not_get_core_plans"))
    return
  end
  self.m_currentSection = areaID
  self:InitAdventureData()
  preRequestBossInfo = true
end
function GameObjectAdventure:gotoBossAreaDirectly170(itemID)
  local battleIdList = GameDataAccessHelper:GetAdventureDrop(itemID)
  if battleIdList then
    local areaID
    areaID = math.floor(tonumber(battleIdList[1]) / 1000)
    isGotoRight = false
    bossWhoIsTobeCheck = tonumber(battleIdList[1]) % 1000
    local progress = GameGlobalData:GetData("progress")
    if areaID > progress.adv_chapter then
      GameTip:Show(GameLoader:GetGameText("LC_ALERT_can_not_get_core_plans"))
      return
    end
    self.m_currentSection = areaID
    self:InitAdventureData()
    preRequestBossInfo = true
  end
end
function GameObjectAdventure:checkBossAreaByBlueprintLevel(buleprintLevel)
  if buleprintLevel == 20 then
    return 1, false, 1
  elseif buleprintLevel == 30 then
    return 1, true, 5
  elseif buleprintLevel == 40 then
    return 2, false, 1
  elseif buleprintLevel == 50 then
    return 3, false, 3
  elseif buleprintLevel == 60 then
    return 4, true, 5
  elseif buleprintLevel == 70 then
    return 5, true, 7
  elseif buleprintLevel == 80 then
    return 7, false, 1
  elseif buleprintLevel == 90 then
    return 8, false, 3
  elseif buleprintLevel == 100 then
    return 9, false, 1
  elseif buleprintLevel == 110 then
    return 10, false, 1
  end
end
