local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
local GameUIWDStuff = LuaObjectManager:GetLuaObject("GameUIWDStuff")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameUIKrypton = LuaObjectManager:GetLuaObject("GameUIKrypton")
local GameUIActivityNew = LuaObjectManager:GetLuaObject("GameUIActivityNew")
GameUIGachaBox = {}
GameUIGachaBox.mFlashObj = nil
GameUIGachaBox.GachaItemsRowList = nil
GameUIGachaBox.GachaRemainsRowList = nil
GameUIGachaBox.GachaChestInfoStr = nil
GameUIGachaBox.mOpenChestCost = 0
GameUIGachaBox.mRefreshChestCost = {}
GameUIGachaBox.totalGachaNum = 0
GameUIGachaBox.remainGachaNum = 0
GameUIGachaBox.bestRewardItem = nil
GameUIGachaBox.randomPos = 0
GameUIGachaBox.GachaLockItem = {}
GameUIGachaBox.subCategory = 0
local preLoadMsgtype, preLoadContent, callbackFunc
GachaItem = {
  mIndex = -1,
  mID,
  mIndexInRemainList = -1,
  mType = "",
  mName = "",
  mNumber = "",
  mLocked = "lock",
  mIconFrame = ""
}
function GachaItem:new()
  local instance = {}
  setmetatable(instance, self)
  self.__index = self
  return instance
end
GachaItemsRow = {
  mRowIndex = -1,
  mGachaItems = {},
  mGachaItemNum = 0
}
function GachaItemsRow:new()
  local instance = {}
  setmetatable(instance, self)
  self.__index = self
  return instance
end
GachaRemainItem = {
  mIndex = -1,
  mID,
  mType,
  mNo,
  mNumber,
  mName,
  mLevel,
  mGachaNumber,
  mRemainNum,
  mTotalNum,
  mIconFrame
}
function GachaRemainItem:new()
  local instance = {}
  setmetatable(instance, self)
  self.__index = self
  return instance
end
GachaRemainItemsRow = {
  mRowIndex = -1,
  mRemainItems = {},
  mRemainItemNumber = 0
}
function GachaRemainItemsRow:new()
  local instance = {}
  setmetatable(instance, self)
  self.__index = self
  return instance
end
function GameUIGachaBox:Init(flashObj, category, subCategory)
  DebugOut("GameUIGachaBox:Init", category, subCategory)
  self.mFlashObj = flashObj
  GameUIGachaBox.mOpenChestCost = 0
  GameUIGachaBox.mRefreshChestCost = {}
  self.curBoxAwards = {}
  GameUIGachaBox.totalGachaNum = 0
  GameUIGachaBox.remainGachaNum = 0
  GameUIGachaBox.category = category
  GameUIGachaBox.subCategory = subCategory
  GameUIGachaBox.OnGlobalResourceChange()
  GameGlobalData:RegisterDataChangeCallback("resource", GameUIGachaBox.OnGlobalResourceChange)
  GameUIGachaBox:SetGachaChestOpenInfo(self.GachaChestInfoStr or "")
end
function GameUIGachaBox:Close()
  GameGlobalData:RemoveDataChangeCallback("resource", GameUIGachaBox.OnGlobalResourceChange)
  GameUIGachaBox.mOpenChestCost = 0
  GameUIGachaBox.mRefreshChestCost = {}
  self.curBoxAwards = {}
  GameUIGachaBox.totalGachaNum = 0
  GameUIGachaBox.remainGachaNum = 0
  preLoadMsgtype = nil
  preLoadContent = nil
end
function GameUIGachaBox:MoveIn()
  local lang = "en"
  if GameSettingData and GameSettingData.Save_Lang then
    local la = GameSettingData.Save_Lang
    if string.find(la, "ru") == 1 then
      lang = "ru"
    end
  end
  if self.mFlashObj then
    self.mFlashObj:InvokeASCallback("_root", "lua2fs_animationsMoveInGacha", lang)
  end
  if preLoadContent ~= nil then
    self:gotoInstanceDirectly()
  else
    self:RequestGachaList()
  end
  GameUIGachaBox.mFlashObj:InvokeASCallback("_root", "EnableRefreshBtn")
end
function GameUIGachaBox:hideGacha()
  if self.mFlashObj then
    self.mFlashObj:InvokeASCallback("_root", "hideGacha")
  end
end
function GameUIGachaBox:Update(dt)
  local flash_obj = self.mFlashObj
  if flash_obj then
    flash_obj:InvokeASCallback("_root", "GachaOnUpdate")
    flash_obj:InvokeASCallback("_root", "updateRollTextPosForBox", dt)
  end
end
function GameUIGachaBox:ParseGachaListInfo(GachaListInfo)
  self.GachaItemsRowList = {}
  self.GachaRemainsRowList = {}
  self.GachaLockItem = {}
  self.processStatus = GachaListInfo.progress_bar
  if GachaListInfo.now_credit_spend then
    self.mOpenChestCost = GachaListInfo.now_credit_spend
  end
  if GachaListInfo.refresh_cost then
    self.mRefreshChestCost = GachaListInfo.refresh_cost
  end
  local remainList = GachaListInfo.reward_state_list
  local tmpIndex = 1
  local it = 1
  local len = #remainList
  DebugOut("aha?")
  DebugTable(remainList)
  self.finalReward = remainList[1].show_item
  DebugOut("finalReward")
  DebugTable(self.finalReward)
  for it = 1, len, 2 do
    local itemsRow = GachaRemainItemsRow:new()
    local item1 = self:ParseRemainItem(remainList[it], 1)
    local item2 = self:ParseRemainItem(remainList[it + 1], 2)
    itemsRow.mRemainItems = {}
    itemsRow.mRemainItems[1] = item1
    itemsRow.mRemainItems[2] = item2
    if item2 == nil then
      itemsRow.mRemainItemNumber = 1
    else
      itemsRow.mRemainItemNumber = 2
    end
    itemsRow.mRowIndex = tmpIndex
    tmpIndex = tmpIndex + 1
    table.insert(self.GachaRemainsRowList, itemsRow)
  end
  GameUIGachaBox.bestRewardItem = remainList[1]
  tmpIndex = 1
  local gachaChestList = GachaListInfo.open_state_list
  self.openStateList = GachaListInfo.open_state_list
  for it = 1, #gachaChestList, 6 do
    local itemsRow = GachaItemsRow:new()
    itemsRow.mGachaItems = {}
    for i = 0, 5 do
      if it + i <= #gachaChestList then
        local item = self:ParseGachaItem(gachaChestList[it + i], i + 1)
        itemsRow.mGachaItemNum = itemsRow.mGachaItemNum + 1
        itemsRow.mGachaItems[i + 1] = item
      end
    end
    itemsRow.mRowIndex = tmpIndex
    tmpIndex = tmpIndex + 1
    table.insert(self.GachaItemsRowList, itemsRow)
  end
end
function GameUIGachaBox:ParseRemainItem(remainContent, subIndex)
  if remainContent == nil then
    return nil
  end
  local remainItem = GachaRemainItem:new()
  local contentItem = remainContent
  remainItem.mID = remainContent.id
  remainItem.mIndex = subIndex
  remainItem.mType = contentItem.item.item_type
  remainItem.mNo = contentItem.item.no
  remainItem.mLevel = contentItem.item.level
  remainItem.mNumber = contentItem.item.number
  remainItem.mName = GameHelper:GetAwardTypeText(contentItem.item.item_type, contentItem.item.number)
  remainItem.mGachaNumber = "x" .. GameUtils.numberConversion(GameHelper:GetAwardCount(contentItem.item.item_type, contentItem.item.number, contentItem.item.no))
  remainItem.mRemainNum = contentItem.cur_num
  remainItem.mTotalNum = contentItem.total_num
  GameUIGachaBox.totalGachaNum = GameUIGachaBox.totalGachaNum + contentItem.total_num
  GameUIGachaBox.remainGachaNum = GameUIGachaBox.remainGachaNum + contentItem.cur_num
  if DynamicResDownloader:IsDynamicStuffByGameItem(contentItem.item) then
    local fullFileName = DynamicResDownloader:GetFullName(contentItem.item.number .. ".png", DynamicResDownloader.resType.PIC)
    local localPath = "data2/" .. fullFileName
    DebugWD("localPath = ", localPath)
    if DynamicResDownloader:IfResExsit(localPath) then
      ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
      remainItem.mIconFrame = "item_" .. contentItem.item.number
    else
      remainItem.mIconFrame = "temp"
      self:AddDownloadPath(contentItem.item.number, remainItem.mID, GameUIGachaBox.donamicDownloadFinishCallbackRemainList)
    end
  else
    remainItem.mIconFrame = GameHelper:GetAwardTypeIconFrameName(contentItem.item.item_type, contentItem.item.number, contentItem.item.no)
  end
  return remainItem
end
function GameUIGachaBox:ParseGachaItem(gachaContent, subIndex)
  local gachaItem = GachaItem:new()
  gachaItem.mIndex = subIndex
  if gachaContent.type_id >= 0 then
    local rItem, iRow, iRowSub = GameUIGachaBox:GetGachaRemainItemByID(gachaContent.type_id)
    gachaItem.mIndexInRemainList = gachaContent.type_id
    gachaItem.mSubIndexInRow = iRowSub
    gachaItem.mID = gachaContent.id
    gachaItem.mType = rItem.mType
    gachaItem.mName = rItem.mName
    gachaItem.mNumber = rItem.mGachaNumber
    gachaItem.mLocked = "unlock"
    gachaItem.mIconFrame = rItem.mIconFrame
  else
    gachaItem.mID = gachaContent.id
    gachaItem.mLocked = "lock"
    gachaItem.mIndexInRemainList = -1
    gachaItem.mSubIndexInRow = -1
    table.insert(GameUIGachaBox.GachaLockItem, gachaContent.id)
  end
  return gachaItem
end
function GameUIGachaBox:makeGachaRemainItem(gachaContent)
  if gachaContent == nil then
    return nil
  end
  local remainItem = GachaRemainItem:new()
  local contentItem = gachaContent
  remainItem.mID = gachaContent.id
  remainItem.mIndex = 1
  remainItem.mType = contentItem.item.item_type
  remainItem.mNo = contentItem.item.no
  remainItem.mLevel = contentItem.item.level
  remainItem.mNumber = contentItem.item.number
  remainItem.mName = GameHelper:GetAwardTypeText(contentItem.item.item_type, contentItem.item.number)
  remainItem.mGachaNumber = "x" .. GameUtils.numberConversion(GameHelper:GetAwardCount(contentItem.item.item_type, contentItem.item.number, contentItem.item.no))
  remainItem.mRemainNum = contentItem.cur_num
  remainItem.mTotalNum = contentItem.total_num
  if DynamicResDownloader:IsDynamicStuffByGameItem(contentItem.item) then
    local fullFileName = DynamicResDownloader:GetFullName(contentItem.item.number .. ".png", DynamicResDownloader.resType.PIC)
    local localPath = "data2/" .. fullFileName
    DebugWD("localPath = ", localPath)
    if DynamicResDownloader:IfResExsit(localPath) then
      ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
      remainItem.mIconFrame = "item_" .. contentItem.item.number
    else
      remainItem.mIconFrame = "temp"
      self:AddDownloadPath(contentItem.item.number, remainItem.mID, GameUIGachaBox.donamicDownloadFinishCallbackRemainList)
    end
  else
    remainItem.mIconFrame = GameHelper:GetAwardTypeIconFrameName(contentItem.item.item_type, contentItem.item.number, contentItem.item.no)
  end
  return remainItem
end
function GameUIGachaBox:preRequestGachaList(callback)
  if callback ~= nil then
    callbackFunc = callback
  else
    callbackFunc = nil
  end
  local reqContent = {
    box_id = tonumber(self.subCategory)
  }
  DebugOut("preRequestGachaList", reqContent.box_id)
  NetMessageMgr:SendMsg(NetAPIList.open_box_info_req.Code, reqContent, GameUIGachaBox.preRequestGachaListCallback, true, nil)
end
function GameUIGachaBox.preRequestGachaListCallback(msgType, content)
  if msgType == NetAPIList.open_box_info_ack.Code then
    DebugOut("preRequestGachaListCallback")
    preLoadMsgtype = msgType
    preLoadContent = content
    DebugTable(content)
    DebugOut("GameUIActivityNew.category.TYPE_BOX", GameUIActivityNew.category.TYPE_BOX)
    GameUIActivityNew.setHasOpen(GameUIActivityNew.category.TYPE_BOX, GameUIGachaBox.subCategory, true)
    if callbackFunc ~= nil then
      callbackFunc()
    end
    return true
  elseif msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.open_box_info_req.Code then
    GameUIActivityNew.setHasOpen(GameUIActivityNew.category.TYPE_BOX, GameUIGachaBox.subCategory, false)
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    if content.code == 101006 then
      DebugOut("GameUIGachaBox Module not open")
    end
    if callbackFunc ~= nil then
      callbackFunc()
    end
    return true
  end
  return false
end
function GameUIGachaBox:gotoInstanceDirectly()
  DebugOut("gotoInstanceDirectly")
  GameUIGachaBox:ParseGachaListInfo(preLoadContent)
  GameUIGachaBox:RefreshGachaRemainList()
  GameUIGachaBox:RefreshGachaList()
  GameUIGachaBox:RefreshUI()
end
function GameUIGachaBox:RequestGachaList()
  local reqContent = {
    box_id = self.subCategory
  }
  NetMessageMgr:SendMsg(NetAPIList.open_box_info_req.Code, reqContent, GameUIGachaBox.RequestGachaListCallback, true, nil)
  DebugOut("start request ")
end
function GameUIGachaBox.RequestGachaListCallback(msgType, content)
  if msgType == NetAPIList.open_box_info_ack.Code then
    DebugOut("RequestGachaListCallback")
    DebugTable(content)
    GameUIGachaBox:ParseGachaListInfo(content)
    GameUIGachaBox:RefreshGachaRemainList()
    GameUIGachaBox:RefreshGachaList()
    GameUIGachaBox:RefreshUI()
    return true
  end
  return false
end
function GameUIGachaBox:RefreshUI()
  local resIcon = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(self.mRefreshChestCost)
  local showText = GameLoader:GetGameText("LC_MENU_GALAXY_CHEST_REFRESH_BUTTON")
  if self.mRefreshChestCost.item_type == "item" then
    if self.mRefreshChestCost.no == 0 then
      showText = GameLoader:GetGameText("LC_MENU_SEVER_BOX_OPTIMIZATION")
    end
    self.mFlashObj:InvokeASCallback("_root", "setRefreshButton", resIcon, self.mRefreshChestCost.no, showText)
  else
    if self.mRefreshChestCost.number == 0 then
      showText = GameLoader:GetGameText("LC_MENU_SEVER_BOX_OPTIMIZATION")
    end
    self.mFlashObj:InvokeASCallback("_root", "setRefreshButton", resIcon, self.mRefreshChestCost.number, showText)
  end
  local constStr = GameLoader:GetGameText("LC_MENU_GALAXY_CHEST_GACHA_DESC_TEXT")
  constStr = string.format(constStr, self.mOpenChestCost)
  self.mFlashObj:InvokeASCallback("_root", "setOpenCost", constStr)
  self.mFlashObj:InvokeASCallback("_root", "setTotalRemain", self.totalGachaNum, self.remainGachaNum)
  local TotalBoxNum = #self.processStatus
  local FinalNum = self.processStatus[TotalBoxNum].step
  local info = {}
  for k, v in ipairs(self.processStatus) do
    local item = {}
    item.step = v.step
    item.stat = v.status
    table.insert(info, item)
  end
  local cur = self:CountCurProcess(self.openStateList)
  if FinalNum < cur then
    cur = FinalNum
  end
  DebugOut("cur:", cur)
  DebugTable(self.openStateList)
  DebugOut("FinalNum", FinalNum)
  DebugTable(info)
  self.mFlashObj:InvokeASCallback("_root", "initAwardProcessBar", FinalNum, info, cur)
end
function GameUIGachaBox:CountCurProcess(ProcessList)
  local count = 0
  for k, v in ipairs(ProcessList) do
    if 0 <= v.type_id then
      count = count + 1
    end
  end
  return count
end
function GameUIGachaBox.OnGlobalResourceChange()
  if not GameUIGachaBox.mFlashObj then
    return
  end
  local resource = GameGlobalData:GetData("resource")
  GameUIGachaBox.mFlashObj:InvokeASCallback("_root", "setTotalCredits", GameUtils.numberConversion(resource.credit))
end
function GameUIGachaBox:RefreshGachaList()
  self.mFlashObj:InvokeASCallback("_root", "clearGachaItemsRowList")
  if self.GachaItemsRowList ~= nil then
    self.mFlashObj:InvokeASCallback("_root", "initGachaItemsRowList")
    for i = 1, #self.GachaItemsRowList do
      self.mFlashObj:InvokeASCallback("_root", "addGachaItemsRow", i)
    end
    self.mFlashObj:InvokeASCallback("_root", "setGachaArrowVisible")
  end
end
function GameUIGachaBox:UpdateGachaItemsRow(id)
  local itemIndex = tonumber(id)
  local itemInfo = self.GachaItemsRowList[itemIndex]
  local itemNameList = ""
  local itemNumList = ""
  local itemLockStateList = ""
  local itemIconFrameList = ""
  for i = 1, itemInfo.mGachaItemNum do
    itemNameList = itemNameList .. itemInfo.mGachaItems[i].mName .. "\001"
    itemNumList = itemNumList .. itemInfo.mGachaItems[i].mNumber .. "\001"
    itemLockStateList = itemLockStateList .. itemInfo.mGachaItems[i].mLocked .. "\001"
    itemIconFrameList = itemIconFrameList .. itemInfo.mGachaItems[i].mIconFrame .. "\001"
  end
  self.mFlashObj:InvokeASCallback("_root", "setGachaItemsRow", itemIndex, itemNameList, itemNumList, itemLockStateList, itemIconFrameList)
end
function GameUIGachaBox:UnlockGachaItem(itemsRowIndex, itemIndex)
  local gachaItemRow = GameUIGachaBox.GachaItemsRowList[itemsRowIndex]
  local gachaItem = gachaItemRow.mGachaItems[itemIndex]
  local reqContent = {
    id = gachaItem.mID,
    box_id = self.subCategory
  }
  NetMessageMgr:SendMsg(NetAPIList.open_box_open_req.Code, reqContent, GameUIGachaBox.UnlockGachaItemCallback, true, nil)
end
function GameUIGachaBox.UnlockGachaItemCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.open_box_open_req.Code then
    if content.api == NetAPIList.open_box_open_req.Code then
      if content.code ~= 0 then
        local GameVip = LuaObjectManager:GetLuaObject("GameVip")
        GameVip:CheckIsNeedShowVip(content.code)
      end
      return true
    end
  elseif msgType == NetAPIList.open_box_open_ack.Code then
    GameUIGachaBox.mOpenChestCost = content.now_credit_spend
    GameUIGachaBox.processStatus = content.progress_bar
    local gachaItem, iRow, iRowSub = GameUIGachaBox:GetGachaItemByID(content.open_state.id)
    local rItem, iRemainRow, iRemainRowSub = GameUIGachaBox:GetGachaRemainItemByID(content.open_state.type_id)
    for i = 1, #GameUIGachaBox.GachaLockItem do
      if GameUIGachaBox.GachaLockItem[i] == content.open_state.id then
        table.remove(GameUIGachaBox.GachaLockItem, i)
      end
    end
    for k, v in ipairs(GameUIGachaBox.openStateList) do
      if v.id == content.open_state.id then
        v.type_id = content.open_state.type_id
        break
      end
    end
    if tonumber(content.open_state.type_id) == GameUIGachaBox.bestRewardItem.id then
      GameUIGachaBox.bestRewardItem.cur_num = GameUIGachaBox.bestRewardItem.cur_num - 1
    end
    gachaItem.mType = rItem.mType
    gachaItem.mName = rItem.mName
    gachaItem.mNumber = rItem.mGachaNumber
    gachaItem.mLocked = "unlock"
    gachaItem.mIconFrame = rItem.mIconFrame
    gachaItem.mIndexInRemainList = content.open_state.type_id
    rItem.mRemainNum = rItem.mRemainNum - 1
    GameUIGachaBox.remainGachaNum = GameUIGachaBox.remainGachaNum - 1
    GameUIGachaBox.mFlashObj:InvokeASCallback("_root", "setGachaItem", iRow, iRowSub, gachaItem.mName, gachaItem.mNumber, gachaItem.mLocked, gachaItem.mIconFrame)
    GameUIGachaBox.mFlashObj:InvokeASCallback("_root", "setGachaRemainItem", iRemainRow, iRemainRowSub, rItem.mName, rItem.mGachaNumber, rItem.mRemainNum, rItem.mTotalNum, rItem.mIconFrame)
    GameUIGachaBox.mFlashObj:InvokeASCallback("_root", "setTotalRemain", GameUIGachaBox.totalGachaNum, GameUIGachaBox.remainGachaNum)
    local constStr = GameLoader:GetGameText("LC_MENU_GALAXY_CHEST_GACHA_DESC_TEXT")
    constStr = string.format(constStr, GameUIGachaBox.mOpenChestCost)
    GameUIGachaBox.mFlashObj:InvokeASCallback("_root", "setOpenCost", constStr)
    local TotalBoxNum = #GameUIGachaBox.processStatus
    local FinalNum = GameUIGachaBox.processStatus[TotalBoxNum].step
    local info = {}
    for k, v in ipairs(GameUIGachaBox.processStatus) do
      local item = {}
      item.step = v.step
      item.stat = v.status
      table.insert(info, item)
    end
    local cur = GameUIGachaBox:CountCurProcess(GameUIGachaBox.openStateList)
    if FinalNum < cur then
      cur = FinalNum
    end
    GameUIGachaBox.mFlashObj:InvokeASCallback("_root", "initAwardProcessBar", FinalNum, info, cur)
    if content.refresh_cost then
      GameUIGachaBox.mRefreshChestCost = content.refresh_cost
    end
    local resIcon = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(GameUIGachaBox.mRefreshChestCost)
    local showText = GameLoader:GetGameText("LC_MENU_GALAXY_CHEST_REFRESH_BUTTON")
    if GameUIGachaBox.mRefreshChestCost.item_type == "item" then
      if GameUIGachaBox.mRefreshChestCost.no == 0 then
        showText = GameLoader:GetGameText("LC_MENU_SEVER_BOX_OPTIMIZATION")
      end
      GameUIGachaBox.mFlashObj:InvokeASCallback("_root", "setRefreshButton", resIcon, GameUIGachaBox.mRefreshChestCost.no, showText)
    else
      if GameUIGachaBox.mRefreshChestCost.number == 0 then
        showText = GameLoader:GetGameText("LC_MENU_SEVER_BOX_OPTIMIZATION")
      end
      GameUIGachaBox.mFlashObj:InvokeASCallback("_root", "setRefreshButton", resIcon, GameUIGachaBox.mRefreshChestCost.number, showText)
    end
    if tonumber(content.open_state.type_id) == GameUIGachaBox.bestRewardItem.id then
      GameUIGachaBox.mFlashObj:InvokeASCallback("_root", "PlayCongratulations")
    end
    return true
  end
  return false
end
function GameUIGachaBox:SetGachaChestOpenInfo(titleStr)
  DebugOut("GameUIGachaBox:SetGachaChestOpenInfo", titleStr)
  if self.mFlashObj then
    DebugOut("self.mFlashObj is ok")
    self.mFlashObj:InvokeASCallback("_root", "setRollTextInfoForBox", titleStr)
  else
    DebugOut("self.mFlashObj is not ok")
  end
end
function GameUIGachaBox:CreateGachaChestInfo(ChestInfos)
  DebugOut("GameUIGachaBox:CreateGachaChestInfo", #ChestInfos.state)
  self.GachaChestInfoStr = ""
  maxInfo = 5
  infoNum = math.min(#ChestInfos.state, 5)
  for i = 1, infoNum do
    local tName = GameUtils:GetUserDisplayName(ChestInfos.state[i].name)
    local tChestName = GameHelper:GetAwardTypeText(ChestInfos.state[i].item.item_type, ChestInfos.state[i].item.number)
    local tChestNum = GameHelper:GetAwardCount(ChestInfos.state[i].item.item_type, ChestInfos.state[i].item.number, ChestInfos.state[i].item.no)
    local str = GameLoader:GetGameText("LC_MENU_MEDAL_OPENBOX_ACHIEVE")
    str = string.gsub(str, "<player>", tName)
    str = string.gsub(str, "<item_name>", tChestName)
    str = string.gsub(str, "<number>", "" .. tChestNum)
    self.GachaChestInfoStr = self.GachaChestInfoStr .. str .. "    "
  end
end
function GameUIGachaBox:RequestRefreshGachaRemainList()
  local reqContent = {
    box_id = self.subCategory
  }
  NetMessageMgr:SendMsg(NetAPIList.open_box_refresh_req.Code, reqContent, GameUIGachaBox.RequestRefreshGachaRemainListCallback, true, nil)
end
function GameUIGachaBox.RequestRefreshGachaRemainListCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.open_box_refresh_req.Code then
    GameUIGachaBox.mFlashObj:InvokeASCallback("_root", "EnableRefreshBtn")
    if content.api == NetAPIList.open_box_refresh_req.Code then
      if content.code ~= 0 then
        local GameVip = LuaObjectManager:GetLuaObject("GameVip")
        GameVip:CheckIsNeedShowVip(content.code)
      end
      return true
    end
  elseif msgType == NetAPIList.open_box_refresh_ack.Code then
    GameUIGachaBox.mFlashObj:InvokeASCallback("_root", "EnableRefreshBtn")
    GameUIGachaBox.totalGachaNum = 0
    GameUIGachaBox.remainGachaNum = 0
    DebugOut("hello refresh")
    DebugTable(content)
    GameUIGachaBox:ParseGachaListInfo(content)
    GameUIGachaBox:RefreshGachaRemainList()
    GameUIGachaBox:RefreshGachaList()
    GameUIGachaBox:RefreshUI()
    return true
  end
  return false
end
function GameUIGachaBox:RefreshGachaRemainList()
  self.mFlashObj:InvokeASCallback("_root", "clearGachaRemainItemList")
  if self.GachaRemainsRowList ~= nil then
    self.mFlashObj:InvokeASCallback("_root", "initGachaRemainItemList")
    for i = 1, #self.GachaRemainsRowList do
      self.mFlashObj:InvokeASCallback("_root", "addGachaRemainItemsRow", i)
    end
    self.mFlashObj:InvokeASCallback("_root", "setRemainArrowVisible")
    if self.finalReward and self.finalReward.item_type == "fleet" then
      do
        local avatar = GameDataAccessHelper:GetFleetAvatar(self.finalReward.number, 1)
        local name = GameLoader:GetGameText("LC_NPC_" .. GameDataAccessHelper:GetCommanderName(self.finalReward.number, 1))
        local vessels_type = GameDataAccessHelper:GetCommanderVesselsType(self.finalReward.number, 1)
        local vessels_text = GameLoader:GetGameText("LC_FLEET_" .. vessels_type)
        local sex = GameDataAccessHelper:GetCommanderSex(self.finalReward.number)
        if sex == 1 then
          sex = "man"
        elseif sex == 0 then
          sex = "woman"
        else
          sex = "unknown"
        end
        local lv = GameGlobalData:GetData("levelinfo").level
        local fleetData = GameGlobalData:GetFleetDetail(self.finalReward.number)
        local req_content = {
          fleet_id = self.finalReward.number,
          level = 0,
          type = 0,
          req_type = 0
        }
        if fleetData == nil then
          local function callback(msgType, content)
            if msgType == NetAPIList.fleet_info_ack.Code then
              GameGlobalData:SetFleetDetail(content.fleet_id, content.fleet_info.fleets[1])
              fleetData = GameGlobalData:GetFleetDetail(GameUIGachaBox.finalReward.number)
              DebugOut("FLEETDATA:")
              DebugTable(fleetData)
              local force = fleetData.force
              local ability = GameDataAccessHelper:GetCommanderAbility(GameUIGachaBox.finalReward.number, 0)
              local rank = GameUtils:GetFleetRankFrame(ability.Rank)
              GameUIGachaBox.mFlashObj:InvokeASCallback("_root", "setFinalRewardFleet", name, avatar, rank, vessels_type, vessels_text, sex, lv, force, self.finalReward.number)
              return true
            elseif msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.fleet_info_req.Code then
              if content.code ~= 0 then
                GameUIGlobalScreen:ShowAlert("error", content.code, nil)
              end
              return true
            end
            return false
          end
          NetMessageMgr:SendMsg(NetAPIList.fleet_info_req.Code, req_content, callback, true, nil)
          return
        end
        local force = fleetData.force
        local ability = GameDataAccessHelper:GetCommanderAbility(self.finalReward.number, 0)
        local rank = GameUtils:GetFleetRankFrame(ability.Rank)
        self.mFlashObj:InvokeASCallback("_root", "setFinalRewardFleet", name, avatar, rank, vessels_type, vessels_text, sex, lv, force, self.finalReward.number)
      end
    elseif self.finalReward then
      do
        local iconFrame = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(self.finalReward)
        local nameText = GameHelper:GetAwardTypeText(self.finalReward.item_type, self.finalReward.number)
        local level = 1
        if not GameHelper:IsResource(self.finalReward.item_type) then
          level = GameDataAccessHelper:GetItemReqLevel(self.finalReward.number)
          if level == -1 then
            local itemIDList = {}
            table.insert(itemIDList, self.finalReward.number)
            local requestParam = {ids = itemIDList}
            local function callbackfun(msgType, content)
              if msgType == NetAPIList.items_ack.Code then
                if #content.items >= 1 then
                  GameData.Item.Keys[content.items[1].id] = {}
                  GameData.Item.Keys[content.items[1].id].ITEM_TYPE = content.items[1].id
                  GameData.Item.Keys[content.items[1].id].ITEM_NAME = ""
                  GameData.Item.Keys[content.items[1].id].ITEM_DESC = ""
                  GameData.Item.Keys[content.items[1].id].PRICE = content.items[1].price
                  GameData.Item.Keys[content.items[1].id].REQ_LEVEL = content.items[1].level_req
                  GameData.Item.Keys[content.items[1].id].SP_TYPE = content.items[1].sp_type
                  GameData.Item.Keys[content.items[1].id].USE_MAX = content.items[1].use_max
                  GameData.Item.Keys[content.items[1].id].LOT = content.items[1].use_more
                  GameData.Item.Keys[content.items[1].id].MAX_LEVEL = content.items[1].max_level
                  DebugOut("enenene")
                  GameUIGachaBox.mFlashObj:InvokeASCallback("_root", "setFinalRewardItem", iconFrame, nameText, content.items[1].level_req)
                end
                return true
              end
              return false
            end
            NetMessageMgr:SendMsg(NetAPIList.items_req.Code, requestParam, callbackfun, true, nil)
            return
          end
        end
        DebugOut("setFinalRewardItem")
        self.mFlashObj:InvokeASCallback("_root", "setFinalRewardItem", iconFrame, nameText, level)
      end
    end
  end
end
function GameUIGachaBox:UpdateRemainItemsRow(id)
  local itemIndex = tonumber(id)
  local itemInfo = self.GachaRemainsRowList[itemIndex]
  DebugTable(itemInfo)
  local itemNameList = ""
  local itemGachaNumList = ""
  local itemRemainNumList = ""
  local itemTotalNumList = ""
  local itemIconFrameList = ""
  local itemIconPicList = ""
  for i = 1, itemInfo.mRemainItemNumber do
    local item = {
      item_type = itemInfo.mRemainItems[i].mType,
      number = itemInfo.mRemainItems[i].mNumber,
      no = itemInfo.mRemainItems[i].mNo
    }
    local info = GameHelper:GetItemInfo(item)
    itemNameList = itemNameList .. itemInfo.mRemainItems[i].mName .. "\001"
    itemGachaNumList = itemGachaNumList .. itemInfo.mRemainItems[i].mGachaNumber .. "\001"
    itemRemainNumList = itemRemainNumList .. itemInfo.mRemainItems[i].mRemainNum .. "\001"
    itemTotalNumList = itemTotalNumList .. itemInfo.mRemainItems[i].mTotalNum .. "\001"
    itemIconFrameList = itemIconFrameList .. info.icon_frame .. "\001"
    itemIconPicList = itemIconPicList .. (info.icon_pic or "") .. "\001"
  end
  self.mFlashObj:InvokeASCallback("_root", "setGachaRemainItemsRow", itemIndex, itemNameList, itemGachaNumList, itemRemainNumList, itemTotalNumList, itemIconFrameList, itemIconPicList)
end
function GameUIGachaBox:OnFSCommand(cmd, arg)
  if cmd == "needUpdateGachaItemsRow" then
    self:UpdateGachaItemsRow(tonumber(arg))
  elseif cmd == "needUpdateRemainItem" then
    self:UpdateRemainItemsRow(tonumber(arg))
  elseif cmd == "refreshGachaList" then
    if self.mRefreshChestCost.item_type == "credit" then
      GameUIGachaBox:showCostConfirmPanel(self.mRefreshChestCost.number, "LC_MENU_GALAXY_CHEST_REFRESH_CONFIRM_ALERT", GameUIGachaBox.refreshboxHandler)
      return
    end
    GameUIGachaBox.refreshboxHandler()
  elseif cmd == "RequestRefreshGachaRemainList" then
    GameUIGachaBox:RequestRefreshGachaRemainList()
  elseif cmd == "BounceAndRefreshGachaList" then
    local gachaItem, iRow, iRowSub = GameUIGachaBox:GetGachaItemByID(GameUIGachaBox.randomPos)
    local rItem, iRemainRow, iRemainRowSub = GameUIGachaBox:GetGachaRemainItemByID(GameUIGachaBox.bestRewardItem.id)
    gachaItem.mType = rItem.mType
    gachaItem.mName = rItem.mName
    gachaItem.mNumber = rItem.mGachaNumber
    gachaItem.mLocked = "unlock"
    gachaItem.mIconFrame = rItem.mIconFrame
    gachaItem.mIndexInRemainList = GameUIGachaBox.bestRewardItem.id
    rItem.mRemainNum = rItem.mRemainNum - 1
    GameUIGachaBox.remainGachaNum = GameUIGachaBox.remainGachaNum - 1
    GameUIGachaBox.mFlashObj:InvokeASCallback("_root", "setGachaItem", iRow, iRowSub, gachaItem.mName, gachaItem.mNumber, gachaItem.mLocked, gachaItem.mIconFrame)
    GameUIGachaBox:RequestRefreshGachaRemainList()
  elseif cmd == "gachaItemReleased" then
    local rowIndex, subIndex = arg:match("(%d+)\001(%d+)")
    self.mGachaOpenRowIndex = tonumber(rowIndex)
    self.mGachaOpenSubIndex = tonumber(subIndex)
    local gachaItemRow = GameUIGachaBox.GachaItemsRowList[self.mGachaOpenRowIndex]
    local gachaItem = gachaItemRow.mGachaItems[self.mGachaOpenSubIndex]
    if gachaItem.mLocked == "lock" then
      GameUIGachaBox:showCostConfirmPanel(self.mOpenChestCost, "LC_MENU_GALAXY_CHEST_GACHA_CONFIRM_ALERT", GameUIGachaBox.openboxHandler)
    elseif gachaItem.mLocked == "unlock" then
      local rItem = GameUIGachaBox:GetGachaRemainItemByID(gachaItem.mIndexInRemainList)
      if rItem then
        GameUIGachaBox:showItemDetil(rItem)
      end
    end
  elseif cmd == "remainItemReleased" then
    local rowIndex, subIndex = arg:match("(%d+)\001(%d+)")
    local rItemRow = GameUIGachaBox.GachaRemainsRowList[tonumber(rowIndex)]
    local rItem = rItemRow.mRemainItems[tonumber(subIndex)]
    if rItem then
      GameUIGachaBox:showItemDetil(rItem)
    end
  elseif cmd == "helpReleased" then
    GameUIWDStuff:Show(GameUIWDStuff.menuType.menu_type_help)
    GameUIWDStuff:SetHelpText(GameLoader:GetGameText("LC_MENU_GALAXY_CHEST_HELP_DESC_TEXT"))
  elseif cmd == "closeGacha" then
    GameUIGachaBox:Close()
  elseif cmd == "getBoxReward" then
    GameUIGachaBox:GetBoxReward(tonumber(arg))
  elseif cmd == "showFleetDetail" then
    local fleetData = GameGlobalData:GetFleetDetail(tonumber(arg))
    ItemBox:ShowCommanderDetail2(tonumber(arg), fleetData)
  elseif cmd == "needUpdateBoxRewardItems" then
    local item = self.curBoxAwards.loots[tonumber(arg)]
    local iteminfo = GameHelper:GetItemInfo(item)
    local info = {
      m_icon = iteminfo.icon_frame,
      pic = iteminfo.icon_pic
    }
    info.num = GameHelper:GetAwardCountWithX(item.item_type, item.number, item.no)
    self.mFlashObj:InvokeASCallback("_root", "setBoxAwardListItems", tonumber(arg), info)
  elseif cmd == "TreasureBoxClicked" then
    local m_args = LuaUtils:string_split(arg, "_")
    self.curBoxAwards = self.processStatus[tonumber(m_args[2])]
    DebugOut("enenenenen")
    DebugTable(self.processStatus)
    DebugTable(self.curBoxAwards)
    if self.curBoxAwards then
      self.mFlashObj:InvokeASCallback("_root", "showBoxDetail", tonumber(m_args[1]), #self.curBoxAwards.loots)
    end
  elseif cmd == "showBoxItemDetail" then
    if self.curBoxAwards then
      local item = self.curBoxAwards.loots[tonumber(arg)]
      self:showItemDetailForGameItem(item)
    end
  elseif cmd == "showFinalRewardDetail" then
    local item = self.finalReward
    if item then
      self:showItemDetailForGameItem(item)
    end
  end
end
function GameUIGachaBox:GetBoxReward(box_number)
  local netparam = {}
  netparam.box_id = self.subCategory
  netparam.step = self.processStatus[box_number].step
  local function callback(msgType, content)
    if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.open_box_progress_award_req.Code then
      if content.code == 0 then
        self.processStatus[box_number].status = 2
        self:RefreshUI()
      end
      return true
    end
    return false
  end
  NetMessageMgr:SendMsg(NetAPIList.open_box_progress_award_req.Code, netparam, callback, true, nil)
end
function GameUIGachaBox:AddDownloadPath(itemID, index, callback)
  DebugStore("Add download path", itemID, index)
  local resName = itemID .. ".png"
  local extInfo = {}
  extInfo.index = index
  extInfo.item_id = itemID
  extInfo.resPath = "data2/LAZY_LOAD_dynamic_" .. resName
  DynamicResDownloader:AddDynamicRes(resName, DynamicResDownloader.resType.PIC, extInfo, callback)
end
function GameUIGachaBox.donamicDownloadFinishCallbackRemainList(extInfo)
  if DynamicResDownloader:IfResExsit(extInfo.resPath) and GameUIGachaBox.mFlashObj then
    local frameName = "item_" .. extInfo.item_id
    rItem, iRow, iRowSub = GameUIGachaBox:GetGachaRemainItemByID(extInfo.index)
    if rItem then
      rItem.mIconFrame = frameName
      GameUIGachaBox.mFlashObj:InvokeASCallback("_root", "setGachaRemainItem", iRow, iRowSub, rItem.mName, rItem.mGachaNumber, rItem.mRemainNum, rItem.mTotalNum, rItem.mIconFrame)
      if GameUIGachaBox.GachaItemsRowList then
        for i = 1, #GameUIGachaBox.GachaItemsRowList do
          gachaRow = GameUIGachaBox.GachaItemsRowList[i]
          for k, v in ipairs(gachaRow.mGachaItems) do
            if v.mIndexInRemainList == rItem.mID then
              v.mIconFrame = frameName
              GameUIGachaBox.mFlashObj:InvokeASCallback("_root", "setGachaItem", i, k, v.mName, v.mNumber, v.mLocked, v.mIconFrame)
            end
          end
        end
      end
    end
  end
  return true
end
function GameUIGachaBox:GetGachaRemainItemByID(keyID)
  if GameUIGachaBox.GachaRemainsRowList then
    for i = 1, #GameUIGachaBox.GachaRemainsRowList do
      rItemRow = GameUIGachaBox.GachaRemainsRowList[i]
      for k, v in ipairs(rItemRow.mRemainItems) do
        if v.mID == keyID then
          return v, i, k
        end
      end
    end
  end
  if self.bestRewardItem then
    local res = GameUIGachaBox:makeGachaRemainItem(self.bestRewardItem)
    if res.mID == keyID then
      return res
    end
  end
  return nil, nil, nil
end
function GameUIGachaBox:GetGachaItemByID(keyID)
  if GameUIGachaBox.GachaItemsRowList then
    for i = 1, #GameUIGachaBox.GachaItemsRowList do
      rItemRow = GameUIGachaBox.GachaItemsRowList[i]
      for k, v in ipairs(rItemRow.mGachaItems) do
        if v.mID == keyID then
          return v, i, k
        end
      end
    end
  end
  return nil, nil, nil
end
function GameUIGachaBox:RecentChestNtfHandler(content)
  GameUIGachaBox:CreateGachaChestInfo(content)
  GameUIGachaBox:SetGachaChestOpenInfo(self.GachaChestInfoStr)
end
function GameUIGachaBox:showItemDetailForGameItem(gameItem)
  if GameHelper:IsResource(gameItem.item_type) then
    return
  end
  if gameItem.item_type == "krypton" then
    if gameItem.level == 0 then
      gameItem.level = 1
    end
    local detail = GameUIKrypton:TryQueryKryptonDetail(gameItem.number, gameItem.level, function(msgType, content)
      local ret = GameUIKrypton.QueryKryptonDetailCallBack(msgType, content)
      if ret then
        local tmp = GameGlobalData:GetKryptonDetail(gameItem.number, gameItem.level)
        if tmp == nil then
          return false
        end
        ItemBox:SetKryptonBox(tmp, gameItem.number)
        local operationTable = {}
        operationTable.btnUnloadVisible = false
        operationTable.btnUpgradeVisible = false
        operationTable.btnUseVisible = false
        operationTable.btnDecomposeVisible = false
        ItemBox:ShowKryptonBox(320, 240, operationTable)
      end
      return ret
    end)
    if detail == nil then
    else
      ItemBox:SetKryptonBox(detail, gameItem.number)
      local operationTable = {}
      operationTable.btnUnloadVisible = false
      operationTable.btnUpgradeVisible = false
      operationTable.btnUseVisible = false
      operationTable.btnDecomposeVisible = false
      ItemBox:ShowKryptonBox(320, 240, operationTable)
    end
  end
  ItemBox:ShowGameItem(gameItem)
end
function GameUIGachaBox:showItemDetil(item_)
  if GameHelper:IsResource(item_type) then
    return
  end
  local item = item_
  if item.mType == "krypton" then
    if item.mLevel == 0 then
      item.mLevel = 1
    end
    local detail = GameUIKrypton:TryQueryKryptonDetail(item.mNumber, item.mLevel, function(msgType, content)
      local ret = GameUIKrypton.QueryKryptonDetailCallBack(msgType, content)
      if ret then
        local tmp = GameGlobalData:GetKryptonDetail(item.mNumber, item.mLevel)
        if tmp == nil then
          return false
        end
        ItemBox:SetKryptonBox(tmp, item.mNumber)
        local operationTable = {}
        operationTable.btnUnloadVisible = false
        operationTable.btnUpgradeVisible = false
        operationTable.btnUseVisible = false
        operationTable.btnDecomposeVisible = false
        ItemBox:ShowKryptonBox(320, 240, operationTable)
      end
      return ret
    end)
    if detail == nil then
    else
      ItemBox:SetKryptonBox(detail, item.mNumber)
      local operationTable = {}
      operationTable.btnUnloadVisible = false
      operationTable.btnUpgradeVisible = false
      operationTable.btnUseVisible = false
      operationTable.btnDecomposeVisible = false
      ItemBox:ShowKryptonBox(320, 240, operationTable)
    end
  end
  local titem = {
    item_type = item.mType,
    number = tonumber(item.mNumber),
    no = item.mNo
  }
  ItemBox:ShowGameItem(titem)
end
function GameUIGachaBox:showCostConfirmPanel(costPrice, strID, messageboxHandler)
  local info_content = GameLoader:GetGameText(strID)
  info_content = string.format(info_content, costPrice)
  GameUtils:CreditCostConfirm(info_content, messageboxHandler)
end
function GameUIGachaBox.openboxHandler()
  DebugOut("GameUIGachaBox.mGachaOpenSubIndex", GameUIGachaBox.mGachaOpenSubIndex)
  GameUIGachaBox:UnlockGachaItem(GameUIGachaBox.mGachaOpenRowIndex, GameUIGachaBox.mGachaOpenSubIndex)
end
function GameUIGachaBox.refreshboxHandler()
  GameUIGachaBox.mFlashObj:InvokeASCallback("_root", "DisableRefreshBtn")
  DebugOut("refreshboxHandler")
  DebugTable(GameUIGachaBox.bestRewardItem)
  if GameUIGachaBox.bestRewardItem.cur_num > 0 then
    if GameUIGachaBox.mRefreshChestCost.item_type == "credit" and GameGlobalData:GetData("resource").credit < GameUIGachaBox.mRefreshChestCost.number then
      local GameVip = LuaObjectManager:GetLuaObject("GameVip")
      GameVip:CheckIsNeedShowVip(900003, false)
      GameUIGachaBox.mFlashObj:InvokeASCallback("_root", "EnableRefreshBtn")
      return
    end
    math.randomseed(tostring(os.time()):reverse():sub(1, 6))
    DebugOut("length", #GameUIGachaBox.GachaLockItem)
    if 0 < #GameUIGachaBox.GachaLockItem then
      GameUIGachaBox.randomPos = GameUIGachaBox.GachaLockItem[math.random(1, #GameUIGachaBox.GachaLockItem)]
      local gachaItem, iRow, iRowSub = GameUIGachaBox:GetGachaItemByID(GameUIGachaBox.randomPos)
      DebugOut("iRow, iRowSub, GameUIGachaBox.randomPos", iRow, iRowSub, GameUIGachaBox.randomPos)
      GameUIGachaBox.mFlashObj:InvokeASCallback("_root", "SetItemsRowTail", iRow)
      GameUIGachaBox.mFlashObj:InvokeASCallback("_root", "setGachaItemBounce", iRow, iRowSub)
    else
      GameUIGachaBox:RequestRefreshGachaRemainList()
    end
  else
    GameUIGachaBox:RequestRefreshGachaRemainList()
  end
end
return GameUIGachaBox
