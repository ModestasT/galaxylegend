local Step = GameData.Ladder.Step
table.insert(Step, {
  ID = 1,
  HEAD = "head31",
  LEVEL = 40,
  Dialog = 4,
  Reward = "[{money,100}, {technique,100}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[{lost_fleet,1}]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = 2,
  HEAD = "head40",
  LEVEL = 41,
  Dialog = 3,
  Reward = "[{money,200}, {technique,200}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[{lost_fleet,1}]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = 3,
  HEAD = "head22",
  LEVEL = 42,
  Dialog = 4,
  Reward = "[{money,300}, {technique,300}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[{lost_hp,75}]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = 4,
  HEAD = "head14",
  LEVEL = 43,
  Dialog = 3,
  Reward = "[{money,400}, {technique,400}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[{lost_hp,75}]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = 5,
  HEAD = "head83",
  LEVEL = 44,
  Dialog = 1,
  Reward = "[{money,500}, {technique,500}]",
  Reward2 = "[{item,{1015,2}},{krypton,{51206,1}}]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = 6,
  HEAD = "head35",
  LEVEL = 45,
  Dialog = 4,
  Reward = "[{money,600}, {technique,600}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[{lost_fleet,1}]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = 7,
  HEAD = "head20",
  LEVEL = 46,
  Dialog = 4,
  Reward = "[{money,700}, {technique,700}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[{round,2}]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = 8,
  HEAD = "head21",
  LEVEL = 47,
  Dialog = 3,
  Reward = "[{money,800}, {technique,800}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[{lost_hp,75}]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = 9,
  HEAD = "head19",
  LEVEL = 48,
  Dialog = 4,
  Reward = "[{money,900}, {technique,900}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[{lost_hp,75}]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = 10,
  HEAD = "head83",
  LEVEL = 49,
  Dialog = 3,
  Reward = "[{money,1000}, {technique,1000}]",
  Reward2 = "[{item,{1015,2}}]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 2,
  Force = 100
})
table.insert(Step, {
  ID = 11,
  HEAD = "head23",
  LEVEL = 50,
  Dialog = 4,
  Reward = "[{money,1100}, {technique,1100}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[{lost_hp,75}]",
  Monster_Name = "51.0",
  BackGroud = 2,
  Force = 100
})
table.insert(Step, {
  ID = 12,
  HEAD = "head36",
  LEVEL = 51,
  Dialog = 1,
  Reward = "[{money,1200}, {technique,1200}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[{lost_hp,75}]",
  Monster_Name = "51.0",
  BackGroud = 2,
  Force = 100
})
table.insert(Step, {
  ID = 13,
  HEAD = "head12",
  LEVEL = 52,
  Dialog = 3,
  Reward = "[{money,1300}, {technique,1300}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[{round,2}]",
  Monster_Name = "51.0",
  BackGroud = 2,
  Force = 100
})
table.insert(Step, {
  ID = 14,
  HEAD = "head13",
  LEVEL = 53,
  Dialog = 3,
  Reward = "[{money,1400}, {technique,1400}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[{lost_hp,75}]",
  Monster_Name = "51.0",
  BackGroud = 2,
  Force = 100
})
table.insert(Step, {
  ID = 15,
  HEAD = "head83",
  LEVEL = 54,
  Dialog = 3,
  Reward = "[{money,1500}, {technique,1500}]",
  Reward2 = "[{credit,160}]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 2,
  Force = 100
})
table.insert(Step, {
  ID = 16,
  HEAD = "head36",
  LEVEL = 55,
  Dialog = 4,
  Reward = "[{money,1600}, {technique,1600}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[{lost_fleet,1}]",
  Monster_Name = "51.0",
  BackGroud = 2,
  Force = 100
})
table.insert(Step, {
  ID = 17,
  HEAD = "head16",
  LEVEL = 56,
  Dialog = 5,
  Reward = "[{money,1700}, {technique,1700}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[{lost_fleet,1}]",
  Monster_Name = "51.0",
  BackGroud = 2,
  Force = 100
})
table.insert(Step, {
  ID = 18,
  HEAD = "head34",
  LEVEL = 57,
  Dialog = 3,
  Reward = "[{money,1800}, {technique,1800}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[{lost_fleet,1}]",
  Monster_Name = "51.0",
  BackGroud = 2,
  Force = 100
})
table.insert(Step, {
  ID = 19,
  HEAD = "head33",
  LEVEL = 58,
  Dialog = 1,
  Reward = "[{money,1900}, {technique,1900}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[{lost_hp,75}]",
  Monster_Name = "51.0",
  BackGroud = 3,
  Force = 100
})
table.insert(Step, {
  ID = 20,
  HEAD = "head83",
  LEVEL = 59,
  Dialog = 5,
  Reward = "[{money,2000}, {technique,2000}]",
  Reward2 = "[{item,{1015,2}}]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 3,
  Force = 100
})
table.insert(Step, {
  ID = 21,
  HEAD = "head16",
  LEVEL = 60,
  Dialog = 3,
  Reward = "[{money,2100}, {technique,2100}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[{lost_hp,75}]",
  Monster_Name = "51.0",
  BackGroud = 3,
  Force = 100
})
table.insert(Step, {
  ID = 22,
  HEAD = "head18",
  LEVEL = 61,
  Dialog = 1,
  Reward = "[{money,2200}, {technique,2200}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[{lost_hp,75}]",
  Monster_Name = "51.0",
  BackGroud = 3,
  Force = 100
})
table.insert(Step, {
  ID = 23,
  HEAD = "head18",
  LEVEL = 62,
  Dialog = 3,
  Reward = "[{money,2300}, {technique,2300}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[{lost_fleet,1}]",
  Monster_Name = "51.0",
  BackGroud = 3,
  Force = 100
})
table.insert(Step, {
  ID = 24,
  HEAD = "head8",
  LEVEL = 63,
  Dialog = 4,
  Reward = "[{money,2400}, {technique,2400}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[{lost_hp,75}]",
  Monster_Name = "51.0",
  BackGroud = 3,
  Force = 100
})
table.insert(Step, {
  ID = 25,
  HEAD = "head83",
  LEVEL = 64,
  Dialog = 3,
  Reward = "[{money,2500}, {technique,2500}]",
  Reward2 = "[{item,{1015,2}}]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 3,
  Force = 100
})
table.insert(Step, {
  ID = 26,
  HEAD = "head17",
  LEVEL = 65,
  Dialog = 4,
  Reward = "[{money,2600}, {technique,2600}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[{round,2}]",
  Monster_Name = "51.0",
  BackGroud = 3,
  Force = 100
})
table.insert(Step, {
  ID = 27,
  HEAD = "head9",
  LEVEL = 66,
  Dialog = 3,
  Reward = "[{money,2700}, {technique,2700}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[{round,2}]",
  Monster_Name = "51.0",
  BackGroud = 3,
  Force = 100
})
table.insert(Step, {
  ID = 28,
  HEAD = "head11",
  LEVEL = 67,
  Dialog = 4,
  Reward = "[{money,2800}, {technique,2800}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[{round,2}]",
  Monster_Name = "51.0",
  BackGroud = 4,
  Force = 100
})
table.insert(Step, {
  ID = 29,
  HEAD = "head15",
  LEVEL = 68,
  Dialog = 1,
  Reward = "[{money,2900}, {technique,2900}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[{lost_hp,75}]",
  Monster_Name = "51.0",
  BackGroud = 4,
  Force = 100
})
table.insert(Step, {
  ID = 30,
  HEAD = "head83",
  LEVEL = 69,
  Dialog = 3,
  Reward = "[{money,3000}, {technique,3000}]",
  Reward2 = "[{credit,310}]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 4,
  Force = 100
})
table.insert(Step, {
  ID = 31,
  HEAD = "head39",
  LEVEL = 70,
  Dialog = 1,
  Reward = "[{money,3100}, {technique,3100}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[{round,2}]",
  Monster_Name = "51.0",
  BackGroud = 4,
  Force = 100
})
table.insert(Step, {
  ID = 32,
  HEAD = "head29",
  LEVEL = 71,
  Dialog = 3,
  Reward = "[{money,3200}, {technique,3200}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[{lost_hp,75}]",
  Monster_Name = "51.0",
  BackGroud = 4,
  Force = 100
})
table.insert(Step, {
  ID = 33,
  HEAD = "head40",
  LEVEL = 72,
  Dialog = 1,
  Reward = "[{money,3300}, {technique,3300}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[{lost_hp,75}]",
  Monster_Name = "51.0",
  BackGroud = 4,
  Force = 100
})
table.insert(Step, {
  ID = 34,
  HEAD = "head17",
  LEVEL = 73,
  Dialog = 5,
  Reward = "[{money,3400}, {technique,3400}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[{lost_hp,75}]",
  Monster_Name = "51.0",
  BackGroud = 4,
  Force = 100
})
table.insert(Step, {
  ID = 35,
  HEAD = "head83",
  LEVEL = 74,
  Dialog = 4,
  Reward = "[{money,3500}, {technique,3500}]",
  Reward2 = "[{credit,360}]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 4,
  Force = 100
})
table.insert(Step, {
  ID = 36,
  HEAD = "head10",
  LEVEL = 75,
  Dialog = 3,
  Reward = "[{money,3600}, {technique,3600}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[{lost_fleet,1}]",
  Monster_Name = "51.0",
  BackGroud = 4,
  Force = 100
})
table.insert(Step, {
  ID = 37,
  HEAD = "head39",
  LEVEL = 76,
  Dialog = 3,
  Reward = "[{money,3700}, {technique,3700}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[{lost_fleet,1}]",
  Monster_Name = "51.0",
  BackGroud = 5,
  Force = 100
})
table.insert(Step, {
  ID = 38,
  HEAD = "head17",
  LEVEL = 77,
  Dialog = 3,
  Reward = "[{money,3800}, {technique,3800}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[{lost_fleet,1}]",
  Monster_Name = "51.0",
  BackGroud = 5,
  Force = 100
})
table.insert(Step, {
  ID = 39,
  HEAD = "head10",
  LEVEL = 78,
  Dialog = 1,
  Reward = "[{money,3900}, {technique,3900}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[{round,2}]",
  Monster_Name = "51.0",
  BackGroud = 5,
  Force = 100
})
table.insert(Step, {
  ID = 40,
  HEAD = "head83",
  LEVEL = 80,
  Dialog = 4,
  Reward = "[{money,4000}, {technique,4000}]",
  Reward2 = "[{credit,410}]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 5,
  Force = 100
})
table.insert(Step, {
  ID = 41,
  HEAD = "head26",
  LEVEL = 81,
  Dialog = 5,
  Reward = "[{money,4100}, {technique,4100}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[{lost_fleet,1}]",
  Monster_Name = "51.0",
  BackGroud = 5,
  Force = 100
})
table.insert(Step, {
  ID = 42,
  HEAD = "head29",
  LEVEL = 82,
  Dialog = 4,
  Reward = "[{money,4200}, {technique,4200}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[{round,2}]",
  Monster_Name = "51.0",
  BackGroud = 5,
  Force = 100
})
table.insert(Step, {
  ID = 43,
  HEAD = "head34",
  LEVEL = 83,
  Dialog = 5,
  Reward = "[{money,4300}, {technique,4300}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[{lost_fleet,1}]",
  Monster_Name = "51.0",
  BackGroud = 5,
  Force = 100
})
table.insert(Step, {
  ID = 44,
  HEAD = "head17",
  LEVEL = 84,
  Dialog = 5,
  Reward = "[{money,4400}, {technique,4400}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[{lost_hp,75}]",
  Monster_Name = "51.0",
  BackGroud = 5,
  Force = 100
})
table.insert(Step, {
  ID = 45,
  HEAD = "head83",
  LEVEL = 85,
  Dialog = 3,
  Reward = "[{money,4500}, {technique,4500}]",
  Reward2 = "[{credit,460}]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 5,
  Force = 100
})
table.insert(Step, {
  ID = 46,
  HEAD = "head19",
  LEVEL = 86,
  Dialog = 1,
  Reward = "[{money,4600}, {technique,4600}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[{round,2}]",
  Monster_Name = "51.0",
  BackGroud = 6,
  Force = 100
})
table.insert(Step, {
  ID = 47,
  HEAD = "head39",
  LEVEL = 87,
  Dialog = 5,
  Reward = "[{money,4700}, {technique,4700}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[{lost_fleet,1}]",
  Monster_Name = "51.0",
  BackGroud = 6,
  Force = 100
})
table.insert(Step, {
  ID = 48,
  HEAD = "head42",
  LEVEL = 88,
  Dialog = 4,
  Reward = "[{money,4800}, {technique,4800}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[{round,2}]",
  Monster_Name = "51.0",
  BackGroud = 6,
  Force = 100
})
table.insert(Step, {
  ID = 49,
  HEAD = "head31",
  LEVEL = 89,
  Dialog = 1,
  Reward = "[{money,4900}, {technique,4900}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[{round,2}]",
  Monster_Name = "51.0",
  BackGroud = 6,
  Force = 100
})
table.insert(Step, {
  ID = 50,
  HEAD = "head83",
  LEVEL = 90,
  Dialog = 1,
  Reward = "[{money,5000}, {technique,5000}]",
  Reward2 = "[{credit,510}]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 6,
  Force = 100
})
