local Step = GameData.medal_boss.Step
table.insert(Step, {
  ID = 1,
  HEAD = "head64",
  LEVEL = 90,
  Monster_Name = 134,
  BackGroud = 1
})
table.insert(Step, {
  ID = 2,
  HEAD = "head69",
  LEVEL = 90,
  Monster_Name = 140,
  BackGroud = 1
})
table.insert(Step, {
  ID = 3,
  HEAD = "head74",
  LEVEL = 90,
  Monster_Name = 145,
  BackGroud = 1
})
table.insert(Step, {
  ID = 4,
  HEAD = "head99",
  LEVEL = 90,
  Monster_Name = 170,
  BackGroud = 2
})
table.insert(Step, {
  ID = 5,
  HEAD = "head104",
  LEVEL = 90,
  Monster_Name = 175,
  BackGroud = 2
})
table.insert(Step, {
  ID = 6,
  HEAD = "head80",
  LEVEL = 90,
  Monster_Name = 151,
  BackGroud = 2
})
table.insert(Step, {
  ID = 7,
  HEAD = "head85",
  LEVEL = 90,
  Monster_Name = 156,
  BackGroud = 3
})
table.insert(Step, {
  ID = 8,
  HEAD = "head90",
  LEVEL = 90,
  Monster_Name = 161,
  BackGroud = 3
})
table.insert(Step, {
  ID = 9,
  HEAD = "head100",
  LEVEL = 90,
  Monster_Name = 171,
  BackGroud = 3
})
table.insert(Step, {
  ID = 10,
  HEAD = "head105",
  LEVEL = 90,
  Monster_Name = 176,
  BackGroud = 4
})
table.insert(Step, {
  ID = 11,
  HEAD = "head110",
  LEVEL = 90,
  Monster_Name = 181,
  BackGroud = 4
})
table.insert(Step, {
  ID = 12,
  HEAD = "head115",
  LEVEL = 90,
  Monster_Name = 186,
  BackGroud = 4
})
table.insert(Step, {
  ID = 13,
  HEAD = "head120",
  LEVEL = 90,
  Monster_Name = 190,
  BackGroud = 5
})
table.insert(Step, {
  ID = 14,
  HEAD = "head125",
  LEVEL = 90,
  Monster_Name = 195,
  BackGroud = 5
})
table.insert(Step, {
  ID = 15,
  HEAD = "head130",
  LEVEL = 90,
  Monster_Name = 200,
  BackGroud = 5
})
