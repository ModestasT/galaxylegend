local GameFleetEquipment = LuaObjectManager:GetLuaObject("GameFleetEquipment")
GameFleetEquipment.xunzhang = {}
local xunzhang = GameFleetEquipment.xunzhang
local GameUIStarSystemPort = LuaObjectManager:GetLuaObject("GameUIStarSystemPort")
local GameUIStarSystemPort_SUB = LuaObjectManager:GetLuaObject("GameUIStarSystemPort_SUB")
local GameStateEquipEnhance = GameStateManager.GameStateEquipEnhance
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
function xunzhang:GetFlashObject()
  return (...), GameFleetEquipment
end
function xunzhang:OnEraseFromGameState()
  xunzhang._MaterialsDates = nil
end
function xunzhang:OnFSCommand(cmd, arg)
  if cmd == "ShowMedalMenu" then
    xunzhang.isMedalSlot = true
    if self._AllMedalData == nil or self._AllMedalData.formations == nil or self._AllMedalData.medals == nil or not self.isEverReqMedal or self._MaterialsDates == nil then
      local function netFailedCallback()
        NetMessageMgr:SendMsg(NetAPIList.medal_equip_req.Code, nil, self.RequestMedalDataCallBack, true, netFailedCallback)
      end
      self._AllMedalData = {}
      self.isEverReqMedal = true
      NetMessageMgr:SendMsg(NetAPIList.medal_equip_req.Code, nil, self.RequestMedalDataCallBack, true, netFailedCallback)
    else
      self:RefreshFleetMedal()
    end
    return true
  elseif cmd == "ShowEquipMenu" then
    xunzhang.isMedalSlot = false
    self:SetMedalButtonNews()
    return true
  elseif cmd == "ShowOpenTip" then
    GameTip:Show(string.format(GameLoader:GetGameText("LC_MENU_DEXTER_INFO_2"), 80))
  elseif cmd == "prevSlotMedal" then
    for i = self.curSelectedSlot - 1, 1, -1 do
      if self.medalEquips[i] and self.medalEquips[i].hasMedal then
        self:MedalSelected(i, true)
        break
      end
    end
  elseif cmd == "nextSlotMedal" then
    for i = self.curSelectedSlot + 1, 5 do
      if self.medalEquips[i] and self.medalEquips[i].hasMedal then
        self:MedalSelected(i, true)
        break
      end
    end
  elseif cmd == "MedalSelected" then
    self:MedalSelected(tonumber(arg), false)
    return true
  elseif cmd == "MedalSlotClicked" then
    if TutorialQuestManager.QuestTutorialEquipStarSystemItem:IsActive() and GameGlobalData:GetModuleStatus("medal") and tonumber(arg) == 1 then
      self:GetFlashObject():InvokeASCallback("_root", "SetShowMedelTip", 1, false)
    end
    self.curSelectedSlot = tonumber(arg)
    self:ShowMedalSelectMenu(tonumber(arg))
    return true
  end
  return false
end
function xunzhang:MedalSelected(slot, isSwitch)
  DebugOutPutTable(xunzhang.curFleetMedals, "MedalSelected slot:" .. slot)
  self.hasPrev = false
  self.hasNext = false
  for k, v in pairs(self.medalEquips) do
    if slot > k and v.hasMedal then
      self.hasPrev = true
    elseif slot < k and v.hasMedal then
      self.hasNext = true
    end
  end
  local medal_id = xunzhang.curFleetMedals[slot]
  local medal_detail
  for k, v in ipairs(self._AllMedalData.medals) do
    if v.id == medal_id then
      medal_detail = v
    end
  end
  if not GameStateManager:GetCurrentGameState():IsObjectInState(GameUIStarSystemPort_SUB) then
    GameStateManager:GetCurrentGameState():AddObject(GameUIStarSystemPort_SUB)
  end
  self.curSelectedMedal = medal_id
  self.curSelectedSlot = slot
  DebugOut("MedalSelected", medal_id)
  DebugTable(medal_detail)
  GameUIStarSystemPort.medalEquip:GetTargetPosMedalList(self.curSelectedSlot)
  GameUIStarSystemPort.medalEquip:ShowMedalDetail(medal_detail, true, isSwitch, true)
end
function xunzhang:Hide()
  self:CheckMedalButton(true)
end
xunzhang._AllMedalData = nil
function xunzhang:CheckMedalButton(isEquip)
  DebugOut("CheckMedalButton", isEquip)
  local level_info = GameGlobalData:GetData("levelinfo")
  local buttonStat = 1
  local hasNews = false
  local showTip = false
  local isEquip = isEquip
  if level_info.level >= 80 then
    buttonStat = 2
  end
  if TutorialQuestManager.QuestTutorialEquipStarSystemItem:IsActive() and GameGlobalData:GetModuleStatus("medal") then
    showTip = true
  end
  DebugOut("buttonStat qkjfs456")
  DebugOut(buttonStat)
  self:GetFlashObject():InvokeASCallback("_root", "InitMedalSwitchButtonStat", buttonStat, hasNews, showTip, isEquip, GameStateEquipEnhance:IsEnhanceEnable())
end
function xunzhang.SetMedalButtonNews()
  if xunzhang:GetFlashObject() then
    local content, index = ZhenXinUI():GetCurFleetContent()
    if content then
      local hasNews = GameGlobalData:GetFleetMedalButtonStat(GameFleetEquipment:GetCurMatrixId(), content.identity)
      xunzhang:GetFlashObject():InvokeASCallback("_root", "SetMedalButtonNews", hasNews)
    end
  end
end
function xunzhang.RequestMedalDataCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.medal_equip_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.medal_equip_ack.Code then
    xunzhang._AllMedalData.materials = content.materials
    xunzhang._AllMedalData.formations = content.formations
    DebugOutPutTable(xunzhang._AllMedalData, "RequestMedalDataCallBack")
    xunzhang:RefreshFleetMedal()
    xunzhang:GenerateMaterials(xunzhang._AllMedalData.materials)
    return true
  end
  return false
end
function xunzhang:RefreshFleetMedal()
  local fleetId = xunzhang:GetCurFleetId()
  local curmatrixindex = GameFleetEquipment:GetCurMatrixId()
  xunzhang.curFleetMedals = {}
  DebugOut("RefreshFleetMedal", fleetId, curmatrixindex)
  DebugTable(self._AllMedalData.formations)
  if curmatrixindex and self._AllMedalData and self._AllMedalData.formations then
    for k, v in ipairs(self._AllMedalData.formations) do
      if v.fleet_id == fleetId and v.formation_id == curmatrixindex then
        for k1, v1 in ipairs(v.equips) do
          xunzhang.curFleetMedals[v1.value] = v1.key
        end
      end
    end
  end
  xunzhang:SetMedalEquipSlot()
  xunzhang:GenerateAvailableMedal()
  if TutorialQuestManager.QuestTutorialEquipStarSystemItem:IsActive() and GameGlobalData:GetModuleStatus("medal") and xunzhang:GetFleetMedalNews(1) then
    xunzhang:GetFlashObject():InvokeASCallback("_root", "SetMedalButtonTip", false)
    xunzhang:GetFlashObject():InvokeASCallback("_root", "SetShowMedelTip", 1, true)
  end
end
function xunzhang:SetMedalEquipSlot()
  xunzhang.medalEquips = {}
  DebugOutPutTable(xunzhang.curFleetMedals, "SetMedalEquipSlot")
  for i = 1, 5 do
    if xunzhang.curFleetMedals[i] then
      local medal_id = xunzhang.curFleetMedals[i]
      local medal = self:GetMedalDetailById(medal_id)
      if medal then
        local detail = {}
        detail.hasMedal = true
        detail.iconFrame, detail.picName = GameHelper:GetMedalIconFrameAndPic(medal.type)
        detail.Lv = medal.level
        detail.can_rank_up = medal.can_rank
        detail.id = medal_id
        detail.slot = i
        detail.quality = medal.quality
        detail.rank = medal.rank
        detail.hasNews = xunzhang:GetFleetMedalNews(i)
        xunzhang.medalEquips[#xunzhang.medalEquips + 1] = detail
      end
    else
      local detail = {}
      detail.hasMedal = false
      detail.hasNews = xunzhang:GetFleetMedalNews(i)
      xunzhang.medalEquips[#xunzhang.medalEquips + 1] = detail
    end
  end
  self:GenerateAvailableMedal()
  DebugOutPutTable(xunzhang.medalEquips, "SetMedalEquipSlotsff23")
  self:GetFlashObject():InvokeASCallback("_root", "SetMedalEquipSlot", xunzhang.medalEquips)
end
function xunzhang:GetFleetMedalNews(slot)
  DebugOut("GetFleetMedalNews ", slot)
  local curMatrixIndex = GameFleetEquipment:GetCurMatrixId()
  local newsData = GameGlobalData:GetFleetMedalRedPointDataByFormation(curMatrixIndex, xunzhang:GetCurFleetId())
  if not newsData then
    return false
  else
    for k, v in ipairs(newsData) do
      if v.key == slot then
        return v.value == 1
      end
    end
  end
end
function xunzhang.OnFleetMedalNewsChange()
  if xunzhang:GetFlashObject() then
    local newsArr = {}
    if not xunzhang.curFleetMedals then
      return
    end
    for i = 1, 5 do
      if xunzhang.curFleetMedals[i] then
        local detail = {}
        local medal_id = xunzhang.curFleetMedals[i]
        local medal = xunzhang:GetMedalDetailById(medal_id)
        if medal then
          detail.hasNews = xunzhang:GetFleetMedalNews(i)
          newsArr[#newsArr + 1] = detail
        end
      else
        local detail = {}
        detail.hasNews = xunzhang:GetFleetMedalNews(i)
        newsArr[#newsArr + 1] = detail
      end
    end
    xunzhang:GetFlashObject():InvokeASCallback("_root", "refreshMedalSlotNews", newsArr)
  end
end
function xunzhang:ShowMedalSelectMenu(slot)
  if not GameStateManager:GetCurrentGameState():IsObjectInState(GameUIStarSystemPort_SUB) then
    GameStateManager:GetCurrentGameState():AddObject(GameUIStarSystemPort_SUB)
  end
  self.curSelectedSlot = slot
  GameUIStarSystemPort.medalEquip.isReplace = false
  GameUIStarSystemPort.medalEquip:ShowMedalSelectMenu(slot)
end
function xunzhang:GetCurFleetId()
  local content, index = ZhenXinUI():GetCurFleetContent()
  return content.identity
end
function xunzhang:GetMedalDetailById(id)
  for k, v in ipairs(self._AllMedalData.medals) do
    if v.id == id then
      if not v.haveConfig then
        v = GameUtils:MedalHelper_GetMedalConfig(v, v.type, v.level)
      end
      return v
    end
  end
  return nil
end
function xunzhang:GenerateAvailableMedal()
  self._AllAvailableMedal = {}
  self._CanMaterialMedal = {}
  if self._AllMedalData.medals then
    for k, v in ipairs(self._AllMedalData.medals) do
      if not self:IsMedalTypeEquiped(v.id) and (#v.formation_info == 0 or not self:IsMedalTypeEquipedInCurFormation(v.formation_info) and not self:IsMedalTypeEquipedInTeamLeagueFormation(v.formation_info)) then
        table.insert(self._AllAvailableMedal, v)
      end
    end
    for k, v in ipairs(self._AllMedalData.medals) do
      if #v.formation_info == 0 and not self:IsMedalEquiped(v.id) and v.is_material == true then
        table.insert(self._CanMaterialMedal, v)
      end
    end
    local sorMedal = function(a, b)
      if a.quality > b.quality then
        return true
      elseif a.quality < b.quality then
        return false
      elseif a.rank > b.rank then
        return true
      elseif a.rank < b.rank then
        return false
      else
        return a.level > b.level
      end
    end
    table.sort(self._CanMaterialMedal, sorMedal)
  end
end
function xunzhang:IsMedalTypeEquiped(id)
  for k, v in pairs(self.curFleetMedals) do
    if v == id then
      return true
    end
  end
  return false
end
function xunzhang:IsMedalTypeEquipedInCurFormation(formation_info)
  local retValue = false
  for k, v in pairs(formation_info) do
    if v.formation_id == GameFleetEquipment:GetCurMatrixId() then
      return true
    end
  end
  return retValue
end
function xunzhang:IsMedalTypeEquipedInTeamLeagueFormation(formation_info)
  local retValue = false
  if GameFleetEquipment.isTeamLeague then
    local team1, team2 = GameFleetEquipment:GetOther2TeamIdInTeamLeague()
    for k, v in pairs(formation_info) do
      if v.formation_id == team1 or v.formation_id == team2 then
        return true
      end
    end
  end
  return retValue
end
function xunzhang:IsMedalEquiped(id)
  for k, v in pairs(self.curFleetMedals) do
    if v == id then
      return true
    end
  end
  return false
end
xunzhang.LevelUpMaterialCnt = 0
function xunzhang:GenerateMaterials(pool)
  self._MaterialsDates = {}
  xunzhang.LevelUpMaterialCnt = 0
  table.sort(pool, function(a, b)
    return a.type < b.type
  end)
  for k, v in ipairs(pool) do
    if v.num == 1 then
      table.insert(self._MaterialsDates, v)
      if v.type < 1000 then
        xunzhang.LevelUpMaterialCnt = xunzhang.LevelUpMaterialCnt + 1
      end
    else
      for i = 1, v.num do
        table.insert(self._MaterialsDates, v)
        if v.type < 1000 then
          xunzhang.LevelUpMaterialCnt = xunzhang.LevelUpMaterialCnt + 1
        end
      end
    end
  end
end
function xunzhang:GetMedalCanMaterialCntByType(type, self_id)
  local cnt = 0
  for k, v in ipairs(self._AllMedalData.medals) do
    if #v.formation_info == 0 and not self:IsMedalEquiped(v.id) and v.id ~= self_id and v.type == type and v.level <= 4 then
      cnt = cnt + 1
    end
  end
  return cnt
end
function xunzhang:GetMaterialCntByType(type)
  if xunzhang._AllMedalData.materials then
    for k, v in ipairs(xunzhang._AllMedalData.materials) do
      if v.type == type then
        return v.num
      end
    end
  end
  return 0
end
function xunzhang:AddFormationData(formation_info)
  local formation
  for k, v in ipairs(self._AllMedalData.formations) do
    if v.fleet_id == formation_info.fleet_id and v.formation_id == formation_info.formation_id then
      formation = v
      break
    end
  end
  local medal = self:GetMedalDetailById(formation_info.medal_id)
  if medal then
    local medal_formation_info = {}
    medal_formation_info.medal_id = formation_info.medal_id
    medal_formation_info.fleet_id = formation_info.fleet_id
    medal_formation_info.formation_id = formation_info.formation_id
    medal_formation_info.pos = formation_info.pos
    table.insert(medal.formation_info, medal_formation_info)
    if formation then
      local equip = {
        key = formation_info.medal_id,
        value = formation_info.pos
      }
      table.insert(formation.equips, equip)
    else
      formation_info.equips = {}
      formation_info.equips[1] = {
        key = formation_info.medal_id,
        value = formation_info.pos
      }
      formation_info.medal_id = nil
      formation_info.pos = nil
      table.insert(self._AllMedalData.formations, formation_info)
    end
  end
  DebugTable(medal)
  DebugOutPutTable(self._AllMedalData.formations, "AddFormationData")
end
function xunzhang:EraseFormationData(formation_info)
  local formation
  for k, v in ipairs(self._AllMedalData.formations) do
    if v.fleet_id == formation_info.fleet_id and v.formation_id == formation_info.formation_id then
      formation = v
      break
    end
  end
  if formation then
    local equip = {
      key = formation_info.medal_id,
      value = formation_info.pos
    }
    for k, v in ipairs(formation.equips) do
      if LuaUtils:table_equal(v, equip) then
        table.remove(formation.equips, k)
      end
    end
  end
  DebugTable(formation_info)
  local medal = self:GetMedalDetailById(formation_info.medal_id)
  DebugOutPutTable(medal, "EraseFormationData")
  if medal then
    for k, v in ipairs(medal.formation_info) do
      if LuaUtils:table_equal(v, formation_info) then
        table.remove(medal.formation_info, k)
      end
    end
  end
end
function xunzhang:OnMedalListNTF(content)
  if not self._AllMedalData then
    self._AllMedalData = {}
  end
  if not self._AllMedalData.medals then
    self._AllMedalData.medals = {}
  end
  DebugOutPutTable(content, "OnMedalListNTF " .. #content.medals)
  local allHaveMedal = {}
  for j, w in ipairs(self._AllMedalData.medals) do
    allHaveMedal[w.id] = j
  end
  for k, v in ipairs(content.medals) do
    local idx = allHaveMedal[v.id]
    if idx then
      self._AllMedalData.medals[idx] = v
    else
      table.insert(self._AllMedalData.medals, v)
    end
  end
end
