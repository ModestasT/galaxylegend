local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
InterstellarAdventureAward = luaClass(nil)
function InterstellarAdventureAward:ctor(awardItem, isGetted, isRareAward)
  self.mAwardItem = awardItem
  self.mIsGetted = isGetted
  self.mIsRare = isRareAward
  self.mName = nil
  self.mDisplayName = nil
  self.mIconFrame = nil
  self.mCount = 0
end
function InterstellarAdventureAward:GenerateDisplayData()
  self.mDisplayName = "x" .. GameUtils.numberConversion(GameHelper:GetAwardCount(self.mAwardItem.item_type, self.mAwardItem.number, self.mAwardItem.no))
  self.mIconFrame = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(self.mAwardItem)
end
function InterstellarAdventureAward:GetAwardNameText()
  return ...
end
function InterstellarAdventureAward:GetAwarIcon()
  return ...
end
