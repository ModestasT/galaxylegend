local GameTradeTabBar = LuaObjectManager:GetLuaObject("GameTradeTabBar")
local GameStateTrade = GameStateManager.GameStateTrade
local GameItemBag = LuaObjectManager:GetLuaObject("GameItemBag")
local GameUICommonDialog = LuaObjectManager:GetLuaObject("GameUICommonDialog")
GameTradeTabBar.TAB_EQUIP = 1
GameTradeTabBar.TAB_USE = 2
GameTradeTabBar.TAB_OTHERS = 3
GameTradeTabBar.m_previousState = nil
function GameTradeTabBar:OnInitGame()
  GameGlobalData:RegisterDataChangeCallback("resource", self.RefreshResource)
end
function GameTradeTabBar:OnEraseFromGameState()
  self:UnloadFlashObject()
end
function GameTradeTabBar.RefreshResource()
  local resource = GameGlobalData:GetData("resource")
  if GameStateManager:GetCurrentGameState():IsObjectInState(GameTradeTabBar) then
    GameTradeTabBar:GetFlashObject():InvokeASCallback("_root", "RefreshResource", GameUtils.numberConversion(resource.money), GameUtils.numberConversion(resource.credit))
  end
end
function GameTradeTabBar:Init(initIndex)
  initIndex = initIndex or self.TAB_EQUIP
  self:SelectTab(initIndex)
  self:RefreshResource()
  if self:GetFlashObject() then
    local matrix = GameGlobalData:GetData("matrix")
    self:GetFlashObject():InvokeASCallback("_root", "setTitle", GameLoader:GetGameText("LC_MENU_BAG_CHAR") .. "-" .. GameLoader:GetGameText("LC_MENU_FORMATION_BUTTON") .. tostring(matrix.id))
  end
end
function GameTradeTabBar:showCloseAnim()
  local function callback()
    self:GetFlashObject():InvokeASCallback("_root", "showCloseAnim")
  end
  GameUICommonDialog:PlayStory({11000311}, callback)
end
function GameTradeTabBar:OnFSCommand(cmd, arg)
  if cmd == "ExitMarket" then
    if GameTradeTabBar.m_previousState then
      GameStateManager:SetCurrentGameState(GameTradeTabBar.m_previousState)
      GameTradeTabBar.m_previousState = nil
    else
      GameStateManager:SetCurrentGameState(GameStateTrade.previousState)
    end
    if GameItemBag.EquipTutorialAgain then
      AddFlurryEvent("TutorialItem_ClosePackage", {}, 2)
    end
  end
  if cmd == "tabReleased" then
    local index = tonumber(arg)
    if GameStateTrade.currentTab ~= index then
      self:SelectTab(index)
    end
  end
end
function GameTradeTabBar:SelectTab(tabIndex)
  self:GetFlashObject():InvokeASCallback("_root", "setTab", tabIndex)
  GameStateTrade:SetTab(tabIndex)
end
if AutoUpdate.isAndroidDevice then
  function GameTradeTabBar.OnAndroidBack()
    if GameTradeTabBar.m_previousState then
      GameStateManager:SetCurrentGameState(GameTradeTabBar.m_previousState)
      GameTradeTabBar.m_previousState = nil
    else
      GameStateManager:SetCurrentGameState(GameStateTrade.previousState)
    end
  end
end
