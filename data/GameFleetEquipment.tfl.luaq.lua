local GameFleetEquipment = LuaObjectManager:GetLuaObject("GameFleetEquipment")
local GameFleetInfoBackground = LuaObjectManager:GetLuaObject("GameFleetInfoBackground")
local GameStateEquipEnhance = GameStateManager.GameStateEquipEnhance
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local QuestTutorialEnhance = TutorialQuestManager.QuestTutorialEnhance
local QuestTutorialBattleMap = TutorialQuestManager.QuestTutorialBattleMap
local QuestTutorialEquipAllByOneKey = TutorialQuestManager.QuestTutorialEquipAllByOneKey
local QuestTutorialEnhance_second = TutorialQuestManager.QuestTutorialEnhance_second
local QuestTutorialEnhance_third = TutorialQuestManager.QuestTutorialEnhance_third
local QuestTutorialBuildAcademy = TutorialQuestManager.QuestTutorialBuildAcademy
local GameUICommonDialog = LuaObjectManager:GetLuaObject("GameUICommonDialog")
local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
local GameVip = LuaObjectManager:GetLuaObject("GameVip")
local GameUIStarSystemPort = LuaObjectManager:GetLuaObject("GameUIStarSystemPort")
local GameUIStarSystemPort_SUB = LuaObjectManager:GetLuaObject("GameUIStarSystemPort_SUB")
local GameUIBattleResult = LuaObjectManager:GetLuaObject("GameUIBattleResult")
local GameUIWDStuff = LuaObjectManager:GetLuaObject("GameUIWDStuff")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
GameFleetEquipment.currentSelect = nil
require("FleetMatrix.tfl")
GameFleetEquipment.functionParam = {}
GameFleetEquipment.isTeamLeague = false
function GameFleetEquipment.OnGlobalResourceChange()
  DebugOut("OnGlobalResourceChange")
  local resource = GameGlobalData:GetData("resource")
  if GameFleetEquipment:GetFlashObject() then
    GameFleetEquipment:GetFlashObject():InvokeASCallback("_root", "setKryptonRes", GameUtils.numberConversion(resource.kenergy))
  end
  ChuanCangUI():setArtPoint(resource.art_point)
end
function GameFleetEquipment:OnInitGame()
  self.m_currentSelectFleetIndex = 1
  GameGlobalData:RegisterDataChangeCallback("resource", self.OnGlobalResourceChange)
  GameGlobalData:RegisterDataChangeCallback("fleetkryptons", KeJinUI().RefreshKrypton)
  GameGlobalData:RegisterDataChangeCallback("FleetMedalButtonRedPoint", XunZhangUI().SetMedalButtonNews)
  GameGlobalData:RegisterDataChangeCallback("FleetMedalRedPoint", XunZhangUI().OnFleetMedalNewsChange)
  ZhenXinUI():OnInitGame()
  KeJinUI().initFleetIndex = 1
  KeJinUI().equipOffDestSlot = -1
  KeJinUI().equipOffSrcSlot = -1
  KeJinUI().equipDestSlot = -1
  KeJinUI().equipSrcSlot = -1
  KeJinUI().tab = 1
  KeJinUI().initDecomposeBar = false
end
function GameFleetEquipment:Init()
  self.functionParam = {
    [1] = {
      strid = "zhuangbei",
      strlock = "",
      strname = GameLoader:GetGameText("LC_MENU_NEW_FLEET_EQUIPMENT"),
      initSelect = true,
      needlevel = 0
    },
    [2] = {
      strid = "shenjie",
      strlock = "",
      strname = GameLoader:GetGameText("LC_MENU_NEW_FLEET_TRANSFORM"),
      initSelect = false,
      needlevel = 0
    },
    [3] = {
      strid = "kejin",
      strlock = "",
      strname = GameLoader:GetGameText("LC_MENU_KRYPTON_TAB"),
      initSelect = false,
      needlevel = 30
    },
    [4] = {
      strid = "xunzhang",
      strlock = "",
      strname = GameLoader:GetGameText("LC_MENU_NEW_FLEET_MEDAL"),
      initSelect = false,
      needlevel = 80
    },
    [5] = {
      strid = "chuancang",
      strlock = "",
      strname = GameLoader:GetGameText("LC_MENU_NEW_FLEET_CABIN"),
      initSelect = false,
      needlevel = 90
    }
  }
  self:GetFlashObject():InvokeASCallback("_root", "setLocalText", "NOT_IN_BATTLE", GameLoader:GetGameText("LC_MENU_NEW_FLEET_NOT_IN_BATTLE"))
  self:GetFlashObject():InvokeASCallback("_root", "setLocalText", "UnlockSlot_3", string.gsub(GameLoader:GetGameText("LC_MENU_NEW_FLEET_PRESTIGE"), "<number>", 500))
  self:GetFlashObject():InvokeASCallback("_root", "setLocalText", "UnlockSlot_4", string.gsub(GameLoader:GetGameText("LC_MENU_NEW_FLEET_PRESTIGE"), "<number>", 4000))
  self:GetFlashObject():InvokeASCallback("_root", "setLocalText", "UnlockSlot_5", string.gsub(GameLoader:GetGameText("LC_MENU_NEW_FLEET_PRESTIGE"), "<number>", 25000))
  self:GetFlashObject():InvokeASCallback("_root", "setLocalText", "UnLockFunction", GameLoader:GetGameText("LC_MENU_LEVEL_LIMIT_CHAR"))
  local level_info = GameGlobalData:GetData("levelinfo")
  for _, v in ipairs(self.functionParam) do
    if v.strid == "shenjie" then
      local level_info = GameGlobalData:GetData("levelinfo")
      if immanentversion170 == nil then
        if level_info.level < 35 then
          v.strlock = "lock"
        end
      elseif (immanentversion170 == 4 or immanentversion170 == 5) and not GameUtils:IsModuleUnlock("reform") then
        v.strlock = "lock"
      end
    else
      if level_info.level < v.needlevel then
        v.strlock = "lock"
      end
      self:GetFlashObject():InvokeASCallback("_root", "setFunctions_news", v.strid, false)
    end
  end
  self:GetFlashObject():InvokeASCallback("_root", "setFunctions", self.functionParam)
  ZhenXinUI():InitMenu()
  self.OnGlobalResourceChange()
  XunZhangUI():CheckMedalButton(true)
  GameFleetEquipment:SelectFunction("zhuangbei")
  ShenJieUI():InitOnce()
  GameFleetEquipment:RefreshKryptonRedPoint()
end
function GameFleetEquipment:OnAddToGameState()
  if not GameFleetEquipment:GetFlashObject() then
    GameFleetEquipment:LoadFlashObject()
  end
  local fileName = "data2/LAZY_LOAD_newEnhance_bg.png"
  if (ext.crc32.crc32(fileName) == nil or ext.crc32.crc32(fileName) == "") and not AutoUpdateInBackground:IsHeroFileUpdatedToServer(fileName) then
    self:GetFlashObject():ReplaceTexture("LAZY_LOAD_newEnhance_bg.png", "territorial_map_bg.png")
  end
  ZhenXinUI():OnAddToGameState()
  ZhenXinUI():GetDismissFleet()
  self:Init()
  GameFleetEquipment:GetFlashObject():InvokeASCallback("_root", "showmain")
end
function GameFleetEquipment:OnEraseFromGameState()
  if GameFleetEquipment:GetFlashObject() then
    GameFleetEquipment:UnLoadFlashObject()
  end
  ZhenXinUI():OnEraseFromGameState()
  KeJinUI():OnEraseFromGameState()
  ShenJieUI():OnEraseFromGameState()
  ZhuangBeiUI():OnEraseFromGameState()
  XunZhangUI():OnEraseFromGameState()
  ChuanCangUI():OnEraseFromGameState()
  self.currentSelect = nil
  GameFleetEquipment.isTeamLeague = false
end
function GameFleetEquipment:Update(dt)
  local flash_obj = self:GetFlashObject()
  if not flash_obj then
    return
  end
  flash_obj:InvokeASCallback("_root", "DoUpdate", dt)
  flash_obj:Update(dt)
  ChuanCangUI():Update(dt)
  if GameFleetEquipment.dragBagKrypton then
    KeJinUI():UpdataFleetSlotState(KeJinUI().onSelectBagSlot, false)
  end
end
function GameFleetEquipment:OnFSCommand(cmd, arg)
  if self.currentSelect == "chuancang" and ChuanCangUI():OnFSCommand(cmd, arg) then
    return
  end
  if self.currentSelect == "kejin" and KeJinUI():OnFSCommand(cmd, arg) then
    return
  end
  if self.currentSelect == "shenjie" and ShenJieUI():OnFSCommand(cmd, arg) then
    return
  end
  if self.currentSelect == "xunzhang" and XunZhangUI():OnFSCommand(cmd, arg) then
    return
  end
  if self.currentSelect == "zhuangbei" and ZhuangBeiUI():OnFSCommand(cmd, arg) then
    return
  end
  if ZhenXinUI():OnFSCommand(cmd, arg) then
    return
  end
  if cmd == "onClickClose" then
    ZhuangBeiUI():Hide()
    ShenJieUI():Hide()
    if GameStateManager.GameStateEquipEnhance:IsObjectInState(GameUIStarSystemPort) then
      GameStateManager.GameStateEquipEnhance:EraseObject(GameUIStarSystemPort)
    end
    if GameStateManager.GameStateEquipEnhance:IsObjectInState(GameUIStarSystemPort_SUB) then
      GameStateManager.GameStateEquipEnhance:EraseObject(GameUIStarSystemPort_SUB)
    end
    if QuestTutorialEquipAllByOneKey:IsActive() then
      GameUICommonDialog:ForcePlayStory({11000313})
      return
    end
    if GameStateManager.GameStateEquipEnhance.basePrveState then
      GameStateManager.GameStateEquipEnhance.previousState = GameStateManager.GameStateEquipEnhance.basePrveState
      GameStateManager.GameStateEquipEnhance.basePrveState = nil
    end
    if GameStateManager.GameStateEquipEnhance.previousState == GameStateManager.GameStateInstance then
      GameStateManager.GameStateInstance:TryToRecoverLadderData()
    end
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateEquipEnhance.previousState)
    if GameStateManager.GameStateEquipEnhance.battle_failed then
      DebugOut("battle_failed")
      GameStateManager:GetCurrentGameState():AddObject(GameUIBattleResult)
      local param = {cli_data = "equip"}
      NetMessageMgr:SendMsg(NetAPIList.battle_rate_req.Code, param, nil, false, nil)
      GameStateManager.GameStateEquipEnhance.battle_failed = nil
    end
  elseif cmd == "help" then
    GameUIWDStuff:Show(GameUIWDStuff.menuType.menu_type_help)
    GameUIWDStuff:SetHelpText(GameLoader:GetGameText("LC_HELP_CONTENT_32"))
  else
    if cmd == "onClickFunction" then
      for _, v in ipairs(self.functionParam) do
        if v.strid == arg then
          local level_info = GameGlobalData:GetData("levelinfo")
          if v.strid == "shenjie" then
            if immanentversion170 == nil then
              if level_info.level < 35 then
                GameTip:Show(GameLoader:GetGameText("LC_ALERT_REFORM_LOCKED_REQUIRE_LEVER_ALERT"))
                return
              end
            elseif (immanentversion170 == 4 or immanentversion170 == 5) and not GameUtils:IsModuleUnlock("reform") then
              local needLevel = GameDataAccessHelper:GetRightMenuPlayerReqLevel("reform")
              local str = string.format(GameLoader:GetGameText("LC_MENU_Evolution_requirement"), needLevel)
              if needLevel <= level_info.level then
                str = GameLoader:GetGameText("LC_MENU_reform_DESC")
              end
              GameTip:Show(str)
              return
            end
          elseif level_info.level < v.needlevel then
            local txt = GameUtils:TryGetText("LC_MENU_DEXTER_INFO_2", "")
            txt = string.format(txt, v.needlevel)
            GameTip:Show(txt)
            return
          end
        end
      end
      GameFleetEquipment:SelectFunction(arg)
    else
    end
  end
end
function GameFleetEquipment:SelectFunction(strid)
  DebugOut("SelectFunction", self.currentSelect, strid)
  if self.currentSelect == strid then
    return
  end
  if self.currentSelect == "zhuangbei" then
    ZhuangBeiUI():Hide()
  elseif self.currentSelect == "kejin" then
    KeJinUI():Hide()
  elseif self.currentSelect == "shenjie" then
    ShenJieUI():Hide()
  elseif self.currentSelect == "xunzhang" then
    XunZhangUI():Hide()
  elseif self.currentSelect == "chuancang" then
    ChuanCangUI():Hide()
  end
  self.currentSelect = strid
  local fleetInfo = ZhenXinUI():GetCurFleetContent()
  local fleet_info = GameGlobalData:GetData("fleetinfo").fleets
  for k, v in ipairs(fleet_info) do
    if v.identity == fleetInfo.identity then
      fleetidx = k
      break
    end
  end
  assert(fleetidx, "must have " .. fleetInfo.identity)
  if self.currentSelect == "zhuangbei" then
    ZhuangBeiUI():Init(fleetInfo, fleetidx, true)
  elseif self.currentSelect == "kejin" then
    self.currentforce = fleetInfo.force
    GameGlobalData:RegisterDataChangeCallback("fleetinfo", GameFleetEquipment.OnEnhanceFleetinfoChange)
    KeJinUI().RefreshKrypton(fleetidx)
    self:GetFlashObject():InvokeASCallback("_root", "enterKrypton")
  elseif self.currentSelect == "shenjie" then
    ShenJieUI():Init(fleetInfo, fleetidx)
  elseif self.currentSelect == "xunzhang" then
    XunZhangUI():OnFSCommand("ShowMedalMenu")
    self:GetFlashObject():InvokeASCallback("_root", "ShowMedalMenu")
  elseif self.currentSelect == "chuancang" then
    self.currentforce = fleetInfo.force
    self:enterArtifactByID()
    GameGlobalData:RegisterDataChangeCallback("fleetinfo", GameFleetEquipment.OnEnhanceFleetinfoChange)
  else
    assert(false, strid)
  end
  if GameFleetEquipment.currentSelect ~= "xunzhang" then
    XunZhangUI().SetMedalButtonNews()
  end
  if GameFleetEquipment.currentSelect ~= "zhuangbei" then
    ZhuangBeiUI():Init(fleetInfo, fleetidx, false)
  end
  if GameFleetEquipment.currentSelect ~= "shenjie" then
    ShenJieUI():UpdateGrowUpState(fleetInfo.identity)
  end
end
if AutoUpdate.isAndroidDevice then
  function GameFleetEquipment.OnAndroidBack()
    GameFleetEquipment:OnFSCommand("onClickClose")
  end
end
function GameFleetEquipment:GetArtUpgradeStatusUpdate(fleetid)
  local param = {}
  param.fleet_id = fleetid
  NetMessageMgr:SendMsg(NetAPIList.art_can_upgrade_req.Code, param, GameFleetEquipment.GetArtUpgradeStatusUpdateCallback, true, nil)
end
function GameFleetEquipment.GetArtUpgradeStatusUpdateCallback(msgType, content)
  if msgType == NetAPIList.art_can_upgrade_ack.Code then
    GameFleetEquipment.art_canupgrade = content.can_upgrade
    return true
  end
  return false
end
function GameFleetEquipment:onBeginDragItem(dragitem)
  KeJinUI():onBeginDragItem(dragitem)
end
function GameFleetEquipment:onEndDragItem(...)
  KeJinUI():onEndDragItem()
end
function GameFleetEquipment:UpdateFleetLevelInfo(...)
  KeJinUI():UpdateFleetLevelInfo()
end
function GameFleetEquipment:GetAllFleetGrowUpDate(...)
  KeJinUI():GetAllFleetGrowUpDate()
end
function GameFleetEquipment:ShouldEquip(DraggedItemID)
  KeJinUI():ShouldEquip(DraggedItemID)
end
function GameFleetEquipment:UnloadEquip(DraggedItemID)
  KeJinUI():UnloadEquip(DraggedItemID)
end
function GameFleetEquipment:SetCurrentChargeKrypton(DraggedItemID)
  KeJinUI():SetCurrentChargeKrypton(DraggedItemID)
end
function GameFleetEquipment:EquipKrypton(arg1, arg2)
  KeJinUI():EquipKrypton(arg1, arg2)
end
function GameFleetEquipment:EquipOffKrypton(arg1, arg2)
  KeJinUI():EquipOffKrypton(arg1, arg2)
end
function GameFleetEquipment:SwapBagKryptonPos(arg1, arg2)
  KeJinUI():SwapBagKryptonPos(arg1, arg2)
end
function GameFleetEquipment:RefreshCurFleetKrypton()
  KeJinUI():RefreshCurFleetKrypton()
end
function GameFleetEquipment:SwapFleetKryptonPos(DraggedItemID, kryptonDestSlot)
  KeJinUI():SwapFleetKryptonPos(DraggedItemID, kryptonDestSlot)
end
function GameFleetEquipment:enterArtifactByID()
  local level_info = GameGlobalData:GetData("levelinfo")
  if GameGlobalData:GetModuleStatus("artifact") == false then
    GameTip:Show(GameLoader:GetGameText("LC_MENU_ARTIFACT_LOCKED"))
    return
  end
  if level_info.level < 90 then
    GameTip:Show(string.format(GameLoader:GetGameText("LC_MENU_LEVEL_FUNCTION_OPEN_INFO"), 90))
    return
  end
  local fleetinfo = ZhenXinUI():GetCurFleetContent()
  self.m_currentSelectFleetIndex = self:findIndexInFleetinfoByID(ZhenXinUI():GetCurFleetContent().identity)
  local power = fleetinfo.force
  ChuanCangUI():Show(fleetinfo.identity, power)
  DebugOut("GameArtifact:Show")
end
function GameFleetEquipment:findIndexInFleetinfoByID(ID)
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  for k, v in pairs(fleets) do
    if v.identity == ID then
      return k
    end
  end
end
function GameFleetEquipment.OnEnhanceFleetinfoChange()
  local fleet = ZhenXinUI():GetCurFleetContent()
  local v = fleet
  DebugOut("GameFleetEquipment.zhuanbei.m_curreetFleetsForce", fleet.identity)
  local enhanceCount = v.force - GameFleetEquipment.currentforce
  DebugOut("force__", GameFleetEquipment.currentforce, v.force)
  GameFleetEquipment.currentforce = v.force
  DebugOut("showEnhanceAnimation", enhanceCount, GameUtils.numberConversion(v.force))
  if GameFleetEquipment:GetFlashObject() then
    GameFleetEquipment:GetFlashObject():InvokeASCallback("_root", "showEnhanceAnimation", enhanceCount, GameUtils.numberConversion(v.force))
  end
  ChuanCangUI():Updatepower(v.force)
end
function GameFleetEquipment.GotoGetMoney()
  local affairBuildInfo = GameGlobalData:GetBuildingInfo("affairs_hall")
  if affairBuildInfo.level >= 1 then
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateAffairInfo)
  else
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateMainPlanet)
    local GameUIBuilding = LuaObjectManager:GetLuaObject("GameUIBuilding")
    GameUIBuilding:DisplayUpgradeDialog("affairs_hall")
  end
end
function GameFleetEquipment.NeedMoreMoney(text)
  GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.Green)
  local cancel = GameLoader:GetGameText("LC_MENU_BUTTON_CAPTION_CANCEL")
  local affairs = GameLoader:GetGameText("LC_MENU_GET_MORE_BUTTON")
  GameUIMessageDialog:SetRightTextButton(cancel)
  GameUIMessageDialog:SetLeftGreenButton(affairs, GameUIKrypton.GotoGetMoney)
  GameUIMessageDialog:Display("", text)
end
function GameFleetEquipment:RefreshKryptonRedPoint(identity)
  local curMatrixIndex = GameFleetEquipment:GetCurMatrixId()
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  local param = {}
  param.matrix_index = curMatrixIndex
  if not identity then
    local fleetinfo = ZhenXinUI():GetCurFleetContent()
    param.fleet_id = fleetinfo.identity
  end
  if identity then
    param.fleet_id = identity
  end
  DebugOut("krypton_redpoint_req", param.matrix_index, param.fleet_id)
  NetMessageMgr:SendMsg(NetAPIList.krypton_redpoint_req.Code, param, GameFleetEquipment.KryptonRedPointCallBack, false, nil)
end
function GameFleetEquipment.KryptonRedPointCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.krypton_redpoint_req.Code then
    if content.code ~= 0 then
      local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.krypton_redpoint_ack.Code then
    if GameFleetEquipment:GetFlashObject() then
      GameFleetEquipment.art_canupgrade = content.show_artfact_red
      DebugOut("RefreshKryptonRedPoint", content.show_krypton_red, content.show_artupgrade_red, content.show_artfact_red)
      DebugTable(content)
      GameFleetEquipment:GetFlashObject():InvokeASCallback("_root", "setKryptonBtnRedPoint", content.show_krypton_red)
      GameFleetEquipment:GetFlashObject():InvokeASCallback("_root", "setArtFactRedPoint", content.show_artupgrade_red or content.show_artfact_red)
    end
    return true
  end
  return false
end
function ZhenXinUI()
  if GameFleetEquipment.isTeamLeague then
    return GameFleetEquipment.zhenxin_teamleague
  else
    return GameFleetEquipment.zhenxin
  end
end
function ZhuangBeiUI()
  return GameFleetEquipment.zhuangbei
end
function KeJinUI()
  return GameFleetEquipment.kejin
end
function XunZhangUI()
  return GameFleetEquipment.xunzhang
end
function ShenJieUI()
  return GameFleetEquipment.shenjie
end
function ChuanCangUI()
  return GameFleetEquipment.chuancang
end
function GameFleetEquipment:GetCurMatrixId()
  if GameFleetEquipment.isTeamLeague then
    if GameFleetEquipment.teamLeagueType == 1 then
      if GameFleetEquipment.curTeamIndex == 1 then
        return 101
      elseif GameFleetEquipment.curTeamIndex == 2 then
        return 102
      elseif GameFleetEquipment.curTeamIndex == 3 then
        return 103
      end
    elseif GameFleetEquipment.teamLeagueType == 2 then
      if GameFleetEquipment.curTeamIndex == 1 then
        return 104
      elseif GameFleetEquipment.curTeamIndex == 2 then
        return 105
      elseif GameFleetEquipment.curTeamIndex == 3 then
        return 106
      end
    end
  else
    return GameGlobalData:GetData("matrix").id
  end
end
function GameFleetEquipment:TeamLeagueMatrix()
  if GameFleetEquipment.teamLeagueType == 1 then
    return (...), FleetTeamLeagueMatrix
  elseif GameFleetEquipment.teamLeagueType == 2 then
    return (...), FleetTeamLeagueMatrix
  end
end
function GameFleetEquipment:GetLeaderFleet()
  local fleetId = 1
  if GameFleetEquipment.isTeamLeague then
    fleetId = GameFleetEquipment:TeamLeagueMatrix():GetLeaderFleet() or 1
  else
    local leaderlist = GameGlobalData:GetData("leaderlist")
    local CurMatrixIndex = GameGlobalData.GlobalData.curMatrixIndex
    fleetId = leaderlist and CurMatrixIndex and (leaderlist.leader_ids[CurMatrixIndex] or 1)
  end
  return fleetId
end
function GameFleetEquipment:GetOther2TeamIdInTeamLeague()
  local curTeamId = GameFleetEquipment:GetCurMatrixId()
  if curTeamId == 101 then
    return 102, 103
  elseif curTeamId == 102 then
    return 101, 103
  elseif curTeamId == 103 then
    return 101, 102
  elseif curTeamId == 104 then
    return 105, 106
  elseif curTeamId == 105 then
    return 104, 106
  elseif curTeamId == 106 then
    return 104, 105
  end
end
