local GameUIPlayerDetailInfo = LuaObjectManager:GetLuaObject("GameUIPlayerDetailInfo")
local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
local GameUIFriend = LuaObjectManager:GetLuaObject("GameUIFriend")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local NetMessageMgr = NetMessageMgr
local NetAPIList = NetAPIList
function GameUIPlayerDetailInfo:OnEraseFromGameState()
  GameUIPlayerDetailInfo:UnloadFlashObject()
end
function GameUIPlayerDetailInfo:Show(id, xPot, yPot)
  self.xPot = xPot
  self.yPot = yPot
  self.id = id
  local friend_detail_req_content = {user_id = id}
  NetMessageMgr:SendMsg(NetAPIList.user_brief_info_req.Code, friend_detail_req_content, self.friendDetailCallback, true, nil)
end
function GameUIPlayerDetailInfo.friendDetailCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and (content.api == NetAPIList.user_brief_info_req.Code or content.api == NetAPIList.add_friend_req.Code) then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.user_brief_info_ack.Code then
    GameUIPlayerDetailInfo.friend_detail = content
    GameStateManager:GetCurrentGameState():AddObject(GameUIPlayerDetailInfo)
    GameUIPlayerDetailInfo:UpdatePlayerDetailInfo()
    return true
  end
  if msgType == NetAPIList.add_friend_ack.Code then
    GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.OK)
    GameUIMessageDialog:Display(GameLoader:GetGameText("LC_MENU_TITLE_SUCCESS_INFO"), GameLoader:GetGameText("LC_MENU_ADD_FRIEND_SUCC"))
    GameUIFriend:AddFriend(content.friend)
    return true
  end
  return false
end
function GameUIPlayerDetailInfo:UpdatePlayerDetailInfo()
  local alliance = self.friend_detail.alliance
  local name = GameUtils:GetUserDisplayName(self.friend_detail.name)
  local force = self.friend_detail.force
  local level = self.friend_detail.level
  local rank = self.friend_detail.rank
  local fleetLevel = 0
  for k, v in pairs(self.friend_detail.cached_fleets) do
    if v.fleet_identity == 1 then
      fleetLevel = v.level
      break
    end
  end
  local avatar = ""
  if self.friend_detail.icon ~= 0 and self.friend_detail.icon ~= 1 then
    avatar = GameDataAccessHelper:GetFleetAvatar(self.friend_detail.icon, fleetLevel)
  else
    avatar = GameUtils:GetPlayerAvatarWithSex(self.friend_detail.icon, fleetLevel)
  end
  DebugOut("alliance  ", alliance, name, force, level, rank, self.xPot, self.yPot)
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  self:GetFlashObject():InvokeASCallback("_root", "setBaseDate", self.xPot, self.yPot)
  self:GetFlashObject():InvokeASCallback("_root", "updateFriendDetailInfo", name, alliance, force, GameLoader:GetGameText("LC_MENU_Level") .. level, rank, avatar)
end
function GameUIPlayerDetailInfo:OnFSCommand(cmd, arg)
  if cmd == "animation_over_moveout" then
    self:GetFlashObject():InvokeASCallback("_root", "hidePopMenu")
    GameStateManager:GetCurrentGameState():EraseObject(self)
  end
  if cmd == "add_to_friend" then
    local content = {
      id = self.id,
      name = self.friend_detail.name
    }
    NetMessageMgr:SendMsg(NetAPIList.add_friend_req.Code, content, self.friendDetailCallback, true, nil)
  elseif cmd == "send_mail" then
    local GameMail = LuaObjectManager:GetLuaObject("GameMail")
    GameStateManager:GetCurrentGameState():AddObject(GameMail)
    GameMail:ShowBox(GameMail.MAILBOX_WRITE_TAB)
    GameMail:SetWrite(self.friend_detail.name, "", "")
  elseif cmd == "view_equipment" then
    local GameUIPlayerView = LuaObjectManager:GetLuaObject("GameUIPlayerView")
    GameUIPlayerView:SetPlayerData(self.friend_detail)
    GameUIPlayerView:ShowPlayerView()
  end
end
if AutoUpdate.isAndroidDevice then
  function GameUIPlayerDetailInfo.OnAndroidBack()
    GameUIPlayerDetailInfo:GetFlashObject():InvokeASCallback("_root", "playerViewMoveOut")
  end
end
