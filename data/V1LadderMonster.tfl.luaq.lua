local Monster = GameData.Ladder.Monster
Monster[5400100] = {
  ID = 5400100,
  vessels = 5400101,
  durability = 5400101,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5400101] = {
  ID = 5400101,
  vessels = 2,
  durability = 750,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5400102] = {
  ID = 5400102,
  vessels = 1,
  durability = 750,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5400103] = {
  ID = 5400103,
  vessels = 2,
  durability = 750,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5400104] = {
  ID = 5400104,
  vessels = 1,
  durability = 750,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5400105] = {
  ID = 5400105,
  vessels = 2,
  durability = 750,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5400106] = {
  ID = 5400106,
  vessels = 2,
  durability = 750,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5400201] = {
  ID = 5400201,
  vessels = 3,
  durability = 750,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5400202] = {
  ID = 5400202,
  vessels = 4,
  durability = 750,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5400203] = {
  ID = 5400203,
  vessels = 2,
  durability = 750,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5400204] = {
  ID = 5400204,
  vessels = 2,
  durability = 750,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5400205] = {
  ID = 5400205,
  vessels = 2,
  durability = 750,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5400206] = {
  ID = 5400206,
  vessels = 2,
  durability = 750,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5400301] = {
  ID = 5400301,
  vessels = 2,
  durability = 750,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5400302] = {
  ID = 5400302,
  vessels = 1,
  durability = 750,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5400303] = {
  ID = 5400303,
  vessels = 2,
  durability = 750,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5400304] = {
  ID = 5400304,
  vessels = 2,
  durability = 750,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5400305] = {
  ID = 5400305,
  vessels = 2,
  durability = 750,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5400306] = {
  ID = 5400306,
  vessels = 4,
  durability = 750,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5400401] = {
  ID = 5400401,
  vessels = 1,
  durability = 750,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5400402] = {
  ID = 5400402,
  vessels = 3,
  durability = 750,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5400403] = {
  ID = 5400403,
  vessels = 2,
  durability = 750,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5400404] = {
  ID = 5400404,
  vessels = 4,
  durability = 750,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5400405] = {
  ID = 5400405,
  vessels = 1,
  durability = 750,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5400406] = {
  ID = 5400406,
  vessels = 3,
  durability = 750,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5400501] = {
  ID = 5400501,
  vessels = 2,
  durability = 1000,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5400502] = {
  ID = 5400502,
  vessels = 4,
  durability = 1000,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5400503] = {
  ID = 5400503,
  vessels = 1,
  durability = 1000,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5400504] = {
  ID = 5400504,
  vessels = 3,
  durability = 1000,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5400505] = {
  ID = 5400505,
  vessels = 2,
  durability = 1000,
  Avatar = "head13",
  Ship = "ship37"
}
Monster[5400506] = {
  ID = 5400506,
  vessels = 4,
  durability = 6000,
  Avatar = "head83",
  Ship = "ship86"
}
Monster[5400601] = {
  ID = 5400601,
  vessels = 5,
  durability = 900,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5400602] = {
  ID = 5400602,
  vessels = 3,
  durability = 900,
  Avatar = "head13",
  Ship = "ship37"
}
Monster[5400603] = {
  ID = 5400603,
  vessels = 2,
  durability = 900,
  Avatar = "head13",
  Ship = "ship37"
}
Monster[5400604] = {
  ID = 5400604,
  vessels = 4,
  durability = 900,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5400605] = {
  ID = 5400605,
  vessels = 5,
  durability = 900,
  Avatar = "head13",
  Ship = "ship37"
}
Monster[5400606] = {
  ID = 5400606,
  vessels = 2,
  durability = 900,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5400701] = {
  ID = 5400701,
  vessels = 1,
  durability = 1000,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5400702] = {
  ID = 5400702,
  vessels = 2,
  durability = 1000,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5400703] = {
  ID = 5400703,
  vessels = 3,
  durability = 1000,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5400704] = {
  ID = 5400704,
  vessels = 1,
  durability = 1000,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5400705] = {
  ID = 5400705,
  vessels = 2,
  durability = 1000,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5400706] = {
  ID = 5400706,
  vessels = 1,
  durability = 1000,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5400801] = {
  ID = 5400801,
  vessels = 2,
  durability = 1000,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5400802] = {
  ID = 5400802,
  vessels = 1,
  durability = 1000,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5400803] = {
  ID = 5400803,
  vessels = 2,
  durability = 1000,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5400804] = {
  ID = 5400804,
  vessels = 2,
  durability = 1000,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5400805] = {
  ID = 5400805,
  vessels = 2,
  durability = 1000,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5400806] = {
  ID = 5400806,
  vessels = 3,
  durability = 1000,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5400901] = {
  ID = 5400901,
  vessels = 4,
  durability = 1000,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5400902] = {
  ID = 5400902,
  vessels = 2,
  durability = 1000,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5400903] = {
  ID = 5400903,
  vessels = 2,
  durability = 1000,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5400904] = {
  ID = 5400904,
  vessels = 1,
  durability = 1000,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5400905] = {
  ID = 5400905,
  vessels = 4,
  durability = 1000,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5400906] = {
  ID = 5400906,
  vessels = 2,
  durability = 1100,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5401001] = {
  ID = 5401001,
  vessels = 1,
  durability = 1600,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5401002] = {
  ID = 5401002,
  vessels = 2,
  durability = 1600,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5401003] = {
  ID = 5401003,
  vessels = 2,
  durability = 1600,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5401004] = {
  ID = 5401004,
  vessels = 4,
  durability = 1600,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5401005] = {
  ID = 5401005,
  vessels = 2,
  durability = 1600,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5401006] = {
  ID = 5401006,
  vessels = 2,
  durability = 4000,
  Avatar = "head83",
  Ship = "ship86"
}
Monster[5401101] = {
  ID = 5401101,
  vessels = 2,
  durability = 1000,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5401102] = {
  ID = 5401102,
  vessels = 2,
  durability = 1000,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5401103] = {
  ID = 5401103,
  vessels = 4,
  durability = 1000,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5401104] = {
  ID = 5401104,
  vessels = 1,
  durability = 1000,
  Avatar = "head8",
  Ship = "ship5"
}
Monster[5401105] = {
  ID = 5401105,
  vessels = 2,
  durability = 1000,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5401106] = {
  ID = 5401106,
  vessels = 2,
  durability = 1000,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5401201] = {
  ID = 5401201,
  vessels = 2,
  durability = 1000,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5401202] = {
  ID = 5401202,
  vessels = 2,
  durability = 1000,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5401203] = {
  ID = 5401203,
  vessels = 2,
  durability = 1000,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5401204] = {
  ID = 5401204,
  vessels = 2,
  durability = 1000,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5401205] = {
  ID = 5401205,
  vessels = 2,
  durability = 1000,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5401206] = {
  ID = 5401206,
  vessels = 1,
  durability = 1000,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5401301] = {
  ID = 5401301,
  vessels = 2,
  durability = 1000,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5401302] = {
  ID = 5401302,
  vessels = 3,
  durability = 1000,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5401303] = {
  ID = 5401303,
  vessels = 1,
  durability = 1000,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5401304] = {
  ID = 5401304,
  vessels = 2,
  durability = 1000,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5401305] = {
  ID = 5401305,
  vessels = 1,
  durability = 1000,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5401306] = {
  ID = 5401306,
  vessels = 2,
  durability = 1000,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5401401] = {
  ID = 5401401,
  vessels = 1,
  durability = 1100,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5401402] = {
  ID = 5401402,
  vessels = 2,
  durability = 1100,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5401403] = {
  ID = 5401403,
  vessels = 2,
  durability = 1100,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5401404] = {
  ID = 5401404,
  vessels = 2,
  durability = 1100,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5401405] = {
  ID = 5401405,
  vessels = 3,
  durability = 1100,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5401406] = {
  ID = 5401406,
  vessels = 4,
  durability = 1100,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5401501] = {
  ID = 5401501,
  vessels = 2,
  durability = 2000,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5401502] = {
  ID = 5401502,
  vessels = 2,
  durability = 2000,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5401503] = {
  ID = 5401503,
  vessels = 1,
  durability = 2000,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5401504] = {
  ID = 5401504,
  vessels = 4,
  durability = 2000,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5401505] = {
  ID = 5401505,
  vessels = 2,
  durability = 2000,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5401506] = {
  ID = 5401506,
  vessels = 1,
  durability = 6000,
  Avatar = "head83",
  Ship = "ship86"
}
Monster[5401601] = {
  ID = 5401601,
  vessels = 2,
  durability = 1300,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5401602] = {
  ID = 5401602,
  vessels = 2,
  durability = 1300,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5401603] = {
  ID = 5401603,
  vessels = 4,
  durability = 1300,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5401604] = {
  ID = 5401604,
  vessels = 2,
  durability = 1300,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5401605] = {
  ID = 5401605,
  vessels = 2,
  durability = 1300,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5401606] = {
  ID = 5401606,
  vessels = 2,
  durability = 1300,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5401701] = {
  ID = 5401701,
  vessels = 2,
  durability = 1300,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5401702] = {
  ID = 5401702,
  vessels = 4,
  durability = 1300,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5401703] = {
  ID = 5401703,
  vessels = 1,
  durability = 1300,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5401704] = {
  ID = 5401704,
  vessels = 2,
  durability = 1300,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5401705] = {
  ID = 5401705,
  vessels = 2,
  durability = 1300,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5401706] = {
  ID = 5401706,
  vessels = 2,
  durability = 1300,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5401801] = {
  ID = 5401801,
  vessels = 3,
  durability = 1600,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5401802] = {
  ID = 5401802,
  vessels = 2,
  durability = 1600,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5401803] = {
  ID = 5401803,
  vessels = 4,
  durability = 1600,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5401804] = {
  ID = 5401804,
  vessels = 5,
  durability = 1600,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5401805] = {
  ID = 5401805,
  vessels = 3,
  durability = 1600,
  Avatar = "head12",
  Ship = "ship16"
}
Monster[5401806] = {
  ID = 5401806,
  vessels = 2,
  durability = 1600,
  Avatar = "head5",
  Ship = "ship16"
}
Monster[5401901] = {
  ID = 5401901,
  vessels = 1,
  durability = 2000,
  Avatar = "head12",
  Ship = "ship16"
}
Monster[5401902] = {
  ID = 5401902,
  vessels = 2,
  durability = 2000,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5401903] = {
  ID = 5401903,
  vessels = 3,
  durability = 2000,
  Avatar = "head12",
  Ship = "ship5"
}
Monster[5401904] = {
  ID = 5401904,
  vessels = 4,
  durability = 2000,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5401905] = {
  ID = 5401905,
  vessels = 5,
  durability = 2000,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5401906] = {
  ID = 5401906,
  vessels = 1,
  durability = 2000,
  Avatar = "head12",
  Ship = "ship16"
}
Monster[5402001] = {
  ID = 5402001,
  vessels = 2,
  durability = 2600,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5402002] = {
  ID = 5402002,
  vessels = 3,
  durability = 2600,
  Avatar = "head12",
  Ship = "ship5"
}
Monster[5402003] = {
  ID = 5402003,
  vessels = 4,
  durability = 2600,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5402004] = {
  ID = 5402004,
  vessels = 5,
  durability = 2600,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5402005] = {
  ID = 5402005,
  vessels = 1,
  durability = 2600,
  Avatar = "head12",
  Ship = "ship16"
}
Monster[5402006] = {
  ID = 5402006,
  vessels = 2,
  durability = 8000,
  Avatar = "head83",
  Ship = "ship86"
}
Monster[5402101] = {
  ID = 5402101,
  vessels = 3,
  durability = 2200,
  Avatar = "head12",
  Ship = "ship5"
}
Monster[5402102] = {
  ID = 5402102,
  vessels = 4,
  durability = 2200,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5402103] = {
  ID = 5402103,
  vessels = 5,
  durability = 2200,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5402104] = {
  ID = 5402104,
  vessels = 1,
  durability = 2200,
  Avatar = "head12",
  Ship = "ship16"
}
Monster[5402105] = {
  ID = 5402105,
  vessels = 2,
  durability = 2200,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5402106] = {
  ID = 5402106,
  vessels = 3,
  durability = 2200,
  Avatar = "head12",
  Ship = "ship5"
}
Monster[5402201] = {
  ID = 5402201,
  vessels = 4,
  durability = 2200,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5402202] = {
  ID = 5402202,
  vessels = 5,
  durability = 2200,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5402203] = {
  ID = 5402203,
  vessels = 1,
  durability = 2200,
  Avatar = "head12",
  Ship = "ship16"
}
Monster[5402204] = {
  ID = 5402204,
  vessels = 2,
  durability = 2200,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5402205] = {
  ID = 5402205,
  vessels = 3,
  durability = 2200,
  Avatar = "head12",
  Ship = "ship5"
}
Monster[5402206] = {
  ID = 5402206,
  vessels = 4,
  durability = 2200,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5402301] = {
  ID = 5402301,
  vessels = 5,
  durability = 2400,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5402302] = {
  ID = 5402302,
  vessels = 1,
  durability = 2400,
  Avatar = "head12",
  Ship = "ship16"
}
Monster[5402303] = {
  ID = 5402303,
  vessels = 2,
  durability = 2400,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5402304] = {
  ID = 5402304,
  vessels = 3,
  durability = 2400,
  Avatar = "head12",
  Ship = "ship5"
}
Monster[5402305] = {
  ID = 5402305,
  vessels = 4,
  durability = 2400,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5402306] = {
  ID = 5402306,
  vessels = 5,
  durability = 2400,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5402401] = {
  ID = 5402401,
  vessels = 1,
  durability = 2600,
  Avatar = "head12",
  Ship = "ship16"
}
Monster[5402402] = {
  ID = 5402402,
  vessels = 2,
  durability = 2600,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5402403] = {
  ID = 5402403,
  vessels = 3,
  durability = 2600,
  Avatar = "head12",
  Ship = "ship5"
}
Monster[5402404] = {
  ID = 5402404,
  vessels = 4,
  durability = 2600,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5402405] = {
  ID = 5402405,
  vessels = 5,
  durability = 2600,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5402406] = {
  ID = 5402406,
  vessels = 1,
  durability = 2600,
  Avatar = "head12",
  Ship = "ship16"
}
Monster[5402501] = {
  ID = 5402501,
  vessels = 2,
  durability = 2800,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5402502] = {
  ID = 5402502,
  vessels = 3,
  durability = 5000,
  Avatar = "head12",
  Ship = "ship5"
}
Monster[5402503] = {
  ID = 5402503,
  vessels = 4,
  durability = 5000,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5402504] = {
  ID = 5402504,
  vessels = 5,
  durability = 5000,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5402505] = {
  ID = 5402505,
  vessels = 1,
  durability = 5000,
  Avatar = "head12",
  Ship = "ship16"
}
Monster[5402506] = {
  ID = 5402506,
  vessels = 2,
  durability = 12000,
  Avatar = "head83",
  Ship = "ship86"
}
Monster[5402601] = {
  ID = 5402601,
  vessels = 3,
  durability = 2600,
  Avatar = "head12",
  Ship = "ship5"
}
Monster[5402602] = {
  ID = 5402602,
  vessels = 4,
  durability = 2600,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5402603] = {
  ID = 5402603,
  vessels = 5,
  durability = 2600,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5402604] = {
  ID = 5402604,
  vessels = 1,
  durability = 2600,
  Avatar = "head12",
  Ship = "ship16"
}
Monster[5402605] = {
  ID = 5402605,
  vessels = 2,
  durability = 2600,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5402606] = {
  ID = 5402606,
  vessels = 3,
  durability = 2600,
  Avatar = "head12",
  Ship = "ship5"
}
Monster[5402701] = {
  ID = 5402701,
  vessels = 4,
  durability = 2600,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5402702] = {
  ID = 5402702,
  vessels = 5,
  durability = 2600,
  Avatar = "head25",
  Ship = "ship47"
}
Monster[5402703] = {
  ID = 5402703,
  vessels = 1,
  durability = 2600,
  Avatar = "head12",
  Ship = "ship16"
}
Monster[5402704] = {
  ID = 5402704,
  vessels = 2,
  durability = 2600,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5402705] = {
  ID = 5402705,
  vessels = 3,
  durability = 2600,
  Avatar = "head12",
  Ship = "ship5"
}
Monster[5402706] = {
  ID = 5402706,
  vessels = 4,
  durability = 2600,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5402801] = {
  ID = 5402801,
  vessels = 5,
  durability = 2600,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5402802] = {
  ID = 5402802,
  vessels = 1,
  durability = 2600,
  Avatar = "head12",
  Ship = "ship16"
}
Monster[5402803] = {
  ID = 5402803,
  vessels = 2,
  durability = 2600,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5402804] = {
  ID = 5402804,
  vessels = 3,
  durability = 2600,
  Avatar = "head12",
  Ship = "ship5"
}
Monster[5402805] = {
  ID = 5402805,
  vessels = 4,
  durability = 2600,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5402806] = {
  ID = 5402806,
  vessels = 5,
  durability = 2600,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5402901] = {
  ID = 5402901,
  vessels = 1,
  durability = 2600,
  Avatar = "head12",
  Ship = "ship16"
}
Monster[5402902] = {
  ID = 5402902,
  vessels = 2,
  durability = 2600,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5402903] = {
  ID = 5402903,
  vessels = 3,
  durability = 2600,
  Avatar = "head12",
  Ship = "ship5"
}
Monster[5402904] = {
  ID = 5402904,
  vessels = 4,
  durability = 2600,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5402905] = {
  ID = 5402905,
  vessels = 5,
  durability = 2600,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5402906] = {
  ID = 5402906,
  vessels = 1,
  durability = 2600,
  Avatar = "head12",
  Ship = "ship16"
}
Monster[5403001] = {
  ID = 5403001,
  vessels = 2,
  durability = 3000,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5403002] = {
  ID = 5403002,
  vessels = 3,
  durability = 3000,
  Avatar = "head12",
  Ship = "ship5"
}
Monster[5403003] = {
  ID = 5403003,
  vessels = 4,
  durability = 8000,
  Avatar = "head24",
  Ship = "ship47"
}
Monster[5403004] = {
  ID = 5403004,
  vessels = 5,
  durability = 8000,
  Avatar = "head12",
  Ship = "ship5"
}
Monster[5403005] = {
  ID = 5403005,
  vessels = 1,
  durability = 8000,
  Avatar = "head12",
  Ship = "ship16"
}
Monster[5403006] = {
  ID = 5403006,
  vessels = 2,
  durability = 30000,
  Avatar = "head83",
  Ship = "ship86"
}
Monster[5403101] = {
  ID = 5403101,
  vessels = 3,
  durability = 3600,
  Avatar = "head12",
  Ship = "ship5"
}
Monster[5403102] = {
  ID = 5403102,
  vessels = 4,
  durability = 3600,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5403103] = {
  ID = 5403103,
  vessels = 5,
  durability = 3600,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5403104] = {
  ID = 5403104,
  vessels = 1,
  durability = 3600,
  Avatar = "head12",
  Ship = "ship16"
}
Monster[5403105] = {
  ID = 5403105,
  vessels = 2,
  durability = 3600,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5403106] = {
  ID = 5403106,
  vessels = 3,
  durability = 3600,
  Avatar = "head12",
  Ship = "ship5"
}
Monster[5403201] = {
  ID = 5403201,
  vessels = 4,
  durability = 3600,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5403202] = {
  ID = 5403202,
  vessels = 5,
  durability = 3600,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5403203] = {
  ID = 5403203,
  vessels = 1,
  durability = 3600,
  Avatar = "head12",
  Ship = "ship16"
}
Monster[5403204] = {
  ID = 5403204,
  vessels = 2,
  durability = 3600,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5403205] = {
  ID = 5403205,
  vessels = 3,
  durability = 3600,
  Avatar = "head12",
  Ship = "ship5"
}
Monster[5403206] = {
  ID = 5403206,
  vessels = 4,
  durability = 3600,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5403301] = {
  ID = 5403301,
  vessels = 5,
  durability = 3600,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5403302] = {
  ID = 5403302,
  vessels = 1,
  durability = 3600,
  Avatar = "head25",
  Ship = "ship47"
}
Monster[5403303] = {
  ID = 5403303,
  vessels = 2,
  durability = 3600,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5403304] = {
  ID = 5403304,
  vessels = 3,
  durability = 3600,
  Avatar = "head12",
  Ship = "ship5"
}
Monster[5403305] = {
  ID = 5403305,
  vessels = 4,
  durability = 3600,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5403306] = {
  ID = 5403306,
  vessels = 5,
  durability = 3600,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5403401] = {
  ID = 5403401,
  vessels = 1,
  durability = 3600,
  Avatar = "head12",
  Ship = "ship16"
}
Monster[5403402] = {
  ID = 5403402,
  vessels = 2,
  durability = 3600,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5403403] = {
  ID = 5403403,
  vessels = 3,
  durability = 3600,
  Avatar = "head12",
  Ship = "ship5"
}
Monster[5403404] = {
  ID = 5403404,
  vessels = 4,
  durability = 3600,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5403405] = {
  ID = 5403405,
  vessels = 5,
  durability = 3600,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5403406] = {
  ID = 5403406,
  vessels = 1,
  durability = 3600,
  Avatar = "head12",
  Ship = "ship16"
}
Monster[5403501] = {
  ID = 5403501,
  vessels = 2,
  durability = 10000,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5403502] = {
  ID = 5403502,
  vessels = 3,
  durability = 10000,
  Avatar = "head12",
  Ship = "ship5"
}
Monster[5403503] = {
  ID = 5403503,
  vessels = 4,
  durability = 10000,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5403504] = {
  ID = 5403504,
  vessels = 5,
  durability = 10000,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5403505] = {
  ID = 5403505,
  vessels = 1,
  durability = 10000,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5403506] = {
  ID = 5403506,
  vessels = 2,
  durability = 70000,
  Avatar = "head83",
  Ship = "ship86"
}
Monster[5403601] = {
  ID = 5403601,
  vessels = 3,
  durability = 1000,
  Avatar = "head12",
  Ship = "ship5"
}
Monster[5403602] = {
  ID = 5403602,
  vessels = 4,
  durability = 1000,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5403603] = {
  ID = 5403603,
  vessels = 5,
  durability = 1000,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5403604] = {
  ID = 5403604,
  vessels = 1,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship16"
}
Monster[5403605] = {
  ID = 5403605,
  vessels = 2,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship47"
}
Monster[5403606] = {
  ID = 5403606,
  vessels = 3,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5403701] = {
  ID = 5403701,
  vessels = 4,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship47"
}
Monster[5403702] = {
  ID = 5403702,
  vessels = 5,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship47"
}
Monster[5403703] = {
  ID = 5403703,
  vessels = 1,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship16"
}
Monster[5403704] = {
  ID = 5403704,
  vessels = 2,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship47"
}
Monster[5403705] = {
  ID = 5403705,
  vessels = 3,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5403706] = {
  ID = 5403706,
  vessels = 4,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship47"
}
Monster[5403801] = {
  ID = 5403801,
  vessels = 5,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship47"
}
Monster[5403802] = {
  ID = 5403802,
  vessels = 1,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship16"
}
Monster[5403803] = {
  ID = 5403803,
  vessels = 2,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship47"
}
Monster[5403804] = {
  ID = 5403804,
  vessels = 3,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5403805] = {
  ID = 5403805,
  vessels = 4,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship47"
}
Monster[5403806] = {
  ID = 5403806,
  vessels = 5,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship47"
}
Monster[5403901] = {
  ID = 5403901,
  vessels = 1,
  durability = 1000,
  Avatar = "head12",
  Ship = "ship5"
}
Monster[5403902] = {
  ID = 5403902,
  vessels = 2,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship47"
}
Monster[5403903] = {
  ID = 5403903,
  vessels = 3,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5403904] = {
  ID = 5403904,
  vessels = 4,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship47"
}
Monster[5403905] = {
  ID = 5403905,
  vessels = 5,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship47"
}
Monster[5403906] = {
  ID = 5403906,
  vessels = 1,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship16"
}
Monster[5404001] = {
  ID = 5404001,
  vessels = 2,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship47"
}
Monster[5404002] = {
  ID = 5404002,
  vessels = 3,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5404003] = {
  ID = 5404003,
  vessels = 4,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship47"
}
Monster[5404004] = {
  ID = 5404004,
  vessels = 5,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship47"
}
Monster[5404005] = {
  ID = 5404005,
  vessels = 1,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship16"
}
Monster[5404006] = {
  ID = 5404006,
  vessels = 2,
  durability = 1000,
  Avatar = "head83",
  Ship = "ship86"
}
Monster[5404101] = {
  ID = 5404101,
  vessels = 3,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5404102] = {
  ID = 5404102,
  vessels = 4,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship47"
}
Monster[5404103] = {
  ID = 5404103,
  vessels = 5,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship47"
}
Monster[5404104] = {
  ID = 5404104,
  vessels = 1,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship16"
}
Monster[5404105] = {
  ID = 5404105,
  vessels = 2,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship47"
}
Monster[5404106] = {
  ID = 5404106,
  vessels = 3,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5404201] = {
  ID = 5404201,
  vessels = 4,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship47"
}
Monster[5404202] = {
  ID = 5404202,
  vessels = 5,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship47"
}
Monster[5404203] = {
  ID = 5404203,
  vessels = 1,
  durability = 1000,
  Avatar = "head24",
  Ship = "ship42"
}
Monster[5404204] = {
  ID = 5404204,
  vessels = 2,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship47"
}
Monster[5404205] = {
  ID = 5404205,
  vessels = 3,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5404206] = {
  ID = 5404206,
  vessels = 4,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship47"
}
Monster[5404301] = {
  ID = 5404301,
  vessels = 5,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship47"
}
Monster[5404302] = {
  ID = 5404302,
  vessels = 1,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship16"
}
Monster[5404303] = {
  ID = 5404303,
  vessels = 2,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship47"
}
Monster[5404304] = {
  ID = 5404304,
  vessels = 3,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5404305] = {
  ID = 5404305,
  vessels = 4,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship47"
}
Monster[5404306] = {
  ID = 5404306,
  vessels = 5,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship47"
}
Monster[5404401] = {
  ID = 5404401,
  vessels = 1,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship16"
}
Monster[5404402] = {
  ID = 5404402,
  vessels = 2,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship47"
}
Monster[5404403] = {
  ID = 5404403,
  vessels = 3,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5404404] = {
  ID = 5404404,
  vessels = 4,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship47"
}
Monster[5404405] = {
  ID = 5404405,
  vessels = 5,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship47"
}
Monster[5404406] = {
  ID = 5404406,
  vessels = 1,
  durability = 1000,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5404501] = {
  ID = 5404501,
  vessels = 2,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship47"
}
Monster[5404502] = {
  ID = 5404502,
  vessels = 3,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5404503] = {
  ID = 5404503,
  vessels = 4,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship47"
}
Monster[5404504] = {
  ID = 5404504,
  vessels = 5,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship47"
}
Monster[5404505] = {
  ID = 5404505,
  vessels = 1,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship16"
}
Monster[5404506] = {
  ID = 5404506,
  vessels = 2,
  durability = 1000,
  Avatar = "head83",
  Ship = "ship86"
}
Monster[5404601] = {
  ID = 5404601,
  vessels = 3,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5404602] = {
  ID = 5404602,
  vessels = 4,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship47"
}
Monster[5404603] = {
  ID = 5404603,
  vessels = 5,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship47"
}
Monster[5404604] = {
  ID = 5404604,
  vessels = 1,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship16"
}
Monster[5404605] = {
  ID = 5404605,
  vessels = 2,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship47"
}
Monster[5404606] = {
  ID = 5404606,
  vessels = 3,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5404701] = {
  ID = 5404701,
  vessels = 4,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship47"
}
Monster[5404702] = {
  ID = 5404702,
  vessels = 5,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship47"
}
Monster[5404703] = {
  ID = 5404703,
  vessels = 1,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship16"
}
Monster[5404704] = {
  ID = 5404704,
  vessels = 2,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship47"
}
Monster[5404705] = {
  ID = 5404705,
  vessels = 3,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5404706] = {
  ID = 5404706,
  vessels = 4,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship47"
}
Monster[5404801] = {
  ID = 5404801,
  vessels = 5,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship47"
}
Monster[5404802] = {
  ID = 5404802,
  vessels = 1,
  durability = 1000,
  Avatar = "head7",
  Ship = "ship42"
}
Monster[5404803] = {
  ID = 5404803,
  vessels = 2,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship47"
}
Monster[5404804] = {
  ID = 5404804,
  vessels = 3,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5404805] = {
  ID = 5404805,
  vessels = 4,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship47"
}
Monster[5404806] = {
  ID = 5404806,
  vessels = 5,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship47"
}
Monster[5404901] = {
  ID = 5404901,
  vessels = 1,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship47"
}
Monster[5404902] = {
  ID = 5404902,
  vessels = 2,
  durability = 1000,
  Avatar = "head7",
  Ship = "ship42"
}
Monster[5404903] = {
  ID = 5404903,
  vessels = 3,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship47"
}
Monster[5404904] = {
  ID = 5404904,
  vessels = 4,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5404905] = {
  ID = 5404905,
  vessels = 5,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship47"
}
Monster[5404906] = {
  ID = 5404906,
  vessels = 6,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship47"
}
Monster[5405001] = {
  ID = 5405001,
  vessels = 1,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship47"
}
Monster[5405002] = {
  ID = 5405002,
  vessels = 2,
  durability = 1000,
  Avatar = "head7",
  Ship = "ship42"
}
Monster[5405003] = {
  ID = 5405003,
  vessels = 3,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship47"
}
Monster[5405004] = {
  ID = 5405004,
  vessels = 4,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5405005] = {
  ID = 5405005,
  vessels = 5,
  durability = 1000,
  Avatar = "head25",
  Ship = "ship47"
}
Monster[5405006] = {
  ID = 5405006,
  vessels = 6,
  durability = 1000,
  Avatar = "head83",
  Ship = "ship86"
}
Monster[5451000] = {
  ID = 5451000,
  vessels = 1,
  durability = 983,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451001] = {
  ID = 5451001,
  vessels = 1,
  durability = 1932,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451002] = {
  ID = 5451002,
  vessels = 1,
  durability = 3025,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451003] = {
  ID = 5451003,
  vessels = 1,
  durability = 5720,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451004] = {
  ID = 5451004,
  vessels = 1,
  durability = 7203,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451005] = {
  ID = 5451005,
  vessels = 1,
  durability = 8831,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451006] = {
  ID = 5451006,
  vessels = 1,
  durability = 10601,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451007] = {
  ID = 5451007,
  vessels = 1,
  durability = 12514,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451008] = {
  ID = 5451008,
  vessels = 1,
  durability = 14572,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451009] = {
  ID = 5451009,
  vessels = 1,
  durability = 16795,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451010] = {
  ID = 5451010,
  vessels = 1,
  durability = 19162,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451011] = {
  ID = 5451011,
  vessels = 1,
  durability = 21677,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451012] = {
  ID = 5451012,
  vessels = 1,
  durability = 24337,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451013] = {
  ID = 5451013,
  vessels = 1,
  durability = 27143,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451014] = {
  ID = 5451014,
  vessels = 1,
  durability = 30094,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451015] = {
  ID = 5451015,
  vessels = 1,
  durability = 33189,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451016] = {
  ID = 5451016,
  vessels = 1,
  durability = 36431,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451017] = {
  ID = 5451017,
  vessels = 1,
  durability = 39819,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451018] = {
  ID = 5451018,
  vessels = 1,
  durability = 43353,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451019] = {
  ID = 5451019,
  vessels = 1,
  durability = 48050,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451020] = {
  ID = 5451020,
  vessels = 1,
  durability = 51990,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451021] = {
  ID = 5451021,
  vessels = 1,
  durability = 57169,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451022] = {
  ID = 5451022,
  vessels = 1,
  durability = 61467,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451023] = {
  ID = 5451023,
  vessels = 1,
  durability = 65918,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451024] = {
  ID = 5451024,
  vessels = 1,
  durability = 70523,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451025] = {
  ID = 5451025,
  vessels = 1,
  durability = 75278,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451026] = {
  ID = 5451026,
  vessels = 1,
  durability = 80188,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451027] = {
  ID = 5451027,
  vessels = 1,
  durability = 85247,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451028] = {
  ID = 5451028,
  vessels = 1,
  durability = 90459,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451029] = {
  ID = 5451029,
  vessels = 1,
  durability = 123643,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451030] = {
  ID = 5451030,
  vessels = 1,
  durability = 131871,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451031] = {
  ID = 5451031,
  vessels = 1,
  durability = 140350,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451032] = {
  ID = 5451032,
  vessels = 1,
  durability = 149079,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451033] = {
  ID = 5451033,
  vessels = 1,
  durability = 158055,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451034] = {
  ID = 5451034,
  vessels = 1,
  durability = 167286,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451035] = {
  ID = 5451035,
  vessels = 1,
  durability = 176767,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451036] = {
  ID = 5451036,
  vessels = 1,
  durability = 186499,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451037] = {
  ID = 5451037,
  vessels = 1,
  durability = 196481,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451038] = {
  ID = 5451038,
  vessels = 1,
  durability = 206710,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451039] = {
  ID = 5451039,
  vessels = 1,
  durability = 242865,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451040] = {
  ID = 5451040,
  vessels = 1,
  durability = 259469,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451041] = {
  ID = 5451041,
  vessels = 1,
  durability = 276530,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451042] = {
  ID = 5451042,
  vessels = 1,
  durability = 294057,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451043] = {
  ID = 5451043,
  vessels = 1,
  durability = 312039,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451044] = {
  ID = 5451044,
  vessels = 1,
  durability = 330486,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451045] = {
  ID = 5451045,
  vessels = 1,
  durability = 349394,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451046] = {
  ID = 5451046,
  vessels = 1,
  durability = 368758,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451047] = {
  ID = 5451047,
  vessels = 1,
  durability = 388588,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451048] = {
  ID = 5451048,
  vessels = 1,
  durability = 408873,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451049] = {
  ID = 5451049,
  vessels = 1,
  durability = 462884,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451050] = {
  ID = 5451050,
  vessels = 1,
  durability = 492725,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451051] = {
  ID = 5451051,
  vessels = 1,
  durability = 523292,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451052] = {
  ID = 5451052,
  vessels = 1,
  durability = 554584,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451053] = {
  ID = 5451053,
  vessels = 1,
  durability = 586602,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451054] = {
  ID = 5451054,
  vessels = 1,
  durability = 619350,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451055] = {
  ID = 5451055,
  vessels = 1,
  durability = 652818,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451056] = {
  ID = 5451056,
  vessels = 1,
  durability = 687012,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451057] = {
  ID = 5451057,
  vessels = 1,
  durability = 721932,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451058] = {
  ID = 5451058,
  vessels = 1,
  durability = 757577,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451059] = {
  ID = 5451059,
  vessels = 1,
  durability = 881005,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451060] = {
  ID = 5451060,
  vessels = 1,
  durability = 934315,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451061] = {
  ID = 5451061,
  vessels = 1,
  durability = 988779,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451062] = {
  ID = 5451062,
  vessels = 1,
  durability = 1044383,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451063] = {
  ID = 5451063,
  vessels = 1,
  durability = 1101135,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451064] = {
  ID = 5451064,
  vessels = 1,
  durability = 1159040,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451065] = {
  ID = 5451065,
  vessels = 1,
  durability = 1218086,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451066] = {
  ID = 5451066,
  vessels = 1,
  durability = 1278286,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451067] = {
  ID = 5451067,
  vessels = 1,
  durability = 1320119,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451068] = {
  ID = 5451068,
  vessels = 1,
  durability = 1402114,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451069] = {
  ID = 5451069,
  vessels = 1,
  durability = 1868676,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451070] = {
  ID = 5451070,
  vessels = 1,
  durability = 2008595,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451071] = {
  ID = 5451071,
  vessels = 1,
  durability = 2151392,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451072] = {
  ID = 5451072,
  vessels = 1,
  durability = 2297074,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451073] = {
  ID = 5451073,
  vessels = 1,
  durability = 2445626,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451074] = {
  ID = 5451074,
  vessels = 1,
  durability = 2597057,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451075] = {
  ID = 5451075,
  vessels = 1,
  durability = 2751366,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451076] = {
  ID = 5451076,
  vessels = 1,
  durability = 2908553,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451077] = {
  ID = 5451077,
  vessels = 1,
  durability = 3068626,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451078] = {
  ID = 5451078,
  vessels = 1,
  durability = 3334755,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451079] = {
  ID = 5451079,
  vessels = 1,
  durability = 4054614,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451080] = {
  ID = 5451080,
  vessels = 1,
  durability = 4367340,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451081] = {
  ID = 5451081,
  vessels = 1,
  durability = 4685941,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451082] = {
  ID = 5451082,
  vessels = 1,
  durability = 5010433,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451083] = {
  ID = 5451083,
  vessels = 1,
  durability = 5340800,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451084] = {
  ID = 5451084,
  vessels = 1,
  durability = 5677058,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451085] = {
  ID = 5451085,
  vessels = 1,
  durability = 6019199,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451086] = {
  ID = 5451086,
  vessels = 1,
  durability = 6367214,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451087] = {
  ID = 5451087,
  vessels = 1,
  durability = 6810332,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451088] = {
  ID = 5451088,
  vessels = 1,
  durability = 7432827,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451089] = {
  ID = 5451089,
  vessels = 1,
  durability = 10752100,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451090] = {
  ID = 5451090,
  vessels = 1,
  durability = 11814914,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451091] = {
  ID = 5451091,
  vessels = 1,
  durability = 12767690,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451092] = {
  ID = 5451092,
  vessels = 1,
  durability = 13618609,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451093] = {
  ID = 5451093,
  vessels = 1,
  durability = 14483628,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451094] = {
  ID = 5451094,
  vessels = 1,
  durability = 15362740,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451095] = {
  ID = 5451095,
  vessels = 1,
  durability = 16255936,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451096] = {
  ID = 5451096,
  vessels = 1,
  durability = 17163233,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451097] = {
  ID = 5451097,
  vessels = 1,
  durability = 18617019,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451098] = {
  ID = 5451098,
  vessels = 1,
  durability = 20045527,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451099] = {
  ID = 5451099,
  vessels = 1,
  durability = 22463910,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451500] = {
  ID = 5451500,
  vessels = 1,
  durability = 1336,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451501] = {
  ID = 5451501,
  vessels = 1,
  durability = 2617,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451502] = {
  ID = 5451502,
  vessels = 1,
  durability = 4092,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451503] = {
  ID = 5451503,
  vessels = 1,
  durability = 7679,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451504] = {
  ID = 5451504,
  vessels = 1,
  durability = 9679,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451505] = {
  ID = 5451505,
  vessels = 1,
  durability = 11874,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451506] = {
  ID = 5451506,
  vessels = 1,
  durability = 14261,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451507] = {
  ID = 5451507,
  vessels = 1,
  durability = 16841,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451508] = {
  ID = 5451508,
  vessels = 1,
  durability = 19617,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451509] = {
  ID = 5451509,
  vessels = 1,
  durability = 22641,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451510] = {
  ID = 5451510,
  vessels = 1,
  durability = 25862,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451511] = {
  ID = 5451511,
  vessels = 1,
  durability = 29284,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451512] = {
  ID = 5451512,
  vessels = 1,
  durability = 32905,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451513] = {
  ID = 5451513,
  vessels = 1,
  durability = 36725,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451514] = {
  ID = 5451514,
  vessels = 1,
  durability = 40744,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451515] = {
  ID = 5451515,
  vessels = 1,
  durability = 44960,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451516] = {
  ID = 5451516,
  vessels = 1,
  durability = 49377,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451517] = {
  ID = 5451517,
  vessels = 1,
  durability = 53994,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451518] = {
  ID = 5451518,
  vessels = 1,
  durability = 58809,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451519] = {
  ID = 5451519,
  vessels = 1,
  durability = 65255,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451520] = {
  ID = 5451520,
  vessels = 1,
  durability = 70702,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451521] = {
  ID = 5451521,
  vessels = 1,
  durability = 78069,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451522] = {
  ID = 5451522,
  vessels = 1,
  durability = 84047,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451523] = {
  ID = 5451523,
  vessels = 1,
  durability = 90243,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451524] = {
  ID = 5451524,
  vessels = 1,
  durability = 96649,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451525] = {
  ID = 5451525,
  vessels = 1,
  durability = 103275,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451526] = {
  ID = 5451526,
  vessels = 1,
  durability = 110114,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451527] = {
  ID = 5451527,
  vessels = 1,
  durability = 117164,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451528] = {
  ID = 5451528,
  vessels = 1,
  durability = 124433,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451529] = {
  ID = 5451529,
  vessels = 1,
  durability = 169236,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451530] = {
  ID = 5451530,
  vessels = 1,
  durability = 180786,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451531] = {
  ID = 5451531,
  vessels = 1,
  durability = 192694,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451532] = {
  ID = 5451532,
  vessels = 1,
  durability = 204964,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451533] = {
  ID = 5451533,
  vessels = 1,
  durability = 217587,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451534] = {
  ID = 5451534,
  vessels = 1,
  durability = 230566,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451535] = {
  ID = 5451535,
  vessels = 1,
  durability = 243902,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451536] = {
  ID = 5451536,
  vessels = 1,
  durability = 257596,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451537] = {
  ID = 5451537,
  vessels = 1,
  durability = 271652,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451538] = {
  ID = 5451538,
  vessels = 1,
  durability = 286060,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451539] = {
  ID = 5451539,
  vessels = 1,
  durability = 335626,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451540] = {
  ID = 5451540,
  vessels = 1,
  durability = 359178,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451541] = {
  ID = 5451541,
  vessels = 1,
  durability = 383385,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451542] = {
  ID = 5451542,
  vessels = 1,
  durability = 408252,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451543] = {
  ID = 5451543,
  vessels = 1,
  durability = 433785,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451544] = {
  ID = 5451544,
  vessels = 1,
  durability = 459972,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451545] = {
  ID = 5451545,
  vessels = 1,
  durability = 486826,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451546] = {
  ID = 5451546,
  vessels = 1,
  durability = 514333,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451547] = {
  ID = 5451547,
  vessels = 1,
  durability = 542501,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451548] = {
  ID = 5451548,
  vessels = 1,
  durability = 571336,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451549] = {
  ID = 5451549,
  vessels = 1,
  durability = 646555,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451550] = {
  ID = 5451550,
  vessels = 1,
  durability = 689640,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451551] = {
  ID = 5451551,
  vessels = 1,
  durability = 733790,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451552] = {
  ID = 5451552,
  vessels = 1,
  durability = 779001,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451553] = {
  ID = 5451553,
  vessels = 1,
  durability = 825271,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451554] = {
  ID = 5451554,
  vessels = 1,
  durability = 872600,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451555] = {
  ID = 5451555,
  vessels = 1,
  durability = 920982,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451556] = {
  ID = 5451556,
  vessels = 1,
  durability = 970430,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451557] = {
  ID = 5451557,
  vessels = 1,
  durability = 1020938,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451558] = {
  ID = 5451558,
  vessels = 1,
  durability = 1072505,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451559] = {
  ID = 5451559,
  vessels = 1,
  durability = 1244423,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451560] = {
  ID = 5451560,
  vessels = 1,
  durability = 1323049,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451561] = {
  ID = 5451561,
  vessels = 1,
  durability = 1403389,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451562] = {
  ID = 5451562,
  vessels = 1,
  durability = 1485435,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451563] = {
  ID = 5451563,
  vessels = 1,
  durability = 1569204,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451564] = {
  ID = 5451564,
  vessels = 1,
  durability = 1654678,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451565] = {
  ID = 5451565,
  vessels = 1,
  durability = 1741874,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451566] = {
  ID = 5451566,
  vessels = 1,
  durability = 1830784,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451567] = {
  ID = 5451567,
  vessels = 1,
  durability = 1876375,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451568] = {
  ID = 5451568,
  vessels = 1,
  durability = 2013738,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451569] = {
  ID = 5451569,
  vessels = 1,
  durability = 2629773,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451570] = {
  ID = 5451570,
  vessels = 1,
  durability = 2830889,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451571] = {
  ID = 5451571,
  vessels = 1,
  durability = 3036159,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451572] = {
  ID = 5451572,
  vessels = 1,
  durability = 3245593,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451573] = {
  ID = 5451573,
  vessels = 1,
  durability = 3459172,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451574] = {
  ID = 5451574,
  vessels = 1,
  durability = 3676905,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451575] = {
  ID = 5451575,
  vessels = 1,
  durability = 3898792,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451576] = {
  ID = 5451576,
  vessels = 1,
  durability = 4124834,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451577] = {
  ID = 5451577,
  vessels = 1,
  durability = 4355039,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451578] = {
  ID = 5451578,
  vessels = 1,
  durability = 4813719,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451579] = {
  ID = 5451579,
  vessels = 1,
  durability = 5814979,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451580] = {
  ID = 5451580,
  vessels = 1,
  durability = 6274225,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451581] = {
  ID = 5451581,
  vessels = 1,
  durability = 6742145,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451582] = {
  ID = 5451582,
  vessels = 1,
  durability = 7218737,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451583] = {
  ID = 5451583,
  vessels = 1,
  durability = 7704003,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451584] = {
  ID = 5451584,
  vessels = 1,
  durability = 8197943,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451585] = {
  ID = 5451585,
  vessels = 1,
  durability = 8700555,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451586] = {
  ID = 5451586,
  vessels = 1,
  durability = 9211841,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451587] = {
  ID = 5451587,
  vessels = 1,
  durability = 9925642,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451588] = {
  ID = 5451588,
  vessels = 1,
  durability = 11002736,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451589] = {
  ID = 5451589,
  vessels = 1,
  durability = 15031608,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451590] = {
  ID = 5451590,
  vessels = 1,
  durability = 16756540,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451591] = {
  ID = 5451591,
  vessels = 1,
  durability = 18228175,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451592] = {
  ID = 5451592,
  vessels = 1,
  durability = 19468286,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451593] = {
  ID = 5451593,
  vessels = 1,
  durability = 20729007,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451594] = {
  ID = 5451594,
  vessels = 1,
  durability = 22010349,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451595] = {
  ID = 5451595,
  vessels = 1,
  durability = 23312289,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451596] = {
  ID = 5451596,
  vessels = 1,
  durability = 24634840,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451597] = {
  ID = 5451597,
  vessels = 1,
  durability = 27089251,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451598] = {
  ID = 5451598,
  vessels = 1,
  durability = 29135454,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5451599] = {
  ID = 5451599,
  vessels = 1,
  durability = 31956989,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5452000] = {
  ID = 5452000,
  vessels = 2,
  durability = 1191,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452001] = {
  ID = 5452001,
  vessels = 2,
  durability = 2335,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452002] = {
  ID = 5452002,
  vessels = 2,
  durability = 3652,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452003] = {
  ID = 5452003,
  vessels = 2,
  durability = 6348,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452004] = {
  ID = 5452004,
  vessels = 2,
  durability = 8097,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452005] = {
  ID = 5452005,
  vessels = 2,
  durability = 10019,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452006] = {
  ID = 5452006,
  vessels = 2,
  durability = 12114,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452007] = {
  ID = 5452007,
  vessels = 2,
  durability = 14381,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452008] = {
  ID = 5452008,
  vessels = 2,
  durability = 16821,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452009] = {
  ID = 5452009,
  vessels = 2,
  durability = 19468,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452010] = {
  ID = 5452010,
  vessels = 2,
  durability = 22291,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452011] = {
  ID = 5452011,
  vessels = 2,
  durability = 25291,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452012] = {
  ID = 5452012,
  vessels = 2,
  durability = 28466,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452013] = {
  ID = 5452013,
  vessels = 2,
  durability = 31818,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452014] = {
  ID = 5452014,
  vessels = 2,
  durability = 35346,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452015] = {
  ID = 5452015,
  vessels = 2,
  durability = 39050,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452016] = {
  ID = 5452016,
  vessels = 2,
  durability = 42930,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452017] = {
  ID = 5452017,
  vessels = 2,
  durability = 46987,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452018] = {
  ID = 5452018,
  vessels = 2,
  durability = 51219,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452019] = {
  ID = 5452019,
  vessels = 2,
  durability = 56874,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452020] = {
  ID = 5452020,
  vessels = 2,
  durability = 61619,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452021] = {
  ID = 5452021,
  vessels = 2,
  durability = 67443,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452022] = {
  ID = 5452022,
  vessels = 2,
  durability = 72618,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452023] = {
  ID = 5452023,
  vessels = 2,
  durability = 77979,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452024] = {
  ID = 5452024,
  vessels = 2,
  durability = 83529,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452025] = {
  ID = 5452025,
  vessels = 2,
  durability = 89262,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452026] = {
  ID = 5452026,
  vessels = 2,
  durability = 95184,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452027] = {
  ID = 5452027,
  vessels = 2,
  durability = 101289,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452028] = {
  ID = 5452028,
  vessels = 2,
  durability = 107579,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452029] = {
  ID = 5452029,
  vessels = 2,
  durability = 147505,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452030] = {
  ID = 5452030,
  vessels = 2,
  durability = 157490,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452031] = {
  ID = 5452031,
  vessels = 2,
  durability = 167782,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452032] = {
  ID = 5452032,
  vessels = 2,
  durability = 178381,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452033] = {
  ID = 5452033,
  vessels = 2,
  durability = 189284,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452034] = {
  ID = 5452034,
  vessels = 2,
  durability = 200497,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452035] = {
  ID = 5452035,
  vessels = 2,
  durability = 212018,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452036] = {
  ID = 5452036,
  vessels = 2,
  durability = 223846,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452037] = {
  ID = 5452037,
  vessels = 2,
  durability = 235982,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452038] = {
  ID = 5452038,
  vessels = 2,
  durability = 248419,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452039] = {
  ID = 5452039,
  vessels = 2,
  durability = 292136,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452040] = {
  ID = 5452040,
  vessels = 2,
  durability = 312406,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452041] = {
  ID = 5452041,
  vessels = 2,
  durability = 333237,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452042] = {
  ID = 5452042,
  vessels = 2,
  durability = 354639,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452043] = {
  ID = 5452043,
  vessels = 2,
  durability = 376601,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452044] = {
  ID = 5452044,
  vessels = 2,
  durability = 399134,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452045] = {
  ID = 5452045,
  vessels = 2,
  durability = 422233,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452046] = {
  ID = 5452046,
  vessels = 2,
  durability = 445892,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452047] = {
  ID = 5452047,
  vessels = 2,
  durability = 470122,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452048] = {
  ID = 5452048,
  vessels = 2,
  durability = 494913,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452049] = {
  ID = 5452049,
  vessels = 2,
  durability = 560576,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452050] = {
  ID = 5452050,
  vessels = 2,
  durability = 597261,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452051] = {
  ID = 5452051,
  vessels = 2,
  durability = 634843,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452052] = {
  ID = 5452052,
  vessels = 2,
  durability = 673321,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452053] = {
  ID = 5452053,
  vessels = 2,
  durability = 712696,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452054] = {
  ID = 5452054,
  vessels = 2,
  durability = 752973,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452055] = {
  ID = 5452055,
  vessels = 2,
  durability = 794141,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452056] = {
  ID = 5452056,
  vessels = 2,
  durability = 836205,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452057] = {
  ID = 5452057,
  vessels = 2,
  durability = 879166,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452058] = {
  ID = 5452058,
  vessels = 2,
  durability = 923023,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452059] = {
  ID = 5452059,
  vessels = 2,
  durability = 1073153,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452060] = {
  ID = 5452060,
  vessels = 2,
  durability = 1139194,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452061] = {
  ID = 5452061,
  vessels = 2,
  durability = 1206669,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452062] = {
  ID = 5452062,
  vessels = 2,
  durability = 1275567,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452063] = {
  ID = 5452063,
  vessels = 2,
  durability = 1345892,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452064] = {
  ID = 5452064,
  vessels = 2,
  durability = 1417654,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452065] = {
  ID = 5452065,
  vessels = 2,
  durability = 1490836,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452066] = {
  ID = 5452066,
  vessels = 2,
  durability = 1565455,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452067] = {
  ID = 5452067,
  vessels = 2,
  durability = 1615417,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452068] = {
  ID = 5452068,
  vessels = 2,
  durability = 1718963,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452069] = {
  ID = 5452069,
  vessels = 2,
  durability = 2280085,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452070] = {
  ID = 5452070,
  vessels = 2,
  durability = 2452239,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452071] = {
  ID = 5452071,
  vessels = 2,
  durability = 2627940,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452072] = {
  ID = 5452072,
  vessels = 2,
  durability = 2807188,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452073] = {
  ID = 5452073,
  vessels = 2,
  durability = 2989983,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452074] = {
  ID = 5452074,
  vessels = 2,
  durability = 3176325,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452075] = {
  ID = 5452075,
  vessels = 2,
  durability = 3366215,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452076] = {
  ID = 5452076,
  vessels = 2,
  durability = 3559651,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452077] = {
  ID = 5452077,
  vessels = 2,
  durability = 3756634,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452078] = {
  ID = 5452078,
  vessels = 2,
  durability = 4102236,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452079] = {
  ID = 5452079,
  vessels = 2,
  durability = 4977921,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452080] = {
  ID = 5452080,
  vessels = 2,
  durability = 5365295,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452081] = {
  ID = 5452081,
  vessels = 2,
  durability = 5759958,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452082] = {
  ID = 5452082,
  vessels = 2,
  durability = 6161918,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452083] = {
  ID = 5452083,
  vessels = 2,
  durability = 6571177,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452084] = {
  ID = 5452084,
  vessels = 2,
  durability = 6987733,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452085] = {
  ID = 5452085,
  vessels = 2,
  durability = 7411597,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452086] = {
  ID = 5452086,
  vessels = 2,
  durability = 7842749,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452087] = {
  ID = 5452087,
  vessels = 2,
  durability = 8416830,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452088] = {
  ID = 5452088,
  vessels = 2,
  durability = 9240744,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452089] = {
  ID = 5452089,
  vessels = 2,
  durability = 13103572,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452090] = {
  ID = 5452090,
  vessels = 2,
  durability = 14460571,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452091] = {
  ID = 5452091,
  vessels = 2,
  durability = 15671867,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452092] = {
  ID = 5452092,
  vessels = 2,
  durability = 16724179,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452093] = {
  ID = 5452093,
  vessels = 2,
  durability = 17793941,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452094] = {
  ID = 5452094,
  vessels = 2,
  durability = 18881153,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452095] = {
  ID = 5452095,
  vessels = 2,
  durability = 19985814,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452096] = {
  ID = 5452096,
  vessels = 2,
  durability = 21107925,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452097] = {
  ID = 5452097,
  vessels = 2,
  durability = 23013483,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452098] = {
  ID = 5452098,
  vessels = 2,
  durability = 24770079,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452099] = {
  ID = 5452099,
  vessels = 2,
  durability = 27526865,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452500] = {
  ID = 5452500,
  vessels = 2,
  durability = 1655,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452501] = {
  ID = 5452501,
  vessels = 2,
  durability = 3245,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452502] = {
  ID = 5452502,
  vessels = 2,
  durability = 5076,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452503] = {
  ID = 5452503,
  vessels = 2,
  durability = 8947,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452504] = {
  ID = 5452504,
  vessels = 2,
  durability = 11390,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452505] = {
  ID = 5452505,
  vessels = 2,
  durability = 14073,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452506] = {
  ID = 5452506,
  vessels = 2,
  durability = 16994,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452507] = {
  ID = 5452507,
  vessels = 2,
  durability = 20158,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452508] = {
  ID = 5452508,
  vessels = 2,
  durability = 23560,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452509] = {
  ID = 5452509,
  vessels = 2,
  durability = 27258,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452510] = {
  ID = 5452510,
  vessels = 2,
  durability = 31199,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452511] = {
  ID = 5452511,
  vessels = 2,
  durability = 35386,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452512] = {
  ID = 5452512,
  vessels = 2,
  durability = 39818,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452513] = {
  ID = 5452513,
  vessels = 2,
  durability = 44496,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452514] = {
  ID = 5452514,
  vessels = 2,
  durability = 49422,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452515] = {
  ID = 5452515,
  vessels = 2,
  durability = 54591,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452516] = {
  ID = 5452516,
  vessels = 2,
  durability = 60005,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452517] = {
  ID = 5452517,
  vessels = 2,
  durability = 65665,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452518] = {
  ID = 5452518,
  vessels = 2,
  durability = 71571,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452519] = {
  ID = 5452519,
  vessels = 2,
  durability = 79468,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452520] = {
  ID = 5452520,
  vessels = 2,
  durability = 86102,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452521] = {
  ID = 5452521,
  vessels = 2,
  durability = 94063,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452522] = {
  ID = 5452522,
  vessels = 2,
  durability = 101272,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452523] = {
  ID = 5452523,
  vessels = 2,
  durability = 108744,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452524] = {
  ID = 5452524,
  vessels = 2,
  durability = 116476,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452525] = {
  ID = 5452525,
  vessels = 2,
  durability = 124466,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452526] = {
  ID = 5452526,
  vessels = 2,
  durability = 132716,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452527] = {
  ID = 5452527,
  vessels = 2,
  durability = 141219,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452528] = {
  ID = 5452528,
  vessels = 2,
  durability = 149986,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452529] = {
  ID = 5452529,
  vessels = 2,
  durability = 205481,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452530] = {
  ID = 5452530,
  vessels = 2,
  durability = 219387,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452531] = {
  ID = 5452531,
  vessels = 2,
  durability = 233728,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452532] = {
  ID = 5452532,
  vessels = 2,
  durability = 248496,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452533] = {
  ID = 5452533,
  vessels = 2,
  durability = 263686,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452534] = {
  ID = 5452534,
  vessels = 2,
  durability = 279310,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452535] = {
  ID = 5452535,
  vessels = 2,
  durability = 295356,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452536] = {
  ID = 5452536,
  vessels = 2,
  durability = 311836,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452537] = {
  ID = 5452537,
  vessels = 2,
  durability = 328745,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452538] = {
  ID = 5452538,
  vessels = 2,
  durability = 346074,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452539] = {
  ID = 5452539,
  vessels = 2,
  durability = 406877,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452540] = {
  ID = 5452540,
  vessels = 2,
  durability = 435124,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452541] = {
  ID = 5452541,
  vessels = 2,
  durability = 464153,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452542] = {
  ID = 5452542,
  vessels = 2,
  durability = 493970,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452543] = {
  ID = 5452543,
  vessels = 2,
  durability = 524576,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452544] = {
  ID = 5452544,
  vessels = 2,
  durability = 555970,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452545] = {
  ID = 5452545,
  vessels = 2,
  durability = 588160,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452546] = {
  ID = 5452546,
  vessels = 2,
  durability = 621130,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452547] = {
  ID = 5452547,
  vessels = 2,
  durability = 654889,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452548] = {
  ID = 5452548,
  vessels = 2,
  durability = 689437,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452549] = {
  ID = 5452549,
  vessels = 2,
  durability = 780831,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452550] = {
  ID = 5452550,
  vessels = 2,
  durability = 831983,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452551] = {
  ID = 5452551,
  vessels = 2,
  durability = 884386,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452552] = {
  ID = 5452552,
  vessels = 2,
  durability = 938048,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452553] = {
  ID = 5452553,
  vessels = 2,
  durability = 992952,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452554] = {
  ID = 5452554,
  vessels = 2,
  durability = 1049115,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452555] = {
  ID = 5452555,
  vessels = 2,
  durability = 1106519,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452556] = {
  ID = 5452556,
  vessels = 2,
  durability = 1165175,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452557] = {
  ID = 5452557,
  vessels = 2,
  durability = 1225090,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452558] = {
  ID = 5452558,
  vessels = 2,
  durability = 1286246,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452559] = {
  ID = 5452559,
  vessels = 2,
  durability = 1495196,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452560] = {
  ID = 5452560,
  vessels = 2,
  durability = 1587375,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452561] = {
  ID = 5452561,
  vessels = 2,
  durability = 1681549,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452562] = {
  ID = 5452562,
  vessels = 2,
  durability = 1777708,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452563] = {
  ID = 5452563,
  vessels = 2,
  durability = 1875872,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452564] = {
  ID = 5452564,
  vessels = 2,
  durability = 1976030,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452565] = {
  ID = 5452565,
  vessels = 2,
  durability = 2078184,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452566] = {
  ID = 5452566,
  vessels = 2,
  durability = 2182332,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452567] = {
  ID = 5452567,
  vessels = 2,
  durability = 2265441,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452568] = {
  ID = 5452568,
  vessels = 2,
  durability = 2396603,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452569] = {
  ID = 5452569,
  vessels = 2,
  durability = 3192603,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452570] = {
  ID = 5452570,
  vessels = 2,
  durability = 3434364,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452571] = {
  ID = 5452571,
  vessels = 2,
  durability = 3681109,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452572] = {
  ID = 5452572,
  vessels = 2,
  durability = 3932838,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452573] = {
  ID = 5452573,
  vessels = 2,
  durability = 4189539,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452574] = {
  ID = 5452574,
  vessels = 2,
  durability = 4451236,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452575] = {
  ID = 5452575,
  vessels = 2,
  durability = 4717917,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452576] = {
  ID = 5452576,
  vessels = 2,
  durability = 4989581,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452577] = {
  ID = 5452577,
  vessels = 2,
  durability = 5266230,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452578] = {
  ID = 5452578,
  vessels = 2,
  durability = 5640942,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452579] = {
  ID = 5452579,
  vessels = 2,
  durability = 6850701,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452580] = {
  ID = 5452580,
  vessels = 2,
  durability = 7382120,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452581] = {
  ID = 5452581,
  vessels = 2,
  durability = 7923533,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452582] = {
  ID = 5452582,
  vessels = 2,
  durability = 8474951,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452583] = {
  ID = 5452583,
  vessels = 2,
  durability = 9036390,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452584] = {
  ID = 5452584,
  vessels = 2,
  durability = 9607822,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452585] = {
  ID = 5452585,
  vessels = 2,
  durability = 10189273,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452586] = {
  ID = 5452586,
  vessels = 2,
  durability = 10780717,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452587] = {
  ID = 5452587,
  vessels = 2,
  durability = 11450165,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452588] = {
  ID = 5452588,
  vessels = 2,
  durability = 12252113,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452589] = {
  ID = 5452589,
  vessels = 2,
  durability = 18326728,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452590] = {
  ID = 5452590,
  vessels = 2,
  durability = 19928248,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452591] = {
  ID = 5452591,
  vessels = 2,
  durability = 21424284,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452592] = {
  ID = 5452592,
  vessels = 2,
  durability = 22855778,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452593] = {
  ID = 5452593,
  vessels = 2,
  durability = 24310991,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452594] = {
  ID = 5452594,
  vessels = 2,
  durability = 25789921,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452595] = {
  ID = 5452595,
  vessels = 2,
  durability = 27292553,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452596] = {
  ID = 5452596,
  vessels = 2,
  durability = 28818918,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452597] = {
  ID = 5452597,
  vessels = 2,
  durability = 30754110,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452598] = {
  ID = 5452598,
  vessels = 2,
  durability = 33118284,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5452599] = {
  ID = 5452599,
  vessels = 2,
  durability = 37919428,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5453000] = {
  ID = 5453000,
  vessels = 3,
  durability = 1287,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453001] = {
  ID = 5453001,
  vessels = 3,
  durability = 2538,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453002] = {
  ID = 5453002,
  vessels = 3,
  durability = 3978,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453003] = {
  ID = 5453003,
  vessels = 3,
  durability = 6923,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453004] = {
  ID = 5453004,
  vessels = 3,
  durability = 8835,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453005] = {
  ID = 5453005,
  vessels = 3,
  durability = 10938,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453006] = {
  ID = 5453006,
  vessels = 3,
  durability = 13228,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453007] = {
  ID = 5453007,
  vessels = 3,
  durability = 15707,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453008] = {
  ID = 5453008,
  vessels = 3,
  durability = 18376,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453009] = {
  ID = 5453009,
  vessels = 3,
  durability = 21244,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453010] = {
  ID = 5453010,
  vessels = 3,
  durability = 24300,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453011] = {
  ID = 5453011,
  vessels = 3,
  durability = 27546,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453012] = {
  ID = 5453012,
  vessels = 3,
  durability = 30985,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453013] = {
  ID = 5453013,
  vessels = 3,
  durability = 34611,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453014] = {
  ID = 5453014,
  vessels = 3,
  durability = 38430,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453015] = {
  ID = 5453015,
  vessels = 3,
  durability = 42435,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453016] = {
  ID = 5453016,
  vessels = 3,
  durability = 46631,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453017] = {
  ID = 5453017,
  vessels = 3,
  durability = 51020,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453018] = {
  ID = 5453018,
  vessels = 3,
  durability = 55596,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453019] = {
  ID = 5453019,
  vessels = 3,
  durability = 61661,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453020] = {
  ID = 5453020,
  vessels = 3,
  durability = 66705,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453021] = {
  ID = 5453021,
  vessels = 3,
  durability = 72615,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453022] = {
  ID = 5453022,
  vessels = 3,
  durability = 78077,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453023] = {
  ID = 5453023,
  vessels = 3,
  durability = 83733,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453024] = {
  ID = 5453024,
  vessels = 3,
  durability = 89582,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453025] = {
  ID = 5453025,
  vessels = 3,
  durability = 95624,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453026] = {
  ID = 5453026,
  vessels = 3,
  durability = 101860,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453027] = {
  ID = 5453027,
  vessels = 3,
  durability = 108290,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453028] = {
  ID = 5453028,
  vessels = 3,
  durability = 114913,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453029] = {
  ID = 5453029,
  vessels = 3,
  durability = 158629,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453030] = {
  ID = 5453030,
  vessels = 3,
  durability = 169052,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453031] = {
  ID = 5453031,
  vessels = 3,
  durability = 179790,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453032] = {
  ID = 5453032,
  vessels = 3,
  durability = 190844,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453033] = {
  ID = 5453033,
  vessels = 3,
  durability = 202212,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453034] = {
  ID = 5453034,
  vessels = 3,
  durability = 213895,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453035] = {
  ID = 5453035,
  vessels = 3,
  durability = 225893,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453036] = {
  ID = 5453036,
  vessels = 3,
  durability = 238207,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453037] = {
  ID = 5453037,
  vessels = 3,
  durability = 250835,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453038] = {
  ID = 5453038,
  vessels = 3,
  durability = 263778,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453039] = {
  ID = 5453039,
  vessels = 3,
  durability = 310786,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453040] = {
  ID = 5453040,
  vessels = 3,
  durability = 331690,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453041] = {
  ID = 5453041,
  vessels = 3,
  durability = 353170,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453042] = {
  ID = 5453042,
  vessels = 3,
  durability = 375226,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453043] = {
  ID = 5453043,
  vessels = 3,
  durability = 397857,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453044] = {
  ID = 5453044,
  vessels = 3,
  durability = 421065,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453045] = {
  ID = 5453045,
  vessels = 3,
  durability = 444849,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453046] = {
  ID = 5453046,
  vessels = 3,
  durability = 469209,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453047] = {
  ID = 5453047,
  vessels = 3,
  durability = 494144,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453048] = {
  ID = 5453048,
  vessels = 3,
  durability = 519656,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453049] = {
  ID = 5453049,
  vessels = 3,
  durability = 588944,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453050] = {
  ID = 5453050,
  vessels = 3,
  durability = 625935,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453051] = {
  ID = 5453051,
  vessels = 3,
  durability = 663817,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453052] = {
  ID = 5453052,
  vessels = 3,
  durability = 702590,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453053] = {
  ID = 5453053,
  vessels = 3,
  durability = 742255,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453054] = {
  ID = 5453054,
  vessels = 3,
  durability = 782810,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453055] = {
  ID = 5453055,
  vessels = 3,
  durability = 824256,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453056] = {
  ID = 5453056,
  vessels = 3,
  durability = 866593,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453057] = {
  ID = 5453057,
  vessels = 3,
  durability = 909822,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453058] = {
  ID = 5453058,
  vessels = 3,
  durability = 953941,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453059] = {
  ID = 5453059,
  vessels = 3,
  durability = 1112351,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453060] = {
  ID = 5453060,
  vessels = 3,
  durability = 1177125,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453061] = {
  ID = 5453061,
  vessels = 3,
  durability = 1243277,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453062] = {
  ID = 5453062,
  vessels = 3,
  durability = 1310805,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453063] = {
  ID = 5453063,
  vessels = 3,
  durability = 1379710,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453064] = {
  ID = 5453064,
  vessels = 3,
  durability = 1449992,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453065] = {
  ID = 5453065,
  vessels = 3,
  durability = 1521652,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453066] = {
  ID = 5453066,
  vessels = 3,
  durability = 1594688,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453067] = {
  ID = 5453067,
  vessels = 3,
  durability = 1659282,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453068] = {
  ID = 5453068,
  vessels = 3,
  durability = 1744891,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453069] = {
  ID = 5453069,
  vessels = 3,
  durability = 2372211,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453070] = {
  ID = 5453070,
  vessels = 3,
  durability = 2546542,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453071] = {
  ID = 5453071,
  vessels = 3,
  durability = 2724456,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453072] = {
  ID = 5453072,
  vessels = 3,
  durability = 2905942,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453073] = {
  ID = 5453073,
  vessels = 3,
  durability = 3090991,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453074] = {
  ID = 5453074,
  vessels = 3,
  durability = 3279621,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453075] = {
  ID = 5453075,
  vessels = 3,
  durability = 3471815,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453076] = {
  ID = 5453076,
  vessels = 3,
  durability = 3667591,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453077] = {
  ID = 5453077,
  vessels = 3,
  durability = 3866940,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453078] = {
  ID = 5453078,
  vessels = 3,
  durability = 4123287,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453079] = {
  ID = 5453079,
  vessels = 3,
  durability = 5047322,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453080] = {
  ID = 5453080,
  vessels = 3,
  durability = 5427809,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453081] = {
  ID = 5453081,
  vessels = 3,
  durability = 5815416,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453082] = {
  ID = 5453082,
  vessels = 3,
  durability = 6210152,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453083] = {
  ID = 5453083,
  vessels = 3,
  durability = 6612018,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453084] = {
  ID = 5453084,
  vessels = 3,
  durability = 7021014,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453085] = {
  ID = 5453085,
  vessels = 3,
  durability = 7437150,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453086] = {
  ID = 5453086,
  vessels = 3,
  durability = 7860404,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453087] = {
  ID = 5453087,
  vessels = 3,
  durability = 8334525,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453088] = {
  ID = 5453088,
  vessels = 3,
  durability = 8889752,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453089] = {
  ID = 5453089,
  vessels = 3,
  durability = 13749750,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453090] = {
  ID = 5453090,
  vessels = 3,
  durability = 14881636,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453091] = {
  ID = 5453091,
  vessels = 3,
  durability = 15963446,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453092] = {
  ID = 5453092,
  vessels = 3,
  durability = 17005745,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453093] = {
  ID = 5453093,
  vessels = 3,
  durability = 18065244,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453094] = {
  ID = 5453094,
  vessels = 3,
  durability = 19141943,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453095] = {
  ID = 5453095,
  vessels = 3,
  durability = 20235842,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453096] = {
  ID = 5453096,
  vessels = 3,
  durability = 21346941,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453097] = {
  ID = 5453097,
  vessels = 3,
  durability = 22712651,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453098] = {
  ID = 5453098,
  vessels = 3,
  durability = 24486029,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453099] = {
  ID = 5453099,
  vessels = 3,
  durability = 28216967,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453500] = {
  ID = 5453500,
  vessels = 3,
  durability = 1893,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453501] = {
  ID = 5453501,
  vessels = 3,
  durability = 3772,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453502] = {
  ID = 5453502,
  vessels = 3,
  durability = 5936,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453503] = {
  ID = 5453503,
  vessels = 3,
  durability = 13480,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453504] = {
  ID = 5453504,
  vessels = 3,
  durability = 16577,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453505] = {
  ID = 5453505,
  vessels = 3,
  durability = 19961,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453506] = {
  ID = 5453506,
  vessels = 3,
  durability = 23627,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453507] = {
  ID = 5453507,
  vessels = 3,
  durability = 27577,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453508] = {
  ID = 5453508,
  vessels = 3,
  durability = 31812,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453509] = {
  ID = 5453509,
  vessels = 3,
  durability = 36358,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453510] = {
  ID = 5453510,
  vessels = 3,
  durability = 41189,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453511] = {
  ID = 5453511,
  vessels = 3,
  durability = 46306,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453512] = {
  ID = 5453512,
  vessels = 3,
  durability = 51713,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453513] = {
  ID = 5453513,
  vessels = 3,
  durability = 57404,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453514] = {
  ID = 5453514,
  vessels = 3,
  durability = 63386,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453515] = {
  ID = 5453515,
  vessels = 3,
  durability = 69651,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453516] = {
  ID = 5453516,
  vessels = 3,
  durability = 76203,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453517] = {
  ID = 5453517,
  vessels = 3,
  durability = 83046,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453518] = {
  ID = 5453518,
  vessels = 3,
  durability = 90172,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453519] = {
  ID = 5453519,
  vessels = 3,
  durability = 99564,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453520] = {
  ID = 5453520,
  vessels = 3,
  durability = 107428,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453521] = {
  ID = 5453521,
  vessels = 3,
  durability = 117239,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453522] = {
  ID = 5453522,
  vessels = 3,
  durability = 125764,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453523] = {
  ID = 5453523,
  vessels = 3,
  durability = 134590,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453524] = {
  ID = 5453524,
  vessels = 3,
  durability = 143704,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453525] = {
  ID = 5453525,
  vessels = 3,
  durability = 153120,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453526] = {
  ID = 5453526,
  vessels = 3,
  durability = 162830,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453527] = {
  ID = 5453527,
  vessels = 3,
  durability = 172829,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453528] = {
  ID = 5453528,
  vessels = 3,
  durability = 183129,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453529] = {
  ID = 5453529,
  vessels = 3,
  durability = 249162,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453530] = {
  ID = 5453530,
  vessels = 3,
  durability = 265264,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453531] = {
  ID = 5453531,
  vessels = 3,
  durability = 281848,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453532] = {
  ID = 5453532,
  vessels = 3,
  durability = 298921,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453533] = {
  ID = 5453533,
  vessels = 3,
  durability = 316468,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453534] = {
  ID = 5453534,
  vessels = 3,
  durability = 334497,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453535] = {
  ID = 5453535,
  vessels = 3,
  durability = 353007,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453536] = {
  ID = 5453536,
  vessels = 3,
  durability = 371999,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453537] = {
  ID = 5453537,
  vessels = 3,
  durability = 391481,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453538] = {
  ID = 5453538,
  vessels = 3,
  durability = 411436,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453539] = {
  ID = 5453539,
  vessels = 3,
  durability = 482726,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453540] = {
  ID = 5453540,
  vessels = 3,
  durability = 514886,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453541] = {
  ID = 5453541,
  vessels = 3,
  durability = 547919,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453542] = {
  ID = 5453542,
  vessels = 3,
  durability = 581835,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453543] = {
  ID = 5453543,
  vessels = 3,
  durability = 616642,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453544] = {
  ID = 5453544,
  vessels = 3,
  durability = 652323,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453545] = {
  ID = 5453545,
  vessels = 3,
  durability = 688895,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453546] = {
  ID = 5453546,
  vessels = 3,
  durability = 726340,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453547] = {
  ID = 5453547,
  vessels = 3,
  durability = 764668,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453548] = {
  ID = 5453548,
  vessels = 3,
  durability = 803887,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453549] = {
  ID = 5453549,
  vessels = 3,
  durability = 909336,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453550] = {
  ID = 5453550,
  vessels = 3,
  durability = 966366,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453551] = {
  ID = 5453551,
  vessels = 3,
  durability = 1024780,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453552] = {
  ID = 5453552,
  vessels = 3,
  durability = 1084568,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453553] = {
  ID = 5453553,
  vessels = 3,
  durability = 1145728,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453554] = {
  ID = 5453554,
  vessels = 3,
  durability = 1208262,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453555] = {
  ID = 5453555,
  vessels = 3,
  durability = 1272158,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453556] = {
  ID = 5453556,
  vessels = 3,
  durability = 1337438,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453557] = {
  ID = 5453557,
  vessels = 3,
  durability = 1404092,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453558] = {
  ID = 5453558,
  vessels = 3,
  durability = 1472119,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453559] = {
  ID = 5453559,
  vessels = 3,
  durability = 1712895,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453560] = {
  ID = 5453560,
  vessels = 3,
  durability = 1813302,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453561] = {
  ID = 5453561,
  vessels = 3,
  durability = 1915848,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453562] = {
  ID = 5453562,
  vessels = 3,
  durability = 2020521,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453563] = {
  ID = 5453563,
  vessels = 3,
  durability = 2127345,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453564] = {
  ID = 5453564,
  vessels = 3,
  durability = 2236296,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453565] = {
  ID = 5453565,
  vessels = 3,
  durability = 2347398,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453566] = {
  ID = 5453566,
  vessels = 3,
  durability = 2460639,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453567] = {
  ID = 5453567,
  vessels = 3,
  durability = 2553590,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453568] = {
  ID = 5453568,
  vessels = 3,
  durability = 2693525,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453569] = {
  ID = 5453569,
  vessels = 3,
  durability = 3632418,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453570] = {
  ID = 5453570,
  vessels = 3,
  durability = 3900090,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453571] = {
  ID = 5453571,
  vessels = 3,
  durability = 4173265,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453572] = {
  ID = 5453572,
  vessels = 3,
  durability = 4451928,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453573] = {
  ID = 5453573,
  vessels = 3,
  durability = 4736065,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453574] = {
  ID = 5453574,
  vessels = 3,
  durability = 5025705,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453575] = {
  ID = 5453575,
  vessels = 3,
  durability = 5320819,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453576] = {
  ID = 5453576,
  vessels = 3,
  durability = 5621435,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453577] = {
  ID = 5453577,
  vessels = 3,
  durability = 5927540,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453578] = {
  ID = 5453578,
  vessels = 3,
  durability = 6361657,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453579] = {
  ID = 5453579,
  vessels = 3,
  durability = 7765247,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453580] = {
  ID = 5453580,
  vessels = 3,
  durability = 8353861,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453581] = {
  ID = 5453581,
  vessels = 3,
  durability = 8953500,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453582] = {
  ID = 5453582,
  vessels = 3,
  durability = 9564179,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453583] = {
  ID = 5453583,
  vessels = 3,
  durability = 10185898,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453584] = {
  ID = 5453584,
  vessels = 3,
  durability = 10818657,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453585] = {
  ID = 5453585,
  vessels = 3,
  durability = 11462472,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453586] = {
  ID = 5453586,
  vessels = 3,
  durability = 12117310,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453587] = {
  ID = 5453587,
  vessels = 3,
  durability = 12881604,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453588] = {
  ID = 5453588,
  vessels = 3,
  durability = 13839825,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453589] = {
  ID = 5453589,
  vessels = 3,
  durability = 20931091,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453590] = {
  ID = 5453590,
  vessels = 3,
  durability = 22767348,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453591] = {
  ID = 5453591,
  vessels = 3,
  durability = 24476209,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453592] = {
  ID = 5453592,
  vessels = 3,
  durability = 26084024,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453593] = {
  ID = 5453593,
  vessels = 3,
  durability = 27718399,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453594] = {
  ID = 5453594,
  vessels = 3,
  durability = 29379335,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453595] = {
  ID = 5453595,
  vessels = 3,
  durability = 31066813,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453596] = {
  ID = 5453596,
  vessels = 3,
  durability = 32780869,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453597] = {
  ID = 5453597,
  vessels = 3,
  durability = 35090396,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453598] = {
  ID = 5453598,
  vessels = 3,
  durability = 37812657,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5453599] = {
  ID = 5453599,
  vessels = 3,
  durability = 43190148,
  Avatar = "head16",
  Ship = "ship5"
}
Monster[5454000] = {
  ID = 5454000,
  vessels = 4,
  durability = 926,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454001] = {
  ID = 5454001,
  vessels = 4,
  durability = 1814,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454002] = {
  ID = 5454002,
  vessels = 4,
  durability = 2835,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454003] = {
  ID = 5454003,
  vessels = 4,
  durability = 4930,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454004] = {
  ID = 5454004,
  vessels = 4,
  durability = 6287,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454005] = {
  ID = 5454005,
  vessels = 4,
  durability = 7779,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454006] = {
  ID = 5454006,
  vessels = 4,
  durability = 9403,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454007] = {
  ID = 5454007,
  vessels = 4,
  durability = 11162,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454008] = {
  ID = 5454008,
  vessels = 4,
  durability = 13056,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454009] = {
  ID = 5454009,
  vessels = 4,
  durability = 15107,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454010] = {
  ID = 5454010,
  vessels = 4,
  durability = 17293,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454011] = {
  ID = 5454011,
  vessels = 4,
  durability = 19617,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454012] = {
  ID = 5454012,
  vessels = 4,
  durability = 22075,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454013] = {
  ID = 5454013,
  vessels = 4,
  durability = 24672,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454014] = {
  ID = 5454014,
  vessels = 4,
  durability = 27405,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454015] = {
  ID = 5454015,
  vessels = 4,
  durability = 30272,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454016] = {
  ID = 5454016,
  vessels = 4,
  durability = 33277,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454017] = {
  ID = 5454017,
  vessels = 4,
  durability = 36417,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454018] = {
  ID = 5454018,
  vessels = 4,
  durability = 39696,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454019] = {
  ID = 5454019,
  vessels = 4,
  durability = 44067,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454020] = {
  ID = 5454020,
  vessels = 4,
  durability = 47731,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454021] = {
  ID = 5454021,
  vessels = 4,
  durability = 52194,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454022] = {
  ID = 5454022,
  vessels = 4,
  durability = 56184,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454023] = {
  ID = 5454023,
  vessels = 4,
  durability = 60318,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454024] = {
  ID = 5454024,
  vessels = 4,
  durability = 64595,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454025] = {
  ID = 5454025,
  vessels = 4,
  durability = 69015,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454026] = {
  ID = 5454026,
  vessels = 4,
  durability = 73578,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454027] = {
  ID = 5454027,
  vessels = 4,
  durability = 78283,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454028] = {
  ID = 5454028,
  vessels = 4,
  durability = 83132,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454029] = {
  ID = 5454029,
  vessels = 4,
  durability = 114105,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454030] = {
  ID = 5454030,
  vessels = 4,
  durability = 121788,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454031] = {
  ID = 5454031,
  vessels = 4,
  durability = 129706,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454032] = {
  ID = 5454032,
  vessels = 4,
  durability = 137861,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454033] = {
  ID = 5454033,
  vessels = 4,
  durability = 146250,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454034] = {
  ID = 5454034,
  vessels = 4,
  durability = 154876,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454035] = {
  ID = 5454035,
  vessels = 4,
  durability = 163737,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454036] = {
  ID = 5454036,
  vessels = 4,
  durability = 172833,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454037] = {
  ID = 5454037,
  vessels = 4,
  durability = 182165,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454038] = {
  ID = 5454038,
  vessels = 4,
  durability = 191733,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454039] = {
  ID = 5454039,
  vessels = 4,
  durability = 225540,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454040] = {
  ID = 5454040,
  vessels = 4,
  durability = 241103,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454041] = {
  ID = 5454041,
  vessels = 4,
  durability = 257100,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454042] = {
  ID = 5454042,
  vessels = 4,
  durability = 273530,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454043] = {
  ID = 5454043,
  vessels = 4,
  durability = 290394,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454044] = {
  ID = 5454044,
  vessels = 4,
  durability = 307692,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454045] = {
  ID = 5454045,
  vessels = 4,
  durability = 325422,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454046] = {
  ID = 5454046,
  vessels = 4,
  durability = 343586,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454047] = {
  ID = 5454047,
  vessels = 4,
  durability = 362184,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454048] = {
  ID = 5454048,
  vessels = 4,
  durability = 381215,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454049] = {
  ID = 5454049,
  vessels = 4,
  durability = 431827,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454050] = {
  ID = 5454050,
  vessels = 4,
  durability = 459891,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454051] = {
  ID = 5454051,
  vessels = 4,
  durability = 488640,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454052] = {
  ID = 5454052,
  vessels = 4,
  durability = 518073,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454053] = {
  ID = 5454053,
  vessels = 4,
  durability = 548190,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454054] = {
  ID = 5454054,
  vessels = 4,
  durability = 578992,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454055] = {
  ID = 5454055,
  vessels = 4,
  durability = 610477,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454056] = {
  ID = 5454056,
  vessels = 4,
  durability = 642647,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454057] = {
  ID = 5454057,
  vessels = 4,
  durability = 675501,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454058] = {
  ID = 5454058,
  vessels = 4,
  durability = 709039,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454059] = {
  ID = 5454059,
  vessels = 4,
  durability = 824758,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454060] = {
  ID = 5454060,
  vessels = 4,
  durability = 875052,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454061] = {
  ID = 5454061,
  vessels = 4,
  durability = 926432,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454062] = {
  ID = 5454062,
  vessels = 4,
  durability = 978897,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454063] = {
  ID = 5454063,
  vessels = 4,
  durability = 1032446,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454064] = {
  ID = 5454064,
  vessels = 4,
  durability = 1087080,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454065] = {
  ID = 5454065,
  vessels = 4,
  durability = 1142799,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454066] = {
  ID = 5454066,
  vessels = 4,
  durability = 1199603,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454067] = {
  ID = 5454067,
  vessels = 4,
  durability = 1240164,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454068] = {
  ID = 5454068,
  vessels = 4,
  durability = 1316466,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454069] = {
  ID = 5454069,
  vessels = 4,
  durability = 1754046,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454070] = {
  ID = 5454070,
  vessels = 4,
  durability = 1885909,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454071] = {
  ID = 5454071,
  vessels = 4,
  durability = 2020480,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454072] = {
  ID = 5454072,
  vessels = 4,
  durability = 2157772,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454073] = {
  ID = 5454073,
  vessels = 4,
  durability = 2297773,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454074] = {
  ID = 5454074,
  vessels = 4,
  durability = 2440487,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454075] = {
  ID = 5454075,
  vessels = 4,
  durability = 2585923,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454076] = {
  ID = 5454076,
  vessels = 4,
  durability = 2734066,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454077] = {
  ID = 5454077,
  vessels = 4,
  durability = 2884931,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454078] = {
  ID = 5454078,
  vessels = 4,
  durability = 3137053,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454079] = {
  ID = 5454079,
  vessels = 4,
  durability = 3812096,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454080] = {
  ID = 5454080,
  vessels = 4,
  durability = 4107221,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454081] = {
  ID = 5454081,
  vessels = 4,
  durability = 4407894,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454082] = {
  ID = 5454082,
  vessels = 4,
  durability = 4714122,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454083] = {
  ID = 5454083,
  vessels = 4,
  durability = 5025905,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454084] = {
  ID = 5454084,
  vessels = 4,
  durability = 5343243,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454085] = {
  ID = 5454085,
  vessels = 4,
  durability = 5666144,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454086] = {
  ID = 5454086,
  vessels = 4,
  durability = 5994593,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454087] = {
  ID = 5454087,
  vessels = 4,
  durability = 6419107,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454088] = {
  ID = 5454088,
  vessels = 4,
  durability = 7019992,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454089] = {
  ID = 5454089,
  vessels = 4,
  durability = 10094301,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454090] = {
  ID = 5454090,
  vessels = 4,
  durability = 11100794,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454091] = {
  ID = 5454091,
  vessels = 4,
  durability = 12007776,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454092] = {
  ID = 5454092,
  vessels = 4,
  durability = 12810340,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454093] = {
  ID = 5454093,
  vessels = 4,
  durability = 13626202,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454094] = {
  ID = 5454094,
  vessels = 4,
  durability = 14455370,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454095] = {
  ID = 5454095,
  vessels = 4,
  durability = 15297828,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454096] = {
  ID = 5454096,
  vessels = 4,
  durability = 16153584,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454097] = {
  ID = 5454097,
  vessels = 4,
  durability = 17552312,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454098] = {
  ID = 5454098,
  vessels = 4,
  durability = 18896885,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454099] = {
  ID = 5454099,
  vessels = 4,
  durability = 21117373,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454500] = {
  ID = 5454500,
  vessels = 4,
  durability = 1191,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454501] = {
  ID = 5454501,
  vessels = 4,
  durability = 2335,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454502] = {
  ID = 5454502,
  vessels = 4,
  durability = 3651,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454503] = {
  ID = 5454503,
  vessels = 4,
  durability = 6489,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454504] = {
  ID = 5454504,
  vessels = 4,
  durability = 8248,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454505] = {
  ID = 5454505,
  vessels = 4,
  durability = 10180,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454506] = {
  ID = 5454506,
  vessels = 4,
  durability = 12282,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454507] = {
  ID = 5454507,
  vessels = 4,
  durability = 14559,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454508] = {
  ID = 5454508,
  vessels = 4,
  durability = 17009,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454509] = {
  ID = 5454509,
  vessels = 4,
  durability = 19666,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454510] = {
  ID = 5454510,
  vessels = 4,
  durability = 22497,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454511] = {
  ID = 5454511,
  vessels = 4,
  durability = 25507,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454512] = {
  ID = 5454512,
  vessels = 4,
  durability = 28690,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454513] = {
  ID = 5454513,
  vessels = 4,
  durability = 32051,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454514] = {
  ID = 5454514,
  vessels = 4,
  durability = 35589,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454515] = {
  ID = 5454515,
  vessels = 4,
  durability = 39300,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454516] = {
  ID = 5454516,
  vessels = 4,
  durability = 43190,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454517] = {
  ID = 5454517,
  vessels = 4,
  durability = 47253,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454518] = {
  ID = 5454518,
  vessels = 4,
  durability = 51495,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454519] = {
  ID = 5454519,
  vessels = 4,
  durability = 57156,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454520] = {
  ID = 5454520,
  vessels = 4,
  durability = 61908,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454521] = {
  ID = 5454521,
  vessels = 4,
  durability = 67942,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454522] = {
  ID = 5454522,
  vessels = 4,
  durability = 73126,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454523] = {
  ID = 5454523,
  vessels = 4,
  durability = 78495,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454524] = {
  ID = 5454524,
  vessels = 4,
  durability = 84050,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454525] = {
  ID = 5454525,
  vessels = 4,
  durability = 89790,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454526] = {
  ID = 5454526,
  vessels = 4,
  durability = 95716,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454527] = {
  ID = 5454527,
  vessels = 4,
  durability = 101827,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454528] = {
  ID = 5454528,
  vessels = 4,
  durability = 108124,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454529] = {
  ID = 5454529,
  vessels = 4,
  durability = 148043,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454530] = {
  ID = 5454530,
  vessels = 4,
  durability = 158021,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454531] = {
  ID = 5454531,
  vessels = 4,
  durability = 168305,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454532] = {
  ID = 5454532,
  vessels = 4,
  durability = 178895,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454533] = {
  ID = 5454533,
  vessels = 4,
  durability = 189791,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454534] = {
  ID = 5454534,
  vessels = 4,
  durability = 200994,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454535] = {
  ID = 5454535,
  vessels = 4,
  durability = 212503,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454536] = {
  ID = 5454536,
  vessels = 4,
  durability = 224317,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454537] = {
  ID = 5454537,
  vessels = 4,
  durability = 236439,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454538] = {
  ID = 5454538,
  vessels = 4,
  durability = 248866,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454539] = {
  ID = 5454539,
  vessels = 4,
  durability = 292541,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454540] = {
  ID = 5454540,
  vessels = 4,
  durability = 312768,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454541] = {
  ID = 5454541,
  vessels = 4,
  durability = 333558,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454542] = {
  ID = 5454542,
  vessels = 4,
  durability = 354913,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454543] = {
  ID = 5454543,
  vessels = 4,
  durability = 376831,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454544] = {
  ID = 5454544,
  vessels = 4,
  durability = 399312,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454545] = {
  ID = 5454545,
  vessels = 4,
  durability = 422358,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454546] = {
  ID = 5454546,
  vessels = 4,
  durability = 445967,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454547] = {
  ID = 5454547,
  vessels = 4,
  durability = 470140,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454548] = {
  ID = 5454548,
  vessels = 4,
  durability = 494877,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454549] = {
  ID = 5454549,
  vessels = 4,
  durability = 560418,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454550] = {
  ID = 5454550,
  vessels = 4,
  durability = 596983,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454551] = {
  ID = 5454551,
  vessels = 4,
  durability = 634441,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454552] = {
  ID = 5454552,
  vessels = 4,
  durability = 672792,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454553] = {
  ID = 5454553,
  vessels = 4,
  durability = 712035,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454554] = {
  ID = 5454554,
  vessels = 4,
  durability = 752171,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454555] = {
  ID = 5454555,
  vessels = 4,
  durability = 793199,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454556] = {
  ID = 5454556,
  vessels = 4,
  durability = 835120,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454557] = {
  ID = 5454557,
  vessels = 4,
  durability = 877933,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454558] = {
  ID = 5454558,
  vessels = 4,
  durability = 921640,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454559] = {
  ID = 5454559,
  vessels = 4,
  durability = 1071474,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454560] = {
  ID = 5454560,
  vessels = 4,
  durability = 1137217,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454561] = {
  ID = 5454561,
  vessels = 4,
  durability = 1204380,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454562] = {
  ID = 5454562,
  vessels = 4,
  durability = 1272965,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454563] = {
  ID = 5454563,
  vessels = 4,
  durability = 1342970,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454564] = {
  ID = 5454564,
  vessels = 4,
  durability = 1414396,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454565] = {
  ID = 5454565,
  vessels = 4,
  durability = 1487243,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454566] = {
  ID = 5454566,
  vessels = 4,
  durability = 1561510,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454567] = {
  ID = 5454567,
  vessels = 4,
  durability = 1614304,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454568] = {
  ID = 5454568,
  vessels = 4,
  durability = 1714308,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454569] = {
  ID = 5454569,
  vessels = 4,
  durability = 2278627,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454570] = {
  ID = 5454570,
  vessels = 4,
  durability = 2450558,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454571] = {
  ID = 5454571,
  vessels = 4,
  durability = 2626022,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454572] = {
  ID = 5454572,
  vessels = 4,
  durability = 2805037,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454573] = {
  ID = 5454573,
  vessels = 4,
  durability = 2987585,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454574] = {
  ID = 5454574,
  vessels = 4,
  durability = 3173675,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454575] = {
  ID = 5454575,
  vessels = 4,
  durability = 3363315,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454576] = {
  ID = 5454576,
  vessels = 4,
  durability = 3556488,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454577] = {
  ID = 5454577,
  vessels = 4,
  durability = 3753212,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454578] = {
  ID = 5454578,
  vessels = 4,
  durability = 4090197,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454579] = {
  ID = 5454579,
  vessels = 4,
  durability = 4964353,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454580] = {
  ID = 5454580,
  vessels = 4,
  durability = 5350157,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454581] = {
  ID = 5454581,
  vessels = 4,
  durability = 5743219,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454582] = {
  ID = 5454582,
  vessels = 4,
  durability = 6143556,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454583] = {
  ID = 5454583,
  vessels = 4,
  durability = 6551152,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454584] = {
  ID = 5454584,
  vessels = 4,
  durability = 6966023,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454585] = {
  ID = 5454585,
  vessels = 4,
  durability = 7388162,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454586] = {
  ID = 5454586,
  vessels = 4,
  durability = 7817557,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454587] = {
  ID = 5454587,
  vessels = 4,
  durability = 8389920,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454588] = {
  ID = 5454588,
  vessels = 4,
  durability = 9222371,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454589] = {
  ID = 5454589,
  vessels = 4,
  durability = 13093204,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454590] = {
  ID = 5454590,
  vessels = 4,
  durability = 14424923,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454591] = {
  ID = 5454591,
  vessels = 4,
  durability = 15632757,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454592] = {
  ID = 5454592,
  vessels = 4,
  durability = 16681484,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454593] = {
  ID = 5454593,
  vessels = 4,
  durability = 17747598,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454594] = {
  ID = 5454594,
  vessels = 4,
  durability = 18831101,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454595] = {
  ID = 5454595,
  vessels = 4,
  durability = 19931980,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454596] = {
  ID = 5454596,
  vessels = 4,
  durability = 21050257,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454597] = {
  ID = 5454597,
  vessels = 4,
  durability = 22973275,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454598] = {
  ID = 5454598,
  vessels = 4,
  durability = 24727247,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5454599] = {
  ID = 5454599,
  vessels = 4,
  durability = 27453826,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5455000] = {
  ID = 5455000,
  vessels = 5,
  durability = 1125,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455001] = {
  ID = 5455001,
  vessels = 5,
  durability = 2205,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455002] = {
  ID = 5455002,
  vessels = 5,
  durability = 3448,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455003] = {
  ID = 5455003,
  vessels = 5,
  durability = 6160,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455004] = {
  ID = 5455004,
  vessels = 5,
  durability = 7823,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455005] = {
  ID = 5455005,
  vessels = 5,
  durability = 9649,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455006] = {
  ID = 5455006,
  vessels = 5,
  durability = 11637,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455007] = {
  ID = 5455007,
  vessels = 5,
  durability = 13788,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455008] = {
  ID = 5455008,
  vessels = 5,
  durability = 16104,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455009] = {
  ID = 5455009,
  vessels = 5,
  durability = 18614,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455010] = {
  ID = 5455010,
  vessels = 5,
  durability = 21288,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455011] = {
  ID = 5455011,
  vessels = 5,
  durability = 24130,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455012] = {
  ID = 5455012,
  vessels = 5,
  durability = 27138,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455013] = {
  ID = 5455013,
  vessels = 5,
  durability = 30312,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455014] = {
  ID = 5455014,
  vessels = 5,
  durability = 33652,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455015] = {
  ID = 5455015,
  vessels = 5,
  durability = 37157,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455016] = {
  ID = 5455016,
  vessels = 5,
  durability = 40829,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455017] = {
  ID = 5455017,
  vessels = 5,
  durability = 44668,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455018] = {
  ID = 5455018,
  vessels = 5,
  durability = 48673,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455019] = {
  ID = 5455019,
  vessels = 5,
  durability = 54015,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455020] = {
  ID = 5455020,
  vessels = 5,
  durability = 58501,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455021] = {
  ID = 5455021,
  vessels = 5,
  durability = 64443,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455022] = {
  ID = 5455022,
  vessels = 5,
  durability = 69348,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455023] = {
  ID = 5455023,
  vessels = 5,
  durability = 74427,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455024] = {
  ID = 5455024,
  vessels = 5,
  durability = 79682,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455025] = {
  ID = 5455025,
  vessels = 5,
  durability = 85112,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455026] = {
  ID = 5455026,
  vessels = 5,
  durability = 90717,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455027] = {
  ID = 5455027,
  vessels = 5,
  durability = 96497,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455028] = {
  ID = 5455028,
  vessels = 5,
  durability = 102453,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455029] = {
  ID = 5455029,
  vessels = 5,
  durability = 140163,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455030] = {
  ID = 5455030,
  vessels = 5,
  durability = 149595,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455031] = {
  ID = 5455031,
  vessels = 5,
  durability = 159316,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455032] = {
  ID = 5455032,
  vessels = 5,
  durability = 169327,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455033] = {
  ID = 5455033,
  vessels = 5,
  durability = 179626,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455034] = {
  ID = 5455034,
  vessels = 5,
  durability = 190215,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455035] = {
  ID = 5455035,
  vessels = 5,
  durability = 201093,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455036] = {
  ID = 5455036,
  vessels = 5,
  durability = 212260,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455037] = {
  ID = 5455037,
  vessels = 5,
  durability = 223716,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455038] = {
  ID = 5455038,
  vessels = 5,
  durability = 235462,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455039] = {
  ID = 5455039,
  vessels = 5,
  durability = 276719,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455040] = {
  ID = 5455040,
  vessels = 5,
  durability = 295831,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455041] = {
  ID = 5455041,
  vessels = 5,
  durability = 315475,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455042] = {
  ID = 5455042,
  vessels = 5,
  durability = 335651,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455043] = {
  ID = 5455043,
  vessels = 5,
  durability = 356360,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455044] = {
  ID = 5455044,
  vessels = 5,
  durability = 377602,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455045] = {
  ID = 5455045,
  vessels = 5,
  durability = 399375,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455046] = {
  ID = 5455046,
  vessels = 5,
  durability = 421682,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455047] = {
  ID = 5455047,
  vessels = 5,
  durability = 444520,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455048] = {
  ID = 5455048,
  vessels = 5,
  durability = 467891,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455049] = {
  ID = 5455049,
  vessels = 5,
  durability = 529800,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455050] = {
  ID = 5455050,
  vessels = 5,
  durability = 564343,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455051] = {
  ID = 5455051,
  vessels = 5,
  durability = 599728,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455052] = {
  ID = 5455052,
  vessels = 5,
  durability = 635957,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455053] = {
  ID = 5455053,
  vessels = 5,
  durability = 673028,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455054] = {
  ID = 5455054,
  vessels = 5,
  durability = 710943,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455055] = {
  ID = 5455055,
  vessels = 5,
  durability = 749700,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455056] = {
  ID = 5455056,
  vessels = 5,
  durability = 789301,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455057] = {
  ID = 5455057,
  vessels = 5,
  durability = 829745,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455058] = {
  ID = 5455058,
  vessels = 5,
  durability = 871031,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455059] = {
  ID = 5455059,
  vessels = 5,
  durability = 1012550,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455060] = {
  ID = 5455060,
  vessels = 5,
  durability = 1074649,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455061] = {
  ID = 5455061,
  vessels = 5,
  durability = 1138090,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455062] = {
  ID = 5455062,
  vessels = 5,
  durability = 1202873,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455063] = {
  ID = 5455063,
  vessels = 5,
  durability = 1268998,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455064] = {
  ID = 5455064,
  vessels = 5,
  durability = 1336464,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455065] = {
  ID = 5455065,
  vessels = 5,
  durability = 1405272,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455066] = {
  ID = 5455066,
  vessels = 5,
  durability = 1475422,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455067] = {
  ID = 5455067,
  vessels = 5,
  durability = 1521212,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455068] = {
  ID = 5455068,
  vessels = 5,
  durability = 1619748,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455069] = {
  ID = 5455069,
  vessels = 5,
  durability = 2147959,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455070] = {
  ID = 5455070,
  vessels = 5,
  durability = 2309884,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455071] = {
  ID = 5455071,
  vessels = 5,
  durability = 2475145,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455072] = {
  ID = 5455072,
  vessels = 5,
  durability = 2643749,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455073] = {
  ID = 5455073,
  vessels = 5,
  durability = 2815680,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455074] = {
  ID = 5455074,
  vessels = 5,
  durability = 2990946,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455075] = {
  ID = 5455075,
  vessels = 5,
  durability = 3169548,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455076] = {
  ID = 5455076,
  vessels = 5,
  durability = 3351485,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455077] = {
  ID = 5455077,
  vessels = 5,
  durability = 3536765,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455078] = {
  ID = 5455078,
  vessels = 5,
  durability = 3859270,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455079] = {
  ID = 5455079,
  vessels = 5,
  durability = 4684373,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455080] = {
  ID = 5455080,
  vessels = 5,
  durability = 5048253,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455081] = {
  ID = 5455081,
  vessels = 5,
  durability = 5418978,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455082] = {
  ID = 5455082,
  vessels = 5,
  durability = 5796556,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455083] = {
  ID = 5455083,
  vessels = 5,
  durability = 6180997,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455084] = {
  ID = 5455084,
  vessels = 5,
  durability = 6572281,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455085] = {
  ID = 5455085,
  vessels = 5,
  durability = 6970429,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455086] = {
  ID = 5455086,
  vessels = 5,
  durability = 7375420,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455087] = {
  ID = 5455087,
  vessels = 5,
  durability = 7909053,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455088] = {
  ID = 5455088,
  vessels = 5,
  durability = 8675330,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455089] = {
  ID = 5455089,
  vessels = 5,
  durability = 12344546,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455090] = {
  ID = 5455090,
  vessels = 5,
  durability = 13612412,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455091] = {
  ID = 5455091,
  vessels = 5,
  durability = 14741915,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455092] = {
  ID = 5455092,
  vessels = 5,
  durability = 15730141,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455093] = {
  ID = 5455093,
  vessels = 5,
  durability = 16734749,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455094] = {
  ID = 5455094,
  vessels = 5,
  durability = 17755740,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455095] = {
  ID = 5455095,
  vessels = 5,
  durability = 18793114,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455096] = {
  ID = 5455096,
  vessels = 5,
  durability = 19846870,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455097] = {
  ID = 5455097,
  vessels = 5,
  durability = 21621075,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455098] = {
  ID = 5455098,
  vessels = 5,
  durability = 23272628,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455099] = {
  ID = 5455099,
  vessels = 5,
  durability = 25905724,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455500] = {
  ID = 5455500,
  vessels = 5,
  durability = 1522,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455501] = {
  ID = 5455501,
  vessels = 5,
  durability = 2980,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455502] = {
  ID = 5455502,
  vessels = 5,
  durability = 4658,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455503] = {
  ID = 5455503,
  vessels = 5,
  durability = 7916,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455504] = {
  ID = 5455504,
  vessels = 5,
  durability = 10132,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455505] = {
  ID = 5455505,
  vessels = 5,
  durability = 12569,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455506] = {
  ID = 5455506,
  vessels = 5,
  durability = 15224,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455507] = {
  ID = 5455507,
  vessels = 5,
  durability = 18099,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455508] = {
  ID = 5455508,
  vessels = 5,
  durability = 21196,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455509] = {
  ID = 5455509,
  vessels = 5,
  durability = 24550,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455510] = {
  ID = 5455510,
  vessels = 5,
  durability = 28125,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455511] = {
  ID = 5455511,
  vessels = 5,
  durability = 31926,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455512] = {
  ID = 5455512,
  vessels = 5,
  durability = 35951,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455513] = {
  ID = 5455513,
  vessels = 5,
  durability = 40200,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455514] = {
  ID = 5455514,
  vessels = 5,
  durability = 44673,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455515] = {
  ID = 5455515,
  vessels = 5,
  durability = 49366,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455516] = {
  ID = 5455516,
  vessels = 5,
  durability = 54286,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455517] = {
  ID = 5455517,
  vessels = 5,
  durability = 59430,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455518] = {
  ID = 5455518,
  vessels = 5,
  durability = 64798,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455519] = {
  ID = 5455519,
  vessels = 5,
  durability = 71958,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455520] = {
  ID = 5455520,
  vessels = 5,
  durability = 77958,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455521] = {
  ID = 5455521,
  vessels = 5,
  durability = 84969,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455522] = {
  ID = 5455522,
  vessels = 5,
  durability = 91492,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455523] = {
  ID = 5455523,
  vessels = 5,
  durability = 98250,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455524] = {
  ID = 5455524,
  vessels = 5,
  durability = 105247,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455525] = {
  ID = 5455525,
  vessels = 5,
  durability = 112473,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455526] = {
  ID = 5455526,
  vessels = 5,
  durability = 119939,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455527] = {
  ID = 5455527,
  vessels = 5,
  durability = 127635,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455528] = {
  ID = 5455528,
  vessels = 5,
  durability = 135565,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455529] = {
  ID = 5455529,
  vessels = 5,
  durability = 186408,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455530] = {
  ID = 5455530,
  vessels = 5,
  durability = 198988,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455531] = {
  ID = 5455531,
  vessels = 5,
  durability = 211954,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455532] = {
  ID = 5455532,
  vessels = 5,
  durability = 225306,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455533] = {
  ID = 5455533,
  vessels = 5,
  durability = 239039,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455534] = {
  ID = 5455534,
  vessels = 5,
  durability = 253164,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455535] = {
  ID = 5455535,
  vessels = 5,
  durability = 267675,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455536] = {
  ID = 5455536,
  vessels = 5,
  durability = 282572,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455537] = {
  ID = 5455537,
  vessels = 5,
  durability = 297856,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455538] = {
  ID = 5455538,
  vessels = 5,
  durability = 313520,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455539] = {
  ID = 5455539,
  vessels = 5,
  durability = 368990,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455540] = {
  ID = 5455540,
  vessels = 5,
  durability = 394488,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455541] = {
  ID = 5455541,
  vessels = 5,
  durability = 420690,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455542] = {
  ID = 5455542,
  vessels = 5,
  durability = 447609,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455543] = {
  ID = 5455543,
  vessels = 5,
  durability = 475232,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455544] = {
  ID = 5455544,
  vessels = 5,
  durability = 503572,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455545] = {
  ID = 5455545,
  vessels = 5,
  durability = 532623,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455546] = {
  ID = 5455546,
  vessels = 5,
  durability = 562377,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455547] = {
  ID = 5455547,
  vessels = 5,
  durability = 592848,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455548] = {
  ID = 5455548,
  vessels = 5,
  durability = 624023,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455549] = {
  ID = 5455549,
  vessels = 5,
  durability = 707039,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455550] = {
  ID = 5455550,
  vessels = 5,
  durability = 753003,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455551] = {
  ID = 5455551,
  vessels = 5,
  durability = 800087,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455552] = {
  ID = 5455552,
  vessels = 5,
  durability = 848292,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455553] = {
  ID = 5455553,
  vessels = 5,
  durability = 897618,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455554] = {
  ID = 5455554,
  vessels = 5,
  durability = 948072,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455555] = {
  ID = 5455555,
  vessels = 5,
  durability = 999640,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455556] = {
  ID = 5455556,
  vessels = 5,
  durability = 1052327,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455557] = {
  ID = 5455557,
  vessels = 5,
  durability = 1106136,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455558] = {
  ID = 5455558,
  vessels = 5,
  durability = 1161065,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455559] = {
  ID = 5455559,
  vessels = 5,
  durability = 1350893,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455560] = {
  ID = 5455560,
  vessels = 5,
  durability = 1433223,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455561] = {
  ID = 5455561,
  vessels = 5,
  durability = 1517338,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455562] = {
  ID = 5455562,
  vessels = 5,
  durability = 1603220,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455563] = {
  ID = 5455563,
  vessels = 5,
  durability = 1690877,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455564] = {
  ID = 5455564,
  vessels = 5,
  durability = 1780319,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455565] = {
  ID = 5455565,
  vessels = 5,
  durability = 1871527,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455566] = {
  ID = 5455566,
  vessels = 5,
  durability = 1964521,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455567] = {
  ID = 5455567,
  vessels = 5,
  durability = 2031540,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455568] = {
  ID = 5455568,
  vessels = 5,
  durability = 2155815,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455569] = {
  ID = 5455569,
  vessels = 5,
  durability = 2874836,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455570] = {
  ID = 5455570,
  vessels = 5,
  durability = 3090839,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455571] = {
  ID = 5455571,
  vessels = 5,
  durability = 3311287,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455572] = {
  ID = 5455572,
  vessels = 5,
  durability = 3536193,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455573] = {
  ID = 5455573,
  vessels = 5,
  durability = 3765534,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455574] = {
  ID = 5455574,
  vessels = 5,
  durability = 3999321,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455575] = {
  ID = 5455575,
  vessels = 5,
  durability = 4237554,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455576] = {
  ID = 5455576,
  vessels = 5,
  durability = 4480233,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455577] = {
  ID = 5455577,
  vessels = 5,
  durability = 4727370,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455578] = {
  ID = 5455578,
  vessels = 5,
  durability = 5146119,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455579] = {
  ID = 5455579,
  vessels = 5,
  durability = 6254440,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455580] = {
  ID = 5455580,
  vessels = 5,
  durability = 6738583,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455581] = {
  ID = 5455581,
  vessels = 5,
  durability = 7231839,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455582] = {
  ID = 5455582,
  vessels = 5,
  durability = 7734209,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455583] = {
  ID = 5455583,
  vessels = 5,
  durability = 8245692,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455584] = {
  ID = 5455584,
  vessels = 5,
  durability = 8766288,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455585] = {
  ID = 5455585,
  vessels = 5,
  durability = 9295997,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455586] = {
  ID = 5455586,
  vessels = 5,
  durability = 9834819,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455587] = {
  ID = 5455587,
  vessels = 5,
  durability = 10531654,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455588] = {
  ID = 5455588,
  vessels = 5,
  durability = 11518410,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455589] = {
  ID = 5455589,
  vessels = 5,
  durability = 16553117,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455590] = {
  ID = 5455590,
  vessels = 5,
  durability = 18219013,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455591] = {
  ID = 5455591,
  vessels = 5,
  durability = 19707741,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455592] = {
  ID = 5455592,
  vessels = 5,
  durability = 21024691,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455593] = {
  ID = 5455593,
  vessels = 5,
  durability = 22363461,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455594] = {
  ID = 5455594,
  vessels = 5,
  durability = 23724051,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455595] = {
  ID = 5455595,
  vessels = 5,
  durability = 25106447,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455596] = {
  ID = 5455596,
  vessels = 5,
  durability = 26510677,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455597] = {
  ID = 5455597,
  vessels = 5,
  durability = 28807312,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455598] = {
  ID = 5455598,
  vessels = 5,
  durability = 31014549,
  Avatar = "head25",
  Ship = "ship5"
}
Monster[5455599] = {
  ID = 5455599,
  vessels = 5,
  durability = 34658643,
  Avatar = "head25",
  Ship = "ship5"
}
