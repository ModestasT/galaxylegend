local GameData = GameData
FleetDataAccessHelper = {}
FleetDataAccessHelper.Fleets = {}
FleetDataAccessHelper.FleetsBasic = {}
FleetDataAccessHelper.ATK_TYPE_common = 0
FleetDataAccessHelper.ATK_TYPE_physics = 1
FleetDataAccessHelper.ATK_TYPE_energy = 2
function FleetDataAccessHelper:GetFleet(fleetId, fleetLevel)
  if fleetLevel == nil or fleetLevel < 0 then
    fleetLevel = 0
  end
  local fleetKey = fleetId .. "_" .. fleetLevel
  if FleetDataAccessHelper.Fleets[fleetKey] then
    return
  end
  for k, v in pairs(GameData.heros.enhance) do
    if v.ID == fleetId and fleetLevel == v.LEVEL then
      FleetDataAccessHelper.Fleets[fleetKey] = v
      return v
    end
  end
  return nil
end
function FleetDataAccessHelper:GetFleetBasic(fleetId, fleetLevel)
  if fleetLevel == nil or fleetLevel < 0 then
    fleetLevel = 0
  end
  local fleetKey = fleetId .. "_" .. fleetLevel
  if FleetDataAccessHelper.FleetsBasic[fleetKey] then
    return FleetDataAccessHelper.FleetsBasic[fleetKey]
  end
  for k, v in pairs(GameData.heros.basic) do
    if v.ID == fleetId and fleetLevel == v.LEVEL then
      FleetDataAccessHelper.FleetsBasic[fleetKey] = v
      return v
    end
  end
  return nil
end
function FleetDataAccessHelper:GetFleetAvatar(fleetId, sex, fleetLevel)
  if fleetId == 1 then
    return ...
  elseif fleetId == 3 then
    return (...), GameUtils, ...
  end
  local fleet = FleetDataAccessHelper:GetFleet(fleetId, fleetLevel)
  if fleet then
    return fleet.AVATAR
  end
  return nil
end
function FleetDataAccessHelper:GetHead(fleetId, areaID, fleetLevel)
  DebugOut("GetHead find head in ", fleetId, areaID)
  if fleetId == 1 then
    return ...
  elseif fleetId == 3 then
    return (...), GameUtils, ...
  end
  local avatar = FleetDataAccessHelper:GetFleetAvatar(fleetId, fleetLevel)
  if avatar then
    return avatar
  end
  avatar = GameDataAccessHelper:GetMonsterHeadImage(areaID, fleetId)
  if avatar then
    return avatar
  end
  assert(false)
  return nil
end
function FleetDataAccessHelper:GetShip(fleetId, areaID, fleetLevel)
  DebugOut("GetShip find ship in ", fleetId, areaID)
  local shipImage
  local fleet = FleetDataAccessHelper:GetFleet(fleetId, fleetLevel)
  if fleet then
    shipImage = fleet.SHIP
    if shipImage then
      return shipImage
    end
  end
  shipImage = GameDataAccessHelper:GetMonsterVesselsImage(areaID, fleetId)
  if shipImage then
    return shipImage
  end
  DebugOut("Error Cannot find ship in ", fleetId, areaID)
  return "ship9001"
end
function FleetDataAccessHelper:GetCommanderBasicInfo(identity, fleetLevel)
  return ...
end
function FleetDataAccessHelper:GetCommanderAbility(identity, fleetLevel)
  return ...
end
function FleetDataAccessHelper:GetCommanderVessels(identity, fleetLevel)
  local fleetBasic = FleetDataAccessHelper:GetFleetBasic(fleetId, fleetLevel)
  return fleetBasic.vessels
end
function FleetDataAccessHelper:GetCommanderName(index, fleetLevel)
  local fleet = FleetDataAccessHelper:GetFleet(index, fleetLevel)
  return fleet.NAME
end
function FleetDataAccessHelper:GetCommanderVesselsName(index, fleetLevel)
  DebugOut(index)
  local fleet = FleetDataAccessHelper:GetFleet(index, fleetLevel)
  local vessels_name = fleet.VESSELS_NAME
  DebugOut(vessels_name)
  return vessels_name
end
function FleetDataAccessHelper:GetCommanderVesselsType(index, fleetLevel)
  local vessels_type = "TYPE_" .. tostring(FleetDataAccessHelper:GetCommanderVessels(index, fleetLevel))
  return vessels_type
end
function FleetDataAccessHelper:GetCommanderVesselsImage(commander_identity, fleetLevel)
  local fleet = FleetDataAccessHelper:GetFleet(commander_identity, fleetLevel)
  return fleet.SHIP
end
function FleetDataAccessHelper:GetCommanderDesc(identity, fleetLevel)
  local fleet = FleetDataAccessHelper:GetFleet(identity, fleetLevel)
  return ...
end
local ColorToFrame = {
  [1] = "green",
  [2] = "blue",
  [3] = "purple",
  [4] = "orange",
  [5] = "golden",
  [6] = "red"
}
local ColorToColorNum = {
  [1] = "#37DD59",
  [2] = "#98FFFD",
  [3] = "#D996F4",
  [4] = "#D9BB59",
  [5] = "#FFCC00",
  [6] = "#FF9900"
}
function FleetDataAccessHelper:GetFleetVesselsFrameByVessel(vessel)
  return "TYPE_" .. vessel
end
function FleetDataAccessHelper:GetFleetVesselsTypeStringByVessel(vessel)
  local vessels_name = GameLoader:GetGameText("LC_FLEET_TYPE_" .. vessel)
  return vessels_name
end
function FleetDataAccessHelper:GetFleetSpellFrameBySpell(spellID)
  return "TYPE_" .. GameDataAccessHelper:GetSkillRangeType(spellID)
end
function FleetDataAccessHelper:GetFleetDisplayNameByName(name, fleetId)
  local commander_name
  if fleetId == 1 then
    local userinfo = GameGlobalData:GetUserInfo()
    commander_name = GameUtils:GetUserDisplayName(userinfo.name)
  else
    commander_name = GameLoader:GetGameText("LC_NPC_" .. name)
  end
  return commander_name
end
function FleetDataAccessHelper:GetFleetColorDisplayName(name, fleetId, color)
  local commander_name
  if fleetId == 1 then
    local userinfo = GameGlobalData:GetUserInfo()
    commander_name = GameUtils:GetUserDisplayName(userinfo.name)
  else
    commander_name = GameLoader:GetGameText("LC_NPC_" .. name)
  end
  commander_name = "<font color='" .. ColorToColorNum[color] .. "'>" .. commander_name .. "</font>"
  return commander_name
end
function FleetDataAccessHelper:GetFleetLevelDisplayName(name, fleetId, color, level, forceOrigin)
  local commander_name
  if fleetId == 1 and not forceOrigin then
    local userinfo = GameGlobalData:GetUserInfo()
    commander_name = GameUtils:GetUserDisplayName(userinfo.name)
  else
    commander_name = GameLoader:GetGameText("LC_NPC_" .. name)
  end
  if level == nil then
    level = 0
  end
  local level_info = GameGlobalData:GetData("levelinfo")
  if level_info.level < 35 or level <= 0 then
    return commander_name
  end
  if level < 16 then
    commander_name = commander_name .. " [+" .. level .. "]"
  elseif level == 16 then
    commander_name = commander_name .. " [+T]"
  else
    commander_name = commander_name .. " [+T" .. level - 16 .. "]"
  end
  return commander_name
end
function FleetDataAccessHelper:GetFleetLevelDisplayName_2(name, fleetId, color, level, forceOrigin)
  local commander_name
  if fleetId == 1 and forceOrigin then
    commander_name = GameLoader:GetGameText("LC_NPC_NPC_New")
  else
    commander_name = GameLoader:GetGameText("LC_NPC_" .. name)
  end
  if level == nil then
    level = 0
  end
  local level_info = GameGlobalData:GetData("levelinfo")
  if level_info.level < 35 or level <= 0 then
    return commander_name
  end
  if level < 16 then
    commander_name = commander_name .. " [+" .. level .. "]"
  elseif level == 16 then
    commander_name = commander_name .. " [+T]"
  else
    commander_name = commander_name .. " [+T" .. level - 16 .. "]"
  end
  return commander_name
end
function FleetDataAccessHelper:GetFleetColorFrameByColor(color)
  return ColorToFrame[color]
end
function FleetDataAccessHelper:GetFleetColorNumberByColor(color)
  return ColorToColorNum[color]
end
function FleetDataAccessHelper:GetFleetAvatarFrameByAvatar(avatar, fleetID, fleetLevel)
  if fleetID == 3 then
    return (...), GameUtils, nil
  else
    if GameDataAccessHelper:IsHeadResNeedDownload(avatar) then
      if GameDataAccessHelper:CheckFleetHasAvataImage(avatar) then
        return avatar
      else
        return "head9001"
      end
    end
    return avatar
  end
end
function FleetDataAccessHelper:GetFleetSexFrame(sex)
  if sex == 1 then
    sex = "man"
  elseif sex == 0 then
    sex = "woman"
  else
    sex = "unknown"
  end
  return sex
end
