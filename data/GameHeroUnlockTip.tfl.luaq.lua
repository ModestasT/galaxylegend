local GameStateGlobalState = GameStateManager.GameStateGlobalState
local GameHeroUnlockTip = LuaObjectManager:GetLuaObject("GameHeroUnlockTip")
local GameUIBarLeft = LuaObjectManager:GetLuaObject("GameUIBarLeft")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
GameHeroUnlockTip.all_ntf_content = {}
GameHeroUnlockTip.ntf_showing = false
function GameHeroUnlockTip:OnFSCommand(cmd, arg)
  if cmd == "eraseNewsTip" then
    GameStateGlobalState:EraseObject(self)
    GameHeroUnlockTip.ntf_showing = false
    if #GameHeroUnlockTip.all_ntf_content > 0 then
      GameHeroUnlockTip.DoLoadHeroUnlockCastNotify(content)
    end
  end
end
function GameHeroUnlockTip.LoadCastNotify(content)
  DebugOut("LoadCastNotify")
  DebugTable(content)
  if GameHeroUnlockTip.ntf_showing then
    DebugOut("after show")
    table.insert(GameHeroUnlockTip.all_ntf_content, content)
  else
    GameHeroUnlockTip.DoLoadHeroUnlockCastNotify(content)
  end
end
function GameHeroUnlockTip.DoLoadHeroUnlockCastNotify(content)
  GameHeroUnlockTip.ntf_showing = true
  DebugOut("should not in ")
  if not GameHeroUnlockTip:GetFlashObject() then
    GameHeroUnlockTip:LoadFlashObject()
  end
  GameHeroUnlockTip:GetFlashObject():InvokeASCallback("_root", "showUnlockHeroTip", content)
  GameStateGlobalState:AddObject(GameHeroUnlockTip)
end
function GameHeroUnlockTip.HeroUnionUnlockNtf(content)
  local item = {}
  local title = ""
  title = GameLoader:GetGameText("LC_MENU_LEAGUE_ACTIVATION")
  title = string.gsub(title, "<number1>", tonumber(content.unlock_count))
  item.Title = title
  DebugTable(content.attribute)
  local attributeStr = ""
  for k, v in ipairs(content.attribute or {}) do
    local attributeName = GameLoader:GetGameText("LC_MENU_Equip_param_" .. v.id)
    local valueStr = ""
    if v.is_percent then
      valueStr = "+" .. v.value / 10 .. "%"
    else
      valueStr = "+" .. GameUtils.numberConversion(v.value)
    end
    if attributeStr ~= "" then
      attributeStr = attributeStr .. "\n"
    end
    attributeStr = attributeStr .. attributeName .. valueStr
  end
  item.valueStr = attributeStr
  item.type = "unlockhero"
  DebugOut("dsdasd:", attributeStr)
  GameHeroUnlockTip.LoadCastNotify(item)
end
function GameHeroUnlockTip:Update(dt)
  if self:GetFlashObject() then
    self:GetFlashObject():Update(dt)
    self:GetFlashObject():InvokeASCallback("_root", "OnUpdate", dt)
  end
end
