local GameUIWorldChampion = LuaObjectManager:GetLuaObject("GameUIWorldChampion")
local GameStateWorldChampion = GameStateManager.GameStateWorldChampion
local GameUIWDStuff = LuaObjectManager:GetLuaObject("GameUIWDStuff")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameStateBattlePlay = GameStateManager.GameStateBattlePlay
local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
local GameUIKrypton = LuaObjectManager:GetLuaObject("GameUIKrypton")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local isMatrixAck = false
local isStageAck = false
GameUIWorldChampion.mCurSelectedRoundListItemId = -1
GameUIWorldChampion.mCurRoundItemId = 1
GameUIWorldChampion.mIsWCEnd = false
GameUIWorldChampion.mShareStoryChecked = true
GameUIWorldChampion.mRoundEnable = false
WC_ENTER_STATUS = {
  REQUEST_ENTER = 1,
  ENTERED = 2,
  EXIT = 3
}
GameUIWorldChampion.mEnterStatus = WC_ENTER_STATUS.EXIT
GameUIWorldChampion.mCurState = STATE_WC_SUB_STATE.SUB_STATE_UNKNOW
WC_REWARD_LIST_TYPE = {ROUND_REWARD = 1, PROMOTION_REWARD = 2}
GameUIWorldChampion.mRewardListType = WC_REWARD_LIST_TYPE.PROMOTION_REWARD
GameUIWorldChampion.mRewardList = nil
GameUIWorldChampion.mJoinInfo = {
  joined = false,
  startLv = 0,
  endLv = 0,
  countDownTime = 0
}
GameUIWorldChampion.mSupportShareStoryChecked = true
GameUIWorldChampion.mDataFetchTime = 0
GameUIWorldChampion.mMassElectionInfo = nil
GameUIWorldChampion.mBTreeData = nil
GameUIWorldChampion.mLvRangeList = nil
GameUIWorldChampion.mIsLvChooseShow = false
GameUIWorldChampion.mCurLvRangeId = 1
GameUIWorldChampion.mMySupportList = nil
GameUIWorldChampion.mMySupportListAll = nil
GameUIWorldChampion.mCurRequsetItemId = -1
GameUIWorldChampion.mRankList = nil
GameUIWorldChampion.mPromotionMyBattle = nil
GameUIWorldChampion.mMyLvRangeId = -1
GameUIWorldChampion.mIsMenuShow = false
GameUIWorldChampion.mReqSupportPlayerIdx = -1
GameUIWorldChampion.mExitForFight = false
GameUIWorldChampion.mCanSubmitMatrix = false
GameUIWorldChampion.mOnlyRefreshRankData = false
GameUIWorldChampion.mCurMatrixIndexSelect = 0
GameUIWorldChampion.mFightRecord = {mRoundId = -1, mFightRecordList = nil}
WC_PLAYER_STATUS = {
  NO_JOIN = 0,
  DEAD = 1,
  JOINED = 2,
  MASS_SUCCESS = 3
}
GameUIWorldChampion.mPlayerStatus = WC_PLAYER_STATUS.JOINED
require("FleetMatrix.tfl")
function GameUIWorldChampion:ResetData()
  GameUIWorldChampion.mJoinInfo = {
    joined = false,
    startLv = 0,
    endLv = 0,
    countDownTime = 0
  }
  GameUIWorldChampion.mLvRangeList = nil
  GameUIWorldChampion.mBTreeData = nil
  GameUIWorldChampion.mMassElectionInfo = nil
  GameUIWorldChampion.mCurSelectedRoundListItemId = -1
  GameUIWorldChampion.mCurRoundItemId = 1
  GameUIWorldChampion.mIsLvChooseShow = false
  GameUIWorldChampion.mCurLvRangeId = 1
  GameUIWorldChampion.mMySupportList = nil
  GameUIWorldChampion.mMySupportListAll = nil
  GameUIWorldChampion.mRewardList = nil
  GameUIWorldChampion.mCurRequsetItemId = -1
  GameUIWorldChampion.mRankList = nil
  GameUIWorldChampion.mPromotionMyBattle = nil
  GameUIWorldChampion.mMyLvRangeId = -1
  GameUIWorldChampion.mReqSupportPlayerIdx = -1
  GameUIWorldChampion.mCanSubmitMatrix = false
end
if AutoUpdate.isAndroidDevice then
  function GameUIWorldChampion.OnAndroidBack()
    if GameUIWDStuff.currentMenu == GameUIWDStuff.menuType.menu_type_help then
      local flash_obj = GameUIWDStuff:GetFlashObject()
      if flash_obj then
        flash_obj:InvokeASCallback("_root", "hideHelp")
      end
    elseif GameUIWorldChampion.IsShowHistoryWindow then
      GameUIWorldChampion:GetFlashObject():InvokeASCallback("_root", "HideHistoryChampionPanel")
    else
      GameUIWorldChampion:OnFSCommand("enroll_state_close")
    end
  end
end
function GameUIWorldChampion:OnInitGame()
  GameUIWorldChampion:RegisterAllMsgHandler()
end
function GameUIWorldChampion.GetMatrixsAck()
  isMatrixAck = true
  GameUIWorldChampion.mCurMatrixIndex = FleetMatrix:getCurrentMartixIndexGot(FleetMatrix.MATRIX_TYPE_BUDO)
  GameUIWorldChampion:BuildFlashSelect()
end
function GameUIWorldChampion:OnAddToGameState(game_state)
  DebugOut("GameUIWorldChampion:OnAddToGameState")
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  self:GetFlashObject():SetText("_root.space_arena_2.space_record.space_record_main.button_record_sumit.LC_MENU_SA_FORMATION_BUTTON", GameLoader:GetGameText("LC_MENU_SA_FORMATION_BUTTON"))
  self:GetFlashObject():SetText("_root.space_arena_3.pop.my_battle.my_battle.my_battle_A.button_record_sumit.LC_MENU_SA_FORMATION_BUTTON", GameLoader:GetGameText("LC_MENU_SA_FORMATION_BUTTON"))
  GameUIWorldChampion:CheckDownloadImage()
  if GameUIWorldChampion.mExitForFight then
    GameUIWorldChampion.mEnterStatus = WC_ENTER_STATUS.ENTERED
    GameUIWorldChampion:RequestPromotionDataByLvRange(GameUIWorldChampion.mCurLvRangeId)
    GameUIWorldChampion.mExitForFight = false
  end
  FleetMatrix:GetMatrixsReq(GameUIWorldChampion.GetMatrixsAck, FleetMatrix.MATRIX_TYPE_BUDO)
end
function GameUIWorldChampion:OnEraseFromGameState(game_state)
  GameUIWorldChampion.mEnterStatus = WC_ENTER_STATUS.EXIT
  self:UnloadFlashObject()
  if not GameUIWorldChampion.mExitForFight then
    GameUIWorldChampion:ResetData()
  end
end
function GameUIWorldChampion:CheckDownloadImage()
  if (ext.crc32.crc32("data2/LAZY_LOAD_domination_bg.png") == "" or ext.crc32.crc32("data2/LAZY_LOAD_domination_bg.png") == nil) and not AutoUpdateInBackground:IsHeroFileUpdatedToServer("data2/LAZY_LOAD_domination_bg.png") then
    self:GetFlashObject():ReplaceTexture("LAZY_LOAD_domination_bg.png", "territorial_map_bg.png")
  end
end
function GameUIWorldChampion:OnFSCommand(cmd, arg)
  if GameUIWorldChampion.mCurState == STATE_WC_SUB_STATE.SUB_STATE_PROMOTION and cmd ~= "button_menu" and GameUIWorldChampion.mIsMenuShow then
    GameUIWorldChampion:HideSubMenu()
  end
  if cmd == "UpdateMarix" then
    local index = tonumber(arg)
    local flash_obj = self:GetFlashObject()
    DebugOut("UpdateMarix:" .. index)
    GameUIWorldChampion.mCurMatrixIndexSelect = index
  elseif cmd == "help_click" then
    GameUIWorldChampion:ShowHelp()
  elseif cmd == "btn_join_click" then
    GameUIWorldChampion:RequestJoin()
  elseif cmd == "NeedUpdateRoundListItem" then
    self:UpdateRoundListItem(tonumber(arg))
  elseif cmd == "mas_election_state_close" or cmd == "promotion_state_close" or cmd == "enroll_state_close" then
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateLab)
  elseif cmd == "RoundItemClicked" then
    local itemId = tonumber(arg)
    self:SelectRoundListItem(itemId)
    if -1 == itemId or itemId < GameUIWorldChampion.mCurRoundItemId then
      GameUIWorldChampion:SetMyRoundRecordVisible(false)
    end
  elseif cmd == "mas_election_reward_clicked" then
    GameUIWorldChampion.mRewardListType = WC_REWARD_LIST_TYPE.ROUND_REWARD
    self:SetRoundRewardList()
    self:ShowRoundRewardList()
  elseif cmd == "NeedUpdateRewardListItem" then
    self:UpdateRewardListItem(tonumber(arg))
  elseif cmd == "matrix_submit_clicked" then
    GameUIWorldChampion:RequestSubmitMatrix()
  elseif cmd == "NeedUpdateRoundFightListItem" then
    self:UpdateRoundFightListItem(tonumber(arg))
  elseif cmd == "button_menu" then
    self:ShowSubMenu()
  elseif cmd == "btn_x4_section" then
    GameUIWorldChampion.mIsLvChooseShow = true
    self:ShowLevelChooseList()
  elseif cmd == "btn_x4_reward" then
    GameUIWorldChampion.mRewardListType = WC_REWARD_LIST_TYPE.PROMOTION_REWARD
    self:ShowPromotionRewardList()
    self:SetRoundRewardList()
  elseif cmd == "btn_x2_rank" then
    GameUIWorldChampion:RequestGetRank(GameUIWorldChampion.mCurLvRangeId)
  elseif cmd == "btn_x4_battle" then
    self:ShowPromotionMyBattle()
  elseif cmd == "btn_x2_support" then
    self:ShowPromotionMySupport()
  elseif cmd == "btn_x4_support" then
    self:ShowPromotionMySupport()
  elseif cmd == "button_vs" then
    self:CheckSupport(1)
  elseif cmd == "NodeClicked" then
    self:CheckSupport(tonumber(arg))
  elseif cmd == "NeedUpdateLevelChooseListItem" then
    self:UpdateLevelChooseItem(tonumber(arg))
  elseif cmd == "LevelChooseClickItem" then
    self:RequestPromotionDataByLvRange(tonumber(arg))
  elseif cmd == "NeedUpdateSupportResultListItem" then
    self:UpdateSupportResultListItem(tonumber(arg))
  elseif cmd == "SupportResultClickItem" then
    self:RequestFightHistory(GameUIWorldChampion.mFightRecord.mFightRecordList[tonumber(arg)].battle_id)
  elseif cmd == "SupportPlayer" then
    self:RequestSupportOnePlayer(tonumber(arg))
  elseif cmd == "RoundFightReplay" then
    self:RequestFightHistory(GameUIWorldChampion.mFightRecord.mFightRecordList[tonumber(arg)].battle_id)
  elseif cmd == "NeedUpdateMySupportListItem" then
    self:UpdateMySupportListItem(tonumber(arg))
  elseif cmd == "MySupportClickItem" then
    self:RequestGetPromotionReward(tonumber(arg))
  elseif cmd == "NeedUpdateRankListItem" then
    self:UpdateRankListItem(tonumber(arg))
  elseif cmd == "NeedUpdateRankLevelChooseListItem" then
    self:UpdateRankLevelChooseListItem(tonumber(arg))
  elseif cmd == "RankLevelChooseClickItem" then
    GameUIWorldChampion.mOnlyRefreshRankData = true
    GameUIWorldChampion:RequestGetRank(tonumber(arg))
  elseif cmd == "RewardItemClicked" then
    if GameUIWorldChampion.mRewardList then
      local param = LuaUtils:string_split(arg, ",")
      local itemId = tonumber(param[1])
      local subItemId = tonumber(param[2])
      if GameUIWorldChampion.mRewardList[itemId] and GameUIWorldChampion.mRewardList[itemId].award_list[subItemId] then
        GameUIWorldChampion:ShowItemDetil(GameUIWorldChampion.mRewardList[itemId].award_list[subItemId])
      end
    end
  elseif cmd == "NeedUpdateHistoryChampionListItem" then
    GameUIWorldChampion:UpdateHistoryChampionListItem(tonumber(arg))
  elseif cmd == "NeedUpdateHistoryChampionPeriodChooseListItem" then
    GameUIWorldChampion:UpdateHistoryChampionPeriodChooseListItem(tonumber(arg))
  elseif cmd == "HistoryChampionPeriodChooseClickItem" then
    local oldIdx = GameUIWorldChampion.mCurHistoryChampionPeriodIdx
    GameUIWorldChampion.mCurHistoryChampionPeriodIdx = tonumber(arg)
    DebugOut("oldIdx " .. tostring(oldIdx))
    GameUIWorldChampion:UpdateHistoryChampionPeriodChooseListItem(oldIdx)
    GameUIWorldChampion:UpdateHistoryChampionPeriodChooseListItem(GameUIWorldChampion.mCurHistoryChampionPeriodIdx)
    local period = GameUIWorldChampion.mHistoryChampionData.effect_list[tonumber(arg)]
    GameUIWorldChampion:RequestChampionList(period)
  elseif cmd == "history_champion_click" then
    GameUIWorldChampion.mCurHistoryChampionPeriodIdx = -1
    GameUIWorldChampion:RequestChampionList(0)
  elseif cmd == "enroll_ui_move_in" then
    if GameUIWorldChampion.mCurState == STATE_WC_SUB_STATE.SUB_STATE_BEFORE then
      GameUIWorldChampion.mCurHistoryChampionPeriodIdx = -1
      GameUIWorldChampion:RequestChampionList(0)
    end
  elseif cmd == "mass_history_click" then
    if tonumber(arg) == 1 then
      if GameUIWorldChampion.mPlayerStatus == WC_PLAYER_STATUS.NO_JOIN then
        local tip = GameLoader:GetGameText("LC_MENU_SA_NO_HISTORY_INFO")
        GameTip:Show(tip, 3000)
      else
        GameUIWorldChampion.mCurSelectedMassHistoryRoundId = -1
        GameUIWorldChampion:RequestMassHistoryList(1)
      end
    else
      GameUIWorldChampion:HideMassHistory()
    end
  elseif cmd == "NeedUpdateMassHistoryRoundChooseListItem" then
    GameUIWorldChampion:UpdateMassHistoryRoundChooseListItem(tonumber(arg))
  elseif cmd == "MassHistoryRoundChooseClickItem" then
    GameUIWorldChampion:MassHistoryRoundChooseItemClickHandle(tonumber(arg))
  elseif cmd == "NeedUpdateMassHistoryListItem" then
    GameUIWorldChampion:UpdateMassHistoryListItem(tonumber(arg))
  elseif cmd == "MassHistoryReplayClick" then
    GameUIWorldChampion:MassHistoryReplayClickHandle(tonumber(arg))
  elseif cmd == "btn_x4_champion" then
    GameUIWorldChampion.mCurHistoryChampionPeriodIdx = -1
    GameUIWorldChampion:RequestChampionList(0)
  elseif cmd == "HistoryChampionListItemClicked" then
    local newIdx = tonumber(arg)
    local oldIdx = GameUIWorldChampion.mCurHistoryChampionListItemIdx
    if newIdx == oldIdx then
      GameUIWorldChampion.mCurHistoryChampionListItemIdx = -1
      GameUIWorldChampion:UpdateHistoryChampionListItem(oldIdx)
    else
      GameUIWorldChampion.mCurHistoryChampionListItemIdx = newIdx
      GameUIWorldChampion:UpdateHistoryChampionListItem(oldIdx)
      GameUIWorldChampion:UpdateHistoryChampionListItem(GameUIWorldChampion.mCurHistoryChampionListItemIdx)
    end
    GameUIWorldChampion:RefreshHistoryChampionListItemHeight()
  elseif cmd == "ChampionHistoryReplay" then
    self:RequestFightHistory(tonumber(arg), true)
  elseif cmd == "button_close_pop" then
    self.IsShowHistoryWindow = false
  elseif cmd == "shareJoinStoryCheckboxClicked" then
    if self.mShareStoryChecked then
      self.mShareStoryChecked = false
    else
      self.mShareStoryChecked = true
    end
    local flash_obj = self:GetFlashObject()
    if flash_obj then
      flash_obj:InvokeASCallback("_root", "setCheckBox", self.mShareStoryChecked, "join")
    end
  else
    if cmd == "shareSupportStoryCheckboxClicked" then
      if self.mSupportShareStoryChecked then
        self.mSupportShareStoryChecked = false
      else
        self.mSupportShareStoryChecked = true
      end
      local flash_obj = self:GetFlashObject()
      if flash_obj then
        flash_obj:InvokeASCallback("_root", "setCheckBox", self.mSupportShareStoryChecked, "support")
      end
    else
    end
  end
end
function GameUIWorldChampion:EnterBeforeUI()
  GameUIWorldChampion:SetBeforeUIInfo()
  GameUIWorldChampion:ShowEnrollUI()
end
function GameUIWorldChampion:EnterEnrollUI()
  GameUIWorldChampion:SetEnrollUIInfo()
  GameUIWorldChampion:ShowEnrollUI()
end
function GameUIWorldChampion:EnterRoundUI()
  GameUIWorldChampion:SetMassElectionLvRange()
  GameUIWorldChampion:InitRoundList()
  GameUIWorldChampion:ShowMassElectionUI()
  local flashObj = self:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "SetRoundRewardListVisible", false)
    flashObj:InvokeASCallback("_root", "SetRoundFightListVisible", false)
  end
end
function GameUIWorldChampion:EnterPromotionUI()
  GameUIWorldChampion:SetBTreeData()
  local flashObj = self:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "ShowPromotionUI")
    flashObj:InvokeASCallback("_root", "ShowMainMenu")
  end
end
function GameUIWorldChampion:ShowHelp()
  GameUIWDStuff:Show(GameUIWDStuff.menuType.menu_type_help)
  GameUIWDStuff:SetHelpText(GameLoader:GetGameText("LC_MENU_SA_HELP_TEXT"))
end
function GameUIWorldChampion:ShowEnrollUI()
  local flashObj = self:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "ShowEnrollUI")
  end
end
function GameUIWorldChampion:SetBeforeUIInfo()
  local flashObj = self:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "SetJoinBtnVisible", false)
    local info = GameLoader:GetGameText("LC_MENU_SA_REST_INFO")
    flashObj:InvokeASCallback("_root", "SetEnrollSectionInfo", info, true)
    flashObj:InvokeASCallback("_root", "SetCountDownTimeVisible", false)
  end
end
function GameUIWorldChampion:SetEnrollUIInfo()
  local flashObj = self:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "SetJoinBtnVisible", not GameUIWorldChampion.mJoinInfo.joined)
    local lvRange = ""
    if GameUIWorldChampion.mJoinInfo.startLv == GameUIWorldChampion.mJoinInfo.endLv then
      lvRange = string.format(GameLoader:GetGameText("LC_MENU_SA_LV_ARENR_TITLE"), GameUIWorldChampion.mJoinInfo.startLv)
    else
      lvRange = GameLoader:GetGameText("LC_MENU_Level") .. tostring(GameUIWorldChampion.mJoinInfo.startLv) .. " - " .. GameLoader:GetGameText("LC_MENU_Level") .. tostring(GameUIWorldChampion.mJoinInfo.endLv)
    end
    local facebookEnabled = false
    if not GameUIWorldChampion.mJoinInfo.joined and FacebookGraphStorySharer:IsStoryEnabled(FacebookGraphStorySharer.StoryType.STORY_TYPE_WORLDCHAMPION_JOIN) then
      facebookEnabled = true
    end
    flashObj:InvokeASCallback("_root", "SetEnrollSectionInfo", lvRange, GameUIWorldChampion.mJoinInfo.joined, facebookEnabled, self.mShareStoryChecked)
    local remainTime = GameUIWorldChampion.mJoinInfo.countDownTime
    if remainTime < 0 then
      remainTime = 0
    end
    flashObj:InvokeASCallback("_root", "SetCountDownTimeVisible", true)
    flashObj:InvokeASCallback("_root", "SetCountDownTime", remainTime)
  end
end
function GameUIWorldChampion:InitRoundList()
  local flashObj = self:GetFlashObject()
  if flashObj then
    DebugOut("InitRoundList1:" .. GameUIWorldChampion.mCurSelectedRoundListItemId)
    if GameUIWorldChampion.mMassElectionInfo and GameUIWorldChampion.mMassElectionInfo.total_stages > 0 then
      flashObj:InvokeASCallback("_root", "SetRoundList", GameUIWorldChampion.mMassElectionInfo.total_stages)
      GameUIWorldChampion:SelectRoundListItem(GameUIWorldChampion.mCurSelectedRoundListItemId)
    end
  else
    DebugOut("InitRoundList2:")
  end
end
function GameUIWorldChampion:SetMassElectionLvRange()
  local flashObj = self:GetFlashObject()
  if flashObj and GameUIWorldChampion.mMassElectionInfo then
    local lvRange = ""
    if GameUIWorldChampion.mMassElectionInfo.budo_level_min == GameUIWorldChampion.mMassElectionInfo.budo_level_max then
      lvRange = string.format(GameLoader:GetGameText("LC_MENU_SA_LV_ARENR_TITLE"), GameUIWorldChampion.mMassElectionInfo.budo_level_min)
    else
      lvRange = GameLoader:GetGameText("LC_MENU_Level") .. tostring(GameUIWorldChampion.mMassElectionInfo.budo_level_min) .. " - " .. GameLoader:GetGameText("LC_MENU_Level") .. tostring(GameUIWorldChampion.mMassElectionInfo.budo_level_max)
    end
    DebugOut("SetMassElectionLvRange " .. lvRange)
    flashObj:InvokeASCallback("_root", "SetMassElectionLvRange", lvRange)
  end
end
function GameUIWorldChampion:ShowMassElectionUI()
  local flashObj = self:GetFlashObject()
  if flashObj then
    DebugOut("ShowMassElectionUI==>")
    flashObj:InvokeASCallback("_root", "ShowMassElectionUI")
  end
end
function GameUIWorldChampion:UpdateRoundListItem(itemId)
  local flashObj = self:GetFlashObject()
  DebugOut("UpdateRoundListItem==>" .. itemId)
  if flashObj then
    local isArrowVisible = true
    if 1 == itemId then
      isArrowVisible = false
    end
    local statusFr = "nom"
    local statusStr = ""
    if itemId < GameUIWorldChampion.mCurRoundItemId then
      statusFr = "end"
      statusStr = GameLoader:GetGameText("LC_MENU_SA_END_INFO")
    elseif itemId == GameUIWorldChampion.mCurRoundItemId then
      statusFr = "fat"
      statusStr = GameLoader:GetGameText("LC_MENU_SA_ING_INFO")
    else
      statusFr = "nom"
    end
    local isFocus = false
    if itemId == GameUIWorldChampion.mCurSelectedRoundListItemId then
      isFocus = true
    end
    flashObj:InvokeASCallback("_root", "UpdateRoundListItem", itemId, statusFr, isFocus, isArrowVisible, string.format(GameLoader:GetGameText("LC_MENU_SA_ROUND_LABBLE"), itemId), statusStr)
  end
end
function GameUIWorldChampion:SelectRoundListItem(itemId)
  if itemId > GameUIWorldChampion.mCurRoundItemId then
    DebugOut("GameUIWorldChampion.mCurRoundItemId " .. GameUIWorldChampion.mCurRoundItemId .. " clicked itemId " .. itemId)
    return
  end
  if GameUIWorldChampion.mPlayerStatus ~= WC_PLAYER_STATUS.NO_JOIN or itemId ~= GameUIWorldChampion.mCurRoundItemId then
  end
  local flashObj = self:GetFlashObject()
  if flashObj then
    local isArrowVisible = true
    if 1 == itemId then
      isArrowVisible = false
    end
    local oldStatusFr = "nom"
    local statusStr = ""
    if GameUIWorldChampion.mCurSelectedRoundListItemId < GameUIWorldChampion.mCurRoundItemId then
      oldStatusFr = "end"
      statusStr = GameLoader:GetGameText("LC_MENU_SA_END_INFO")
    elseif GameUIWorldChampion.mCurSelectedRoundListItemId == GameUIWorldChampion.mCurRoundItemId then
      oldStatusFr = "fat"
      statusStr = GameLoader:GetGameText("LC_MENU_SA_ING_INFO")
    else
      oldStatusFr = "nom"
    end
    if -1 ~= GameUIWorldChampion.mCurSelectedRoundListItemId then
      flashObj:InvokeASCallback("_root", "UpdateRoundListItem", GameUIWorldChampion.mCurSelectedRoundListItemId, oldStatusFr, false, GameUIWorldChampion.mCurSelectedRoundListItemId ~= 1, string.format(GameLoader:GetGameText("LC_MENU_SA_ROUND_LABBLE"), GameUIWorldChampion.mCurSelectedRoundListItemId), statusStr)
    end
    local statusFr = "nom"
    local statusStr = ""
    if itemId < GameUIWorldChampion.mCurRoundItemId then
      statusFr = "end"
      statusStr = GameLoader:GetGameText("LC_MENU_SA_END_INFO")
    elseif itemId == GameUIWorldChampion.mCurRoundItemId then
      statusFr = "fat"
      statusStr = GameLoader:GetGameText("LC_MENU_SA_ING_INFO")
    else
      statusFr = "nom"
    end
    flashObj:InvokeASCallback("_root", "UpdateRoundListItem", itemId, statusFr, true, isArrowVisible, string.format(GameLoader:GetGameText("LC_MENU_SA_ROUND_LABBLE"), itemId), statusStr)
    if -1 ~= itemId and itemId == GameUIWorldChampion.mCurRoundItemId then
      DebugOut("Click cur round.")
      flashObj:InvokeASCallback("_root", "HideRoundFightList")
      flashObj:InvokeASCallback("_root", "SetRoundRewardListVisible", false)
      local remainTime = GameUIWorldChampion.mMassElectionInfo.left_battle_time - (os.time() - GameUIWorldChampion.mDataFetchTime)
      if remainTime < 0 then
        remainTime = 0
      end
      flashObj:InvokeASCallback("_root", "SetMyRoundRecord", GameUIWorldChampion.mMassElectionInfo.win_time, GameUIWorldChampion.mMassElectionInfo.failed_time, remainTime)
      GameUIWorldChampion:SetMyRoundRecordVisible(true)
    else
      DebugOut("Click old round.")
      if GameUIWorldChampion.mPlayerStatus == WC_PLAYER_STATUS.NO_JOIN then
        GameUIWorldChampion.mFightRecord.mRoundId = itemId
        GameUIWorldChampion.mFightRecord.mFightRecordList = nil
        GameUIWorldChampion:SetRoundFightList()
        GameUIWorldChampion:ShowRoundFightList()
      else
        GameUIWorldChampion:RequestFightRecordList(itemId)
      end
    end
    GameUIWorldChampion.mCurSelectedRoundListItemId = itemId
  end
end
function GameUIWorldChampion:SetRoundRewardList()
  local flashObj = self:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "SetRewardListType", GameUIWorldChampion.mRewardListType)
    if GameUIWorldChampion.mRewardList then
      flashObj:InvokeASCallback("_root", "SetRewardList", #GameUIWorldChampion.mRewardList)
    else
      DebugOut("GameUIWorldChampion.mRewardList is nil.")
    end
  end
end
function GameUIWorldChampion:ShowRoundRewardList()
  local flashObj = self:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "ShowRoundRewardList")
  end
end
function GameUIWorldChampion.RefreshRewardList()
  GameUIWorldChampion:SetRoundRewardList()
end
function GameUIWorldChampion:UpdateRewardListItem(itemId)
  local flashObj = self:GetFlashObject()
  if flashObj then
    local count = 0
    local icons = ""
    local names = ""
    local nums = ""
    local rewards = GameUIWorldChampion.mRewardList[itemId].award_list
    if rewards and #rewards > 0 then
      for k = 1, #rewards do
        local iconFrame = GameHelper:GetCommonIconFrame(rewards[k], GameUIWorldChampion.RefreshRewardList)
        local itemName = GameHelper:GetAwardTypeText(rewards[k].item_type, rewards[k].number)
        local itemNum = ""
        if rewards[k].item_type == "krypton" or rewards[k].item_type == "item" then
          itemNum = rewards[k].no
        else
          itemNum = rewards[k].number
        end
        icons = icons .. iconFrame .. "\001"
        names = names .. itemName .. "\001"
        nums = nums .. "x " .. itemNum .. "\001"
        count = count + 1
      end
    end
    if self.mRewardListType == WC_REWARD_LIST_TYPE.PROMOTION_REWARD then
      flashObj:InvokeASCallback("_root", "UpdateRewardListItem", itemId, count, GameLoader:GetGameText("LC_MENU_SA_BEST_TITLE_" .. itemId), icons, names, nums)
    else
      flashObj:InvokeASCallback("_root", "UpdateRewardListItem", itemId, count, string.format(GameLoader:GetGameText("LC_MENU_SA_ROUND_LABBLE"), itemId), icons, names, nums)
    end
  end
end
function GameUIWorldChampion:SetRoundFightList()
  local flashObj = self:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "SetRoundFightTitle", string.format(GameLoader:GetGameText("LC_MENU_SA_ROUND_LABBLE"), GameUIWorldChampion.mFightRecord.mRoundId))
    local count = 0
    if GameUIWorldChampion.mFightRecord.mFightRecordList then
      count = #GameUIWorldChampion.mFightRecord.mFightRecordList
    end
    flashObj:InvokeASCallback("_root", "SetRoundFightList", count)
  end
end
function GameUIWorldChampion:ShowRoundFightList()
  local flashObj = self:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "ShowRoundFightList")
  end
end
function GameUIWorldChampion:UpdateRoundFightListItem(itemId)
  local flashObj = self:GetFlashObject()
  if flashObj and itemId <= #GameUIWorldChampion.mFightRecord.mFightRecordList then
    local item = GameUIWorldChampion.mFightRecord.mFightRecordList[itemId]
    local aSex = GameUtils:GetPlayerSexByAvatra(item.win_player.avatar)
    local aAvatar = ""
    if item.win_player.avatar == "male" or item.win_player.avatar == "female" then
      aAvatar = GameDataAccessHelper:GetFleetAvatar(1, item.win_player.main_fleet_level, aSex)
    else
      aAvatar = GameDataAccessHelper:GetFleetAvatar(tonumber(item.win_player.avatar), item.win_player.main_fleet_level, aSex)
    end
    local playerAInfo = {
      playerId = item.win_player.player_id,
      playerName = item.win_player.name,
      playerLv = GameLoader:GetGameText("LC_MENU_Level") .. tostring(item.win_player.player_level),
      avatar = aAvatar,
      isWin = true
    }
    local bSex = GameUtils:GetPlayerSexByAvatra(item.failed_player.avatar)
    local bAvatar = ""
    if item.failed_player.avatar == "male" or item.failed_player.avatar == "female" then
      bAvatar = GameDataAccessHelper:GetFleetAvatar(1, item.failed_player.main_fleet_level, bSex)
    else
      bAvatar = GameDataAccessHelper:GetFleetAvatar(tonumber(item.failed_player.avatar), item.failed_player.main_fleet_level, bSex)
    end
    local playerBInfo = {
      playerId = item.failed_player.player_id,
      playerName = item.failed_player.name,
      playerLv = GameLoader:GetGameText("LC_MENU_Level") .. tostring(item.failed_player.player_level),
      avatar = bAvatar,
      isWin = false
    }
    flashObj:InvokeASCallback("_root", "UpdateRoundFightListItem", itemId, playerAInfo, playerBInfo)
  end
end
function GameUIWorldChampion:SetMyRoundRecordVisible(isVisible)
  self.mRoundEnable = isVisible
  local flashObj = self:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "SetMyRoundRecordVisible", isVisible)
    if isVisible then
      DebugOut(" SetMyRoundRecordVisible GameUIWorldChampion.mPlayerStatus " .. GameUIWorldChampion.mPlayerStatus)
      if GameUIWorldChampion.mPlayerStatus == WC_PLAYER_STATUS.NO_JOIN then
        flashObj:InvokeASCallback("_root", "SetMassSuccessInfoVisible", false)
        flashObj:InvokeASCallback("_root", "SetRoundScoreVisible", false)
        flashObj:InvokeASCallback("_root", "SetMyRoundNoJoinTextVisible", true)
        flashObj:InvokeASCallback("_root", "SetMyRoundNextFightTimeVisible", true)
        flashObj:InvokeASCallback("_root", "SetMyRoundDeadTextVisible", false)
        flashObj:InvokeASCallback("_root", "SetMyRoundSubmitBtnEnable", false)
      elseif GameUIWorldChampion.mPlayerStatus == WC_PLAYER_STATUS.DEAD then
        flashObj:InvokeASCallback("_root", "SetMassSuccessInfoVisible", false)
        flashObj:InvokeASCallback("_root", "SetRoundScoreVisible", false)
        flashObj:InvokeASCallback("_root", "SetMyRoundNoJoinTextVisible", false)
        flashObj:InvokeASCallback("_root", "SetMyRoundNextFightTimeVisible", true)
        flashObj:InvokeASCallback("_root", "SetMyRoundDeadTextVisible", true)
        flashObj:InvokeASCallback("_root", "SetMyRoundSubmitBtnEnable", false)
      elseif GameUIWorldChampion.mPlayerStatus == WC_PLAYER_STATUS.MASS_SUCCESS then
        flashObj:InvokeASCallback("_root", "SetMassSuccessInfoVisible", true)
        flashObj:InvokeASCallback("_root", "SetRoundScoreVisible", false)
        flashObj:InvokeASCallback("_root", "SetMyRoundNoJoinTextVisible", false)
        flashObj:InvokeASCallback("_root", "SetMyRoundNextFightTimeVisible", true)
        flashObj:InvokeASCallback("_root", "SetMyRoundDeadTextVisible", false)
        flashObj:InvokeASCallback("_root", "SetMyRoundSubmitBtnEnable", false)
      else
        flashObj:InvokeASCallback("_root", "SetMassSuccessInfoVisible", false)
        flashObj:InvokeASCallback("_root", "SetRoundScoreVisible", true)
        flashObj:InvokeASCallback("_root", "SetMyRoundNoJoinTextVisible", false)
        flashObj:InvokeASCallback("_root", "SetMyRoundNextFightTimeVisible", true)
        flashObj:InvokeASCallback("_root", "SetMyRoundDeadTextVisible", false)
        flashObj:InvokeASCallback("_root", "SetMyRoundSubmitBtnEnable", 1 == GameUIWorldChampion.mMassElectionInfo.can_submit)
      end
    end
  end
end
function GameUIWorldChampion:SetBTreeData()
  local flashObj = self:GetFlashObject()
  if flashObj and GameUIWorldChampion.mBTreeData then
    local playerIds = ""
    local playerNames = ""
    for k = 1, #GameUIWorldChampion.mBTreeData do
      if GameUIWorldChampion.mBTreeData[k].playerInfo then
        playerIds = playerIds .. GameUIWorldChampion.mBTreeData[k].playerInfo.player_id .. "\001"
        playerNames = playerNames .. GameUIWorldChampion.mBTreeData[k].playerInfo.name .. "\001"
      else
        playerIds = playerIds .. "-1" .. "\001"
        playerNames = playerNames .. "-1" .. "\001"
      end
    end
    flashObj:InvokeASCallback("_root", "SetPlayerData", #GameUIWorldChampion.mBTreeData, playerIds, playerNames)
    if GameUIWorldChampion.mBTreeData[1].playerInfo then
      local bSex = GameUtils:GetPlayerSexByAvatra(GameUIWorldChampion.mBTreeData[1].playerInfo.avatar)
      DebugOut("setbtree", GameUIWorldChampion.mBTreeData[1].playerInfo.avatar)
      local bAvatar = ""
      if GameUIWorldChampion.mBTreeData[1].playerInfo.avatar == "male" or GameUIWorldChampion.mBTreeData[1].playerInfo.avatar == "female" then
        bAvatar = GameDataAccessHelper:GetFleetAvatar(1, GameUIWorldChampion.mBTreeData[1].playerInfo.main_fleet_level, bSex)
      else
        bAvatar = GameDataAccessHelper:GetFleetAvatar(tonumber(GameUIWorldChampion.mBTreeData[1].playerInfo.avatar), GameUIWorldChampion.mBTreeData[1].playerInfo.main_fleet_level, bSex)
      end
      local name = GameUIWorldChampion.mBTreeData[1].playerInfo.name
      local lv = GameLoader:GetGameText("LC_MENU_Level") .. GameUIWorldChampion.mBTreeData[1].playerInfo.player_level
      flashObj:InvokeASCallback("_root", "SetChampionInfo", true, bAvatar, name, lv)
    else
      flashObj:InvokeASCallback("_root", "SetChampionInfo", false, "male", "", "")
    end
  end
end
function GameUIWorldChampion:ShowSubMenu()
  local flashObj = self:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "HideMainMenu")
    if GameUIWorldChampion.mIsWCEnd then
      flashObj:InvokeASCallback("_root", "ShowButton_x2Menu")
    else
      flashObj:InvokeASCallback("_root", "ShowButton_x4Menu")
    end
    GameUIWorldChampion.mIsMenuShow = true
  end
end
function GameUIWorldChampion:HideSubMenu()
  local flashObj = self:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "ShowMainMenu")
    if GameUIWorldChampion.mIsWCEnd then
      flashObj:InvokeASCallback("_root", "HideButton_x2Menu")
    else
      flashObj:InvokeASCallback("_root", "HideButton_x4Menu")
    end
    GameUIWorldChampion.mIsMenuShow = false
  end
end
function GameUIWorldChampion:HideLevelChooseList()
  local flashObj = self:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "HideLevelChooseList")
  end
end
function GameUIWorldChampion:ShowLevelChooseList()
  local flashObj = self:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "ShowLevelChooseList")
    if GameUIWorldChampion.mLvRangeList then
      flashObj:InvokeASCallback("_root", "SetLevelChooseList", #GameUIWorldChampion.mLvRangeList, GameUIWorldChampion.mCurLvRangeId)
    end
  end
end
function GameUIWorldChampion:UpdateLevelChooseItem(itemId)
  local flashObj = self:GetFlashObject()
  if flashObj then
    local lvRange = GameUIWorldChampion:GetLvRangeStr(itemId)
    local isHighLight = GameUIWorldChampion.mCurLvRangeId == itemId
    flashObj:InvokeASCallback("_root", "UpdateLevelChooseListItem", itemId, lvRange, isHighLight)
  end
end
function GameUIWorldChampion:ShowPromotionRewardList()
  local flashObj = self:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "ShowPromotionRewardList")
    flashObj:InvokeASCallback("_root", "SetRewardListType", GameUIWorldChampion.mRewardListType)
  end
end
function GameUIWorldChampion:ShowRankList()
  local flashObj = self:GetFlashObject()
  if flashObj then
    local myName = GameGlobalData:GetData("userinfo").name
    local myRank = 0
    local myPoint = 0
    for k = 1, #GameUIWorldChampion.mRankList do
      local item = GameUIWorldChampion.mRankList[k]
      if item.name == myName then
        myRank = item.rank
        myPoint = item.point
        break
      end
    end
    flashObj:InvokeASCallback("_root", "SetMyRankInfo", myRank, myName, myPoint)
    if not GameUIWorldChampion.mOnlyRefreshRankData then
      flashObj:InvokeASCallback("_root", "ShowRankList")
    end
    GameUIWorldChampion.mOnlyRefreshRankData = false
    flashObj:InvokeASCallback("_root", "SetRankLevelChooseList", #GameUIWorldChampion.mLvRangeList, GameUIWorldChampion.mCurLvRangeId)
    flashObj:InvokeASCallback("_root", "SetRankList", #GameUIWorldChampion.mRankList)
  end
end
function GameUIWorldChampion:HideRankList()
  local flashObj = self:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "HideRankList")
  end
end
function GameUIWorldChampion:UpdateRankLevelChooseListItem(itemId)
  local flashObj = self:GetFlashObject()
  if flashObj then
    local lvRange = GameUIWorldChampion:GetLvRangeStr(itemId)
    local isHighLight = GameUIWorldChampion.mCurLvRangeId == itemId
    flashObj:InvokeASCallback("_root", "UpdateRankLevelChooseListItem", itemId, lvRange, isHighLight)
  end
end
function GameUIWorldChampion:UpdateRankListItem(itemId)
  local flashObj = self:GetFlashObject()
  if flashObj then
    local item = GameUIWorldChampion.mRankList[itemId]
    flashObj:InvokeASCallback("_root", "UpdateRankListItem", itemId, item.rank, item.name, item.point)
  end
end
function GameUIWorldChampion:ShowPromotionMyBattle()
  local flashObj = self:GetFlashObject()
  if flashObj and GameUIWorldChampion.mPromotionMyBattle then
    local lvRange = GameUIWorldChampion:GetLvRangeStr(GameUIWorldChampion.mMyLvRangeId)
    local remainTime = GameUIWorldChampion.mPromotionMyBattle.nextFightTime - (os.time() - GameUIWorldChampion.mDataFetchTime)
    if remainTime < 0 then
      remainTime = 0
    end
    if GameUIWorldChampion.mPromotionMyBattle.myInfo then
      local myInfo = GameUIWorldChampion.mPromotionMyBattle.myInfo
      local rivalInfo = GameUIWorldChampion.mPromotionMyBattle.rivalInfo
      local aSex = GameUtils:GetPlayerSexByAvatra(myInfo.avatar)
      local aAvatar = ""
      if myInfo.avatar == "male" or myInfo.avatar == "female" then
        aAvatar = GameDataAccessHelper:GetFleetAvatar(1, myInfo.main_fleet_level, aSex)
      else
        aAvatar = GameDataAccessHelper:GetFleetAvatar(tonumber(myInfo.avatar), myInfo.main_fleet_level, aSex)
      end
      local playerAInfo = {
        playerId = myInfo.player_id,
        playerName = myInfo.name,
        playerLv = GameLoader:GetGameText("LC_MENU_Level") .. tostring(myInfo.player_level),
        avatar = aAvatar
      }
      local playerBInfo
      if rivalInfo then
        local bSex = GameUtils:GetPlayerSexByAvatra(rivalInfo.avatar)
        local bAvatar = ""
        if rivalInfo.avatar == "male" or rivalInfo.avatar == "female" then
          bAvatar = GameDataAccessHelper:GetFleetAvatar(1, rivalInfo.main_fleet_level, bSex)
        else
          bAvatar = GameDataAccessHelper:GetFleetAvatar(tonumber(rivalInfo.avatar), rivalInfo.main_fleet_level, bSex)
        end
        playerBInfo = {
          playerId = rivalInfo.player_id,
          playerName = rivalInfo.name,
          playerLv = GameLoader:GetGameText("LC_MENU_Level") .. tostring(rivalInfo.player_level),
          avatar = bAvatar
        }
      end
      DebugOut("a:", myInfo.avatar, "b", rivalInfo.avatar)
      flashObj:InvokeASCallback("_root", "SetPromotionMyBattleInfo", lvRange, remainTime, GameUIWorldChampion.mPromotionMyBattle.winCount, GameUIWorldChampion.mPromotionMyBattle.loseCount, playerAInfo, playerBInfo)
    else
      flashObj:InvokeASCallback("_root", "SetPromotionMyBattleInfo", lvRange, remainTime, GameUIWorldChampion.mPromotionMyBattle.winCount, GameUIWorldChampion.mPromotionMyBattle.loseCount, nil, nil)
    end
    GameUIWorldChampion:SetPromotionRecordVisible(true)
    flashObj:InvokeASCallback("_root", "ShowPromotionMyBattle")
  end
end
function GameUIWorldChampion:ShowPromotionMySupport()
  local flashObj = self:GetFlashObject()
  if flashObj and GameUIWorldChampion.mMySupportList then
    DebugOut("SetMySupportList " .. #GameUIWorldChampion.mMySupportList)
    flashObj:InvokeASCallback("_root", "SetMySupportList", #GameUIWorldChampion.mMySupportList)
    flashObj:InvokeASCallback("_root", "ShowPromotionMySupport")
  end
end
function GameUIWorldChampion:HidePromotionMySupport()
  local flashObj = self:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "HidePromotionMySupport")
  end
end
function GameUIWorldChampion.RefreshMySupportList()
  if flashObj and GameUIWorldChampion.mMySupportList then
    flashObj:InvokeASCallback("_root", "SetMySupportList", #GameUIWorldChampion.mMySupportList)
  end
end
function GameUIWorldChampion:UpdateMySupportListItem(itemId)
  if GameUIWorldChampion.mMySupportList then
    local item = GameUIWorldChampion.mMySupportList[itemId]
    local supportIdx = GameUIWorldChampion:GetSupportPlayerIdx(item.round, item.index)
    if supportIdx > 0 then
      local playerA, playerB
      if supportIdx % 2 == 0 then
        playerA = GameUIWorldChampion.mBTreeData[supportIdx].playerInfo
        playerB = GameUIWorldChampion.mBTreeData[supportIdx + 1].playerInfo
      else
        playerA = GameUIWorldChampion.mBTreeData[supportIdx - 1].playerInfo
        playerB = GameUIWorldChampion.mBTreeData[supportIdx].playerInfo
      end
      local aIsWin = false
      local rewardStatus = 0
      local parent = GameUIWorldChampion.mBTreeData[GameUIWorldChampion:BT_GetParentIdx(supportIdx)].playerInfo
      if not parent then
        rewardStatus = 0
      elseif parent then
        if GameUIWorldChampion.mBTreeData[supportIdx].playerInfo.player_id == parent.player_id then
          if item.award_status == 0 then
            rewardStatus = 1
          else
            rewardStatus = 3
          end
          if supportIdx % 2 == 0 then
            aIsWin = true
          end
        else
          if supportIdx % 2 == 1 then
            aIsWin = true
          end
          rewardStatus = 2
        end
      end
      DebugOut("UpdateMySupportListItem ")
      DebugTable(parent)
      DebugTable(playerA)
      DebugTable(playerB)
      local num = ""
      local iconFrame = ""
      if 1 <= #item.award then
        if item.award[1].item_type == "krypton" or item.award[1].item_type == "item" then
          num = tostring(item.award[1].no)
        else
          num = tostring(item.award[1].number)
        end
        iconFrame = GameHelper:GetCommonIconFrame(item.award[1], GameUIWorldChampion.RefreshMySupportList)
      end
      local aSex = GameUtils:GetPlayerSexByAvatra(playerA.avatar)
      local aAvatar = ""
      if playerA.avatar == "male" or playerA.avatar == "female" then
        aAvatar = GameDataAccessHelper:GetFleetAvatar(1, playerA.main_fleet_level, aSex)
      else
        aAvatar = GameDataAccessHelper:GetFleetAvatar(tonumber(playerA.avatar), playerA.main_fleet_level, aSex)
      end
      local playerAInfo = {
        playerId = playerA.player_id,
        playerName = playerA.name,
        playerLv = GameLoader:GetGameText("LC_MENU_Level") .. playerA.player_level,
        avatar = aAvatar,
        isWin = aIsWin,
        price = playerA.get_support,
        isSupport = supportIdx % 2 == 0
      }
      local bSex = GameUtils:GetPlayerSexByAvatra(playerB.avatar)
      local bAvatar = ""
      if playerB.avatar == "male" or playerB.avatar == "female" then
        bAvatar = GameDataAccessHelper:GetFleetAvatar(1, playerB.main_fleet_level, bSex)
      else
        bAvatar = GameDataAccessHelper:GetFleetAvatar(tonumber(playerB.avatar), playerB.main_fleet_level, bSex)
      end
      local playerBInfo = {
        playerId = playerB.player_id,
        playerName = playerB.name,
        playerLv = GameLoader:GetGameText("LC_MENU_Level") .. playerB.player_level,
        avatar = bAvatar,
        isWin = not aIsWin,
        price = playerB.get_support,
        isSupport = supportIdx % 2 ~= 0
      }
      local receiveText = GameLoader:GetGameText("LC_MENU_SA_RECEIVE_BUTTON")
      local supportFailedText = GameLoader:GetGameText("LC_MENU_SA_SUPPORT_FAILED_INFO")
      local receivedText = GameLoader:GetGameText("LC_MENU_SA_RECEIVED_INFO")
      local supportText = GameLoader:GetGameText("LC_MENU_SA_SUPPORT_BUTTON")
      local flashObj = self:GetFlashObject()
      if flashObj then
        flashObj:InvokeASCallback("_root", "UpdateMySupportListItem", itemId, playerAInfo, playerBInfo, string.format(GameLoader:GetGameText("LC_MENU_SA_BEST_TITLE"), item.round), rewardStatus, num, iconFrame, receiveText, supportFailedText, receivedText, supportText)
      end
    end
  end
end
function GameUIWorldChampion:SetPromotionSupportResultInfo(aIdx, bIdx)
  local flashObj = self:GetFlashObject()
  if flashObj then
    local playerA = GameUIWorldChampion.mBTreeData[aIdx].playerInfo
    local playerB = GameUIWorldChampion.mBTreeData[bIdx].playerInfo
    local aSex = GameUtils:GetPlayerSexByAvatra(playerA.avatar)
    local aAvatar = ""
    local priceTitle = " <font color='#5098C5'> " .. GameLoader:GetGameText("LC_MENU_SA_PRICE_DESC") .. "</font>"
    if playerA.avatar == "male" or playerA.avatar == "female" then
      aAvatar = GameDataAccessHelper:GetFleetAvatar(1, playerA.main_fleet_level, aSex)
    else
      aAvatar = GameDataAccessHelper:GetFleetAvatar(tonumber(playerA.avatar), playerA.main_fleet_level, aSex)
    end
    local playerAInfo = {
      playerId = playerA.player_id,
      playerName = playerA.name,
      playerLv = GameLoader:GetGameText("LC_MENU_Level") .. playerA.player_level,
      avatar = aAvatar,
      isWin = false,
      price = priceTitle .. " <font color='#FFDE28'> " .. tostring(playerA.get_support) .. "</font>",
      isSupport = false
    }
    local bSex = GameUtils:GetPlayerSexByAvatra(playerB.avatar)
    local bAvatar = ""
    if playerB.avatar == "male" or playerB.avatar == "female" then
      bAvatar = GameDataAccessHelper:GetFleetAvatar(1, playerB.main_fleet_level, bSex)
    else
      bAvatar = GameDataAccessHelper:GetFleetAvatar(tonumber(playerB.avatar), playerB.main_fleet_level, bSex)
    end
    local playerBInfo = {
      playerId = playerB.player_id,
      playerName = playerB.name,
      playerLv = GameLoader:GetGameText("LC_MENU_Level") .. playerB.player_level,
      avatar = bAvatar,
      isWin = false,
      price = priceTitle .. " <font color='#FFDE28'> " .. tostring(playerB.get_support) .. "</font>",
      isSupport = false
    }
    flashObj:InvokeASCallback("_root", "SetPromotionSupportResultInfo", playerAInfo, playerBInfo)
  end
end
function GameUIWorldChampion:ShowPromotionSupportResult()
  local flashObj = self:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "SetSupportResultList", #GameUIWorldChampion.mFightRecord.mFightRecordList)
    flashObj:InvokeASCallback("_root", "ShowPromotionSupportResult")
  end
end
function GameUIWorldChampion:UpdateSupportResultListItem(itemId)
  local item = GameUIWorldChampion.mFightRecord.mFightRecordList[itemId]
  local flashObj = self:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "UpdateSupportResultListItem", itemId, string.format(GameLoader:GetGameText("LC_MENU_SA_EVENT_LABBLE"), itemId), item.win_player.name)
  end
end
function GameUIWorldChampion:ShowPromotionSupport()
  local flashObj = self:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "ShowPromotionSupport")
  end
end
function GameUIWorldChampion:SetPromotionSupportInfo(aIdx, bIdx)
  DebugOut("SetPromotionSupportInfo " .. aIdx .. " " .. bIdx)
  local flashObj = self:GetFlashObject()
  if flashObj then
    local playerA = GameUIWorldChampion.mBTreeData[aIdx].playerInfo
    local playerB = GameUIWorldChampion.mBTreeData[bIdx].playerInfo
    local supportInfoA = GameUIWorldChampion:GetSupportInfo(playerA.battle_round, playerA.index)
    local supportInfoB = GameUIWorldChampion:GetSupportInfo(playerB.battle_round, playerB.index)
    local isSupportBtnVisible = not GameUIWorldChampion:IsAlreadySupportInRound(playerA.battle_round)
    local facebookEnabled = false
    if isSupportBtnVisible and FacebookGraphStorySharer:IsStoryEnabled(FacebookGraphStorySharer.StoryType.STORY_TYPE_WORLDCHAMPION_SUPPORT) then
      facebookEnabled = true
    end
    local aSex = GameUtils:GetPlayerSexByAvatra(playerA.avatar)
    local aAvatar = FleetDataAccessHelper:GetFleetAvatarFrameByAvatar(playerA.avatar, 1)
    local priceTitle = " <font color='#5098C5'> " .. GameLoader:GetGameText("LC_MENU_SA_PRICE_DESC") .. "</font>"
    if playerA.avatar == "male" or playerA.avatar == "female" then
      aAvatar = GameDataAccessHelper:GetFleetAvatar(1, playerA.main_fleet_level, aSex)
    else
      aAvatar = GameDataAccessHelper:GetFleetAvatar(tonumber(playerA.avatar), playerA.main_fleet_level, aSex)
    end
    local aIsSupported = false
    if supportInfoA then
      aIsSupported = true
    end
    local playerAInfo = {
      playerId = aIdx,
      playerName = playerA.name,
      playerLv = GameLoader:GetGameText("LC_MENU_Level") .. playerA.player_level,
      avatar = aAvatar,
      isWin = false,
      price = priceTitle .. " <font color='#FFDE28'> " .. tostring(playerA.get_support) .. "</font>",
      isSupport = aIsSupported,
      supportBtnVisible = isSupportBtnVisible
    }
    local bSex = GameUtils:GetPlayerSexByAvatra(playerB.avatar)
    local bAvatar = ""
    if playerB.avatar == "male" or playerB.avatar == "female" then
      bAvatar = GameDataAccessHelper:GetFleetAvatar(1, playerB.main_fleet_level, bSex)
    else
      bAvatar = GameDataAccessHelper:GetFleetAvatar(tonumber(playerB.avatar), playerB.main_fleet_level, bSex)
    end
    local bIsSupported = false
    if supportInfoB then
      bIsSupported = true
    end
    local playerBInfo = {
      playerId = bIdx,
      playerName = playerB.name,
      playerLv = GameLoader:GetGameText("LC_MENU_Level") .. playerB.player_level,
      avatar = bAvatar,
      isWin = false,
      price = priceTitle .. " <font color='#FFDE28'> " .. tostring(playerB.get_support) .. "</font>",
      isSupport = bIsSupported,
      supportBtnVisible = isSupportBtnVisible
    }
    DebugOut("playerBInfo = ", bSex, bAvatar)
    DebugTable(playerBInfo)
    flashObj:InvokeASCallback("_root", "SetPromotionSupportInfo", playerAInfo, playerBInfo, facebookEnabled, self.mSupportShareStoryChecked)
  end
end
function GameUIWorldChampion:CheckSupport(playerIdx)
  if playerIdx > #GameUIWorldChampion.mBTreeData or playerIdx <= 0 then
    return
  end
  local aIdx, bIdx = GameUIWorldChampion:BT_GetChildrenIdxs(playerIdx)
  DebugOut("tpcheck")
  DebugTable(GameUIWorldChampion.mBTreeData)
  if aIdx <= #GameUIWorldChampion.mBTreeData and bIdx <= #GameUIWorldChampion.mBTreeData then
    if GameUIWorldChampion.mBTreeData[playerIdx].playerInfo then
      GameUIWorldChampion:RequestPromotionFightRecordList(aIdx)
      GameUIWorldChampion:SetPromotionSupportResultInfo(aIdx, bIdx)
    elseif GameUIWorldChampion.mBTreeData[aIdx].playerInfo and GameUIWorldChampion.mBTreeData[bIdx].playerInfo then
      GameUIWorldChampion:SetPromotionSupportInfo(aIdx, bIdx)
      GameUIWorldChampion:SetPromotionSupportResultInfo(aIdx, bIdx)
      GameUIWorldChampion:RequestPromotionFightRecordList(aIdx)
    end
  end
end
function GameUIWorldChampion:SetPromotionLvRangeAndRound()
  local flashObj = GameUIWorldChampion:GetFlashObject()
  if flashObj then
    local itemId = GameUIWorldChampion.mCurLvRangeId
    local lvRange = GameUIWorldChampion:GetLvRangeStr(itemId)
    DebugOut("lua SetPromotionLvRange " .. lvRange)
    local round = GameUIWorldChampion:GetCurRound()
    flashObj:InvokeASCallback("_root", "SetPromotionLvRange", lvRange)
    flashObj:InvokeASCallback("_root", "SetPromotionRound", string.format(GameLoader:GetGameText("LC_MENU_SA_ROUND_LABBLE"), round))
  else
    DebugOut("flashObj is nil.")
  end
end
function GameUIWorldChampion:SetPromotionRecordVisible(isVisible)
  local flashObj = self:GetFlashObject()
  if flashObj and isVisible then
    if GameUIWorldChampion.mPlayerStatus == WC_PLAYER_STATUS.NO_JOIN then
      flashObj:InvokeASCallback("_root", "SetPromotionScoreVisible", false)
      flashObj:InvokeASCallback("_root", "SetPromotionNoJoinTextVisible", true)
      flashObj:InvokeASCallback("_root", "SetPromotionNextFightTimeVisible", true)
      flashObj:InvokeASCallback("_root", "SetPromotionDeadTextVisible", false)
      flashObj:InvokeASCallback("_root", "SetPromotionSubmitBtnEnable", false)
    elseif GameUIWorldChampion.mPlayerStatus == WC_PLAYER_STATUS.DEAD then
      flashObj:InvokeASCallback("_root", "SetPromotionScoreVisible", false)
      flashObj:InvokeASCallback("_root", "SetPromotionNoJoinTextVisible", false)
      flashObj:InvokeASCallback("_root", "SetPromotionNextFightTimeVisible", true)
      flashObj:InvokeASCallback("_root", "SetPromotionDeadTextVisible", true)
      flashObj:InvokeASCallback("_root", "SetPromotionSubmitBtnEnable", false)
    else
      flashObj:InvokeASCallback("_root", "SetPromotionScoreVisible", true)
      flashObj:InvokeASCallback("_root", "SetPromotionNoJoinTextVisible", false)
      flashObj:InvokeASCallback("_root", "SetPromotionNextFightTimeVisible", true)
      flashObj:InvokeASCallback("_root", "SetPromotionDeadTextVisible", false)
      flashObj:InvokeASCallback("_root", "SetPromotionSubmitBtnEnable", GameUIWorldChampion.mCanSubmitMatrix)
    end
  end
end
function GameUIWorldChampion:RegisterAllMsgHandler()
  NetMessageMgr:RegisterMsgHandler(NetAPIList.budo_stage_ntf.Code, GameUIWorldChampion.StageNtf)
  NetMessageMgr:RegisterMsgHandler(NetAPIList.budo_sign_up_ntf.Code, GameUIWorldChampion.SignUpNtf)
  NetMessageMgr:RegisterMsgHandler(NetAPIList.budo_mass_election_ntf.Code, GameUIWorldChampion.MassElectionNtf)
  NetMessageMgr:RegisterMsgHandler(NetAPIList.budo_promotion_ntf.Code, GameUIWorldChampion.PromotionNtf)
end
function GameUIWorldChampion:RemoveMessageHandler()
  NetMessageMgr:RemoveMsgHandler(NetAPIList.budo_stage_ntf.Code)
  NetMessageMgr:RemoveMsgHandler(NetAPIList.budo_sign_up_ntf.Code)
  NetMessageMgr:RemoveMsgHandler(NetAPIList.budo_mass_election_ntf.Code)
  NetMessageMgr:RemoveMsgHandler(NetAPIList.budo_promotion_ntf.Code)
end
function GameUIWorldChampion:RequestEnterWC()
  GameUIWorldChampion.mCurState = 0
  GameUIWorldChampion.mEnterStatus = WC_ENTER_STATUS.REQUEST_ENTER
  local req = {activity_id = 3}
  DebugOut("GameUIWorldChampion:RequestEnterWC")
  NetMessageMgr:SendMsg(NetAPIList.budo_req.Code, nil, self.RequestEnterWCCallback, false, nil)
end
function GameUIWorldChampion.RequestEnterWCCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.budo_req.Code then
    DebugOut("RequestEnterWCCallback err.")
    DebugTable(content)
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      GameUIWorldChampion.mEnterStatus = WC_ENTER_STATUS.EXIT
    else
      GameUIWorldChampion.mEnterStatus = WC_ENTER_STATUS.ENTERED
    end
    return true
  end
  return false
end
function GameUIWorldChampion:BuildFlashSelect()
  if not isMatrixAck or not isStageAck then
    return
  end
  local Name = "initMartix"
  if GameUIWorldChampion.mCurState == 3 then
    Name = "initMartix2"
  end
  local flash_obj = GameUIWorldChampion:GetFlashObject()
  if flash_obj then
    GameUIWorldChampion.mCurMatrixIndex = FleetMatrix:getCurrentMartixIndexGot(FleetMatrix.MATRIX_TYPE_BUDO)
    local index = GameUIWorldChampion.mCurMatrixIndex
    if index == 0 then
      flash_obj:InvokeASCallback("_root", "setMatrixDefault", GameLoader:GetGameText("LC_MENU_SAVE_FORMATION_CHANGE_FORMATION"))
    end
    flash_obj:InvokeASCallback("_root", Name, FleetMatrix.matrixs_in_as, index)
    GameUIWorldChampion:CheckAndGotoSubState()
  end
end
function GameUIWorldChampion:CheckAndGotoSubState()
  if GameUIWorldChampion.mCurState == -1 then
    GameStateWorldChampion:GotoSubState(STATE_WC_SUB_STATE.SUB_STATE_BEFORE)
  elseif GameUIWorldChampion.mCurState == 1 then
    GameStateWorldChampion:GotoSubState(STATE_WC_SUB_STATE.SUB_STATE_ENROLL)
  elseif GameUIWorldChampion.mCurState == 2 then
    GameStateWorldChampion:GotoSubState(STATE_WC_SUB_STATE.SUB_STATE_MASS_ELECTION)
  elseif GameUIWorldChampion.mCurState == 3 then
    GameStateWorldChampion:GotoSubState(STATE_WC_SUB_STATE.SUB_STATE_PROMOTION)
  else
    DebugOut("Invalid state " .. GameUIWorldChampion.mCurState)
  end
end
function GameUIWorldChampion.StageNtf(content)
  DebugOut("GameUIWorldChampion.StageNtf .")
  DebugTable(content)
  if GameUIWorldChampion.mEnterStatus == WC_ENTER_STATUS.REQUEST_ENTER or GameUIWorldChampion.mEnterStatus == WC_ENTER_STATUS.ENTERED then
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateWorldChampion)
    GameUIWorldChampion.mCurState = content.cur_stage
    GameUIWorldChampion.mLvRangeList = content.budo_level_range
    isStageAck = true
    GameUIWorldChampion:BuildFlashSelect()
  end
end
function GameUIWorldChampion.SignUpNtf(content)
  DebugOut("GameUIWorldChampion.SignUpNtf .")
  DebugTable(content)
  if GameUIWorldChampion.mEnterStatus == WC_ENTER_STATUS.REQUEST_ENTER or GameUIWorldChampion.mEnterStatus == WC_ENTER_STATUS.ENTERED then
    GameUIWorldChampion.mJoinInfo.countDownTime = content.left_time
    GameUIWorldChampion.mJoinInfo.joined = content.does_join
    GameUIWorldChampion.mJoinInfo.startLv = content.budo_level_min
    GameUIWorldChampion.mJoinInfo.endLv = content.budo_level_max
  end
  GameUIWorldChampion:SetEnrollUIInfo()
end
function GameUIWorldChampion.MassElectionNtf(content)
  DebugOut("GameUIWorldChampion.MassElectionNtf .")
  DebugTable(content)
  if GameUIWorldChampion.mEnterStatus == WC_ENTER_STATUS.REQUEST_ENTER or GameUIWorldChampion.mEnterStatus == WC_ENTER_STATUS.ENTERED then
    GameUIWorldChampion.mDataFetchTime = os.time()
    GameUIWorldChampion.mPlayerStatus = content.player_status
    GameUIWorldChampion.mCurRoundItemId = content.cur_kid_stage
    GameUIWorldChampion.mCurSelectedRoundListItemId = content.cur_kid_stage
    GameUIWorldChampion.mMassElectionInfo = content
    GameUIWorldChampion.mRewardList = content.award_list
    local sort = function(a, b)
      if a.round < b.round then
        return true
      else
        return false
      end
    end
    table.sort(GameUIWorldChampion.mRewardList, sort)
    GameUIWorldChampion:SetMassElectionLvRange()
    GameUIWorldChampion:InitRoundList()
  end
end
function GameUIWorldChampion.PromotionNtf(content)
  DebugOut("GameUIWorldChampion.PromotionNtf . GameUIWorldChampion.mEnterStatus " .. GameUIWorldChampion.mEnterStatus)
  DebugTable(content)
  if GameUIWorldChampion.mEnterStatus == WC_ENTER_STATUS.REQUEST_ENTER or GameUIWorldChampion.mEnterStatus == WC_ENTER_STATUS.ENTERED then
    if nil == GameUIWorldChampion.mPromotionMyBattle then
      GameUIWorldChampion.mDataFetchTime = os.time()
      GameUIWorldChampion.mPromotionMyBattle = {
        winCount = content.win,
        loseCount = content.lose,
        nextFightTime = content.left_battle_time,
        massHistoryCount = content.audition_round
      }
    end
    GameUIWorldChampion.mCanSubmitMatrix = 1 == content.can_submit
    GameUIWorldChampion.mPlayerStatus = content.player_status
    GameUIWorldChampion.mCurLvRangeId = content.step
    if -1 == GameUIWorldChampion.mMyLvRangeId then
      GameUIWorldChampion.mMyLvRangeId = content.step
    end
    GameUIWorldChampion.mRewardList = content.my_award_array
    local sort = function(a, b)
      if a.round < b.round then
        return true
      else
        return false
      end
    end
    table.sort(GameUIWorldChampion.mRewardList, sort)
    GameUIWorldChampion:InitPromotionData(content)
    GameUIWorldChampion:SetBTreeData()
    GameUIWorldChampion:SetPromotionLvRangeAndRound()
    GameUIWorldChampion:RequestMySupport()
    if GameUIWorldChampion.mPromotionMyBattle and GameUIWorldChampion.mPromotionMyBattle.myInfo and nil == GameUIWorldChampion.mBTreeData[1].playerInfo then
      GameUIWorldChampion:ShowPromotionMyBattle()
    end
  end
end
function GameUIWorldChampion:RequestJoin()
  NetMessageMgr:SendMsg(NetAPIList.budo_join_req.Code, nil, self.RequestJoinCallback, true, nil)
end
function GameUIWorldChampion.RequestJoinCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.budo_join_req.Code then
    DebugOut("RequestJoinCallback err.")
    DebugTable(content)
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.budo_join_ack.Code then
    DebugOut("RequestJoinCallback ok.")
    DebugTable(content)
    local countryStr = ext.GetDeviceCountry and ext.GetDeviceCountry() or "unknow"
    if GameUIWorldChampion.mShareStoryChecked then
      FacebookGraphStorySharer:ShareStory(FacebookGraphStorySharer.StoryType.STORY_TYPE_WORLDCHAMPION_JOIN)
    else
    end
    GameUIWorldChampion.mJoinInfo.startLv = content.budo_level_min
    GameUIWorldChampion.mJoinInfo.endLv = content.budo_level_max
    GameUIWorldChampion.mJoinInfo.joined = true
    GameUIWorldChampion:SetEnrollUIInfo()
    return true
  end
  return false
end
function GameUIWorldChampion:RequestSubmitMatrix()
  NetMessageMgr:SendMsg(NetAPIList.budo_change_formation_req.Code, {
    id = GameUIWorldChampion.mCurMatrixIndexSelect
  }, self.RequestSubmitMatrixCallback, true, nil)
end
function GameUIWorldChampion.RequestSubmitMatrixCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.budo_change_formation_req.Code then
    DebugOut("RequestSubmitMatrixCallback err.")
    DebugTable(content)
    if 1 == content.code then
      local flashObj = GameUIWorldChampion:GetFlashObject()
      if flashObj then
        if GameStateWorldChampion.mMassElectionInfo then
          GameUIWorldChampion.mMassElectionInfo.can_submit = 1
          flashObj:InvokeASCallback("_root", "SetMyRoundSubmitBtnEnable", true)
        end
        GameUIWorldChampion.mCanSubmitMatrix = true
        flashObj:InvokeASCallback("_root", "SetPromotionSubmitBtnEnable", GameUIWorldChampion.mCanSubmitMatrix)
      end
    elseif content.code == 0 then
      local flashObj = GameUIWorldChampion:GetFlashObject()
      if flashObj then
        if GameUIWorldChampion.mMassElectionInfo then
          GameUIWorldChampion.mMassElectionInfo.can_submit = 0
          flashObj:InvokeASCallback("_root", "SetMyRoundSubmitBtnEnable", false)
        end
        GameUIWorldChampion.mCanSubmitMatrix = true
        flashObj:InvokeASCallback("_root", "SetPromotionSubmitBtnEnable", GameUIWorldChampion.mCanSubmitMatrix)
      end
    else
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.budo_change_formation_ack.Code then
    DebugOut("RequestSubmitMatrixCallback ok.")
    DebugTable(content)
    local flashObj = GameUIWorldChampion:GetFlashObject()
    if flashObj then
      if GameUIWorldChampion.mMassElectionInfo then
        GameUIWorldChampion.mMassElectionInfo.can_submit = 0
      end
      GameUIWorldChampion.mCanSubmitMatrix = false
      flashObj:InvokeASCallback("_root", "SetMyRoundSubmitBtnEnable", false)
      flashObj:InvokeASCallback("_root", "SetPromotionSubmitBtnEnable", false)
    end
    GameUIWorldChampion.mCurMatrixIndex = GameUIWorldChampion.mCurMatrixIndexSelect
    return true
  end
  return false
end
function GameUIWorldChampion:RequestFightRecordList(roundId)
  GameUIWorldChampion.mFightRecord.mRoundId = roundId
  GameUIWorldChampion.mFightRecord.mFightRecordList = nil
  local req = {
    player_id = GameGlobalData:GetData("userinfo").player_id,
    server_id = GameUtils:GetActiveServerInfo().logic_id,
    stage = GameUIWorldChampion.mCurState,
    round = roundId
  }
  DebugOut("GameUIWorldChampion:RequestFightRecordList " .. roundId)
  DebugTable(req)
  NetMessageMgr:SendMsg(NetAPIList.battle_report_req.Code, req, self.RequestFightRecordListCallback, true, nil)
end
function GameUIWorldChampion.RequestFightRecordListCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.battle_report_req.Code then
    DebugOut("RequestFightRecordListCallback err.")
    DebugTable(content)
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.battle_report_ack.Code then
    DebugOut("RequestFightRecordListCallback ok.")
    DebugTable(content)
    GameUIWorldChampion.mFightRecord.mFightRecordList = content.battle_report
    GameUIWorldChampion:SetMyRoundRecordVisible(false)
    GameUIWorldChampion:SetRoundFightList()
    GameUIWorldChampion:ShowRoundFightList()
    return true
  end
  return false
end
function GameUIWorldChampion:BT_GetParentIdx(idx)
  if idx > 1 then
    return (...), idx / 2
  else
    return -1
  end
end
function GameUIWorldChampion:BT_GetChildrenIdxs(idx)
  return 2 * idx, 2 * idx + 1
end
function GameUIWorldChampion:GetBTreeNodeLevel(idx)
  local h = math.floor(math.log(idx) / math.log(2)) + 1
  DebugOut("GetBTreeNodeLevel idx " .. idx .. " h " .. h)
  return h
end
function GameUIWorldChampion:InitPromotionData(content)
  local data = GameUIWorldChampion:ConvertData(content.vs_player)
  DebugOut("InitPromotionData")
  DebugTable(data)
  GameUIWorldChampion.mBTreeData = data
  if GameUIWorldChampion.mBTreeData[1].playerInfo then
    GameUIWorldChampion.mIsWCEnd = true
  else
    GameUIWorldChampion.mIsWCEnd = false
  end
  local myInfo, rivalInfo = GameUIWorldChampion:GetMyAndRivalInfo()
  if myInfo and GameUIWorldChampion.mPromotionMyBattle then
    GameUIWorldChampion.mPromotionMyBattle.myInfo = myInfo
    GameUIWorldChampion.mPromotionMyBattle.rivalInfo = rivalInfo
  end
end
function GameUIWorldChampion:GetMyAndRivalInfo()
  local myInfo, rivalInfo
  DebugOut("GetMyAndRivalInfo")
  if GameUIWorldChampion.mBTreeData then
    local myPlayerId = tonumber(GameGlobalData:GetData("userinfo").player_id)
    local myServerId = tonumber(GameUtils:GetActiveServerInfo().logic_id)
    DebugTable(GameUIWorldChampion.mBTreeData)
    for k = 1, #GameUIWorldChampion.mBTreeData do
      if GameUIWorldChampion.mBTreeData[k].playerInfo and GameUIWorldChampion.mBTreeData[k].playerInfo.player_id == myPlayerId and GameUIWorldChampion.mBTreeData[k].playerInfo.server_id == myServerId then
        DebugOut("K " .. k)
        if nil == myInfo or myInfo.battle_round > GameUIWorldChampion.mBTreeData[k].playerInfo.battle_round then
          myInfo = GameUIWorldChampion.mBTreeData[k].playerInfo
          DebugOut("get my info ")
          if 1 == k then
            rivalInfo = nil
          elseif k % 2 == 0 then
            rivalInfo = GameUIWorldChampion.mBTreeData[k + 1].playerInfo
          else
            rivalInfo = GameUIWorldChampion.mBTreeData[k - 1].playerInfo
          end
        end
      end
    end
    DebugOut("GetMyAndRivalInfo my player id " .. myPlayerId .. " my server id " .. myServerId)
  end
  DebugTable(myInfo)
  DebugTable(rivalInfo)
  return myInfo, rivalInfo
end
function GameUIWorldChampion:ConvertData(mapArray)
  local ret = {}
  local idx = 1
  for roundId = 1, 6 do
    local roundDataCount = math.pow(2, roundId - 1)
    for k = 1, roundDataCount do
      local tmp = GameUIWorldChampion:GetDataByIndex(mapArray, roundDataCount, k)
      local item = {idx = idx, playerInfo = tmp}
      table.insert(ret, item)
      idx = idx + 1
    end
  end
  return ret
end
function GameUIWorldChampion:GetDataByIndex(mapArray, battle_round, index)
  DebugOut("GetDataByIndex " .. battle_round .. " " .. index)
  for k = 1, #mapArray do
    if mapArray[k].battle_round == battle_round and mapArray[k].index == index then
      return mapArray[k]
    end
  end
  return nil
end
function GameUIWorldChampion:RequestFightHistory(reportId, isChampionHistory)
  DebugOut("RequestFightHistory: " .. reportId)
  if GameStateManager:GetCurrentGameState().fightRoundData then
    GameStateManager:GetCurrentGameState().fightRoundData = nil
  end
  if isChampionHistory then
    local idx = GameUIWorldChampion.mHistoryChampionData.effect_list[GameUIWorldChampion.mCurHistoryChampionPeriodIdx]
    local req = {index = idx, battle_id = reportId}
    NetMessageMgr:SendMsg(NetAPIList.budo_champion_report_req.Code, req, self.RequestFightHistoryCallback, true, nil)
  else
    local req = {battle_id = reportId}
    NetMessageMgr:SendMsg(NetAPIList.get_budo_replay_req.Code, req, self.RequestFightHistoryCallback, true, nil)
  end
end
function GameUIWorldChampion.RequestFightHistoryCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and (content.api == NetAPIList.get_budo_replay_req.Code or content.api == NetAPIList.budo_champion_report_req.Code) then
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.get_budo_replay_ack.Code or msgType == NetAPIList.budo_champion_report_ack.Code then
    DebugOut("GameUIWorldChampion.RequestFightHistoryCallback")
    DebugTable(content)
    local fight_report = content.battle_record
    if #fight_report.rounds == 0 and GameStateManager:GetCurrentGameState().fightRoundData then
      fight_report.rounds = GameStateManager:GetCurrentGameState().fightRoundData
    end
    GameStateBattlePlay.curBattleType = "UIWorldChampion"
    GameStateBattlePlay:InitBattle(GameStateBattlePlay.MODE_HISTORY, fight_report)
    GameStateBattlePlay:RegisterOverCallback(function()
      if GameUIWorldChampion.mExitForFight then
        GameStateManager:SetCurrentGameState(GameStateWorldChampion)
      else
        GameUIWorldChampion:RequestEnterWC()
      end
    end)
    GameUIWorldChampion.mEnterStatus = WC_ENTER_STATUS.EXIT
    if GameStateWorldChampion.mCurSubState == STATE_WC_SUB_STATE.SUB_STATE_PROMOTION then
      GameUIWorldChampion.mExitForFight = true
    end
    GameStateManager:SetCurrentGameState(GameStateBattlePlay)
    return true
  end
  return false
end
function GameUIWorldChampion:RequestSupportOnePlayer(idx)
  GameUIWorldChampion.mReqSupportPlayerIdx = idx
  local player = GameUIWorldChampion.mBTreeData[idx].playerInfo
  local levelInTree = GameUIWorldChampion:GetBTreeNodeLevel(idx)
  local step = 6 - levelInTree + 1
  local req = {
    player_id = player.player_id,
    server_id = player.server_id,
    round = player.battle_round,
    step = GameUIWorldChampion.mCurLvRangeId,
    index = player.index
  }
  DebugOut("RequestSupportOnePlayer")
  DebugTable(req)
  NetMessageMgr:SendMsg(NetAPIList.support_one_player_req.Code, req, self.RequestSupportOnePlayerCallback, true, nil)
end
function GameUIWorldChampion.RequestSupportOnePlayerCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.support_one_player_req.Code then
    DebugOut("GameUIWorldChampion.RequestSupportOnePlayerCallback")
    DebugTable(content)
    GameUIWorldChampion.mReqSupportPlayerIdx = -1
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.support_one_player_ack.Code then
    DebugOut("GameUIWorldChampion.RequestSupportOnePlayerCallback")
    DebugTable(content)
    local supportPlayer = GameUIWorldChampion.mBTreeData[GameUIWorldChampion.mReqSupportPlayerIdx].playerInfo
    supportPlayer.get_support = content.worth
    if GameUIWorldChampion.mReqSupportPlayerIdx ~= -1 then
      local supportInfo = {
        step = GameUIWorldChampion.mCurLvRangeId,
        index = supportPlayer.index,
        player_id = supportPlayer.player_id,
        server_id = supportPlayer.server_id,
        round = supportPlayer.battle_round
      }
      table.insert(GameUIWorldChampion.mMySupportList, supportInfo)
      table.insert(GameUIWorldChampion.mMySupportListAll, supportInfo)
      DebugTable(GameUIWorldChampion.mMySupportList)
      DebugTable(GameUIWorldChampion.mMySupportListAll)
      local aIdx = -1
      local bIdx = -1
      if GameUIWorldChampion.mReqSupportPlayerIdx % 2 == 0 then
        aIdx = GameUIWorldChampion.mReqSupportPlayerIdx
        bIdx = GameUIWorldChampion.mReqSupportPlayerIdx + 1
      else
        aIdx = GameUIWorldChampion.mReqSupportPlayerIdx - 1
        bIdx = GameUIWorldChampion.mReqSupportPlayerIdx
      end
      GameUIWorldChampion:SetPromotionSupportInfo(aIdx, bIdx)
      local flashObj = GameUIWorldChampion:GetFlashObject()
      if flashObj then
        flashObj:InvokeASCallback("_root", "SetPromotionSupportSuccessInfo", true)
      end
    end
    GameUIWorldChampion.mReqSupportPlayerIdx = -1
    GameUIWorldChampion:RequestMySupport()
    local countryStr = ext.GetDeviceCountry and ext.GetDeviceCountry() or "unknow"
    if GameUIWorldChampion.mSupportShareStoryChecked then
      local extraInfo = {}
      DebugOut("supportPlayer.name = ", supportPlayer.name)
      extraInfo.playerName = supportPlayer.name or ""
      FacebookGraphStorySharer:ShareStory(FacebookGraphStorySharer.StoryType.STORY_TYPE_WORLDCHAMPION_SUPPORT, extraInfo)
    else
    end
    return true
  end
  return false
end
function GameUIWorldChampion:RequestMySupport()
  NetMessageMgr:SendMsg(NetAPIList.get_my_support_req.Code, nil, self.RequestMySupportCallback, true, nil)
end
function GameUIWorldChampion.RequestMySupportCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.get_my_support_req.Code then
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.get_my_support_ack.Code then
    DebugOut("GameUIWorldChampion.RequestMySupportCallback")
    DebugTable(content)
    GameUIWorldChampion.mMySupportListAll = content.support_info
    GameUIWorldChampion.mMySupportList = {}
    if #content.support_info > 0 then
      for k = 1, #content.support_info do
        local item = content.support_info[k]
        if item.step == GameUIWorldChampion.mCurLvRangeId then
          table.insert(GameUIWorldChampion.mMySupportList, item)
        end
      end
    end
    return true
  end
  return false
end
function GameUIWorldChampion:RequestPromotionFightRecordList(onePlayerIdx)
  GameUIWorldChampion.mFightRecord.mFightRecordList = nil
  local playerInfo = GameUIWorldChampion.mBTreeData[onePlayerIdx].playerInfo
  local levelInTree = GameUIWorldChampion:GetBTreeNodeLevel(onePlayerIdx)
  local round = 6 - levelInTree + 1
  local req = {
    player_id = playerInfo.player_id,
    server_id = playerInfo.server_id,
    stage = GameUIWorldChampion.mCurState,
    round = round
  }
  DebugOut("GameUIWorldChampion:RequestPromotionFightRecordList ")
  DebugTable(req)
  NetMessageMgr:SendMsg(NetAPIList.battle_report_req.Code, req, self.RequestPromotionFightRecordListCallback, true, nil)
end
function GameUIWorldChampion.RequestPromotionFightRecordListCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.battle_report_req.Code then
    DebugOut("RequestPromotionFightRecordListCallback err.")
    DebugTable(content)
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.battle_report_ack.Code then
    DebugOut("RequestPromotionFightRecordListCallback ok.")
    DebugTable(content)
    if #content.battle_report > 0 then
      GameUIWorldChampion.mFightRecord.mFightRecordList = content.battle_report
      GameUIWorldChampion:ShowPromotionSupportResult()
    else
      GameUIWorldChampion:ShowPromotionSupport()
    end
    return true
  end
  return false
end
function GameUIWorldChampion:RequestPromotionDataByLvRange(rangeId)
  local req = {budo_level = rangeId}
  DebugOutPutTable(req, "RequestPromotionDataByLvRange")
  NetMessageMgr:SendMsg(NetAPIList.budo_promotion_req.Code, req, self.RequestPromotionDataByLvRangeCallback, true, nil)
end
function GameUIWorldChampion.RequestPromotionDataByLvRangeCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.budo_promotion_req.Code then
    DebugOut("RequestPromotionDataByLvRangeCallback err.")
    DebugTable(content)
    if 0 == content.code then
      GameUIWorldChampion.mIsLvChooseShow = false
      GameUIWorldChampion:HideLevelChooseList()
    else
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  return false
end
function GameUIWorldChampion:RequestGetPromotionReward(itemId)
  GameUIWorldChampion.mCurRequsetItemId = itemId
  if GameUIWorldChampion.mMySupportList then
    local supportInfo = GameUIWorldChampion.mMySupportList[itemId]
    local req = {
      step = supportInfo.step,
      round = supportInfo.round
    }
    DebugOut("GameUIWorldChampion:RequestGetPromotionReward ")
    DebugTable(req)
    NetMessageMgr:SendMsg(NetAPIList.get_promotion_reward_req.Code, req, self.RequestGetPromotionRewardCallback, true, nil)
  else
    DebugOut("Error: GameUIWorldChampion.mMySupportList is nil.")
  end
end
function GameUIWorldChampion.RequestGetPromotionRewardCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.get_promotion_reward_req.Code then
    DebugOut("RequestGetPromotionRewardCallback.")
    DebugTable(content)
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    if 0 == content.code and -1 ~= GameUIWorldChampion.mCurRequsetItemId and GameUIWorldChampion.mMySupportList then
      GameUIWorldChampion.mMySupportList[GameUIWorldChampion.mCurRequsetItemId].award_status = 1
      GameUIWorldChampion:UpdateMySupportListItem(GameUIWorldChampion.mCurRequsetItemId)
      GameUIWorldChampion.mCurRequsetItemId = -1
    end
    return true
  end
  DebugOut("RequestGetPromotionRewardCallback err.")
  DebugTable(content)
  return false
end
function GameUIWorldChampion:RequestGetRank(lvRangeId)
  if GameUIWorldChampion.mCurLvRangeId ~= lvRangeId then
    GameUIWorldChampion:RequestPromotionDataByLvRange(lvRangeId)
  end
  local req = {step = lvRangeId}
  DebugOut("GameUIWorldChampion:RequestGetRank ")
  NetMessageMgr:SendMsg(NetAPIList.get_budo_rank_req.Code, req, self.RequestGetRankCallback, true, nil)
end
function GameUIWorldChampion.RequestGetRankCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.get_budo_rank_req.Code then
    DebugOut("RequestGetRankCallback err.")
    DebugTable(content)
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.get_budo_rank_ack.Code then
    DebugOut("RequestGetRankCallback ok.")
    DebugTable(content)
    GameUIWorldChampion.mRankList = content.rank_info
    GameUIWorldChampion:ShowRankList()
    return true
  end
  return false
end
function GameUIWorldChampion:GetPlayerIdx(round, index)
  if GameUIWorldChampion.mBTreeData then
    for k = 1, #GameUIWorldChampion.mBTreeData do
      if GameUIWorldChampion.mBTreeData[k].playerInfo and GameUIWorldChampion.mBTreeData[k].playerInfo.battle_round == round and GameUIWorldChampion.mBTreeData[k].playerInfo.index == index then
        return k
      end
    end
  end
  return nil
end
function GameUIWorldChampion:GetSupportInfo(roundId, index)
  DebugOut("GetSupportInfo " .. roundId .. " " .. index)
  if GameUIWorldChampion.mMySupportList then
    for k = 1, #GameUIWorldChampion.mMySupportList do
      local supportInfo = GameUIWorldChampion.mMySupportList[k]
      if supportInfo.round == roundId and supportInfo.index == index then
        DebugTable(supportInfo)
        return supportInfo
      end
    end
  end
  return nil
end
function GameUIWorldChampion:IsAlreadySupportInRound(roundId)
  if GameUIWorldChampion.mMySupportListAll then
    for k = 1, #GameUIWorldChampion.mMySupportListAll do
      local supportInfo = GameUIWorldChampion.mMySupportListAll[k]
      if supportInfo.round == roundId then
        return true
      end
    end
  end
  return false
end
function GameUIWorldChampion:GetSupportPlayerIdx(roundId, index)
  if GameUIWorldChampion.mBTreeData then
    for k = 1, #GameUIWorldChampion.mBTreeData do
      local item = GameUIWorldChampion.mBTreeData[k]
      if item.playerInfo and item.playerInfo.battle_round == roundId and item.playerInfo.index == index then
        return k
      end
    end
  end
  return -1
end
function GameUIWorldChampion:GetCurRound()
  local round = -1
  if GameUIWorldChampion.mBTreeData then
    for k = 1, #GameUIWorldChampion.mBTreeData do
      local item = GameUIWorldChampion.mBTreeData[k]
      if item.playerInfo then
        local tmp = 6 - GameUIWorldChampion:GetBTreeNodeLevel(k) + 1
        if round < tmp then
          round = tmp
        end
      end
    end
  end
  if round > 5 then
    round = 5
  end
  return round
end
function GameUIWorldChampion:GetLvRangeStr(lvRandId)
  local lvRange = ""
  if GameUIWorldChampion.mLvRangeList[lvRandId].min == GameUIWorldChampion.mLvRangeList[lvRandId].max then
    DebugOut("hello", GameLoader:GetGameText("LC_MENU_SA_LV_ARENR_TITLE"))
    lvRange = string.format(GameLoader:GetGameText("LC_MENU_SA_LV_ARENR_TITLE"), GameUIWorldChampion.mLvRangeList[lvRandId].min)
  else
    lvRange = GameLoader:GetGameText("LC_MENU_Level") .. GameUIWorldChampion.mLvRangeList[lvRandId].min .. "-Lv" .. GameUIWorldChampion.mLvRangeList[lvRandId].max
  end
  DebugOut("GetLvRangeStr " .. lvRandId .. " " .. lvRange)
  return lvRange
end
function GameUIWorldChampion:ShowItemDetil(item)
  if item then
    local item_type = item.item_type
    if GameHelper:IsResource(item_type) then
      return
    end
    if item_type == "krypton" then
      if item.level == 0 then
        item.level = 1
      end
      local detail = GameUIKrypton:TryQueryKryptonDetail(item.number, item.level, function(msgType, content)
        local ret = GameUIKrypton.QueryKryptonDetailCallBack(msgType, content)
        if ret then
          local tmp = GameGlobalData:GetKryptonDetail(item.number, item.level)
          if tmp == nil then
            return false
          end
          ItemBox:SetKryptonBox(tmp, item.number)
          local operationTable = {}
          operationTable.btnUnloadVisible = false
          operationTable.btnUpgradeVisible = false
          operationTable.btnUseVisible = false
          operationTable.btnDecomposeVisible = false
          ItemBox:ShowKryptonBox(320, 240, operationTable)
        end
        return ret
      end)
      if detail == nil then
      else
        ItemBox:SetKryptonBox(detail, item.number)
        local operationTable = {}
        operationTable.btnUnloadVisible = false
        operationTable.btnUpgradeVisible = false
        operationTable.btnUseVisible = false
        operationTable.btnDecomposeVisible = false
        ItemBox:ShowKryptonBox(320, 240, operationTable)
      end
    end
    if item_type == "item" then
      item.cnt = 1
      ItemBox:showItemBox("Item", item, tonumber(item.number), 320, 480, 200, 200, "", nil, "", nil)
    end
    if item_type == "fleet" then
      ItemBox:ShowCommanderDetail2(tonumber(item.number))
    end
  else
    DebugOut("ShowItemDetil error.")
  end
end
GameUIWorldChampion.mHistoryChampionData = nil
GameUIWorldChampion.mCurHistoryChampionPeriodIdx = -1
GameUIWorldChampion.mCurHistoryChampionListItemIdx = -1
GameUIWorldChampion.mMassHistoryData = {
  roundList = {}
}
GameUIWorldChampion.mRequestRoundId = -1
GameUIWorldChampion.mCurSelectedMassHistoryRoundId = -1
function GameUIWorldChampion:RequestChampionList(periodId)
  local req = {index = periodId}
  DebugOut("GameUIWorldChampion:RequestChampionList " .. tostring(periodId))
  NetMessageMgr:SendMsg(NetAPIList.budo_champion_req.Code, req, self.RequestChampionListCallback, true, nil)
end
function GameUIWorldChampion.RequestChampionListCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.budo_champion_req.Code then
    DebugOut("RequestChampionListCallback err.")
    DebugTable(content)
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.budo_champion_ack.Code then
    DebugOut("RequestChampionListCallback ok.")
    DebugTable(content)
    GameUIWorldChampion.mHistoryChampionData = content
    if 0 == content.max_index then
      local tip = GameLoader:GetGameText("LC_MENU_SA_NO_CHAMPION_LIST_INFO")
      GameTip:Show(tip, 3000)
    else
      local needPopPanel = false
      if -1 == GameUIWorldChampion.mCurHistoryChampionPeriodIdx then
        GameUIWorldChampion.mCurHistoryChampionPeriodIdx = GameUIWorldChampion:GetIdxByPeriod(GameUIWorldChampion.mHistoryChampionData.effect_list, GameUIWorldChampion.mHistoryChampionData.now_index)
        needPopPanel = true
      end
      GameUIWorldChampion:SetHistoryChampionList()
      if needPopPanel then
        GameUIWorldChampion:SetHistoryChampionPeriodChooseList()
        GameUIWorldChampion:ShowHistoryChampionPanel()
        if GameUIWorldChampion.mCurHistoryChampionPeriodIdx > 3 then
          GameUIWorldChampion:SetPeriodItemToTail(GameUIWorldChampion.mCurHistoryChampionPeriodIdx)
        end
      end
    end
    return true
  end
  return false
end
function GameUIWorldChampion:GetIdxByPeriod(array, period)
  if array and #array >= 1 then
    for k = 1, #array do
      if array[k] == period then
        return k
      end
    end
  end
  return 1
end
function GameUIWorldChampion:SetPeriodItemToTail(itemId)
  local flashObj = self:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "SetPeriodItemToTail", itemId)
  end
end
function GameUIWorldChampion:ShowHistoryChampionPanel()
  local flashObj = self:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "ShowHistoryChampionPanel")
    self.IsShowHistoryWindow = true
  end
end
function GameUIWorldChampion:SetHistoryChampionList()
  local flashObj = self:GetFlashObject()
  if flashObj and GameUIWorldChampion.mHistoryChampionData and GameUIWorldChampion.mHistoryChampionData.info then
    GameUIWorldChampion.mCurHistoryChampionListItemIdx = -1
    local itemCnt = #GameUIWorldChampion.mHistoryChampionData.info
    flashObj:InvokeASCallback("_root", "SetHistoryChampionList", itemCnt)
  end
end
function GameUIWorldChampion:RefreshHistoryChampionListItemHeight()
  local flashObj = self:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "RefreshHistoryChampionListItemHeight")
  end
end
function GameUIWorldChampion:UpdateHistoryChampionListItem(itemId)
  local flashObj = self:GetFlashObject()
  if flashObj and GameUIWorldChampion.mHistoryChampionData and GameUIWorldChampion.mHistoryChampionData.info then
    local info = GameUIWorldChampion.mHistoryChampionData.info[itemId]
    if info then
      local lvRange = GameLoader:GetGameText("LC_MENU_Level") .. tostring(info.min_lev) .. " - " .. GameLoader:GetGameText("LC_MENU_Level") .. tostring(info.max_lev)
      if info.min_lev == info.max_lev then
        lvRange = GameLoader:GetGameText("LC_MENU_Level") .. tostring(info.min_lev)
      end
      local playerName = info.champion.name
      local serverName = info.champion_server
      local point = tostring(info.champion.get_support)
      local battleCount = 0
      local winCount = 0
      local loseCount = 0
      local twoPlayerName = ""
      local winOrLose = ""
      local battleIds = ""
      local isShowDetail = false
      if GameUIWorldChampion.mCurHistoryChampionListItemIdx == itemId then
        isShowDetail = true
        if info.battle and 0 < #info.battle then
          for k = 1, #info.battle do
            local win = "lose"
            if info.battle[k].is_win then
              win = "win"
              winCount = winCount + 1
            else
              loseCount = loseCount + 1
            end
            winOrLose = winOrLose .. win .. "\001"
            twoPlayerName = twoPlayerName .. info.champion.name .. " VS " .. info.enemy.name .. "\001"
            battleIds = battleIds .. tostring(info.battle[k].battle_id) .. "\001"
            battleCount = battleCount + 1
          end
        end
      end
      flashObj:InvokeASCallback("_root", "UpdateHistoryChampionListItem", itemId, lvRange, playerName, serverName, point, isShowDetail, tostring(winCount), tostring(loseCount), battleCount, twoPlayerName, winOrLose, battleIds)
    end
  end
end
function GameUIWorldChampion:SetHistoryChampionPeriodChooseList()
  local flashObj = self:GetFlashObject()
  if flashObj and GameUIWorldChampion.mHistoryChampionData then
    local itemCnt = #GameUIWorldChampion.mHistoryChampionData.effect_list
    flashObj:InvokeASCallback("_root", "SetHistoryChampionPeriodChooseList", itemCnt)
  end
end
function GameUIWorldChampion:UpdateHistoryChampionPeriodChooseListItem(itemId)
  DebugOut("itemId " .. tostring(itemId))
  local flashObj = self:GetFlashObject()
  if flashObj then
    local period = GameLoader:GetGameText("LC_MENU_SA_SESSION_BUTTON")
    period = string.format(period, GameUIWorldChampion.mHistoryChampionData.effect_list[itemId])
    local isHighLight = itemId == GameUIWorldChampion.mCurHistoryChampionPeriodIdx
    flashObj:InvokeASCallback("_root", "UpdateHistoryChampionPeriodChooseListItem", itemId, period, isHighLight)
  end
end
function GameUIWorldChampion:SetMassHistoryRoundChooseList()
  local flashObj = self:GetFlashObject()
  if flashObj and GameUIWorldChampion.mPromotionMyBattle then
    local itemCnt = GameUIWorldChampion.mPromotionMyBattle.massHistoryCount
    flashObj:InvokeASCallback("_root", "SetMassHistoryRoundChooseList", itemCnt)
  end
end
function GameUIWorldChampion:UpdateMassHistoryRoundChooseListItem(itemId)
  DebugOut("itemId " .. tostring(itemId))
  local flashObj = self:GetFlashObject()
  if flashObj then
    local round = GameLoader:GetGameText("LC_MENU_SA_ROUND_LABBLE")
    round = string.format(round, itemId)
    local isHighLight = itemId == GameUIWorldChampion.mCurSelectedMassHistoryRoundId
    flashObj:InvokeASCallback("_root", "UpdateMassHistoryRoundChooseListItem", itemId, round, isHighLight)
  end
end
function GameUIWorldChampion:SetMassHistoryList(roundId)
  local flashObj = self:GetFlashObject()
  if flashObj and GameUIWorldChampion.mMassHistoryData then
    local itemCnt = #GameUIWorldChampion.mMassHistoryData.roundList[roundId]
    flashObj:InvokeASCallback("_root", "SetMassHistoryList", itemCnt)
  end
end
function GameUIWorldChampion:UpdateMassHistoryListItem(itemId)
  DebugOut("itemId " .. tostring(itemId))
  local flashObj = self:GetFlashObject()
  if flashObj then
    local listLen = #GameUIWorldChampion.mMassHistoryData.roundList[GameUIWorldChampion.mCurSelectedMassHistoryRoundId]
    local round = GameLoader:GetGameText("LC_MENU_SA_EVENT_LABBLE")
    round = string.format(round, listLen - (itemId - 1))
    local playerName = GameLoader:GetGameText("LC_MENU_SA_YOU_VS_PLAYER")
    local isWin = true
    local item = GameUIWorldChampion.mMassHistoryData.roundList[GameUIWorldChampion.mCurSelectedMassHistoryRoundId][listLen - (itemId - 1)]
    DebugOutPutTable(item, "fightrecord")
    local myPlayerId = tonumber(GameGlobalData:GetData("userinfo").player_id)
    local myServerId = tonumber(GameUtils:GetActiveServerInfo().logic_id)
    if myPlayerId == item.win_player.player_id then
      playerName = string.format(playerName, item.failed_player.name)
      isWin = true
    else
      playerName = string.format(playerName, item.win_player.name)
      isWin = false
    end
    flashObj:InvokeASCallback("_root", "UpdateMassHistoryListItem", itemId, round, playerName, isWin)
  end
end
function GameUIWorldChampion:RequestMassHistoryList(roundId)
  GameUIWorldChampion.mRequestRoundId = roundId
  local req = {
    player_id = GameGlobalData:GetData("userinfo").player_id,
    server_id = GameUtils:GetActiveServerInfo().logic_id,
    stage = 2,
    round = roundId
  }
  DebugOut("GameUIWorldChampion:RequestMassHistoryList " .. roundId)
  DebugTable(req)
  NetMessageMgr:SendMsg(NetAPIList.battle_report_req.Code, req, self.RequestMassHistoryListCallback, true, nil)
end
function GameUIWorldChampion.RequestMassHistoryListCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.battle_report_req.Code then
    DebugOut("RequestMassHistoryListCallback err.")
    DebugTable(content)
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.battle_report_ack.Code then
    DebugOut("RequestMassHistoryListCallback ok.")
    DebugTable(content)
    GameUIWorldChampion.mMassHistoryData.roundList[GameUIWorldChampion.mRequestRoundId] = content.battle_report
    if -1 == GameUIWorldChampion.mCurSelectedMassHistoryRoundId then
      GameUIWorldChampion.mCurSelectedMassHistoryRoundId = GameUIWorldChampion.mRequestRoundId
      GameUIWorldChampion:SetMassHistoryRoundChooseList()
      GameUIWorldChampion:ShowMassHistory()
    end
    GameUIWorldChampion:SetMassHistoryList(GameUIWorldChampion.mRequestRoundId)
    return true
  end
  return false
end
function GameUIWorldChampion:MassHistoryRoundChooseItemClickHandle(itemId)
  local oldIdx = GameUIWorldChampion.mCurSelectedMassHistoryRoundId
  GameUIWorldChampion.mCurSelectedMassHistoryRoundId = itemId
  GameUIWorldChampion:UpdateMassHistoryRoundChooseListItem(oldIdx)
  GameUIWorldChampion:UpdateMassHistoryRoundChooseListItem(GameUIWorldChampion.mCurSelectedMassHistoryRoundId)
  if GameUIWorldChampion.mMassHistoryData.roundList[GameUIWorldChampion.mCurSelectedMassHistoryRoundId] then
    GameUIWorldChampion:SetMassHistoryList(GameUIWorldChampion.mCurSelectedMassHistoryRoundId)
  else
    GameUIWorldChampion:RequestMassHistoryList(GameUIWorldChampion.mCurSelectedMassHistoryRoundId)
  end
end
function GameUIWorldChampion:MassHistoryReplayClickHandle(itemId)
  local listLen = #GameUIWorldChampion.mMassHistoryData.roundList[GameUIWorldChampion.mCurSelectedMassHistoryRoundId]
  local item = GameUIWorldChampion.mMassHistoryData.roundList[GameUIWorldChampion.mCurSelectedMassHistoryRoundId][listLen - (itemId - 1)]
  local battleId = item.battle_id
  self:RequestFightHistory(battleId)
end
function GameUIWorldChampion:ShowMassHistory()
  local flashObj = self:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "ShowMassHistory")
    flashObj:InvokeASCallback("_root", "SetMassHistoryBtnStatus", true, "pageBtoA")
  end
end
function GameUIWorldChampion:HideMassHistory()
  local flashObj = self:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "HideMassHistory")
    flashObj:InvokeASCallback("_root", "SetMassHistoryBtnStatus", true, "pageAtoB")
  end
end
