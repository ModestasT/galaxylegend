local GameUISevenBenefit = LuaObjectManager:GetLuaObject("GameUISevenBenefit")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameVip = LuaObjectManager:GetLuaObject("GameVip")
local GameStateStore = GameStateManager.GameStateStore
local GameUIBarLeft = LuaObjectManager:GetLuaObject("GameUIBarLeft")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameGoodsBuyBox = LuaObjectManager:GetLuaObject("GameGoodsBuyBox")
local GameGlobalData = GameGlobalData
GameUISevenBenefit.ShowPanleTypeId = {
  EVERYDAY_BENEFIT = 1,
  SEVENDAY_BENEFIT = 2,
  HALF_PRICE = 3
}
GameUISevenBenefit.todayUnlockData = {}
GameUISevenBenefit.everyDayBenefitData = {}
GameUISevenBenefit.sevenDayGoalData = {}
GameUISevenBenefit.halfPriceData = {}
GameUISevenBenefit.awardState = nil
GameUISevenBenefit.activityLefttime = nil
GameUISevenBenefit.awardleftitme = nil
GameUISevenBenefit.currentTabId = GameUISevenBenefit.ShowPanleTypeId.EVERYDAY_BENEFIT
GameUISevenBenefit.halfpriceType = -1
GameUISevenBenefit.time = 0
function GameUISevenBenefit:OnAddToGameState()
  DebugOut("GameUISevenBenefit:OnAddToGameState")
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  GameUISevenBenefit:InitTabButtonText()
  self:MoveinShowAnimation()
end
function GameUISevenBenefit:OnEraseFromGameState()
  self:UnloadFlashObject()
  GameUISevenBenefit.todayUnlockData = {}
  GameUISevenBenefit.everyDayBenefitData = {}
  GameUISevenBenefit.sevenDayGoalData = {}
  GameUISevenBenefit.halfPriceData = {}
  GameUISevenBenefit.currentTabId = GameUISevenBenefit.ShowPanleTypeId.EVERYDAY_BENEFIT
end
function GameUISevenBenefit:OnFSCommand(cmd, args)
  DebugOut("GameUISevenBenefit:OnFSCommand: ", cmd)
  if cmd == "sevenLayerMoveinover" then
    self:ShowCurrentTab(GameUISevenBenefit.currentTabId)
  elseif cmd == "sevenLayerMoveoutover" then
    GameStateManager:GetCurrentGameState():EraseObject(GameUISevenBenefit)
  elseif cmd == "tab_command" then
    self:ShowCurrentTab(tonumber(args))
  elseif cmd == "svenday_tabcommand" then
    self:ShowCurrentSevenGoalTab(tonumber(args))
  elseif cmd == "close_layer" then
    self:GetFlashObject():InvokeASCallback("_root", "moveoutAnimation")
  elseif cmd == "updatelistbox_everyday" then
    self:UpdateEveryDayListboxItem(tonumber(args))
  elseif cmd == "everyday_itembutton" then
    self:EveryDayListItemButtonCallback(tonumber(args))
  elseif cmd == "updatelistbox_sevendaygoal" then
    self:UpdateSevenDayListboxItem(tonumber(args))
  elseif cmd == "sevenDayListbox_Button_Clicked" then
    self:SevenDayGoalListboxItemButtonClicked(tonumber(args))
  elseif cmd == "showhalfpriceinfo" then
    self:ShowHalfPriceBenefitInfo(tonumber(args))
  elseif cmd == "updatelistbox_halfprice" then
    self:UpdateHalfPriceListboxItem(tonumber(args))
  elseif cmd == "carnival_move_out" then
    local m_flahsObj = self:GetFlashObject()
    if m_flahsObj then
      m_flahsObj:InvokeASCallback("_root", "moveoutTipLayerAnimation")
      self.IsShowCarnivalPopWindow = false
    end
  end
end
function GameUISevenBenefit:Update(dt)
  self:GetFlashObject():Update(dt)
  local m_flahsObj = self:GetFlashObject()
  if m_flahsObj then
    m_flahsObj:InvokeASCallback("_root", "onUpdate", dt)
  end
  GameUISevenBenefit.time = GameUISevenBenefit.time + dt
  if GameUISevenBenefit.time >= 1000 then
    GameUISevenBenefit.time = 0
    local timeText = ""
    if 0 < GameUISevenBenefit.activityLefttime then
      GameUISevenBenefit.activityLefttime = GameUISevenBenefit.activityLefttime - 1
      timeText = GameLoader:GetGameText("LC_MENU_CARNIVAL_EVENT_END_TIME") .. GameUtils:formatTimeStringAsTwitterStyle(GameUISevenBenefit.activityLefttime)
    elseif 0 < GameUISevenBenefit.awardleftitme then
      GameUISevenBenefit.awardleftitme = GameUISevenBenefit.awardleftitme - 1
      timeText = GameLoader:GetGameText("LC_MENU_CARNIVAL_CLOSE_TIME_INFO") .. GameUtils:formatTimeStringAsTwitterStyle(GameUISevenBenefit.awardleftitme)
    end
    if m_flahsObj then
      m_flahsObj:InvokeASCallback("_root", "setTimeText", timeText)
    end
  end
end
function GameUISevenBenefit:InitShowTipsLayer()
  NetMessageMgr:SendMsg(NetAPIList.enter_seven_day_req.Code, nil, GameUISevenBenefit.GetNewUnlockCallback, true, nil)
end
function GameUISevenBenefit:SetTab(currentTab)
end
function GameUISevenBenefit:MoveinShowAnimation()
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "moveinAnimation")
  end
end
function GameUISevenBenefit:ShowCurrentTab(currentTab)
  DebugOut("GameUISevenBenefit:ShowCurrentTab:", currentTab)
  GameUISevenBenefit.currentTabId = currentTab
  self:ShowCurrentTabButtonState(currentTab)
  if currentTab == GameUISevenBenefit.ShowPanleTypeId.EVERYDAY_BENEFIT then
    self:ShowEveryDayBenefitLayer()
  elseif currentTab == GameUISevenBenefit.ShowPanleTypeId.SEVENDAY_BENEFIT then
    GameUISevenBenefit.ShowCurrentSevenGoalTabId = 1
    self:ShowSevenDayBenefitLayer()
  elseif currentTab == GameUISevenBenefit.ShowPanleTypeId.HALF_PRICE then
    self:ShowHalfPriceBenefitLayer()
  end
end
function GameUISevenBenefit:SetTabButtonState()
  if GameUISevenBenefit.awardState ~= nil then
    local everyDayButtonState = false
    local sevenGoalButtonState = false
    local halfPriceButtonState = false
    if GameUISevenBenefit.awardState[1] == 1 then
      everyDayButtonState = true
    else
      everyDayButtonState = false
    end
    if GameUISevenBenefit.awardState[2] == 1 then
      sevenGoalButtonState = true
    else
      sevenGoalButtonState = false
    end
    if GameUISevenBenefit.awardState[3] == 1 then
      halfPriceButtonState = true
    else
      halfPriceButtonState = false
    end
    DebugOut("InitTab:", everyDayButtonState, sevenGoalButtonState, halfPriceButtonState)
    if self:GetFlashObject() then
      self:GetFlashObject():InvokeASCallback("_root", "setAllTabHasAwardState", everyDayButtonState, sevenGoalButtonState, halfPriceButtonState)
    end
  end
end
function GameUISevenBenefit:InitTabButtonText()
  local everydayTabButtonText = GameLoader:GetGameText("LC_MENU_CARNIVAL_DAILY_GIFT_BUTTON")
  local sevenTabButtonText = GameLoader:GetGameText("LC_MENU_CARNIVAL_SEVEN_GOAL_BUTTON")
  local halfpriceTabButtonText = GameLoader:GetGameText("LC_MENU_CARNIVAL_HALF_PRICE_BUTTON")
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "setAllTabButtonText", everydayTabButtonText, sevenTabButtonText, halfpriceTabButtonText)
  end
end
function GameUISevenBenefit:ShowCurrentTabButtonState(currentTab)
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "showCurrentTab", currentTab)
  end
end
function GameUISevenBenefit.CommanAckCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.daily_benefits_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.get_goal_award_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.daily_benefits_award_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    NetMessageMgr:SendMsg(NetAPIList.daily_benefits_req.Code, nil, GameUISevenBenefit.GetDailyBenefitsDataCallback, true, nil)
    return true
  end
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.select_goal_type_req.Code then
    if content.code ~= 0 then
      GameUISevenBenefit:ShowAlert("error", content.code, nil)
    end
    return true
  end
  return false
end
function GameUISevenBenefit.SevendayNtfCallback(content)
  DebugOut("SevendayNtfCallback:")
  DebugTable(content)
  GameUISevenBenefit.awardState = content.awards_status
  GameUISevenBenefit.activityLefttime = content.activity_left_time
  GameUISevenBenefit.awardleftitme = content.award_left_time
  GameUISevenBenefit.open = content.open_status
  if GameUISevenBenefit.open == 0 then
    if GameUISevenBenefit:GetFlashObject() then
      GameTip:Show(GameLoader:GetGameText("LC_MENU_CARNIVAL_EVENT_ENDIN_INFO"))
      GameStateManager:GetCurrentGameState():EraseObject(GameUISevenBenefit)
    end
  elseif GameUISevenBenefit.open == 1 then
  end
  GameUISevenBenefit:SetTabButtonState()
end
function GameUISevenBenefit.GetNewUnlockCallback(msgType, content)
  if msgType == NetAPIList.enter_seven_day_ack.Code then
    DebugOut("GetNewUnlockCallback:")
    DebugTable(content)
    GameUISevenBenefit.todayUnlockData = content.today_unlock
    GameUISevenBenefit:IsShowTipsLayer()
    return true
  end
  return false
end
function GameUISevenBenefit:GetAllHaveAwardStatus()
  if GameUISevenBenefit.awardState ~= nil then
    for k, v in pairs(GameUISevenBenefit.awardState) do
      if v == 1 then
        return true
      end
    end
    return false
  end
end
function GameUISevenBenefit:IsShowTipsLayer()
  local isShow = false
  if #GameUISevenBenefit.todayUnlockData.daily_gift > 0 or 0 < #GameUISevenBenefit.todayUnlockData.seven_day_goal or 0 < #GameUISevenBenefit.todayUnlockData.half_buy_item then
    isShow = true
  end
  DebugOut("isShow:", isShow)
  if isShow == false then
    return
  end
  local dailygiftDataText = GameUISevenBenefit:GetShowTipDailyGiftDataText(GameUISevenBenefit.todayUnlockData)
  local sevendaygoalDataText = GameUISevenBenefit:GetShowTipSevendaygoalDataText(GameUISevenBenefit.todayUnlockData)
  local halfpriceDataText = GameUISevenBenefit:GetShowTipHalfPriceDataText(GameUISevenBenefit.todayUnlockData)
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "showTipsLayer", isShow, dailygiftDataText, sevendaygoalDataText, halfpriceDataText)
    if isShow then
      self:GetFlashObject():InvokeASCallback("_root", "moveinTipLayerAnimation")
      self.IsShowCarnivalPopWindow = true
    end
  end
end
function GameUISevenBenefit:GetShowTipDailyGiftDataText(content)
  local text = ""
  item = content.daily_gift
  for key, value in pairs(item) do
    local awardText = ""
    if value.type == "open_award" then
      awardText = GameLoader:GetGameText("LC_MENU_CARNIVAL_REWARD_BUTTON")
    elseif value.type == "acc_purchase" then
      awardText = GameLoader:GetGameText("LC_MENU_CARNIVAL_PAY_REWARD_BUTTON")
    end
    local awardItemText = ""
    for i = 1, 5 do
      local awardIcon = ""
      local awardItem = value.award[i]
      if awardItem then
        awardIcon = GameHelper:GetCommonIconFrame(awardItem, nil)
      else
        awardIcon = "empty"
      end
      awardItemText = awardItemText .. awardIcon .. "\001"
    end
    text = text .. awardText .. "\002"
    text = text .. awardItemText .. "\002"
  end
  DebugOut("GetShowTipDailyGiftDataText:", text)
  return text
end
function GameUISevenBenefit:GetShowTipSevendaygoalDataText(content)
  local text = ""
  local name = ""
  local unlockList = ""
  name = GameLoader:GetGameText("LC_MENU_CARNIVAL_SEVEN_GOAL_BUTTON")
  local sevendaygoalData = content.seven_day_goal
  for key, value in pairs(sevendaygoalData) do
    unlockList = unlockList .. tostring(value) .. "\002"
  end
  text = text .. name .. "\001"
  text = text .. unlockList .. "\001"
  DebugOut("GetShowTipSevendaygoalDataText:", text)
  return text
end
function GameUISevenBenefit:GetShowTipHalfPriceDataText(content)
  local text = ""
  local halfprice = content.half_buy_item
  for key, value in pairs(halfprice) do
    if value.status == 4 then
      local award = value.item
      text = text .. value.price .. "\001"
      text = text .. value.discount .. "\001"
      text = text .. GameHelper:GetCommonIconFrame(award, nil) .. "\001"
      text = text .. GameHelper:GetAwardCount(award.item_type, award.number, award.no) .. "\001"
      local priceType = ""
      if value.price_type == 1 then
        priceType = "money"
      elseif value.price_type == 2 then
        priceType = "credit"
      end
      text = text .. priceType .. "\001"
    end
  end
  local name = GameLoader:GetGameText("LC_MENU_CARNIVAL_HALF_PRICE_BUTTON")
  text = text .. name .. "\001"
  DebugOut("GetShowTipHalfPriceDataText:", text)
  return text
end
function GameUISevenBenefit:ShowEveryDayBenefitLayer()
  DebugOut("ShowEveryDayBenefitLayer:")
  DebugTable(GameUISevenBenefit.everyDayBenefitData)
  if GameUISevenBenefit.everyDayBenefitData and #GameUISevenBenefit.everyDayBenefitData > 0 then
    if self:GetFlashObject() then
      self:GetFlashObject():InvokeASCallback("_root", "initListBoxEveryDayBenefit", #GameUISevenBenefit.everyDayBenefitData)
    end
    self:InitShowTipsLayer()
  else
    GameUISevenBenefit.GetDailyBenefitsDataFromSever()
  end
end
function GameUISevenBenefit.GetDailyBenefitsDataCallback(msgType, content)
  if msgType == NetAPIList.daily_benefits_ack.Code then
    DebugOut("GetDailyBenefitsDataCallback:")
    DebugTable(content)
    GameUISevenBenefit.everyDayBenefitData = content.info
    if GameUISevenBenefit:GetFlashObject() then
      GameUISevenBenefit:GetFlashObject():InvokeASCallback("_root", "initListBoxEveryDayBenefit", #GameUISevenBenefit.everyDayBenefitData)
    end
    GameUISevenBenefit:InitShowTipsLayer()
    return true
  end
  return false
end
function GameUISevenBenefit:UpdateEveryDayListboxItem(index)
  local item = GameUISevenBenefit.everyDayBenefitData[index]
  if item == nil then
    return
  end
  local itemText = GameUISevenBenefit:EveryDayBenefitItemDate(item)
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "updateListboxEveryDay", index, itemText)
  end
end
function GameUISevenBenefit:EveryDayBenefitItemDate(item)
  local text = ""
  local titleText = ""
  local buttonState = ""
  local buttonText = ""
  local type = ""
  local progresstext = ""
  local awardText = ""
  if item.type == "open_award" then
    titleText = string.format(GameLoader:GetGameText("LC_MENU_CARNIVAL_PAY_REWARD_INFO"), item.level)
  else
    titleText = string.format(GameLoader:GetGameText("LC_MENU_CARNIVAL_REWARD_INFO"), item.level)
  end
  buttonState = tostring(item.flag)
  if item.flag == 1 then
    buttonText = GameLoader:GetGameText("LC_MENU_CARNIVAL_CLAIMED_BUTTON")
  elseif item.flag == 2 then
    buttonText = GameLoader:GetGameText("LC_MENU_CARNIVAL_LOCKED_BUTTON")
  elseif item.flag == 3 then
    if item.type == "open_award" then
      buttonText = GameLoader:GetGameText("LC_MENU_CARNIVAL_INCOMPLETE_BUTTON")
    else
      buttonText = GameLoader:GetGameText("LC_MENU_CARNIVAL_PURCHASE_BUTTON")
    end
  elseif item.flag == 4 then
    buttonText = GameLoader:GetGameText("LC_MENU_CARNIVAL_RECEIVE_BUTTON")
  elseif item.flag == 5 then
    buttonText = GameLoader:GetGameText("LC_MENU_CARNIVAL_EXPIRED_BUTTON")
  end
  type = item.type
  progresstext = tostring(item.current) .. "/" .. tostring(item.level)
  for i = 1, 4 do
    local awardIcon = ""
    local awardCount = ""
    local awardItemText = ""
    local awardItem = item.award[i]
    if awardItem then
      awardIcon = GameHelper:GetCommonIconFrame(awardItem, nil)
      awardCount = GameUtils.numberConversion(GameHelper:GetAwardCount(awardItem.item_type, awardItem.number, awardItem.no))
    else
      awardIcon = "empty"
      awardCount = "empty"
    end
    awardItemText = awardItemText .. awardIcon .. "\001"
    awardItemText = awardItemText .. awardCount .. "\001"
    awardText = awardText .. awardItemText .. "\002"
  end
  text = text .. titleText .. "\003"
  text = text .. buttonState .. "\003"
  text = text .. buttonText .. "\003"
  text = text .. type .. "\003"
  text = text .. progresstext .. "\003"
  text = text .. awardText .. "\003"
  DebugOut("EveryDayBenefitItemDate:", text)
  return text
end
function GameUISevenBenefit:EveryDayListItemButtonCallback(itemId)
  DebugOut("EveryDayListItemButtonCallback:", itemId)
  local item = GameUISevenBenefit.everyDayBenefitData[itemId]
  if item == nil then
    return
  end
  if item.flag == 4 then
    local param = {
      type = item.type,
      level = item.level
    }
    DebugTable(param)
    NetMessageMgr:SendMsg(NetAPIList.daily_benefits_award_req.Code, param, GameUISevenBenefit.CommanAckCallback, true, nil)
  end
  if item.type == "acc_purchase" and item.flag == 3 and not GameGlobalData:FTPVersion() then
    GameVip:showVip(false)
  end
end
function GameUISevenBenefit.GetDailyBenefitsDataFromSever()
  NetMessageMgr:SendMsg(NetAPIList.daily_benefits_req.Code, nil, GameUISevenBenefit.GetDailyBenefitsDataCallback, true, nil)
end
function GameUISevenBenefit.updateDailyBenefitsNtf(content)
  DebugOut("updateDailyBenefitsNtf")
  DebugTable(content)
  GameUISevenBenefit.everyDayBenefitData = content.info
  if GameUISevenBenefit.currentTabId == GameUISevenBenefit.ShowPanleTypeId.EVERYDAY_BENEFIT and GameUISevenBenefit:GetFlashObject() then
    GameUISevenBenefit:GetFlashObject():InvokeASCallback("_root", "initListBoxEveryDayBenefit", #GameUISevenBenefit.everyDayBenefitData)
  end
end
GameUISevenBenefit.SevenGoalTab = {}
GameUISevenBenefit.SevenGoalSubTaskButtonState = {
  LOOT_CAN = 0,
  LOOT_UNFINISHED = 1,
  LOOT_ALREADY = 2,
  LOOT_OVER = 3
}
GameUISevenBenefit.ShowCurrentSevenGoalTabId = 1
function GameUISevenBenefit:SetSevenDayBenifitData()
  local haveAwardStates = GameUISevenBenefit:GetSevenDayHasAwardState(GameUISevenBenefit.sevenDayGoalData)
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "setSevenDayTabHaveAward", haveAwardStates)
  end
end
function GameUISevenBenefit:GetSevenDayHasAwardState(content)
  local text = ""
  for key, value in pairs(content) do
    if value.status == GameUISevenBenefit.SevenGoalSubTaskButtonState.LOOT_CAN then
      text = text .. tostring(1) .. "\001"
    else
      text = text .. tostring(0) .. "\001"
    end
  end
  DebugOut("text:", text)
  return text
end
function GameUISevenBenefit:SetSevenDayBenifitTabIconAndStates(content)
  local text = ""
  for key, value in pairs(content) do
    local itemText = ""
    local icon = tostring(value.type)
    local status = value.locked
    itemText = itemText .. icon .. "\001"
    itemText = itemText .. status .. "\001"
    text = text .. itemText .. "\002"
  end
  DebugOut("SetSevenDayBenifitTabIconAndStates:", text)
  if GameUISevenBenefit:GetFlashObject() then
    GameUISevenBenefit:GetFlashObject():InvokeASCallback("_root", "setSevenDayTabIconAndStatus", text)
  end
end
function GameUISevenBenefit:ShowSevenDayBenefitLayer()
  DebugOut("ShowSevenDayBenefitLayer:")
  if GameUISevenBenefit.sevenDayGoalData and #GameUISevenBenefit.sevenDayGoalData > 0 then
    GameUISevenBenefit:SetSevenDayBenifitTabIconAndStates(GameUISevenBenefit.sevenDayGoalData)
    GameUISevenBenefit:ShowCurrentSevenGoalTab(GameUISevenBenefit.ShowCurrentSevenGoalTabId)
  else
    NetMessageMgr:SendMsg(NetAPIList.goal_info_req.Code, nil, GameUISevenBenefit.GetSevenDayBenefitDataCallback, true, nil)
  end
end
function GameUISevenBenefit.GetSevenDayBenefitDataCallback(msgType, content)
  if msgType == NetAPIList.goal_info_ack.Code then
    DebugOut("GetSevenDayBenefitDataCallback:")
    DebugTable(content)
    GameUISevenBenefit.sevenDayGoalData = content.task_list
    GameUISevenBenefit:SetSevenDayBenifitData()
    GameUISevenBenefit:SetSevenDayBenifitTabIconAndStates(GameUISevenBenefit.sevenDayGoalData)
    GameUISevenBenefit:ShowCurrentSevenGoalTab(GameUISevenBenefit.ShowCurrentSevenGoalTabId)
    return true
  end
  return false
end
function GameUISevenBenefit:InitSevenGoalTab(...)
end
function GameUISevenBenefit:SetSevenGoalHintState()
end
function GameUISevenBenefit:SetCurrentSevenGoalTabState(currentTabId)
  local titleText = GameLoader:GetGameText("LC_MENU_SEVEN_GOAL_" .. tostring(GameUISevenBenefit.sevenDayGoalData[currentTabId].type) .. "_TITLE")
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "showCurrentSevenDayTab", currentTabId, titleText)
  end
end
function GameUISevenBenefit:ShowCurrentSevenGoalTab(currentSevenTab)
  DebugOut("GameUISevenBenefit:ShowCurrentSevenGoalTab:", currentSevenTab)
  local currentItem = GameUISevenBenefit.sevenDayGoalData[currentSevenTab]
  if currentItem == nil then
    return
  end
  DebugOut("currentItem:")
  DebugTable(currentItem)
  if currentItem.locked == 0 then
    GameTip:Show(GameLoader:GetGameText("LC_MENU_CARNIVAL_TOMORROW_REWARD_INFO"))
    return
  end
  GameUISevenBenefit:SetCurrentSevenGoalTabState(currentSevenTab)
  GameUISevenBenefit.ShowCurrentSevenGoalTabId = currentSevenTab
  if currentItem.locked == 1 then
    currentItem.locked = 2
    local param = {
      type = currentItem.type
    }
    NetMessageMgr:SendMsg(NetAPIList.select_goal_type_req.Code, param, GameUISevenBenefit.CommanAckCallback, false, nil)
  end
  if GameUISevenBenefit:GetFlashObject() then
    GameUISevenBenefit:GetFlashObject():InvokeASCallback("_root", "initListBoxSevenDayBenefit", #currentItem.subtasks)
  end
end
function GameUISevenBenefit:UpdateSevenDayListboxItem(itemId)
  DebugOut("UpdateSevenDayListboxItem:", itemId)
  local item = GameUISevenBenefit.sevenDayGoalData[GameUISevenBenefit.ShowCurrentSevenGoalTabId].subtasks[itemId]
  if item == nil then
    return
  end
  DebugOut("item:")
  DebugTable(item)
  local itemText = GameUISevenBenefit:GetSevenDayListboxItemText(item)
  DebugOut("itemText:")
  DebugTable(item)
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "updateListboxSevendayGoal", itemId, itemText)
  end
end
function GameUISevenBenefit:GetSevenDayListboxItemText(item)
  local text = ""
  local taskName = ""
  local finishTask = ""
  local needTask = ""
  local buttonState = ""
  local awardIcon = ""
  local awardCount = ""
  local buttonText = ""
  taskName = GameLoader:GetGameText("LC_MENU_SEVEN_GOAL_" .. tostring(item.id))
  finishTask = GameUtils.numberConversion(item.finished)
  needTask = GameUtils.numberConversion(item.total)
  buttonState = tostring(item.status)
  awardIcon = GameHelper:GetCommonIconFrame(item.awards[1], nil)
  awardCount = GameUtils.numberConversion(GameHelper:GetAwardCount(item.awards[1].item_type, item.awards[1].number, item.awards[1].no))
  if item.status == GameUISevenBenefit.SevenGoalSubTaskButtonState.LOOT_ALREADY then
    buttonText = GameLoader:GetGameText("LC_MENU_CARNIVAL_CLAIMED_BUTTON")
  elseif item.status == GameUISevenBenefit.SevenGoalSubTaskButtonState.LOOT_UNFINISHED then
    buttonText = GameLoader:GetGameText("LC_MENU_CARNIVAL_INCOMPLETE_BUTTON")
  elseif item.status == GameUISevenBenefit.SevenGoalSubTaskButtonState.LOOT_CAN then
    buttonText = GameLoader:GetGameText("LC_MENU_CARNIVAL_RECEIVE_BUTTON")
  elseif item.status == GameUISevenBenefit.SevenGoalSubTaskButtonState.LOOT_OVER then
    buttonText = GameLoader:GetGameText("LC_MENU_CARNIVAL_EXPIRED_BUTTON")
  end
  text = text .. taskName .. "\001"
  text = text .. finishTask .. "\001"
  text = text .. needTask .. "\001"
  text = text .. buttonState .. "\001"
  text = text .. awardIcon .. "\001"
  text = text .. awardCount .. "\001"
  text = text .. buttonText .. "\001"
  DebugOut("GetSevenDayListboxItemText:", text)
  return text
end
function GameUISevenBenefit:SevenDayGoalListboxItemButtonClicked(itemId)
  DebugOut("SevenDayGoalListboxItemButtonClicked:", itemId)
  local item = GameUISevenBenefit.sevenDayGoalData[GameUISevenBenefit.ShowCurrentSevenGoalTabId].subtasks[itemId]
  DebugTable(item)
  if item == nil then
    return
  end
  if item.status == GameUISevenBenefit.SevenGoalSubTaskButtonState.LOOT_CAN then
    local param = {
      task_id = item.id
    }
    NetMessageMgr:SendMsg(NetAPIList.get_goal_award_req.Code, param, GameUISevenBenefit.SevenDayGoalListboxItemButtonClickedCallback, true, nil)
  end
end
function GameUISevenBenefit.SevenDayGoalListboxItemButtonClickedCallback(msgType, content)
  if msgType == NetAPIList.get_goal_award_ack.Code then
    DebugOut("SevenDayGoalListboxItemButtonClickedCallback")
    if content.status then
      GameUISevenBenefit.sevenDayGoalData[GameUISevenBenefit.ShowCurrentSevenGoalTabId] = content.task_info
      GameUISevenBenefit:SetSevenDayBenifitData()
      GameUISevenBenefit:ShowCurrentSevenGoalTab(GameUISevenBenefit.ShowCurrentSevenGoalTabId)
    end
    return true
  end
  return false
end
GameUISevenBenefit.HalfpriceAwardStatus = {
  ALREADY = 1,
  LOCKED = 2,
  OVER = 3,
  CAN = 4
}
function GameUISevenBenefit:GetHalfPriceBenefitDataFromServer(...)
  NetMessageMgr:SendMsg(NetAPIList.cutoff_info_req.Code, nil, GameUISevenBenefit.GetHalfPriceBenefitDataCallback, true, nil)
end
function GameUISevenBenefit:ShowHalfPriceBenefitLayer()
  DebugOut("ShowHalfPriceBenefitLayer:")
  DebugTable(GameUISevenBenefit.halfPriceData)
  local resource = GameGlobalData:GetData("resource")
  if GameUISevenBenefit.halfPriceData and #GameUISevenBenefit.halfPriceData > 0 then
    if self:GetFlashObject() then
      self:GetFlashObject():InvokeASCallback("_root", "initListBoxHalfPrice", #GameUISevenBenefit.halfPriceData, GameUtils.numberConversion(resource.credit))
    end
  else
    GameUISevenBenefit:GetHalfPriceBenefitDataFromServer()
  end
end
function GameUISevenBenefit.GetHalfPriceBenefitDataCallback(msgType, content)
  if msgType == NetAPIList.cutoff_info_ack.Code then
    DebugOut("GetHalfPriceBenefitDataCallback:")
    DebugTable(content)
    GameUISevenBenefit.halfPriceData = content.cutoff_item_list
    local resource = GameGlobalData:GetData("resource")
    if GameUISevenBenefit:GetFlashObject() then
      GameUISevenBenefit:GetFlashObject():InvokeASCallback("_root", "initListBoxHalfPrice", #GameUISevenBenefit.halfPriceData, GameUtils.numberConversion(resource.credit))
    end
    return true
  end
  return false
end
function GameUISevenBenefit:UpdateHalfPriceListboxItem(index)
  DebugOut("UpdateHalfPriceListboxItem--index: ", index)
  local item = GameUISevenBenefit.halfPriceData[index]
  if item == nil then
    return
  end
  DebugOut("item:")
  DebugTable(item)
  local itemText = GameUISevenBenefit:GetHalfPriceListboxItemText(item)
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "updateListboxHalfPrice", index, itemText)
  end
end
function GameUISevenBenefit.GetDynamicResCallback()
  GameUISevenBenefit:ShowHalfPriceBenefitLayer()
end
function GameUISevenBenefit:GetHalfPriceListboxItemText(item)
  local text = ""
  local status = ""
  local statusText = ""
  local priceType = ""
  local priceText = ""
  local disPriceText = ""
  local discountText = ""
  local numText = ""
  local icon = ""
  local count = ""
  local name = ""
  status = item.status
  if status == GameUISevenBenefit.HalfpriceAwardStatus.ALREADY then
    statusText = GameLoader:GetGameText("LC_MENU_CARNIVAL_PURCHASED")
  elseif status == GameUISevenBenefit.HalfpriceAwardStatus.OVER then
    statusText = GameLoader:GetGameText("LC_MENU_CARNIVAL_EXPIRED_BUTTON")
  end
  if item.price_type == 1 then
    priceType = "money"
  elseif item.price_type == 2 then
    priceType = "credit"
  end
  priceText = item.price
  disPriceText = item.price * item.discount / 100
  discountText = 100 - item.discount
  numText = GameLoader:GetGameText("LC_MENU_CARNIVAL_REMAINING_AMOUNT") .. item.number .. "/" .. item.max_number
  icon = GameHelper:GetCommonIconFrame(item.item, GameUISevenBenefit.GetDynamicResCallback)
  count = GameUtils.numberConversion(GameHelper:GetAwardCount(item.item.item_type, item.item.number, item.item.no))
  name = GameHelper:GetAwardNameText(item.item.item_type, item.item.number)
  text = text .. status .. "\001"
  text = text .. statusText .. "\001"
  text = text .. priceType .. "\001"
  text = text .. priceText .. "\001"
  text = text .. disPriceText .. "\001"
  text = text .. discountText .. "\001"
  text = text .. numText .. "\001"
  text = text .. icon .. "\001"
  text = text .. count .. "\001"
  text = text .. name .. "\001"
  DebugOut("GetHalfPriceListboxItemText: ", text)
  return text
end
function GameUISevenBenefit:ShowHalfPriceBenefitInfo(itemId)
  local item = GameUISevenBenefit.halfPriceData[itemId]
  DebugOut("ShowHalfPriceBenefitInfo:")
  DebugTable(item)
  if item == nil then
    return
  end
  if item.status == GameUISevenBenefit.HalfpriceAwardStatus.ALREADY then
    GameTip:Show(GameLoader:GetGameText("LC_MENU_CARNIVAL_PURCHASED_INFO"))
  elseif item.status == GameUISevenBenefit.HalfpriceAwardStatus.LOCKED then
    GameTip:Show(GameLoader:GetGameText("LC_MENU_CARNIVAL_LOCKED_INFO"))
  elseif item.status == GameUISevenBenefit.HalfpriceAwardStatus.CAN then
    GameStateStore:SetCurItemDetail(item)
  end
end
if AutoUpdate.isAndroidDevice then
  function GameUISevenBenefit.OnAndroidBack()
    if GameUISevenBenefit.IsShowCarnivalPopWindow then
      GameUISevenBenefit:OnFSCommand("carnival_move_out")
    else
      GameUISevenBenefit:OnFSCommand("close_layer")
    end
  end
end
