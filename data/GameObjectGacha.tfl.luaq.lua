local GameStateGacha = GameStateManager.GameStateGacha
local GameObjectGacha = LuaObjectManager:GetLuaObject("GameObjectGacha")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local QuestTutorialSlot = TutorialQuestManager.QuestTutorialSlot
local GameUIBarLeft = LuaObjectManager:GetLuaObject("GameUIBarLeft")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameStateMainPlanet = GameStateManager.GameStateMainPlanet
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local GameUICommonDialog = LuaObjectManager:GetLuaObject("GameUICommonDialog")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local GameUIWDStuff = LuaObjectManager:GetLuaObject("GameUIWDStuff")
local currentTimes = 1
local currentCoinCost = 1
local currentSlotNumber = 40
local currentCoinNumber = 0
GameObjectGacha.mShareStoryChecked = {
  [2] = true,
  [4] = true,
  [8] = true
}
GameObjectGacha.mCurrentRate = 1
local awardsPool
function GameObjectGacha:OnAddToGameState(stateParent)
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  GameObjectGacha:UpdateRollTimes()
  GameObjectGacha:CheckDownloadImage()
  local boardCast_1 = GameLoader:GetGameText("LC_MENU_GALAXY_SLOT_BREFIN_CHAR") or ""
  local boardCast_2 = GameLoader:GetGameText("LC_MENU_GALAXY_SLOT_BREFIN_CHAR_2") or ""
  DebugOut("boardCast_1", boardCast_1)
  DebugOut("boardCast_2", boardCast_2)
  local flashObject = GameObjectGacha:GetFlashObject()
  if flashObject then
    flashObject:InvokeASCallback("_root", "setRollTextInfo", boardCast_1 .. "  " .. boardCast_2)
    if awardsPool ~= nil and #awardsPool > 0 then
      local poolStr = ""
      for _, item in pairs(awardsPool) do
        poolStr = poolStr .. item .. "\001"
      end
      flashObject:InvokeASCallback("_root", "lua2fs_updateAwardPool", poolStr)
    end
    local lang = "en"
    if GameSettingData and GameSettingData.Save_Lang then
      lang = GameSettingData.Save_Lang
      if string.find(lang, "ru") == 1 then
        lang = "ru"
      elseif GameUtils:IsChinese() and GameUtils:IsSimplifiedChinese() then
        lang = "zh"
      end
    end
    flashObject:InvokeASCallback("_root", "setTitleLang", lang)
  end
  GameObjectGacha:CheckTutorial()
end
function GameObjectGacha:OnEraseFromGameState()
  self:UnloadFlashObject()
  if QuestTutorialSlot:IsActive() then
    QuestTutorialSlot:SetFinish(true)
    GameUIBarLeft:CheckNeedShowQuestTutorial()
    if immanentversion == 2 then
      GameStateMainPlanet:SetStoryWhenFocusGain({51215})
    elseif immanentversion == 1 then
      if not TutorialQuestManager.QuestTutorialSign:IsFinished() then
        TutorialQuestManager.QuestTutorialSign:SetActive(true, true)
      end
      GameStateMainPlanet:SetStoryWhenFocusGain({1100044})
    end
  end
  GameObjectGacha.isOnAward = nil
end
function GameObjectGacha:CheckDownloadImage()
  if (ext.crc32.crc32("data2/LAZY_LOAD_map_star_01.png") == "" or ext.crc32.crc32("data2/LAZY_LOAD_map_star_01.png") == nil) and not AutoUpdateInBackground:IsHeroFileUpdatedToServer("data2/LAZY_LOAD_map_star_01.png") then
    self:GetFlashObject():ReplaceTexture("LAZY_LOAD_map_star_01.png", "territorial_map_bg.png")
  end
end
function GameObjectGacha.OnRollSupplyChange()
  local flashObject = GameObjectGacha:GetFlashObject()
  if flashObject then
    GameObjectGacha:UpdateRollTimes()
  end
end
function GameObjectGacha:OnInitGame()
  GameGlobalData:RegisterDataChangeCallback("laba_supply", self.OnRollSupplyChange)
end
function GameObjectGacha:UpdateRollTimes()
  DebugOut("UpdateRollTimes")
  local rollSupply = GameGlobalData:GetData("laba_supply")
  self:GetFlashObject():InvokeASCallback("_root", "setRollTimes", rollSupply.current)
  GameObjectGacha:GetFlashObject():InvokeASCallback("_root", "lua2fs_setNextBingoTime", currentTimes, currentSlotNumber)
end
function GameObjectGacha:OnFSCommand(cmd, arg)
  if cmd == "onClose" then
    if arg == "all" then
      GameStateGacha:QuitState()
    end
  elseif cmd == "ShowHelp" then
    GameUIWDStuff:Show(GameUIWDStuff.menuType.menu_type_help)
    GameUIWDStuff:SetHelpText(GameLoader:GetGameText("LC_MENU_LABA_DESC"))
  elseif cmd == "onStartRollItems" then
    currentCoinNumber = tonumber(arg)
    local requestParam = {
      coin = tonumber(currentCoinCost),
      power = tonumber(currentCoinNumber)
    }
    DebugOut("currentSlotNumber", currentSlotNumber)
    DebugOut("currentCoinNumber", currentCoinNumber)
    DebugTable(requestParam)
    NetMessageMgr:SendMsg(NetAPIList.laba_req.Code, requestParam, self.NetCallbackRollItems, false, nil)
  elseif cmd == "currentSlotNumber" then
    currentSlotNumber = tonumber(arg)
  elseif cmd == "coinCost" then
    DebugOut("coinCost", arg)
    currentCoinCost = tonumber(arg)
  elseif cmd == "onClaimAward" then
    local requestParam = {type = 1}
    if currentCoinNumber == 0 then
      requestParam.type = 0
    end
    NetMessageMgr:SendMsg(NetAPIList.laba_award_req.Code, requestParam, self.NetCallbackClaimAward, true, nil)
  elseif cmd == "onReroll" then
    NetMessageMgr:SendMsg(NetAPIList.laba_rate_reroll_req.Code, nil, self.NetCallbackReroll, true, nil)
  elseif cmd == "needUpdateAwardResult" then
    self:UpdateResultData()
  elseif cmd == "showNoRewardTip" then
    local function callBack()
      self:GetFlashObject():InvokeASCallback("_root", "resetAwardItemTable")
    end
    local text = GameLoader:GetGameText("LC_MENU_LABA_NO_REWARD_ALERT")
    GameTip:Show(text, nil, callBack, nil)
  elseif cmd == "shareStoryCheckclicked" then
    if not GameObjectGacha.mCurrentRate then
      return
    end
    if self.mShareStoryChecked[GameObjectGacha.mCurrentRate] then
      self.mShareStoryChecked[GameObjectGacha.mCurrentRate] = false
    else
      self.mShareStoryChecked[GameObjectGacha.mCurrentRate] = true
    end
    local flash_obj = self:GetFlashObject()
    if flash_obj then
      flash_obj:InvokeASCallback("_root", "setCheckBox", self.mShareStoryChecked[GameObjectGacha.mCurrentRate])
    end
  end
end
function GameObjectGacha:ShowItemsBox()
  DebugOut("ShowTimesBox", currentTimes)
  self:GetFlashObject():InvokeASCallback("_root", "showItemsBox", currentTimes)
end
function GameObjectGacha:ShowTimesBox()
  DebugOut("ShowTimesBox", currentTimes)
  self:GetFlashObject():InvokeASCallback("_root", "showTimesBox", currentTimes)
end
function GameObjectGacha:GetIconFrame(name)
  if name == nil then
    return
  end
  local s, e = string.find(name, "item_")
  local frame = name
  if s then
    local number = string.sub(name, e + 1)
    local fullFileName = DynamicResDownloader:GetFullName(number .. ".png", DynamicResDownloader.resType.PIC)
    local localPath = "data2/" .. fullFileName
    DebugOut("GameObjectGacha localPath = ", localPath)
    if DynamicResDownloader:IsDynamicStuff(tonumber(number), DynamicResDownloader.resType.PIC) and not DynamicResDownloader:IfResExsit(localPath) then
      frame = "temp"
      GameHelper:AddDownloadPathForGameItem(number, nil, nil)
    else
      ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
    end
  end
  return frame
end
function GameObjectGacha:SetAwardData()
  DebugOut("GameObjectGacha:SetAwardData")
  DebugTable(self.UserGachaData)
  local awardData = ""
  local pos1frame, pos2frame, pos3frame
  if awardsPool ~= nil then
    pos1frame = GameObjectGacha:GetIconFrame(awardsPool[self.UserGachaData.pos1])
    pos2frame = GameObjectGacha:GetIconFrame(awardsPool[self.UserGachaData.pos2])
    pos3frame = GameObjectGacha:GetIconFrame(awardsPool[self.UserGachaData.pos3])
  else
    pos1frame = GameDataAccessHelper:GetLabaAwardType(self.UserGachaData.pos1)
    pos2frame = GameDataAccessHelper:GetLabaAwardType(self.UserGachaData.pos2)
    pos3frame = GameDataAccessHelper:GetLabaAwardType(self.UserGachaData.pos3)
  end
  awardData = pos1frame .. "," .. pos2frame .. "," .. pos3frame
  local noAnimation = false
  if pos1frame == "slot_empty" and pos2frame == "slot_empty" and pos3frame == "slot_empty" then
    noAnimation = true
  end
  DebugOut("noAnimation", noAnimation)
  self:GetFlashObject():InvokeASCallback("_root", "setResultAwardData", awardData, noAnimation)
  self:GetFlashObject():InvokeASCallback("_root", "setAwardResultRate2", self.UserGachaData.rate)
  self:UpdateResultData()
  GameObjectGacha.isOnAward = true
end
function GameObjectGacha:UpdateResultData()
  DebugOut("UpdateResultData")
  DebugTable(self.UserGachaData)
  local resultDataTable = {}
  for i = 1, 3 do
    local awardData = self.UserGachaData.awards[i]
    if awardData then
      local count = GameHelper:GetAwardCount(awardData.item_type, awardData.number, awardData.no)
      local icon = ""
      if awardData.item_type == "slot_empty" then
        icon = "slot_empty"
      else
        icon = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(awardData, nil, GameObjectGacha.DynamicResDowloadFinished)
      end
      table.insert(resultDataTable, icon)
      table.insert(resultDataTable, tostring(count))
    else
      table.insert(resultDataTable, "empty")
      table.insert(resultDataTable, "empty")
    end
  end
  local resultDataString = table.concat(resultDataTable, ",")
  DebugOutPutTable(resultDataTable, "resultDataTable")
  local isHaveReward = true
  if self.UserGachaData and #self.UserGachaData.awards <= 0 then
    DebugOut("isHaveReward", isHaveReward)
    isHaveReward = false
  end
  local showRerollBtn = true
  if (resultDataTable[1] == "slot_empty" or resultDataTable[1] == nil or resultDataTable[1] == "empty") and (resultDataTable[3] == "slot_empty" or resultDataTable[3] == nil or resultDataTable[3] == "empty") and (resultDataTable[5] == "slot_empty" or resultDataTable[5] == nil or resultDataTable[5] == "empty") then
    showRerollBtn = false
    DebugOut("all empty", showRerollBtn)
  elseif resultDataTable[1] ~= nil and string.find(resultDataTable[1], "item") == 1 then
    if checkIsResource(resultDataTable[3]) or checkIsResource(resultDataTable[5]) then
      showRerollBtn = true
    else
      showRerollBtn = false
    end
  elseif resultDataTable[3] ~= nil and string.find(resultDataTable[3], "item") == 1 then
    if checkIsResource(resultDataTable[1]) or checkIsResource(resultDataTable[5]) then
      showRerollBtn = true
    else
      showRerollBtn = false
    end
  elseif resultDataTable[5] ~= nil and string.find(resultDataTable[5], "item") == 1 then
    if checkIsResource(resultDataTable[1]) or checkIsResource(resultDataTable[3]) then
      showRerollBtn = true
    else
      showRerollBtn = false
    end
  else
    DebugOut("unpredicated case")
  end
  local isShareEnabled = false
  local shareChecked = false
  if FacebookGraphStorySharer:IsStoryEnabled(FacebookGraphStorySharer.StoryType.STORY_TYPE_LABA_AWARD) and self.UserGachaData and (self.UserGachaData.rate == 2 or self.UserGachaData.rate == 4 or self.UserGachaData.rate == 8) then
    isShareEnabled = true
    GameObjectGacha.mCurrentRate = self.UserGachaData.rate
    shareChecked = self.mShareStoryChecked[GameObjectGacha.mCurrentRate]
    DebugOut("isShareEnabled should be true")
  end
  if self.UserGachaData then
    GameObjectGacha.mCurrentRate = self.UserGachaData.rate
  end
  self:GetFlashObject():InvokeASCallback("_root", "setResultData", resultDataString, isHaveReward, showRerollBtn, isShareEnabled, shareChecked)
end
function checkIsResource(nameStr)
  DebugOut("checkIsResource", nameStr)
  if not nameStr then
    return false
  end
  if nameStr == "empty" then
    return false
  end
  if string.find(nameStr, "money") == 1 or string.find(nameStr, "technique") == 1 or string.find(nameStr, "prestige") == 1 or string.find(nameStr, "credit") == 1 or string.find(nameStr, "laba") == 1 then
    DebugOut("find resource", nameStr)
    return true
  end
  return false
end
function GameObjectGacha:SetAwardRate(rate)
  DebugOut("GameObjectGacha:SetAwardRate", rate)
  local lang = "en"
  if GameSettingData and GameSettingData.Save_Lang then
    lang = GameSettingData.Save_Lang
    if string.find(lang, "ru") == 1 then
      lang = "ru"
    elseif GameUtils:IsChinese() and GameUtils:IsSimplifiedChinese() then
      lang = "zh"
    end
  end
  self:GetFlashObject():InvokeASCallback("_root", "setAwardResultRate", rate, lang)
end
function GameObjectGacha:OnClaimAwardOver()
  GameObjectGacha.isOnAward = nil
  self:GetFlashObject():InvokeASCallback("_root", "onClaimAwardOver")
end
function GameObjectGacha:OnReroll()
  local isShareEnabled = false
  if FacebookGraphStorySharer:IsStoryEnabled(FacebookGraphStorySharer.StoryType.STORY_TYPE_LABA_AWARD) and self.UserGachaData and (self.UserGachaData.rate == 2 or self.UserGachaData.rate == 4 or self.UserGachaData.rate == 8) then
    isShareEnabled = true
    GameObjectGacha.mCurrentRate = self.UserGachaData.rate
  end
  if self.UserGachaData then
    GameObjectGacha.mCurrentRate = self.UserGachaData.rate
  end
  self:GetFlashObject():InvokeASCallback("_root", "onReroll", isShareEnabled, self.mShareStoryChecked[GameObjectGacha.mCurrentRate])
end
function GameObjectGacha:ShowPullDownAnimation()
  self:GetFlashObject():InvokeASCallback("_root", "showPullDownAnimation")
end
function GameObjectGacha:ResetPullDownHandle()
  self:GetFlashObject():InvokeASCallback("_root", "resetPullDownHandle")
end
function GameObjectGacha:Update(dt)
  local flashObject = self:GetFlashObject()
  if flashObject then
    flashObject:InvokeASCallback("_root", "onUpdateFrame", dt)
    flashObject:InvokeASCallback("_root", "updateRollTextPos", dt)
    flashObject:Update(dt)
    flashObject:InvokeASCallback("_root", "lua2fs_UpdateFrame", dt)
  end
end
function GameObjectGacha.NetCallbackRollItems(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.laba_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      GameObjectGacha:ResetPullDownHandle()
      if content.code == 136 then
        DebugOut("NetCallbackRollItems \229\184\129\228\184\141\229\164\159\228\186\134")
      end
      GameObjectGacha:GetFlashObject():InvokeASCallback("_root", "coinNotEnough")
    else
      GameObjectGacha:SetAwardData()
    end
    return true
  end
  return false
end
function GameObjectGacha.NetCallbackClaimAward(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.laba_award_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    else
      local countryStr = ext.GetDeviceCountry and ext.GetDeviceCountry() or "unknow"
      if (GameObjectGacha.mCurrentRate == 2 or GameObjectGacha.mCurrentRate == 4 or GameObjectGacha.mCurrentRate == 8) and GameObjectGacha.mShareStoryChecked[GameObjectGacha.mCurrentRate] then
        if FacebookGraphStorySharer:IsStoryEnabled(FacebookGraphStorySharer.StoryType.STORY_TYPE_LABA_AWARD) then
          local extraInfo = {}
          extraInfo.subType = GameObjectGacha.mCurrentRate
          FacebookGraphStorySharer:ShareStory(FacebookGraphStorySharer.StoryType.STORY_TYPE_LABA_AWARD, extraInfo)
        end
      elseif FacebookGraphStorySharer:IsStoryEnabled(FacebookGraphStorySharer.StoryType.STORY_TYPE_LABA_AWARD) then
      end
      GameObjectGacha:OnClaimAwardOver()
    end
    return true
  end
  return false
end
function GameObjectGacha.NetCallbackReroll(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.laba_rate_reroll_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    else
      DebugOut("NetCallbackReroll")
      GameObjectGacha:SetAwardRate(GameObjectGacha.UserGachaData.rate)
      GameObjectGacha:OnReroll()
    end
    return true
  end
  return false
end
function GameObjectGacha.GachaInfoNotifyHandler(content)
  DebugOut("GachaInfoNotifyHandler")
  DebugTable(content)
  GameObjectGacha.UserGachaData = content
end
if AutoUpdate.isAndroidDevice then
  function GameObjectGacha.OnAndroidBack()
    if GameObjectGacha.isOnAward then
    else
      GameObjectGacha:GetFlashObject():InvokeASCallback("_root", "itemsGachaBoxMoveOut")
    end
  end
end
function GameObjectGacha.GachaTimesChangeHandler(content)
  DebugOut("GachaTimesChangeHandler")
  DebugTable(content)
  currentTimes = content.times
  currentSlotNumber = content.area
  if GameObjectGacha:GetFlashObject() then
    GameObjectGacha:GetFlashObject():InvokeASCallback("_root", "lua2fs_setNextBingoTime", content.times, content.area)
  end
end
function GameObjectGacha.LabaAwardPoolNtf(content)
  DebugOut("LabaAwardPoolNtf")
  DebugTable(content)
  awardsPool = content.info
  for _, item in pairs(content.item) do
    GameHelper:GetAwardTypeIconFrameNameSupportDynamic(item, nil, nil)
  end
end
function GameObjectGacha.DynamicResDowloadFinished()
  DebugOut("DynamicResDowloadFinished")
  GameObjectGacha:GetFlashObject():InvokeASCallback("_root", "lua2fs_refreshIcon")
end
function GameObjectGacha:CheckTutorial(...)
  if QuestTutorialSlot:IsActive() then
    GameUICommonDialog:PlayStory({100068}, nil)
  end
end
