local GameStateAlliance = GameStateManager.GameStateAlliance
local GameUIAllianceList = LuaObjectManager:GetLuaObject("GameUIAllianceList")
local GameUIAllianceInfo = LuaObjectManager:GetLuaObject("GameUIAllianceInfo")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
function GameUIAllianceList:OnAddToGameState(game_state)
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  local alliances_info = {}
  alliances_info.data = {}
  alliances_info.total = 0
  alliances_info.index = 0
  alliances_info.perpage = 20
  alliances_info.keyword = ""
  self.alliances_info = alliances_info
  self:UpdateAlliancesList(0)
  self.is_active = true
end
function GameUIAllianceList:OnEraseFromGameState(game_state)
  self.is_active = nil
  self:UnloadFlashObject()
end
function GameUIAllianceList:IsActive()
  return self.is_active
end
function GameUIAllianceList:InitFlashObject()
  self:GetFlashObject():InvokeASCallback("_root", "initAlliancesList")
end
function GameUIAllianceList:OnFSCommand(cmd, arg)
  if cmd == "update_alliance_item" then
    self:UpdateAllianceItem(tonumber(arg))
    return
  elseif cmd == "select_alliance" then
    local index_alliance = tonumber(arg)
    self:SelectAlliance(index_alliance)
    return
  elseif cmd == "search_alliance" then
    GameUIAllianceList.alliances_info.keyword = arg
    GameUIAllianceList:UpdateAlliancesList(0)
    return
  elseif cmd == "return" then
    GameStateAlliance:EraseObject(self)
    return
  end
  GameTextEdit:fscommand(GameUIAllianceList:GetFlashObject(), cmd, arg)
end
function GameUIAllianceList:UpdateAlliancesList(page_index)
  local alliances_info = self.alliances_info
  if page_index and page_index >= 0 then
    local function netCallProcess()
      local packet = {
        keyword = alliances_info.keyword,
        index = alliances_info.index,
        size = alliances_info.perpage
      }
      NetMessageMgr:SendMsg(NetAPIList.alliances_req.Code, packet, self.NetCallbackAlliances, true)
    end
    netCallProcess()
  else
    self:GetFlashObject():InvokeASCallback("_root", "setAlliancesList", #self.alliances_info.data, self.alliances_info.total)
  end
end
function GameUIAllianceList:UpdateAllianceItem(index)
  local alliances_info = self.alliances_info
  local data_alliance = alliances_info.data[index]
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "setAllianceItem", index, data_alliance.rank, data_alliance.name, data_alliance.level_info.level, GameUtils:GetUserDisplayName(data_alliance.creator_name), data_alliance.members_count, data_alliance.members_max)
end
function GameUIAllianceList:SelectAlliance(index)
  local data_alliance = GameUIAllianceList.alliances_info.data[index]
  if data_alliance then
    self:GetFlashObject():InvokeASCallback("_root", "selectAllianceItem", index)
    GameUIAllianceInfo:SetAllianceData(data_alliance)
    GameUIAllianceInfo:SetApplyStatus(-1)
    GameStateAlliance:AddObject(GameUIAllianceInfo)
  end
end
function GameUIAllianceList:Update(dt)
  local flash_obj = self:GetFlashObject()
  flash_obj:Update(dt)
  flash_obj:InvokeASCallback("_root", "onUpdateFrame", dt)
end
function GameUIAllianceList.NetCallbackAlliances(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.alliances_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  if msgtype == NetAPIList.alliances_ack.Code then
    local alliances_info = GameUIAllianceList.alliances_info
    alliances_info.data = content.alliances
    alliances_info.total = content.total
    alliances_info.index = content.index
    table.sort(alliances_info.data, function(a, b)
      return a.rank < b.rank
    end)
    GameUIAllianceList:UpdateAlliancesList()
    return true
  end
  return false
end
if AutoUpdate.isAndroidDevice then
  function GameUIAllianceList.OnAndroidBack()
    GameStateAlliance:EraseObject(GameUIAllianceList)
  end
end
