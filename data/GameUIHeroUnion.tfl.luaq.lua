local GameStateRecruit = GameStateManager.GameStateRecruit
local GameUIHeroUnion = LuaObjectManager:GetLuaObject("GameUIHeroUnion")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local GameUIRecruitTopbar = LuaObjectManager:GetLuaObject("GameUIRecruitTopbar")
local GameSetting = LuaObjectManager:GetLuaObject("GameSetting")
GameUIHeroUnion.defaultImg = "primus.png"
GameUIHeroUnion.heroUnionData = nil
GameUIHeroUnion.collectData = nil
GameUIHeroUnion.rankData = nil
GameUIHeroUnion.baseRankData = nil
GameUIHeroUnion.curType = -1
GameUIHeroUnion.isAllServer = false
GameUIHeroUnion.rankType = 0
GameUIHeroUnion.fleetShowInfo = nil
GameUIHeroUnion.curCollectList = nil
GameUIHeroUnion.selectedIndex = -1
GameUIHeroUnion.curShowUI = "main"
GameUIHeroUnion.Title = {
  [1] = "LC_MENU_CELESTIAL_PORTAL_COLLECTION_1",
  [2] = "LC_MENU_CELESTIAL_PORTAL_COLLECTION_2",
  [3] = "LC_MENU_CELESTIAL_PORTAL_COLLECTION_3",
  [4] = "LC_MENU_CELESTIAL_PORTAL_COLLECTION_4"
}
GameUIHeroUnion.Desc = {
  [1] = "LC_MENU_COMMANDER_COLLECTION_BUFF_1",
  [2] = "LC_MENU_COMMANDER_COLLECTION_BUFF_2",
  [3] = "LC_MENU_COMMANDER_COLLECTION_BUFF_2",
  [4] = "LC_MENU_COMMANDER_COLLECTION_BUFF_2"
}
function GameUIHeroUnion:OnInitGame()
end
function GameUIHeroUnion:OnAddToGameState()
  DebugOut("GameUIHeroUnion:OnAddToGameState")
  GameUIHeroUnion.curShowUI = "main"
  self:LoadFlashObject()
  self:GetFlashObject():InvokeASCallback("_root", "initText", GameLoader:GetGameText("LC_MENU_Level"))
end
function GameUIHeroUnion:OnEraseFromGameState()
  self.heroUnionData = nil
  GameUIHeroUnion.collectData = nil
  GameUIHeroUnion.fleetShowInfo = nil
  GameUIHeroUnion.selectedIndex = -1
  self:UnloadFlashObject()
  collectgarbage("collect")
end
function GameUIHeroUnion:OnFSCommand(cmd, arg)
  if cmd == "close" then
  elseif cmd == "update_achievement_item" then
    self:updateAchievementListItem(tonumber(arg))
  elseif cmd == "ShowAchievement" then
    local curtype, islock = unpack(LuaUtils:string_split(arg, ":"))
    islock = tonumber(islock) == 1
    self.curType = tonumber(curtype)
    self:reqAchievement(self.curType, islock)
  elseif cmd == "Show_Rank" then
    self:reqCollectRank(true, 0)
  elseif cmd == "ShowDetail" then
    local itemType, fleetid = unpack(LuaUtils:string_split(arg, ":"))
    if itemType == "item" then
      local item = self.curRewards[index].item_reward
      item.cnt = 1
      ItemBox:showItemBox("Item", item, tonumber(item.number), 320, 480, 200, 200, "", nil, "", nil)
    else
      local fleetID = fleetid
      ItemBox:ShowCommanderDetail2(fleetID, nil, nil)
    end
  elseif cmd == "update_attribute_item" then
    self:updateAttributeItemList(tonumber(arg))
  elseif cmd == "select_tab" then
    GameUIHeroUnion.selectedIndex = -1
    GameUIHeroUnion:SetCollectList(tonumber(arg))
  elseif cmd == "update_collect_attribute_item" then
    self:updateCollectAttributeList(tonumber(arg))
  elseif cmd == "update_collect_item" then
    local itemIndex, tab = unpack(LuaUtils:string_split(arg, ":"))
    self:updateCollectItemList(tonumber(itemIndex), tonumber(tab))
  elseif cmd == "Selected_Collect" then
    self:SelectedCollectItem(tonumber(arg))
  elseif cmd == "recruit" then
    GameUIRecruitTopbar:SetVisible(true)
    GameStateRecruit:SelectTab("recruitNormal")
    GameUIHeroUnion.selectedIndex = -1
  elseif cmd == "update_rank_item" then
    self:updateRankItem(tonumber(arg))
  elseif cmd == "select_rank_tab" then
    GameUIHeroUnion:reqCollectRank(tonumber(arg) == 2, 0)
  elseif cmd == "rank_type" then
    GameUIHeroUnion:reqCollectRank(GameUIHeroUnion.isAllServer, tonumber(arg))
  elseif cmd == "close_rank" or cmd == "close_collect_all" then
    GameUIRecruitTopbar:SetVisible(true)
    GameUIHeroUnion.selectedIndex = -1
    GameUIHeroUnion.curShowUI = "main"
  end
end
function GameUIHeroUnion:updateAchievementListItem(itemIndex)
  local itemData = GameUIHeroUnion.heroUnionData.achievements[itemIndex]
  if itemData then
    local localPath = "data2/" .. DynamicResDownloader:GetFullName(itemData.imgBg, DynamicResDownloader.resType.WELCOME_PIC)
    if ext.crc32.crc32(localPath) ~= "" then
      itemData.isExist = true
    else
      local extendInfo = {}
      extendInfo.img = itemData.imgBg
      extendInfo.itemIndex = itemIndex
      DynamicResDownloader:AddDynamicRes(itemData.imgBg, DynamicResDownloader.resType.WELCOME_PIC, extendInfo, GameUIHeroUnion.DynamicBackgroundCallback)
      itemData.isExist = false
    end
    self:GetFlashObject():InvokeASCallback("_root", "updateAchievementItem", itemIndex, itemData)
  end
end
function GameUIHeroUnion.DynamicBackgroundCallback(extinfo)
  if GameUIHeroUnion:GetFlashObject() then
    GameUIHeroUnion:GetFlashObject():InvokeASCallback("_root", "setAchievementBg", extinfo.itemIndex, extinfo.img)
  end
end
function GameUIHeroUnion:updateAttributeItemList(itemIndex)
  local itemData = GameUIHeroUnion.heroUnionData.attribute[itemIndex]
  if itemData then
    self:GetFlashObject():InvokeASCallback("_root", "updateAttributeItem", itemIndex, itemData)
  end
end
function GameUIHeroUnion:UpdateGameData()
  if not self.heroUnionData then
    GameUIHeroUnion.collectData = nil
    NetMessageMgr:SendMsg(NetAPIList.hero_union_req.Code, nil, self.heroUnionCallBack, true, nil)
  else
    self:RefreshMain()
  end
end
function GameUIHeroUnion.heroUnionCallBack(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.hero_union_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  if msgtype == NetAPIList.hero_union_ack.Code then
    DebugTable(content)
    GameUIHeroUnion:GenerateData(content)
    return true
  end
  return false
end
function GameUIHeroUnion:GenerateData(content)
  local data = {}
  data.attribute = {}
  data.achievements = {}
  for k, v in ipairs(content.achievement or {}) do
    local item = {}
    item.isLock = v.lock
    item.lockText1 = string.gsub(GameLoader:GetGameText("LC_MENU_CELESTIAL_PORTAL_UNLOCK_CONDITION_1"), "<level>", v.need_level)
    local str = ""
    if v.need_type > 0 then
      str = GameLoader:GetGameText(GameUIHeroUnion.Title[v.need_type])
      str = string.gsub(GameLoader:GetGameText("LC_MENU_CELESTIAL_PORTAL_UNLOCK_CONDITION_2"), "<mission>", str)
      str = string.gsub(str, "<number1>", v.need_count)
    end
    item.lockText2 = str
    item.lockTitle = GameLoader:GetGameText("LC_MENU_CELESTIAL_PORTAL_UNLOCK_CONDITION")
    item.name = GameLoader:GetGameText(GameUIHeroUnion.Title[v.type])
    item.type = v.type
    item.imgBg = v.img
    item.isExist = true
    item.desc = GameLoader:GetGameText(GameUIHeroUnion.Desc[v.type])
    item.curCount = v.count
    item.maxCount = v.max_count
    item.loadText = GameLoader:GetGameText("LC_MENU_NEWS_DOWNLOAD_CHAR")
    data.achievements[#data.achievements + 1] = item
  end
  for k, v in ipairs(content.attribute or {}) do
    local item = {}
    item.attributeName = GameLoader:GetGameText("LC_MENU_Equip_param_" .. v.id)
    local valueStr = ""
    if v.is_percent then
      valueStr = "+" .. v.value / 10 .. "%"
    else
      valueStr = "+" .. GameUtils.numberConversion(v.value)
    end
    item.attributeValue = valueStr
    item.id = v.id
    item.value = v.value
    item.isPercent = v.is_percent
    data.attribute[#data.attribute + 1] = item
  end
  GameUIHeroUnion.heroUnionData = data
  GameUIHeroUnion:RefreshMain()
end
function GameUIHeroUnion:RefreshMain()
  self:GetFlashObject():InvokeASCallback("_root", "InitAchievement", #GameUIHeroUnion.heroUnionData.achievements)
  self:GetFlashObject():InvokeASCallback("_root", "InitAttribute", #GameUIHeroUnion.heroUnionData.attribute)
  self:GetFlashObject():unlockInput()
end
function GameUIHeroUnion:reqAchievement(itemType, islock)
  GameUIHeroUnion.curType = itemType
  GameUIHeroUnion.curIsLock = islock
  if GameUIHeroUnion.collectData == nil then
    NetMessageMgr:SendMsg(NetAPIList.hero_collect_req.Code, nil, self.heroCollectCallBack, true, nil)
  else
    GameUIHeroUnion:SetCollectList(1)
    GameUIHeroUnion:SetCollectAttributeList()
  end
end
function GameUIHeroUnion.heroCollectCallBack(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.hero_collect_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  if msgtype == NetAPIList.hero_collect_ack.Code then
    GameUIHeroUnion.fleetShowInfo = content.fleet_info
    GameUIHeroUnion:GenerateCollectData(content)
    GameUIHeroUnion:SetCollectList(1)
    GameUIHeroUnion:SetCollectAttributeList()
    return true
  end
  return false
end
function GameUIHeroUnion:GenerateCollectData(content)
  local data = {}
  data.ownList = {}
  data.unOwnList = {}
  for k, v in ipairs(content.collect_info or {}) do
    local item = {}
    item.attributs = {}
    item.fleets = {}
    item.type = v.type
    item.collects = v.collects
    for k1, v1 in ipairs(v.attributes or {}) do
      local attribut = {}
      attribut.attributeName = GameLoader:GetGameText("LC_MENU_Equip_param_" .. v1.id)
      local valueStr = ""
      if v1.is_percent then
        valueStr = "+" .. v1.value / 10 .. "%"
      else
        valueStr = "+" .. v1.value
      end
      attribut.attributeValue = valueStr
      attribut.id = v1.id
      attribut.value = v1.value
      attribut.isPercent = v1.is_percent
      item.attributs[#item.attributs + 1] = attribut
    end
    if v.is_own then
      item.own = v.is_own
      item.id = #data.ownList + 1
      data.ownList[#data.ownList + 1] = item
    else
      item.id = #data.unOwnList + 1
      item.own = v.is_own
      data.unOwnList[#data.unOwnList + 1] = item
    end
  end
  GameUIHeroUnion.collectData = data
end
function GameUIHeroUnion:GetFleetInfo(fleetid)
  for k, v in pairs(GameUIHeroUnion.fleetShowInfo or {}) do
    if v.fleet_id == fleetid then
      return v
    end
  end
  return nil
end
function GameUIHeroUnion.DownloadGrowUpRankImageCallback(extInfo)
  if GameUIHeroUnion:GetFlashObject() then
    DebugOut("GameUIHeroUnion.DownloadGrowUpRankImageCallback")
    DebugTable(extInfo)
  end
end
function GameUIHeroUnion:SetCollectList(tab)
  local curdata
  if tab == 1 then
    curdata = GameUIHeroUnion.collectData.ownList
  else
    curdata = GameUIHeroUnion.collectData.unOwnList
  end
  local list = {}
  local function _gendata(curdata, locked)
    for k, v in ipairs(curdata) do
      if v.type == self.curType then
        if #v.fleets == 0 then
          for k2, v2 in ipairs(v.collects or {}) do
            local fleet = {}
            fleet.isOwn = v2.is_own
            local fleetInfo = GameUIHeroUnion:GetFleetInfo(v2.fleet_id)
            fleet.name = FleetDataAccessHelper:GetFleetLevelDisplayName(fleetInfo.name, fleetInfo.fleet_id, fleetInfo.color, 0)
            fleet.vesselsType = FleetDataAccessHelper:GetFleetVesselsFrameByVessel(fleetInfo.vessels)
            fleet.vesselsName = FleetDataAccessHelper:GetFleetVesselsTypeStringByVessel(fleetInfo.vessels)
            fleet.ship = GameDataAccessHelper:GetCommanderShipByDownloadState(fleetInfo.ship)
            fleet.avatar = FleetDataAccessHelper:GetFleetAvatarFrameByAvatar(fleetInfo.avatar, fleetInfo.fleet_id)
            fleet.color = FleetDataAccessHelper:GetFleetColorFrameByColor(fleetInfo.color)
            fleet.spellFrame = FleetDataAccessHelper:GetFleetSpellFrameBySpell(fleetInfo.spell_id)
            fleet.rankFrame = GameUtils:GetFleetRankFrame(fleetInfo.rank, GameUIHeroUnion.DownloadGrowUpRankImageCallback)
            fleet.fleet_id = fleetInfo.fleet_id
            v.fleets[#v.fleets + 1] = fleet
          end
        end
        v.locked = locked
        list[#list + 1] = v
      end
    end
  end
  if GameUIHeroUnion.curIsLock then
    _gendata(GameUIHeroUnion.collectData.ownList, true)
    _gendata(GameUIHeroUnion.collectData.unOwnList, false)
  else
    _gendata(curdata, false)
  end
  GameUIHeroUnion.curCollectList = list
  local title = GameLoader:GetGameText(GameUIHeroUnion.Title[self.curType])
  self:GetFlashObject():InvokeASCallback("_root", "SetCollectData", tab, math.ceil(#list / 2), self.curType, title)
  GameUIRecruitTopbar:SetVisible(false)
  GameUIHeroUnion.curShowUI = "collect"
end
function GameUIHeroUnion:SetCollectAttributeList()
  self:GetFlashObject():InvokeASCallback("_root", "SetCollectAttribute", #GameUIHeroUnion.heroUnionData.attribute, GameUIHeroUnion.curIsLock)
end
function GameUIHeroUnion:updateCollectItemList(itemIndex, tab)
  local curdata = GameUIHeroUnion.curCollectList
  local startIndex = (itemIndex - 1) * 2 + 1
  local endIndex = itemIndex * 2
  local data = {}
  for i = startIndex, endIndex do
    local item = curdata[i]
    if item then
      item.isSel = false
      if tab == 1 and i == GameUIHeroUnion.selectedIndex then
        item.isSel = true
      end
    end
    data[#data + 1] = item
  end
  local strtitle = GameLoader:GetGameText(GameUIHeroUnion.Title[GameUIHeroUnion.curType])
  local strlocked = GameLoader:GetGameText("LC_MENU_CELESTIAL_PORTAL_HERO_GATHER_TIPS")
  strlocked = string.format(strlocked, strtitle)
  self:GetFlashObject():InvokeASCallback("_root", "updateCollectItem", itemIndex, data, self.curType, strlocked)
end
function GameUIHeroUnion:updateCollectAttributeList(itemIndex)
  local itemData = GameUIHeroUnion.heroUnionData.attribute[itemIndex]
  if itemData then
    self:GetFlashObject():InvokeASCallback("_root", "updateCollectAttributeItem", itemIndex, itemData)
  end
end
function GameUIHeroUnion:SelectedCollectItem(index)
  GameUIHeroUnion.selectedIndex = index
  local itemData = GameUIHeroUnion.collectData.ownList[index]
  if itemData then
    local data = {}
    for k, v in ipairs(itemData.attributs or {}) do
      local baseItem, index = self:GetBaseAttributeById(v.id)
      local item = {}
      item.id = index
      local str = baseItem.value - v.value
      local valueStr = ""
      if v.isPercent then
        valueStr = "<font color='#d9ffff'>" .. str / 10 .. "%" .. "</font>" .. "+" .. v.value .. "%"
      else
        valueStr = "<font color='#d9ffff'>" .. GameUtils.numberConversion(str) .. "</font>" .. "+" .. GameUtils.numberConversion(v.value)
      end
      item.valueStr = valueStr
      item.attributeName = baseItem.attributeName
      data[#data + 1] = item
    end
    self:GetFlashObject():InvokeASCallback("_root", "SetAttributeChange", data)
  end
end
function GameUIHeroUnion:GetBaseAttributeById(id)
  for k, v in pairs(GameUIHeroUnion.heroUnionData.attribute or {}) do
    if v.id == id then
      return v, k
    end
  end
  return nil
end
function GameUIHeroUnion:reqCollectRank(isAllServer, types)
  local param = {}
  param.is_all_server = isAllServer
  param.type = types
  GameUIHeroUnion.isAllServer = isAllServer
  GameUIHeroUnion.rankType = types
  NetMessageMgr:SendMsg(NetAPIList.hero_collect_rank_req.Code, param, self.collectRankCallBack, true, nil)
end
function GameUIHeroUnion.collectRankCallBack(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.hero_collect_rank_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  if msgtype == NetAPIList.hero_collect_rank_ack.Code then
    GameUIHeroUnion:GenerateRankData(content)
    GameUIHeroUnion:SetRankData()
    return true
  end
  return false
end
function GameUIHeroUnion:GenerateRankData(content)
  GameUIHeroUnion.rankData = content.players
  GameUIHeroUnion.baseRankData = content.base_info
end
function GameUIHeroUnion:SetRankData()
  self:GetFlashObject():InvokeASCallback("_root", "setRankData", GameUIHeroUnion.isAllServer, GameUIHeroUnion.rankType, #GameUIHeroUnion.rankData, GameUIHeroUnion.baseRankData)
  GameUIRecruitTopbar:SetVisible(false)
  GameUIHeroUnion.curShowUI = "rank"
end
function GameUIHeroUnion:updateRankItem(itemIndex)
  local itemData = GameUIHeroUnion.rankData[itemIndex]
  if itemData then
    self:GetFlashObject():InvokeASCallback("_root", "updateRankItem", itemIndex, itemData)
  end
end
function GameUIHeroUnion:SetVisible(mcpath, isVisible)
  mcpath = mcpath or -1
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "lua2fs_setVisible", mcpath, isVisible)
  end
end
function GameUIHeroUnion:Update(dt)
  local flashObj = GameUIHeroUnion:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "onUpdateFrame", dt)
    flashObj:Update(dt)
  end
end
if AutoUpdate.isAndroidDevice then
  function GameUIHeroUnion.OnAndroidBack()
    if GameUIHeroUnion.curShowUI == "main" then
      GameStateRecruit:Quit()
    elseif GameUIHeroUnion.curShowUI == "rank" then
      GameUIHeroUnion:GetFlashObject():InvokeASCallback("_root", "HideRank")
    elseif GameUIHeroUnion.curShowUI == "collect" then
      GameUIHeroUnion:GetFlashObject():InvokeASCallback("_root", "HideCollect")
    end
  end
end
