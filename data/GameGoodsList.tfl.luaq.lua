local GameGoodsList = LuaObjectManager:GetLuaObject("GameGoodsList")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameVip = LuaObjectManager:GetLuaObject("GameVip")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameSetting = LuaObjectManager:GetLuaObject("GameSetting")
local GameItemBag = LuaObjectManager:GetLuaObject("GameItemBag")
local GameGoodsBuyBox = LuaObjectManager:GetLuaObject("GameGoodsBuyBox")
local GameStateStore = GameStateManager.GameStateStore
local DynamicResDownloader = DynamicResDownloader
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local GameVipDetailInfoPanel = LuaObjectManager:GetLuaObject("GameVipDetailInfoPanel")
local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
local GameStateInstance = GameStateManager.GameStateInstance
GameGoodsList.initList = false
GameGoodsList.goodsList = nil
GameGoodsList.receiveGoodsListTime = -1
GameGoodsList.receiveGoodsLevel = -1
GameGoodsList.receiveGoodsVIPLv = -1
GameGoodsList.nowStoreType = -1
GameGoodsList.STORE_TYPE_MYSTERY_EXPEDITION = 6
local ifnotNeedToBack = false
function GameGoodsList:OnInitGame()
  DebugStore("GameGoodsList:OnInitGame()")
end
local _serverIDIsRes = function(currency)
  return currency >= 99000001
end
function GameGoodsList:Init()
  GameGlobalData:RegisterDataChangeCallback("resource", self.RefreshResource)
  GameGlobalData:RegisterDataChangeCallback("item_count", self.RefreshResource)
  GameGlobalData:RegisterDataChangeCallback("modules_status", self.ModuleStatusChange)
end
function GameGoodsList:OnAddToGameState()
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  self:Init()
  self:MoveIn()
  self:RefreshResource()
  self:ModuleStatusChange()
  GameGoodsList:EntryStore(self.enterStoreType or 1)
  self.enterStoreType = nil
  GameTimer:Add(self._OnTimerTick, 1000)
  GameStateManager:RegisterGameResumeNotification(GameGoodsList.RequestRefreshData, nil)
end
function GameGoodsList:OnEraseFromGameState()
  GameStateManager:RemoveGameResumeNotification(GameGoodsList.RequestRefreshData)
  self:UnloadFlashObject()
  GameGoodsList.param_PriceOffStore = nil
  GameGlobalData:RemoveDataChangeCallback("resource", self.RefreshResource)
  GameGlobalData:RemoveDataChangeCallback("item_count", self.RefreshResource)
  GameGlobalData:RemoveDataChangeCallback("modules_status", self.ModuleStatusChange)
end
function GameGoodsList:SetEnterStoreType(store_type)
  self.enterStoreType = store_type
end
function GameGoodsList:EntryStore(store_type)
  local refresh_type = 0
  if self.MysteryData then
    refresh_type = self.MysteryData.refresh_type
  end
  DebugOut("EntryStore:", store_type, refresh_type)
  self:GetFlashObject():InvokeASCallback("_root", "ActivateStore", store_type, refresh_type)
  self.nowStoreType = store_type
  self.RequestRefreshData(false)
end
function GameGoodsList:serverIDToIconFrame(currency)
  if currency == 99000001 then
    return "money"
  elseif currency == 99000002 then
    return "credit"
  elseif currency == 99000003 then
    return "technique"
  elseif currency == 99000004 then
    return "silver"
  elseif currency == 99000005 then
    return "crystal"
  elseif currency == 99000006 then
    return "expedition"
  else
    local gameItem = {
      number = currency,
      item_type = "item",
      no = 1
    }
    local strframe = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(gameItem)
    return strframe
  end
end
function GameGoodsList.bannerDownloaderCallback(extInfo)
  if DynamicResDownloader:IfResExsit(extInfo.localPath) and GameGoodsList:GetFlashObject() then
    GameGoodsList:GetFlashObject():ReplaceTexture("store_adv.png", extInfo.pngName)
    GameGoodsList:GetFlashObject():InvokeASCallback("_root", "SetAdText", extInfo.title, extInfo.content)
  end
  return true
end
function GameGoodsList:RefreshResource()
  local resource = GameGlobalData:GetData("resource")
  local flash_obj = GameGoodsList:GetFlashObject()
  local goods_list = GameGoodsList.goodsList or {}
  local pre_token_type = GameGoodsList:serverIDToIconFrame(tonumber(goods_list.normal) or 99000002)
  local post_token_type = "credit"
  local pre_token_number, post_token_number
  local currency = goods_list.currency or 99000002
  post_token_type = GameGoodsList:serverIDToIconFrame(currency)
  if pre_token_type == "credit" and post_token_type == "credit" then
    pre_token_type = "money"
  end
  if tonumber(resource[pre_token_type]) == nil then
    pre_token_type = "money"
  end
  pre_token_number = tonumber(resource[pre_token_type])
  assert(pre_token_number, "must have number " .. pre_token_type)
  if not _serverIDIsRes(currency) then
    post_token_number = GameGlobalData:GetItemCount(currency)
  else
    post_token_number = tonumber(resource[post_token_type])
  end
  assert(post_token_number, "must have number " .. currency)
  DebugOut("RefreshResource:", pre_token_type, ",", post_token_type, ",", pre_token_number, ",", post_token_number)
  if flash_obj then
    flash_obj:InvokeASCallback("_root", "RefreshResource", GameUtils.numberConversion(resource.money), GameUtils.numberConversion(resource.credit), pre_token_type, GameUtils.numberConversion(pre_token_number), post_token_type, GameUtils.numberConversion(post_token_number))
  end
end
function GameGoodsList:ModuleStatusChange()
  local modules = GameGlobalData:GetData("modules_status").modules
  local flash_obj = GameGoodsList:GetFlashObject()
  for k, v in pairs(modules or {}) do
    if flash_obj and v.name == "price_off" then
      flash_obj:InvokeASCallback("_root", "SetShop3Enabled", v.status)
      return
    end
  end
end
function GameGoodsList:IsInstanceStore()
  return GameGoodsList.nowStoreType == GameGoodsList.STORE_TYPE_MYSTERY_EXPEDITION
end
function GameGoodsList:InitShop3Tab()
  local flash_obj = self:GetFlashObject()
  if GameGoodsList:IsInstanceStore() then
    local param = {
      [1] = {},
      [2] = {}
    }
    flash_obj:InvokeASCallback("_root", "SetInstanceStoreTab", param)
    return
  end
  if GameGoodsList.param_PriceOffStore and flash_obj then
    local param = {
      [1] = {},
      [2] = {}
    }
    local info = GameGoodsList.param_PriceOffStore.store_infos
    param[1].isEnabled = info[1].status
    param[1].left_time = info[1].left_seconds
    param[1].has_left_time = param[1].left_time > 0
    param[1].shoptype = info[1].group + 2
    param[1].count = GameGoodsList.new_status3
    param[1].tabtxt = GameLoader:GetGameText("LC_MENU_SALES_STORE_NAME_1")
    param[2].isEnabled = info[2].status
    param[2].left_time = info[2].left_seconds
    param[2].has_left_time = param[2].left_time > 0
    param[2].shoptype = info[2].group + 2
    param[2].count = GameGoodsList.new_status4
    param[2].tabtxt = GameLoader:GetGameText("LC_MENU_SALES_STORE_NAME_2")
    if not param[1].isEnabled and param[2].isEnabled then
      local t = param[1]
      param[1] = param[2]
      param[2] = t
    end
    GameGoodsList.asShop3Param = param
    flash_obj:InvokeASCallback("_root", "SetShop3Tab", param)
  end
end
function GameGoodsList:MoveIn()
  local function checkfunc()
    if GameGoodsList.param_PriceOffStore or GameGoodsList:IsInstanceStore() then
      GameGoodsList:InitShop3Tab()
      local flash_obj = self:GetFlashObject()
      if not flash_obj then
        return
      end
      local title = GameLoader:GetGameText("LC_MENU_SHOP_CHAR_TITLE")
      if GameGoodsList:IsInstanceStore() then
        title = GameLoader:GetGameText("LC_MENU_EXPEDITION_SHOP_TITLE")
      end
      flash_obj:InvokeASCallback("_root", "SetTitle", title)
      flash_obj:InvokeASCallback("_root", "animationMoveIn")
      if not GameGoodsList:IsInstanceStore() then
        local name = GameLoader:GetGameText("LC_MENU_MYSTERY_SHOP_BUTTON")
        local name2 = GameGoodsList.asShop3Param[1].tabtxt
        local name3 = GameGoodsList.asShop3Param[2].tabtxt
        flash_obj:InvokeASCallback("_root", "SetShop2Name", name, name2, name3)
      end
      return nil
    end
    return 1000
  end
  GameTimer:Add(checkfunc, 1)
end
function GameGoodsList:MoveOut()
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "animationMoveOut")
end
function GameGoodsList:Update(dt)
  self:GetFlashObject():InvokeASCallback("_root", "OnUpdate", dt)
  self:GetFlashObject():Update(dt)
end
if AutoUpdate.isAndroidDevice then
  function GameGoodsList.OnAndroidBack()
    if GameStateStore.CurItemDetail then
      GameStateStore:HideBuyConfirmWin()
    else
      GameGoodsList:OnFSCommand("close_menu")
    end
  end
end
function GameGoodsList:OnFSCommand(cmd, arg)
  DebugStore("store command: ", cmd)
  GameTextEdit:fscommand(self:GetFlashObject(), cmd, arg)
  if cmd == "close_menu" then
    if GameGoodsList:IsInstanceStore() then
      GameStateManager:GetCurrentGameState():EraseObject(self)
    elseif GameStateStore:GetPreState() == GameStateInstance then
      GameStateInstance:EnterInstance()
    else
      GameStateManager:SetCurrentGameState(GameStateStore:GetPreState())
    end
  elseif cmd == "buyCredit" then
    GameVip:showVip(false)
  elseif cmd == "needUpdateShopItem" then
    self:UpdateGoodsItem(arg)
  elseif cmd == "shopItemReleased" then
    local itemIndex = tonumber(arg)
    self:QueryBuyItem(itemIndex)
  elseif cmd == "EnterStore" then
    self:EntryStore(tonumber(arg))
  elseif cmd == "MysteryRefresh" then
    self:RequestStoreRefreshPrice()
  elseif cmd == "MysteryRefresh_new" then
    self:ClickRefreshMysteryBtn()
  elseif cmd == "ShowMysteryStroeClose" then
    local GameTip = LuaObjectManager:GetLuaObject("GameTip")
    GameTip:Show(GameLoader:GetGameText("LC_MENU_MYSTERY_SHOP_CLOSE_INFO"))
  elseif cmd == "BuyItem" then
    local itemKey, num = unpack(LuaUtils:string_split(arg, ":"))
    GameGoodsList:OnBuyShop3Item(tonumber(itemKey), tonumber(num), false)
  elseif cmd == "showshop3info" then
    GameTip:Show(arg)
  elseif cmd == "onClick_limitItem" then
    local titem = LuaUtils:deserializeTable(arg)
    GameGoodsList:onClick_limitItem(titem)
  elseif cmd == "onQuickBuyNumberChanged" then
    local itemKey, itemID, num = unpack(LuaUtils:string_split(arg, ":"))
    GameGoodsList.param_query_12 = {
      nItemKey = tonumber(itemKey),
      strItemID = itemID,
      nCount = tonumber(num)
    }
    GameGoodsList.RequestQuickBuyCast(tonumber(itemID), tonumber(num), false)
  elseif cmd == "onShopBuy" then
    local itemKey, num = unpack(LuaUtils:string_split(arg, ":"))
    local discountPaper = GameGlobalData:GetCuponChoices()
    local itemDetail
    if GameGoodsList.goodsList then
      for k, v in ipairs(GameGoodsList.goodsList.items) do
        if v.id == tonumber(itemKey) then
          itemDetail = v
          break
        end
      end
    end
    if GameGoodsList.nowStoreType == 2 and itemDetail ~= nil and itemDetail.refresh_buy_limit > 0 and discountPaper ~= nil and #discountPaper > 0 then
      itemDetail.buyCount = num
      GameStateStore:ShowBuyConfirmWin(itemDetail)
      return
    end
    GameGoodsList:OnBuyItem_12(tonumber(itemKey), tonumber(num))
  elseif cmd == "onShopIcon" then
    local nItemid = tonumber(arg)
    local itemDetail
    for k, v in ipairs(GameGoodsList.goodsList.items) do
      if v.id == nItemid then
        itemDetail = v
        break
      end
    end
    assert(itemDetail, tostring(nItemid))
    ItemBox.showItemParam.hideCost = true
    ItemBox:ShowGameItem(itemDetail.item)
  end
end
function GameGoodsList:onClick_limitItem(titem)
  ItemBox.showItemParam.hideCost = true
  ItemBox:ShowGameItem(titem)
end
function GameGoodsList:ClickRefreshMysteryBtn()
  local data = self.MysteryData
  if not data then
    self.doRefreshMysteryStore()
    return
  end
  if tonumber(data.cur_free_times) > 0 or data.item_num and data.item_cost and tonumber(data.item_num) >= tonumber(data.item_cost) then
    self.doRefreshMysteryStore()
  else
    local enablePayRemind = GameSettingData.EnablePayRemind
    if enablePayRemind == nil then
      enablePayRemind = true
    end
    if enablePayRemind then
      local msgBoxContent = ""
      if GameGoodsList:IsInstanceStore() then
        msgBoxContent = string.format(GameLoader:GetGameText("LC_MENU_EXPEDITION_SHOP_BUY_INFO"), data.currency_cost)
      else
        msgBoxContent = string.format(GameLoader:GetGameText("LC_MENU_MYSTERY_SHOP_BUY_INFO"), data.credit_cost)
      end
      local TitleInfo = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
      GameUIGlobalScreen:ShowMessageBox(GameUIGlobalScreen.MessageBoxType_YesNo, TitleInfo, msgBoxContent, GameGoodsList.doRefreshMysteryStore, nil)
    else
      self.doRefreshMysteryStore()
    end
  end
end
function GameGoodsList:RequestStoreRefreshPrice()
  local store_refresh_price_content = {
    price_type = "refresh_store_cost",
    type = 0
  }
  if GameGoodsList:IsInstanceStore() then
    store_refresh_price_content.price_type = "expedition_refresh_store_cost"
  end
  local function callback(msgType, content)
    if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.price_req.Code then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      return true
    elseif msgType == NetAPIList.price_ack.Code then
      DebugStoreTable(content)
      local msgBoxContent = string.format(GameLoader:GetGameText("LC_MENU_MYSTERY_SHOP_BUY_INFO"), content.price)
      if GameGoodsList:IsInstanceStore() then
        msgBoxContent = ""
        msgBoxContent = string.format(GameLoader:GetGameText("LC_MENU_EXPEDITION_SHOP_BUY_INFO"), content.price)
      end
      local TitleInfo = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
      GameUIGlobalScreen:ShowMessageBox(GameUIGlobalScreen.MessageBoxType_YesNo, TitleInfo, msgBoxContent, GameGoodsList.doRefreshMysteryStore, nil)
      return true
    end
    return false
  end
  NetMessageMgr:SendMsg(NetAPIList.price_req.Code, store_refresh_price_content, callback, true, nil)
end
function GameGoodsList.doRefreshMysteryStore()
  local function callback(msgType, content)
    DebugOut("doRefreshMysteryStore:", msgType)
    DebugTable(content)
    if msgType == NetAPIList.common_ack.Code and (content.api == NetAPIList.stores_refresh_req.Code or content.api == NetAPIList.expedition_stores_refresh_req.Code) then
      if content.code == 5 then
        local GameTip = LuaObjectManager:GetLuaObject("GameTip")
        GameTip:Show(GameLoader:GetGameText("LC_MENU_MYSTERY_SHOP_CLOSE_INFO"))
        return true
      elseif content.code ~= 0 then
        GameUIGlobalScreen:ShowAlert("error", content.code, nil)
        return true
      end
      return true
    elseif msgType == NetAPIList.stores_refresh_ack.Code or msgType == NetAPIList.expedition_stores_refresh_ack.Code then
      GameGoodsList.RequestRefreshData()
      return true
    end
    return false
  end
  if GameGoodsList:IsInstanceStore() then
    NetMessageMgr:SendMsg(NetAPIList.expedition_stores_refresh_req.Code, nil, callback, true, nil)
  else
    NetMessageMgr:SendMsg(NetAPIList.stores_refresh_req.Code, nil, callback, true, nil)
  end
end
function GameGoodsList:QueryBuyItem(itemIndex)
  DebugStore("item released: ", itemIndex)
  if GameGoodsList.goodsList ~= nil then
    GameStateStore:ShowBuyConfirmWin(GameGoodsList.goodsList.items[itemIndex])
  else
    DebugOut("GameGoodsList.goodsList == nil")
    local GameTip = LuaObjectManager:GetLuaObject("GameTip")
    GameTip:Show(GameLoader:GetGameText("LC_MENU_MYSTERY_SHOP_CLOSE_INFO"))
  end
end
function GameGoodsList:TempText(id)
  if id == 1022 or id == 2518 or id == 2320 or id == 2320 or id == 2519 or id == 2311 or id == 2312 or id == 2313 or id == 2314 or id == 2315 then
    return true
  else
    return false
  end
end
local function _setImage(data, idx, imgname)
  local localPath = "data2/" .. DynamicResDownloader:GetFullName(imgname, DynamicResDownloader.resType.WELCOME_PIC)
  data.imgURL = imgname
  if ext.crc32.crc32(localPath) ~= "" then
  else
    local extendInfo = {}
    extendInfo.img = imgname
    extendInfo.itemIndex = idx
    local function _dynamicCallback(extinfo)
      if GameGoodsList:GetFlashObject() then
        GameGoodsList:GetFlashObject():InvokeASCallback("_root", "setShop3ItemBG", extinfo.itemIndex, extinfo.img)
      end
    end
    DynamicResDownloader:AddDynamicRes(imgname, DynamicResDownloader.resType.WELCOME_PIC, extendInfo, _dynamicCallback)
  end
end
function GameGoodsList:UpdateShop3Item(goodsList, Id)
  local itemKey = tonumber(Id)
  local data = {}
  data.itemList = {}
  local itemData = goodsList[itemKey]
  if itemData then
    DebugOut("asdas:", itemKey)
    DebugTable(itemData)
    data.content = itemData
    data.buyBtnText = GameLoader:GetGameText("LC_MENU_LIMIT_PACKAGE_BUY_BUTTON")
    data.titleName = GameHelper:GetAwardNameText(itemData.item.item_type, itemData.item.number)
    data.iconframe = GameGoodsList:serverIDToIconFrame(data.content.price_type)
    if itemData.pics[1] then
      _setImage(data, Id, itemData.pics[1])
    else
      error("must piccs " .. LuaUtils:serializeTable(itemData))
    end
    data.infotxt = GameLoader:GetGameText("LC_ITEM_ITEM_DESC_" .. itemData.item.number)
    if not data.infotxt or #data.infotxt == 0 then
      data.infotxt = "LC_ITEM_ITEM_DESC_" .. itemData.item.number
    end
    if data.content.day_buy_left ~= -1 then
      data.saleTipText = GameLoader:GetGameText("LC_MENU_LIMIT_PACKAGE_TODAY_NUMBER") .. data.content.day_buy_left
    elseif data.content.all_buy_left ~= -1 then
      data.saleTipText = GameLoader:GetGameText("LC_MENU_LIMIT_PACKAGE_REMAIN_NUMBER") .. data.content.all_buy_left
    else
      data.saleTipText = ""
    end
    for k, v in ipairs(itemData.must_items) do
      local item = {}
      local info = GameHelper:GetItemInfo(v)
      item.name = info.name
      item.itemType = info.icon_frame
      item.style = "p1"
      item.countText = "x" .. GameUtils.numberConversion(info.cnt)
      item.stritem = LuaUtils:serializeTable(v)
      item.iconpic = info.icon_pic
      table.insert(data.itemList, item)
    end
    if 0 < LuaUtils:table_size(itemData.maybe_items or {}) then
      local title = {}
      title.name = GameLoader:GetGameText("LC_MENU_LIMIT_PACKAGE_PROBABILITY_GET")
      title.style = "p2"
      table.insert(data.itemList, title)
      for i, j in ipairs(itemData.maybe_items) do
        local info = GameHelper:GetItemInfo(j)
        item.name = info.name
        item.itemType = info.icon_frame
        item.style = "p1"
        item.countText = "<font size='14'>" .. "max" .. "</font>" .. info.cnt
        item.stritem = LuaUtils:serializeTable(j)
        item.iconpic = info.icon_pic
        table.insert(data.itemList, item)
      end
    end
    self:UpdateShop3ItemCDTime(itemKey, itemData)
  else
    DebugOut("ItemKey %d is nil", itemKey)
  end
  self:GetFlashObject():InvokeASCallback("_root", "setShop3Item", itemKey, data)
end
function GameGoodsList:UpdateGoodsItem(Id)
  if GameGoodsList.nowStoreType == 3 then
    self:UpdateShop3Item(GameGoodsList.goodsList3, Id)
    return
  end
  if GameGoodsList.nowStoreType == 4 then
    self:UpdateShop3Item(GameGoodsList.goodsList4, Id)
    return
  end
  local itemKey = tonumber(Id)
  local name, ori_cost, discount, real_cost, price_type, day_buy_left, all_buy_left, frame, limitInfo = "", "", "", "", "", "", "", "", ""
  local lock_state, corner_state, if_time_left = "", "", ""
  local v1, v2
  local vipInfo = GameGlobalData:GetData("vipinfo")
  local levelInfo = GameGlobalData:GetData("levelinfo")
  local curLevel = GameVipDetailInfoPanel:GetVipLevel()
  local refresh_buy_limit = ""
  local str_refresh_buy_limit = ""
  local item_num = ""
  local isQuickBuy = ""
  local minCount = ""
  local maxCount = ""
  local itemID = ""
  local canBuy = ""
  for k, v in ipairs(GameGoodsList.goodsList.items) do
    if math.ceil(k / 2) == itemKey then
      DebugStore("print goods items")
      if curLevel < v.vip_limit or v.level_limit > levelInfo.level then
        local lockInfo = ""
        if v.level_limit > levelInfo.level then
          lockInfo = lockInfo .. GameLoader:GetGameText("LC_MENU_Level") .. v.level_limit
          if curLevel < v.vip_limit then
            lockInfo = lockInfo .. " "
          end
        end
        if curLevel < v.vip_limit then
          lockInfo = lockInfo .. "VIP " .. v.vip_limit
        end
        limitInfo = limitInfo .. lockInfo .. "\001"
      else
        limitInfo = limitInfo .. " " .. "\001"
      end
      local curItem = v.item
      name = name .. GameHelper:GetAwardNameText(curItem.item_type, curItem.number) .. "\001"
      if curLevel >= v.vip_limit and levelInfo.level >= v.level_limit then
        lock_state = lock_state .. "0\001"
      else
        lock_state = lock_state .. "1\001"
      end
      itemID = itemID .. v.id .. "\001"
      ori_cost = ori_cost .. v.price .. "\001"
      discount = discount .. 100 - v.discount .. "\001"
      real_cost = real_cost .. math.ceil(v.price * v.discount / 100) .. "\001"
      price_type = price_type .. v.price_type .. "\001"
      local strday = "-1"
      if v.day_buy_left ~= -1 then
        strday = GameLoader:GetGameText("LC_MENU_SHOP_DAY_BUY_TIME_CHAR") .. v.day_buy_left
      end
      local strall = "-1"
      if v.all_buy_left ~= -1 then
        strall = GameLoader:GetGameText("LC_MENU_SHOP_BUY_TIME_CHAR") .. v.all_buy_left
      end
      day_buy_left = day_buy_left .. strday .. "\001"
      all_buy_left = all_buy_left .. strall .. "\001"
      if not v.enableBuy then
        canBuy = canBuy .. "0\001"
      else
        canBuy = canBuy .. "1\001"
      end
      refresh_buy_limit = refresh_buy_limit .. v.refresh_buy_limit .. "\001"
      local _desc = GameLoader:GetGameText("LC_MENU_SHOP_BUY_TIME_CHAR") .. v.refresh_buy_limit
      str_refresh_buy_limit = str_refresh_buy_limit .. _desc .. "\001"
      isQuickBuy = isQuickBuy .. v.quick_buy .. "\001"
      local itemIndex = 1
      if v1 == nil then
        v1 = v
        itemIndex = 1
      else
        v2 = v
        itemIndex = 2
      end
      local countv
      for _, _cv in ipairs(GameGoodsList.goodsList.buy_count_infos) do
        if _cv.id == v.id then
          countv = _cv
          break
        end
      end
      if v.quick_buy ~= 0 then
        assert(countv, "notfindid " .. v.id)
      end
      if countv then
        if countv.count_ack.max_count == 0 then
          minCount = minCount .. 1 .. "\001"
          maxCount = maxCount .. 1 .. "\001"
        else
          minCount = minCount .. countv.count_ack.count .. "\001"
          maxCount = maxCount .. countv.count_ack.max_count .. "\001"
        end
      else
        minCount = minCount .. 1 .. "\001"
        maxCount = maxCount .. 10 .. "\001"
      end
      local extInfo = {}
      extInfo.itemKey = itemKey
      extInfo.index = itemIndex
      extInfo.frameName = GameHelper:GetAwardTypeIconFrameName(curItem.item_type, curItem.number, curItem.no)
      local count = GameHelper:GetAwardCount(curItem.item_type, curItem.number, curItem.no)
      item_num = item_num .. "X" .. count .. "\001"
      local icon = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(curItem, extInfo, GameGoodsList.donamicDownloadFinishCallback)
      frame = frame .. icon .. "\001"
      if v.state == 1 then
        corner_state = corner_state .. "1\001"
      else
        corner_state = corner_state .. "0\001"
      end
      if v.time_left == -1 then
        if_time_left = if_time_left .. "0\001"
      else
        if_time_left = if_time_left .. "1\001"
      end
    end
  end
  local leftOverDesc = GameLoader:GetGameText("LC_MENU_WELFAREE_PURCHASED_BUTTON")
  local th_token_type = ""
  local param = {}
  param.itemKey = itemKey
  param.lock_state = lock_state
  param.nameString = name
  param.oriCost = ori_cost
  param.discount = discount
  param.realCost = real_cost
  param.priceType = price_type
  param.day_buy_left = day_buy_left
  param.all_buy_left = all_buy_left
  param.corner_state = corner_state
  param.if_time_left = if_time_left
  param.frame_name = frame
  param.limitInfo = limitInfo
  param.th_token_type = th_token_type
  param.refresh_buy_limit = refresh_buy_limit
  param.str_refresh_buy_limit = str_refresh_buy_limit
  param.leftOverDesc = leftOverDesc
  param.item_num = item_num
  param.isQuickBuy = isQuickBuy
  param.minCount = minCount
  param.maxCount = maxCount
  param.itemID = itemID
  param.canBuy = canBuy
  param.strBuyBtn = GameLoader:GetGameText("LC_MENU_LIMIT_PACKAGE_BUY_BUTTON")
  if GameGoodsList.nowStoreType == 2 or GameGoodsList:IsInstanceStore() then
    param.has_refresh = true
  else
    param.has_refresh = false
  end
  self:GetFlashObject():InvokeASCallback("_root", "setItem_new", param)
  GameGoodsList:UpdateItemCDTime(itemKey, v1, v2)
end
function GameGoodsList:RefreshGoodsList()
  DebugStore("sdfs__")
  DebugStoreTable(self.goodsList)
  if ifnotNeedToBack then
    ifnotNeedToBack = false
    self:GetFlashObject():InvokeASCallback("_root", "needUpdateItemsInfo")
    return
  end
  self:GetFlashObject():InvokeASCallback("_root", "clearListItem")
  if GameGoodsList.goodsList ~= nil then
    local function _additem(goodslist)
      local itemCount = #goodslist
      self:GetFlashObject():InvokeASCallback("_root", "initListItem", "store_normal")
      for i = 1, itemCount do
        self:GetFlashObject():InvokeASCallback("_root", "addListItem", i)
      end
      self:GetFlashObject():InvokeASCallback("_root", "SetArrowInitVisible")
    end
    if GameGoodsList.nowStoreType == 3 then
      _additem(GameGoodsList.goodsList3)
    elseif GameGoodsList.nowStoreType == 4 then
      _additem(GameGoodsList.goodsList4)
    else
      local itemCount = math.ceil(#GameGoodsList.goodsList.items / 2)
      self:GetFlashObject():InvokeASCallback("_root", "initListItem", "shop_item")
      for i = 1, itemCount do
        self:GetFlashObject():InvokeASCallback("_root", "addListItem", i)
      end
      self:GetFlashObject():InvokeASCallback("_root", "SetArrowInitVisible")
      self:UpdateMysteryCDTime()
    end
  end
end
function GameGoodsList:GetRefreshLeftBuyTimes()
  local item_id
  if GameStateStore.CurItemDetail ~= nil then
    item_id = GameStateStore.CurItemDetail.id
  end
  if not item_id then
    return
  end
  for k, v in pairs(GameGoodsList.goodsList.items) do
    if v.id == item_id then
      return v.refresh_buy_limit
    end
  end
  return nil
end
function GameGoodsList:ReduceBuyTime(count)
  if GameStateStore.CurItemDetail ~= nil then
    DebugStore("GameGoodsList:ReduceBuyTime()")
    DebugStoreTable(GameStateStore.CurItemDetail)
    for k, v in ipairs(GameGoodsList.goodsList.items) do
      if v.id == GameStateStore.CurItemDetail.id then
        if v.day_buy_left ~= -1 then
          v.day_buy_left = v.day_buy_left - count
          if v.day_buy_left < 0 then
            v.day_buy_left = 0
          end
        end
        if v.all_buy_left ~= -1 then
          v.all_buy_left = v.all_buy_left - count
          if 0 > v.all_buy_left then
            v.all_buy_left = 0
          end
        end
        if v.refresh_buy_limit ~= -1 then
          v.refresh_buy_limit = v.refresh_buy_limit - count
          if 0 > v.refresh_buy_limit then
            v.refresh_buy_limit = 0
          end
        end
        local key = math.ceil(k / 2)
        self:UpdateGoodsItem(key)
        return
      end
    end
  end
end
function GameGoodsList:UpdateItemCDTime(itemKey, v1, v2)
  local timeString1 = ""
  local timeString2 = ""
  local index = 0
  if v1 ~= nil and v1.enableBuy and v1.time_left ~= -1 then
    local _lefttime = v1.time_left - (os.time() - GameGoodsList.receiveGoodsListTime)
    if _lefttime < 0 then
      _lefttime = 0
    end
    timeString1 = GameUtils:formatTimeString(_lefttime)
  end
  if v2 ~= nil and v2.enableBuy and v2.time_left ~= -1 then
    local _lefttime = v2.time_left - (os.time() - GameGoodsList.receiveGoodsListTime)
    if _lefttime < 0 then
      _lefttime = 0
    end
    timeString2 = GameUtils:formatTimeString(_lefttime)
  end
  if timeString1 ~= "" or timeString2 ~= "" then
    self:GetFlashObject():InvokeASCallback("_root", "setItemCDTime", itemKey, timeString1, timeString2)
  end
end
function GameGoodsList:UpdateShop3ItemCDTime(itemKey, item)
  local timeString = ""
  local _lefttime = 0
  if item and item.time_left ~= -1 then
    _lefttime = item.time_left - (os.time() - GameGoodsList.receiveGoodsListTime)
    if _lefttime < 0 then
      _lefttime = 0
    end
    timeString = GameUtils:formatTimeString(_lefttime)
    if _lefttime < 3600 then
      timeString = "<font color='#FF4A4A'>" .. timeString .. "</font>"
    end
  end
  if timeString ~= "" then
    self:GetFlashObject():InvokeASCallback("_root", "setShop3ItemCDTime", itemKey, timeString, _lefttime)
  end
end
function GameGoodsList:UpdateMysteryCDTime_old()
  if GameGoodsList.nowStoreType ~= 2 and not GameGoodsList:IsInstanceStore() then
    return
  end
  if not GameGoodsList.goodsList or not GameGoodsList.goodsList.time then
    return
  end
  local cd_time = GameGoodsList.goodsList.time - (os.time() - GameGoodsList.receiveGoodsListTime)
  if cd_time < 1 and GameGoodsList.goodsList.time > 0 then
    DebugOut("RequestStoreGoodsList old")
    GameGoodsList.goodsList.time = nil
    if self:IsInstanceStore() then
      self:RequestStoreGoodsList(GameGoodsList.STORE_TYPE_MYSTERY_EXPEDITION, true)
    else
      self:RequestStoreGoodsList(2, true)
    end
    return
  end
  local str_cd_time = GameUtils:formatTimeString(cd_time)
  self:GetFlashObject():InvokeASCallback("_root", "setMysteryCDTime", str_cd_time)
end
function GameGoodsList._OnTimerTick()
  if GameStateStore:IsObjectInState(GameGoodsList) or GameStateManager:GetCurrentGameState():IsObjectInState(GameGoodsList) and GameGoodsList:IsInstanceStore() then
    if GameGoodsList.goodsList ~= nil then
      if GameGoodsList.nowStoreType == 3 or GameGoodsList.nowStoreType == 4 then
        for k, v in ipairs(GameGoodsList.goodsList.items) do
          GameGoodsList:UpdateShop3ItemCDTime(k, v)
        end
      else
        local innerIndex = 0
        local itemKey, v1, v2
        for k, v in ipairs(GameGoodsList.goodsList.items) do
          itemKey = math.ceil(k / 2)
          if v1 == nil then
            v1 = v
          else
            v2 = v
          end
          if v1 ~= nil and v2 ~= nil then
            GameGoodsList:UpdateItemCDTime(itemKey, v1, v2)
            v1, v2 = nil, nil
          end
        end
        if v1 ~= nil or v2 ~= nil then
          GameGoodsList:UpdateItemCDTime(itemKey, v1, v2)
          v1, v2 = nil, nil
        end
        GameGoodsList:UpdateMysteryCDTime()
      end
      return 1000
    end
    return 1000
  end
  return nil
end
function GameGoodsList:UpdateMysteryCDTime()
  local data = self.MysteryData
  if not data then
    self:UpdateMysteryCDTime_old()
    return
  end
  if tonumber(data.refresh_type) == 0 then
    self:UpdateMysteryCDTime_old()
  elseif tonumber(data.refresh_type) == 1 then
    self:UpdateMysteryCDTime_new()
  end
end
function GameGoodsList.RequestRefreshData()
  DebugOut("RequestRefreshData:", GameGlobalData.isLogin)
  if GameGlobalData.isLogin then
    if GameGoodsList:IsInstanceStore() then
      NetMessageMgr:SendMsg(NetAPIList.expedition_refresh_type_req.Code, nil, GameGoodsList.RefreshMysteryCallback, true, nil)
    else
      NetMessageMgr:SendMsg(NetAPIList.refresh_type_req.Code, nil, GameGoodsList.RefreshMysteryCallback, true, nil)
    end
  end
end
function GameGoodsList:UpdateMysteryCDTime_new()
  local data = self.MysteryData
  local flash_obj = self:GetFlashObject()
  if not data or not flash_obj or self.nowStoreType ~= 2 and not GameGoodsList:IsInstanceStore() then
    return
  end
  local cd_desc, refresh_type, refresh_icon, refresh_cost
  local cd_time = 0
  if GameGoodsList.goodsList and GameGoodsList.goodsList.time then
    cd_time = GameGoodsList.goodsList.time - (os.time() - GameGoodsList.receiveGoodsListTime)
    if 0 < tonumber(GameGoodsList.goodsList.time) and cd_time < 0 and tonumber(data.cur_free_times) < tonumber(data.max_free_times) then
      GameGoodsList.goodsList.time = nil
      DebugOut("RefreshMysteryReq new")
      self.RequestRefreshData()
      return
    end
  end
  local str_cd_time = ""
  if cd_time > 0 and data.cur_free_times ~= data.max_free_times then
    str_cd_time = GameUtils:formatTimeString(cd_time)
  end
  cd_desc = GameLoader:GetGameText("LC_MENU_FREE_REFRESH_CHAR")
  cd_desc = cd_desc .. tostring(data.cur_free_times) .. "/" .. tostring(data.max_free_times) .. " " .. str_cd_time
  if 0 < tonumber(data.cur_free_times) then
    refresh_type = "free"
  elseif data.item_num and data.item_cost and tonumber(data.item_num) >= tonumber(data.item_cost) then
    refresh_type = "item"
    refresh_icon = "item_" .. tostring(data.item_type)
    refresh_cost = data.item_cost
  elseif data.currency_cost then
    refresh_type = "credit"
    refresh_cost = data.currency_cost
  else
    refresh_type = "credit"
    refresh_cost = data.credit_cost
  end
  flash_obj:InvokeASCallback("_root", "SetNewRefreshData", cd_desc, refresh_type, refresh_icon, refresh_cost, self:IsInstanceStore())
end
function GameGoodsList:RequestStoreGoodsList(store_type, force_request)
  local vipInfo = GameGlobalData:GetData("vipinfo")
  local curLevel = GameVipDetailInfoPanel:GetVipLevel()
  local levelInfo = GameGlobalData:GetData("levelinfo")
  if not ifnotNeedToBack then
    print("RefreshGoodsList = nil")
    GameGoodsList.goodsList = nil
    GameGoodsList.receiveGoodsListTime = -1
    GameGoodsList.nowStoreType = store_type
  end
  if GameGoodsList.goodsList == nil then
    print("GameGoodsList.goodsList == nil ")
    local stores_req_content = {
      store_id = store_type or 1,
      locale = GameSettingData.Save_Lang
    }
    NetMessageMgr:SendMsg(NetAPIList.stores_req.Code, stores_req_content, GameGoodsList.GoodsListCallback, true, nil)
  else
    print("GameGoodsList.goodsList ~= nil ")
    local item
    for k, v in pairs(GameGoodsList.goodsList.items) do
      if v.id == GameGoodsList.Itemid then
        item = v
      end
    end
    if item then
      if item.day_buy_left ~= -1 then
        item.day_buy_left = item.day_buy_left - GameGoodsList.buyItemCount
        if item.day_buy_left <= 0 then
          item.day_buy_left = 0
          item.enableBuy = false
        end
      end
      if item.all_buy_left ~= -1 then
        item.all_buy_left = item.all_buy_left - GameGoodsList.buyItemCount
        if 0 >= item.all_buy_left then
          item.all_buy_left = 0
          item.enableBuy = false
        end
      end
      if store_type == 2 and item.refresh_buy_limit ~= -1 then
        item.refresh_buy_limit = item.refresh_buy_limit - GameGoodsList.buyItemCount
        if 0 >= item.refresh_buy_limit then
          item.refresh_buy_limit = 0
          item.enableBuy = false
        end
      end
      print("iteminfo", item.id, item.day_buy_left, item.all_buy_left)
    end
    GameGoodsList.sortItem()
    self:RefreshResource()
    self:RefreshGoodsList()
  end
end
function GameGoodsList:setMysteryShopEnable(isEnable)
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "setStore2Enable", isEnable)
  end
end
function GameGoodsList.RefreshMysteryCallback(msgType, content)
  print("RefreshMysteryCallback")
  DebugOut("!!GameGoodsList.RefreshMysteryCallback:", msgType)
  DebugTable(content)
  if msgType == NetAPIList.refresh_type_ack.Code or msgType == NetAPIList.expedition_refresh_type_ack.Code then
    print("RefreshMysteryCallback_refresh_type_ack", msgType, content)
    GameGoodsList.MysteryData = content
    GameGoodsList:RefreshMysteryData()
    GameGoodsList:RequestStoreGoodsList(GameGoodsList.nowStoreType, true)
    return true
  elseif msgType == NetAPIList.common_ack.Code and (content.api == NetAPIList.refresh_type_req.Code or content.api == NetAPIList.expedition_refresh_type_req.Code) then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  return false
end
function GameGoodsList.RefreshMysteryCallback2(msgType, content)
  if msgType == NetAPIList.refresh_type_ack.Code or msgType == NetAPIList.expedition_refresh_type_ack.Code then
    GameGoodsList.MysteryData = content
    GameGoodsList:RefreshMysteryData()
    return true
  elseif msgType == NetAPIList.common_ack.Code and (content.api == NetAPIList.refresh_type_req.Code or content.api == NetAPIList.expedition_refresh_type_req.Code) then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  return false
end
function GameGoodsList:RefreshMysteryData()
  print("RefreshMysteryData")
  if not self.MysteryData or self.store_type == 1 then
    return
  end
  local flash_obj = self:GetFlashObject()
  local data = self.MysteryData
  local isVisible = false
  if not flash_obj then
    return
  end
  if tonumber(data.refresh_type) == 1 and tonumber(data.cur_free_times) > 0 then
    isVisible = true
  end
  flash_obj:InvokeASCallback("_root", "SetMysteryNewsStatus", isVisible)
  if tonumber(self.nowStoreType) == 2 or GameGoodsList:IsInstanceStore() then
    self:UpdateMysteryCDTime()
  end
  if data.item_type then
    local item_type, item_num
    item_type = "item_" .. data.item_type
    item_num = tonumber(data.item_num)
    flash_obj:InvokeASCallback("_root", "SetMysteryRefreshResource", item_type, item_num, not GameGoodsList:IsInstanceStore())
  end
end
function GameGoodsList:RefreshRedPoint(price_off_status)
  local param = {}
  if price_off_status == 1 or price_off_status == 3 or price_off_status == 5 or price_off_status == 7 then
    param.count = 1
  else
    param.count = 0
  end
  self:GetFlashObject():InvokeASCallback("_root", "RefreshShopTab", param, 3)
  GameGoodsList.new_status3 = param.count
  if price_off_status == 2 or price_off_status == 3 or price_off_status == 6 or price_off_status == 7 then
    param.count = 1
  else
    param.count = 0
  end
  GameGoodsList.new_status4 = param.count
  self:GetFlashObject():InvokeASCallback("_root", "RefreshShopTab", param, 4)
  if price_off_status == 4 or price_off_status == 5 or price_off_status == 6 or price_off_status == 7 then
    param.count = 1
  else
    param.count = 0
  end
  GameGoodsList.new_status1 = param.count
  self:GetFlashObject():InvokeASCallback("_root", "RefreshShopTab", param, 1)
end
function GameGoodsList.sortItem()
  print("sortItem")
  local t_a = {}
  local t_b = {}
  local t_c = {}
  local levelInfo = GameGlobalData:GetData("levelinfo")
  local curLevel = GameVipDetailInfoPanel:GetVipLevel()
  for i, v in pairs(GameGoodsList.goodsList.items) do
    local enableBuy = true
    if not (curLevel >= v.vip_limit) or not (levelInfo.level >= v.level_limit) then
      enableBuy = false
      table.insert(t_b, v)
    end
    if enableBuy then
      if GameGoodsList.nowStoreType == 2 or GameGoodsList:IsInstanceStore() then
        if v.refresh_buy_limit == 0 then
          enableBuy = false
          table.insert(t_c, v)
        end
      elseif v.day_buy_left == 0 or v.all_buy_left == 0 then
        enableBuy = false
        table.insert(t_c, v)
      end
    end
    v.enableBuy = enableBuy
    if enableBuy then
      table.insert(t_a, v)
    end
  end
  if GameGoodsList.nowStoreType ~= 2 and (#t_b > 0 or #t_c > 0) then
    GameGoodsList.goodsList.items = {}
    for _, v in ipairs(t_a) do
      table.insert(GameGoodsList.goodsList.items, v)
    end
    for _, v in ipairs(t_b) do
      table.insert(GameGoodsList.goodsList.items, v)
    end
    for _, v in ipairs(t_c) do
      table.insert(GameGoodsList.goodsList.items, v)
    end
  end
end
function GameGoodsList.GoodsListCallback(msgType, content)
  if msgType == NetAPIList.stores_ack.Code then
    if content.mysterystore == false and GameGoodsList.nowStoreType == 2 then
      local GameTip = LuaObjectManager:GetLuaObject("GameTip")
      GameTip:Show(GameLoader:GetGameText("LC_MENU_MYSTERY_SHOP_CLOSE_INFO"))
      GameGoodsList:EntryStore(1)
      return true
    end
    GameGoodsList.goodsList = content
    local function _getShop_limittime(ngroup)
      local t = {}
      for i, v in pairs(GameGoodsList.goodsList.items) do
        if v.group == ngroup then
          table.insert(t, v)
        end
      end
      return t
    end
    GameGoodsList.sortItem()
    if GameGoodsList.param_PriceOffStore then
      GameGoodsList.goodsList3 = _getShop_limittime(GameGoodsList.param_PriceOffStore.store_infos[1].group)
      GameGoodsList.goodsList4 = _getShop_limittime(GameGoodsList.param_PriceOffStore.store_infos[2].group)
    end
    GameGoodsList.receiveGoodsListTime = os.time()
    local vipInfo = GameGlobalData:GetData("vipinfo")
    local levelInfo = GameGlobalData:GetData("levelinfo")
    GameGoodsList.receiveGoodsLevel = levelInfo.level
    GameGoodsList.receiveGoodsVIPLv = curLevel
    GameGoodsList:RefreshResource()
    GameGoodsList:RefreshGoodsList()
    GameGoodsList:RefreshRedPoint(content.price_off_status)
    return true
  elseif msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.stores_req.Code then
    DebugOut("common ack error")
    DebugStoreTable(content)
    if content.code == 5 then
      local GameTip = LuaObjectManager:GetLuaObject("GameTip")
      GameTip:Show(GameLoader:GetGameText("LC_MENU_MYSTERY_SHOP_CLOSE_INFO"))
      return true
    elseif content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      return true
    end
  end
  return false
end
function GameGoodsList.donamicDownloadFinishCallback(extInfo)
  if DynamicResDownloader:IfResExsit(extInfo.localPath) and GameGoodsList:GetFlashObject() then
    local frameName = extInfo.frameName
    DebugOut("GameGoodsList.donamicDownloadFinishCallback", frameName)
    DebugStore("goto frame", frameName, extInfo.itemKey, extInfo.index)
    GameGoodsList:GetFlashObject():InvokeASCallback("_root", "SetItemFrame", extInfo.itemKey, extInfo.index, frameName)
  end
  if GameStateManager:GetCurrentGameState():IsObjectInState(GameGoodsBuyBox) then
    GameGoodsBuyBox:UpdateDisplayIcon()
  end
  return true
end
function GameGoodsList.donamicDownloadShop3FinishCallback(extInfo)
  if DynamicResDownloader:IfResExsit(extInfo.localPath) and GameGoodsList:GetFlashObject() then
    GameGoodsList:GetFlashObject():InvokeASCallback("_root", "SetShop3SubItemFrame", extInfo)
  end
  return true
end
function GameGoodsList:OnBuyShop3Item(itemKey, count, useCrditExchange)
  GameGoodsList.buyItemKey = itemKey
  GameGoodsList.buyItemCount = count
  ItemBox.totalPurchaseCount = count
  print("ItemBox.totalPurchaseCount~", count, GameGoodsList.buyItemCount, ItemBox.totalPurchaseCount)
  local shop_purchase_req_param = {
    store_id = GameGoodsList.nowStoreType,
    id = GameGoodsList.goodsList.items[itemKey].id,
    buy_times = count,
    state = 0,
    coupon = 0
  }
  if useCrditExchange then
    shop_purchase_req_param.state = 1
  end
  local function _buy()
    NetMessageMgr:SendMsg(NetAPIList.stores_buy_req.Code, shop_purchase_req_param, GameGoodsList.BuyItem3_Callback, true, nil)
    GameUtils:RecordForTongdui(NetAPIList.stores_buy_req.Code, shop_purchase_req_param, GameGoodsList.BuyItem3_Callback, true, nil)
  end
  if GameSettingData.EnablePayRemind then
    local iteminfo = GameGoodsList.goodsList.items[itemKey]
    local price = iteminfo.price
    if iteminfo.discount < 100 then
      price = math.ceil(iteminfo.price * iteminfo.discount / 100)
    end
    GameGoodsList.OpenQuickBuyConfirmingUI(1, price, iteminfo, _buy)
  else
    _buy()
  end
end
function GameGoodsList.PriceOffStoreNTF(content)
  GameGoodsList.param_PriceOffStore = content
end
function GameGoodsList.FTEGiftNTF(content)
  local function checkfunc()
    local GameStateGlobalState = GameStateManager.GameStateGlobalState
    local GameStateMainPlanet = GameStateManager.GameStateMainPlanet
    if GameStateManager:GetCurrentGameState() == GameStateManager.GameStateMainPlanet then
      local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
      function ItemBox.afterRewardCallback()
        local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
        GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
        local text = GameLoader:GetGameText("LC_MENU_COME_RATE_US_CHAR")
        if GameUtils:IsQihooApp() then
          GameUIMessageDialog:SetYesButton(GameGlobalData.OpenAccountL20Url)
        else
          GameUIMessageDialog:SetYesButton(GameGlobalData.OpenUrl)
        end
        GameUIMessageDialog:Display("", text)
      end
      ItemBox:ShowRewardBox(content.gifts, 600000, true)
      return nil
    end
    return 1000
  end
  GameTimer:Add(checkfunc, 1)
end
function GameGoodsList.BuyItem3_Callback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.stores_buy_req.Code then
    if content.code ~= 0 then
      local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.stores_buy_ack.Code then
    local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
    ItemBox:ShowRewardBox(content.award)
    for k, v in pairs(GameGoodsList.goodsList.items) do
      if v.id == content.item.id then
        GameGoodsList.goodsList.items[k] = content.item
        break
      end
    end
    if GameGoodsList.nowStoreType == 3 then
      for k1, v1 in pairs(GameGoodsList.goodsList3) do
        if v1.id == content.item.id then
          GameGoodsList.goodsList3[k1] = content.item
          break
        end
      end
    elseif GameGoodsList.nowStoreType == 4 then
      for k2, v2 in pairs(GameGoodsList.goodsList4) do
        if v2.id == content.item.id then
          GameGoodsList.goodsList4[k2] = content.item
          break
        end
      end
    end
    GameGoodsList:UpdateGoodsItem(GameGoodsList.buyItemKey)
    if GameGlobalData.isLogin then
      if GameGoodsList:IsInstanceStore() then
        NetMessageMgr:SendMsg(NetAPIList.expedition_refresh_type_req.Code, nil, GameGoodsList.RefreshMysteryCallback2, true, nil)
      else
        NetMessageMgr:SendMsg(NetAPIList.refresh_type_req.Code, nil, GameGoodsList.RefreshMysteryCallback2, true, nil)
      end
    end
    return true
  end
  return false
end
function GameGoodsList.NetRequestCallback(msgType, content)
  if msgtype == NetAPIList.common_ack.Code and (NetAPIList.store_quick_buy_count_req.code == content.api or NetAPIList.store_quick_buy_price_req.code == content.api) then
    if content.code ~= 0 then
      local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.store_quick_buy_count_ack.Code then
    return true
  elseif msgType == NetAPIList.store_quick_buy_price_ack.Code then
    if GameGoodsList.isRquestPriceForConfirming then
      local function buyCallback()
        GameGoodsList.RequestBuyItem(GameGoodsList.param_buy_12.quickBuyCountRecord, 0)
      end
      GameGoodsList.OpenQuickBuyConfirmingUI(GameGoodsList.param_buy_12.quickBuyCountRecord, content.dis_price, GameGoodsList.param_buy_12.itemDetail, buyCallback)
      GameGoodsList.isRquestPriceForConfirming = false
    end
    if GameGoodsList.param_query_12 and GameGoodsList.param_query_12.nCount == content.buy_times then
      GameGoodsList:GetFlashObject():InvokeASCallback("_root", "UpdatePrice_12", GameGoodsList.param_query_12.nItemKey, GameGoodsList.param_query_12.strItemID, content.dis_price, content.nor_price, content.buy_times)
      GameGoodsList.param_query_12 = nil
    end
    return true
  end
  return false
end
function GameGoodsList.OpenQuickBuyConfirmingUI(count, price, storeitem, buyCallback)
  local function _getCurStoreItemCastTypeName()
    local storeItem = storeitem
    local name = GameGoodsList:serverIDToIconFrame(storeItem.price_type) or "crystal"
    if string.find(name, "item_") then
      name = GameHelper:GetAwardTypeText("item", storeItem.price_type)
    else
      name = GameHelper:GetAwardNameText(name)
    end
    name = name .. " "
    return name
  end
  local function _getCurStoreItemItemName()
    local gameItem = storeitem.item
    local itemName = GameHelper:GetAwardNameText(gameItem.item_type, gameItem.number)
    local count = GameHelper:GetAwardCount(gameItem.item_type, gameItem.number, gameItem.no)
    return itemName or "errorName", count
  end
  local castTypeName = _getCurStoreItemCastTypeName()
  local gameItemName, gameItemCount = _getCurStoreItemItemName()
  local textMsg = GameLoader:GetGameText("LC_MENU_BUY_MORE_ITEMS_PROMPT")
  textMsg = string.gsub(textMsg, "<number1>", tostring(price))
  textMsg = string.gsub(textMsg, "<number2>", tostring(castTypeName))
  textMsg = string.gsub(textMsg, "<number3>", tostring(count * gameItemCount))
  textMsg = string.gsub(textMsg, "<number4>", tostring(gameItemName))
  local textMsgAfterFormat = textMsg
  GameUtils:CreditCostConfirm(textMsgAfterFormat, buyCallback, true, nil)
end
function GameGoodsList.RequestBuyItem(count, mstate)
  GameGoodsList.param_buy_12.buyCount = count
  GameGoodsList.param_buy_12.mybuycount = count
  local itemDetail = GameGoodsList.param_buy_12.itemDetail
  local shop_purchase_req_param
  if GameGoodsList.usedItem ~= nil and GameGoodsList.isUseDiscount then
    shop_purchase_req_param = {
      store_id = GameGoodsList.nowStoreType,
      id = itemDetail.id,
      buy_times = count,
      state = mstate,
      coupon = GameGoodsList.usedItem.number
    }
  else
    shop_purchase_req_param = {
      store_id = GameGoodsList.nowStoreType,
      id = itemDetail.id,
      buy_times = count,
      state = mstate,
      coupon = 0
    }
  end
  NetMessageMgr:SendMsg(NetAPIList.stores_buy_req.Code, shop_purchase_req_param, GameGoodsList.BuyItem_12_Callback, true, nil)
  GameUtils:RecordForTongdui(NetAPIList.stores_buy_req.Code, shop_purchase_req_param, GameGoodsList.BuyItem_12_Callback, true, nil)
end
function GameGoodsList.BuyItem_12_Callback(msgType, content)
  print("BuyItem_12_Callback", msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.stores_buy_req.Code then
    local GameWaiting = LuaObjectManager:GetLuaObject("GameWaiting")
    GameWaiting:HideLoadingScreen()
    if content.api == NetAPIList.stores_buy_req.Code then
      if content.code ~= 0 then
        local GameVip = LuaObjectManager:GetLuaObject("GameVip")
        GameVip:CheckIsNeedShowVip(content.code)
      else
        GameGoodsList.usedItem = nil
        if GameGoodsList.param_buy_12.itemDetail.quick_buy then
          GameGoodsList:ReduceBuyTime(GameGoodsList.param_buy_12.quickBuyCountRecord)
        else
          GameGoodsList:ReduceBuyTime(1)
        end
        local _curitem = GameGoodsList.param_buy_12.itemDetail.item
        local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
        local param = {}
        local _param = {}
        _param.item_type = _curitem.item_type
        if GameHelper:IsResource(_param.item_type) then
          _param.number = _curitem.number * GameGoodsList.param_buy_12.mybuycount
          _param.no = _curitem.no
        else
          _param.no = _curitem.no * GameGoodsList.param_buy_12.mybuycount
          _param.number = _curitem.number
        end
        _param.level = _curitem.level
        table.insert(param, _param)
        ItemBox:ShowRewardBox(param)
        GameGoodsList.param_buy_12 = {}
        ifnotNeedToBack = true
        GameGoodsList.RequestRefreshData(true)
        GameGoodsList:RequestStoreGoodsList(GameGoodsList.nowStoreType, true)
      end
    end
    return true
  elseif NetAPIList.stores_buy_ack and msgType == NetAPIList.stores_buy_ack.Code then
    GameGoodsList.usedItem = nil
    local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
    ItemBox:ShowRewardBox(content.award)
    if GameGoodsList.param_buy_12.itemDetail.quick_buy then
      GameGoodsList:ReduceBuyTime(GameGoodsList.param_buy_12.quickBuyCountRecord)
    else
      GameGoodsList:ReduceBuyTime(1)
    end
    ifnotNeedToBack = true
    GameGoodsList.RequestRefreshData(true)
    GameGoodsList:RequestStoreGoodsList(GameGoodsList.nowStoreType, true)
    return true
  elseif msgType == NetAPIList.credit_exchange_ack.Code and content.api == NetAPIList.stores_buy_req.Code then
    local itemName = GameHelper:GetAwardNameText(content.item.item_type, content.item.number)
    local str = string.gsub(GameLoader:GetGameText("LC_MENU_LACK_OF_RESOURCES"), "<name1>", itemName)
    local infoText = string.gsub(str, "<name2>", content.credit)
    local function _func()
      GameGoodsList.RequestBuyItem(GameGoodsList.param_buy_12.buyCount, 1)
    end
    GameUtils:CreditCostConfirm(infoText, _func, true, nil)
    return true
  end
  return false
end
function GameGoodsList.RequestQuickBuyCast(nid, count, isBlock)
  local reqParam = {
    store_id = GameGoodsList.nowStoreType,
    buy_times = count,
    id = nid
  }
  NetMessageMgr:SendMsg(NetAPIList.store_quick_buy_price_req.Code, reqParam, GameGoodsList.NetRequestCallback, isBlock, nil)
end
function GameGoodsList:OnBuyItem_12(nItemid, ncount)
  print("OnBuyItem_12")
  GameGoodsList.Itemid = nItemid
  GameGoodsList.buyItemCount = ncount
  local GameUISevenBenefit = LuaObjectManager:GetLuaObject("GameUISevenBenefit")
  if GameUISevenBenefit.currentTabId == GameUISevenBenefit.ShowPanleTypeId.HALF_PRICE then
    local param = {id = nItemid}
    local function halfCallBack(msgType, content)
      if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.buy_cutoff_item_req.Code then
        if content.code ~= 0 then
          GameUIGlobalScreen:ShowAlert("error", content.code, nil)
        end
        GameUISevenBenefit:GetHalfPriceBenefitDataFromServer()
        return true
      end
      return false
    end
    NetMessageMgr:SendMsg(NetAPIList.buy_cutoff_item_req.Code, param, halfCallBack, true, nil)
  else
    local itemDetail
    if GameGoodsList.goodsList == nil then
      return
    end
    for k, v in ipairs(GameGoodsList.goodsList.items) do
      if v.id == nItemid then
        itemDetail = v
        break
      end
    end
    assert(itemDetail, tostring(nItemid))
    self.param_buy_12 = {itemDetail = itemDetail, quickBuyCountRecord = ncount}
    if itemDetail.quick_buy then
      local buyCount = ncount
      if 0 == buyCount then
        do break end
        -- unhandled boolean indicator
        assert(true)
      end
      if GameSettingData.EnablePayRemind then
        self.isRquestPriceForConfirming = true
        self.RequestQuickBuyCast(itemDetail.id, buyCount, true)
      else
        self.RequestBuyItem(buyCount, 0)
      end
    else
      self.RequestBuyItem(1, 0)
    end
  end
end
