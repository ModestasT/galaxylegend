local GameObjectBattleBG = LuaObjectManager:GetLuaObject("GameObjectBattleBG")
local AutoUpdateInBackground = AutoUpdateInBackground
local GameStateBattlePlay = GameStateManager.GameStateBattlePlay
require("data1/DownloadImageList.tfl")
local BgResListMissile = {
  [1] = "data2/LAZY_LOAD_DOWN_SPELL_Missile_atk_0.png",
  [2] = "data2/LAZY_LOAD_DOWN_SPELL_Missile_atk_1.png",
  [3] = "data2/LAZY_LOAD_DOWN_SPELL_Missile_atk_10.png",
  [4] = "data2/LAZY_LOAD_DOWN_SPELL_Missile_atk_11.png",
  [5] = "data2/LAZY_LOAD_DOWN_SPELL_Missile_atk_12.png",
  [6] = "data2/LAZY_LOAD_DOWN_SPELL_Missile_atk_13.png",
  [7] = "data2/LAZY_LOAD_DOWN_SPELL_Missile_atk_14.png",
  [8] = "data2/LAZY_LOAD_DOWN_SPELL_Missile_atk_15.png",
  [9] = "data2/LAZY_LOAD_DOWN_SPELL_Missile_atk_16.png",
  [10] = "data2/LAZY_LOAD_DOWN_SPELL_Missile_atk_17.png",
  [11] = "data2/LAZY_LOAD_DOWN_SPELL_Missile_atk_18.png",
  [12] = "data2/LAZY_LOAD_DOWN_SPELL_Missile_atk_19.png",
  [13] = "data2/LAZY_LOAD_DOWN_SPELL_Missile_atk_2.png",
  [14] = "data2/LAZY_LOAD_DOWN_SPELL_Missile_atk_20.png",
  [15] = "data2/LAZY_LOAD_DOWN_SPELL_Missile_atk_21.png",
  [16] = "data2/LAZY_LOAD_DOWN_SPELL_Missile_atk_22.png",
  [17] = "data2/LAZY_LOAD_DOWN_SPELL_Missile_atk_23.png",
  [18] = "data2/LAZY_LOAD_DOWN_SPELL_Missile_atk_24.png",
  [19] = "data2/LAZY_LOAD_DOWN_SPELL_Missile_atk_3.png",
  [20] = "data2/LAZY_LOAD_DOWN_SPELL_Missile_atk_4.png",
  [21] = "data2/LAZY_LOAD_DOWN_SPELL_Missile_atk_5.png",
  [22] = "data2/LAZY_LOAD_DOWN_SPELL_Missile_atk_6.png",
  [23] = "data2/LAZY_LOAD_DOWN_SPELL_Missile_atk_7.png",
  [24] = "data2/LAZY_LOAD_DOWN_SPELL_Missile_atk_8.png",
  [25] = "data2/LAZY_LOAD_DOWN_SPELL_Missile_atk_9.png",
  [26] = "data2/LAZY_LOAD_DOWN_SPELL_Missile_hurt_1.png",
  [27] = "data2/LAZY_LOAD_DOWN_SPELL_Missile_hurt_2.png",
  [28] = "data2/LAZY_LOAD_DOWN_SPELL_Missile_hurt_3.png",
  [29] = "data2/LAZY_LOAD_DOWN_SPELL_Missile_hurt_4.png",
  [30] = "data2/LAZY_LOAD_DOWN_SPELL_Missile_hurt_5.png",
  [31] = "data2/LAZY_LOAD_DOWN_SPELL_Missile_hurt_6.png",
  [32] = "data2/LAZY_LOAD_DOWN_SPELL_Missile_hurt_7.png",
  [33] = "data2/LAZY_LOAD_DOWN_SPELL_Missile_hurt_8.png",
  [34] = "data2/LAZY_LOAD_DOWN_SPELL_NormalAttackNew_atk_0.png",
  [35] = "data2/LAZY_LOAD_DOWN_SPELL_NormalAttackNew_atk_2.png",
  [36] = "data2/LAZY_LOAD_DOWN_SPELL_NormalAttackNew_atk_4.png",
  [37] = "data2/LAZY_LOAD_DOWN_SPELL_NormalAttackNew_atk_6.png",
  [38] = "data2/LAZY_LOAD_DOWN_SPELL_NormalAttackNew_atk_8.png",
  [39] = "data2/LAZY_LOAD_DOWN_SPELL_NormalAttackNew_atk_10.png",
  [40] = "data2/LAZY_LOAD_DOWN_SPELL_NormalAttackNew_atk_12.png",
  [41] = "data2/LAZY_LOAD_DOWN_SPELL_NormalAttackNew_atk_14.png",
  [42] = "data2/LAZY_LOAD_DOWN_SPELL_NormalAttackNew_atk_16.png",
  [43] = "data2/LAZY_LOAD_DOWN_SPELL_NormalAttackNew_atk_18.png",
  [44] = "data2/LAZY_LOAD_DOWN_SPELL_NormalAttackNew_atk_20.png",
  [45] = "data2/LAZY_LOAD_DOWN_SPELL_NormalAttackNew_atk_22.png",
  [46] = "data2/LAZY_LOAD_DOWN_SPELL_NormalAttackNew_atk_24.png",
  [47] = "data2/LAZY_LOAD_DOWN_SPELL_LaserNew_hurt_0.png",
  [48] = "data2/LAZY_LOAD_DOWN_SPELL_LaserNew_hurt_2.png",
  [49] = "data2/LAZY_LOAD_DOWN_SPELL_LaserNew_hurt_3.png",
  [50] = "data2/LAZY_LOAD_DOWN_SPELL_LaserNew_hurt_5.png",
  [51] = "data2/LAZY_LOAD_DOWN_SPELL_LaserNew_hurt_7.png",
  [52] = "data2/LAZY_LOAD_DOWN_SPELL_LaserNew_hurt_9.png",
  [53] = "data2/LAZY_LOAD_DOWN_SPELL_LaserNew_hurt_11.png",
  [54] = "data2/LAZY_LOAD_DOWN_SPELL_LaserNew_hurt_13.png",
  [55] = "data2/LAZY_LOAD_DOWN_SPELL_LaserNew_hurt_15.png"
}
GameObjectBattleBG.fixBG = nil
function GameObjectBattleBG:OnInitGame()
  self:Unload()
end
function GameObjectBattleBG:Unload()
  if self.m_currentBG then
    self.m_currentBG:UnloadFlashObject()
  end
  self.m_currentBG = nil
  self.m_textureType = nil
end
function GameObjectBattleBG:OnAddToGameState()
  if self.m_currentBG then
    local disPlayBg = true
    for k, v in pairs(BgResListMissile) do
      if (ext.crc32.crc32(v) == "" or ext.crc32.crc32(v) == nil) and not AutoUpdateInBackground:IsHeroFileUpdatedToServer(v) then
        disPlayBg = false
      end
      if AutoUpdate.isAndroidDevice and AlphaListImage[v] and (ext.crc32.crc32(AlphaListImage[v]) == "" or ext.crc32.crc32(AlphaListImage[v]) == nil) and not AutoUpdateInBackground:IsHeroFileUpdatedToServer(AlphaListImage[v]) then
        disPlayBg = false
      end
    end
    self:GetFlashObject():InvokeASCallback("_root", "showBattleAnim", disPlayBg)
  end
end
function GameObjectBattleBG:OnEraseFromGameState()
  self:Unload()
end
function GameObjectBattleBG:LoadBattleBG(areaID)
  local textureType = "blue"
  if GameObjectBattleBG.fixBG then
    textureType = GameObjectBattleBG.fixBG
  elseif areaID == 3 or areaID == 4 then
    textureType = "cyan"
  elseif areaID == 5 or areaID == 8 then
    textureType = "red"
  elseif areaID == 6 or areaID == 7 then
    textureType = "yellow"
  elseif areaID == 9 then
    textureType = "ice"
  else
    textureType = "blue"
  end
  if self.m_textureType == textureType and self.m_currentBG then
    self:SetBattleBGFrame()
    self:PreLoadBattleBGImage()
    return self.m_currentBG
  end
  local filename = string.format("battleplay_bg_blue.tfs")
  if textureType ~= "blue" then
    local alphaCheckOk = true
    local textureFullName = string.format("data2/BG_DOWN_battle_bg_%s.png", textureType)
    local textureFullNameAlpah = string.format("data2/BG_DOWN_battle_bg_%s_alpha.png", textureType)
    local flashFullName = ""
    local flashCheckName = ""
    if AutoUpdate.isLzma3to1Support then
      flashFullName = string.format("BG_DOWN_battleplay_bg_%s.tfs", textureType)
      flashCheckName = string.format("BG_DOWN_battleplay_bg_%s_lzma_3to1.tfs", textureType)
    elseif AutoUpdate.isLzmaSupport then
      flashFullName = string.format("BG_DOWN_battleplay_bg_%s.tfs", textureType)
      flashCheckName = string.format("BG_DOWN_battleplay_bg_%s_lzma.tfs", textureType)
    else
      flashFullName = string.format("BG_DOWN_battleplay_bg_%s.tfs", textureType)
      flashCheckName = string.format("BG_DOWN_battleplay_bg_%s.tfs", textureType)
    end
    local textureOk = false
    if ext.crc32.crc32(textureFullName) == "" or ext.crc32.crc32(textureFullName) == nil then
      textureOk = AutoUpdateInBackground:IsFileUpdatedToServer(textureFullName)
    else
      textureOk = true
    end
    local flashCheckOk = false
    if ext.crc32.crc32("data2/" .. flashCheckName) == "" or ext.crc32.crc32("data2/" .. flashCheckName) == nil then
      flashCheckOk = AutoUpdateInBackground:IsFileUpdatedToServer("data2/" .. flashCheckName)
    else
      flashCheckOk = true
    end
    if AutoUpdate.isAndroidDevice then
      alphaCheckOk = false
      if ext.crc32.crc32(textureFullNameAlpah) == "" or ext.crc32.crc32(textureFullNameAlpah) == nil then
        textureOk = AutoUpdateInBackground:IsFileUpdatedToServer(textureFullNameAlpah)
      else
        textureOk = true
      end
    end
    if textureOk and flashCheckOk and alphaCheckOk then
      filename = flashFullName
    else
      textureType = "blue"
    end
  end
  self:Unload()
  DebugOut("LoadBGMap ", filename)
  self.m_currentBG = self:NewInstance(filename, false)
  self.m_textureType = textureType
  self.m_currentBG:LoadFlashObject()
  self:SetBattleBGFrame()
  self:PreLoadBattleBGImage()
end
function GameObjectBattleBG:SetBattleBGFrame()
  local currentBGFrame = math.random(1, 3)
  if USE_DEFAULT_EFFECT or USE_SIMPLE_BATTLE_BG then
    currentBGFrame = 4
  else
    while currentBGFrame == self.m_lastBGFrame do
      currentBGFrame = math.random(1, 3)
    end
  end
  self.m_lastBGFrame = currentBGFrame
  DebugOutBattlePlay("playBG: ", "frame" .. self.m_lastBGFrame)
  self:GetFlashObject():InvokeASCallback("_root", "playBG", "frame" .. self.m_lastBGFrame)
end
function GameObjectBattleBG:GetFlashObject()
  if self == self.m_currentBG then
    return self.m_renderFx
  else
    if self.m_currentBG then
      return (...), self.m_currentBG
    end
    return nil
  end
end
function GameObjectBattleBG:PreLoadBattleBGImage()
  if SKIP_BATTLE_BG_PRELOAD or self.m_lastBGFrame == 4 then
    return
  end
  DebugOutBattlePlay("PreLoadBattleBGImage")
  local flashObj = self:GetFlashObject()
end
