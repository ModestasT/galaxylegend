local GameUIWDInBattle = LuaObjectManager:GetLuaObject("GameUIWDInBattle")
local GameWaiting = LuaObjectManager:GetLuaObject("GameWaiting")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameUIWDDefence = LuaObjectManager:GetLuaObject("GameUIWDDefence")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameUIWDStuff = LuaObjectManager:GetLuaObject("GameUIWDStuff")
local GameSetting = LuaObjectManager:GetLuaObject("GameSetting")
local GameStateWD = GameStateManager.GameStateWD
local GameUIBarLeft = LuaObjectManager:GetLuaObject("GameUIBarLeft")
local FacebookPopUI = LuaObjectManager:GetLuaObject("FacebookPopUI")
GameUIWDInBattle.mShareStoryChecked = true
function GameUIWDInBattle:OnInitGame()
  DebugWD("GameUIWDInBattle:OnInitGame()")
end
function GameUIWDInBattle:Init()
  self:SetBattleUIInfo()
  self:SetAllianceState()
end
function GameUIWDInBattle:OnAddToGameState()
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  GameUIWDInBattle:CheckDownloadImage()
  self:Init()
  self:MoveIn()
end
function GameUIWDInBattle:OnEraseFromGameState()
  self:UnloadFlashObject()
end
function GameUIWDInBattle:CheckDownloadImage()
  if (ext.crc32.crc32("data2/LAZY_LOAD_map_star_01.png") == "" or ext.crc32.crc32("data2/LAZY_LOAD_map_star_01.png") == nil) and not AutoUpdateInBackground:IsHeroFileUpdatedToServer("data2/LAZY_LOAD_map_star_01.png") then
    self:GetFlashObject():ReplaceTexture("LAZY_LOAD_map_star_01.png", "territorial_map_bg.png")
  end
end
function GameUIWDInBattle:MoveIn()
  DebugWD("GameUIWDInBattle:MoveIn()")
  self:ChangeBaseScreen()
end
function GameUIWDInBattle:MoveOut()
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "animationMoveOut")
end
function GameUIWDInBattle:Update(dt)
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    flash_obj:InvokeASCallback("_root", "OnUpdate")
    flash_obj:Update(dt)
    flash_obj:InvokeASCallback("_root", "onUpdateFrame", dt)
    self:UpdateCDTime()
    if GameStateWD.NeedChangeBaseScreen and GameStateWD:AllDataFetched() and GameStateWD:OpponentDataFetched() then
      GameStateWD.NeedChangeBaseScreen = false
      self:ChangeBaseScreen()
    end
  end
end
function GameUIWDInBattle:OnFSCommand(cmd, arg)
  DebugWD("GameUIWDInBattle:OnFSCommand: ", cmd)
  if cmd == "main_win_closed" then
    GameStateWD.ManuallyExit = true
    if GameStateWD:GetPreState() == GameStateManager.GameStateWD then
      GameStateManager:SetCurrentGameState(GameStateManager.GameStateMainPlanet)
    else
      GameStateManager:SetCurrentGameState(GameStateWD:GetPreState())
    end
  elseif cmd == "join_released" then
    local req = {is_local_matched = false}
    NetMessageMgr:SendMsg(NetAPIList.domination_join_req.Code, req, self.JoinWDCallback, true, nil)
  elseif cmd == "battle_released" then
    GameStateWD:EnterEnemyScene()
    local extraInfo = {}
    if GameStateWD.activeWD then
      local curWD = GameStateWD.activeWD
      local battleName = GameLoader:GetGameText(string.upper("LC_MENU_WD_BATTLE_NAME_" .. curWD.fight_name))
      extraInfo.myAlliance = GameStateWD.BothAllianceInfo.infoes[1].name
      extraInfo.enemyAlliance = GameStateWD.BothAllianceInfo.infoes[2].name
      extraInfo.wdStarName = battleName
    end
    DebugOut("extraInfo")
    DebugTable(extraInfo)
    local countryStr = ext.GetDeviceCountry and ext.GetDeviceCountry() or "unknow"
    if self.mShareStoryChecked then
      FacebookGraphStorySharer:ShareStory(FacebookGraphStorySharer.StoryType.STORY_TYPE_WD_ENTER, extraInfo)
    else
    end
  elseif cmd == "alliance_released" then
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateAlliance)
  elseif cmd == "join_alliance_released" then
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateAlliance)
  elseif cmd == "revival_released" then
    do
      local function purchase_callback()
        NetMessageMgr:SendMsg(NetAPIList.alliance_defence_revive_req.Code, nil, self.ReviveCallback, true, nil)
      end
      if GameSettingData.EnablePayRemind ~= nil and GameSettingData.EnablePayRemind then
        do
          local price = 300
          local function price_callback(msgType, content)
            if msgType == NetAPIList.common_ack.Code then
              if content.api == NetAPIList.price_req.Code then
                if content.code ~= 0 then
                  GameUIGlobalScreen:ShowAlert("error", content.code, nil)
                end
                return true
              end
            elseif msgType == NetAPIList.price_ack.Code then
              price = content.price
              local text_content = GameLoader:GetGameText("LC_MENU_WD_REVIVE_DEFENCER_CONFIRM_ALERT")
              text_content = string.format(text_content, price)
              GameUtils:CreditCostConfirm(text_content, purchase_callback)
              return true
            end
            return false
          end
          local price_requestParam = {
            price_type = "wd_leader_revival_cost",
            type = 0
          }
          NetMessageMgr:SendMsg(NetAPIList.price_req.Code, price_requestParam, price_callback, true, nil)
        end
      else
        purchase_callback()
      end
    end
  elseif cmd == "recruit_released" then
    if os.time() - GameUIWDInBattle.LastReCallTime < GameUIWDInBattle.ReCallInterval then
      local timeString = GameUtils:formatTimeStringAsPartion(GameUIWDInBattle.ReCallInterval - (os.time() - GameUIWDInBattle.LastReCallTime))
      local msg = GameLoader:GetGameText("LC_MENU_COSMIC_EXPEDITON_CALL_CD_ALERT")
      msg = string.format(msg, timeString)
      GameTip:Show(msg, 2000)
    else
      local data_alliance = GameGlobalData:GetData("alliance")
      if data_alliance and data_alliance.id then
        local chat_req = {
          content = GameLoader:GetGameText("LC_MENU_WD_CALL_CHAT"),
          receiver = "",
          channel = "alchat_" .. data_alliance.id,
          show_effect = GameSettingData.EnableChatEffect and 1 or 0
        }
        NetMessageMgr:SendMsg(NetAPIList.chat_req.Code, chat_req, self.NetCallbackSendMessage, true, nil)
      end
    end
  elseif cmd == "reward_released" then
    GameUIWDStuff:Show(GameUIWDStuff.menuType.menu_type_reword)
  elseif cmd == "rank_released" then
    GameUIWDStuff:Show(GameUIWDStuff.menuType.menu_type_rank)
    GameUIWDStuff.IsShowRankWindow = true
  elseif cmd == "Help_released" then
    local flash_obj = self:GetFlashObject()
    if flash_obj then
      flash_obj:InvokeASCallback("_root", "setHelpText", GameLoader:GetGameText("LC_MENU_WD_HELP"))
      flash_obj:InvokeASCallback("_root", "showHelp")
      GameUIWDInBattle.IsShowHelpWindow = true
    end
  elseif cmd == "needUpdateBattleItem" then
    self:SetBattleListItem(tonumber(arg))
  elseif cmd == "checkHistory" then
    GameStateWD:CheckHistoryInfoWhenBattle()
  elseif cmd == "hideHelpWindow" then
    GameUIWDInBattle.IsShowHelpWindow = false
  elseif cmd == "shareStoryCheckboxClicked" then
    if self.mShareStoryChecked then
      self.mShareStoryChecked = false
    else
      self.mShareStoryChecked = true
    end
    local flash_obj = self:GetFlashObject()
    if flash_obj then
      flash_obj:InvokeASCallback("_root", "setCheckBox", self.mShareStoryChecked)
    end
  end
end
function GameUIWDInBattle:UpdateCDTime()
  if not GameStateWD.activeWD then
    return
  end
  local timeString = ""
  local _lefttime = 0
  if GameUIWDInBattle.CurScreenState ~= GameStateWD.WDState.BATTLE then
    if GameStateWD.WDBaseInfo.State == GameStateWD.WDState.WAITING_OPPONENTS then
      if GameStateWD.WDBaseInfo.WaitingOpponentTime and GameStateWD.WDBaseInfo.WaitingOpponentFetchTime then
        _lefttime = GameStateWD.WDBaseInfo.WaitingOpponentTime + (os.time() - GameStateWD.WDBaseInfo.WaitingOpponentFetchTime)
      else
        _lefttime = -1
      end
      if _lefttime <= 0 then
        _lefttime = -1
      end
    else
      local centerItem = GameStateWD.activeWD
      _lefttime = math.abs(centerItem.left_time) - (os.time() - GameStateWD.fetchWDListTime)
    end
  else
    _lefttime = GameStateWD.OpponentAllianceDefenceInfo.left_seconds - (os.time() - GameStateWD.fetchBattleInfoTime)
    if _lefttime < 0 then
      _lefttime = 0
    end
  end
  local finished = false
  if _lefttime == 0 then
    timeString = GameLoader:GetGameText("LC_MENU_EVENT_OVER_BUTTON")
    finished = true
  elseif _lefttime == -1 then
    timeString = ""
    finished = true
  else
    timeString = GameUtils:formatTimeString(_lefttime)
    finished = false
  end
  local flash = self:GetFlashObject()
  if flash then
    flash:InvokeASCallback("_root", "setCDTime", timeString, finished)
  end
end
function GameUIWDInBattle:SetBattleUIInfo()
  if not GameStateWD.activeWD then
    return
  end
  local item = GameStateWD.activeWD
  local title = GameLoader:GetGameText("LC_MENU_WD_TITLE")
  local subTitle = GameLoader:GetGameText("LC_MENU_WD_LEBAL")
  subTitle = string.format(subTitle, item.id)
  if item.id == -1 then
    subTitle = GameLoader:GetGameText("LC_MENU_WD_LIST_WAITING_BUTTON")
  end
  local battleName = GameLoader:GetGameText(string.upper("LC_MENU_WD_BATTLE_NAME_" .. item.fight_name))
  local battleDesc = GameLoader:GetGameText(string.upper("LC_MENU_WD_BATTLE_DESC_" .. item.fight_name))
  local icon = GameStateWD:GetIconFrame(item)
  local state = GameStateWD.WDState.NO_DECLARATION
  if GameStateWD.BothAllianceInfo then
    state = GameStateWD.BothAllianceInfo.status
  end
  local joinedAlliance = GameStateWD:HaveAlliance()
  local desc = ""
  if not GameStateWD:IfAllianceEnabled() then
    desc = GameLoader:GetGameText("LC_MENU_WD_NO_ALLIANCE_DESC_TEXT")
  elseif state == GameStateWD.WDState.NO_DECLARATION then
    desc = GameLoader:GetGameText("LC_MENU_WD_IN_BATTLE_DESC_TEXT")
  elseif state == GameStateWD.WDState.WAITING_OPPONENTS then
    desc = GameLoader:GetGameText("LC_MENU_WD_MATCHING_DESC_TEXT")
  end
  local allianceEnabled = GameStateWD:IfAllianceEnabled()
  local facebookEnabled = false
  if FacebookGraphStorySharer:IsStoryEnabled(FacebookGraphStorySharer.StoryType.STORY_TYPE_WD_ENTER) then
    facebookEnabled = true
  end
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    flash_obj:InvokeASCallback("_root", "setBattleUIInfo", title, subTitle, battleName, icon, state, joinedAlliance, allianceEnabled, desc, facebookEnabled, self.mShareStoryChecked)
  end
end
GameUIWDInBattle.CurScreenState = GameStateWD.WDState.NO_DECLARATION
function GameUIWDInBattle:ChangeBaseScreen()
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    GameUIWDInBattle.CurScreenState = GameStateWD.WDBaseInfo.State
    local winAndFailStr = GameLoader:GetGameText("LC_MENU_WD_LEAGUE_BATTLE_RECORD_WIN_LABEL") .. "/" .. GameLoader:GetGameText("LC_MENU_WD_LEAGUE_BATTLE_RECORD_LOSE_LABEL")
    flash_obj:InvokeASCallback("_root", "changeBaseScreen", GameStateWD.WDBaseInfo.State, winAndFailStr)
    self:SetUIInfo()
    self:SetBattleUIInfo()
  end
end
function GameUIWDInBattle:SetUIInfo()
  DebugOut("Set wd in battle ui info")
  DebugTable(GameStateWD.OpponentAllianceDefenceInfo)
  local flash_obj = self:GetFlashObject()
  if flash_obj and GameStateWD.BothAllianceInfo and #GameStateWD.BothAllianceInfo.infoes > 0 and GameStateWD.WDBaseInfo.State == GameStateWD.WDState.BATTLE and GameStateWD.BothAllianceInfo and GameStateWD.OpponentAllianceDefenceInfo then
    local yourScore = GameStateWD.ScoreInfo.player_point
    local alliance1Info = ""
    alliance1Info = alliance1Info .. GameStateWD.BothAllianceInfo.infoes[1].name .. "\001"
    alliance1Info = alliance1Info .. GameStateWD:GetAlliancePoints(true) .. "\001"
    alliance1Info = alliance1Info .. GameUIWDDefence.DefenceInfo.hp .. "\001"
    alliance1Info = alliance1Info .. GameUIWDDefence.DefenceInfo.hp_limit .. "\001"
    alliance1Info = alliance1Info .. GameUIWDDefence.DefenceInfo.leader_hp .. "\001"
    alliance1Info = alliance1Info .. GameUIWDDefence.DefenceInfo.leader_hp_limit .. "\001"
    alliance1Info = alliance1Info .. GameStateWD:GetAllianceWins(true) .. "\001"
    alliance1Info = alliance1Info .. GameUtils:GetFlagFrameName(GameStateWD.BothAllianceInfo.infoes[1].flag) .. "\001"
    alliance1Info = alliance1Info .. GameStateWD.BothAllianceInfo.infoes[1].server_name .. "\001"
    local alliance2Info = ""
    alliance2Info = alliance2Info .. GameStateWD.BothAllianceInfo.infoes[2].name .. "\001"
    alliance2Info = alliance2Info .. GameStateWD:GetAlliancePoints(false) .. "\001"
    alliance2Info = alliance2Info .. GameStateWD.OpponentAllianceDefenceInfo.hp .. "\001"
    alliance2Info = alliance2Info .. GameStateWD.OpponentAllianceDefenceInfo.hp_limit .. "\001"
    alliance2Info = alliance2Info .. GameStateWD.OpponentAllianceDefenceInfo.leader_hp .. "\001"
    alliance2Info = alliance2Info .. GameStateWD.OpponentAllianceDefenceInfo.leader_hp_limit .. "\001"
    alliance2Info = alliance2Info .. GameStateWD:GetAllianceWins(false) .. "\001"
    alliance2Info = alliance2Info .. GameUtils:GetFlagFrameName(GameStateWD.BothAllianceInfo.infoes[2].flag) .. "\001"
    alliance2Info = alliance2Info .. GameStateWD.BothAllianceInfo.infoes[2].server_name .. "\001"
    DebugOut("GameStateWD.OpponentAllianceDefenceInfo.hp = ", GameStateWD.OpponentAllianceDefenceInfo.hp)
    flash_obj:InvokeASCallback("_root", "setBattleInfo", yourScore, alliance1Info, alliance2Info)
    flash_obj:InvokeASCallback("_root", "setFacebookState", facebookEnabled, self.mShareStoryChecked)
    self:RefreshBattleList()
  end
end
function GameUIWDInBattle:SetBattleListItem(itemKey)
  local item = GameUIWDInBattle.enabledBattleForDisplay[itemKey]
  local name = ""
  local userInfo = GameStateWD:GetUserInfoByID(item.user_id)
  if userInfo then
    name = GameUtils:GetUserDisplayName(userInfo.name)
  end
  local allianceName = GameStateWD.BothAllianceInfo.infoes[1].name
  if item.is_defender then
    allianceName = GameStateWD.BothAllianceInfo.infoes[2].name
  end
  local flash = GameUIWDInBattle:GetFlashObject()
  if flash then
    flash:InvokeASCallback("_root", "SetBattleListItem", itemKey, name, allianceName, item.win_count .. "/" .. item.lose_count, item.point, GameLoader:GetGameText("LC_MENU_WD_LEAGUE_BATTLE_RECORD_WIN_LABEL"), GameLoader:GetGameText("LC_MENU_WD_LEAGUE_BATTLE_RECORD_LOSE_LABEL"))
  end
end
function GameUIWDInBattle:RefreshBattleList()
  if GameUIWDInBattle.enabledBattleForDisplay == nil or GameUIWDInBattle.enabledBattleForDisplay == nil or #GameUIWDInBattle.enabledBattleForDisplay == 0 then
    local flash = GameUIWDInBattle:GetFlashObject()
    if flash then
      GameUIWDInBattle:GetFlashObject():InvokeASCallback("_root", "clearBattleListItem")
    end
  elseif GameUIWDInBattle.enabledBattleForDisplay ~= nil then
    local flash = GameUIWDInBattle:GetFlashObject()
    if flash then
      flash:InvokeASCallback("_root", "clearBattleListItem")
      flash:InvokeASCallback("_root", "initBattleList")
      for i = 1, #GameUIWDInBattle.enabledBattleForDisplay do
        flash:InvokeASCallback("_root", "addBattleListItem", i)
      end
      flash:InvokeASCallback("_root", "SetArrowInitVisible")
    end
  end
end
GameUIWDInBattle.enabledBattleForDisplay = nil
function GameUIWDInBattle:GetMyRankAndPoint(userID)
  local myRank, myPoint = "-", "0"
  for i, v in ipairs(GameUIWDInBattle.enabledBattleForDisplay) do
    if v.user_id == userID then
      myRank = i
      myPoint = v.point
      break
    end
  end
  return myRank, myPoint
end
function GameUIWDInBattle.JoinWDCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.domination_join_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    elseif GameStateWD.WDBaseInfo.State == GameStateWD.WDState.NO_DECLARATION then
      GameStateWD.BothAllianceInfo.status = GameStateWD.WDState.WAITING_OPPONENTS
      GameStateWD.WDBaseInfo.State = GameStateWD.WDState.WAITING_OPPONENTS
      GameUIWDInBattle:ChangeBaseScreen()
    end
    return true
  end
  return false
end
function GameUIWDInBattle.ReviveCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.alliance_defence_revive_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    else
    end
    return true
  end
  return false
end
GameUIWDInBattle.LastReCallTime = 0
GameUIWDInBattle.ReCallInterval = 600
function GameUIWDInBattle.NetCallbackSendMessage(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.chat_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    else
      GameTip:Show(GameLoader:GetGameText("LC_MENU_COSMIC_EXPEDITON_CALL_SUCCESS_ALERT"))
      GameUIWDInBattle.LastReCallTime = os.time()
    end
    return true
  end
  return false
end
function GameUIWDInBattle:SetAllianceState()
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    flash_obj:InvokeASCallback("_root", "setAllianceState", GameStateWD:IfAllianceEnabled(), GameStateWD:HaveAlliance())
  end
end
if AutoUpdate.isAndroidDevice then
  function GameUIWDInBattle.OnAndroidBack()
    local flashObj = GameUIWDStuff:GetFlashObject()
    GameUtils:printByAndroid("GameUIWDInBattle:OnAndroidBack" .. tostring(GameUIWDInBattle.IsShowHelpWindow) .. "," .. tostring(GameUIWDStuff.currentMenu))
    if GameUIWDInBattle.IsShowHelpWindow then
      GameUIWDInBattle:GetFlashObject():InvokeASCallback("_root", "hideHelp")
      GameUIWDInBattle.IsShowHelpWindow = false
    elseif GameUIWDStuff.IsShowRankWindow then
      if flashObj then
        flashObj:InvokeASCallback("_root", "animationMoveOut")
      end
      GameUIWDStuff.IsShowRankWindow = false
    elseif GameUIWDStuff.currentMenu == GameUIWDStuff.menuType.menu_type_reword then
      if flashObj then
        flashObj:InvokeASCallback("_root", "HideRewardWindow")
      end
      GameUIWDStuff.currentMenu = GameUIWDStuff.menuType.menu_type_rank
    elseif GameUIWDInBattle:GetFlashObject() then
      GameUIWDInBattle:GetFlashObject():InvokeASCallback("_root", "CloseInbattleWindow")
    elseif ext.GetBundleIdentifier() == "com.tap4fun.galaxyempire2_android.platform.uc" and AutoUpdate.localAppVersion >= 10608 then
      IPlatformExt.ExitGame()
    else
      ext.ShowExitGameDialog()
    end
  end
end
