local GameUIBarLeft = LuaObjectManager:GetLuaObject("GameUIBarLeft")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameUIBarRight = LuaObjectManager:GetLuaObject("GameUIBarRight")
local GameItemBag = LuaObjectManager:GetLuaObject("GameItemBag")
local GameUICommonDialog = LuaObjectManager:GetLuaObject("GameUICommonDialog")
local GameStateKrypton = GameStateManager.GameStateKrypton
local GameStateRecruit = GameStateManager.GameStateRecruit
local GameObjectTutorialCutscene = LuaObjectManager:GetLuaObject("GameObjectTutorialCutscene")
local NetMessageMgr = NetMessageMgr
local NetAPIList = NetAPIList
local AlertDataList = AlertDataList
local GameStateFormation = GameStateManager.GameStateFormation
local QuestTutorialEquip = TutorialQuestManager.QuestTutorialEquip
local GameNewMenuItem = LuaObjectManager:GetLuaObject("GameNewMenuItem")
local GameStateCampaignSelection = GameStateManager.GameStateCampaignSelection
local GameStateSetting = GameStateManager.GameStateSetting
local GameStateStore = GameStateManager.GameStateStore
local QuestTutorialEngineer = TutorialQuestManager.QuestTutorialEngineer
local QuestTutorialEnhance = TutorialQuestManager.QuestTutorialEnhance
local QuestTutorialArena = TutorialQuestManager.QuestTutorialArena
local QuestTutorialGetGift = TutorialQuestManager.QuestTutorialGetGift
local QuestTutorialPrestige = TutorialQuestManager.QuestTutorialPrestige
local QuestTutorialBattleMap = TutorialQuestManager.QuestTutorialBattleMap
local QuestTutorialBattleMap_second = TutorialQuestManager.QuestTutorialBattleMap_second
local QuestTutorialsChangeFleets1 = TutorialQuestManager.QuestTutorialsChangeFleets1
local QuestTutorialEnhance_third = TutorialQuestManager.QuestTutorialEnhance_third
local QuestTutorialArenaReward = TutorialQuestManager.QuestTutorialArenaReward
local QuestTutorialBuildAcademy = TutorialQuestManager.QuestTutorialBuildAcademy
local QuestTutorialBattleMapAfterRecruit = TutorialQuestManager.QuestTutorialBattleMapAfterRecruit
local QuestTutorialEquipAllByOneKey = TutorialQuestManager.QuestTutorialEquipAllByOneKey
local QuestTutorialEquipmentEvolution = TutorialQuestManager.QuestTutorialEquipmentEvolution
local QuestTutorialFirstGetSilvaEquip = TutorialQuestManager.QuestTutorialFirstGetSilvaEquip
local QuestTutorialFirstChapter = TutorialQuestManager.QuestTutorialFirstChapter
local QuestTutorialFirstReform = TutorialQuestManager.QuestTutorialFirstReform
local QuestTutorialPveMapPoint = TutorialQuestManager.QuestTutorialPveMapPoint
local GameQuestMenu = LuaObjectManager:GetLuaObject("GameQuestMenu")
local GameUIStarCraftMap = LuaObjectManager:GetLuaObject("GameUIStarCraftMap")
local GameFleetInfoTabBar = LuaObjectManager:GetLuaObject("GameFleetInfoTabBar")
local GameStateMainPlanet = GameStateManager.GameStateMainPlanet
local QuestTutorialComboGachaGetHero = TutorialQuestManager.QuestTutorialComboGachaGetHero
local QuestTutorialTacticsCenter = TutorialQuestManager.QuestTutorialTacticsCenter
local TutorialAdjutantManager = LuaObjectManager:GetLuaObject("TutorialAdjutantManager")
local GameUIUnalliance = LuaObjectManager:GetLuaObject("GameUIUnalliance")
local GameUIMaskLayer = LuaObjectManager:GetLuaObject("GameUIMaskLayer")
local GameUIArena = LuaObjectManager:GetLuaObject("GameUIArena")
GameUIBarRight.uiBarMoveIn = false
function GameUIBarRight:ShowRightMenu()
  if not self.uiBarMoveIn then
    self.uiBarMoveIn = true
    if self:GetFlashObject() then
      self:GetFlashObject():InvokeASCallback("_root", "ShowRightMenu")
    end
  end
end
function GameUIBarRight:OnAddToGameState(game_state)
  DebugOut("GameUIBarRight:OnAddToGameState", "qj test")
  TutorialQuestManager.WanaGetCallback = GameUIBarRight.CheckWanaTutorail
  TutorialQuestManager.ComboGachaGetHeroCallback = GameUIBarRight.ComboGachaGetHeroCallback
  if not GameUIBarRight:GetFlashObject() then
    GameUIBarRight:LoadFlashObject()
  end
  local isStatMainPlanet = false
  if GameStateManager:GetCurrentGameState() == GameStateManager.GameStateMainPlanet then
    isStatMainPlanet = true
    GameUIBarLeft:ShowChatIcon()
  end
  GameUIBarRight:GetFlashObject():InvokeASCallback("_root", "SetStatType", isStatMainPlanet)
end
function GameUIBarRight:OnEraseFromGameState(game_state)
  TutorialQuestManager.WanaGetCallback = nil
  TutorialQuestManager.ComboGachaGetHeroCallback = nil
end
function GameUIBarRight:ChechNeedShowTutorialBattleMap()
  local quest = GameGlobalData:GetData("user_quest")
  DebugOut("GameUIBarRight:ChechNeedShowTutorialBattleMap", self.m_tempShowBattleMapTutorial)
  if self.m_tempShowBattleMapTutorial or quest.status ~= 2 and (QuestTutorialBattleMap:IsActive() or QuestTutorialBattleMap_second:IsActive()) then
    GameUIBarRight:GetFlashObject():InvokeASCallback("_root", "ShowTutorialBattleMapBtn")
    DebugOut("ajksdjkajksdjkadj:", QuestTutorialBattleMap:IsActive())
    if QuestTutorialBattleMap:IsActive() then
      GameUIBarRight:GetFlashObject():InvokeASCallback("_root", "SetRadarMaskVisible", true)
    else
      GameUIBarRight:GetFlashObject():InvokeASCallback("_root", "SetRadarMaskVisible", false)
    end
  else
    if QuestTutorialFirstChapter:IsActive() or QuestTutorialPveMapPoint:IsActive() then
      GameUIBarRight:GetFlashObject():InvokeASCallback("_root", "ShowTutorialBattleMapBtn")
    else
      GameUIBarRight:GetFlashObject():InvokeASCallback("_root", "HideTutorialBattleMapBtn")
    end
    GameUIBarRight:GetFlashObject():InvokeASCallback("_root", "SetRadarMaskVisible", false)
    if QuestTutorialFirstGetSilvaEquip:IsActive() or TutorialQuestManager.QuestTutorialBuildTechLab:IsActive() or TutorialQuestManager.QuestTutorialPrestige:IsActive() then
      GameUIBarRight:GetFlashObject():InvokeASCallback("_root", "ShowTutorialBattleMapBtn")
    end
  end
end
function GameUIBarRight:SetForceShowTutorialBattleMapOnce()
  self.m_tempShowBattleMapTutorial = true
  DebugOut("SetForceShowTutorialBattleMapOnce", self.m_tempShowBattleMapTutorial)
end
function GameUIBarRight:OnParentStateGetFocus()
  if not GameObjectTutorialCutscene:IsActive() then
    DebugOut("OnParentStateGetFocus:")
    self:ShowRightMenu()
    GameUIBarRight:CheckQuestTutorial()
  end
  local isShowGalaxy = GameGlobalData:CanShowGalaxyButton()
  self:ShowGalaxyIcon(isShowGalaxy)
  self:SetVersionText(GameSettingData.DebugVersion)
  if QuestTutorialEquip:IsActive() or QuestTutorialArena:IsActive() or QuestTutorialArenaReward:IsActive() or QuestTutorialGetGift:IsActive() or QuestTutorialEnhance_third:IsActive() or QuestTutorialEquipAllByOneKey:IsActive() or QuestTutorialFirstReform:IsActive() then
    GameUIBarRight:MoveInRightMenu()
  end
  TutorialAdjutantManager:StartTutorialAdjutantBind_UIBarRight()
  if not GameStateMainPlanet.hadRequestFleetDisplayInfo then
    GameStateMainPlanet.hadRequestFleetDisplayInfo = true
  end
end
function GameUIBarRight:CheckQuestTutorial()
  DebugOut("check QuestTutorialEquipmentEvolution:", QuestTutorialEquipmentEvolution:IsActive(), ",", QuestTutorialEquipmentEvolution:IsFinished())
  DebugOut("_GameUIBarRight:CheckQuestTutorial", QuestTutorialPrestige:IsActive(), QuestTutorialEquip:IsActive(), QuestTutorialsChangeFleets1:IsActive(), QuestTutorialEquipAllByOneKey:IsActive())
  DebugOut("enenenen", self.m_isMovedIn)
  DebugOut("enenenenen", TutorialQuestManager.QuestTutorialEquipStarSystemItem:IsActive())
  if QuestTutorialEquip:IsActive() or QuestTutorialArena:IsActive() or QuestTutorialArenaReward:IsActive() or QuestTutorialsChangeFleets1:IsActive() or QuestTutorialEquipmentEvolution:IsActive() or QuestTutorialGetGift:IsActive() or TutorialQuestManager.QuestTutorialFirstGetWanaArmor:IsActive() or TutorialQuestManager.QuestTutorialFirstGetSilvaEquip:IsActive() or QuestTutorialComboGachaGetHero:IsActive() or QuestTutorialTacticsCenter:IsActive() or QuestTutorialFirstReform:IsActive() or TutorialQuestManager.QuestTutorialEquipStarSystemItem:IsActive() or QuestTutorialTacticsCenter:IsActive() then
    if not self.m_isMovedIn then
      self:GetFlashObject():InvokeASCallback("_root", "ShowTutorialAnimTip")
    elseif QuestTutorialGetGift:IsActive() or QuestTutorialComboGachaGetHero:IsActive() then
      self:GetFlashObject():InvokeASCallback("_root", "ShowTutorialBag")
    elseif QuestTutorialArena:IsActive() or QuestTutorialArenaReward:IsActive() then
      self:GetFlashObject():InvokeASCallback("_root", "ShowTutorialArena")
    elseif QuestTutorialEquip:IsActive() or QuestTutorialsChangeFleets1:IsActive() or QuestTutorialEquipAllByOneKey:IsActive() or QuestTutorialEnhance_third:IsActive() or TutorialQuestManager.QuestTutorialFirstGetWanaArmor:IsActive() or QuestTutorialEquipmentEvolution:IsActive() or TutorialQuestManager.QuestTutorialFirstGetSilvaEquip:IsActive() or QuestTutorialFirstReform:IsActive() or TutorialQuestManager.QuestTutorialEquipStarSystemItem:IsActive() then
      DebugOut("ShowTutorialEquip 22222")
      self:ShowTutorialEquip()
    elseif QuestTutorialPrestige:IsActive() then
    elseif QuestTutorialTacticsCenter:IsActive() then
      self:ShowTutorialMatrix()
    end
  else
    self:GetFlashObject():InvokeASCallback("_root", "HideTutorialAnimTip")
    self:GetFlashObject():InvokeASCallback("_root", "HideTutorialEquip")
    self:GetFlashObject():InvokeASCallback("_root", "HideTutorialPrestige")
    self:GetFlashObject():InvokeASCallback("_root", "HideTutorialMatrix")
    self:GetFlashObject():InvokeASCallback("_root", "HideTutorialArena")
  end
end
function GameUIBarRight:ShowTutorialEquip()
  do return end
  local flashObj = self:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "ShowTutorialEquip")
  end
end
function GameUIBarRight:ShowTutorialMatrix()
  local flashObj = self:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "ShowTutorialMatrix")
  end
end
function GameUIBarRight:HideTutorialMatrix()
  local flashObj = self:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "HideTutorialMatrix")
  end
end
function GameUIBarRight:ShowTutorialTlc()
  local flashObj = self:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "ShowTutorialArena")
  end
end
function GameUIBarRight:MoveInRightMenu()
  if self.m_isMovedIn then
    DebugOut("Warning: GameUIBar is MovedIn already")
    TutorialAdjutantManager:StartTutorialAdjutantBind_UIBarRightEquip()
    return
  end
  self.m_isMovedIn = true
  local GameUIStarSystemPort = LuaObjectManager:GetLuaObject("GameUIStarSystemPort")
  if GameStateManager:GetCurrentGameState() ~= GameStateManager.GameStateMainPlanet then
    GameUIBarLeft:HideChatIcon()
  end
  GameUIStarCraftMap:HideChatIcon()
  GameUIBarRight:RefreshRedPoint()
  if self:GetFlashObject() ~= nil then
    self:GetFlashObject():InvokeASCallback("_root", "MoveInRightMenu")
    self:FormatRightMenu()
  end
  self:GetFlashObject():InvokeASCallback("_root", "HideAllTutorial")
  if QuestTutorialEquip:IsActive() or QuestTutorialsChangeFleets1:IsActive() or QuestTutorialEquipAllByOneKey:IsActive() or QuestTutorialEnhance_third:IsActive() or TutorialQuestManager.QuestTutorialFirstGetWanaArmor:IsActive() or TutorialQuestManager.QuestTutorialFirstGetSilvaEquip:IsActive() or QuestTutorialFirstReform:IsActive() then
    DebugOut("ShowTutorialEquip 11111")
    DebugOut("QuestTutorialFirstGetWanaArmor = ", TutorialQuestManager.QuestTutorialFirstGetWanaArmor:IsActive())
    self:ShowTutorialEquip()
  elseif QuestTutorialArena:IsActive() or QuestTutorialArenaReward:IsActive() then
    self:GetFlashObject():InvokeASCallback("_root", "ShowTutorialArena")
  elseif QuestTutorialGetGift:IsActive() or QuestTutorialComboGachaGetHero:IsActive() then
    self:GetFlashObject():InvokeASCallback("_root", "ShowTutorialBag")
  elseif QuestTutorialPrestige:IsActive() then
    self:GetFlashObject():InvokeASCallback("_root", "ShowTutorialPrestige")
  elseif QuestTutorialEquipmentEvolution:IsActive() then
    DebugOut("ShowTutorialEquip 33333")
    self:ShowTutorialEquip()
  elseif QuestTutorialTacticsCenter:IsActive() then
    DebugOut("ShowTutorial tactics center")
    self:ShowTutorialMatrix()
  elseif TutorialQuestManager.QuestTutorialEquipStarSystemItem:IsActive() and GameGlobalData:GetModuleStatus("medal") then
    DebugOut("ShowTutorialEquip")
    self:ShowTutorialEquip()
  end
  TutorialQuestManager:CheckTlcTutorial()
  if TutorialQuestManager.QuestTutorialTlcMainUiEntry:IsActive() then
    DebugOut("ShowTutorialTlc")
    self:ShowTutorialTlc()
  end
  TutorialAdjutantManager:StartTutorialAdjutantBind_UIBarRightEquip()
end
function GameUIBarRight:MoveOutRightMenu()
  if not self.m_isMovedIn then
    return
  end
  self.m_isMovedIn = false
  GameUIBarLeft:ShowChatIcon()
  GameUIStarCraftMap:ShowChatIcon()
  if self:GetFlashObject() ~= nil then
    self:GetFlashObject():InvokeASCallback("_root", "MoveOutRightMenu")
  end
  self:CheckQuestTutorial()
end
function GameUIBarRight:OnInitGame()
  GameGlobalData:RegisterDataChangeCallback("modules_status", self.OnMenuStatusChange)
end
function GameUIBarRight.OnMenuStatusChange()
  DebugOut("OnMenuStatusChange ")
  GameUIBarRight:FormatRightMenu()
end
function GameUIBarRight:FormatRightMenu()
  if not self:GetFlashObject() then
    return
  end
  local rightMenu = GameGlobalData:GetData("modules_status").modules
  for k, v in pairs(rightMenu) do
    local iconPath = "_root.downCorner." .. v.name .. ".icons"
    local bgPath = "_root.downCorner." .. v.name .. ".bg"
    local unlock = v.status
    local name = v.name
    if name == "fleets" or name == "enhance" then
      name = "fleets"
    end
    if name ~= "price_off" then
      self:GetFlashObject():InvokeASCallback("_root", "SetBtnEnabled", v.name, unlock)
    else
      self:GetFlashObject():InvokeASCallback("_root", "SetShopOff", unlock)
    end
  end
  self:GetFlashObject():InvokeASCallback("_root", "SetBtnEnabled", "friends", true)
  self:GetFlashObject():InvokeASCallback("_root", "SetBtnEnabled", "prestige", GameFleetInfoTabBar:IsPrestigeEnabled())
  if GameUtils:IsModuleUnlock("centalsystem") and GameStateManager:GetCurrentGameState() == GameStateMainPlanet then
    GameStateManager.GameStateMainPlanet:CheckShowTutorial()
  end
  if GameUtils:IsModuleUnlock("alliance") then
    self:CheckQuestTutorial()
  end
end
function GameUIBarRight:ShowGalaxyIcon(isShow)
  self:GetFlashObject():InvokeASCallback("_root", "ShowGalaxyIcon", isShow)
  self:ChechNeedShowTutorialBattleMap()
end
function GameUIBarRight:Update(dt)
  self:GetFlashObject():InvokeASCallback("_root", "OnUpdate", dt)
  self:GetFlashObject():Update(dt)
end
function GameUIBarRight:SetVersionText(version)
  if not AutoUpdate.isDeveloper then
    if version == nil then
      version = "unknow"
    end
    self:GetFlashObject():InvokeASCallback("_root", "showVersion", "")
  end
end
function GameUIBarRight:OnFSCommand(cmd, arg)
  if cmd == "checkHideStore" then
    self:GetFlashObject():InvokeASCallback("_root", "SetShowStore", not GameUtils:IsTutorialScene())
  elseif cmd == "FS_CLICK_GALAXY" then
    self.m_tempShowBattleMapTutorial = nil
    self:ForceMoveOutRightMenu()
    local quest = GameGlobalData:GetData("user_quest")
    if quest.status ~= 2 and QuestTutorialBattleMap:IsActive() then
      AddFlurryEvent("ClickTutorialBattleMap", {}, 1)
      AddFlurryEvent("EnterStartMapFirstTime", {}, 2)
    elseif GameQuestMenu.receivedQuest6Reward and self.RadarClicked == nil then
      AddFlurryEvent("ClickBattleMapRadarAfterBuildAcademy", {}, 1)
      self.RadarClicked = true
    end
    if GameStateManager:GetCurrentGameState() == GameStateManager.GameStateMainPlanet or GameStateManager:GetCurrentGameState() == GameStateManager.GameStateBattleMap then
      local progress = GameGlobalData:GetData("progress")
      GameStateCampaignSelection:EnterArea(progress.act)
      if GameStateManager:GetCurrentGameState() == GameStateManager.GameStateBattleMap then
        do
          local GameUISection = LuaObjectManager:GetLuaObject("GameUISection")
          local GameStateBattleMap = GameStateManager.GameStateBattleMap
          local aid = GameStateBattleMap.m_currentAreaID
          local sid = GameStateBattleMap.m_currentSectionID
          function GameUISection.callback_close()
            GameStateManager.GameStateBattleMap:EnterSection(aid, sid, nil)
          end
        end
      end
      if immanentversion == 2 and not QuestTutorialBattleMap_second:IsFinished() and QuestTutorialBattleMap_second:IsActive() then
        QuestTutorialBattleMap_second:SetFinish(true)
      end
    else
      GameStateManager:SetCurrentGameState(GameStateManager.GameStateMainPlanet)
    end
  elseif cmd == "FS_CLICK_FLEETS" then
    self:ForceMoveOutRightMenu()
    local GameStateEquipEnhance = GameStateManager.GameStateEquipEnhance
    if QuestTutorialEnhance:IsActive() then
      GameStateEquipEnhance.isNeedShowEnhanceUI = true
      GameStateEquipEnhance:EnterEquipEnhance()
    else
      GameStateEquipEnhance:EnterEquipEnhance(-1)
    end
  elseif cmd == "FS_CLICK_PRESTIGE" then
    self:ForceMoveOutRightMenu()
    if GameItemBag.EquipTutorialAgain then
      AddFlurryEvent("TutorialItem_SelectFleet", {}, 2)
    end
    if QuestTutorialEquip:IsActive() and not QuestTutorialEquip:IsFinished() then
      AddFlurryEvent("FirstClickFleets", {}, 1)
      AddFlurryEvent("TutorialSelectFleets", {}, 2)
    elseif QuestTutorialPrestige:IsActive() and not QuestTutorialPrestige:IsFinished() then
      AddFlurryEvent("SecondClickFleets", {}, 1)
    end
    local GameUIPrestige = LuaObjectManager:GetLuaObject("GameUIPrestige")
    GameUIPrestige:Show()
  elseif cmd == "FS_CLICK_ENHANCE" then
  elseif cmd == "FS_CLICK_FRIENDS" then
    self:ForceMoveOutRightMenu()
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateHeroHandbook)
  elseif cmd == "FS_CLICK_ALLIANCE" then
    self:ForceMoveOutRightMenu()
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateAlliance)
  elseif cmd == "FS_CLICK_ARENA" then
    if TutorialQuestManager.QuestTutorialTlcMainUiEntry:IsActive() then
      TutorialQuestManager.QuestTutorialTlcMainUiEntry:SetFinish(true)
      self:GetFlashObject():InvokeASCallback("_root", "HideTutorialArena")
      TutorialQuestManager.QuestTutorialTlcArenaEntry:SetActive(true)
    end
    GameUIArena:OnEnterLayer()
  elseif cmd == "FS_CLICK_HIRE" then
    self:ForceMoveOutRightMenu()
    local GameStatePlayerMatrix = GameStateManager.GameStatePlayerMatrix
    GameStateManager:SetCurrentGameState(GameStatePlayerMatrix)
  elseif cmd == "FS_CLICK_BAG" then
    self:ForceMoveOutRightMenu()
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateTrade)
    if QuestTutorialGetGift:IsActive() then
      AddFlurryEvent("TutorialItem_SelectPackage", {}, 2)
    end
    if immanentversion == 1 and QuestTutorialGetGift:IsActive() then
      QuestTutorialGetGift:SetFinish(true)
    end
  elseif cmd == "FS_CLICK_SETTING" then
    self:ForceMoveOutRightMenu()
    GameStateManager:SetCurrentGameState(GameStateSetting)
  elseif cmd == "FS_CLICK_STORE" then
    self:ForceMoveOutRightMenu()
    local function checkfunc()
      GameStateManager:SetCurrentGameState(GameStateStore)
    end
    GameTimer:Add(checkfunc, 200)
  elseif cmd == "FS_CLICK_SWITCH" then
    if self.m_isMovedIn then
      self:MoveOutRightMenu()
    else
      if QuestTutorialPrestige:IsActive() then
        AddFlurryEvent("ClickMainMenu", {}, 1)
      end
      self:MoveInRightMenu()
    end
  elseif cmd == "putonEquip_pos" then
    if GameUtils:GetTutorialHelp() then
      local param = LuaUtils:string_split(arg, "\001")
      GameUtils:ShowMaskLayer(tonumber(param[1]), tonumber(param[2]), GameUIMaskLayer.ButtonPos.right, GameLoader:GetGameText("LC_MENU_ASSISTANT_INFO_6"), GameUIMaskLayer.MaskStyle.small)
    end
  elseif cmd == "downCornergoinover" then
  elseif cmd == "ExplorePlanetary_pos" then
    if GameUtils:GetTutorialHelp() then
      local param = LuaUtils:string_split(arg, "\001")
      GameUtils:ShowMaskLayer(tonumber(param[1]), tonumber(param[2]), GameUIMaskLayer.ButtonPos.left, GameLoader:GetGameText("LC_MENU_ASSISTANT_INFO_16"), GameUIMaskLayer.MaskStyle.small)
    end
  elseif cmd == "Pvp_pos" and GameUtils:GetTutorialHelp() then
    local param = LuaUtils:string_split(arg, "\001")
    GameUtils:ShowMaskLayer(tonumber(param[1]), tonumber(param[2]), GameUIMaskLayer.ButtonPos.left, GameLoader:GetGameText("LC_MENU_ASSISTANT_INFO_17"), GameUIMaskLayer.MaskStyle.small)
  end
end
function GameUIBarRight:ForceMoveOutRightMenu()
  if self.m_isMovedIn then
    self:MoveOutRightMenu()
  end
end
function GameUIBarRight:checkIsShowJoinAllianceTip(...)
  local mShowFlag = GameUIUnalliance.mJoinAllanceAwardData.is_guid
  if GameUtils:IsModuleUnlock("alliance") and mShowFlag then
    return true
  else
    return false
  end
end
function GameUIBarRight.CheckWanaTutorail()
  local curState = GameStateManager:GetCurrentGameState()
  if curState and curState:IsObjectInState(GameUIBarRight) and GameUIBarRight:GetFlashObject() then
    GameUIBarRight:CheckQuestTutorial()
  end
end
function GameUIBarRight.ComboGachaGetHeroCallback()
  local curState = GameStateManager:GetCurrentGameState()
  if curState and curState:IsObjectInState(GameUIBarRight) and GameUIBarRight:GetFlashObject() then
    DebugOut("GameUIBarRight:CheckQuestTutorial()..")
    GameUIBarRight:CheckQuestTutorial()
  end
end
function GameUIBarRight:getPutonEquipPos(...)
  self:MoveInRightMenu()
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "getPutonEquipPos")
  end
end
function GameUIBarRight:getExplorePlanetary(...)
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "getExplorePlanetary")
  end
end
function GameUIBarRight:getPvpPos(...)
  self:MoveInRightMenu()
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "getPvpPos")
  end
end
function GameUIBarRight:getMenuPos(item)
  self:MoveInRightMenu()
  if self:GetFlashObject() then
    local pos = self:GetFlashObject():InvokeASCallback("_root", "getMenuPos", item)
    local pos_s = LuaUtils:string_split(pos, "\001")
    return pos_s
  end
  return nil
end
function GameUIBarRight:RefreshRedPoint()
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "setMatrixRedPoint", false)
    self:GetFlashObject():InvokeASCallback("_root", "setAllianceRedPoint", false)
    self:GetFlashObject():InvokeASCallback("_root", "setFleetRedPoint", false)
    self:GetFlashObject():InvokeASCallback("_root", "setMainMenuRedPoint", false)
    self:GetFlashObject():InvokeASCallback("_root", "setStoreRedPoint", false)
    if GameGlobalData.redPointStates then
      for k, v in pairs(GameGlobalData.redPointStates.states) do
        if v.key == 100000001 and v.value == 1 then
          self:GetFlashObject():InvokeASCallback("_root", "setMatrixRedPoint", true)
          self:GetFlashObject():InvokeASCallback("_root", "setMainMenuRedPoint", true)
        end
        if v.key == 100000011 and v.value == 1 then
          self:GetFlashObject():InvokeASCallback("_root", "setAllianceRedPoint", true)
        end
        if v.key < 100000000 and v.value == 1 then
          self:GetFlashObject():InvokeASCallback("_root", "setFleetRedPoint", true)
          self:GetFlashObject():InvokeASCallback("_root", "setMainMenuRedPoint", true)
        end
        if v.key == 100000010 and v.value == 1 then
          self:GetFlashObject():InvokeASCallback("_root", "setStoreRedPoint", true)
        end
      end
    end
  end
end
