local GameStateBase = GameStateBase
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameObjectTutorialCutscene = LuaObjectManager:GetLuaObject("GameObjectTutorialCutscene")
local GameStateTracking = {
  m_trackGSIndex = {},
  m_trackGSCount = 0,
  m_trackCGS = 0,
  m_trackCGSObject = {},
  m_trackGGSObject = {},
  m_needOutput = false,
  m_lastFurry = "",
  m_needsend = false,
  m_trackingudid = "",
  m_trackingdata = ""
}
GameStateManager = {
  m_currentGameState = nil,
  m_frameCounter = 0,
  m_timer_ms = 0,
  m_tracking = GameStateTracking,
  m_waitForQuickStartVerCheck = false,
  m_lastPressedX = 0,
  m_lastPressedY = 0,
  m_lastReleasedX = 0,
  m_lastReleasedY = 0
}
function GameStateManager:BuildTrackingIndex(state)
  if self.m_tracking == nil then
    return
  end
  self.m_tracking.m_trackGSCount = self.m_tracking.m_trackGSCount + 1
  self.m_tracking.m_trackGSIndex[state] = self.m_tracking.m_trackGSCount
end
function GameStateManager:setTrackingState(enable)
  if enable then
    if self.m_tracking == nil and ext.is16x9() then
      self.m_tracking = GameStateTracking
    end
  elseif self.m_tracking ~= nil then
    self.m_tracking = nil
  end
end
function GameStateManager:NewGameState(statename)
  local newState = {}
  local metaTable = {}
  function metaTable.__index(t, k)
    local ret = GameStateBase[k]
    newState[k] = ret
    return ret
  end
  setmetatable(newState, metaTable)
  GameStateBase.InitMember(newState)
  newState.stateName = statename
  self:BuildTrackingIndex(statename)
  return newState
end
GameStateManager.GameStateWebview = GameStateManager:NewGameState("GameStateWebview")
GameStateManager.GameStateGlobalState = GameStateManager:NewGameState("GameStateGlobalState")
GameStateManager.GameStateSetting = GameStateManager:NewGameState("GameStateSetting")
GameStateManager.GameStateLoading = GameStateManager:NewGameState("GameStateLoading")
GameStateManager.GameStateMainPlanet = GameStateManager:NewGameState("GameStateMainPlanet")
GameStateManager.GameStateCampaignSelection = GameStateManager:NewGameState("GameStateCampaignSelection")
GameStateManager.GameStateFormation = GameStateManager:NewGameState("GameStateFormation")
GameStateManager.GameStateBattlePlay = GameStateManager:NewGameState("GameStateBattlePlay")
GameStateManager.GameStateFleetInfo = GameStateManager:NewGameState("GameStateFleetInfo")
GameStateManager.GameStateEquipEnhance = GameStateManager:NewGameState("GameStateEquipEnhance")
GameStateManager.GameStateTrade = GameStateManager:NewGameState("GameStateTrade")
GameStateManager.GameStateStore = GameStateManager:NewGameState("GameStateStore")
GameStateManager.GameStateAffairInfo = GameStateManager:NewGameState("GameStateAffairInfo")
GameStateManager.GameStateConfrontation = GameStateManager:NewGameState("GameStateConfrontation")
GameStateManager.GameStateArena = GameStateManager:NewGameState("GameStateArena")
GameStateManager.GameStateRecruit = GameStateManager:NewGameState("GameStateRecruit")
GameStateManager.GameStateFactory = GameStateManager:NewGameState("GameStateFactory")
GameStateManager.GameStateAlliance = GameStateManager:NewGameState("GameStateAlliance")
GameStateManager.GameStateKrypton = GameStateManager:NewGameState("GameStateKrypton")
GameStateManager.GameStateBattleMap = GameStateManager:NewGameState("GameStateBattleMap")
GameStateManager.GameStatePlayerMatrix = GameStateManager:NewGameState("GameStatePlayerMatrix")
GameStateManager.GameStateMineMap = GameStateManager:NewGameState("GameStateMineMap")
GameStateManager.GameStateGacha = GameStateManager:NewGameState("GameStateGacha")
GameStateManager.GameStateToprank = GameStateManager:NewGameState("GameStateToprank")
GameStateManager.GameStateArcane = GameStateManager:NewGameState("GameStateArcane")
GameStateManager.GameStateAccountSelect = GameStateManager:NewGameState("GameStateAccountSelect")
GameStateManager.GameStateWorldBoss = GameStateManager:NewGameState("GameStateWorldBoss")
GameStateManager.GameStateColonization = GameStateManager:NewGameState("GameStateColonization")
GameStateManager.GameStateWD = GameStateManager:NewGameState("GameStateWD")
GameStateManager.GameStateEmptyState = GameStateManager:NewGameState("GameStateEmptyState")
GameStateManager.GameStateDebug = GameStateManager:NewGameState("GameStateDebug")
GameStateManager.GameStateStarCraft = GameStateManager:NewGameState("GameStateStarCraft")
GameStateManager.GameStateItem = GameStateManager:NewGameState("GameStateItem")
GameStateManager.GameStateInstance = GameStateManager:NewGameState("GameStateInstance")
GameStateManager.GameStateLab = GameStateManager:NewGameState("GameStateLab")
GameStateManager.GameStateWorldChampion = GameStateManager:NewGameState("GameStateWorldChampion")
GameStateManager.GameStateHeroHandbook = GameStateManager:NewGameState("GameStateHeroHandbook")
GameStateManager.GameStateConsortium = GameStateManager:NewGameState("GameStateConsortium")
GameStateManager.GameStateWVE = GameStateManager:NewGameState("GameStateWVE")
GameStateManager.GameStateTacticsCenter = GameStateManager:NewGameState("GameStateTacticsCenter")
GameStateManager.GameStateInterstellar = GameStateManager:NewGameState("GameStateInterstellar")
GameStateManager.GameStateDaily = GameStateManager:NewGameState("GameStateDaily")
GameStateManager.GameStateDice = GameStateManager:NewGameState("GameStateDice")
GameStateManager.GameStateMiningWorld = GameStateManager:NewGameState("GameStateMiningWorld")
GameStateManager.GameStateFairArena = GameStateManager:NewGameState("GameStateFairArena")
GameStateManager.GameStateScratchGacha = GameStateManager:NewGameState("GameStateScratchGacha")
GameStateManager.GameStateLargeMap = GameStateManager:NewGameState("GameStateLargeMap")
GameStateManager.GameStateStarSystem = GameStateManager:NewGameState("GameStateStarSystem")
GameStateManager.GameStateFpsFte = GameStateManager:NewGameState("GameStateFpsFte")
GameStateManager.GameStateLargeMapAlliance = GameStateManager:NewGameState("GameStateLargeMapAlliance")
GameStateManager.GameState2x1DeviceAdapter = GameStateManager:NewGameState("GameState2x1DeviceAdapter")
GameStateManager.GameStateLargeMapRank = GameStateManager:NewGameState("GameStateLargeMapRank")
GameStateManager.GameStateTeamLeagueAtkFormation = GameStateManager:NewGameState("GameStateTeamLeagueAtkFormation")
GameStateManager.GameStateTlcFormation = GameStateManager:NewGameState("GameStateTlcFormation")
function GameStateManager:GetCurrentGameState()
  return self.m_currentGameState
end
function GameStateManager:SetCurrentGameState(newState)
  assert(newState ~= nil)
  if newState == self.m_currentGameState then
    DebugOut("same state return")
    GameUtils:Warning("setCurrentGameState: newState is same as currentState")
    return
  end
  GameStateManager.GameStateWD.CheckAndTryToRemoveEnemyUI(GameStateManager.GameStateWD)
  if self.m_currentGameState ~= nil then
    self.m_currentGameState:OnFocusLost(newState)
    self.m_currentGameState:ForceCompleteCammandList()
  end
  if isLowMemDevice then
    DebugOut("do collect in low memory device")
    collectgarbage("collect")
  end
  local self = GameStateManager
  local previousState = self.m_currentGameState
  self.m_currentGameState = newState
  DebugWD("newState:OnFocusGain")
  DebugWD(newState)
  newState.fightRoundData = {}
  newState.m_previousState = previousState
  newState:OnFocusGain(previousState)
  newState:ForceCompleteCammandList()
  newState:AfterStateGain()
  collectgarbage("collect")
  if DebugConfig.isDebugState then
    local currentState = self:GetCurrentGameState()
    local skipList = {
      "m_currentGameState"
    }
    local stateName = LuaUtils:getKeyFromValue(self, currentState, skipList)
    DebugState("===================== SetCurrentGameState State " .. stateName .. " =====================")
  end
end
function GameStateManager:ReEnterCurrentGameState()
  if self.m_currentGameState == nil then
    return
  end
  local curState = self.m_currentGameState
  local newState = self.m_currentGameState
  self.m_currentGameState:OnFocusLost(newState)
  self.m_currentGameState:ForceCompleteCammandList()
  newState:OnFocusGain(previousState)
  newState:ForceCompleteCammandList()
  newState:AfterStateGain()
  collectgarbage("collect")
  if DebugConfig.isDebugState then
    local currentState = self:GetCurrentGameState()
    local skipList = {
      "m_currentGameState"
    }
    local stateName = LuaUtils:getKeyFromValue(self, currentState, skipList)
    DebugState("===================== SetCurrentGameState State " .. stateName .. " =====================")
  end
end
function GameStateManager:Init()
  print("GameStateManager:Init")
  self.GameStateLoading:InitGameState()
  self.GameStateAccountSelect:InitGameState()
  self.GameStateSetting:InitGameState()
  self.GameStateMainPlanet:InitGameState()
  self.GameStateCampaignSelection:InitGameState()
  self.GameStateFormation:InitGameState()
  self.GameStateBattlePlay:InitGameState()
  self.GameStateFleetInfo:InitGameState()
  self.GameStateTrade:InitGameState()
  self.GameStateStore:InitGameState()
  self.GameStateColonization:InitGameState()
  self.GameStateWD:InitGameState()
  self.GameStateAffairInfo:InitGameState()
  self.GameStateConfrontation:InitGameState()
  self.GameStateArena:InitGameState()
  self.GameStateRecruit:InitGameState()
  self.GameStateFactory:InitGameState()
  self.GameStateAlliance:InitGameState()
  self.GameStateKrypton:InitGameState()
  self.GameStateBattleMap:InitGameState()
  self.GameStateInstance:InitGameState()
  self.GameStateConsortium:InitGameState()
  self.GameStateWVE:InitGameState()
  self.GameStateTacticsCenter:InitGameState()
  self.GameStateInterstellar:InitGameState()
  self.GameStateDice:InitGameState()
  self.GameStateMiningWorld:InitGameState()
  self.GameStateScratchGacha:InitGameState()
  self.GameStateLargeMap:InitGameState()
  self.GameStateLargeMapAlliance:InitGameState()
  self.GameStateLargeMapRank:InitGameState()
  if AutoUpdate.isTestGW then
    self.GameStateDebug:InitGameState()
  end
  GameSettingData.LastLoginSuccess = false
  GameUtils:SaveSettingData()
  local registAppid = GameSettingData.LastLogin and GameSettingData.LastLogin.RegistAppId or nil
  local localAppid = ext.GetBundleIdentifier()
  if registAppid and registAppid ~= "" and registAppid ~= localAppid then
    AutoUpdate.isQuickStartGame = false
  end
  if AutoUpdate.isQuickStartGame then
    GameUtils:SetLoginInfo(LuaUtils:table_rcopy(GameSettingData.LastLogin))
    local serverInfo = GameSettingData.LastLogin.ServerInfo
    GameUtils:HTTP_SendRequest("servers/client_login?logic_id=" .. serverInfo.logic_id, nil, nil, false, nil, "GET", "notencrypt")
    DelayLoad()
    local function conntectCallBack(checkres)
      if checkres and checkres >= 0 then
        GameStateManager:SetCurrentGameState(GameStateManager.GameStateLoading)
        GameStateManager.GameStateLoading.m_nextLoginTimer = -1
        self.m_waitForQuickStartVerCheck = true
      else
        self.m_waitForQuickStartVerCheck = false
        print("Connect server failed.")
        GameUtils:RestartGame(0)
      end
    end
    NetMessageMgr:InitNet(conntectCallBack)
  else
    self.m_waitForQuickStartVerCheck = false
    self:SetCurrentGameState(self.GameStateAccountSelect)
  end
  self:OutputStateIndex()
  if self.m_tracking ~= nil and (immanentversion == 1 or not ext.is16x9()) then
    self.m_tracking = nil
  end
  self._isNeedCheckNotification = false
end
GameStateManager.job_all = {}
GameStateManager.job_lastframe = 0
function GameStateManager:AddJob(func, tparam)
  assert(func)
  table.insert(GameStateManager.job_all, {func = func, tparam = tparam})
end
function GameStateManager:UpdateJob()
  if self.m_frameCounter - GameStateManager.job_lastframe < 5 then
    return
  end
  GameStateManager.job_lastframe = self.m_frameCounter
  if #GameStateManager.job_all == 0 then
    return
  end
  local t = table.remove(GameStateManager.job_all)
  t.func(t.tparam)
end
function GameStateManager:Update(dt)
  self.m_frameCounter = self.m_frameCounter + 1
  self.m_timer_ms = self.m_timer_ms + dt
  local curState = self:GetCurrentGameState()
  if curState then
    curState:Update(dt)
  end
  AutoUpdateInBackground:OnUpdate(dt)
  if AutoUpdate.isTestGW and ext.GetIOSMemInfo and self.m_frameCounter % 30 == 0 then
    local a, u = ext.GetIOSMemInfo()
    local GameDebugBox = LuaObjectManager:GetLuaObject("GameDebugBox")
    self.GameStateDebug:AppendInfo("used", tostring(u))
    self.GameStateDebug:AppendInfo("avai", tostring(a))
  end
  self.GameStateGlobalState:Update(dt)
  self.GameStateItem:Update(dt)
  self.GameStateWebview:Update(dt)
  if ext.is2x1 and ext.is2x1() and not AutoUpdate.isAndroidDevice then
    self.GameState2x1DeviceAdapter:Update(dt)
  end
  DebugCommand.Update()
  GameStateManager:UpdateJob()
  if AutoUpdate.isTestGW then
    self.GameStateDebug:Update(dt)
  end
  DeviceAutoConfig.UpdateStatistics()
  if self.m_tracking ~= nil and self.m_frameCounter % 300 == 0 then
    self:SendTrackingData()
  end
  if DebugConfig.isDebugState and curState and self.m_frameCounter % 300 == 0 then
    local currentState = self:GetCurrentGameState()
    local skipList = {
      "m_currentGameState"
    }
    local stateName = LuaUtils:getKeyFromValue(self, currentState, skipList)
    DebugState("===================== Currant State " .. stateName .. " =====================")
    self:GetCurrentGameState():PrintAllObject()
    DebugState("Global State Object")
    self.GameStateGlobalState:PrintAllObject()
    DebugState("GameStateItem State Object")
    self.GameStateItem:PrintAllObject()
    DebugState("GameStateWebview State Object")
    self.GameStateWebview:PrintAllObject()
  end
  if self.m_waitForQuickStartVerCheck and AutoUpdate.isQuickStartGameVerNtfRecv and AutoUpdate.isQuickStartGameVerOk then
    GameStateManager.GameStateLoading:Login(true)
    self.m_waitForQuickStartVerCheck = false
  end
  if self._isNeedCheckNotification then
    DebugOut("_isNeedCheckNotification:", GameGlobalData.isLogin)
    if GameGlobalData.isLogin then
      self:DoNotificationGameResume()
    end
  end
end
function GameStateManager:Render()
  local curState = self:GetCurrentGameState()
  if curState then
    curState:Render()
  end
  self.GameStateGlobalState:Render()
  self.GameStateItem:Render()
  self.GameStateWebview:Render()
  if ext.is2x1 and ext.is2x1() and not AutoUpdate.isAndroidDevice then
    self.GameState2x1DeviceAdapter:Render()
  end
  if AutoUpdate.isTestGW then
    self.GameStateDebug:Render()
  end
end
function GameStateManager:OnTouchPressed(x, y)
  self.m_lastPressedX = x
  self.m_lastPressedY = y
  self:BuildTrackingData(x, y)
  if self.GameStateWebview:OnTouchPressed(x, y) then
    return
  end
  if self.GameStateItem:OnTouchPressed(x, y) then
    return
  end
  if self.GameStateGlobalState:OnTouchPressed(x, y) then
    return
  end
  if AutoUpdate.isTestGW then
    self.GameStateDebug:OnTouchPressed(x, y)
  end
  local curState = self:GetCurrentGameState()
  if curState then
    curState:OnTouchPressed(x, y)
  end
end
function GameStateManager:OnTouchMoved(x, y)
  if self.GameStateWebview:OnTouchMoved(x, y) then
    return
  end
  if self.GameStateItem:OnTouchMoved(x, y) then
    return
  end
  if self.GameStateGlobalState:OnTouchMoved(x, y) then
    return
  end
  local curState = self:GetCurrentGameState()
  if curState then
    curState:OnTouchMoved(x, y)
  end
end
function GameStateManager:OnTouchReleased(x, y)
  self.m_lastReleasedX = x
  self.m_lastReleasedY = y
  DebugOut("GameStateManager:OnTouchReleased ")
  local GameUIChat = LuaObjectManager:GetLuaObject("GameUIChat")
  local curState = self:GetCurrentGameState()
  if not curState:IsObjectInState(GameUIChat) then
    DebugOut("---aaaaaaaa")
    GameTextEdit:HideKeyboard(x, y)
  end
  if self.GameStateWebview:OnTouchReleased(x, y) then
    return
  end
  if self.GameStateItem:OnTouchReleased(x, y) then
    return
  end
  if self.GameStateGlobalState:OnTouchReleased(x, y) then
    return
  end
  if AutoUpdate.isTestGW then
    self.GameStateDebug:OnTouchReleased(x, y)
  end
  if curState then
    curState:OnTouchReleased(x, y)
  end
  if curState:IsObjectInState(GameUIChat) and not GameTextEdit.TouchInInput then
    DebugOut("---bbbbbbbb")
    GameTextEdit:HideKeyboard(x, y)
  end
  GameTextEdit.TouchInInput = false
end
function GameStateManager:OnCommonTouchEvent(x, y, touchid, ntype)
  local curState = self:GetCurrentGameState()
  if curState and curState.OnCommonTouchEvent then
    curState:OnCommonTouchEvent(x, y, touchid, ntype)
  end
end
function GameStateManager:OnMultiTouchBegin(x1, y1, x2, y2)
  local curState = self:GetCurrentGameState()
  if curState then
    curState:OnMultiTouchBegin(x1, y1, x2, y2)
  end
end
function GameStateManager:OnMultiTouchMove(x1, y1, x2, y2)
  local curState = self:GetCurrentGameState()
  if curState then
    curState:OnMultiTouchMove(x1, y1, x2, y2)
  end
end
function GameStateManager:OnMultiTouchEnd(x1, y1, x2, y2)
  local curState = self:GetCurrentGameState()
  if curState then
    curState:OnMultiTouchEnd(x1, y1, x2, y2)
  end
end
function GameStateManager:OutputStateIndex()
  if self.m_tracking == nil or self.m_tracking.m_needOutput == false then
    return
  end
  file = io.open("../../../stateindex.lua", "w+")
  file:write("stateindex = {\n")
  for k, v in pairs(self.m_tracking.m_trackGSIndex) do
    file:write("\t[" .. v .. "] \t= ")
    file:write("\t\t'" .. k .. "',\n")
  end
  file:write("}\n")
  file:close()
end
function GameStateManager:BuildTrackingData(x, y)
  if self.m_tracking == nil or LuaObjectManager.m_tracking == nil then
    return
  end
  local statetrack = self.m_tracking
  local objecttrack = LuaObjectManager.m_tracking
  if statetrack.m_needsend == false then
    statetrack.m_trackingudid = ext.GetIOSOpenUdid()
    statetrack.m_trackingdata = ""
    statetrack.m_needsend = true
  end
  local currentState = self:GetCurrentGameState()
  if nil == currentState then
    return
  end
  local skipList = {
    "m_currentGameState"
  }
  local stateName = LuaUtils:getKeyFromValue(self, currentState, skipList)
  local curstateobjects = {}
  local object = 0
  for i, v in ipairs(currentState.m_menuObjectList) do
    object = objecttrack.m_trackIndex[v.m_objectName]
    object = object or 0
    table.insert(curstateobjects, object)
  end
  for i, v in ipairs(currentState.m_bgObjectList) do
    object = objecttrack.m_trackIndex[v.m_objectName]
    object = object or 0
    table.insert(curstateobjects, object)
  end
  if statetrack.m_trackCGS == statetrack.m_trackGSIndex[stateName] then
    table.sort(statetrack.m_trackCGSObject)
    table.sort(curstateobjects)
    if not LuaUtils:table_equal(statetrack.m_trackCGSObject, curstateobjects) then
      statetrack.m_trackingdata = statetrack.m_trackingdata .. "s" .. statetrack.m_trackGSIndex[stateName]
      for k, v in pairs(curstateobjects) do
        statetrack.m_trackingdata = statetrack.m_trackingdata .. "." .. v
      end
      statetrack.m_trackingdata = statetrack.m_trackingdata .. "|"
      statetrack.m_trackCGSObject = curstateobjects
    end
  else
    statetrack.m_trackingdata = statetrack.m_trackingdata .. "s" .. statetrack.m_trackGSIndex[stateName]
    for k, v in pairs(curstateobjects) do
      statetrack.m_trackingdata = statetrack.m_trackingdata .. "." .. v
    end
    statetrack.m_trackingdata = statetrack.m_trackingdata .. "|"
    statetrack.m_trackCGSObject = curstateobjects
    statetrack.m_trackCGS = statetrack.m_trackGSIndex[stateName]
  end
  local globalobjects = {}
  for i, v in ipairs(self.GameStateGlobalState.m_menuObjectList) do
    object = objecttrack.m_trackIndex[v.m_objectName]
    object = object or 0
    table.insert(globalobjects, object)
  end
  for i, v in ipairs(self.GameStateGlobalState.m_bgObjectList) do
    object = objecttrack.m_trackIndex[v.m_objectName]
    object = object or 0
    table.insert(globalobjects, object)
  end
  if not LuaUtils:table_equal(statetrack.m_trackGGSObject, globalobjects) then
    statetrack.m_trackingdata = statetrack.m_trackingdata .. "g"
    for k, v in pairs(globalobjects) do
      statetrack.m_trackingdata = statetrack.m_trackingdata .. "." .. v
    end
    statetrack.m_trackingdata = statetrack.m_trackingdata .. "|"
    statetrack.m_trackGGSObject = globalobjects
  end
  local webviewobjects = {}
  for i, v in ipairs(self.GameStateWebview.m_menuObjectList) do
    object = objecttrack.m_trackIndex[v.m_objectName]
    object = object or 0
    table.insert(webviewobjects, object)
  end
  for i, v in ipairs(self.GameStateWebview.m_bgObjectList) do
    object = objecttrack.m_trackIndex[v.m_objectName]
    object = object or 0
    table.insert(webviewobjects, object)
  end
  if not LuaUtils:table_equal(statetrack.m_trackGGSObject, webviewobjects) then
    statetrack.m_trackingdata = statetrack.m_trackingdata .. "g"
    for k, v in pairs(webviewobjects) do
      statetrack.m_trackingdata = statetrack.m_trackingdata .. "." .. v
    end
    statetrack.m_trackingdata = statetrack.m_trackingdata .. "|"
    statetrack.m_trackGGSObject = webviewobjects
  end
  statetrack.m_trackingdata = statetrack.m_trackingdata .. x .. "."
  statetrack.m_trackingdata = statetrack.m_trackingdata .. y .. "|"
end
function GameStateManager:SendTrackingData()
  if self.m_tracking == nil then
    return
  end
  local track = self.m_tracking
  if track.m_needsend then
    do
      local track_req = {
        udid = track.m_trackingudid,
        state_object = track.m_trackingdata
      }
      local function netFailedCallback()
        NetMessageMgr:SendMsg(NetAPIList.screen_track_req.Code, track_req, nil, true, nil)
      end
      NetMessageMgr:SendMsg(NetAPIList.screen_track_req.Code, track_req, nil, true, netFailedCallback)
      track.m_needsend = false
    end
  end
end
function GameStateManager:SaveFlurryTracking(flurry)
  if self.m_tracking == nil then
    return
  end
  local track = self.m_tracking
  if track.m_needsend then
    track.m_trackingdata = track.m_trackingdata .. "f" .. flurry .. "|"
  end
end
function GameStateManager:RegisterGameResumeNotification(func, args)
  DebugOut("RegisterGameResumeNotification:", func, ",", args)
  if not self._notificationTable then
    self._notificationTable = {}
  end
  local notify = {}
  notify.func = func
  notify.args = args
  table.insert(self._notificationTable, notify)
end
function GameStateManager:RemoveGameResumeNotification(func)
  DebugOut("RemoveGameResumeNotification", func)
  if not self._notificationTable or not func then
    return
  end
  local key
  for k, v in pairs(self._notificationTable) do
    if v.func == func then
      key = k
      break
    end
  end
  if key then
    DebugOut("do RemoveGameResumeNotification:", key, ",", #self._notificationTable)
    table.remove(self._notificationTable, key)
    DebugOut("after RemoveGameResumeNotification:", #self._notificationTable)
  end
end
function GameStateManager:NotificationGameResume()
  DebugOut("NotificationGameResume", self._notificationTable)
  if not self._notificationTable then
    return
  end
  self._isNeedCheckNotification = true
end
function GameStateManager:DoNotificationGameResume()
  DebugOut("DoNotificationGameResume")
  if not self._notificationTable then
    return
  end
  for k, v in pairs(self._notificationTable) do
    if v.args and #v.args > 0 then
      v.func(unpack(v.args))
    else
      v.func(v.args)
    end
  end
  self._isNeedCheckNotification = false
end
