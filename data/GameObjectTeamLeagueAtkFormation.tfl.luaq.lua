local GlobalData = GameGlobalData.GlobalData
local GameStateTeamLeagueAtkFormation = GameStateManager.GameStateTeamLeagueAtkFormation
local GameUICommonDialog = LuaObjectManager:GetLuaObject("GameUICommonDialog")
local GameObjectTeamLeagueAtkFormation = LuaObjectManager:GetLuaObject("GameObjectTeamLeagueAtkFormation")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameUIEnemyInfo = LuaObjectManager:GetLuaObject("GameUIEnemyInfo")
local GameUIEvent = LuaObjectManager:GetLuaObject("GameUIEvent")
local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameUIAdjutant = LuaObjectManager:GetLuaObject("GameUIAdjutant")
local GameUIGameUIAdvancedArenaLayer = require("data1/GameUIAdvancedArenaLayer.tfl")
local GameUIArena = LuaObjectManager:GetLuaObject("GameUIArena")
require("FleetTeamLeagueMatrix.tfl")
local FleetTeamLeagueAtkMatrix
function GameObjectTeamLeagueAtkFormation:Init()
  self:Clear()
  if not self.m_enemysFormation then
    self.m_enemysFormation = {}
    for i_cell = 1, 9 do
      self.m_enemysFormation[i_cell] = 0
    end
  end
  GameObjectTeamLeagueAtkFormation.mCurFreeMatrixSubIdx = 0
  FleetTeamLeagueAtkMatrix = FleetTeamLeagueMatrix:GetAttackInstance()
  GameObjectTeamLeagueAtkFormation:InitFormation()
  GameObjectTeamLeagueAtkFormation:InitUI()
end
function GameObjectTeamLeagueAtkFormation:Clear()
  self.m_formationFleets = {}
  self.m_freeFleets = {}
  self.m_nextTeamMatrix = {}
  self.m_nextPlayerRestFleets = {}
end
function GameObjectTeamLeagueAtkFormation:InitFlashObject()
end
function GameObjectTeamLeagueAtkFormation:InitFormation()
  DebugOut("InitFormation")
  self:Clear()
  if self.currentMatrixIndex == nil then
    self.currentMatrixIndex = 1
  end
  local init_fleets_matrix = FleetTeamLeagueAtkMatrix:GetMatrixByIndex(self.currentMatrixIndex)
  local leaderlist = GameGlobalData:GetData("leaderlist")
  self.curMainfleetIndex = 1
  self.mainFleetList = leaderlist.options
  local curr_id = FleetTeamLeagueAtkMatrix:GetLeaderFleet()
  for k, v in ipairs(self.mainFleetList) do
    DebugOut("v:" .. v)
    if v == curr_id then
      self.curMainfleetIndex = k
      DebugOut("k ;;", k)
    end
  end
  DebugOut("curMainFleetIndex", self.curMainfleetIndex)
  DebugTable(self.mainFleetList)
  for i = 1, 9 do
    self.m_formationFleets[i] = 0
  end
  for i, v in pairs(init_fleets_matrix.cells) do
    self.m_formationFleets[v.cell_id] = v.fleet_identity
  end
  for i, v in pairs(self.m_formationFleets) do
    self:UpdateBattleCommanderGrid(i, v)
  end
  self.m_freeFleets = FleetTeamLeagueAtkMatrix:GetFreeFleets()
  for i = 1, 9 do
    if not self.m_freeFleets[i] then
      self.m_freeFleets[i] = -1
    end
  end
  DebugOutPutTable(self.m_freeFleets, "freeFleets")
  for i, v in pairs(self.m_freeFleets) do
    self:UpdateRestCommanderGrid(i, v)
  end
end
function GameObjectTeamLeagueAtkFormation:UpdatePlayerInfo()
  local player_info = GameGlobalData:GetUserInfo()
  local player_main_fleet = GameGlobalData:GetFleetInfo(1)
  DebugOut("self.curMainfleetIndex:" .. self.curMainfleetIndex)
  DebugTable(self.mainFleetList)
  local player_avatar = GameDataAccessHelper:GetFleetAvatar(self.mainFleetList[self.curMainfleetIndex], player_main_fleet.level)
  local player_name = GameUtils:GetUserDisplayName(player_info.name)
  self:GetFlashObject():InvokeASCallback("_root", "UpdatePlayerInfo", player_name, player_avatar)
end
function GameObjectTeamLeagueAtkFormation:UpdateEnemyInfo()
  local enemyInfo
  enemyInfo = GameUIArena.enemyInfo_tlc
  if enemyInfo then
    self:SetEnemyInfo(enemyInfo)
  end
end
function GameObjectTeamLeagueAtkFormation:SetEnemyInfo(enemyInfo)
  DebugOut("enemyInfo ==")
  DebugTable(enemyInfo)
  self:GetFlashObject():InvokeASCallback("_root", "UpdateEnemyInfo", enemyInfo.name, enemyInfo.avatar)
  self:GetFlashObject():InvokeASCallback("_root", "UpdatePowerPoint", -1, GameUtils.numberAddComma(enemyInfo.force))
end
function GameObjectTeamLeagueAtkFormation:OnAddToGameState(state)
  if not self:GetFlashObject() then
    self:LoadFlashObject()
    local uiText = {}
    uiText.team1Text = GameLoader:GetGameText("LC_MENU_TEAMLEAGUE_TEAM1_CHAR")
    uiText.team2Text = GameLoader:GetGameText("LC_MENU_TEAMLEAGUE_TEAM2_CHAR")
    uiText.team3Text = GameLoader:GetGameText("LC_MENU_TEAMLEAGUE_TEAM3_CHAR")
    uiText.fleetBtnText = GameLoader:GetGameText("LC_MENU_TEAMLEAGUE_TEAM_FLEETS_BTN")
    self:GetFlashObject():InvokeASCallback("_root", "initAsTeamLeague", uiText)
  end
  self.m_currentSelectFleetId = 1
  self:Init()
end
function GameObjectTeamLeagueAtkFormation:OnEraseFromGameState(state)
  GameObjectTeamLeagueAtkFormation:Clear()
  self.m_enemysFormation = nil
  GameObjectTeamLeagueAtkFormation.m_renderFx = nil
  self:UnloadFlashObject()
end
function GameObjectTeamLeagueAtkFormation:AnimationMoveIn()
  self:GetFlashObject():InvokeASCallback("_root", "AnimationMoveIn")
end
function GameObjectTeamLeagueAtkFormation:AnimationMoveOut()
  self:GetFlashObject():InvokeASCallback("_root", "AnimationMoveOut")
end
function GameObjectTeamLeagueAtkFormation:UpdatePlayerPowerPoint()
  local power_point = 0
  for _, v in pairs(self.m_formationFleets) do
    if v > 0 then
      local fleet_info
      DebugOut("UpdatePlayerPowerPoint:", v)
      fleet_info = GameGlobalData:GetFleetInfo(v)
      if fleet_info then
        power_point = power_point + fleet_info.force
      end
    end
  end
  for i = 1, 3 do
    if i ~= self.currentMatrixIndex then
      local fleets = FleetTeamLeagueAtkMatrix:GetMatrixByIndex(i).cells
      for _, v in pairs(fleets) do
        if 0 < v.fleet_identity then
          local fleet_info
          fleet_info = GameGlobalData:GetFleetInfo(v.fleet_identity)
          if fleet_info then
            power_point = power_point + fleet_info.force
          end
        end
      end
    end
  end
  local powerText = GameUtils.numberConversion2(power_point)
  self:GetFlashObject():InvokeASCallback("_root", "UpdatePowerPoint", GameUtils.numberAddComma(powerText), -1)
end
function GameObjectTeamLeagueAtkFormation:SelectCommanderSkill(commander_id, skill_id)
  local commander_data = GameGlobalData:GetFleetInfo(commander_id)
  if not commander_data then
    return
  end
  commander_data.active_spell = skill_id
  FleetTeamLeagueAtkMatrix:SetCurrentSkill(commander_id, skill_id)
  local leaderlist = GameGlobalData:GetData("leaderlist")
  local curSpells = leaderlist.active_spells
  for indexSkill = 1, GameGlobalData:GetNumMaxSkills() do
    if curSpells[indexSkill] == skill_id then
      local skillinfo = GameDataAccessHelper:GetSkillInfo(skill_id)
      local datatable = {}
      datatable[#datatable + 1] = GameDataAccessHelper:GetSkillNameText(skill_id)
      datatable[#datatable + 1] = GameDataAccessHelper:GetSkillDesc(skill_id)
      local skillrangetype = GameDataAccessHelper:GetSkillRangeType(skill_id)
      datatable[#datatable + 1] = "TYPE_" .. skillrangetype
      datatable[#datatable + 1] = GameLoader:GetGameText("LC_MENU_SKILL_RANGE_TYPE_" .. skillrangetype)
      local datastring = table.concat(datatable, "\001")
      self:GetFlashObject():InvokeASCallback("_root", "selectCommanderSkill", indexSkill, datastring)
      break
    end
  end
  self:UpdateCommanderSkillAttackRange(self:GetBattleCommanderGridIndex(commander_id), skill_id)
end
function GameObjectTeamLeagueAtkFormation:ExpandSkillSelectorNew(index)
  local grid_type, index_commander = self:GetCommanderGridIndex(1)
  self:UpdateBattleCommanderGrid(index_commander, 1)
  self:UpdatePlayerInfo()
  self:SelectMainFleet(index)
end
function GameObjectTeamLeagueAtkFormation:ShowMainFleetSelector(id_commander)
  DebugOut("GameObjectTeamLeagueAtkFormation ExpandSkillSelector", id_commander)
  if id_commander ~= 1 then
    return
  end
  local grid_type, index_commander = self:GetCommanderGridIndex(id_commander)
  if grid_type == "rest" then
    return
  end
  local commander_data = GameGlobalData:GetFleetInfo(id_commander)
  local param = {}
  for k, v in ipairs(self.mainFleetList) do
    local param1 = {}
    param1.avatar = GameDataAccessHelper:GetFleetAvatar(v, 0)
    table.insert(param, param1)
  end
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    flash_obj:InvokeASCallback("_root", "ShowMainFleetSelector", index_commander, param, self.curMainfleetIndex)
  end
  self:ExpandSkillSelectorNew(self.curMainfleetIndex)
end
function GameObjectTeamLeagueAtkFormation:setMainFleet()
  local id = self.mainFleetList[self.curMainfleetIndex]
  local param = {}
  param.index = 101
  param.id = id
  NetMessageMgr:SendMsg(NetAPIList.change_tlc_leader_req.Code, param, GameObjectTeamLeagueAtkFormation.ChangeLeaderCallback, true, nil)
end
function GameObjectTeamLeagueAtkFormation.ChangeLeaderCallback(msgType, content)
  DebugOut("hangeLeaderCallback")
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.change_tlc_leader_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    elseif GameObjectTeamLeagueAtkFormation:GetFlashObject() then
      local grid_type, index_commander = GameObjectTeamLeagueAtkFormation:GetCommanderGridIndex(1)
      GameObjectTeamLeagueAtkFormation:UpdateBattleCommanderGrid(index_commander, 1)
      GameObjectTeamLeagueAtkFormation:UpdateBigHead()
    end
    return true
  end
  return false
end
function GameObjectTeamLeagueAtkFormation:UpdateBigHead()
  local player_info = GameGlobalData:GetUserInfo()
  local player_main_fleet = GameGlobalData:GetFleetInfo(1)
  local player_avatar = GameDataAccessHelper:GetFleetAvatar(self.mainFleetList[self.curMainfleetIndex], player_main_fleet.level)
  local player_name = GameUtils:GetUserDisplayName(player_info.name)
  self:GetFlashObject():InvokeASCallback("_root", "UpdatePlayerInfo", player_name, player_avatar)
end
function GameObjectTeamLeagueAtkFormation:ShrinkSkillSelector()
  self:GetFlashObject():InvokeASCallback("_root", "ShrinkSkillSelector")
end
function GameObjectTeamLeagueAtkFormation:UpdateCommanderSkillAttackRange(attack_grid, id_skill)
  DebugOut("UpdateCommanderSkillAttackRange ", attack_grid, id_skill)
  local attack_type = -1
  local target_pos = -1
  local isBackAttack = false
  if id_skill then
    attack_type = GameDataAccessHelper:GetSkillRangeType(id_skill)
  end
  if attack_type == GameGlobalData.AttackRangeType.Back then
    isBackAttack = true
  end
  if attack_grid then
    target_pos = self:ComputeTargetPos(attack_grid, isBackAttack)
  end
  local target_table = self:ComputeAttackRange(target_pos, attack_type)
  local target_data = ""
  for i = 1, 9 do
    if target_data ~= "" then
      target_data = target_data .. ","
    end
    target_data = target_data .. tostring(target_table[i])
  end
  self:GetFlashObject():InvokeASCallback("_root", "UpdateCommanderSkillAttackRange", target_data)
end
function GameObjectTeamLeagueAtkFormation:ComputeTargetPos(index_grid, isBackAttack)
  DebugOut("ComputeTargetPos ", index_grid)
  assert(index_grid and index_grid >= 1 and index_grid <= 9)
  local ypos = (index_grid - 1) % 3 + 1
  local order = GameGlobalData.AttackTargetOrder[ypos]
  local startGrid = 1
  local endGrid = 9
  local step = 1
  if isBackAttack then
    order = GameGlobalData.AttackTargetOrderBack[ypos]
  end
end
function GameObjectTeamLeagueAtkFormation:ComputeAttackRange(target_pos, attack_type)
  local target_table = {}
  for i = 1, 9 do
    target_table[i] = -1
  end
  local IsTargetInRange = function(target)
    return target >= 1 and target <= 9
  end
  if target_pos and IsTargetInRange(target_pos) then
    local AttackRangeType = GameGlobalData.AttackRangeType
    if attack_type == AttackRangeType.Monomer or attack_type == AttackRangeType.Back then
      target_table[target_pos] = 1
    elseif attack_type == AttackRangeType.Vertical then
      local start_pos = math.floor((target_pos - 1) / 3) * 3
      for i = 1, 3 do
        local attack_pos = start_pos + i
        if IsTargetInRange(attack_pos) then
          target_table[attack_pos] = 1
        end
      end
    elseif attack_type == AttackRangeType.Horizontal then
      local start_pos = math.floor((target_pos - 1) % 3)
      for i = 0, 2 do
        local attack_pos = i * 3 + start_pos + 1
        if IsTargetInRange(attack_pos) then
          target_table[attack_pos] = 1
        end
      end
    elseif attack_type == AttackRangeType.Cross then
      local check_table
      if target_pos == 1 or target_pos == 4 or target_pos == 7 then
        check_table = {
          target_pos,
          target_pos + 1,
          target_pos + 2,
          target_pos - 3,
          target_pos - 6,
          target_pos + 3,
          target_pos + 6
        }
      elseif target_pos == 2 or target_pos == 5 or target_pos == 8 then
        check_table = {
          target_pos,
          target_pos - 1,
          target_pos + 1,
          target_pos - 3,
          target_pos - 6,
          target_pos + 3,
          target_pos + 6
        }
      else
        check_table = {
          target_pos,
          target_pos - 1,
          target_pos - 2,
          target_pos - 3,
          target_pos - 6,
          target_pos + 3,
          target_pos + 6
        }
      end
      for _, target_check in ipairs(check_table) do
        if IsTargetInRange(target_check) then
          target_table[target_check] = 1
        end
      end
    elseif attack_type == AttackRangeType.Plenary then
      for i = 1, 9 do
        target_table[i] = 1
      end
    elseif attack_type == 0 then
    else
      target_table[target_pos] = 1
    end
  end
  return target_table
end
function GameObjectTeamLeagueAtkFormation:SelectMainFleet(fleet_index)
  DebugOut("GameObjectTeamLeagueAtkFormation:SelectMainFleet:" .. fleet_index)
  DebugTable(self.mainFleetList)
  local fleet_id = self.mainFleetList[fleet_index]
  local commander_data = GameGlobalData:GetFleetInfo(1)
  local num_max_skill = GameGlobalData:GetNumMaxSkills()
  if fleet_id == 1 then
    self:GetFlashObject():InvokeASCallback("_root", "ExpandSkillSelectorNew", fleet_index)
    DebugOut("active_spell: ", commander_data.active_spell)
    local leaderlist = GameGlobalData:GetData("leaderlist")
    local curSpells = leaderlist.active_spells
    DebugTable(curSpells)
    for index_skill = 1, num_max_skill do
      local id_skill = curSpells[index_skill]
      DebugOut("id_skill: ", id_skill)
      id_skill = id_skill or -1
      local is_active = id_skill == commander_data.active_spell
      local grid_type, index_commander = self:GetCommanderGridIndex(1)
      self:GetFlashObject():InvokeASCallback("_root", "UpdateCommanderSkillItem", index_commander, index_skill, id_skill, false)
      if is_active then
        self:SelectCommanderSkill(fleet_id, id_skill)
      end
    end
  else
    local basic_info = GameDataAccessHelper:GetCommanderBasicInfo(fleet_id)
    local skill_id = basic_info.SPELL_ID
    local skillinfo = GameDataAccessHelper:GetSkillInfo(skill_id)
    local datatable = {}
    datatable[#datatable + 1] = GameDataAccessHelper:GetSkillNameText(skill_id)
    datatable[#datatable + 1] = GameDataAccessHelper:GetSkillDesc(skill_id)
    local skillrangetype = GameDataAccessHelper:GetSkillRangeType(skill_id)
    datatable[#datatable + 1] = "TYPE_" .. skillrangetype
    datatable[#datatable + 1] = GameLoader:GetGameText("LC_MENU_SKILL_RANGE_TYPE_" .. skillrangetype)
    local datastring = table.concat(datatable, "\001")
    DebugOut("dataString:", datastring)
    local grid_type, index_commander = self:GetCommanderGridIndex(1)
    self:GetFlashObject():InvokeASCallback("_root", "shrinkSkillSelectorNew")
    self:GetFlashObject():InvokeASCallback("_root", "selectCommanderSkillNew", fleet_index, datastring, index_commander)
  end
end
function GameObjectTeamLeagueAtkFormation:SelectCommander(id_commander)
  local index_commander = -1
  local id_skill = -1
  local commander_identity = -1
  local grid_type
  if id_commander and id_commander > 0 then
    grid_type, index_commander = self:GetCommanderGridIndex(id_commander)
    if id_commander ~= 1 then
      local commander_data = GameGlobalData:GetFleetInfo(id_commander)
      DebugTable(commander_data)
      id_skill = commander_data.active_spell
      commander_identity = commander_data.identity
    else
      local commander_data = GameGlobalData:GetFleetInfo(id_commander)
      id_skill = commander_data.active_spell
      local leaderlist = GameGlobalData:GetData("leaderlist")
      commander_identity = 1
    end
  end
  DebugOut("SelectCommander", id_commander)
  DebugOut("grid_type", grid_type)
  DebugOut("index_commander", index_commander)
  self:GetFlashObject():InvokeASCallback("_root", "SelectCommanderGrid", grid_type, index_commander, commander_identity)
  self:UpdateCommanderSkillAttackRange(index_commander, id_skill)
  self.selected_commander = id_commander
end
function GameObjectTeamLeagueAtkFormation:UpdateCommanderGrid(grid_type, grid_index, id_commander)
  if grid_type == "rest" then
    self:UpdateRestCommanderGrid(grid_index, id_commander)
  elseif grid_type == "battle" then
    self:UpdateBattleCommanderGrid(grid_index, id_commander)
  else
    assert(false)
  end
end
function GameObjectTeamLeagueAtkFormation:UpdateBattleCommanderGrid(index_grid, id_commander, ingoreForce)
  if not self.m_formationFleets[index_grid] then
    self.m_formationFleets[index_grid] = nil
  end
  self.m_formationFleets[index_grid] = id_commander
  local tempid = id_commander
  local commanderType = -1
  local commander_identity = -1
  local commander_avatar = -1
  local commander_vessels = -1
  local commander_vessels_type = -1
  local commander_level = 0
  local commander_data = GameGlobalData:GetFleetInfo(id_commander)
  local commander_sex = "unknown"
  if id_commander == 1 then
    id_commander = FleetTeamLeagueAtkMatrix:GetLeaderFleet() or 1
  end
  local commander_color = "blue"
  if commander_data == nil then
    DebugOut("error: battle id_commander")
  end
  if id_commander > 1 and commander_data ~= nil then
    commander_identity = commander_data.identity
    commander_level = commander_data.level
    commanderType = GameGlobalData:GetCommanderTypeWithIdentity(commander_identity)
    commander_avatar = GameDataAccessHelper:GetFleetAvatar(id_commander, commander_level)
    commander_vessels = GameDataAccessHelper:GetCommanderVesselsImage(id_commander, commander_level, false)
    commander_vessels_type = GameDataAccessHelper:GetCommanderVesselsType(id_commander, commander_level)
    commander_color = GameDataAccessHelper:GetCommanderColorFrame(id_commander, commander_level)
    commander_sex = GameDataAccessHelper:GetCommanderSex(id_commander)
    if commander_sex == 1 then
      commander_sex = "man"
    elseif commander_sex == 0 then
      commander_sex = "woman"
    else
      commander_sex = "unknown"
    end
  elseif id_commander == 1 then
    commander_data = GameGlobalData:GetFleetInfo(1)
    commander_level = commander_data.level
    commanderType = GameGlobalData:GetCommanderTypeWithIdentity(self.mainFleetList[self.curMainfleetIndex])
    commander_avatar = GameDataAccessHelper:GetFleetAvatar(self.mainFleetList[self.curMainfleetIndex], commander_level)
    commander_vessels = GameDataAccessHelper:GetCommanderVesselsImage(self.mainFleetList[self.curMainfleetIndex], commander_level, false)
    commander_vessels_type = GameDataAccessHelper:GetCommanderVesselsType(self.mainFleetList[self.curMainfleetIndex], commander_level)
    commander_color = GameDataAccessHelper:GetCommanderColorFrame(self.mainFleetList[self.curMainfleetIndex], commander_level)
    commander_sex = GameDataAccessHelper:GetCommanderSex(self.mainFleetList[self.curMainfleetIndex])
    if commander_sex == 1 then
      commander_sex = "man"
    elseif commander_sex == 0 then
      commander_sex = "woman"
    else
      commander_sex = "unknown"
    end
  end
  DebugTable(commander_data)
  DebugOut("commander_vessels", commander_vessels)
  DebugOut("commander_vessels_type", commander_vessels_type)
  local dataTable = {}
  dataTable[#dataTable + 1] = commanderType
  dataTable[#dataTable + 1] = tempid
  dataTable[#dataTable + 1] = commander_avatar
  dataTable[#dataTable + 1] = commander_vessels
  dataTable[#dataTable + 1] = commander_vessels_type
  dataTable[#dataTable + 1] = commander_color
  dataTable[#dataTable + 1] = commander_sex
  local hasAdjutant = "false"
  if commander_data and commander_data.cur_adjutant and 0 < commander_data.cur_adjutant then
    hasAdjutant = "true"
  end
  dataTable[#dataTable + 1] = hasAdjutant
  local dataString = table.concat(dataTable, "\001")
  DebugOut("dataString:", dataString)
  if ext.setCommanderItemDataInFormationInC ~= nil then
    ext.setCommanderItemDataInFormationInC(self:GetFlashObject(), "battle", index_grid, dataString)
  else
    self:GetFlashObject():InvokeASCallback("_root", "setCommanderItemData", "battle", index_grid, dataString)
  end
  if not ingoreForce then
    self:UpdatePlayerPowerPoint()
  end
  GameObjectTeamLeagueAtkFormation:UpdateAdjutantBar()
end
function GameObjectTeamLeagueAtkFormation:UpdateRestCommanderGrid(index_grid, id_commander)
  self.m_freeFleets[index_grid] = id_commander
  local tempid = id_commander
  local commanderType = -1
  local commander_identity = -1
  local commander_avatar = -1
  local commander_vessels = -1
  local commander_vessels_type = -1
  local commander_level = 0
  local commander_color = "blue"
  local commander_sex = "unknown"
  local commander_data = GameGlobalData:GetFleetInfo(id_commander)
  if commander_data == nil then
    DebugOut("error: Rest id_commander")
  end
  if id_commander == 1 then
    id_commander = FleetTeamLeagueAtkMatrix:GetLeaderFleet() or 1
  end
  if id_commander > 0 and commander_data ~= nil then
    commander_identity = commander_data.identity
    commander_level = commander_data.level
    commander_color = GameDataAccessHelper:GetCommanderColorFrame(id_commander, commander_level)
    commanderType = GameGlobalData:GetCommanderTypeWithIdentity(commander_identity)
    commander_avatar = GameDataAccessHelper:GetFleetAvatar(id_commander, commander_level)
    commander_vessels = GameDataAccessHelper:GetCommanderVesselsImage(id_commander, commander_level, false)
    commander_vessels_type = GameDataAccessHelper:GetCommanderVesselsType(id_commander, commander_level)
    commander_sex = GameDataAccessHelper:GetCommanderSex(id_commander)
    if commander_sex == 1 then
      commander_sex = "man"
    elseif commander_sex == 0 then
      commander_sex = "woman"
    else
      commander_sex = "unknown"
    end
  end
  local dataTable = {}
  dataTable[#dataTable + 1] = commanderType
  dataTable[#dataTable + 1] = tempid
  dataTable[#dataTable + 1] = commander_avatar
  dataTable[#dataTable + 1] = commander_vessels
  dataTable[#dataTable + 1] = commander_vessels_type
  dataTable[#dataTable + 1] = commander_color
  dataTable[#dataTable + 1] = commander_sex
  local hasAdjutant = "false"
  if commander_data and commander_data.cur_adjutant and 0 < commander_data.cur_adjutant then
    hasAdjutant = "true"
  end
  dataTable[#dataTable + 1] = hasAdjutant
  local dataString = table.concat(dataTable, "\001")
  if ext.setCommanderItemDataInFormationInC ~= nil then
    ext.setCommanderItemDataInFormationInC(self:GetFlashObject(), "rest", index_grid, dataString)
  else
    self:GetFlashObject():InvokeASCallback("_root", "setCommanderItemData", "rest", index_grid, dataString)
  end
end
function GameObjectTeamLeagueAtkFormation:UpdateNextCommanderGrid(index_grid, id_commander, type)
  local commanderType = -1
  local commander_identity = -1
  local commander_avatar = -1
  local commander_vessels = -1
  local commander_vessels_type = -1
  local commander_level = 0
  local commander_color = "blue"
  local commander_sex = "unknown"
  local commander_data = GameGlobalData:GetFleetInfo(id_commander)
  if commander_data == nil then
    DebugOut("error: Next id_commander")
  end
  DebugOut("id_commander = " .. id_commander)
  if id_commander == 1 then
    id_commander = FleetTeamLeagueAtkMatrix:GetLeaderFleet() or 1
  end
  if id_commander > 0 and commander_data ~= nil then
    commander_identity = commander_data.identity
    commander_level = commander_data.level
    commander_color = GameDataAccessHelper:GetCommanderColorFrame(id_commander, commander_level)
    commanderType = GameGlobalData:GetCommanderTypeWithIdentity(commander_identity)
    commander_avatar = GameDataAccessHelper:GetFleetAvatar(id_commander, commander_level)
    commander_vessels = GameDataAccessHelper:GetCommanderVesselsImage(id_commander, commander_level, false)
    commander_vessels_type = GameDataAccessHelper:GetCommanderVesselsType(id_commander, commander_level)
    commander_sex = GameDataAccessHelper:GetCommanderSex(id_commander)
    if commander_sex == 1 then
      commander_sex = "man"
    elseif commander_sex == 0 then
      commander_sex = "woman"
    else
      commander_sex = "unknown"
    end
  end
  local dataTable = {}
  dataTable[#dataTable + 1] = commanderType
  dataTable[#dataTable + 1] = id_commander
  dataTable[#dataTable + 1] = commander_avatar
  dataTable[#dataTable + 1] = commander_vessels
  dataTable[#dataTable + 1] = commander_vessels_type
  dataTable[#dataTable + 1] = commander_color
  dataTable[#dataTable + 1] = commander_sex
  local hasAdjutant = "false"
  if commander_data and commander_data.cur_adjutant and 0 < commander_data.cur_adjutant then
    hasAdjutant = "true"
  end
  dataTable[#dataTable + 1] = hasAdjutant
  local dataString = table.concat(dataTable, "\001")
  if ext.setCommanderItemDataInFormationInC ~= nil then
    ext.setCommanderItemDataInFormationInC(self:GetFlashObject(), type, index_grid, dataString)
  else
    self:GetFlashObject():InvokeASCallback("_root", "setCommanderItemData", type, index_grid, dataString)
  end
end
function GameObjectTeamLeagueAtkFormation:SetEnemyGridData(index_grid, id_enemy)
  local obj_flash = self:GetFlashObject()
  local enemy_avatar = -1
  local enemy_vessels = -1
  local enemy_vessels_type = -1
  local enemy_color = "blue"
  local enmey_sex = "unknown"
  if not id_enemy or id_enemy <= 0 then
    id_enemy = -1
  else
    local commander_level = 0
    enemyColor = GameDataAccessHelper:GetCommanderColorFrame(id_enemy, commander_level)
    local enemyInfo = GameUIArena.enemyInfo
    enemy_avatar = GameDataAccessHelper:GetFleetAvatar(id_enemy, commander_level, enemyInfo.mSex)
    enemy_vessels = GameDataAccessHelper:GetCommanderVesselsImage(id_enemy, commander_level, false)
    enemy_vessels_type = GameDataAccessHelper:GetCommanderVesselsType(id_enemy, commander_level)
    enemy_color = GameDataAccessHelper:GetCommanderColorFrame(id_enemy, commander_level)
    enmey_sex = GameDataAccessHelper:GetCommanderSex(id_enemy)
    if enmey_sex == 1 then
      enmey_sex = "man"
    elseif enmey_sex == 0 then
      enmey_sex = "woman"
    else
      enmey_sex = "unknown"
    end
  end
  local dataTable = {}
  dataTable[#dataTable + 1] = -1
  dataTable[#dataTable + 1] = id_enemy
  dataTable[#dataTable + 1] = enemy_avatar
  dataTable[#dataTable + 1] = enemy_vessels
  dataTable[#dataTable + 1] = enemy_vessels_type
  dataTable[#dataTable + 1] = enemy_color
  dataTable[#dataTable + 1] = enmey_sex
  local adjust = "false"
  dataTable[#dataTable + 1] = adjust
  local isDamage = "false"
  dataTable[#dataTable + 1] = isDamage
  local dataString = table.concat(dataTable, "\001")
  if ext.setCommanderItemDataInFormationInC ~= nil then
    ext.setCommanderItemDataInFormationInC(self:GetFlashObject(), "enemy", index_grid, dataString)
  else
    self:GetFlashObject():InvokeASCallback("_root", "setCommanderItemData", "enemy", index_grid, dataString)
  end
end
function GameObjectTeamLeagueAtkFormation:OnLocateCommander(object_dragger)
  DebugOut("OnLocateCommander:")
  local located_type = object_dragger.DragToGridType
  local located_index = object_dragger.DragToGridIndex
  local located_src_commander = self:GetCommanderIDFromGrid(located_type, located_index)
  DebugOut("located_src_commander - ", located_src_commander)
  DebugOut("located_type - ", located_type)
  DebugOut("located_index - ", located_index)
  DebugOut("object_dragger.DraggedFleetID - ", object_dragger.DraggedFleetID)
  DebugOut("object_dragger.DraggedFleetGridType - ", object_dragger.DraggedFleetGridType)
  DebugOut("object_dragger.DraggedFleetGridIndex - ", object_dragger.DraggedFleetGridIndex)
  self:UpdateCommanderGrid(located_type, located_index, object_dragger.DraggedFleetID)
  if located_type == "battle" and object_dragger.DraggedFleetGridType == "battle" then
    FleetTeamLeagueAtkMatrix:SwapMatrix(self.currentMatrixIndex, located_index, object_dragger.DraggedFleetGridIndex)
  elseif located_type == "battle" then
    FleetTeamLeagueAtkMatrix:SetMatrix(self.currentMatrixIndex, located_index, object_dragger.DraggedFleetID)
  elseif object_dragger.DraggedFleetGridType == "battle" then
    FleetTeamLeagueAtkMatrix:SetMatrix(self.currentMatrixIndex, object_dragger.DraggedFleetGridIndex, located_src_commander)
  end
  if located_src_commander and located_src_commander > 0 then
    self:UpdateCommanderGrid(object_dragger.DraggedFleetGridType, object_dragger.DraggedFleetGridIndex, located_src_commander)
  end
  self:GetFlashObject():InvokeASCallback("_root", "OnCommanderDragOver")
  self.selected_commander = object_dragger.DraggedFleetID
  self:SelectCommander(object_dragger.DraggedFleetID)
  object_dragger.DraggedFleetID = nil
  object_dragger.DragToGridType = nil
  object_dragger.DragToGridIndex = nil
  object_dragger.DraggedFleetGridType = nil
  object_dragger.DraggedFleetGridIndex = nil
  self:UpdateBtnEquip()
end
function GameObjectTeamLeagueAtkFormation:OnBeginDragFleet(gameObjectFleetDragger)
  local draggedFleedID = gameObjectFleetDragger.DraggedFleetID
  DebugOut("OnBeginDragFleet ", draggedFleedID)
  local grid_type, grid_index = self:GetCommanderGridIndex(draggedFleedID)
  if grid_type == "rest" or grid_type == "battle" then
    gameObjectFleetDragger.DraggedFleetGridType = grid_type
    gameObjectFleetDragger.DraggedFleetGridIndex = grid_index
    self:UpdateCommanderGrid(grid_type, grid_index, -1)
  else
    assert(false)
  end
end
function GameObjectTeamLeagueAtkFormation:GetCommanderGridIndex(fleet_id)
  DebugOut(fleet_id)
  DebugOutPutTable(self.m_formationFleets, "formation")
  for index, id in pairs(self.m_formationFleets) do
    if id == fleet_id then
      return "battle", index
    end
  end
  DebugOutPutTable(self.m_freeFleets, "free")
  for index, id in pairs(self.m_freeFleets) do
    if id == fleet_id then
      return "rest", index
    end
  end
  return nil
end
function GameObjectTeamLeagueAtkFormation:GetCommanderIDFromGrid(grid_type, grid_index)
  local grid_list
  if grid_type == "rest" then
    grid_list = self.m_freeFleets
  elseif grid_type == "battle" then
    grid_list = self.m_formationFleets
  else
    assert(false)
  end
  return grid_list[grid_index]
end
function GameObjectTeamLeagueAtkFormation:CountBattleCommander()
  local count = 0
  for index, data in pairs(self.m_formationFleets) do
    if data > 0 then
      count = count + 1
    end
  end
  return count
end
function GameObjectTeamLeagueAtkFormation:GetBattleCommanderGridIndex(fleet_id)
  for index, id in pairs(self.m_formationFleets) do
    if id == fleet_id then
      return index
    end
  end
  return nil
end
function GameObjectTeamLeagueAtkFormation:Update(dt)
  if not GameStateTeamLeagueAtkFormation.GameObjectDraggerFleet:IsAutoMove() then
  end
  self:GetFlashObject():InvokeASCallback("_root", "UpdateFrame", dt)
  self:GetFlashObject():Update(dt)
end
function GameObjectTeamLeagueAtkFormation:GetCommanderLocatedInfo()
  local located_type, located_pos
  local located_info = self:GetFlashObject():InvokeASCallback("_root", "GetCommanderLocatedInfo")
  if located_info then
    local param_list = LuaUtils:string_split(located_info, ",")
    located_type = param_list[1]
    located_pos = param_list[2]
  end
  return located_type, located_pos
end
function GameObjectTeamLeagueAtkFormation:BattleNow()
  DebugOut("BattleNow")
  TeamLeagueCup:StartBattle()
end
function GameObjectTeamLeagueAtkFormation:GetCommanderGridPos(grid_type, grid_index)
  local grid_pos
  local pos_info = self:GetFlashObject():InvokeASCallback("_root", "GetCommanderGridPos", grid_type, grid_index)
  DebugOut(pos_info)
  if pos_info then
    local param_list = LuaUtils:string_split(pos_info, ",")
    grid_pos = {}
    grid_pos._x = tonumber(param_list[1])
    grid_pos._y = tonumber(param_list[2])
  end
  return grid_pos
end
function GameObjectTeamLeagueAtkFormation:OnFSCommand(cmd, args)
  DebugOut("GameObjectTeamLeagueAtkFormation:OnFSCommand", cmd, args)
  if cmd == "NeedUpdateFreeFleetInfo" then
    local fleetID = tonumber(args)
    local commander_data = GameGlobalData:GetFleetInfo(fleetID)
    self:GetFlashObject():InvokeASCallback("_root", "UpdateFreeFleetInfo", fleetID, GameDataAccessHelper:GetFleetAvatar(fleetID, commander_data.level))
  elseif cmd == "clicked_btn_close" then
    if GameObjectTeamLeagueAtkFormation:CheckAdjutantBeforeSaveCurrentMatrix() then
      DebugOut("CloseFormation 1")
      FleetTeamLeagueAtkMatrix:SaveMatrixChanged()
      GameObjectTeamLeagueAtkFormation:AnimationMoveOut()
      if GameStateTeamLeagueAtkFormation.basePrveState then
        GameStateTeamLeagueAtkFormation.m_prevState = GameStateTeamLeagueAtkFormation.basePrveState
        GameStateTeamLeagueAtkFormation.basePrveState = nil
      end
      GameStateManager:SetCurrentGameState(GameStateTeamLeagueAtkFormation.m_prevState)
    else
      DebugOut("CloseFormation 2")
    end
  elseif cmd == "OnMoveInAnimOver" then
    DebugOut("GameObjectTeamLeagueAtkFormation:OnMoveInAnimOver")
    if TutorialQuestManager.QuestTutorialTlcSwitchTeam:IsActive() then
      DebugOut("ShowQuestTutorialTlcSwitchTeam")
      self:GetFlashObject():InvokeASCallback("_root", "showTutorialSwitchTeam")
      GameUICommonDialog:ForcePlayStory({9903})
    end
  elseif cmd == "begin_drag" then
    local param = LuaUtils:string_split(args, ",")
    local fleetID = tonumber(param[1])
    local initPosX = tonumber(param[2])
    local initPosY = tonumber(param[3])
    local mouseX = tonumber(param[4])
    local mouseY = tonumber(param[5])
    local isInFreeList = param[6] == "true"
    local extraid = self.mainFleetList[self.curMainfleetIndex]
    DebugOut("extraid:", extraid)
    GameStateTeamLeagueAtkFormation:BeginDragFleet(fleetID, -1, initPosX, initPosY, mouseX, mouseY, extraid)
  elseif cmd == "select_commander_skill" then
    local id_fleet, id_skillgrid = unpack(LuaUtils:string_split(args, ","))
    self:SelectCommanderSkill(tonumber(id_fleet), tonumber(id_skillgrid))
  elseif cmd == "SelectFleet" then
    local fleetID = tonumber(args)
    self.m_currentSelectFleetId = fleetID
    self:UpdateCommanderSkillInfo(fleetID)
  elseif cmd == "select_commander" then
    local commander_id = tonumber(args)
    self:SelectCommander(commander_id)
    self:ShowMainFleetSelector(commander_id)
  elseif cmd == "battle_now" then
    if GameObjectTeamLeagueAtkFormation:CheckAdjutantBeforeSaveCurrentMatrix() then
      FleetTeamLeagueAtkMatrix:SaveMatrixChanged(function()
        GameObjectTeamLeagueAtkFormation:BattleLater()
      end)
    end
  elseif cmd == "locate_commander" then
    local param_list = LuaUtils:string_split(args, ",")
    self.locatedType = param_list[1]
    self.locatedPos = tonumber(param_list[2])
  elseif cmd == "arrow_L" then
    self:trySaveAndExchangeToMatrix(self.currentMatrixIndex, true)
  elseif cmd == "arrow_R" then
    self:trySaveAndExchangeToMatrix(self.currentMatrixIndex, false)
  elseif cmd == "teamMatrixButtonClicked" then
    if not GameObjectTeamLeagueAtkFormation:CheckAdjutantBeforeSaveCurrentMatrix() then
      self:GetFlashObject():InvokeASCallback("_root", "lua2fs_clickTeamIndexButton", self.currentMatrixIndex)
      return
    end
    DebugOut("currentMatrixIndex ", self.currentMatrixIndex)
    local index = tonumber(args)
    if index == self.currentMatrixIndex then
      self:GetFlashObject():InvokeASCallback("_root", "lua2fs_clickTeamIndexButton", self.currentMatrixIndex)
      return
    end
    local isMoveLeft = true
    if index < self.currentMatrixIndex then
      isMoveLeft = true
    else
      isMoveLeft = false
    end
    GameObjectTeamLeagueAtkFormation.mCurFreeMatrixSubIdx = 0
    self:loadNextMatrixAndRestFleets(index)
    self:setNextMatrixAndRestFleetsUI(isMoveLeft)
    self:ShrinkSkillSelector()
    self:GetFlashObject():InvokeASCallback("_root", "lua2fs_clickTeamIndexButton", index)
    if TutorialQuestManager.QuestTutorialTlcSwitchTeam:IsActive() then
      DebugOut("ShowQuestTutorialTlcSwitchTeam")
      TutorialQuestManager.QuestTutorialTlcSwitchTeam:SetFinish(true)
      self:GetFlashObject():InvokeASCallback("_root", "hideTutorialSwitchTeam")
      GameUICommonDialog:ForcePlayStory({9904})
    end
    FleetTeamLeagueAtkMatrix:SaveMatrixChanged()
    FleetTeamLeagueAtkMatrix:SwitchTeamReq(index)
  elseif cmd == "move_prevRest_over" or cmd == "move_prev_over" or cmd == "move_nextRest_over" or cmd == "move_next_over" then
    self.m_formationFleets = LuaUtils:table_rcopy(self.m_nextTeamMatrix)
    self.m_freeFleets = LuaUtils:table_rcopy(self.m_nextPlayerRestFleets)
    for i = 1, 9 do
      local commander_id = self.m_formationFleets[i]
      if commander_id == nil then
        commander_id = -1
      end
      self:UpdateBattleCommanderGrid(i, commander_id, true)
    end
    for i = 1, 9 do
      local commander_id = self.m_freeFleets[i]
      if commander_id == nil then
        commander_id = -1
      end
      self:UpdateRestCommanderGrid(i, commander_id)
    end
    self:GetFlashObject():InvokeASCallback("_root", "lua2fs_updateLockBtn")
    self:SelectCommander(1)
    GameObjectTeamLeagueAtkFormation:CheckAdjutantBeforeSaveCurrentMatrix()
  elseif cmd == "rest_list_move_prev_over" or cmd == "rest_list_move_next_over" then
    self.m_freeFleets = LuaUtils:table_rcopy(self.m_nextPlayerRestFleets)
    for i = 1, 9 do
      local commander_id = self.m_freeFleets[i]
      if commander_id == nil then
        commander_id = -1
      end
      self:UpdateRestCommanderGrid(i, commander_id)
    end
    self:SelectCommander(1)
  elseif cmd == "choose_Item" then
    local curr_id = tonumber(args)
    self.curMainfleetIndex = tonumber(args)
    for k, v in ipairs(self.mainFleetList) do
      DebugOut("v:" .. v)
      if v == curr_id then
        self.curMainfleetIndex = k
      end
    end
    self:ExpandSkillSelectorNew(tonumber(args))
  elseif cmd == "setMainFleet" then
    self:setMainFleet()
  elseif cmd == "adjutantClicked" then
    if GameObjectTeamLeagueAtkFormation:CheckAdjutantBeforeSaveCurrentMatrix() then
      local function saveSuccessCallback()
        local function exitAdjutantUICallback()
          GameObjectTeamLeagueAtkFormation:RefreshBattleAndRestGrid()
          GameObjectTeamLeagueAtkFormation:UpdateAdjutantBar()
          GameObjectTeamLeagueAtkFormation:CheckAdjutantBeforeSaveCurrentMatrix()
        end
        GameUIAdjutant.mFleetIdBeforeEnter = 1
        GameUIAdjutant:EnterAdjutantUI(exitAdjutantUICallback, 1)
      end
      FleetTeamLeagueAtkMatrix:SaveMatrixChanged(saveSuccessCallback)
    end
  elseif cmd == "enterEquip" then
    FleetTeamLeagueAtkMatrix:SaveMatrixChanged()
    local GameStateEquipEnhance = GameStateManager.GameStateEquipEnhance
    if GameStateTeamLeagueAtkFormation.basePrveState == nil then
      GameStateTeamLeagueAtkFormation.basePrveState = GameStateTeamLeagueAtkFormation.m_prevState
    end
    GameStateEquipEnhance:EnterTeamLeagueEquipEnhance(TEAMLEAGUE_MATRIX_ATTACK, self.currentMatrixIndex)
  end
end
function GameObjectTeamLeagueAtkFormation:BattleLater()
  GameObjectTeamLeagueAtkFormation:BattleNow()
end
function GameObjectTeamLeagueAtkFormation:InitUI()
  self:GetFlashObject():InvokeASCallback("_root", "chooseTeam", self.currentMatrixIndex)
  self:GetFlashObject():InvokeASCallback("_root", "hideBattleTipAnimation")
  self:GetFlashObject():InvokeASCallback("_root", "setEnemyFog", true)
  self:UpdateBtnEquip()
  DebugOut("in initui")
  self:UpdateEnemyInfo()
  self:UpdatePlayerInfo()
  self:UpdatePlayerPowerPoint()
  self:SelectCommander(1)
  self:AnimationMoveIn()
end
function GameObjectTeamLeagueAtkFormation:setNextMatrixAndRestFleetsUI(isLeft, isSubRest)
  if isLeft then
    for i = 1, 9 do
      local commander_id = self.m_nextTeamMatrix[i]
      if commander_id == nil then
        commander_id = -1
      end
      self:UpdateNextCommanderGrid(i, commander_id, "yours_L")
    end
    for i = 1, 9 do
      local commander_id = self.m_nextPlayerRestFleets[i]
      if commander_id == nil then
        commander_id = -1
      end
      local listType = "rest_list_L"
      if isSubRest then
        listType = "rest_list_L2"
      end
      self:UpdateNextCommanderGrid(i, commander_id, listType)
    end
  else
    for i = 1, 9 do
      local commander_id = self.m_nextTeamMatrix[i]
      if commander_id == nil then
        commander_id = -1
      end
      self:UpdateNextCommanderGrid(i, commander_id, "yours_R")
    end
    for i = 1, 9 do
      local commander_id = self.m_nextPlayerRestFleets[i]
      if commander_id == nil then
        commander_id = -1
      end
      local listType = "rest_list_R"
      if isSubRest then
        listType = "rest_list_R2"
      end
      self:UpdateNextCommanderGrid(i, commander_id, listType)
    end
  end
  self:UpdateBtnEquip()
  self:GetFlashObject():InvokeASCallback("_root", "HideAllSelectRound")
end
function GameObjectTeamLeagueAtkFormation:loadNextMatrixAndRestFleets(index, subIndex)
  DebugOut("GameObjectTeamLeagueAtkFormation loadNextMatrixAndRestFleets")
  self.currentMatrixIndex = index
  self.m_nextTeamMatrix = {}
  for i = 1, 9 do
    self.m_nextTeamMatrix[i] = 0
  end
  self.m_nextPlayerRestFleets = {}
  local nextMatrix = FleetTeamLeagueAtkMatrix:GetMatrixByIndex(index).cells
  for _, v in pairs(nextMatrix) do
    self.m_nextTeamMatrix[v.cell_id] = v.fleet_identity
  end
  self.m_nextPlayerRestFleets = FleetTeamLeagueAtkMatrix:GetFreeFleets()
  if subIndex then
    local pageCnt = math.ceil((#self.m_nextPlayerRestFleets + 1) / 9)
    DebugOut("pageCnt " .. tostring(pageCnt) .. " " .. tostring(GameObjectTeamLeagueAtkFormation.mCurFreeMatrixSubIdx))
    GameObjectTeamLeagueAtkFormation.mCurFreeMatrixSubIdx = (GameObjectTeamLeagueAtkFormation.mCurFreeMatrixSubIdx + subIndex + pageCnt) % pageCnt
    local subRestFleets = {}
    for k = 1, 9 do
      if self.m_nextPlayerRestFleets[k + 9 * GameObjectTeamLeagueAtkFormation.mCurFreeMatrixSubIdx] then
        subRestFleets[k] = self.m_nextPlayerRestFleets[k + 9 * GameObjectTeamLeagueAtkFormation.mCurFreeMatrixSubIdx]
      else
        subRestFleets[k] = -1
      end
    end
    self.m_nextPlayerRestFleets = subRestFleets
  end
end
function GameObjectTeamLeagueAtkFormation:trySaveAndExchangeToMatrix(index, isleft)
  DebugOut("trySaveAndExchangeToMatrix", index)
  FleetTeamLeagueAtkMatrix:SaveMatrixChanged()
  local subIndex = 1
  if isleft then
    subIndex = -1
  end
  self:loadNextMatrixAndRestFleets(index, subIndex)
  self:setNextMatrixAndRestFleetsUI(isleft, true)
  if isleft then
    self:GetFlashObject():InvokeASCallback("_root", "RestFormationMoveToPrev")
  else
    self:GetFlashObject():InvokeASCallback("_root", "RestFormationMoveToNext")
  end
end
function GameObjectTeamLeagueAtkFormation:RefreshBattleAndRestGrid()
  for i = 1, 9 do
    local commander_id = self.m_formationFleets[i]
    if commander_id == nil then
      commander_id = -1
    end
    self:UpdateBattleCommanderGrid(i, commander_id)
  end
  for i = 1, 9 do
    local commander_id = self.m_freeFleets[i]
    if commander_id == nil then
      commander_id = -1
    end
    self:UpdateRestCommanderGrid(i, commander_id)
  end
  self:SelectCommander(1)
end
function GameObjectTeamLeagueAtkFormation:GetMajorIdByAdjutantId(adjutantId)
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  for k, v in pairs(fleets) do
    if v.cur_adjutant == adjutantId then
      return v.identity
    end
  end
  return -1
end
function GameObjectTeamLeagueAtkFormation:GetAdjutantCount(matrix)
  local cnt = 0
  for k, v in pairs(matrix) do
    local fleet = GameGlobalData:GetFleetInfo(v)
    if fleet and 0 < fleet.cur_adjutant then
      cnt = cnt + 1
    end
  end
  return cnt
end
function GameObjectTeamLeagueAtkFormation:IsFleetOnMatrix(matrix, fleetId)
  for k, v in pairs(matrix) do
    local fleet = GameGlobalData:GetFleetInfo(v)
    if fleet and fleet.identity == fleetId then
      return true
    end
  end
  return false
end
function GameObjectTeamLeagueAtkFormation:CheckAdjutant(src, des, draggedFleedId, fromIndex, toIndex)
  DebugOutPutTable(self.m_formationFleets, "battle_matrix")
  DebugOutPutTable(self.m_freeFleets, "rest_matrix")
  local adjutantMaxCnt = GameGlobalData:GetData("adjutant_max_count")
  DebugOut("adjutantMaxCnt " .. tostring(adjutantMaxCnt))
  local srcMatrix
  if src == "battle" then
    srcMatrix = self.m_formationFleets
  else
    srcMatrix = self.m_freeFleets
  end
  local destMatrix
  if des == "battle" then
    destMatrix = self.m_formationFleets
  else
    destMatrix = self.m_freeFleets
  end
  local destFleetId = destMatrix[toIndex]
  if nil == destFleetId then
    destFleetId = -1
  end
  local newBattleMatrix = {}
  for k, v in pairs(self.m_formationFleets) do
    newBattleMatrix[k] = v
  end
  if src == des then
    return 0
  elseif src == "battle" and des == "rest" and destFleetId == -1 then
    return 0
  elseif destFleetId > 0 then
    local addFleetId = -1
    if src == "battle" and des == "rest" then
      addFleetId = destFleetId
      newBattleMatrix[fromIndex] = destFleetId
    else
      addFleetId = draggedFleedId
      newBattleMatrix[toIndex] = draggedFleedId
    end
    if adjutantMaxCnt < GameObjectTeamLeagueAtkFormation:GetAdjutantCount(newBattleMatrix) then
      return -1
    else
      local majorId = GameObjectTeamLeagueAtkFormation:GetMajorIdByAdjutantId(addFleetId)
      if majorId > 0 then
        return 1, majorId, addFleetId
      else
        local fleetInfo = GameGlobalData:GetFleetInfo(addFleetId)
        if GameObjectTeamLeagueAtkFormation:IsFleetOnMatrix(newBattleMatrix, fleetInfo.cur_adjutant) then
          return 1, addFleetId, fleetInfo.cur_adjutant
        else
          return 0
        end
      end
    end
  elseif src == "rest" and des == "battle" then
    newBattleMatrix[toIndex] = draggedFleedId
    if adjutantMaxCnt < GameObjectTeamLeagueAtkFormation:GetAdjutantCount(newBattleMatrix) then
      return -1
    end
    local majorId = GameObjectTeamLeagueAtkFormation:GetMajorIdByAdjutantId(draggedFleedId)
    if majorId > 0 then
      return 1, majorId, draggedFleedId
    else
      local fleetInfo = GameGlobalData:GetFleetInfo(draggedFleedId)
      if GameObjectTeamLeagueAtkFormation:IsFleetOnMatrix(newBattleMatrix, fleetInfo.cur_adjutant) then
        return 1, draggedFleedId, fleetInfo.cur_adjutant
      else
        return 0
      end
    end
  end
end
function GameObjectTeamLeagueAtkFormation:CheckAdjutantBeforeSaveCurrentMatrix(onlyForResult)
  local adjutantMaxCnt = GameGlobalData:GetData("adjutant_max_count")
  if adjutantMaxCnt < GameObjectTeamLeagueAtkFormation:GetAdjutantCount(self.m_formationFleets) then
    DebugOut("Out of max count adjutant can battle.")
    GameTip:Show(GameLoader:GetGameText("LC_MENU_BRIDGE_INFO_4"), 3000)
    return false
  end
  local matrixFleets = FleetTeamLeagueAtkMatrix:GetMatrixFleets()
  DebugOutPutTable(matrixFleets, "wawaaawawa")
  for k, v in pairs(matrixFleets) do
    if v > 0 then
      do
        local majorId = GameObjectTeamLeagueAtkFormation:GetMajorIdByAdjutantId(v)
        if majorId > 0 then
          if nil == onlyForResult or not onlyForResult then
            do
              local tip = GameLoader:GetGameText("LC_MENU_BRIDGE_INFO_3")
              DebugOut("tip " .. tip)
              local adjutantName = GameDataAccessHelper:GetFleetLevelDisplayName(v)
              local majorName = GameDataAccessHelper:GetFleetLevelDisplayName(majorId)
              tip = string.gsub(tip, "<npc2_name>", adjutantName)
              tip = string.gsub(tip, "<npc1_name>", majorName)
              local function releaseAdjutantResultCallback(success)
                DebugOut("releaseAdjutantResultCallback " .. tostring(success))
                if success then
                  GameObjectTeamLeagueAtkFormation:UpdateAdjutantBar()
                  GameObjectTeamLeagueAtkFormation:UpdateBattleCommanderByFleetId(majorId)
                  GameObjectTeamLeagueAtkFormation:CheckAdjutantBeforeSaveCurrentMatrix()
                else
                end
              end
              local function okCallback()
                GameStateTeamLeagueAtkFormation:RequestReleaseAdjutant(majorId, releaseAdjutantResultCallback)
              end
              local cancelCallback = function()
              end
              GameUIGlobalScreen:ShowMessageBox(2, "", tip, okCallback, cancelCallback)
            end
          end
          return false
        end
      end
    end
  end
  return true
end
function GameObjectTeamLeagueAtkFormation:UpdateAdjutantBar()
  local flashObj = self:GetFlashObject()
  if flashObj then
    local adjutantCnt = 0
    local adjutantAvatarFrs = ""
    local adjutantbgColor = ""
    for k, v in pairs(self.m_formationFleets) do
      local commander_data2 = GameGlobalData:GetFleetInfo(v)
      local level = 0
      if v > 0 and commander_data2 ~= nil and commander_data2.cur_adjutant and commander_data2.cur_adjutant > 1 then
        if commander_data2.level then
          level = commander_data2.level
        end
        adjutantAvatarFrs = adjutantAvatarFrs .. GameDataAccessHelper:GetFleetAvatar(commander_data2.cur_adjutant) .. "\001"
        adjutantbgColor = adjutantbgColor .. GameDataAccessHelper:GetCommanderColorFrame(commander_data2.cur_adjutant, commander_data2.adjutant_level) .. "\001"
        adjutantCnt = adjutantCnt + 1
      end
    end
    local adjutantUnlockCnt = GameGlobalData:GetData("adjutant_max_count")
    DebugOut("SetAdjutantBar " .. tostring(adjutantCnt) .. " " .. adjutantAvatarFrs .. " " .. adjutantbgColor)
    flashObj:InvokeASCallback("_root", "SetAdjutantBar", adjutantUnlockCnt, adjutantCnt, adjutantAvatarFrs, adjutantbgColor)
  end
end
function GameObjectTeamLeagueAtkFormation:UpdateBattleCommanderByFleetId(fleetId)
  for k, v in pairs(self.m_formationFleets) do
    if fleetId == v then
      GameObjectTeamLeagueAtkFormation:UpdateCommanderGrid("battle", k, fleetId)
      break
    end
  end
end
function GameObjectTeamLeagueAtkFormation:UpdateBtnEquip()
  local bEmptyTeam = FleetTeamLeagueAtkMatrix:IsEmptyTeam(self.currentMatrixIndex)
  self:GetFlashObject():InvokeASCallback("_root", "UpdateBtnEquip", bEmptyTeam)
end
