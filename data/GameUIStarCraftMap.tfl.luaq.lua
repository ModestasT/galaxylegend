local GameUIStarCraftMap = LuaObjectManager:GetLuaObject("GameUIStarCraftMap")
local GameStateStarCraft = GameStateManager.GameStateStarCraft
local GameVip = LuaObjectManager:GetLuaObject("GameVip")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameUIChat = LuaObjectManager:GetLuaObject("GameUIChat")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameUIBarLeft = LuaObjectManager:GetLuaObject("GameUIBarLeft")
local GameVipDetailInfoPanel = LuaObjectManager:GetLuaObject("GameVipDetailInfoPanel")
local GameUIStarCraftDetail = require("data1/GameUIStarCraftDetail.tfl")
require("GameUIStarCraftCalendar.tfl")
local m_chatMsgQueue = {}
local m_initEnter = true
GameUIStarCraftMap.updateStep = 1
GameUIStarCraftMap.updateDT = 0
GameUIStarCraftMap.currentSeason = {}
GameUIStarCraftMap.currentShowSeason = {}
GameUIStarCraftMap.allSeason = {}
GameUIStarCraftMap.firstTimeEnterChat = true
GameUIStarCraftMap.mLastKeyPointIDKingIn = -1
GameUIStarCraftMap.mKingUserID = -1
GameUIStarCraftMap.mIncomeBuffLeftTime = 0
GameUIStarCraftMap.mIncomeBuffLeftTimeFetchTime = 0
GameUIStarCraftMap.mIsIncomeBuffLeftTimeShow = false
GameUIStarCraftMap.mCurIncomeBuffLeftTime = 0
GameUIStarCraftMap.mCalendar = nil
GameUIStarCraftMap.mCollectAll = false
EKeyPointType = {
  Type_Normal = 0,
  Type_Camp = 1,
  Type_Target = 2,
  Type_White_Hole = 3,
  Type_Boat_Yard = 4,
  Type_Missile = 5,
  Type_Point_Level_1 = 10,
  Type_Point_Level_2 = 11,
  Type_Point_Level_3 = 12
}
local MapKeyPoint = {
  mID,
  mType,
  mPosition = {mX = -1, mY = -1},
  mRelationShip,
  mArea,
  mBuffList,
  mPointName,
  mCuttentState,
  mServerID,
  mOccupyPlayerList,
  mHasResources = false,
  mHasKing = false,
  mAllPower = 0,
  mCurrentActiveEvent = nil
}
function MapKeyPoint:new()
  local instance = {}
  setmetatable(instance, self)
  self.__index = self
  return instance
end
local BuffIdToLocalizationId = {
  [1] = "LC_MENU_TC_BUFF_1_DESC",
  [2] = "LC_MENU_TC_BUFF_2_DESC",
  [3] = "LC_MENU_TC_BUFF_3_DESC",
  [4] = "LC_MENU_TC_BUFF_4_DESC",
  [5] = "LC_MENU_TC_BUFF_5_DESC",
  [6] = "LC_MENU_TC_BUFF_6_DESC"
}
local RESOURCE_ID = 1
local Dynamic_Buff_Type = {Dynamic_Special_Buff = 1, Dynamic_Boat_Yard_Buff = 2}
local EBuff_Type = {
  BUFF_EFFECT = 1,
  BUFF_RESOURCE = 2,
  BUFF_ITEM = 3,
  BUFF_PRODUCTION_SPEED_UP = 4,
  BUFF_ATTRIBUTE_PER_SECONDS = 5,
  BUFF_BOAT_YARD = 6
}
local MapKeyPointBuff = {
  mID,
  mBuffDetailItems,
  mDisplayString,
  mDisplayFrame,
  mLeftTime,
  mValue
}
function MapKeyPointBuff:new()
  local instance = {}
  setmetatable(instance, self)
  self.__index = self
  return instance
end
function MapKeyPointBuff:GetDisplayerString()
  if not self.mDisplayString then
    local locID = "LC_MENU_TC_BUFF_" .. self.mID .. "_DESC"
    self.mDisplayString = GameLoader:GetGameText(locID)
    if self.mID >= 10 and self.mID <= 30 then
      local buffItem = self.mBuffDetailItems[1]
      local keyLocID = "LC_MENU_" .. string.upper(buffItem.key) .. "_CHAR"
      DebugOut("local id = ", keyLocID)
      local key = GameLoader:GetGameText(keyLocID)
      DebugOut("key = ", key)
      self.mDisplayString = string.format(self.mDisplayString, key, buffItem.limit_value)
    elseif self.mID > 30 and self.mID <= 50 then
      local buffItem = self.mBuffDetailItems[1]
      local itemText = GameHelper:GetAwardTypeText(buffItem.key, buffItem.type_id)
      self.mDisplayString = string.format(self.mDisplayString, itemText, buffItem.limit_value)
    elseif self.mID == 301 then
      local locID = "LC_MENU_TC_DOCK_BUFF_PARAM_" .. self.mKey
      self.mDisplayString = GameLoader:GetGameText(locID)
    end
  end
  return self.mDisplayString
end
function MapKeyPointBuff:GetDisplayFrame()
  if not self.mDisplayFrame then
    if self.mID == 301 then
      self.mDisplayFrame = "tc_4"
    elseif self.mID > 200 and self.mID <= 250 then
      self.mDisplayFrame = "tc_" .. self.mID
    elseif self.mID >= 10 and self.mID <= 200 then
      local buffItem = self.mBuffDetailItems[1]
      if DynamicResDownloader:IsDynamicStuffByBuffItem(buffItem) then
        self.mDisplayFrame = "item_" .. buffItem.type_id
      elseif buffItem.key == "fleet" then
        frame = GameDataAccessHelper:GetFleetAvatar(buffItem.type_id, 0)
      else
        self.mDisplayFrame = GameHelper:GetAwardTypeIconFrameName(buffItem.key, buffItem.type_id, 1)
      end
    else
      self.mDisplayFrame = "tc_" .. self.mIcon
    end
  end
  return self.mDisplayFrame
end
function MapKeyPointBuff:GetType()
  if self.mID < 10 then
    return EBuff_Type.BUFF_EFFECT
  elseif self.mID >= 10 and self.mID <= 30 then
    return EBuff_Type.BUFF_RESOURCE
  elseif self.mID > 30 and self.mID <= 50 then
    return EBuff_Type.BUFF_ITEM
  elseif self.mID > 200 and self.mID <= 250 then
    return EBuff_Type.BUFF_PRODUCTION_SPEED_UP
  elseif self.mID > 250 and self.mID <= 300 then
    return EBuff_Type.BUFF_ATTRIBUTE_PER_SECONDS
  elseif self.mID == 301 then
    return EBuff_Type.BUFF_BOAT_YARD
  end
  return 1
end
local MapKeyPointPlayer = {
  mPlayerID,
  mPlayerName,
  mPlayerLevel,
  mPlayerHP,
  mPlayerAllianceID,
  mBattleInfo
}
function MapKeyPointPlayer:new()
  local instance = {}
  setmetatable(instance, self)
  self.__index = self
  return instance
end
local MapPath = {
  mID,
  mStartPoint,
  mEndPoint,
  mDistance
}
function MapPath:new()
  local instance = {}
  setmetatable(instance, self)
  self.__index = self
  return instance
end
local MapPlayerMySelf = {
  mID = -1,
  mServerID = -1,
  mCurrentHP = -1,
  mMaxHP = -1,
  mCurrentKeyPointID = -1,
  mCurrentPathID = -1,
  mCurrentStartPointID = -1,
  mCurrentEndPointID = -1,
  mIsOnPath = false,
  mLeftTime = 0,
  mNotifyGetTime = 0,
  mInited = false,
  mBuffList = {},
  mStableBuffList = {},
  mIsWaitingRevive = false,
  mCurrentPathList = nil,
  mCurrentStartTeleportID = -1,
  mCurrentEndTeleportID = -1,
  mSpeed,
  mKilledBy,
  mHasDynamicBuff = false,
  mProductionSpeedUp = -1,
  mDynamicBuffGetTime = 0,
  mBoatBuffGetTime = 0,
  mIsKing = false
}
local MapPlayer = {
  mID,
  mPlayerName,
  mServerID,
  mStartPoint,
  mEndPoint,
  mPathID,
  mSpeed,
  mTimeStamp,
  mCurrentPos
}
function MapPlayer:new()
  local instance = {}
  setmetatable(instance, self)
  self.__index = self
  return instance
end
GameUIStarCraftMap.MapKeyPointList = {}
GameUIStarCraftMap.MapPathList = {}
GameUIStarCraftMap.MapPlayerList = {}
GameUIStarCraftMap.ServerColorFrame = {}
GameUIStarCraftMap.ServerName = {}
GameUIStarCraftMap.serverIDList = {}
GameUIStarCraftMap.ColorList = {
  "red",
  "purple",
  "orange",
  "deongaree"
}
GameUIStarCraftMap.MapBuffList = {}
GameUIStarCraftMap.mCurClickedPoint = nil
local updateMarchTimeFrame = 0
local updateReviveTimeFrame = 0
local UPDATE_TIME_FRAME = 15
local currentMarchLeftTime = 0
local currentReviveLeftTime = 0
local currentStarcraftLeftTime = 0
function GameUIStarCraftMap:OnAddToGameState()
  if not self:GetFlashObject() then
    self:LoadFlashObject()
    if GameUIStarCraftMap.mCalendar then
      GameUIStarCraftMap.mCalendar:SetFlashObj(self:GetFlashObject())
    end
  end
  GameUIBarLeft:SetServerTimeChangehandler(GameUIStarCraftMap.ServerTimeChangeHandler)
  self:Init()
  self:MoveIn()
  GameUIStarCraftDetail:Init(self:GetFlashObject())
  GameUIStarCraftMap.updateStep = 1
  GameUIStarCraftMap.updateDT = 0
  if GameUIStarCraftMap.mCalendar == nil then
    local flash = self:GetFlashObject()
    GameUIStarCraftMap.mCalendar = GameUIStarCraftCalendar.new(flash)
  end
  GameGlobalData:RegisterDataChangeCallback("newChatTotalCount", GameUIStarCraftMap.UpdateChatButtonStatus)
  local chatCount = GameGlobalData:GetData("newChatTotalCount")
  if chatCount and chatCount > 0 then
    self:GetFlashObject():InvokeASCallback("_root", "lua2fs_ShowOrangeChatBtn", true)
  end
end
function GameUIStarCraftMap:OnEraseFromGameState()
  self:UnloadFlashObject()
  collectgarbage("collect")
  if GameStateStarCraft.nextState == GameStateManager.GameStateMainPlanet then
    GameUIStarCraftMap:ResetPlayerMySelf()
  end
  GameGlobalData:RemoveDataChangeCallback("newChatTotalCount", GameUIStarCraftMap.UpdateChatButtonStatus)
end
function GameUIStarCraftMap:Init()
  GameUIStarCraftMap.mIsIncomeBuffLeftTimeShow = false
  DebugOut("m_initEnter")
  self:GetFlashObject():InvokeASCallback("_root", "updateChatBarText", "")
  m_initEnter = false
  DebugOut("GameStateStarCraft.lastState = ", GameStateStarCraft.lastState)
  if GameStateStarCraft.lastState ~= GameStateManager.GameStateMainPlanet then
    GameUIStarCraftMap:BuildMap()
    GameUIStarCraftMap:RequestAllNtf()
    GameUIStarCraftMap:RefreshCalenderBtn()
    self:GetFlashObject():InvokeASCallback("_root", "init", GameGlobalData.starCraftData.type)
    return
  end
  self:GetFlashObject():InvokeASCallback("_root", "init", GameGlobalData.starCraftData.type)
  updateMarchTimeFrame = 0
  updateReviveTimeFrame = 0
  currentMarchLeftTime = 0
  currentReviveLeftTime = 0
  currentStarcraftLeftTime = 0
  MapPlayerMySelf.mInited = false
  self:RegisterAllMsgHandler()
  self:OnRegesterAvatarInfoChangeHandler()
  self:RequestMapData()
  GameUIStarCraftMap:RequestTeleportPrice()
  GameStateStarCraft:RequestRevivePrice()
  GameUIStarCraftMap:RequestAllNtf()
  local stateStr = GameLoader:GetGameText("LC_MENU_TC_STATUS_REST_LABEL")
  GameUIStarCraftMap.OnGlobalUserInfoChange()
  GameUIStarCraftMap.OnGlobalLevelInfoChange()
end
function GameUIStarCraftMap:RequestTeleportPrice()
  local price_content = {
    price_type = "tc_base_jump",
    type = 0
  }
  NetMessageMgr:SendMsg(NetAPIList.price_req.Code, price_content, GameUIStarCraftMap.RequestTeleportPriceCallback, false, nil)
end
function GameUIStarCraftMap.RequestTeleportPriceCallback(msgType, content)
  if msgType == NetAPIList.price_ack.Code and content.price_type == "tc_base_jump" then
    GameStateStarCraft.mTeleportPrice = content.price
    return true
  end
  return false
end
function GameStateStarCraft:RequestRevivePrice()
  local price_content = {
    price_type = "tc_revivial_cost",
    type = 0
  }
  NetMessageMgr:SendMsg(NetAPIList.price_req.Code, price_content, GameUIStarCraftMap.RequestRevivePriceCallback, false, nil)
end
function GameUIStarCraftMap.RequestRevivePriceCallback(msgType, content)
  if msgType == NetAPIList.price_ack.Code and content.price_type == "tc_revivial_cost" then
    GameStateStarCraft.mRevivePrice = content.price
    return true
  end
  return false
end
function GameUIStarCraftMap:ClearMap()
  GameUIStarCraftMap.MapKeyPointList = {}
  GameUIStarCraftMap.MapPathList = {}
  GameUIStarCraftMap.MapPlayerList = {}
  GameUIStarCraftMap.MapBuffList = {}
end
function GameUIStarCraftMap.Reconnected()
  DebugOut("reconnect and send tc ntf")
  GameUIStarCraftMap:ResetPlayerMySelf()
  GameUIStarCraftMap:CleanUpMySelfPath()
  local flash_obj = GameUIStarCraftMap:GetFlashObject()
  if flash_obj then
    flash_obj:InvokeASCallback("_root", "killAllPlayer")
  end
  GameUIStarCraftMap:RequestAllNtf()
end
function GameUIStarCraftMap:RequestAllNtf()
  local content = {
    notifies = {
      "contention_info",
      "contention_station",
      "contention_trip",
      "contention_name",
      "contention_production",
      "contention_missiles"
    }
  }
  NetMessageMgr:SendMsg(NetAPIList.contention_notifies_req.Code, content, nil, false, nil)
end
function GameUIStarCraftMap:RegisterAllMsgHandler()
  NetMessageMgr:RegisterMsgHandler(NetAPIList.contention_info_ntf.Code, GameUIStarCraftMap.MyInfoChangedNtfHandler)
  NetMessageMgr:RegisterMsgHandler(NetAPIList.contention_trip_ntf.Code, GameUIStarCraftMap.MapPlayermarchChangedNtfHandler)
  NetMessageMgr:RegisterMsgHandler(NetAPIList.contention_station_ntf.Code, GameUIStarCraftMap.MapPlayerKeyPointStateChangedNtfHandler)
  NetMessageMgr:RegisterMsgHandler(NetAPIList.contention_battle_ntf.Code, GameUIStarCraftMap.MapPlayerBattleNtfHandler)
  NetMessageMgr:RegisterMsgHandler(NetAPIList.contention_name_ntf.Code, GameUIStarCraftMap.ServerDisplayNameChangeNtfHandler)
  NetMessageMgr:RegisterMsgHandler(NetAPIList.contention_production_ntf.Code, GameUIStarCraftMap.ProductionChangeNtfHandler)
  NetMessageMgr:RegisterMsgHandler(NetAPIList.contention_jump_ntf.Code, GameUIStarCraftMap.TeleportNtfHandler)
  NetMessageMgr:RegisterReconnectHandler(GameUIStarCraftMap.Reconnected)
  NetMessageMgr:RegisterMsgHandler(NetAPIList.contention_king_change_ntf.Code, GameUIStarCraftMap.KingChangeNtfHandler)
  NetMessageMgr:RegisterMsgHandler(NetAPIList.contention_missiles_ntf.Code, GameUIStarCraftMap.FireMissilesNtfHandler)
end
function GameUIStarCraftMap:RemoveMessageHandler()
  NetMessageMgr:SendMsg(NetAPIList.contention_leave_req.Code, nil, nil, false, nil)
  GameGlobalData:RemoveDataChangeCallback("newChatTotalCount", GameUIStarCraftMap.UpdateChatButtonStatus)
  NetMessageMgr:RemoveMsgHandler(NetAPIList.contention_info_ntf.Code)
  NetMessageMgr:RemoveMsgHandler(NetAPIList.contention_trip_ntf.Code)
  NetMessageMgr:RemoveMsgHandler(NetAPIList.contention_station_ntf.Code)
  NetMessageMgr:RemoveMsgHandler(NetAPIList.contention_battle_ntf.Code)
  NetMessageMgr:RemoveMsgHandler(NetAPIList.contention_name_ntf.Code)
  NetMessageMgr:RemoveMsgHandler(NetAPIList.contention_production_ntf.Code)
  NetMessageMgr:RemoveMsgHandler(NetAPIList.contention_jump_ntf.Code)
  NetMessageMgr:RemoveMsgHandler(NetAPIList.contention_king_change_ntf.Code)
  NetMessageMgr:RemoveMsgHandler(NetAPIList.contention_missiles_ntf.Code)
end
function GameUIStarCraftMap:MoveIn()
  self:MoveInPlayerAvata()
  local flash_obj = GameUIStarCraftMap:GetFlashObject()
  if flash_obj then
    flash_obj:InvokeASCallback("_root", "moveInChatBar")
  end
end
function GameUIStarCraftMap:MoveOut()
end
function GameUIStarCraftMap:RequestMapData()
  local content = {
    version = GameStateStarCraft.mCurrentMapVersion
  }
  NetMessageMgr:SendMsg(NetAPIList.contention_map_req.Code, content, GameUIStarCraftMap.RequestMapDataCallback, true, nil)
end
function GameUIStarCraftMap:RefreshCalenderBtn()
  if GameUIStarCraftMap.mCalendar then
    GameUIStarCraftMap.mCalendar:UpdateCurrentEvents()
    local haveActiveEvent = GameUIStarCraftMap.mCalendar:HasActiveEvent()
    local flash_obj = GameUIStarCraftMap:GetFlashObject()
    if flash_obj then
      flash_obj:InvokeASCallback("_root", "SetEventStart", haveActiveEvent)
    end
  end
end
function GameUIStarCraftMap.RequestMapDataCallback(msgType, content)
  if msgType == NetAPIList.contention_map_ack.Code then
    if content ~= nil then
      if content.version == GameStateStarCraft.mCurrentMapVersion then
        GameUIStarCraftMap:BuildMap()
        GameUIStarCraftMap:RefreshCalenderBtn()
        return true
      end
      GameUIStarCraftMap.mCalendar:InitWithContent(content, GameUIStarCraftMap.OnEventStarted, GameUIStarCraftMap.OnEventEnded)
      GameUIStarCraftMap:ClearMap()
      GameStateStarCraft.mCurrentMapVersion = content.version
      if GameGlobalData.starCraftData then
        if GameGlobalData.starCraftData.type == 2 then
          MapPlayerMySelf.mServerID = content.my_data.server_id
        else
          for k, v in ipairs(content.bases) do
            DebugOut("v.server_id = ", v.server_id)
            DebugOut("MapPlayerMySelf.mServerID = ", MapPlayerMySelf.mServerID)
            DebugOut("GameUtils:GetActiveServerInfo().logic_id = ", GameUtils:GetActiveServerInfo().logic_id)
            if v.server_id == tonumber(GameUtils:GetActiveServerInfo().logic_id) then
              MapPlayerMySelf.mServerID = v.team_id
              break
            end
          end
        end
        MapPlayerMySelf.mID = content.my_data.user_id
      end
      DebugOut("after init MapPlayerMySelf.mServerID = ", MapPlayerMySelf.mServerID)
      for i, v in ipairs(content.buffers) do
        GameUIStarCraftMap.MapBuffList[v.id] = v
      end
      for k, v in ipairs(content.dots) do
        local tmpKeyPoint = MapKeyPoint:new()
        tmpKeyPoint.mID = v.id
        tmpKeyPoint.mType = v.dot_type
        tmpKeyPoint.mBuffList = {}
        if tmpKeyPoint.mType == 1 then
          tmpKeyPoint.mPointName = GameLoader:GetGameText("LC_MENU_TC_DOT_1")
        elseif tmpKeyPoint.mType == 2 then
          tmpKeyPoint.mPointName = GameLoader:GetGameText("LC_MENU_TC_DOT_2")
        else
          local rowIndex = math.floor(v.id / 1000)
          local cloumIndex = v.id % 1000
          local name = rowIndex * 100 + cloumIndex
          local locID = "LC_MENU_TC_DOT_" .. tmpKeyPoint.mType
          local namePreStr = GameLoader:GetGameText(locID)
          tmpKeyPoint.mPointName = string.format(namePreStr, name)
        end
        DebugOut("point id = ", v.id)
        DebugTable(v.buffers)
        for iBuf, vBuf in ipairs(v.buffers) do
          local tmpBuff = MapKeyPointBuff:new()
          tmpBuff.mID = vBuf
          DebugOut("vBuf = ", vBuf)
          DebugOut("GameUIStarCraftMap.MapBuffList = ", GameUIStarCraftMap.MapBuffList[vBuf])
          tmpBuff.mBuffDetailItems = GameUIStarCraftMap.MapBuffList[vBuf].buffs
          tmpBuff.mIcon = GameUIStarCraftMap.MapBuffList[vBuf].icon
          table.insert(tmpKeyPoint.mBuffList, tmpBuff)
          GameUIStarCraftMap:CheckDynamicBuffItem(tmpBuff.mBuffDetailItems[1])
        end
        tmpKeyPoint.mRelationShip = {}
        for kRelation, vRelation in ipairs(v.effection) do
          table.insert(tmpKeyPoint.mRelationShip, vRelation)
        end
        GameUIStarCraftMap.MapKeyPointList[tmpKeyPoint.mID] = tmpKeyPoint
      end
      for k, v in ipairs(content.lines) do
        local tmpPath = MapPath:new()
        tmpPath.mID = v.id
        tmpPath.mStartPoint = v.dot_start
        tmpPath.mEndPoint = v.dot_end
        tmpPath.mDistance = v.distance
        GameUIStarCraftMap.MapPathList[tmpPath.mID] = tmpPath
      end
      GameUIStarCraftMap.currentSeason = content.season
      table.sort(content.all_season, function(a, b)
        return a.id < b.id
      end)
      GameUIStarCraftMap.allSeason = content.all_season
      if content.all_season[1] ~= nil then
        GameUIStarCraftMap.currentShowSeason = content.all_season[1]
        GameUIStarCraftDetail:SetSeasons(false, false)
      else
        GameUIStarCraftMap.currentShowSeason.id = 1
      end
      GameUIStarCraftMap:BuildMap()
    end
    return true
  end
  return false
end
function GameUIStarCraftMap:CheckDynamicBuffItem(buffItem)
  local rewardText = GameHelper:GetAwardTypeText(buffItem.key, buffItem.type_id)
  local frame = ""
  if DynamicResDownloader:IsDynamicStuffByBuffItem(buffItem) then
    local fullFileName = DynamicResDownloader:GetFullName(buffItem.type_id .. ".png", DynamicResDownloader.resType.PIC)
    local localPath = "data2/" .. fullFileName
    DebugWD("localPath = ", localPath)
    if DynamicResDownloader:IfResExsit(localPath) then
      ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
      frame = "item_" .. buffItem.type_id
    else
      frame = "temp"
      self:AddDownloadPath(buffItem.type_id)
    end
  elseif buffItem.key == "fleet" then
    frame = GameDataAccessHelper:GetFleetAvatar(buffItem.type_id, 0)
  else
    frame = GameHelper:GetAwardTypeIconFrameName(buffItem.key, buffItem.type_id, 1)
  end
end
function GameUIStarCraftMap:AddDownloadPath(itemID)
  local resName = itemID .. ".png"
  local extInfo = {}
  extInfo.item_id = itemID
  extInfo.localPath = "data2/LAZY_LOAD_dynamic_" .. resName
  DynamicResDownloader:AddDynamicRes(resName, DynamicResDownloader.resType.PIC, extInfo, GameUIStarCraftMap.donamicDownloadFinishCallback)
end
function GameUIStarCraftMap.donamicDownloadFinishCallback(extInfo)
  return true
end
function GameUIStarCraftMap:NextSeason()
  if GameUIStarCraftMap.currentSeasonIndex < #GameUIStarCraftMap.allSeason then
    GameUIStarCraftMap.currentSeasonIndex = GameUIStarCraftMap.currentSeasonIndex + 1
    GameUIStarCraftMap.currentShowSeason = GameUIStarCraftMap.allSeason[GameUIStarCraftMap.currentSeasonIndex]
  end
  if GameUIStarCraftMap.currentSeasonIndex == #GameUIStarCraftMap.allSeason then
    GameUIStarCraftMap.currentShowSeason = GameUIStarCraftMap.allSeason[GameUIStarCraftMap.currentSeasonIndex]
  end
end
function GameUIStarCraftMap:BeforeSeason()
  DebugTable(GameUIStarCraftMap.allSeason)
  if GameUIStarCraftMap.currentSeasonIndex > 1 then
    GameUIStarCraftMap.currentSeasonIndex = GameUIStarCraftMap.currentSeasonIndex - 1
    GameUIStarCraftMap.currentShowSeason = GameUIStarCraftMap.allSeason[GameUIStarCraftMap.currentSeasonIndex]
  end
  if GameUIStarCraftMap.currentSeasonIndex == 1 then
    GameUIStarCraftMap.currentShowSeason = GameUIStarCraftMap.allSeason[1]
  end
end
function GameUIStarCraftMap:BuildMap()
  local flash_obj = self:GetFlashObject()
  if not flash_obj then
    return
  end
  local keyPointIDArray = ""
  local typeArray = ""
  local nameArray = ""
  local hasKingArray = ""
  local eventStateArray = ""
  local relationPointArray = ""
  for k, v in pairs(GameUIStarCraftMap.MapKeyPointList) do
    keyPointIDArray = keyPointIDArray .. v.mID .. "\001"
    typeArray = typeArray .. v.mType .. "\001"
    nameArray = nameArray .. v.mPointName .. "\001"
    local hasKingStr = "false"
    if v.mHasKing then
      hasKingStr = "true"
    end
    hasKingArray = hasKingArray .. hasKingStr .. "\001"
    local eventStarted = 0
    if v.mCurrentActiveEvent then
      eventStarted = 1
    end
    eventStateArray = eventStateArray .. eventStarted .. "\001"
    for iRelation, vRelation in ipairs(v.mRelationShip) do
      relationPointArray = relationPointArray .. vRelation .. "\001"
    end
  end
  flash_obj:InvokeASCallback("_root", "setKeyPointType", keyPointIDArray, typeArray, nameArray, hasKingArray, eventStateArray)
  flash_obj:InvokeASCallback("_root", "setRelationPoint", relationPointArray)
  local pathIDArray = ""
  local pathStartPointArray = ""
  local endPointArray = ""
  for kPath, vPath in pairs(GameUIStarCraftMap.MapPathList) do
    pathIDArray = pathIDArray .. vPath.mID .. "\001"
    pathStartPointArray = pathStartPointArray .. vPath.mStartPoint .. "\001"
    endPointArray = endPointArray .. vPath.mEndPoint .. "\001"
  end
  flash_obj:InvokeASCallback("_root", "setUpPath", pathIDArray, pathStartPointArray, endPointArray)
end
function GameUIStarCraftMap.MapPlayermarchChangedNtfHandler(content)
  DebugOut("hello hello lua")
  if content ~= nil then
    local flash_obj = GameUIStarCraftMap.GetFlashObject(GameUIStarCraftMap)
    local callScriptCount = 0
    local callScriptCountMax = 10
    local playerIDArray = ""
    local serverFrameArray = ""
    local pathIDArray = ""
    local startIDArray = ""
    local endIDArray = ""
    local distanceArray = ""
    local speedArray = ""
    local timeStampArray = ""
    local isMySelfArray = ""
    local isKingArray = ""
    local moveToCenter = false
    for k, v in ipairs(content.trips) do
      if GameUIStarCraftMap.MapPlayerList[v.user_id] ~= nil then
        GameUIStarCraftMap.MapPlayerList[v.user_id].mStartPoint = v.dot_start
        GameUIStarCraftMap.MapPlayerList[v.user_id].mEndPoint = v.dot_end
        GameUIStarCraftMap.MapPlayerList[v.user_id].mTimeStamp = v.trip_seconds
        GameUIStarCraftMap.MapPlayerList[v.user_id].mSpeed = v.speed
      else
        local tmpPlayer = MapPlayer:new()
        tmpPlayer.mID = v.user_id
        tmpPlayer.mServerID = v.server_id
        tmpPlayer.mPlayerName = v.user_name
        tmpPlayer.mStartPoint = v.dot_start
        tmpPlayer.mEndPoint = v.dot_end
        tmpPlayer.mTimeStamp = v.trip_seconds
        tmpPlayer.mSpeed = v.speed
        GameUIStarCraftMap.MapPlayerList[tmpPlayer.mID] = tmpPlayer
      end
      local path = GameUIStarCraftMap.getPathByStartEnd(GameUIStarCraftMap, v.dot_start, v.dot_end)
      if flash_obj then
        callScriptCount = callScriptCount + 1
        local serverColor = GameUIStarCraftMap:GetColorFrameByServerID(v.server_id)
        local mySelfID = tonumber(GameGlobalData:GetData("userinfo").player_id)
        mySelfID = MapPlayerMySelf.mID
        DebugOut("my self id = ", mySelfID)
        DebugOut("v.user_id = ", v.user_id)
        playerIDArray = playerIDArray .. v.user_id .. "\001"
        serverFrameArray = serverFrameArray .. serverColor .. "\001"
        pathIDArray = pathIDArray .. path.mID .. "\001"
        startIDArray = startIDArray .. v.dot_start .. "\001"
        endIDArray = endIDArray .. v.dot_end .. "\001"
        distanceArray = distanceArray .. path.mDistance .. "\001"
        speedArray = speedArray .. v.speed .. "\001"
        timeStampArray = timeStampArray .. v.trip_seconds .. "\001"
        local isMyStr = "false"
        if tostring(mySelfID) == tostring(v.user_id) then
          isMyStr = "true"
        end
        isMySelfArray = isMySelfArray .. isMyStr .. "\001"
        local isKingStr = "false"
        if nil ~= v.king and 1 == tonumber(v.king) then
          if -1 ~= GameUIStarCraftMap.mLastKeyPointIDKingIn and GameUIStarCraftMap.MapKeyPointList[GameUIStarCraftMap.mLastKeyPointIDKingIn] then
            GameUIStarCraftMap.MapKeyPointList[GameUIStarCraftMap.mLastKeyPointIDKingIn].mHasKing = false
            GameUIStarCraftMap.mLastKeyPointIDKingIn = -1
          end
          isKingStr = "true"
          if -1 ~= GameUIStarCraftMap.mKingUserID and GameUIStarCraftMap.mKingUserID ~= v.user_id then
            GameUIStarCraftMap:SetFleetKingFlg(GameUIStarCraftMap.mKingUserID, false)
          end
          GameUIStarCraftMap.mKingUserID = v.user_id
        end
        isKingArray = isKingArray .. isKingStr .. "\001"
        DebugOut("callScriptCount = ", callScriptCount)
        DebugOut("k = ", k)
        if callScriptCountMax <= callScriptCount then
          flash_obj:InvokeASCallback("_root", "initPathPlayer", playerIDArray, serverFrameArray, pathIDArray, startIDArray, endIDArray, distanceArray, speedArray, timeStampArray, isMySelfArray, isKingArray)
          playerIDArray = ""
          serverFrameArray = ""
          pathIDArray = ""
          startIDArray = ""
          endIDArray = ""
          distanceArray = ""
          speedArray = ""
          timeStampArray = ""
          isMySelfArray = ""
          isKingArray = ""
          callScriptCount = 0
        end
      end
      DebugOut("kskskalskdkdjkfjsjska:", v.user_id, MapPlayerMySelf.mID)
      if tostring(v.user_id) == tostring(MapPlayerMySelf.mID) then
        MapPlayerMySelf.mNotifyGetTime = os.time()
        MapPlayerMySelf.mLeftTime = math.floor(path.mDistance / v.speed - v.trip_seconds)
        MapPlayerMySelf.mIsOnPath = true
        MapPlayerMySelf.mCurrentStartPointID = v.dot_start
        MapPlayerMySelf.mCurrentEndPointID = v.dot_end
        MapPlayerMySelf.mCurrentKeyPointID = -1
        if nil ~= v.king and 1 == tonumber(v.king) then
          MapPlayerMySelf.mIsKing = true
        else
          MapPlayerMySelf.mIsKing = false
        end
        moveToCenter = true
        if flash_obj then
          flash_obj:InvokeASCallback("_root", "tryToDisplayMySelfInCenter", MapPlayerMySelf.mIsOnPath, MapPlayerMySelf.mID, MapPlayerMySelf.mCurrentKeyPointID)
        end
      end
    end
    if callScriptCount > 0 then
      if flash_obj then
        flash_obj:InvokeASCallback("_root", "initPathPlayer", playerIDArray, serverFrameArray, pathIDArray, startIDArray, endIDArray, distanceArray, speedArray, timeStampArray, isMySelfArray, isKingArray)
        if moveToCenter then
          flash_obj:InvokeASCallback("_root", "tryToDisplayMySelfInCenter", MapPlayerMySelf.mIsOnPath, MapPlayerMySelf.mID, MapPlayerMySelf.mCurrentKeyPointID)
        end
      end
      callScriptCount = 0
    end
  end
end
function GameUIStarCraftMap.MapPlayerKeyPointStateChangedNtfHandler(content)
  DebugOut("hello hello lua22")
  if content ~= nil then
    local flash_obj = GameUIStarCraftMap.GetFlashObject(GameUIStarCraftMap)
    for k, v in ipairs(content.stations) do
      local hasKing = 1 == tonumber(v.king)
      if GameUIStarCraftMap.MapKeyPointList[v.dot_id] and GameUIStarCraftMap.MapKeyPointList[v.dot_id].mHasKing ~= hasKing then
        GameUIStarCraftMap.MapKeyPointList[v.dot_id].mHasKing = hasKing
        local keyPointID = v.dot_id
        if hasKing then
          if -1 ~= GameUIStarCraftMap.mKingUserID then
            GameUIStarCraftMap:SetFleetKingFlg(GameUIStarCraftMap.mKingUserID, false)
          end
          if -1 ~= GameUIStarCraftMap.mLastKeyPointIDKingIn and keyPointID ~= GameUIStarCraftMap.mLastKeyPointIDKingIn then
            GameUIStarCraftMap.MapKeyPointList[GameUIStarCraftMap.mLastKeyPointIDKingIn].mHasKing = false
          end
          GameUIStarCraftMap.mLastKeyPointIDKingIn = keyPointID
        end
      end
      if GameUIStarCraftMap.MapKeyPointList[v.dot_id] then
        GameUIStarCraftMap.MapKeyPointList[v.dot_id].mAllPower = v.all_power
      end
      if v.server_id ~= 0 then
        if GameUIStarCraftMap.MapKeyPointList[v.dot_id] then
          if GameUIStarCraftMap.MapKeyPointList[v.dot_id].mServerID == MapPlayerMySelf.mServerID and v.server_id ~= MapPlayerMySelf.mServerID then
            GameUIStarCraftMap.MapKeyPointList[v.dot_id].mHasResources = false
            if flash_obj then
              flash_obj:InvokeASCallback("_root", "hideKeyPointResource", v.dot_id)
            end
          end
          GameUIStarCraftMap.MapKeyPointList[v.dot_id].mServerID = v.server_id
          if GameUIStarCraftMap.serverIDList[v.server_id] == nil and tonumber(MapPlayerMySelf.mServerID) ~= tonumber(v.server_id) then
            GameUIStarCraftMap.serverIDList[v.server_id] = v.server_id
          end
        else
          DebugOut("Error: Unknow map key point , dot_id: " .. v.dot_id)
        end
      end
    end
    GameUIStarCraftMap:DeterminAllServerColor()
    for k, v in ipairs(content.stations) do
      if flash_obj then
        local colorFrame = GameUIStarCraftMap:GetColorFrameByServerID(v.server_id)
        local areaPoints = ""
        DebugOut("v.dot_id = ", v.dot_id)
        for iRelation, vRelation in ipairs(GameUIStarCraftMap.MapKeyPointList[v.dot_id].mRelationShip) do
          areaPoints = areaPoints .. vRelation .. "\001"
        end
        flash_obj:InvokeASCallback("_root", "setKeyPointServer", v.dot_id, areaPoints, colorFrame)
      end
    end
    GameUIStarCraftMap:RefreshMyBuff()
  end
end
function GameUIStarCraftMap.MyInfoChangedNtfHandler(content)
  if content ~= nil then
    local isKing = 1 == tonumber(content.info.king)
    if MapPlayerMySelf.mIsKing ~= isKing and isKing then
      if -1 ~= GameUIStarCraftMap.mKingUserID then
        GameUIStarCraftMap:SetFleetKingFlg(GameUIStarCraftMap.mKingUserID, false)
      end
      GameUIStarCraftMap:SetFleetKingFlg(MapPlayerMySelf.mID, isKing)
      GameUIStarCraftMap.mKingUserID = MapPlayerMySelf.mID
    end
    local moveToCenter = false
    for i, v in ipairs(content.info.special_buff) do
      GameUIStarCraftMap:AddDynamicBuff(v, Dynamic_Buff_Type.Dynamic_Special_Buff)
    end
    local needRefreshBuff = false
    if content.info.hp > 0 then
      if 0 < #content.info.fight_buff then
        local aindex = 1
        while aindex <= #MapPlayerMySelf.mBuffList do
          local buff = MapPlayerMySelf.mBuffList[aindex]
          if buff:GetType() == EBuff_Type.BUFF_BOAT_YARD then
            table.remove(MapPlayerMySelf.mBuffList, aindex)
          else
            aindex = aindex + 1
          end
        end
        MapPlayerMySelf.mBoatBuffGetTime = os.time()
        needRefreshBuff = true
      end
      for i, v in ipairs(content.info.fight_buff) do
        GameUIStarCraftMap:AddDynamicBuff(v, Dynamic_Buff_Type.Dynamic_Boat_Yard_Buff)
      end
    end
    if #content.info.special_buff > 0 then
      MapPlayerMySelf.mDynamicBuffGetTime = os.time()
      needRefreshBuff = true
    end
    if needRefreshBuff then
      GameUIStarCraftMap:RefreshMyBuff()
    end
    if 0 >= MapPlayerMySelf.mCurrentHP and content.info.hp > 0 then
      MapPlayerMySelf.mIsWaitingRevive = false
      MapPlayerMySelf.mDieBattleID = -1
      GameUIStarCraftMap:ClosePlayerDiePanel()
      moveToCenter = true
    end
    MapPlayerMySelf.mCurrentHP = content.info.hp
    MapPlayerMySelf.mMaxHP = content.info.hp_limit
    MapPlayerMySelf.mSpeed = content.info.speed
    GameUIStarCraftMap:ConvertHPAndRefresh(MapPlayerMySelf.mCurrentHP, MapPlayerMySelf.mMaxHP)
    if 0 < content.info.revived_left_second then
      MapPlayerMySelf.mLeftTime = content.info.revived_left_second
      MapPlayerMySelf.mNotifyGetTime = os.time()
    end
    local flash_obj = GameUIStarCraftMap.GetFlashObject(GameUIStarCraftMap)
    DebugOut("init server id = ", MapPlayerMySelf.mServerID)
    if content.info.hp <= 0 then
      MapPlayerMySelf.mKilledBy = content.info.killer
      GameUIStarCraftMap:ShowPlayerDiePanel()
      local isDie = true
      local isMarching = false
      local isIdle = false
      GameUIStarCraftMap:SetPlayerState(isMarching, isDie, isIdle, "")
      GameUIStarCraftMap:CleanUpMySelfPath()
      GameUIStarCraftMap:KillMySelf()
      GameUIStarCraftMap:removeTeleportAnim()
      MapPlayerMySelf.mCurrentKeyPointID = content.info.dot_start
      MapPlayerMySelf.mIsOnPath = false
      MapPlayerMySelf.mCurrentPathID = -1
      MapPlayerMySelf.mCurrentStartPointID = -1
      MapPlayerMySelf.mCurrentEndPointID = -1
      GameUIStarCraftMap:RefreshMyBuff()
      MapPlayerMySelf.mDieBattleID = content.info.last_report
      return
    end
    local refBuf = false
    local curPoint = GameUIStarCraftMap.MapKeyPointList[MapPlayerMySelf.mCurrentKeyPointID]
    if curPoint then
      for iBuff, vBuff in ipairs(curPoint.mBuffList) do
        if vBuff:GetType() == EBuff_Type.BUFF_ATTRIBUTE_PER_SECONDS then
          refBuf = true
        end
      end
    end
    if content.info.dot_start == content.info.dot_end then
      local isDie = false
      local isMarching = false
      local isIdle = true
      local stateStr = GameLoader:GetGameText("LC_MENU_TC_STATUS_REST_LABEL")
      MapPlayerMySelf.mCurrentKeyPointID = content.info.dot_start
      MapPlayerMySelf.mIsOnPath = false
      MapPlayerMySelf.mCurrentPathID = -1
      MapPlayerMySelf.mCurrentStartPointID = -1
      MapPlayerMySelf.mCurrentEndPointID = -1
      MapPlayerMySelf.mLeftTime = 0
      GameUIStarCraftMap:SetPlayerState(isMarching, isDie, isIdle, stateStr)
      GameUIStarCraftMap:CleanUpMySelfPath()
      GameUIStarCraftMap:KillMySelf()
      curPoint = GameUIStarCraftMap.MapKeyPointList[MapPlayerMySelf.mCurrentKeyPointID]
      if curPoint then
        for iBuff, vBuff in ipairs(curPoint.mBuffList) do
          if vBuff:GetType() == EBuff_Type.BUFF_ATTRIBUTE_PER_SECONDS then
            refBuf = true
          end
        end
      end
    else
      local isDie = false
      local isMarching = true
      local isIdle = false
      local stateStr = GameLoader:GetGameText("LC_MENU_TC_STATUS_HANGING_LABEL")
      GameUIStarCraftMap:SetPlayerState(isMarching, isDie, isIdle, stateStr)
      if MapPlayerMySelf.mIsOnPath and MapPlayerMySelf.mCurrentStartPointID == content.info.dot_start and MapPlayerMySelf.mCurrentEndPointID == content.info.dot_end then
        DebugOut("why happen !!!!!!!!!!!!!")
      else
        MapPlayerMySelf.mCurrentStartPointID = content.info.dot_start
        MapPlayerMySelf.mCurrentEndPointID = content.info.dot_end
        MapPlayerMySelf.mIsOnPath = true
        MapPlayerMySelf.mCurrentKeyPointID = -1
        MapPlayerMySelf.mCurrentPathID = GameUIStarCraftMap:getPathByStartEnd(content.info.dot_start, content.info.dot_end)
        local curStart = MapPlayerMySelf.mCurrentStartPointID
        for iPath, vPath in ipairs(content.info.now_lines) do
          if flash_obj then
            local path = GameUIStarCraftMap.MapPathList[vPath]
            if path.mStartPoint == curStart then
              flash_obj:InvokeASCallback("_root", "setUpArrowPath", path.mID, path.mStartPoint, path.mEndPoint)
              curStart = path.mEndPoint
            else
              flash_obj:InvokeASCallback("_root", "setUpArrowPath", path.mID, path.mEndPoint, path.mStartPoint)
              curStart = path.mStartPoint
            end
          end
        end
      end
    end
    if refBuf then
      GameUIStarCraftMap:RefreshMyBuff()
    end
    if MapPlayerMySelf.mInited == false or moveToCenter then
      if flash_obj then
        flash_obj:InvokeASCallback("_root", "tryToDisplayMySelfInCenter", MapPlayerMySelf.mIsOnPath, MapPlayerMySelf.mID, MapPlayerMySelf.mCurrentKeyPointID)
      end
      MapPlayerMySelf.mInited = true
    end
  end
end
function GameUIStarCraftMap:ConvertHPAndRefresh(curHP, maxHP)
  DebugOut("curHP, maxHP", curHP, " ", maxHP)
  local percentHp = curHP / maxHP
  local percetText = curHP .. "/" .. maxHP
  percent = math.floor(percentHp * 100)
  if percent <= 0 then
    percent = 1
  elseif percent > 100 then
    percent = 100
  end
  if not GameUIStarCraftMap:GetFlashObject() then
    return
  end
  GameUIStarCraftMap:GetFlashObject():InvokeASCallback("_root", "SetUserHP", percent, percetText)
end
function GameUIStarCraftMap.MapPlayerBattleNtfHandler(content)
  if content ~= nil then
    local flash_obj = GameUIStarCraftMap.GetFlashObject(GameUIStarCraftMap)
    for i, v in ipairs(content.battles) do
      local battleId_1 = v.attk_user_id
      local battleId_2 = v.def_user_id
      if v.on_station > 0 then
        if flash_obj then
          flash_obj:InvokeASCallback("_root", "animationBattle", battleId_1, battleId_2, v.on_station)
        end
      elseif flash_obj then
        flash_obj:InvokeASCallback("_root", "animationBattle", battleId_1, battleId_2, 0)
      end
      for iDie, vDie in ipairs(v.dead_list) do
        if vDie == MapPlayerMySelf.mID then
          if v.attk_user_id == MapPlayerMySelf.mID then
            MapPlayerMySelf.mKilledBy = v.def_user_name
          else
            MapPlayerMySelf.mKilledBy = v.attk_user_name
          end
          local killer = GameUtils:GetUserDisplayName(MapPlayerMySelf.mKilledBy)
          DebugOut("MapPlayerMySelf.mKilledBy", MapPlayerMySelf.mKilledBy)
          if MapPlayerMySelf.mKilledBy ~= nil then
            local killer = GameUtils:GetUserDisplayName(MapPlayerMySelf.mKilledBy)
            local killerTex = GameLoader:GetGameText("LC_MENU_TC_REBUILD_ALERT_KILLER_DESC")
            if killer ~= nil then
              killerTex = string.gsub(killerTex, "<player>", killer)
              if flash_obj then
                flash_obj:InvokeASCallback("_root", "setPlayerDieKiller", killerTex)
              end
            end
          end
        end
        GameUIStarCraftMap.MapPlayerList[vDie] = nil
        if flash_obj then
          flash_obj:InvokeASCallback("_root", "killPlayer", vDie)
        end
      end
    end
  end
end
function GameUIStarCraftMap.ServerDisplayNameChangeNtfHandler(content)
  if content ~= nil then
    for i, v in ipairs(content.stations) do
      GameUIStarCraftMap.ServerName[v.dot_id] = v.dot_name
    end
  end
end
function GameUIStarCraftMap.ProductionChangeNtfHandler(content)
  if content ~= nil then
    local flash_obj = GameUIStarCraftMap.GetFlashObject(GameUIStarCraftMap)
    for i, v in ipairs(content.productions) do
      if #v.resources > 0 and GameUIStarCraftMap.MapKeyPointList[v.dot_id].mServerID == MapPlayerMySelf.mServerID then
        GameUIStarCraftMap.MapKeyPointList[v.dot_id].mHasResources = true
        if flash_obj then
          local index = 1
          for iResource, vResource in ipairs(v.resources) do
            local resourceFrame = GameHelper:GetAwardTypeIconFrameName(vResource.item_type, vResource.number, vResource.no)
            local resourceCount = GameHelper:GetAwardCount(vResource.item_type, vResource.number, vResource.no)
            if resourceCount > 0 then
              flash_obj:InvokeASCallback("_root", "setKeyPointResource", v.dot_id, resourceFrame, index)
              index = index + 1
            end
          end
        end
      else
        GameUIStarCraftMap.MapKeyPointList[v.dot_id].mHasResources = false
      end
    end
    return true
  end
  return false
end
function GameUIStarCraftMap.TeleportNtfHandler(content)
  if content ~= nil and tostring(content.user_id) ~= tostring(MapPlayerMySelf.mID) then
    local flash_obj = GameUIStarCraftMap:GetFlashObject()
    GameUIStarCraftMap.MapPlayerList[content.user_id] = nil
    if flash_obj then
      flash_obj:InvokeASCallback("_root", "killPlayer", content.user_id)
    end
  end
  return true
end
function GameUIStarCraftMap.FireMissilesNtfHandler(content)
  if content ~= nil then
    for i, v in pairs(content.missiles_list) do
      GameUIStarCraftMap:FireMissile(v.begin_dot, v.end_dot, v.now_step, v.max_step)
    end
  end
  return true
end
function GameUIStarCraftMap:RequestMarch(pathList)
  local content = {lines = pathList}
  MapPlayerMySelf.mCurrentPathList = pathList
  NetMessageMgr:SendMsg(NetAPIList.contention_starting_req.Code, content, GameUIStarCraftMap.RequestMarchCallback, true, nil)
end
function GameUIStarCraftMap.RequestMarchCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.contention_starting_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      return true
    else
      DebugOut("march started!!!!!!!")
      local stateStr = GameLoader:GetGameText("LC_MENU_TC_STATUS_HANGING_LABEL")
      GameUIStarCraftMap:SetPlayerState(true, false, false, stateStr)
      local flash_obj = GameUIStarCraftMap.GetFlashObject(GameUIStarCraftMap)
      for k, v in pairs(MapPlayerMySelf.mCurrentPathList) do
        if flash_obj then
          DebugOut("try to set up arrow path")
          local path = GameUIStarCraftMap.MapPathList[v.line_id]
          if path.mStartPoint == v.dot_start then
            flash_obj:InvokeASCallback("_root", "setUpArrowPath", path.mID, path.mStartPoint, path.mEndPoint)
          else
            flash_obj:InvokeASCallback("_root", "setUpArrowPath", path.mID, path.mEndPoint, path.mStartPoint)
          end
        end
      end
      return true
    end
  end
  return false
end
function GameUIStarCraftMap:UpdateMarch(dt)
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    flash_obj:InvokeASCallback("_root", "updatePlayerPos", dt)
  end
end
function GameUIStarCraftMap:UpdateMyMarchTime()
  local flash_obj = self:GetFlashObject()
  local tmpLeftTime = 0
  if 0 < MapPlayerMySelf.mLeftTime then
    tmpLeftTime = MapPlayerMySelf.mLeftTime - (os.time() - MapPlayerMySelf.mNotifyGetTime)
    if tmpLeftTime < 0 then
      tmpLeftTime = 0
    end
  else
    tmpLeftTime = 0
  end
  if currentMarchLeftTime == tmpLeftTime then
    return
  end
  currentMarchLeftTime = tmpLeftTime
  local timeStr = GameUtils:formatTimeString(tmpLeftTime)
  if flash_obj then
    flash_obj:InvokeASCallback("_root", "updatePlayerState", true, false, timeStr)
    flash_obj:InvokeASCallback("_root", "updatePlayerFleetTime", timeStr)
  end
end
function GameUIStarCraftMap:CleanUpMySelfPath()
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    flash_obj:InvokeASCallback("_root", "cleanupArrowPath")
  end
end
function GameUIStarCraftMap:RequestTeleport(pathList)
  local content = {lines = pathList}
  NetMessageMgr:SendMsg(NetAPIList.contention_transfer_req.Code, content, GameUIStarCraftMap.RequestTeleportCallback, true, nil)
end
function GameUIStarCraftMap.RequestTeleportCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.contention_transfer_req.Code then
    if content.code == 0 then
      local killPlayerID = MapPlayerMySelf.mID
      GameUIStarCraftMap.MapPlayerList[killPlayerID] = nil
      local flash_obj = GameUIStarCraftMap:GetFlashObject()
      if flash_obj then
        if 0 < MapPlayerMySelf.mCurrentHP then
          local isKing = GameUIStarCraftMap.mKingUserID == MapPlayerMySelf.mID
          flash_obj:InvokeASCallback("_root", "animationTeleportMoveIn", MapPlayerMySelf.mIsOnPath, MapPlayerMySelf.mCurrentStartTeleportID, MapPlayerMySelf.mCurrentEndTeleportID, isKing)
        end
        flash_obj:InvokeASCallback("_root", "killPlayer", killPlayerID)
      end
      GameUIStarCraftMap:CleanUpMySelfPath()
    else
      local GameVip = LuaObjectManager:GetLuaObject("GameVip")
      GameVip:CheckIsNeedShowVip(content.code)
    end
    return true
  end
  return false
end
function GameUIStarCraftMap:ShowPlayerDiePanel()
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    local hasReplay = true
    if MapPlayerMySelf.mKilledBy ~= nil then
      local killer = GameUtils:GetUserDisplayName(MapPlayerMySelf.mKilledBy)
      local killerTex = GameLoader:GetGameText("LC_MENU_TC_REBUILD_ALERT_KILLER_DESC")
      if killer ~= nil then
        if killer == "missile" then
          hasReplay = false
          killer = GameLoader:GetGameText("LC_MENU_TC_NPC_MISSILE")
        end
        killerTex = string.gsub(killerTex, "<player>", killer)
        flash_obj:InvokeASCallback("_root", "setPlayerDieKiller", killerTex)
      end
    end
    local timeStr = GameLoader:GetGameText("LC_MENU_TC_REBUILD_ALERT_DESC")
    local leftTimeStr = GameUtils:formatTimeString(MapPlayerMySelf.mLeftTime)
    timeStr = string.format(timeStr, leftTimeStr)
    local replayString = GameLoader:GetGameText("LC_MENU_TC_REPLAY_BUTTON")
    flash_obj:InvokeASCallback("_root", "showPlayerDiePanel", timeStr, hasReplay, replayString)
  end
end
function GameUIStarCraftMap:ClosePlayerDiePanel()
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    flash_obj:InvokeASCallback("_root", "hidePlayerDiePanel")
  end
end
function GameUIStarCraftMap:revive()
  local startID = MapPlayerMySelf.mCurrentKeyPointID
  if MapPlayerMySelf.mIsOnPath then
    startID = MapPlayerMySelf.mCurrentStartPointID
  end
  local content = {dot_start = startID, pay_for_alive = true}
  NetMessageMgr:SendMsg(NetAPIList.contention_revive_req.Code, content, GameUIStarCraftMap.reviveCallback, true, nil)
end
function GameUIStarCraftMap.reviveCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.contention_revive_req.Code then
    if content.code ~= 0 then
      local GameVip = LuaObjectManager:GetLuaObject("GameVip")
      GameVip:CheckIsNeedShowVip(content.code)
    end
    MapPlayerMySelf.mIsWaitingRevive = false
    return true
  end
  return false
end
function GameUIStarCraftMap:UpdateReviveTime()
  local tmpLeftTime = 0
  if 0 < MapPlayerMySelf.mLeftTime then
    tmpLeftTime = MapPlayerMySelf.mLeftTime - (os.time() - MapPlayerMySelf.mNotifyGetTime)
    if tmpLeftTime < 0 then
      tmpLeftTime = 0
      if 0 >= MapPlayerMySelf.mCurrentHP and not MapPlayerMySelf.mIsWaitingRevive then
        MapPlayerMySelf.mIsWaitingRevive = true
        local content = {
          notifies = {
            "contention_info"
          }
        }
        NetMessageMgr:SendMsg(NetAPIList.contention_notifies_req.Code, content, nil, false, nil)
      end
    end
  else
    tmpLeftTime = 0
  end
  if currentReviveLeftTime == tmpLeftTime then
    return
  end
  currentReviveLeftTime = tmpLeftTime
  local timeStr = GameUtils:formatTimeString(tmpLeftTime)
  local diePanelTimeStr = GameLoader:GetGameText("LC_MENU_TC_REBUILD_ALERT_DESC")
  diePanelTimeStr = string.format(diePanelTimeStr, timeStr)
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    flash_obj:InvokeASCallback("_root", "updatePlayerState", false, true, timeStr)
    flash_obj:InvokeASCallback("_root", "setPlayerDiePanelTime", diePanelTimeStr)
  end
end
function GameUIStarCraftMap.reviveHandler()
  GameUIStarCraftMap:revive()
end
function GameUIStarCraftMap:SetPlayerState(isMarch, isDie, isIdle, stateStr)
  local flash_obj = self:GetFlashObject()
  currentReviveLeftTime = 0
  currentMarchLeftTime = 0
  if flash_obj then
    flash_obj:InvokeASCallback("_root", "setPlayerState", isMarch, isDie, isIdle, stateStr)
  end
end
function GameUIStarCraftMap:UpdatePlayerState()
  if MapPlayerMySelf.mCurrentHP <= 0 then
    GameUIStarCraftMap:UpdateReviveTime()
  else
    GameUIStarCraftMap:UpdateMyMarchTime()
  end
end
function GameUIStarCraftMap.OnGlobalUserInfoChange()
  DebugOut("\230\137\147\229\141\176OnGlobalUserInfoChange")
  if not GameUIStarCraftMap:GetFlashObject() then
    return
  end
  local userInfo = GameGlobalData:GetData("userinfo")
  if userInfo then
    local name_display = GameUtils:GetUserDisplayName(userInfo.name)
    GameUIStarCraftMap:GetFlashObject():SetText("_root.maps.user_main.PlayerName.Text", name_display)
    GameUIStarCraftMap:GetFlashObject():InvokeASCallback("_root", "setVipPosition")
  end
end
function GameUIStarCraftMap.OnGlobalLevelInfoChange()
  if not GameUIStarCraftMap:GetFlashObject() then
    return
  end
  DebugOut("GameUIBarLeft.OnGlobalLevelInfoChange")
  local levelInfo = GameGlobalData:GetData("levelinfo")
  DebugTable(levelInfo)
  if levelInfo then
    GameUIStarCraftMap:GetFlashObject():SetText("_root.maps.user_main.PlayerLevel.Text", levelInfo.level)
    local levelProgress = math.floor(100 * levelInfo.experience / levelInfo.uplevel_experience)
    GameUIStarCraftMap:GetFlashObject():InvokeASCallback("_root", "setExperiencePersent", levelProgress)
  end
end
function GameUIStarCraftMap.onGlobalVipChange()
  local vipInfo = GameGlobalData:GetData("vipinfo")
  GameUIStarCraftMap:SetVipInfo()
end
function GameUIStarCraftMap.OnGlobalResourceChange()
  DebugOut("\230\137\147\229\141\176_root_OnGlobalResourceChange")
  if not GameUIStarCraftMap:GetFlashObject() then
    return
  end
  local resource = GameGlobalData:GetData("resource")
  if resource then
    GameUIStarCraftMap:GetFlashObject():SetText("_root.maps.user_main.money_bar.CubitNum.Text", GameUtils.numberConversion(resource.money))
    GameUIStarCraftMap:GetFlashObject():SetText("_root.maps.user_main.money_bar.CreditNum.Text", GameUtils.numberConversion(resource.credit))
  end
end
function GameUIStarCraftMap:OnRegesterAvatarInfoChangeHandler()
  GameGlobalData:RegisterDataChangeCallback("resource", self.OnGlobalResourceChange)
  GameGlobalData:RegisterDataChangeCallback("userinfo", self.OnGlobalUserInfoChange)
  GameGlobalData:RegisterDataChangeCallback("levelinfo", self.OnGlobalLevelInfoChange)
  GameGlobalData:RegisterDataChangeCallback("vipinfo", self.onGlobalVipChange)
end
function GameUIStarCraftMap:OnRemoveAvatarInfoChangeHandler()
  GameGlobalData:RemoveDataChangeCallback("resource", self.OnGlobalResourceChange)
  GameGlobalData:RemoveDataChangeCallback("userinfo", self.OnGlobalUserInfoChange)
  GameGlobalData:RemoveDataChangeCallback("levelinfo", self.OnGlobalLevelInfoChange)
  GameGlobalData:RemoveDataChangeCallback("vipinfo", self.onGlobalVipChange)
end
function GameUIStarCraftMap:SetVipInfo()
  if not GameUIStarCraftMap:GetFlashObject() then
    return
  end
  local curLevel = GameVipDetailInfoPanel:GetVipLevel()
  local m, k = 0, 0
  if curLevel >= 10 then
    m = math.floor(curLevel / 10)
    k = curLevel - m * 10
  else
    k = curLevel
  end
  self:GetFlashObject():InvokeASCallback("_root", "refreshVipInfo", m, k)
end
function GameUIStarCraftMap:MoveInPlayerAvata()
  GameUIStarCraftMap:SetVipInfo()
  GameUIStarCraftMap:RefreshMyBuff()
  GameUIStarCraftMap.OnGlobalUserInfoChange()
  GameUIStarCraftMap.OnGlobalLevelInfoChange()
  local sex = GameGlobalData:GetUserInfo().sex
  local player_main_fleet = GameGlobalData:GetFleetInfo(1)
  local leaderlist = GameGlobalData:GetData("leaderlist")
  local player_avatar = ""
  local matrixIndex = GameGlobalData.GlobalData.curMatrixIndex
  if leaderlist and matrixIndex then
    player_avatar = GameDataAccessHelper:GetFleetAvatar(leaderlist.leader_ids[matrixIndex], player_main_fleet.level)
  else
    player_avatar = GameUtils:GetPlayerAvatar(nil, player_main_fleet.level)
  end
  DebugOut("main player_avatar = ", player_avatar)
  self:GetFlashObject():InvokeASCallback("_root", "MoveInUserInfo", player_avatar)
end
function GameUIStarCraftMap:RefreshMyBuff()
  GameUIStarCraftMap:ClearMyBuffList()
  local buffIconList = {}
  local buffIconListInUse = {}
  for i, v in ipairs(MapPlayerMySelf.mBuffList) do
    if v:GetType() == 4 then
      framName = v:GetDisplayFrame()
      if false == LuaUtils:isKeyInTable(buffIconListInUse, framName) then
        table.insert(buffIconListInUse, framName)
      end
    elseif v:GetType() == 6 and MapPlayerMySelf.mCurrentHP > 0 then
      framName = v:GetDisplayFrame()
      if false == LuaUtils:isKeyInTable(buffIconListInUse, framName) then
        table.insert(buffIconListInUse, framName)
      end
    end
  end
  DebugOut("buff list before set ")
  DebugTable(MapPlayerMySelf.mBuffList)
  for k, v in pairs(GameUIStarCraftMap.MapKeyPointList) do
    if v.mServerID == MapPlayerMySelf.mServerID then
      for iBuff, vBuff in ipairs(v.mBuffList) do
        if vBuff:GetType() == EBuff_Type.BUFF_ATTRIBUTE_PER_SECONDS then
          if MapPlayerMySelf.mCurrentKeyPointID == v.mID then
            table.insert(MapPlayerMySelf.mBuffList, vBuff)
            DebugOut("add attribute buff")
          end
        else
          table.insert(MapPlayerMySelf.mBuffList, vBuff)
        end
        if vBuff:GetType() == 1 and buffIconList[vBuff.mID] == nil then
          buffIconList[vBuff.mID] = vBuff:GetDisplayFrame()
          framName = vBuff:GetDisplayFrame()
          DebugOut("framName on point = ", framName)
          if false == LuaUtils:isKeyInTable(buffIconListInUse, framName) then
            table.insert(buffIconListInUse, framName)
          end
        elseif vBuff:GetType() == EBuff_Type.BUFF_ATTRIBUTE_PER_SECONDS and buffIconList[vBuff.mID] == nil then
          buffIconList[vBuff.mID] = vBuff:GetDisplayFrame()
          if MapPlayerMySelf.mCurrentKeyPointID == v.mID then
            framName = vBuff:GetDisplayFrame()
            DebugOut("framName on point = ", framName)
            if false == LuaUtils:isKeyInTable(buffIconListInUse, framName) then
              table.insert(buffIconListInUse, framName)
            end
          end
        end
      end
    end
  end
  if not self:GetFlashObject() then
    return
  end
  self:GetFlashObject():InvokeASCallback("_root", "resetPlayerBuff")
  local index = 1
  for k, v in pairs(buffIconListInUse) do
    if index > 5 then
      break
    end
    self:GetFlashObject():InvokeASCallback("_root", "setPlayerBuff", index, v)
    index = index + 1
  end
end
function GameUIStarCraftMap:ClearMyBuffList()
  MapPlayerMySelf.mBuffList = MapPlayerMySelf.mBuffList or {}
  if MapPlayerMySelf.mBuffList then
    local buffList = {}
    for i, v in ipairs(MapPlayerMySelf.mBuffList) do
      if v:GetType() == 4 then
        local timeLeft = v.mLeftTime - (os.time() - MapPlayerMySelf.mDynamicBuffGetTime)
        if timeLeft > 0 then
          table.insert(buffList, v)
        end
      elseif v:GetType() == 6 and 0 < MapPlayerMySelf.mCurrentHP then
        local timeLeft = v.mLeftTime - (os.time() - MapPlayerMySelf.mBoatBuffGetTime)
        if timeLeft > 0 then
          table.insert(buffList, v)
        end
      end
    end
    MapPlayerMySelf.mBuffList = buffList
  end
end
function GameUIStarCraftMap:AddDynamicBuff(dynamicBuff, dynamicBuffType)
  if dynamicBuffType == Dynamic_Buff_Type.Dynamic_Special_Buff then
    local index = -1
    for i, v in ipairs(MapPlayerMySelf.mBuffList) do
      if v.mID == dynamicBuff.id then
        index = i
        break
      end
    end
    if dynamicBuff.id > 200 and dynamicBuff.id <= 250 and dynamicBuff.end_time > 0 then
      DebugOut("hello dynamic buffs!!!")
      MapPlayerMySelf.mHasDynamicBuff = true
      MapPlayerMySelf.mProductionSpeedUp = dynamicBuff.ext
      GameUIStarCraftMap.mIncomeBuffLeftTime = dynamicBuff.end_time
      GameUIStarCraftMap.mIncomeBuffLeftTimeFetchTime = os.time()
      if not GameUIStarCraftMap.mIsIncomeBuffLeftTimeShow then
        GameUIStarCraftMap.mIsIncomeBuffLeftTimeShow = true
        GameTimer:Add(GameUIStarCraftMap.IncomeBuffUpdate, 500)
      end
    end
    if index <= 0 then
      buffItem = MapKeyPointBuff:new()
      buffItem.mID = dynamicBuff.id
      buffItem.mValue = dynamicBuff.ext
      buffItem.mLeftTime = dynamicBuff.end_time
      table.insert(MapPlayerMySelf.mBuffList, buffItem)
    else
      MapPlayerMySelf.mBuffList[index].mValue = dynamicBuff.ext
      MapPlayerMySelf.mBuffList[index].mLeftTime = dynamicBuff.end_time
    end
  elseif dynamicBuffType == Dynamic_Buff_Type.Dynamic_Boat_Yard_Buff then
    buffItem = MapKeyPointBuff:new()
    buffItem.mID = 301
    buffItem.mKey = dynamicBuff.value1
    buffItem.mValue = dynamicBuff.value3
    buffItem.mLeftTime = dynamicBuff.endtime
    buffItem.mIcon = dynamicBuff.icon
    table.insert(MapPlayerMySelf.mBuffList, buffItem)
  end
end
function GameUIStarCraftMap:RemoveDynamicBuff(buffID)
  for i, v in MapPlayerMySelf.mBuffList, nil, nil do
    if v.mID == buffID then
      table.remove(MapPlayerMySelf.mBuffList, i)
      return
    end
  end
end
function GameUIStarCraftMap:CheckDynamicBuffState()
  if MapPlayerMySelf.mBuffList then
    local i = 1
    local needRefresh = false
    while i <= #MapPlayerMySelf.mBuffList do
      local buff = MapPlayerMySelf.mBuffList[i]
      if buff:GetType() == 4 then
        local timeLeft = buff.mLeftTime - (os.time() - MapPlayerMySelf.mDynamicBuffGetTime)
        if timeLeft <= 0 then
          table.remove(MapPlayerMySelf.mBuffList, i)
          MapPlayerMySelf.mProductionSpeedUp = -1
          MapPlayerMySelf.mHasDynamicBuff = false
          needRefresh = true
        else
          i = i + 1
        end
      elseif buff:GetType() == EBuff_Type.BUFF_BOAT_YARD then
        local timeLeft = buff.mLeftTime - (os.time() - MapPlayerMySelf.mBoatBuffGetTime)
        if timeLeft <= 0 then
          table.remove(MapPlayerMySelf.mBuffList, i)
          needRefresh = true
        else
          i = i + 1
        end
      else
        i = i + 1
      end
    end
    if needRefresh then
      GameUIStarCraftMap:RefreshMyBuff()
    end
  end
end
function GameUIStarCraftMap.ServerTimeChangeHandler()
  if GameStateManager:GetCurrentGameState() == GameStateStarCraft then
    GameUIStarCraftMap:CheckDynamicBuffState()
    for i, v in ipairs(MapPlayerMySelf.mStableBuffList) do
      if v:GetType() == 4 then
        DebugOut("hello we have dynamic buffs!!!")
        DebugOut("buff v = ", v)
        local leftTime = v.mLeftTime - (os.time() - MapPlayerMySelf.mDynamicBuffGetTime)
        if leftTime >= 0 then
          GameUIStarCraftMap:UpdateBuffListItem(i)
        end
      elseif v:GetType() == 6 then
        local leftTime = v.mLeftTime - (os.time() - MapPlayerMySelf.mBoatBuffGetTime)
        if leftTime >= 0 then
          GameUIStarCraftMap:UpdateBuffListItem(i)
        end
      end
    end
  end
end
function GameUIStarCraftMap:ShowBuffPanel()
  if not GameUIStarCraftMap:GetFlashObject() then
    return
  end
  self:GetFlashObject():InvokeASCallback("_root", "showPlayerBuffPanel")
end
function GameUIStarCraftMap:RefreshBuffPanelList()
  local flash = GameUIStarCraftMap:GetFlashObject()
  if not flash then
    return
  end
  flash:InvokeASCallback("_root", "clearBuffList")
  flash:InvokeASCallback("_root", "initBuffList")
  MapPlayerMySelf.mStableBuffList = {}
  for _, v in pairs(MapPlayerMySelf.mBuffList) do
    if v:GetType() == EBuff_Type.BUFF_ATTRIBUTE_PER_SECONDS then
      local curPoint = GameUIStarCraftMap.MapKeyPointList[MapPlayerMySelf.mCurrentKeyPointID]
      if curPoint and curPoint.mType == EKeyPointType.Type_Boat_Yard then
        table.insert(MapPlayerMySelf.mStableBuffList, v)
      end
    else
      table.insert(MapPlayerMySelf.mStableBuffList, v)
    end
  end
  DebugOut("refresh buff")
  DebugTable(MapPlayerMySelf.mStableBuffList)
  DebugTable(MapPlayerMySelf.mBuffList)
  tmpStableBuffList = {}
  local count = #MapPlayerMySelf.mStableBuffList
  if count > 0 then
    for i = 1, count do
      local merged = false
      local len = #tmpStableBuffList
      for j = 1, len do
        if GameUIStarCraftMap:IsCanMergeBuff(tmpStableBuffList[j], MapPlayerMySelf.mStableBuffList[i]) then
          local newBuff = GameUIStarCraftMap:MergeSameBuff(tmpStableBuffList[j], MapPlayerMySelf.mStableBuffList[i])
          DebugTable(newBuff)
          tmpStableBuffList[j] = newBuff
          merged = true
          break
        end
      end
      if not merged then
        table.insert(tmpStableBuffList, MapPlayerMySelf.mStableBuffList[i])
      end
    end
    MapPlayerMySelf.mStableBuffList = tmpStableBuffList
  end
  local buffNum = #MapPlayerMySelf.mStableBuffList
  for i = 1, buffNum do
    flash:InvokeASCallback("_root", "addBuffListItem", i)
  end
  flash:InvokeASCallback("_root", "setBuffListArrowVisible")
end
function GameUIStarCraftMap:UpdateBuffListItem(id)
  local flash = self:GetFlashObject()
  if not flash then
    return
  end
  local itemIndex = tonumber(id)
  local buff = MapPlayerMySelf.mStableBuffList[itemIndex]
  local desc = buff:GetDisplayerString()
  local value, key, preStr
  local leftTimeStr = -1
  local speedUpValue
  if buff.mID < 100 then
    preStr = "+ "
  elseif buff.mID <= 200 then
    preStr = "- "
  else
    preStr = "+ "
  end
  if buff:GetType() == EBuff_Type.BUFF_EFFECT then
    value = preStr .. buff.mBuffDetailItems[1].value .. "%"
  elseif buff:GetType() == EBuff_Type.BUFF_ATTRIBUTE_PER_SECONDS then
    preStr = "+ "
    value = preStr .. buff.mBuffDetailItems[1].limit_value .. "/" .. buff.mBuffDetailItems[1].key .. "S"
  elseif buff:GetType() == EBuff_Type.BUFF_PRODUCTION_SPEED_UP then
    value = preStr .. buff.mValue .. "%"
    leftTimeStr = buff.mLeftTime - (os.time() - MapPlayerMySelf.mDynamicBuffGetTime)
    if leftTimeStr < 0 then
      leftTimeStr = 0
    end
    leftTimeStr = GameUtils:formatTimeString(leftTimeStr)
  elseif buff:GetType() == EBuff_Type.BUFF_BOAT_YARD then
    value = preStr .. buff.mValue .. "%"
    leftTimeStr = buff.mLeftTime - (os.time() - MapPlayerMySelf.mBoatBuffGetTime)
    if leftTimeStr < 0 then
      leftTimeStr = 0
    end
    leftTimeStr = GameUtils:formatTimeString(leftTimeStr)
  else
    local floatValue = buff.mBuffDetailItems[1].value * 60
    if floatValue >= 1 then
      if 0 < MapPlayerMySelf.mProductionSpeedUp then
        speedUpValue = "<font color='#FF9000'>(" .. preStr .. GameUtils.numberConversionFloat(MapPlayerMySelf.mProductionSpeedUp / 100 * floatValue) .. "/H)</font>"
      end
      value = preStr .. GameUtils.numberConversionFloat(floatValue) .. "/H"
    else
      floatValue = floatValue * 24
      if 0 < MapPlayerMySelf.mProductionSpeedUp then
        speedUpValue = "<font color='#FF9000'>(" .. preStr .. GameUtils.numberConversionFloat(MapPlayerMySelf.mProductionSpeedUp / 100 * floatValue) .. "/D)</font>"
      end
      value = preStr .. GameUtils.numberConversionFloat(floatValue) .. "/D"
    end
    if speedUpValue then
      value = value .. speedUpValue
    end
  end
  local frame = buff:GetDisplayFrame()
  flash:InvokeASCallback("_root", "setBuffListItem", itemIndex, buff:GetType(), desc, value, frame, leftTimeStr)
end
function GameUIStarCraftMap:ShowHelpPanel()
  self:GetFlashObject():InvokeASCallback("_root", "showHelpPanel", true)
  GameUIStarCraftMap:UpdateHelpPanelTime(true)
  currentStarcraftLeftTime = 0
end
function GameUIStarCraftMap:UpdateHelpPanelTime(force)
  local tmpLeftTime = 0
  if 0 < GameStateStarCraft.mStarCraftLeftTime then
    tmpLeftTime = GameStateStarCraft.mStarCraftLeftTime - (os.time() - GameStateStarCraft.mStartCraftFetchTime)
    if tmpLeftTime < 0 then
      tmpLeftTime = 0
    end
  end
  if currentStarcraftLeftTime == tmpLeftTime and not force then
    return
  end
  currentStarcraftLeftTime = tmpLeftTime
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    local timeStr = GameUtils:formatTimeString(tmpLeftTime)
    local locStr = GameLoader:GetGameText("LC_MENU_EVENT_TIME_LEFT_CHAR")
    locStr = string.format(locStr, timeStr)
    flash_obj:InvokeASCallback("_root", "updateHelpPanelTime", locStr)
  end
end
function GameUIStarCraftMap:RequestCollectResource(keyPointIDList)
  local content = {dots = keyPointIDList}
  if keyPointIDList and #keyPointIDList > 0 then
    GameUIStarCraftMap.mCollectAll = false
  else
    GameUIStarCraftMap.mCollectAll = true
  end
  NetMessageMgr:SendMsg(NetAPIList.contention_collect_req.Code, content, GameUIStarCraftMap.RequestCollectResourceCallback, true, nil)
end
function GameUIStarCraftMap.RequestCollectResourceCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.contention_collect_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    GameUIStarCraftMap.mCollectAll = false
    return true
  elseif msgType == NetAPIList.contention_collect_ack.Code then
    if content ~= nil then
      local flash_obj = GameUIStarCraftMap:GetFlashObject()
      local hasResource = false
      local collectedTable = {}
      for i, v in ipairs(content.productions) do
        GameUIStarCraftMap.MapKeyPointList[v.dot_id].mHasResources = false
        if GameUIStarCraftMap.MapKeyPointList[v.dot_id].mServerID == MapPlayerMySelf.mServerID then
          for iResource, vResources in ipairs(v.resources) do
            hasResource = true
            if flash_obj then
              local resourceCount = GameHelper:GetAwardCount(vResources.item_type, vResources.number, vResources.no)
              local resourceFrame = GameHelper:GetAwardTypeIconFrameName(vResources.item_type, vResources.number, vResources.no)
              local resourceStr = GameHelper:GetAwardTypeText(vResources.item_type, vResources.number, vResources.no)
              if collectedTable[vResources.item_type] then
                collectedTable[vResources.item_type].count = collectedTable[vResources.item_type].count + resourceCount
              else
                collectedTable[vResources.item_type] = {}
                collectedTable[vResources.item_type].count = resourceCount
                collectedTable[vResources.item_type].name = resourceStr
              end
              flash_obj:InvokeASCallback("_root", "setupPointCollectAnim", v.dot_id, resourceFrame, resourceCount, iResource)
            end
          end
          if 0 < #v.resources then
            flash_obj:InvokeASCallback("_root", "animationPointCollect", v.dot_id)
            flash_obj:InvokeASCallback("_root", "hideKeyPointResource", v.dot_id)
          end
        end
      end
      if not hasResource then
        GameUIStarCraftMap.mCollectAll = false
        local errorContent = GameLoader:GetGameText("LC_ALERT_TC_NO_RESOURCE")
        GameTip:Show(errorContent)
      else
        if GameUIStarCraftMap.mCollectAll then
          GameUIStarCraftMap.mCollectAll = false
          local collectedStr = ""
          local txt = GameLoader:GetGameText("LC_MENU_DUNGEON_SPACE_COLLECT_DESC")
          for k, v in pairs(collectedTable) do
            local aStr = v.name .. "x " .. GameUtils.numberConversion(tonumber(v.count))
            collectedStr = collectedStr .. aStr .. " "
          end
          GameTip:Show(txt .. collectedStr)
        end
        GameTimer:Add(GameUIStarCraftMap.requestProductionTimerHanler, 120000)
      end
    end
    return true
  end
  return false
end
function GameUIStarCraftMap:HideChatIcon()
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "moveOutChatBar")
  end
end
function GameUIStarCraftMap:ShowChatIcon()
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "moveInChatBar")
  end
end
function GameUIStarCraftMap:Update(dt)
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    if GameUIStarCraftMap.updateStep == 5 then
      local vipTime = 0
      local vipTimeStr
      if GameVipDetailInfoPanel.mTmpInfoFetchTime and 0 < GameVipDetailInfoPanel.mTmpInfoFetchTime then
        vipTime = GameVipDetailInfoPanel.mTmpInfo.left_time - (os.time() - GameVipDetailInfoPanel.mTmpInfoFetchTime)
      end
      if vipTime > 0 then
        vipTimeStr = GameUtils:formatTimeString(vipTime)
      end
      flash_obj:InvokeASCallback("_root", "OnUpdate", GameUIStarCraftMap.updateDT, vipTimeStr)
      GameUIStarCraftMap.updateStep = 1
      GameUIStarCraftMap.updateDT = 0
    else
      GameUIStarCraftMap.updateStep = GameUIStarCraftMap.updateStep + 1
      GameUIStarCraftMap.updateDT = GameUIStarCraftMap.updateDT + dt
    end
    flash_obj:Update(dt)
  end
  self.mCalendar:UpdateCurrentEvents()
  self:UpdatePlayerState()
  self:UpdateHelpPanelTime(false)
  GameUIStarCraftDetail:UpdateKingRewardLeftTime()
end
function GameUIStarCraftMap:FireMissile(startID, endID, nowTime, totalTime)
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    flash_obj:InvokeASCallback("_root", "fireMissile", startID, endID, nowTime, totalTime)
  end
end
function GameUIStarCraftMap:OnFSCommand(cmd, arg)
  GameUIStarCraftDetail:OnFSCommand(cmd, arg)
  if cmd == "KeyPointClicked" then
    if GameUIStarCraftMap.MapKeyPointList[tonumber(arg)].mHasResources then
      local collectList = {}
      table.insert(collectList, tonumber(arg))
      GameUIStarCraftMap:RequestCollectResource(collectList)
    else
      GameUIStarCraftDetail:RequestKeyPointDetailData(tonumber(arg))
    end
  elseif cmd == "collectAll" then
    local hasRes = false
    for k, v in pairs(GameUIStarCraftMap.MapKeyPointList) do
      if v.mHasResources then
        hasRes = true
      end
    end
    if hasRes then
      local collectList = {}
      GameUIStarCraftMap:RequestCollectResource(collectList)
    else
      GameUIStarCraftDetail:RequestCollectResMenu()
    end
  elseif cmd == "closeMap" then
    NetMessageMgr:RegisterReconnectHandler(nil)
    GameUIBarLeft:SetServerTimeChangehandler(nil)
    GameUIStarCraftMap:OnRemoveAvatarInfoChangeHandler()
    GameUIStarCraftMap:RemoveMessageHandler()
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateMainPlanet)
  elseif cmd == "helpClicked" then
    self:ShowHelpPanel(true)
  elseif cmd == "marchClicked" then
    if GameUIStarCraftMap:IsMySelfDie() then
      local errorContent = GameLoader:GetGameText("LC_ALERT_tc_player_already_die")
      GameTip:Show(errorContent)
    elseif not MapPlayerMySelf.mIsOnPath then
      DebugOut("arg = " .. arg)
      local startID = MapPlayerMySelf.mCurrentKeyPointID
      local endID = tonumber(arg)
      if self:IsEnemyCamp(endID) then
        return
      end
      local path = GameUIStarCraftMap:CalculaeMarchPath(startID, endID, false)
      if path then
        GameUIStarCraftMap:RequestMarch(path)
      end
    end
  elseif cmd == "teleportClicked" then
    local startID = MapPlayerMySelf.mCurrentKeyPointID
    if MapPlayerMySelf.mIsOnPath then
      startID = MapPlayerMySelf.mCurrentStartPointID
    end
    local endID = tonumber(arg)
    if self:IsEnemyCamp(endID) then
      return
    end
    local path = GameUIStarCraftMap:CalculaeMarchPath(startID, endID, true)
    if path then
      MapPlayerMySelf.mCurrentStartTeleportID = startID
      MapPlayerMySelf.mCurrentEndTeleportID = endID
      GameUIStarCraftMap:RequestTeleport(path)
    end
  elseif cmd == "teleport_move_in" then
    local flash_obj = GameUIStarCraftMap:GetFlashObject()
    local isKing = GameUIStarCraftMap.mKingUserID == MapPlayerMySelf.mID
    flash_obj:InvokeASCallback("_root", "animationTeleportMoveOut", MapPlayerMySelf.mCurrentEndTeleportID, isKing)
  elseif cmd == "reviveClicked" then
    GameUIStarCraftMap:showCostConfirmPanel(GameStateStarCraft.mRevivePrice, "LC_MENU_TC_REBUILD_IMMEDIATELY_CONFIRM_ALERT", GameUIStarCraftMap.reviveHandler)
  elseif cmd == "player_state_clicked" then
    if MapPlayerMySelf.mCurrentHP <= 0 then
      GameUIStarCraftMap:ShowPlayerDiePanel()
    else
      local flash_obj = GameUIStarCraftMap:GetFlashObject()
      if flash_obj then
        DebugOut("asdkllalsdlajdlaksjd:", MapPlayerMySelf.mIsOnPath, MapPlayerMySelf.mID, MapPlayerMySelf.mCurrentKeyPointID)
        flash_obj:InvokeASCallback("_root", "tryToDisplayMySelfInCenter", MapPlayerMySelf.mIsOnPath, MapPlayerMySelf.mID, MapPlayerMySelf.mCurrentKeyPointID)
      end
    end
  elseif cmd == "EnterVip" then
    if not GameGlobalData:FTPVersion() then
      GameVip:showVip(true)
    end
  elseif cmd == "showPointNameOnMap" then
    GameUIStarCraftMap:ShowPointNameOnMap(true)
  elseif cmd == "hidePointNameOnMap" then
    GameUIStarCraftMap:ShowPointNameOnMap(false)
  elseif cmd == "enterSubMenu" then
    GameStateStarCraft:EnterSubMenu()
  elseif cmd == "leaveSubMenu" then
    GameStateStarCraft:LeaveSubMenu()
  elseif cmd == "PopupChatWindow" then
    self:HideChatIcon()
    GameStateStarCraft:AddObject(GameUIChat)
    if GameUIStarCraftMap.firstTimeEnterChat == true then
      GameUIChat:SelectChannel("battle")
    else
      GameUIChat:SelectChannel(GameUIChat.activeChannel)
    end
  elseif cmd == "needUpdateBuffListItem" then
    GameUIStarCraftMap:UpdateBuffListItem(arg)
  elseif cmd == "showBuffList" then
    GameUIStarCraftMap:ShowBuffPanel()
    GameUIStarCraftMap:RefreshBuffPanelList()
  elseif cmd == "showCalendar" then
    GameUIStarCraftMap.mCalendar:ShowTimeLine()
  elseif cmd == "replayDieClicked" then
    GameUIStarCraftMap:RequestDieReplay()
  end
  GameUIStarCraftMap.mCalendar:OnFSCommand(cmd, arg)
end
function GameUIStarCraftMap:ForceOut()
  NetMessageMgr:RegisterReconnectHandler(nil)
  GameUIStarCraftMap:OnRemoveAvatarInfoChangeHandler()
  GameUIStarCraftMap:RemoveMessageHandler()
  GameStateManager:SetCurrentGameState(GameStateManager.GameStateMainPlanet)
end
function GameUIStarCraftMap:ShowPointNameOnMap(isShow)
  local flash_obj = GameUIStarCraftMap:GetFlashObject()
  if flash_obj then
    local keyPointArray = ""
    local hasKingArray = ""
    for k, v in pairs(GameUIStarCraftMap.MapKeyPointList) do
      keyPointArray = keyPointArray .. v.mID .. "\001"
      local hasKing = "false"
      if v.mHasKing then
        hasKing = "true"
      end
      hasKingArray = hasKingArray .. hasKing .. "\001"
    end
    flash_obj:InvokeASCallback("_root", "showKeyPointName", keyPointArray, isShow, hasKingArray)
  end
end
function GameUIStarCraftMap:getPathByStartEnd(startPoint, endPoint)
  for k, v in pairs(GameUIStarCraftMap.MapPathList) do
    if v.mStartPoint == startPoint and v.mEndPoint == endPoint or v.mStartPoint == endPoint and v.mEndPoint == startPoint then
      return v
    end
  end
  return nil
end
function GameUIStarCraftMap:IsPathConnected(path)
  if path then
    local startPoint = GameUIStarCraftMap.MapKeyPointList[path.mStartPoint]
    local endPoint = GameUIStarCraftMap.MapKeyPointList[path.mEndPoint]
    if startPoint.mServerID == endPoint.mServerID then
      return true
    else
      return false
    end
  end
  return false
end
function GameUIStarCraftMap:DeterminServerColor(serverID)
  if not GameUIStarCraftMap.ServerColorFrame[serverID] then
    local myServerID = MapPlayerMySelf.mServerID
    DebugOut("Determin server color ", serverID)
    if serverID == myServerID then
      DebugOut("green color!!!")
      GameUIStarCraftMap.ServerColorFrame[serverID] = "green"
    else
      for k, v in pairs(GameUIStarCraftMap.ServerColorFrame) do
        if tonumber(k) ~= myServerID then
          if serverID > tonumber(k) then
            GameUIStarCraftMap.ServerColorFrame[k] = "red"
            GameUIStarCraftMap.ServerColorFrame[serverID] = "purple"
            DebugOut("purple color")
          elseif serverID < tonumber(k) then
            GameUIStarCraftMap.ServerColorFrame[k] = "purple"
            GameUIStarCraftMap.ServerColorFrame[serverID] = "red"
            DebugOut("red color")
          end
        end
      end
      if not GameUIStarCraftMap.ServerColorFrame[serverID] then
        GameUIStarCraftMap.ServerColorFrame[serverID] = "red"
        DebugOut("no color, so red")
      end
    end
  end
end
function GameUIStarCraftMap:DeterminAllServerColor()
  local sortFunc = function(a, b)
    return tonumber(a) < tonumber(b)
  end
  local tmpServerList = {}
  for k, v in pairs(GameUIStarCraftMap.serverIDList) do
    table.insert(tmpServerList, v)
  end
  table.sort(tmpServerList, sortFunc)
  local index = 1
  for i, v in ipairs(tmpServerList) do
    GameUIStarCraftMap.ServerColorFrame[v] = GameUIStarCraftMap.ColorList[index]
    index = index + 1
  end
  GameUIStarCraftMap.ServerColorFrame[MapPlayerMySelf.mServerID] = "green"
end
function GameUIStarCraftMap:GetServerIDByKeyPoint(pointID)
  if GameUIStarCraftMap.MapKeyPointList[pointID] then
    return GameUIStarCraftMap.MapKeyPointList[pointID].mServerID
  end
  return nil
end
function GameUIStarCraftMap:GetPointTypeByID(pointID)
  if GameUIStarCraftMap.MapKeyPointList[pointID] then
    return GameUIStarCraftMap.MapKeyPointList[pointID].mType
  end
  return nil
end
function GameUIStarCraftMap:GetDetailInfoColorFrameByPointID(pointID)
  local serverID = GameUIStarCraftMap:GetServerIDByKeyPoint(pointID)
  if serverID then
    if serverID == MapPlayerMySelf.mServerID then
      return "green"
    else
      return "red"
    end
  end
  return "blue"
end
function GameUIStarCraftMap:GetBuffListByPointID(pointID)
  if GameUIStarCraftMap.MapKeyPointList[pointID] then
    return GameUIStarCraftMap.MapKeyPointList[pointID].mBuffList
  end
  return nil
end
function GameUIStarCraftMap:GetPointNameByPointID(pointID)
  if GameUIStarCraftMap.MapKeyPointList[pointID] then
    return GameUIStarCraftMap.MapKeyPointList[pointID].mPointName
  end
  return nil
end
local MAX_INT = -10000
function GameUIStarCraftMap:CalculaeMarchPath(startPoint, endPoint, isConnection)
  DebugOut("calculate start = ", startPoint)
  DebugOut("calculate end = ", endPoint)
  if startPoint <= 0 or endPoint <= 0 then
    return nil
  end
  local pathList = {}
  if startPoint == endPoint then
    return pathList
  end
  local sQueue = {}
  local uQueue = {}
  local pathNodeStart = {
    point = startPoint,
    parentNode = nil,
    distanceToStart = 0
  }
  sQueue[pathNodeStart.point] = pathNodeStart
  for k, v in pairs(GameUIStarCraftMap.MapKeyPointList) do
    if v.mID ~= startPoint then
      local pathNode = {
        point = v.mID,
        parentNode = pathNodeStart,
        distanceToStart = 0
      }
      if GameUIStarCraftMap:IsEnemyCamp(v.mID) then
        pathNode.distanceToStart = MAX_INT
      else
        local path = GameUIStarCraftMap:getPathByStartEnd(startPoint, v.mID)
        if path then
          if isConnection then
            if GameUIStarCraftMap:IsPathConnected(path) or v.mID == endPoint then
              pathNode.distanceToStart = path.mDistance
            else
              pathNode.distanceToStart = MAX_INT
            end
          else
            pathNode.distanceToStart = path.mDistance
          end
        else
          pathNode.distanceToStart = MAX_INT
        end
      end
      table.insert(uQueue, pathNode)
    end
  end
  local finding = true
  local curParentNode = pathNodeStart
  while finding do
    local minDistanceNode
    local index = -1
    for i, v in ipairs(uQueue) do
      if not minDistanceNode then
        minDistanceNode = v
        index = i
      elseif minDistanceNode.distanceToStart == MAX_INT and v.distanceToStart ~= MAX_INT then
        minDistanceNode = v
        index = i
      elseif minDistanceNode.distanceToStart ~= MAX_INT and v.distanceToStart ~= MAX_INT and minDistanceNode.distanceToStart > v.distanceToStart then
        minDistanceNode = v
        index = i
      end
    end
    if not minDistanceNode then
      finding = false
    else
      table.remove(uQueue, index)
      sQueue[minDistanceNode.point] = minDistanceNode
      for i, v in ipairs(uQueue) do
        local tmpPath = GameUIStarCraftMap:getPathByStartEnd(minDistanceNode.point, v.point)
        if tmpPath and minDistanceNode.distanceToStart ~= MAX_INT and not GameUIStarCraftMap:IsEnemyCamp(v.point) then
          if isConnection then
            if (GameUIStarCraftMap:IsPathConnected(tmpPath) or v.point == endPoint) and (tmpPath.mDistance + minDistanceNode.distanceToStart < v.distanceToStart or v.distanceToStart == MAX_INT) then
              v.distanceToStart = tmpPath.mDistance + minDistanceNode.distanceToStart
              v.parentNode = minDistanceNode
            end
          elseif tmpPath.mDistance + minDistanceNode.distanceToStart < v.distanceToStart or v.distanceToStart == MAX_INT then
            v.distanceToStart = tmpPath.mDistance + minDistanceNode.distanceToStart
            v.parentNode = minDistanceNode
          end
        end
      end
    end
  end
  DebugOut("sQueue = ")
  DebugTable(sQueue)
  local parent = sQueue[endPoint].parentNode
  local current = sQueue[endPoint]
  if current.distanceToStart == MAX_INT then
    return pathList
  end
  while current.point ~= startPoint do
    DebugOut("current.point = ", current.point)
    DebugOut("parent.point = ", parent.point)
    local pathLine = GameUIStarCraftMap:getPathByStartEnd(current.point, parent.point)
    local pathNodeInfo = {
      dot_start = parent.point,
      line_id = pathLine.mID
    }
    table.insert(pathList, 1, pathNodeInfo)
    current = parent
    if current.parentNode then
      parent = sQueue[parent.parentNode.point]
    end
  end
  DebugOut("we find a path form ", startPoint .. " to " .. endPoint)
  DebugTable(pathList)
  return pathList
end
function GameUIStarCraftMap:GetColorFrameByServerID(serverID)
  if serverID and GameUIStarCraftMap.ServerColorFrame[serverID] then
    return GameUIStarCraftMap.ServerColorFrame[serverID]
  end
  return "free"
end
function GameUIStarCraftMap:GetColorFrameByPointID(pointID)
  if GameUIStarCraftMap.MapKeyPointList[pointID] then
    serverID = GameUIStarCraftMap.MapKeyPointList[pointID].mServerID
    if serverID then
      return GameUIStarCraftMap.ServerColorFrame[serverID]
    end
    return "free"
  end
  return "free"
end
function GameUIStarCraftMap:ResetPlayerMySelf()
  MapPlayerMySelf.mCurrentHP = -1
  MapPlayerMySelf.mMaxHP = -1
  MapPlayerMySelf.mCurrentKeyPointID = -1
  MapPlayerMySelf.mCurrentPathID = -1
  MapPlayerMySelf.mCurrentStartPointID = -1
  MapPlayerMySelf.mCurrentEndPointID = -1
  MapPlayerMySelf.mIsOnPath = false
  MapPlayerMySelf.mLeftTime = 0
  MapPlayerMySelf.mNotifyGetTime = 0
  MapPlayerMySelf.mInited = false
  MapPlayerMySelf.mBuffList = {}
  MapPlayerMySelf.mIsWaitingRevive = false
  MapPlayerMySelf.mDieBattleID = -1
  MapPlayerMySelf.mCurrentPathList = nil
  MapPlayerMySelf.mCurrentStartTeleportID = -1
  MapPlayerMySelf.mCurrentEndTeleportID = -1
  MapPlayerMySelf.mSpeed = 0
  MapPlayerMySelf.mDieBattleID = -1
end
function GameUIStarCraftMap:IsEnemyCamp(pointID)
  local point = GameUIStarCraftMap.MapKeyPointList[pointID]
  if point.mType == EKeyPointType.Type_Camp and point.mServerID ~= MapPlayerMySelf.mServerID then
    return true
  end
  return false
end
function GameUIStarCraftMap:IsMySelfOnPath()
  return MapPlayerMySelf.mIsOnPath
end
function GameUIStarCraftMap:IsMySelfDie()
  return MapPlayerMySelf.mCurrentHP <= 0
end
function GameUIStarCraftMap:GetMyPointID()
  return MapPlayerMySelf.mCurrentKeyPointID
end
function GameUIStarCraftMap:GetMySpeed()
  return MapPlayerMySelf.mSpeed
end
function GameUIStarCraftMap:GetKeyPointName(pointID)
  if GameUIStarCraftMap.MapKeyPointList[pointID] then
    return GameUIStarCraftMap.MapKeyPointList[pointID].mPointName
  end
end
function GameUIStarCraftMap:GetMyStartPointID()
  if MapPlayerMySelf.mIsOnPath then
    return MapPlayerMySelf.mCurrentStartPointID
  else
    return MapPlayerMySelf.mCurrentKeyPointID
  end
end
function GameUIStarCraftMap:GetMyCurrentPathEndPoint()
  if GameUIStarCraftMap.MapPlayerList[MapPlayerMySelf.mID] then
    return GameUIStarCraftMap.MapPlayerList[MapPlayerMySelf.mID].mEndPoint
  end
  return nil
end
function GameUIStarCraftMap:GetServerDisplayNameByServerID(serverID)
  if serverID then
    return GameUIStarCraftMap.ServerName[serverID]
  end
  return nil
end
function GameUIStarCraftMap:GetServerDisplayNameByPointID(pointID)
  if GameUIStarCraftMap.MapKeyPointList[pointID] then
    serverID = GameUIStarCraftMap.MapKeyPointList[pointID].mServerID
    return GameUIStarCraftMap.ServerName[serverID]
  end
  return nil
end
function GameUIStarCraftMap:GetSelfServerName()
  return GameUIStarCraftMap.ServerName[MapPlayerMySelf.mServerID]
end
function GameUIStarCraftMap:showCostConfirmPanel(costPrice, strID, messageboxHandler)
  local info_content = GameLoader:GetGameText(strID)
  info_content = string.format(info_content, costPrice)
  GameUtils:CreditCostConfirm(info_content, messageboxHandler)
end
function GameUIStarCraftMap:KillMySelf()
  local flash_obj = GameUIStarCraftMap:GetFlashObject()
  if flash_obj then
    flash_obj:InvokeASCallback("_root", "killPlayer", MapPlayerMySelf.mID)
  end
end
function GameUIStarCraftMap:removeTeleportAnim()
  local flash_obj = GameUIStarCraftMap:GetFlashObject()
  if flash_obj then
    flash_obj:InvokeASCallback("_root", "animationTeleportStop")
  end
end
function GameUIStarCraftMap:MsgIncoming(content)
  if content.channel == "tc_chat_channel" then
    table.insert(m_chatMsgQueue, content)
    DebugOut("nowInsertChatMsg:")
    DebugOutPutTable(content, "content")
  end
end
function GameUIStarCraftMap:UpdateChatBarText(htmlText)
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    flash_obj:InvokeASCallback("_root", "updateChatBarText", htmlText)
  end
end
function GameUIStarCraftMap.requestProductionTimerHanler()
  local content = {
    notifies = {
      "contention_production"
    }
  }
  NetMessageMgr:SendMsg(NetAPIList.contention_notifies_req.Code, content, nil, false, nil)
  return nil
end
function GameUIStarCraftMap:GetMyselfServerID()
  return MapPlayerMySelf.mServerID
end
function GameUIStarCraftMap.IncomeBuffUpdate()
  local tmpLeftTime = GameUIStarCraftMap.mIncomeBuffLeftTime - (os.time() - GameUIStarCraftMap.mIncomeBuffLeftTimeFetchTime)
  if tmpLeftTime < 0 then
    GameUIStarCraftMap.mCurIncomeBuffLeftTime = 0
    GameUIStarCraftMap.mIsIncomeBuffLeftTimeShow = false
    local flash_obj = GameUIStarCraftMap:GetFlashObject()
    if flash_obj then
      flash_obj:InvokeASCallback("_root", "setIncomeBuff", false, "")
    end
    return false
  else
    GameUIStarCraftMap.mCurIncomeBuffLeftTime = tmpLeftTime
    local timeStr = GameUtils:formatTimeString(tmpLeftTime)
    local flash_obj = GameUIStarCraftMap:GetFlashObject()
    if flash_obj then
      flash_obj:InvokeASCallback("_root", "setIncomeBuff", true, timeStr)
    else
      return false
    end
    return 500
  end
end
function GameUIStarCraftMap:SetFleetKingFlg(userID, isVisible)
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    flash_obj:InvokeASCallback("_root", "SetFleetKingFlg", userID, isVisible)
  end
end
function GameUIStarCraftMap.KingChangeNtfHandler(content)
  DebugOut("contention_king_change_ntf")
  DebugTable(content)
  if content ~= nil then
    if -1 ~= GameUIStarCraftMap.mLastKeyPointIDKingIn and GameUIStarCraftMap.MapKeyPointList[GameUIStarCraftMap.mLastKeyPointIDKingIn] then
      GameUIStarCraftMap.MapKeyPointList[GameUIStarCraftMap.mLastKeyPointIDKingIn].mHasKing = false
      GameUIStarCraftMap.mLastKeyPointIDKingIn = -1
    end
    local nowKingUserID = tonumber(content.now_king)
    if -1 ~= GameUIStarCraftMap.mKingUserID and GameUIStarCraftMap.mKingUserID ~= nowKingUserID then
      GameUIStarCraftMap:SetFleetKingFlg(GameUIStarCraftMap.mKingUserID, false)
    end
    GameUIStarCraftMap.mKingUserID = nowKingUserID
    if -1 ~= GameUIStarCraftMap.mKingUserID then
      GameUIStarCraftMap:SetFleetKingFlg(GameUIStarCraftMap.mKingUserID, true)
    end
  end
end
function GameUIStarCraftMap:IsCanMergeBuff(buff1, buff2)
  if buff1 and buff2 and buff1:GetType() == buff2:GetType() and buff1:GetDisplayFrame() == buff2:GetDisplayFrame() then
    if buff1:GetType() == EBuff_Type.BUFF_PRODUCTION_SPEED_UP then
      if buff1.mLeftTime == buff2.mLeftTime then
        return true
      else
        return false
      end
    elseif buff1:GetType() == EBuff_Type.BUFF_BOAT_YARD then
      return false
    else
      return true
    end
  else
    return false
  end
end
function GameUIStarCraftMap:MergeSameBuff(buff1, buff2)
  local newBuff = MapKeyPointBuff:new()
  newBuff.mID = buff1.mID
  newBuff.mDisplayString = buff1:GetDisplayerString()
  newBuff.mDisplayFrame = buff1:GetDisplayFrame()
  if buff1:GetType() == EBuff_Type.BUFF_PRODUCTION_SPEED_UP then
    newBuff.mValue = tonumber(buff1.value) + tonumber(buff1.value)
    newBuff.mLeftTime = buff1.mLeftTime
  else
    newBuff.mIcon = buff1.mIcon
    newBuff.mBuffDetailItems = {}
    local tmpItem = {}
    tmpItem.value = tonumber(buff1.mBuffDetailItems[1].value) + tonumber(buff2.mBuffDetailItems[1].value)
    tmpItem.limit_value = buff1.mBuffDetailItems[1].limit_value
    tmpItem.key = buff1.mBuffDetailItems[1].key
    tmpItem.type_id = buff1.mBuffDetailItems[1].type_id
    table.insert(newBuff.mBuffDetailItems, tmpItem)
  end
  return newBuff
end
function GameUIStarCraftMap.OnEventStarted(event)
  local flash_obj = GameUIStarCraftMap:GetFlashObject()
  if flash_obj then
    flash_obj:InvokeASCallback("_root", "SetEventStart", true)
  end
  local keyPointIDArray = ""
  local typeArray = ""
  local nameArray = ""
  local hasKingArray = ""
  local eventStateArray = ""
  if event:IsResourceEvent() then
    local pointIDs = event:GetEventEffectPointList()
    DebugOut("begin pointIDs = ")
    DebugTable(pointIDs)
    local x = 1
    if flash_obj then
      for i, v in ipairs(pointIDs) do
        if GameUIStarCraftMap.MapKeyPointList[v] then
          local point = GameUIStarCraftMap.MapKeyPointList[v]
          point.mCurrentActiveEvent = event
          local hasKingStr = "false"
          if point.mHasKing then
            hasKingStr = "true"
          end
          local eventStarted = 1
          keyPointIDArray = keyPointIDArray .. point.mID .. "\001"
          typeArray = typeArray .. point.mType .. "\001"
          nameArray = nameArray .. point.mPointName .. "\001"
          hasKingArray = hasKingArray .. hasKingStr .. "\001"
          eventStateArray = eventStateArray .. eventStarted .. "\001"
        end
      end
      flash_obj:InvokeASCallback("_root", "setKeyPointType", keyPointIDArray, typeArray, nameArray, hasKingArray, eventStateArray)
    end
  elseif event:IsMissionEvent() then
    local pointList = ""
    local pointTypelist = ""
    local activeStateList = ""
    if flash_obj then
      for iSubMission, vSubMission in ipairs(event.mMissionDetails) do
        local targetPoint = vSubMission:GetMissionTargetPoint()
        if targetPoint then
          for iPoint, vPoint in ipairs(targetPoint) do
            local point = GameUIStarCraftMap.MapKeyPointList[vPoint]
            pointList = pointList .. vPoint .. "\001"
            pointTypelist = pointTypelist .. point.mType .. "\001"
            activeStateList = activeStateList .. 1 .. "\001"
          end
        end
      end
      DebugOut("mission eventStart!!!!!!!!!")
      flash_obj:InvokeASCallback("_root", "setKeyPointAsMissionTarget", pointList, pointTypelist, activeStateList)
    end
  end
end
function GameUIStarCraftMap.OnEventEnded(event)
  local flash_obj = GameUIStarCraftMap:GetFlashObject()
  if flash_obj then
    local hasActiveEvent = GameUIStarCraftMap.mCalendar:HasActiveEvent()
    flash_obj:InvokeASCallback("_root", "SetEventStart", hasActiveEvent)
  end
  local keyPointIDArray = ""
  local typeArray = ""
  local nameArray = ""
  local hasKingArray = ""
  local eventStateArray = ""
  if event:IsResourceEvent() then
    local pointIDs = event:GetEventEffectPointList()
    DebugOut("end pointIDs = ")
    DebugTable(pointIDs)
    if flash_obj then
      for i, v in ipairs(pointIDs) do
        if GameUIStarCraftMap.MapKeyPointList[v] then
          local point = GameUIStarCraftMap.MapKeyPointList[v]
          point.mCurrentActiveEvent = nil
          local hasKingStr = "false"
          if point.mHasKing then
            hasKingStr = "true"
          end
          local eventStarted = 0
          keyPointIDArray = keyPointIDArray .. point.mID .. "\001"
          typeArray = typeArray .. point.mType .. "\001"
          nameArray = nameArray .. point.mPointName .. "\001"
          hasKingArray = hasKingArray .. hasKingStr .. "\001"
          eventStateArray = eventStateArray .. eventStarted .. "\001"
        end
      end
      flash_obj:InvokeASCallback("_root", "setKeyPointType", keyPointIDArray, typeArray, nameArray, hasKingArray, eventStateArray)
    end
  elseif event:IsMissionEvent() then
    local pointList = ""
    local pointTypelist = ""
    local activeStateList = ""
    if flash_obj then
      for iSubMission, vSubMission in ipairs(event.mMissionDetails) do
        local targetPoint = vSubMission:GetMissionTargetPoint()
        if targetPoint then
          for iPoint, vPoint in ipairs(targetPoint) do
            local point = GameUIStarCraftMap.MapKeyPointList[vPoint]
            pointList = pointList .. vPoint .. "\001"
            pointTypelist = pointTypelist .. point.mType .. "\001"
            activeStateList = activeStateList .. 0 .. "\001"
          end
        end
      end
      flash_obj:InvokeASCallback("_root", "setKeyPointAsMissionTarget", pointList, pointTypelist, activeStateList)
    end
  end
end
function GameUIStarCraftMap:GetEventByPointID(pointID)
  local point = GameUIStarCraftMap.MapKeyPointList[pointID]
  return point.mCurrentActiveEvent
end
function GameUIStarCraftMap:SetKeyPointAllPower(pointID)
  if GameUIStarCraftMap.MapKeyPointList[pointID] then
    local allPower = GameUIStarCraftMap.MapKeyPointList[pointID].mAllPower
    local flash_obj = GameUIStarCraftMap:GetFlashObject()
    if flash_obj then
      flash_obj:InvokeASCallback("_root", "SetKeyPointAllPower", GameUtils.numberAddComma(allPower))
    end
  end
end
function GameUIStarCraftMap.UpdateChatButtonStatus()
  DebugOut("UpdateChatButtonStatus")
  local chatCount = GameGlobalData:GetData("newChatTotalCount")
  local flash_obj = GameUIStarCraftMap:GetFlashObject()
  if flash_obj == nil then
    return
  end
  if chatCount and chatCount > 0 then
    flash_obj:InvokeASCallback("_root", "lua2fs_ShowOrangeChatBtn", true)
  else
    flash_obj:InvokeASCallback("_root", "lua2fs_ShowOrangeChatBtn", false)
  end
end
function GameUIStarCraftMap:RequestDieReplay()
  local battleID = MapPlayerMySelf.mDieBattleID
  DebugOut("BattleReplay ", battleID)
  if GameStateManager:GetCurrentGameState().fightRoundData then
    GameStateManager:GetCurrentGameState().fightRoundData = nil
  end
  local content = {id = battleID}
  NetMessageMgr:SendMsg(NetAPIList.contention_logbattle_detail_req.Code, content, GameUIStarCraftMap.RequestDieReplayCallback, true, nil)
end
function GameUIStarCraftMap.RequestDieReplayCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.contention_logbattle_detail_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.contention_logbattle_detail_ack.Code and content.report then
    GameHelper:StartUpABattle(content.report, content.reward_list, GameHelper.EBattleResultType.TYPE_CHALLENGE, GameUIStarCraftMap.DieBattlePlayFinished)
    return true
  end
  return false
end
function GameUIStarCraftMap.DieBattlePlayFinished()
  GameUIStarCraftMap:ShowPlayerDiePanel()
end
if AutoUpdate.isAndroidDevice then
  function GameUIStarCraftMap.OnAndroidBack()
    if GameUIStarCraftDetail.IsShowCollectMenu then
      GameUIStarCraftMap:GetFlashObject():InvokeASCallback("_root", "hideCollectResMenu")
    elseif GameUIStarCraftDetail.mCalendar and GameUIStarCraftDetail.mCalendar:IsTimelineShow() then
      GameUIStarCraftMap:GetFlashObject():InvokeASCallback("_root", "HideEventCalendarWindow")
    elseif GameUIStarCraftDetail.mIsRewardPanelShow then
      GameUIStarCraftMap:GetFlashObject():InvokeASCallback("_root", "HideRewardPanelWindow")
    elseif GameUIStarCraftDetail.mHistory.inHistory then
      GameUIStarCraftMap:GetFlashObject():InvokeASCallback("_root", "HideHistoryWindow")
    elseif GameUIStarCraftDetail.IsShowRankWindow then
      GameUIStarCraftMap:GetFlashObject():InvokeASCallback("_root", "HideRankWindow")
    elseif GameUIStarCraftDetail.IsShowBattleReportWindow then
      GameUIStarCraftMap:GetFlashObject():InvokeASCallback("_root", "HideBattleReportWindow")
    elseif GameUIStarCraftDetail.mPlayerInfoPanelOpened then
      GameUIStarCraftDetail:OnFSCommand("closePlayerInfo")
    else
      GameUIStarCraftMap:OnFSCommand("closeMap")
    end
  end
end
