local GameAreaPopBox = LuaObjectManager:GetLuaObject("GameAreaPopBox")
local GameStateCampaignSelection = GameStateManager.GameStateCampaignSelection
function GameAreaPopBox:OnInitGame()
  GameGlobalData:RegisterDataChangeCallback("progress", self.RefreshProgress)
end
function GameAreaPopBox.RefreshProgress()
  if GameStateManager:GetCurrentGameState():IsObjectInState(GameAreaPopBox) then
    GameAreaPopBox:RefreshAreaSectiion()
    GameAreaPopBox:RefreshContent()
  end
end
function GameAreaPopBox:ShowPopBox(areaId, sectionId)
  self.areaId = areaId or 1
  self.sectionId = sectionId or 1
  self.enterPress = false
  self:GetFlashObject():InvokeASCallback("_root", "showAreaPopBox", self.areaId)
  self:RefreshContent()
end
function GameAreaPopBox:RefreshAreaSectiion()
  local progress = GameGlobalData:GetData("progress")
  local completedId = self:GetCompletedId()
  self:GetFlashObject():InvokeASCallback("_root", "RefreshSection", self.sectionId, completedId)
  if completedId > 0 and completedId >= self.sectionId then
    self:GetFlashObject():SetBtnEnable("_root.BODY.info_box.btn_enter", true)
  else
    self:GetFlashObject():SetBtnEnable("_root.BODY.info_box.btn_enter", false)
  end
end
function GameAreaPopBox:RefreshContent()
  local areaNameText = GameLoader:GetGameText("LC_MENU_AREA_NAME_" .. self.areaId)
  local sectionNameText = GameLoader:GetGameText("LC_MENU_SECTION_NAME_" .. self.areaId .. "_" .. self.sectionId)
  local sectionDesText = GameLoader:GetGameText("LC_MENU_SECTION_DES_" .. self.areaId .. "_" .. self.sectionId)
  local progress = GameGlobalData:GetData("progress")
  local canleft, canright = self.sectionId <= progress.chapter - 1, true
  self:GetFlashObject():InvokeASCallback("_root", "RefreshContent", areaNameText, sectionNameText, sectionDesText, canleft, canright)
end
function GameAreaPopBox:GetCompletedId()
  local progress = GameGlobalData:GetData("progress")
  local completedId = -1
  if self.areaId == progress.act then
    completedId = progress.chapter
  end
  return completedId
end
function GameAreaPopBox:OnFSCommand(cmd, arg)
  if cmd == "popBoxFadeOut" then
    GameStateManager:GetCurrentGameState():EraseObject(self)
    if self.enterPress then
      GameStateCampaignSelection:Activate(self.areaId, self.sectionId)
      GameStateCampaignSelection:ResetSceneShot()
    end
  end
  if cmd == "enterPress" then
    self.enterPress = true
  end
  if cmd == "partShowIn" then
    self:GetFlashObject():InvokeASCallback("_root", "showPart", self.sectionId, self:GetCompletedId())
  end
  if cmd == "AreaLeftPress" or cmd == "AreaRightPress" then
    if cmd == "AreaLeftPress" then
      self.sectionId = self.sectionId - 1
    else
      self.sectionId = self.sectionId + 1
    end
    GameAreaPopBox:RefreshAreaSectiion()
    GameAreaPopBox:RefreshContent()
  end
  if cmd == "sectionChange" then
    local id = tonumber(arg)
    if id and id ~= self.sectionId then
      self.sectionId = id
      GameAreaPopBox:RefreshAreaSectiion()
      GameAreaPopBox:RefreshContent()
    end
  end
end
