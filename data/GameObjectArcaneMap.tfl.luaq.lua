local GameStateArcane = GameStateManager.GameStateArcane
local GameObjectArcaneMap = LuaObjectManager:GetLuaObject("GameObjectArcaneMap")
local GameObjectArcaneEnhance = LuaObjectManager:GetLuaObject("GameObjectArcaneEnhance")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameUIEnemyInfo = LuaObjectManager:GetLuaObject("GameUIEnemyInfo")
local GameUIArcaneEnemyInfo = LuaObjectManager:GetLuaObject("GameUIArcaneEnemyInfo")
local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
function GameObjectArcaneMap:OnAddToGameState()
  self:LoadFlashObject()
  self:UpdateArcaneData()
  self:GetFlashObject():InvokeASCallback("_root", "moveInAll")
  GameGlobalData:RegisterDataChangeCallback("arcane_info", GameObjectArcaneMap.OnArcaneInfoChange)
  self:RefreshResourceCount()
  GameGlobalData:RegisterDataChangeCallback("item_count", self.RefreshResourceCount)
end
function GameObjectArcaneMap:OnEraseFromGameState()
  self.m_arcaneMapInfo = self:GetFlashObject():InvokeASCallback("_root", "recordBattleMapInfo")
  self:UnloadFlashObject()
  GameGlobalData:RemoveDataChangeCallback("arcane_info", GameObjectArcaneMap.OnArcaneInfoChange)
end
function GameObjectArcaneMap:RestoreBattleMap()
  DebugOut("RestoreArcaneMap", self.m_arcaneMapInfo)
  if not self.m_arcaneMapInfo or not self:GetFlashObject() then
    return
  end
  local param_list = LuaUtils:string_split(self.m_arcaneMapInfo, "^")
  local shipX = param_list[1]
  local shipY = param_list[2]
  local shipRot = param_list[3]
  self:GetFlashObject():InvokeASCallback("_root", "restoreInfo", shipX, shipY, shipRot)
end
function GameObjectArcaneMap:OnFSCommand(cmd, arg)
  if cmd == "onClose" then
    GameStateArcane:Quit()
  elseif cmd == "BGClicked" then
  elseif cmd == "openEnhance" then
    GameStateArcane:AddObject(GameObjectArcaneEnhance)
  elseif cmd == "TriggerTarget" then
    GameStateArcane:AddObject(GameUIArcaneEnemyInfo)
  elseif cmd == "JAM" then
    local arcaneInfo = GameStateArcane:GetArcaneInfo()
    local function callback()
      NetMessageMgr:SendMsg(NetAPIList.ac_jam_req.Code, nil, self.NetCallbackJam, true, nil)
    end
    local text_content = GameLoader:GetGameText("LC_MENU_COST_CREDIT_JAM_CHAR")
    text_content = string.format(text_content, arcaneInfo.jam_cost or 240)
    if GameGlobalData:GetItemCount(2505) > 0 then
      text_content = string.format(GameLoader:GetGameText("LC_MENU_ITEM_USE_ALERT"), GameLoader:GetGameText("LC_ITEM_ITEM_NAME_2505"))
    end
    GameUtils:CreditCostConfirm(text_content, callback)
  elseif cmd == "Touch_Sweep" then
    local arcaneInfo = GameStateArcane:GetArcaneInfo()
    if arcaneInfo.can_sweep == 0 then
      GameTip:Show(GameLoader:GetGameText("LC_MENU_FINISH_BATTLE_TRAINING_VIP"))
    else
      local _callback = function(msgType, content)
        if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.ac_sweep_req.Code then
          if content.code ~= 0 then
            local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
            GameUIGlobalScreen:ShowAlert("error", content.code, nil)
          end
          return true
        end
        return false
      end
      NetMessageMgr:SendMsg(NetAPIList.ac_sweep_req.Code, nil, _callback, true, nil)
    end
  end
end
function GameObjectArcaneMap:UpdateArcaneData()
  local arcaneInfo = GameStateArcane:GetArcaneInfo()
  self:UpdateProgressInfo()
  self:UpdateAvailableCommanderCount()
  self:UpdateJamedStatus()
  self:UpdateTechPointInfo()
  self:UpdateArcaneSupplyInfo()
  self:UpdateRefreshTime()
  self:UpdateSweepInfo()
  for indexStep = 1, 5 do
    if indexStep <= arcaneInfo.ac_step then
      local battleInfo = GameDataAccessHelper:GetArcaneBattleInfo(arcaneInfo.ac_level * 1000 + indexStep)
      local dataTable = {}
      local statusContext = ""
      dataTable[#dataTable + 1] = GameLoader:GetGameText("LC_BATTLE_AREA52_1001_NAME")
      dataTable[#dataTable + 1] = battleInfo.level_req
      if indexStep == arcaneInfo.ac_step then
        dataTable[#dataTable + 1] = "active"
        statusContext = GameLoader:GetGameText("LC_MENU_TARGET_CHAR")
        local vesselsTable = {}
        for _, monsterID in ipairs(battleInfo.MONSTERS) do
          if monsterID > 0 and #vesselsTable < 5 then
            table.insert(vesselsTable, GameDataAccessHelper:GetMonsterShip(monsterID, false))
          end
        end
        dataTable[#dataTable + 1] = table.concat(vesselsTable, ",")
      else
        dataTable[#dataTable + 1] = "defeated"
        statusContext = GameLoader:GetGameText("LC_MENU_ENEMY_STATUS_PASSED")
        dataTable[#dataTable + 1] = -1
      end
      local dataString = table.concat(dataTable, "\001")
      assert(dataString)
      self:GetFlashObject():InvokeASCallback("_root", "setEnemyItem", indexStep, dataString, statusContext)
    else
      self:GetFlashObject():InvokeASCallback("_root", "setEnemyItem", indexStep, -1)
    end
  end
  self:RefreshResourceCount()
end
function GameObjectArcaneMap:UpdateSweepInfo()
  local arcaneInfo = GameStateArcane:GetArcaneInfo()
  self:GetFlashObject():InvokeASCallback("_root", "setSweepBtnStatus", arcaneInfo.can_sweep)
end
function GameObjectArcaneMap:UpdateProgressInfo()
  local arcaneInfo = GameStateArcane:GetArcaneInfo()
  self:GetFlashObject():InvokeASCallback("_root", "setProgressPercent", (arcaneInfo.ac_step - 1) / 5 * 100)
end
function GameObjectArcaneMap:UpdateAvailableCommanderCount()
  local arcaneInfo = GameStateArcane:GetArcaneInfo()
  self:GetFlashObject():InvokeASCallback("_root", "setAvailableCommanderCount", 6 - arcaneInfo.ac_step)
end
function GameObjectArcaneMap:UpdateJamedStatus()
  local arcaneInfo = GameStateArcane:GetArcaneInfo()
  local isJamed = arcaneInfo.jam == 1
  self:GetFlashObject():InvokeASCallback("_root", "setJamedStatus", isJamed)
end
function GameObjectArcaneMap:UpdateTechPointInfo()
  local resourceInfo = GameGlobalData:GetData("resource")
  self:GetFlashObject():InvokeASCallback("_root", "setTechPointInfo", GameUtils.numberConversion(resourceInfo.technique))
end
function GameObjectArcaneMap:UpdateArcaneSupplyInfo()
  local arcaneInfo = GameStateArcane:GetArcaneInfo()
  self:GetFlashObject():InvokeASCallback("_root", "setArcaneSupplyInfo", GameUtils.numberConversion(arcaneInfo.ac_supply))
end
function GameObjectArcaneMap.NetCallbackBattleResult(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.ac_battle_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  return false
end
function GameObjectArcaneMap.NetCallbackJam(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.ac_jam_req.Code then
    if content.code ~= 0 then
      local GameVip = LuaObjectManager:GetLuaObject("GameVip")
      GameVip:CheckIsNeedShowVip(content.code)
    end
    return true
  end
  return false
end
function GameObjectArcaneMap.OnArcaneInfoChange()
  GameObjectArcaneMap:UpdateArcaneData()
end
function GameObjectArcaneMap:Update(dt)
  local flashObject = self:GetFlashObject()
  if flashObject then
    self:GetFlashObject():InvokeASCallback("_root", "OnUpdate", dt)
    local arcaneInfo = GameStateArcane:GetArcaneInfo()
    if arcaneInfo and arcaneInfo.refresh_time > 0 then
      arcaneInfo.refresh_time = arcaneInfo.refresh_time - dt
      if arcaneInfo.refresh_time < 0 then
        arcaneInfo.refresh_time = 0
      end
      self:UpdateRefreshTime()
    end
    flashObject:Update(dt)
  end
end
function GameObjectArcaneMap:UpdateRefreshTime()
  local timeText = -1
  local arcaneInfo = GameStateArcane:GetArcaneInfo()
  if arcaneInfo.refresh_time > 0 then
    timeText = GameUtils:formatTimeString(arcaneInfo.refresh_time / 1000)
  end
  self:GetFlashObject():InvokeASCallback("_root", "setRefreshTimeText", timeText)
end
if AutoUpdate.isAndroidDevice then
  function GameObjectArcaneMap.OnAndroidBack()
    GameObjectArcaneMap:GetFlashObject():InvokeASCallback("_root", "moveOutAll")
  end
end
function GameObjectArcaneMap:RefreshResourceCount()
  local resource = GameGlobalData:GetData("item_count")
  local flash_obj = GameObjectArcaneMap:GetFlashObject()
  if flash_obj and resource then
    local item_2505_count = 0
    for i, v in ipairs(resource.items) do
      if v.item_id == 2505 then
        item_2505_count = v.item_no
        break
      end
    end
    local jamCost = 0
    if GameStateArcane:GetArcaneInfo() then
      jamCost = GameStateArcane:GetArcaneInfo().jam_cost
    end
    flash_obj:InvokeASCallback("_root", "setItemCount", item_2505_count, jamCost)
  end
end
