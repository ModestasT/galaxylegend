local GameStateLab = GameStateManager.GameStateLab
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local GameObjectFormationBackground = LuaObjectManager:GetLuaObject("GameObjectFormationBackground")
local GameStateBattlePlay = GameStateManager.GameStateBattlePlay
local GameUIWDStuff = LuaObjectManager:GetLuaObject("GameUIWDStuff")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameUILab = LuaObjectManager:GetLuaObject("GameUILab")
local GameUIBattleResult = LuaObjectManager:GetLuaObject("GameUIBattleResult")
local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
local GameUIKrypton = LuaObjectManager:GetLuaObject("GameUIKrypton")
require("FleetMatrix.tfl")
GameUICrusade = {}
GameUICrusade.flash_obj = nil
GameUICrusade.mData = {}
GameUICrusade.mCrusadeMap = nil
GameUICrusade.mCurSelectEnemyPos = -1
GameUICrusade.mFromLab = true
GameUICrusade.mActiveSpell = -1
GameUICrusade.mGetRewardList = nil
GameUICrusade.mLastFightEnemyPos = -1
GameUICrusade.mShareStoryChecked = {}
GameUICrusade.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_CRUSADE_AWARD] = true
require("CrusadeMap.tfl")
function GameUICrusade:Init(flash_obj)
  self.flash_obj = flash_obj
  GameUICrusade.mCrusadeMap = CrusadeMap.new(flash_obj)
end
function GameUICrusade:Destory()
  local shipPos = GameUICrusade.mCrusadeMap:GetShipPos()
  if shipPos and "" ~= shipPos then
    self.mShipPos = shipPos
  end
  self.flash_obj = nil
  GameUICrusade.mCrusadeMap = nil
end
function GameUICrusade:OnFSCommand(cmd, arg)
  DebugOut("GameUICrusade OnFSCommand : " .. cmd .. " arg " .. arg)
  if "ShowRepairPanel" == cmd then
    GameUICrusade:SetRepairPanelInfo()
    GameUICrusade:ShowRepairPanel()
  elseif "ShowReward" == cmd then
    GameUICrusade:SetReward()
    GameUICrusade:ShowReward()
  elseif "OpenBox" == cmd then
    local rewardPos = GameUICrusade:GetRewardPosCanGet()
    if rewardPos then
      GameUICrusade:RequestGetReward(rewardPos)
    end
  elseif "crusade_move_in" == cmd then
    if GameUICrusade.mFromLab or not self.mShipPos or "" == self.mShipPos then
      GameUICrusade.mCrusadeMap:ResetShip()
      GameUICrusade.mFromLab = false
      GameUICrusade:CheckReward()
    else
      GameUICrusade.mCrusadeMap:RestoreShip(self.mShipPos)
    end
  elseif "TriggerTarget" == cmd then
    GameUICrusade:TriggerTarget(tonumber(arg))
  elseif "ClickShip" == cmd then
    GameUICrusade:RequestRepair(tonumber(arg))
  elseif "btnBuyCrossClicked" == cmd then
    GameUICrusade:RequestCrossPrice()
  elseif "btnCrossClicked" == cmd then
    if GameUICrusade.mData.data.info.cross > 0 then
      GameUICrusade:RequestCross(GameUICrusade.mCurSelectEnemyPos)
    else
      GameUICrusade:RequestCrossPrice()
    end
  elseif "Replay" == cmd then
    local enemy = self:GetEnemyByPos(GameUICrusade.mCurSelectEnemyPos)
    GameUICrusade:RequestFightHistory(enemy.log_id)
  elseif "MapLoadCompleted" == cmd then
    GameUICrusade:SetMapEnemyInfo()
    GameUICrusade:SetCurGroupEnemy()
  elseif "crusade_help" == cmd then
    GameUICrusade:ShowHelp()
  elseif "crusade_closed" == cmd then
    self.mShipPos = GameUICrusade.mCrusadeMap:GetShipPos()
    GameStateLab:GoToSubState(STATE_LAB_SUB_STATE.SUBSTATE_NORMAL)
  elseif "comfirm_yes" == cmd then
    FleetMatrix:SetTypeMatrix(FleetMatrix.MATRIX_TYPE_BURNING)
    GameUICrusade:RequestEnterCrusade()
  elseif "OnClickRewardItem" == cmd then
    local rewards = self:GetNextBoxRewards()
    if rewards then
      local item = rewards[tonumber(arg)]
      GameUICrusade:ShowItemDetil(item)
    end
  elseif "OnClickBoxItem" == cmd then
    if GameUICrusade.mGetRewardList then
      local item = GameUICrusade.mGetRewardList[tonumber(arg)]
      GameUICrusade:ShowItemDetil(item)
    end
  elseif "shareCrusadeAwardCheckClicked" == cmd then
    if self.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_CRUSADE_AWARD] then
      self.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_CRUSADE_AWARD] = false
    else
      self.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_CRUSADE_AWARD] = true
    end
    if self.flash_obj then
      self.flash_obj:InvokeASCallback("_root", "setCheckBox", self.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_CRUSADE_AWARD], FacebookGraphStorySharer.StoryType.STORY_TYPE_CRUSADE_AWARD)
    end
  elseif "hideCongratulations" == cmd then
    local countryStr = ext.GetDeviceCountry and ext.GetDeviceCountry() or "unknow"
    if self.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_CRUSADE_AWARD] then
      local extraInfo = {}
      extraInfo.spaceNum = GameUICrusade.rewardPos or 3
      FacebookGraphStorySharer:ShareStory(FacebookGraphStorySharer.StoryType.STORY_TYPE_CRUSADE_AWARD, extraInfo)
    else
    end
  elseif "UpdateMarix" == cmd then
    local index = tonumber(arg)
    FleetMatrix:SwitchMatrixClient(index, self.UpdateMatrixFleetAvatar)
  elseif "comfirm_no" == cmd and GameUICrusade.isFromActivityEvent then
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateMainPlanet)
  end
end
function GameUICrusade:RequestEnterCrusade()
  DebugOut("GameUICrusade:RequestEnterCrusade")
  NetMessageMgr:SendMsg(NetAPIList.crusade_enter_req.Code, nil, self.RequestEnterCrusadeCallback, true, nil)
end
function GameUICrusade:InitData(content)
  GameUICrusade.mData.data = content
  local selfAdjutantList = GameUICrusade.mData.data.info.self_adjutant
  local selfFleetList = GameUICrusade.mData.data.info.self_fleet
  if selfFleetList and #selfFleetList > 0 then
    for k, v in pairs(selfFleetList) do
      v.cur_adjutant = 0
      v.hp = v.hp + v.shield
      v.max_hp = v.max_hp + v.max_shield
    end
    if selfAdjutantList and #selfAdjutantList > 0 then
      for k, v in pairs(selfAdjutantList) do
        for k2, v2 in pairs(selfFleetList) do
          if v.major == v2.fleet_id then
            v2.cur_adjutant = v.adjutant
            break
          end
        end
      end
    end
  end
end
function GameUICrusade.RequestEnterCrusadeCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.crusade_enter_req.Code then
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.crusade_enter_ack.Code then
    DebugOut("GameUICrusade.RequestEnterCrusadeCallback")
    DebugTable(content)
    GameStateLab:GoToSubState(STATE_LAB_SUB_STATE.SUBSTATE_CRUSADE)
    GameUICrusade:InitData(content)
    GameUICrusade:EnterCrusade()
    return true
  end
  return false
end
function GameUICrusade:EnterCrusade()
  GameUICrusade.mCurSelectEnemyPos = -1
  GameUICrusade:SetRepairPercent()
  if self.flash_obj then
    local mapResName = "CrusadeMap.tfs"
    local mapBgResName = "enemy_scene_bg_1_1.tfs"
    if ext.is16x9() then
      mapResName = "CrusadeMap_16x9.tfs"
      mapBgResName = "enemy_scene_bg_1_1_16x9.tfs"
    elseif ext.is4x3() then
      mapResName = "CrusadeMap_4x3.tfs"
      mapBgResName = "enemy_scene_bg_1_1_4x3.tfs"
    elseif ext.is2x1 and ext.is2x1() then
      mapResName = "CrusadeMap_16x9.tfs"
      mapBgResName = "enemy_scene_bg_1_1_16x9.tfs"
    end
    self.flash_obj:InvokeASCallback("_root", "SetMapResName", mapResName, mapBgResName)
    local lang = "en"
    if GameSettingData and GameSettingData.Save_Lang then
      local la = GameSettingData.Save_Lang
      if string.find(la, "ru") == 1 then
        lang = "ru"
      end
    end
    self.flash_obj:InvokeASCallback("_root", "EnterCrusade", lang)
  end
end
function GameUICrusade:SetRepairPercent()
  if self.flash_obj then
    local resource_info = GameGlobalData:GetData("resource")
    local maxRepair = GameUICrusade.mData.data.info.max_repair
    self.flash_obj:InvokeASCallback("_root", "SetRepairPercent", resource_info.crusade_repair, maxRepair)
  end
end
function GameUICrusade:SetRepairPanelInfo()
  if self.flash_obj then
    local ids = ""
    local headIcons = ""
    local colors = ""
    local shipIcons = ""
    local percents = ""
    local count = 0
    local equipShipList = {}
    local otherShipList = {}
    for k = 1, #GameUICrusade.mData.data.info.self_matrix.cells do
      local tmp = GameUICrusade:GetShipById(GameUICrusade.mData.data.info.self_matrix.cells[k].fleet_identity)
      table.insert(equipShipList, tmp)
    end
    for k = 1, #GameUICrusade.mData.data.info.self_fleet do
      local equiped = false
      for k2 = 1, #equipShipList do
        if equipShipList[k2].fleet_id == GameUICrusade.mData.data.info.self_fleet[k].fleet_id then
          equiped = true
          break
        end
      end
      if not equiped then
        table.insert(otherShipList, GameUICrusade.mData.data.info.self_fleet[k])
      end
    end
    local foreSort = function(a, b)
      if not a.force or not b.force then
        return false
      end
      if a.force > b.force then
        return true
      else
        return false
      end
    end
    table.sort(equipShipList, foreSort)
    table.sort(otherShipList, foreSort)
    local allShipList = equipShipList
    if #otherShipList >= 1 then
      for k = 1, #otherShipList do
        table.insert(allShipList, otherShipList[k])
      end
    end
    for k = 1, #allShipList do
      local ship = allShipList[k]
      ids = ids .. ship.fleet_id .. "\001"
      if ship.fleet_id == 1 then
        headIcons = headIcons .. GameDataAccessHelper:GetFleetAvatar(GameUICrusade.mData.data.info.real_id, ship.level) .. "\001"
        shipIcons = shipIcons .. GameDataAccessHelper:GetCommanderVesselsImage(GameUICrusade.mData.data.info.real_id, ship.level) .. "\001"
      else
        headIcons = headIcons .. GameDataAccessHelper:GetFleetAvatar(ship.fleet_id, ship.level) .. "\001"
        shipIcons = shipIcons .. GameDataAccessHelper:GetCommanderVesselsImage(ship.fleet_id, ship.level) .. "\001"
      end
      local percent = math.floor(ship.hp / ship.max_hp * 100)
      percents = percents .. percent .. "%" .. "\001"
      local color = "blue"
      if percent <= 50 and percent > 0 then
        color = "yellow"
      elseif percent == 0 then
        color = "red"
      end
      colors = colors .. color .. "\001"
      count = count + 1
    end
    self.flash_obj:InvokeASCallback("_root", "SetRepairPanelInfo", count, ids, colors, headIcons, shipIcons, percents)
  end
end
function GameUICrusade:ShowRepairPanel()
  if self.flash_obj then
    self.flash_obj:InvokeASCallback("_root", "ShowRepairPanel")
  end
end
function GameUICrusade.RefreshReward()
  GameUICrusade:SetReward()
end
function GameUICrusade:SetReward()
  if self.flash_obj then
    local count = 0
    local icons = ""
    local names = ""
    local nums = ""
    local rewards = self:GetNextBoxRewards()
    if rewards and #rewards > 0 then
      for k = 1, #rewards do
        local iconFrame = GameHelper:GetCommonIconFrame(rewards[k], GameUICrusade.RefreshReward)
        local itemName = GameHelper:GetAwardTypeText(rewards[k].item_type, rewards[k].number)
        local itemNum = ""
        if rewards[k].item_type == "krypton" or rewards[k].item_type == "item" then
          itemNum = rewards[k].no
        else
          itemNum = rewards[k].number
        end
        icons = icons .. iconFrame .. "\001"
        names = names .. itemName .. "\001"
        nums = nums .. itemNum .. "\001"
        count = count + 1
      end
    end
    self.flash_obj:InvokeASCallback("_root", "SetReward", count, icons, names, nums)
  end
end
function GameUICrusade:ShowReward()
  if self.flash_obj then
    self.flash_obj:InvokeASCallback("_root", "ShowReward")
  end
end
function GameUICrusade:ShowBox()
  if self.flash_obj then
    self.flash_obj:InvokeASCallback("_root", "ShowBox")
  end
end
function GameUICrusade:SetBoxRewardList(rewards)
  if self.flash_obj then
    local icons = ""
    local names = ""
    local nums = ""
    local count = 0
    if rewards and #rewards > 0 then
      for k = 1, #rewards do
        local iconFrame = GameHelper:GetCommonIconFrame(rewards[k], GameUICrusade.RefreshReward)
        local itemName = GameHelper:GetAwardTypeText(rewards[k].item_type, rewards[k].number)
        local itemNum = ""
        if rewards[k].item_type == "krypton" or rewards[k].item_type == "item" then
          itemNum = rewards[k].no
        else
          itemNum = rewards[k].number
        end
        icons = icons .. iconFrame .. "\001"
        names = names .. itemName .. "\001"
        nums = nums .. itemNum .. "\001"
        count = count + 1
      end
    end
    self.flash_obj:InvokeASCallback("_root", "SetBoxRewardList", count, icons, names, nums)
  end
end
function GameUICrusade:OpenBox(shareStoryEnabled, shareChecked)
  if self.flash_obj then
    local awardText = GameLoader:GetGameText("LC_MENU_TBC_SUCCESS_SHARE")
    awardText = string.format(awardText, GameUICrusade.rewardPos or 3)
    self.flash_obj:InvokeASCallback("_root", "OpenBox", shareStoryEnabled, shareChecked, awardText)
  end
end
function GameUICrusade:MapFrameUpdate(dt)
  if GameUICrusade.mCrusadeMap then
    GameUICrusade.mCrusadeMap:MapFrameUpdate(dt)
  end
end
function GameUICrusade:SetMapEnemyInfo()
  GameUICrusade.mCrusadeMap:ResetEnemy()
  local enemys = GameUICrusade.mData.data.info.pos_info
  local lastShowIdx = GameUICrusade:GetLastShowEnemyIdx()
  for k = 1, #enemys do
    local enemy = enemys[k]
    if k > lastShowIdx then
      break
    end
    local areaId = math.ceil(enemy.pos / 3)
    local enemyItemName = "target_" .. areaId .. "_" .. GameUICrusade.mData.data.info.place[enemy.pos]
    local isVisible = true
    local pos = enemy.pos
    local statusFrm = "normal"
    local statusStr = GameLoader:GetGameText("LC_MENU_TARGET_CHAR")
    if enemy.pass then
      statusFrm = "dead"
      statusStr = GameLoader:GetGameText("LC_MENU_ENEMY_STATUS_PASSED")
    end
    local shipCount = #enemy.matrix
    local shipFrms = ""
    for l = 1, #enemy.matrix do
      DebugOut("GameDataAccessHelper:GetCommanderVesselsImage(enemy.matrix[l].identity)", GameDataAccessHelper:GetCommanderVesselsImage(enemy.matrix[l].identity, enemy.matrix[l].level))
      shipFrms = shipFrms .. GameDataAccessHelper:GetCommanderVesselsImage(enemy.matrix[l].identity, enemy.matrix[l].level) .. "\001"
    end
    local forceTitle = GameLoader:GetGameText("LC_MENU_ARENA_CAPTION_FORCE")
    local forceStr = GameUtils.numberConversion(enemy.force)
    local enemyName = enemy.name
    local lvStr = tostring(enemy.level)
    DebugOut("Set enemy : " .. enemyItemName)
    GameUICrusade.mCrusadeMap:SetEnemy(enemyItemName, isVisible, pos, statusFrm, shipCount, shipFrms, forceTitle, forceStr, enemyName, lvStr, statusStr)
  end
  GameUICrusade:SetPath()
  GameUICrusade.mCrusadeMap:CalculateMaxMapRootOffset()
end
function GameUICrusade:GetLastPassedEnemyIdx()
  local enemys = GameUICrusade.mData.data.info.pos_info
  local lastPassedIdx = 0
  for k = 1, #enemys do
    local enemy = enemys[k]
    if not enemy.pass then
      break
    else
      lastPassedIdx = k
    end
  end
  DebugOut("GetLastPassedEnemyIdx : " .. lastPassedIdx)
  return lastPassedIdx
end
function GameUICrusade:GetLastShowEnemyIdx()
  local enemys = GameUICrusade.mData.data.info.pos_info
  local lastShowIdx = #enemys
  for k = 1, #enemys do
    local enemy = enemys[k]
    if not enemy.pass then
      lastShowIdx = k + (3 - k % 3) % 3
      DebugOut("GetLastShowEnemyIdx : " .. lastShowIdx)
      break
    end
  end
  return lastShowIdx
end
function GameUICrusade:GetNextBoxRewards()
  local lastShowIdx = GameUICrusade:GetLastShowEnemyIdx()
  local rewards = GameUICrusade.mData.data.info.rewards
  local curReward = rewards[1].rewards
  for k = 1, #rewards do
    local reward = rewards[k]
    if lastShowIdx == reward.pos then
      curReward = rewards[k].rewards
      break
    end
  end
  return curReward
end
function GameUICrusade:GetEnemyByPos(pos)
  local enemys = GameUICrusade.mData.data.info.pos_info
  for k = 1, #enemys do
    local enemy = enemys[k]
    if enemy.pos == pos then
      return enemys[k]
    end
  end
  return nil
end
function GameUICrusade:TriggerTarget(pos)
  GameUICrusade.mCurSelectEnemyPos = pos
  local enemy = GameUICrusade.mData.data.info.pos_info[pos]
  if enemy and not enemy.pass then
    DebugOut("TriggerTarget : " .. pos)
    DebugTable(enemy)
    GameUICrusade.mLastFightEnemyPos = GameUICrusade.mCurSelectEnemyPos
    local enemyInfo = {}
    local fleetLevel = 0
    for k, v in pairs(enemy.matrix) do
      if v.identity == 1 then
        fleetLevel = v.level
      end
    end
    enemyInfo.avatar = GameDataAccessHelper:GetFleetAvatar(1, fleetLevel, enemy.sex)
    enemyInfo.name = enemy.name
    enemyInfo.mSex = enemy.sex
    self.mEnemyInfo = enemyInfo
    self.mTriggerEnemyPos = pos
    GameObjectFormationBackground.force = enemy.force
    GameObjectFormationBackground:SetEnemiesMatrixData(enemy.matrix)
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateFormation)
  elseif not enemy then
    DebugOut("Cannot find enemy by pos : " .. pos)
  else
    DebugOut("This enemy is passed : " .. pos)
    local fleetLevel = 0
    for k, v in pairs(enemy.matrix) do
      if v.identity == 1 then
        fleetLevel = v.level
      end
    end
    if self.flash_obj then
      local headIconFr = GameDataAccessHelper:GetFleetAvatar(1, fleetLevel, enemy.sex)
      local crossNum = GameUICrusade.mData.data.info.cross
      local canCross = false
      if crossNum > 0 then
        canCross = true
      end
      local crossNumStr = GameLoader:GetGameText("LC_MENU_TBC_ROLLBACK_TIMES") .. tostring(crossNum)
      self.flash_obj:InvokeASCallback("_root", "SetEnemyInfo", headIconFr, enemy.name, enemy.level, enemy.server)
      self.flash_obj:InvokeASCallback("_root", "SetCrossCountDesc", crossNumStr)
      self.flash_obj:InvokeASCallback("_root", "SetCrossBtnVisible", canCross)
      self.flash_obj:InvokeASCallback("_root", "SetBuyCrossBtnVisible", not canCross)
      self.flash_obj:InvokeASCallback("_root", "ShowEnemyInfo")
    end
  end
end
function GameUICrusade:GetTriggerEnemyInfo()
  return self.mEnemyInfo
end
function GameUICrusade:GetTriggerEnemyMatrix()
  local enemy = GameUICrusade.mData.data.info.pos_info[GameUICrusade.mCurSelectEnemyPos]
  return enemy.matrix
end
function HaveDemageShip(matrixFormation)
  for k = 1, #matrixFormation do
    if 0 ~= matrixFormation[k] and -1 ~= matrixFormation[k] then
      local ship = GameUICrusade:GetShipById(matrixFormation[k])
      if ship and 0 == ship.hp then
        return true
      end
    end
  end
  return false
end
function GameUICrusade:RequestFight(curFormation)
  DebugOutPutTable(curFormation, "curFormation: ")
  local curMatrix = {}
  curMatrix.count = 0
  curMatrix.id = 1
  curMatrix.cells = {}
  curMatrix.name = ""
  for k = 1, #curFormation do
    if curFormation[k] ~= 0 then
      if curFormation[k] ~= -1 then
        local cell = {}
        cell.cell_id = k
        cell.fleet_identity = curFormation[k]
        local commander_data = GameUICrusade:GetSelfFleet(curFormation[k])
        if 1 == curFormation[k] then
          commander_data = GameGlobalData:GetFleetInfo(curFormation[k])
        end
        cell.fleet_spell = commander_data.active_spell
        table.insert(curMatrix.cells, cell)
      end
      curMatrix.count = curMatrix.count + 1
    end
  end
  if self.mTriggerEnemyPos then
    local fightMatrix = curMatrix
    DebugTable(fightMatrix)
    DebugOut("RequestFight  ")
    local req = {
      pos = self.mTriggerEnemyPos,
      matrix = fightMatrix
    }
    if GameStateManager:GetCurrentGameState().fightRoundData then
      GameStateManager:GetCurrentGameState().fightRoundData = nil
    end
    NetMessageMgr:SendMsg(NetAPIList.crusade_fight_req.Code, req, self.RequestFightCallback, true, nil)
  else
    DebugOut("self.mTriggerEnemyPos is nil.")
  end
end
function GameUICrusade.RequestFightCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.crusade_fight_req.Code then
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.crusade_fight_ack.Code then
    DebugOut("GameUICrusade.RequestFightCallback")
    DebugTable(content)
    do
      local battleReport = content.report
      GameUIBattleResult:SetFightReport(battleReport, nil, nil)
      if battleReport then
        local function playOverCallback()
          GameUIBattleResult:LoadFlashObject()
          local windowType = ""
          local awardDataTable = {}
          if battleReport.result == 1 then
            for indexAward, dataAward in ipairs(content.rewards) do
              local itemName, icon = GameHelper:GetAwardTypeTextAndIcon(dataAward.item_type, dataAward.number)
              if dataAward.item_type ~= "exp" then
                table.insert(awardDataTable, {
                  itemType = dataAward.item_type,
                  itemIcon = icon,
                  itemDesc = itemName,
                  itemNumber = dataAward.number
                })
              end
            end
            GameUIBattleResult:UpdateAwardList("challenge_win", awardDataTable)
            windowType = "challenge_win"
          else
            windowType = "challenge_lose"
          end
          GameUIBattleResult:SetFightReport(battleReport, nil, nil)
          GameUIBattleResult:UpdateExperiencePercent()
          GameUIBattleResult:UpdateAccomplishLevel(-1)
          local function enterCallback()
            GameUIBattleResult:AnimationMoveIn(windowType, false)
            GameStateLab:AddObject(GameUIBattleResult)
          end
          GameStateLab:SetEnterCallback(enterCallback)
          GameStateManager:SetCurrentGameState(GameStateLab)
        end
        if #battleReport.rounds == 0 and GameStateManager:GetCurrentGameState().fightRoundData then
          battleReport.rounds = GameStateManager:GetCurrentGameState().fightRoundData
        end
        GameStateBattlePlay.curBattleType = "chaosspace"
        GameStateBattlePlay:InitBattle(GameStateBattlePlay.MODE_PLAY, battleReport)
        GameStateBattlePlay:RegisterOverCallback(playOverCallback)
        GameStateManager:SetCurrentGameState(GameStateBattlePlay)
      end
      return true
    end
  end
  return false
end
function GameUICrusade:RequestFightHistory(reportId)
  local req = {battle_report_id = reportId}
  GameStateBattlePlay.report_id = reportId
  NetMessageMgr:SendMsg(NetAPIList.battle_fight_report_req.Code, req, self.RequestFightHistoryCallback, true, nil)
end
function GameUICrusade.RequestFightHistoryCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.battle_fight_report_req.Code then
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.battle_fight_report_ack.Code then
    DebugOut("GameUICrusade.RequestFightHistoryCallback")
    DebugTable(content)
    if content.code ~= 0 then
      return true
    end
    local fight_report = content.histories[1].report
    if #fight_report.rounds == 0 and GameStateManager:GetCurrentGameState().fightRoundData then
      fight_report.rounds = GameStateManager:GetCurrentGameState().fightRoundData
    end
    GameStateBattlePlay.curBattleType = "chaosspace"
    GameStateBattlePlay:InitBattle(GameStateBattlePlay.MODE_HISTORY, fight_report)
    GameStateBattlePlay:RegisterOverCallback(function()
      GameStateManager:SetCurrentGameState(GameStateLab)
    end)
    GameStateManager:SetCurrentGameState(GameStateBattlePlay)
    return true
  end
  return false
end
function GameUICrusade:RequestRepair(shipId)
  local ship = GameUICrusade:GetShipById(shipId)
  local percent = math.floor(ship.hp / ship.max_hp * 100)
  if percent == 100 then
    return
  end
  local req = {fleet_id = shipId}
  NetMessageMgr:SendMsg(NetAPIList.crusade_repair_req.Code, req, nil, true, nil)
  local resource_info = GameGlobalData:GetData("resource")
  local needPoint = 20
  if percent > 80 then
    needPoint = 100 - percent
  end
  if needPoint <= resource_info.crusade_repair then
    ship.hp = ship.hp + math.ceil(ship.max_hp * needPoint / 100)
    resource_info.crusade_repair = resource_info.crusade_repair - needPoint
  else
    ship.hp = ship.hp + math.ceil(ship.max_hp * resource_info.crusade_repair / 100)
    resource_info.crusade_repair = 0
  end
  if ship.hp > ship.max_hp then
    ship.hp = ship.max_hp
  end
  if self.flash_obj then
    local headIcon = GameDataAccessHelper:GetFleetAvatar(ship.fleet_id, ship.level)
    local shipIcon = GameDataAccessHelper:GetCommanderVesselsImage(ship.fleet_id, ship.level)
    if ship.fleet_id == 1 then
      headIcon = GameDataAccessHelper:GetFleetAvatar(GameUICrusade.mData.data.info.real_id, ship.level)
      shipIcon = GameDataAccessHelper:GetCommanderVesselsImage(GameUICrusade.mData.data.info.real_id, ship.level)
    end
    local newPercent = math.floor(ship.hp / ship.max_hp * 100)
    local percentStr = newPercent .. "%"
    local color = "blue"
    if newPercent <= 50 and newPercent > 0 then
      color = "yellow"
    elseif newPercent == 0 then
      color = "red"
    end
    self.flash_obj:InvokeASCallback("_root", "SetSpaceshipItemById", shipId, color, headIcon, shipIcon, percentStr)
  end
  GameUICrusade:SetRepairPercent()
end
function GameUICrusade:RequestCross(crossToPos)
  local req = {pos = crossToPos}
  NetMessageMgr:SendMsg(NetAPIList.crusade_cross_req.Code, req, self.RequestCrossCallback, true, nil)
end
function GameUICrusade.RequestCrossCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.crusade_cross_req.Code then
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.crusade_cross_ack.Code then
    DebugOut("GameUICrusade.RequestCrossCallback")
    DebugTable(content)
    GameUICrusade:InitData(content)
    if GameUICrusade.flash_obj then
      GameUICrusade.flash_obj:InvokeASCallback("_root", "HideEnemyInfo")
    end
    GameUICrusade:SetRepairPercent()
    GameUICrusade:SetMapEnemyInfo()
    GameUICrusade:SetCurGroupEnemy()
    return true
  end
  return false
end
function GameUICrusade:RequestBuyCross()
  NetMessageMgr:SendMsg(NetAPIList.crusade_buy_cross_req.Code, req, self.RequestBuyCrossCallback, true, nil)
end
function GameUICrusade.RequestBuyCrossCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.crusade_buy_cross_req.Code then
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.crusade_buy_cross_ack.Code then
    DebugOut("GameUICrusade.RequestBuyCrossCallback")
    DebugTable(content)
    GameUICrusade.mData.data.info.cross = content.num
    if GameUICrusade.flash_obj then
      local crossNumStr = GameLoader:GetGameText("LC_MENU_TBC_ROLLBACK_TIMES") .. tostring(content.num)
      GameUICrusade.flash_obj:InvokeASCallback("_root", "SetCrossCountDesc", crossNumStr)
      GameUICrusade.flash_obj:InvokeASCallback("_root", "SetCrossBtnVisible", true)
      GameUICrusade.flash_obj:InvokeASCallback("_root", "SetBuyCrossBtnVisible", false)
    end
    return true
  end
  return false
end
function GameUICrusade:RequestGetReward(rewardPos)
  DebugOut("RequestGetReward " .. rewardPos)
  GameUICrusade.rewardPos = rewardPos
  local req = {pos = rewardPos}
  NetMessageMgr:SendMsg(NetAPIList.crusade_reward_req.Code, req, self.RequestGetRewardCallback, true, nil)
end
function GameUICrusade.RequestGetRewardCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.crusade_reward_req.Code then
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    GameUICrusade.rewardPos = nil
    return true
  elseif msgType == NetAPIList.crusade_reward_ack.Code then
    DebugOut("GameUICrusade.RequestGetRewardCallback")
    DebugTable(content)
    GameUICrusade.mGetRewardList = content.reward
    GameUICrusade:SetBoxRewardList(content.reward)
    local shareStoryEnabled = false
    if FacebookGraphStorySharer:IsStoryEnabled(FacebookGraphStorySharer.StoryType.STORY_TYPE_CRUSADE_AWARD) then
      shareStoryEnabled = true
    end
    GameUICrusade:OpenBox(shareStoryEnabled, GameUICrusade.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_CRUSADE_AWARD])
    return true
  end
  return false
end
function GameUICrusade:GetShipById(shipId)
  for k = 1, #GameUICrusade.mData.data.info.self_fleet do
    local ship = GameUICrusade.mData.data.info.self_fleet[k]
    if ship.fleet_id == shipId then
      return ship
    end
  end
  return nil
end
function GameUICrusade:GetRewardPosCanGet()
  local rewards = GameUICrusade.mData.data.info.rewards
  local rewardGot = GameUICrusade.mData.data.info.reward_get
  local lastPassedEnemyPos = GameUICrusade:GetLastPassedEnemyIdx()
  for k = 1, #rewards do
    local rewardPos = rewards[k].pos
    if lastPassedEnemyPos >= rewardPos then
      local canGet = true
      for gotIdx = 1, #rewardGot do
        if rewardPos == rewardGot[gotIdx] then
          canGet = false
          break
        end
      end
      if canGet then
        return rewardPos
      end
    end
  end
  return nil
end
function GameUICrusade:CheckReward()
  if GameUICrusade:GetRewardPosCanGet() then
    GameUICrusade:ShowBox()
  end
end
function GameUICrusade:SetCurGroupEnemy()
  local lastShowEnemyPos = GameUICrusade:GetLastShowEnemyIdx()
  local icon1 = "2"
  local icon2 = "2"
  local icon3 = "2"
  local rewardLockVisible = false
  local enemy = GameUICrusade:GetEnemyByPos(lastShowEnemyPos - 2)
  if enemy and enemy.pass then
    icon1 = "1"
  else
    rewardLockVisible = true
  end
  local enemy = GameUICrusade:GetEnemyByPos(lastShowEnemyPos - 1)
  if enemy and enemy.pass then
    icon2 = "1"
  else
    rewardLockVisible = true
  end
  local enemy = GameUICrusade:GetEnemyByPos(lastShowEnemyPos)
  if enemy and enemy.pass then
    icon3 = "1"
  else
    rewardLockVisible = true
  end
  if self.flash_obj then
    self.flash_obj:InvokeASCallback("_root", "SetFloorShipIcon", icon1, icon2, icon3)
    self.flash_obj:InvokeASCallback("_root", "SetRewardLockIconVisible", rewardLockVisible)
  end
end
function GameUICrusade:GetSelfMatrix()
  DebugOutPutTable(GameGlobalData:GetData("matrix"), "global data matrix ")
  local selfMatrixCells = GameUICrusade.mData.data.info.self_matrix.cells
  local curMatrix = {}
  curMatrix.cells = {}
  curMatrix.count = 0
  for k = 1, #selfMatrixCells do
    table.insert(curMatrix.cells, selfMatrixCells[k])
    curMatrix.count = curMatrix.count + 1
  end
  local realCells = GameGlobalData:GetData("matrix").cells
  for k = 1, #realCells do
    local used = false
    for k2 = 1, #curMatrix.cells do
      if realCells[k].cell_id == curMatrix.cells[k2].cell_id then
        used = true
      end
    end
    if not used then
      local cell = {}
      cell.cell_id = realCells[k].cell_id
      cell.fleet_identity = -1
      cell.fleet_spell = -1
      table.insert(curMatrix.cells, cell)
      curMatrix.count = curMatrix.count + 1
    end
  end
  return curMatrix
end
function GameUICrusade:GetSelfFleetInfos()
  return GameUICrusade.mData.data.info.self_fleet
end
function GameUICrusade:GetRealFleetID()
  return GameUICrusade.mData.data.info.real_id
end
function GameUICrusade:GetSelfFleet(fleetId)
  local selfFleets = GameUICrusade.mData.data.info.self_fleet
  for k = 1, #selfFleets do
    if selfFleets[k].fleet_id == fleetId then
      local commander = {}
      commander.identity = selfFleets[k].fleet_id
      commander.level = selfFleets[k].level
      commander.cur_adjutant = selfFleets[k].cur_adjutant
      local baseInfo = GameDataAccessHelper:GetCommanderBasicInfo(fleetId, selfFleets[k].level)
      commander.active_spell = baseInfo.SPELL_ID
      commander.spells = {}
      table.insert(commander.spells, commander.active_spell)
      if 1 == fleetId then
        local mainShip = GameGlobalData:GetFleetInfo(fleetId)
        commander.active_spell = mainShip.active_spell
        commander.spells = mainShip.spells
      end
      return commander
    end
  end
  return nil
end
function GameUICrusade:SetActiveSpell(activeSpell)
  local mainShip = GameGlobalData:GetFleetInfo(1)
  if mainShip then
    mainShip.active_spell = activeSpell
  end
end
function GameUICrusade:IsDamage(fleetId)
  local fleetList = GameUICrusade.mData.data.info.self_fleet
  for k = 1, #fleetList do
    if fleetId == fleetList[k].fleet_id then
      if 0 == fleetList[k].hp then
        return true
      else
        return false
      end
    end
  end
  return false
end
function GameUICrusade:GetDamagePercent(fleetId)
  local fleetList = GameUICrusade.mData.data.info.self_fleet
  for k = 1, #fleetList do
    if fleetId == fleetList[k].fleet_id then
      local percent = math.floor(fleetList[k].hp / fleetList[k].max_hp * 100)
      local percentColor = "green"
      if percent <= 40 then
        percentColor = "red"
      elseif percent <= 99 then
        percentColor = "yellow"
      end
      return percent .. "%", percentColor
    end
  end
  return "100%", "green"
end
function GameUICrusade:SetPath()
  local posPath = GameUICrusade.mData.data.info.pos_path
  local path = ""
  local count = 0
  for k = 1, #posPath do
    local areaId = math.ceil(posPath[k] / 3)
    local enemyItemName = "target_" .. areaId .. "_" .. GameUICrusade.mData.data.info.place[posPath[k]]
    path = path .. enemyItemName .. "\001"
    count = count + 1
  end
  GameUICrusade.mCrusadeMap:SetAttackPath(count, path)
end
function GameUICrusade:RequestCheckFirstEnterToday()
  NetMessageMgr:SendMsg(NetAPIList.crusade_first_req.Code, nil, self.RequestCheckFirstEnterTodayCallback, true, nil)
end
function GameUICrusade.UpdateMatrixFleetAvatar()
end
function GameUICrusade.RequestMultiMatrixCallBack()
  if GameUICrusade.flash_obj then
    GameUICrusade.flash_obj:InvokeASCallback("_root", "initMartix", FleetMatrix.matrixs_in_as, FleetMatrix:getCurrentMartixIndex(FleetMatrix.MATRIX_TYPE_BURNING))
    GameUICrusade:UpdateMatrixFleetAvatar()
    local playerText = GameLoader:GetGameText("LC_MENU_ARENA_PLAYER_MEMBER_CHAR")
    local contentText = GameLoader:GetGameText("LC_MENU_SAVE_FORMATION_INFO_9")
    local cancel = GameLoader:GetGameText("LC_MENU_BUTTON_CAPTION_CANCEL")
    local confirm = GameLoader:GetGameText("LC_MENU_BUTTON_CAPTION_CONFIRM")
    GameUICrusade.flash_obj:InvokeASCallback("_root", "initDialog", playerText, contentText, cancel, confirm)
  end
end
function GameUICrusade:GetCurrentMatrixFleetAvatarAndRank(cur_index)
  local curMatrix = GameGlobalData:GetData("matrix").cells
  DebugOut("curMatrix = ")
  DebugTable(curMatrix)
  local curMatrixAvatar = {}
  if curMatrix and #curMatrix > 0 then
    for k, v in pairs(curMatrix) do
      if 0 < v.fleet_identity then
        local avatarT = {}
        DebugOut("fleet_identity = " .. v.fleet_identity)
        avatarT.fleetAvatar = GameDataAccessHelper:GetFleetAvatar(v.fleet_identity)
        avatarT.rankid = GameDataAccessHelper:GetCommanderColorFrame(v.fleet_identity)
        table.insert(curMatrixAvatar, avatarT)
      end
    end
  end
  DebugOut("curMatrixAvatar = ")
  DebugTable(curMatrixAvatar)
  return curMatrixAvatar
end
function GameUICrusade.RequestCheckFirstEnterTodayCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.crusade_first_req.Code then
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.crusade_first_ack.Code then
    DebugOut("GameUICrusade.RequestCheckFirstEnterTodayCallback")
    DebugTable(content)
    if content.enter_level > 0 then
      local tip = string.format(GameLoader:GetGameText("LC_ALERT_tbc_level_less"), content.enter_level)
      GameTip:Show(tip)
    elseif content.first then
      if GameUICrusade.flash_obj then
        FleetMatrix:GetMatrixsReq(GameUICrusade.RequestMultiMatrixCallBack, FleetMatrix.MATRIX_TYPE_BURNING)
      else
        DebugOut("GameUICrusade flash is nil. ")
      end
    else
      GameUICrusade:RequestEnterCrusade()
    end
    return true
  end
  return false
end
function GameUICrusade:RequestCrossPrice()
  local req = {
    price_type = "crusade_cross_cost",
    type = 0
  }
  NetMessageMgr:SendMsg(NetAPIList.price_req.Code, req, self.RequestCrossPriceCallback, true, nil)
end
function GameUICrusade.RequestCrossPriceCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.price_req.Code then
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.price_ack.Code then
    DebugOut("GameUICrusade.RequestCrossPriceCallback")
    DebugTable(content)
    local okCallback = function()
      GameUICrusade:RequestBuyCross()
    end
    local info = string.format(GameLoader:GetGameText("LC_MENU_COST_CREDIT_ALERT_CHAR"), content.price)
    GameUIGlobalScreen:ShowMessageBox(2, "", info, okCallback, nil)
    return true
  end
  return false
end
function GameUICrusade:ShowHelp()
  GameUIWDStuff:Show(GameUIWDStuff.menuType.menu_type_help)
  GameUIWDStuff:SetHelpText(GameLoader:GetGameText("LC_HELP_CONTENT_31"))
end
function GameUICrusade:ShowItemDetil(item)
  if item then
    local item_type = item.item_type
    if GameHelper:IsResource(item_type) then
      return
    end
    if item_type == "krypton" then
      if item.level == 0 then
        item.level = 1
      end
      local detail = GameUIKrypton:TryQueryKryptonDetail(item.number, item.level, function(msgType, content)
        local ret = GameUIKrypton.QueryKryptonDetailCallBack(msgType, content)
        if ret then
          local tmp = GameGlobalData:GetKryptonDetail(item.number, item.level)
          if tmp == nil then
            return false
          end
          ItemBox:SetKryptonBox(tmp, item.number)
          local operationTable = {}
          operationTable.btnUnloadVisible = false
          operationTable.btnUpgradeVisible = false
          operationTable.btnUseVisible = false
          operationTable.btnDecomposeVisible = false
          ItemBox:ShowKryptonBox(320, 240, operationTable)
        end
        return ret
      end)
      if detail == nil then
      else
        ItemBox:SetKryptonBox(detail, item.number)
        local operationTable = {}
        operationTable.btnUnloadVisible = false
        operationTable.btnUpgradeVisible = false
        operationTable.btnUseVisible = false
        operationTable.btnDecomposeVisible = false
        ItemBox:ShowKryptonBox(320, 240, operationTable)
      end
    end
    if item_type == "item" then
      item.cnt = 1
      ItemBox:showItemBox("Item", item, tonumber(item.number), 320, 480, 200, 200, "", nil, "", nil)
    end
    if item_type == "fleet" then
      ItemBox:ShowCommanderDetail2(tonumber(item.number))
    end
  else
    DebugOut("ShowItemDetil error.")
  end
end
return GameUICrusade
