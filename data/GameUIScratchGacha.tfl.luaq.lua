local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local GameStateScratchGacha = GameStateManager.GameStateScratchGacha
local GameUIScratchGacha = LuaObjectManager:GetLuaObject("GameUIScratchGacha")
local cardPrice, resetPrice, exchangeInfo, progressInfo, itemBalance, currencyTypes, cardAwards
local isShowBuyButton = true
local CurRoundOver = false
local needExitMenu = false
local needGetCardAward = false
local ui_img = "gacga_dynamic_all.png"
local bg_img = "gacha_bg_all.png"
local scratchArea_img = "gacga_ggl.png"
GameUIScratchGacha.tempNum = 0
function sortExchangeList(a, b)
  if a.award_id < b.award_id then
    return true
  else
    return false
  end
end
function GameUIScratchGacha:OnEraseFromGameState()
  self:UnloadFlashObject()
end
function GameUIScratchGacha:OnAddToGameState(game_state)
  self:LoadFlashObject()
  self:RequestMenuInfo()
  GameGlobalData:RegisterDataChangeCallback("item_count", self.RefreshResource)
  GameGlobalData:RegisterDataChangeCallback("resource", self.RefreshResource)
  GameUIScratchGacha.tempNum = 0
  if self:GetFlashObject() then
    GameUIScratchGacha:GetFlashObject():InvokeASCallback("_root", "setLocalText", "overText", GameLoader:GetGameText("LC_MENU_SPACE_STATION_BUY_END"))
    GameUIScratchGacha:GetFlashObject():InvokeASCallback("_root", "setLocalText", "buyCardText", GameLoader:GetGameText("LC_MENU_SPACE_STATION_BUY_ONE"))
  end
end
function GameUIScratchGacha:InitMenuInfo(isMoveIn)
  if cardPrice.off_count == 0 and cardPrice.origin_price == 0 and cardPrice.price == 0 then
    CurRoundOver = true
  end
  local flashObj = self:GetFlashObject()
  if flashObj then
    local exchangeListCnt = #exchangeInfo
    local progressMax = progressInfo.max
    local progressCur = progressInfo.now
    local treasureBoxDetail = self:GetProgressBarDetail()
    local currencyDetail = self:GetCurrencyDetail()
    local itemBalanceDetail = self:GetItemBalanceDetail()
    local resetInfo = self:GetResetInfo()
    if #currencyTypes == 1 then
      cardPrice.icon = "credit"
    else
      cardPrice.icon = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(currencyTypes[2])
    end
    if #cardAwards > 0 and isMoveIn then
      isShowBuyButton = false
      GameUIScratchGacha:EnableScratchArea()
    end
    DebugOut("Lua InitMenuInfo", isShowBuyButton)
    flashObj:InvokeASCallback("_root", "InitMenuInfo", cardPrice, resetInfo, exchangeListCnt, progressMax, progressCur, treasureBoxDetail, currencyDetail, itemBalanceDetail, isShowBuyButton, CurRoundOver, isMoveIn)
  else
    self:ExitMenu()
  end
end
function GameUIScratchGacha.CardPicCallBack(extInfo)
  local iconFrame = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(cardAwards[extInfo.idx], nil, nil)
  DebugOutPutTable(extInfo, "CardPicCallBack")
  DebugOut(iconFrame)
  if GameUIScratchGacha:GetFlashObject() then
    GameUIScratchGacha:GetFlashObject():InvokeASCallback("_root", "SetCardIconByIdx", iconFrame, extInfo.idx)
  end
end
function GameUIScratchGacha:EnableScratchArea()
  local cardAwardsDetailList = {}
  if #cardAwards > 0 then
    for i = 1, #cardAwards do
      local card = {}
      local extInfo = {}
      extInfo.idx = i
      card.iconFrame = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(cardAwards[i], extInfo, GameUIScratchGacha.CardPicCallBack)
      card.cnt = GameHelper:GetAwardCount(cardAwards[i].item_type, cardAwards[i].number, cardAwards[i].no)
      table.insert(cardAwardsDetailList, card)
    end
    local flashObj = self:GetFlashObject()
    if flashObj then
      flashObj:InvokeASCallback("_root", "SetRewarCardItem", cardAwardsDetailList)
    end
  end
end
function GameUIScratchGacha.ResetIconCallBack(extInfo)
  local resetInfo = GameUIScratchGacha:GetResetInfo()
  if GameUIScratchGacha:GetFlashObject() then
    GameUIScratchGacha:GetFlashObject():InvokeASCallback("_root", "SetResetPrice", resetInfo)
  end
end
function GameUIScratchGacha:GetResetInfo()
  local resetInfo = {}
  resetInfo.iconFrame = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(resetPrice, nil, GameUIScratchGacha.ResetIconCallBack)
  resetInfo.cnt = GameHelper:GetAwardCount(resetPrice.item_type, resetPrice.number, resetPrice.no)
  return resetInfo
end
function GameUIScratchGacha:GetItemBalanceCount(type)
  local cnt = 0
  for i = 1, #itemBalance do
    if itemBalance[i].item_type == type then
      cnt = GameHelper:GetAwardCount(itemBalance[i].item_type, itemBalance[i].number, itemBalance[i].no)
      return cnt
    end
  end
  return cnt
end
function GameUIScratchGacha.ItemBalanceCallBack(extInfo)
  local iconFrame = itemBalance[extInfo.idx].item_type
  DebugOut("ItemBalanceCallBack ", iconFrame, extInfo.idx)
  if extInfo.tt == 1 and (GameUIScratchGacha.tempNum ~= 1 or GameUIScratchGacha.tempNum ~= 3 or GameUIScratchGacha.tempNum ~= 7) then
    GameUIScratchGacha.tempNum = GameUIScratchGacha.tempNum + 1
  elseif extInfo.tt == 2 and (GameUIScratchGacha.tempNum ~= 2 or GameUIScratchGacha.tempNum ~= 3 or GameUIScratchGacha.tempNum ~= 6) then
    GameUIScratchGacha.tempNum = GameUIScratchGacha.tempNum + 2
  elseif extInfo.tt == 4 and (GameUIScratchGacha.tempNum ~= 4 or GameUIScratchGacha.tempNum ~= 5 or GameUIScratchGacha.tempNum ~= 6) then
    GameUIScratchGacha.tempNum = GameUIScratchGacha.tempNum + 4
  end
  if GameUIScratchGacha.tempNum == 7 then
    GameSettingData.ScratchPreActivity = GameStateScratchGacha.act_id * 10 + 7
  end
  if GameUIScratchGacha:GetFlashObject() then
    GameUIScratchGacha:GetFlashObject():InvokeASCallback("_root", "SetItemBalanceIconByIdx", iconFrame, extInfo.idx)
  end
end
function GameUIScratchGacha:GetItemBalanceDetail()
  local itemBalanceDetail = {}
  DebugOutPutTable(itemBalance, "GetItemBalanceDetail")
  for i = 1, #itemBalance do
    local item = {}
    local extInfo = {}
    extInfo.idx = i
    extInfo.tt = i
    if i == 3 then
      extInfo.tt = 4
    end
    item.icon = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(itemBalance[i], extInfo, GameUIScratchGacha.ItemBalanceCallBack)
    item.cnt = GameHelper:GetAwardCount(itemBalance[i].item_type, itemBalance[i].number, itemBalance[i].no)
    table.insert(itemBalanceDetail, item)
  end
  return itemBalanceDetail
end
function GameUIScratchGacha.GetCurrencyPicCallBack(extInfo)
  local iconFrame = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(currencyTypes[extInfo.idx], nil, nil)
  if GameUIScratchGacha:GetFlashObject() then
    GameUIScratchGacha:GetFlashObject():InvokeASCallback("_root", "SetCurrencyPicCallBack", iconFrame, extInfo.idx)
  end
end
function GameUIScratchGacha:GetCurrencyDetail()
  local currencyDetail = {}
  for i = 1, #currencyTypes do
    local currency = {}
    local extInfo = {}
    extInfo.idx = i
    local info = GameHelper:GetItemInfo(currencyTypes[i])
    currency.icon = info.icon_frame
    currency.pic = info.icon_pic
    currency.currencyCnt = 0
    if GameHelper:IsResource(currencyTypes[i].item_type) then
      local resource = GameGlobalData:GetData("resource")
      currency.currencyCnt = GameUtils.numberConversion(resource[currencyTypes[i].item_type])
    else
      currency.currencyCnt = GameUtils.numberConversion(GameGlobalData:GetItemCount(currencyTypes[i].number))
    end
    table.insert(currencyDetail, currency)
  end
  return currencyDetail
end
function GameUIScratchGacha:GetProgressBarDetail()
  local treasureBoxDetail = {}
  local sortProgressAward = function(a, b)
    if a.times < b.times then
      return true
    else
      return false
    end
  end
  table.sort(progressInfo.times_award, sortProgressAward)
  for i = 1, #progressInfo.times_award do
    local box = {}
    box.number = progressInfo.times_award[i].times
    box.status = progressInfo.times_award[i].award_status
    box.text = string.gsub(GameLoader:GetGameText("LC_MENU_SPACE_STATION_PROGESS"), "<number1>", box.number)
    table.insert(treasureBoxDetail, box)
  end
  return treasureBoxDetail
end
function GameUIScratchGacha.ExchangeListRewardPicCallBack(extInfo)
  local item = exchangeInfo[extInfo.itemId]
  if item then
    local iconFrame = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(item.award, nil, nil)
    if GameUIScratchGacha:GetFlashObject() then
      GameUIScratchGacha:GetFlashObject():InvokeASCallback("_root", "SetExchangeListRewardIcon", iconFrame, extInfo.itemId)
    end
  end
end
function GameUIScratchGacha.ExchangeListConditionPicCallBack(extInfo)
  local item = exchangeInfo[extInfo.itemId]
  if item then
    local iconFrame = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(item.condition.items[extInfo.idx], nil, nil)
    if GameUIScratchGacha:GetFlashObject() then
      GameUIScratchGacha:GetFlashObject():InvokeASCallback("_root", "SetExchangeListConditionIcon", iconFrame, extInfo.itemId, extInfo.idx)
    end
  end
end
function GameUIScratchGacha:UpdateExchangeList(itemId)
  local item = exchangeInfo[itemId]
  if item then
    local stat = item.award_status
    local can_buy_times = item.can_buy_times
    local extInfo = {}
    extInfo.itemId = itemId
    local info = GameHelper:GetItemInfo(item.award)
    local destItem = info.icon_frame
    local destPic = info.icon_pic
    local destItemCnt = GameHelper:GetAwardCount(item.award.item_type, item.award.number, item.award.no)
    local sourceItemList = {}
    for i = 1, #item.condition.items do
      local condition = {}
      local extInfo2 = {}
      extInfo2.itemId = itemId
      extInfo2.idx = i
      condition.iconFrame = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(item.condition.items[i], extInfo2, GameUIScratchGacha.ExchangeListConditionPicCallBack)
      condition.num = GameHelper:GetAwardCount(item.condition.items[i].item_type, item.condition.items[i].number, item.condition.items[i].no)
      table.insert(sourceItemList, condition)
    end
    local flashObj = self:GetFlashObject()
    if flashObj then
      DebugOut("UpdateExchangeList", itemId, #sourceItemList)
      DebugTable(sourceItemList)
      flashObj:InvokeASCallback("_root", "UpdateExchangeList", itemId, sourceItemList, destItem, destPic, destItemCnt, stat, item.award_id, can_buy_times)
    end
  end
end
local exchangeBoxDetail = {}
function GameUIScratchGacha:ShowExchangeBox(award_id)
  local detail = {}
  for k, v in ipairs(exchangeInfo) do
    if v.award_id == award_id then
      detail = v
      break
    end
  end
  GameUIScratchGacha.choosable_items = nil
  DebugOut("ShowExchangeBox", award_id)
  DebugTable(detail)
  if detail.award.item_type == "medal" or detail.award.item_type == "medal_item" then
    local function cb()
      local flashObj = self:GetFlashObject()
      if flashObj then
        flashObj:InvokeASCallback("_root", "ResetExchangeListSelect")
      end
      print("xxpp here cb")
    end
    ItemBox:ShowGameItem(detail.award, cb)
    return
  end
  if detail.can_buy_times ~= 0 then
    local reqLevel = " "
    local reqText = GameHelper:GetAwardText(detail.award.item_type, detail.award.number, detail.award.no)
    local desText = " "
    if not GameHelper:IsResource(detail.award.item_type) then
      desText = GameLoader:GetGameText("LC_ITEM_ITEM_DESC_" .. detail.award.number)
    end
    local available_times = detail.can_buy_times
    local available_times_desc = GameLoader:GetGameText("LC_MENU_SPACE_STATION_EXCHANGE_REMAIN") .. available_times
    if available_times == 0 then
      available_times_desc = GameLoader:GetGameText("LC_MENU_SPACE_STATION_EXCHANGE_LIMIT")
    elseif available_times == -1 then
      available_times_desc = " "
    end
    local canMultipleExchange = detail.can_times_buy
    local stat = detail.award_status
    local sourceItemList = {}
    for i = 1, #detail.condition.items do
      local condition = {}
      condition.iconFrame = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(detail.condition.items[i], nil, nil)
      condition.num = GameHelper:GetAwardCount(detail.condition.items[i].item_type, detail.condition.items[i].number, detail.condition.items[i].no)
      condition.total_num = self:GetItemBalanceCount(detail.condition.items[i].item_type)
      table.insert(sourceItemList, condition)
    end
    DebugTable(sourceItemList)
    local flashObj = self:GetFlashObject()
    DebugOut("ShowExchangeBox ", detail.award.number, GameHelper:IsResource(detail.award.item_type), GameDataAccessHelper:isChoosableItem(tonumber(detail.award.number)))
    if flashObj then
      if GameHelper:IsResource(detail.award.item_type) then
        flashObj:InvokeASCallback("_root", "ShowExchangeBox", award_id, sourceItemList, reqText, desText, available_times, canMultipleExchange, stat, available_times_desc, nil)
      else
        exchangeBoxDetail.itemType = tonumber(detail.award.number)
        exchangeBoxDetail.award_id = award_id
        exchangeBoxDetail.sourceItemList = sourceItemList
        exchangeBoxDetail.reqText = reqText
        exchangeBoxDetail.desText = desText
        exchangeBoxDetail.available_times = available_times
        exchangeBoxDetail.canMultipleExchange = canMultipleExchange
        exchangeBoxDetail.stat = stat
        exchangeBoxDetail.available_times_desc = available_times_desc
        local itemIDList = {}
        table.insert(itemIDList, tonumber(detail.award.number))
        local requestParam = {ids = itemIDList}
        NetMessageMgr:SendMsg(NetAPIList.items_req.Code, requestParam, GameUIScratchGacha.QueryItemDetailCallBack, true, nil)
      end
    end
  end
end
function GameUIScratchGacha.QueryItemDetailCallBack(msgType, content)
  if msgType == NetAPIList.common_ack and content.api == NetAPIList.items_req.Code then
    GameUIScratchGacha:GetFlashObject():InvokeASCallback("_root", "ShowExchangeBox", exchangeBoxDetail.award_id, exchangeBoxDetail.sourceItemList, exchangeBoxDetail.reqText, exchangeBoxDetail.desText, exchangeBoxDetail.available_times, exchangeBoxDetail.canMultipleExchange, exchangeBoxDetail.stat, exchangeBoxDetail.available_times_desc, nil)
  elseif msgType == NetAPIList.items_ack.Code then
    if #content.items >= 1 and content.items[1].sp_type == 102 then
      local param = {
        id = exchangeBoxDetail.itemType
      }
      local function callback(msgType, content)
        if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.choosable_item_req.Code then
          GameUIScratchGacha:GetFlashObject():InvokeASCallback("_root", "ShowExchangeBox", exchangeBoxDetail.award_id, exchangeBoxDetail.sourceItemList, exchangeBoxDetail.reqText, exchangeBoxDetail.desText, exchangeBoxDetail.available_times, exchangeBoxDetail.canMultipleExchange, exchangeBoxDetail.stat, exchangeBoxDetail.available_times_desc, nil)
          return true
        elseif msgType == NetAPIList.choosable_item_ack.Code then
          GameUIScratchGacha.choosable_items = content.choosable_items
          DebugOut("GameUIScratchGacha.ChoosableItemChoicesCallback")
          for k, v in ipairs(GameUIScratchGacha.choosable_items) do
            local info = GameHelper:GetItemInfo(v)
            v.cnt = info.cnt
            v.iconframe = info.icon_frame
            v.iconpic = info.icon_pic
          end
          DebugTable(GameUIScratchGacha.choosable_items)
          GameUIScratchGacha:GetFlashObject():InvokeASCallback("_root", "ShowExchangeBox", exchangeBoxDetail.award_id, exchangeBoxDetail.sourceItemList, exchangeBoxDetail.reqText, exchangeBoxDetail.desText, exchangeBoxDetail.available_times, exchangeBoxDetail.canMultipleExchange, exchangeBoxDetail.stat, exchangeBoxDetail.available_times_desc, GameUIScratchGacha.choosable_items)
          return true
        end
        return false
      end
      NetMessageMgr:SendMsg(NetAPIList.choosable_item_req.Code, param, callback, true, nil)
    else
      GameUIScratchGacha:GetFlashObject():InvokeASCallback("_root", "ShowExchangeBox", exchangeBoxDetail.award_id, exchangeBoxDetail.sourceItemList, exchangeBoxDetail.reqText, exchangeBoxDetail.desText, exchangeBoxDetail.available_times, exchangeBoxDetail.canMultipleExchange, exchangeBoxDetail.stat, exchangeBoxDetail.available_times_desc, nil)
    end
    return true
  end
  return false
end
function GameUIScratchGacha:ExitMenu()
  GameStateManager:SetCurrentGameState(GameStateScratchGacha:GetPreState())
end
function GameUIScratchGacha._OnTimerTick()
  local flashObj = GameUIScratchGacha:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "ShowBuyCardButton")
    return nil
  end
  return nil
end
function GameUIScratchGacha.OnBoardNtf(content)
  DebugOutPutTable(content, "GameUIScratchGacha.OnBoardNtf")
  GameUIScratchGacha.records_info = {}
  GameUIScratchGacha.records_info.data = content.boards
  GameUIScratchGacha:SetRollText()
end
function GameUIScratchGacha:GetRollTextString()
  local max = math.min(#self.records_info.data, 6)
  local rollText = ""
  for i = 1, max do
    local record_data = self.records_info.data[i]
    local msg_content = GameLoader:GetGameText("LC_TECH_NEWSTIP_6")
    msg_content = string.gsub(msg_content, "<playerName_char>", record_data.user)
    msg_content = string.gsub(msg_content, "<itemName_char>", GameHelper:GetAwardNameText(record_data.award.item_type, record_data.award.number))
    rollText = rollText .. msg_content .. "   "
    if i < max then
      rollText = rollText .. "   "
    end
  end
  return rollText
end
function GameUIScratchGacha:SetRollText()
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    flash_obj:InvokeASCallback("_root", "setRollTextInfo", self:GetRollTextString())
  end
end
function GameUIScratchGacha:OnFSCommand(cmd, arg)
  GameTextEdit:fscommand(self:GetFlashObject(), cmd, arg)
  if cmd == "close_menu" then
    self:ExitMenu()
    return
  elseif cmd == "onBingoEffectOver" then
    if needGetCardAward then
      needGetCardAward = false
      GameUIScratchGacha.resetCallback()
    else
      GameUIScratchGacha:InitMenuInfo(false)
      isShowBuyButton = true
      GameTimer:Add(self._OnTimerTick, 1000)
    end
  elseif cmd == "getBoxReward" then
    self:GetBoxREward(tonumber(arg))
  elseif cmd == "UpdateListItem" then
    self:UpdateExchangeList(tonumber(arg))
  elseif cmd == "TreasureBoxClicked" then
    self:ShowTresureBoxReward(tonumber(arg))
  elseif cmd == "GetAllCardReward" then
    self:GetAllCardReward()
  elseif cmd == "ShowExchangeBox" then
    self:ShowExchangeBox(tonumber(arg))
  elseif cmd == "ExchangeItem" then
    local param = LuaUtils:string_split(arg, "\001")
    self:ExchangeReward(tonumber(param[1]), tonumber(param[2]))
  elseif cmd == "buyCards" then
    self:BuyCards()
  elseif cmd == "buyReset" then
    self:BuyReset()
  elseif cmd == "ShowHelpInfo" then
    local text_title = ""
    local info = GameLoader:GetGameText("LC_HELP_SPACE_STATION_CONTENT")
    GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.HELP)
    GameUIMessageDialog:Display(text_title, info)
  elseif cmd == "updateBoxAwardItem" then
    self:UpdateBoxAwardItem(tonumber(arg))
  elseif cmd == "ShowBoxAwardItemDetail" then
    self:ShowBoxAwardItemDetail(tonumber(arg))
  elseif cmd == "ExitMenuWithAward" then
    needExitMenu = true
    self:GetAllCardReward()
  elseif cmd == "ResetWithAward" then
    needGetCardAward = true
    self:BuyReset()
  elseif cmd == "ShowChooseItemDetail" then
    local choosed_itemindex = tonumber(arg)
    local item = self.choosable_items[choosed_itemindex]
    local iteminfo = self:getItemInfo(item, item.itemType)
    self:GetFlashObject():InvokeASCallback("_root", "showChoiceDetail", iteminfo)
  end
end
function GameUIScratchGacha:getItemInfo(item, itemType)
  local item_info = ""
  local nameTxt = GameHelper:GetAwardTypeText(item.item_type, item.number)
  if item.item_type == "money" then
    local desctxt = GameLoader:GetGameText("LC_MENU_MONEY_CHAR_DESC")
    item_info = item_info .. nameTxt .. " \n" .. desctxt
  elseif item.item_type == "vip_exp" then
  elseif item.item_type == "wd_supply" then
  elseif item.item_type == "technique" then
    local desctxt = GameLoader:GetGameText("LC_MENU_TECHNIQUE_CHAR_DESC")
    item_info = item_info .. nameTxt .. " \n" .. desctxt
  elseif item.item_type == "crystal" then
    local desctxt = GameLoader:GetGameText("LC_MENU_LOOT_CRYSTAL_DESC")
    item_info = item_info .. nameTxt .. " \n" .. desctxt
  elseif item.item_type == "credit" then
  elseif item.item_type == "art_point" then
    local desctxt = GameLoader:GetGameText("LC_MENU_ARTIFACT_RESOURCE_DESC")
    item_info = item_info .. nameTxt .. " \n" .. desctxt
  elseif item.item_type == "kenergy" then
    local desctxt = GameLoader:GetGameText("LC_MENU_krypton_energy_DESC")
    item_info = item_info .. nameTxt .. " \n" .. desctxt
  elseif item.item_type == "ac_supply" then
  elseif item.item_type == "silver" then
  elseif item.item_type == "item" then
    local descTxt = GameLoader:GetGameText("LC_ITEM_ITEM_DESC_" .. item.number)
    item_info = item_info .. nameTxt .. "\n" .. descTxt
  elseif item.item_type == "krypton" then
    item_info = item_info .. nameTxt .. "\n"
    for k, v in ipairs(item.krypton_value) do
      item_info = item_info .. "\n" .. self:getBuffName(v.buff_id) .. " "
      item_info = item_info .. self:GetBuffNum(v.buff_id, v.buff_num)
    end
  elseif item.item_type == "fleet" then
    local ability_table = GameDataAccessHelper:GetCommanderAbility(item.number, 0)
    local commander_name = GameDataAccessHelper:GetCommanderName(item.number, 0)
    local nameTxt = GameLoader:GetGameText("LC_NPC_" .. commander_name)
    local skillDesc = GameDataAccessHelper:GetSkillDesc(ability_table.SPELL_ID)
    item_info = item_info .. nameTxt .. "\n" .. skillDesc
  elseif item.item_type == "medal" or item.item_type == "medal_item" then
    item_info = GameHelper:GetAwardTypeText(item.item_type, item.number)
    item_info = item_info .. "\n" .. GameHelper:GetAwardTypeDesc(item.item_type, item.number)
  else
    item_info = "item info"
  end
  return item_info
end
function GameUIScratchGacha:RequestMenuInfo()
  local param = {
    act_id = GameStateScratchGacha.act_id
  }
  NetMessageMgr:SendMsg(NetAPIList.enter_scratch_req.Code, param, GameUIScratchGacha.MenuInfoCallBack, true, nil)
end
function GameUIScratchGacha.MenuInfoCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.enter_scratch_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      GameUIScratchGacha:ExitMenu()
    end
    return true
  elseif msgType == NetAPIList.enter_scratch_ack.Code then
    DebugOut("MenuInfoCallBack")
    DebugTable(content)
    cardPrice = content.price
    resetPrice = content.reset_price
    exchangeInfo = content.awards
    progressInfo = content.progress_award
    itemBalance = content.items.item.items
    currencyTypes = content.costs.cost.items
    cardAwards = content.origin_award
    if #content.costs.cost.items == 0 or content.costs.cost.items[1].item_type ~= "credit" then
      local item = {
        item_type = "credit",
        number = 0,
        no = 0,
        level = 0
      }
      table.insert(content.costs.cost.items, 1, item)
    end
    table.sort(exchangeInfo, sortExchangeList)
    GameUIScratchGacha:CheckImageReplace(content.background.scratch_image, scratchArea_img)
    GameUIScratchGacha:CheckImageReplace(content.background.ui_image, ui_img)
    GameUIScratchGacha:CheckImageReplace(content.background.bg_image, bg_img)
    GameUIScratchGacha:InitMenuInfo(true)
    return true
  end
  return false
end
function GameUIScratchGacha:CheckImageReplace(img_name, src_image)
  if "undefined" ~= img_name then
    local image = img_name .. ".png"
    local localPath = "data2/" .. image
    DebugOut("extInfo.localPath = " .. localPath)
    if DynamicResDownloader:IfResExsit(localPath) then
      GameUIScratchGacha:GetFlashObject():ReplaceTexture(src_image, image)
    else
      local extendInfo = {}
      extendInfo.srcRes = src_image
      extendInfo.curRes = image
      DynamicResDownloader:AddDynamicRes(image, DynamicResDownloader.resType.WELCOME_PIC, extendInfo, GameUIScratchGacha.DynamicBackgroundCallback)
    end
  end
end
function GameUIScratchGacha.DynamicBackgroundCallback(extendInfo)
  GameUIScratchGacha:GetFlashObject():ReplaceTexture(extendInfo.srcRes, extendInfo.curRes)
end
GameUIScratchGacha.SelectedBoxDetail = nil
function GameUIScratchGacha:ShowTresureBoxReward(step)
  local tresureInfo = {}
  for i = 1, #progressInfo.times_award do
    if step == progressInfo.times_award[i].times then
      tresureInfo = progressInfo.times_award[i]
    end
  end
  GameUIScratchGacha.SelectedBoxDetail = tresureInfo.award.items
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "InitBoxAwardsList", #tresureInfo.award.items)
  end
end
function GameUIScratchGacha:UpdateBoxAwardItem(itemIdx)
  local awardsList = GameUIScratchGacha.SelectedBoxDetail[itemIdx]
  local tmp_type = awardsList.item_type
  local tmp_number = awardsList.number
  local no = awardsList.no
  no = no or 1
  local isItem = awardsList.item_type == "item"
  tmp_type, tmp_number = self:getAwardItemDetailInfo(tmp_type, tmp_number, no)
  self:GetFlashObject():InvokeASCallback("_root", "SetBoxAwardsListItem", itemIdx, tmp_type, tmp_number, isItem, no)
end
function GameUIScratchGacha:ShowBoxAwardItemDetail(index)
  local award = GameUIScratchGacha.SelectedBoxDetail[index]
  if award then
    self:showItemDetil(award, award.item_type)
  end
end
function GameUIScratchGacha:showItemDetil(item_, item_type)
  local item = item_
  if item_type == "krypton" then
    if item.level == 0 then
      item.level = 1
    end
    local detail = GameUIKrypton:TryQueryKryptonDetail(item.number, item.level, function(msgType, content)
      local ret = GameUIKrypton.QueryKryptonDetailCallBack(msgType, content)
      if ret then
        local tmp = GameGlobalData:GetKryptonDetail(item.number, item.level)
        if tmp == nil then
          return false
        end
        ItemBox:SetKryptonBox(tmp, item.number)
        local operationTable = {}
        operationTable.btnUnloadVisible = false
        operationTable.btnUpgradeVisible = false
        operationTable.btnUseVisible = false
        operationTable.btnDecomposeVisible = false
        ItemBox:ShowKryptonBox(320, 1080, operationTable)
      end
      return ret
    end)
    if detail == nil then
    else
      ItemBox:SetKryptonBox(detail, item.number)
      local operationTable = {}
      operationTable.btnUnloadVisible = false
      operationTable.btnUpgradeVisible = false
      operationTable.btnUseVisible = false
      operationTable.btnDecomposeVisible = false
      ItemBox:ShowKryptonBox(320, 1080, operationTable)
    end
  end
  if item_type == "fleet" then
    GameUIFirstCharge:ShowCommanderInfo(tonumber(item.number))
  end
  if item_type == "item" then
    item.cnt = 1
    ItemBox:showItemBox("ChoosableItem", item, tonumber(item.number), 320, 1080, 200, 200, "", nil, "", nil)
  end
end
function GameUIScratchGacha:getAwardItemDetailInfo(itemType, number, no)
  DebugActivity("getSignItemDetailInfo", itemType, number, no)
  local itemType = itemType
  local rewards = tonumber(number)
  no = no or 1
  if itemType == "fleet" then
    itemType = GameDataAccessHelper:GetFleetAvatar(rewards)
    rewards = GameLoader:GetGameText("LC_NPC_" .. GameDataAccessHelper:GetCommanderName(rewards))
  elseif itemType == "item" or itemType == "krypton" then
    local isitem = itemType == "item"
    if DynamicResDownloader:IsDynamicStuff(rewards, DynamicResDownloader.resType.PIC) then
      local fullFileName = DynamicResDownloader:GetFullName(rewards .. ".png", DynamicResDownloader.resType.PIC)
      local localPath = "data2/" .. fullFileName
      if DynamicResDownloader:IfResExsit(localPath) then
        ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
        itemType = "item_" .. rewards
      else
        itemType = "temp"
        self:AddDownloadPath(rewards, 0, 0)
      end
    else
      itemType = "item_" .. rewards
    end
    if isitem then
      rewards = GameLoader:GetGameText("LC_MENU_ITEM_CHAR")
    else
      rewards = GameLoader:GetGameText("LC_MENU_KRYPTON_TAB")
    end
  elseif itemType == "pve_supply" then
    rewards = GameLoader:GetGameText("LC_MENU_SUPPLY_CHAR")
  else
    rewards = GameUtils.numberConversion(rewards)
  end
  return itemType, rewards
end
function GameUIScratchGacha:AddDownloadPath(itemID, itemKey, index)
  local resName = itemID .. ".png"
  DynamicResDownloader:AddDynamicRes(resName, DynamicResDownloader.resType.PIC, extInfo, nil)
end
function GameUIScratchGacha.resetCallback()
  local param = {
    act_id = GameStateScratchGacha.act_id
  }
  if needGetCardAward then
    local balance = 0
    local price = 0
    if GameHelper:IsResource(resetPrice.item_type) then
      local resource = GameGlobalData:GetData("resource")
      balance = resource[resetPrice.item_type]
      price = tonumber(resetPrice.number)
    else
      balance = GameGlobalData:GetItemCount(resetPrice.number)
      price = tonumber(resetPrice.no)
    end
    if balance >= price then
      GameUIScratchGacha:GetFlashObject():InvokeASCallback("_root", "HideMosaic")
      GameUIScratchGacha:GetAllCardReward()
    else
      NetMessageMgr:SendMsg(NetAPIList.reset_scratch_card_req.Code, param, GameUIScratchGacha.BuyResetCallBack, true, nil)
    end
  else
    NetMessageMgr:SendMsg(NetAPIList.reset_scratch_card_req.Code, param, GameUIScratchGacha.BuyResetCallBack, true, nil)
  end
end
function GameUIScratchGacha:BuyReset()
  if GameSettingData.EnablePayRemind == nil then
    GameSettingData.EnablePayRemind = true
    GameUtils:SaveSettingData()
  end
  if not GameSettingData.EnablePayRemind then
    local text_title = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
    local info = GameLoader:GetGameText("LC_MENU_SPACE_STATION_ITEM_REFRESH_NEW")
    GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
    GameUIMessageDialog:SetYesButton(GameUIScratchGacha.resetCallback)
    GameUIMessageDialog:Display(text_title, info)
  elseif resetPrice.item_type == "credit" then
    local textInfo = string.gsub(GameLoader:GetGameText("LC_MENU_SPACE_STATION_RESET_ALERT"), "<number1>", tostring(resetPrice.number))
    GameUtils:CreditCostConfirm(textInfo, GameUIScratchGacha.resetCallback, false, nil)
  else
    local textInfo = string.gsub(GameLoader:GetGameText("LC_MENU_SPACE_STATION_ITEM_REFRESH"), "<name>", GameHelper:GetItemText(resetPrice.item_type, resetPrice.number, resetPrice.no))
    GameUtils:CreditCostConfirm(textInfo, GameUIScratchGacha.resetCallback, false, nil)
  end
end
function GameUIScratchGacha.RefreshResource()
  DebugOut("GameUIScratchGacha.OnVipClosed")
  if currencyTypes then
    local currencyDetail = GameUIScratchGacha:GetCurrencyDetail()
    if GameUIScratchGacha:GetFlashObject() then
      GameUIScratchGacha:GetFlashObject():InvokeASCallback("_root", "SetCurrency", currencyDetail)
    end
  end
end
function GameUIScratchGacha.BuyResetCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.reset_scratch_card_req.Code then
    if content.code ~= 0 then
      needGetCardAward = false
      if resetPrice.item_type == "credit" then
        local GameVip = LuaObjectManager:GetLuaObject("GameVip")
        GameVip:CheckIsNeedShowVip(content.code)
      else
        GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      end
    end
    return true
  elseif msgType == NetAPIList.reset_scratch_card_ack.Code then
    DebugOutPutTable(content, "BuyResetCallBack")
    exchangeInfo = content.awards
    table.sort(exchangeInfo, sortExchangeList)
    progressInfo = content.progress_award
    resetPrice = content.reset_price
    cardPrice = content.price
    itemBalance = content.exchange.item.items
    CurRoundOver = false
    GameUIScratchGacha:InitMenuInfo(false)
    isShowBuyButton = true
    GameUIScratchGacha:GetFlashObject():InvokeASCallback("_root", "ShowBuyCardButton")
    return true
  end
  return false
end
function GameUIScratchGacha:BuyCards()
  local param = {
    act_id = GameStateScratchGacha.act_id
  }
  local function callback()
    NetMessageMgr:SendMsg(NetAPIList.buy_scratch_card_req.Code, param, GameUIScratchGacha.BuyCardsCallBack, true, nil)
    GameUtils:RecordForTongdui(NetAPIList.buy_scratch_card_req.Code, param, GameUIScratchGacha.BuyCardsCallBack, true, nil)
  end
  if #currencyTypes == 1 then
    local textInfo = string.gsub(GameLoader:GetGameText("LC_MENU_SPACE_STATION_BUY_ALERT"), "<number1>", tostring(cardPrice.price))
    GameUtils:CreditCostConfirm(textInfo, callback, false, nil)
  else
    callback()
  end
end
function GameUIScratchGacha.BuyCardsCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.buy_scratch_card_req.Code then
    if content.code ~= 0 then
      local GameVip = LuaObjectManager:GetLuaObject("GameVip")
      GameVip:CheckIsNeedShowVip(content.code)
    end
    return true
  elseif msgType == NetAPIList.buy_scratch_card_ack.Code then
    DebugOutPutTable(content, "BuyCardsCallBack")
    isShowBuyButton = false
    itemBalance = content.items.item.items
    progressInfo = content.progress_award
    cardPrice = content.next_price
    cardAwards = content.award.items
    GameUIScratchGacha:InitMenuInfo(false)
    GameUIScratchGacha:EnableScratchArea()
    return true
  end
  return false
end
function GameUIScratchGacha:ExchangeReward(award_id, num)
  local param = {}
  param.act_id = GameStateScratchGacha.act_id
  param.award_id = award_id
  param.num = num
  NetMessageMgr:SendMsg(NetAPIList.exchange_award_req.Code, param, GameUIScratchGacha.ExchangeRewardCallBack, true, nil)
end
function GameUIScratchGacha.ExchangeRewardCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.exchange_award_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.exchange_award_ack.Code then
    DebugOutPutTable(content, "ExchangeRewardCallBack")
    itemBalance = content.items.item.items
    exchangeInfo = content.awards
    resetPrice = content.reset_price
    table.sort(exchangeInfo, sortExchangeList)
    GameUIScratchGacha:InitMenuInfo(false)
    return true
  end
  return false
end
function GameUIScratchGacha:GetAllCardReward()
  local param = {
    act_id = GameStateScratchGacha.act_id
  }
  NetMessageMgr:SendMsg(NetAPIList.end_scratch_req.Code, param, GameUIScratchGacha.GetAllCardRewardCallBack, true, nil)
end
function GameUIScratchGacha.GetAllCardRewardCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.end_scratch_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.end_scratch_ack.Code then
    DebugOutPutTable(content, "GetAllCardRewardCallBack")
    itemBalance = content.item.item.items
    exchangeInfo = content.awards
    table.sort(exchangeInfo, sortExchangeList)
    if GameUIScratchGacha:GetFlashObject() then
      local balanceDetail = GameUIScratchGacha:GetItemBalanceDetail()
      GameUIScratchGacha:GetFlashObject():InvokeASCallback("_root", "StartBingoEffect", balanceDetail)
    end
    return true
  end
  return false
end
function GameUIScratchGacha:GetBoxREward(award_id)
  local param = {}
  param.act_id = GameStateScratchGacha.act_id
  param.award_id = award_id
  NetMessageMgr:SendMsg(NetAPIList.get_process_award_req.Code, param, GameUIScratchGacha.GetBoxREwardCallBack, true, nil)
end
function GameUIScratchGacha.GetBoxREwardCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.get_process_award_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.get_process_award_ack.Code then
    DebugOutPutTable(content, "GetBoxREwardCallBack")
    progressInfo = content.progress_award
    local progressMax = progressInfo.max
    local progressCur = progressInfo.now
    local treasureBoxDetail = GameUIScratchGacha:GetProgressBarDetail()
    itemBalance = content.items.item.items
    local itemBalanceDetail = GameUIScratchGacha:GetItemBalanceDetail()
    if GameUIScratchGacha:GetFlashObject() then
      GameUIScratchGacha:GetFlashObject():InvokeASCallback("_root", "GetBoxREwardCallBack", progressMax, progressCur, treasureBoxDetail, itemBalanceDetail)
    end
    return true
  end
  return false
end
function GameUIScratchGacha:Update(dt)
  local flashObj = GameUIScratchGacha:GetFlashObject()
  if flashObj then
    flashObj:Update(dt)
    flashObj:InvokeASCallback("_root", "OnUpdateFrame", dt)
  end
end
if AutoUpdate.isAndroidDevice then
  function GameUIScratchGacha.OnAndroidBack()
    GameUIScratchGacha:OnFSCommand("close_menu", "")
  end
end
