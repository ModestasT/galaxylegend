WVEInfoPanelEscape = luaClass(nil)
local WVEGameManeger = LuaObjectManager:GetLuaObject("WVEGameManeger")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
function WVEInfoPanelEscape:ctor(wveGameManager)
  self.mGameManger = wveGameManager
end
function WVEInfoPanelEscape:OnFSCommand(cmd, arg)
  if cmd == "primeEscapeHelp" then
    self:Show()
    return true
  elseif cmd == "EscapeMC_Hide" then
    self:Hide()
    return true
  end
end
function WVEInfoPanelEscape:Show()
  local flash_obj = self.mGameManger:GetFlashObject()
  if not flash_obj then
    return
  end
  flash_obj:InvokeASCallback("_root", "EscapeMC_Show")
end
function WVEInfoPanelEscape:Hide()
  local flash_obj = self.mGameManger:GetFlashObject()
  if not flash_obj then
    return
  end
  flash_obj:InvokeASCallback("_root", "EscapeMC_Hide")
end
