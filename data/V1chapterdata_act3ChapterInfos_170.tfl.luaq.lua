local ChapterInfos_170 = GameData.chapterdata_act3.ChapterInfos_170
ChapterInfos_170[1] = {
  ChaperID = 1,
  ICON = "icon1",
  MapIndex = 1,
  BossPos = 5,
  EntroStory = {3101, 3102},
  AFTER_FINISH = {3106}
}
ChapterInfos_170[2] = {
  ChaperID = 2,
  ICON = "icon2",
  MapIndex = 2,
  BossPos = 10,
  EntroStory = {3201, 3202},
  AFTER_FINISH = {
    3207,
    3208,
    3209
  }
}
ChapterInfos_170[3] = {
  ChaperID = 3,
  ICON = "icon30",
  MapIndex = 3,
  BossPos = 16,
  EntroStory = {3301, 3302},
  AFTER_FINISH = {
    3308,
    3309,
    3310,
    3311
  }
}
ChapterInfos_170[4] = {
  ChaperID = 4,
  ICON = "icon400",
  MapIndex = 1,
  BossPos = 18,
  EntroStory = {3401},
  AFTER_FINISH = {
    3408,
    3409,
    3410
  }
}
ChapterInfos_170[5] = {
  ChaperID = 5,
  ICON = "icon5",
  MapIndex = 1,
  BossPos = 18,
  EntroStory = {
    3501,
    3502,
    3503,
    3504
  },
  AFTER_FINISH = {
    3514,
    3515,
    3516
  }
}
ChapterInfos_170[6] = {
  ChaperID = 6,
  ICON = "icon400",
  MapIndex = 1,
  BossPos = 18,
  EntroStory = {
    3601,
    3602,
    3603
  },
  AFTER_FINISH = {3606, 3607}
}
ChapterInfos_170[7] = {
  ChaperID = 7,
  ICON = "icon6",
  MapIndex = 1,
  BossPos = 18,
  EntroStory = {
    3701,
    3702,
    3703
  },
  AFTER_FINISH = {
    3705,
    3706,
    3707
  }
}
ChapterInfos_170[8] = {
  ChaperID = 8,
  ICON = "icon400",
  MapIndex = 1,
  BossPos = 18,
  EntroStory = {3801, 3802},
  AFTER_FINISH = {
    3806,
    3807,
    3808,
    3809,
    3810,
    3811
  }
}
ChapterInfos_170[9] = {
  ChaperID = 9,
  ICON = "icon5",
  MapIndex = 1,
  BossPos = 18,
  EntroStory = {},
  AFTER_FINISH = {}
}
ChapterInfos_170[10] = {
  ChaperID = 10,
  ICON = "icon5",
  MapIndex = 1,
  BossPos = 18,
  EntroStory = {},
  AFTER_FINISH = {}
}
