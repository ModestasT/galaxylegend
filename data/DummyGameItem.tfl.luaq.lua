DummyGameItem = luaClass(nil)
function DummyGameItem:ctor(id, itemType, gridType, titleType, subType, pos, cnt)
  self.item_id = id
  self.item_type = itemType
  self.mGridType = gridType
  self.mTitleType = titleType
  self.mSubType = subType
  self.pos = pos
  self.cnt = cnt
  self.mDummyPos = -1
  self.mNotInRealBag = false
  self.mItemLevel = 0
end
