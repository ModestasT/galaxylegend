local basic = GameData.heros.basic
table.insert(basic, {
  ID = 1,
  LEVEL = 0,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "player",
  SHIP = "ship17",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 1,
  LEVEL = 1,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "player",
  SHIP = "ship17",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 1,
  LEVEL = 2,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "player",
  SHIP = "ship17",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 1,
  LEVEL = 3,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "player",
  SHIP = "ship17",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 1,
  LEVEL = 4,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "player",
  SHIP = "ship17",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 1,
  LEVEL = 5,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "player",
  SHIP = "ship17",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 1,
  LEVEL = 6,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "player",
  SHIP = "ship300",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 1,
  LEVEL = 7,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "player",
  SHIP = "ship300",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 1,
  LEVEL = 8,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "player",
  SHIP = "ship300",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 1,
  LEVEL = 9,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "player",
  SHIP = "ship300",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 1,
  LEVEL = 10,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "player",
  SHIP = "ship300",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 1,
  LEVEL = 11,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "player",
  SHIP = "ship300",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 1,
  LEVEL = 12,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "player",
  SHIP = "ship300",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 1,
  LEVEL = 13,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "player",
  SHIP = "ship300",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 1,
  LEVEL = 14,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "player",
  SHIP = "ship300",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 1,
  LEVEL = 15,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "player",
  SHIP = "ship300",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 1,
  LEVEL = 16,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "player",
  SHIP = "ship300",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 2,
  LEVEL = 0,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 13,
  AVATAR = "head1",
  SHIP = "ship18",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 2,
  LEVEL = 1,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 13,
  AVATAR = "head1",
  SHIP = "ship18",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 2,
  LEVEL = 2,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 13,
  AVATAR = "head1",
  SHIP = "ship18",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 2,
  LEVEL = 3,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 13,
  AVATAR = "head1",
  SHIP = "ship18",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 2,
  LEVEL = 4,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 13,
  AVATAR = "head1",
  SHIP = "ship18",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 2,
  LEVEL = 5,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 13,
  AVATAR = "head1",
  SHIP = "ship18",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 2,
  LEVEL = 6,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 13,
  AVATAR = "head1",
  SHIP = "ship18",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 2,
  LEVEL = 7,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 13,
  AVATAR = "head1",
  SHIP = "ship18",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 2,
  LEVEL = 8,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 13,
  AVATAR = "head1",
  SHIP = "ship18",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 2,
  LEVEL = 9,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 13,
  AVATAR = "head1",
  SHIP = "ship18",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 2,
  LEVEL = 10,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 13,
  AVATAR = "head1",
  SHIP = "ship18",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 2,
  LEVEL = 11,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 13,
  AVATAR = "head1",
  SHIP = "ship18",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 2,
  LEVEL = 12,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 13,
  AVATAR = "head1",
  SHIP = "ship18",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 2,
  LEVEL = 13,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 13,
  AVATAR = "head1",
  SHIP = "ship18",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 2,
  LEVEL = 14,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 13,
  AVATAR = "head1",
  SHIP = "ship18",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 2,
  LEVEL = 15,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 13,
  AVATAR = "head1",
  SHIP = "ship18",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 2,
  LEVEL = 16,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 13,
  AVATAR = "head1",
  SHIP = "ship18",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 3,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 12,
  AVATAR = "fu  guan",
  SHIP = "ship2",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 3,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 12,
  AVATAR = "fu  guan",
  SHIP = "ship2",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 3,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 12,
  AVATAR = "fu  guan",
  SHIP = "ship2",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 3,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 12,
  AVATAR = "fu  guan",
  SHIP = "ship2",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 3,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 12,
  AVATAR = "fu  guan",
  SHIP = "ship2",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 3,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 12,
  AVATAR = "fu  guan",
  SHIP = "ship2",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 3,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 12,
  AVATAR = "fu  guan",
  SHIP = "ship2",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 3,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 12,
  AVATAR = "fu  guan",
  SHIP = "ship2",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 3,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 12,
  AVATAR = "fu  guan",
  SHIP = "ship2",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 3,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 12,
  AVATAR = "fu  guan",
  SHIP = "ship2",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 3,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 12,
  AVATAR = "fu  guan",
  SHIP = "ship2",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 3,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 12,
  AVATAR = "fu  guan",
  SHIP = "ship2",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 3,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 12,
  AVATAR = "fu  guan",
  SHIP = "ship2",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 3,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 12,
  AVATAR = "fu  guan",
  SHIP = "ship2",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 3,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 12,
  AVATAR = "fu  guan",
  SHIP = "ship2",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 3,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 12,
  AVATAR = "fu  guan",
  SHIP = "ship2",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 3,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 12,
  AVATAR = "fu  guan",
  SHIP = "ship2",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 4,
  LEVEL = 0,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 12,
  AVATAR = "head14",
  SHIP = "ship19",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 4,
  LEVEL = 1,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 12,
  AVATAR = "head14",
  SHIP = "ship19",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 4,
  LEVEL = 2,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 12,
  AVATAR = "head14",
  SHIP = "ship19",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 4,
  LEVEL = 3,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 12,
  AVATAR = "head14",
  SHIP = "ship19",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 4,
  LEVEL = 4,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 12,
  AVATAR = "head14",
  SHIP = "ship19",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 4,
  LEVEL = 5,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 12,
  AVATAR = "head14",
  SHIP = "ship19",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 4,
  LEVEL = 6,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 12,
  AVATAR = "head14",
  SHIP = "ship19",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 4,
  LEVEL = 7,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 12,
  AVATAR = "head14",
  SHIP = "ship19",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 4,
  LEVEL = 8,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 12,
  AVATAR = "head14",
  SHIP = "ship19",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 4,
  LEVEL = 9,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 12,
  AVATAR = "head14",
  SHIP = "ship19",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 4,
  LEVEL = 10,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 12,
  AVATAR = "head14",
  SHIP = "ship19",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 4,
  LEVEL = 11,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 12,
  AVATAR = "head14",
  SHIP = "ship19",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 4,
  LEVEL = 12,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 12,
  AVATAR = "head14",
  SHIP = "ship19",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 4,
  LEVEL = 13,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 12,
  AVATAR = "head14",
  SHIP = "ship19",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 4,
  LEVEL = 14,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 12,
  AVATAR = "head14",
  SHIP = "ship19",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 4,
  LEVEL = 15,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 12,
  AVATAR = "head14",
  SHIP = "ship19",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 4,
  LEVEL = 16,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 12,
  AVATAR = "head14",
  SHIP = "ship19",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 5,
  LEVEL = 0,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 23,
  AVATAR = "head16",
  SHIP = "ship47",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 5,
  LEVEL = 1,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 23,
  AVATAR = "head16",
  SHIP = "ship47",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 5,
  LEVEL = 2,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 23,
  AVATAR = "head16",
  SHIP = "ship47",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 5,
  LEVEL = 3,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 23,
  AVATAR = "head16",
  SHIP = "ship47",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 5,
  LEVEL = 4,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 23,
  AVATAR = "head16",
  SHIP = "ship47",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 5,
  LEVEL = 5,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 23,
  AVATAR = "head16",
  SHIP = "ship47",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 5,
  LEVEL = 6,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 23,
  AVATAR = "head16",
  SHIP = "ship47",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 5,
  LEVEL = 7,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 23,
  AVATAR = "head16",
  SHIP = "ship47",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 5,
  LEVEL = 8,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 23,
  AVATAR = "head16",
  SHIP = "ship47",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 5,
  LEVEL = 9,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 23,
  AVATAR = "head16",
  SHIP = "ship47",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 5,
  LEVEL = 10,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 23,
  AVATAR = "head16",
  SHIP = "ship47",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 5,
  LEVEL = 11,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 23,
  AVATAR = "head16",
  SHIP = "ship47",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 5,
  LEVEL = 12,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 23,
  AVATAR = "head16",
  SHIP = "ship47",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 5,
  LEVEL = 13,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 23,
  AVATAR = "head16",
  SHIP = "ship47",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 5,
  LEVEL = 14,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 23,
  AVATAR = "head16",
  SHIP = "ship47",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 5,
  LEVEL = 15,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 23,
  AVATAR = "head16",
  SHIP = "ship47",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 5,
  LEVEL = 16,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 23,
  AVATAR = "head16",
  SHIP = "ship47",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 6,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 22,
  AVATAR = "head22",
  SHIP = "ship42",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 6,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 22,
  AVATAR = "head22",
  SHIP = "ship42",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 6,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 22,
  AVATAR = "head22",
  SHIP = "ship42",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 6,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 22,
  AVATAR = "head22",
  SHIP = "ship42",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 6,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 22,
  AVATAR = "head22",
  SHIP = "ship42",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 6,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 22,
  AVATAR = "head22",
  SHIP = "ship42",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 6,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 22,
  AVATAR = "head22",
  SHIP = "ship42",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 6,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 22,
  AVATAR = "head22",
  SHIP = "ship42",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 6,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 22,
  AVATAR = "head22",
  SHIP = "ship42",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 6,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 22,
  AVATAR = "head22",
  SHIP = "ship42",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 6,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 22,
  AVATAR = "head22",
  SHIP = "ship42",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 6,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 22,
  AVATAR = "head22",
  SHIP = "ship42",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 6,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 22,
  AVATAR = "head22",
  SHIP = "ship42",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 6,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 22,
  AVATAR = "head22",
  SHIP = "ship42",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 6,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 22,
  AVATAR = "head22",
  SHIP = "ship42",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 6,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 22,
  AVATAR = "head22",
  SHIP = "ship42",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 6,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 22,
  AVATAR = "head22",
  SHIP = "ship42",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 7,
  LEVEL = 0,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 20,
  AVATAR = "head2",
  SHIP = "ship5",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 7,
  LEVEL = 1,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 20,
  AVATAR = "head2",
  SHIP = "ship5",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 7,
  LEVEL = 2,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 20,
  AVATAR = "head2",
  SHIP = "ship5",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 7,
  LEVEL = 3,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 20,
  AVATAR = "head2",
  SHIP = "ship5",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 7,
  LEVEL = 4,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 20,
  AVATAR = "head2",
  SHIP = "ship5",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 7,
  LEVEL = 5,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 20,
  AVATAR = "head2",
  SHIP = "ship5",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 7,
  LEVEL = 6,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 20,
  AVATAR = "head2",
  SHIP = "ship5",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 7,
  LEVEL = 7,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 20,
  AVATAR = "head2",
  SHIP = "ship5",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 7,
  LEVEL = 8,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 20,
  AVATAR = "head2",
  SHIP = "ship5",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 7,
  LEVEL = 9,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 20,
  AVATAR = "head2",
  SHIP = "ship5",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 7,
  LEVEL = 10,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 20,
  AVATAR = "head2",
  SHIP = "ship5",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 7,
  LEVEL = 11,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 20,
  AVATAR = "head2",
  SHIP = "ship5",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 7,
  LEVEL = 12,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 20,
  AVATAR = "head2",
  SHIP = "ship5",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 7,
  LEVEL = 13,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 20,
  AVATAR = "head2",
  SHIP = "ship5",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 7,
  LEVEL = 14,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 20,
  AVATAR = "head2",
  SHIP = "ship5",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 7,
  LEVEL = 15,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 20,
  AVATAR = "head2",
  SHIP = "ship5",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 7,
  LEVEL = 16,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 20,
  AVATAR = "head2",
  SHIP = "ship5",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 8,
  LEVEL = 0,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 19,
  AVATAR = "head9",
  SHIP = "ship43",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 8,
  LEVEL = 1,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 19,
  AVATAR = "head9",
  SHIP = "ship43",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 8,
  LEVEL = 2,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 19,
  AVATAR = "head9",
  SHIP = "ship43",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 8,
  LEVEL = 3,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 19,
  AVATAR = "head9",
  SHIP = "ship43",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 8,
  LEVEL = 4,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 19,
  AVATAR = "head9",
  SHIP = "ship43",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 8,
  LEVEL = 5,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 19,
  AVATAR = "head9",
  SHIP = "ship43",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 8,
  LEVEL = 6,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 19,
  AVATAR = "head9",
  SHIP = "ship43",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 8,
  LEVEL = 7,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 19,
  AVATAR = "head9",
  SHIP = "ship43",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 8,
  LEVEL = 8,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 19,
  AVATAR = "head9",
  SHIP = "ship43",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 8,
  LEVEL = 9,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 19,
  AVATAR = "head9",
  SHIP = "ship43",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 8,
  LEVEL = 10,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 19,
  AVATAR = "head9",
  SHIP = "ship43",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 8,
  LEVEL = 11,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 19,
  AVATAR = "head9",
  SHIP = "ship43",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 8,
  LEVEL = 12,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 19,
  AVATAR = "head9",
  SHIP = "ship43",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 8,
  LEVEL = 13,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 19,
  AVATAR = "head9",
  SHIP = "ship43",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 8,
  LEVEL = 14,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 19,
  AVATAR = "head9",
  SHIP = "ship43",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 8,
  LEVEL = 15,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 19,
  AVATAR = "head9",
  SHIP = "ship43",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 8,
  LEVEL = 16,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 19,
  AVATAR = "head9",
  SHIP = "ship43",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 9,
  LEVEL = 0,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 11,
  AVATAR = "head19",
  SHIP = "ship49",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 9,
  LEVEL = 1,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 11,
  AVATAR = "head19",
  SHIP = "ship49",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 9,
  LEVEL = 2,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 11,
  AVATAR = "head19",
  SHIP = "ship49",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 9,
  LEVEL = 3,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 11,
  AVATAR = "head19",
  SHIP = "ship49",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 9,
  LEVEL = 4,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 11,
  AVATAR = "head19",
  SHIP = "ship49",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 9,
  LEVEL = 5,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 11,
  AVATAR = "head19",
  SHIP = "ship49",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 9,
  LEVEL = 6,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 11,
  AVATAR = "head19",
  SHIP = "ship49",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 9,
  LEVEL = 7,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 11,
  AVATAR = "head19",
  SHIP = "ship49",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 9,
  LEVEL = 8,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 11,
  AVATAR = "head19",
  SHIP = "ship49",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 9,
  LEVEL = 9,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 11,
  AVATAR = "head19",
  SHIP = "ship49",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 9,
  LEVEL = 10,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 11,
  AVATAR = "head19",
  SHIP = "ship49",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 9,
  LEVEL = 11,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 11,
  AVATAR = "head19",
  SHIP = "ship49",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 9,
  LEVEL = 12,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 11,
  AVATAR = "head19",
  SHIP = "ship49",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 9,
  LEVEL = 13,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 11,
  AVATAR = "head19",
  SHIP = "ship49",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 9,
  LEVEL = 14,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 11,
  AVATAR = "head19",
  SHIP = "ship49",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 9,
  LEVEL = 15,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 11,
  AVATAR = "head19",
  SHIP = "ship49",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 9,
  LEVEL = 16,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 11,
  AVATAR = "head19",
  SHIP = "ship49",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 10,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 12,
  AVATAR = "head6",
  SHIP = "ship2",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 10,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 12,
  AVATAR = "head6",
  SHIP = "ship2",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 10,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 12,
  AVATAR = "head6",
  SHIP = "ship2",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 10,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 12,
  AVATAR = "head6",
  SHIP = "ship2",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 10,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 12,
  AVATAR = "head6",
  SHIP = "ship2",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 10,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 12,
  AVATAR = "head6",
  SHIP = "ship2",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 10,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 12,
  AVATAR = "head6",
  SHIP = "ship2",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 10,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 12,
  AVATAR = "head6",
  SHIP = "ship2",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 10,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 12,
  AVATAR = "head6",
  SHIP = "ship2",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 10,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 12,
  AVATAR = "head6",
  SHIP = "ship2",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 10,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 12,
  AVATAR = "head6",
  SHIP = "ship2",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 10,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 12,
  AVATAR = "head6",
  SHIP = "ship2",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 10,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 12,
  AVATAR = "head6",
  SHIP = "ship2",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 10,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 12,
  AVATAR = "head6",
  SHIP = "ship2",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 10,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 12,
  AVATAR = "head6",
  SHIP = "ship2",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 10,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 12,
  AVATAR = "head6",
  SHIP = "ship2",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 10,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 12,
  AVATAR = "head6",
  SHIP = "ship2",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 11,
  LEVEL = 0,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 20,
  AVATAR = "head28",
  SHIP = "ship41",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 11,
  LEVEL = 1,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 20,
  AVATAR = "head28",
  SHIP = "ship41",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 11,
  LEVEL = 2,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 20,
  AVATAR = "head28",
  SHIP = "ship41",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 11,
  LEVEL = 3,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 20,
  AVATAR = "head28",
  SHIP = "ship41",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 11,
  LEVEL = 4,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 20,
  AVATAR = "head28",
  SHIP = "ship41",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 11,
  LEVEL = 5,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 20,
  AVATAR = "head28",
  SHIP = "ship41",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 11,
  LEVEL = 6,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 20,
  AVATAR = "head28",
  SHIP = "ship41",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 11,
  LEVEL = 7,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 20,
  AVATAR = "head28",
  SHIP = "ship41",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 11,
  LEVEL = 8,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 20,
  AVATAR = "head28",
  SHIP = "ship41",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 11,
  LEVEL = 9,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 20,
  AVATAR = "head28",
  SHIP = "ship41",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 11,
  LEVEL = 10,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 20,
  AVATAR = "head28",
  SHIP = "ship41",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 11,
  LEVEL = 11,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 20,
  AVATAR = "head28",
  SHIP = "ship41",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 11,
  LEVEL = 12,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 20,
  AVATAR = "head28",
  SHIP = "ship41",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 11,
  LEVEL = 13,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 20,
  AVATAR = "head28",
  SHIP = "ship41",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 11,
  LEVEL = 14,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 20,
  AVATAR = "head28",
  SHIP = "ship41",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 11,
  LEVEL = 15,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 20,
  AVATAR = "head28",
  SHIP = "ship41",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 11,
  LEVEL = 16,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 20,
  AVATAR = "head28",
  SHIP = "ship41",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 12,
  LEVEL = 0,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 19,
  AVATAR = "head27",
  SHIP = "ship38",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 12,
  LEVEL = 1,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 19,
  AVATAR = "head27",
  SHIP = "ship38",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 12,
  LEVEL = 2,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 19,
  AVATAR = "head27",
  SHIP = "ship38",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 12,
  LEVEL = 3,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 19,
  AVATAR = "head27",
  SHIP = "ship38",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 12,
  LEVEL = 4,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 19,
  AVATAR = "head27",
  SHIP = "ship38",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 12,
  LEVEL = 5,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 19,
  AVATAR = "head27",
  SHIP = "ship38",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 12,
  LEVEL = 6,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 19,
  AVATAR = "head27",
  SHIP = "ship38",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 12,
  LEVEL = 7,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 19,
  AVATAR = "head27",
  SHIP = "ship38",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 12,
  LEVEL = 8,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 19,
  AVATAR = "head27",
  SHIP = "ship38",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 12,
  LEVEL = 9,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 19,
  AVATAR = "head27",
  SHIP = "ship38",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 12,
  LEVEL = 10,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 19,
  AVATAR = "head27",
  SHIP = "ship38",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 12,
  LEVEL = 11,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 19,
  AVATAR = "head27",
  SHIP = "ship38",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 12,
  LEVEL = 12,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 19,
  AVATAR = "head27",
  SHIP = "ship38",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 12,
  LEVEL = 13,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 19,
  AVATAR = "head27",
  SHIP = "ship38",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 12,
  LEVEL = 14,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 19,
  AVATAR = "head27",
  SHIP = "ship38",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 12,
  LEVEL = 15,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 19,
  AVATAR = "head27",
  SHIP = "ship38",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 12,
  LEVEL = 16,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 19,
  AVATAR = "head27",
  SHIP = "ship38",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 13,
  LEVEL = 0,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 11,
  AVATAR = "head21",
  SHIP = "ship49",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 13,
  LEVEL = 1,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 11,
  AVATAR = "head21",
  SHIP = "ship49",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 13,
  LEVEL = 2,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 11,
  AVATAR = "head21",
  SHIP = "ship49",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 13,
  LEVEL = 3,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 11,
  AVATAR = "head21",
  SHIP = "ship49",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 13,
  LEVEL = 4,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 11,
  AVATAR = "head21",
  SHIP = "ship49",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 13,
  LEVEL = 5,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 11,
  AVATAR = "head21",
  SHIP = "ship49",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 13,
  LEVEL = 6,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 11,
  AVATAR = "head21",
  SHIP = "ship49",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 13,
  LEVEL = 7,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 11,
  AVATAR = "head21",
  SHIP = "ship49",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 13,
  LEVEL = 8,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 11,
  AVATAR = "head21",
  SHIP = "ship49",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 13,
  LEVEL = 9,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 11,
  AVATAR = "head21",
  SHIP = "ship49",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 13,
  LEVEL = 10,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 11,
  AVATAR = "head21",
  SHIP = "ship49",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 13,
  LEVEL = 11,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 11,
  AVATAR = "head21",
  SHIP = "ship49",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 13,
  LEVEL = 12,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 11,
  AVATAR = "head21",
  SHIP = "ship49",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 13,
  LEVEL = 13,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 11,
  AVATAR = "head21",
  SHIP = "ship49",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 13,
  LEVEL = 14,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 11,
  AVATAR = "head21",
  SHIP = "ship49",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 13,
  LEVEL = 15,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 11,
  AVATAR = "head21",
  SHIP = "ship49",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 13,
  LEVEL = 16,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 11,
  AVATAR = "head21",
  SHIP = "ship49",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 14,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 12,
  AVATAR = "head31",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 14,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 12,
  AVATAR = "head31",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 14,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 12,
  AVATAR = "head31",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 14,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 12,
  AVATAR = "head31",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 14,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 12,
  AVATAR = "head31",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 14,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 12,
  AVATAR = "head31",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 14,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 12,
  AVATAR = "head31",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 14,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 12,
  AVATAR = "head31",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 14,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 12,
  AVATAR = "head31",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 14,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 12,
  AVATAR = "head31",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 14,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 12,
  AVATAR = "head31",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 14,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 12,
  AVATAR = "head31",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 14,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 12,
  AVATAR = "head31",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 14,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 12,
  AVATAR = "head31",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 14,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 12,
  AVATAR = "head31",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 14,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 12,
  AVATAR = "head31",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 14,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 12,
  AVATAR = "head31",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 15,
  LEVEL = 0,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 20,
  AVATAR = "head15",
  SHIP = "ship43",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 15,
  LEVEL = 1,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 20,
  AVATAR = "head15",
  SHIP = "ship43",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 15,
  LEVEL = 2,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 20,
  AVATAR = "head15",
  SHIP = "ship43",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 15,
  LEVEL = 3,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 20,
  AVATAR = "head15",
  SHIP = "ship43",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 15,
  LEVEL = 4,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 20,
  AVATAR = "head15",
  SHIP = "ship43",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 15,
  LEVEL = 5,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 20,
  AVATAR = "head15",
  SHIP = "ship43",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 15,
  LEVEL = 6,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 20,
  AVATAR = "head15",
  SHIP = "ship43",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 15,
  LEVEL = 7,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 20,
  AVATAR = "head15",
  SHIP = "ship43",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 15,
  LEVEL = 8,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 20,
  AVATAR = "head15",
  SHIP = "ship43",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 15,
  LEVEL = 9,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 20,
  AVATAR = "head15",
  SHIP = "ship43",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 15,
  LEVEL = 10,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 20,
  AVATAR = "head15",
  SHIP = "ship43",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 15,
  LEVEL = 11,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 20,
  AVATAR = "head15",
  SHIP = "ship43",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 15,
  LEVEL = 12,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 20,
  AVATAR = "head15",
  SHIP = "ship43",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 15,
  LEVEL = 13,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 20,
  AVATAR = "head15",
  SHIP = "ship43",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 15,
  LEVEL = 14,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 20,
  AVATAR = "head15",
  SHIP = "ship43",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 15,
  LEVEL = 15,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 20,
  AVATAR = "head15",
  SHIP = "ship43",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 15,
  LEVEL = 16,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 20,
  AVATAR = "head15",
  SHIP = "ship43",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 16,
  LEVEL = 0,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 10,
  AVATAR = "head32",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 16,
  LEVEL = 1,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 10,
  AVATAR = "head32",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 16,
  LEVEL = 2,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 10,
  AVATAR = "head32",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 16,
  LEVEL = 3,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 10,
  AVATAR = "head32",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 16,
  LEVEL = 4,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 10,
  AVATAR = "head32",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 16,
  LEVEL = 5,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 10,
  AVATAR = "head32",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 16,
  LEVEL = 6,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 10,
  AVATAR = "head32",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 16,
  LEVEL = 7,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 10,
  AVATAR = "head32",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 16,
  LEVEL = 8,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 10,
  AVATAR = "head32",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 16,
  LEVEL = 9,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 10,
  AVATAR = "head32",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 16,
  LEVEL = 10,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 10,
  AVATAR = "head32",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 16,
  LEVEL = 11,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 10,
  AVATAR = "head32",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 16,
  LEVEL = 12,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 10,
  AVATAR = "head32",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 16,
  LEVEL = 13,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 10,
  AVATAR = "head32",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 16,
  LEVEL = 14,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 10,
  AVATAR = "head32",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 16,
  LEVEL = 15,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 10,
  AVATAR = "head32",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 16,
  LEVEL = 16,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 10,
  AVATAR = "head32",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 17,
  LEVEL = 0,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 11,
  AVATAR = "head41",
  SHIP = "ship16",
  sex = 2,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 17,
  LEVEL = 1,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 11,
  AVATAR = "head41",
  SHIP = "ship16",
  sex = 2,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 17,
  LEVEL = 2,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 11,
  AVATAR = "head41",
  SHIP = "ship16",
  sex = 2,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 17,
  LEVEL = 3,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 11,
  AVATAR = "head41",
  SHIP = "ship16",
  sex = 2,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 17,
  LEVEL = 4,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 11,
  AVATAR = "head41",
  SHIP = "ship16",
  sex = 2,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 17,
  LEVEL = 5,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 11,
  AVATAR = "head41",
  SHIP = "ship16",
  sex = 2,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 17,
  LEVEL = 6,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 11,
  AVATAR = "head41",
  SHIP = "ship16",
  sex = 2,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 17,
  LEVEL = 7,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 11,
  AVATAR = "head41",
  SHIP = "ship16",
  sex = 2,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 17,
  LEVEL = 8,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 11,
  AVATAR = "head41",
  SHIP = "ship16",
  sex = 2,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 17,
  LEVEL = 9,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 11,
  AVATAR = "head41",
  SHIP = "ship16",
  sex = 2,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 17,
  LEVEL = 10,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 11,
  AVATAR = "head41",
  SHIP = "ship16",
  sex = 2,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 17,
  LEVEL = 11,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 11,
  AVATAR = "head41",
  SHIP = "ship16",
  sex = 2,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 17,
  LEVEL = 12,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 11,
  AVATAR = "head41",
  SHIP = "ship16",
  sex = 2,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 17,
  LEVEL = 13,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 11,
  AVATAR = "head41",
  SHIP = "ship16",
  sex = 2,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 17,
  LEVEL = 14,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 11,
  AVATAR = "head41",
  SHIP = "ship16",
  sex = 2,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 17,
  LEVEL = 15,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 11,
  AVATAR = "head41",
  SHIP = "ship16",
  sex = 2,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 17,
  LEVEL = 16,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 11,
  AVATAR = "head41",
  SHIP = "ship16",
  sex = 2,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 18,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 24,
  AVATAR = "head40",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 18,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 24,
  AVATAR = "head40",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 18,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 24,
  AVATAR = "head40",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 18,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 24,
  AVATAR = "head40",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 18,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 24,
  AVATAR = "head40",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 18,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 24,
  AVATAR = "head40",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 18,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 24,
  AVATAR = "head40",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 18,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 24,
  AVATAR = "head40",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 18,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 24,
  AVATAR = "head40",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 18,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 24,
  AVATAR = "head40",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 18,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 24,
  AVATAR = "head40",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 18,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 24,
  AVATAR = "head40",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 18,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 24,
  AVATAR = "head40",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 18,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 24,
  AVATAR = "head40",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 18,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 24,
  AVATAR = "head40",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 18,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 24,
  AVATAR = "head40",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 18,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 24,
  AVATAR = "head40",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 19,
  LEVEL = 0,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 28,
  AVATAR = "head34",
  SHIP = "ship16",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 19,
  LEVEL = 1,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 28,
  AVATAR = "head34",
  SHIP = "ship16",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 19,
  LEVEL = 2,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 28,
  AVATAR = "head34",
  SHIP = "ship16",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 19,
  LEVEL = 3,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 28,
  AVATAR = "head34",
  SHIP = "ship16",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 19,
  LEVEL = 4,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 28,
  AVATAR = "head34",
  SHIP = "ship16",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 19,
  LEVEL = 5,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 28,
  AVATAR = "head34",
  SHIP = "ship16",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 19,
  LEVEL = 6,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 28,
  AVATAR = "head34",
  SHIP = "ship16",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 19,
  LEVEL = 7,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 28,
  AVATAR = "head34",
  SHIP = "ship16",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 19,
  LEVEL = 8,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 28,
  AVATAR = "head34",
  SHIP = "ship16",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 19,
  LEVEL = 9,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 28,
  AVATAR = "head34",
  SHIP = "ship16",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 19,
  LEVEL = 10,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 28,
  AVATAR = "head34",
  SHIP = "ship16",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 19,
  LEVEL = 11,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 28,
  AVATAR = "head34",
  SHIP = "ship16",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 19,
  LEVEL = 12,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 28,
  AVATAR = "head34",
  SHIP = "ship16",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 19,
  LEVEL = 13,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 28,
  AVATAR = "head34",
  SHIP = "ship16",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 19,
  LEVEL = 14,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 28,
  AVATAR = "head34",
  SHIP = "ship16",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 19,
  LEVEL = 15,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 28,
  AVATAR = "head34",
  SHIP = "ship16",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 19,
  LEVEL = 16,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 28,
  AVATAR = "head34",
  SHIP = "ship16",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 20,
  LEVEL = 0,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 51,
  AVATAR = "head62",
  SHIP = "ship65",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 20,
  LEVEL = 1,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 51,
  AVATAR = "head62",
  SHIP = "ship65",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 20,
  LEVEL = 2,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 51,
  AVATAR = "head62",
  SHIP = "ship65",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 20,
  LEVEL = 3,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 51,
  AVATAR = "head62",
  SHIP = "ship65",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 20,
  LEVEL = 4,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 51,
  AVATAR = "head62",
  SHIP = "ship65",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 20,
  LEVEL = 5,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 51,
  AVATAR = "head62",
  SHIP = "ship65",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 20,
  LEVEL = 6,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 51,
  AVATAR = "head62",
  SHIP = "ship65",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 20,
  LEVEL = 7,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 51,
  AVATAR = "head62",
  SHIP = "ship65",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 20,
  LEVEL = 8,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 51,
  AVATAR = "head62",
  SHIP = "ship65",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 20,
  LEVEL = 9,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 51,
  AVATAR = "head62",
  SHIP = "ship65",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 20,
  LEVEL = 10,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 51,
  AVATAR = "head62",
  SHIP = "ship65",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 20,
  LEVEL = 11,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 51,
  AVATAR = "head62",
  SHIP = "ship65",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 20,
  LEVEL = 12,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 51,
  AVATAR = "head62",
  SHIP = "ship65",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 20,
  LEVEL = 13,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 51,
  AVATAR = "head62",
  SHIP = "ship65",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 20,
  LEVEL = 14,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 51,
  AVATAR = "head62",
  SHIP = "ship65",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 20,
  LEVEL = 15,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 51,
  AVATAR = "head62",
  SHIP = "ship65",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 20,
  LEVEL = 16,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 51,
  AVATAR = "head62",
  SHIP = "ship65",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 21,
  LEVEL = 0,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 52,
  AVATAR = "head63",
  SHIP = "ship66",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 21,
  LEVEL = 1,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 52,
  AVATAR = "head63",
  SHIP = "ship66",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 21,
  LEVEL = 2,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 52,
  AVATAR = "head63",
  SHIP = "ship66",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 21,
  LEVEL = 3,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 52,
  AVATAR = "head63",
  SHIP = "ship66",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 21,
  LEVEL = 4,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 52,
  AVATAR = "head63",
  SHIP = "ship66",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 21,
  LEVEL = 5,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 52,
  AVATAR = "head63",
  SHIP = "ship66",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 21,
  LEVEL = 6,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 52,
  AVATAR = "head63",
  SHIP = "ship66",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 21,
  LEVEL = 7,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 52,
  AVATAR = "head63",
  SHIP = "ship66",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 21,
  LEVEL = 8,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 52,
  AVATAR = "head63",
  SHIP = "ship66",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 21,
  LEVEL = 9,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 52,
  AVATAR = "head63",
  SHIP = "ship66",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 21,
  LEVEL = 10,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 52,
  AVATAR = "head63",
  SHIP = "ship66",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 21,
  LEVEL = 11,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 52,
  AVATAR = "head63",
  SHIP = "ship66",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 21,
  LEVEL = 12,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 52,
  AVATAR = "head63",
  SHIP = "ship66",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 21,
  LEVEL = 13,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 52,
  AVATAR = "head63",
  SHIP = "ship66",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 21,
  LEVEL = 14,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 52,
  AVATAR = "head63",
  SHIP = "ship66",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 21,
  LEVEL = 15,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 52,
  AVATAR = "head63",
  SHIP = "ship66",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 21,
  LEVEL = 16,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 52,
  AVATAR = "head63",
  SHIP = "ship66",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 22,
  LEVEL = 0,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 53,
  AVATAR = "head64",
  SHIP = "ship67",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 22,
  LEVEL = 1,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 53,
  AVATAR = "head64",
  SHIP = "ship67",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 22,
  LEVEL = 2,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 53,
  AVATAR = "head64",
  SHIP = "ship67",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 22,
  LEVEL = 3,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 53,
  AVATAR = "head64",
  SHIP = "ship67",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 22,
  LEVEL = 4,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 53,
  AVATAR = "head64",
  SHIP = "ship67",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 22,
  LEVEL = 5,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 53,
  AVATAR = "head64",
  SHIP = "ship67",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 22,
  LEVEL = 6,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 53,
  AVATAR = "head64",
  SHIP = "ship67",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 22,
  LEVEL = 7,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 53,
  AVATAR = "head64",
  SHIP = "ship67",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 22,
  LEVEL = 8,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 53,
  AVATAR = "head64",
  SHIP = "ship67",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 22,
  LEVEL = 9,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 53,
  AVATAR = "head64",
  SHIP = "ship67",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 22,
  LEVEL = 10,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 53,
  AVATAR = "head64",
  SHIP = "ship67",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 22,
  LEVEL = 11,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 53,
  AVATAR = "head64",
  SHIP = "ship67",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 22,
  LEVEL = 12,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 53,
  AVATAR = "head64",
  SHIP = "ship67",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 22,
  LEVEL = 13,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 53,
  AVATAR = "head64",
  SHIP = "ship67",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 22,
  LEVEL = 14,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 53,
  AVATAR = "head64",
  SHIP = "ship67",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 22,
  LEVEL = 15,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 53,
  AVATAR = "head64",
  SHIP = "ship67",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 22,
  LEVEL = 16,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 53,
  AVATAR = "head64",
  SHIP = "ship67",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 102,
  LEVEL = 0,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1002,
  AVATAR = "player",
  SHIP = "ship17",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 102,
  LEVEL = 1,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1002,
  AVATAR = "player",
  SHIP = "ship17",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 102,
  LEVEL = 2,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1002,
  AVATAR = "player",
  SHIP = "ship17",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 102,
  LEVEL = 3,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1002,
  AVATAR = "player",
  SHIP = "ship17",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 102,
  LEVEL = 4,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1002,
  AVATAR = "player",
  SHIP = "ship17",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 102,
  LEVEL = 5,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1002,
  AVATAR = "player",
  SHIP = "ship17",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 102,
  LEVEL = 6,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1002,
  AVATAR = "player",
  SHIP = "ship17",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 102,
  LEVEL = 7,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1002,
  AVATAR = "player",
  SHIP = "ship17",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 102,
  LEVEL = 8,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1002,
  AVATAR = "player",
  SHIP = "ship17",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 102,
  LEVEL = 9,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1002,
  AVATAR = "player",
  SHIP = "ship17",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 102,
  LEVEL = 10,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1002,
  AVATAR = "player",
  SHIP = "ship17",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 102,
  LEVEL = 11,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1002,
  AVATAR = "player",
  SHIP = "ship17",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 102,
  LEVEL = 12,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1002,
  AVATAR = "player",
  SHIP = "ship17",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 102,
  LEVEL = 13,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1002,
  AVATAR = "player",
  SHIP = "ship17",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 102,
  LEVEL = 14,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1002,
  AVATAR = "player",
  SHIP = "ship17",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 102,
  LEVEL = 15,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1002,
  AVATAR = "player",
  SHIP = "ship17",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 102,
  LEVEL = 16,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1002,
  AVATAR = "player",
  SHIP = "ship17",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 103,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1003,
  AVATAR = "head13",
  SHIP = "ship37",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 103,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1003,
  AVATAR = "head13",
  SHIP = "ship37",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 103,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1003,
  AVATAR = "head13",
  SHIP = "ship37",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 103,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1003,
  AVATAR = "head13",
  SHIP = "ship37",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 103,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1003,
  AVATAR = "head13",
  SHIP = "ship37",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 103,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1003,
  AVATAR = "head13",
  SHIP = "ship37",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 103,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1003,
  AVATAR = "head13",
  SHIP = "ship37",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 103,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1003,
  AVATAR = "head13",
  SHIP = "ship37",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 103,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1003,
  AVATAR = "head13",
  SHIP = "ship37",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 103,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1003,
  AVATAR = "head13",
  SHIP = "ship37",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 103,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1003,
  AVATAR = "head13",
  SHIP = "ship37",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 103,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1003,
  AVATAR = "head13",
  SHIP = "ship37",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 103,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1003,
  AVATAR = "head13",
  SHIP = "ship37",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 103,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1003,
  AVATAR = "head13",
  SHIP = "ship37",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 103,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1003,
  AVATAR = "head13",
  SHIP = "ship37",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 103,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1003,
  AVATAR = "head13",
  SHIP = "ship37",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 103,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1003,
  AVATAR = "head13",
  SHIP = "ship37",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 104,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1004,
  AVATAR = "head13",
  SHIP = "ship37",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 104,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1004,
  AVATAR = "head13",
  SHIP = "ship37",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 104,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1004,
  AVATAR = "head13",
  SHIP = "ship37",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 104,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1004,
  AVATAR = "head13",
  SHIP = "ship37",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 104,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1004,
  AVATAR = "head13",
  SHIP = "ship37",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 104,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1004,
  AVATAR = "head13",
  SHIP = "ship37",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 104,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1004,
  AVATAR = "head13",
  SHIP = "ship37",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 104,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1004,
  AVATAR = "head13",
  SHIP = "ship37",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 104,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1004,
  AVATAR = "head13",
  SHIP = "ship37",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 104,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1004,
  AVATAR = "head13",
  SHIP = "ship37",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 104,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1004,
  AVATAR = "head13",
  SHIP = "ship37",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 104,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1004,
  AVATAR = "head13",
  SHIP = "ship37",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 104,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1004,
  AVATAR = "head13",
  SHIP = "ship37",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 104,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1004,
  AVATAR = "head13",
  SHIP = "ship37",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 104,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1004,
  AVATAR = "head13",
  SHIP = "ship37",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 104,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1004,
  AVATAR = "head13",
  SHIP = "ship37",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 104,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1004,
  AVATAR = "head13",
  SHIP = "ship37",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 105,
  LEVEL = 0,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 501,
  AVATAR = "head13",
  SHIP = "ship37",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 105,
  LEVEL = 1,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 501,
  AVATAR = "head13",
  SHIP = "ship37",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 105,
  LEVEL = 2,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 501,
  AVATAR = "head13",
  SHIP = "ship37",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 105,
  LEVEL = 3,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 501,
  AVATAR = "head13",
  SHIP = "ship37",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 105,
  LEVEL = 4,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 501,
  AVATAR = "head13",
  SHIP = "ship37",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 105,
  LEVEL = 5,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 501,
  AVATAR = "head13",
  SHIP = "ship37",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 105,
  LEVEL = 6,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 501,
  AVATAR = "head13",
  SHIP = "ship37",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 105,
  LEVEL = 7,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 501,
  AVATAR = "head13",
  SHIP = "ship37",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 105,
  LEVEL = 8,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 501,
  AVATAR = "head13",
  SHIP = "ship37",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 105,
  LEVEL = 9,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 501,
  AVATAR = "head13",
  SHIP = "ship37",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 105,
  LEVEL = 10,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 501,
  AVATAR = "head13",
  SHIP = "ship37",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 105,
  LEVEL = 11,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 501,
  AVATAR = "head13",
  SHIP = "ship37",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 105,
  LEVEL = 12,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 501,
  AVATAR = "head13",
  SHIP = "ship37",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 105,
  LEVEL = 13,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 501,
  AVATAR = "head13",
  SHIP = "ship37",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 105,
  LEVEL = 14,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 501,
  AVATAR = "head13",
  SHIP = "ship37",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 105,
  LEVEL = 15,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 501,
  AVATAR = "head13",
  SHIP = "ship37",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 105,
  LEVEL = 16,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 501,
  AVATAR = "head13",
  SHIP = "ship37",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 106,
  LEVEL = 0,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 502,
  AVATAR = "head13",
  SHIP = "ship2",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 106,
  LEVEL = 1,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 502,
  AVATAR = "head13",
  SHIP = "ship2",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 106,
  LEVEL = 2,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 502,
  AVATAR = "head13",
  SHIP = "ship2",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 106,
  LEVEL = 3,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 502,
  AVATAR = "head13",
  SHIP = "ship2",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 106,
  LEVEL = 4,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 502,
  AVATAR = "head13",
  SHIP = "ship2",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 106,
  LEVEL = 5,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 502,
  AVATAR = "head13",
  SHIP = "ship2",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 106,
  LEVEL = 6,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 502,
  AVATAR = "head13",
  SHIP = "ship2",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 106,
  LEVEL = 7,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 502,
  AVATAR = "head13",
  SHIP = "ship2",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 106,
  LEVEL = 8,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 502,
  AVATAR = "head13",
  SHIP = "ship2",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 106,
  LEVEL = 9,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 502,
  AVATAR = "head13",
  SHIP = "ship2",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 106,
  LEVEL = 10,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 502,
  AVATAR = "head13",
  SHIP = "ship2",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 106,
  LEVEL = 11,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 502,
  AVATAR = "head13",
  SHIP = "ship2",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 106,
  LEVEL = 12,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 502,
  AVATAR = "head13",
  SHIP = "ship2",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 106,
  LEVEL = 13,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 502,
  AVATAR = "head13",
  SHIP = "ship2",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 106,
  LEVEL = 14,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 502,
  AVATAR = "head13",
  SHIP = "ship2",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 106,
  LEVEL = 15,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 502,
  AVATAR = "head13",
  SHIP = "ship2",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 106,
  LEVEL = 16,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 502,
  AVATAR = "head13",
  SHIP = "ship2",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 111,
  LEVEL = 0,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 29,
  AVATAR = "head14",
  SHIP = "ship41",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 111,
  LEVEL = 1,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 29,
  AVATAR = "head14",
  SHIP = "ship41",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 111,
  LEVEL = 2,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 29,
  AVATAR = "head14",
  SHIP = "ship41",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 111,
  LEVEL = 3,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 29,
  AVATAR = "head14",
  SHIP = "ship41",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 111,
  LEVEL = 4,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 29,
  AVATAR = "head14",
  SHIP = "ship41",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 111,
  LEVEL = 5,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 29,
  AVATAR = "head14",
  SHIP = "ship41",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 111,
  LEVEL = 6,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 29,
  AVATAR = "head14",
  SHIP = "ship41",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 111,
  LEVEL = 7,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 29,
  AVATAR = "head14",
  SHIP = "ship41",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 111,
  LEVEL = 8,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 29,
  AVATAR = "head14",
  SHIP = "ship41",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 111,
  LEVEL = 9,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 29,
  AVATAR = "head14",
  SHIP = "ship41",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 111,
  LEVEL = 10,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 29,
  AVATAR = "head14",
  SHIP = "ship41",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 111,
  LEVEL = 11,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 29,
  AVATAR = "head14",
  SHIP = "ship41",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 111,
  LEVEL = 12,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 29,
  AVATAR = "head14",
  SHIP = "ship41",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 111,
  LEVEL = 13,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 29,
  AVATAR = "head14",
  SHIP = "ship41",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 111,
  LEVEL = 14,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 29,
  AVATAR = "head14",
  SHIP = "ship41",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 111,
  LEVEL = 15,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 29,
  AVATAR = "head14",
  SHIP = "ship41",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 111,
  LEVEL = 16,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 29,
  AVATAR = "head14",
  SHIP = "ship41",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 112,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 30,
  AVATAR = "head22",
  SHIP = "ship42",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 112,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 30,
  AVATAR = "head22",
  SHIP = "ship42",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 112,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 30,
  AVATAR = "head22",
  SHIP = "ship42",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 112,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 30,
  AVATAR = "head22",
  SHIP = "ship42",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 112,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 30,
  AVATAR = "head22",
  SHIP = "ship42",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 112,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 30,
  AVATAR = "head22",
  SHIP = "ship42",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 112,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 30,
  AVATAR = "head22",
  SHIP = "ship42",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 112,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 30,
  AVATAR = "head22",
  SHIP = "ship42",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 112,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 30,
  AVATAR = "head22",
  SHIP = "ship42",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 112,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 30,
  AVATAR = "head22",
  SHIP = "ship42",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 112,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 30,
  AVATAR = "head22",
  SHIP = "ship42",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 112,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 30,
  AVATAR = "head22",
  SHIP = "ship42",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 112,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 30,
  AVATAR = "head22",
  SHIP = "ship42",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 112,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 30,
  AVATAR = "head22",
  SHIP = "ship42",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 112,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 30,
  AVATAR = "head22",
  SHIP = "ship42",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 112,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 30,
  AVATAR = "head22",
  SHIP = "ship42",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 112,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 30,
  AVATAR = "head22",
  SHIP = "ship42",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 113,
  LEVEL = 0,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 31,
  AVATAR = "head16",
  SHIP = "ship43",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 113,
  LEVEL = 1,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 31,
  AVATAR = "head16",
  SHIP = "ship43",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 113,
  LEVEL = 2,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 31,
  AVATAR = "head16",
  SHIP = "ship43",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 113,
  LEVEL = 3,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 31,
  AVATAR = "head16",
  SHIP = "ship43",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 113,
  LEVEL = 4,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 31,
  AVATAR = "head16",
  SHIP = "ship43",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 113,
  LEVEL = 5,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 31,
  AVATAR = "head16",
  SHIP = "ship43",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 113,
  LEVEL = 6,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 31,
  AVATAR = "head16",
  SHIP = "ship43",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 113,
  LEVEL = 7,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 31,
  AVATAR = "head16",
  SHIP = "ship43",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 113,
  LEVEL = 8,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 31,
  AVATAR = "head16",
  SHIP = "ship43",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 113,
  LEVEL = 9,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 31,
  AVATAR = "head16",
  SHIP = "ship43",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 113,
  LEVEL = 10,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 31,
  AVATAR = "head16",
  SHIP = "ship43",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 113,
  LEVEL = 11,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 31,
  AVATAR = "head16",
  SHIP = "ship43",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 113,
  LEVEL = 12,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 31,
  AVATAR = "head16",
  SHIP = "ship43",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 113,
  LEVEL = 13,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 31,
  AVATAR = "head16",
  SHIP = "ship43",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 113,
  LEVEL = 14,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 31,
  AVATAR = "head16",
  SHIP = "ship43",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 113,
  LEVEL = 15,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 31,
  AVATAR = "head16",
  SHIP = "ship43",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 113,
  LEVEL = 16,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 31,
  AVATAR = "head16",
  SHIP = "ship43",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 114,
  LEVEL = 0,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 32,
  AVATAR = "head27",
  SHIP = "ship47",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 114,
  LEVEL = 1,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 32,
  AVATAR = "head27",
  SHIP = "ship47",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 114,
  LEVEL = 2,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 32,
  AVATAR = "head27",
  SHIP = "ship47",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 114,
  LEVEL = 3,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 32,
  AVATAR = "head27",
  SHIP = "ship47",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 114,
  LEVEL = 4,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 32,
  AVATAR = "head27",
  SHIP = "ship47",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 114,
  LEVEL = 5,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 32,
  AVATAR = "head27",
  SHIP = "ship47",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 114,
  LEVEL = 6,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 32,
  AVATAR = "head27",
  SHIP = "ship47",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 114,
  LEVEL = 7,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 32,
  AVATAR = "head27",
  SHIP = "ship47",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 114,
  LEVEL = 8,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 32,
  AVATAR = "head27",
  SHIP = "ship47",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 114,
  LEVEL = 9,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 32,
  AVATAR = "head27",
  SHIP = "ship47",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 114,
  LEVEL = 10,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 32,
  AVATAR = "head27",
  SHIP = "ship47",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 114,
  LEVEL = 11,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 32,
  AVATAR = "head27",
  SHIP = "ship47",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 114,
  LEVEL = 12,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 32,
  AVATAR = "head27",
  SHIP = "ship47",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 114,
  LEVEL = 13,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 32,
  AVATAR = "head27",
  SHIP = "ship47",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 114,
  LEVEL = 14,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 32,
  AVATAR = "head27",
  SHIP = "ship47",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 114,
  LEVEL = 15,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 32,
  AVATAR = "head27",
  SHIP = "ship47",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 114,
  LEVEL = 16,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 32,
  AVATAR = "head27",
  SHIP = "ship47",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 115,
  LEVEL = 0,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 28,
  AVATAR = "head1",
  SHIP = "ship48",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 115,
  LEVEL = 1,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 28,
  AVATAR = "head1",
  SHIP = "ship48",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 115,
  LEVEL = 2,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 28,
  AVATAR = "head1",
  SHIP = "ship48",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 115,
  LEVEL = 3,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 28,
  AVATAR = "head1",
  SHIP = "ship48",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 115,
  LEVEL = 4,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 28,
  AVATAR = "head1",
  SHIP = "ship48",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 115,
  LEVEL = 5,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 28,
  AVATAR = "head1",
  SHIP = "ship48",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 115,
  LEVEL = 6,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 28,
  AVATAR = "head1",
  SHIP = "ship48",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 115,
  LEVEL = 7,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 28,
  AVATAR = "head1",
  SHIP = "ship48",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 115,
  LEVEL = 8,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 28,
  AVATAR = "head1",
  SHIP = "ship48",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 115,
  LEVEL = 9,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 28,
  AVATAR = "head1",
  SHIP = "ship48",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 115,
  LEVEL = 10,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 28,
  AVATAR = "head1",
  SHIP = "ship48",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 115,
  LEVEL = 11,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 28,
  AVATAR = "head1",
  SHIP = "ship48",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 115,
  LEVEL = 12,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 28,
  AVATAR = "head1",
  SHIP = "ship48",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 115,
  LEVEL = 13,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 28,
  AVATAR = "head1",
  SHIP = "ship48",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 115,
  LEVEL = 14,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 28,
  AVATAR = "head1",
  SHIP = "ship48",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 115,
  LEVEL = 15,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 28,
  AVATAR = "head1",
  SHIP = "ship48",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 115,
  LEVEL = 16,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 28,
  AVATAR = "head1",
  SHIP = "ship48",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 116,
  LEVEL = 0,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 30,
  AVATAR = "head21",
  SHIP = "ship47",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 116,
  LEVEL = 1,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 30,
  AVATAR = "head21",
  SHIP = "ship47",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 116,
  LEVEL = 2,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 30,
  AVATAR = "head21",
  SHIP = "ship47",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 116,
  LEVEL = 3,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 30,
  AVATAR = "head21",
  SHIP = "ship47",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 116,
  LEVEL = 4,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 30,
  AVATAR = "head21",
  SHIP = "ship47",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 116,
  LEVEL = 5,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 30,
  AVATAR = "head21",
  SHIP = "ship47",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 116,
  LEVEL = 6,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 30,
  AVATAR = "head21",
  SHIP = "ship47",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 116,
  LEVEL = 7,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 30,
  AVATAR = "head21",
  SHIP = "ship47",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 116,
  LEVEL = 8,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 30,
  AVATAR = "head21",
  SHIP = "ship47",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 116,
  LEVEL = 9,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 30,
  AVATAR = "head21",
  SHIP = "ship47",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 116,
  LEVEL = 10,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 30,
  AVATAR = "head21",
  SHIP = "ship47",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 116,
  LEVEL = 11,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 30,
  AVATAR = "head21",
  SHIP = "ship47",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 116,
  LEVEL = 12,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 30,
  AVATAR = "head21",
  SHIP = "ship47",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 116,
  LEVEL = 13,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 30,
  AVATAR = "head21",
  SHIP = "ship47",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 116,
  LEVEL = 14,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 30,
  AVATAR = "head21",
  SHIP = "ship47",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 116,
  LEVEL = 15,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 30,
  AVATAR = "head21",
  SHIP = "ship47",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 116,
  LEVEL = 16,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 30,
  AVATAR = "head21",
  SHIP = "ship47",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 117,
  LEVEL = 0,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 33,
  AVATAR = "head1001",
  SHIP = "ship1001",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 117,
  LEVEL = 1,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 33,
  AVATAR = "head1001",
  SHIP = "ship1001",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 117,
  LEVEL = 2,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 33,
  AVATAR = "head1001",
  SHIP = "ship1001",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 117,
  LEVEL = 3,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 33,
  AVATAR = "head1001",
  SHIP = "ship1001",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 117,
  LEVEL = 4,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 33,
  AVATAR = "head1001",
  SHIP = "ship1001",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 117,
  LEVEL = 5,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 33,
  AVATAR = "head1001",
  SHIP = "ship1001",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 117,
  LEVEL = 6,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 33,
  AVATAR = "head1001",
  SHIP = "ship1001",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 117,
  LEVEL = 7,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 33,
  AVATAR = "head1001",
  SHIP = "ship1001",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 117,
  LEVEL = 8,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 33,
  AVATAR = "head1001",
  SHIP = "ship1001",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 117,
  LEVEL = 9,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 33,
  AVATAR = "head1001",
  SHIP = "ship1001",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 117,
  LEVEL = 10,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 33,
  AVATAR = "head1001",
  SHIP = "ship1001",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 117,
  LEVEL = 11,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 33,
  AVATAR = "head1001",
  SHIP = "ship1001",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 117,
  LEVEL = 12,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 33,
  AVATAR = "head1001",
  SHIP = "ship1001",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 117,
  LEVEL = 13,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 33,
  AVATAR = "head1001",
  SHIP = "ship1001",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 117,
  LEVEL = 14,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 33,
  AVATAR = "head1001",
  SHIP = "ship1001",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 117,
  LEVEL = 15,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 33,
  AVATAR = "head1001",
  SHIP = "ship1001",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 117,
  LEVEL = 16,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 33,
  AVATAR = "head1001",
  SHIP = "ship1001",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 118,
  LEVEL = 0,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 10,
  AVATAR = "head48",
  SHIP = "ship51",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 118,
  LEVEL = 1,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 10,
  AVATAR = "head48",
  SHIP = "ship51",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 118,
  LEVEL = 2,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 10,
  AVATAR = "head48",
  SHIP = "ship51",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 118,
  LEVEL = 3,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 10,
  AVATAR = "head48",
  SHIP = "ship51",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 118,
  LEVEL = 4,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 10,
  AVATAR = "head48",
  SHIP = "ship51",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 118,
  LEVEL = 5,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 10,
  AVATAR = "head48",
  SHIP = "ship51",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 118,
  LEVEL = 6,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 10,
  AVATAR = "head48",
  SHIP = "ship51",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 118,
  LEVEL = 7,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 10,
  AVATAR = "head48",
  SHIP = "ship51",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 118,
  LEVEL = 8,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 10,
  AVATAR = "head48",
  SHIP = "ship51",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 118,
  LEVEL = 9,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 10,
  AVATAR = "head48",
  SHIP = "ship51",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 118,
  LEVEL = 10,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 10,
  AVATAR = "head48",
  SHIP = "ship51",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 118,
  LEVEL = 11,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 10,
  AVATAR = "head48",
  SHIP = "ship51",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 118,
  LEVEL = 12,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 10,
  AVATAR = "head48",
  SHIP = "ship51",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 118,
  LEVEL = 13,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 10,
  AVATAR = "head48",
  SHIP = "ship51",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 118,
  LEVEL = 14,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 10,
  AVATAR = "head48",
  SHIP = "ship51",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 118,
  LEVEL = 15,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 10,
  AVATAR = "head48",
  SHIP = "ship51",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 118,
  LEVEL = 16,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 10,
  AVATAR = "head48",
  SHIP = "ship51",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 119,
  LEVEL = 0,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 1003,
  AVATAR = "head49",
  SHIP = "ship52",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 119,
  LEVEL = 1,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 1003,
  AVATAR = "head49",
  SHIP = "ship52",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 119,
  LEVEL = 2,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 1003,
  AVATAR = "head49",
  SHIP = "ship52",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 119,
  LEVEL = 3,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 1003,
  AVATAR = "head49",
  SHIP = "ship52",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 119,
  LEVEL = 4,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 1003,
  AVATAR = "head49",
  SHIP = "ship52",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 119,
  LEVEL = 5,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 1003,
  AVATAR = "head49",
  SHIP = "ship52",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 119,
  LEVEL = 6,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 1003,
  AVATAR = "head49",
  SHIP = "ship52",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 119,
  LEVEL = 7,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 1003,
  AVATAR = "head49",
  SHIP = "ship52",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 119,
  LEVEL = 8,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 1003,
  AVATAR = "head49",
  SHIP = "ship52",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 119,
  LEVEL = 9,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 1003,
  AVATAR = "head49",
  SHIP = "ship52",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 119,
  LEVEL = 10,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 1003,
  AVATAR = "head49",
  SHIP = "ship52",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 119,
  LEVEL = 11,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 1003,
  AVATAR = "head49",
  SHIP = "ship52",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 119,
  LEVEL = 12,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 1003,
  AVATAR = "head49",
  SHIP = "ship52",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 119,
  LEVEL = 13,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 1003,
  AVATAR = "head49",
  SHIP = "ship52",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 119,
  LEVEL = 14,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 1003,
  AVATAR = "head49",
  SHIP = "ship52",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 119,
  LEVEL = 15,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 1003,
  AVATAR = "head49",
  SHIP = "ship52",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 119,
  LEVEL = 16,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 1003,
  AVATAR = "head49",
  SHIP = "ship52",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 120,
  LEVEL = 0,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 36,
  AVATAR = "head50",
  SHIP = "ship53",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 120,
  LEVEL = 1,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 36,
  AVATAR = "head50",
  SHIP = "ship53",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 120,
  LEVEL = 2,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 36,
  AVATAR = "head50",
  SHIP = "ship53",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 120,
  LEVEL = 3,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 36,
  AVATAR = "head50",
  SHIP = "ship53",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 120,
  LEVEL = 4,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 36,
  AVATAR = "head50",
  SHIP = "ship53",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 120,
  LEVEL = 5,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 36,
  AVATAR = "head50",
  SHIP = "ship53",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 120,
  LEVEL = 6,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 36,
  AVATAR = "head50",
  SHIP = "ship53",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 120,
  LEVEL = 7,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 36,
  AVATAR = "head50",
  SHIP = "ship53",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 120,
  LEVEL = 8,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 36,
  AVATAR = "head50",
  SHIP = "ship53",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 120,
  LEVEL = 9,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 36,
  AVATAR = "head50",
  SHIP = "ship53",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 120,
  LEVEL = 10,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 36,
  AVATAR = "head50",
  SHIP = "ship53",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 120,
  LEVEL = 11,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 36,
  AVATAR = "head50",
  SHIP = "ship53",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 120,
  LEVEL = 12,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 36,
  AVATAR = "head50",
  SHIP = "ship53",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 120,
  LEVEL = 13,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 36,
  AVATAR = "head50",
  SHIP = "ship53",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 120,
  LEVEL = 14,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 36,
  AVATAR = "head50",
  SHIP = "ship53",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 120,
  LEVEL = 15,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 36,
  AVATAR = "head50",
  SHIP = "ship53",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 120,
  LEVEL = 16,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 36,
  AVATAR = "head50",
  SHIP = "ship53",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 121,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 37,
  AVATAR = "head51",
  SHIP = "ship54",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 121,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 37,
  AVATAR = "head51",
  SHIP = "ship54",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 121,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 37,
  AVATAR = "head51",
  SHIP = "ship54",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 121,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 37,
  AVATAR = "head51",
  SHIP = "ship54",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 121,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 37,
  AVATAR = "head51",
  SHIP = "ship54",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 121,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 37,
  AVATAR = "head51",
  SHIP = "ship54",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 121,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 37,
  AVATAR = "head51",
  SHIP = "ship54",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 121,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 37,
  AVATAR = "head51",
  SHIP = "ship54",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 121,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 37,
  AVATAR = "head51",
  SHIP = "ship54",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 121,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 37,
  AVATAR = "head51",
  SHIP = "ship54",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 121,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 37,
  AVATAR = "head51",
  SHIP = "ship54",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 121,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 37,
  AVATAR = "head51",
  SHIP = "ship54",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 121,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 37,
  AVATAR = "head51",
  SHIP = "ship54",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 121,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 37,
  AVATAR = "head51",
  SHIP = "ship54",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 121,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 37,
  AVATAR = "head51",
  SHIP = "ship54",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 121,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 37,
  AVATAR = "head51",
  SHIP = "ship54",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 121,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 37,
  AVATAR = "head51",
  SHIP = "ship54",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 122,
  LEVEL = 0,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 38,
  AVATAR = "head52",
  SHIP = "ship55",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 122,
  LEVEL = 1,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 38,
  AVATAR = "head52",
  SHIP = "ship55",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 122,
  LEVEL = 2,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 38,
  AVATAR = "head52",
  SHIP = "ship55",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 122,
  LEVEL = 3,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 38,
  AVATAR = "head52",
  SHIP = "ship55",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 122,
  LEVEL = 4,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 38,
  AVATAR = "head52",
  SHIP = "ship55",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 122,
  LEVEL = 5,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 38,
  AVATAR = "head52",
  SHIP = "ship55",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 122,
  LEVEL = 6,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 38,
  AVATAR = "head52",
  SHIP = "ship55",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 122,
  LEVEL = 7,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 38,
  AVATAR = "head52",
  SHIP = "ship55",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 122,
  LEVEL = 8,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 38,
  AVATAR = "head52",
  SHIP = "ship55",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 122,
  LEVEL = 9,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 38,
  AVATAR = "head52",
  SHIP = "ship55",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 122,
  LEVEL = 10,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 38,
  AVATAR = "head52",
  SHIP = "ship55",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 122,
  LEVEL = 11,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 38,
  AVATAR = "head52",
  SHIP = "ship55",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 122,
  LEVEL = 12,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 38,
  AVATAR = "head52",
  SHIP = "ship55",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 122,
  LEVEL = 13,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 38,
  AVATAR = "head52",
  SHIP = "ship55",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 122,
  LEVEL = 14,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 38,
  AVATAR = "head52",
  SHIP = "ship55",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 122,
  LEVEL = 15,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 38,
  AVATAR = "head52",
  SHIP = "ship55",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 122,
  LEVEL = 16,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 38,
  AVATAR = "head52",
  SHIP = "ship55",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 123,
  LEVEL = 0,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 39,
  AVATAR = "head53",
  SHIP = "ship56",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 123,
  LEVEL = 1,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 39,
  AVATAR = "head53",
  SHIP = "ship56",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 123,
  LEVEL = 2,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 39,
  AVATAR = "head53",
  SHIP = "ship56",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 123,
  LEVEL = 3,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 39,
  AVATAR = "head53",
  SHIP = "ship56",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 123,
  LEVEL = 4,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 39,
  AVATAR = "head53",
  SHIP = "ship56",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 123,
  LEVEL = 5,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 39,
  AVATAR = "head53",
  SHIP = "ship56",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 123,
  LEVEL = 6,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 39,
  AVATAR = "head53",
  SHIP = "ship56",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 123,
  LEVEL = 7,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 39,
  AVATAR = "head53",
  SHIP = "ship56",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 123,
  LEVEL = 8,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 39,
  AVATAR = "head53",
  SHIP = "ship56",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 123,
  LEVEL = 9,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 39,
  AVATAR = "head53",
  SHIP = "ship56",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 123,
  LEVEL = 10,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 39,
  AVATAR = "head53",
  SHIP = "ship56",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 123,
  LEVEL = 11,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 39,
  AVATAR = "head53",
  SHIP = "ship56",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 123,
  LEVEL = 12,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 39,
  AVATAR = "head53",
  SHIP = "ship56",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 123,
  LEVEL = 13,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 39,
  AVATAR = "head53",
  SHIP = "ship56",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 123,
  LEVEL = 14,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 39,
  AVATAR = "head53",
  SHIP = "ship56",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 123,
  LEVEL = 15,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 39,
  AVATAR = "head53",
  SHIP = "ship56",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 123,
  LEVEL = 16,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 39,
  AVATAR = "head53",
  SHIP = "ship56",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 124,
  LEVEL = 0,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 40,
  AVATAR = "head54",
  SHIP = "ship59",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 124,
  LEVEL = 1,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 40,
  AVATAR = "head54",
  SHIP = "ship59",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 124,
  LEVEL = 2,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 40,
  AVATAR = "head54",
  SHIP = "ship59",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 124,
  LEVEL = 3,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 40,
  AVATAR = "head54",
  SHIP = "ship59",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 124,
  LEVEL = 4,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 40,
  AVATAR = "head54",
  SHIP = "ship59",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 124,
  LEVEL = 5,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 40,
  AVATAR = "head54",
  SHIP = "ship59",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 124,
  LEVEL = 6,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 40,
  AVATAR = "head54",
  SHIP = "ship59",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 124,
  LEVEL = 7,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 40,
  AVATAR = "head54",
  SHIP = "ship59",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 124,
  LEVEL = 8,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 40,
  AVATAR = "head54",
  SHIP = "ship59",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 124,
  LEVEL = 9,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 40,
  AVATAR = "head54",
  SHIP = "ship59",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 124,
  LEVEL = 10,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 40,
  AVATAR = "head54",
  SHIP = "ship59",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 124,
  LEVEL = 11,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 40,
  AVATAR = "head54",
  SHIP = "ship59",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 124,
  LEVEL = 12,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 40,
  AVATAR = "head54",
  SHIP = "ship59",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 124,
  LEVEL = 13,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 40,
  AVATAR = "head54",
  SHIP = "ship59",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 124,
  LEVEL = 14,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 40,
  AVATAR = "head54",
  SHIP = "ship59",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 124,
  LEVEL = 15,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 40,
  AVATAR = "head54",
  SHIP = "ship59",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 124,
  LEVEL = 16,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 40,
  AVATAR = "head54",
  SHIP = "ship59",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 125,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 41,
  AVATAR = "head55",
  SHIP = "ship58",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 125,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 41,
  AVATAR = "head55",
  SHIP = "ship58",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 125,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 41,
  AVATAR = "head55",
  SHIP = "ship58",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 125,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 41,
  AVATAR = "head55",
  SHIP = "ship58",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 125,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 41,
  AVATAR = "head55",
  SHIP = "ship58",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 125,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 41,
  AVATAR = "head55",
  SHIP = "ship58",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 125,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 41,
  AVATAR = "head55",
  SHIP = "ship58",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 125,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 41,
  AVATAR = "head55",
  SHIP = "ship58",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 125,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 41,
  AVATAR = "head55",
  SHIP = "ship58",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 125,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 41,
  AVATAR = "head55",
  SHIP = "ship58",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 125,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 41,
  AVATAR = "head55",
  SHIP = "ship58",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 125,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 41,
  AVATAR = "head55",
  SHIP = "ship58",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 125,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 41,
  AVATAR = "head55",
  SHIP = "ship58",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 125,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 41,
  AVATAR = "head55",
  SHIP = "ship58",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 125,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 41,
  AVATAR = "head55",
  SHIP = "ship58",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 125,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 41,
  AVATAR = "head55",
  SHIP = "ship58",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 125,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 41,
  AVATAR = "head55",
  SHIP = "ship58",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 126,
  LEVEL = 0,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 42,
  AVATAR = "head56",
  SHIP = "ship57",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 126,
  LEVEL = 1,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 42,
  AVATAR = "head56",
  SHIP = "ship57",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 126,
  LEVEL = 2,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 42,
  AVATAR = "head56",
  SHIP = "ship57",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 126,
  LEVEL = 3,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 42,
  AVATAR = "head56",
  SHIP = "ship57",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 126,
  LEVEL = 4,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 42,
  AVATAR = "head56",
  SHIP = "ship57",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 126,
  LEVEL = 5,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 42,
  AVATAR = "head56",
  SHIP = "ship57",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 126,
  LEVEL = 6,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 42,
  AVATAR = "head56",
  SHIP = "ship57",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 126,
  LEVEL = 7,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 42,
  AVATAR = "head56",
  SHIP = "ship57",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 126,
  LEVEL = 8,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 42,
  AVATAR = "head56",
  SHIP = "ship57",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 126,
  LEVEL = 9,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 42,
  AVATAR = "head56",
  SHIP = "ship57",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 126,
  LEVEL = 10,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 42,
  AVATAR = "head56",
  SHIP = "ship57",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 126,
  LEVEL = 11,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 42,
  AVATAR = "head56",
  SHIP = "ship57",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 126,
  LEVEL = 12,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 42,
  AVATAR = "head56",
  SHIP = "ship57",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 126,
  LEVEL = 13,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 42,
  AVATAR = "head56",
  SHIP = "ship57",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 126,
  LEVEL = 14,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 42,
  AVATAR = "head56",
  SHIP = "ship57",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 126,
  LEVEL = 15,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 42,
  AVATAR = "head56",
  SHIP = "ship57",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 126,
  LEVEL = 16,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 42,
  AVATAR = "head56",
  SHIP = "ship57",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 127,
  LEVEL = 0,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 43,
  AVATAR = "head57",
  SHIP = "ship60",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 127,
  LEVEL = 1,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 43,
  AVATAR = "head57",
  SHIP = "ship60",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 127,
  LEVEL = 2,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 43,
  AVATAR = "head57",
  SHIP = "ship60",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 127,
  LEVEL = 3,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 43,
  AVATAR = "head57",
  SHIP = "ship60",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 127,
  LEVEL = 4,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 43,
  AVATAR = "head57",
  SHIP = "ship60",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 127,
  LEVEL = 5,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 43,
  AVATAR = "head57",
  SHIP = "ship60",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 127,
  LEVEL = 6,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 43,
  AVATAR = "head57",
  SHIP = "ship60",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 127,
  LEVEL = 7,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 43,
  AVATAR = "head57",
  SHIP = "ship60",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 127,
  LEVEL = 8,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 43,
  AVATAR = "head57",
  SHIP = "ship60",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 127,
  LEVEL = 9,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 43,
  AVATAR = "head57",
  SHIP = "ship60",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 127,
  LEVEL = 10,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 43,
  AVATAR = "head57",
  SHIP = "ship60",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 127,
  LEVEL = 11,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 43,
  AVATAR = "head57",
  SHIP = "ship60",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 127,
  LEVEL = 12,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 43,
  AVATAR = "head57",
  SHIP = "ship60",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 127,
  LEVEL = 13,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 43,
  AVATAR = "head57",
  SHIP = "ship60",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 127,
  LEVEL = 14,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 43,
  AVATAR = "head57",
  SHIP = "ship60",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 127,
  LEVEL = 15,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 43,
  AVATAR = "head57",
  SHIP = "ship60",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 127,
  LEVEL = 16,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 43,
  AVATAR = "head57",
  SHIP = "ship60",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 128,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 44,
  AVATAR = "head58",
  SHIP = "ship61",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 128,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 44,
  AVATAR = "head58",
  SHIP = "ship61",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 128,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 44,
  AVATAR = "head58",
  SHIP = "ship61",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 128,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 44,
  AVATAR = "head58",
  SHIP = "ship61",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 128,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 44,
  AVATAR = "head58",
  SHIP = "ship61",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 128,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 44,
  AVATAR = "head58",
  SHIP = "ship61",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 128,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 44,
  AVATAR = "head58",
  SHIP = "ship61",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 128,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 44,
  AVATAR = "head58",
  SHIP = "ship61",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 128,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 44,
  AVATAR = "head58",
  SHIP = "ship61",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 128,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 44,
  AVATAR = "head58",
  SHIP = "ship61",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 128,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 44,
  AVATAR = "head58",
  SHIP = "ship61",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 128,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 44,
  AVATAR = "head58",
  SHIP = "ship61",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 128,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 44,
  AVATAR = "head58",
  SHIP = "ship61",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 128,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 44,
  AVATAR = "head58",
  SHIP = "ship61",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 128,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 44,
  AVATAR = "head58",
  SHIP = "ship61",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 128,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 44,
  AVATAR = "head58",
  SHIP = "ship61",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 128,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 44,
  AVATAR = "head58",
  SHIP = "ship61",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 129,
  LEVEL = 0,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 45,
  AVATAR = "head59",
  SHIP = "ship62",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 129,
  LEVEL = 1,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 45,
  AVATAR = "head59",
  SHIP = "ship62",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 129,
  LEVEL = 2,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 45,
  AVATAR = "head59",
  SHIP = "ship62",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 129,
  LEVEL = 3,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 45,
  AVATAR = "head59",
  SHIP = "ship62",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 129,
  LEVEL = 4,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 45,
  AVATAR = "head59",
  SHIP = "ship62",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 129,
  LEVEL = 5,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 45,
  AVATAR = "head59",
  SHIP = "ship62",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 129,
  LEVEL = 6,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 45,
  AVATAR = "head59",
  SHIP = "ship62",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 129,
  LEVEL = 7,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 45,
  AVATAR = "head59",
  SHIP = "ship62",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 129,
  LEVEL = 8,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 45,
  AVATAR = "head59",
  SHIP = "ship62",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 129,
  LEVEL = 9,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 45,
  AVATAR = "head59",
  SHIP = "ship62",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 129,
  LEVEL = 10,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 45,
  AVATAR = "head59",
  SHIP = "ship62",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 129,
  LEVEL = 11,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 45,
  AVATAR = "head59",
  SHIP = "ship62",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 129,
  LEVEL = 12,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 45,
  AVATAR = "head59",
  SHIP = "ship62",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 129,
  LEVEL = 13,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 45,
  AVATAR = "head59",
  SHIP = "ship62",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 129,
  LEVEL = 14,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 45,
  AVATAR = "head59",
  SHIP = "ship62",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 129,
  LEVEL = 15,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 45,
  AVATAR = "head59",
  SHIP = "ship62",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 129,
  LEVEL = 16,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 45,
  AVATAR = "head59",
  SHIP = "ship62",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 130,
  LEVEL = 0,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 46,
  AVATAR = "head60",
  SHIP = "ship63",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 130,
  LEVEL = 1,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 46,
  AVATAR = "head60",
  SHIP = "ship63",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 130,
  LEVEL = 2,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 46,
  AVATAR = "head60",
  SHIP = "ship63",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 130,
  LEVEL = 3,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 46,
  AVATAR = "head60",
  SHIP = "ship63",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 130,
  LEVEL = 4,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 46,
  AVATAR = "head60",
  SHIP = "ship63",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 130,
  LEVEL = 5,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 46,
  AVATAR = "head60",
  SHIP = "ship63",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 130,
  LEVEL = 6,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 46,
  AVATAR = "head60",
  SHIP = "ship63",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 130,
  LEVEL = 7,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 46,
  AVATAR = "head60",
  SHIP = "ship63",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 130,
  LEVEL = 8,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 46,
  AVATAR = "head60",
  SHIP = "ship63",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 130,
  LEVEL = 9,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 46,
  AVATAR = "head60",
  SHIP = "ship63",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 130,
  LEVEL = 10,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 46,
  AVATAR = "head60",
  SHIP = "ship63",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 130,
  LEVEL = 11,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 46,
  AVATAR = "head60",
  SHIP = "ship63",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 130,
  LEVEL = 12,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 46,
  AVATAR = "head60",
  SHIP = "ship63",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 130,
  LEVEL = 13,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 46,
  AVATAR = "head60",
  SHIP = "ship63",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 130,
  LEVEL = 14,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 46,
  AVATAR = "head60",
  SHIP = "ship63",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 130,
  LEVEL = 15,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 46,
  AVATAR = "head60",
  SHIP = "ship63",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 130,
  LEVEL = 16,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 46,
  AVATAR = "head60",
  SHIP = "ship63",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 131,
  LEVEL = 0,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 47,
  AVATAR = "head61",
  SHIP = "ship64",
  sex = 2,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 131,
  LEVEL = 1,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 47,
  AVATAR = "head61",
  SHIP = "ship64",
  sex = 2,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 131,
  LEVEL = 2,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 47,
  AVATAR = "head61",
  SHIP = "ship64",
  sex = 2,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 131,
  LEVEL = 3,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 47,
  AVATAR = "head61",
  SHIP = "ship64",
  sex = 2,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 131,
  LEVEL = 4,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 47,
  AVATAR = "head61",
  SHIP = "ship64",
  sex = 2,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 131,
  LEVEL = 5,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 47,
  AVATAR = "head61",
  SHIP = "ship64",
  sex = 2,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 131,
  LEVEL = 6,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 47,
  AVATAR = "head61",
  SHIP = "ship64",
  sex = 2,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 131,
  LEVEL = 7,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 47,
  AVATAR = "head61",
  SHIP = "ship64",
  sex = 2,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 131,
  LEVEL = 8,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 47,
  AVATAR = "head61",
  SHIP = "ship64",
  sex = 2,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 131,
  LEVEL = 9,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 47,
  AVATAR = "head61",
  SHIP = "ship64",
  sex = 2,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 131,
  LEVEL = 10,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 47,
  AVATAR = "head61",
  SHIP = "ship64",
  sex = 2,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 131,
  LEVEL = 11,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 47,
  AVATAR = "head61",
  SHIP = "ship64",
  sex = 2,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 131,
  LEVEL = 12,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 47,
  AVATAR = "head61",
  SHIP = "ship64",
  sex = 2,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 131,
  LEVEL = 13,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 47,
  AVATAR = "head61",
  SHIP = "ship64",
  sex = 2,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 131,
  LEVEL = 14,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 47,
  AVATAR = "head61",
  SHIP = "ship64",
  sex = 2,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 131,
  LEVEL = 15,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 47,
  AVATAR = "head61",
  SHIP = "ship64",
  sex = 2,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 131,
  LEVEL = 16,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 47,
  AVATAR = "head61",
  SHIP = "ship64",
  sex = 2,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 132,
  LEVEL = 0,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 48,
  AVATAR = "head62",
  SHIP = "ship65",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 132,
  LEVEL = 1,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 48,
  AVATAR = "head62",
  SHIP = "ship65",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 132,
  LEVEL = 2,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 48,
  AVATAR = "head62",
  SHIP = "ship65",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 132,
  LEVEL = 3,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 48,
  AVATAR = "head62",
  SHIP = "ship65",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 132,
  LEVEL = 4,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 48,
  AVATAR = "head62",
  SHIP = "ship65",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 132,
  LEVEL = 5,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 48,
  AVATAR = "head62",
  SHIP = "ship65",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 132,
  LEVEL = 6,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 48,
  AVATAR = "head62",
  SHIP = "ship65",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 132,
  LEVEL = 7,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 48,
  AVATAR = "head62",
  SHIP = "ship65",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 132,
  LEVEL = 8,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 48,
  AVATAR = "head62",
  SHIP = "ship65",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 132,
  LEVEL = 9,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 48,
  AVATAR = "head62",
  SHIP = "ship65",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 132,
  LEVEL = 10,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 48,
  AVATAR = "head62",
  SHIP = "ship65",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 132,
  LEVEL = 11,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 48,
  AVATAR = "head62",
  SHIP = "ship65",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 132,
  LEVEL = 12,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 48,
  AVATAR = "head62",
  SHIP = "ship65",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 132,
  LEVEL = 13,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 48,
  AVATAR = "head62",
  SHIP = "ship65",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 132,
  LEVEL = 14,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 48,
  AVATAR = "head62",
  SHIP = "ship65",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 132,
  LEVEL = 15,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 48,
  AVATAR = "head62",
  SHIP = "ship65",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 132,
  LEVEL = 16,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 48,
  AVATAR = "head62",
  SHIP = "ship65",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 133,
  LEVEL = 0,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 49,
  AVATAR = "head63",
  SHIP = "ship66",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 133,
  LEVEL = 1,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 49,
  AVATAR = "head63",
  SHIP = "ship66",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 133,
  LEVEL = 2,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 49,
  AVATAR = "head63",
  SHIP = "ship66",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 133,
  LEVEL = 3,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 49,
  AVATAR = "head63",
  SHIP = "ship66",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 133,
  LEVEL = 4,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 49,
  AVATAR = "head63",
  SHIP = "ship66",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 133,
  LEVEL = 5,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 49,
  AVATAR = "head63",
  SHIP = "ship66",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 133,
  LEVEL = 6,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 49,
  AVATAR = "head63",
  SHIP = "ship66",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 133,
  LEVEL = 7,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 49,
  AVATAR = "head63",
  SHIP = "ship66",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 133,
  LEVEL = 8,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 49,
  AVATAR = "head63",
  SHIP = "ship66",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 133,
  LEVEL = 9,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 49,
  AVATAR = "head63",
  SHIP = "ship66",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 133,
  LEVEL = 10,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 49,
  AVATAR = "head63",
  SHIP = "ship66",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 133,
  LEVEL = 11,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 49,
  AVATAR = "head63",
  SHIP = "ship66",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 133,
  LEVEL = 12,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 49,
  AVATAR = "head63",
  SHIP = "ship66",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 133,
  LEVEL = 13,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 49,
  AVATAR = "head63",
  SHIP = "ship66",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 133,
  LEVEL = 14,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 49,
  AVATAR = "head63",
  SHIP = "ship66",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 133,
  LEVEL = 15,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 49,
  AVATAR = "head63",
  SHIP = "ship66",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 133,
  LEVEL = 16,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 49,
  AVATAR = "head63",
  SHIP = "ship66",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 134,
  LEVEL = 0,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 50,
  AVATAR = "head64",
  SHIP = "ship67",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 134,
  LEVEL = 1,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 50,
  AVATAR = "head64",
  SHIP = "ship67",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 134,
  LEVEL = 2,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 50,
  AVATAR = "head64",
  SHIP = "ship67",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 134,
  LEVEL = 3,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 50,
  AVATAR = "head64",
  SHIP = "ship67",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 134,
  LEVEL = 4,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 50,
  AVATAR = "head64",
  SHIP = "ship67",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 134,
  LEVEL = 5,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 50,
  AVATAR = "head64",
  SHIP = "ship67",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 134,
  LEVEL = 6,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 50,
  AVATAR = "head64",
  SHIP = "ship67",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 134,
  LEVEL = 7,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 50,
  AVATAR = "head64",
  SHIP = "ship67",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 134,
  LEVEL = 8,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 50,
  AVATAR = "head64",
  SHIP = "ship67",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 134,
  LEVEL = 9,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 50,
  AVATAR = "head64",
  SHIP = "ship67",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 134,
  LEVEL = 10,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 50,
  AVATAR = "head64",
  SHIP = "ship67",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 134,
  LEVEL = 11,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 50,
  AVATAR = "head64",
  SHIP = "ship67",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 134,
  LEVEL = 12,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 50,
  AVATAR = "head64",
  SHIP = "ship67",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 134,
  LEVEL = 13,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 50,
  AVATAR = "head64",
  SHIP = "ship67",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 134,
  LEVEL = 14,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 50,
  AVATAR = "head64",
  SHIP = "ship67",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 134,
  LEVEL = 15,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 50,
  AVATAR = "head64",
  SHIP = "ship67",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 134,
  LEVEL = 16,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 50,
  AVATAR = "head64",
  SHIP = "ship67",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 135,
  LEVEL = 0,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 54,
  AVATAR = "head65",
  SHIP = "ship49",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 135,
  LEVEL = 1,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 54,
  AVATAR = "head65",
  SHIP = "ship49",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 135,
  LEVEL = 2,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 54,
  AVATAR = "head65",
  SHIP = "ship49",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 135,
  LEVEL = 3,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 54,
  AVATAR = "head65",
  SHIP = "ship49",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 135,
  LEVEL = 4,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 54,
  AVATAR = "head65",
  SHIP = "ship49",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 135,
  LEVEL = 5,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 54,
  AVATAR = "head65",
  SHIP = "ship49",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 135,
  LEVEL = 6,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 54,
  AVATAR = "head65",
  SHIP = "ship49",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 135,
  LEVEL = 7,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 54,
  AVATAR = "head65",
  SHIP = "ship49",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 135,
  LEVEL = 8,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 54,
  AVATAR = "head65",
  SHIP = "ship49",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 135,
  LEVEL = 9,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 54,
  AVATAR = "head65",
  SHIP = "ship49",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 135,
  LEVEL = 10,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 54,
  AVATAR = "head65",
  SHIP = "ship49",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 135,
  LEVEL = 11,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 54,
  AVATAR = "head65",
  SHIP = "ship49",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 135,
  LEVEL = 12,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 54,
  AVATAR = "head65",
  SHIP = "ship49",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 135,
  LEVEL = 13,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 54,
  AVATAR = "head65",
  SHIP = "ship49",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 135,
  LEVEL = 14,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 54,
  AVATAR = "head65",
  SHIP = "ship49",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 135,
  LEVEL = 15,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 54,
  AVATAR = "head65",
  SHIP = "ship49",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 135,
  LEVEL = 16,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 54,
  AVATAR = "head65",
  SHIP = "ship49",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 136,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 55,
  AVATAR = "head65",
  SHIP = "ship68",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 136,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 55,
  AVATAR = "head65",
  SHIP = "ship68",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 136,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 55,
  AVATAR = "head65",
  SHIP = "ship68",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 136,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 55,
  AVATAR = "head65",
  SHIP = "ship68",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 136,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 55,
  AVATAR = "head65",
  SHIP = "ship68",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 136,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 55,
  AVATAR = "head65",
  SHIP = "ship68",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 136,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 55,
  AVATAR = "head65",
  SHIP = "ship68",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 136,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 55,
  AVATAR = "head65",
  SHIP = "ship68",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 136,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 55,
  AVATAR = "head65",
  SHIP = "ship68",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 136,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 55,
  AVATAR = "head65",
  SHIP = "ship68",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 136,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 55,
  AVATAR = "head65",
  SHIP = "ship68",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 136,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 55,
  AVATAR = "head65",
  SHIP = "ship68",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 136,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 55,
  AVATAR = "head65",
  SHIP = "ship68",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 136,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 55,
  AVATAR = "head65",
  SHIP = "ship68",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 136,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 55,
  AVATAR = "head65",
  SHIP = "ship68",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 136,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 55,
  AVATAR = "head65",
  SHIP = "ship68",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 136,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 55,
  AVATAR = "head65",
  SHIP = "ship68",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 137,
  LEVEL = 0,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 56,
  AVATAR = "head66",
  SHIP = "ship69",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 137,
  LEVEL = 1,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 56,
  AVATAR = "head66",
  SHIP = "ship69",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 137,
  LEVEL = 2,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 56,
  AVATAR = "head66",
  SHIP = "ship69",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 137,
  LEVEL = 3,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 56,
  AVATAR = "head66",
  SHIP = "ship69",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 137,
  LEVEL = 4,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 56,
  AVATAR = "head66",
  SHIP = "ship69",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 137,
  LEVEL = 5,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 56,
  AVATAR = "head66",
  SHIP = "ship69",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 137,
  LEVEL = 6,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 56,
  AVATAR = "head66",
  SHIP = "ship69",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 137,
  LEVEL = 7,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 56,
  AVATAR = "head66",
  SHIP = "ship69",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 137,
  LEVEL = 8,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 56,
  AVATAR = "head66",
  SHIP = "ship69",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 137,
  LEVEL = 9,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 56,
  AVATAR = "head66",
  SHIP = "ship69",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 137,
  LEVEL = 10,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 56,
  AVATAR = "head66",
  SHIP = "ship69",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 137,
  LEVEL = 11,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 56,
  AVATAR = "head66",
  SHIP = "ship69",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 137,
  LEVEL = 12,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 56,
  AVATAR = "head66",
  SHIP = "ship69",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 137,
  LEVEL = 13,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 56,
  AVATAR = "head66",
  SHIP = "ship69",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 137,
  LEVEL = 14,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 56,
  AVATAR = "head66",
  SHIP = "ship69",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 137,
  LEVEL = 15,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 56,
  AVATAR = "head66",
  SHIP = "ship69",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 137,
  LEVEL = 16,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 56,
  AVATAR = "head66",
  SHIP = "ship69",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 138,
  LEVEL = 0,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 57,
  AVATAR = "head67",
  SHIP = "ship70",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 138,
  LEVEL = 1,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 57,
  AVATAR = "head67",
  SHIP = "ship70",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 138,
  LEVEL = 2,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 57,
  AVATAR = "head67",
  SHIP = "ship70",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 138,
  LEVEL = 3,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 57,
  AVATAR = "head67",
  SHIP = "ship70",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 138,
  LEVEL = 4,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 57,
  AVATAR = "head67",
  SHIP = "ship70",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 138,
  LEVEL = 5,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 57,
  AVATAR = "head67",
  SHIP = "ship70",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 138,
  LEVEL = 6,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 57,
  AVATAR = "head67",
  SHIP = "ship70",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 138,
  LEVEL = 7,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 57,
  AVATAR = "head67",
  SHIP = "ship70",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 138,
  LEVEL = 8,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 57,
  AVATAR = "head67",
  SHIP = "ship70",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 138,
  LEVEL = 9,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 57,
  AVATAR = "head67",
  SHIP = "ship70",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 138,
  LEVEL = 10,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 57,
  AVATAR = "head67",
  SHIP = "ship70",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 138,
  LEVEL = 11,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 57,
  AVATAR = "head67",
  SHIP = "ship70",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 138,
  LEVEL = 12,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 57,
  AVATAR = "head67",
  SHIP = "ship70",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 138,
  LEVEL = 13,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 57,
  AVATAR = "head67",
  SHIP = "ship70",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 138,
  LEVEL = 14,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 57,
  AVATAR = "head67",
  SHIP = "ship70",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 138,
  LEVEL = 15,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 57,
  AVATAR = "head67",
  SHIP = "ship70",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 138,
  LEVEL = 16,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 57,
  AVATAR = "head67",
  SHIP = "ship70",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 139,
  LEVEL = 0,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 58,
  AVATAR = "head68",
  SHIP = "ship71",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 139,
  LEVEL = 1,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 58,
  AVATAR = "head68",
  SHIP = "ship71",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 139,
  LEVEL = 2,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 58,
  AVATAR = "head68",
  SHIP = "ship71",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 139,
  LEVEL = 3,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 58,
  AVATAR = "head68",
  SHIP = "ship71",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 139,
  LEVEL = 4,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 58,
  AVATAR = "head68",
  SHIP = "ship71",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 139,
  LEVEL = 5,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 58,
  AVATAR = "head68",
  SHIP = "ship71",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 139,
  LEVEL = 6,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 58,
  AVATAR = "head68",
  SHIP = "ship71",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 139,
  LEVEL = 7,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 58,
  AVATAR = "head68",
  SHIP = "ship71",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 139,
  LEVEL = 8,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 58,
  AVATAR = "head68",
  SHIP = "ship71",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 139,
  LEVEL = 9,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 58,
  AVATAR = "head68",
  SHIP = "ship71",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 139,
  LEVEL = 10,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 58,
  AVATAR = "head68",
  SHIP = "ship71",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 139,
  LEVEL = 11,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 58,
  AVATAR = "head68",
  SHIP = "ship71",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 139,
  LEVEL = 12,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 58,
  AVATAR = "head68",
  SHIP = "ship71",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 139,
  LEVEL = 13,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 58,
  AVATAR = "head68",
  SHIP = "ship71",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 139,
  LEVEL = 14,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 58,
  AVATAR = "head68",
  SHIP = "ship71",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 139,
  LEVEL = 15,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 58,
  AVATAR = "head68",
  SHIP = "ship71",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 139,
  LEVEL = 16,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 58,
  AVATAR = "head68",
  SHIP = "ship71",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 140,
  LEVEL = 0,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 59,
  AVATAR = "head69",
  SHIP = "ship72",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 140,
  LEVEL = 1,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 59,
  AVATAR = "head69",
  SHIP = "ship72",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 140,
  LEVEL = 2,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 59,
  AVATAR = "head69",
  SHIP = "ship72",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 140,
  LEVEL = 3,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 59,
  AVATAR = "head69",
  SHIP = "ship72",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 140,
  LEVEL = 4,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 59,
  AVATAR = "head69",
  SHIP = "ship72",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 140,
  LEVEL = 5,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 59,
  AVATAR = "head69",
  SHIP = "ship72",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 140,
  LEVEL = 6,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 59,
  AVATAR = "head69",
  SHIP = "ship72",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 140,
  LEVEL = 7,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 59,
  AVATAR = "head69",
  SHIP = "ship72",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 140,
  LEVEL = 8,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 59,
  AVATAR = "head69",
  SHIP = "ship72",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 140,
  LEVEL = 9,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 59,
  AVATAR = "head69",
  SHIP = "ship72",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 140,
  LEVEL = 10,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 59,
  AVATAR = "head69",
  SHIP = "ship72",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 140,
  LEVEL = 11,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 59,
  AVATAR = "head69",
  SHIP = "ship72",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 140,
  LEVEL = 12,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 59,
  AVATAR = "head69",
  SHIP = "ship72",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 140,
  LEVEL = 13,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 59,
  AVATAR = "head69",
  SHIP = "ship72",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 140,
  LEVEL = 14,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 59,
  AVATAR = "head69",
  SHIP = "ship72",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 140,
  LEVEL = 15,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 59,
  AVATAR = "head69",
  SHIP = "ship72",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 140,
  LEVEL = 16,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 59,
  AVATAR = "head69",
  SHIP = "ship72",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 141,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 60,
  AVATAR = "head70",
  SHIP = "ship73",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 141,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 60,
  AVATAR = "head70",
  SHIP = "ship73",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 141,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 60,
  AVATAR = "head70",
  SHIP = "ship73",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 141,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 60,
  AVATAR = "head70",
  SHIP = "ship73",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 141,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 60,
  AVATAR = "head70",
  SHIP = "ship73",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 141,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 60,
  AVATAR = "head70",
  SHIP = "ship73",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 141,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 60,
  AVATAR = "head70",
  SHIP = "ship73",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 141,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 60,
  AVATAR = "head70",
  SHIP = "ship73",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 141,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 60,
  AVATAR = "head70",
  SHIP = "ship73",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 141,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 60,
  AVATAR = "head70",
  SHIP = "ship73",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 141,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 60,
  AVATAR = "head70",
  SHIP = "ship73",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 141,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 60,
  AVATAR = "head70",
  SHIP = "ship73",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 141,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 60,
  AVATAR = "head70",
  SHIP = "ship73",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 141,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 60,
  AVATAR = "head70",
  SHIP = "ship73",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 141,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 60,
  AVATAR = "head70",
  SHIP = "ship73",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 141,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 60,
  AVATAR = "head70",
  SHIP = "ship73",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 141,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 60,
  AVATAR = "head70",
  SHIP = "ship73",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 142,
  LEVEL = 0,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 61,
  AVATAR = "head71",
  SHIP = "ship74",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 142,
  LEVEL = 1,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 61,
  AVATAR = "head71",
  SHIP = "ship74",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 142,
  LEVEL = 2,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 61,
  AVATAR = "head71",
  SHIP = "ship74",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 142,
  LEVEL = 3,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 61,
  AVATAR = "head71",
  SHIP = "ship74",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 142,
  LEVEL = 4,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 61,
  AVATAR = "head71",
  SHIP = "ship74",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 142,
  LEVEL = 5,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 61,
  AVATAR = "head71",
  SHIP = "ship74",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 142,
  LEVEL = 6,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 61,
  AVATAR = "head71",
  SHIP = "ship74",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 142,
  LEVEL = 7,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 61,
  AVATAR = "head71",
  SHIP = "ship74",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 142,
  LEVEL = 8,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 61,
  AVATAR = "head71",
  SHIP = "ship74",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 142,
  LEVEL = 9,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 61,
  AVATAR = "head71",
  SHIP = "ship74",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 142,
  LEVEL = 10,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 61,
  AVATAR = "head71",
  SHIP = "ship74",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 142,
  LEVEL = 11,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 61,
  AVATAR = "head71",
  SHIP = "ship74",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 142,
  LEVEL = 12,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 61,
  AVATAR = "head71",
  SHIP = "ship74",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 142,
  LEVEL = 13,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 61,
  AVATAR = "head71",
  SHIP = "ship74",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 142,
  LEVEL = 14,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 61,
  AVATAR = "head71",
  SHIP = "ship74",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 142,
  LEVEL = 15,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 61,
  AVATAR = "head71",
  SHIP = "ship74",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 142,
  LEVEL = 16,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 61,
  AVATAR = "head71",
  SHIP = "ship74",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 143,
  LEVEL = 0,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 1005,
  AVATAR = "head72",
  SHIP = "ship75",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 143,
  LEVEL = 1,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 1005,
  AVATAR = "head72",
  SHIP = "ship75",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 143,
  LEVEL = 2,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 1005,
  AVATAR = "head72",
  SHIP = "ship75",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 143,
  LEVEL = 3,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 1005,
  AVATAR = "head72",
  SHIP = "ship75",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 143,
  LEVEL = 4,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 1005,
  AVATAR = "head72",
  SHIP = "ship75",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 143,
  LEVEL = 5,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 1005,
  AVATAR = "head72",
  SHIP = "ship75",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 143,
  LEVEL = 6,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 1005,
  AVATAR = "head72",
  SHIP = "ship75",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 143,
  LEVEL = 7,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 1005,
  AVATAR = "head72",
  SHIP = "ship75",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 143,
  LEVEL = 8,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 1005,
  AVATAR = "head72",
  SHIP = "ship75",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 143,
  LEVEL = 9,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 1005,
  AVATAR = "head72",
  SHIP = "ship75",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 143,
  LEVEL = 10,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 1005,
  AVATAR = "head72",
  SHIP = "ship75",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 143,
  LEVEL = 11,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 1005,
  AVATAR = "head72",
  SHIP = "ship75",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 143,
  LEVEL = 12,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 1005,
  AVATAR = "head72",
  SHIP = "ship75",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 143,
  LEVEL = 13,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 1005,
  AVATAR = "head72",
  SHIP = "ship75",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 143,
  LEVEL = 14,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 1005,
  AVATAR = "head72",
  SHIP = "ship75",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 143,
  LEVEL = 15,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 1005,
  AVATAR = "head72",
  SHIP = "ship75",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 143,
  LEVEL = 16,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 1005,
  AVATAR = "head72",
  SHIP = "ship75",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 144,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 63,
  AVATAR = "head73",
  SHIP = "ship76",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 144,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 63,
  AVATAR = "head73",
  SHIP = "ship76",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 144,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 63,
  AVATAR = "head73",
  SHIP = "ship76",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 144,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 63,
  AVATAR = "head73",
  SHIP = "ship76",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 144,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 63,
  AVATAR = "head73",
  SHIP = "ship76",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 144,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 63,
  AVATAR = "head73",
  SHIP = "ship76",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 144,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 63,
  AVATAR = "head73",
  SHIP = "ship76",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 144,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 63,
  AVATAR = "head73",
  SHIP = "ship76",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 144,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 63,
  AVATAR = "head73",
  SHIP = "ship76",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 144,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 63,
  AVATAR = "head73",
  SHIP = "ship76",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 144,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 63,
  AVATAR = "head73",
  SHIP = "ship76",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 144,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 63,
  AVATAR = "head73",
  SHIP = "ship76",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 144,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 63,
  AVATAR = "head73",
  SHIP = "ship76",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 144,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 63,
  AVATAR = "head73",
  SHIP = "ship76",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 144,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 63,
  AVATAR = "head73",
  SHIP = "ship76",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 144,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 63,
  AVATAR = "head73",
  SHIP = "ship76",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 144,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 63,
  AVATAR = "head73",
  SHIP = "ship76",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 145,
  LEVEL = 0,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 64,
  AVATAR = "head74",
  SHIP = "ship77",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 145,
  LEVEL = 1,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 64,
  AVATAR = "head74",
  SHIP = "ship77",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 145,
  LEVEL = 2,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 64,
  AVATAR = "head74",
  SHIP = "ship77",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 145,
  LEVEL = 3,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 64,
  AVATAR = "head74",
  SHIP = "ship77",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 145,
  LEVEL = 4,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 64,
  AVATAR = "head74",
  SHIP = "ship77",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 145,
  LEVEL = 5,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 64,
  AVATAR = "head74",
  SHIP = "ship77",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 145,
  LEVEL = 6,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 64,
  AVATAR = "head74",
  SHIP = "ship77",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 145,
  LEVEL = 7,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 64,
  AVATAR = "head74",
  SHIP = "ship77",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 145,
  LEVEL = 8,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 64,
  AVATAR = "head74",
  SHIP = "ship77",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 145,
  LEVEL = 9,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 64,
  AVATAR = "head74",
  SHIP = "ship77",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 145,
  LEVEL = 10,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 64,
  AVATAR = "head74",
  SHIP = "ship77",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 145,
  LEVEL = 11,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 64,
  AVATAR = "head74",
  SHIP = "ship77",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 145,
  LEVEL = 12,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 64,
  AVATAR = "head74",
  SHIP = "ship77",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 145,
  LEVEL = 13,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 64,
  AVATAR = "head74",
  SHIP = "ship77",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 145,
  LEVEL = 14,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 64,
  AVATAR = "head74",
  SHIP = "ship77",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 145,
  LEVEL = 15,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 64,
  AVATAR = "head74",
  SHIP = "ship77",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 145,
  LEVEL = 16,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 64,
  AVATAR = "head74",
  SHIP = "ship77",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 146,
  LEVEL = 0,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 65,
  AVATAR = "head75",
  SHIP = "ship78",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 146,
  LEVEL = 1,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 65,
  AVATAR = "head75",
  SHIP = "ship78",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 146,
  LEVEL = 2,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 65,
  AVATAR = "head75",
  SHIP = "ship78",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 146,
  LEVEL = 3,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 65,
  AVATAR = "head75",
  SHIP = "ship78",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 146,
  LEVEL = 4,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 65,
  AVATAR = "head75",
  SHIP = "ship78",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 146,
  LEVEL = 5,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 65,
  AVATAR = "head75",
  SHIP = "ship78",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 146,
  LEVEL = 6,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 65,
  AVATAR = "head75",
  SHIP = "ship78",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 146,
  LEVEL = 7,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 65,
  AVATAR = "head75",
  SHIP = "ship78",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 146,
  LEVEL = 8,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 65,
  AVATAR = "head75",
  SHIP = "ship78",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 146,
  LEVEL = 9,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 65,
  AVATAR = "head75",
  SHIP = "ship78",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 146,
  LEVEL = 10,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 65,
  AVATAR = "head75",
  SHIP = "ship78",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 146,
  LEVEL = 11,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 65,
  AVATAR = "head75",
  SHIP = "ship78",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 146,
  LEVEL = 12,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 65,
  AVATAR = "head75",
  SHIP = "ship78",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 146,
  LEVEL = 13,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 65,
  AVATAR = "head75",
  SHIP = "ship78",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 146,
  LEVEL = 14,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 65,
  AVATAR = "head75",
  SHIP = "ship78",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 146,
  LEVEL = 15,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 65,
  AVATAR = "head75",
  SHIP = "ship78",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 146,
  LEVEL = 16,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 65,
  AVATAR = "head75",
  SHIP = "ship78",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 147,
  LEVEL = 0,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 66,
  AVATAR = "head76",
  SHIP = "ship79",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 147,
  LEVEL = 1,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 66,
  AVATAR = "head76",
  SHIP = "ship79",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 147,
  LEVEL = 2,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 66,
  AVATAR = "head76",
  SHIP = "ship79",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 147,
  LEVEL = 3,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 66,
  AVATAR = "head76",
  SHIP = "ship79",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 147,
  LEVEL = 4,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 66,
  AVATAR = "head76",
  SHIP = "ship79",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 147,
  LEVEL = 5,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 66,
  AVATAR = "head76",
  SHIP = "ship79",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 147,
  LEVEL = 6,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 66,
  AVATAR = "head76",
  SHIP = "ship79",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 147,
  LEVEL = 7,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 66,
  AVATAR = "head76",
  SHIP = "ship79",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 147,
  LEVEL = 8,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 66,
  AVATAR = "head76",
  SHIP = "ship79",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 147,
  LEVEL = 9,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 66,
  AVATAR = "head76",
  SHIP = "ship79",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 147,
  LEVEL = 10,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 66,
  AVATAR = "head76",
  SHIP = "ship79",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 147,
  LEVEL = 11,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 66,
  AVATAR = "head76",
  SHIP = "ship79",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 147,
  LEVEL = 12,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 66,
  AVATAR = "head76",
  SHIP = "ship79",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 147,
  LEVEL = 13,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 66,
  AVATAR = "head76",
  SHIP = "ship79",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 147,
  LEVEL = 14,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 66,
  AVATAR = "head76",
  SHIP = "ship79",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 147,
  LEVEL = 15,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 66,
  AVATAR = "head76",
  SHIP = "ship79",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 147,
  LEVEL = 16,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 66,
  AVATAR = "head76",
  SHIP = "ship79",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 148,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 67,
  AVATAR = "head77",
  SHIP = "ship80",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 148,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 67,
  AVATAR = "head77",
  SHIP = "ship80",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 148,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 67,
  AVATAR = "head77",
  SHIP = "ship80",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 148,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 67,
  AVATAR = "head77",
  SHIP = "ship80",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 148,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 67,
  AVATAR = "head77",
  SHIP = "ship80",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 148,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 67,
  AVATAR = "head77",
  SHIP = "ship80",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 148,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 67,
  AVATAR = "head77",
  SHIP = "ship80",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 148,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 67,
  AVATAR = "head77",
  SHIP = "ship80",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 148,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 67,
  AVATAR = "head77",
  SHIP = "ship80",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 148,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 67,
  AVATAR = "head77",
  SHIP = "ship80",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 148,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 67,
  AVATAR = "head77",
  SHIP = "ship80",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 148,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 67,
  AVATAR = "head77",
  SHIP = "ship80",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 148,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 67,
  AVATAR = "head77",
  SHIP = "ship80",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 148,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 67,
  AVATAR = "head77",
  SHIP = "ship80",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 148,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 67,
  AVATAR = "head77",
  SHIP = "ship80",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 148,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 67,
  AVATAR = "head77",
  SHIP = "ship80",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 148,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 67,
  AVATAR = "head77",
  SHIP = "ship80",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 149,
  LEVEL = 0,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 68,
  AVATAR = "head78",
  SHIP = "ship81",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 149,
  LEVEL = 1,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 68,
  AVATAR = "head78",
  SHIP = "ship81",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 149,
  LEVEL = 2,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 68,
  AVATAR = "head78",
  SHIP = "ship81",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 149,
  LEVEL = 3,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 68,
  AVATAR = "head78",
  SHIP = "ship81",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 149,
  LEVEL = 4,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 68,
  AVATAR = "head78",
  SHIP = "ship81",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 149,
  LEVEL = 5,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 68,
  AVATAR = "head78",
  SHIP = "ship81",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 149,
  LEVEL = 6,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 68,
  AVATAR = "head78",
  SHIP = "ship81",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 149,
  LEVEL = 7,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 68,
  AVATAR = "head78",
  SHIP = "ship81",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 149,
  LEVEL = 8,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 68,
  AVATAR = "head78",
  SHIP = "ship81",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 149,
  LEVEL = 9,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 68,
  AVATAR = "head78",
  SHIP = "ship81",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 149,
  LEVEL = 10,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 68,
  AVATAR = "head78",
  SHIP = "ship81",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 149,
  LEVEL = 11,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 68,
  AVATAR = "head78",
  SHIP = "ship81",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 149,
  LEVEL = 12,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 68,
  AVATAR = "head78",
  SHIP = "ship81",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 149,
  LEVEL = 13,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 68,
  AVATAR = "head78",
  SHIP = "ship81",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 149,
  LEVEL = 14,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 68,
  AVATAR = "head78",
  SHIP = "ship81",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 149,
  LEVEL = 15,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 68,
  AVATAR = "head78",
  SHIP = "ship81",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 149,
  LEVEL = 16,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 68,
  AVATAR = "head78",
  SHIP = "ship81",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 150,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 69,
  AVATAR = "head79",
  SHIP = "ship82",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 150,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 69,
  AVATAR = "head79",
  SHIP = "ship82",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 150,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 69,
  AVATAR = "head79",
  SHIP = "ship82",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 150,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 69,
  AVATAR = "head79",
  SHIP = "ship82",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 150,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 69,
  AVATAR = "head79",
  SHIP = "ship82",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 150,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 69,
  AVATAR = "head79",
  SHIP = "ship82",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 150,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 69,
  AVATAR = "head79",
  SHIP = "ship82",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 150,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 69,
  AVATAR = "head79",
  SHIP = "ship82",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 150,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 69,
  AVATAR = "head79",
  SHIP = "ship82",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 150,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 69,
  AVATAR = "head79",
  SHIP = "ship82",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 150,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 69,
  AVATAR = "head79",
  SHIP = "ship82",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 150,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 69,
  AVATAR = "head79",
  SHIP = "ship82",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 150,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 69,
  AVATAR = "head79",
  SHIP = "ship82",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 150,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 69,
  AVATAR = "head79",
  SHIP = "ship82",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 150,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 69,
  AVATAR = "head79",
  SHIP = "ship82",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 150,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 69,
  AVATAR = "head79",
  SHIP = "ship82",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 150,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 69,
  AVATAR = "head79",
  SHIP = "ship82",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 151,
  LEVEL = 0,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 70,
  AVATAR = "head80",
  SHIP = "ship83",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 151,
  LEVEL = 1,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 70,
  AVATAR = "head80",
  SHIP = "ship83",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 151,
  LEVEL = 2,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 70,
  AVATAR = "head80",
  SHIP = "ship83",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 151,
  LEVEL = 3,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 70,
  AVATAR = "head80",
  SHIP = "ship83",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 151,
  LEVEL = 4,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 70,
  AVATAR = "head80",
  SHIP = "ship83",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 151,
  LEVEL = 5,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 70,
  AVATAR = "head80",
  SHIP = "ship83",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 151,
  LEVEL = 6,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 70,
  AVATAR = "head80",
  SHIP = "ship83",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 151,
  LEVEL = 7,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 70,
  AVATAR = "head80",
  SHIP = "ship83",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 151,
  LEVEL = 8,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 70,
  AVATAR = "head80",
  SHIP = "ship83",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 151,
  LEVEL = 9,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 70,
  AVATAR = "head80",
  SHIP = "ship83",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 151,
  LEVEL = 10,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 70,
  AVATAR = "head80",
  SHIP = "ship83",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 151,
  LEVEL = 11,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 70,
  AVATAR = "head80",
  SHIP = "ship83",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 151,
  LEVEL = 12,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 70,
  AVATAR = "head80",
  SHIP = "ship83",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 151,
  LEVEL = 13,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 70,
  AVATAR = "head80",
  SHIP = "ship83",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 151,
  LEVEL = 14,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 70,
  AVATAR = "head80",
  SHIP = "ship83",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 151,
  LEVEL = 15,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 70,
  AVATAR = "head80",
  SHIP = "ship83",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 151,
  LEVEL = 16,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 70,
  AVATAR = "head80",
  SHIP = "ship83",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 152,
  LEVEL = 0,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 71,
  AVATAR = "head81",
  SHIP = "ship84",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 152,
  LEVEL = 1,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 71,
  AVATAR = "head81",
  SHIP = "ship84",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 152,
  LEVEL = 2,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 71,
  AVATAR = "head81",
  SHIP = "ship84",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 152,
  LEVEL = 3,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 71,
  AVATAR = "head81",
  SHIP = "ship84",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 152,
  LEVEL = 4,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 71,
  AVATAR = "head81",
  SHIP = "ship84",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 152,
  LEVEL = 5,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 71,
  AVATAR = "head81",
  SHIP = "ship84",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 152,
  LEVEL = 6,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 71,
  AVATAR = "head81",
  SHIP = "ship84",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 152,
  LEVEL = 7,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 71,
  AVATAR = "head81",
  SHIP = "ship84",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 152,
  LEVEL = 8,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 71,
  AVATAR = "head81",
  SHIP = "ship84",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 152,
  LEVEL = 9,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 71,
  AVATAR = "head81",
  SHIP = "ship84",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 152,
  LEVEL = 10,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 71,
  AVATAR = "head81",
  SHIP = "ship84",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 152,
  LEVEL = 11,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 71,
  AVATAR = "head81",
  SHIP = "ship84",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 152,
  LEVEL = 12,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 71,
  AVATAR = "head81",
  SHIP = "ship84",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 152,
  LEVEL = 13,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 71,
  AVATAR = "head81",
  SHIP = "ship84",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 152,
  LEVEL = 14,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 71,
  AVATAR = "head81",
  SHIP = "ship84",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 152,
  LEVEL = 15,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 71,
  AVATAR = "head81",
  SHIP = "ship84",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 152,
  LEVEL = 16,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 71,
  AVATAR = "head81",
  SHIP = "ship84",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 153,
  LEVEL = 0,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 1004,
  AVATAR = "head82",
  SHIP = "ship85",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 153,
  LEVEL = 1,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 1004,
  AVATAR = "head82",
  SHIP = "ship85",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 153,
  LEVEL = 2,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 1004,
  AVATAR = "head82",
  SHIP = "ship85",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 153,
  LEVEL = 3,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 1004,
  AVATAR = "head82",
  SHIP = "ship85",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 153,
  LEVEL = 4,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 1004,
  AVATAR = "head82",
  SHIP = "ship85",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 153,
  LEVEL = 5,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 1004,
  AVATAR = "head82",
  SHIP = "ship85",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 153,
  LEVEL = 6,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 1004,
  AVATAR = "head82",
  SHIP = "ship85",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 153,
  LEVEL = 7,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 1004,
  AVATAR = "head82",
  SHIP = "ship85",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 153,
  LEVEL = 8,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 1004,
  AVATAR = "head82",
  SHIP = "ship85",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 153,
  LEVEL = 9,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 1004,
  AVATAR = "head82",
  SHIP = "ship85",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 153,
  LEVEL = 10,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 1004,
  AVATAR = "head82",
  SHIP = "ship85",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 153,
  LEVEL = 11,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 1004,
  AVATAR = "head82",
  SHIP = "ship85",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 153,
  LEVEL = 12,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 1004,
  AVATAR = "head82",
  SHIP = "ship85",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 153,
  LEVEL = 13,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 1004,
  AVATAR = "head82",
  SHIP = "ship85",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 153,
  LEVEL = 14,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 1004,
  AVATAR = "head82",
  SHIP = "ship85",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 153,
  LEVEL = 15,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 1004,
  AVATAR = "head82",
  SHIP = "ship85",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 153,
  LEVEL = 16,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 1004,
  AVATAR = "head82",
  SHIP = "ship85",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 154,
  LEVEL = 0,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 73,
  AVATAR = "head83",
  SHIP = "ship86",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 154,
  LEVEL = 1,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 73,
  AVATAR = "head83",
  SHIP = "ship86",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 154,
  LEVEL = 2,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 73,
  AVATAR = "head83",
  SHIP = "ship86",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 154,
  LEVEL = 3,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 73,
  AVATAR = "head83",
  SHIP = "ship86",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 154,
  LEVEL = 4,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 73,
  AVATAR = "head83",
  SHIP = "ship86",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 154,
  LEVEL = 5,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 73,
  AVATAR = "head83",
  SHIP = "ship86",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 154,
  LEVEL = 6,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 73,
  AVATAR = "head83",
  SHIP = "ship86",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 154,
  LEVEL = 7,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 73,
  AVATAR = "head83",
  SHIP = "ship86",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 154,
  LEVEL = 8,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 73,
  AVATAR = "head83",
  SHIP = "ship86",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 154,
  LEVEL = 9,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 73,
  AVATAR = "head83",
  SHIP = "ship86",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 154,
  LEVEL = 10,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 73,
  AVATAR = "head83",
  SHIP = "ship86",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 154,
  LEVEL = 11,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 73,
  AVATAR = "head83",
  SHIP = "ship86",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 154,
  LEVEL = 12,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 73,
  AVATAR = "head83",
  SHIP = "ship86",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 154,
  LEVEL = 13,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 73,
  AVATAR = "head83",
  SHIP = "ship86",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 154,
  LEVEL = 14,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 73,
  AVATAR = "head83",
  SHIP = "ship86",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 154,
  LEVEL = 15,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 73,
  AVATAR = "head83",
  SHIP = "ship86",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 154,
  LEVEL = 16,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 73,
  AVATAR = "head83",
  SHIP = "ship86",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 155,
  LEVEL = 0,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 74,
  AVATAR = "head84",
  SHIP = "ship87",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 155,
  LEVEL = 1,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 74,
  AVATAR = "head84",
  SHIP = "ship87",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 155,
  LEVEL = 2,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 74,
  AVATAR = "head84",
  SHIP = "ship87",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 155,
  LEVEL = 3,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 74,
  AVATAR = "head84",
  SHIP = "ship87",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 155,
  LEVEL = 4,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 74,
  AVATAR = "head84",
  SHIP = "ship87",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 155,
  LEVEL = 5,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 74,
  AVATAR = "head84",
  SHIP = "ship87",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 155,
  LEVEL = 6,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 74,
  AVATAR = "head84",
  SHIP = "ship87",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 155,
  LEVEL = 7,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 74,
  AVATAR = "head84",
  SHIP = "ship87",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 155,
  LEVEL = 8,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 74,
  AVATAR = "head84",
  SHIP = "ship87",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 155,
  LEVEL = 9,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 74,
  AVATAR = "head84",
  SHIP = "ship87",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 155,
  LEVEL = 10,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 74,
  AVATAR = "head84",
  SHIP = "ship87",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 155,
  LEVEL = 11,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 74,
  AVATAR = "head84",
  SHIP = "ship87",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 155,
  LEVEL = 12,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 74,
  AVATAR = "head84",
  SHIP = "ship87",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 155,
  LEVEL = 13,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 74,
  AVATAR = "head84",
  SHIP = "ship87",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 155,
  LEVEL = 14,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 74,
  AVATAR = "head84",
  SHIP = "ship87",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 155,
  LEVEL = 15,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 74,
  AVATAR = "head84",
  SHIP = "ship87",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 155,
  LEVEL = 16,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 74,
  AVATAR = "head84",
  SHIP = "ship87",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 156,
  LEVEL = 0,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 75,
  AVATAR = "head85",
  SHIP = "ship88",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 156,
  LEVEL = 1,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 75,
  AVATAR = "head85",
  SHIP = "ship88",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 156,
  LEVEL = 2,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 75,
  AVATAR = "head85",
  SHIP = "ship88",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 156,
  LEVEL = 3,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 75,
  AVATAR = "head85",
  SHIP = "ship88",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 156,
  LEVEL = 4,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 75,
  AVATAR = "head85",
  SHIP = "ship88",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 156,
  LEVEL = 5,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 75,
  AVATAR = "head85",
  SHIP = "ship88",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 156,
  LEVEL = 6,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 75,
  AVATAR = "head85",
  SHIP = "ship88",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 156,
  LEVEL = 7,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 75,
  AVATAR = "head85",
  SHIP = "ship88",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 156,
  LEVEL = 8,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 75,
  AVATAR = "head85",
  SHIP = "ship88",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 156,
  LEVEL = 9,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 75,
  AVATAR = "head85",
  SHIP = "ship88",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 156,
  LEVEL = 10,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 75,
  AVATAR = "head85",
  SHIP = "ship88",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 156,
  LEVEL = 11,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 75,
  AVATAR = "head85",
  SHIP = "ship88",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 156,
  LEVEL = 12,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 75,
  AVATAR = "head85",
  SHIP = "ship88",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 156,
  LEVEL = 13,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 75,
  AVATAR = "head85",
  SHIP = "ship88",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 156,
  LEVEL = 14,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 75,
  AVATAR = "head85",
  SHIP = "ship88",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 156,
  LEVEL = 15,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 75,
  AVATAR = "head85",
  SHIP = "ship88",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 156,
  LEVEL = 16,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 75,
  AVATAR = "head85",
  SHIP = "ship88",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 157,
  LEVEL = 0,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 76,
  AVATAR = "head86",
  SHIP = "ship89",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 157,
  LEVEL = 1,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 76,
  AVATAR = "head86",
  SHIP = "ship89",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 157,
  LEVEL = 2,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 76,
  AVATAR = "head86",
  SHIP = "ship89",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 157,
  LEVEL = 3,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 76,
  AVATAR = "head86",
  SHIP = "ship89",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 157,
  LEVEL = 4,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 76,
  AVATAR = "head86",
  SHIP = "ship89",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 157,
  LEVEL = 5,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 76,
  AVATAR = "head86",
  SHIP = "ship89",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 157,
  LEVEL = 6,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 76,
  AVATAR = "head86",
  SHIP = "ship89",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 157,
  LEVEL = 7,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 76,
  AVATAR = "head86",
  SHIP = "ship89",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 157,
  LEVEL = 8,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 76,
  AVATAR = "head86",
  SHIP = "ship89",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 157,
  LEVEL = 9,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 76,
  AVATAR = "head86",
  SHIP = "ship89",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 157,
  LEVEL = 10,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 76,
  AVATAR = "head86",
  SHIP = "ship89",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 157,
  LEVEL = 11,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 76,
  AVATAR = "head86",
  SHIP = "ship89",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 157,
  LEVEL = 12,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 76,
  AVATAR = "head86",
  SHIP = "ship89",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 157,
  LEVEL = 13,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 76,
  AVATAR = "head86",
  SHIP = "ship89",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 157,
  LEVEL = 14,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 76,
  AVATAR = "head86",
  SHIP = "ship89",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 157,
  LEVEL = 15,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 76,
  AVATAR = "head86",
  SHIP = "ship89",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 157,
  LEVEL = 16,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 76,
  AVATAR = "head86",
  SHIP = "ship89",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 158,
  LEVEL = 0,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 77,
  AVATAR = "head87",
  SHIP = "ship90",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 158,
  LEVEL = 1,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 77,
  AVATAR = "head87",
  SHIP = "ship90",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 158,
  LEVEL = 2,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 77,
  AVATAR = "head87",
  SHIP = "ship90",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 158,
  LEVEL = 3,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 77,
  AVATAR = "head87",
  SHIP = "ship90",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 158,
  LEVEL = 4,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 77,
  AVATAR = "head87",
  SHIP = "ship90",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 158,
  LEVEL = 5,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 77,
  AVATAR = "head87",
  SHIP = "ship90",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 158,
  LEVEL = 6,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 77,
  AVATAR = "head87",
  SHIP = "ship90",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 158,
  LEVEL = 7,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 77,
  AVATAR = "head87",
  SHIP = "ship90",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 158,
  LEVEL = 8,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 77,
  AVATAR = "head87",
  SHIP = "ship90",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 158,
  LEVEL = 9,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 77,
  AVATAR = "head87",
  SHIP = "ship90",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 158,
  LEVEL = 10,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 77,
  AVATAR = "head87",
  SHIP = "ship90",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 158,
  LEVEL = 11,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 77,
  AVATAR = "head87",
  SHIP = "ship90",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 158,
  LEVEL = 12,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 77,
  AVATAR = "head87",
  SHIP = "ship90",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 158,
  LEVEL = 13,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 77,
  AVATAR = "head87",
  SHIP = "ship90",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 158,
  LEVEL = 14,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 77,
  AVATAR = "head87",
  SHIP = "ship90",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 158,
  LEVEL = 15,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 77,
  AVATAR = "head87",
  SHIP = "ship90",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 158,
  LEVEL = 16,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 77,
  AVATAR = "head87",
  SHIP = "ship90",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 159,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 78,
  AVATAR = "head88",
  SHIP = "ship91",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 159,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 78,
  AVATAR = "head88",
  SHIP = "ship91",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 159,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 78,
  AVATAR = "head88",
  SHIP = "ship91",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 159,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 78,
  AVATAR = "head88",
  SHIP = "ship91",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 159,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 78,
  AVATAR = "head88",
  SHIP = "ship91",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 159,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 78,
  AVATAR = "head88",
  SHIP = "ship91",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 159,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 78,
  AVATAR = "head88",
  SHIP = "ship91",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 159,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 78,
  AVATAR = "head88",
  SHIP = "ship91",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 159,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 78,
  AVATAR = "head88",
  SHIP = "ship91",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 159,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 78,
  AVATAR = "head88",
  SHIP = "ship91",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 159,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 78,
  AVATAR = "head88",
  SHIP = "ship91",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 159,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 78,
  AVATAR = "head88",
  SHIP = "ship91",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 159,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 78,
  AVATAR = "head88",
  SHIP = "ship91",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 159,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 78,
  AVATAR = "head88",
  SHIP = "ship91",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 159,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 78,
  AVATAR = "head88",
  SHIP = "ship91",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 159,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 78,
  AVATAR = "head88",
  SHIP = "ship91",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 159,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 78,
  AVATAR = "head88",
  SHIP = "ship91",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 160,
  LEVEL = 0,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 79,
  AVATAR = "head89",
  SHIP = "ship92",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 160,
  LEVEL = 1,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 79,
  AVATAR = "head89",
  SHIP = "ship92",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 160,
  LEVEL = 2,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 79,
  AVATAR = "head89",
  SHIP = "ship92",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 160,
  LEVEL = 3,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 79,
  AVATAR = "head89",
  SHIP = "ship92",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 160,
  LEVEL = 4,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 79,
  AVATAR = "head89",
  SHIP = "ship92",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 160,
  LEVEL = 5,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 79,
  AVATAR = "head89",
  SHIP = "ship92",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 160,
  LEVEL = 6,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 79,
  AVATAR = "head89",
  SHIP = "ship92",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 160,
  LEVEL = 7,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 79,
  AVATAR = "head89",
  SHIP = "ship92",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 160,
  LEVEL = 8,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 79,
  AVATAR = "head89",
  SHIP = "ship92",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 160,
  LEVEL = 9,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 79,
  AVATAR = "head89",
  SHIP = "ship92",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 160,
  LEVEL = 10,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 79,
  AVATAR = "head89",
  SHIP = "ship92",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 160,
  LEVEL = 11,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 79,
  AVATAR = "head89",
  SHIP = "ship92",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 160,
  LEVEL = 12,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 79,
  AVATAR = "head89",
  SHIP = "ship92",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 160,
  LEVEL = 13,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 79,
  AVATAR = "head89",
  SHIP = "ship92",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 160,
  LEVEL = 14,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 79,
  AVATAR = "head89",
  SHIP = "ship92",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 160,
  LEVEL = 15,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 79,
  AVATAR = "head89",
  SHIP = "ship92",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 160,
  LEVEL = 16,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 79,
  AVATAR = "head89",
  SHIP = "ship92",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 161,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 80,
  AVATAR = "head90",
  SHIP = "ship93",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 161,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 80,
  AVATAR = "head90",
  SHIP = "ship93",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 161,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 80,
  AVATAR = "head90",
  SHIP = "ship93",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 161,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 80,
  AVATAR = "head90",
  SHIP = "ship93",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 161,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 80,
  AVATAR = "head90",
  SHIP = "ship93",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 161,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 80,
  AVATAR = "head90",
  SHIP = "ship93",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 161,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 80,
  AVATAR = "head90",
  SHIP = "ship93",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 161,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 80,
  AVATAR = "head90",
  SHIP = "ship93",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 161,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 80,
  AVATAR = "head90",
  SHIP = "ship93",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 161,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 80,
  AVATAR = "head90",
  SHIP = "ship93",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 161,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 80,
  AVATAR = "head90",
  SHIP = "ship93",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 161,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 80,
  AVATAR = "head90",
  SHIP = "ship93",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 161,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 80,
  AVATAR = "head90",
  SHIP = "ship93",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 161,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 80,
  AVATAR = "head90",
  SHIP = "ship93",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 161,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 80,
  AVATAR = "head90",
  SHIP = "ship93",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 161,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 80,
  AVATAR = "head90",
  SHIP = "ship93",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 161,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 80,
  AVATAR = "head90",
  SHIP = "ship93",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 162,
  LEVEL = 0,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 81,
  AVATAR = "head91",
  SHIP = "ship94",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 162,
  LEVEL = 1,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 81,
  AVATAR = "head91",
  SHIP = "ship94",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 162,
  LEVEL = 2,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 81,
  AVATAR = "head91",
  SHIP = "ship94",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 162,
  LEVEL = 3,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 81,
  AVATAR = "head91",
  SHIP = "ship94",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 162,
  LEVEL = 4,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 81,
  AVATAR = "head91",
  SHIP = "ship94",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 162,
  LEVEL = 5,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 81,
  AVATAR = "head91",
  SHIP = "ship94",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 162,
  LEVEL = 6,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 81,
  AVATAR = "head91",
  SHIP = "ship94",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 162,
  LEVEL = 7,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 81,
  AVATAR = "head91",
  SHIP = "ship94",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 162,
  LEVEL = 8,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 81,
  AVATAR = "head91",
  SHIP = "ship94",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 162,
  LEVEL = 9,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 81,
  AVATAR = "head91",
  SHIP = "ship94",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 162,
  LEVEL = 10,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 81,
  AVATAR = "head91",
  SHIP = "ship94",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 162,
  LEVEL = 11,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 81,
  AVATAR = "head91",
  SHIP = "ship94",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 162,
  LEVEL = 12,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 81,
  AVATAR = "head91",
  SHIP = "ship94",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 162,
  LEVEL = 13,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 81,
  AVATAR = "head91",
  SHIP = "ship94",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 162,
  LEVEL = 14,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 81,
  AVATAR = "head91",
  SHIP = "ship94",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 162,
  LEVEL = 15,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 81,
  AVATAR = "head91",
  SHIP = "ship94",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 162,
  LEVEL = 16,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 81,
  AVATAR = "head91",
  SHIP = "ship94",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 163,
  LEVEL = 0,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 82,
  AVATAR = "head92",
  SHIP = "ship95",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 163,
  LEVEL = 1,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 82,
  AVATAR = "head92",
  SHIP = "ship95",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 163,
  LEVEL = 2,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 82,
  AVATAR = "head92",
  SHIP = "ship95",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 163,
  LEVEL = 3,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 82,
  AVATAR = "head92",
  SHIP = "ship95",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 163,
  LEVEL = 4,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 82,
  AVATAR = "head92",
  SHIP = "ship95",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 163,
  LEVEL = 5,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 82,
  AVATAR = "head92",
  SHIP = "ship95",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 163,
  LEVEL = 6,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 82,
  AVATAR = "head92",
  SHIP = "ship95",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 163,
  LEVEL = 7,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 82,
  AVATAR = "head92",
  SHIP = "ship95",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 163,
  LEVEL = 8,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 82,
  AVATAR = "head92",
  SHIP = "ship95",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 163,
  LEVEL = 9,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 82,
  AVATAR = "head92",
  SHIP = "ship95",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 163,
  LEVEL = 10,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 82,
  AVATAR = "head92",
  SHIP = "ship95",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 163,
  LEVEL = 11,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 82,
  AVATAR = "head92",
  SHIP = "ship95",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 163,
  LEVEL = 12,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 82,
  AVATAR = "head92",
  SHIP = "ship95",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 163,
  LEVEL = 13,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 82,
  AVATAR = "head92",
  SHIP = "ship95",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 163,
  LEVEL = 14,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 82,
  AVATAR = "head92",
  SHIP = "ship95",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 163,
  LEVEL = 15,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 82,
  AVATAR = "head92",
  SHIP = "ship95",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 163,
  LEVEL = 16,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 82,
  AVATAR = "head92",
  SHIP = "ship95",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 164,
  LEVEL = 0,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 83,
  AVATAR = "head93",
  SHIP = "ship96",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 164,
  LEVEL = 1,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 83,
  AVATAR = "head93",
  SHIP = "ship96",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 164,
  LEVEL = 2,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 83,
  AVATAR = "head93",
  SHIP = "ship96",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 164,
  LEVEL = 3,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 83,
  AVATAR = "head93",
  SHIP = "ship96",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 164,
  LEVEL = 4,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 83,
  AVATAR = "head93",
  SHIP = "ship96",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 164,
  LEVEL = 5,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 83,
  AVATAR = "head93",
  SHIP = "ship96",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 164,
  LEVEL = 6,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 83,
  AVATAR = "head93",
  SHIP = "ship96",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 164,
  LEVEL = 7,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 83,
  AVATAR = "head93",
  SHIP = "ship96",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 164,
  LEVEL = 8,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 83,
  AVATAR = "head93",
  SHIP = "ship96",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 164,
  LEVEL = 9,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 83,
  AVATAR = "head93",
  SHIP = "ship96",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 164,
  LEVEL = 10,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 83,
  AVATAR = "head93",
  SHIP = "ship96",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 164,
  LEVEL = 11,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 83,
  AVATAR = "head93",
  SHIP = "ship96",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 164,
  LEVEL = 12,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 83,
  AVATAR = "head93",
  SHIP = "ship96",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 164,
  LEVEL = 13,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 83,
  AVATAR = "head93",
  SHIP = "ship96",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 164,
  LEVEL = 14,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 83,
  AVATAR = "head93",
  SHIP = "ship96",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 164,
  LEVEL = 15,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 83,
  AVATAR = "head93",
  SHIP = "ship96",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 164,
  LEVEL = 16,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 83,
  AVATAR = "head93",
  SHIP = "ship96",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 165,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 84,
  AVATAR = "head94",
  SHIP = "ship97",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 165,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 84,
  AVATAR = "head94",
  SHIP = "ship97",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 165,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 84,
  AVATAR = "head94",
  SHIP = "ship97",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 165,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 84,
  AVATAR = "head94",
  SHIP = "ship97",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 165,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 84,
  AVATAR = "head94",
  SHIP = "ship97",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 165,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 84,
  AVATAR = "head94",
  SHIP = "ship97",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 165,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 84,
  AVATAR = "head94",
  SHIP = "ship97",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 165,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 84,
  AVATAR = "head94",
  SHIP = "ship97",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 165,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 84,
  AVATAR = "head94",
  SHIP = "ship97",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 165,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 84,
  AVATAR = "head94",
  SHIP = "ship97",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 165,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 84,
  AVATAR = "head94",
  SHIP = "ship97",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 165,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 84,
  AVATAR = "head94",
  SHIP = "ship97",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 165,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 84,
  AVATAR = "head94",
  SHIP = "ship97",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 165,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 84,
  AVATAR = "head94",
  SHIP = "ship97",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 165,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 84,
  AVATAR = "head94",
  SHIP = "ship97",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 165,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 84,
  AVATAR = "head94",
  SHIP = "ship97",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 165,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 84,
  AVATAR = "head94",
  SHIP = "ship97",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 166,
  LEVEL = 0,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 85,
  AVATAR = "head95",
  SHIP = "ship98",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 166,
  LEVEL = 1,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 85,
  AVATAR = "head95",
  SHIP = "ship98",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 166,
  LEVEL = 2,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 85,
  AVATAR = "head95",
  SHIP = "ship98",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 166,
  LEVEL = 3,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 85,
  AVATAR = "head95",
  SHIP = "ship98",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 166,
  LEVEL = 4,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 85,
  AVATAR = "head95",
  SHIP = "ship98",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 166,
  LEVEL = 5,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 85,
  AVATAR = "head95",
  SHIP = "ship98",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 166,
  LEVEL = 6,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 85,
  AVATAR = "head95",
  SHIP = "ship98",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 166,
  LEVEL = 7,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 85,
  AVATAR = "head95",
  SHIP = "ship98",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 166,
  LEVEL = 8,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 85,
  AVATAR = "head95",
  SHIP = "ship98",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 166,
  LEVEL = 9,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 85,
  AVATAR = "head95",
  SHIP = "ship98",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 166,
  LEVEL = 10,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 85,
  AVATAR = "head95",
  SHIP = "ship98",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 166,
  LEVEL = 11,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 85,
  AVATAR = "head95",
  SHIP = "ship98",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 166,
  LEVEL = 12,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 85,
  AVATAR = "head95",
  SHIP = "ship98",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 166,
  LEVEL = 13,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 85,
  AVATAR = "head95",
  SHIP = "ship98",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 166,
  LEVEL = 14,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 85,
  AVATAR = "head95",
  SHIP = "ship98",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 166,
  LEVEL = 15,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 85,
  AVATAR = "head95",
  SHIP = "ship98",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 166,
  LEVEL = 16,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 85,
  AVATAR = "head95",
  SHIP = "ship98",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 167,
  LEVEL = 0,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 86,
  AVATAR = "head96",
  SHIP = "ship99",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 167,
  LEVEL = 1,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 86,
  AVATAR = "head96",
  SHIP = "ship99",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 167,
  LEVEL = 2,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 86,
  AVATAR = "head96",
  SHIP = "ship99",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 167,
  LEVEL = 3,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 86,
  AVATAR = "head96",
  SHIP = "ship99",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 167,
  LEVEL = 4,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 86,
  AVATAR = "head96",
  SHIP = "ship99",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 167,
  LEVEL = 5,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 86,
  AVATAR = "head96",
  SHIP = "ship99",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 167,
  LEVEL = 6,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 86,
  AVATAR = "head96",
  SHIP = "ship99",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 167,
  LEVEL = 7,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 86,
  AVATAR = "head96",
  SHIP = "ship99",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 167,
  LEVEL = 8,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 86,
  AVATAR = "head96",
  SHIP = "ship99",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 167,
  LEVEL = 9,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 86,
  AVATAR = "head96",
  SHIP = "ship99",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 167,
  LEVEL = 10,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 86,
  AVATAR = "head96",
  SHIP = "ship99",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 167,
  LEVEL = 11,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 86,
  AVATAR = "head96",
  SHIP = "ship99",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 167,
  LEVEL = 12,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 86,
  AVATAR = "head96",
  SHIP = "ship99",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 167,
  LEVEL = 13,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 86,
  AVATAR = "head96",
  SHIP = "ship99",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 167,
  LEVEL = 14,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 86,
  AVATAR = "head96",
  SHIP = "ship99",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 167,
  LEVEL = 15,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 86,
  AVATAR = "head96",
  SHIP = "ship99",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 167,
  LEVEL = 16,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 86,
  AVATAR = "head96",
  SHIP = "ship99",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 168,
  LEVEL = 0,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 87,
  AVATAR = "head97",
  SHIP = "ship100",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 168,
  LEVEL = 1,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 87,
  AVATAR = "head97",
  SHIP = "ship100",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 168,
  LEVEL = 2,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 87,
  AVATAR = "head97",
  SHIP = "ship100",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 168,
  LEVEL = 3,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 87,
  AVATAR = "head97",
  SHIP = "ship100",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 168,
  LEVEL = 4,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 87,
  AVATAR = "head97",
  SHIP = "ship100",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 168,
  LEVEL = 5,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 87,
  AVATAR = "head97",
  SHIP = "ship100",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 168,
  LEVEL = 6,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 87,
  AVATAR = "head97",
  SHIP = "ship100",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 168,
  LEVEL = 7,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 87,
  AVATAR = "head97",
  SHIP = "ship100",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 168,
  LEVEL = 8,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 87,
  AVATAR = "head97",
  SHIP = "ship100",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 168,
  LEVEL = 9,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 87,
  AVATAR = "head97",
  SHIP = "ship100",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 168,
  LEVEL = 10,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 87,
  AVATAR = "head97",
  SHIP = "ship100",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 168,
  LEVEL = 11,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 87,
  AVATAR = "head97",
  SHIP = "ship100",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 168,
  LEVEL = 12,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 87,
  AVATAR = "head97",
  SHIP = "ship100",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 168,
  LEVEL = 13,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 87,
  AVATAR = "head97",
  SHIP = "ship100",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 168,
  LEVEL = 14,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 87,
  AVATAR = "head97",
  SHIP = "ship100",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 168,
  LEVEL = 15,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 87,
  AVATAR = "head97",
  SHIP = "ship100",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 168,
  LEVEL = 16,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 87,
  AVATAR = "head97",
  SHIP = "ship100",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 169,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 88,
  AVATAR = "head98",
  SHIP = "ship101",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 169,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 88,
  AVATAR = "head98",
  SHIP = "ship101",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 169,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 88,
  AVATAR = "head98",
  SHIP = "ship101",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 169,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 88,
  AVATAR = "head98",
  SHIP = "ship101",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 169,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 88,
  AVATAR = "head98",
  SHIP = "ship101",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 169,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 88,
  AVATAR = "head98",
  SHIP = "ship101",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 169,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 88,
  AVATAR = "head98",
  SHIP = "ship101",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 169,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 88,
  AVATAR = "head98",
  SHIP = "ship101",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 169,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 88,
  AVATAR = "head98",
  SHIP = "ship101",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 169,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 88,
  AVATAR = "head98",
  SHIP = "ship101",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 169,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 88,
  AVATAR = "head98",
  SHIP = "ship101",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 169,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 88,
  AVATAR = "head98",
  SHIP = "ship101",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 169,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 88,
  AVATAR = "head98",
  SHIP = "ship101",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 169,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 88,
  AVATAR = "head98",
  SHIP = "ship101",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 169,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 88,
  AVATAR = "head98",
  SHIP = "ship101",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 169,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 88,
  AVATAR = "head98",
  SHIP = "ship101",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 169,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 88,
  AVATAR = "head98",
  SHIP = "ship101",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 170,
  LEVEL = 0,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 89,
  AVATAR = "head99",
  SHIP = "ship102",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 170,
  LEVEL = 1,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 89,
  AVATAR = "head99",
  SHIP = "ship102",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 170,
  LEVEL = 2,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 89,
  AVATAR = "head99",
  SHIP = "ship102",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 170,
  LEVEL = 3,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 89,
  AVATAR = "head99",
  SHIP = "ship102",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 170,
  LEVEL = 4,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 89,
  AVATAR = "head99",
  SHIP = "ship102",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 170,
  LEVEL = 5,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 89,
  AVATAR = "head99",
  SHIP = "ship102",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 170,
  LEVEL = 6,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 89,
  AVATAR = "head99",
  SHIP = "ship102",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 170,
  LEVEL = 7,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 89,
  AVATAR = "head99",
  SHIP = "ship102",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 170,
  LEVEL = 8,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 89,
  AVATAR = "head99",
  SHIP = "ship102",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 170,
  LEVEL = 9,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 89,
  AVATAR = "head99",
  SHIP = "ship102",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 170,
  LEVEL = 10,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 89,
  AVATAR = "head99",
  SHIP = "ship102",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 170,
  LEVEL = 11,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 89,
  AVATAR = "head99",
  SHIP = "ship102",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 170,
  LEVEL = 12,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 89,
  AVATAR = "head99",
  SHIP = "ship102",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 170,
  LEVEL = 13,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 89,
  AVATAR = "head99",
  SHIP = "ship102",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 170,
  LEVEL = 14,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 89,
  AVATAR = "head99",
  SHIP = "ship102",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 170,
  LEVEL = 15,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 89,
  AVATAR = "head99",
  SHIP = "ship102",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 170,
  LEVEL = 16,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 89,
  AVATAR = "head99",
  SHIP = "ship102",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 171,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 90,
  AVATAR = "head100",
  SHIP = "ship103",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 171,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 90,
  AVATAR = "head100",
  SHIP = "ship103",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 171,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 90,
  AVATAR = "head100",
  SHIP = "ship103",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 171,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 90,
  AVATAR = "head100",
  SHIP = "ship103",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 171,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 90,
  AVATAR = "head100",
  SHIP = "ship103",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 171,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 90,
  AVATAR = "head100",
  SHIP = "ship103",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 171,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 90,
  AVATAR = "head100",
  SHIP = "ship103",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 171,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 90,
  AVATAR = "head100",
  SHIP = "ship103",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 171,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 90,
  AVATAR = "head100",
  SHIP = "ship103",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 171,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 90,
  AVATAR = "head100",
  SHIP = "ship103",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 171,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 90,
  AVATAR = "head100",
  SHIP = "ship103",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 171,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 90,
  AVATAR = "head100",
  SHIP = "ship103",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 171,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 90,
  AVATAR = "head100",
  SHIP = "ship103",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 171,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 90,
  AVATAR = "head100",
  SHIP = "ship103",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 171,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 90,
  AVATAR = "head100",
  SHIP = "ship103",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 171,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 90,
  AVATAR = "head100",
  SHIP = "ship103",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 171,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 90,
  AVATAR = "head100",
  SHIP = "ship103",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 172,
  LEVEL = 0,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 92,
  AVATAR = "head101",
  SHIP = "ship104",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 172,
  LEVEL = 1,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 92,
  AVATAR = "head101",
  SHIP = "ship104",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 172,
  LEVEL = 2,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 92,
  AVATAR = "head101",
  SHIP = "ship104",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 172,
  LEVEL = 3,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 92,
  AVATAR = "head101",
  SHIP = "ship104",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 172,
  LEVEL = 4,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 92,
  AVATAR = "head101",
  SHIP = "ship104",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 172,
  LEVEL = 5,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 92,
  AVATAR = "head101",
  SHIP = "ship104",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 172,
  LEVEL = 6,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 92,
  AVATAR = "head101",
  SHIP = "ship104",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 172,
  LEVEL = 7,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 92,
  AVATAR = "head101",
  SHIP = "ship104",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 172,
  LEVEL = 8,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 92,
  AVATAR = "head101",
  SHIP = "ship104",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 172,
  LEVEL = 9,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 92,
  AVATAR = "head101",
  SHIP = "ship104",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 172,
  LEVEL = 10,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 92,
  AVATAR = "head101",
  SHIP = "ship104",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 172,
  LEVEL = 11,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 92,
  AVATAR = "head101",
  SHIP = "ship104",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 172,
  LEVEL = 12,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 92,
  AVATAR = "head101",
  SHIP = "ship104",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 172,
  LEVEL = 13,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 92,
  AVATAR = "head101",
  SHIP = "ship104",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 172,
  LEVEL = 14,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 92,
  AVATAR = "head101",
  SHIP = "ship104",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 172,
  LEVEL = 15,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 92,
  AVATAR = "head101",
  SHIP = "ship104",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 172,
  LEVEL = 16,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 92,
  AVATAR = "head101",
  SHIP = "ship104",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 173,
  LEVEL = 0,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 93,
  AVATAR = "head102",
  SHIP = "ship105",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 173,
  LEVEL = 1,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 93,
  AVATAR = "head102",
  SHIP = "ship105",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 173,
  LEVEL = 2,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 93,
  AVATAR = "head102",
  SHIP = "ship105",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 173,
  LEVEL = 3,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 93,
  AVATAR = "head102",
  SHIP = "ship105",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 173,
  LEVEL = 4,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 93,
  AVATAR = "head102",
  SHIP = "ship105",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 173,
  LEVEL = 5,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 93,
  AVATAR = "head102",
  SHIP = "ship105",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 173,
  LEVEL = 6,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 93,
  AVATAR = "head102",
  SHIP = "ship105",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 173,
  LEVEL = 7,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 93,
  AVATAR = "head102",
  SHIP = "ship105",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 173,
  LEVEL = 8,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 93,
  AVATAR = "head102",
  SHIP = "ship105",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 173,
  LEVEL = 9,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 93,
  AVATAR = "head102",
  SHIP = "ship105",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 173,
  LEVEL = 10,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 93,
  AVATAR = "head102",
  SHIP = "ship105",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 173,
  LEVEL = 11,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 93,
  AVATAR = "head102",
  SHIP = "ship105",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 173,
  LEVEL = 12,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 93,
  AVATAR = "head102",
  SHIP = "ship105",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 173,
  LEVEL = 13,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 93,
  AVATAR = "head102",
  SHIP = "ship105",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 173,
  LEVEL = 14,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 93,
  AVATAR = "head102",
  SHIP = "ship105",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 173,
  LEVEL = 15,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 93,
  AVATAR = "head102",
  SHIP = "ship105",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 173,
  LEVEL = 16,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 93,
  AVATAR = "head102",
  SHIP = "ship105",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 174,
  LEVEL = 0,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 94,
  AVATAR = "head103",
  SHIP = "ship106",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 174,
  LEVEL = 1,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 94,
  AVATAR = "head103",
  SHIP = "ship106",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 174,
  LEVEL = 2,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 94,
  AVATAR = "head103",
  SHIP = "ship106",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 174,
  LEVEL = 3,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 94,
  AVATAR = "head103",
  SHIP = "ship106",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 174,
  LEVEL = 4,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 94,
  AVATAR = "head103",
  SHIP = "ship106",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 174,
  LEVEL = 5,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 94,
  AVATAR = "head103",
  SHIP = "ship106",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 174,
  LEVEL = 6,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 94,
  AVATAR = "head103",
  SHIP = "ship106",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 174,
  LEVEL = 7,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 94,
  AVATAR = "head103",
  SHIP = "ship106",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 174,
  LEVEL = 8,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 94,
  AVATAR = "head103",
  SHIP = "ship106",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 174,
  LEVEL = 9,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 94,
  AVATAR = "head103",
  SHIP = "ship106",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 174,
  LEVEL = 10,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 94,
  AVATAR = "head103",
  SHIP = "ship106",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 174,
  LEVEL = 11,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 94,
  AVATAR = "head103",
  SHIP = "ship106",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 174,
  LEVEL = 12,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 94,
  AVATAR = "head103",
  SHIP = "ship106",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 174,
  LEVEL = 13,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 94,
  AVATAR = "head103",
  SHIP = "ship106",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 174,
  LEVEL = 14,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 94,
  AVATAR = "head103",
  SHIP = "ship106",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 174,
  LEVEL = 15,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 94,
  AVATAR = "head103",
  SHIP = "ship106",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 174,
  LEVEL = 16,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 94,
  AVATAR = "head103",
  SHIP = "ship106",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 175,
  LEVEL = 0,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 95,
  AVATAR = "head104",
  SHIP = "ship107",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 175,
  LEVEL = 1,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 95,
  AVATAR = "head104",
  SHIP = "ship107",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 175,
  LEVEL = 2,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 95,
  AVATAR = "head104",
  SHIP = "ship107",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 175,
  LEVEL = 3,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 95,
  AVATAR = "head104",
  SHIP = "ship107",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 175,
  LEVEL = 4,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 95,
  AVATAR = "head104",
  SHIP = "ship107",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 175,
  LEVEL = 5,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 95,
  AVATAR = "head104",
  SHIP = "ship107",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 175,
  LEVEL = 6,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 95,
  AVATAR = "head104",
  SHIP = "ship107",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 175,
  LEVEL = 7,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 10095,
  AVATAR = "head104",
  SHIP = "ship107",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 175,
  LEVEL = 8,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 10095,
  AVATAR = "head104",
  SHIP = "ship107",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 175,
  LEVEL = 9,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 10095,
  AVATAR = "head104",
  SHIP = "ship107",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 175,
  LEVEL = 10,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 11095,
  AVATAR = "head104",
  SHIP = "ship107",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 175,
  LEVEL = 11,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 11095,
  AVATAR = "head104",
  SHIP = "ship107",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 175,
  LEVEL = 12,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 11095,
  AVATAR = "head104",
  SHIP = "ship107",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 175,
  LEVEL = 13,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 11095,
  AVATAR = "head104",
  SHIP = "ship107",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 175,
  LEVEL = 14,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 11095,
  AVATAR = "head104",
  SHIP = "ship107",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 175,
  LEVEL = 15,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 12095,
  AVATAR = "head104",
  SHIP = "ship107",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 175,
  LEVEL = 16,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 12095,
  AVATAR = "head104",
  SHIP = "ship107",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 176,
  LEVEL = 0,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 97,
  AVATAR = "head105",
  SHIP = "ship108",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 176,
  LEVEL = 1,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 97,
  AVATAR = "head105",
  SHIP = "ship108",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 176,
  LEVEL = 2,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 97,
  AVATAR = "head105",
  SHIP = "ship108",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 176,
  LEVEL = 3,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 97,
  AVATAR = "head105",
  SHIP = "ship108",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 176,
  LEVEL = 4,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 97,
  AVATAR = "head105",
  SHIP = "ship108",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 176,
  LEVEL = 5,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 97,
  AVATAR = "head105",
  SHIP = "ship108",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 176,
  LEVEL = 6,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 97,
  AVATAR = "head105",
  SHIP = "ship108",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 176,
  LEVEL = 7,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 97,
  AVATAR = "head105",
  SHIP = "ship108",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 176,
  LEVEL = 8,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 97,
  AVATAR = "head105",
  SHIP = "ship108",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 176,
  LEVEL = 9,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 97,
  AVATAR = "head105",
  SHIP = "ship108",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 176,
  LEVEL = 10,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 97,
  AVATAR = "head105",
  SHIP = "ship108",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 176,
  LEVEL = 11,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 97,
  AVATAR = "head105",
  SHIP = "ship108",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 176,
  LEVEL = 12,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 97,
  AVATAR = "head105",
  SHIP = "ship108",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 176,
  LEVEL = 13,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 97,
  AVATAR = "head105",
  SHIP = "ship108",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 176,
  LEVEL = 14,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 97,
  AVATAR = "head105",
  SHIP = "ship108",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 176,
  LEVEL = 15,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 97,
  AVATAR = "head105",
  SHIP = "ship108",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 176,
  LEVEL = 16,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 97,
  AVATAR = "head105",
  SHIP = "ship108",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 177,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 98,
  AVATAR = "head106",
  SHIP = "ship109",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 177,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 98,
  AVATAR = "head106",
  SHIP = "ship109",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 177,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 98,
  AVATAR = "head106",
  SHIP = "ship109",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 177,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 98,
  AVATAR = "head106",
  SHIP = "ship109",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 177,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 98,
  AVATAR = "head106",
  SHIP = "ship109",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 177,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 98,
  AVATAR = "head106",
  SHIP = "ship109",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 177,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 98,
  AVATAR = "head106",
  SHIP = "ship109",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 177,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 98,
  AVATAR = "head106",
  SHIP = "ship109",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 177,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 98,
  AVATAR = "head106",
  SHIP = "ship109",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 177,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 98,
  AVATAR = "head106",
  SHIP = "ship109",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 177,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 98,
  AVATAR = "head106",
  SHIP = "ship109",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 177,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 98,
  AVATAR = "head106",
  SHIP = "ship109",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 177,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 98,
  AVATAR = "head106",
  SHIP = "ship109",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 177,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 98,
  AVATAR = "head106",
  SHIP = "ship109",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 177,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 98,
  AVATAR = "head106",
  SHIP = "ship109",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 177,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 98,
  AVATAR = "head106",
  SHIP = "ship109",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 177,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 98,
  AVATAR = "head106",
  SHIP = "ship109",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 178,
  LEVEL = 0,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 99,
  AVATAR = "head107",
  SHIP = "ship110",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 178,
  LEVEL = 1,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 99,
  AVATAR = "head107",
  SHIP = "ship110",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 178,
  LEVEL = 2,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 99,
  AVATAR = "head107",
  SHIP = "ship110",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 178,
  LEVEL = 3,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 99,
  AVATAR = "head107",
  SHIP = "ship110",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 178,
  LEVEL = 4,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 99,
  AVATAR = "head107",
  SHIP = "ship110",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 178,
  LEVEL = 5,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 99,
  AVATAR = "head107",
  SHIP = "ship110",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 178,
  LEVEL = 6,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 99,
  AVATAR = "head107",
  SHIP = "ship110",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 178,
  LEVEL = 7,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 99,
  AVATAR = "head107",
  SHIP = "ship110",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 178,
  LEVEL = 8,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 99,
  AVATAR = "head107",
  SHIP = "ship110",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 178,
  LEVEL = 9,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 99,
  AVATAR = "head107",
  SHIP = "ship110",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 178,
  LEVEL = 10,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 99,
  AVATAR = "head107",
  SHIP = "ship110",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 178,
  LEVEL = 11,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 99,
  AVATAR = "head107",
  SHIP = "ship110",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 178,
  LEVEL = 12,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 99,
  AVATAR = "head107",
  SHIP = "ship110",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 178,
  LEVEL = 13,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 99,
  AVATAR = "head107",
  SHIP = "ship110",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 178,
  LEVEL = 14,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 99,
  AVATAR = "head107",
  SHIP = "ship110",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 178,
  LEVEL = 15,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 99,
  AVATAR = "head107",
  SHIP = "ship110",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 178,
  LEVEL = 16,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 99,
  AVATAR = "head107",
  SHIP = "ship110",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 179,
  LEVEL = 0,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 103,
  AVATAR = "head108",
  SHIP = "ship111",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 179,
  LEVEL = 1,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 103,
  AVATAR = "head108",
  SHIP = "ship111",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 179,
  LEVEL = 2,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 103,
  AVATAR = "head108",
  SHIP = "ship111",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 179,
  LEVEL = 3,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 103,
  AVATAR = "head108",
  SHIP = "ship111",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 179,
  LEVEL = 4,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 103,
  AVATAR = "head108",
  SHIP = "ship111",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 179,
  LEVEL = 5,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 103,
  AVATAR = "head108",
  SHIP = "ship111",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 179,
  LEVEL = 6,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 103,
  AVATAR = "head108",
  SHIP = "ship111",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 179,
  LEVEL = 7,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 103,
  AVATAR = "head108",
  SHIP = "ship111",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 179,
  LEVEL = 8,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 103,
  AVATAR = "head108",
  SHIP = "ship111",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 179,
  LEVEL = 9,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 103,
  AVATAR = "head108",
  SHIP = "ship111",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 179,
  LEVEL = 10,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 103,
  AVATAR = "head108",
  SHIP = "ship111",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 179,
  LEVEL = 11,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 103,
  AVATAR = "head108",
  SHIP = "ship111",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 179,
  LEVEL = 12,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 103,
  AVATAR = "head108",
  SHIP = "ship111",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 179,
  LEVEL = 13,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 103,
  AVATAR = "head108",
  SHIP = "ship111",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 179,
  LEVEL = 14,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 103,
  AVATAR = "head108",
  SHIP = "ship111",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 179,
  LEVEL = 15,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 103,
  AVATAR = "head108",
  SHIP = "ship111",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 179,
  LEVEL = 16,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 103,
  AVATAR = "head108",
  SHIP = "ship111",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 180,
  LEVEL = 0,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 104,
  AVATAR = "head109",
  SHIP = "ship112",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 180,
  LEVEL = 1,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 104,
  AVATAR = "head109",
  SHIP = "ship112",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 180,
  LEVEL = 2,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 104,
  AVATAR = "head109",
  SHIP = "ship112",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 180,
  LEVEL = 3,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 104,
  AVATAR = "head109",
  SHIP = "ship112",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 180,
  LEVEL = 4,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 104,
  AVATAR = "head109",
  SHIP = "ship112",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 180,
  LEVEL = 5,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 104,
  AVATAR = "head109",
  SHIP = "ship112",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 180,
  LEVEL = 6,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 104,
  AVATAR = "head109",
  SHIP = "ship112",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 180,
  LEVEL = 7,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 104,
  AVATAR = "head109",
  SHIP = "ship112",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 180,
  LEVEL = 8,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 104,
  AVATAR = "head109",
  SHIP = "ship112",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 180,
  LEVEL = 9,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 104,
  AVATAR = "head109",
  SHIP = "ship112",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 180,
  LEVEL = 10,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 104,
  AVATAR = "head109",
  SHIP = "ship112",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 180,
  LEVEL = 11,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 104,
  AVATAR = "head109",
  SHIP = "ship112",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 180,
  LEVEL = 12,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 104,
  AVATAR = "head109",
  SHIP = "ship112",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 180,
  LEVEL = 13,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 104,
  AVATAR = "head109",
  SHIP = "ship112",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 180,
  LEVEL = 14,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 104,
  AVATAR = "head109",
  SHIP = "ship112",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 180,
  LEVEL = 15,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 104,
  AVATAR = "head109",
  SHIP = "ship112",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 180,
  LEVEL = 16,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 104,
  AVATAR = "head109",
  SHIP = "ship112",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 181,
  LEVEL = 0,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 105,
  AVATAR = "head110",
  SHIP = "ship113",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 181,
  LEVEL = 1,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 105,
  AVATAR = "head110",
  SHIP = "ship113",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 181,
  LEVEL = 2,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 105,
  AVATAR = "head110",
  SHIP = "ship113",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 181,
  LEVEL = 3,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 105,
  AVATAR = "head110",
  SHIP = "ship113",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 181,
  LEVEL = 4,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 105,
  AVATAR = "head110",
  SHIP = "ship113",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 181,
  LEVEL = 5,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 105,
  AVATAR = "head110",
  SHIP = "ship113",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 181,
  LEVEL = 6,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 105,
  AVATAR = "head110",
  SHIP = "ship113",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 181,
  LEVEL = 7,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 105,
  AVATAR = "head110",
  SHIP = "ship113",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 181,
  LEVEL = 8,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 105,
  AVATAR = "head110",
  SHIP = "ship113",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 181,
  LEVEL = 9,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 105,
  AVATAR = "head110",
  SHIP = "ship113",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 181,
  LEVEL = 10,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 105,
  AVATAR = "head110",
  SHIP = "ship113",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 181,
  LEVEL = 11,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 105,
  AVATAR = "head110",
  SHIP = "ship113",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 181,
  LEVEL = 12,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 105,
  AVATAR = "head110",
  SHIP = "ship113",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 181,
  LEVEL = 13,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 105,
  AVATAR = "head110",
  SHIP = "ship113",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 181,
  LEVEL = 14,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 105,
  AVATAR = "head110",
  SHIP = "ship113",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 181,
  LEVEL = 15,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 105,
  AVATAR = "head110",
  SHIP = "ship113",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 181,
  LEVEL = 16,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 105,
  AVATAR = "head110",
  SHIP = "ship113",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 182,
  LEVEL = 0,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 106,
  AVATAR = "head111",
  SHIP = "ship114",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 182,
  LEVEL = 1,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 106,
  AVATAR = "head111",
  SHIP = "ship114",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 182,
  LEVEL = 2,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 106,
  AVATAR = "head111",
  SHIP = "ship114",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 182,
  LEVEL = 3,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 106,
  AVATAR = "head111",
  SHIP = "ship114",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 182,
  LEVEL = 4,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 106,
  AVATAR = "head111",
  SHIP = "ship114",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 182,
  LEVEL = 5,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 106,
  AVATAR = "head111",
  SHIP = "ship114",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 182,
  LEVEL = 6,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 106,
  AVATAR = "head111",
  SHIP = "ship114",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 182,
  LEVEL = 7,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 106,
  AVATAR = "head111",
  SHIP = "ship114",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 182,
  LEVEL = 8,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 106,
  AVATAR = "head111",
  SHIP = "ship114",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 182,
  LEVEL = 9,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 106,
  AVATAR = "head111",
  SHIP = "ship114",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 182,
  LEVEL = 10,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 106,
  AVATAR = "head111",
  SHIP = "ship114",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 182,
  LEVEL = 11,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 106,
  AVATAR = "head111",
  SHIP = "ship114",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 182,
  LEVEL = 12,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 106,
  AVATAR = "head111",
  SHIP = "ship114",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 182,
  LEVEL = 13,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 106,
  AVATAR = "head111",
  SHIP = "ship114",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 182,
  LEVEL = 14,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 106,
  AVATAR = "head111",
  SHIP = "ship114",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 182,
  LEVEL = 15,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 106,
  AVATAR = "head111",
  SHIP = "ship114",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 182,
  LEVEL = 16,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 106,
  AVATAR = "head111",
  SHIP = "ship114",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 183,
  LEVEL = 0,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 107,
  AVATAR = "head112",
  SHIP = "ship115",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 183,
  LEVEL = 1,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 107,
  AVATAR = "head112",
  SHIP = "ship115",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 183,
  LEVEL = 2,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 107,
  AVATAR = "head112",
  SHIP = "ship115",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 183,
  LEVEL = 3,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 107,
  AVATAR = "head112",
  SHIP = "ship115",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 183,
  LEVEL = 4,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 107,
  AVATAR = "head112",
  SHIP = "ship115",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 183,
  LEVEL = 5,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 107,
  AVATAR = "head112",
  SHIP = "ship115",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 183,
  LEVEL = 6,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 107,
  AVATAR = "head112",
  SHIP = "ship115",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 183,
  LEVEL = 7,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 107,
  AVATAR = "head112",
  SHIP = "ship115",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 183,
  LEVEL = 8,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 107,
  AVATAR = "head112",
  SHIP = "ship115",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 183,
  LEVEL = 9,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 107,
  AVATAR = "head112",
  SHIP = "ship115",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 183,
  LEVEL = 10,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 107,
  AVATAR = "head112",
  SHIP = "ship115",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 183,
  LEVEL = 11,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 107,
  AVATAR = "head112",
  SHIP = "ship115",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 183,
  LEVEL = 12,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 107,
  AVATAR = "head112",
  SHIP = "ship115",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 183,
  LEVEL = 13,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 107,
  AVATAR = "head112",
  SHIP = "ship115",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 183,
  LEVEL = 14,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 107,
  AVATAR = "head112",
  SHIP = "ship115",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 183,
  LEVEL = 15,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 107,
  AVATAR = "head112",
  SHIP = "ship115",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 183,
  LEVEL = 16,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 107,
  AVATAR = "head112",
  SHIP = "ship115",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 184,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 108,
  AVATAR = "head113",
  SHIP = "ship116",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 184,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 108,
  AVATAR = "head113",
  SHIP = "ship116",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 184,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 108,
  AVATAR = "head113",
  SHIP = "ship116",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 184,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 108,
  AVATAR = "head113",
  SHIP = "ship116",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 184,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 108,
  AVATAR = "head113",
  SHIP = "ship116",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 184,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 108,
  AVATAR = "head113",
  SHIP = "ship116",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 184,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 108,
  AVATAR = "head113",
  SHIP = "ship116",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 184,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 108,
  AVATAR = "head113",
  SHIP = "ship116",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 184,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 108,
  AVATAR = "head113",
  SHIP = "ship116",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 184,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 108,
  AVATAR = "head113",
  SHIP = "ship116",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 184,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 108,
  AVATAR = "head113",
  SHIP = "ship116",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 184,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 108,
  AVATAR = "head113",
  SHIP = "ship116",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 184,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 108,
  AVATAR = "head113",
  SHIP = "ship116",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 184,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 108,
  AVATAR = "head113",
  SHIP = "ship116",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 184,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 108,
  AVATAR = "head113",
  SHIP = "ship116",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 184,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 108,
  AVATAR = "head113",
  SHIP = "ship116",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 184,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 108,
  AVATAR = "head113",
  SHIP = "ship116",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 185,
  LEVEL = 0,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 109,
  AVATAR = "head114",
  SHIP = "ship117",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 185,
  LEVEL = 1,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 10109,
  AVATAR = "head114",
  SHIP = "ship117",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 185,
  LEVEL = 2,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 10109,
  AVATAR = "head114",
  SHIP = "ship117",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 185,
  LEVEL = 3,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 11109,
  AVATAR = "head114",
  SHIP = "ship117",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 185,
  LEVEL = 4,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 11109,
  AVATAR = "head114",
  SHIP = "ship117",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 185,
  LEVEL = 5,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 12109,
  AVATAR = "head114",
  SHIP = "ship117",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 185,
  LEVEL = 6,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 12109,
  AVATAR = "head114",
  SHIP = "ship117",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 185,
  LEVEL = 7,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 13109,
  AVATAR = "head114",
  SHIP = "ship117",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 185,
  LEVEL = 8,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 13109,
  AVATAR = "head114",
  SHIP = "ship117",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 185,
  LEVEL = 9,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 13109,
  AVATAR = "head114",
  SHIP = "ship117",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 185,
  LEVEL = 10,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 14109,
  AVATAR = "head114",
  SHIP = "ship117",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 185,
  LEVEL = 11,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 14109,
  AVATAR = "head114",
  SHIP = "ship117",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 185,
  LEVEL = 12,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 14109,
  AVATAR = "head114",
  SHIP = "ship117",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 185,
  LEVEL = 13,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 14109,
  AVATAR = "head114",
  SHIP = "ship117",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 185,
  LEVEL = 14,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 14109,
  AVATAR = "head114",
  SHIP = "ship117",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 185,
  LEVEL = 15,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 14109,
  AVATAR = "head114",
  SHIP = "ship117",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 185,
  LEVEL = 16,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 14109,
  AVATAR = "head114",
  SHIP = "ship117",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 186,
  LEVEL = 0,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 110,
  AVATAR = "head115",
  SHIP = "ship118",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 186,
  LEVEL = 1,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 110,
  AVATAR = "head115",
  SHIP = "ship118",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 186,
  LEVEL = 2,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 110,
  AVATAR = "head115",
  SHIP = "ship118",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 186,
  LEVEL = 3,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 110,
  AVATAR = "head115",
  SHIP = "ship118",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 186,
  LEVEL = 4,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 110,
  AVATAR = "head115",
  SHIP = "ship118",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 186,
  LEVEL = 5,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 110,
  AVATAR = "head115",
  SHIP = "ship118",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 186,
  LEVEL = 6,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 110,
  AVATAR = "head115",
  SHIP = "ship118",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 186,
  LEVEL = 7,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 110,
  AVATAR = "head115",
  SHIP = "ship118",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 186,
  LEVEL = 8,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 110,
  AVATAR = "head115",
  SHIP = "ship118",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 186,
  LEVEL = 9,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 110,
  AVATAR = "head115",
  SHIP = "ship118",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 186,
  LEVEL = 10,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 110,
  AVATAR = "head115",
  SHIP = "ship118",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 186,
  LEVEL = 11,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 110,
  AVATAR = "head115",
  SHIP = "ship118",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 186,
  LEVEL = 12,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 110,
  AVATAR = "head115",
  SHIP = "ship118",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 186,
  LEVEL = 13,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 110,
  AVATAR = "head115",
  SHIP = "ship118",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 186,
  LEVEL = 14,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 110,
  AVATAR = "head115",
  SHIP = "ship118",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 186,
  LEVEL = 15,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 110,
  AVATAR = "head115",
  SHIP = "ship118",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 186,
  LEVEL = 16,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 110,
  AVATAR = "head115",
  SHIP = "ship118",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 187,
  LEVEL = 0,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 111,
  AVATAR = "head116",
  SHIP = "ship119",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 187,
  LEVEL = 1,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 111,
  AVATAR = "head116",
  SHIP = "ship119",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 187,
  LEVEL = 2,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 111,
  AVATAR = "head116",
  SHIP = "ship119",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 187,
  LEVEL = 3,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 111,
  AVATAR = "head116",
  SHIP = "ship119",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 187,
  LEVEL = 4,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 111,
  AVATAR = "head116",
  SHIP = "ship119",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 187,
  LEVEL = 5,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 111,
  AVATAR = "head116",
  SHIP = "ship119",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 187,
  LEVEL = 6,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 111,
  AVATAR = "head116",
  SHIP = "ship119",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 187,
  LEVEL = 7,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 111,
  AVATAR = "head116",
  SHIP = "ship119",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 187,
  LEVEL = 8,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 111,
  AVATAR = "head116",
  SHIP = "ship119",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 187,
  LEVEL = 9,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 111,
  AVATAR = "head116",
  SHIP = "ship119",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 187,
  LEVEL = 10,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 111,
  AVATAR = "head116",
  SHIP = "ship119",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 187,
  LEVEL = 11,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 111,
  AVATAR = "head116",
  SHIP = "ship119",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 187,
  LEVEL = 12,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 111,
  AVATAR = "head116",
  SHIP = "ship119",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 187,
  LEVEL = 13,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 111,
  AVATAR = "head116",
  SHIP = "ship119",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 187,
  LEVEL = 14,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 111,
  AVATAR = "head116",
  SHIP = "ship119",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 187,
  LEVEL = 15,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 111,
  AVATAR = "head116",
  SHIP = "ship119",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 187,
  LEVEL = 16,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 111,
  AVATAR = "head116",
  SHIP = "ship119",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 188,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 113,
  AVATAR = "head118",
  SHIP = "ship121",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 188,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 113,
  AVATAR = "head118",
  SHIP = "ship121",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 188,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 113,
  AVATAR = "head118",
  SHIP = "ship121",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 188,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 113,
  AVATAR = "head118",
  SHIP = "ship121",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 188,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 113,
  AVATAR = "head118",
  SHIP = "ship121",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 188,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 113,
  AVATAR = "head118",
  SHIP = "ship121",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 188,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 113,
  AVATAR = "head118",
  SHIP = "ship121",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 188,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 113,
  AVATAR = "head118",
  SHIP = "ship121",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 188,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 113,
  AVATAR = "head118",
  SHIP = "ship121",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 188,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 113,
  AVATAR = "head118",
  SHIP = "ship121",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 188,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 113,
  AVATAR = "head118",
  SHIP = "ship121",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 188,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 113,
  AVATAR = "head118",
  SHIP = "ship121",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 188,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 113,
  AVATAR = "head118",
  SHIP = "ship121",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 188,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 113,
  AVATAR = "head118",
  SHIP = "ship121",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 188,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 113,
  AVATAR = "head118",
  SHIP = "ship121",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 188,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 113,
  AVATAR = "head118",
  SHIP = "ship121",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 188,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 113,
  AVATAR = "head118",
  SHIP = "ship121",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 189,
  LEVEL = 0,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 114,
  AVATAR = "head119",
  SHIP = "ship122",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 189,
  LEVEL = 1,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 114,
  AVATAR = "head119",
  SHIP = "ship122",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 189,
  LEVEL = 2,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 114,
  AVATAR = "head119",
  SHIP = "ship122",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 189,
  LEVEL = 3,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 114,
  AVATAR = "head119",
  SHIP = "ship122",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 189,
  LEVEL = 4,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 114,
  AVATAR = "head119",
  SHIP = "ship122",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 189,
  LEVEL = 5,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 114,
  AVATAR = "head119",
  SHIP = "ship122",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 189,
  LEVEL = 6,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 114,
  AVATAR = "head119",
  SHIP = "ship122",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 189,
  LEVEL = 7,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 114,
  AVATAR = "head119",
  SHIP = "ship122",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 189,
  LEVEL = 8,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 114,
  AVATAR = "head119",
  SHIP = "ship122",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 189,
  LEVEL = 9,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 114,
  AVATAR = "head119",
  SHIP = "ship122",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 189,
  LEVEL = 10,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 114,
  AVATAR = "head119",
  SHIP = "ship122",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 189,
  LEVEL = 11,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 114,
  AVATAR = "head119",
  SHIP = "ship122",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 189,
  LEVEL = 12,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 114,
  AVATAR = "head119",
  SHIP = "ship122",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 189,
  LEVEL = 13,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 114,
  AVATAR = "head119",
  SHIP = "ship122",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 189,
  LEVEL = 14,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 114,
  AVATAR = "head119",
  SHIP = "ship122",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 189,
  LEVEL = 15,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 114,
  AVATAR = "head119",
  SHIP = "ship122",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 189,
  LEVEL = 16,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 114,
  AVATAR = "head119",
  SHIP = "ship122",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 190,
  LEVEL = 0,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 115,
  AVATAR = "head120",
  SHIP = "ship123",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 190,
  LEVEL = 1,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 115,
  AVATAR = "head120",
  SHIP = "ship123",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 190,
  LEVEL = 2,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 115,
  AVATAR = "head120",
  SHIP = "ship123",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 190,
  LEVEL = 3,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 115,
  AVATAR = "head120",
  SHIP = "ship123",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 190,
  LEVEL = 4,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 115,
  AVATAR = "head120",
  SHIP = "ship123",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 190,
  LEVEL = 5,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 115,
  AVATAR = "head120",
  SHIP = "ship123",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 190,
  LEVEL = 6,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 115,
  AVATAR = "head120",
  SHIP = "ship123",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 190,
  LEVEL = 7,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 115,
  AVATAR = "head120",
  SHIP = "ship123",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 190,
  LEVEL = 8,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 115,
  AVATAR = "head120",
  SHIP = "ship123",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 190,
  LEVEL = 9,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 115,
  AVATAR = "head120",
  SHIP = "ship123",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 190,
  LEVEL = 10,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 115,
  AVATAR = "head120",
  SHIP = "ship123",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 190,
  LEVEL = 11,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 115,
  AVATAR = "head120",
  SHIP = "ship123",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 190,
  LEVEL = 12,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 115,
  AVATAR = "head120",
  SHIP = "ship123",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 190,
  LEVEL = 13,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 115,
  AVATAR = "head120",
  SHIP = "ship123",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 190,
  LEVEL = 14,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 115,
  AVATAR = "head120",
  SHIP = "ship123",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 190,
  LEVEL = 15,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 115,
  AVATAR = "head120",
  SHIP = "ship123",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 190,
  LEVEL = 16,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 115,
  AVATAR = "head120",
  SHIP = "ship123",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 191,
  LEVEL = 0,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 116,
  AVATAR = "head121",
  SHIP = "ship124",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 191,
  LEVEL = 1,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 116,
  AVATAR = "head121",
  SHIP = "ship124",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 191,
  LEVEL = 2,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 116,
  AVATAR = "head121",
  SHIP = "ship124",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 191,
  LEVEL = 3,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 116,
  AVATAR = "head121",
  SHIP = "ship124",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 191,
  LEVEL = 4,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 116,
  AVATAR = "head121",
  SHIP = "ship124",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 191,
  LEVEL = 5,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 116,
  AVATAR = "head121",
  SHIP = "ship124",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 191,
  LEVEL = 6,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 116,
  AVATAR = "head121",
  SHIP = "ship124",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 191,
  LEVEL = 7,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 116,
  AVATAR = "head121",
  SHIP = "ship124",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 191,
  LEVEL = 8,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 116,
  AVATAR = "head121",
  SHIP = "ship124",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 191,
  LEVEL = 9,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 116,
  AVATAR = "head121",
  SHIP = "ship124",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 191,
  LEVEL = 10,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 116,
  AVATAR = "head121",
  SHIP = "ship124",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 191,
  LEVEL = 11,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 116,
  AVATAR = "head121",
  SHIP = "ship124",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 191,
  LEVEL = 12,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 116,
  AVATAR = "head121",
  SHIP = "ship124",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 191,
  LEVEL = 13,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 116,
  AVATAR = "head121",
  SHIP = "ship124",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 191,
  LEVEL = 14,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 116,
  AVATAR = "head121",
  SHIP = "ship124",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 191,
  LEVEL = 15,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 116,
  AVATAR = "head121",
  SHIP = "ship124",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 191,
  LEVEL = 16,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 116,
  AVATAR = "head121",
  SHIP = "ship124",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 192,
  LEVEL = 0,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 117,
  AVATAR = "head122",
  SHIP = "ship125",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 192,
  LEVEL = 1,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 117,
  AVATAR = "head122",
  SHIP = "ship125",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 192,
  LEVEL = 2,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 117,
  AVATAR = "head122",
  SHIP = "ship125",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 192,
  LEVEL = 3,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 117,
  AVATAR = "head122",
  SHIP = "ship125",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 192,
  LEVEL = 4,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 117,
  AVATAR = "head122",
  SHIP = "ship125",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 192,
  LEVEL = 5,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 117,
  AVATAR = "head122",
  SHIP = "ship125",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 192,
  LEVEL = 6,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 117,
  AVATAR = "head122",
  SHIP = "ship125",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 192,
  LEVEL = 7,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 117,
  AVATAR = "head122",
  SHIP = "ship125",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 192,
  LEVEL = 8,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 117,
  AVATAR = "head122",
  SHIP = "ship125",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 192,
  LEVEL = 9,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 117,
  AVATAR = "head122",
  SHIP = "ship125",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 192,
  LEVEL = 10,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 117,
  AVATAR = "head122",
  SHIP = "ship125",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 192,
  LEVEL = 11,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 117,
  AVATAR = "head122",
  SHIP = "ship125",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 192,
  LEVEL = 12,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 117,
  AVATAR = "head122",
  SHIP = "ship125",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 192,
  LEVEL = 13,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 117,
  AVATAR = "head122",
  SHIP = "ship125",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 192,
  LEVEL = 14,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 117,
  AVATAR = "head122",
  SHIP = "ship125",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 192,
  LEVEL = 15,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 117,
  AVATAR = "head122",
  SHIP = "ship125",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 192,
  LEVEL = 16,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 117,
  AVATAR = "head122",
  SHIP = "ship125",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 193,
  LEVEL = 0,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 118,
  AVATAR = "head123",
  SHIP = "ship126",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 193,
  LEVEL = 1,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 118,
  AVATAR = "head123",
  SHIP = "ship126",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 193,
  LEVEL = 2,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 118,
  AVATAR = "head123",
  SHIP = "ship126",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 193,
  LEVEL = 3,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 118,
  AVATAR = "head123",
  SHIP = "ship126",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 193,
  LEVEL = 4,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 118,
  AVATAR = "head123",
  SHIP = "ship126",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 193,
  LEVEL = 5,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 118,
  AVATAR = "head123",
  SHIP = "ship126",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 193,
  LEVEL = 6,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 118,
  AVATAR = "head123",
  SHIP = "ship126",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 193,
  LEVEL = 7,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 118,
  AVATAR = "head123",
  SHIP = "ship126",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 193,
  LEVEL = 8,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 118,
  AVATAR = "head123",
  SHIP = "ship126",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 193,
  LEVEL = 9,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 118,
  AVATAR = "head123",
  SHIP = "ship126",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 193,
  LEVEL = 10,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 118,
  AVATAR = "head123",
  SHIP = "ship126",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 193,
  LEVEL = 11,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 118,
  AVATAR = "head123",
  SHIP = "ship126",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 193,
  LEVEL = 12,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 118,
  AVATAR = "head123",
  SHIP = "ship126",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 193,
  LEVEL = 13,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 118,
  AVATAR = "head123",
  SHIP = "ship126",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 193,
  LEVEL = 14,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 118,
  AVATAR = "head123",
  SHIP = "ship126",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 193,
  LEVEL = 15,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 118,
  AVATAR = "head123",
  SHIP = "ship126",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 193,
  LEVEL = 16,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 118,
  AVATAR = "head123",
  SHIP = "ship126",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 194,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 119,
  AVATAR = "head124",
  SHIP = "ship127",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 194,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 119,
  AVATAR = "head124",
  SHIP = "ship127",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 194,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 119,
  AVATAR = "head124",
  SHIP = "ship127",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 194,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 119,
  AVATAR = "head124",
  SHIP = "ship127",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 194,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 119,
  AVATAR = "head124",
  SHIP = "ship127",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 194,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 119,
  AVATAR = "head124",
  SHIP = "ship127",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 194,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 119,
  AVATAR = "head124",
  SHIP = "ship127",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 194,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 119,
  AVATAR = "head124",
  SHIP = "ship127",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 194,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 119,
  AVATAR = "head124",
  SHIP = "ship127",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 194,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 119,
  AVATAR = "head124",
  SHIP = "ship127",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 194,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 119,
  AVATAR = "head124",
  SHIP = "ship127",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 194,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 119,
  AVATAR = "head124",
  SHIP = "ship127",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 194,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 119,
  AVATAR = "head124",
  SHIP = "ship127",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 194,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 119,
  AVATAR = "head124",
  SHIP = "ship127",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 194,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 119,
  AVATAR = "head124",
  SHIP = "ship127",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 194,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 119,
  AVATAR = "head124",
  SHIP = "ship127",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 194,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 119,
  AVATAR = "head124",
  SHIP = "ship127",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 195,
  LEVEL = 0,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 120,
  AVATAR = "head125",
  SHIP = "ship128",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 195,
  LEVEL = 1,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 120,
  AVATAR = "head125",
  SHIP = "ship128",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 195,
  LEVEL = 2,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 120,
  AVATAR = "head125",
  SHIP = "ship128",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 195,
  LEVEL = 3,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 120,
  AVATAR = "head125",
  SHIP = "ship128",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 195,
  LEVEL = 4,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 120,
  AVATAR = "head125",
  SHIP = "ship128",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 195,
  LEVEL = 5,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 120,
  AVATAR = "head125",
  SHIP = "ship128",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 195,
  LEVEL = 6,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 120,
  AVATAR = "head125",
  SHIP = "ship128",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 195,
  LEVEL = 7,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 120,
  AVATAR = "head125",
  SHIP = "ship128",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 195,
  LEVEL = 8,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 120,
  AVATAR = "head125",
  SHIP = "ship128",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 195,
  LEVEL = 9,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 120,
  AVATAR = "head125",
  SHIP = "ship128",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 195,
  LEVEL = 10,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 120,
  AVATAR = "head125",
  SHIP = "ship128",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 195,
  LEVEL = 11,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 120,
  AVATAR = "head125",
  SHIP = "ship128",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 195,
  LEVEL = 12,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 120,
  AVATAR = "head125",
  SHIP = "ship128",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 195,
  LEVEL = 13,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 120,
  AVATAR = "head125",
  SHIP = "ship128",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 195,
  LEVEL = 14,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 120,
  AVATAR = "head125",
  SHIP = "ship128",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 195,
  LEVEL = 15,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 120,
  AVATAR = "head125",
  SHIP = "ship128",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 195,
  LEVEL = 16,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 120,
  AVATAR = "head125",
  SHIP = "ship128",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 196,
  LEVEL = 0,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 121,
  AVATAR = "head126",
  SHIP = "ship129",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 196,
  LEVEL = 1,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 121,
  AVATAR = "head126",
  SHIP = "ship129",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 196,
  LEVEL = 2,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 121,
  AVATAR = "head126",
  SHIP = "ship129",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 196,
  LEVEL = 3,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 121,
  AVATAR = "head126",
  SHIP = "ship129",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 196,
  LEVEL = 4,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 121,
  AVATAR = "head126",
  SHIP = "ship129",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 196,
  LEVEL = 5,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 121,
  AVATAR = "head126",
  SHIP = "ship129",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 196,
  LEVEL = 6,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 121,
  AVATAR = "head126",
  SHIP = "ship129",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 196,
  LEVEL = 7,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 121,
  AVATAR = "head126",
  SHIP = "ship129",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 196,
  LEVEL = 8,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 121,
  AVATAR = "head126",
  SHIP = "ship129",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 196,
  LEVEL = 9,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 121,
  AVATAR = "head126",
  SHIP = "ship129",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 196,
  LEVEL = 10,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 121,
  AVATAR = "head126",
  SHIP = "ship129",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 196,
  LEVEL = 11,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 121,
  AVATAR = "head126",
  SHIP = "ship129",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 196,
  LEVEL = 12,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 121,
  AVATAR = "head126",
  SHIP = "ship129",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 196,
  LEVEL = 13,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 121,
  AVATAR = "head126",
  SHIP = "ship129",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 196,
  LEVEL = 14,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 121,
  AVATAR = "head126",
  SHIP = "ship129",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 196,
  LEVEL = 15,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 121,
  AVATAR = "head126",
  SHIP = "ship129",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 196,
  LEVEL = 16,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 121,
  AVATAR = "head126",
  SHIP = "ship129",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 197,
  LEVEL = 0,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 122,
  AVATAR = "head127",
  SHIP = "ship130",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 197,
  LEVEL = 1,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 122,
  AVATAR = "head127",
  SHIP = "ship130",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 197,
  LEVEL = 2,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 122,
  AVATAR = "head127",
  SHIP = "ship130",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 197,
  LEVEL = 3,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 122,
  AVATAR = "head127",
  SHIP = "ship130",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 197,
  LEVEL = 4,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 122,
  AVATAR = "head127",
  SHIP = "ship130",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 197,
  LEVEL = 5,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 122,
  AVATAR = "head127",
  SHIP = "ship130",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 197,
  LEVEL = 6,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 122,
  AVATAR = "head127",
  SHIP = "ship130",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 197,
  LEVEL = 7,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 122,
  AVATAR = "head127",
  SHIP = "ship130",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 197,
  LEVEL = 8,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 122,
  AVATAR = "head127",
  SHIP = "ship130",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 197,
  LEVEL = 9,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 122,
  AVATAR = "head127",
  SHIP = "ship130",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 197,
  LEVEL = 10,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 122,
  AVATAR = "head127",
  SHIP = "ship130",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 197,
  LEVEL = 11,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 122,
  AVATAR = "head127",
  SHIP = "ship130",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 197,
  LEVEL = 12,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 122,
  AVATAR = "head127",
  SHIP = "ship130",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 197,
  LEVEL = 13,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 122,
  AVATAR = "head127",
  SHIP = "ship130",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 197,
  LEVEL = 14,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 122,
  AVATAR = "head127",
  SHIP = "ship130",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 197,
  LEVEL = 15,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 122,
  AVATAR = "head127",
  SHIP = "ship130",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 197,
  LEVEL = 16,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 122,
  AVATAR = "head127",
  SHIP = "ship130",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 198,
  LEVEL = 0,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 123,
  AVATAR = "head128",
  SHIP = "ship131",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 198,
  LEVEL = 1,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 123,
  AVATAR = "head128",
  SHIP = "ship131",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 198,
  LEVEL = 2,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 123,
  AVATAR = "head128",
  SHIP = "ship131",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 198,
  LEVEL = 3,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 123,
  AVATAR = "head128",
  SHIP = "ship131",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 198,
  LEVEL = 4,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 123,
  AVATAR = "head128",
  SHIP = "ship131",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 198,
  LEVEL = 5,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 123,
  AVATAR = "head128",
  SHIP = "ship131",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 198,
  LEVEL = 6,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 123,
  AVATAR = "head128",
  SHIP = "ship131",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 198,
  LEVEL = 7,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 123,
  AVATAR = "head128",
  SHIP = "ship131",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 198,
  LEVEL = 8,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 123,
  AVATAR = "head128",
  SHIP = "ship131",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 198,
  LEVEL = 9,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 123,
  AVATAR = "head128",
  SHIP = "ship131",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 198,
  LEVEL = 10,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 123,
  AVATAR = "head128",
  SHIP = "ship131",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 198,
  LEVEL = 11,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 123,
  AVATAR = "head128",
  SHIP = "ship131",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 198,
  LEVEL = 12,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 123,
  AVATAR = "head128",
  SHIP = "ship131",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 198,
  LEVEL = 13,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 123,
  AVATAR = "head128",
  SHIP = "ship131",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 198,
  LEVEL = 14,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 123,
  AVATAR = "head128",
  SHIP = "ship131",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 198,
  LEVEL = 15,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 123,
  AVATAR = "head128",
  SHIP = "ship131",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 198,
  LEVEL = 16,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 123,
  AVATAR = "head128",
  SHIP = "ship131",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 199,
  LEVEL = 0,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 124,
  AVATAR = "head129",
  SHIP = "ship132",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 199,
  LEVEL = 1,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 124,
  AVATAR = "head129",
  SHIP = "ship132",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 199,
  LEVEL = 2,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 124,
  AVATAR = "head129",
  SHIP = "ship132",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 199,
  LEVEL = 3,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 124,
  AVATAR = "head129",
  SHIP = "ship132",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 199,
  LEVEL = 4,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 124,
  AVATAR = "head129",
  SHIP = "ship132",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 199,
  LEVEL = 5,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 124,
  AVATAR = "head129",
  SHIP = "ship132",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 199,
  LEVEL = 6,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 124,
  AVATAR = "head129",
  SHIP = "ship132",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 199,
  LEVEL = 7,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 124,
  AVATAR = "head129",
  SHIP = "ship132",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 199,
  LEVEL = 8,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 124,
  AVATAR = "head129",
  SHIP = "ship132",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 199,
  LEVEL = 9,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 124,
  AVATAR = "head129",
  SHIP = "ship132",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 199,
  LEVEL = 10,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 124,
  AVATAR = "head129",
  SHIP = "ship132",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 199,
  LEVEL = 11,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 124,
  AVATAR = "head129",
  SHIP = "ship132",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 199,
  LEVEL = 12,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 124,
  AVATAR = "head129",
  SHIP = "ship132",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 199,
  LEVEL = 13,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 124,
  AVATAR = "head129",
  SHIP = "ship132",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 199,
  LEVEL = 14,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 124,
  AVATAR = "head129",
  SHIP = "ship132",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 199,
  LEVEL = 15,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 124,
  AVATAR = "head129",
  SHIP = "ship132",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 199,
  LEVEL = 16,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 124,
  AVATAR = "head129",
  SHIP = "ship132",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 200,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 125,
  AVATAR = "head130",
  SHIP = "ship133",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 200,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 125,
  AVATAR = "head130",
  SHIP = "ship133",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 200,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 125,
  AVATAR = "head130",
  SHIP = "ship133",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 200,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 125,
  AVATAR = "head130",
  SHIP = "ship133",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 200,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 125,
  AVATAR = "head130",
  SHIP = "ship133",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 200,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 125,
  AVATAR = "head130",
  SHIP = "ship133",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 200,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 125,
  AVATAR = "head130",
  SHIP = "ship133",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 200,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 125,
  AVATAR = "head130",
  SHIP = "ship133",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 200,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 125,
  AVATAR = "head130",
  SHIP = "ship133",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 200,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 125,
  AVATAR = "head130",
  SHIP = "ship133",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 200,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 125,
  AVATAR = "head130",
  SHIP = "ship133",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 200,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 125,
  AVATAR = "head130",
  SHIP = "ship133",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 200,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 125,
  AVATAR = "head130",
  SHIP = "ship133",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 200,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 125,
  AVATAR = "head130",
  SHIP = "ship133",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 200,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 125,
  AVATAR = "head130",
  SHIP = "ship133",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 200,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 125,
  AVATAR = "head130",
  SHIP = "ship133",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 200,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 125,
  AVATAR = "head130",
  SHIP = "ship133",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 202,
  LEVEL = 0,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 13,
  AVATAR = "head8",
  SHIP = "ship48",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 202,
  LEVEL = 1,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 13,
  AVATAR = "head8",
  SHIP = "ship48",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 202,
  LEVEL = 2,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 13,
  AVATAR = "head8",
  SHIP = "ship48",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 202,
  LEVEL = 3,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 13,
  AVATAR = "head8",
  SHIP = "ship48",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 202,
  LEVEL = 4,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 13,
  AVATAR = "head8",
  SHIP = "ship48",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 202,
  LEVEL = 5,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 13,
  AVATAR = "head8",
  SHIP = "ship48",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 202,
  LEVEL = 6,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 13,
  AVATAR = "head8",
  SHIP = "ship48",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 202,
  LEVEL = 7,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 13,
  AVATAR = "head8",
  SHIP = "ship48",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 202,
  LEVEL = 8,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 13,
  AVATAR = "head8",
  SHIP = "ship48",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 202,
  LEVEL = 9,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 13,
  AVATAR = "head8",
  SHIP = "ship48",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 202,
  LEVEL = 10,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 13,
  AVATAR = "head8",
  SHIP = "ship48",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 202,
  LEVEL = 11,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 13,
  AVATAR = "head8",
  SHIP = "ship48",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 202,
  LEVEL = 12,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 13,
  AVATAR = "head8",
  SHIP = "ship48",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 202,
  LEVEL = 13,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 13,
  AVATAR = "head8",
  SHIP = "ship48",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 202,
  LEVEL = 14,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 13,
  AVATAR = "head8",
  SHIP = "ship48",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 202,
  LEVEL = 15,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 13,
  AVATAR = "head8",
  SHIP = "ship48",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 202,
  LEVEL = 16,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 13,
  AVATAR = "head8",
  SHIP = "ship48",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 203,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 601,
  AVATAR = "head7",
  SHIP = "ship37",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 203,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 601,
  AVATAR = "head7",
  SHIP = "ship37",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 203,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 601,
  AVATAR = "head7",
  SHIP = "ship37",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 203,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 601,
  AVATAR = "head7",
  SHIP = "ship37",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 203,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 601,
  AVATAR = "head7",
  SHIP = "ship37",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 203,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 601,
  AVATAR = "head7",
  SHIP = "ship37",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 203,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 601,
  AVATAR = "head7",
  SHIP = "ship37",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 203,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 601,
  AVATAR = "head7",
  SHIP = "ship37",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 203,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 601,
  AVATAR = "head7",
  SHIP = "ship37",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 203,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 601,
  AVATAR = "head7",
  SHIP = "ship37",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 203,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 601,
  AVATAR = "head7",
  SHIP = "ship37",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 203,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 601,
  AVATAR = "head7",
  SHIP = "ship37",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 203,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 601,
  AVATAR = "head7",
  SHIP = "ship37",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 203,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 601,
  AVATAR = "head7",
  SHIP = "ship37",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 203,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 601,
  AVATAR = "head7",
  SHIP = "ship37",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 203,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 601,
  AVATAR = "head7",
  SHIP = "ship37",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 203,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 601,
  AVATAR = "head7",
  SHIP = "ship37",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 205,
  LEVEL = 0,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 18,
  AVATAR = "head19",
  SHIP = "ship20",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 205,
  LEVEL = 1,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 18,
  AVATAR = "head19",
  SHIP = "ship20",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 205,
  LEVEL = 2,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 18,
  AVATAR = "head19",
  SHIP = "ship20",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 205,
  LEVEL = 3,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 18,
  AVATAR = "head19",
  SHIP = "ship20",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 205,
  LEVEL = 4,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 18,
  AVATAR = "head19",
  SHIP = "ship20",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 205,
  LEVEL = 5,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 18,
  AVATAR = "head19",
  SHIP = "ship20",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 205,
  LEVEL = 6,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 18,
  AVATAR = "head19",
  SHIP = "ship20",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 205,
  LEVEL = 7,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 18,
  AVATAR = "head19",
  SHIP = "ship20",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 205,
  LEVEL = 8,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 18,
  AVATAR = "head19",
  SHIP = "ship20",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 205,
  LEVEL = 9,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 18,
  AVATAR = "head19",
  SHIP = "ship20",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 205,
  LEVEL = 10,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 18,
  AVATAR = "head19",
  SHIP = "ship20",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 205,
  LEVEL = 11,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 18,
  AVATAR = "head19",
  SHIP = "ship20",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 205,
  LEVEL = 12,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 18,
  AVATAR = "head19",
  SHIP = "ship20",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 205,
  LEVEL = 13,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 18,
  AVATAR = "head19",
  SHIP = "ship20",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 205,
  LEVEL = 14,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 18,
  AVATAR = "head19",
  SHIP = "ship20",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 205,
  LEVEL = 15,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 18,
  AVATAR = "head19",
  SHIP = "ship20",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 205,
  LEVEL = 16,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 18,
  AVATAR = "head19",
  SHIP = "ship20",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 206,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 17,
  AVATAR = "head29",
  SHIP = "ship42",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 206,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 17,
  AVATAR = "head29",
  SHIP = "ship42",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 206,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 17,
  AVATAR = "head29",
  SHIP = "ship42",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 206,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 17,
  AVATAR = "head29",
  SHIP = "ship42",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 206,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 17,
  AVATAR = "head29",
  SHIP = "ship42",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 206,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 17,
  AVATAR = "head29",
  SHIP = "ship42",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 206,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 17,
  AVATAR = "head29",
  SHIP = "ship42",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 206,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 17,
  AVATAR = "head29",
  SHIP = "ship42",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 206,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 17,
  AVATAR = "head29",
  SHIP = "ship42",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 206,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 17,
  AVATAR = "head29",
  SHIP = "ship42",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 206,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 17,
  AVATAR = "head29",
  SHIP = "ship42",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 206,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 17,
  AVATAR = "head29",
  SHIP = "ship42",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 206,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 17,
  AVATAR = "head29",
  SHIP = "ship42",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 206,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 17,
  AVATAR = "head29",
  SHIP = "ship42",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 206,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 17,
  AVATAR = "head29",
  SHIP = "ship42",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 206,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 17,
  AVATAR = "head29",
  SHIP = "ship42",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 206,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 17,
  AVATAR = "head29",
  SHIP = "ship42",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 207,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 16,
  AVATAR = "head7",
  SHIP = "ship42",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 207,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 16,
  AVATAR = "head7",
  SHIP = "ship42",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 207,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 16,
  AVATAR = "head7",
  SHIP = "ship42",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 207,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 16,
  AVATAR = "head7",
  SHIP = "ship42",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 207,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 16,
  AVATAR = "head7",
  SHIP = "ship42",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 207,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 16,
  AVATAR = "head7",
  SHIP = "ship42",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 207,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 16,
  AVATAR = "head7",
  SHIP = "ship42",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 207,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 16,
  AVATAR = "head7",
  SHIP = "ship42",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 207,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 16,
  AVATAR = "head7",
  SHIP = "ship42",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 207,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 16,
  AVATAR = "head7",
  SHIP = "ship42",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 207,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 16,
  AVATAR = "head7",
  SHIP = "ship42",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 207,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 16,
  AVATAR = "head7",
  SHIP = "ship42",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 207,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 16,
  AVATAR = "head7",
  SHIP = "ship42",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 207,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 16,
  AVATAR = "head7",
  SHIP = "ship42",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 207,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 16,
  AVATAR = "head7",
  SHIP = "ship42",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 207,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 16,
  AVATAR = "head7",
  SHIP = "ship42",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 207,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 16,
  AVATAR = "head7",
  SHIP = "ship42",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 208,
  LEVEL = 0,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 20,
  AVATAR = "head18",
  SHIP = "ship48",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 208,
  LEVEL = 1,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 20,
  AVATAR = "head18",
  SHIP = "ship48",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 208,
  LEVEL = 2,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 20,
  AVATAR = "head18",
  SHIP = "ship48",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 208,
  LEVEL = 3,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 20,
  AVATAR = "head18",
  SHIP = "ship48",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 208,
  LEVEL = 4,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 20,
  AVATAR = "head18",
  SHIP = "ship48",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 208,
  LEVEL = 5,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 20,
  AVATAR = "head18",
  SHIP = "ship48",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 208,
  LEVEL = 6,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 20,
  AVATAR = "head18",
  SHIP = "ship48",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 208,
  LEVEL = 7,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 20,
  AVATAR = "head18",
  SHIP = "ship48",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 208,
  LEVEL = 8,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 20,
  AVATAR = "head18",
  SHIP = "ship48",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 208,
  LEVEL = 9,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 20,
  AVATAR = "head18",
  SHIP = "ship48",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 208,
  LEVEL = 10,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 20,
  AVATAR = "head18",
  SHIP = "ship48",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 208,
  LEVEL = 11,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 20,
  AVATAR = "head18",
  SHIP = "ship48",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 208,
  LEVEL = 12,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 20,
  AVATAR = "head18",
  SHIP = "ship48",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 208,
  LEVEL = 13,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 20,
  AVATAR = "head18",
  SHIP = "ship48",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 208,
  LEVEL = 14,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 20,
  AVATAR = "head18",
  SHIP = "ship48",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 208,
  LEVEL = 15,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 20,
  AVATAR = "head18",
  SHIP = "ship48",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 208,
  LEVEL = 16,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 20,
  AVATAR = "head18",
  SHIP = "ship48",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 209,
  LEVEL = 0,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 25,
  AVATAR = "head5",
  SHIP = "ship43",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 209,
  LEVEL = 1,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 25,
  AVATAR = "head5",
  SHIP = "ship43",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 209,
  LEVEL = 2,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 25,
  AVATAR = "head5",
  SHIP = "ship43",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 209,
  LEVEL = 3,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 25,
  AVATAR = "head5",
  SHIP = "ship43",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 209,
  LEVEL = 4,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 25,
  AVATAR = "head5",
  SHIP = "ship43",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 209,
  LEVEL = 5,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 25,
  AVATAR = "head5",
  SHIP = "ship43",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 209,
  LEVEL = 6,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 25,
  AVATAR = "head5",
  SHIP = "ship43",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 209,
  LEVEL = 7,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 25,
  AVATAR = "head5",
  SHIP = "ship43",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 209,
  LEVEL = 8,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 25,
  AVATAR = "head5",
  SHIP = "ship43",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 209,
  LEVEL = 9,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 25,
  AVATAR = "head5",
  SHIP = "ship43",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 209,
  LEVEL = 10,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 25,
  AVATAR = "head5",
  SHIP = "ship43",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 209,
  LEVEL = 11,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 25,
  AVATAR = "head5",
  SHIP = "ship43",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 209,
  LEVEL = 12,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 25,
  AVATAR = "head5",
  SHIP = "ship43",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 209,
  LEVEL = 13,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 25,
  AVATAR = "head5",
  SHIP = "ship43",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 209,
  LEVEL = 14,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 25,
  AVATAR = "head5",
  SHIP = "ship43",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 209,
  LEVEL = 15,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 25,
  AVATAR = "head5",
  SHIP = "ship43",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 209,
  LEVEL = 16,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 25,
  AVATAR = "head5",
  SHIP = "ship43",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 210,
  LEVEL = 0,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 17,
  AVATAR = "head33",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 210,
  LEVEL = 1,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 17,
  AVATAR = "head33",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 210,
  LEVEL = 2,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 17,
  AVATAR = "head33",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 210,
  LEVEL = 3,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 17,
  AVATAR = "head33",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 210,
  LEVEL = 4,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 17,
  AVATAR = "head33",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 210,
  LEVEL = 5,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 17,
  AVATAR = "head33",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 210,
  LEVEL = 6,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 17,
  AVATAR = "head33",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 210,
  LEVEL = 7,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 17,
  AVATAR = "head33",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 210,
  LEVEL = 8,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 17,
  AVATAR = "head33",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 210,
  LEVEL = 9,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 17,
  AVATAR = "head33",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 210,
  LEVEL = 10,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 17,
  AVATAR = "head33",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 210,
  LEVEL = 11,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 17,
  AVATAR = "head33",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 210,
  LEVEL = 12,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 17,
  AVATAR = "head33",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 210,
  LEVEL = 13,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 17,
  AVATAR = "head33",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 210,
  LEVEL = 14,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 17,
  AVATAR = "head33",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 210,
  LEVEL = 15,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 17,
  AVATAR = "head33",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 210,
  LEVEL = 16,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 17,
  AVATAR = "head33",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 211,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 24,
  AVATAR = "head42",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 211,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 24,
  AVATAR = "head42",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 211,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 24,
  AVATAR = "head42",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 211,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 24,
  AVATAR = "head42",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 211,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 24,
  AVATAR = "head42",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 211,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 24,
  AVATAR = "head42",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 211,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 24,
  AVATAR = "head42",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 211,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 24,
  AVATAR = "head42",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 211,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 24,
  AVATAR = "head42",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 211,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 24,
  AVATAR = "head42",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 211,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 24,
  AVATAR = "head42",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 211,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 24,
  AVATAR = "head42",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 211,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 24,
  AVATAR = "head42",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 211,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 24,
  AVATAR = "head42",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 211,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 24,
  AVATAR = "head42",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 211,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 24,
  AVATAR = "head42",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 211,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 24,
  AVATAR = "head42",
  SHIP = "ship16",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 212,
  LEVEL = 0,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 28,
  AVATAR = "head4",
  SHIP = "ship48",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 212,
  LEVEL = 1,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 28,
  AVATAR = "head4",
  SHIP = "ship48",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 212,
  LEVEL = 2,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 28,
  AVATAR = "head4",
  SHIP = "ship48",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 212,
  LEVEL = 3,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 28,
  AVATAR = "head4",
  SHIP = "ship48",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 212,
  LEVEL = 4,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 28,
  AVATAR = "head4",
  SHIP = "ship48",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 212,
  LEVEL = 5,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 28,
  AVATAR = "head4",
  SHIP = "ship48",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 212,
  LEVEL = 6,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 28,
  AVATAR = "head4",
  SHIP = "ship48",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 212,
  LEVEL = 7,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 28,
  AVATAR = "head4",
  SHIP = "ship48",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 212,
  LEVEL = 8,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 28,
  AVATAR = "head4",
  SHIP = "ship48",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 212,
  LEVEL = 9,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 28,
  AVATAR = "head4",
  SHIP = "ship48",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 212,
  LEVEL = 10,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 28,
  AVATAR = "head4",
  SHIP = "ship48",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 212,
  LEVEL = 11,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 28,
  AVATAR = "head4",
  SHIP = "ship48",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 212,
  LEVEL = 12,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 28,
  AVATAR = "head4",
  SHIP = "ship48",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 212,
  LEVEL = 13,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 28,
  AVATAR = "head4",
  SHIP = "ship48",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 212,
  LEVEL = 14,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 28,
  AVATAR = "head4",
  SHIP = "ship48",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 212,
  LEVEL = 15,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 28,
  AVATAR = "head4",
  SHIP = "ship48",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 212,
  LEVEL = 16,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 28,
  AVATAR = "head4",
  SHIP = "ship48",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 213,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 91,
  AVATAR = "head6",
  SHIP = "ship2",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 213,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 91,
  AVATAR = "head6",
  SHIP = "ship2",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 213,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 91,
  AVATAR = "head6",
  SHIP = "ship2",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 213,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 91,
  AVATAR = "head6",
  SHIP = "ship2",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 213,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 91,
  AVATAR = "head6",
  SHIP = "ship2",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 213,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 91,
  AVATAR = "head6",
  SHIP = "ship2",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 213,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 91,
  AVATAR = "head6",
  SHIP = "ship2",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 213,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 91,
  AVATAR = "head6",
  SHIP = "ship2",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 213,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 91,
  AVATAR = "head6",
  SHIP = "ship2",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 213,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 91,
  AVATAR = "head6",
  SHIP = "ship2",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 213,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 91,
  AVATAR = "head6",
  SHIP = "ship2",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 213,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 91,
  AVATAR = "head6",
  SHIP = "ship2",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 213,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 91,
  AVATAR = "head6",
  SHIP = "ship2",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 213,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 91,
  AVATAR = "head6",
  SHIP = "ship2",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 213,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 91,
  AVATAR = "head6",
  SHIP = "ship2",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 213,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 91,
  AVATAR = "head6",
  SHIP = "ship2",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 213,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 91,
  AVATAR = "head6",
  SHIP = "ship2",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 214,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 96,
  AVATAR = "head6",
  SHIP = "ship2",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 214,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 96,
  AVATAR = "head6",
  SHIP = "ship2",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 214,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 96,
  AVATAR = "head6",
  SHIP = "ship2",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 214,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 96,
  AVATAR = "head6",
  SHIP = "ship2",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 214,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 96,
  AVATAR = "head6",
  SHIP = "ship2",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 214,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 96,
  AVATAR = "head6",
  SHIP = "ship2",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 214,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 96,
  AVATAR = "head6",
  SHIP = "ship2",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 214,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 96,
  AVATAR = "head6",
  SHIP = "ship2",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 214,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 96,
  AVATAR = "head6",
  SHIP = "ship2",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 214,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 96,
  AVATAR = "head6",
  SHIP = "ship2",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 214,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 96,
  AVATAR = "head6",
  SHIP = "ship2",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 214,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 96,
  AVATAR = "head6",
  SHIP = "ship2",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 214,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 96,
  AVATAR = "head6",
  SHIP = "ship2",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 214,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 96,
  AVATAR = "head6",
  SHIP = "ship2",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 214,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 96,
  AVATAR = "head6",
  SHIP = "ship2",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 214,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 96,
  AVATAR = "head6",
  SHIP = "ship2",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 214,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 96,
  AVATAR = "head6",
  SHIP = "ship2",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 215,
  LEVEL = 0,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 112,
  AVATAR = "head117",
  SHIP = "ship120",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 215,
  LEVEL = 1,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 112,
  AVATAR = "head117",
  SHIP = "ship120",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 215,
  LEVEL = 2,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 112,
  AVATAR = "head117",
  SHIP = "ship120",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 215,
  LEVEL = 3,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 112,
  AVATAR = "head117",
  SHIP = "ship120",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 215,
  LEVEL = 4,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 112,
  AVATAR = "head117",
  SHIP = "ship120",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 215,
  LEVEL = 5,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 112,
  AVATAR = "head117",
  SHIP = "ship120",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 215,
  LEVEL = 6,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 112,
  AVATAR = "head117",
  SHIP = "ship120",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 215,
  LEVEL = 7,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 112,
  AVATAR = "head117",
  SHIP = "ship120",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 215,
  LEVEL = 8,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 112,
  AVATAR = "head117",
  SHIP = "ship120",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 215,
  LEVEL = 9,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 112,
  AVATAR = "head117",
  SHIP = "ship120",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 215,
  LEVEL = 10,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 112,
  AVATAR = "head117",
  SHIP = "ship120",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 215,
  LEVEL = 11,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 112,
  AVATAR = "head117",
  SHIP = "ship120",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 215,
  LEVEL = 12,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 112,
  AVATAR = "head117",
  SHIP = "ship120",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 215,
  LEVEL = 13,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 112,
  AVATAR = "head117",
  SHIP = "ship120",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 215,
  LEVEL = 14,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 112,
  AVATAR = "head117",
  SHIP = "ship120",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 215,
  LEVEL = 15,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 112,
  AVATAR = "head117",
  SHIP = "ship120",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 215,
  LEVEL = 16,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 112,
  AVATAR = "head117",
  SHIP = "ship120",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 301,
  LEVEL = 0,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 11126,
  AVATAR = "head131",
  SHIP = "ship134",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 301,
  LEVEL = 1,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 11126,
  AVATAR = "head131",
  SHIP = "ship134",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 301,
  LEVEL = 2,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 11126,
  AVATAR = "head131",
  SHIP = "ship134",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 301,
  LEVEL = 3,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 11126,
  AVATAR = "head131",
  SHIP = "ship134",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 301,
  LEVEL = 4,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 11126,
  AVATAR = "head131",
  SHIP = "ship134",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 301,
  LEVEL = 5,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 11126,
  AVATAR = "head131",
  SHIP = "ship134",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 301,
  LEVEL = 6,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 11126,
  AVATAR = "head131",
  SHIP = "ship134",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 301,
  LEVEL = 7,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 12126,
  AVATAR = "head131",
  SHIP = "ship134",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 301,
  LEVEL = 8,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 12126,
  AVATAR = "head131",
  SHIP = "ship134",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 301,
  LEVEL = 9,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 12126,
  AVATAR = "head131",
  SHIP = "ship134",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 301,
  LEVEL = 10,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 13126,
  AVATAR = "head131",
  SHIP = "ship134",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 301,
  LEVEL = 11,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 13126,
  AVATAR = "head131",
  SHIP = "ship134",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 301,
  LEVEL = 12,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 13126,
  AVATAR = "head131",
  SHIP = "ship134",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 301,
  LEVEL = 13,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 13126,
  AVATAR = "head131",
  SHIP = "ship134",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 301,
  LEVEL = 14,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 13126,
  AVATAR = "head131",
  SHIP = "ship134",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 301,
  LEVEL = 15,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 13126,
  AVATAR = "head131",
  SHIP = "ship134",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 301,
  LEVEL = 16,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 13126,
  AVATAR = "head131",
  SHIP = "ship134",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 302,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 127,
  AVATAR = "head132",
  SHIP = "ship135",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 302,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 127,
  AVATAR = "head132",
  SHIP = "ship135",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 302,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 127,
  AVATAR = "head132",
  SHIP = "ship135",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 302,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 127,
  AVATAR = "head132",
  SHIP = "ship135",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 302,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 127,
  AVATAR = "head132",
  SHIP = "ship135",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 302,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 127,
  AVATAR = "head132",
  SHIP = "ship135",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 302,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 127,
  AVATAR = "head132",
  SHIP = "ship135",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 302,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 127,
  AVATAR = "head132",
  SHIP = "ship135",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 302,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 127,
  AVATAR = "head132",
  SHIP = "ship135",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 302,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 127,
  AVATAR = "head132",
  SHIP = "ship135",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 302,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 127,
  AVATAR = "head132",
  SHIP = "ship135",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 302,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 127,
  AVATAR = "head132",
  SHIP = "ship135",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 302,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 127,
  AVATAR = "head132",
  SHIP = "ship135",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 302,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 127,
  AVATAR = "head132",
  SHIP = "ship135",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 302,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 127,
  AVATAR = "head132",
  SHIP = "ship135",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 302,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 127,
  AVATAR = "head132",
  SHIP = "ship135",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 302,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 127,
  AVATAR = "head132",
  SHIP = "ship135",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 303,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 128,
  AVATAR = "head125",
  SHIP = "ship136",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 303,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 128,
  AVATAR = "head125",
  SHIP = "ship136",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 303,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 128,
  AVATAR = "head125",
  SHIP = "ship136",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 303,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 128,
  AVATAR = "head125",
  SHIP = "ship136",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 303,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 128,
  AVATAR = "head125",
  SHIP = "ship136",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 303,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 128,
  AVATAR = "head125",
  SHIP = "ship136",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 303,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 128,
  AVATAR = "head125",
  SHIP = "ship136",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 303,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 128,
  AVATAR = "head125",
  SHIP = "ship136",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 303,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 128,
  AVATAR = "head125",
  SHIP = "ship136",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 303,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 128,
  AVATAR = "head125",
  SHIP = "ship136",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 303,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 128,
  AVATAR = "head125",
  SHIP = "ship136",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 303,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 128,
  AVATAR = "head125",
  SHIP = "ship136",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 303,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 128,
  AVATAR = "head125",
  SHIP = "ship136",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 303,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 128,
  AVATAR = "head125",
  SHIP = "ship136",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 303,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 128,
  AVATAR = "head125",
  SHIP = "ship136",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 303,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 128,
  AVATAR = "head125",
  SHIP = "ship136",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 303,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 128,
  AVATAR = "head125",
  SHIP = "ship136",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 304,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 129,
  AVATAR = "head125",
  SHIP = "ship137",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 304,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 129,
  AVATAR = "head125",
  SHIP = "ship137",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 304,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 129,
  AVATAR = "head125",
  SHIP = "ship137",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 304,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 129,
  AVATAR = "head125",
  SHIP = "ship137",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 304,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 129,
  AVATAR = "head125",
  SHIP = "ship137",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 304,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 129,
  AVATAR = "head125",
  SHIP = "ship137",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 304,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 129,
  AVATAR = "head125",
  SHIP = "ship137",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 304,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 129,
  AVATAR = "head125",
  SHIP = "ship137",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 304,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 129,
  AVATAR = "head125",
  SHIP = "ship137",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 304,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 129,
  AVATAR = "head125",
  SHIP = "ship137",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 304,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 129,
  AVATAR = "head125",
  SHIP = "ship137",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 304,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 129,
  AVATAR = "head125",
  SHIP = "ship137",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 304,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 129,
  AVATAR = "head125",
  SHIP = "ship137",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 304,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 129,
  AVATAR = "head125",
  SHIP = "ship137",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 304,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 129,
  AVATAR = "head125",
  SHIP = "ship137",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 304,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 129,
  AVATAR = "head125",
  SHIP = "ship137",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 304,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 129,
  AVATAR = "head125",
  SHIP = "ship137",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 305,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 130,
  AVATAR = "head125",
  SHIP = "ship138",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 305,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 130,
  AVATAR = "head125",
  SHIP = "ship138",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 305,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 130,
  AVATAR = "head125",
  SHIP = "ship138",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 305,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 130,
  AVATAR = "head125",
  SHIP = "ship138",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 305,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 130,
  AVATAR = "head125",
  SHIP = "ship138",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 305,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 130,
  AVATAR = "head125",
  SHIP = "ship138",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 305,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 130,
  AVATAR = "head125",
  SHIP = "ship138",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 305,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 130,
  AVATAR = "head125",
  SHIP = "ship138",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 305,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 130,
  AVATAR = "head125",
  SHIP = "ship138",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 305,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 130,
  AVATAR = "head125",
  SHIP = "ship138",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 305,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 130,
  AVATAR = "head125",
  SHIP = "ship138",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 305,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 130,
  AVATAR = "head125",
  SHIP = "ship138",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 305,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 130,
  AVATAR = "head125",
  SHIP = "ship138",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 305,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 130,
  AVATAR = "head125",
  SHIP = "ship138",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 305,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 130,
  AVATAR = "head125",
  SHIP = "ship138",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 305,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 130,
  AVATAR = "head125",
  SHIP = "ship138",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 305,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 130,
  AVATAR = "head125",
  SHIP = "ship138",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 306,
  LEVEL = 0,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 131,
  AVATAR = "head136",
  SHIP = "ship139",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 306,
  LEVEL = 1,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 131,
  AVATAR = "head136",
  SHIP = "ship139",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 306,
  LEVEL = 2,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 131,
  AVATAR = "head136",
  SHIP = "ship139",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 306,
  LEVEL = 3,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 131,
  AVATAR = "head136",
  SHIP = "ship139",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 306,
  LEVEL = 4,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 131,
  AVATAR = "head136",
  SHIP = "ship139",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 306,
  LEVEL = 5,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 131,
  AVATAR = "head136",
  SHIP = "ship139",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 306,
  LEVEL = 6,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 131,
  AVATAR = "head136",
  SHIP = "ship139",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 306,
  LEVEL = 7,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 131,
  AVATAR = "head136",
  SHIP = "ship139",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 306,
  LEVEL = 8,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 131,
  AVATAR = "head136",
  SHIP = "ship139",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 306,
  LEVEL = 9,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 131,
  AVATAR = "head136",
  SHIP = "ship139",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 306,
  LEVEL = 10,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 131,
  AVATAR = "head136",
  SHIP = "ship139",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 306,
  LEVEL = 11,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 131,
  AVATAR = "head136",
  SHIP = "ship139",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 306,
  LEVEL = 12,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 131,
  AVATAR = "head136",
  SHIP = "ship139",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 306,
  LEVEL = 13,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 131,
  AVATAR = "head136",
  SHIP = "ship139",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 306,
  LEVEL = 14,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 131,
  AVATAR = "head136",
  SHIP = "ship139",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 306,
  LEVEL = 15,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 131,
  AVATAR = "head136",
  SHIP = "ship139",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 306,
  LEVEL = 16,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 131,
  AVATAR = "head136",
  SHIP = "ship139",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 307,
  LEVEL = 0,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 132,
  AVATAR = "head137",
  SHIP = "ship140",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 307,
  LEVEL = 1,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 132,
  AVATAR = "head137",
  SHIP = "ship140",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 307,
  LEVEL = 2,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 132,
  AVATAR = "head137",
  SHIP = "ship140",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 307,
  LEVEL = 3,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 132,
  AVATAR = "head137",
  SHIP = "ship140",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 307,
  LEVEL = 4,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 132,
  AVATAR = "head137",
  SHIP = "ship140",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 307,
  LEVEL = 5,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 132,
  AVATAR = "head137",
  SHIP = "ship140",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 307,
  LEVEL = 6,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 132,
  AVATAR = "head137",
  SHIP = "ship140",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 307,
  LEVEL = 7,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 132,
  AVATAR = "head137",
  SHIP = "ship140",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 307,
  LEVEL = 8,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 132,
  AVATAR = "head137",
  SHIP = "ship140",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 307,
  LEVEL = 9,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 132,
  AVATAR = "head137",
  SHIP = "ship140",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 307,
  LEVEL = 10,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 132,
  AVATAR = "head137",
  SHIP = "ship140",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 307,
  LEVEL = 11,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 132,
  AVATAR = "head137",
  SHIP = "ship140",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 307,
  LEVEL = 12,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 132,
  AVATAR = "head137",
  SHIP = "ship140",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 307,
  LEVEL = 13,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 132,
  AVATAR = "head137",
  SHIP = "ship140",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 307,
  LEVEL = 14,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 132,
  AVATAR = "head137",
  SHIP = "ship140",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 307,
  LEVEL = 15,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 132,
  AVATAR = "head137",
  SHIP = "ship140",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 307,
  LEVEL = 16,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 132,
  AVATAR = "head137",
  SHIP = "ship140",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 308,
  LEVEL = 0,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 133,
  AVATAR = "head138",
  SHIP = "ship141",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 308,
  LEVEL = 1,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 133,
  AVATAR = "head138",
  SHIP = "ship141",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 308,
  LEVEL = 2,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 133,
  AVATAR = "head138",
  SHIP = "ship141",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 308,
  LEVEL = 3,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 133,
  AVATAR = "head138",
  SHIP = "ship141",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 308,
  LEVEL = 4,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 133,
  AVATAR = "head138",
  SHIP = "ship141",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 308,
  LEVEL = 5,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 133,
  AVATAR = "head138",
  SHIP = "ship141",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 308,
  LEVEL = 6,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 133,
  AVATAR = "head138",
  SHIP = "ship141",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 308,
  LEVEL = 7,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 133,
  AVATAR = "head138",
  SHIP = "ship141",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 308,
  LEVEL = 8,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 133,
  AVATAR = "head138",
  SHIP = "ship141",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 308,
  LEVEL = 9,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 133,
  AVATAR = "head138",
  SHIP = "ship141",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 308,
  LEVEL = 10,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 133,
  AVATAR = "head138",
  SHIP = "ship141",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 308,
  LEVEL = 11,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 133,
  AVATAR = "head138",
  SHIP = "ship141",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 308,
  LEVEL = 12,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 133,
  AVATAR = "head138",
  SHIP = "ship141",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 308,
  LEVEL = 13,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 133,
  AVATAR = "head138",
  SHIP = "ship141",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 308,
  LEVEL = 14,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 133,
  AVATAR = "head138",
  SHIP = "ship141",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 308,
  LEVEL = 15,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 133,
  AVATAR = "head138",
  SHIP = "ship141",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 308,
  LEVEL = 16,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 133,
  AVATAR = "head138",
  SHIP = "ship141",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 309,
  LEVEL = 0,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 134,
  AVATAR = "head139",
  SHIP = "ship142",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 309,
  LEVEL = 1,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 134,
  AVATAR = "head139",
  SHIP = "ship142",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 309,
  LEVEL = 2,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 134,
  AVATAR = "head139",
  SHIP = "ship142",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 309,
  LEVEL = 3,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 134,
  AVATAR = "head139",
  SHIP = "ship142",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 309,
  LEVEL = 4,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 134,
  AVATAR = "head139",
  SHIP = "ship142",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 309,
  LEVEL = 5,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 134,
  AVATAR = "head139",
  SHIP = "ship142",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 309,
  LEVEL = 6,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 134,
  AVATAR = "head139",
  SHIP = "ship142",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 309,
  LEVEL = 7,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 134,
  AVATAR = "head139",
  SHIP = "ship142",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 309,
  LEVEL = 8,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 134,
  AVATAR = "head139",
  SHIP = "ship142",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 309,
  LEVEL = 9,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 134,
  AVATAR = "head139",
  SHIP = "ship142",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 309,
  LEVEL = 10,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 134,
  AVATAR = "head139",
  SHIP = "ship142",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 309,
  LEVEL = 11,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 134,
  AVATAR = "head139",
  SHIP = "ship142",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 309,
  LEVEL = 12,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 134,
  AVATAR = "head139",
  SHIP = "ship142",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 309,
  LEVEL = 13,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 134,
  AVATAR = "head139",
  SHIP = "ship142",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 309,
  LEVEL = 14,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 134,
  AVATAR = "head139",
  SHIP = "ship142",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 309,
  LEVEL = 15,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 134,
  AVATAR = "head139",
  SHIP = "ship142",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 309,
  LEVEL = 16,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 134,
  AVATAR = "head139",
  SHIP = "ship142",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 310,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 135,
  AVATAR = "head125",
  SHIP = "ship143",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 310,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 135,
  AVATAR = "head125",
  SHIP = "ship143",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 310,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 135,
  AVATAR = "head125",
  SHIP = "ship143",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 310,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 135,
  AVATAR = "head125",
  SHIP = "ship143",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 310,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 135,
  AVATAR = "head125",
  SHIP = "ship143",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 310,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 135,
  AVATAR = "head125",
  SHIP = "ship143",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 310,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 135,
  AVATAR = "head125",
  SHIP = "ship143",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 310,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 135,
  AVATAR = "head125",
  SHIP = "ship143",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 310,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 135,
  AVATAR = "head125",
  SHIP = "ship143",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 310,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 135,
  AVATAR = "head125",
  SHIP = "ship143",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 310,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 135,
  AVATAR = "head125",
  SHIP = "ship143",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 310,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 135,
  AVATAR = "head125",
  SHIP = "ship143",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 310,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 135,
  AVATAR = "head125",
  SHIP = "ship143",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 310,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 135,
  AVATAR = "head125",
  SHIP = "ship143",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 310,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 135,
  AVATAR = "head125",
  SHIP = "ship143",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 310,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 135,
  AVATAR = "head125",
  SHIP = "ship143",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 310,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 135,
  AVATAR = "head125",
  SHIP = "ship143",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 311,
  LEVEL = 0,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 11136,
  AVATAR = "head141",
  SHIP = "ship144",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 311,
  LEVEL = 1,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 11136,
  AVATAR = "head141",
  SHIP = "ship144",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 311,
  LEVEL = 2,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 11136,
  AVATAR = "head141",
  SHIP = "ship144",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 311,
  LEVEL = 3,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 11136,
  AVATAR = "head141",
  SHIP = "ship144",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 311,
  LEVEL = 4,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 11136,
  AVATAR = "head141",
  SHIP = "ship144",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 311,
  LEVEL = 5,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 11136,
  AVATAR = "head141",
  SHIP = "ship144",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 311,
  LEVEL = 6,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 11136,
  AVATAR = "head141",
  SHIP = "ship144",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 311,
  LEVEL = 7,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 11136,
  AVATAR = "head141",
  SHIP = "ship144",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 311,
  LEVEL = 8,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 11136,
  AVATAR = "head141",
  SHIP = "ship144",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 311,
  LEVEL = 9,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 11136,
  AVATAR = "head141",
  SHIP = "ship144",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 311,
  LEVEL = 10,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 12136,
  AVATAR = "head141",
  SHIP = "ship144",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 311,
  LEVEL = 11,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 12136,
  AVATAR = "head141",
  SHIP = "ship144",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 311,
  LEVEL = 12,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 12136,
  AVATAR = "head141",
  SHIP = "ship144",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 311,
  LEVEL = 13,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 12136,
  AVATAR = "head141",
  SHIP = "ship144",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 311,
  LEVEL = 14,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 12136,
  AVATAR = "head141",
  SHIP = "ship144",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 311,
  LEVEL = 15,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 13136,
  AVATAR = "head141",
  SHIP = "ship144",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 311,
  LEVEL = 16,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 13136,
  AVATAR = "head141",
  SHIP = "ship144",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 312,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 137,
  AVATAR = "head142",
  SHIP = "ship145",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 312,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 137,
  AVATAR = "head142",
  SHIP = "ship145",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 312,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 137,
  AVATAR = "head142",
  SHIP = "ship145",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 312,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 137,
  AVATAR = "head142",
  SHIP = "ship145",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 312,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 137,
  AVATAR = "head142",
  SHIP = "ship145",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 312,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 137,
  AVATAR = "head142",
  SHIP = "ship145",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 312,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 137,
  AVATAR = "head142",
  SHIP = "ship145",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 312,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 137,
  AVATAR = "head142",
  SHIP = "ship145",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 312,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 137,
  AVATAR = "head142",
  SHIP = "ship145",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 312,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 137,
  AVATAR = "head142",
  SHIP = "ship145",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 312,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 137,
  AVATAR = "head142",
  SHIP = "ship145",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 312,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 137,
  AVATAR = "head142",
  SHIP = "ship145",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 312,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 137,
  AVATAR = "head142",
  SHIP = "ship145",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 312,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 137,
  AVATAR = "head142",
  SHIP = "ship145",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 312,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 137,
  AVATAR = "head142",
  SHIP = "ship145",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 312,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 137,
  AVATAR = "head142",
  SHIP = "ship145",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 312,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 137,
  AVATAR = "head142",
  SHIP = "ship145",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 313,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 139,
  AVATAR = "head143",
  SHIP = "ship146",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 313,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 139,
  AVATAR = "head143",
  SHIP = "ship146",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 313,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 139,
  AVATAR = "head143",
  SHIP = "ship146",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 313,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 10139,
  AVATAR = "head143",
  SHIP = "ship146",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 313,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 10139,
  AVATAR = "head143",
  SHIP = "ship146",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 313,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 10139,
  AVATAR = "head143",
  SHIP = "ship146",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 313,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 10139,
  AVATAR = "head143",
  SHIP = "ship146",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 313,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 11139,
  AVATAR = "head143",
  SHIP = "ship146",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 313,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 11139,
  AVATAR = "head143",
  SHIP = "ship146",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 313,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 11139,
  AVATAR = "head143",
  SHIP = "ship146",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 313,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 11139,
  AVATAR = "head143",
  SHIP = "ship146",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 313,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 11139,
  AVATAR = "head143",
  SHIP = "ship146",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 313,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 12139,
  AVATAR = "head143",
  SHIP = "ship146",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 313,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 12139,
  AVATAR = "head143",
  SHIP = "ship146",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 313,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 12139,
  AVATAR = "head143",
  SHIP = "ship146",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 313,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 12139,
  AVATAR = "head143",
  SHIP = "ship146",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 313,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 12139,
  AVATAR = "head143",
  SHIP = "ship146",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 314,
  LEVEL = 0,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 140,
  AVATAR = "head144",
  SHIP = "ship147",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 314,
  LEVEL = 1,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 140,
  AVATAR = "head144",
  SHIP = "ship147",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 314,
  LEVEL = 2,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 140,
  AVATAR = "head144",
  SHIP = "ship147",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 314,
  LEVEL = 3,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 140,
  AVATAR = "head144",
  SHIP = "ship147",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 314,
  LEVEL = 4,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 140,
  AVATAR = "head144",
  SHIP = "ship147",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 314,
  LEVEL = 5,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 140,
  AVATAR = "head144",
  SHIP = "ship147",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 314,
  LEVEL = 6,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 140,
  AVATAR = "head144",
  SHIP = "ship147",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 314,
  LEVEL = 7,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 140,
  AVATAR = "head144",
  SHIP = "ship147",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 314,
  LEVEL = 8,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 140,
  AVATAR = "head144",
  SHIP = "ship147",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 314,
  LEVEL = 9,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 140,
  AVATAR = "head144",
  SHIP = "ship147",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 314,
  LEVEL = 10,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 140,
  AVATAR = "head144",
  SHIP = "ship147",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 314,
  LEVEL = 11,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 140,
  AVATAR = "head144",
  SHIP = "ship147",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 314,
  LEVEL = 12,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 140,
  AVATAR = "head144",
  SHIP = "ship147",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 314,
  LEVEL = 13,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 140,
  AVATAR = "head144",
  SHIP = "ship147",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 314,
  LEVEL = 14,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 140,
  AVATAR = "head144",
  SHIP = "ship147",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 314,
  LEVEL = 15,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 140,
  AVATAR = "head144",
  SHIP = "ship147",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 314,
  LEVEL = 16,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 140,
  AVATAR = "head144",
  SHIP = "ship147",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 315,
  LEVEL = 0,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 141,
  AVATAR = "head145",
  SHIP = "ship148",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 315,
  LEVEL = 1,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 141,
  AVATAR = "head145",
  SHIP = "ship148",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 315,
  LEVEL = 2,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 141,
  AVATAR = "head145",
  SHIP = "ship148",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 315,
  LEVEL = 3,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 10141,
  AVATAR = "head145",
  SHIP = "ship148",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 315,
  LEVEL = 4,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 10141,
  AVATAR = "head145",
  SHIP = "ship148",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 315,
  LEVEL = 5,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 10141,
  AVATAR = "head145",
  SHIP = "ship148",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 315,
  LEVEL = 6,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 10141,
  AVATAR = "head145",
  SHIP = "ship148",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 315,
  LEVEL = 7,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 11141,
  AVATAR = "head145",
  SHIP = "ship148",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 315,
  LEVEL = 8,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 11141,
  AVATAR = "head145",
  SHIP = "ship148",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 315,
  LEVEL = 9,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 11141,
  AVATAR = "head145",
  SHIP = "ship148",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 315,
  LEVEL = 10,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 12141,
  AVATAR = "head145",
  SHIP = "ship148",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 315,
  LEVEL = 11,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 12141,
  AVATAR = "head145",
  SHIP = "ship148",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 315,
  LEVEL = 12,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 12141,
  AVATAR = "head145",
  SHIP = "ship148",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 315,
  LEVEL = 13,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 12141,
  AVATAR = "head145",
  SHIP = "ship148",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 315,
  LEVEL = 14,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 12141,
  AVATAR = "head145",
  SHIP = "ship148",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 315,
  LEVEL = 15,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 13141,
  AVATAR = "head145",
  SHIP = "ship148",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 315,
  LEVEL = 16,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 13141,
  AVATAR = "head145",
  SHIP = "ship148",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 316,
  LEVEL = 0,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 142,
  AVATAR = "head146",
  SHIP = "ship149",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 316,
  LEVEL = 1,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 142,
  AVATAR = "head146",
  SHIP = "ship149",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 316,
  LEVEL = 2,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 142,
  AVATAR = "head146",
  SHIP = "ship149",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 316,
  LEVEL = 3,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 142,
  AVATAR = "head146",
  SHIP = "ship149",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 316,
  LEVEL = 4,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 142,
  AVATAR = "head146",
  SHIP = "ship149",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 316,
  LEVEL = 5,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 142,
  AVATAR = "head146",
  SHIP = "ship149",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 316,
  LEVEL = 6,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 142,
  AVATAR = "head146",
  SHIP = "ship149",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 316,
  LEVEL = 7,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 142,
  AVATAR = "head146",
  SHIP = "ship149",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 316,
  LEVEL = 8,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 142,
  AVATAR = "head146",
  SHIP = "ship149",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 316,
  LEVEL = 9,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 142,
  AVATAR = "head146",
  SHIP = "ship149",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 316,
  LEVEL = 10,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 142,
  AVATAR = "head146",
  SHIP = "ship149",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 316,
  LEVEL = 11,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 142,
  AVATAR = "head146",
  SHIP = "ship149",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 316,
  LEVEL = 12,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 142,
  AVATAR = "head146",
  SHIP = "ship149",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 316,
  LEVEL = 13,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 142,
  AVATAR = "head146",
  SHIP = "ship149",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 316,
  LEVEL = 14,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 142,
  AVATAR = "head146",
  SHIP = "ship149",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 316,
  LEVEL = 15,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 142,
  AVATAR = "head146",
  SHIP = "ship149",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 316,
  LEVEL = 16,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 142,
  AVATAR = "head146",
  SHIP = "ship149",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 317,
  LEVEL = 0,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 144,
  AVATAR = "head147",
  SHIP = "ship150",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 317,
  LEVEL = 1,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 144,
  AVATAR = "head147",
  SHIP = "ship150",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 317,
  LEVEL = 2,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 144,
  AVATAR = "head147",
  SHIP = "ship150",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 317,
  LEVEL = 3,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 144,
  AVATAR = "head147",
  SHIP = "ship150",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 317,
  LEVEL = 4,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 144,
  AVATAR = "head147",
  SHIP = "ship150",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 317,
  LEVEL = 5,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 144,
  AVATAR = "head147",
  SHIP = "ship150",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 317,
  LEVEL = 6,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 144,
  AVATAR = "head147",
  SHIP = "ship150",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 317,
  LEVEL = 7,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 144,
  AVATAR = "head147",
  SHIP = "ship150",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 317,
  LEVEL = 8,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 144,
  AVATAR = "head147",
  SHIP = "ship150",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 317,
  LEVEL = 9,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 144,
  AVATAR = "head147",
  SHIP = "ship150",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 317,
  LEVEL = 10,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 144,
  AVATAR = "head147",
  SHIP = "ship150",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 317,
  LEVEL = 11,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 144,
  AVATAR = "head147",
  SHIP = "ship150",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 317,
  LEVEL = 12,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 144,
  AVATAR = "head147",
  SHIP = "ship150",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 317,
  LEVEL = 13,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 144,
  AVATAR = "head147",
  SHIP = "ship150",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 317,
  LEVEL = 14,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 144,
  AVATAR = "head147",
  SHIP = "ship150",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 317,
  LEVEL = 15,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 144,
  AVATAR = "head147",
  SHIP = "ship150",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 317,
  LEVEL = 16,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 144,
  AVATAR = "head147",
  SHIP = "ship150",
  sex = 2,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 318,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 145,
  AVATAR = "head148",
  SHIP = "ship151",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 318,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 145,
  AVATAR = "head148",
  SHIP = "ship151",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 318,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 145,
  AVATAR = "head148",
  SHIP = "ship151",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 318,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 145,
  AVATAR = "head148",
  SHIP = "ship151",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 318,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 145,
  AVATAR = "head148",
  SHIP = "ship151",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 318,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 145,
  AVATAR = "head148",
  SHIP = "ship151",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 318,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 145,
  AVATAR = "head148",
  SHIP = "ship151",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 318,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 145,
  AVATAR = "head148",
  SHIP = "ship151",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 318,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 145,
  AVATAR = "head148",
  SHIP = "ship151",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 318,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 145,
  AVATAR = "head148",
  SHIP = "ship151",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 318,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 145,
  AVATAR = "head148",
  SHIP = "ship151",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 318,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 145,
  AVATAR = "head148",
  SHIP = "ship151",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 318,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 145,
  AVATAR = "head148",
  SHIP = "ship151",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 318,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 145,
  AVATAR = "head148",
  SHIP = "ship151",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 318,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 145,
  AVATAR = "head148",
  SHIP = "ship151",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 318,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 145,
  AVATAR = "head148",
  SHIP = "ship151",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 318,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 145,
  AVATAR = "head148",
  SHIP = "ship151",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 319,
  LEVEL = 0,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 146,
  AVATAR = "head149",
  SHIP = "ship152",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 319,
  LEVEL = 1,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 146,
  AVATAR = "head149",
  SHIP = "ship152",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 319,
  LEVEL = 2,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 146,
  AVATAR = "head149",
  SHIP = "ship152",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 319,
  LEVEL = 3,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 146,
  AVATAR = "head149",
  SHIP = "ship152",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 319,
  LEVEL = 4,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 146,
  AVATAR = "head149",
  SHIP = "ship152",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 319,
  LEVEL = 5,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 146,
  AVATAR = "head149",
  SHIP = "ship152",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 319,
  LEVEL = 6,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 146,
  AVATAR = "head149",
  SHIP = "ship152",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 319,
  LEVEL = 7,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 146,
  AVATAR = "head149",
  SHIP = "ship152",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 319,
  LEVEL = 8,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 146,
  AVATAR = "head149",
  SHIP = "ship152",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 319,
  LEVEL = 9,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 146,
  AVATAR = "head149",
  SHIP = "ship152",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 319,
  LEVEL = 10,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 146,
  AVATAR = "head149",
  SHIP = "ship152",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 319,
  LEVEL = 11,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 146,
  AVATAR = "head149",
  SHIP = "ship152",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 319,
  LEVEL = 12,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 146,
  AVATAR = "head149",
  SHIP = "ship152",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 319,
  LEVEL = 13,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 146,
  AVATAR = "head149",
  SHIP = "ship152",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 319,
  LEVEL = 14,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 146,
  AVATAR = "head149",
  SHIP = "ship152",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 319,
  LEVEL = 15,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 146,
  AVATAR = "head149",
  SHIP = "ship152",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 319,
  LEVEL = 16,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 146,
  AVATAR = "head149",
  SHIP = "ship152",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 320,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 11147,
  AVATAR = "head150",
  SHIP = "ship153",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 320,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 11147,
  AVATAR = "head150",
  SHIP = "ship153",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 320,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 11147,
  AVATAR = "head150",
  SHIP = "ship153",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 320,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 11147,
  AVATAR = "head150",
  SHIP = "ship153",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 320,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 11147,
  AVATAR = "head150",
  SHIP = "ship153",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 320,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 11147,
  AVATAR = "head150",
  SHIP = "ship153",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 320,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 11147,
  AVATAR = "head150",
  SHIP = "ship153",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 320,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 12147,
  AVATAR = "head150",
  SHIP = "ship153",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 320,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 12147,
  AVATAR = "head150",
  SHIP = "ship153",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 320,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 12147,
  AVATAR = "head150",
  SHIP = "ship153",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 320,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 13147,
  AVATAR = "head150",
  SHIP = "ship153",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 320,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 13147,
  AVATAR = "head150",
  SHIP = "ship153",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 320,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 13147,
  AVATAR = "head150",
  SHIP = "ship153",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 320,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 13147,
  AVATAR = "head150",
  SHIP = "ship153",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 320,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 13147,
  AVATAR = "head150",
  SHIP = "ship153",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 320,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 13147,
  AVATAR = "head150",
  SHIP = "ship153",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 320,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 13147,
  AVATAR = "head150",
  SHIP = "ship153",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 321,
  LEVEL = 0,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 148,
  AVATAR = "head151",
  SHIP = "ship154",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 321,
  LEVEL = 1,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 148,
  AVATAR = "head151",
  SHIP = "ship154",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 321,
  LEVEL = 2,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 148,
  AVATAR = "head151",
  SHIP = "ship154",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 321,
  LEVEL = 3,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 10148,
  AVATAR = "head151",
  SHIP = "ship154",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 321,
  LEVEL = 4,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 10148,
  AVATAR = "head151",
  SHIP = "ship154",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 321,
  LEVEL = 5,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 10148,
  AVATAR = "head151",
  SHIP = "ship154",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 321,
  LEVEL = 6,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 10148,
  AVATAR = "head151",
  SHIP = "ship154",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 321,
  LEVEL = 7,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 11148,
  AVATAR = "head151",
  SHIP = "ship154",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 321,
  LEVEL = 8,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 11148,
  AVATAR = "head151",
  SHIP = "ship154",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 321,
  LEVEL = 9,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 11148,
  AVATAR = "head151",
  SHIP = "ship154",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 321,
  LEVEL = 10,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 11148,
  AVATAR = "head151",
  SHIP = "ship154",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 321,
  LEVEL = 11,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 11148,
  AVATAR = "head151",
  SHIP = "ship154",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 321,
  LEVEL = 12,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 12148,
  AVATAR = "head151",
  SHIP = "ship154",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 321,
  LEVEL = 13,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 12148,
  AVATAR = "head151",
  SHIP = "ship154",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 321,
  LEVEL = 14,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 12148,
  AVATAR = "head151",
  SHIP = "ship154",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 321,
  LEVEL = 15,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 12148,
  AVATAR = "head151",
  SHIP = "ship154",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 321,
  LEVEL = 16,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 12148,
  AVATAR = "head151",
  SHIP = "ship154",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 322,
  LEVEL = 0,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 149,
  AVATAR = "head152",
  SHIP = "ship155",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 322,
  LEVEL = 1,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 149,
  AVATAR = "head152",
  SHIP = "ship155",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 322,
  LEVEL = 2,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 149,
  AVATAR = "head152",
  SHIP = "ship155",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 322,
  LEVEL = 3,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 149,
  AVATAR = "head152",
  SHIP = "ship155",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 322,
  LEVEL = 4,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 149,
  AVATAR = "head152",
  SHIP = "ship155",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 322,
  LEVEL = 5,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 149,
  AVATAR = "head152",
  SHIP = "ship155",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 322,
  LEVEL = 6,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 149,
  AVATAR = "head152",
  SHIP = "ship155",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 322,
  LEVEL = 7,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 149,
  AVATAR = "head152",
  SHIP = "ship155",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 322,
  LEVEL = 8,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 149,
  AVATAR = "head152",
  SHIP = "ship155",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 322,
  LEVEL = 9,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 149,
  AVATAR = "head152",
  SHIP = "ship155",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 322,
  LEVEL = 10,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 149,
  AVATAR = "head152",
  SHIP = "ship155",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 322,
  LEVEL = 11,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 149,
  AVATAR = "head152",
  SHIP = "ship155",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 322,
  LEVEL = 12,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 149,
  AVATAR = "head152",
  SHIP = "ship155",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 322,
  LEVEL = 13,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 149,
  AVATAR = "head152",
  SHIP = "ship155",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 322,
  LEVEL = 14,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 149,
  AVATAR = "head152",
  SHIP = "ship155",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 322,
  LEVEL = 15,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 149,
  AVATAR = "head152",
  SHIP = "ship155",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 322,
  LEVEL = 16,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 149,
  AVATAR = "head152",
  SHIP = "ship155",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 323,
  LEVEL = 0,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 150,
  AVATAR = "head153",
  SHIP = "ship156",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 323,
  LEVEL = 1,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 150,
  AVATAR = "head153",
  SHIP = "ship156",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 323,
  LEVEL = 2,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 150,
  AVATAR = "head153",
  SHIP = "ship156",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 323,
  LEVEL = 3,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 150,
  AVATAR = "head153",
  SHIP = "ship156",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 323,
  LEVEL = 4,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 150,
  AVATAR = "head153",
  SHIP = "ship156",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 323,
  LEVEL = 5,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 150,
  AVATAR = "head153",
  SHIP = "ship156",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 323,
  LEVEL = 6,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 150,
  AVATAR = "head153",
  SHIP = "ship156",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 323,
  LEVEL = 7,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 150,
  AVATAR = "head153",
  SHIP = "ship156",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 323,
  LEVEL = 8,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 150,
  AVATAR = "head153",
  SHIP = "ship156",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 323,
  LEVEL = 9,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 150,
  AVATAR = "head153",
  SHIP = "ship156",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 323,
  LEVEL = 10,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 150,
  AVATAR = "head153",
  SHIP = "ship156",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 323,
  LEVEL = 11,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 150,
  AVATAR = "head153",
  SHIP = "ship156",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 323,
  LEVEL = 12,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 150,
  AVATAR = "head153",
  SHIP = "ship156",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 323,
  LEVEL = 13,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 150,
  AVATAR = "head153",
  SHIP = "ship156",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 323,
  LEVEL = 14,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 150,
  AVATAR = "head153",
  SHIP = "ship156",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 323,
  LEVEL = 15,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 150,
  AVATAR = "head153",
  SHIP = "ship156",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 323,
  LEVEL = 16,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 150,
  AVATAR = "head153",
  SHIP = "ship156",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 324,
  LEVEL = 0,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 13151,
  AVATAR = "head154",
  SHIP = "ship157",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 324,
  LEVEL = 1,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 13151,
  AVATAR = "head154",
  SHIP = "ship157",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 324,
  LEVEL = 2,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 13151,
  AVATAR = "head154",
  SHIP = "ship157",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 324,
  LEVEL = 3,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 13151,
  AVATAR = "head154",
  SHIP = "ship157",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 324,
  LEVEL = 4,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 13151,
  AVATAR = "head154",
  SHIP = "ship157",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 324,
  LEVEL = 5,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 13151,
  AVATAR = "head154",
  SHIP = "ship157",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 324,
  LEVEL = 6,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 13151,
  AVATAR = "head154",
  SHIP = "ship157",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 324,
  LEVEL = 7,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 13151,
  AVATAR = "head154",
  SHIP = "ship157",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 324,
  LEVEL = 8,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 13151,
  AVATAR = "head154",
  SHIP = "ship157",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 324,
  LEVEL = 9,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 13151,
  AVATAR = "head154",
  SHIP = "ship157",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 324,
  LEVEL = 10,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 13151,
  AVATAR = "head154",
  SHIP = "ship157",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 324,
  LEVEL = 11,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 13151,
  AVATAR = "head154",
  SHIP = "ship157",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 324,
  LEVEL = 12,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 13151,
  AVATAR = "head154",
  SHIP = "ship157",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 324,
  LEVEL = 13,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 13151,
  AVATAR = "head154",
  SHIP = "ship157",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 324,
  LEVEL = 14,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 13151,
  AVATAR = "head154",
  SHIP = "ship157",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 324,
  LEVEL = 15,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 13151,
  AVATAR = "head154",
  SHIP = "ship157",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 324,
  LEVEL = 16,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 13151,
  AVATAR = "head154",
  SHIP = "ship157",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 325,
  LEVEL = 0,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 152,
  AVATAR = "head155",
  SHIP = "ship158",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 325,
  LEVEL = 1,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 152,
  AVATAR = "head155",
  SHIP = "ship158",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 325,
  LEVEL = 2,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 152,
  AVATAR = "head155",
  SHIP = "ship158",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 325,
  LEVEL = 3,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 152,
  AVATAR = "head155",
  SHIP = "ship158",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 325,
  LEVEL = 4,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 152,
  AVATAR = "head155",
  SHIP = "ship158",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 325,
  LEVEL = 5,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 152,
  AVATAR = "head155",
  SHIP = "ship158",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 325,
  LEVEL = 6,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 152,
  AVATAR = "head155",
  SHIP = "ship158",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 325,
  LEVEL = 7,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 152,
  AVATAR = "head155",
  SHIP = "ship158",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 325,
  LEVEL = 8,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 152,
  AVATAR = "head155",
  SHIP = "ship158",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 325,
  LEVEL = 9,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 152,
  AVATAR = "head155",
  SHIP = "ship158",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 325,
  LEVEL = 10,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 152,
  AVATAR = "head155",
  SHIP = "ship158",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 325,
  LEVEL = 11,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 152,
  AVATAR = "head155",
  SHIP = "ship158",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 325,
  LEVEL = 12,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 152,
  AVATAR = "head155",
  SHIP = "ship158",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 325,
  LEVEL = 13,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 152,
  AVATAR = "head155",
  SHIP = "ship158",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 325,
  LEVEL = 14,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 152,
  AVATAR = "head155",
  SHIP = "ship158",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 325,
  LEVEL = 15,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 152,
  AVATAR = "head155",
  SHIP = "ship158",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 325,
  LEVEL = 16,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 152,
  AVATAR = "head155",
  SHIP = "ship158",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 326,
  LEVEL = 0,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 12153,
  AVATAR = "head156",
  SHIP = "ship159",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 326,
  LEVEL = 1,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 12153,
  AVATAR = "head156",
  SHIP = "ship159",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 326,
  LEVEL = 2,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 12153,
  AVATAR = "head156",
  SHIP = "ship159",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 326,
  LEVEL = 3,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 12153,
  AVATAR = "head156",
  SHIP = "ship159",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 326,
  LEVEL = 4,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 12153,
  AVATAR = "head156",
  SHIP = "ship159",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 326,
  LEVEL = 5,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 12153,
  AVATAR = "head156",
  SHIP = "ship159",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 326,
  LEVEL = 6,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 12153,
  AVATAR = "head156",
  SHIP = "ship159",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 326,
  LEVEL = 7,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 12153,
  AVATAR = "head156",
  SHIP = "ship159",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 326,
  LEVEL = 8,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 12153,
  AVATAR = "head156",
  SHIP = "ship159",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 326,
  LEVEL = 9,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 12153,
  AVATAR = "head156",
  SHIP = "ship159",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 326,
  LEVEL = 10,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 12153,
  AVATAR = "head156",
  SHIP = "ship159",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 326,
  LEVEL = 11,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 12153,
  AVATAR = "head156",
  SHIP = "ship159",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 326,
  LEVEL = 12,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 12153,
  AVATAR = "head156",
  SHIP = "ship159",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 326,
  LEVEL = 13,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 12153,
  AVATAR = "head156",
  SHIP = "ship159",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 326,
  LEVEL = 14,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 12153,
  AVATAR = "head156",
  SHIP = "ship159",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 326,
  LEVEL = 15,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 12153,
  AVATAR = "head156",
  SHIP = "ship159",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 326,
  LEVEL = 16,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 12153,
  AVATAR = "head156",
  SHIP = "ship159",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 327,
  LEVEL = 0,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 819,
  AVATAR = "head157",
  SHIP = "ship160",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 327,
  LEVEL = 1,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 819,
  AVATAR = "head157",
  SHIP = "ship160",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 327,
  LEVEL = 2,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 819,
  AVATAR = "head157",
  SHIP = "ship160",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 327,
  LEVEL = 3,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 819,
  AVATAR = "head157",
  SHIP = "ship160",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 327,
  LEVEL = 4,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 819,
  AVATAR = "head157",
  SHIP = "ship160",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 327,
  LEVEL = 5,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 819,
  AVATAR = "head157",
  SHIP = "ship160",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 327,
  LEVEL = 6,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 819,
  AVATAR = "head157",
  SHIP = "ship160",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 327,
  LEVEL = 7,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 819,
  AVATAR = "head157",
  SHIP = "ship160",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 327,
  LEVEL = 8,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 819,
  AVATAR = "head157",
  SHIP = "ship160",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 327,
  LEVEL = 9,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 819,
  AVATAR = "head157",
  SHIP = "ship160",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 327,
  LEVEL = 10,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 819,
  AVATAR = "head157",
  SHIP = "ship160",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 327,
  LEVEL = 11,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 819,
  AVATAR = "head157",
  SHIP = "ship160",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 327,
  LEVEL = 12,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 819,
  AVATAR = "head157",
  SHIP = "ship160",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 327,
  LEVEL = 13,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 819,
  AVATAR = "head157",
  SHIP = "ship160",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 327,
  LEVEL = 14,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 819,
  AVATAR = "head157",
  SHIP = "ship160",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 327,
  LEVEL = 15,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 819,
  AVATAR = "head157",
  SHIP = "ship160",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 327,
  LEVEL = 16,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 819,
  AVATAR = "head157",
  SHIP = "ship160",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 328,
  LEVEL = 0,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 814,
  AVATAR = "head172",
  SHIP = "ship179",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 328,
  LEVEL = 1,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 814,
  AVATAR = "head172",
  SHIP = "ship179",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 328,
  LEVEL = 2,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 814,
  AVATAR = "head172",
  SHIP = "ship179",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 328,
  LEVEL = 3,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 814,
  AVATAR = "head172",
  SHIP = "ship179",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 328,
  LEVEL = 4,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 814,
  AVATAR = "head172",
  SHIP = "ship179",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 328,
  LEVEL = 5,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 814,
  AVATAR = "head172",
  SHIP = "ship179",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 328,
  LEVEL = 6,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 814,
  AVATAR = "head172",
  SHIP = "ship179",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 328,
  LEVEL = 7,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 814,
  AVATAR = "head172",
  SHIP = "ship179",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 328,
  LEVEL = 8,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 814,
  AVATAR = "head172",
  SHIP = "ship179",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 328,
  LEVEL = 9,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 814,
  AVATAR = "head172",
  SHIP = "ship179",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 328,
  LEVEL = 10,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 814,
  AVATAR = "head172",
  SHIP = "ship179",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 328,
  LEVEL = 11,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 814,
  AVATAR = "head172",
  SHIP = "ship179",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 328,
  LEVEL = 12,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 814,
  AVATAR = "head172",
  SHIP = "ship179",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 328,
  LEVEL = 13,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 814,
  AVATAR = "head172",
  SHIP = "ship179",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 328,
  LEVEL = 14,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 814,
  AVATAR = "head172",
  SHIP = "ship179",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 328,
  LEVEL = 15,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 814,
  AVATAR = "head172",
  SHIP = "ship179",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 328,
  LEVEL = 16,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 814,
  AVATAR = "head172",
  SHIP = "ship179",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 329,
  LEVEL = 0,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 11801,
  AVATAR = "head159",
  SHIP = "ship162",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 329,
  LEVEL = 1,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 11801,
  AVATAR = "head159",
  SHIP = "ship162",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 329,
  LEVEL = 2,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 11801,
  AVATAR = "head159",
  SHIP = "ship162",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 329,
  LEVEL = 3,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 11801,
  AVATAR = "head159",
  SHIP = "ship162",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 329,
  LEVEL = 4,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 11801,
  AVATAR = "head159",
  SHIP = "ship162",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 329,
  LEVEL = 5,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 11801,
  AVATAR = "head159",
  SHIP = "ship162",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 329,
  LEVEL = 6,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 11801,
  AVATAR = "head159",
  SHIP = "ship162",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 329,
  LEVEL = 7,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 12801,
  AVATAR = "head159",
  SHIP = "ship162",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 329,
  LEVEL = 8,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 12801,
  AVATAR = "head159",
  SHIP = "ship162",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 329,
  LEVEL = 9,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 12801,
  AVATAR = "head159",
  SHIP = "ship162",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 329,
  LEVEL = 10,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 13801,
  AVATAR = "head159",
  SHIP = "ship162",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 329,
  LEVEL = 11,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 13801,
  AVATAR = "head159",
  SHIP = "ship162",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 329,
  LEVEL = 12,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 13801,
  AVATAR = "head159",
  SHIP = "ship162",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 329,
  LEVEL = 13,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 13801,
  AVATAR = "head159",
  SHIP = "ship162",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 329,
  LEVEL = 14,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 13801,
  AVATAR = "head159",
  SHIP = "ship162",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 329,
  LEVEL = 15,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 13801,
  AVATAR = "head159",
  SHIP = "ship162",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 329,
  LEVEL = 16,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 13801,
  AVATAR = "head159",
  SHIP = "ship162",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 330,
  LEVEL = 0,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 811,
  AVATAR = "head160",
  SHIP = "ship163",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 330,
  LEVEL = 1,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 811,
  AVATAR = "head160",
  SHIP = "ship163",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 330,
  LEVEL = 2,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 811,
  AVATAR = "head160",
  SHIP = "ship163",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 330,
  LEVEL = 3,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 811,
  AVATAR = "head160",
  SHIP = "ship163",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 330,
  LEVEL = 4,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 811,
  AVATAR = "head160",
  SHIP = "ship163",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 330,
  LEVEL = 5,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 811,
  AVATAR = "head160",
  SHIP = "ship163",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 330,
  LEVEL = 6,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 811,
  AVATAR = "head160",
  SHIP = "ship163",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 330,
  LEVEL = 7,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 811,
  AVATAR = "head160",
  SHIP = "ship163",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 330,
  LEVEL = 8,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 811,
  AVATAR = "head160",
  SHIP = "ship163",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 330,
  LEVEL = 9,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 811,
  AVATAR = "head160",
  SHIP = "ship163",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 330,
  LEVEL = 10,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 811,
  AVATAR = "head160",
  SHIP = "ship163",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 330,
  LEVEL = 11,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 811,
  AVATAR = "head160",
  SHIP = "ship163",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 330,
  LEVEL = 12,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 811,
  AVATAR = "head160",
  SHIP = "ship163",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 330,
  LEVEL = 13,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 811,
  AVATAR = "head160",
  SHIP = "ship163",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 330,
  LEVEL = 14,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 811,
  AVATAR = "head160",
  SHIP = "ship163",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 330,
  LEVEL = 15,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 811,
  AVATAR = "head160",
  SHIP = "ship163",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 330,
  LEVEL = 16,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 811,
  AVATAR = "head160",
  SHIP = "ship163",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 331,
  LEVEL = 0,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 158,
  AVATAR = "head161",
  SHIP = "ship164",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 331,
  LEVEL = 1,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 158,
  AVATAR = "head161",
  SHIP = "ship164",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 331,
  LEVEL = 2,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 158,
  AVATAR = "head161",
  SHIP = "ship164",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 331,
  LEVEL = 3,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 158,
  AVATAR = "head161",
  SHIP = "ship164",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 331,
  LEVEL = 4,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 158,
  AVATAR = "head161",
  SHIP = "ship164",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 331,
  LEVEL = 5,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 158,
  AVATAR = "head161",
  SHIP = "ship164",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 331,
  LEVEL = 6,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 158,
  AVATAR = "head161",
  SHIP = "ship164",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 331,
  LEVEL = 7,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 158,
  AVATAR = "head161",
  SHIP = "ship164",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 331,
  LEVEL = 8,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 158,
  AVATAR = "head161",
  SHIP = "ship164",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 331,
  LEVEL = 9,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 158,
  AVATAR = "head161",
  SHIP = "ship164",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 331,
  LEVEL = 10,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 158,
  AVATAR = "head161",
  SHIP = "ship164",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 331,
  LEVEL = 11,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 158,
  AVATAR = "head161",
  SHIP = "ship164",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 331,
  LEVEL = 12,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 158,
  AVATAR = "head161",
  SHIP = "ship164",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 331,
  LEVEL = 13,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 158,
  AVATAR = "head161",
  SHIP = "ship164",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 331,
  LEVEL = 14,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 158,
  AVATAR = "head161",
  SHIP = "ship164",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 331,
  LEVEL = 15,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 158,
  AVATAR = "head161",
  SHIP = "ship164",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 331,
  LEVEL = 16,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 158,
  AVATAR = "head161",
  SHIP = "ship164",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 332,
  LEVEL = 0,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 800,
  AVATAR = "head162",
  SHIP = "ship165",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 332,
  LEVEL = 1,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 800,
  AVATAR = "head162",
  SHIP = "ship165",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 332,
  LEVEL = 2,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 800,
  AVATAR = "head162",
  SHIP = "ship165",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 332,
  LEVEL = 3,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 800,
  AVATAR = "head162",
  SHIP = "ship165",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 332,
  LEVEL = 4,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 800,
  AVATAR = "head162",
  SHIP = "ship165",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 332,
  LEVEL = 5,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 10800,
  AVATAR = "head162",
  SHIP = "ship165",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 332,
  LEVEL = 6,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 10800,
  AVATAR = "head162",
  SHIP = "ship165",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 332,
  LEVEL = 7,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 11800,
  AVATAR = "head162",
  SHIP = "ship165",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 332,
  LEVEL = 8,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 11800,
  AVATAR = "head162",
  SHIP = "ship165",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 332,
  LEVEL = 9,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 11800,
  AVATAR = "head162",
  SHIP = "ship165",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 332,
  LEVEL = 10,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 11800,
  AVATAR = "head162",
  SHIP = "ship165",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 332,
  LEVEL = 11,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 11800,
  AVATAR = "head162",
  SHIP = "ship165",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 332,
  LEVEL = 12,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 12800,
  AVATAR = "head162",
  SHIP = "ship165",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 332,
  LEVEL = 13,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 12800,
  AVATAR = "head162",
  SHIP = "ship165",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 332,
  LEVEL = 14,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 12800,
  AVATAR = "head162",
  SHIP = "ship165",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 332,
  LEVEL = 15,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 13800,
  AVATAR = "head162",
  SHIP = "ship165",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 332,
  LEVEL = 16,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 13800,
  AVATAR = "head162",
  SHIP = "ship165",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 333,
  LEVEL = 0,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 13802,
  AVATAR = "head112",
  SHIP = "ship115",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 333,
  LEVEL = 1,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 13802,
  AVATAR = "head112",
  SHIP = "ship115",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 333,
  LEVEL = 2,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 13802,
  AVATAR = "head112",
  SHIP = "ship115",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 333,
  LEVEL = 3,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 13802,
  AVATAR = "head112",
  SHIP = "ship115",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 333,
  LEVEL = 4,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 13802,
  AVATAR = "head112",
  SHIP = "ship115",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 333,
  LEVEL = 5,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 13802,
  AVATAR = "head112",
  SHIP = "ship115",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 333,
  LEVEL = 6,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 13802,
  AVATAR = "head112",
  SHIP = "ship115",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 333,
  LEVEL = 7,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 13802,
  AVATAR = "head112",
  SHIP = "ship115",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 333,
  LEVEL = 8,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 13802,
  AVATAR = "head112",
  SHIP = "ship115",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 333,
  LEVEL = 9,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 13802,
  AVATAR = "head112",
  SHIP = "ship115",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 333,
  LEVEL = 10,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 13802,
  AVATAR = "head112",
  SHIP = "ship115",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 333,
  LEVEL = 11,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 13802,
  AVATAR = "head112",
  SHIP = "ship115",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 333,
  LEVEL = 12,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 13802,
  AVATAR = "head112",
  SHIP = "ship115",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 333,
  LEVEL = 13,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 13802,
  AVATAR = "head112",
  SHIP = "ship115",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 333,
  LEVEL = 14,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 13802,
  AVATAR = "head112",
  SHIP = "ship115",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 333,
  LEVEL = 15,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 13802,
  AVATAR = "head112",
  SHIP = "ship115",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 333,
  LEVEL = 16,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 13802,
  AVATAR = "head112",
  SHIP = "ship115",
  sex = 2,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 334,
  LEVEL = 0,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 1006,
  AVATAR = "head164",
  SHIP = "ship170",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 334,
  LEVEL = 1,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 1006,
  AVATAR = "head164",
  SHIP = "ship170",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 334,
  LEVEL = 2,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 1006,
  AVATAR = "head164",
  SHIP = "ship170",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 334,
  LEVEL = 3,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 1006,
  AVATAR = "head164",
  SHIP = "ship170",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 334,
  LEVEL = 4,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 1006,
  AVATAR = "head164",
  SHIP = "ship170",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 334,
  LEVEL = 5,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 1006,
  AVATAR = "head164",
  SHIP = "ship170",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 334,
  LEVEL = 6,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 1006,
  AVATAR = "head164",
  SHIP = "ship170",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 334,
  LEVEL = 7,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 1006,
  AVATAR = "head164",
  SHIP = "ship170",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 334,
  LEVEL = 8,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 1006,
  AVATAR = "head164",
  SHIP = "ship170",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 334,
  LEVEL = 9,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 1006,
  AVATAR = "head164",
  SHIP = "ship170",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 334,
  LEVEL = 10,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 1006,
  AVATAR = "head164",
  SHIP = "ship170",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 334,
  LEVEL = 11,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 1006,
  AVATAR = "head164",
  SHIP = "ship170",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 334,
  LEVEL = 12,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 1006,
  AVATAR = "head164",
  SHIP = "ship170",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 334,
  LEVEL = 13,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 1006,
  AVATAR = "head164",
  SHIP = "ship170",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 334,
  LEVEL = 14,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 1006,
  AVATAR = "head164",
  SHIP = "ship170",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 334,
  LEVEL = 15,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 1006,
  AVATAR = "head164",
  SHIP = "ship170",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 334,
  LEVEL = 16,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 1006,
  AVATAR = "head164",
  SHIP = "ship170",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 335,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 13803,
  AVATAR = "ship171",
  SHIP = "1.0",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 335,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 13803,
  AVATAR = "ship171",
  SHIP = "1.0",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 335,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 13803,
  AVATAR = "ship171",
  SHIP = "1.0",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 335,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 13803,
  AVATAR = "ship171",
  SHIP = "1.0",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 335,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 13803,
  AVATAR = "ship171",
  SHIP = "1.0",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 335,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 13803,
  AVATAR = "ship171",
  SHIP = "1.0",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 335,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 13803,
  AVATAR = "ship171",
  SHIP = "1.0",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 335,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 13803,
  AVATAR = "ship171",
  SHIP = "1.0",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 335,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 13803,
  AVATAR = "ship171",
  SHIP = "1.0",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 335,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 13803,
  AVATAR = "ship171",
  SHIP = "1.0",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 335,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 13803,
  AVATAR = "ship171",
  SHIP = "1.0",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 335,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 13803,
  AVATAR = "ship171",
  SHIP = "1.0",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 335,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 13803,
  AVATAR = "ship171",
  SHIP = "1.0",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 335,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 13803,
  AVATAR = "ship171",
  SHIP = "1.0",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 335,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 13803,
  AVATAR = "ship171",
  SHIP = "1.0",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 335,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 13803,
  AVATAR = "ship171",
  SHIP = "1.0",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 335,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 13803,
  AVATAR = "ship171",
  SHIP = "1.0",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 336,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 806,
  AVATAR = "head166",
  SHIP = "ship172",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 336,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 806,
  AVATAR = "head166",
  SHIP = "ship172",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 336,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 806,
  AVATAR = "head166",
  SHIP = "ship172",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 336,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 806,
  AVATAR = "head166",
  SHIP = "ship172",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 336,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 806,
  AVATAR = "head166",
  SHIP = "ship172",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 336,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 806,
  AVATAR = "head166",
  SHIP = "ship172",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 336,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 806,
  AVATAR = "head166",
  SHIP = "ship172",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 336,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 806,
  AVATAR = "head166",
  SHIP = "ship172",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 336,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 806,
  AVATAR = "head166",
  SHIP = "ship172",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 336,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 806,
  AVATAR = "head166",
  SHIP = "ship172",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 336,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 806,
  AVATAR = "head166",
  SHIP = "ship172",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 336,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 806,
  AVATAR = "head166",
  SHIP = "ship172",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 336,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 806,
  AVATAR = "head166",
  SHIP = "ship172",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 336,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 806,
  AVATAR = "head166",
  SHIP = "ship172",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 336,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 806,
  AVATAR = "head166",
  SHIP = "ship172",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 336,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 806,
  AVATAR = "head166",
  SHIP = "ship172",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 336,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 806,
  AVATAR = "head166",
  SHIP = "ship172",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 337,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 820,
  AVATAR = "head167",
  SHIP = "ship173",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 337,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 820,
  AVATAR = "head167",
  SHIP = "ship173",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 337,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 820,
  AVATAR = "head167",
  SHIP = "ship173",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 337,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 820,
  AVATAR = "head167",
  SHIP = "ship173",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 337,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 820,
  AVATAR = "head167",
  SHIP = "ship173",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 337,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 820,
  AVATAR = "head167",
  SHIP = "ship173",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 337,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 820,
  AVATAR = "head167",
  SHIP = "ship173",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 337,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 820,
  AVATAR = "head167",
  SHIP = "ship173",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 337,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 820,
  AVATAR = "head167",
  SHIP = "ship173",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 337,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 820,
  AVATAR = "head167",
  SHIP = "ship173",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 337,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 820,
  AVATAR = "head167",
  SHIP = "ship173",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 337,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 820,
  AVATAR = "head167",
  SHIP = "ship173",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 337,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 820,
  AVATAR = "head167",
  SHIP = "ship173",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 337,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 820,
  AVATAR = "head167",
  SHIP = "ship173",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 337,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 820,
  AVATAR = "head167",
  SHIP = "ship173",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 337,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 820,
  AVATAR = "head167",
  SHIP = "ship173",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 337,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 820,
  AVATAR = "head167",
  SHIP = "ship173",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 338,
  LEVEL = 0,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 13805,
  AVATAR = "head168",
  SHIP = "ship174",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 338,
  LEVEL = 1,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 13805,
  AVATAR = "head168",
  SHIP = "ship174",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 338,
  LEVEL = 2,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 13805,
  AVATAR = "head168",
  SHIP = "ship174",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 338,
  LEVEL = 3,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 13805,
  AVATAR = "head168",
  SHIP = "ship174",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 338,
  LEVEL = 4,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 13805,
  AVATAR = "head168",
  SHIP = "ship174",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 338,
  LEVEL = 5,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 13805,
  AVATAR = "head168",
  SHIP = "ship174",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 338,
  LEVEL = 6,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 13805,
  AVATAR = "head168",
  SHIP = "ship174",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 338,
  LEVEL = 7,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 13805,
  AVATAR = "head168",
  SHIP = "ship174",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 338,
  LEVEL = 8,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 13805,
  AVATAR = "head168",
  SHIP = "ship174",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 338,
  LEVEL = 9,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 13805,
  AVATAR = "head168",
  SHIP = "ship174",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 338,
  LEVEL = 10,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 13805,
  AVATAR = "head168",
  SHIP = "ship174",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 338,
  LEVEL = 11,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 13805,
  AVATAR = "head168",
  SHIP = "ship174",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 338,
  LEVEL = 12,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 13805,
  AVATAR = "head168",
  SHIP = "ship174",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 338,
  LEVEL = 13,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 13805,
  AVATAR = "head168",
  SHIP = "ship174",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 338,
  LEVEL = 14,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 13805,
  AVATAR = "head168",
  SHIP = "ship174",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 338,
  LEVEL = 15,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 13805,
  AVATAR = "head168",
  SHIP = "ship174",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 338,
  LEVEL = 16,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 13805,
  AVATAR = "head168",
  SHIP = "ship174",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 339,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 163,
  AVATAR = "head169",
  SHIP = "ship176",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 339,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 163,
  AVATAR = "head169",
  SHIP = "ship176",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 339,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 163,
  AVATAR = "head169",
  SHIP = "ship176",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 339,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 163,
  AVATAR = "head169",
  SHIP = "ship176",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 339,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 163,
  AVATAR = "head169",
  SHIP = "ship176",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 339,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 163,
  AVATAR = "head169",
  SHIP = "ship176",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 339,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 163,
  AVATAR = "head169",
  SHIP = "ship176",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 339,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 163,
  AVATAR = "head169",
  SHIP = "ship176",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 339,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 163,
  AVATAR = "head169",
  SHIP = "ship176",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 339,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 163,
  AVATAR = "head169",
  SHIP = "ship176",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 339,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 163,
  AVATAR = "head169",
  SHIP = "ship176",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 339,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 163,
  AVATAR = "head169",
  SHIP = "ship176",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 339,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 163,
  AVATAR = "head169",
  SHIP = "ship176",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 339,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 163,
  AVATAR = "head169",
  SHIP = "ship176",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 339,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 163,
  AVATAR = "head169",
  SHIP = "ship176",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 339,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 163,
  AVATAR = "head169",
  SHIP = "ship176",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 339,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 163,
  AVATAR = "head169",
  SHIP = "ship176",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 340,
  LEVEL = 0,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 807,
  AVATAR = "head170",
  SHIP = "ship177",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 340,
  LEVEL = 1,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 807,
  AVATAR = "head170",
  SHIP = "ship177",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 340,
  LEVEL = 2,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 807,
  AVATAR = "head170",
  SHIP = "ship177",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 340,
  LEVEL = 3,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 807,
  AVATAR = "head170",
  SHIP = "ship177",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 340,
  LEVEL = 4,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 807,
  AVATAR = "head170",
  SHIP = "ship177",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 340,
  LEVEL = 5,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 807,
  AVATAR = "head170",
  SHIP = "ship177",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 340,
  LEVEL = 6,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 807,
  AVATAR = "head170",
  SHIP = "ship177",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 340,
  LEVEL = 7,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 807,
  AVATAR = "head170",
  SHIP = "ship177",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 340,
  LEVEL = 8,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 807,
  AVATAR = "head170",
  SHIP = "ship177",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 340,
  LEVEL = 9,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 807,
  AVATAR = "head170",
  SHIP = "ship177",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 340,
  LEVEL = 10,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 807,
  AVATAR = "head170",
  SHIP = "ship177",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 340,
  LEVEL = 11,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 807,
  AVATAR = "head170",
  SHIP = "ship177",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 340,
  LEVEL = 12,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 807,
  AVATAR = "head170",
  SHIP = "ship177",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 340,
  LEVEL = 13,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 807,
  AVATAR = "head170",
  SHIP = "ship177",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 340,
  LEVEL = 14,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 807,
  AVATAR = "head170",
  SHIP = "ship177",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 340,
  LEVEL = 15,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 807,
  AVATAR = "head170",
  SHIP = "ship177",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 340,
  LEVEL = 16,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 807,
  AVATAR = "head170",
  SHIP = "ship177",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 341,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 813,
  AVATAR = "head171",
  SHIP = "ship178",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 341,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 813,
  AVATAR = "head171",
  SHIP = "ship178",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 341,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 813,
  AVATAR = "head171",
  SHIP = "ship178",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 341,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 813,
  AVATAR = "head171",
  SHIP = "ship178",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 341,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 813,
  AVATAR = "head171",
  SHIP = "ship178",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 341,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 813,
  AVATAR = "head171",
  SHIP = "ship178",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 341,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 813,
  AVATAR = "head171",
  SHIP = "ship178",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 341,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 813,
  AVATAR = "head171",
  SHIP = "ship178",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 341,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 813,
  AVATAR = "head171",
  SHIP = "ship178",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 341,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 813,
  AVATAR = "head171",
  SHIP = "ship178",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 341,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 813,
  AVATAR = "head171",
  SHIP = "ship178",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 341,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 813,
  AVATAR = "head171",
  SHIP = "ship178",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 341,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 813,
  AVATAR = "head171",
  SHIP = "ship178",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 341,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 813,
  AVATAR = "head171",
  SHIP = "ship178",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 341,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 813,
  AVATAR = "head171",
  SHIP = "ship178",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 341,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 813,
  AVATAR = "head171",
  SHIP = "ship178",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 341,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 813,
  AVATAR = "head171",
  SHIP = "ship178",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 342,
  LEVEL = 0,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 13804,
  AVATAR = "head172",
  SHIP = "ship179",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 342,
  LEVEL = 1,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 13804,
  AVATAR = "head172",
  SHIP = "ship179",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 342,
  LEVEL = 2,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 13804,
  AVATAR = "head172",
  SHIP = "ship179",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 342,
  LEVEL = 3,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 13804,
  AVATAR = "head172",
  SHIP = "ship179",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 342,
  LEVEL = 4,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 13804,
  AVATAR = "head172",
  SHIP = "ship179",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 342,
  LEVEL = 5,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 13804,
  AVATAR = "head172",
  SHIP = "ship179",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 342,
  LEVEL = 6,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 13804,
  AVATAR = "head172",
  SHIP = "ship179",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 342,
  LEVEL = 7,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 13804,
  AVATAR = "head172",
  SHIP = "ship179",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 342,
  LEVEL = 8,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 13804,
  AVATAR = "head172",
  SHIP = "ship179",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 342,
  LEVEL = 9,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 13804,
  AVATAR = "head172",
  SHIP = "ship179",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 342,
  LEVEL = 10,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 13804,
  AVATAR = "head172",
  SHIP = "ship179",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 342,
  LEVEL = 11,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 13804,
  AVATAR = "head172",
  SHIP = "ship179",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 342,
  LEVEL = 12,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 13804,
  AVATAR = "head172",
  SHIP = "ship179",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 342,
  LEVEL = 13,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 13804,
  AVATAR = "head172",
  SHIP = "ship179",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 342,
  LEVEL = 14,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 13804,
  AVATAR = "head172",
  SHIP = "ship179",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 342,
  LEVEL = 15,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 13804,
  AVATAR = "head172",
  SHIP = "ship179",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 342,
  LEVEL = 16,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 13804,
  AVATAR = "head172",
  SHIP = "ship179",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 343,
  LEVEL = 0,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 810,
  AVATAR = "head173",
  SHIP = "ship180",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 343,
  LEVEL = 1,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 810,
  AVATAR = "head173",
  SHIP = "ship180",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 343,
  LEVEL = 2,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 810,
  AVATAR = "head173",
  SHIP = "ship180",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 343,
  LEVEL = 3,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 810,
  AVATAR = "head173",
  SHIP = "ship180",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 343,
  LEVEL = 4,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 810,
  AVATAR = "head173",
  SHIP = "ship180",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 343,
  LEVEL = 5,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 810,
  AVATAR = "head173",
  SHIP = "ship180",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 343,
  LEVEL = 6,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 810,
  AVATAR = "head173",
  SHIP = "ship180",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 343,
  LEVEL = 7,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 810,
  AVATAR = "head173",
  SHIP = "ship180",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 343,
  LEVEL = 8,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 810,
  AVATAR = "head173",
  SHIP = "ship180",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 343,
  LEVEL = 9,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 810,
  AVATAR = "head173",
  SHIP = "ship180",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 343,
  LEVEL = 10,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 810,
  AVATAR = "head173",
  SHIP = "ship180",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 343,
  LEVEL = 11,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 810,
  AVATAR = "head173",
  SHIP = "ship180",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 343,
  LEVEL = 12,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 810,
  AVATAR = "head173",
  SHIP = "ship180",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 343,
  LEVEL = 13,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 810,
  AVATAR = "head173",
  SHIP = "ship180",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 343,
  LEVEL = 14,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 810,
  AVATAR = "head173",
  SHIP = "ship180",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 343,
  LEVEL = 15,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 810,
  AVATAR = "head173",
  SHIP = "ship180",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 343,
  LEVEL = 16,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 810,
  AVATAR = "head173",
  SHIP = "ship180",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 344,
  LEVEL = 0,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 812,
  AVATAR = "head104",
  SHIP = "ship107",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 344,
  LEVEL = 1,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 812,
  AVATAR = "head104",
  SHIP = "ship107",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 344,
  LEVEL = 2,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 812,
  AVATAR = "head104",
  SHIP = "ship107",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 344,
  LEVEL = 3,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 812,
  AVATAR = "head104",
  SHIP = "ship107",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 344,
  LEVEL = 4,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 812,
  AVATAR = "head104",
  SHIP = "ship107",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 344,
  LEVEL = 5,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 812,
  AVATAR = "head104",
  SHIP = "ship107",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 344,
  LEVEL = 6,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 812,
  AVATAR = "head104",
  SHIP = "ship107",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 344,
  LEVEL = 7,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 10812,
  AVATAR = "head104",
  SHIP = "ship107",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 344,
  LEVEL = 8,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 10812,
  AVATAR = "head104",
  SHIP = "ship107",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 344,
  LEVEL = 9,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 10812,
  AVATAR = "head104",
  SHIP = "ship107",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 344,
  LEVEL = 10,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 11812,
  AVATAR = "head104",
  SHIP = "ship107",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 344,
  LEVEL = 11,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 11812,
  AVATAR = "head104",
  SHIP = "ship107",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 344,
  LEVEL = 12,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 11812,
  AVATAR = "head104",
  SHIP = "ship107",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 344,
  LEVEL = 13,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 11812,
  AVATAR = "head104",
  SHIP = "ship107",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 344,
  LEVEL = 14,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 11812,
  AVATAR = "head104",
  SHIP = "ship107",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 344,
  LEVEL = 15,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 12812,
  AVATAR = "head104",
  SHIP = "ship107",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 344,
  LEVEL = 16,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 12812,
  AVATAR = "head104",
  SHIP = "ship107",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 345,
  LEVEL = 0,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 808,
  AVATAR = "head175",
  SHIP = "ship182",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 345,
  LEVEL = 1,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 808,
  AVATAR = "head175",
  SHIP = "ship182",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 345,
  LEVEL = 2,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 808,
  AVATAR = "head175",
  SHIP = "ship182",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 345,
  LEVEL = 3,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 808,
  AVATAR = "head175",
  SHIP = "ship182",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 345,
  LEVEL = 4,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 808,
  AVATAR = "head175",
  SHIP = "ship182",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 345,
  LEVEL = 5,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 808,
  AVATAR = "head175",
  SHIP = "ship182",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 345,
  LEVEL = 6,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 808,
  AVATAR = "head175",
  SHIP = "ship182",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 345,
  LEVEL = 7,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 808,
  AVATAR = "head175",
  SHIP = "ship182",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 345,
  LEVEL = 8,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 808,
  AVATAR = "head175",
  SHIP = "ship182",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 345,
  LEVEL = 9,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 808,
  AVATAR = "head175",
  SHIP = "ship182",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 345,
  LEVEL = 10,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 808,
  AVATAR = "head175",
  SHIP = "ship182",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 345,
  LEVEL = 11,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 808,
  AVATAR = "head175",
  SHIP = "ship182",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 345,
  LEVEL = 12,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 808,
  AVATAR = "head175",
  SHIP = "ship182",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 345,
  LEVEL = 13,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 808,
  AVATAR = "head175",
  SHIP = "ship182",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 345,
  LEVEL = 14,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 808,
  AVATAR = "head175",
  SHIP = "ship182",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 345,
  LEVEL = 15,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 808,
  AVATAR = "head175",
  SHIP = "ship182",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 345,
  LEVEL = 16,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 808,
  AVATAR = "head175",
  SHIP = "ship182",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 345,
  LEVEL = 17,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 808,
  AVATAR = "head175",
  SHIP = "ship182",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 345,
  LEVEL = 18,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 808,
  AVATAR = "head175",
  SHIP = "ship182",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 345,
  LEVEL = 19,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 808,
  AVATAR = "head175",
  SHIP = "ship182",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 345,
  LEVEL = 20,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 808,
  AVATAR = "head175",
  SHIP = "ship182",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 2001,
  LEVEL = 0,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 2001,
  AVATAR = "head117",
  SHIP = "ship120",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 2001,
  LEVEL = 1,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 2001,
  AVATAR = "head117",
  SHIP = "ship120",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 2001,
  LEVEL = 2,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 2001,
  AVATAR = "head117",
  SHIP = "ship120",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 2001,
  LEVEL = 3,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 2001,
  AVATAR = "head117",
  SHIP = "ship120",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 2001,
  LEVEL = 4,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 2001,
  AVATAR = "head117",
  SHIP = "ship120",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 2001,
  LEVEL = 5,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 2001,
  AVATAR = "head117",
  SHIP = "ship120",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 2001,
  LEVEL = 6,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 2001,
  AVATAR = "head117",
  SHIP = "ship120",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 2001,
  LEVEL = 7,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 2001,
  AVATAR = "head117",
  SHIP = "ship120",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 2001,
  LEVEL = 8,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 2001,
  AVATAR = "head117",
  SHIP = "ship120",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 2001,
  LEVEL = 9,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 2001,
  AVATAR = "head117",
  SHIP = "ship120",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 2001,
  LEVEL = 10,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 2001,
  AVATAR = "head117",
  SHIP = "ship120",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 2001,
  LEVEL = 11,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 2001,
  AVATAR = "head117",
  SHIP = "ship120",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 2001,
  LEVEL = 12,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 2001,
  AVATAR = "head117",
  SHIP = "ship120",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 2001,
  LEVEL = 13,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 2001,
  AVATAR = "head117",
  SHIP = "ship120",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 2001,
  LEVEL = 14,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 2001,
  AVATAR = "head117",
  SHIP = "ship120",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 2001,
  LEVEL = 15,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 2001,
  AVATAR = "head117",
  SHIP = "ship120",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 2001,
  LEVEL = 16,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 2001,
  AVATAR = "head117",
  SHIP = "ship120",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 346,
  LEVEL = 0,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 815,
  AVATAR = "head145",
  SHIP = "ship148",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 346,
  LEVEL = 1,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 815,
  AVATAR = "head145",
  SHIP = "ship148",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 346,
  LEVEL = 2,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 815,
  AVATAR = "head145",
  SHIP = "ship148",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 346,
  LEVEL = 3,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 815,
  AVATAR = "head145",
  SHIP = "ship148",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 346,
  LEVEL = 4,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 815,
  AVATAR = "head145",
  SHIP = "ship148",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 346,
  LEVEL = 5,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 815,
  AVATAR = "head145",
  SHIP = "ship148",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 346,
  LEVEL = 6,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 815,
  AVATAR = "head145",
  SHIP = "ship148",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 346,
  LEVEL = 7,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 10815,
  AVATAR = "head145",
  SHIP = "ship148",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 346,
  LEVEL = 8,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 10815,
  AVATAR = "head145",
  SHIP = "ship148",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 346,
  LEVEL = 9,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 10815,
  AVATAR = "head145",
  SHIP = "ship148",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 346,
  LEVEL = 10,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 11815,
  AVATAR = "head145",
  SHIP = "ship148",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 346,
  LEVEL = 11,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 11815,
  AVATAR = "head145",
  SHIP = "ship148",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 346,
  LEVEL = 12,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 11815,
  AVATAR = "head145",
  SHIP = "ship148",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 346,
  LEVEL = 13,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 11815,
  AVATAR = "head145",
  SHIP = "ship148",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 346,
  LEVEL = 14,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 11815,
  AVATAR = "head145",
  SHIP = "ship148",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 346,
  LEVEL = 15,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 11815,
  AVATAR = "head145",
  SHIP = "ship148",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 346,
  LEVEL = 16,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 11815,
  AVATAR = "head145",
  SHIP = "ship148",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 347,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 809,
  AVATAR = "head177",
  SHIP = "ship183",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 347,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 809,
  AVATAR = "head177",
  SHIP = "ship183",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 347,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 809,
  AVATAR = "head177",
  SHIP = "ship183",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 347,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 809,
  AVATAR = "head177",
  SHIP = "ship183",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 347,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 809,
  AVATAR = "head177",
  SHIP = "ship183",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 347,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 809,
  AVATAR = "head177",
  SHIP = "ship183",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 347,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 809,
  AVATAR = "head177",
  SHIP = "ship183",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 347,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 809,
  AVATAR = "head177",
  SHIP = "ship183",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 347,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 809,
  AVATAR = "head177",
  SHIP = "ship183",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 347,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 809,
  AVATAR = "head177",
  SHIP = "ship183",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 347,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 809,
  AVATAR = "head177",
  SHIP = "ship183",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 347,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 809,
  AVATAR = "head177",
  SHIP = "ship183",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 347,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 809,
  AVATAR = "head177",
  SHIP = "ship183",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 347,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 809,
  AVATAR = "head177",
  SHIP = "ship183",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 347,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 809,
  AVATAR = "head177",
  SHIP = "ship183",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 347,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 809,
  AVATAR = "head177",
  SHIP = "ship183",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 347,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 809,
  AVATAR = "head177",
  SHIP = "ship183",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 348,
  LEVEL = 0,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 802,
  AVATAR = "head178",
  SHIP = "ship185",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 348,
  LEVEL = 1,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 802,
  AVATAR = "head178",
  SHIP = "ship185",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 348,
  LEVEL = 2,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 802,
  AVATAR = "head178",
  SHIP = "ship185",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 348,
  LEVEL = 3,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 802,
  AVATAR = "head178",
  SHIP = "ship185",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 348,
  LEVEL = 4,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 802,
  AVATAR = "head178",
  SHIP = "ship185",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 348,
  LEVEL = 5,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 802,
  AVATAR = "head178",
  SHIP = "ship185",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 348,
  LEVEL = 6,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 802,
  AVATAR = "head178",
  SHIP = "ship185",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 348,
  LEVEL = 7,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 802,
  AVATAR = "head178",
  SHIP = "ship185",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 348,
  LEVEL = 8,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 802,
  AVATAR = "head178",
  SHIP = "ship185",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 348,
  LEVEL = 9,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 802,
  AVATAR = "head178",
  SHIP = "ship185",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 348,
  LEVEL = 10,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 802,
  AVATAR = "head178",
  SHIP = "ship185",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 348,
  LEVEL = 11,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 802,
  AVATAR = "head178",
  SHIP = "ship185",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 348,
  LEVEL = 12,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 802,
  AVATAR = "head178",
  SHIP = "ship185",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 348,
  LEVEL = 13,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 802,
  AVATAR = "head178",
  SHIP = "ship185",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 348,
  LEVEL = 14,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 802,
  AVATAR = "head178",
  SHIP = "ship185",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 348,
  LEVEL = 15,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 802,
  AVATAR = "head178",
  SHIP = "ship185",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 348,
  LEVEL = 16,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 802,
  AVATAR = "head178",
  SHIP = "ship185",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 349,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 818,
  AVATAR = "head179",
  SHIP = "ship186",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 349,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 818,
  AVATAR = "head179",
  SHIP = "ship186",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 349,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 818,
  AVATAR = "head179",
  SHIP = "ship186",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 349,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 10818,
  AVATAR = "head179",
  SHIP = "ship186",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 349,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 10818,
  AVATAR = "head179",
  SHIP = "ship186",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 349,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 10818,
  AVATAR = "head179",
  SHIP = "ship186",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 349,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 10818,
  AVATAR = "head179",
  SHIP = "ship186",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 349,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 11818,
  AVATAR = "head179",
  SHIP = "ship186",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 349,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 11818,
  AVATAR = "head179",
  SHIP = "ship186",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 349,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 11818,
  AVATAR = "head179",
  SHIP = "ship186",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 349,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 11818,
  AVATAR = "head179",
  SHIP = "ship186",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 349,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 11818,
  AVATAR = "head179",
  SHIP = "ship186",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 349,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 12818,
  AVATAR = "head179",
  SHIP = "ship186",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 349,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 12818,
  AVATAR = "head179",
  SHIP = "ship186",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 349,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 12818,
  AVATAR = "head179",
  SHIP = "ship186",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 349,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 12818,
  AVATAR = "head179",
  SHIP = "ship186",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 349,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 12818,
  AVATAR = "head179",
  SHIP = "ship186",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 350,
  LEVEL = 0,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 817,
  AVATAR = "head180",
  SHIP = "ship187",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 350,
  LEVEL = 1,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 817,
  AVATAR = "head180",
  SHIP = "ship187",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 350,
  LEVEL = 2,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 817,
  AVATAR = "head180",
  SHIP = "ship187",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 350,
  LEVEL = 3,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 817,
  AVATAR = "head180",
  SHIP = "ship187",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 350,
  LEVEL = 4,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 817,
  AVATAR = "head180",
  SHIP = "ship187",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 350,
  LEVEL = 5,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 817,
  AVATAR = "head180",
  SHIP = "ship187",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 350,
  LEVEL = 6,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 817,
  AVATAR = "head180",
  SHIP = "ship187",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 350,
  LEVEL = 7,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 817,
  AVATAR = "head180",
  SHIP = "ship187",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 350,
  LEVEL = 8,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 817,
  AVATAR = "head180",
  SHIP = "ship187",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 350,
  LEVEL = 9,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 817,
  AVATAR = "head180",
  SHIP = "ship187",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 350,
  LEVEL = 10,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 817,
  AVATAR = "head180",
  SHIP = "ship187",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 350,
  LEVEL = 11,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 817,
  AVATAR = "head180",
  SHIP = "ship187",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 350,
  LEVEL = 12,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 817,
  AVATAR = "head180",
  SHIP = "ship187",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 350,
  LEVEL = 13,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 817,
  AVATAR = "head180",
  SHIP = "ship187",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 350,
  LEVEL = 14,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 817,
  AVATAR = "head180",
  SHIP = "ship187",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 350,
  LEVEL = 15,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 817,
  AVATAR = "head180",
  SHIP = "ship187",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 350,
  LEVEL = 16,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 817,
  AVATAR = "head180",
  SHIP = "ship187",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 351,
  LEVEL = 0,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 816,
  AVATAR = "head181",
  SHIP = "ship188",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 351,
  LEVEL = 1,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 816,
  AVATAR = "head181",
  SHIP = "ship188",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 351,
  LEVEL = 2,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 816,
  AVATAR = "head181",
  SHIP = "ship188",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 351,
  LEVEL = 3,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 816,
  AVATAR = "head181",
  SHIP = "ship188",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 351,
  LEVEL = 4,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 816,
  AVATAR = "head181",
  SHIP = "ship188",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 351,
  LEVEL = 5,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 816,
  AVATAR = "head181",
  SHIP = "ship188",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 351,
  LEVEL = 6,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 816,
  AVATAR = "head181",
  SHIP = "ship188",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 351,
  LEVEL = 7,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 816,
  AVATAR = "head181",
  SHIP = "ship188",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 351,
  LEVEL = 8,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 816,
  AVATAR = "head182",
  SHIP = "ship188",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 351,
  LEVEL = 9,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 816,
  AVATAR = "head182",
  SHIP = "ship188",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 351,
  LEVEL = 10,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 816,
  AVATAR = "head182",
  SHIP = "ship188",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 351,
  LEVEL = 11,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 816,
  AVATAR = "head182",
  SHIP = "ship188",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 351,
  LEVEL = 12,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 816,
  AVATAR = "head182",
  SHIP = "ship188",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 351,
  LEVEL = 13,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 816,
  AVATAR = "head182",
  SHIP = "ship188",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 351,
  LEVEL = 14,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 816,
  AVATAR = "head182",
  SHIP = "ship188",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 351,
  LEVEL = 15,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 816,
  AVATAR = "head182",
  SHIP = "ship188",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 351,
  LEVEL = 16,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 816,
  AVATAR = "head182",
  SHIP = "ship188",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 352,
  LEVEL = 0,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 816,
  AVATAR = "head183",
  SHIP = "ship189",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 352,
  LEVEL = 1,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 816,
  AVATAR = "head183",
  SHIP = "ship189",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 352,
  LEVEL = 2,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 816,
  AVATAR = "head183",
  SHIP = "ship189",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 352,
  LEVEL = 3,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 816,
  AVATAR = "head183",
  SHIP = "ship189",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 352,
  LEVEL = 4,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 816,
  AVATAR = "head183",
  SHIP = "ship189",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 352,
  LEVEL = 5,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 816,
  AVATAR = "head183",
  SHIP = "ship189",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 352,
  LEVEL = 6,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 816,
  AVATAR = "head183",
  SHIP = "ship189",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 352,
  LEVEL = 7,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 816,
  AVATAR = "head183",
  SHIP = "ship189",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 352,
  LEVEL = 8,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 816,
  AVATAR = "head184",
  SHIP = "ship189",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 352,
  LEVEL = 9,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 816,
  AVATAR = "head184",
  SHIP = "ship189",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 352,
  LEVEL = 10,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 816,
  AVATAR = "head184",
  SHIP = "ship189",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 352,
  LEVEL = 11,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 816,
  AVATAR = "head184",
  SHIP = "ship189",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 352,
  LEVEL = 12,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 816,
  AVATAR = "head184",
  SHIP = "ship189",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 352,
  LEVEL = 13,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 816,
  AVATAR = "head184",
  SHIP = "ship189",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 352,
  LEVEL = 14,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 816,
  AVATAR = "head184",
  SHIP = "ship189",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 352,
  LEVEL = 15,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 816,
  AVATAR = "head184",
  SHIP = "ship189",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 352,
  LEVEL = 16,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 816,
  AVATAR = "head184",
  SHIP = "ship189",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 353,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head185",
  SHIP = "ship190",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 353,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head185",
  SHIP = "ship190",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 353,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head185",
  SHIP = "ship190",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 353,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head185",
  SHIP = "ship190",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 353,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head185",
  SHIP = "ship190",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 353,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head185",
  SHIP = "ship190",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 353,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head185",
  SHIP = "ship190",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 353,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head185",
  SHIP = "ship190",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 353,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head185",
  SHIP = "ship190",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 353,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head185",
  SHIP = "ship190",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 353,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head185",
  SHIP = "ship190",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 353,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head185",
  SHIP = "ship190",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 353,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head185",
  SHIP = "ship190",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 353,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head185",
  SHIP = "ship190",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 353,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head185",
  SHIP = "ship190",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 353,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head185",
  SHIP = "ship190",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 353,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head185",
  SHIP = "ship190",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 354,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head186",
  SHIP = "ship191",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 354,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head186",
  SHIP = "ship191",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 354,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head186",
  SHIP = "ship191",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 354,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head186",
  SHIP = "ship191",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 354,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head186",
  SHIP = "ship191",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 354,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head186",
  SHIP = "ship191",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 354,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head186",
  SHIP = "ship191",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 354,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head186",
  SHIP = "ship191",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 354,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head186",
  SHIP = "ship191",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 354,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head186",
  SHIP = "ship191",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 354,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head186",
  SHIP = "ship191",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 354,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head186",
  SHIP = "ship191",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 354,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head186",
  SHIP = "ship191",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 354,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head186",
  SHIP = "ship191",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 354,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head186",
  SHIP = "ship191",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 354,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head186",
  SHIP = "ship191",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 354,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head186",
  SHIP = "ship191",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 355,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head187",
  SHIP = "ship192",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 355,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head187",
  SHIP = "ship192",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 355,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head187",
  SHIP = "ship192",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 355,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head187",
  SHIP = "ship192",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 355,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head187",
  SHIP = "ship192",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 355,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head187",
  SHIP = "ship192",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 355,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head187",
  SHIP = "ship192",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 355,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head187",
  SHIP = "ship192",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 355,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head187",
  SHIP = "ship192",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 355,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head187",
  SHIP = "ship192",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 355,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head187",
  SHIP = "ship192",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 355,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head187",
  SHIP = "ship192",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 355,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head187",
  SHIP = "ship192",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 355,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head187",
  SHIP = "ship192",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 355,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head187",
  SHIP = "ship192",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 355,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head187",
  SHIP = "ship192",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 355,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head187",
  SHIP = "ship192",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 355,
  LEVEL = 17,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 836,
  AVATAR = "head187",
  SHIP = "ship192",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 355,
  LEVEL = 18,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 836,
  AVATAR = "head187",
  SHIP = "ship192",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 355,
  LEVEL = 19,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 836,
  AVATAR = "head187",
  SHIP = "ship192",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 355,
  LEVEL = 20,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 836,
  AVATAR = "head187",
  SHIP = "ship192",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 356,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head188",
  SHIP = "ship193",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 356,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head188",
  SHIP = "ship193",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 356,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head188",
  SHIP = "ship193",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 356,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head188",
  SHIP = "ship193",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 356,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head188",
  SHIP = "ship193",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 356,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head188",
  SHIP = "ship193",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 356,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head188",
  SHIP = "ship193",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 356,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head188",
  SHIP = "ship193",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 356,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head188",
  SHIP = "ship193",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 356,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head188",
  SHIP = "ship193",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 356,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head188",
  SHIP = "ship193",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 356,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head188",
  SHIP = "ship193",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 356,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head188",
  SHIP = "ship193",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 356,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head188",
  SHIP = "ship193",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 356,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head188",
  SHIP = "ship193",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 356,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head188",
  SHIP = "ship193",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 356,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head188",
  SHIP = "ship193",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 357,
  LEVEL = 0,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 827,
  AVATAR = "head189",
  SHIP = "ship194",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 357,
  LEVEL = 1,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 827,
  AVATAR = "head189",
  SHIP = "ship194",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 357,
  LEVEL = 2,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 827,
  AVATAR = "head189",
  SHIP = "ship194",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 357,
  LEVEL = 3,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 827,
  AVATAR = "head189",
  SHIP = "ship194",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 357,
  LEVEL = 4,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 827,
  AVATAR = "head189",
  SHIP = "ship194",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 357,
  LEVEL = 5,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 827,
  AVATAR = "head189",
  SHIP = "ship194",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 357,
  LEVEL = 6,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 827,
  AVATAR = "head189",
  SHIP = "ship194",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 357,
  LEVEL = 7,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 827,
  AVATAR = "head189",
  SHIP = "ship194",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 357,
  LEVEL = 8,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 827,
  AVATAR = "head189",
  SHIP = "ship194",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 357,
  LEVEL = 9,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 827,
  AVATAR = "head189",
  SHIP = "ship194",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 357,
  LEVEL = 10,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 827,
  AVATAR = "head189",
  SHIP = "ship194",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 357,
  LEVEL = 11,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 827,
  AVATAR = "head189",
  SHIP = "ship194",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 357,
  LEVEL = 12,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 827,
  AVATAR = "head189",
  SHIP = "ship194",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 357,
  LEVEL = 13,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 827,
  AVATAR = "head189",
  SHIP = "ship194",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 357,
  LEVEL = 14,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 827,
  AVATAR = "head189",
  SHIP = "ship194",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 357,
  LEVEL = 15,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 827,
  AVATAR = "head189",
  SHIP = "ship194",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 357,
  LEVEL = 16,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 827,
  AVATAR = "head189",
  SHIP = "ship194",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 358,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head190",
  SHIP = "ship195",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 358,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head190",
  SHIP = "ship195",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 358,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head190",
  SHIP = "ship195",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 358,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head190",
  SHIP = "ship195",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 358,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head190",
  SHIP = "ship195",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 358,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head190",
  SHIP = "ship195",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 358,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head190",
  SHIP = "ship195",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 358,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head190",
  SHIP = "ship195",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 358,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head190",
  SHIP = "ship195",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 358,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head190",
  SHIP = "ship195",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 358,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head190",
  SHIP = "ship195",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 358,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head190",
  SHIP = "ship195",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 358,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head190",
  SHIP = "ship195",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 358,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head190",
  SHIP = "ship195",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 358,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head190",
  SHIP = "ship195",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 358,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head190",
  SHIP = "ship195",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 358,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head190",
  SHIP = "ship195",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 359,
  LEVEL = 0,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 822,
  AVATAR = "head191",
  SHIP = "ship196",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 359,
  LEVEL = 1,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 822,
  AVATAR = "head191",
  SHIP = "ship196",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 359,
  LEVEL = 2,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 822,
  AVATAR = "head191",
  SHIP = "ship196",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 359,
  LEVEL = 3,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 822,
  AVATAR = "head191",
  SHIP = "ship196",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 359,
  LEVEL = 4,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 822,
  AVATAR = "head191",
  SHIP = "ship196",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 359,
  LEVEL = 5,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 10822,
  AVATAR = "head191",
  SHIP = "ship196",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 359,
  LEVEL = 6,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 10822,
  AVATAR = "head191",
  SHIP = "ship196",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 359,
  LEVEL = 7,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 11822,
  AVATAR = "head191",
  SHIP = "ship196",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 359,
  LEVEL = 8,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 11822,
  AVATAR = "head191",
  SHIP = "ship196",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 359,
  LEVEL = 9,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 11822,
  AVATAR = "head191",
  SHIP = "ship196",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 359,
  LEVEL = 10,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 11822,
  AVATAR = "head191",
  SHIP = "ship196",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 359,
  LEVEL = 11,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 11822,
  AVATAR = "head191",
  SHIP = "ship196",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 359,
  LEVEL = 12,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 12822,
  AVATAR = "head191",
  SHIP = "ship196",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 359,
  LEVEL = 13,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 12822,
  AVATAR = "head191",
  SHIP = "ship196",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 359,
  LEVEL = 14,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 12822,
  AVATAR = "head191",
  SHIP = "ship196",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 359,
  LEVEL = 15,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 13822,
  AVATAR = "head191",
  SHIP = "ship196",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 359,
  LEVEL = 16,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 13822,
  AVATAR = "head191",
  SHIP = "ship196",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 360,
  LEVEL = 0,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 825,
  AVATAR = "head192",
  SHIP = "ship197",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 360,
  LEVEL = 1,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 825,
  AVATAR = "head192",
  SHIP = "ship197",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 360,
  LEVEL = 2,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 825,
  AVATAR = "head192",
  SHIP = "ship197",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 360,
  LEVEL = 3,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 825,
  AVATAR = "head192",
  SHIP = "ship197",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 360,
  LEVEL = 4,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 825,
  AVATAR = "head192",
  SHIP = "ship197",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 360,
  LEVEL = 5,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 825,
  AVATAR = "head192",
  SHIP = "ship197",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 360,
  LEVEL = 6,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 825,
  AVATAR = "head192",
  SHIP = "ship197",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 360,
  LEVEL = 7,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 825,
  AVATAR = "head192",
  SHIP = "ship197",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 360,
  LEVEL = 8,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 825,
  AVATAR = "head192",
  SHIP = "ship197",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 360,
  LEVEL = 9,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 825,
  AVATAR = "head192",
  SHIP = "ship197",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 360,
  LEVEL = 10,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 825,
  AVATAR = "head192",
  SHIP = "ship197",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 360,
  LEVEL = 11,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 825,
  AVATAR = "head192",
  SHIP = "ship197",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 360,
  LEVEL = 12,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 825,
  AVATAR = "head192",
  SHIP = "ship197",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 360,
  LEVEL = 13,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 825,
  AVATAR = "head192",
  SHIP = "ship197",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 360,
  LEVEL = 14,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 825,
  AVATAR = "head192",
  SHIP = "ship197",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 360,
  LEVEL = 15,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 825,
  AVATAR = "head192",
  SHIP = "ship197",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 360,
  LEVEL = 16,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 825,
  AVATAR = "head192",
  SHIP = "ship197",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 361,
  LEVEL = 0,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head193",
  SHIP = "ship198",
  sex = 1,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 361,
  LEVEL = 1,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head193",
  SHIP = "ship198",
  sex = 1,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 361,
  LEVEL = 2,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head193",
  SHIP = "ship198",
  sex = 1,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 361,
  LEVEL = 3,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head193",
  SHIP = "ship198",
  sex = 1,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 361,
  LEVEL = 4,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head193",
  SHIP = "ship198",
  sex = 1,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 361,
  LEVEL = 5,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head193",
  SHIP = "ship198",
  sex = 1,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 361,
  LEVEL = 6,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head193",
  SHIP = "ship198",
  sex = 1,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 361,
  LEVEL = 7,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head193",
  SHIP = "ship198",
  sex = 1,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 361,
  LEVEL = 8,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head194",
  SHIP = "ship198",
  sex = 1,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 361,
  LEVEL = 9,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head194",
  SHIP = "ship198",
  sex = 1,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 361,
  LEVEL = 10,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head194",
  SHIP = "ship198",
  sex = 1,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 361,
  LEVEL = 11,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head194",
  SHIP = "ship198",
  sex = 1,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 361,
  LEVEL = 12,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head194",
  SHIP = "ship198",
  sex = 1,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 361,
  LEVEL = 13,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head194",
  SHIP = "ship198",
  sex = 1,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 361,
  LEVEL = 14,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head194",
  SHIP = "ship198",
  sex = 1,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 361,
  LEVEL = 15,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head194",
  SHIP = "ship198",
  sex = 1,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 361,
  LEVEL = 16,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head194",
  SHIP = "ship198",
  sex = 1,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 362,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head195",
  SHIP = "ship199",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 362,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head195",
  SHIP = "ship199",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 362,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head195",
  SHIP = "ship199",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 362,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head195",
  SHIP = "ship199",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 362,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head195",
  SHIP = "ship199",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 362,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head195",
  SHIP = "ship199",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 362,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head195",
  SHIP = "ship199",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 362,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head195",
  SHIP = "ship199",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 362,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head195",
  SHIP = "ship199",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 362,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head195",
  SHIP = "ship199",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 362,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head195",
  SHIP = "ship199",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 362,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head195",
  SHIP = "ship199",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 362,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head195",
  SHIP = "ship199",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 362,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head195",
  SHIP = "ship199",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 362,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head195",
  SHIP = "ship199",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 362,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head195",
  SHIP = "ship199",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 362,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head195",
  SHIP = "ship199",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 362,
  LEVEL = 17,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 831,
  AVATAR = "head195",
  SHIP = "ship199",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 362,
  LEVEL = 18,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 831,
  AVATAR = "head195",
  SHIP = "ship199",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 362,
  LEVEL = 19,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 831,
  AVATAR = "head195",
  SHIP = "ship199",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 362,
  LEVEL = 20,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 831,
  AVATAR = "head195",
  SHIP = "ship199",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 2002,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 2002,
  AVATAR = "head6",
  SHIP = "ship2",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 2002,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 2002,
  AVATAR = "head6",
  SHIP = "ship2",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 2002,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 2002,
  AVATAR = "head6",
  SHIP = "ship2",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 2002,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 2002,
  AVATAR = "head6",
  SHIP = "ship2",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 2002,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 2002,
  AVATAR = "head6",
  SHIP = "ship2",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 2002,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 2002,
  AVATAR = "head6",
  SHIP = "ship2",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 2002,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 2002,
  AVATAR = "head6",
  SHIP = "ship2",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 2002,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 2002,
  AVATAR = "head6",
  SHIP = "ship2",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 2002,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 2002,
  AVATAR = "head6",
  SHIP = "ship2",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 2002,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 2002,
  AVATAR = "head6",
  SHIP = "ship2",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 2002,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 2002,
  AVATAR = "head6",
  SHIP = "ship2",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 2002,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 2002,
  AVATAR = "head6",
  SHIP = "ship2",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 2002,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 2002,
  AVATAR = "head6",
  SHIP = "ship2",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 2002,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 2002,
  AVATAR = "head6",
  SHIP = "ship2",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 2002,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 2002,
  AVATAR = "head6",
  SHIP = "ship2",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 2002,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 2002,
  AVATAR = "head6",
  SHIP = "ship2",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 2002,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 2002,
  AVATAR = "head6",
  SHIP = "ship2",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 363,
  LEVEL = 0,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 826,
  AVATAR = "head196",
  SHIP = "ship200",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 363,
  LEVEL = 1,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 826,
  AVATAR = "head196",
  SHIP = "ship200",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 363,
  LEVEL = 2,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 826,
  AVATAR = "head196",
  SHIP = "ship200",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 363,
  LEVEL = 3,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 826,
  AVATAR = "head196",
  SHIP = "ship200",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 363,
  LEVEL = 4,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 826,
  AVATAR = "head196",
  SHIP = "ship200",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 363,
  LEVEL = 5,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 826,
  AVATAR = "head196",
  SHIP = "ship200",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 363,
  LEVEL = 6,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 826,
  AVATAR = "head196",
  SHIP = "ship200",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 363,
  LEVEL = 7,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 826,
  AVATAR = "head196",
  SHIP = "ship200",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 363,
  LEVEL = 8,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 826,
  AVATAR = "head196",
  SHIP = "ship200",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 363,
  LEVEL = 9,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 826,
  AVATAR = "head196",
  SHIP = "ship200",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 363,
  LEVEL = 10,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 826,
  AVATAR = "head196",
  SHIP = "ship200",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 363,
  LEVEL = 11,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 826,
  AVATAR = "head196",
  SHIP = "ship200",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 363,
  LEVEL = 12,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 826,
  AVATAR = "head196",
  SHIP = "ship200",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 363,
  LEVEL = 13,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 826,
  AVATAR = "head196",
  SHIP = "ship200",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 363,
  LEVEL = 14,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 826,
  AVATAR = "head196",
  SHIP = "ship200",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 363,
  LEVEL = 15,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 826,
  AVATAR = "head196",
  SHIP = "ship200",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 363,
  LEVEL = 16,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 826,
  AVATAR = "head196",
  SHIP = "ship200",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 364,
  LEVEL = 0,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head197",
  SHIP = "ship201",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 364,
  LEVEL = 1,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head197",
  SHIP = "ship201",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 364,
  LEVEL = 2,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head197",
  SHIP = "ship201",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 364,
  LEVEL = 3,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head197",
  SHIP = "ship201",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 364,
  LEVEL = 4,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head197",
  SHIP = "ship201",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 364,
  LEVEL = 5,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head197",
  SHIP = "ship201",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 364,
  LEVEL = 6,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head198",
  SHIP = "ship201",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 364,
  LEVEL = 7,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head198",
  SHIP = "ship201",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 364,
  LEVEL = 8,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head198",
  SHIP = "ship201",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 364,
  LEVEL = 9,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head198",
  SHIP = "ship201",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 364,
  LEVEL = 10,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head198",
  SHIP = "ship201",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 364,
  LEVEL = 11,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head198",
  SHIP = "ship201",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 364,
  LEVEL = 12,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head199",
  SHIP = "ship201",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 364,
  LEVEL = 13,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head199",
  SHIP = "ship201",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 364,
  LEVEL = 14,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head199",
  SHIP = "ship201",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 364,
  LEVEL = 15,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head199",
  SHIP = "ship201",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 364,
  LEVEL = 16,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head199",
  SHIP = "ship201",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 365,
  LEVEL = 0,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head200",
  SHIP = "ship202",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 365,
  LEVEL = 1,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head200",
  SHIP = "ship202",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 365,
  LEVEL = 2,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head200",
  SHIP = "ship202",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 365,
  LEVEL = 3,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head200",
  SHIP = "ship202",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 365,
  LEVEL = 4,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head200",
  SHIP = "ship202",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 365,
  LEVEL = 5,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head200",
  SHIP = "ship202",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 365,
  LEVEL = 6,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head201",
  SHIP = "ship202",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 365,
  LEVEL = 7,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head201",
  SHIP = "ship202",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 365,
  LEVEL = 8,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head201",
  SHIP = "ship202",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 365,
  LEVEL = 9,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head201",
  SHIP = "ship202",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 365,
  LEVEL = 10,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head201",
  SHIP = "ship202",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 365,
  LEVEL = 11,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head201",
  SHIP = "ship202",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 365,
  LEVEL = 12,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head202",
  SHIP = "ship202",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 365,
  LEVEL = 13,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head202",
  SHIP = "ship202",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 365,
  LEVEL = 14,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head202",
  SHIP = "ship202",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 365,
  LEVEL = 15,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head202",
  SHIP = "ship202",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 365,
  LEVEL = 16,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head202",
  SHIP = "ship202",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 366,
  LEVEL = 0,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 834,
  AVATAR = "head203",
  SHIP = "ship203",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 366,
  LEVEL = 1,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 834,
  AVATAR = "head203",
  SHIP = "ship203",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 366,
  LEVEL = 2,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 834,
  AVATAR = "head203",
  SHIP = "ship203",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 366,
  LEVEL = 3,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 834,
  AVATAR = "head203",
  SHIP = "ship203",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 366,
  LEVEL = 4,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 834,
  AVATAR = "head203",
  SHIP = "ship203",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 366,
  LEVEL = 5,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 834,
  AVATAR = "head203",
  SHIP = "ship203",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 366,
  LEVEL = 6,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 834,
  AVATAR = "head204",
  SHIP = "ship203",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 366,
  LEVEL = 7,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 834,
  AVATAR = "head204",
  SHIP = "ship203",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 366,
  LEVEL = 8,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 834,
  AVATAR = "head204",
  SHIP = "ship203",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 366,
  LEVEL = 9,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 834,
  AVATAR = "head204",
  SHIP = "ship203",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 366,
  LEVEL = 10,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 834,
  AVATAR = "head204",
  SHIP = "ship203",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 366,
  LEVEL = 11,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 834,
  AVATAR = "head204",
  SHIP = "ship203",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 366,
  LEVEL = 12,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 834,
  AVATAR = "head205",
  SHIP = "ship203",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 366,
  LEVEL = 13,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 834,
  AVATAR = "head205",
  SHIP = "ship203",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 366,
  LEVEL = 14,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 834,
  AVATAR = "head205",
  SHIP = "ship203",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 366,
  LEVEL = 15,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 834,
  AVATAR = "head205",
  SHIP = "ship203",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 366,
  LEVEL = 16,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 834,
  AVATAR = "head205",
  SHIP = "ship203",
  sex = 1,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 367,
  LEVEL = 1,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head206",
  SHIP = "ship204",
  sex = 1,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 367,
  LEVEL = 2,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head206",
  SHIP = "ship204",
  sex = 1,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 367,
  LEVEL = 3,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head206",
  SHIP = "ship204",
  sex = 1,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 367,
  LEVEL = 4,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head206",
  SHIP = "ship204",
  sex = 1,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 367,
  LEVEL = 5,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head206",
  SHIP = "ship204",
  sex = 1,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 367,
  LEVEL = 6,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head206",
  SHIP = "ship204",
  sex = 1,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 367,
  LEVEL = 7,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head206",
  SHIP = "ship204",
  sex = 1,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 367,
  LEVEL = 8,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head206",
  SHIP = "ship204",
  sex = 1,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 367,
  LEVEL = 9,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head207",
  SHIP = "ship204",
  sex = 1,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 367,
  LEVEL = 10,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head207",
  SHIP = "ship204",
  sex = 1,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 367,
  LEVEL = 11,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head207",
  SHIP = "ship204",
  sex = 1,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 367,
  LEVEL = 12,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head207",
  SHIP = "ship204",
  sex = 1,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 367,
  LEVEL = 13,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head207",
  SHIP = "ship204",
  sex = 1,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 367,
  LEVEL = 14,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head207",
  SHIP = "ship204",
  sex = 1,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 367,
  LEVEL = 15,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head207",
  SHIP = "ship204",
  sex = 1,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 367,
  LEVEL = 16,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head207",
  SHIP = "ship204",
  sex = 1,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 368,
  LEVEL = 0,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 1,
  AVATAR = "head208",
  SHIP = "ship205",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 368,
  LEVEL = 1,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 1,
  AVATAR = "head208",
  SHIP = "ship205",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 368,
  LEVEL = 2,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 1,
  AVATAR = "head208",
  SHIP = "ship205",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 368,
  LEVEL = 3,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 1,
  AVATAR = "head208",
  SHIP = "ship205",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 368,
  LEVEL = 4,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 1,
  AVATAR = "head208",
  SHIP = "ship205",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 368,
  LEVEL = 5,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 1,
  AVATAR = "head208",
  SHIP = "ship205",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 368,
  LEVEL = 6,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 1,
  AVATAR = "head208",
  SHIP = "ship205",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 368,
  LEVEL = 7,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 1,
  AVATAR = "head208",
  SHIP = "ship205",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 368,
  LEVEL = 8,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 1,
  AVATAR = "head208",
  SHIP = "ship205",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 368,
  LEVEL = 9,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 1,
  AVATAR = "head208",
  SHIP = "ship205",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 368,
  LEVEL = 10,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 1,
  AVATAR = "head208",
  SHIP = "ship205",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 368,
  LEVEL = 11,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 1,
  AVATAR = "head208",
  SHIP = "ship205",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 368,
  LEVEL = 12,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 1,
  AVATAR = "head208",
  SHIP = "ship205",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 368,
  LEVEL = 13,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 1,
  AVATAR = "head208",
  SHIP = "ship205",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 368,
  LEVEL = 14,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 1,
  AVATAR = "head208",
  SHIP = "ship205",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 368,
  LEVEL = 15,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 1,
  AVATAR = "head208",
  SHIP = "ship205",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 368,
  LEVEL = 16,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 1,
  AVATAR = "head208",
  SHIP = "ship205",
  sex = 1,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 369,
  LEVEL = 0,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head209",
  SHIP = "ship206",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 369,
  LEVEL = 1,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head209",
  SHIP = "ship206",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 369,
  LEVEL = 2,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head209",
  SHIP = "ship206",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 369,
  LEVEL = 3,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head209",
  SHIP = "ship206",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 369,
  LEVEL = 4,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head209",
  SHIP = "ship206",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 369,
  LEVEL = 5,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head209",
  SHIP = "ship206",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 369,
  LEVEL = 6,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head209",
  SHIP = "ship206",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 369,
  LEVEL = 7,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head209",
  SHIP = "ship206",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 369,
  LEVEL = 8,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head209",
  SHIP = "ship206",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 369,
  LEVEL = 9,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head209",
  SHIP = "ship206",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 369,
  LEVEL = 10,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head209",
  SHIP = "ship206",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 369,
  LEVEL = 11,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head209",
  SHIP = "ship206",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 369,
  LEVEL = 12,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head209",
  SHIP = "ship206",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 369,
  LEVEL = 13,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head209",
  SHIP = "ship206",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 369,
  LEVEL = 14,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head209",
  SHIP = "ship206",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 369,
  LEVEL = 15,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head209",
  SHIP = "ship206",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 369,
  LEVEL = 16,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head209",
  SHIP = "ship206",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 370,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 839,
  AVATAR = "head210",
  SHIP = "ship207",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 370,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 839,
  AVATAR = "head210",
  SHIP = "ship207",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 370,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 839,
  AVATAR = "head210",
  SHIP = "ship207",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 370,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 839,
  AVATAR = "head210",
  SHIP = "ship207",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 370,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 839,
  AVATAR = "head210",
  SHIP = "ship207",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 370,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 839,
  AVATAR = "head210",
  SHIP = "ship207",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 370,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 839,
  AVATAR = "head210",
  SHIP = "ship207",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 370,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 839,
  AVATAR = "head210",
  SHIP = "ship207",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 370,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 839,
  AVATAR = "head210",
  SHIP = "ship207",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 370,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 839,
  AVATAR = "head210",
  SHIP = "ship207",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 370,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 839,
  AVATAR = "head210",
  SHIP = "ship207",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 370,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 839,
  AVATAR = "head210",
  SHIP = "ship207",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 370,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 839,
  AVATAR = "head210",
  SHIP = "ship207",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 370,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 839,
  AVATAR = "head210",
  SHIP = "ship207",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 370,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 839,
  AVATAR = "head210",
  SHIP = "ship207",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 370,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 839,
  AVATAR = "head210",
  SHIP = "ship207",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 370,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 839,
  AVATAR = "head210",
  SHIP = "ship207",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 371,
  LEVEL = 0,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 838,
  AVATAR = "head211",
  SHIP = "ship208",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 371,
  LEVEL = 1,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 838,
  AVATAR = "head211",
  SHIP = "ship208",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 371,
  LEVEL = 2,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 838,
  AVATAR = "head211",
  SHIP = "ship208",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 371,
  LEVEL = 3,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 838,
  AVATAR = "head211",
  SHIP = "ship208",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 371,
  LEVEL = 4,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 838,
  AVATAR = "head211",
  SHIP = "ship208",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 371,
  LEVEL = 5,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 838,
  AVATAR = "head211",
  SHIP = "ship208",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 371,
  LEVEL = 6,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 838,
  AVATAR = "head211",
  SHIP = "ship208",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 371,
  LEVEL = 7,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 838,
  AVATAR = "head211",
  SHIP = "ship208",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 371,
  LEVEL = 8,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 838,
  AVATAR = "head211",
  SHIP = "ship208",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 371,
  LEVEL = 9,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 838,
  AVATAR = "head211",
  SHIP = "ship208",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 371,
  LEVEL = 10,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 838,
  AVATAR = "head211",
  SHIP = "ship208",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 371,
  LEVEL = 11,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 838,
  AVATAR = "head211",
  SHIP = "ship208",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 371,
  LEVEL = 12,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 838,
  AVATAR = "head211",
  SHIP = "ship208",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 371,
  LEVEL = 13,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 838,
  AVATAR = "head211",
  SHIP = "ship208",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 371,
  LEVEL = 14,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 838,
  AVATAR = "head211",
  SHIP = "ship208",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 371,
  LEVEL = 15,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 838,
  AVATAR = "head211",
  SHIP = "ship208",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 371,
  LEVEL = 16,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 838,
  AVATAR = "head211",
  SHIP = "ship208",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 372,
  LEVEL = 0,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 842,
  AVATAR = "head212",
  SHIP = "ship209",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 372,
  LEVEL = 1,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 842,
  AVATAR = "head212",
  SHIP = "ship209",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 372,
  LEVEL = 2,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 842,
  AVATAR = "head212",
  SHIP = "ship209",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 372,
  LEVEL = 3,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 842,
  AVATAR = "head212",
  SHIP = "ship209",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 372,
  LEVEL = 4,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 842,
  AVATAR = "head212",
  SHIP = "ship209",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 372,
  LEVEL = 5,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 842,
  AVATAR = "head212",
  SHIP = "ship209",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 372,
  LEVEL = 6,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 842,
  AVATAR = "head212",
  SHIP = "ship209",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 372,
  LEVEL = 7,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 842,
  AVATAR = "head212",
  SHIP = "ship209",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 372,
  LEVEL = 8,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 842,
  AVATAR = "head212",
  SHIP = "ship209",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 372,
  LEVEL = 9,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 842,
  AVATAR = "head212",
  SHIP = "ship209",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 372,
  LEVEL = 10,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 842,
  AVATAR = "head212",
  SHIP = "ship209",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 372,
  LEVEL = 11,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 842,
  AVATAR = "head212",
  SHIP = "ship209",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 372,
  LEVEL = 12,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 842,
  AVATAR = "head212",
  SHIP = "ship209",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 372,
  LEVEL = 13,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 842,
  AVATAR = "head212",
  SHIP = "ship209",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 372,
  LEVEL = 14,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 842,
  AVATAR = "head212",
  SHIP = "ship209",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 372,
  LEVEL = 15,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 842,
  AVATAR = "head212",
  SHIP = "ship209",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 372,
  LEVEL = 16,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 842,
  AVATAR = "head212",
  SHIP = "ship209",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 373,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head213",
  SHIP = "ship210",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 373,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head213",
  SHIP = "ship210",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 373,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head213",
  SHIP = "ship210",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 373,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head213",
  SHIP = "ship210",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 373,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head213",
  SHIP = "ship210",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 373,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head213",
  SHIP = "ship210",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 373,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head213",
  SHIP = "ship210",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 373,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head213",
  SHIP = "ship210",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 373,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head213",
  SHIP = "ship210",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 373,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head213",
  SHIP = "ship210",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 373,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head213",
  SHIP = "ship210",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 373,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head213",
  SHIP = "ship210",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 373,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head213",
  SHIP = "ship210",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 373,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head213",
  SHIP = "ship210",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 373,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head213",
  SHIP = "ship210",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 373,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head213",
  SHIP = "ship210",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 373,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head213",
  SHIP = "ship210",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 374,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 843,
  AVATAR = "head214",
  SHIP = "ship211",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 374,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 843,
  AVATAR = "head214",
  SHIP = "ship211",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 374,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 843,
  AVATAR = "head214",
  SHIP = "ship211",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 374,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 843,
  AVATAR = "head214",
  SHIP = "ship211",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 374,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 843,
  AVATAR = "head214",
  SHIP = "ship211",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 374,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 843,
  AVATAR = "head214",
  SHIP = "ship211",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 374,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 843,
  AVATAR = "head214",
  SHIP = "ship211",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 374,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 843,
  AVATAR = "head214",
  SHIP = "ship211",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 374,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 843,
  AVATAR = "head214",
  SHIP = "ship211",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 374,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 843,
  AVATAR = "head215",
  SHIP = "ship211",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 374,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 843,
  AVATAR = "head215",
  SHIP = "ship211",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 374,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 843,
  AVATAR = "head215",
  SHIP = "ship211",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 374,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 843,
  AVATAR = "head215",
  SHIP = "ship211",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 374,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 843,
  AVATAR = "head215",
  SHIP = "ship211",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 374,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 843,
  AVATAR = "head215",
  SHIP = "ship211",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 374,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 843,
  AVATAR = "head215",
  SHIP = "ship211",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 374,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 843,
  AVATAR = "head215",
  SHIP = "ship211",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 375,
  LEVEL = 0,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 848,
  AVATAR = "head218",
  SHIP = "ship213",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 375,
  LEVEL = 1,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 848,
  AVATAR = "head218",
  SHIP = "ship213",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 375,
  LEVEL = 2,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 848,
  AVATAR = "head218",
  SHIP = "ship213",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 375,
  LEVEL = 3,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 848,
  AVATAR = "head218",
  SHIP = "ship213",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 375,
  LEVEL = 4,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 848,
  AVATAR = "head218",
  SHIP = "ship213",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 375,
  LEVEL = 5,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 848,
  AVATAR = "head218",
  SHIP = "ship213",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 375,
  LEVEL = 6,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 848,
  AVATAR = "head218",
  SHIP = "ship213",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 375,
  LEVEL = 7,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 848,
  AVATAR = "head218",
  SHIP = "ship213",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 375,
  LEVEL = 8,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 848,
  AVATAR = "head218",
  SHIP = "ship213",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 375,
  LEVEL = 9,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 848,
  AVATAR = "head218",
  SHIP = "ship213",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 375,
  LEVEL = 10,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 848,
  AVATAR = "head218",
  SHIP = "ship213",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 375,
  LEVEL = 11,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 848,
  AVATAR = "head218",
  SHIP = "ship213",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 375,
  LEVEL = 12,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 848,
  AVATAR = "head218",
  SHIP = "ship213",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 375,
  LEVEL = 13,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 848,
  AVATAR = "head218",
  SHIP = "ship213",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 375,
  LEVEL = 14,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 848,
  AVATAR = "head218",
  SHIP = "ship213",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 375,
  LEVEL = 15,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 848,
  AVATAR = "head218",
  SHIP = "ship213",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 375,
  LEVEL = 16,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 848,
  AVATAR = "head218",
  SHIP = "ship213",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 376,
  LEVEL = 0,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 846,
  AVATAR = "head216",
  SHIP = "ship212",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 376,
  LEVEL = 1,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 846,
  AVATAR = "head216",
  SHIP = "ship212",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 376,
  LEVEL = 2,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 846,
  AVATAR = "head216",
  SHIP = "ship212",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 376,
  LEVEL = 3,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 846,
  AVATAR = "head216",
  SHIP = "ship212",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 376,
  LEVEL = 4,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 846,
  AVATAR = "head216",
  SHIP = "ship212",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 376,
  LEVEL = 5,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 846,
  AVATAR = "head216",
  SHIP = "ship212",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 376,
  LEVEL = 6,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 846,
  AVATAR = "head216",
  SHIP = "ship212",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 376,
  LEVEL = 7,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 846,
  AVATAR = "head216",
  SHIP = "ship212",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 376,
  LEVEL = 8,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 846,
  AVATAR = "head216",
  SHIP = "ship212",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 376,
  LEVEL = 9,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 846,
  AVATAR = "head217",
  SHIP = "ship212",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 376,
  LEVEL = 10,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 846,
  AVATAR = "head217",
  SHIP = "ship212",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 376,
  LEVEL = 11,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 846,
  AVATAR = "head217",
  SHIP = "ship212",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 376,
  LEVEL = 12,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 846,
  AVATAR = "head217",
  SHIP = "ship212",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 376,
  LEVEL = 13,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 846,
  AVATAR = "head217",
  SHIP = "ship212",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 376,
  LEVEL = 14,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 846,
  AVATAR = "head217",
  SHIP = "ship212",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 376,
  LEVEL = 15,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 846,
  AVATAR = "head217",
  SHIP = "ship212",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 376,
  LEVEL = 16,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 846,
  AVATAR = "head217",
  SHIP = "ship212",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 377,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head219",
  SHIP = "ship214",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 377,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head219",
  SHIP = "ship214",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 377,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head219",
  SHIP = "ship214",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 377,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head219",
  SHIP = "ship214",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 377,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head219",
  SHIP = "ship214",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 377,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head219",
  SHIP = "ship214",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 377,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head219",
  SHIP = "ship214",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 377,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head219",
  SHIP = "ship214",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 377,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head219",
  SHIP = "ship214",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 377,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head220",
  SHIP = "ship214",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 377,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head220",
  SHIP = "ship214",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 377,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head220",
  SHIP = "ship214",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 377,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head220",
  SHIP = "ship214",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 377,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head220",
  SHIP = "ship214",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 377,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head220",
  SHIP = "ship214",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 377,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head220",
  SHIP = "ship214",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 377,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head220",
  SHIP = "ship214",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 377,
  LEVEL = 17,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 849,
  AVATAR = "head220",
  SHIP = "ship214",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 377,
  LEVEL = 18,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 849,
  AVATAR = "head220",
  SHIP = "ship214",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 377,
  LEVEL = 19,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 849,
  AVATAR = "head220",
  SHIP = "ship214",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 377,
  LEVEL = 20,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 849,
  AVATAR = "head220",
  SHIP = "ship214",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 378,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head221",
  SHIP = "ship215",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 378,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head221",
  SHIP = "ship215",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 378,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head221",
  SHIP = "ship215",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 378,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head221",
  SHIP = "ship215",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 378,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head221",
  SHIP = "ship215",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 378,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head221",
  SHIP = "ship215",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 378,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head221",
  SHIP = "ship215",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 378,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head221",
  SHIP = "ship215",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 378,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head221",
  SHIP = "ship215",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 378,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head222",
  SHIP = "ship215",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 378,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head222",
  SHIP = "ship215",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 378,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head222",
  SHIP = "ship215",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 378,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head222",
  SHIP = "ship215",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 378,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head222",
  SHIP = "ship215",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 378,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head222",
  SHIP = "ship215",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 378,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head222",
  SHIP = "ship215",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 378,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head222",
  SHIP = "ship215",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 379,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 851,
  AVATAR = "head223",
  SHIP = "ship216",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 379,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 851,
  AVATAR = "head223",
  SHIP = "ship216",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 379,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 851,
  AVATAR = "head223",
  SHIP = "ship216",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 379,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 851,
  AVATAR = "head223",
  SHIP = "ship216",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 379,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 851,
  AVATAR = "head223",
  SHIP = "ship216",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 379,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 851,
  AVATAR = "head223",
  SHIP = "ship216",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 379,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 851,
  AVATAR = "head223",
  SHIP = "ship216",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 379,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 851,
  AVATAR = "head223",
  SHIP = "ship216",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 379,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 851,
  AVATAR = "head223",
  SHIP = "ship216",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 379,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 851,
  AVATAR = "head223",
  SHIP = "ship216",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 379,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 851,
  AVATAR = "head223",
  SHIP = "ship216",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 379,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 851,
  AVATAR = "head223",
  SHIP = "ship216",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 379,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 851,
  AVATAR = "head223",
  SHIP = "ship216",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 379,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 851,
  AVATAR = "head223",
  SHIP = "ship216",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 379,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 851,
  AVATAR = "head223",
  SHIP = "ship216",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 379,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 851,
  AVATAR = "head223",
  SHIP = "ship216",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 379,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 851,
  AVATAR = "head223",
  SHIP = "ship216",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 380,
  LEVEL = 0,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 852,
  AVATAR = "head224",
  SHIP = "ship136",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 380,
  LEVEL = 1,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 852,
  AVATAR = "head224",
  SHIP = "ship136",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 380,
  LEVEL = 2,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 852,
  AVATAR = "head224",
  SHIP = "ship136",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 380,
  LEVEL = 3,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 852,
  AVATAR = "head224",
  SHIP = "ship136",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 380,
  LEVEL = 4,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 852,
  AVATAR = "head224",
  SHIP = "ship136",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 380,
  LEVEL = 5,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 852,
  AVATAR = "head224",
  SHIP = "ship136",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 380,
  LEVEL = 6,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 852,
  AVATAR = "head224",
  SHIP = "ship136",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 380,
  LEVEL = 7,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 852,
  AVATAR = "head224",
  SHIP = "ship136",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 380,
  LEVEL = 8,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 852,
  AVATAR = "head224",
  SHIP = "ship136",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 380,
  LEVEL = 9,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 852,
  AVATAR = "head225",
  SHIP = "ship136",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 380,
  LEVEL = 10,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 852,
  AVATAR = "head225",
  SHIP = "ship136",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 380,
  LEVEL = 11,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 852,
  AVATAR = "head225",
  SHIP = "ship136",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 380,
  LEVEL = 12,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 852,
  AVATAR = "head225",
  SHIP = "ship136",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 380,
  LEVEL = 13,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 852,
  AVATAR = "head225",
  SHIP = "ship136",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 380,
  LEVEL = 14,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 852,
  AVATAR = "head225",
  SHIP = "ship136",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 380,
  LEVEL = 15,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 852,
  AVATAR = "head225",
  SHIP = "ship136",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 380,
  LEVEL = 16,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 852,
  AVATAR = "head225",
  SHIP = "ship136",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 381,
  LEVEL = 0,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 853,
  AVATAR = "head226",
  SHIP = "ship217",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 381,
  LEVEL = 1,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 853,
  AVATAR = "head226",
  SHIP = "ship217",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 381,
  LEVEL = 2,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 853,
  AVATAR = "head226",
  SHIP = "ship217",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 381,
  LEVEL = 3,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 853,
  AVATAR = "head226",
  SHIP = "ship217",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 381,
  LEVEL = 4,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 853,
  AVATAR = "head226",
  SHIP = "ship217",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 381,
  LEVEL = 5,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 853,
  AVATAR = "head226",
  SHIP = "ship217",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 381,
  LEVEL = 6,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 853,
  AVATAR = "head226",
  SHIP = "ship217",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 381,
  LEVEL = 7,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 853,
  AVATAR = "head226",
  SHIP = "ship217",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 381,
  LEVEL = 8,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 853,
  AVATAR = "head227",
  SHIP = "ship217",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 381,
  LEVEL = 9,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 853,
  AVATAR = "head227",
  SHIP = "ship217",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 381,
  LEVEL = 10,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 853,
  AVATAR = "head227",
  SHIP = "ship217",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 381,
  LEVEL = 11,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 853,
  AVATAR = "head227",
  SHIP = "ship217",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 381,
  LEVEL = 12,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 853,
  AVATAR = "head227",
  SHIP = "ship217",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 381,
  LEVEL = 13,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 853,
  AVATAR = "head227",
  SHIP = "ship217",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 381,
  LEVEL = 14,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 853,
  AVATAR = "head227",
  SHIP = "ship217",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 381,
  LEVEL = 15,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 853,
  AVATAR = "head227",
  SHIP = "ship217",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 381,
  LEVEL = 16,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 853,
  AVATAR = "head227",
  SHIP = "ship217",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 382,
  LEVEL = 0,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 854,
  AVATAR = "head230",
  SHIP = "ship219",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 382,
  LEVEL = 1,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 854,
  AVATAR = "head230",
  SHIP = "ship219",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 382,
  LEVEL = 2,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 854,
  AVATAR = "head230",
  SHIP = "ship219",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 382,
  LEVEL = 3,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 854,
  AVATAR = "head230",
  SHIP = "ship219",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 382,
  LEVEL = 4,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 854,
  AVATAR = "head230",
  SHIP = "ship219",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 382,
  LEVEL = 5,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 854,
  AVATAR = "head230",
  SHIP = "ship219",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 382,
  LEVEL = 6,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 854,
  AVATAR = "head230",
  SHIP = "ship219",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 382,
  LEVEL = 7,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 854,
  AVATAR = "head230",
  SHIP = "ship219",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 382,
  LEVEL = 8,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 854,
  AVATAR = "head230",
  SHIP = "ship219",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 382,
  LEVEL = 9,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 854,
  AVATAR = "head231",
  SHIP = "ship219",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 382,
  LEVEL = 10,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 854,
  AVATAR = "head231",
  SHIP = "ship219",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 382,
  LEVEL = 11,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 854,
  AVATAR = "head231",
  SHIP = "ship219",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 382,
  LEVEL = 12,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 854,
  AVATAR = "head231",
  SHIP = "ship219",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 382,
  LEVEL = 13,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 854,
  AVATAR = "head231",
  SHIP = "ship219",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 382,
  LEVEL = 14,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 854,
  AVATAR = "head231",
  SHIP = "ship219",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 382,
  LEVEL = 15,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 854,
  AVATAR = "head231",
  SHIP = "ship219",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 382,
  LEVEL = 16,
  vessels = 3,
  ATK_TYPE = 1,
  SPELL_ID = 854,
  AVATAR = "head231",
  SHIP = "ship219",
  sex = 0,
  point_page = "[7,8,9]"
})
table.insert(basic, {
  ID = 383,
  LEVEL = 0,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head228",
  SHIP = "ship218",
  sex = 1,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 383,
  LEVEL = 1,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head228",
  SHIP = "ship218",
  sex = 1,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 383,
  LEVEL = 2,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head228",
  SHIP = "ship218",
  sex = 1,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 383,
  LEVEL = 3,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head228",
  SHIP = "ship218",
  sex = 1,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 383,
  LEVEL = 4,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head228",
  SHIP = "ship218",
  sex = 1,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 383,
  LEVEL = 5,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head228",
  SHIP = "ship218",
  sex = 1,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 383,
  LEVEL = 6,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head228",
  SHIP = "ship218",
  sex = 1,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 383,
  LEVEL = 7,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head228",
  SHIP = "ship218",
  sex = 1,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 383,
  LEVEL = 8,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head228",
  SHIP = "ship218",
  sex = 1,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 383,
  LEVEL = 9,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head229",
  SHIP = "ship218",
  sex = 1,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 383,
  LEVEL = 10,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head229",
  SHIP = "ship218",
  sex = 1,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 383,
  LEVEL = 11,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head229",
  SHIP = "ship218",
  sex = 1,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 383,
  LEVEL = 12,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head229",
  SHIP = "ship218",
  sex = 1,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 383,
  LEVEL = 13,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head229",
  SHIP = "ship218",
  sex = 1,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 383,
  LEVEL = 14,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head229",
  SHIP = "ship218",
  sex = 1,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 383,
  LEVEL = 15,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head229",
  SHIP = "ship218",
  sex = 1,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 383,
  LEVEL = 16,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head229",
  SHIP = "ship218",
  sex = 1,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 384,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head232",
  SHIP = "ship220",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 384,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head232",
  SHIP = "ship220",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 384,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head232",
  SHIP = "ship220",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 384,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head232",
  SHIP = "ship220",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 384,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head232",
  SHIP = "ship220",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 384,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head232",
  SHIP = "ship220",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 384,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head233",
  SHIP = "ship220",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 384,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head233",
  SHIP = "ship220",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 384,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head233",
  SHIP = "ship220",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 384,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head233",
  SHIP = "ship220",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 384,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head233",
  SHIP = "ship220",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 384,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head233",
  SHIP = "ship220",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 384,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head234",
  SHIP = "ship220",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 384,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head234",
  SHIP = "ship220",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 384,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head234",
  SHIP = "ship220",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 384,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head234",
  SHIP = "ship220",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 384,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head234",
  SHIP = "ship220",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 385,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head235",
  SHIP = "ship221",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 385,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head235",
  SHIP = "ship221",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 385,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head235",
  SHIP = "ship221",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 385,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head235",
  SHIP = "ship221",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 385,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head235",
  SHIP = "ship221",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 385,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head235",
  SHIP = "ship221",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 385,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head235",
  SHIP = "ship221",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 385,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head235",
  SHIP = "ship221",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 385,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head235",
  SHIP = "ship221",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 385,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head236",
  SHIP = "ship221",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 385,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head236",
  SHIP = "ship221",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 385,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head236",
  SHIP = "ship221",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 385,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head236",
  SHIP = "ship221",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 385,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head236",
  SHIP = "ship221",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 385,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head236",
  SHIP = "ship221",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 385,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head236",
  SHIP = "ship221",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 385,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head236",
  SHIP = "ship221",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 386,
  LEVEL = 0,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 860,
  AVATAR = "head239",
  SHIP = "ship223",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 386,
  LEVEL = 1,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 860,
  AVATAR = "head239",
  SHIP = "ship223",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 386,
  LEVEL = 2,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 860,
  AVATAR = "head239",
  SHIP = "ship223",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 386,
  LEVEL = 3,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 860,
  AVATAR = "head239",
  SHIP = "ship223",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 386,
  LEVEL = 4,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 860,
  AVATAR = "head239",
  SHIP = "ship223",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 386,
  LEVEL = 5,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 860,
  AVATAR = "head239",
  SHIP = "ship223",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 386,
  LEVEL = 6,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 860,
  AVATAR = "head239",
  SHIP = "ship223",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 386,
  LEVEL = 7,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 860,
  AVATAR = "head239",
  SHIP = "ship223",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 386,
  LEVEL = 8,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 860,
  AVATAR = "head239",
  SHIP = "ship223",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 386,
  LEVEL = 9,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 860,
  AVATAR = "head239",
  SHIP = "ship223",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 386,
  LEVEL = 10,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 860,
  AVATAR = "head239",
  SHIP = "ship223",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 386,
  LEVEL = 11,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 860,
  AVATAR = "head239",
  SHIP = "ship223",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 386,
  LEVEL = 12,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 860,
  AVATAR = "head239",
  SHIP = "ship223",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 386,
  LEVEL = 13,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 860,
  AVATAR = "head239",
  SHIP = "ship223",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 386,
  LEVEL = 14,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 860,
  AVATAR = "head239",
  SHIP = "ship223",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 386,
  LEVEL = 15,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 860,
  AVATAR = "head239",
  SHIP = "ship223",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 386,
  LEVEL = 16,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 860,
  AVATAR = "head239",
  SHIP = "ship223",
  sex = 2,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 387,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head237",
  SHIP = "ship222",
  sex = 1,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 387,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head237",
  SHIP = "ship222",
  sex = 1,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 387,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head237",
  SHIP = "ship222",
  sex = 1,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 387,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head237",
  SHIP = "ship222",
  sex = 1,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 387,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head237",
  SHIP = "ship222",
  sex = 1,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 387,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head237",
  SHIP = "ship222",
  sex = 1,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 387,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head237",
  SHIP = "ship222",
  sex = 1,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 387,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head237",
  SHIP = "ship222",
  sex = 1,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 387,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head238",
  SHIP = "ship222",
  sex = 1,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 387,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head238",
  SHIP = "ship222",
  sex = 1,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 387,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head238",
  SHIP = "ship222",
  sex = 1,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 387,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head238",
  SHIP = "ship222",
  sex = 1,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 387,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head238",
  SHIP = "ship222",
  sex = 1,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 387,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head238",
  SHIP = "ship222",
  sex = 1,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 387,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head238",
  SHIP = "ship222",
  sex = 1,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 387,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head238",
  SHIP = "ship222",
  sex = 1,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 387,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head238",
  SHIP = "ship222",
  sex = 1,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 388,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head240",
  SHIP = "ship224",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 388,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head240",
  SHIP = "ship224",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 388,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head240",
  SHIP = "ship224",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 388,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head240",
  SHIP = "ship224",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 388,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head240",
  SHIP = "ship224",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 388,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head240",
  SHIP = "ship224",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 388,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head240",
  SHIP = "ship224",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 388,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head240",
  SHIP = "ship224",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 388,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head241",
  SHIP = "ship224",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 388,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head241",
  SHIP = "ship224",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 388,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head241",
  SHIP = "ship224",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 388,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head241",
  SHIP = "ship224",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 388,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head241",
  SHIP = "ship224",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 388,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head241",
  SHIP = "ship224",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 388,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head241",
  SHIP = "ship224",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 388,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head241",
  SHIP = "ship224",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 388,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head241",
  SHIP = "ship224",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 389,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head242",
  SHIP = "ship225",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 389,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head242",
  SHIP = "ship225",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 389,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head242",
  SHIP = "ship225",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 389,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head242",
  SHIP = "ship225",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 389,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head242",
  SHIP = "ship225",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 389,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head242",
  SHIP = "ship225",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 389,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head242",
  SHIP = "ship225",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 389,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head242",
  SHIP = "ship225",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 389,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head242",
  SHIP = "ship225",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 389,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head243",
  SHIP = "ship225",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 389,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head243",
  SHIP = "ship225",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 389,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head243",
  SHIP = "ship225",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 389,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head243",
  SHIP = "ship225",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 389,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head243",
  SHIP = "ship225",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 389,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head243",
  SHIP = "ship225",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 389,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head243",
  SHIP = "ship225",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 389,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head243",
  SHIP = "ship225",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 390,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head244",
  SHIP = "ship226",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 390,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head244",
  SHIP = "ship226",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 390,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head244",
  SHIP = "ship226",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 390,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head244",
  SHIP = "ship226",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 390,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head244",
  SHIP = "ship226",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 390,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head244",
  SHIP = "ship226",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 390,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head244",
  SHIP = "ship226",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 390,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head244",
  SHIP = "ship226",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 390,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head245",
  SHIP = "ship226",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 390,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head245",
  SHIP = "ship226",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 390,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head245",
  SHIP = "ship226",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 390,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head245",
  SHIP = "ship226",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 390,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head245",
  SHIP = "ship226",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 390,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head245",
  SHIP = "ship226",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 390,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head245",
  SHIP = "ship226",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 390,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head245",
  SHIP = "ship226",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 390,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head245",
  SHIP = "ship226",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 391,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head246",
  SHIP = "ship227",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 391,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head246",
  SHIP = "ship227",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 391,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head246",
  SHIP = "ship227",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 391,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head246",
  SHIP = "ship227",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 391,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head246",
  SHIP = "ship227",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 391,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head246",
  SHIP = "ship227",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 391,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head246",
  SHIP = "ship227",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 391,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head246",
  SHIP = "ship227",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 391,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head247",
  SHIP = "ship227",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 391,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head247",
  SHIP = "ship227",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 391,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head247",
  SHIP = "ship227",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 391,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head247",
  SHIP = "ship227",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 391,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head247",
  SHIP = "ship227",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 391,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head247",
  SHIP = "ship227",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 391,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head247",
  SHIP = "ship227",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 391,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head247",
  SHIP = "ship227",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 391,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head247",
  SHIP = "ship227",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 392,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head248",
  SHIP = "ship228",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 392,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head248",
  SHIP = "ship228",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 392,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head248",
  SHIP = "ship228",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 392,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head248",
  SHIP = "ship228",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 392,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head248",
  SHIP = "ship228",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 392,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head248",
  SHIP = "ship228",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 392,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head248",
  SHIP = "ship228",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 392,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head248",
  SHIP = "ship228",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 392,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head248",
  SHIP = "ship228",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 392,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head248",
  SHIP = "ship228",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 392,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head248",
  SHIP = "ship228",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 392,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head248",
  SHIP = "ship228",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 392,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head248",
  SHIP = "ship228",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 392,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head248",
  SHIP = "ship228",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 392,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head248",
  SHIP = "ship228",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 392,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head248",
  SHIP = "ship228",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 392,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head248",
  SHIP = "ship228",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 393,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head249",
  SHIP = "ship229",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 393,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head249",
  SHIP = "ship229",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 393,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head249",
  SHIP = "ship229",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 393,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head249",
  SHIP = "ship229",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 393,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head249",
  SHIP = "ship229",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 393,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head249",
  SHIP = "ship229",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 393,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head249",
  SHIP = "ship229",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 393,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head249",
  SHIP = "ship229",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 393,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head249",
  SHIP = "ship229",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 393,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head250",
  SHIP = "ship229",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 393,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head250",
  SHIP = "ship229",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 393,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head250",
  SHIP = "ship229",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 393,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head250",
  SHIP = "ship229",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 393,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head250",
  SHIP = "ship229",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 393,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head250",
  SHIP = "ship229",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 393,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head250",
  SHIP = "ship229",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 393,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head250",
  SHIP = "ship229",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 394,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head251",
  SHIP = "ship230",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 394,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head251",
  SHIP = "ship230",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 394,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head251",
  SHIP = "ship230",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 394,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head251",
  SHIP = "ship230",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 394,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head251",
  SHIP = "ship230",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 394,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head251",
  SHIP = "ship230",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 394,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head251",
  SHIP = "ship230",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 394,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head251",
  SHIP = "ship230",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 394,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head251",
  SHIP = "ship230",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 394,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head252",
  SHIP = "ship230",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 394,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head252",
  SHIP = "ship230",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 394,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head252",
  SHIP = "ship230",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 394,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head252",
  SHIP = "ship230",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 394,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head252",
  SHIP = "ship230",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 394,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head252",
  SHIP = "ship230",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 394,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head252",
  SHIP = "ship230",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 394,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head252",
  SHIP = "ship230",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 395,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head253",
  SHIP = "ship231",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 395,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head253",
  SHIP = "ship231",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 395,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head253",
  SHIP = "ship231",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 395,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head253",
  SHIP = "ship231",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 395,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head253",
  SHIP = "ship231",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 395,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head253",
  SHIP = "ship231",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 395,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head253",
  SHIP = "ship231",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 395,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head253",
  SHIP = "ship231",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 395,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head253",
  SHIP = "ship231",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 395,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head253",
  SHIP = "ship231",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 395,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head253",
  SHIP = "ship231",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 395,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head253",
  SHIP = "ship231",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 395,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head253",
  SHIP = "ship231",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 395,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head253",
  SHIP = "ship231",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 395,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head253",
  SHIP = "ship231",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 395,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head253",
  SHIP = "ship231",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 395,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head253",
  SHIP = "ship231",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 396,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head256",
  SHIP = "ship233",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 396,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head256",
  SHIP = "ship233",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 396,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head256",
  SHIP = "ship233",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 396,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head256",
  SHIP = "ship233",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 396,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head256",
  SHIP = "ship233",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 396,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head256",
  SHIP = "ship233",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 396,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head256",
  SHIP = "ship233",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 396,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head256",
  SHIP = "ship233",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 396,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head256",
  SHIP = "ship233",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 396,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head257",
  SHIP = "ship233",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 396,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head257",
  SHIP = "ship233",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 396,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head257",
  SHIP = "ship233",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 396,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head257",
  SHIP = "ship233",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 396,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head257",
  SHIP = "ship233",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 396,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head257",
  SHIP = "ship233",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 396,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head257",
  SHIP = "ship233",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 396,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head257",
  SHIP = "ship233",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 397,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head258",
  SHIP = "ship234",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 397,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head258",
  SHIP = "ship234",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 397,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head258",
  SHIP = "ship234",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 397,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head258",
  SHIP = "ship234",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 397,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head258",
  SHIP = "ship234",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 397,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head258",
  SHIP = "ship234",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 397,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head258",
  SHIP = "ship234",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 397,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head258",
  SHIP = "ship234",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 397,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head258",
  SHIP = "ship234",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 397,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head259",
  SHIP = "ship234",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 397,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head259",
  SHIP = "ship234",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 397,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head259",
  SHIP = "ship234",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 397,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head259",
  SHIP = "ship234",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 397,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head259",
  SHIP = "ship234",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 397,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head259",
  SHIP = "ship234",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 397,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head259",
  SHIP = "ship234",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 397,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head259",
  SHIP = "ship235",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 398,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head260",
  SHIP = "ship235",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 398,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head260",
  SHIP = "ship235",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 398,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head260",
  SHIP = "ship235",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 398,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head260",
  SHIP = "ship235",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 398,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head260",
  SHIP = "ship235",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 398,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head260",
  SHIP = "ship235",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 398,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head260",
  SHIP = "ship235",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 398,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head260",
  SHIP = "ship235",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 398,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head260",
  SHIP = "ship235",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 398,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head261",
  SHIP = "ship235",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 398,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head261",
  SHIP = "ship235",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 398,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head261",
  SHIP = "ship235",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 398,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head261",
  SHIP = "ship235",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 398,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head261",
  SHIP = "ship235",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 398,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head261",
  SHIP = "ship235",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 398,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head261",
  SHIP = "ship235",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 398,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head261",
  SHIP = "ship235",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 399,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head262",
  SHIP = "ship236",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 399,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head262",
  SHIP = "ship236",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 399,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head262",
  SHIP = "ship236",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 399,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head262",
  SHIP = "ship236",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 399,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head262",
  SHIP = "ship236",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 399,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head262",
  SHIP = "ship236",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 399,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head262",
  SHIP = "ship236",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 399,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head262",
  SHIP = "ship236",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 399,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head262",
  SHIP = "ship236",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 399,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head262",
  SHIP = "ship236",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 399,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head262",
  SHIP = "ship236",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 399,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head262",
  SHIP = "ship236",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 399,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head262",
  SHIP = "ship236",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 399,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head262",
  SHIP = "ship236",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 399,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head262",
  SHIP = "ship236",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 399,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head262",
  SHIP = "ship236",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 399,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head262",
  SHIP = "ship236",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 400,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head263",
  SHIP = "ship237",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 400,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head263",
  SHIP = "ship237",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 400,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head263",
  SHIP = "ship237",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 400,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head263",
  SHIP = "ship237",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 400,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head263",
  SHIP = "ship237",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 400,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head263",
  SHIP = "ship237",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 400,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head263",
  SHIP = "ship237",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 400,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head263",
  SHIP = "ship237",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 400,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head264",
  SHIP = "ship237",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 400,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head264",
  SHIP = "ship237",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 400,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head264",
  SHIP = "ship237",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 400,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head264",
  SHIP = "ship237",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 400,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head264",
  SHIP = "ship237",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 400,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head264",
  SHIP = "ship237",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 400,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head264",
  SHIP = "ship237",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 400,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head264",
  SHIP = "ship237",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 400,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head264",
  SHIP = "ship237",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 401,
  LEVEL = 0,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head265",
  SHIP = "ship238",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 401,
  LEVEL = 1,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head265",
  SHIP = "ship238",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 401,
  LEVEL = 2,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head265",
  SHIP = "ship238",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 401,
  LEVEL = 3,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head265",
  SHIP = "ship238",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 401,
  LEVEL = 4,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head265",
  SHIP = "ship238",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 401,
  LEVEL = 5,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head265",
  SHIP = "ship238",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 401,
  LEVEL = 6,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head265",
  SHIP = "ship238",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 401,
  LEVEL = 7,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head265",
  SHIP = "ship238",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 401,
  LEVEL = 8,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head265",
  SHIP = "ship238",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 401,
  LEVEL = 9,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head265",
  SHIP = "ship238",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 401,
  LEVEL = 10,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head265",
  SHIP = "ship238",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 401,
  LEVEL = 11,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head265",
  SHIP = "ship238",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 401,
  LEVEL = 12,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head265",
  SHIP = "ship238",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 401,
  LEVEL = 13,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head265",
  SHIP = "ship238",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 401,
  LEVEL = 14,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head265",
  SHIP = "ship238",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 401,
  LEVEL = 15,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head265",
  SHIP = "ship238",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 401,
  LEVEL = 16,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head265",
  SHIP = "ship238",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 402,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head254",
  SHIP = "ship232",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 402,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head254",
  SHIP = "ship232",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 402,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head254",
  SHIP = "ship232",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 402,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head254",
  SHIP = "ship232",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 402,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head254",
  SHIP = "ship232",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 402,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head254",
  SHIP = "ship232",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 402,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head254",
  SHIP = "ship232",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 402,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head254",
  SHIP = "ship232",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 402,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head254",
  SHIP = "ship232",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 402,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head255",
  SHIP = "ship232",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 402,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head255",
  SHIP = "ship232",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 402,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head255",
  SHIP = "ship232",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 402,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head255",
  SHIP = "ship232",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 402,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head255",
  SHIP = "ship232",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 402,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head255",
  SHIP = "ship232",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 402,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head255",
  SHIP = "ship232",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 402,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head255",
  SHIP = "ship232",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 403,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head266",
  SHIP = "ship239",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 403,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head266",
  SHIP = "ship239",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 403,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head266",
  SHIP = "ship239",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 403,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head266",
  SHIP = "ship239",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 403,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head266",
  SHIP = "ship239",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 403,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head266",
  SHIP = "ship239",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 403,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head266",
  SHIP = "ship239",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 403,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head266",
  SHIP = "ship239",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 403,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head266",
  SHIP = "ship239",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 403,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head267",
  SHIP = "ship239",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 403,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head267",
  SHIP = "ship239",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 403,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head267",
  SHIP = "ship239",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 403,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head267",
  SHIP = "ship239",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 403,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head267",
  SHIP = "ship239",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 403,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head267",
  SHIP = "ship239",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 403,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head267",
  SHIP = "ship239",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 403,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head267",
  SHIP = "ship239",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 404,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head268",
  SHIP = "ship240",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 404,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head268",
  SHIP = "ship240",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 404,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head268",
  SHIP = "ship240",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 404,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head268",
  SHIP = "ship240",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 404,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head268",
  SHIP = "ship240",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 404,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head268",
  SHIP = "ship240",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 404,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head268",
  SHIP = "ship240",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 404,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head268",
  SHIP = "ship240",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 404,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head268",
  SHIP = "ship240",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 404,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head269",
  SHIP = "ship240",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 404,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head269",
  SHIP = "ship240",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 404,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head269",
  SHIP = "ship240",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 404,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head269",
  SHIP = "ship240",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 404,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head269",
  SHIP = "ship240",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 404,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head269",
  SHIP = "ship240",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 404,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head269",
  SHIP = "ship240",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 404,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head269",
  SHIP = "ship240",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 405,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head270",
  SHIP = "ship241",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 405,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head270",
  SHIP = "ship241",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 405,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head270",
  SHIP = "ship241",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 405,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head270",
  SHIP = "ship241",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 405,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head270",
  SHIP = "ship241",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 405,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head270",
  SHIP = "ship241",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 405,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head270",
  SHIP = "ship241",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 405,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head270",
  SHIP = "ship241",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 405,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head270",
  SHIP = "ship241",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 405,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head271",
  SHIP = "ship241",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 405,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head271",
  SHIP = "ship241",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 405,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head271",
  SHIP = "ship241",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 405,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head271",
  SHIP = "ship241",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 405,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head271",
  SHIP = "ship241",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 405,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head271",
  SHIP = "ship241",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 405,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head271",
  SHIP = "ship241",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 405,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head271",
  SHIP = "ship241",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 405,
  LEVEL = 17,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head271",
  SHIP = "ship241",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 405,
  LEVEL = 18,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head271",
  SHIP = "ship241",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 405,
  LEVEL = 19,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head271",
  SHIP = "ship241",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 405,
  LEVEL = 20,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head271",
  SHIP = "ship241",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 406,
  LEVEL = 0,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head272",
  SHIP = "ship242",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 406,
  LEVEL = 1,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head272",
  SHIP = "ship242",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 406,
  LEVEL = 2,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head272",
  SHIP = "ship242",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 406,
  LEVEL = 3,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head272",
  SHIP = "ship242",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 406,
  LEVEL = 4,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head272",
  SHIP = "ship242",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 406,
  LEVEL = 5,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head272",
  SHIP = "ship242",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 406,
  LEVEL = 6,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head272",
  SHIP = "ship242",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 406,
  LEVEL = 7,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head272",
  SHIP = "ship242",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 406,
  LEVEL = 8,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head272",
  SHIP = "ship242",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 406,
  LEVEL = 9,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head272",
  SHIP = "ship242",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 406,
  LEVEL = 10,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head272",
  SHIP = "ship242",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 406,
  LEVEL = 11,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head272",
  SHIP = "ship242",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 406,
  LEVEL = 12,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head272",
  SHIP = "ship242",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 406,
  LEVEL = 13,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head272",
  SHIP = "ship242",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 406,
  LEVEL = 14,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head272",
  SHIP = "ship242",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 406,
  LEVEL = 15,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head272",
  SHIP = "ship242",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 406,
  LEVEL = 16,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head272",
  SHIP = "ship242",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 406,
  LEVEL = 17,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head272",
  SHIP = "ship242",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 406,
  LEVEL = 18,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head272",
  SHIP = "ship242",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 406,
  LEVEL = 19,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head272",
  SHIP = "ship242",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 406,
  LEVEL = 20,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head272",
  SHIP = "ship242",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 407,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head273",
  SHIP = "ship243",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 407,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head273",
  SHIP = "ship243",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 407,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head273",
  SHIP = "ship243",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 407,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head273",
  SHIP = "ship243",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 407,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head273",
  SHIP = "ship243",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 407,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head273",
  SHIP = "ship243",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 407,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head273",
  SHIP = "ship243",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 407,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head273",
  SHIP = "ship243",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 407,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head273",
  SHIP = "ship243",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 407,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head274",
  SHIP = "ship243",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 407,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head274",
  SHIP = "ship243",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 407,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head274",
  SHIP = "ship243",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 407,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head274",
  SHIP = "ship243",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 407,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head274",
  SHIP = "ship243",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 407,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head274",
  SHIP = "ship243",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 407,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head274",
  SHIP = "ship243",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 407,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head274",
  SHIP = "ship243",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 407,
  LEVEL = 17,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head274",
  SHIP = "ship243",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 407,
  LEVEL = 18,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head274",
  SHIP = "ship243",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 407,
  LEVEL = 19,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head274",
  SHIP = "ship243",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 407,
  LEVEL = 20,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head274",
  SHIP = "ship243",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 408,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head275",
  SHIP = "ship244",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 408,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head275",
  SHIP = "ship244",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 408,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head275",
  SHIP = "ship244",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 408,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head275",
  SHIP = "ship244",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 408,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head275",
  SHIP = "ship244",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 408,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head275",
  SHIP = "ship244",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 408,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head275",
  SHIP = "ship244",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 408,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head275",
  SHIP = "ship244",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 408,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head275",
  SHIP = "ship244",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 408,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head276",
  SHIP = "ship244",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 408,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head276",
  SHIP = "ship244",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 408,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head276",
  SHIP = "ship244",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 408,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head276",
  SHIP = "ship244",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 408,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head276",
  SHIP = "ship244",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 408,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head276",
  SHIP = "ship244",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 408,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head276",
  SHIP = "ship244",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 408,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head276",
  SHIP = "ship244",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 408,
  LEVEL = 17,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head276",
  SHIP = "ship244",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 408,
  LEVEL = 18,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head276",
  SHIP = "ship244",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 408,
  LEVEL = 19,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head276",
  SHIP = "ship244",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 408,
  LEVEL = 20,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head276",
  SHIP = "ship244",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 409,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head277",
  SHIP = "ship245",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 409,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head277",
  SHIP = "ship245",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 409,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head277",
  SHIP = "ship245",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 409,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head277",
  SHIP = "ship245",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 409,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head277",
  SHIP = "ship245",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 409,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head277",
  SHIP = "ship245",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 409,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head277",
  SHIP = "ship245",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 409,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head277",
  SHIP = "ship245",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 409,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head277",
  SHIP = "ship245",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 409,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head277",
  SHIP = "ship245",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 409,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head277",
  SHIP = "ship245",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 409,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head277",
  SHIP = "ship245",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 409,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head277",
  SHIP = "ship245",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 409,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head277",
  SHIP = "ship245",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 409,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head277",
  SHIP = "ship245",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 409,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head277",
  SHIP = "ship245",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 409,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head277",
  SHIP = "ship245",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 409,
  LEVEL = 17,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head277",
  SHIP = "ship245",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 409,
  LEVEL = 18,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head277",
  SHIP = "ship245",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 409,
  LEVEL = 19,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head277",
  SHIP = "ship245",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 409,
  LEVEL = 20,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head277",
  SHIP = "ship245",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 410,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head278",
  SHIP = "ship246",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 410,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head278",
  SHIP = "ship246",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 410,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head278",
  SHIP = "ship246",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 410,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head278",
  SHIP = "ship246",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 410,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head278",
  SHIP = "ship246",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 410,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head278",
  SHIP = "ship246",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 410,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head278",
  SHIP = "ship246",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 410,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head278",
  SHIP = "ship246",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 410,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head278",
  SHIP = "ship246",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 410,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head279",
  SHIP = "ship246",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 410,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head279",
  SHIP = "ship246",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 410,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head279",
  SHIP = "ship246",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 410,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head279",
  SHIP = "ship246",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 410,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head279",
  SHIP = "ship246",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 410,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head279",
  SHIP = "ship246",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 410,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head279",
  SHIP = "ship246",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 410,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head279",
  SHIP = "ship246",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 410,
  LEVEL = 17,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head279",
  SHIP = "ship246",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 410,
  LEVEL = 18,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head279",
  SHIP = "ship246",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 410,
  LEVEL = 19,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head279",
  SHIP = "ship246",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 410,
  LEVEL = 20,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head279",
  SHIP = "ship246",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 411,
  LEVEL = 0,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head280",
  SHIP = "ship247",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 411,
  LEVEL = 1,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head280",
  SHIP = "ship247",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 411,
  LEVEL = 2,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head280",
  SHIP = "ship247",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 411,
  LEVEL = 3,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head280",
  SHIP = "ship247",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 411,
  LEVEL = 4,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head280",
  SHIP = "ship247",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 411,
  LEVEL = 5,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head280",
  SHIP = "ship247",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 411,
  LEVEL = 6,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head280",
  SHIP = "ship247",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 411,
  LEVEL = 7,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head280",
  SHIP = "ship247",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 411,
  LEVEL = 8,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head280",
  SHIP = "ship247",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 411,
  LEVEL = 9,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head281",
  SHIP = "ship247",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 411,
  LEVEL = 10,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head281",
  SHIP = "ship247",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 411,
  LEVEL = 11,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head281",
  SHIP = "ship247",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 411,
  LEVEL = 12,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head281",
  SHIP = "ship247",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 411,
  LEVEL = 13,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head281",
  SHIP = "ship247",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 411,
  LEVEL = 14,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head281",
  SHIP = "ship247",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 411,
  LEVEL = 15,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head281",
  SHIP = "ship247",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 411,
  LEVEL = 16,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head281",
  SHIP = "ship247",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 411,
  LEVEL = 17,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head281",
  SHIP = "ship247",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 411,
  LEVEL = 18,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head281",
  SHIP = "ship247",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 411,
  LEVEL = 19,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head281",
  SHIP = "ship247",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 411,
  LEVEL = 20,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head281",
  SHIP = "ship247",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 412,
  LEVEL = 0,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head282",
  SHIP = "ship248",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 412,
  LEVEL = 1,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head282",
  SHIP = "ship248",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 412,
  LEVEL = 2,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head282",
  SHIP = "ship248",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 412,
  LEVEL = 3,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head282",
  SHIP = "ship248",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 412,
  LEVEL = 4,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head282",
  SHIP = "ship248",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 412,
  LEVEL = 5,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head282",
  SHIP = "ship248",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 412,
  LEVEL = 6,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head282",
  SHIP = "ship248",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 412,
  LEVEL = 7,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head282",
  SHIP = "ship248",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 412,
  LEVEL = 8,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head282",
  SHIP = "ship248",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 412,
  LEVEL = 9,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head282",
  SHIP = "ship248",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 412,
  LEVEL = 10,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head282",
  SHIP = "ship248",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 412,
  LEVEL = 11,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head282",
  SHIP = "ship248",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 412,
  LEVEL = 12,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head282",
  SHIP = "ship248",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 412,
  LEVEL = 13,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head282",
  SHIP = "ship248",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 412,
  LEVEL = 14,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head282",
  SHIP = "ship248",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 412,
  LEVEL = 15,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head282",
  SHIP = "ship248",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 412,
  LEVEL = 16,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head282",
  SHIP = "ship248",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 412,
  LEVEL = 17,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head282",
  SHIP = "ship248",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 412,
  LEVEL = 18,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head282",
  SHIP = "ship248",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 412,
  LEVEL = 19,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head282",
  SHIP = "ship248",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 412,
  LEVEL = 20,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head282",
  SHIP = "ship248",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 413,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head283",
  SHIP = "ship249",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 413,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head283",
  SHIP = "ship249",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 413,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head283",
  SHIP = "ship249",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 413,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head283",
  SHIP = "ship249",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 413,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head283",
  SHIP = "ship249",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 413,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head283",
  SHIP = "ship249",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 413,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head283",
  SHIP = "ship249",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 413,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head283",
  SHIP = "ship249",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 413,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head283",
  SHIP = "ship249",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 413,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head284",
  SHIP = "ship249",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 413,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head284",
  SHIP = "ship249",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 413,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head284",
  SHIP = "ship249",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 413,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head284",
  SHIP = "ship249",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 413,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head284",
  SHIP = "ship249",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 413,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head284",
  SHIP = "ship249",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 413,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head284",
  SHIP = "ship249",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 413,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head284",
  SHIP = "ship249",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 413,
  LEVEL = 17,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head284",
  SHIP = "ship249",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 413,
  LEVEL = 18,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head284",
  SHIP = "ship249",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 413,
  LEVEL = 19,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head284",
  SHIP = "ship249",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 413,
  LEVEL = 20,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head284",
  SHIP = "ship249",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 414,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head285",
  SHIP = "ship250",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 414,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head285",
  SHIP = "ship250",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 414,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head285",
  SHIP = "ship250",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 414,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head285",
  SHIP = "ship250",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 414,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head285",
  SHIP = "ship250",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 414,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head285",
  SHIP = "ship250",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 414,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head285",
  SHIP = "ship250",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 414,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head285",
  SHIP = "ship250",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 414,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head285",
  SHIP = "ship250",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 414,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head286",
  SHIP = "ship250",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 414,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head286",
  SHIP = "ship250",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 414,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head286",
  SHIP = "ship250",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 414,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head286",
  SHIP = "ship250",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 414,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head286",
  SHIP = "ship250",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 414,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head286",
  SHIP = "ship250",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 414,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head286",
  SHIP = "ship250",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 414,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head286",
  SHIP = "ship250",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 414,
  LEVEL = 17,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head286",
  SHIP = "ship250",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 414,
  LEVEL = 18,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head286",
  SHIP = "ship250",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 414,
  LEVEL = 19,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head286",
  SHIP = "ship250",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 414,
  LEVEL = 20,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head286",
  SHIP = "ship250",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 415,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head287",
  SHIP = "ship251",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 415,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head287",
  SHIP = "ship251",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 415,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head287",
  SHIP = "ship251",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 415,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head287",
  SHIP = "ship251",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 415,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head287",
  SHIP = "ship251",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 415,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head287",
  SHIP = "ship251",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 415,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head287",
  SHIP = "ship251",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 415,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head287",
  SHIP = "ship251",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 415,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head287",
  SHIP = "ship251",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 415,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head288",
  SHIP = "ship251",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 415,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head288",
  SHIP = "ship251",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 415,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head288",
  SHIP = "ship251",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 415,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head288",
  SHIP = "ship251",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 415,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head288",
  SHIP = "ship251",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 415,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head288",
  SHIP = "ship251",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 415,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head288",
  SHIP = "ship251",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 415,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head288",
  SHIP = "ship251",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 415,
  LEVEL = 17,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head288",
  SHIP = "ship251",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 415,
  LEVEL = 18,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head288",
  SHIP = "ship251",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 415,
  LEVEL = 19,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head288",
  SHIP = "ship251",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 415,
  LEVEL = 20,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head288",
  SHIP = "ship251",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 416,
  LEVEL = 0,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head289",
  SHIP = "ship252",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 416,
  LEVEL = 1,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head289",
  SHIP = "ship252",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 416,
  LEVEL = 2,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head289",
  SHIP = "ship252",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 416,
  LEVEL = 3,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head289",
  SHIP = "ship252",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 416,
  LEVEL = 4,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head289",
  SHIP = "ship252",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 416,
  LEVEL = 5,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head289",
  SHIP = "ship252",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 416,
  LEVEL = 6,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head289",
  SHIP = "ship252",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 416,
  LEVEL = 7,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head289",
  SHIP = "ship252",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 416,
  LEVEL = 8,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head289",
  SHIP = "ship252",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 416,
  LEVEL = 9,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head289",
  SHIP = "ship252",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 416,
  LEVEL = 10,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head289",
  SHIP = "ship252",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 416,
  LEVEL = 11,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head289",
  SHIP = "ship252",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 416,
  LEVEL = 12,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head289",
  SHIP = "ship252",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 416,
  LEVEL = 13,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head289",
  SHIP = "ship252",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 416,
  LEVEL = 14,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head289",
  SHIP = "ship252",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 416,
  LEVEL = 15,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head289",
  SHIP = "ship252",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 416,
  LEVEL = 16,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head289",
  SHIP = "ship252",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 416,
  LEVEL = 17,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head289",
  SHIP = "ship252",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 416,
  LEVEL = 18,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head289",
  SHIP = "ship252",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 416,
  LEVEL = 19,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head289",
  SHIP = "ship252",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 416,
  LEVEL = 20,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head289",
  SHIP = "ship252",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 417,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head290",
  SHIP = "ship253",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 417,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head290",
  SHIP = "ship253",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 417,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head290",
  SHIP = "ship253",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 417,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head290",
  SHIP = "ship253",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 417,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head290",
  SHIP = "ship253",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 417,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head290",
  SHIP = "ship253",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 417,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head290",
  SHIP = "ship253",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 417,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head290",
  SHIP = "ship253",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 417,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head290",
  SHIP = "ship253",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 417,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head290",
  SHIP = "ship253",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 417,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head290",
  SHIP = "ship253",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 417,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head290",
  SHIP = "ship253",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 417,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head290",
  SHIP = "ship253",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 417,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head290",
  SHIP = "ship253",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 417,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head290",
  SHIP = "ship253",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 417,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head290",
  SHIP = "ship253",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 417,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head290",
  SHIP = "ship253",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 417,
  LEVEL = 17,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head290",
  SHIP = "ship253",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 417,
  LEVEL = 18,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head290",
  SHIP = "ship253",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 417,
  LEVEL = 19,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head290",
  SHIP = "ship253",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 417,
  LEVEL = 20,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head290",
  SHIP = "ship253",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 418,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head291",
  SHIP = "ship254",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 418,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head291",
  SHIP = "ship254",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 418,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head291",
  SHIP = "ship254",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 418,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head291",
  SHIP = "ship254",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 418,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head291",
  SHIP = "ship254",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 418,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head291",
  SHIP = "ship254",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 418,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head291",
  SHIP = "ship254",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 418,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head291",
  SHIP = "ship254",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 418,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head291",
  SHIP = "ship254",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 418,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head292",
  SHIP = "ship254",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 418,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head292",
  SHIP = "ship254",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 418,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head292",
  SHIP = "ship254",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 418,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head292",
  SHIP = "ship254",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 418,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head292",
  SHIP = "ship254",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 418,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head292",
  SHIP = "ship254",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 418,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head292",
  SHIP = "ship254",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 418,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head292",
  SHIP = "ship254",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 418,
  LEVEL = 17,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head292",
  SHIP = "ship254",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 418,
  LEVEL = 18,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head292",
  SHIP = "ship254",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 418,
  LEVEL = 19,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head292",
  SHIP = "ship254",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 418,
  LEVEL = 20,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head292",
  SHIP = "ship254",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 419,
  LEVEL = 0,
  vessels = 4,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head293",
  SHIP = "ship255",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 419,
  LEVEL = 1,
  vessels = 4,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head293",
  SHIP = "ship255",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 419,
  LEVEL = 2,
  vessels = 4,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head293",
  SHIP = "ship255",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 419,
  LEVEL = 3,
  vessels = 4,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head293",
  SHIP = "ship255",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 419,
  LEVEL = 4,
  vessels = 4,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head293",
  SHIP = "ship255",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 419,
  LEVEL = 5,
  vessels = 4,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head293",
  SHIP = "ship255",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 419,
  LEVEL = 6,
  vessels = 4,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head293",
  SHIP = "ship255",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 419,
  LEVEL = 7,
  vessels = 4,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head293",
  SHIP = "ship255",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 419,
  LEVEL = 8,
  vessels = 4,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head293",
  SHIP = "ship255",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 419,
  LEVEL = 9,
  vessels = 4,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head293",
  SHIP = "ship255",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 419,
  LEVEL = 10,
  vessels = 4,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head293",
  SHIP = "ship255",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 419,
  LEVEL = 11,
  vessels = 4,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head293",
  SHIP = "ship255",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 419,
  LEVEL = 12,
  vessels = 4,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head293",
  SHIP = "ship255",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 419,
  LEVEL = 13,
  vessels = 4,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head293",
  SHIP = "ship255",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 419,
  LEVEL = 14,
  vessels = 4,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head293",
  SHIP = "ship255",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 419,
  LEVEL = 15,
  vessels = 4,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head293",
  SHIP = "ship255",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 419,
  LEVEL = 16,
  vessels = 4,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head293",
  SHIP = "ship255",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 419,
  LEVEL = 17,
  vessels = 4,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head293",
  SHIP = "ship255",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 419,
  LEVEL = 18,
  vessels = 4,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head293",
  SHIP = "ship255",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 419,
  LEVEL = 19,
  vessels = 4,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head293",
  SHIP = "ship255",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 419,
  LEVEL = 20,
  vessels = 4,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head293",
  SHIP = "ship255",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 420,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head294",
  SHIP = "ship256",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 420,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head294",
  SHIP = "ship256",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 420,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head294",
  SHIP = "ship256",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 420,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head294",
  SHIP = "ship256",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 420,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head294",
  SHIP = "ship256",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 420,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head294",
  SHIP = "ship256",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 420,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head294",
  SHIP = "ship256",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 420,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head294",
  SHIP = "ship256",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 420,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head294",
  SHIP = "ship256",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 420,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head295",
  SHIP = "ship256",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 420,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head295",
  SHIP = "ship256",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 420,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head295",
  SHIP = "ship256",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 420,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head295",
  SHIP = "ship256",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 420,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head295",
  SHIP = "ship256",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 420,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head295",
  SHIP = "ship256",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 420,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head295",
  SHIP = "ship256",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 420,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head295",
  SHIP = "ship256",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 420,
  LEVEL = 17,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head295",
  SHIP = "ship256",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 420,
  LEVEL = 18,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head295",
  SHIP = "ship256",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 420,
  LEVEL = 19,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head295",
  SHIP = "ship256",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 420,
  LEVEL = 20,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head295",
  SHIP = "ship256",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 421,
  LEVEL = 0,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head296",
  SHIP = "ship257",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 421,
  LEVEL = 1,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head296",
  SHIP = "ship257",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 421,
  LEVEL = 2,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head296",
  SHIP = "ship257",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 421,
  LEVEL = 3,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head296",
  SHIP = "ship257",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 421,
  LEVEL = 4,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head296",
  SHIP = "ship257",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 421,
  LEVEL = 5,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head296",
  SHIP = "ship257",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 421,
  LEVEL = 6,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head296",
  SHIP = "ship257",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 421,
  LEVEL = 7,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head296",
  SHIP = "ship257",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 421,
  LEVEL = 8,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head296",
  SHIP = "ship257",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 421,
  LEVEL = 9,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head297",
  SHIP = "ship257",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 421,
  LEVEL = 10,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head297",
  SHIP = "ship257",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 421,
  LEVEL = 11,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head297",
  SHIP = "ship257",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 421,
  LEVEL = 12,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head297",
  SHIP = "ship257",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 421,
  LEVEL = 13,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head297",
  SHIP = "ship257",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 421,
  LEVEL = 14,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head297",
  SHIP = "ship257",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 421,
  LEVEL = 15,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head297",
  SHIP = "ship257",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 421,
  LEVEL = 16,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head297",
  SHIP = "ship257",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 421,
  LEVEL = 17,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head297",
  SHIP = "ship257",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 421,
  LEVEL = 18,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head297",
  SHIP = "ship257",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 421,
  LEVEL = 19,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head297",
  SHIP = "ship257",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 421,
  LEVEL = 20,
  vessels = 2,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head297",
  SHIP = "ship257",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 422,
  LEVEL = 0,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 1,
  AVATAR = "head298",
  SHIP = "ship258",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 422,
  LEVEL = 1,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 1,
  AVATAR = "head298",
  SHIP = "ship258",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 422,
  LEVEL = 2,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 1,
  AVATAR = "head298",
  SHIP = "ship258",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 422,
  LEVEL = 3,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 1,
  AVATAR = "head298",
  SHIP = "ship258",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 422,
  LEVEL = 4,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 1,
  AVATAR = "head298",
  SHIP = "ship258",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 422,
  LEVEL = 5,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 1,
  AVATAR = "head298",
  SHIP = "ship258",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 422,
  LEVEL = 6,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 1,
  AVATAR = "head298",
  SHIP = "ship258",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 422,
  LEVEL = 7,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 1,
  AVATAR = "head298",
  SHIP = "ship258",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 422,
  LEVEL = 8,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 1,
  AVATAR = "head298",
  SHIP = "ship258",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 422,
  LEVEL = 9,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 1,
  AVATAR = "head299",
  SHIP = "ship258",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 422,
  LEVEL = 10,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 1,
  AVATAR = "head299",
  SHIP = "ship258",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 422,
  LEVEL = 11,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 1,
  AVATAR = "head299",
  SHIP = "ship258",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 422,
  LEVEL = 12,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 1,
  AVATAR = "head299",
  SHIP = "ship258",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 422,
  LEVEL = 13,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 1,
  AVATAR = "head299",
  SHIP = "ship258",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 422,
  LEVEL = 14,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 1,
  AVATAR = "head299",
  SHIP = "ship258",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 422,
  LEVEL = 15,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 1,
  AVATAR = "head299",
  SHIP = "ship258",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 422,
  LEVEL = 16,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 1,
  AVATAR = "head299",
  SHIP = "ship258",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 422,
  LEVEL = 17,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 1,
  AVATAR = "head299",
  SHIP = "ship258",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 422,
  LEVEL = 18,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 1,
  AVATAR = "head299",
  SHIP = "ship258",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 422,
  LEVEL = 19,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 1,
  AVATAR = "head299",
  SHIP = "ship258",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 422,
  LEVEL = 20,
  vessels = 4,
  ATK_TYPE = 2,
  SPELL_ID = 1,
  AVATAR = "head299",
  SHIP = "ship258",
  sex = 0,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 423,
  LEVEL = 0,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head302",
  SHIP = "ship259",
  sex = 0,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 423,
  LEVEL = 1,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head302",
  SHIP = "ship259",
  sex = 0,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 423,
  LEVEL = 2,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head302",
  SHIP = "ship259",
  sex = 0,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 423,
  LEVEL = 3,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head302",
  SHIP = "ship259",
  sex = 0,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 423,
  LEVEL = 4,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head302",
  SHIP = "ship259",
  sex = 0,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 423,
  LEVEL = 5,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head302",
  SHIP = "ship259",
  sex = 0,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 423,
  LEVEL = 6,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head302",
  SHIP = "ship259",
  sex = 0,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 423,
  LEVEL = 7,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head302",
  SHIP = "ship259",
  sex = 0,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 423,
  LEVEL = 8,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head302",
  SHIP = "ship259",
  sex = 0,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 423,
  LEVEL = 9,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head302",
  SHIP = "ship259",
  sex = 0,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 423,
  LEVEL = 10,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head302",
  SHIP = "ship259",
  sex = 0,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 423,
  LEVEL = 11,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head302",
  SHIP = "ship259",
  sex = 0,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 423,
  LEVEL = 12,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head302",
  SHIP = "ship259",
  sex = 0,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 423,
  LEVEL = 13,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head302",
  SHIP = "ship259",
  sex = 0,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 423,
  LEVEL = 14,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head302",
  SHIP = "ship259",
  sex = 0,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 423,
  LEVEL = 15,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head302",
  SHIP = "ship259",
  sex = 0,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 423,
  LEVEL = 16,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head302",
  SHIP = "ship259",
  sex = 0,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 423,
  LEVEL = 17,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head302",
  SHIP = "ship259",
  sex = 0,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 423,
  LEVEL = 18,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head302",
  SHIP = "ship259",
  sex = 0,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 423,
  LEVEL = 19,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head302",
  SHIP = "ship259",
  sex = 0,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 423,
  LEVEL = 20,
  vessels = 6,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head302",
  SHIP = "ship259",
  sex = 0,
  point_page = "[10,11,12]"
})
table.insert(basic, {
  ID = 424,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head303",
  SHIP = "ship260",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 424,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head303",
  SHIP = "ship260",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 424,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head303",
  SHIP = "ship260",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 424,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head303",
  SHIP = "ship260",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 424,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head303",
  SHIP = "ship260",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 424,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head303",
  SHIP = "ship260",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 424,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head303",
  SHIP = "ship260",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 424,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head303",
  SHIP = "ship260",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 424,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head303",
  SHIP = "ship260",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 424,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head304",
  SHIP = "ship260",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 424,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head304",
  SHIP = "ship260",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 424,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head304",
  SHIP = "ship260",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 424,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head304",
  SHIP = "ship260",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 424,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head304",
  SHIP = "ship260",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 424,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head304",
  SHIP = "ship260",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 424,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head304",
  SHIP = "ship260",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 424,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head304",
  SHIP = "ship260",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 424,
  LEVEL = 17,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head304",
  SHIP = "ship260",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 424,
  LEVEL = 18,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head304",
  SHIP = "ship260",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 424,
  LEVEL = 19,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head304",
  SHIP = "ship260",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 424,
  LEVEL = 20,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head304",
  SHIP = "ship260",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 425,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head305",
  SHIP = "ship261",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 425,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head305",
  SHIP = "ship261",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 425,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head305",
  SHIP = "ship261",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 425,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head305",
  SHIP = "ship261",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 425,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head305",
  SHIP = "ship261",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 425,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head305",
  SHIP = "ship261",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 425,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head305",
  SHIP = "ship261",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 425,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head305",
  SHIP = "ship261",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 425,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head305",
  SHIP = "ship261",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 425,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head306",
  SHIP = "ship261",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 425,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head306",
  SHIP = "ship261",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 425,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head306",
  SHIP = "ship261",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 425,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head306",
  SHIP = "ship261",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 425,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head306",
  SHIP = "ship261",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 425,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head306",
  SHIP = "ship261",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 425,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head306",
  SHIP = "ship261",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 425,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head306",
  SHIP = "ship261",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 425,
  LEVEL = 17,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head306",
  SHIP = "ship261",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 425,
  LEVEL = 18,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head306",
  SHIP = "ship261",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 425,
  LEVEL = 19,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head306",
  SHIP = "ship261",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 425,
  LEVEL = 20,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head306",
  SHIP = "ship261",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 426,
  LEVEL = 0,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head307",
  SHIP = "ship262",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 426,
  LEVEL = 1,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head307",
  SHIP = "ship262",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 426,
  LEVEL = 2,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head307",
  SHIP = "ship262",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 426,
  LEVEL = 3,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head307",
  SHIP = "ship262",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 426,
  LEVEL = 4,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head307",
  SHIP = "ship262",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 426,
  LEVEL = 5,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head307",
  SHIP = "ship262",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 426,
  LEVEL = 6,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head307",
  SHIP = "ship262",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 426,
  LEVEL = 7,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head307",
  SHIP = "ship262",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 426,
  LEVEL = 8,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head307",
  SHIP = "ship262",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 426,
  LEVEL = 9,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head308",
  SHIP = "ship262",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 426,
  LEVEL = 10,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head308",
  SHIP = "ship262",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 426,
  LEVEL = 11,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head308",
  SHIP = "ship262",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 426,
  LEVEL = 12,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head308",
  SHIP = "ship262",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 426,
  LEVEL = 13,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head308",
  SHIP = "ship262",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 426,
  LEVEL = 14,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head308",
  SHIP = "ship262",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 426,
  LEVEL = 15,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head308",
  SHIP = "ship262",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 426,
  LEVEL = 16,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head308",
  SHIP = "ship262",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 426,
  LEVEL = 17,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head308",
  SHIP = "ship262",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 426,
  LEVEL = 18,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head308",
  SHIP = "ship262",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 426,
  LEVEL = 19,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head308",
  SHIP = "ship262",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 426,
  LEVEL = 20,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head308",
  SHIP = "ship262",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 427,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head309",
  SHIP = "ship263",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 427,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head309",
  SHIP = "ship263",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 427,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head309",
  SHIP = "ship263",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 427,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head309",
  SHIP = "ship263",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 427,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head309",
  SHIP = "ship263",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 427,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head309",
  SHIP = "ship263",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 427,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head309",
  SHIP = "ship263",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 427,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head309",
  SHIP = "ship263",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 427,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head309",
  SHIP = "ship263",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 427,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head310",
  SHIP = "ship263",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 427,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head310",
  SHIP = "ship263",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 427,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head310",
  SHIP = "ship263",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 427,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head310",
  SHIP = "ship263",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 427,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head310",
  SHIP = "ship263",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 427,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head310",
  SHIP = "ship263",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 427,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head310",
  SHIP = "ship263",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 427,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head310",
  SHIP = "ship263",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 427,
  LEVEL = 17,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head310",
  SHIP = "ship263",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 427,
  LEVEL = 18,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head310",
  SHIP = "ship263",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 427,
  LEVEL = 19,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head310",
  SHIP = "ship263",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 427,
  LEVEL = 20,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head310",
  SHIP = "ship263",
  sex = 0,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 428,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head311",
  SHIP = "ship264",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 428,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head311",
  SHIP = "ship264",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 428,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head311",
  SHIP = "ship264",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 428,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head311",
  SHIP = "ship264",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 428,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head311",
  SHIP = "ship264",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 428,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head311",
  SHIP = "ship264",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 428,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head311",
  SHIP = "ship264",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 428,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head311",
  SHIP = "ship264",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 428,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head311",
  SHIP = "ship264",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 428,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head312",
  SHIP = "ship264",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 428,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head312",
  SHIP = "ship264",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 428,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head312",
  SHIP = "ship264",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 428,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head312",
  SHIP = "ship264",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 428,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head312",
  SHIP = "ship264",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 428,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head312",
  SHIP = "ship264",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 428,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head312",
  SHIP = "ship264",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 428,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head312",
  SHIP = "ship264",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 428,
  LEVEL = 17,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head312",
  SHIP = "ship264",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 428,
  LEVEL = 18,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head312",
  SHIP = "ship264",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 428,
  LEVEL = 19,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head312",
  SHIP = "ship264",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 428,
  LEVEL = 20,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head312",
  SHIP = "ship264",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 429,
  LEVEL = 0,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head313",
  SHIP = "ship265",
  sex = 2,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 429,
  LEVEL = 1,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head313",
  SHIP = "ship265",
  sex = 2,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 429,
  LEVEL = 2,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head313",
  SHIP = "ship265",
  sex = 2,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 429,
  LEVEL = 3,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head313",
  SHIP = "ship265",
  sex = 2,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 429,
  LEVEL = 4,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head313",
  SHIP = "ship265",
  sex = 2,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 429,
  LEVEL = 5,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head313",
  SHIP = "ship265",
  sex = 2,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 429,
  LEVEL = 6,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head313",
  SHIP = "ship265",
  sex = 2,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 429,
  LEVEL = 7,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head313",
  SHIP = "ship265",
  sex = 2,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 429,
  LEVEL = 8,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head313",
  SHIP = "ship265",
  sex = 2,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 429,
  LEVEL = 9,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head313",
  SHIP = "ship265",
  sex = 2,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 429,
  LEVEL = 10,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head313",
  SHIP = "ship265",
  sex = 2,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 429,
  LEVEL = 11,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head313",
  SHIP = "ship265",
  sex = 2,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 429,
  LEVEL = 12,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head313",
  SHIP = "ship265",
  sex = 2,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 429,
  LEVEL = 13,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head313",
  SHIP = "ship265",
  sex = 2,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 429,
  LEVEL = 14,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head313",
  SHIP = "ship265",
  sex = 2,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 429,
  LEVEL = 15,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head313",
  SHIP = "ship265",
  sex = 2,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 429,
  LEVEL = 16,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head313",
  SHIP = "ship265",
  sex = 2,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 429,
  LEVEL = 17,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head313",
  SHIP = "ship265",
  sex = 2,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 429,
  LEVEL = 18,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head313",
  SHIP = "ship265",
  sex = 2,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 429,
  LEVEL = 19,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head313",
  SHIP = "ship265",
  sex = 2,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 429,
  LEVEL = 20,
  vessels = 1,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head313",
  SHIP = "ship265",
  sex = 2,
  point_page = "[4,5,6]"
})
table.insert(basic, {
  ID = 430,
  LEVEL = 0,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head314",
  SHIP = "ship266",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 430,
  LEVEL = 1,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head314",
  SHIP = "ship266",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 430,
  LEVEL = 2,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head314",
  SHIP = "ship266",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 430,
  LEVEL = 3,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head314",
  SHIP = "ship266",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 430,
  LEVEL = 4,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head314",
  SHIP = "ship266",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 430,
  LEVEL = 5,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head314",
  SHIP = "ship266",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 430,
  LEVEL = 6,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head314",
  SHIP = "ship266",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 430,
  LEVEL = 7,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head314",
  SHIP = "ship266",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 430,
  LEVEL = 8,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head314",
  SHIP = "ship266",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 430,
  LEVEL = 9,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head314",
  SHIP = "ship266",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 430,
  LEVEL = 10,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head314",
  SHIP = "ship266",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 430,
  LEVEL = 11,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head314",
  SHIP = "ship266",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 430,
  LEVEL = 12,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head314",
  SHIP = "ship266",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 430,
  LEVEL = 13,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head314",
  SHIP = "ship266",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 430,
  LEVEL = 14,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head314",
  SHIP = "ship266",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 430,
  LEVEL = 15,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head314",
  SHIP = "ship266",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 430,
  LEVEL = 16,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head314",
  SHIP = "ship266",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 430,
  LEVEL = 17,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head314",
  SHIP = "ship266",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 430,
  LEVEL = 18,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head314",
  SHIP = "ship266",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 430,
  LEVEL = 19,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head314",
  SHIP = "ship266",
  sex = 1,
  point_page = "[1,2,3]"
})
table.insert(basic, {
  ID = 430,
  LEVEL = 20,
  vessels = 5,
  ATK_TYPE = 1,
  SPELL_ID = 1,
  AVATAR = "head314",
  SHIP = "ship266",
  sex = 1,
  point_page = "[1,2,3]"
})
