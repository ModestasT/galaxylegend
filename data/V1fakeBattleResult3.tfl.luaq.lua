local fakeBattleResult3 = {
  player1 = "vddfv. vbb.s991",
  player1_identity = 1,
  player1_pos = 5,
  player2_identity = 6000302,
  player2_pos = 3,
  player2_avatar = "head13",
  player2 = "battle",
  player1_avatar = "female",
  result = 1,
  rounds = {
    [1] = {
      attacks = {
        [1] = {
          attack_type = 1,
          atk_effect = "normal_new2",
          def_pos = 2,
          shield = 0,
          atk_acc_change = 25,
          acc = 75,
          intercept = false,
          atk_acc = 75,
          shield_damage = 0,
          acc_change = 25,
          hit = true,
          dur_damage = 123,
          durability = 0,
          atk_pos = 5,
          crit = false
        }
      },
      sp_attacks = {},
      buff = {},
      player1_action = true,
      dot = {},
      pos = 5,
      round_cnt = 1
    },
    [2] = {
      attacks = {
        [1] = {
          attack_type = 1,
          atk_effect = "normal",
          def_pos = 5,
          shield = 0,
          atk_acc_change = 25,
          acc = 100,
          intercept = false,
          atk_acc = 75,
          shield_damage = 0,
          acc_change = 25,
          hit = true,
          dur_damage = 26,
          durability = 94,
          atk_pos = 3,
          crit = false
        }
      },
      sp_attacks = {},
      buff = {},
      player1_action = false,
      dot = {},
      pos = 3,
      round_cnt = 2
    },
    [3] = {
      attacks = {},
      sp_attacks = {
        [1] = {
          durability = 63,
          def_pos = 5,
          sp_id = 303,
          shield = 0,
          buff_effect = 204,
          acc = 125,
          intercept = false,
          self_cast = false,
          shield_damage = 0,
          acc_change = 25,
          atk_acc_change = -100,
          atk_acc = 0,
          hit = true,
          dur_damage = 31,
          round_cnt = 2,
          atk_pos = 4,
          crit = false
        }
      },
      buff = {},
      player1_action = false,
      dot = {},
      pos = 4,
      round_cnt = 3
    },
    [4] = {
      attacks = {
        [1] = {
          attack_type = 1,
          atk_effect = "normal",
          def_pos = 5,
          shield = 0,
          atk_acc_change = 25,
          acc = 150,
          intercept = false,
          atk_acc = 100,
          shield_damage = 0,
          acc_change = 25,
          hit = true,
          dur_damage = 24,
          durability = 39,
          atk_pos = 3,
          crit = false
        }
      },
      sp_attacks = {},
      buff = {},
      player1_action = false,
      dot = {},
      pos = 3,
      round_cnt = 4
    },
    [5] = {
      attacks = {
        [1] = {
          attack_type = 1,
          atk_effect = "normal",
          def_pos = 5,
          shield = 0,
          atk_acc_change = 25,
          acc = 175,
          intercept = false,
          atk_acc = 25,
          shield_damage = 0,
          acc_change = 25,
          hit = true,
          dur_damage = 26,
          durability = 13,
          atk_pos = 4,
          crit = false
        }
      },
      sp_attacks = {},
      buff = {},
      player1_action = false,
      dot = {},
      pos = 4,
      round_cnt = 5
    },
    [6] = {
      attacks = {
        [1] = {
          attack_type = 1,
          atk_effect = "missile",
          def_pos = 4,
          shield = 0,
          atk_acc_change = 25,
          acc = 50,
          intercept = false,
          atk_acc = 75,
          shield_damage = 0,
          acc_change = 25,
          hit = true,
          dur_damage = 172,
          durability = 0,
          atk_pos = 2,
          crit = true
        }
      },
      sp_attacks = {},
      buff = {},
      player1_action = true,
      dot = {},
      pos = 2,
      round_cnt = 6
    },
    [7] = {
      attacks = {},
      sp_attacks = {
        [1] = {
          durability = 60,
          def_pos = 2,
          sp_id = 303,
          shield = 0,
          buff_effect = 204,
          acc = 100,
          intercept = false,
          self_cast = false,
          shield_damage = 0,
          acc_change = 25,
          atk_acc_change = -100,
          atk_acc = 0,
          hit = true,
          dur_damage = 46,
          round_cnt = 2,
          atk_pos = 3,
          crit = false
        }
      },
      buff = {},
      player1_action = false,
      dot = {},
      pos = 3,
      round_cnt = 7
    },
    [8] = {
      attacks = {},
      sp_attacks = {
        [1] = {
          durability = 0,
          def_pos = 3,
          sp_id = 2,
          shield = 0,
          buff_effect = 0,
          acc = 25,
          intercept = false,
          self_cast = false,
          shield_damage = 0,
          acc_change = 25,
          atk_acc_change = -100,
          atk_acc = 0,
          hit = true,
          dur_damage = 258,
          round_cnt = 0,
          atk_pos = 5,
          crit = false
        }
      },
      buff = {
        [1] = {
          buff_step = 2,
          side = "p1",
          param = 0,
          buff_effect = 204,
          pos = 5,
          round_cnt = 1
        }
      },
      player1_action = true,
      dot = {},
      pos = 5,
      round_cnt = 8
    }
  },
  player1_fleets = {
    [1] = {
      max_shield = 0,
      shield = 0,
      cur_durability = 120,
      cur_accumulator = 50,
      max_durability = 120,
      identity = 1,
      pos = 5
    },
    [2] = {
      max_shield = 0,
      shield = 0,
      cur_durability = 106,
      cur_accumulator = 50,
      max_durability = 106,
      identity = 4,
      pos = 2
    }
  },
  player2_fleets = {
    [1] = {
      max_shield = 0,
      shield = 0,
      cur_durability = 120,
      cur_accumulator = 50,
      max_durability = 120,
      identity = 6000301,
      pos = 2
    },
    [2] = {
      max_shield = 0,
      shield = 0,
      cur_durability = 100,
      cur_accumulator = 50,
      max_durability = 100,
      identity = 6000302,
      pos = 3
    },
    [3] = {
      max_shield = 0,
      shield = 0,
      cur_durability = 100,
      cur_accumulator = 100,
      max_durability = 100,
      identity = 6000303,
      pos = 4
    }
  }
}
local fakeBattleAnim3 = {
  [1] = {
    round_index = 4,
    round_step = 2,
    attack_round_index = -1,
    dialog_id = {1100022},
    befor_dialog_anim = nil,
    hide_anim_in_dialog = true,
    after_dialog_anim = nil
  },
  [2] = {
    round_index = 6,
    round_step = 2,
    attack_round_index = -1,
    dialog_id = {1100023},
    befor_dialog_anim = {
      {
        "ShowShipAppear",
        true,
        2
      }
    },
    hide_anim_in_dialog = true,
    after_dialog_anim = nil
  }
}
local fakeBattleCommand3 = {
  {
    "HideShipInFakeBattle",
    true,
    2
  }
}
GameData.fakeBattleAnim3 = fakeBattleAnim3
GameData.fakeBattleResult3 = fakeBattleResult3
GameData.fakeBattleCommand3 = fakeBattleCommand3
