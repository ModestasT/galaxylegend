require("FleetTeamLeagueMatrix.tfl")
local GameStateTlcFormation = GameStateManager.GameStateTlcFormation
local GameObjectTlcFormation = LuaObjectManager:GetLuaObject("GameObjectTlcFormation")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameUIAdjutant = LuaObjectManager:GetLuaObject("GameUIAdjutant")
local GameStateTacticsCenter = GameStateManager.GameStateTacticsCenter
local RefineDataManager = LuaObjectManager:GetLuaObject("RefineDataManager")
local QuestTutorialTacticsCenter = TutorialQuestManager.QuestTutorialTacticsCenter
local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
local GameUIWDStuff = LuaObjectManager:GetLuaObject("GameUIWDStuff")
local GameUIMaster = LuaObjectManager:GetLuaObject("GameUIMaster")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local curMatrixInstance
function GameObjectTlcFormation:Init()
  self:Clear()
  assert(GameObjectTlcFormation.currentMatrixIndex ~= nil)
  assert(GameObjectTlcFormation.currentFormationType ~= nil)
  curMatrixInstance = nil
  if GameObjectTlcFormation.currentFormationType == TEAMLEAGUE_MATRIX_ATTACK then
    curMatrixInstance = FleetTeamLeagueMatrix:GetAttackInstance()
  elseif GameObjectTlcFormation.currentFormationType == TEAMLEAGUE_MATRIX_DEFENCE then
    curMatrixInstance = FleetTeamLeagueMatrix:GetDefenceInstance()
  end
  assert(curMatrixInstance)
  assert(curMatrixInstance:HasInit() == true)
  GameObjectTlcFormation.battle_matrix = {}
  GameObjectTlcFormation.rest_matrix = {}
  GameObjectTlcFormation.mCurFreeMatrixSubIdx = 0
  self:InitFormation()
  self:InitUI()
end
function GameObjectTlcFormation:Clear()
  self.battle_matrix = {}
  self.rest_matrix = {}
  self.m_nextTeamMatrix = {}
  self.m_nextPlayerRestFleets = {}
end
function GameObjectTlcFormation:OnAddToGameState(new_parent)
  DebugOut("OnAddToGameState")
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  self:Init()
end
function GameObjectTlcFormation:InitFormation()
  self:Clear()
  if self.currentMatrixIndex == nil then
    self.currentMatrixIndex = 1
  end
  local init_fleets_matrix = curMatrixInstance:GetMatrixByIndex(self.currentMatrixIndex)
  local leaderlist = GameGlobalData:GetData("leaderlist")
  self.curMainFleetIndex = 1
  self.mainFleetList = leaderlist.options
  local curr_id = curMatrixInstance:GetLeaderFleet()
  for k, v in ipairs(self.mainFleetList) do
    DebugOut("v:" .. v)
    if v == curr_id then
      self.curMainFleetIndex = k
      DebugOut("k ;;", k)
    end
  end
  for i = 1, 9 do
    self.battle_matrix[i] = 0
  end
  for i, v in pairs(init_fleets_matrix.cells) do
    self.battle_matrix[v.cell_id] = v.fleet_identity
  end
  self.rest_matrix = curMatrixInstance:GetFreeFleets()
  for i = 1, 9 do
    if not self.rest_matrix[i] then
      self.rest_matrix[i] = -1
    end
  end
  for i = 1, 9 do
    GameObjectTlcFormation:UpdateCommanderItem("battle", i)
    GameObjectTlcFormation:UpdateCommanderItem("rest", i)
  end
end
function GameObjectTlcFormation:InitUI()
  local uiText = {}
  uiText.team1Text = GameLoader:GetGameText("LC_MENU_TEAMLEAGUE_TEAM1_CHAR")
  uiText.team2Text = GameLoader:GetGameText("LC_MENU_TEAMLEAGUE_TEAM2_CHAR")
  uiText.team3Text = GameLoader:GetGameText("LC_MENU_TEAMLEAGUE_TEAM3_CHAR")
  if GameObjectTlcFormation.currentFormationType == TEAMLEAGUE_MATRIX_ATTACK then
    uiText.titleText = GameLoader:GetGameText("LC_MENU_TEAMLEAGUE_ATTACK_FORMATION_TITLE")
  elseif GameObjectTlcFormation.currentFormationType == TEAMLEAGUE_MATRIX_DEFENCE then
    uiText.titleText = GameLoader:GetGameText("LC_MENU_TEAMLEAGUE_DEFENSIVE_FORMATION_TITLE")
  end
  uiText.fleetBtnText = GameLoader:GetGameText("LC_MENU_TEAMLEAGUE_TEAM_FLEETS_BTN")
  self:GetFlashObject():InvokeASCallback("_root", "initAsTeamLeague", uiText)
  self:GetFlashObject():InvokeASCallback("_root", "chooseTeam", self.currentMatrixIndex)
  self:UpdateBtnFormation()
  local levelinfo = GameGlobalData:GetData("levelinfo")
  self:UpdatePlayerInfo(levelinfo.level)
  self:UpdateTotalBattleForce(self:ComputeBattleForce())
  self:SelectCommander(1)
  self:AnimationMoveIn()
end
function GameObjectTlcFormation:OnEraseFromGameState(old_parent)
  self:Clear()
  self:UnloadFlashObject()
end
function GameObjectTlcFormation:UpdatePlayerInfo(player_level)
  self:GetFlashObject():InvokeASCallback("_root", "updatePlayerInfo", player_level, GameLoader:GetGameText("LC_MENU_Level"))
end
function GameObjectTlcFormation:ComputeBattleForce()
  DebugOutPutTable(GameGlobalData:GetData("fleetinfo").fleets, "all fleets")
  DebugOutPutTable(self.battle_matrix, "all battle_matrix")
  local force_value = 0
  for _, commander_id in pairs(curMatrixInstance:GetMatrixFleets()) do
    if commander_id > 0 then
      local commander_data = GameGlobalData:GetFleetInfo(commander_id)
      force_value = force_value + commander_data.force
    end
  end
  DebugOut("ComputeBattleForce", force_value)
  return force_value
end
function GameObjectTlcFormation:UpdateTotalBattleForce(total_force)
  self:GetFlashObject():InvokeASCallback("_root", "updateBattleCommandersForce", GameUtils.numberAddComma(total_force))
end
function GameObjectTlcFormation:GetCommanderGrid(commander_id)
  DebugOut("GetCommanderGrid", commander_id)
  for grid_index, grid_data in pairs(self.battle_matrix) do
    if grid_data == commander_id then
      return "battle", grid_index
    end
  end
  for grid_index, grid_data in pairs(self.rest_matrix) do
    if grid_data == commander_id then
      return "rest", grid_index
    end
  end
  return -1, -1
end
function GameObjectTlcFormation:GetCommanderIDWithGrid(grid_type, grid_index)
  local grid_table
  if grid_type == "battle" then
    grid_table = self.battle_matrix
  elseif grid_type == "rest" then
    grid_table = self.rest_matrix
  elseif grid_type == "yours_L" or grid_type == "yours_R" then
    grid_table = self.m_nextTeamMatrix
  elseif grid_type == "rest_list_L" or grid_type == "rest_list_R" or grid_type == "rest_list_L2" or grid_type == "rest_list_R2" then
    grid_table = self.m_nextPlayerRestFleets
  else
    assert(false)
  end
  local id = -1
  DebugOut("GetCommanderIDWithGrid:", grid_type, grid_index)
  DebugTable(grid_table)
  if grid_table[grid_index] ~= nil then
    id = grid_table[grid_index]
  end
  return id
end
function GameObjectTlcFormation:GetCommanderMatrixWithType(matrix_type)
  if matrix_type == "battle" then
    return self.battle_matrix
  elseif matrix_type == "rest" then
    return self.rest_matrix
  else
    assert(false)
  end
end
function GameObjectTlcFormation:CountBattleCommander()
  local count = 0
  for index, data in pairs(self.battle_matrix) do
    if data > 0 then
      count = count + 1
    end
  end
  return count
end
function GameObjectTlcFormation:UpdateCommanderItem(grid_type, grid_index)
  DebugOut("UpdateCommanderItem:", grid_type, ",", grid_index)
  local commander_id = self:GetCommanderIDWithGrid(grid_type, grid_index)
  local tempid = commander_id
  local commanderType = -1
  local commander_identity = -1
  local commander_avatar = -1
  local commander_vessels = -1
  local commander_vesselstype = -1
  local commander_level = 0
  local commander_color = "blue"
  local commander_sex = "unknown"
  local commander_data = GameGlobalData:GetFleetInfo(commander_id)
  if commander_id == 1 then
    commander_id = curMatrixInstance:GetLeaderFleet() or 1
  end
  if commander_data == nil then
    if tempid == 1 then
      commander_data = {}
      commander_data.identity = 1
    end
    DebugOut("error:id_commander")
  else
    commander_level = commander_data.level
  end
  DebugOut("GameObjectTlcFormation:UpdateCommanderItem")
  DebugOut("commander_id: ", commander_id)
  DebugOut("commander_level: ", commander_level)
  if commander_id > 0 and commander_data ~= nil then
    commander_identity = commander_data.identity
    commanderType = GameGlobalData:GetCommanderTypeWithIdentity(commander_identity)
    commander_avatar = GameDataAccessHelper:GetFleetAvatar(commander_id, commander_level)
    commander_vessels = GameDataAccessHelper:GetCommanderVesselsImage(commander_id, commander_level, false)
    commander_vesselstype = GameDataAccessHelper:GetCommanderVesselsType(commander_id, commander_level)
    commander_color = GameDataAccessHelper:GetCommanderColorFrame(commander_id, commander_level)
    commander_sex = GameDataAccessHelper:GetCommanderSex(commander_id)
    if commander_sex == 1 then
      commander_sex = "man"
    elseif commander_sex == 0 then
      commander_sex = "woman"
    else
      commander_sex = "unknown"
    end
  end
  local dataTable = {}
  dataTable[#dataTable + 1] = grid_type
  dataTable[#dataTable + 1] = grid_index
  dataTable[#dataTable + 1] = commanderType
  dataTable[#dataTable + 1] = tempid
  dataTable[#dataTable + 1] = commander_vessels
  dataTable[#dataTable + 1] = commander_avatar
  dataTable[#dataTable + 1] = commander_vesselstype
  dataTable[#dataTable + 1] = commander_color
  dataTable[#dataTable + 1] = commander_sex
  local hasAdjutant = "false"
  if commander_data and commander_data.cur_adjutant and 0 < commander_data.cur_adjutant then
    hasAdjutant = "true"
  end
  dataTable[#dataTable + 1] = hasAdjutant
  local dataString = table.concat(dataTable, ",")
  DebugOut("datastring:", dataString)
  self:GetFlashObject():InvokeASCallback("_root", "updateCommanderItem", dataString)
  if grid_type == "battle" then
    GameObjectTlcFormation:UpdateAdjutantBar()
  end
end
function GameObjectTlcFormation:GetCommanderLocatedInfo()
  local located_type, located_pos
  local located_info = self:GetFlashObject():InvokeASCallback("_root", "getCommanderLocatedInfo")
  if located_info then
    local param_list = LuaUtils:string_split(located_info, ",")
    located_type = param_list[1]
    located_pos = param_list[2]
  end
  return located_type, located_pos
end
function GameObjectTlcFormation:GetCommanderGridPos(grid_type, grid_index)
  local grid_pos
  local pos_info = self:GetFlashObject():InvokeASCallback("_root", "getCommanderGridPos", grid_type, grid_index)
  DebugOut(pos_info)
  if pos_info then
    local param_list = LuaUtils:string_split(pos_info, ",")
    grid_pos = {}
    grid_pos._x = tonumber(param_list[1])
    grid_pos._y = tonumber(param_list[2])
  end
  return grid_pos
end
function GameObjectTlcFormation:OnBeginDragCommander(vessels_dragger)
  local draggedFleedID = vessels_dragger.DraggedFleetID
  local grid_type, grid_index = self:GetCommanderGrid(draggedFleedID)
  if grid_type == "rest" or grid_type == "battle" then
    vessels_dragger.DraggedFleetGridType = grid_type
    vessels_dragger.DraggedFleetGridIndex = grid_index
    local commander_matrix = self:GetCommanderMatrixWithType(grid_type)
    commander_matrix[grid_index] = -1
    self:UpdateCommanderItem(grid_type, grid_index)
  else
    assert(false)
  end
end
function GameObjectTlcFormation:UnExpandSkillSelector(commander_id)
  local active_spell = curMatrixInstance:GetActiviteSpell(commander_id)
  DebugOut("ReExpandSkillSelector:" .. active_spell)
  self:SelectCommanderSkill(commander_id, active_spell)
end
function GameObjectTlcFormation:ExpandSkillSelector(commander_id)
  DebugOut("GameObjectTlcFormation ExpandSkillSelector", commander_id)
  local grid_type, index_commander = self:GetCommanderGrid(commander_id)
  if grid_type == "rest" then
    return
  end
  local flash_obj = self:GetFlashObject()
  local commander_data = GameGlobalData:GetFleetInfo(commander_id)
  local active_spell = GetActiviteSpell:GetActiviteSpell(commander_id)
  local num_max_skill = GameGlobalData:GetNumMaxSkills()
  self:GetFlashObject():InvokeASCallback("_root", "expandSkillSelector", index_commander)
  DebugOut("active_spell: ", commander_data.active_spell)
  for index_skill = 1, num_max_skill do
    local id_skill = commander_data.spells[index_skill]
    DebugOut("id_skill: ", id_skill)
    id_skill = id_skill or -1
    local is_active = id_skill == active_spell
    flash_obj:InvokeASCallback("_root", "updateCommanderSkillItem", index_commander, index_skill, id_skill, false)
    if is_active then
      self:SelectCommanderSkill(commander_id, id_skill)
    end
  end
end
function GameObjectTlcFormation:ShowMainFleetSelector(commander_id)
  local grid_type, index_commander = self:GetCommanderGrid(commander_id)
  if grid_type == "rest" then
    return
  end
  local flash_obj = self:GetFlashObject()
  local commander_data = GameGlobalData:GetFleetInfo(commander_id)
  local num_max_skill = GameGlobalData:GetNumMaxSkills()
  self.curMainFleetIndex = 1
  local leaderdata = GameGlobalData:GetData("leaderlist")
  DebugOut("hello")
  DebugTable(leaderdata)
  local curr_id = curMatrixInstance:GetLeaderFleet()
  self.mainFleetList = leaderdata.options
  DebugOut("leaderlist")
  DebugTable(self.mainFleetList)
  local param = {}
  for k, v in ipairs(self.mainFleetList) do
    local param1 = {}
    local player_main_fleet = GameGlobalData:GetFleetInfo(v)
    if v == curr_id then
      self.curMainFleetIndex = k
    end
    param1.avatar = GameDataAccessHelper:GetFleetAvatar(v, 0)
    table.insert(param, param1)
  end
  DebugOut("index: ", self.curMainFleetIndex)
  flash_obj:InvokeASCallback("_root", "ShowMainFleetSelector", index_commander, param, self.curMainFleetIndex)
  DebugOut("hello:tp")
  self:SelectCommanderNew(self.curMainFleetIndex)
  self:showMainFleetSkillSelector(self.curMainFleetIndex)
end
function GameObjectTlcFormation:setMainFleet()
  local id = self.mainFleetList[self.curMainFleetIndex]
  local param = {}
  if GameObjectTlcFormation.currentFormationType == TEAMLEAGUE_MATRIX_ATTACK then
    param.index = 101
  elseif GameObjectTlcFormation.currentFormationType == TEAMLEAGUE_MATRIX_DEFENCE then
    param.index = 104
  end
  param.id = id
  NetMessageMgr:SendMsg(NetAPIList.change_tlc_leader_req.Code, param, GameObjectTlcFormation.ChangeLeaderCallback, true, nil)
end
function GameObjectTlcFormation.ChangeLeaderCallback(msgType, content)
  DebugOut("hangeLeaderCallback")
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.change_tlc_leader_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    elseif GameObjectTlcFormation:GetFlashObject() then
      local fleet_id = GameObjectTlcFormation.mainFleetList[GameObjectTlcFormation.curMainFleetIndex]
      local grid_type, index_commander = GameObjectTlcFormation:GetCommanderGrid(1)
      GameObjectTlcFormation:changeMainfleetIcon(index_commander, grid_type, fleet_id)
      GameObjectTlcFormation:UpdateTotalBattleForce(GameObjectTlcFormation:ComputeBattleForce())
    end
    return true
  end
  return false
end
function GameObjectTlcFormation:changeMainfleetIcon(grid_index, grid_type, id_i)
  DebugOut("changeMainfleetIcon:", grid_type, ",", grid_index)
  local commander_id = id_i
  local commanderType = -1
  local commander_identity = -1
  local commander_avatar = -1
  local commander_vessels = -1
  local commander_vesselstype = -1
  local commander_level = 0
  local commander_color = "blue"
  local commander_sex = "unknown"
  local commander_data = GameGlobalData:GetFleetInfo(1)
  if commander_data == nil then
    DebugOut("error:id_commander")
  else
    commander_level = commander_data.level
  end
  DebugOut("GameObjectTlcFormation:UpdateCommanderItem")
  DebugOut("commander_id: ", commander_id)
  DebugOut("commander_level: ", commander_level)
  if commander_id > 0 and commander_data ~= nil then
    commander_identity = id_i
    commanderType = GameGlobalData:GetCommanderTypeWithIdentity(commander_identity)
    commander_avatar = GameDataAccessHelper:GetFleetAvatar(commander_id, commander_level)
    commander_vessels = GameDataAccessHelper:GetCommanderVesselsImage(commander_id, commander_level, false)
    commander_vesselstype = GameDataAccessHelper:GetCommanderVesselsType(commander_id, commander_level)
    commander_color = GameDataAccessHelper:GetCommanderColorFrame(commander_id, commander_level)
    commander_sex = GameDataAccessHelper:GetCommanderSex(commander_id)
    if commander_sex == 1 then
      commander_sex = "man"
    elseif commander_sex == 0 then
      commander_sex = "woman"
    else
      commander_sex = "unknown"
    end
  end
  local dataTable = {}
  dataTable[#dataTable + 1] = grid_type
  dataTable[#dataTable + 1] = grid_index
  dataTable[#dataTable + 1] = commanderType
  dataTable[#dataTable + 1] = 1
  dataTable[#dataTable + 1] = commander_vessels
  dataTable[#dataTable + 1] = commander_avatar
  dataTable[#dataTable + 1] = commander_vesselstype
  dataTable[#dataTable + 1] = commander_color
  dataTable[#dataTable + 1] = commander_sex
  local hasAdjutant = "false"
  if commander_data and commander_data.cur_adjutant and 0 < commander_data.cur_adjutant then
    hasAdjutant = "true"
  end
  dataTable[#dataTable + 1] = hasAdjutant
  local dataString = table.concat(dataTable, ",")
  DebugOut("dataString", dataString)
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "updateCommanderItem", dataString)
  end
  GameObjectTlcFormation:SelectCommander(1)
  if grid_type == "battle" then
    GameObjectTlcFormation:UpdateAdjutantBar()
  end
end
function GameObjectTlcFormation:showMainFleetSkillSelector(fleet_index)
  local iteminfo = self.mainFleetList[fleet_index]
  local grid_type, index_commander = self:GetCommanderGrid(1)
  if grid_type == "rest" then
    return
  end
  local flash_obj = self:GetFlashObject()
  DebugOut("GameObjectTlcFormation:showMainFleetSkillSelector:", fleet_index, iteminfo)
  if iteminfo == 1 then
    local commander_data = GameGlobalData:GetFleetInfo(iteminfo)
    DebugOut("fleetTable")
    DebugTable(GameGlobalData.GlobalData.fleetinfo.fleets)
    DebugOut("why nil?")
    DebugTable(commander_data)
    local active_spell = curMatrixInstance:GetActiviteSpell(iteminfo)
    local num_max_skill = GameGlobalData:GetNumMaxSkills()
    flash_obj:InvokeASCallback("_root", "expandSkillSelectorNew", fleet_index, iteminfo, "skill_name", "skill_desc")
    DebugOut("active_spell: ", commander_data.active_spell)
    local leaderlist = GameGlobalData:GetData("leaderlist")
    local curSpells = leaderlist.active_spells or {}
    for index_skill = 1, num_max_skill do
      local id_skill = curSpells[index_skill]
      DebugOut("id_skill: ", id_skill)
      id_skill = id_skill or -1
      local is_active = id_skill == active_spell
      flash_obj:InvokeASCallback("_root", "updateCommanderSkillItem", index_commander, index_skill, id_skill, false)
      if is_active then
        self:SelectCommanderSkill(iteminfo, id_skill)
      end
    end
  else
    local datatable = {}
    datatable[#datatable + 1] = GameDataAccessHelper:GetSkillNameFromHeroID(iteminfo)
    datatable[#datatable + 1] = GameDataAccessHelper:GetSkillDescFromHeroID(iteminfo)
    local skill_id = GameDataAccessHelper:GetSkillIdFromHeroId(iteminfo)
    local rangetype = GameDataAccessHelper:GetSkillRangeType(skill_id)
    datatable[#datatable + 1] = "TYPE_" .. tostring(rangetype)
    datatable[#datatable + 1] = GameLoader:GetGameText("LC_MENU_SKILL_RANGE_TYPE_" .. tostring(rangetype))
    local datastring = table.concat(datatable, "\001")
    DebugOut("datastring", datastring)
    self:GetFlashObject():InvokeASCallback("_root", "shrinkSkillSelectorNew")
    self:GetFlashObject():InvokeASCallback("_root", "selectCommanderSkillNew", fleet_index, datastring, index_commander)
  end
end
function GameObjectTlcFormation:SelectCommanderNew(fleet_index)
  local commander_info = GameGlobalData:GetFleetInfo(1)
  local commander_level = 0
  local leaderlist = GameGlobalData:GetData("leaderlist")
  if commander_info then
    commander_level = commander_info.level
  end
  local commander_force = GameGlobalData:GetFleetInfo(1).force or 0
  local commander_name = ""
  local commander_avatar = ""
  local commander_vessels = ""
  local commander_color = ""
  local commander_vesselstype = ""
  local forceContext = GameLoader:GetGameText("LC_MENU_ARENA_CAPTION_FORCE")
  local basic_info = {}
  local player_info = GameGlobalData:GetData("userinfo")
  commander_name = GameUtils:GetUserDisplayName(player_info.name)
  commander_name = GameDataAccessHelper:CreateLevelDisplayName(commander_name, commander_level)
  local commanderType = -1
  commander_avatar = GameDataAccessHelper:GetFleetAvatar(self.mainFleetList[fleet_index], commander_level)
  commander_vesselstype = GameDataAccessHelper:GetCommanderVesselsType(self.mainFleetList[fleet_index], commander_level)
  commander_vessels = GameDataAccessHelper:GetCommanderVesselsImage(self.mainFleetList[fleet_index], commander_level, false)
  commander_color = GameDataAccessHelper:GetCommanderColorFrame(self.mainFleetList[fleet_index], commander_level)
  basic_info = GameDataAccessHelper:GetCommanderBasicInfo(self.mainFleetList[fleet_index], commander_level)
  local skill = basic_info.SPELL_ID
  local skillIconFram = "TYPE_" .. GameDataAccessHelper:GetSkillRangeType(skill)
  if self.mainFleetList[fleet_index] == 1 then
    local commander_data = GameGlobalData:GetFleetInfo(1)
    local num_max_skill = GameGlobalData:GetNumMaxSkills()
    for index_skill = 1, num_max_skill do
      local id_skill = commander_data.spells[index_skill]
      id_skill = id_skill or -1
      local is_active = id_skill == commander_data.active_spell
      if is_active then
        skillIconFram = "TYPE_" .. GameDataAccessHelper:GetSkillRangeType(id_skill)
      end
    end
  end
  local ability = GameDataAccessHelper:GetCommanderAbility(self.mainFleetList[fleet_index], commander_level)
  local sex = GameDataAccessHelper:GetCommanderSex(self.mainFleetList[fleet_index])
  local fleetRank = GameUtils:GetFleetRankFrame(ability.Rank, GameObjectTlcFormation.DownloadRankImageCallback)
  commanderType = GameGlobalData:GetCommanderTypeWithIdentity(self.mainFleetList[fleet_index])
  if sex == 1 then
    sex = "man"
  elseif sex == 0 then
    sex = "woman"
  else
    sex = "unknown"
  end
  local grid_type, grid_index = GameObjectTlcFormation:GetCommanderGrid(1)
  local dataTable = {}
  dataTable[#dataTable + 1] = grid_type
  dataTable[#dataTable + 1] = grid_index
  dataTable[#dataTable + 1] = commanderType
  dataTable[#dataTable + 1] = 1
  dataTable[#dataTable + 1] = commander_vessels
  dataTable[#dataTable + 1] = commander_avatar
  dataTable[#dataTable + 1] = commander_vesselstype
  dataTable[#dataTable + 1] = commander_color
  dataTable[#dataTable + 1] = sex
  local hasAdjutant = "false"
  if commander_info and commander_info.cur_adjutant and 0 < commander_info.cur_adjutant then
    hasAdjutant = "true"
  end
  dataTable[#dataTable + 1] = hasAdjutant
  local dataString = table.concat(dataTable, ",")
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "selectCommanderItem", matrix_type, pos_index, commander_name, commander_identity, commander_avatar, commander_vesselstype, GameUtils.numberAddComma(commander_force), commander_color, forceContext, skillIconFram, fleetRank, sex, GameLoader:GetGameText("LC_MENU_Level"))
    self:GetFlashObject():InvokeASCallback("_root", "updateCommanderItem", dataString)
  end
end
function GameObjectTlcFormation:SelectCommander(commander_id)
  DebugOut("SelectCommander", commander_id)
  GameObjectTlcFormation.curFleetId = commander_id
  local matrix_type, pos_index = self:GetCommanderGrid(commander_id)
  local commander_info = GameGlobalData:GetFleetInfo(commander_id)
  local commander_level = 0
  local leaderlist = GameGlobalData:GetData("leaderlist")
  if commander_info then
    commander_level = commander_info.level
  end
  local commander_name = ""
  local commander_avatar = ""
  if commander_id == 1 then
    local player_info = GameGlobalData:GetData("userinfo")
    commander_name = GameUtils:GetUserDisplayName(player_info.name)
    commander_name = GameDataAccessHelper:CreateLevelDisplayName(commander_name, commander_level)
    commander_avatar = GameUtils:GetPlayerAvatar(nil, commander_level)
    if curMatrixInstance:GetLeaderFleet() ~= 1 then
      commander_avatar = GameDataAccessHelper:GetFleetAvatar(curMatrixInstance:GetLeaderFleet(), commander_level)
    end
  else
    commander_name = GameLoader:GetGameText("LC_NPC_" .. GameDataAccessHelper:GetCommanderName(commander_id, commander_level))
    commander_name = GameDataAccessHelper:CreateLevelDisplayName(commander_name, commander_level)
    commander_avatar = GameDataAccessHelper:GetFleetAvatar(commander_id, commander_level)
  end
  local commander_vesselstype = GameDataAccessHelper:GetCommanderVesselsType(commander_id, commander_level)
  local commander_force = commander_info.force
  local commander_identity = commander_info.identity
  local commander_color = GameDataAccessHelper:GetCommanderColorFrame(commander_id, commander_level)
  local forceContext = GameLoader:GetGameText("LC_MENU_ARENA_CAPTION_FORCE")
  local basic_info = GameDataAccessHelper:GetCommanderBasicInfo(commander_id, commander_level)
  if commander_id == 1 and curMatrixInstance:GetLeaderFleet() ~= 1 then
    commander_vesselstype = GameDataAccessHelper:GetCommanderVesselsType(curMatrixInstance:GetLeaderFleet(), commander_level)
    commander_color = GameDataAccessHelper:GetCommanderColorFrame(curMatrixInstance:GetLeaderFleet(), commander_level)
    basic_info = GameDataAccessHelper:GetCommanderBasicInfo(curMatrixInstance:GetLeaderFleet(), commander_level)
  end
  local skill = basic_info.SPELL_ID
  local skillIconFram = "TYPE_" .. GameDataAccessHelper:GetSkillRangeType(skill)
  if commander_id == 1 and curMatrixInstance:GetLeaderFleet() == 1 then
    local commander_data = GameGlobalData:GetFleetInfo(commander_id)
    local num_max_skill = GameGlobalData:GetNumMaxSkills()
    for index_skill = 1, num_max_skill do
      local id_skill = commander_data.spells[index_skill]
      DebugOut("id_skill: ", id_skill)
      id_skill = id_skill or -1
      local is_active = id_skill == commander_data.active_spell
      if is_active then
        skillIconFram = "TYPE_" .. GameDataAccessHelper:GetSkillRangeType(id_skill)
      end
    end
  end
  local ability = GameDataAccessHelper:GetCommanderAbility(commander_id, commander_level)
  local sex = GameDataAccessHelper:GetCommanderSex(commander_id)
  if commander_id == 1 and curMatrixInstance:GetLeaderFleet() ~= 1 then
    ability = GameDataAccessHelper:GetCommanderAbility(curMatrixInstance:GetLeaderFleet(), 0)
    sex = GameDataAccessHelper:GetCommanderSex(curMatrixInstance:GetLeaderFleet())
  end
  local fleetRank = GameUtils:GetFleetRankFrame(ability.Rank, GameObjectTlcFormation.DownloadRankImageCallback)
  if sex == 1 then
    sex = "man"
  elseif sex == 0 then
    sex = "woman"
  else
    sex = "unknown"
  end
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "selectCommanderItem", matrix_type, pos_index, commander_name, commander_identity, commander_avatar, commander_vesselstype, GameUtils.numberAddComma(commander_force), commander_color, forceContext, skillIconFram, fleetRank, sex, GameLoader:GetGameText("LC_MENU_Level"))
  end
  self.selected_commander = commander_id
end
function GameObjectTlcFormation.DownloadRankImageCallback(extinfo)
  if GameObjectTlcFormation:GetFlashObject() then
    GameObjectTlcFormation:GetFlashObject():InvokeASCallback("_root", "updateRankImage", extinfo.rank_id)
  end
end
function GameObjectTlcFormation:SelectCommanderSkill(commander_id, skill_id)
  if commander_id ~= -1 and skill_id ~= -1 then
    curMatrixInstance:SetCurrentSkill(commander_id, skill_id)
    local commander_data = GameGlobalData:GetFleetInfo(commander_id)
    commander_data.active_spell = skill_id
    local leaderlist = GameGlobalData:GetData("leaderlist")
    local curSpells = leaderlist.active_spells
    for indexSkill = 1, GameGlobalData:GetNumMaxSkills() do
      if curSpells[indexSkill] == skill_id then
        local datatable = {}
        datatable[#datatable + 1] = GameDataAccessHelper:GetSkillNameText(skill_id)
        datatable[#datatable + 1] = GameDataAccessHelper:GetSkillDesc(skill_id)
        local rangetype = GameDataAccessHelper:GetSkillRangeType(skill_id)
        datatable[#datatable + 1] = "TYPE_" .. tostring(rangetype)
        datatable[#datatable + 1] = GameLoader:GetGameText("LC_MENU_SKILL_RANGE_TYPE_" .. tostring(rangetype))
        local datastring = table.concat(datatable, "\001")
        self:GetFlashObject():InvokeASCallback("_root", "selectCommanderSkill", indexSkill, datastring)
        break
      end
    end
  else
    local leaderlist = GameGlobalData:GetData("leaderlist")
    if self.mainFleetList[self.curMainFleetIndex] ~= 1 then
      self:GetFlashObject():InvokeASCallback("_root", "shrinkSkillSelectorNew")
    else
      self:GetFlashObject():InvokeASCallback("_root", "shrinkSkillSelector")
    end
  end
end
function GameObjectTlcFormation:OnLocatedCommander(object_dragger)
  local located_type = object_dragger.DragToGridType
  local located_index = object_dragger.DragToGridIndex
  local located_src_commander = self:GetCommanderIDWithGrid(located_type, located_index)
  local commander_matrix = self:GetCommanderMatrixWithType(located_type)
  commander_matrix[located_index] = object_dragger.DraggedFleetID
  self:UpdateCommanderItem(located_type, located_index)
  if located_type == "battle" and object_dragger.DraggedFleetGridType == "battle" then
    curMatrixInstance:SwapMatrix(self.currentMatrixIndex, object_dragger.DraggedFleetGridIndex, located_index)
  elseif located_type == "battle" then
    curMatrixInstance:SetMatrix(self.currentMatrixIndex, located_index, object_dragger.DraggedFleetID)
  elseif object_dragger.DraggedFleetGridType == "battle" then
    curMatrixInstance:SetMatrix(self.currentMatrixIndex, object_dragger.DraggedFleetGridIndex, located_src_commander)
  end
  if located_src_commander and located_src_commander > 0 then
    local src_matrix = self:GetCommanderMatrixWithType(object_dragger.DraggedFleetGridType)
    src_matrix[object_dragger.DraggedFleetGridIndex] = located_src_commander
    self:UpdateCommanderItem(object_dragger.DraggedFleetGridType, object_dragger.DraggedFleetGridIndex)
  end
  self:GetFlashObject():InvokeASCallback("_root", "onCommanderDragOver")
  self.selected_commander = object_dragger.DraggedFleetID
  self:SelectCommander(self.selected_commander)
  object_dragger.DraggedFleetID = nil
  object_dragger.DragToGridType = nil
  object_dragger.DragToGridIndex = nil
  object_dragger.DraggedFleetGridType = nil
  object_dragger.DraggedFleetGridIndex = nil
  self:UpdateTotalBattleForce(self:ComputeBattleForce())
  self:UpdateBtnFormation()
end
function GameObjectTlcFormation:OnFSCommand(cmd, arg)
  GameTextEdit:fscommand(self:GetFlashObject(), cmd, arg)
  DebugOut("OnFSCommand", cmd, arg)
  if cmd == "SaveMatrixName" then
  elseif cmd == "show_help" then
    GameUIWDStuff:Show(GameUIWDStuff.menuType.menu_type_help)
    GameUIWDStuff:SetHelpText(GameLoader:GetGameText("LC_MENU_SAVE_FORMATION_HELP"))
  elseif cmd == "moveout_over" then
    if GameStateTlcFormation.basePrveState then
      GameStateTlcFormation.m_prevState = GameStateTlcFormation.basePrveState
      GameStateTlcFormation.basePrveState = nil
    end
    GameStateManager:SetCurrentGameState(GameStateTlcFormation.m_prevState)
  elseif cmd == "begin_drag" then
    DebugOut("begin_drag:" .. arg)
    local param_list = LuaUtils:string_split(arg, ",")
    local commander_id = tonumber(param_list[1])
    local xpos_init = tonumber(param_list[2])
    local ypos_init = tonumber(param_list[3])
    local xpos_mouse = tonumber(param_list[4])
    local ypos_mouse = tonumber(param_list[5])
    local grid_type, grid_index = self:GetCommanderGrid(commander_id)
    if grid_type == "battle" or grid_type == "rest" then
      local leaderlist = GameGlobalData:GetData("leaderlist")
      if commander_id == 1 and curMatrixInstance:GetLeaderFleet() ~= 1 then
        GameStateTlcFormation:BeginDragCommanderVessels(1, xpos_init, ypos_init, xpos_mouse, ypos_mouse, curMatrixInstance:GetLeaderFleet())
      else
        GameStateTlcFormation:BeginDragCommanderVessels(commander_id, xpos_init, ypos_init, xpos_mouse, ypos_mouse)
      end
    else
      assert(false)
    end
  elseif cmd == "select_commander" then
    local commander_id = tonumber(arg)
    self:SelectCommander(commander_id)
    if commander_id == 1 then
      self:ShowMainFleetSelector(commander_id)
    end
  elseif cmd == "select_commander_skill" then
    local commander_id, skill_id = unpack(LuaUtils:string_split(arg, ","))
    commander_id = tonumber(commander_id)
    skill_id = tonumber(skill_id)
    GameObjectTlcFormation:SelectCommanderSkill(commander_id, skill_id)
  elseif cmd == "clicked_btn_close" then
    if GameObjectTlcFormation:CheckAdjutantBeforeSaveCurrentMatrix() then
      curMatrixInstance:SaveMatrixChanged()
      GameObjectTlcFormation:AnimationMoveOut()
    else
      DebugOut("GameObjectTlcFormation:CheckAdjutantBeforeSaveCurrentMatrix() false")
    end
  elseif cmd == "arrow_L" then
    self:trySaveAndExchangeToMatrix(self.currentMatrixIndex, true)
  elseif cmd == "arrow_R" then
    self:trySaveAndExchangeToMatrix(self.currentMatrixIndex, false)
  elseif cmd == "teamMatrixButtonClicked" then
    DebugOut("currentMatrix =1=", self.currentMatrixIndex)
    if not GameObjectTlcFormation:CheckAdjutantBeforeSaveCurrentMatrix() then
      self:GetFlashObject():InvokeASCallback("_root", "lua2fs_clickTeamIndexButton", self.currentMatrixIndex)
      return
    end
    local index = tonumber(arg)
    DebugOut("index", index)
    DebugOut("currentMatrix", self.currentMatrixIndex)
    local isMoveLeft = true
    if index == self.currentMatrixIndex then
      self:GetFlashObject():InvokeASCallback("_root", "lua2fs_clickTeamIndexButton", self.currentMatrixIndex)
      return
    elseif index < self.currentMatrixIndex then
      isMoveLeft = true
    else
      isMoveLeft = false
    end
    GameObjectTlcFormation.mCurFreeMatrixSubIdx = 0
    self:loadNextMatrixAndRestFleets(index)
    self:setNextMatrixAndRestFleetsUI(isMoveLeft)
    self:GetFlashObject():InvokeASCallback("_root", "closeMainFleetSelector")
    if self.mainFleetList[self.curMainFleetIndex] ~= 1 then
      self:GetFlashObject():InvokeASCallback("_root", "shrinkSkillSelectorNew")
    else
      self:GetFlashObject():InvokeASCallback("_root", "shrinkSkillSelector")
    end
    self:GetFlashObject():InvokeASCallback("_root", "lua2fs_clickTeamIndexButton", index)
    self.currentMatrixIndex = index
    local leaderdata = GameGlobalData:GetData("leaderlist")
    local curr_id = curMatrixInstance:GetLeaderFleet()
    for k, v in ipairs(self.mainFleetList) do
      if v == curr_id then
        self.curMainFleetIndex = k
        break
      end
    end
    curMatrixInstance:SaveMatrixChanged()
    curMatrixInstance:SwitchTeamReq(index)
  elseif cmd == "move_prevRest_over" or cmd == "move_prev_over" or cmd == "move_nextRest_over" or cmd == "move_next_over" then
    self.battle_matrix = LuaUtils:table_rcopy(self.m_nextTeamMatrix)
    self.rest_matrix = LuaUtils:table_rcopy(self.m_nextPlayerRestFleets)
    for i = 1, 9 do
      self:UpdateCommanderItem("battle", i)
      self:UpdateCommanderItem("rest", i)
    end
    self:SelectCommander(1)
    GameObjectTlcFormation:CheckAdjutantBeforeSaveCurrentMatrix()
  elseif cmd == "rest_list_move_prev_over" or cmd == "rest_list_move_next_over" then
    self.rest_matrix = LuaUtils:table_rcopy(self.m_nextPlayerRestFleets)
    for i = 1, 9 do
      local commander_id = self.rest_matrix[i]
      if commander_id == nil then
        commander_id = -1
      end
      self:UpdateCommanderItem("rest", i)
    end
    self:SelectCommander(1)
  elseif cmd == "adjutantClicked" then
    if GameObjectTlcFormation:CheckAdjutantBeforeSaveCurrentMatrix() then
      local function saveSuccessCallback()
        local function exitAdjutantUICallback()
          GameObjectTlcFormation:RefreshBattleAndRestGrid()
          GameObjectTlcFormation:UpdateAdjutantBar()
          GameObjectTlcFormation:CheckAdjutantBeforeSaveCurrentMatrix()
        end
        GameUIAdjutant.mFleetIdBeforeEnter = 1
        GameUIAdjutant:EnterAdjutantUI(exitAdjutantUICallback, GameObjectTlcFormation.currentFormationType)
      end
      curMatrixInstance:SaveMatrixChanged(saveSuccessCallback)
    else
      DebugOut("GameObjectTlcFormation:CheckAdjutantBeforeSaveCurrentMatrix() false")
    end
  elseif cmd == "showDetailInfo" then
    local fleet = GameGlobalData:GetFleetInfo(self.curFleetId)
    if self.curFleetId == 1 then
      local leaderlist = GameGlobalData:GetData("leaderlist")
      DebugOut("fleetinfo")
      DebugTable(fleet)
      ItemBox:ShowCommanderDetail2(self.mainFleetList[self.curMainFleetIndex], fleet, 1)
    else
      ItemBox:ShowCommanderDetail2(fleet.identity, fleet, 1)
    end
  elseif cmd == "choose_Item" then
    self.curMainFleetIndex = tonumber(arg)
    self:SelectCommanderNew(tonumber(arg))
    self:showMainFleetSkillSelector(tonumber(arg))
  elseif cmd == "setMainFleet" then
    self:setMainFleet()
  elseif cmd == "enterFleets" then
    curMatrixInstance:SaveMatrixChanged()
    local GameStateEquipEnhance = GameStateManager.GameStateEquipEnhance
    if GameStateTlcFormation.basePrveState == nil then
      GameStateTlcFormation.basePrveState = GameStateTlcFormation.m_prevState
    end
    GameStateEquipEnhance:EnterTeamLeagueEquipEnhance(GameObjectTlcFormation.currentFormationType, self.currentMatrixIndex)
  end
end
function GameObjectTlcFormation:AnimationMoveIn()
  self:GetFlashObject():InvokeASCallback("_root", "animationMoveIn")
end
function GameObjectTlcFormation:AnimationMoveOut()
  self:GetFlashObject():InvokeASCallback("_root", "animationMoveOut")
end
function GameObjectTlcFormation:GetCommanderLocatedInfo()
  local located_type, located_index
  local located_info = self:GetFlashObject():InvokeASCallback("_root", "getCommanderLocatedInfo")
  if located_info then
    local param_list = LuaUtils:string_split(located_info, ",")
    located_type = param_list[1]
    located_index = tonumber(param_list[2])
  end
  return located_type, located_index
end
function GameObjectTlcFormation:GetCommanderGridPos(grid_type, grid_index)
  local result_pos
  local pos_info = self:GetFlashObject():InvokeASCallback("_root", "getCommanderGridPos", grid_type, grid_index)
  if pos_info then
    local param_list = LuaUtils:string_split(pos_info, ",")
    local xpos = tonumber(param_list[1])
    local ypos = tonumber(param_list[2])
    result_pos = {_x = xpos, _y = ypos}
  end
  return result_pos
end
local frameOptimize = 0
local frameOptimizeDT = 0
function GameObjectTlcFormation:Update(dt)
  local flash_obj = self:GetFlashObject()
  flash_obj:Update(dt)
  flash_obj:InvokeASCallback("_root", "onUpdateFrame", dt)
end
if AutoUpdate.isAndroidDevice then
  function GameObjectTlcFormation.OnAndroidBack()
    GameObjectTlcFormation:GetFlashObject():InvokeASCallback("_root", "mainMoveOut")
  end
end
function GameObjectTlcFormation:setNextMatrixAndRestFleetsUI(isLeft, isSubRest)
  DebugOut("setNextMatrixAndRestFleetsUI", isLeft)
  DebugTable(self.m_nextTeamMatrix)
  DebugTable(self.m_nextPlayerRestFleets)
  if isLeft then
    DebugOut("isLeft")
    for i = 1, 9 do
      self:UpdateCommanderItem("yours_L", i)
    end
    local listType = "rest_list_L"
    if isSubRest then
      listType = "rest_list_L2"
    end
    for i = 1, 9 do
      self:UpdateCommanderItem(listType, i)
    end
  else
    DebugOut("isRight")
    for i = 1, 9 do
      self:UpdateCommanderItem("yours_R", i)
    end
    local listType = "rest_list_R"
    if isSubRest then
      listType = "rest_list_R2"
    end
    for i = 1, 9 do
      self:UpdateCommanderItem(listType, i)
    end
  end
  self:GetFlashObject():InvokeASCallback("_root", "HideAllSelectRound")
  self:UpdateBtnFormation()
end
function GameObjectTlcFormation:loadNextMatrixAndRestFleets(index, subIndex)
  DebugOut("GameObjectTlcFormation loadNextMatrixAndRestFleets", index)
  self.currentMatrixIndex = index
  self.m_nextTeamMatrix = {}
  self.m_nextPlayerRestFleets = {}
  for i = 1, 9 do
    self.m_nextTeamMatrix[i] = 0
  end
  local nextMatrix = curMatrixInstance:GetMatrixByIndex(index).cells
  for _, v in pairs(nextMatrix) do
    self.m_nextTeamMatrix[v.cell_id] = v.fleet_identity
  end
  self.m_nextPlayerRestFleets = curMatrixInstance:GetFreeFleets()
  if subIndex then
    local pageCnt = math.ceil((#self.m_nextPlayerRestFleets + 1) / 9)
    DebugOut("pageCnt " .. tostring(pageCnt) .. tostring(GameObjectTlcFormation.mCurFreeMatrixSubIdx))
    GameObjectTlcFormation.mCurFreeMatrixSubIdx = (GameObjectTlcFormation.mCurFreeMatrixSubIdx + subIndex + pageCnt) % pageCnt
    local subRestFleets = {}
    for k = 1, 9 do
      if self.m_nextPlayerRestFleets[k + 9 * GameObjectTlcFormation.mCurFreeMatrixSubIdx] then
        subRestFleets[k] = self.m_nextPlayerRestFleets[k + 9 * GameObjectTlcFormation.mCurFreeMatrixSubIdx]
      else
        subRestFleets[k] = -1
      end
    end
    DebugOut("subIndex = " .. tostring(subIndex) .. " mCurFreeMatrixSubIdx " .. tostring(GameObjectTlcFormation.mCurFreeMatrixSubIdx))
    DebugOut("before m_nextPlayerRestFleets")
    DebugTable(self.m_nextPlayerRestFleets)
    self.m_nextPlayerRestFleets = subRestFleets
  end
end
function GameObjectTlcFormation:trySaveAndExchangeToMatrix(index, isleft)
  DebugOut("trySaveAndExchangeToMatrix", index)
  curMatrixInstance:SaveMatrixChanged()
  local subIndex = 1
  if isleft then
    subIndex = -1
  end
  self:loadNextMatrixAndRestFleets(index, subIndex)
  self:setNextMatrixAndRestFleetsUI(isleft, true)
  if isleft then
    self:GetFlashObject():InvokeASCallback("_root", "RestFormationMoveToPrev")
  else
    self:GetFlashObject():InvokeASCallback("_root", "RestFormationMoveToNext")
  end
end
function GameObjectTlcFormation:GetMajorIdByAdjutantId(adjutantId)
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  for k, v in pairs(fleets) do
    if v.cur_adjutant == adjutantId then
      return v.identity
    end
  end
  return -1
end
function GameObjectTlcFormation:GetAdjutantCount(matrix)
  local cnt = 0
  for k, v in pairs(matrix) do
    local fleet = GameGlobalData:GetFleetInfo(v)
    if fleet and 0 < fleet.cur_adjutant then
      cnt = cnt + 1
    end
  end
  return cnt
end
function GameObjectTlcFormation:IsFleetOnMatrix(matrix, fleetId)
  for k, v in pairs(matrix) do
    local fleet = GameGlobalData:GetFleetInfo(v)
    if fleet and fleet.identity == fleetId then
      return true
    end
  end
  return false
end
function GameObjectTlcFormation:CheckAdjutant(src, des, draggedFleedId, fromIndex, toIndex)
  DebugOutPutTable(self.battle_matrix, "battle_matrix")
  DebugOutPutTable(self.rest_matrix, "rest_matrix")
  local adjutantMaxCnt = GameGlobalData:GetData("adjutant_max_count")
  DebugOut("adjutantMaxCnt " .. tostring(adjutantMaxCnt))
  local srcMatrix
  if src == "battle" then
    srcMatrix = self.battle_matrix
  else
    srcMatrix = self.rest_matrix
  end
  local destMatrix
  if des == "battle" then
    destMatrix = self.battle_matrix
  else
    destMatrix = self.rest_matrix
  end
  local destFleetId = destMatrix[toIndex]
  if nil == destFleetId then
    destFleetId = -1
  end
  local newBattleMatrix = {}
  for k, v in pairs(self.battle_matrix) do
    newBattleMatrix[k] = v
  end
  if src == des then
    return 0
  elseif src == "battle" and des == "rest" and destFleetId == -1 then
    return 0
  elseif destFleetId > 0 then
    local addFleetId = -1
    if src == "battle" and des == "rest" then
      addFleetId = destFleetId
      newBattleMatrix[fromIndex] = destFleetId
    else
      addFleetId = draggedFleedId
      newBattleMatrix[toIndex] = draggedFleedId
    end
    if adjutantMaxCnt < GameObjectTlcFormation:GetAdjutantCount(newBattleMatrix) then
      return -1
    else
      local majorId = GameObjectTlcFormation:GetMajorIdByAdjutantId(addFleetId)
      if majorId > 0 then
        return 1, majorId, addFleetId
      else
        local fleetInfo = GameGlobalData:GetFleetInfo(addFleetId)
        if GameObjectTlcFormation:IsFleetOnMatrix(newBattleMatrix, fleetInfo.cur_adjutant) then
          return 1, addFleetId, fleetInfo.cur_adjutant
        else
          return 0
        end
      end
    end
  elseif src == "rest" and des == "battle" then
    newBattleMatrix[toIndex] = draggedFleedId
    if adjutantMaxCnt < GameObjectTlcFormation:GetAdjutantCount(newBattleMatrix) then
      return -1
    else
      local majorId = GameObjectTlcFormation:GetMajorIdByAdjutantId(draggedFleedId)
      if majorId > 0 then
        return 1, majorId, draggedFleedId
      else
        local fleetInfo = GameGlobalData:GetFleetInfo(draggedFleedId)
        if GameObjectTlcFormation:IsFleetOnMatrix(newBattleMatrix, fleetInfo.cur_adjutant) then
          return 1, draggedFleedId, fleetInfo.cur_adjutant
        else
          return 0
        end
      end
    end
  end
end
function GameObjectTlcFormation:CheckAdjutantBeforeSaveCurrentMatrix(onlyForResult)
  local adjutantMaxCnt = GameGlobalData:GetData("adjutant_max_count")
  if adjutantMaxCnt < GameObjectTlcFormation:GetAdjutantCount(self.battle_matrix) then
    DebugOut("Out of max count adjutant can battle.")
    GameTip:Show(GameLoader:GetGameText("LC_MENU_BRIDGE_INFO_4"), 3000)
    return false
  end
  for k, v in pairs(self.battle_matrix) do
    if v > 0 then
      do
        local majorId = GameObjectTlcFormation:GetMajorIdByAdjutantId(v)
        if majorId > 0 then
          if nil == onlyForResult or not onlyForResult then
            do
              local tip = GameLoader:GetGameText("LC_MENU_BRIDGE_INFO_3")
              DebugOut("tip " .. tip)
              local adjutantName = GameDataAccessHelper:GetFleetLevelDisplayName(v)
              local majorName = GameDataAccessHelper:GetFleetLevelDisplayName(majorId)
              tip = string.gsub(tip, "<npc2_name>", adjutantName)
              tip = string.gsub(tip, "<npc1_name>", majorName)
              local function releaseAdjutantResultCallback(success)
                DebugOut("releaseAdjutantResultCallback " .. tostring(success))
                if success then
                  GameObjectTlcFormation:UpdateAdjutantBar()
                  GameObjectTlcFormation:UpdateBattleCommanderByFleetId(majorId)
                  GameObjectTlcFormation:CheckAdjutantBeforeSaveCurrentMatrix()
                else
                end
              end
              local function okCallback()
                GameStateTlcFormation:RequestReleaseAdjutant(majorId, releaseAdjutantResultCallback)
              end
              local cancelCallback = function()
              end
              GameUIGlobalScreen:ShowMessageBox(2, "", tip, okCallback, cancelCallback)
            end
          end
          return false
        end
      end
    end
  end
  return true
end
function GameObjectTlcFormation:UpdateAdjutantBar()
  local flashObj = self:GetFlashObject()
  if flashObj then
    local adjutantCnt = 0
    local adjutantAvatarFrs = ""
    local adjutantbgColor = ""
    for k, v in pairs(self.battle_matrix) do
      local commander_data2 = GameGlobalData:GetFleetInfo(v)
      if v > 0 and commander_data2 ~= nil and commander_data2.cur_adjutant and commander_data2.cur_adjutant > 1 then
        adjutantAvatarFrs = adjutantAvatarFrs .. GameDataAccessHelper:GetFleetAvatar(commander_data2.cur_adjutant) .. "\001"
        adjutantbgColor = adjutantbgColor .. GameDataAccessHelper:GetCommanderColorFrame(commander_data2.cur_adjutant, commander_data2.adjutant_level) .. "\001"
        local ability = GameDataAccessHelper:GetCommanderAbility(commander_data2.cur_adjutant)
        adjutantCnt = adjutantCnt + 1
      end
    end
    local adjutantUnlockCnt = GameGlobalData:GetData("adjutant_max_count")
    DebugOut("SetAdjutantBar " .. tostring(adjutantCnt) .. " " .. adjutantAvatarFrs .. "adjutantbgColor = " .. adjutantbgColor)
    flashObj:InvokeASCallback("_root", "SetAdjutantBar", adjutantUnlockCnt, adjutantCnt, adjutantAvatarFrs, adjutantbgColor)
  end
end
function GameObjectTlcFormation:RefreshBattleAndRestGrid()
  for i = 1, 9 do
    self:UpdateCommanderItem("battle", i)
    self:UpdateCommanderItem("rest", i)
  end
end
function GameObjectTlcFormation:UpdateBattleCommanderByFleetId(fleetId)
  for k, v in pairs(self.battle_matrix) do
    if fleetId == v then
      GameObjectTlcFormation:UpdateCommanderItem("battle", k)
      break
    end
  end
end
function GameObjectTlcFormation:UpdateBtnFormation()
  local bEmptyTeam = curMatrixInstance:IsEmptyTeam(self.currentMatrixIndex)
  self:GetFlashObject():InvokeASCallback("_root", "UpdateBtnFormation", bEmptyTeam)
end
