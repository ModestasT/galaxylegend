local WVEGameManeger = LuaObjectManager:GetLuaObject("WVEGameManeger")
WVEPlayerManager = luaClass(nil)
function WVEPlayerManager:ctor(wveGameManager)
  self.mGameManger = wveGameManager
  self.mPlayerList = {}
  self.mEnemyList = {}
  self.mPlayerMyself = WVEPlayerMySelf.new()
  self.mPlayerMyself:SetUpManager(self)
  self.mShowEnemyInfo = false
  self.mEnemyInfoDT = 0
end
function WVEPlayerManager:UpdatePlayerPos()
end
function WVEPlayerManager:RemovePlayer(playerID)
  if self.mPlayerList[playerID] then
    self.mPlayerList[playerID] = nil
    local flashObj = self.mGameManger:GetFlashObject()
    if not flashObj then
      return
    end
    flashObj:InvokeASCallback("_root", "WVEMAP_KillPlayer", playerID)
  end
end
function WVEPlayerManager:AddPlayer(id, startPoint, endPoint, speed, movedDistance, playerStatus)
  local player = WVEPlayer.new(id, startPoint, endPoint, speed, movedDistance, playerStatus)
  self.mPlayerList[id] = player
  return player
end
function WVEPlayerManager:RemoveEnemy(enemyID)
  if self.mEnemyList[enemyID] then
    self.mEnemyList[enemyID] = nil
    local flashObj = self.mGameManger:GetFlashObject()
    if not flashObj then
      return
    end
    flashObj:InvokeASCallback("_root", "WVEMAP_KillPlayer", enemyID)
  end
end
function WVEPlayerManager:AddEnemy(id, startPoint, endPoint, speed, movedDistance, playerStatus, nextPointArriveTime, finallPointArriveTime)
  local enemy = WVEEnemy.new(id, startPoint, endPoint, speed, movedDistance, playerStatus, nextPointArriveTime, finallPointArriveTime)
  self.mEnemyList[id] = enemy
  return enemy
end
function WVEPlayerManager:GetPlayerMyself()
  return self.mPlayerMyself
end
function WVEPlayerManager:PlayerBattleStart(playerIDList)
end
function WVEPlayerManager:PlayerBattleEnd(playerIDList)
end
function WVEPlayerManager:ParsePathIDFromMarchID(marchID)
  return marchID % 1000
end
function WVEPlayerManager:UpdatePlayerMarch(playerContent, forceCleanUp)
  local playerNeedUpdate, enemyNeedUpdate, playerMySelf
  DebugOut("playerContent = ")
  DebugTable(playerContent)
  if not playerContent then
    return
  end
  if forceCleanUp then
    self:CleanAllMarchPlayer()
  end
  for i, v in ipairs(playerContent) do
    local pathID = self:ParsePathIDFromMarchID(v.id)
    local path = self.mGameManger:GetMap():getPathByID(pathID)
    local startPoint = path:GetAnotherPointID(v.end_dot)
    DebugOut("pathID = ", pathID)
    DebugOut("startPoint = ", startPoint)
    if v.player_type == EWVEPlayerType.TYPE_PLAYER then
      if v.status == EWVEPlayerMarchStep.STEP_MARCH_END then
        self:RemovePlayer(v.id)
      else
        local player = self.mPlayerList[v.id]
        if player then
          player:Refresh(startPoint, v.end_dot, v.speed, v.current, v.status)
        else
          player = self:AddPlayer(v.id, startPoint, v.end_dot, v.speed, v.current, v.status)
        end
        player.mCurrentPathID = path.mID
        player.mCurrentPathDistance = path.mPathLength
        local tmpPlayer = {}
        tmpPlayer.mID = player.mID
        tmpPlayer.mStartPoint = player.mStartPoint
        tmpPlayer.mEndPoint = player.mEndPoint
        tmpPlayer.mCurrentPathDistance = player.mCurrentPathDistance
        tmpPlayer.mSpeed = player.mSpeed
        tmpPlayer.mCurrentMovedDistance = player.mCurrentMovedDistance
        tmpPlayer.mTypeFrame = player.mTypeFrame
        tmpPlayer.mType = player.mType
        tmpPlayer.mStatus = player.mStatus
        DebugOut("tmpPlayer.mStatus = ", tmpPlayer.mStatus)
        if not self.mPlayerMyself:IsMySelfInMarch(v.id) then
          playerNeedUpdate = playerNeedUpdate or {}
          table.insert(playerNeedUpdate, tmpPlayer)
        else
          tmpPlayer.mTypeFrame = self.mPlayerMyself.mTypeFrame
          tmpPlayer.mType = self.mPlayerMyself.mType
          playerMySelf = tmpPlayer
        end
      end
    elseif v.player_type == EWVEPlayerType.TYPE_ENEMY then
      if v.status == EWVEPlayerMarchStep.STEP_MARCH_END then
        self:RemoveEnemy(v.id)
      else
        local enemy = self.mEnemyList[v.id]
        local leftMoveTime = self.mGameManger:GetMap():CalculateMarchTime(startPoint, v.end_dot, v.current, v.speed)
        local baseID = self.mGameManger:GetMap():GetPlyaerBasePointID()
        local leftTotalMoveTime = self.mGameManger:GetMap():CalculateMarchTime(startPoint, baseID, v.current, v.speed)
        if enemy then
          enemy:Refresh(startPoint, v.end_dot, v.speed, v.current, v.status, leftMoveTime, leftTotalMoveTime)
        else
          enemy = self:AddEnemy(v.id, startPoint, v.end_dot, v.speed, v.current, v.status, leftMoveTime, leftTotalMoveTime)
        end
        enemy:SetDetailInfo(v.prime_count, v.monster_count)
        enemy.mCurrentPathID = path.mID
        enemy.mCurrentPathDistance = path.mPathLength
        local tmpEnemy = {}
        tmpEnemy.mID = enemy.mID
        tmpEnemy.mStartPoint = enemy.mStartPoint
        tmpEnemy.mEndPoint = enemy.mEndPoint
        tmpEnemy.mCurrentPathDistance = enemy.mCurrentPathDistance
        tmpEnemy.mSpeed = enemy.mSpeed
        tmpEnemy.mCurrentMovedDistance = enemy.mCurrentMovedDistance
        tmpEnemy.mTypeFrame = enemy.mTypeFrame
        tmpEnemy.mType = enemy.mType
        tmpEnemy.mStatus = enemy.mStatus
        tmpEnemy.mBigBossCount = enemy.mBigBossCount
        enemyNeedUpdate = enemyNeedUpdate or {}
        table.insert(enemyNeedUpdate, tmpEnemy)
      end
    end
  end
  local flashObj = self.mGameManger:GetFlashObject()
  if not flashObj then
    return
  end
  if playerNeedUpdate or enemyNeedUpdate or playerMySelf then
    flashObj:InvokeASCallback("_root", "WVEMAP_updateMarch", playerNeedUpdate, enemyNeedUpdate, playerMySelf)
  end
  if playerMySelf and self.mPlayerMyself:GetState() == EWVEPlayerState.STATE_PLAYER_MARCH then
    self:FoucsOnMyself()
  end
end
function WVEPlayerManager:CleanAllMarchPlayer()
  DebugOut("kill all player when reconnect and Refresh")
  self.mPlayerList = {}
  self.mEnemyList = {}
  local flashObj = self.mGameManger:GetFlashObject()
  if not flashObj then
    return
  end
  flashObj:InvokeASCallback("_root", "WVEMAP_KillAllPlayer")
end
function WVEPlayerManager:UpdateMyselfStatus(playerContent)
  DebugOut("UpdateMyselfStatus:")
  DebugTable(playerContent)
  self.mPlayerMyself:UpdateToStatus(playerContent)
  self:FoucsOnMyself()
end
function WVEPlayerManager:FoucsOnMyself()
  local isOnpath = self.mPlayerMyself:GetState() == EWVEPlayerState.STATE_PLAYER_MARCH
  local pointID = self.mPlayerMyself:GetPointID()
  local marchID = self.mPlayerMyself:GetMarchID()
  local flashObj = self.mGameManger:GetFlashObject()
  if isOnpath and not self.mPlayerList[marchID] then
    return
  end
  if self.mPlayerMyself:GetState() == EWVEPlayerState.STATE_PLAYER_DIE then
    return
  end
  DebugOut("isOnpath = ", isOnpath)
  DebugOut("pointID = ", pointID)
  DebugOut("marchID = ", marchID)
  if flashObj then
    flashObj:InvokeASCallback("_root", "WVEMAP_FoucsOnMyself", isOnpath, marchID, pointID)
  end
end
function WVEPlayerManager:PlayerMyselfLeavePoint(pointID)
  self.mGameManger.mWVEMap:PlayerMyselfLeavePoint(pointID)
end
function WVEPlayerManager:PlayerMyselfEnterPoint(pointID)
  self.mGameManger.mWVEMap:PlayerMyselfEnterPoint(pointID)
end
function WVEPlayerManager:PlayerMyselfLeaveMarch(marchID)
  local flashObj = self.mGameManger:GetFlashObject()
  if not flashObj then
    return
  end
  flashObj:InvokeASCallback("_root", "WVEMAP_playerMyselfLeaveMarch", marchID)
end
function WVEPlayerManager:PlayerJumpOutAnim()
end
function WVEPlayerManager:PlayerJumpInAnim()
end
function WVEPlayerManager:PrepareJumpTo(targetID)
  self.mPlayerMyself:PrepareJumpTo(targetID)
end
function WVEPlayerManager:OnUpdate(dt)
  self.mEnemyInfoDT = self.mEnemyInfoDT + dt
  self:UpdateMarchEnemyInfo(dt)
end
function WVEPlayerManager:ShowMarchEnemyInfo(enemyID)
  self.mShowEnemyInfo = true
  self.mEnemyInfoDT = 0
  local flashObj = self.mGameManger:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "WVEMAP_ShowMarchEnemyInfo", true, enemyID)
  end
  self:UpdateMarchEnemyInfo(0, true)
end
function WVEPlayerManager:HideMarchEnemyInfo(enemyID)
  self.mShowEnemyInfo = false
  local flashObj = self.mGameManger:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "WVEMAP_ShowMarchEnemyInfo", false, enemyID)
  end
end
function WVEPlayerManager:UpdateMarchEnemyInfo(dt, forceUpdate)
  if self.mShowEnemyInfo and (self.mEnemyInfoDT >= 1000 or forceUpdate) then
    self.mEnemyInfoDT = 0
    local marchEnemyInfos = {}
    for k, v in pairs(self.mEnemyList) do
      DebugOut("v = ")
      DebugTable(v)
      if v.mStatus == EWVEPlayerMarchStep.STEP_MARCH or v.mStatus == EWVEPlayerMarchStep.STEP_INBATTLE then
        local tmpEnemyInfo = {}
        tmpEnemyInfo.mID = v.mID
        tmpEnemyInfo.mBigCount = v.mBigBossCount
        tmpEnemyInfo.mSmallCount = v.mSmallBossCount
        tmpEnemyInfo.mReachNextPointTimeStr = GameUtils:formatTimeString(v.mNextPointTime - (os.time() - v.mUpdateTime))
        tmpEnemyInfo.mReachFinalPointTimeStr = GameUtils:formatTimeString(v.mFinallPointTime - (os.time() - v.mUpdateTime))
        table.insert(marchEnemyInfos, tmpEnemyInfo)
      end
    end
    if #marchEnemyInfos > 0 then
      local flashObj = self.mGameManger:GetFlashObject()
      if flashObj then
        flashObj:InvokeASCallback("_root", "WVEMAP_UpdateMarchEnemyInfo", marchEnemyInfos)
      end
    end
  end
end
