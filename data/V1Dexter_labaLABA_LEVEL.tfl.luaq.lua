local LABA_LEVEL = GameData.Dexter_laba.LABA_LEVEL
LABA_LEVEL[1] = {
  level = 1,
  AWARD = "[{money,100},{technique,100},{prestige,100},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[2] = {
  level = 2,
  AWARD = "[{money,100},{technique,100},{prestige,100},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[3] = {
  level = 3,
  AWARD = "[{money,100},{technique,100},{prestige,100},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[4] = {
  level = 4,
  AWARD = "[{money,100},{technique,100},{prestige,100},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[5] = {
  level = 5,
  AWARD = "[{money,100},{technique,100},{prestige,100},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[6] = {
  level = 6,
  AWARD = "[{money,100},{technique,100},{prestige,100},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[7] = {
  level = 7,
  AWARD = "[{money,100},{technique,100},{prestige,100},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[8] = {
  level = 8,
  AWARD = "[{money,100},{technique,100},{prestige,100},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[9] = {
  level = 9,
  AWARD = "[{money,100},{technique,100},{prestige,100},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[10] = {
  level = 10,
  AWARD = "[{money,100},{technique,100},{prestige,100},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[11] = {
  level = 11,
  AWARD = "[{money,121},{technique,121},{prestige,121},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[12] = {
  level = 12,
  AWARD = "[{money,144},{technique,144},{prestige,144},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[13] = {
  level = 13,
  AWARD = "[{money,169},{technique,169},{prestige,169},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[14] = {
  level = 14,
  AWARD = "[{money,196},{technique,196},{prestige,196},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[15] = {
  level = 15,
  AWARD = "[{money,225},{technique,225},{prestige,225},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[16] = {
  level = 16,
  AWARD = "[{money,256},{technique,256},{prestige,256},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[17] = {
  level = 17,
  AWARD = "[{money,289},{technique,289},{prestige,289},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[18] = {
  level = 18,
  AWARD = "[{money,324},{technique,324},{prestige,324},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[19] = {
  level = 19,
  AWARD = "[{money,361},{technique,361},{prestige,361},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[20] = {
  level = 20,
  AWARD = "[{money,400},{technique,400},{prestige,400},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[21] = {
  level = 21,
  AWARD = "[{money,441},{technique,441},{prestige,441},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[22] = {
  level = 22,
  AWARD = "[{money,484},{technique,484},{prestige,484},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[23] = {
  level = 23,
  AWARD = "[{money,529},{technique,529},{prestige,529},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[24] = {
  level = 24,
  AWARD = "[{money,576},{technique,576},{prestige,576},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[25] = {
  level = 25,
  AWARD = "[{money,625},{technique,625},{prestige,625},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[26] = {
  level = 26,
  AWARD = "[{money,676},{technique,676},{prestige,676},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[27] = {
  level = 27,
  AWARD = "[{money,729},{technique,729},{prestige,729},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[28] = {
  level = 28,
  AWARD = "[{money,784},{technique,784},{prestige,784},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[29] = {
  level = 29,
  AWARD = "[{money,841},{technique,841},{prestige,841},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[30] = {
  level = 30,
  AWARD = "[{money,900},{technique,900},{prestige,900},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[31] = {
  level = 31,
  AWARD = "[{money,961},{technique,961},{prestige,961},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[32] = {
  level = 32,
  AWARD = "[{money,1024},{technique,1024},{prestige,1024},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[33] = {
  level = 33,
  AWARD = "[{money,1089},{technique,1089},{prestige,1089},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[34] = {
  level = 34,
  AWARD = "[{money,1156},{technique,1156},{prestige,1156},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[35] = {
  level = 35,
  AWARD = "[{money,1225},{technique,1225},{prestige,1225},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[36] = {
  level = 36,
  AWARD = "[{money,1296},{technique,1296},{prestige,1296},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[37] = {
  level = 37,
  AWARD = "[{money,1369},{technique,1369},{prestige,1369},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[38] = {
  level = 38,
  AWARD = "[{money,1444},{technique,1444},{prestige,1444},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[39] = {
  level = 39,
  AWARD = "[{money,1521},{technique,1521},{prestige,1521},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[40] = {
  level = 40,
  AWARD = "[{money,1600},{technique,1600},{prestige,1600},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[41] = {
  level = 41,
  AWARD = "[{money,1681},{technique,1681},{prestige,1681},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[42] = {
  level = 42,
  AWARD = "[{money,1764},{technique,1764},{prestige,1764},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[43] = {
  level = 43,
  AWARD = "[{money,1849},{technique,1849},{prestige,1849},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[44] = {
  level = 44,
  AWARD = "[{money,1936},{technique,1936},{prestige,1936},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[45] = {
  level = 45,
  AWARD = "[{money,2025},{technique,2025},{prestige,2025},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[46] = {
  level = 46,
  AWARD = "[{money,2116},{technique,2116},{prestige,2116},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[47] = {
  level = 47,
  AWARD = "[{money,2209},{technique,2209},{prestige,2209},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[48] = {
  level = 48,
  AWARD = "[{money,2304},{technique,2304},{prestige,2304},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[49] = {
  level = 49,
  AWARD = "[{money,2401},{technique,2401},{prestige,2401},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[50] = {
  level = 50,
  AWARD = "[{money,2500},{technique,2500},{prestige,2500},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[51] = {
  level = 51,
  AWARD = "[{money,2601},{technique,2601},{prestige,2601},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[52] = {
  level = 52,
  AWARD = "[{money,2704},{technique,2704},{prestige,2704},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[53] = {
  level = 53,
  AWARD = "[{money,2809},{technique,2809},{prestige,2809},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[54] = {
  level = 54,
  AWARD = "[{money,2916},{technique,2916},{prestige,2916},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[55] = {
  level = 55,
  AWARD = "[{money,3025},{technique,3025},{prestige,3025},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[56] = {
  level = 56,
  AWARD = "[{money,3136},{technique,3136},{prestige,3136},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[57] = {
  level = 57,
  AWARD = "[{money,3249},{technique,3249},{prestige,3249},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[58] = {
  level = 58,
  AWARD = "[{money,3364},{technique,3364},{prestige,3364},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[59] = {
  level = 59,
  AWARD = "[{money,3481},{technique,3481},{prestige,3481},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[60] = {
  level = 60,
  AWARD = "[{money,3600},{technique,3600},{prestige,3600},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[61] = {
  level = 61,
  AWARD = "[{money,3721},{technique,3721},{prestige,3721},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[62] = {
  level = 62,
  AWARD = "[{money,3844},{technique,3844},{prestige,3844},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[63] = {
  level = 63,
  AWARD = "[{money,3969},{technique,3969},{prestige,3969},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[64] = {
  level = 64,
  AWARD = "[{money,4096},{technique,4096},{prestige,4096},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[65] = {
  level = 65,
  AWARD = "[{money,4225},{technique,4225},{prestige,4225},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[66] = {
  level = 66,
  AWARD = "[{money,4356},{technique,4356},{prestige,4356},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[67] = {
  level = 67,
  AWARD = "[{money,4489},{technique,4489},{prestige,4489},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[68] = {
  level = 68,
  AWARD = "[{money,4624},{technique,4624},{prestige,4624},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[69] = {
  level = 69,
  AWARD = "[{money,4761},{technique,4761},{prestige,4761},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[70] = {
  level = 70,
  AWARD = "[{money,4900},{technique,4900},{prestige,4900},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[71] = {
  level = 71,
  AWARD = "[{money,5041},{technique,5041},{prestige,5041},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[72] = {
  level = 72,
  AWARD = "[{money,5184},{technique,5184},{prestige,5184},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[73] = {
  level = 73,
  AWARD = "[{money,5329},{technique,5329},{prestige,5329},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[74] = {
  level = 74,
  AWARD = "[{money,5476},{technique,5476},{prestige,5476},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[75] = {
  level = 75,
  AWARD = "[{money,5625},{technique,5625},{prestige,5625},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[76] = {
  level = 76,
  AWARD = "[{money,5776},{technique,5776},{prestige,5776},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[77] = {
  level = 77,
  AWARD = "[{money,5929},{technique,5929},{prestige,5929},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[78] = {
  level = 78,
  AWARD = "[{money,6084},{technique,6084},{prestige,6084},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[79] = {
  level = 79,
  AWARD = "[{money,6241},{technique,6241},{prestige,6241},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[80] = {
  level = 80,
  AWARD = "[{money,6400},{technique,6400},{prestige,6400},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[81] = {
  level = 81,
  AWARD = "[{money,6561},{technique,6561},{prestige,6561},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[82] = {
  level = 82,
  AWARD = "[{money,6724},{technique,6724},{prestige,6724},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[83] = {
  level = 83,
  AWARD = "[{money,6889},{technique,6889},{prestige,6889},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[84] = {
  level = 84,
  AWARD = "[{money,7056},{technique,7056},{prestige,7056},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[85] = {
  level = 85,
  AWARD = "[{money,7225},{technique,7225},{prestige,7225},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[86] = {
  level = 86,
  AWARD = "[{money,7396},{technique,7396},{prestige,7396},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[87] = {
  level = 87,
  AWARD = "[{money,7569},{technique,7569},{prestige,7569},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[88] = {
  level = 88,
  AWARD = "[{money,7744},{technique,7744},{prestige,7744},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[89] = {
  level = 89,
  AWARD = "[{money,7921},{technique,7921},{prestige,7921},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[90] = {
  level = 90,
  AWARD = "[{money,8100},{technique,8100},{prestige,8100},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[91] = {
  level = 91,
  AWARD = "[{money,8281},{technique,8281},{prestige,8281},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[92] = {
  level = 92,
  AWARD = "[{money,8464},{technique,8464},{prestige,8464},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[93] = {
  level = 93,
  AWARD = "[{money,8649},{technique,8649},{prestige,8649},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[94] = {
  level = 94,
  AWARD = "[{money,8836},{technique,8836},{prestige,8836},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[95] = {
  level = 95,
  AWARD = "[{money,9025},{technique,9025},{prestige,9025},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[96] = {
  level = 96,
  AWARD = "[{money,9216},{technique,9216},{prestige,9216},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[97] = {
  level = 97,
  AWARD = "[{money,9409},{technique,9409},{prestige,9409},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[98] = {
  level = 98,
  AWARD = "[{money,9604},{technique,9604},{prestige,9604},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[99] = {
  level = 99,
  AWARD = "[{money,9801},{technique,9801},{prestige,9801},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
LABA_LEVEL[100] = {
  level = 100,
  AWARD = "[{money,10000},{technique,10000},{prestige,10000},{credit,1},{laba_supply,1},{item,1},{equip,1}]"
}
