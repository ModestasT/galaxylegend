local GameStateGlobalState = GameStateManager.GameStateGlobalState
local GameStateMainPlanet = GameStateManager.GameStateMainPlanet
local GameStateBattleMap = GameStateManager.GameStateBattleMap
local GameStateStarCraft = GameStateManager.GameStateStarCraft
local NewsTip = LuaObjectManager:GetLuaObject("NewsTip")
local GameUIBarLeft = LuaObjectManager:GetLuaObject("GameUIBarLeft")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local GameStateWD = GameStateManager.GameStateWD
local GameVip = LuaObjectManager:GetLuaObject("GameVip")
local LocalizationReplaceOrder = {
  [100] = {"<name>"},
  [111] = {"<name>", "<hp_num>"},
  [112] = {"<name>", "<hp_num>"},
  [113] = {"<name>", "<hp_num>"},
  [200] = {
    "<judian>",
    "<shili>",
    "<name>"
  },
  [210] = {"<name>", "<judian>"},
  [211] = {
    "<name>",
    "<shili>",
    "<judian>"
  },
  [220] = {"<judian>"},
  [221] = {"<shili>", "<judian>"},
  [1] = {"<player>", "<nothing>"},
  [2] = {
    "<end_player>",
    "<kill_player>",
    "<nothing>"
  }
}
NewsTip.ServerNoticeType = {
  CAST_WORLD_BOSS_PREPARE = 14,
  CAST_WORLD_BOSS_START = 15,
  CAST_WORLD_BOSS_OFF = 16,
  CAST_WORLD_BOSS_25KILL = 17,
  CAST_WORLD_BOSS_50KILL = 18,
  CAST_WORLD_BOSS_75KILL = 19,
  CAST_WORLD_BOSS_100_KILL = 20,
  CAST_WORLD_BOSS_KILLED = 21
}
NewsTip.all_ntf_content = {}
NewsTip.ntf_showing = false
function NewsTip:OnFSCommand(cmd, arg)
  if cmd == "eraseNewsTip" then
    DebugOut("eraseNewsTip", #NewsTip.all_ntf_content)
    if #NewsTip.all_ntf_content > 0 then
      local content = table.remove(NewsTip.all_ntf_content)
      NewsTip.DoLoadCastNotify(content)
    else
      GameStateGlobalState:EraseObject(self)
    end
  end
end
function NewsTip.LoadCastNotify(content)
  DebugOut("LoadCastNotify")
  DebugTable(content)
  if NewsTip.all_ntf_content and #NewsTip.all_ntf_content > 0 then
    DebugOut("after show")
    table.insert(NewsTip.all_ntf_content, content)
  else
    local GameUIBattleResult = LuaObjectManager:GetLuaObject("GameUIBattleResult")
    local curState = GameStateManager:GetCurrentGameState()
    if curState == GameStateManager.GameStateBattlePlay or curState == GameStateManager.GameStateToprank or curState == GameStateManager.GameStateArena or curState == GameStateManager.GameStateFairArena or curState == GameStateManager.GameStateWorldChampion or curState == GameStateManager.GameStateFormation or curState:IsObjectInState(GameUIBattleResult) then
      if content.show_in_panel and not content.show_in_chat then
        return
      else
        content.show_in_panel = false
      end
    end
    NewsTip.DoLoadCastNotify(content)
  end
end
function NewsTip.GetActivityTitle(sub_category)
  local GameUIActivityNew = LuaObjectManager:GetLuaObject("GameUIActivityNew")
  local activitylist = GameUIActivityNew:getInstanceData()
  if activitylist then
    for k, v in ipairs(activitylist) do
      if v.sub_category == sub_category then
        return v.title
      end
    end
    return ""
  else
    return ""
  end
end
function NewsTip.DoLoadCastNotify(content)
  DebugOut("FUCK_NewsTip.LoadCastNotify")
  local playerName = GameUtils:GetUserDisplayName(content.name)
  local action = content.action
  local param = content.value
  local text = ""
  if action == 99 then
    text = content.local_msg
  elseif action == 101 then
    text = content.local_msg
  elseif action == 102 then
    text = content.local_msg
  elseif action == 98 then
    local alliance = GameGlobalData:GetData("alliance")
    DebugOut(not alliance)
    DebugOut(alliance.id)
    DebugOut(content.value)
    DebugOut(type(alliance.id))
    DebugOut(type(content.value))
    DebugOut(alliance.id ~= tostring(content.value))
    if not alliance or alliance.id == "" or alliance.id ~= tostring(content.value) then
      return
    end
    text = GameLoader:GetGameText(content.local_msg)
    DebugOut(text)
    if content.local_msg == "LC_MENU_ALLIANCE_PAY_GIFT_CHAT_TEXT" then
      text = string.gsub(text, "%[PLAYER%]", GameUtils:GetUserDisplayName(content.name))
      text = string.gsub(text, "%[ITEM_NAME%]", GameHelper:GetAwardNameText("item", content.valuestr))
    elseif content.local_msg ~= "LC_MENU_WD_CHAT_START" then
      text = string.format(text, GameUtils:GetUserDisplayName(content.valuestr))
    end
  else
    text = GameLoader:GetGameText("LC_TECH_NEWSTIP_" .. action)
  end
  DebugOut("recv loud cast notify ")
  DebugTable(content)
  DebugOut("fuck action string ", text)
  if action == 1 then
    local areaID, battleID = GameUtils:ResolveBattleID(param)
    local monsterName = GameDataAccessHelper:GetAreaNameText(areaID, battleID)
    text = string.gsub(text, "<playerName_char>", playerName)
    text = string.gsub(text, "<enemy_char>", monsterName)
  elseif action == 2 then
    local areaID, battleID = GameUtils:ResolveBattleID(param)
    local monsterName = GameDataAccessHelper:GetAreaNameText(areaID, battleID)
    text = string.gsub(text, "<playerName_char>", playerName)
    text = string.gsub(text, "<enemy_char>", monsterName)
  elseif action == 3 then
    text = string.format(text, playerName, param)
  elseif action == 4 then
    local areaID, battleID = GameUtils:ResolveBattleID(param)
    local sectionID = math.floor(battleID / 1000)
    local sectionNameID = string.format("LC_MENU_SECTION_NAME_%d_%d", areaID, sectionID)
    local sectionName = GameLoader:GetGameText(sectionNameID)
    text = string.gsub(text, "<playerName_char>", playerName)
    text = string.gsub(text, "<pveSection_char>", sectionName)
  elseif action == 5 then
    local areaID, battleID = GameUtils:ResolveBattleID(param)
    local areaNameID = string.format("LC_MENU_AREA_NAME_%d", areaID)
    local areaName = GameLoader:GetGameText(areaNameID)
    text = string.gsub(text, "<playerName_char>", playerName)
    text = string.gsub(text, "<pveSection_char>", areaName)
  elseif action == 6 then
    local itemNameID = string.format("LC_ITEM_ITEM_NAME_%d", param)
    local itemName = GameLoader:GetGameText(itemNameID)
    text = string.gsub(text, "<playerName_char>", playerName)
    text = string.gsub(text, "<itemName_char>", itemName)
  elseif action == 7 then
    local itemNameID = string.format("LC_ITEM_ITEM_NAME_%d", param)
    local itemName = GameLoader:GetGameText(itemNameID)
    text = string.format(text, playerName, itemName)
    text = string.gsub(text, "<playerName_char>", playerName)
    text = string.gsub(text, "<equipment_char>", itemName)
  elseif action == 8 then
    text = string.format(text, playerName, param)
  elseif action == 9 then
    text = string.format(text, playerName)
  elseif action == 10 then
    text = string.format(text, playerName)
  elseif action == 11 then
    text = GameLoader:GetGameText("LC_MENU_ARENA_TIPS_TOP10")
    text = string.gsub(text, "<playerA_char>", playerName)
    text = string.gsub(text, "<playerB_char>", content.valuestr)
    text = string.gsub(text, "<arenaRank_num>", content.value)
  elseif action == 12 then
    text = string.format(GameLoader:GetGameText("LC_MENU_SLOT_AWARD_COMBO_CHAR"), playerName, param)
  elseif action == 13 then
    text = string.format(GameLoader:GetGameText("LC_MENU_SLOT_AWARD_CHAR"), playerName)
  elseif action == NewsTip.ServerNoticeType.CAST_WORLD_BOSS_PREPARE then
    text = GameLoader:GetGameText("LC_TECH_NEWSTIP_PRIMUS_READY")
  elseif action == NewsTip.ServerNoticeType.CAST_WORLD_BOSS_START then
    text = GameLoader:GetGameText("LC_TECH_NEWSTIP_PRIMUS_ATTACK")
  elseif action == NewsTip.ServerNoticeType.CAST_WORLD_BOSS_OFF then
    text = GameLoader:GetGameText("LC_TECH_NEWSTIP_PRIMUS_OVER")
  elseif action == NewsTip.ServerNoticeType.CAST_WORLD_BOSS_25KILL then
    text = GameLoader:GetGameText("LC_TECH_NEWSTIP_PRIMUS_INJURED")
  elseif action == NewsTip.ServerNoticeType.CAST_WORLD_BOSS_50KILL then
    text = GameLoader:GetGameText("LC_TECH_NEWSTIP_PRIMUS_INJURED")
  elseif action == NewsTip.ServerNoticeType.CAST_WORLD_BOSS_75KILL then
    text = GameLoader:GetGameText("LC_TECH_NEWSTIP_PRIMUS_INJURED")
  elseif action == NewsTip.ServerNoticeType.CAST_WORLD_BOSS_100_KILL then
    text = GameLoader:GetGameText("LC_TECH_NEWSTIP_PRIMUS_INJURED")
  elseif action == NewsTip.ServerNoticeType.CAST_WORLD_BOSS_KILLED then
    text = GameLoader:GetGameText("LC_TECH_NEWSTIP_PRIMUS_WIN")
  elseif action == 24 then
    text = GameLoader:GetGameText("LC_TECH_NEWSTIP_COSMIC_EXPEDITION")
    local allianceName = content.valuestr
    text = string.format(text, allianceName)
  elseif action == 25 then
    local param2 = ""
    if content.valuestr == "colony_capture" then
      param2 = GameLoader:GetGameText("LC_MENU_EVENT_COLONIAL_COLONIZE_BUTTON")
    elseif content.valuestr == "colony_resist" then
      param2 = GameLoader:GetGameText("LC_MENU_EVENT_COLONIAL_INDEPEND_BUTTON")
    else
      param2 = GameLoader:GetGameText("LC_MENU_EVENT_COLONIAL_LIBERATION_BUTTON")
    end
    text = GameLoader:GetGameText("LC_MENU_COLONY_EXCHANGE_ALERT")
    text = string.gsub(text, "PLAYER", playerName)
    text = string.gsub(text, "OPERATION", param2)
  elseif action == 26 then
    text = GameLoader:GetGameText("LC_MENU_KRYPTON_EXCHANGE_ALERT")
    text = string.gsub(text, "PLAYER", playerName)
    text = string.gsub(text, "ITEM", GameLoader:GetGameText("LC_ITEM_ITEM_NAME_" .. content.value))
  elseif action == 22 or action == 23 then
    text = GameLoader:GetGameText("LC_TECH_NEWSTIP_LUNAFAST_FIRE")
    local itemName = ""
    local itemNameID = ""
    local itemName2 = ""
    text = string.gsub(text, "<play_name>", playerName)
    if content.valuestr == "kenergy" then
      itemNameID = "LC_MENU_krypton_energy"
      itemName2 = GameLoader:GetGameText("LC_ITEM_ITEM_NAME_2516")
      itemName = GameLoader:GetGameText(itemNameID)
      text = string.gsub(text, "<item_name>", itemName2)
      text = string.gsub(text, "<reward_name>", itemName)
      text = string.gsub(text, "<num>", content.value)
    else
      itemNameID = "LC_ITEM_ITEM_NAME_" .. content.value
      itemName2 = GameLoader:GetGameText("LC_ITEM_ITEM_NAME_2516")
      text = string.gsub(text, "<reward_name>", itemName)
      text = string.gsub(text, "<num>", 1)
    end
  elseif action == 201 then
    text = GameLoader:GetGameText("LC_MENU_mining_rare_notice")
    text = string.gsub(text, "<player>", playerName)
  elseif action == 203 then
    text = GameLoader:GetGameText("LC_MENU_pass_chaos_quasar_15")
    text = string.gsub(text, "<player>", playerName)
  elseif action == 204 then
    text = GameLoader:GetGameText("LC_MENU_pass_infinite_cosmos_notice")
    text = string.gsub(text, "<player>", playerName)
    text = string.gsub(text, "<number>", content.value)
  elseif action == 205 then
    text = GameLoader:GetGameText("LC_MENU_pass_wormhole_notice")
    text = string.gsub(text, "<player>", playerName)
    text = string.gsub(text, "<wormhole_name>", NewsTip.GetActivityTitle(tonumber(content.valuestr)))
  elseif action == 206 then
    text = GameLoader:GetGameText("LC_MENU_pass_class_wormhole_notice")
    text = string.gsub(text, "<player>", playerName)
    text = string.gsub(text, "<class_wormhole_name>", NewsTip.GetActivityTitle(tonumber(content.valuestr)))
  elseif action == 207 or action == 208 or action == 209 or action == 210 or action == 211 or action == 212 or action == 213 or action == 214 or action == 215 or action == 216 or action == 217 or action == 218 then
    text = content.local_msg
  end
  DebugOut("fuck newstip text ", text)
  if content.show_in_chat then
    local showChat = true
    local channelStr = "global_chat_channel"
    if action == 98 then
      if not GameStateWD:HaveAlliance() then
        showChat = false
      end
      channelStr = "alchat_" .. content.value
    end
    if action == 102 then
      channelStr = "tc_chat_channel"
    end
    local showInChatText = ""
    if action == 101 then
      showInChatText = text
    else
      showInChatText = "<font color='#FF6A6A'>" .. text .. "</font>"
    end
    if showChat then
      local msg = {
        sender_name = GameLoader:GetGameText("LC_MENU_CHAT_SYSTEM_CHAR"),
        content = showInChatText,
        receiver = "",
        channel = channelStr,
        timestamp = GameUIBarLeft:GetServerTimeUTC(),
        viplevel = 0,
        show_effect = 0
      }
      NetMessageHandler.ChatMessageNotifyHandler(msg)
    end
  end
  if content.show_in_panel then
    NewsTip.ntf_showing = true
    if not NewsTip:GetFlashObject() then
      NewsTip:LoadFlashObject()
    end
    NewsTip:GetFlashObject():InvokeASCallback("_root", "showNewsTip", text)
    GameStateGlobalState:AddObject(NewsTip)
  end
end
function NewsTip.StarCraftHitNotify(content)
  local activeGameState = GameStateManager:GetCurrentGameState()
  if activeGameState == GameStateMainPlanet or activeGameState == GameStateBattleMap or activeGameState == GameStateStarCraft then
    local itemInfo = content.hit
    local itemLoclizationID = "LC_MENU_TC_REPORT_" .. content.hit.id
    if itemInfo.id == 1 then
      itemLoclizationID = "LC_MENU_TC_REPORT_KILL_" .. content.hit.params[2]
    elseif itemInfo.id == 2 then
      itemLoclizationID = "LC_MENU_TC_REPORT_END_KILL_" .. content.hit.params[3]
    end
    local text = GameLoader:GetGameText(itemLoclizationID)
    for i, v in ipairs(content.hit.params) do
      local value = v
      if text ~= nil and text ~= "" then
        DebugOut("text = ", text)
        text = string.gsub(text, LocalizationReplaceOrder[content.hit.id][i], value)
      end
    end
    NewsTip:GetFlashObject():InvokeASCallback("_root", "showNewsTip", text)
    GameStateGlobalState:AddObject(NewsTip)
  end
end
function NewsTip.MycardPayNotify(content)
  if GameUtils:IsMycardAPP() and GameVip:IsMycardEnabled() then
    local GameTip = LuaObjectManager:GetLuaObject("GameTip")
    GameTip:Show(content.msg)
  end
end
function NewsTip:Update(dt)
  if self:GetFlashObject() then
    self:GetFlashObject():Update(dt)
    self:GetFlashObject():InvokeASCallback("_root", "OnUpdate", dt)
  end
end
