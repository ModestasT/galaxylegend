local GameUIFpsFte = LuaObjectManager:GetLuaObject("GameUIFpsFte")
GameUIFpsFte.config = {
  stones = {
    [1] = {
      info = {
        start_time = 0,
        end_time = 17000,
        is_random = false,
        random_num = 2,
        interval_time = 1000
      },
      data = {
        [1] = {
          info = {
            isloop = false,
            interval_time = 1000,
            loop_num = 100,
            timestart_rel = 1500
          },
          data = {
            [1] = {
              bpos = {
                0,
                -0.8,
                10
              },
              epos = {
                0,
                0,
                0
              },
              speed = 2,
              curhp = 100,
              damage = 6,
              type = 1
            }
          }
        },
        [2] = {
          info = {
            isloop = false,
            interval_time = 1000,
            loop_num = 1,
            timestart_rel = 3000
          },
          data = {
            [1] = {
              bpos = {
                1,
                -0.8,
                10
              },
              epos = {
                0,
                0,
                0
              },
              speed = 2,
              curhp = 100,
              damage = 6,
              type = 1
            }
          }
        },
        [3] = {
          info = {
            isloop = true,
            interval_time = 1000,
            loop_num = 3,
            timestart_rel = 4000
          },
          data = {
            [1] = {
              bpos = {
                -1,
                -0.8,
                10
              },
              epos = {
                0,
                0,
                0
              },
              speed = 2,
              curhp = 100,
              damage = 6,
              type = 1
            }
          }
        },
        [2] = {
          info = {
            isloop = true,
            interval_time = 1000,
            loop_num = 5,
            timestart_rel = 3000
          },
          data = {
            [1] = {
              bpos = {
                0.5,
                -0.8,
                10
              },
              epos = {
                0,
                0,
                0
              },
              speed = 2,
              curhp = 100,
              damage = 6,
              type = 1
            }
          }
        },
        [3] = {
          info = {
            isloop = false,
            interval_time = 1000,
            loop_num = 1,
            timestart_rel = 4000
          },
          data = {
            [1] = {
              bpos = {
                -0.5,
                -0.8,
                10
              },
              epos = {
                0,
                0,
                0
              },
              speed = 2,
              curhp = 100,
              damage = 6,
              type = 1
            }
          }
        }
      }
    },
    [2] = {
      info = {
        start_time = 7000,
        end_time = 16000,
        is_random = false,
        random_num = 0,
        interval_time = 100
      },
      data = {
        [1] = {
          info = {
            isloop = true,
            interval_time = 5000,
            loop_num = 100,
            timestart_rel = 0
          },
          data = {
            [1] = {
              bpos = {
                0,
                -0.8,
                10
              },
              epos = {
                0,
                0,
                0
              },
              speed = 2,
              curhp = 100,
              damage = 60,
              type = 1
            },
            [2] = {
              bpos = {
                -0.4,
                -0.20000000000000007,
                10
              },
              epos = {
                0,
                0,
                0
              },
              speed = 2,
              curhp = 100,
              damage = 60,
              type = 1
            },
            [3] = {
              bpos = {
                0.2,
                -2.2,
                10
              },
              epos = {
                0,
                0,
                0
              },
              speed = 2,
              curhp = 100,
              damage = 60,
              type = 1
            }
          }
        },
        [2] = {
          info = {
            isloop = true,
            interval_time = 1600,
            loop_num = 100,
            timestart_rel = 500
          },
          data = {
            [1] = {
              bpos = {
                3.1,
                -2.2,
                10
              },
              epos = {
                0,
                0,
                0
              },
              speed = 2,
              curhp = 100,
              damage = 60,
              type = 1
            },
            [2] = {
              bpos = {
                -3.6,
                -2.2,
                10
              },
              epos = {
                0,
                0,
                0
              },
              speed = 2,
              curhp = 100,
              damage = 60,
              type = 1
            },
            [3] = {
              bpos = {
                -3.5,
                1.7,
                10
              },
              epos = {
                0,
                0,
                0
              },
              speed = 2,
              curhp = 100,
              damage = 60,
              type = 1
            }
          }
        }
      }
    },
    [3] = {
      info = {
        start_time = 17000,
        end_time = 27000,
        is_random = false,
        random_num = 0,
        interval_time = 100
      },
      data = {
        [1] = {
          info = {
            isloop = false,
            interval_time = 1000,
            loop_num = 1,
            timestart_rel = 0
          },
          data = {
            [1] = {
              bpos = {
                0,
                -0.8,
                10
              },
              epos = {
                0,
                0,
                0
              },
              speed = 2,
              curhp = 100,
              damage = 60,
              type = 1
            }
          }
        },
        [2] = {
          info = {
            isloop = true,
            interval_time = 3500,
            loop_num = 100,
            timestart_rel = 1000
          },
          data = {
            [1] = {
              bpos = {
                1,
                -1.1,
                10
              },
              epos = {
                0,
                0,
                0
              },
              speed = 2,
              curhp = 100,
              damage = 60,
              type = 1
            },
            [2] = {
              bpos = {
                -0.4,
                -0.20000000000000007,
                10
              },
              epos = {
                0,
                0,
                0
              },
              speed = 2,
              curhp = 100,
              damage = 60,
              type = 1
            },
            [3] = {
              bpos = {
                0.2,
                -2.2,
                10
              },
              epos = {
                0,
                0,
                0
              },
              speed = 2,
              curhp = 100,
              damage = 60,
              type = 1
            },
            [4] = {
              bpos = {
                -1.6,
                -1.5,
                10
              },
              epos = {
                0,
                0,
                0
              },
              speed = 2,
              curhp = 100,
              damage = 60,
              type = 1
            },
            [5] = {
              bpos = {
                -1.2,
                0.8,
                10
              },
              epos = {
                0,
                0,
                0
              },
              speed = 2,
              curhp = 100,
              damage = 60,
              type = 1
            }
          }
        },
        [3] = {
          info = {
            isloop = true,
            interval_time = 2000,
            loop_num = 100,
            timestart_rel = 1500
          },
          data = {
            [1] = {
              bpos = {
                2,
                0.09999999999999998,
                10
              },
              epos = {
                0,
                0,
                0
              },
              speed = 2,
              curhp = 100,
              damage = 60,
              type = 1
            },
            [2] = {
              bpos = {
                0.5,
                -2.9000000000000004,
                10
              },
              epos = {
                0,
                0,
                0
              },
              speed = 2,
              curhp = 100,
              damage = 60,
              type = 1
            },
            [3] = {
              bpos = {
                -1.1,
                -2.9000000000000004,
                10
              },
              epos = {
                0,
                0,
                0
              },
              speed = 2,
              curhp = 100,
              damage = 60,
              type = 1
            },
            [4] = {
              bpos = {
                -2.6,
                -0.10000000000000009,
                10
              },
              epos = {
                0,
                0,
                0
              },
              speed = 2,
              curhp = 100,
              damage = 60,
              type = 1
            },
            [5] = {
              bpos = {
                0.9,
                1.8,
                10
              },
              epos = {
                0,
                0,
                0
              },
              speed = 2,
              curhp = 100,
              damage = 60,
              type = 1
            }
          }
        }
      }
    },
    [4] = {
      info = {
        start_time = 27000,
        end_time = 5800000,
        is_random = false,
        random_num = 0,
        interval_time = 100
      },
      data = {
        [1] = {
          info = {
            isloop = false,
            interval_time = 3000,
            loop_num = 1,
            timestart_rel = 0
          },
          data = {
            [1] = {
              bpos = {
                0,
                -0.8,
                10
              },
              epos = {
                0,
                0,
                0
              },
              speed = 2,
              curhp = 100,
              damage = 100,
              type = 1
            }
          }
        },
        [2] = {
          info = {
            isloop = true,
            interval_time = 3000,
            loop_num = 100,
            timestart_rel = 1000
          },
          data = {
            [1] = {
              bpos = {
                1,
                -1.1,
                10
              },
              epos = {
                0,
                0,
                0
              },
              speed = 2,
              curhp = 100,
              damage = 100,
              type = 1
            },
            [2] = {
              bpos = {
                -0.4,
                -0.20000000000000007,
                10
              },
              epos = {
                0,
                0,
                0
              },
              speed = 2,
              curhp = 100,
              damage = 100,
              type = 1
            },
            [3] = {
              bpos = {
                0.2,
                -2.2,
                10
              },
              epos = {
                0,
                0,
                0
              },
              speed = 2,
              curhp = 100,
              damage = 100,
              type = 1
            },
            [4] = {
              bpos = {
                -1.6,
                -1.5,
                10
              },
              epos = {
                0,
                0,
                0
              },
              speed = 2,
              curhp = 100,
              damage = 100,
              type = 1
            },
            [5] = {
              bpos = {
                -1.2,
                0.8,
                10
              },
              epos = {
                0,
                0,
                0
              },
              speed = 2,
              curhp = 100,
              damage = 100,
              type = 1
            }
          }
        },
        [3] = {
          info = {
            isloop = true,
            interval_time = 2500,
            loop_num = 100,
            timestart_rel = 1500
          },
          data = {
            [1] = {
              bpos = {
                2,
                0.09999999999999998,
                10
              },
              epos = {
                0,
                0,
                0
              },
              speed = 2,
              curhp = 100,
              damage = 100,
              type = 1
            },
            [2] = {
              bpos = {
                0.5,
                -2.9000000000000004,
                10
              },
              epos = {
                0,
                0,
                0
              },
              speed = 2,
              curhp = 100,
              damage = 100,
              type = 1
            },
            [3] = {
              bpos = {
                -1.1,
                -2.9000000000000004,
                10
              },
              epos = {
                0,
                0,
                0
              },
              speed = 2,
              curhp = 100,
              damage = 100,
              type = 1
            },
            [4] = {
              bpos = {
                -2.6,
                -0.10000000000000009,
                10
              },
              epos = {
                0,
                0,
                0
              },
              speed = 2,
              curhp = 100,
              damage = 100,
              type = 1
            },
            [5] = {
              bpos = {
                0.9,
                1.8,
                10
              },
              epos = {
                0,
                0,
                0
              },
              speed = 2,
              curhp = 100,
              damage = 100,
              type = 1
            }
          }
        }
      }
    }
  },
  isLoop = true,
  istuto = true,
  tutoMove_time = 1100,
  tutoMove_len = 30,
  tutoShoot_time = 5100,
  tutoSkill_time = 15100,
  tutoEnd_HP = 0,
  canbehit_dist = 10,
  fingerparam = 2.3,
  fingerScaleY = 1,
  angMin_x = -40 * math.pi / 180,
  angMax_x = 40 * math.pi / 180,
  angMin_y = -30 * math.pi / 180,
  angMax_y = 30 * math.pi / 180,
  bg_h = 10,
  maxSpeedbg = 1000000,
  inertiaparam = 10,
  time_sec = 60,
  hp_self = 1000,
  speed_bg = 1,
  atk_common = 10,
  atk_skill = 50,
  skill_cd = 15000,
  sound_close_mid = 2,
  sound_mid_far = 5
}
