local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameUIMaskLayer = LuaObjectManager:GetLuaObject("GameUIMaskLayer")
GameAndroidBackManager = {}
GameAndroidBackManager.TableCallbacks = {}
function GameAndroidBackManager:AddCallback(callback)
  for i, exist_callback in ipairs(self.TableCallbacks) do
    if callback == exist_callback then
      return
    end
  end
  table.insert(self.TableCallbacks, 1, callback)
end
function GameAndroidBackManager:RemoveCallback(callback)
  for i, exist_callback in ipairs(self.TableCallbacks) do
    if callback == exist_callback then
      table.remove(self.TableCallbacks, i)
      return
    end
  end
end
function GameAndroidBackManager:BackPressed()
  if GameUtils:GetTutorialHelp() and GameUIMaskLayer.OnAndroidBackCustom then
    GameUIMaskLayer.OnAndroidBackCustom()
    return true
  end
  local active_callback = self.TableCallbacks[1]
  if active_callback then
    active_callback()
    return true
  else
    AutoUpdateInBackground:SaveHRFileList()
    if ext.GetBundleIdentifier() == "com.tap4fun.galaxyempire2_android.platform.uc" and AutoUpdate.localAppVersion >= 10608 then
      IPlatformExt.ExitGame()
    else
      ext.ShowExitGameDialog()
    end
    return false
  end
end
