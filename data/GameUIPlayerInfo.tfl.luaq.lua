local GameUIPlayerInfo = LuaObjectManager:GetLuaObject("GameUIPlayerInfo")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
GameUIPlayerInfo.AllData = {}
GameUIPlayerInfo.currentLeftItem = 1
GameUIPlayerInfo.isShowBuff = false
GameUIPlayerInfo.CoatingTextID = {
  coating_ph_atk = "20008",
  coating_ph_armor = "20009",
  coating_sp_atk = "20010",
  coating_sp_armor = "20011",
  coating_durability = "20012",
  coating_ph_atk_v2 = "20024",
  coating_ph_armor_v2 = "20025",
  coating_sp_atk_v2 = "20026",
  coating_sp_armor_v2 = "20027",
  coating_durability_v2 = "20028"
}
function GameUIPlayerInfo:addUIPlayerInfo()
  if not GameStateManager:GetCurrentGameState():IsObjectInState(self) then
    GameStateManager:GetCurrentGameState():AddObject(self)
  end
end
function GameUIPlayerInfo:EraseUIPlayerInfo(...)
  if GameStateManager:GetCurrentGameState():IsObjectInState(self) then
    GameStateManager:GetCurrentGameState():EraseObject(self)
  end
end
function GameUIPlayerInfo:OnAddToGameState(parent_state)
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  NetMessageMgr:SendMsg(NetAPIList.user_info_req.Code, nil, GameUIPlayerInfo.RequestDataCallback, true, nil)
  GameGlobalData:RegisterDataChangeCallback("wdcPlayerInfo", GameUIPlayerInfo.OnGlobalWdcPlayerInfoChange)
end
function GameUIPlayerInfo.OnGlobalWdcPlayerInfoChange(content)
  DebugOut("OnGlobalWdcPlayerInfoChange = ")
  DebugTable(content)
end
function GameUIPlayerInfo.RequestDataCallback(msgType, content)
  if msgType and msgType == NetAPIList.user_info_ack.Code and content then
    GameUIPlayerInfo.RecordData(content)
    GameUIPlayerInfo:MoveInAnimation()
    GameUIPlayerInfo:Init()
    GameUIPlayerInfo:CheckShowGP()
    DebugOut("RequestDataCallback:success")
    DebugTable(GameUIPlayerInfo.AllData)
  else
    DebugOut("RequestDataCallback:failed")
    return false
  end
  return true
end
function GameUIPlayerInfo.RecordData(content)
  local data = {}
  for k, v in pairs(content) do
    if type(v) == "table" then
    else
      data[k] = v
    end
  end
  data.fleet = GameUIPlayerInfo.SortFleetTable(content.fleet)
  data.force = GameUIPlayerInfo.SortForceTable(content.force)
  data.resource = GameUIPlayerInfo.SortResourceTable(content.resource)
  data.other = GameUIPlayerInfo.SortOtherTable(content.other)
  GameUIPlayerInfo.AllData = data
end
function GameUIPlayerInfo.ReconstructTable(content)
  local data = {}
  for k, v in pairs(content) do
    local item = {}
    item.key = k
    item.value = v
    table.insert(data, item)
  end
  return data
end
function GameUIPlayerInfo.SortFleetTable(content)
  local data = {}
  data = content.ranks
  table.sort(data, function(a, b)
    return a.key > b.key
  end)
  local total = {
    key = 0,
    value = content.total_count
  }
  table.insert(data, total)
  DebugOut("data = ")
  DebugTable(data)
  return data
end
function GameUIPlayerInfo.SortForceTable(content)
  local data = {}
  local i = 1
  for k, v in pairs(content) do
    if not string.find(k, "coating") and k ~= "wdc_buff_time" then
      data[i] = {}
      i = i + 1
    end
  end
  if GameGlobalData:GetModuleStatus("medal") == true then
    data[1].key = "total_force"
    data[2].key = "total_speed"
    data[3].key = "red_medal_count"
    data[4].key = "golden_medal_count"
    data[5].key = "purple_medal_count"
    data[6].key = "average_medal_level"
    data[7].key = "average_medal_rank"
    data[8].key = "highest_medal_level"
    data[9].key = "highest_medal_rank"
    data[10].key = "r7_krypton_count"
    data[11].key = "r6_krypton_count"
    data[12].key = "highest_krypton_level"
    data[13].key = "aver_krypton_level"
    data[14].key = "fleet_highest_remould_level"
    data[15].key = "fleet_aver_remould_level"
    data[16].key = "total_remodel_level"
    data[17].key = "total_tech_level"
    data[18].key = "intensify_level"
    data[19].key = "aver_equip_level"
    data[20].key = "aver_interfere_level"
    data[21].key = "highest_interfere_level"
  else
    data[1].key = "total_force"
    data[2].key = "total_speed"
    data[3].key = "red_medal_count"
    data[4].key = "golden_medal_count"
    data[5].key = "purple_medal_count"
    data[6].key = "r7_krypton_count"
    data[7].key = "r6_krypton_count"
    data[8].key = "highest_krypton_level"
    data[9].key = "aver_krypton_level"
    data[10].key = "fleet_highest_remould_level"
    data[11].key = "fleet_aver_remould_level"
    data[12].key = "total_remodel_level"
    data[13].key = "total_tech_level"
    data[14].key = "intensify_level"
    data[15].key = "aver_equip_level"
    data[16].key = "aver_interfere_level"
    data[17].key = "highest_interfere_level"
    data[18] = nil
    data[19] = nil
    data[20] = nil
    data[21] = nil
  end
  i = 1
  local coating = {}
  for k, v in pairs(content) do
    if not string.find(k, "coating") and k ~= "wdc_buff_time" then
      if data[i] then
        data[i].value = content[data[i].key]
        i = i + 1
      else
        break
      end
    elseif tonumber(v) and tonumber(v) > 0 then
      local coatingData = {}
      coatingData.key = k
      coatingData.value = v
      table.insert(coating, coatingData)
      DebugOut("coating:", k, v)
    end
  end
  DebugOut("SortForceTable:coating")
  DebugTable(coating)
  for j = 1, #coating do
    table.insert(data, 1 + j, coating[j])
  end
  DebugTable(data)
  return data
end
function GameUIPlayerInfo.SortResourceTable(content)
  local data = {}
  local i = 1
  for k, v in pairs(content) do
    data[i] = {}
    i = i + 1
  end
  data[1].key = "credit"
  data[2].key = "money"
  data[3].key = "technique"
  data[4].key = "kenergy"
  data[5].key = "ac_supply"
  data[6].key = "laba_supply"
  data[7].key = "art_point"
  data[8].key = "medal_currency"
  data[9].key = "expedition"
  data[10] = nil
  data[11] = nil
  i = 1
  for k, v in pairs(content) do
    if data[i] then
      data[i].value = content[data[i].key]
      i = i + 1
    end
  end
  return data
end
function GameUIPlayerInfo.SortOtherTable(content)
  local data = {}
  local i = 1
  for k, v in pairs(content) do
    if not string.find(k, "total") then
      data[i] = {}
      i = i + 1
    end
  end
  data[1].key = "prestige"
  data[2].key = "achieve_end"
  data[3].key = "plat_achieve_end"
  data[4].key = "gold_achieve_end"
  data[5].key = "silver_achieve_end"
  data[6].key = "bronze_achieve_end"
  data[7].key = "achieve_credit_get"
  data[1].value = content.prestige
  data[2].value = content.achieve_end .. "/" .. content.achieve_total
  data[3].value = content.plat_achieve_end .. "/" .. content.plat_achieve_total
  data[4].value = content.gold_achieve_end .. "/" .. content.gold_achieve_total
  data[5].value = content.silver_achieve_end .. "/" .. content.silver_achieve_total
  data[6].value = content.bronze_achieve_end .. "/" .. content.bronze_achieve_total
  data[7].value = content.achieve_credit_get
  DebugOut("GameUIPlayerInfo:SortOtherTable")
  DebugTable(data)
  return data
end
function GameUIPlayerInfo:OnEraseFromGameState(old_parent)
  self:UnloadFlashObject()
end
if AutoUpdate.isAndroidDevice then
  function GameUIPlayerInfo.OnAndroidBack()
    local flashObj = GameUIPlayerInfo:GetFlashObject()
    if GameUIPlayerInfo.isShowBuff then
      if flashObj then
        flashObj:InvokeASCallback("_root", "isShowBuffLayer", false)
      end
      GameUIPlayerInfo.isShowBuff = false
    elseif flashObj then
      flashObj:InvokeASCallback("_root", "moveOutAnimation")
    end
  end
end
function GameUIPlayerInfo:OnFSCommand(cmd, arg)
  DebugOut("cmd = " .. cmd)
  if cmd == "move_in_over" then
  elseif cmd == "move_out_over" then
    GameUIPlayerInfo:EraseUIPlayerInfo()
  elseif cmd == "select_leftitem" then
    GameUIPlayerInfo:selectLeftItem(arg)
  elseif cmd == "updateItemDate" then
    GameUIPlayerInfo:updateItemDate(tonumber(arg))
  elseif cmd == "gp_atck" then
    DebugOut("gp_atck")
    local localAppid = ext.GetBundleIdentifier()
    if localAppid == AutoUpdate.bundleIdentifierType.BUNDLE_IDENTIFIER_NORMAL_ANDROID then
      IPlatformExt.show_LeaderBord()
    end
  elseif cmd == "gp_achivement" then
    DebugOut("gp_achivement")
    local localAppid = ext.GetBundleIdentifier()
    if localAppid == AutoUpdate.bundleIdentifierType.BUNDLE_IDENTIFIER_NORMAL_ANDROID then
      IPlatformExt.show_Achievement()
    end
  elseif cmd == "showBuff" then
    GameUIPlayerInfo.isShowBuff = true
  end
end
function GameUIPlayerInfo:Update(dt)
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    flash_obj:Update(dt)
    flash_obj:InvokeASCallback("_root", "onUpdateFrame", dt)
    self:UpdateItemTime()
  end
end
function GameUIPlayerInfo:UpdateItemTime()
  if self.TimeUpdateData then
    for k, v in pairs(self.TimeUpdateData) do
      local value = tonumber(v.value) - os.time()
      self.AllData.force[v.index].value = value
      if value < 0 then
        value = 0
      end
      value = GameUtils:formatTimeString(value)
      GameUIPlayerInfo:GetFlashObject():InvokeASCallback("_root", "updateItemDate", v.index, v.name, value)
    end
  end
end
function GameUIPlayerInfo:Init(...)
  GameUIPlayerInfo:setPlayerBasicInfo()
  GameUIPlayerInfo:selectLeftItem(GameUIPlayerInfo.currentLeftItem)
end
function GameUIPlayerInfo:MoveInAnimation(...)
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "moveInAnimation")
  end
end
function GameUIPlayerInfo:MoveOutAnimation(...)
end
function GameUIPlayerInfo:setPlayerBasicInfo()
  DebugOut("GameUIPlayerInfo:setPlayerBasicInfo")
  local data = self.AllData
  local stringTable = LuaUtils:string_split(data.name, ".")
  local playerName = ""
  local serverName = ""
  local icon = GameUtils:GetPlayerAvatarWithSex(GameGlobalData:GetUserInfo().sex, data.level)
  local leaderlist = GameGlobalData:GetData("leaderlist")
  local curMatrixIndex = GameGlobalData.GlobalData.curMatrixIndex
  local rankName = GameDataAccessHelper:GetMilitaryRankName(data.military_rank)
  local arenaName = data.champion or 0
  local alliance = data.alliance_name or "-"
  local levelInfo = GameGlobalData:GetData("levelinfo")
  local curLevel = levelInfo.level
  local curExp = levelInfo.experience
  local nextExp = levelInfo.uplevel_experience
  local levelPercent = 0
  if GameUtils:isPlayerMaxLevel(curLevel) then
    curExp = "-"
    nextExp = "-"
    levelPercent = 100
  else
    levelPercent = math.floor(100 * levelInfo.experience / levelInfo.uplevel_experience)
  end
  if leaderlist and curMatrixIndex then
    local player_main_fleet = GameGlobalData:GetFleetInfo(1)
    DebugOut("fleetid:", leaderlist.leader_ids[curMatrixIndex])
    DebugOut("level ", data.level)
    DebugTable(leaderlist)
    icon = GameDataAccessHelper:GetFleetAvatar(leaderlist.leader_ids[curMatrixIndex], player_main_fleet.level)
  end
  for i = 1, #stringTable - 1 do
    playerName = playerName .. stringTable[i]
  end
  local wdcPlayerInfo = GameGlobalData:GetData("wdcPlayerInfo")
  DebugTable(wdcPlayerInfo)
  local rank = GameUtils:GetPlayerRankInWdc()
  local playerNameText = playerName .. rank
  local isBuffer = false
  if 0 < #wdcPlayerInfo.buffers then
    isBuffer = true
  end
  serverName = stringTable[#stringTable]
  if rankName == "" then
    rankName = "-"
  end
  if tonumber(arenaName) == 0 then
    arenaName = "-"
  end
  if alliance == "" then
    alliance = "-"
  end
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "setPlayerBasicInfo", playerNameText, GameLoader:GetGameText("LC_MENU_Level") .. data.level, serverName, rankName, arenaName, alliance, icon, isBuffer)
    self:GetFlashObject():InvokeASCallback("_root", "setExpPercentAndNumber", levelPercent, curExp, nextExp)
    DebugOut("setPlayerBasicInfo:", icon)
  end
  self:setBufferinfo(wdcPlayerInfo.buffers)
end
function GameUIPlayerInfo:setBufferinfo(buffers)
  DebugOut("setBufferinfo = ")
  DebugTable(buffers)
  local propertyName = ""
  local propertyValue = ""
  for i = 1, 8 do
    propertyName = propertyName .. GameLoader:GetGameText("LC_MENU_Equip_param_" .. i) .. "\001"
    local flag = false
    for k, v in ipairs(buffers) do
      if tonumber(v.key) == i then
        flag = true
        propertyValue = propertyValue .. GameUtils.numberConversion(tonumber(v.value)) .. "\001"
        break
      end
    end
    if not flag then
      propertyValue = propertyValue .. "-" .. "\001"
    end
  end
  DebugOut("ddd" .. propertyValue)
  local tipText = GameLoader:GetGameText("LC_MENU_LEAGUE_MEDAL_REWARD_INFO")
  local rank = GameUtils:GetPlayerRankInWdc()
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "setBufferInfo", propertyName, propertyValue, tipText, rank)
  end
end
function GameUIPlayerInfo:selectLeftItem(arg)
  GameUIPlayerInfo.currentLeftItem = tonumber(arg)
  GameUIPlayerInfo:ShowItemHigh(arg)
  DebugOut("select_leftitem:", GameUIPlayerInfo.currentLeftItem)
  self.TimeUpdateData = nil
  if 1 == tonumber(arg) then
    GameUIPlayerInfo:ShowSummary(arg)
  elseif 2 == tonumber(arg) then
    GameUIPlayerInfo:InitListBox(#GameUIPlayerInfo.AllData.fleet)
  elseif 3 == tonumber(arg) then
    GameUIPlayerInfo:InitListBox(#GameUIPlayerInfo.AllData.force)
  elseif 4 == tonumber(arg) then
    GameUIPlayerInfo:InitListBox(#GameUIPlayerInfo.AllData.resource)
  elseif 5 == tonumber(arg) then
    GameUIPlayerInfo:InitListBox(#GameUIPlayerInfo.AllData.other)
  end
end
function GameUIPlayerInfo:ShowItemHigh(arg)
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "showItem", arg)
  end
end
function GameUIPlayerInfo:InitListBox(count)
  if self:GetFlashObject() then
    DebugOut("InitListBox:", count)
    self:GetFlashObject():InvokeASCallback("_root", "initListBox", count)
  end
end
function GameUIPlayerInfo:ShowSummary()
  local allData = GameUIPlayerInfo.AllData
  local count = #allData.fleet + #allData.force + #allData.resource + #allData.other + 4
  GameUIPlayerInfo:InitListBox(count)
end
function GameUIPlayerInfo:updateItemDate(item_index)
  local data = {}
  DebugOut("updateItemDate:", item_index)
  if self.currentLeftItem == 1 then
    self:updateSumItemData(item_index)
    return
  elseif self.currentLeftItem == 2 then
    data = self.AllData.fleet
  elseif self.currentLeftItem == 3 then
    data = self.AllData.force
  elseif self.currentLeftItem == 4 then
    data = self.AllData.resource
  elseif self.currentLeftItem == 5 then
    data = self.AllData.other
  end
  if data == nil then
    DebugOut("GameUIPlayerInfo:updateItemDate:data is nil")
    return
  end
  local itemValue = data[item_index].value
  local itemName = GameLoader:GetGameText("LC_MENU_PLAYER_INFO_" .. data[item_index].key)
  if tonumber(itemValue) == 0 and data[item_index].key == "highest_krypton_rank" then
    itemValue = "-"
  end
  if string.find(data[item_index].key, "coating") then
    itemName = GameLoader:GetGameText("LC_ITEM_ITEM_NAME_" .. self.CoatingTextID[data[item_index].key])
    self:RecordTimeUpdateData(item_index, tonumber(itemValue), itemName)
    itemValue = GameUtils:formatTimeString(tonumber(itemValue))
  end
  if data[item_index].key == "wdc_buff_time" then
    local wdcPlayerInfo = GameGlobalData:GetData("wdcPlayerInfo")
    if wdcPlayerInfo.player_info.using_rank ~= 0 then
      itemName = GameLoader:GetGameText("LC_MENU_LEAGUE_GRADENAME_" .. wdcPlayerInfo.player_info.using_rank)
    end
    self:RecordTimeUpdateData(item_index, tonumber(itemValue), itemName)
    itemValue = GameUtils:formatTimeString(tonumber(itemValue))
  end
  GameUIPlayerInfo:GetFlashObject():InvokeASCallback("_root", "updateItemDate", item_index, itemName, GameUtils.numberAddComma(itemValue))
end
function GameUIPlayerInfo:RecordTimeUpdateData(index, itemValue, itemName)
  if not self.TimeUpdateData then
    self.TimeUpdateData = {}
  end
  local data = {}
  data.index = index
  data.name = itemName
  data.value = itemValue + os.time()
  table.insert(self.TimeUpdateData, data)
end
function GameUIPlayerInfo:updateSumItemData(index)
  local data = self.AllData
  DebugOutPutTable(data, "xcfdd")
  local flashObj = self:GetFlashObject()
  local item = ""
  local itemName = ""
  if data == nil or flashObj == nil then
    return
  end
  if index == 1 then
    flashObj:InvokeASCallback("_root", "updateItemTitleData", index, GameLoader:GetGameText("LC_MENU_PLAYER_INFORMATION_SHIP"))
  elseif index < 2 + #data.fleet then
    itemName = GameLoader:GetGameText("LC_MENU_PLAYER_INFO_" .. data.fleet[index - 1].key)
    flashObj:InvokeASCallback("_root", "updateItemDate", index, itemName, GameUtils.numberAddComma(data.fleet[index - 1].value))
  elseif index == 2 + #data.fleet then
    flashObj:InvokeASCallback("_root", "updateItemTitleData", index, GameLoader:GetGameText("LC_MENU_PLAYER_INFORMATION_FORCE"))
  elseif index < 3 + #data.fleet + #data.force then
    item = data.force[index - 2 - #data.fleet]
    DebugOutPutTable(item, "itemxxx ")
    local itemValue = tonumber(item.value)
    if itemValue == 0 and item.key == "highest_krypton_rank" then
      itemValue = "-"
    end
    itemName = GameLoader:GetGameText("LC_MENU_PLAYER_INFO_" .. item.key)
    if string.find(item.key, "coating") then
      itemName = GameLoader:GetGameText("LC_ITEM_ITEM_NAME_" .. self.CoatingTextID[item.key])
      self:RecordTimeUpdateData(index - 2 - #data.fleet, itemValue, itemName)
      itemValue = GameUtils:formatTimeString(itemValue)
    end
    if item.key == "wdc_buff_time" then
      local wdcPlayerInfo = GameGlobalData:GetData("wdcPlayerInfo")
      if wdcPlayerInfo.player_info.using_rank ~= 0 then
        itemName = GameLoader:GetGameText("LC_MENU_LEAGUE_GRADENAME_" .. wdcPlayerInfo.player_info.using_rank)
      end
      self:RecordTimeUpdateData(index - 2 - #data.fleet, itemValue, itemName)
      itemValue = GameUtils:formatTimeString(itemValue)
    end
    flashObj:InvokeASCallback("_root", "updateItemDate", index, itemName, GameUtils.numberAddComma(item.value))
  elseif index == 3 + #data.fleet + #data.force then
    flashObj:InvokeASCallback("_root", "updateItemTitleData", index, GameLoader:GetGameText("LC_MENU_PLAYER_INFORMATION_RESOURCE"))
  elseif index < 4 + #data.fleet + #data.force + #data.resource then
    item = data.resource[index - 3 - #data.fleet - #data.force]
    itemName = GameLoader:GetGameText("LC_MENU_PLAYER_INFO_" .. item.key)
    flashObj:InvokeASCallback("_root", "updateItemDate", index, itemName, GameUtils.numberAddComma(item.value))
  elseif index == 4 + #data.fleet + #data.force + #data.resource then
    flashObj:InvokeASCallback("_root", "updateItemTitleData", index, GameLoader:GetGameText("LC_MENU_PLAYER_INFORMATION_OTHERS"))
  else
    item = data.other[index - 4 - #data.fleet - #data.force - #data.resource]
    itemName = GameLoader:GetGameText("LC_MENU_PLAYER_INFO_" .. item.key)
    flashObj:InvokeASCallback("_root", "updateItemDate", index, itemName, GameUtils.numberAddComma(item.value))
  end
end
function GameUIPlayerInfo:CheckShowGP()
  local localAppid = ext.GetBundleIdentifier()
  if localAppid == AutoUpdate.bundleIdentifierType.BUNDLE_IDENTIFIER_NORMAL_ANDROID then
    self:GetFlashObject():InvokeASCallback("_root", "showGPbutton", true)
  else
    self:GetFlashObject():InvokeASCallback("_root", "showGPbutton", false)
  end
end
