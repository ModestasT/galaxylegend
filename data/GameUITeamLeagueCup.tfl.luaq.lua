local NetMessageMgr = NetMessageMgr
local NetAPIList = NetAPIList
local GameTimer = GameTimer
local AlertDataList = AlertDataList
local GameGlobalData = GameGlobalData
local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
local GameUIArena = LuaObjectManager:GetLuaObject("GameUIArena")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameObjectFormationBackground = LuaObjectManager:GetLuaObject("GameObjectFormationBackground")
local GameStateArena = GameStateManager.GameStateArena
local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
local GameStateBattlePlay = GameStateManager.GameStateBattlePlay
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local GameUIArena = LuaObjectManager:GetLuaObject("GameUIArena")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameStateBattlePlay = GameStateManager.GameStateBattlePlay
local GameStateArena = GameStateManager.GameStateArena
local GameUIBattleResult = LuaObjectManager:GetLuaObject("GameUIBattleResult")
local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
local GameCommonGoodsList = LuaObjectManager:GetLuaObject("GameCommonGoodsList")
local GameObjectFormationBackground = LuaObjectManager:GetLuaObject("GameObjectFormationBackground")
local GameUIWDStuff = LuaObjectManager:GetLuaObject("GameUIWDStuff")
local GameStateTlcFormation = GameStateManager.GameStateTlcFormation
local GameStateTeamLeagueAtkFormation = GameStateManager.GameStateTeamLeagueAtkFormation
local GameObjectTeamLeagueAtkFormation = LuaObjectManager:GetLuaObject("GameObjectTeamLeagueAtkFormation")
local GameObjectTlcFormation = LuaObjectManager:GetLuaObject("GameObjectTlcFormation")
local GameUICommonDialog = LuaObjectManager:GetLuaObject("GameUICommonDialog")
print("good_1")
TeamLeagueCup = {}
TeamLeagueCup.advanceArenaInfo = nil
TeamLeagueCup.protectTime = nil
TeamLeagueCup.recoveryTime = nil
TeamLeagueCup.accountingTime = nil
TeamLeagueCup.currentEnemyInfo = nil
TeamLeagueCup.searchOpponentinfo = nil
TeamLeagueCup.isShowHelp = false
TeamLeagueCup.isShowStore = false
TeamLeagueCup.isShowMatrix = false
TeamLeagueCup.StateType = {first = 1, other = 2}
TeamLeagueCup.searchOpponentinfoState = TeamLeagueCup.StateType.first
GameUIArena.TlcKingNumber = 200
function GameUIArena:RequestTeamLeagueCupRewards()
  DebugOut("QJtest GameUIArena:RequestTeamLeagueCupRewards( )")
  NetMessageMgr:SendMsg(NetAPIList.tlc_awards_req.Code, nil, GameUIArena.RequestTeamLeagueCupRewardsCallback, true, nil)
end
function GameUIArena.RequestTeamLeagueCupRewardsCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.tlc_awards_req.Code then
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.tlc_awards_ack.Code then
    DebugOut("GameUIArena.RequestTeamLeagueCupRewardsCallback")
    DebugTable(content)
    GameUIArena.teamLeagueCupRewardContent = content
    local SortCallback = function(a, b)
      if a.id < b.id then
        return true
      end
    end
    table.sort(GameUIArena.teamLeagueCupRewardContent.class_awards, SortCallback)
    table.sort(GameUIArena.teamLeagueCupRewardContent.ranking_awards, SortCallback)
    local nowGradeIndex = 1
    local new_class_awards = {}
    local gradeDetail = {}
    for i = 1, #content.class_awards do
      if nowGradeIndex == content.class_awards[i].rank_level then
        table.insert(gradeDetail, content.class_awards[i])
      else
        nowGradeIndex = content.class_awards[i].rank_level
        table.insert(new_class_awards, gradeDetail)
        gradeDetail = {}
        table.insert(gradeDetail, content.class_awards[i])
      end
    end
    table.insert(new_class_awards, gradeDetail)
    GameUIArena.teamLeagueCupRewardContent.class_awards = new_class_awards
    DebugOut("RequestTeamLeagueCupRewardsCallback new_class_awards=")
    DebugTable(new_class_awards)
    if GameUIArena:GetFlashObject() then
      local gradeTitleLeftText = GameLoader:GetGameText("LC_MENU_TEAMLEAGUE_GRADE_REWARDS_BTN")
      local gradeTitleRightText = GameLoader:GetGameText("LC_MENU_LEAGUE_RANK_REWARDS_BUTTON")
      GameUIArena:GetFlashObject():InvokeASCallback("_root", "setLocalText", "TEAM_LEAGUE_CUP_GRADE_LEFT_TEXT", gradeTitleLeftText)
      GameUIArena:GetFlashObject():InvokeASCallback("_root", "setLocalText", "TEAM_LEAGUE_CUP_GRADE_RIGHT_TEXT", gradeTitleRightText)
    end
    GameUIArena:showGradeRewardList_tlc()
    return true
  end
  return false
end
function GameUIArena:showGradeRewardList_tlc()
  if not GameUIArena.teamLeagueCupRewardContent then
    GameUIArena:RequestTeamLeagueCupRewards()
    return
  end
  local flashObj = self:GetFlashObject()
  local rank_level = GameUIArena.teamLeagueCupRewardContent.rank_level
  flashObj:InvokeASCallback("_root", "setLocalText", "LEAGUE_SHORT_OF_CREDIT_TLC", GameLoader:GetGameText("LC_MENU_TEAMLEAGUE_NEED_SCORES_SHORT_CHAR"))
  local player_main_fleet = GameGlobalData:GetFleetInfo(1)
  local leaderlist = GameGlobalData:GetData("leaderlist")
  local curMatrixIndex = GameGlobalData.GlobalData.curMatrixIndex
  local leaderFleet = 1
  local atk = FleetTeamLeagueMatrix:GetAttackInstance()
  if atk then
    leaderFleet = atk:GetLeaderFleet()
  end
  local player_avatar = ""
  if leaderFleet == 1 then
    player_avatar = GameUtils:GetPlayerAvatarWithSex(GameGlobalData:GetUserInfo().sex, player_main_fleet.level)
  else
    player_avatar = GameDataAccessHelper:GetFleetAvatar(leaderFleet, player_main_fleet.level)
  end
  local left_time = GameLoader:GetGameText("LC_MENU_LEAGUE_SEASON_TIME_CHAR") .. GameUtils:formatTimeStringAsPartion(GameUIArena.teamLeagueCupRewardContent.left_time)
  local data = {}
  data.player_avatar = player_avatar
  data.score = GameUIArena.teamLeagueCupRewardContent.score
  data.rank_level = rank_level
  data.left_time = left_time
  data.season = GameUIArena.teamLeagueCupRewardContent.season
  data.TextSeason = string.gsub(GameLoader:GetGameText("LC_MENU_LEAGUE_SEASON_TITLE"), "<number>", data.season)
  data.class_awards = GameUIArena.teamLeagueCupRewardContent.class_awards
  flashObj:InvokeASCallback("_root", "showGradeRewardList_tlc", data)
  GameUIArena:showGradeRewardRankIcon_tlc()
end
function GameUIArena:showGradeRewardRankIcon_tlc()
  local param = {}
  local rankIcons = {}
  local benefitDetail = {}
  local extInfo = {}
  local selfRank_level = GameUIArena.teamLeagueCupRewardContent.rank_level
  for i = 1, #GameUIArena.teamLeagueCupRewardContent.class_awards do
    local rank_img = GameUIArena.teamLeagueCupRewardContent.class_awards[i][1].rank_img
    if i == selfRank_level then
      for _, v in ipairs(GameUIArena.teamLeagueCupRewardContent.class_awards[i]) do
        if v.id == GameUIArena.teamLeagueCupRewardContent.rank then
          rank_img = v.rank_img
          break
        end
      end
    end
    if i < selfRank_level then
      local length = #GameUIArena.teamLeagueCupRewardContent.class_awards[i]
      rank_img = GameUIArena.teamLeagueCupRewardContent.class_awards[i][length].rank_img
    end
    local frame = GameUtils:GetPlayerRankingInWdc(rank_img, {itemIndex = i, rank_img = rank_img}, GameUIArena.setGradeRewardRankIcon_tlc)
    DebugOut("showGradeRewardRankIcon_tlc ", rank_img, frame)
    table.insert(rankIcons, frame)
  end
  for i = 1, #GameUIArena.teamLeagueCupRewardContent.class_awards do
    local benefitGrade = {
      detail = {}
    }
    benefitGrade.id = GameUIArena.teamLeagueCupRewardContent.class_awards[i][1].rank_level
    local awards = GameUIArena.teamLeagueCupRewardContent.class_awards[i][1]
    if selfRank_level == benefitGrade.id then
      for _, v in ipairs(GameUIArena.teamLeagueCupRewardContent.class_awards[i]) do
        if v.id == GameUIArena.teamLeagueCupRewardContent.rank then
          awards = v
          break
        end
      end
    elseif selfRank_level < benefitGrade.id then
      awards = GameUIArena.teamLeagueCupRewardContent.class_awards[i][#GameUIArena.teamLeagueCupRewardContent.class_awards[i]]
    end
    DebugOut("awards.benefits = ")
    DebugTable(awards.benefits)
    for j = 1, #awards.benefits do
      local detail = {}
      extInfo.mc_1 = benefitGrade.id
      extInfo.mc_2 = j
      extInfo.param = awards.benefits[j]
      detail.icon = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(extInfo.param, extInfo, GameUIArena.setGradeItemBenefitIcon_tlc)
      detail.number = GameUtils.numberConversion(tonumber(GameHelper:GetAwardCount(extInfo.param.item_type, extInfo.param.number, extInfo.param.no))) .. "/H"
      table.insert(benefitGrade.detail, detail)
    end
    table.insert(benefitDetail, benefitGrade)
  end
  param.rankIcons = rankIcons
  param.benefitDetail = benefitDetail
  param.nextGradeScoreNeed = 0
  if selfRank_level < 8 then
    param.nextGradeScoreNeed = GameUIArena.teamLeagueCupRewardContent.class_awards[selfRank_level + 1][1].begin_rank - GameUIArena.teamLeagueCupRewardContent.score
  end
  DebugOut("GameUIArena:showGradeRewardRankIcon_tlc = ", selfRank_level, param.nextGradeScoreNeed)
  DebugTable(param)
  self:GetFlashObject():InvokeASCallback("_root", "showGradeRewardRankIcon_tlc", param)
  DebugOut("Show RefreshGradeReward_tlc")
  DebugTable(GameUIArena.needGetRewardList_tlc)
  if GameUIArena.needGetRewardList_tlc and 0 < #GameUIArena.needGetRewardList_tlc then
    self:GetFlashObject():InvokeASCallback("_root", "RefreshGradeReward_tlc", GameUIArena.needGetRewardList_tlc)
  end
end
GameUIArena.needGetRewardList_tlc = {}
function GameUIArena.RefreshGradeReward_tlc(content)
  DebugOut("GameUIArena.RefreshGradeReward_tlc 1 =")
  DebugTable(content)
  local extInfo = {}
  for i = 1, #content.awards do
    local rewardDetail = {}
    rewardDetail.rank_level = content.awards[i].rank_level
    rewardDetail.id = content.awards[i].id
    rewardDetail.itemDetail = {}
    for j = 1, #content.awards[i].awards do
      local detail = {}
      extInfo.rank_level = content.awards[i].rank_level
      extInfo.id = content.awards[i].id
      extInfo.index = j
      extInfo.param = content.awards[i].awards[j]
      local item = content.awards[i].awards[j]
      detail.number = GameUtils.numberConversion(tonumber(GameHelper:GetAwardCount(item.item_type, item.number, item.no)))
      detail.icon = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(extInfo.param, extInfo, GameUIArena.setNeedGetRewardList_tlc)
      table.insert(rewardDetail.itemDetail, detail)
    end
    table.insert(GameUIArena.needGetRewardList_tlc, rewardDetail)
  end
  DebugOut("GameUIArena.RefreshGradeReward_tlc", GameUIArena.mCurrentArenaType)
  DebugTable(GameUIArena.needGetRewardList_tlc)
  if GameUIArena.mCurrentArenaType == GameUIArena.ARENA_TYPE.wdc and GameUIArena:GetFlashObject() and GameUIArena.needGetRewardList_tlc and #GameUIArena.needGetRewardList_tlc > 0 then
    GameUIArena:GetFlashObject():InvokeASCallback("_root", "RefreshGradeReward_tlc", GameUIArena.needGetRewardList_tlc)
  end
  if GameUIArena.needGetRewardList_tlc and #GameUIArena.needGetRewardList_tlc > 0 and GameUIArena:GetFlashObject() then
    GameUIArena:GetFlashObject():InvokeASCallback("_root", "showRedPoint_tlc")
  elseif GameUIArena:GetFlashObject() then
    GameUIArena:GetFlashObject():InvokeASCallback("_root", "hideRedPoint_tlc")
  end
end
function GameUIArena.setNeedGetRewardList_tlc(extInfo)
  local icon = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(extInfo.param, nil, nil)
  for k, v in ipairs(GameUIArena.needGetRewardList_tlc) do
    if extInfo.id == v.id then
      v.itemDetail[extInfo.index].icon = icon
      return
    end
  end
  if GameUIArena.mCurrentArenaType == GameUIArena.ARENA_TYPE.tlc and GameUIArena:GetFlashObject() and GameUIArena.needGetRewardList_tlc and #GameUIArena.needGetRewardList_tlc > 0 then
    GameUIArena:GetFlashObject():InvokeASCallback("_root", "RefreshGradeReward_tlc", GameUIArena.needGetRewardList_tlc)
  end
end
function GameUIArena.setGradeRewardRankIcon_tlc(param)
  local frame = GameUtils:GetPlayerRankingInWdc(param.rank_img, nil, nil)
  if GameUIArena:GetFlashObject() then
    GameUIArena:GetFlashObject():InvokeASCallback("_root", "setGradeRewardRankIcon_tlc", param.itemIndex, frame)
  end
end
function GameUIArena.setGradeItemBenefitIcon_tlc(extInfo)
  local icon = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(extInfo.param, nil, nil)
  local number = GameHelper:GetAwardCount(extInfo.param.item_type, extInfo.param.number, extInfo.param.no)
  if GameUIArena:GetFlashObject() then
    GameUIArena:GetFlashObject():InvokeASCallback("_root", "setGradeItemBenefitDetail_tlc", extInfo.mc_1, extInfo.mc_2, icon, number)
  end
  DebugOut("GameUIArena.setGradeItemBenefitIcon_tlc", extInfo.mc_1, extInfo.mc_2, icon, extInfo.param.number)
end
function GameUIArena:UpdateRankRewardItem_tlc(indexItem)
  local DataString = -1
  local ItemInfo
  local rankRewards = GameUIArena.teamLeagueCupRewardContent.ranking_awards
  if indexItem > 0 and indexItem <= #rankRewards then
    ItemInfo = rankRewards[indexItem]
  end
  if ItemInfo then
    local DataTable = {}
    table.insert(DataTable, ItemInfo.begin_rank)
    table.insert(DataTable, ItemInfo.end_rank)
    table.insert(DataTable, #ItemInfo.awards)
    for i = 1, #ItemInfo.awards do
      local extInfo = ItemInfo.awards[i]
      extInfo.indexItem = indexItem
      extInfo.mcIndex = i
      table.insert(DataTable, GameHelper:GetAwardTypeIconFrameNameSupportDynamic(ItemInfo.awards[i], extInfo, GameUIArena.UpdateRankRewardItemIcon))
      table.insert(DataTable, GameHelper:GetAwardCount(ItemInfo.awards[i].item_type, ItemInfo.awards[i].number, ItemInfo.awards[i].no))
    end
    DataString = table.concat(DataTable, "\001")
  end
  DebugOut("QJtest UpdateRankRewardItem_tlc ", indexItem, DataString)
  self:GetFlashObject():InvokeASCallback("_root", "setRankRewardItemData_tlc", indexItem, DataString)
end
function GameUIArena:showRankRewardList_tlc()
  if not GameUIArena.teamLeagueCupRewardContent or not GameUIArena.teamLeagueCupRewardContent.ranking_awards then
    GameUIArena:RequestTeamLeagueCupRewards()
    return
  end
  DebugOut("GameUIArena:showRankRewardList_tlc")
  DebugTable(GameUIArena.teamLeagueCupRewardContent.ranking_awards)
  self:GetFlashObject():InvokeASCallback("_root", "showRankRewardList_tlc", #GameUIArena.teamLeagueCupRewardContent.ranking_awards)
end
function GameUIArena:showGradeDetail_tlc(index)
  DebugOut("showGradeDetail_tlc_1 " .. tostring(index))
  local parame = {}
  local selfRank_level = GameUIArena.teamLeagueCupRewardContent.rank_level
  local detail = GameUIArena.teamLeagueCupRewardContent.class_awards[index][#GameUIArena.teamLeagueCupRewardContent.class_awards[index]]
  if selfRank_level == index then
    for _, v in ipairs(GameUIArena.teamLeagueCupRewardContent.class_awards[index]) do
      if v.id == GameUIArena.teamLeagueCupRewardContent.rank then
        detail = v
        break
      end
    end
  elseif index > selfRank_level then
    detail = GameUIArena.teamLeagueCupRewardContent.class_awards[index][1]
  end
  local extInfo = {}
  extInfo.mc_1 = "rankIcon"
  extInfo.param = detail.rank_img
  parame.rankIcon = GameUtils:GetPlayerRankingInWdc(detail.rank_img, extInfo, GameUIArena.UpdateGradeDetailIcon)
  parame.gradeName = GameLoader:GetGameText("LC_MENU_TEAMLEAGUE_GRADENAME_" .. detail.rank_name)
  parame.score = GameLoader:GetGameText("LC_MENU_TEAMLEAGUE_GRADE_REQUIRED_SCORES_CHAR") .. " " .. detail.begin_rank
  if index == 8 then
    parame.score = string.gsub(GameLoader:GetGameText("LC_MENU_TLC_TOP_RANK_TITLE"), "<number>", GameUIArena.TlcKingNumber)
  end
  parame.playerTitle = GameLoader:GetGameText("LC_MENU_LEAGUE_PLAYER_TITLE") .. " " .. GameUtils:GetPlayerRank(detail.rank_name, 2)
  if index == 8 and selfRank_level == index and GameUIArena.teamLeagueCupRewardContent.ranking == 1 then
    parame.playerTitle = GameLoader:GetGameText("LC_MENU_LEAGUE_PLAYER_TITLE") .. " " .. GameUtils:GetPlayerRank(detail.rank_name, GameUIArena.teamLeagueCupRewardContent.ranking)
  end
  parame.text_season = GameLoader:GetGameText("LC_MENU_LEAGUE_RANK_SEASON_REWARDS")
  parame.rewardsSpeed = GameLoader:GetGameText("LC_MENU_TEAMLEAGUE_PRODUCTION_SPEED_CHAR")
  parame.buffText_1 = GameLoader:GetGameText("LC_MENU_Equip_param_1")
  parame.buffText_2 = GameLoader:GetGameText("LC_MENU_Equip_param_2")
  parame.buffText_3 = GameLoader:GetGameText("LC_MENU_Equip_param_3")
  parame.buffText_4 = GameLoader:GetGameText("LC_MENU_Equip_param_4")
  parame.buffText_5 = GameLoader:GetGameText("LC_MENU_Equip_param_5")
  parame.buffText_6 = GameLoader:GetGameText("LC_MENU_Equip_param_6")
  parame.buffText_7 = GameLoader:GetGameText("LC_MENU_Equip_param_7")
  parame.buffText_8 = GameLoader:GetGameText("LC_MENU_Equip_param_8")
  parame.textInfo = GameLoader:GetGameText("LC_MENU_LEAGUE_MEDAL_REWARD_INFO")
  parame.benefit = {}
  for i = 1, #detail.benefits do
    local benefitDetail = {}
    extInfo.mc_1 = "reward_1"
    extInfo.mc_2 = i
    extInfo.param = detail.benefits[i]
    benefitDetail.icon = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(detail.benefits[i], extInfo, GameUIArena.UpdateGradeDetailIcon)
    benefitDetail.text = GameUtils.numberConversion(tonumber(GameHelper:GetAwardCount(extInfo.param.item_type, extInfo.param.number, extInfo.param.no))) .. "/H"
    table.insert(parame.benefit, benefitDetail)
  end
  parame.awards = {}
  for i = 1, #detail.awards do
    local awards = {}
    extInfo.mc_1 = "reward_2"
    extInfo.mc_2 = i
    extInfo.param = detail.awards[i]
    awards.icon = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(detail.awards[i], extInfo, GameUIArena.UpdateGradeDetailIcon)
    awards.text = GameUtils.numberConversion(tonumber(GameHelper:GetAwardCount(extInfo.param.item_type, extInfo.param.number, extInfo.param.no)))
    table.insert(parame.awards, awards)
  end
  parame.buffers = detail.buffers
  local tableRankGrades = {}
  DebugOut(" SmallRanIcons ")
  DebugTable(GameUIArena.teamLeagueCupRewardContent.class_awards[index])
  DebugOut(" SmallRanIcons 2222")
  DebugTable(parame.benefit)
  for i = 1, #GameUIArena.teamLeagueCupRewardContent.class_awards[index] do
    local detail = {}
    local rank_img = GameUIArena.teamLeagueCupRewardContent.class_awards[index][i].rank_img
    local rankId = GameUIArena.teamLeagueCupRewardContent.class_awards[index][i].rank_name
    detail.name = GameLoader:GetGameText("LC_MENU_TEAMLEAGUE_GRADENAME_" .. rankId)
    detail.icon = GameUtils:GetPlayerRankingInWdc(rank_img, {id = i, rank_img = rank_img}, GameUIArena.UpdateGradeDetailGradesIcon)
    table.insert(tableRankGrades, detail)
  end
  parame.gradesIcons = tableRankGrades
  parame.gradeNameBottom = GameLoader:GetGameText("LC_MENU_TEAMLEAGUE_SERIES_MEDALNAME_" .. index)
  DebugOut(" SmallRanIcons 2")
  DebugTable(tableRankGrades)
  self:GetFlashObject():InvokeASCallback("_root", "showGradeDetail_tlc", parame)
end
function TeamLeagueCup:OnEnter()
  GameGlobalData:RegisterDataChangeCallback("tlcPlayerInfo", TeamLeagueCup.UpdateTlcInfo)
  NetMessageMgr:SendMsg(NetAPIList.tlc_enter_req.Code, nil, TeamLeagueCup.NetCallbackEnterTeamLeagueCupLayer, true)
  GameGlobalData:RegisterDataChangeCallback("resource", TeamLeagueCup.updateSilverCount)
  GameGlobalData:RegisterDataChangeCallback("tlc_supply", TeamLeagueCup.updateTlcSupplyCallback)
end
function TeamLeagueCup.UpdateTlcInfo()
  if GameUIArena:GetFlashObject() then
    local tlcPlayerInfo = GameGlobalData:GetData("tlcPlayerInfo")
    local content = {}
    content.info = tlcPlayerInfo
    TeamLeagueCup:Init(content)
  end
end
function TeamLeagueCup:SetProtectTime(time)
  local timeStatus = 0
  if time <= 0 then
    timeStatus = 0
  else
    timeStatus = 1
  end
  local timeText = GameLoader:GetGameText("LC_MENU_TEAMLEAGUE_PROTECTION_TIME_CHAR") .. GameUtils:formatTimeString(time)
  if GameUIArena:GetFlashObject() then
    GameUIArena:GetFlashObject():InvokeASCallback("_root", "setProtectTime_tlc", timeStatus, timeText)
  end
end
function TeamLeagueCup:Init(content)
  DebugOut("TeamLeagueCup InitData = ")
  DebugTable(content)
  self.teamLeagueCupInfo = content.info
  if self.teamLeagueCupInfo.player_info.protect_time <= 0 then
    TeamLeagueCup.protectTime = nil
    self:SetProtectTime(self.teamLeagueCupInfo.player_info.protect_time)
  else
    TeamLeagueCup.protectTime = os.time() + self.teamLeagueCupInfo.player_info.protect_time
  end
  TeamLeagueCup.recoveryTime = os.time() + self.teamLeagueCupInfo.player_info.final_time
  TeamLeagueCup.accountingTime = os.time() + self.teamLeagueCupInfo.player_info.end_time
  self:SetDanAndintegrateInfo(self.teamLeagueCupInfo.player_info.rank_img, self.teamLeagueCupInfo.player_info.point, self.teamLeagueCupInfo.player_info.next_rank_point, self.teamLeagueCupInfo.player_info.rank_point)
  local count = GameGlobalData:GetData("resource")
  DebugOut("TeamLeagueCup_init count = ")
  DebugTable(count)
  local wdcSourceCount = GameUtils.numberConversion(count.silver)
  local currentForce = GameUtils.numberConversion(self.teamLeagueCupInfo.player_info.force)
  local hoursSilver = GameUtils.numberConversion(self.teamLeagueCupInfo.player_info.res_per_hour)
  self:SetAdvanceOutputSourceInfo(wdcSourceCount, currentForce)
  local current_supply = GameUtils.numberConversion(count.tlc_supply)
  self:SetAdvanceEnergy(self.teamLeagueCupInfo.search_cost, self.teamLeagueCupInfo.max_supply, current_supply, self.teamLeagueCupInfo.can_buy, self.teamLeagueCupInfo.player_info.end_time)
  self:SetCurrencyItem()
  GameUIArena.TlcKingNumber = self.teamLeagueCupInfo.king_num
  if TeamLeagueCup.searchOpponentinfoState == TeamLeagueCup.StateType.first then
    DebugOut("123232")
    local buttonText = GameLoader:GetGameText("LC_MENU_TEAMLEAGUE_SEARCH_RIVAL_BTN")
    TeamLeagueCup:SetSearchOpponentButtonText(buttonText)
  else
    DebugOut("12222")
    local buttonText = GameLoader:GetGameText("LC_MENU_TLC_CHANGE_OPPONENT")
    TeamLeagueCup:SetSearchOpponentButtonText(buttonText)
  end
  if self.teamLeagueCupInfo.pop_report then
    GameUIArena.nowReportTypeTlc = "defend_list"
    TeamLeagueCup:RequestGalaxyArenaReport()
  end
end
GameUIArena.tlc_historyReport = nil
GameUIArena.nowReportTypeTlc = "atk_list"
function TeamLeagueCup:RequestGalaxyArenaReport()
  NetMessageMgr:SendMsg(NetAPIList.tlc_report_req.Code, nil, TeamLeagueCup.RequestGalaxyArenaReportCallback, true, nil)
end
function TeamLeagueCup.RequestGalaxyArenaReportCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.tlc_report_req.Code then
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.tlc_report_ack.Code then
    GameUIArena.tlc_historyReport = content
    local SortCallback = function(a, b)
      if a.fight_time < b.fight_time then
        return true
      end
    end
    table.sort(GameUIArena.tlc_historyReport.atk_list, SortCallback)
    table.sort(GameUIArena.tlc_historyReport.defend_list, SortCallback)
    DebugOut(" GameUIArena.RequestGalaxyArenaReportCallback ")
    DebugTable(content)
    TeamLeagueCup:AnimationMoveInGalaxyArenaReport()
    return true
  end
  return false
end
function TeamLeagueCup:replayGalaxyArenaBattle(indexItem)
  local report_data = GameUIArena.tlc_historyReport[GameUIArena.nowReportTypeTlc][indexItem]
  if GameStateManager:GetCurrentGameState().fightRoundData then
    GameStateManager:GetCurrentGameState().fightRoundData = nil
  end
  if report_data then
    NetMessageMgr:SendMsg(NetAPIList.tlc_report_detail_req.Code, {
      id = tonumber(report_data.report_id)
    }, TeamLeagueCup.RequestReplayGalaxyArenaBattleCallback, true, nil)
  end
end
function GameUIArena:checkStat_tlc()
  NetMessageMgr:SendMsg(NetAPIList.tlc_fight_status_req.Code, nil, TeamLeagueCup.getProtectTimeStartBattleNetCallback, true, nil)
end
GameUIArena.revengeIndex_tlc = nil
function TeamLeagueCup.revengeFromReport()
  local indexItem = GameUIArena.revengeIndex_tlc
  local report_data = GameUIArena.tlc_historyReport[GameUIArena.nowReportTypeTlc][indexItem]
  if report_data then
    local enemyInfo = {}
    enemyInfo.avatar = GameUtils:GetPlayerAvatar(report_data.enemy.sex, report_data.enemy.level)
    enemyInfo.name = report_data.enemy.user_name
    enemyInfo.mSex = report_data.enemy.sex
    enemyInfo.force = tonumber(report_data.enemy.force)
    GameUIArena.enemyInfo_tlc = enemyInfo
    local ArenaDefender = {
      defender = {}
    }
    ArenaDefender.defender.server_id = report_data.enemy.server_id
    ArenaDefender.defender.user_id = tonumber(report_data.enemy.user_id)
    ArenaDefender.defender.force = tonumber(report_data.enemy.force)
    ArenaDefender.defender.revenge_id = tonumber(report_data.report_id)
    TeamLeagueCup.currentEnemyInfo = ArenaDefender
    DebugOut("GameUIArena:revengeFromReport")
    DebugTable(TeamLeagueCup.currentEnemyInfo)
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateTeamLeagueAtkFormation)
    GameUIArena.currentMenu = "GalaxyArenaReport_tlc"
  end
end
function TeamLeagueCup.getProtectTimeStartBattleNetCallback(msgType, content)
  if msgType == NetAPIList.tlc_fight_status_ack.Code then
    if content.protect_time > 0 then
      local text_title = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
      local info = GameLoader:GetGameText("LC_MENU_LEAGUE_ATTACK_INFO")
      GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
      GameUIMessageDialog:SetYesButton(TeamLeagueCup.revengeFromReport)
      GameUIMessageDialog:SetNoButton(nil)
      GameUIMessageDialog:Display(text_title, info)
    else
      TeamLeagueCup.revengeFromReport()
    end
    return true
  end
  return false
end
function TeamLeagueCup.RequestReplayGalaxyArenaBattleCallback(msgtype, content)
  if msgtype == NetAPIList.tlc_report_detail_ack.Code then
    GameUIArena.currentMenu = "GalaxyArenaReport_tlc"
    local GameObjectBattleReplay = LuaObjectManager:GetLuaObject("GameObjectBattleReplay")
    GameObjectBattleReplay.isGroupBattle = true
    GameObjectBattleReplay.curGroupBattleIndex = 1
    GameObjectBattleReplay.GroupBattleReportArr = {}
    GameObjectBattleReplay.GroupBattleReportArr[1] = content
    GameStateBattlePlay:InitBattle(GameStateBattlePlay.MODE_HISTORY, content.headers[1].report, 3, nil)
    GameStateBattlePlay:RegisterOverCallback(function()
      GameStateManager:SetCurrentGameState(GameStateArena)
    end, nil)
    GameStateManager:SetCurrentGameState(GameStateBattlePlay)
    return true
  end
  return false
end
function TeamLeagueCup:UpdateReportItem(index_item)
  local history_data = GameUIArena.tlc_historyReport[GameUIArena.nowReportTypeTlc][index_item]
  local text_timeago = -1
  local text_result = -1
  local score_change = 0
  DebugOut(" TeamLeagueCup:UpdateReportItem = ")
  DebugTable(history_data)
  if history_data then
    text_timeago = history_data.fight_time
    if history_data.is_win and GameUIArena.nowReportTypeTlc == "atk_list" then
      text_result = GameLoader:GetGameText("LC_MENU_YOU_VS_OPPONENT_WIN")
      text_result = string.format(text_result, GameUtils:GetUserDisplayName(history_data.enemy.user_name))
      score_change = history_data.score_after - history_data.score_before
    elseif not history_data.is_win and GameUIArena.nowReportTypeTlc == "atk_list" then
      text_result = GameLoader:GetGameText("LC_MENU_YOU_VS_OPPONENT_LOSE")
      text_result = string.format(text_result, GameUtils:GetUserDisplayName(history_data.enemy.user_name))
      score_change = history_data.score_after - history_data.score_before
    elseif history_data.is_win and GameUIArena.nowReportTypeTlc == "defend_list" then
      text_result = GameLoader:GetGameText("LC_MENU_OPPONENT_VS_YOU_LOSE")
      text_result = string.format(text_result, GameUtils:GetUserDisplayName(history_data.enemy.user_name))
      score_change = history_data.score_after - history_data.score_before
    elseif not history_data.is_win and GameUIArena.nowReportTypeTlc == "defend_list" then
      text_result = GameLoader:GetGameText("LC_MENU_OPPONENT_VS_YOU_WIN")
      text_result = string.format(text_result, GameUtils:GetUserDisplayName(history_data.enemy.user_name))
      score_change = history_data.score_after - history_data.score_before
    else
      assert(false)
    end
  else
    text_timeago = GameLoader:GetGameText("LC_MENU_NO_DATA_CHAR")
  end
  local isEndTime = TeamLeagueCup:IsEndTime()
  local buttonText = GameLoader:GetGameText("LC_MENU_LEAGUE_REVENGE")
  GameUIArena:GetFlashObject():InvokeASCallback("_root", "updateReportItem_tlc", index_item, text_result, text_timeago, score_change, history_data.can_revenge, buttonText, isEndTime, GameUIArena.nowReportTypeTlc)
end
function TeamLeagueCup:IsEndTime()
  if self.teamLeagueCupInfo.player_info.end_time > 0 then
    return true
  end
  return false
end
function TeamLeagueCup:AnimationMoveInGalaxyArenaReport()
  if not GameUIArena.tlc_historyReport then
    TeamLeagueCup:RequestGalaxyArenaReport()
  end
  local item_count = #GameUIArena.tlc_historyReport[GameUIArena.nowReportTypeTlc]
  DebugOut("AnimationMoveInGalaxyArenaReport " .. GameUIArena.nowReportTypeTlc)
  GameUIArena:GetFlashObject():InvokeASCallback("_root", "GalaxyArenaMoveInReportBox_tlc", item_count, GameUIArena.nowReportTypeTlc)
end
function TeamLeagueCup:SetSearchOpponentButtonText(txt)
  if GameUIArena:GetFlashObject() then
    GameUIArena:GetFlashObject():InvokeASCallback("_root", "setSearchButtonText_tlc", txt)
  end
end
function TeamLeagueCup:SetDanAndintegrateInfo(dan, currentScore, nextScore, preScore)
  local extInfo = {}
  extInfo.ranking = dan
  local rankingFrame = GameUtils:GetPlayerRankingInWdc(dan, extInfo, TeamLeagueCup.DownloadRaningImageCallback)
  GameUIArena:GetFlashObject():InvokeASCallback("_root", "setDanAndintegrateInfo_as_tlc", rankingFrame, currentScore, nextScore, preScore)
end
function TeamLeagueCup:SetAdvanceOutputSourceInfo(token, per_hour)
  local tokenCountText = token
  local per_hourCountText = per_hour
  local levelInfo = GameGlobalData:GetData("levelinfo")
  local lvltxt = GameLoader:GetGameText("LC_MENU_Level") .. tostring(levelInfo.level)
  GameUIArena:GetFlashObject():InvokeASCallback("_root", "setAdvanceOutputSourceInfo_as_tlc", lvltxt, per_hourCountText)
  local player_main_fleet = GameGlobalData:GetFleetInfo(1)
  local leaderFleet = 1
  local atk = FleetTeamLeagueMatrix:GetAttackInstance()
  if atk then
    leaderFleet = atk:GetLeaderFleet()
  end
  local player_avatar = ""
  if leaderFleet == 1 then
    player_avatar = GameUtils:GetPlayerAvatarWithSex(GameGlobalData:GetUserInfo().sex, player_main_fleet.level)
  else
    player_avatar = GameDataAccessHelper:GetFleetAvatar(leaderFleet, player_main_fleet.level)
  end
  GameUIArena:GetFlashObject():InvokeASCallback("_root", "setPlayerAvatar_tlc", player_avatar)
end
function TeamLeagueCup:SetAdvanceEnergy(matchCost, costMax, costCurrent, canBuy, finalTime)
  local name = GameLoader:GetGameText("")
  local isCanSearch = true
  if tonumber(matchCost) > tonumber(costCurrent) or finalTime > 0 then
    isCanSearch = false
  end
  local energyText = GameUIArena:GetEnergyColor(costCurrent) .. "/" .. costMax
  DebugOut("SetAdvanceEnergy_energyText = " .. energyText)
  local hasCooldownTxt = false
  hasCooldownTxt = GameUIArena.TeamLeaqueCup_cd_time and GameUIArena.TeamLeaqueCup_cd_time > os.time()
  GameUIArena:GetFlashObject():InvokeASCallback("_root", "setAdvanceEnergy_as_tlc", matchCost, name, canBuy, isCanSearch, energyText)
  GameUIArena:UpdateTeamLeagueCupCDTime(true)
end
function TeamLeagueCup:SetCurrencyItem()
  local itemFrame = GameHelper:GetAwardTypeIconFrameName("silver", nil, nil)
  if GameUIArena:GetFlashObject() then
    GameUIArena:GetFlashObject():InvokeASCallback("_root", "setCurrencyFram_tlc", itemFrame)
  end
end
function TeamLeagueCup:SetSearchOpponentStates()
  if TeamLeagueCup.searchOpponentinfo then
    TeamLeagueCup.searchOpponentinfoState = TeamLeagueCup.StateType.other
  else
    TeamLeagueCup.searchOpponentinfoState = TeamLeagueCup.StateType.first
  end
end
function TeamLeagueCup:MoveIn()
  if GameUIArena:GetFlashObject() then
    GameUIArena:GetFlashObject():InvokeASCallback("_root", "MoveInTeamLeagueCupLayer")
  end
  DebugOut("TeamLeagueCupMoveIn")
  if TutorialQuestManager.QuestTutorialTlcSearch:IsActive() then
    GameUIArena:GetFlashObject():InvokeASCallback("_root", "showTutorialTlcArenaSearch")
    GameUICommonDialog:ForcePlayStory({9902})
  end
end
function TeamLeagueCup.NetCallbackEnterTLCWaitHonorWallData(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.tlc_honor_wall_req.Code then
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.tlc_honor_wall_ack.Code then
    DebugTable(content)
    GameUIArena.HonorList_tlc = content
    if not GameUIArena.HonorList_tlc.self or #GameUIArena.HonorList_tlc.self < 1 then
      GameUIArena:GetFlashObject():InvokeASCallback("_root", "SetHonorEntryEnable", false)
    else
      GameUIArena:GetFlashObject():InvokeASCallback("_root", "SetHonorEntryEnable", true)
    end
    TeamLeagueCup:MoveIn()
    if TeamLeagueCup.searchOpponentinfo then
      DebugOut("shufsd")
      TeamLeagueCup:ShowPlayerDetail()
      GameUIArena:GetFlashObject():InvokeASCallback("_root", "stopSearchAnimation_tlc")
    end
    if GameUIArena.currentMenu == "GalaxyArenaReport_tlc" then
      TeamLeagueCup:RequestGalaxyArenaReport()
    end
    return true
  end
  return false
end
function TeamLeagueCup.NetCallbackEnterTeamLeagueCupLayer(msgType, content)
  if msgType == NetAPIList.tlc_enter_ack.Code then
    TeamLeagueCup:SetSearchOpponentStates()
    local function initFunc()
      TeamLeagueCup:Init(content)
      NetMessageMgr:SendMsg(NetAPIList.tlc_honor_wall_req.Code, nil, TeamLeagueCup.NetCallbackEnterTLCWaitHonorWallData, true, nil)
    end
    FleetTeamLeagueMatrix:GetMatrixsReq(TEAMLEAGUE_MATRIX_ATTACK, initFunc)
    return true
  end
  return false
end
function TeamLeagueCup.updateSilverCount()
  local count = GameGlobalData:GetData("resource")
  local silverCount = GameUtils.numberConversion(count.silver)
  local levelInfo = GameGlobalData:GetData("levelinfo")
  local lvltxt = GameLoader:GetGameText("LC_MENU_Level") .. tostring(levelInfo.level)
  if GameUIArena:GetFlashObject() then
    GameUIArena:GetFlashObject():InvokeASCallback("_root", "setCurrencyCount_tlc", lvltxt)
  end
end
function GameUIArena:collectReward_tlc(index)
  NetMessageMgr:SendMsg(NetAPIList.tlc_awards_get_req.Code, {id = index}, GameUIArena.collectRewardCallback_tlc, true, nil)
end
function GameUIArena.collectRewardCallback_tlc(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.tlc_awards_get_req.Code then
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.tlc_awards_get_ack.Code then
    if content.code == 0 then
      GameUIArena:GetFlashObject():InvokeASCallback("_root", "SetGradeItemBenefit_tlc", content.id)
      for i = #GameUIArena.needGetRewardList_tlc, 1, -1 do
        if GameUIArena.needGetRewardList_tlc[i].rank_level == content.id then
          table.remove(GameUIArena.needGetRewardList_tlc, i)
        end
      end
    end
    if #GameUIArena.needGetRewardList_tlc == 0 then
      GameUIArena:GetFlashObject():InvokeASCallback("_root", "hideRedPoint_tlc")
    end
    return true
  end
  return false
end
function GameUIArena:updateTlcSupply()
  if TeamLeagueCup.teamLeagueCupInfo == nil then
    return
  end
  if GameUIArena:GetFlashObject() then
    local resourceCount = GameGlobalData:GetData("resource")
    local energyText = GameUIArena:GetEnergyColor(resourceCount.tlc_supply) .. "/" .. TeamLeagueCup.teamLeagueCupInfo.max_supply
    if GameUIArena.teamLeaqueCupEnterData.status ~= 2 and resourceCount.tlc_supply >= TeamLeagueCup.teamLeagueCupInfo.max_supply and self:GetFlashObject() then
      self.TeamLeaqueCup_cd_time = nil
      GameUIArena:GetFlashObject():InvokeASCallback("_root", "SetEnterTeamLeagueCupButtonTime", "", false, GameUIArena.teamLeaqueCupEnterData.status == 2)
    end
    GameUIArena:GetFlashObject():InvokeASCallback("_root", "updateTlcSupply", energyText)
    if TeamLeagueCup.teamLeagueCupInfo then
      local isCanSearch = true
      local matchCost = TeamLeagueCup.teamLeagueCupInfo.search_cost
      local costCurrent = resourceCount.tlc_supply
      local finalTime = TeamLeagueCup.teamLeagueCupInfo.player_info.end_time
      if tonumber(matchCost) > tonumber(costCurrent) or finalTime > 0 then
        isCanSearch = false
      end
      GameUIArena:GetFlashObject():InvokeASCallback("_root", "setSearchButtonState_tlc", isCanSearch)
    end
  end
end
function TeamLeagueCup.updateTlcSupplyCallback()
  if TeamLeagueCup.teamLeagueCupInfo == nil then
    return
  end
  if GameUIArena:GetFlashObject() then
    local resourceCount = GameGlobalData:GetData("resource")
    local energyText = GameUIArena:GetEnergyColor(resourceCount.tlc_supply) .. "/" .. TeamLeagueCup.teamLeagueCupInfo.max_supply
    DebugOut("energyText == " .. energyText)
    GameUIArena:updateTlcSupply()
    GameUIArena:UpdateTeamLeagueCupCDTime(resourceCount.tlc_supply < TeamLeagueCup.teamLeagueCupInfo.max_supply)
    if TeamLeagueCup.teamLeagueCupInfo then
      local isCanSearch = true
      local matchCost = TeamLeagueCup.teamLeagueCupInfo.search_cost
      local costCurrent = resourceCount.tlc_supply
      local finalTime = TeamLeagueCup.teamLeagueCupInfo.player_info.end_time
      if tonumber(matchCost) > tonumber(costCurrent) or finalTime > 0 then
        isCanSearch = false
      end
      GameUIArena:GetFlashObject():InvokeASCallback("_root", "setSearchButtonState_tlc", isCanSearch)
    end
  end
end
function TeamLeagueCup.onEnterFormation()
  TeamLeagueCup:RequireMatrix()
end
function TeamLeagueCup.getProtectTimeMoveInFormationNetCallback(msgType, content)
  if msgType == NetAPIList.tlc_fight_status_ack.Code then
    if content.protect_time > 0 then
      local text_title = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
      local info = GameLoader:GetGameText("LC_MENU_LEAGUE_ATTACK_INFO")
      GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
      GameUIMessageDialog:SetYesButton(TeamLeagueCup.onEnterFormation)
      GameUIMessageDialog:SetNoButton(nil)
      GameUIMessageDialog:Display(text_title, info)
    else
      TeamLeagueCup:RequireMatrix()
    end
    return true
  end
  return false
end
function TeamLeagueCup:SetEnemyInfo(serverid, userid)
  local enemyInfo = {}
  DebugOut("serverid = " .. serverid .. " userid = " .. userid)
  DebugTable(self.searchOpponentinfo)
  local currentEnemy = TeamLeagueCup:GetOpponentPlayer(serverid, userid)
  DebugOut("currentEnemy = ")
  DebugTable(currentEnemy)
  if currentEnemy.defender.icon == 0 or currentEnemy.defender.icon == 1 then
    enemyInfo.avatar = GameUtils:GetPlayerAvatar(currentEnemy.defender.sex, 0)
  else
    enemyInfo.avatar = GameDataAccessHelper:GetFleetAvatar(currentEnemy.defender.icon, 0)
  end
  enemyInfo.name = currentEnemy.defender.user_name
  enemyInfo.mSex = currentEnemy.defender.sex
  enemyInfo.force = currentEnemy.defender.force
  GameUIArena.enemyInfo_tlc = enemyInfo
end
function TeamLeagueCup:RequireMatrix()
  local data = LuaUtils:string_split(self.currentPlayer, "\001")
  local param = {
    server_id = data[1],
    user_id = data[2]
  }
  DebugOut("param = ")
  DebugTable(param)
  local currentForce = GameUtils.numberConversion(self.teamLeagueCupInfo.player_info.force)
  self:SetEnemyInfo(tonumber(data[1]), tonumber(data[2]))
  TeamLeagueCup.currentEnemyInfo = TeamLeagueCup:GetOpponentPlayer(tonumber(data[1]), tonumber(data[2]))
  local attackMatrix = FleetTeamLeagueMatrix:GetAttackInstance()
  local function callback()
    GameStateTeamLeagueAtkFormation:EnterState()
  end
  FleetTeamLeagueMatrix:GetMatrixsReq(TEAMLEAGUE_MATRIX_ATTACK, callback)
end
function TeamLeagueCup:CleanSearchInfo()
  TeamLeagueCup.searchOpponentinfo = nil
  TeamLeagueCup.searchOpponentinfoState = TeamLeagueCup.StateType.first
end
function TeamLeagueCup:GetOpponentPlayer(serverid, userid)
  for k, v in ipairs(TeamLeagueCup.searchOpponentinfo) do
    if serverid == tonumber(v.defender.server_id) and userid == tonumber(v.defender.user_id) then
      return v
    end
  end
end
function TeamLeagueCup:SearchOpponentAction()
  NetMessageMgr:SendMsg(NetAPIList.tlc_fight_status_req.Code, nil, TeamLeagueCup.getProtectTimeNetCallback, true, nil)
end
function TeamLeagueCup:SearchOpponentInfo()
  NetMessageMgr:SendMsg(NetAPIList.tlc_match_req.Code, nil, TeamLeagueCup.SearchOpponentNetCallback, true)
end
function TeamLeagueCup.SearchOpponentNetCallback(msgType, content)
  if msgType == NetAPIList.tlc_match_ack.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      return true
    end
    TeamLeagueCup:SetSearchOpponent(content)
    return true
  end
  return false
end
function TeamLeagueCup:ShowPlayerDetailAnimBegin()
  if #TeamLeagueCup.searchOpponentinfo == 1 then
    TeamLeagueCup:SetPlayerInfo(self.searchOpponentinfo[1], "_root.TeamLeagueCup.Pop_RivalAll.Pop_Rival5")
  else
    TeamLeagueCup:SetPlayerInfo(self.searchOpponentinfo[1], "_root.TeamLeagueCup.Pop_RivalAll.Pop_Rival4")
    TeamLeagueCup:SetPlayerInfo(self.searchOpponentinfo[2], "_root.TeamLeagueCup.Pop_RivalAll.Pop_Rival3")
  end
end
function TeamLeagueCup:ShowPlayerDetail()
  DebugOut("TeamLeagueCup:ShowPlayerDetail")
  DebugTable(TeamLeagueCup.searchOpponentinfo)
  if GameUIArena:GetFlashObject() then
    GameUIArena:GetFlashObject():InvokeASCallback("_root", "HideArenaOpponentLayer_tlc")
  end
  if #TeamLeagueCup.searchOpponentinfo == 1 then
    TeamLeagueCup:SetPlayerInfo(self.searchOpponentinfo[1], "_root.TeamLeagueCup.Pop_RivalAll.Pop_Rival")
    if TutorialQuestManager.QuestTutorialTlcChallenge:IsActive() then
      DebugOut("showTutorialTlcArenaChallenge_lua")
      GameUIArena:GetFlashObject():InvokeASCallback("_root", "showTutorialTlcArenaChallenge1")
    end
  else
    TeamLeagueCup:SetPlayerInfo(self.searchOpponentinfo[1], "_root.TeamLeagueCup.Pop_RivalAll.Pop_Rival1")
    TeamLeagueCup:SetPlayerInfo(self.searchOpponentinfo[2], "_root.TeamLeagueCup.Pop_RivalAll.Pop_Rival2")
    if TutorialQuestManager.QuestTutorialTlcChallenge:IsActive() then
      DebugOut("showTutorialTlcArenaChallenge_lua")
      GameUIArena:GetFlashObject():InvokeASCallback("_root", "showTutorialTlcArenaChallenge2")
    end
  end
end
function TeamLeagueCup.DownloadOpponetRanking(extInfo)
  if GameUIArena:GetFlashObject() then
    local rank = GameUtils:GetPlayerRankingInWdc(extInfo.rank_img, nil, nil)
    GameUIArena:GetFlashObject():InvokeASCallback("_root", "setOponnentPlayerRank_tlc", extInfo.asObject, rank)
  end
end
function TeamLeagueCup:SetPlayerInfo(playerInfo, asObject)
  local _playerInfo = {}
  DebugOut("fortp")
  DebugTable(playerInfo)
  _playerInfo.username = playerInfo.defender.user_name
  _playerInfo.level = GameLoader:GetGameText("LC_MENU_Level") .. playerInfo.defender.level
  _playerInfo.force = playerInfo.defender.force
  if playerInfo.defender.icon == 0 or playerInfo.defender.icon == 1 then
    _playerInfo.avatar = GameUtils:GetPlayerAvatar(playerInfo.defender.sex, 0)
  else
    _playerInfo.avatar = GameDataAccessHelper:GetFleetAvatar(playerInfo.defender.icon, 0)
  end
  local extInfo = {}
  extInfo.rank_img = playerInfo.defender.rank_img
  extInfo.asObject = asObject
  _playerInfo.ranking = GameUtils:GetPlayerRankingInWdc(playerInfo.defender.rank_img, extInfo, TeamLeagueCup.DownloadOpponetRanking)
  _playerInfo.fleetcommands = ""
  _playerInfo.fleetAvatars = ""
  for j = 1, 3 do
    for i = 1, 5 do
      if j > #playerInfo.defender.fleets or i > #playerInfo.defender.fleets[j].fleets then
        _playerInfo.fleetcommands = _playerInfo.fleetcommands .. "" .. "\001"
        _playerInfo.fleetAvatars = _playerInfo.fleetAvatars .. "" .. "\001"
      else
        local v = playerInfo.defender.fleets[j].fleets[i]
        if v.identity ~= 1 then
          _playerInfo.fleetcommands = _playerInfo.fleetcommands .. GameDataAccessHelper:GetCommanderColorFrame(v.identity, v.level) .. "\001"
          _playerInfo.fleetAvatars = _playerInfo.fleetAvatars .. GameDataAccessHelper:GetFleetAvatar(v.identity, v.level) .. "\001"
        else
          local leaderFrame, leaderAvatar
          if playerInfo.defender.icon == 0 or playerInfo.defender.icon == 1 then
            leaderFrame = GameDataAccessHelper:GetCommanderColorFrame(v.identity, v.level)
            leaderAvatar = GameUtils:GetPlayerAvatar(playerInfo.defender.sex, v.level)
          else
            leaderFrame = GameDataAccessHelper:GetCommanderColorFrame(playerInfo.defender.icon, v.level)
            leaderAvatar = GameDataAccessHelper:GetFleetAvatar(playerInfo.defender.icon, v.level)
          end
          _playerInfo.fleetcommands = _playerInfo.fleetcommands .. leaderFrame .. "\001"
          _playerInfo.fleetAvatars = _playerInfo.fleetAvatars .. leaderAvatar .. "\001"
        end
      end
    end
  end
  if 0 <= playerInfo.win_score then
    _playerInfo.winAddForce = "+" .. playerInfo.win_score
  else
    _playerInfo.winAddForce = playerInfo.win_score
  end
  if 0 <= playerInfo.lose_score then
    _playerInfo.faileForce = "+" .. playerInfo.lose_score
  else
    _playerInfo.faileForce = playerInfo.lose_score
  end
  _playerInfo.serverid = playerInfo.defender.server_id
  _playerInfo.userid = playerInfo.defender.user_id
  if GameUIArena:GetFlashObject() then
    GameUIArena:GetFlashObject():InvokeASCallback("_root", "setPlayerInfo_as_tlc", _playerInfo, asObject)
  end
end
function TeamLeagueCup:SetSearchOpponent(content)
  DebugOut("match_resout = ")
  DebugTable(content)
  TeamLeagueCup.searchOpponentinfo = content.challenge_list
  if GameUIArena:GetFlashObject() then
    if TeamLeagueCup.searchOpponentinfoState == TeamLeagueCup.StateType.first then
      GameUIArena:GetFlashObject():InvokeASCallback("_root", "showSearchAnimation_tlc")
      TeamLeagueCup:ShowPlayerDetail()
    else
      TeamLeagueCup:ShowPlayerDetailAnimBegin()
      GameUIArena:GetFlashObject():InvokeASCallback("_root", "showSearchAnimation2_tlc")
    end
  end
  TeamLeagueCup:SetSearchOpponentStates()
  local buttonText = GameLoader:GetGameText("LC_MENU_TLC_CHANGE_OPPONENT")
  TeamLeagueCup:SetSearchOpponentButtonText(buttonText)
end
function TeamLeagueCup.SearchOpponentButtonCallback()
  TeamLeagueCup:SearchOpponent()
end
function TeamLeagueCup:SearchOpponent()
  self:SetSearchOpponentStates()
  self:SearchOpponentInfo()
end
function TeamLeagueCup.getProtectTimeNetCallback(msgType, content)
  if msgType == NetAPIList.tlc_fight_status_ack.Code then
    if content.protect_time > 0 then
      local text_title = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
      local info = GameLoader:GetGameText("LC_MENU_LEAGUE_ATTACK_INFO")
      GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
      GameUIMessageDialog:SetYesButton(TeamLeagueCup.SearchOpponentButtonCallback)
      GameUIMessageDialog:SetNoButton(nil)
      GameUIMessageDialog:Display(text_title, info)
    else
      TeamLeagueCup:SearchOpponent()
    end
    return true
  end
  return true
end
function TeamLeagueCup:MoveOutAnimation()
  if GameUIArena:GetFlashObject() then
    GameUIArena:GetFlashObject():InvokeASCallback("_root", "moveOutAdvancedLayer_tlc")
  end
  TeamLeagueCup:SendQuitReq()
end
function TeamLeagueCup:SendQuitReq()
  DebugOut("send tlc_quit_req!!!")
  NetMessageMgr:SendMsg(NetAPIList.tlc_quit_req.Code, nil, nil, false, nil)
end
function TeamLeagueCup.CloseButtonYesCallback()
  TeamLeagueCup:CleanSearchInfo()
  TeamLeagueCup:MoveOutAnimation()
  NetMessageMgr:SendMsg(NetAPIList.tlc_enemy_release_req.Code, nil, TeamLeagueCup.releaseEnemyCallback, false, nil)
end
function TeamLeagueCup.releaseEnemyCallback(msgType, content)
  if content.api == NetAPIList.tlc_enemy_release_req.Code and msgType == NetAPIList.common_ack.Code then
    return true
  end
  return false
end
function TeamLeagueCup:OnFSCommand(cmd, arg)
  if "close_tlc" == cmd then
    if TeamLeagueCup.searchOpponentinfo then
      local text_title = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
      local info = GameLoader:GetGameText("LC_MENU_LEAGUE_QUIT_INFO")
      GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
      GameUIMessageDialog:SetYesButton(TeamLeagueCup.CloseButtonYesCallback)
      GameUIMessageDialog:SetNoButton(nil)
      GameUIMessageDialog:Display(text_title, info)
    else
      TeamLeagueCup:MoveOutAnimation()
    end
  end
  DebugOut("TeamLeagueCup:OnFSCommand " .. cmd)
  if "tlc_anim_play_over" == cmd then
    DebugOut("tlc_anim_play_over")
  end
  if "moveOutOver_tlc" == cmd then
    GameStateArena:Quit()
  end
  if "SearchOpponent_tlc" == cmd then
    if TutorialQuestManager.QuestTutorialTlcSearch:IsActive() then
      TutorialQuestManager.QuestTutorialTlcSearch:SetFinish(true)
      TutorialQuestManager.QuestTutorialTlcChallenge:SetActive(true)
      GameUIArena:GetFlashObject():InvokeASCallback("_root", "hideTutorialTlcArenaSearch")
    end
    TeamLeagueCup:SearchOpponentAction()
  end
  if "tlc_right_anim_over" == cmd then
    TeamLeagueCup:ShowPlayerDetail()
  end
  if "advanceArena_store_tlc" == cmd then
    local exchange = {}
    exchange.items = {}
    table.insert(exchange.items, "silver")
    GameCommonGoodsList:SetID(exchange)
    GameCommonGoodsList:SetStoreType(GameCommonGoodsList.TYPE.wdc)
    GameStateManager:GetCurrentGameState():AddObject(GameCommonGoodsList)
    self.isShowStore = true
  end
  if "change_matrix_tlc" == cmd then
    local uiText = {}
    uiText.title = GameLoader:GetGameText("LC_MENU_TEAMLEAGUE_FORMATION_CHOICE_TITLE_CHAR")
    uiText.atkBtn = GameLoader:GetGameText("LC_MENU_TEAMLEAGUE_FORMATION_CHOICE_ATTACK_BTN")
    uiText.defBtn = GameLoader:GetGameText("LC_MENU_TEAMLEAGUE_FORMATION_CHOICE_DEFENSIVE_BTN")
    GameUIArena:GetFlashObject():InvokeASCallback("_root", "showTlcFormationPanel", uiText)
  end
  if "editTlcAtkFormation" == cmd then
    DebugOut("editTlcAtkFormation")
    local atkMatrix = FleetTeamLeagueMatrix:GetDefenceInstance()
    local function matrixInitCallback()
      GameStateTlcFormation:EnterState(TEAMLEAGUE_MATRIX_ATTACK, 1)
    end
    FleetTeamLeagueMatrix:GetMatrixsReq(TEAMLEAGUE_MATRIX_ATTACK, matrixInitCallback)
    self.isShowMatrix = true
  end
  if "editTlcDefFormation" == cmd then
    DebugOut("editTlcDefFormation")
    local defenceMatrix = FleetTeamLeagueMatrix:GetDefenceInstance()
    local function matrixInitCallback()
      GameStateTlcFormation:EnterState(TEAMLEAGUE_MATRIX_DEFENCE, 1)
    end
    FleetTeamLeagueMatrix:GetMatrixsReq(TEAMLEAGUE_MATRIX_DEFENCE, matrixInitCallback)
    self.isShowMatrix = true
  end
  if "UpdateMarix" == cmd then
    local index = tonumber(arg)
  end
  if "choose_worldchampion_matrix_tlc" == cmd then
  end
  if "buy_credit_tlc" == cmd then
    TeamLeagueCup:RequestBuyEnegyPrice()
  end
  if "challenge_player_tlc" == cmd then
    DebugOut("challenge_player_tlc_1 " .. arg)
    if TutorialQuestManager.QuestTutorialTlcChallenge:IsActive() then
      TutorialQuestManager.QuestTutorialTlcChallenge:SetFinish(true)
      TutorialQuestManager.QuestTutorialTlcSwitchTeam:SetActive(true)
      GameUIArena:GetFlashObject():InvokeASCallback("_root", "hideTutorialTlcArenaChallenge")
    end
    NetMessageMgr:SendMsg(NetAPIList.tlc_fight_status_req.Code, nil, TeamLeagueCup.getProtectTimeMoveInFormationNetCallback, true, nil)
    self.currentPlayer = arg
  end
  if "reward" == cmd then
  end
  if "leaderboard" == cmd then
  end
  if "honor" == cmd then
  end
  if "report" == cmd then
  end
  if "advanced_help_tlc" == cmd then
    GameUIWDStuff:Show(GameUIWDStuff.menuType.menu_type_help)
    GameUIWDStuff:SetHelpText(GameLoader:GetGameText("LC_MENU_TEAMLEAGUE_HELP_INFO_CHAR"))
  end
  if "hideMatrix" == cmd then
    self.isShowMatrix = false
  end
end
function TeamLeagueCup:UpdateProtectTime()
  if GameUIArena:GetFlashObject() then
    local left_time = -1
    if self.protectTime then
      left_time = self.protectTime - os.time()
      if left_time < 0 then
        left_time = -1
        self.protectTime = nil
      end
      self:SetProtectTime(left_time)
    end
  end
end
function TeamLeagueCup:UpdateRecoveryTime()
  if GameUIArena:GetFlashObject() then
    local left_time = -1
    if self.recoveryTime then
      left_time = self.recoveryTime - os.time()
      if left_time < 0 then
        left_time = -1
        self.recoveryTime = nil
      end
      self:SetRecoveryTime(left_time)
    end
  end
end
function TeamLeagueCup:SetRecoveryTime(time)
  local timeStatus = 0
  if time <= 0 then
    timeStatus = 0
  else
    timeStatus = 1
  end
  local text = GameLoader:GetGameText("LC_MENU_LEAGUE_SEASON_END_INFO")
  GameUIArena:GetFlashObject():InvokeASCallback("_root", "setRecoveryTime_tlc", timeStatus, GameUtils:formatTimeString(time), text)
end
function TeamLeagueCup:UpdateAcountingTime()
  if GameUIArena:GetFlashObject() then
    local left_time = -1
    if self.accountingTime then
      left_time = self.accountingTime - os.time()
      if left_time < 0 then
        left_time = -1
        self.accountingTime = nil
      end
      self:SetAccountingTime(left_time)
    end
  end
end
function TeamLeagueCup:SetAccountingTime(time)
  local timeStatus = 0
  if time <= 0 then
    timeStatus = 0
  else
    timeStatus = 1
  end
  local text = GameLoader:GetGameText("LC_MENU_LEAGUE_SEASON_END_INFO") .. GameUtils:formatTimeString(time)
  GameUIArena:GetFlashObject():InvokeASCallback("_root", "setAccountingTime_tlc", timeStatus, text)
end
function TeamLeagueCup:Update(dt)
  if self.protectTime then
    self:UpdateProtectTime()
  end
  if self.recoveryTime then
    self:UpdateRecoveryTime()
  end
  if self.accountingTime then
    self:UpdateAcountingTime()
  end
end
function TeamLeagueCup:RequestBuyEnegyPrice()
  local param = {type = "tlc_supply"}
  NetMessageMgr:SendMsg(NetAPIList.supply_info_req.Code, param, TeamLeagueCup.RequestButEnegyCallback, true, nil)
end
function TeamLeagueCup.RequestButEnegyCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.supply_info_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      return true
    end
    return true
  end
  if msgType == NetAPIList.supply_info_ack.Code then
    local price = content.exchange_cost
    local count = content.count
    local textContentInfo = string.gsub(GameLoader:GetGameText("LC_MENU_TEAMLEAGUE_RECHARGE_RECOVERY_INFO"), "<number1>", price)
    local info = string.gsub(textContentInfo, "<number2>", count)
    local function buyCallback()
      local param = {type = "tlc_supply"}
      NetMessageMgr:SendMsg(NetAPIList.supply_exchange_req.Code, param, TeamLeagueCup.BuyEnegy, false, nil)
    end
    GameUtils:CreditCostConfirm(info, buyCallback)
    return true
  end
  return false
end
function TeamLeagueCup.BuyEnegy(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.supply_exchange_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  return false
end
GameUIArena.HonorList_tlc = nil
function GameUIArena:RequestHonorWallList_tlc()
  NetMessageMgr:SendMsg(NetAPIList.tlc_honor_wall_req.Code, nil, GameUIArena.RequestHonorWallListCallback_tlc, true, nil)
end
function GameUIArena.RequestHonorWallListCallback_tlc(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.tlc_honor_wall_req.Code then
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.tlc_honor_wall_ack.Code then
    DebugTable(content)
    GameUIArena.HonorList_tlc = content
    GameUIArena:showHonorWall_tlc()
    GameUIArena:showSelfHonorWall_tlc()
    DebugOut("RequestHonorWallListCallback_tlc")
    DebugTable(content)
    return true
  end
  return false
end
function GameUIArena:showHonorWall_tlc()
  local flashObj = self:GetFlashObject()
  if flashObj ~= nil then
    local disableHonorAll = #GameUIArena.HonorList_tlc.all == 0
    local text = GameLoader:GetGameText("LC_MENU_LEAGUE_SERVER_HONOR")
    flashObj:InvokeASCallback("_root", "showHonorWall_tlc", disableHonorAll, text)
  end
end
function GameUIArena:showHonorWallAll_tlc()
  if not GameUIArena.HonorList_tlc then
    GameUIArena:RequestHonorWallList_tlc()
  end
  if #GameUIArena.HonorList_tlc.all == 0 then
    return
  end
  local flashObj = self:GetFlashObject()
  local param = {}
  local seasonList = GameUIArena.HonorList_tlc.all
  flashObj:InvokeASCallback("_root", "setLocalText", "LEAGUE_CREDIT_CHAR", GameLoader:GetGameText("LC_MENU_LEAGUE_CREDIT_CHAR"))
  local SortCallback_season = function(a, b)
    if a.season < b.season then
      return true
    end
  end
  table.sort(seasonList, SortCallback_season)
  local SortCallback = function(a, b)
    if a.ranking < b.ranking then
      return true
    end
  end
  for i = 1, #seasonList do
    local seasonDetail = {}
    seasonDetail.season = seasonList[i].season
    seasonDetail.TextSeason = string.gsub(GameLoader:GetGameText("LC_MENU_TEAMLEAGUE_SEASON_TITLE"), "<number>", seasonList[i].season)
    seasonDetail.players = {}
    for j = 1, #seasonList[i].top_players do
      table.sort(seasonList[i].top_players, SortCallback)
      local playerInfo = {}
      if seasonList[i].top_players[j].icon == 0 or seasonList[i].top_players[j].icon == 1 then
        playerInfo.avatar = GameUtils:GetPlayerAvatarWithSex(seasonList[i].top_players[j].sex, seasonList[i].top_players[j].flevel)
      else
        playerInfo.avatar = GameDataAccessHelper:GetFleetAvatar(seasonList[i].top_players[j].icon, seasonList[i].top_players[j].flevel)
      end
      playerInfo.user_name = seasonList[i].top_players[j].user_name
      playerInfo.score = seasonList[i].top_players[j].point
      table.insert(seasonDetail.players, playerInfo)
    end
    table.insert(param, seasonDetail)
  end
  DebugOut("showHonorWallAll_tlc = ")
  DebugTable(param)
  flashObj:InvokeASCallback("_root", "showHonorWallAll_tlc", param)
end
function GameUIArena:showSelfHonorWall_tlc()
  if not GameUIArena.HonorList_tlc then
    GameUIArena:RequestHonorWallList_tlc()
  end
  local flashObj = self:GetFlashObject()
  local param = {}
  local selfHonorData = GameUIArena.HonorList_tlc.self
  local lastSeasonGroupIndex = 1
  local SortCallback = function(a, b)
    if a.season < b.season then
      return true
    end
  end
  table.sort(selfHonorData, SortCallback)
  local groupCount = math.ceil(#selfHonorData / 4)
  for i = 1, groupCount do
    local group = {}
    for j = 1, 4 do
      local seasonInfo = {}
      seasonInfo.hasData = false
      for k, v in pairs(selfHonorData) do
        if j + (i - 1) * 4 == tonumber(v.season) then
          seasonInfo = v
          local extInfo = {}
          extInfo.rank_img = seasonInfo.rank_img
          extInfo.season = seasonInfo.season
          seasonInfo.rankIcon = GameUtils:GetPlayerRankingInWdc(seasonInfo.rank_img, extInfo, GameUIArena.setSelfHonorRankIcon_tlc)
          if not seasonInfo.formatData then
            seasonInfo.start_date = os.date("%Y-%m-%d", tonumber(seasonInfo.start_date))
            seasonInfo.end_date = os.date("%Y-%m-%d", tonumber(seasonInfo.end_date))
          end
          seasonInfo.formatData = true
          seasonInfo.hasData = true
          seasonInfo.TextSeason = string.gsub(GameLoader:GetGameText("LC_MENU_LEAGUE_SEASON_TITLE"), "<number>", seasonInfo.season)
          break
        end
      end
      if #seasonInfo > 1 and seasonInfo.rank ~= 0 and lastSeasonGroupIndex ~= i then
        lastSeasonGroupIndex = i
      end
      if not seasonInfo.hasData then
        seasonInfo.season = (i - 1) * 4 + j
        seasonInfo.rankIcon = "rank1_0"
        seasonInfo.TextSeason = string.gsub(GameLoader:GetGameText("LC_MENU_LEAGUE_SEASON_TITLE"), "<number>", seasonInfo.season)
      end
      table.insert(group, seasonInfo)
    end
    table.insert(param, group)
  end
  DebugOut("GameUIArena:showSelfHonorWall_tlc")
  DebugTable(param)
  flashObj:InvokeASCallback("_root", "setLocalText", "LEAGUE_SEASON_TITLE", GameLoader:GetGameText("LC_MENU_LEAGUE_SEASON_TITLE"))
  flashObj:InvokeASCallback("_root", "showHonorWallSelf_tlc", param, lastSeasonGroupIndex)
end
function GameUIArena.setSelfHonorRankIcon_tlc(extInfo)
  extInfo.rankIcon = GameUtils:GetPlayerRankingInWdc(extInfo.rank_img, nil, nil)
  GameUIArena:GetFlashObject():InvokeASCallback("_root", "setSelfRankIcon_tlc", extInfo)
end
GameUIArena.RankList_tlc = {}
GameUIArena.RankList_tlc.top_list = {}
GameUIArena.RankList_tlc.class_list = {}
GameUIArena.nowRankListType_tlc = "top_list"
function GameUIArena:clearRankListData_tlc()
  GameUIArena.RankList_tlc = {}
  GameUIArena.RankList_tlc.top_list = {}
  GameUIArena.RankList_tlc.class_list = {}
end
function GameUIArena.getRankListData_tlc(content)
  if content.type == 1 then
    for k, v in ipairs(content.players) do
      local flag = false
      for _, m in ipairs(GameUIArena.RankList_tlc.top_list) do
        if v.user_id == m.user_id and v.server_id == m.server_id then
          flag = true
        end
      end
      if not flag then
        table.insert(GameUIArena.RankList_tlc.top_list, v)
      end
    end
  elseif content.type == 2 then
    for k, v in ipairs(content.players) do
      local flag = false
      for _, m in ipairs(GameUIArena.RankList_tlc.class_list) do
        if v.user_id == m.user_id and v.server_id == m.server_id then
          flag = true
        end
      end
      if not flag then
        table.insert(GameUIArena.RankList_tlc.class_list, v)
      end
    end
  end
end
function GameUIArena:RequestGalaxyArenaRankList_tlc()
  GameUIArena:clearRankListData_tlc()
  NetMessageMgr:SendMsg(NetAPIList.tlc_rank_board_req.Code, nil, GameUIArena.RequestGalaxyArenaRankListCallback_tlc, true, nil)
end
function GameUIArena.RequestGalaxyArenaRankListCallback_tlc(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.tlc_rank_board_req.Code then
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.tlc_rank_board_ack.Code then
    DebugTable(content)
    GameUIArena.RankList_tlc.self = content.self
    GameUIArena.RankList_tlc.world_ranking = content.world_ranking
    GameUIArena.RankList_tlc.rank_level = content.rank_level
    GameUIArena.RankList_tlc.rank_img = content.rank_img
    GameUIArena.RankList_tlc.rank_name = content.rank_name
    GameUIArena.RankList_tlc.rank_ceil = content.rank_ceil
    table.sort(GameUIArena.RankList_tlc.top_list, function(v1, v2)
      return v1.ranking < v2.ranking
    end)
    table.sort(GameUIArena.RankList_tlc.class_list, function(v1, v2)
      return v1.ranking < v2.ranking
    end)
    GameUIArena:showRankList_tlc(GameUIArena.nowRankListType_tlc)
    return true
  end
  return false
end
function GameUIArena:showRankList_tlc(type)
  if not GameUIArena.RankList_tlc then
    GameUIArena:RequestGalaxyArenaRankList_tlc()
    return
  end
  local data = {}
  data.count = #GameUIArena.RankList_tlc[type]
  data.self = GameUIArena.RankList_tlc.self
  local rankImag = GameUtils:GetPlayerRankingInWdc(GameUIArena.RankList_tlc.self.rank_img, nil, nil)
  data.self.rankImag = rankImag
  data.rank_type = GameUIArena.nowRankListType_tlc
  data.rank_level = GameUIArena.RankList_tlc.rank_level
  data.world_ranking = GameUIArena.RankList_tlc.world_ranking
  data.RankingCeil = GameUIArena.RankList_tlc.rank_ceil
  local rankName = GameLoader:GetGameText("LC_MENU_LEAGUE_RANK_CHAR")
  if GameUIArena.nowRankListType_tlc == "class_list" then
    local nowRank = GameUIArena.RankList_tlc.rank_name
    rankName = GameLoader:GetGameText("LC_MENU_TEAMLEAGUE_GRADENAME_" .. nowRank) .. " " .. GameLoader:GetGameText("LC_MENU_LEAGUE_RANK_CHAR")
  end
  local TextInfo = string.gsub(GameLoader:GetGameText("LC_MENU_TEAMLEAGUE_GRADE_RANK_INFO"), "<number>", GameUIArena.KingNumber)
  DebugOut("GameUIArena:showRankList_tlc")
  DebugTable(data)
  self:GetFlashObject():InvokeASCallback("_root", "setLocalText", "TEAMLEAGUE_TOP_GRADENAME_RANK_BTN", GameLoader:GetGameText("LC_MENU_TEAMLEAGUE_TOP_GRADENAME_RANK_BTN"))
  self:GetFlashObject():InvokeASCallback("_root", "showRankList_tlc", data, rankName, TextInfo)
end
function GameUIArena:UpdateRankItem_tlc(indexItem)
  local DataString = -1
  local ItemInfo
  local rankList = GameUIArena.RankList_tlc[GameUIArena.nowRankListType_tlc]
  if indexItem > 0 and indexItem <= #rankList then
    ItemInfo = rankList[indexItem]
  end
  if ItemInfo then
    local DataTable = {}
    table.insert(DataTable, ItemInfo.ranking)
    table.insert(DataTable, ItemInfo.user_name)
    table.insert(DataTable, ItemInfo.force)
    table.insert(DataTable, ItemInfo.point)
    local rankingImage = GameUtils:GetPlayerRankingInWdc(ItemInfo.rank_img, nil, nil)
    table.insert(DataTable, rankingImage)
    DataString = table.concat(DataTable, "\001")
  end
  self:GetFlashObject():InvokeASCallback("_root", "setRankItemData_tlc", indexItem, DataString)
end
function GameUIArena:showRankPlayerInfo_tlc(indexItem)
  local playerInfo = GameUIArena.RankList_tlc.self
  if indexItem ~= 0 then
    playerInfo = GameUIArena.RankList_tlc[GameUIArena.nowRankListType_tlc][indexItem]
  end
  ItemBox:showRankPlayerInfo_tlc(playerInfo)
end
function TeamLeagueCup:StartBattle()
  NetMessageMgr:SendMsg(NetAPIList.tlc_fight_status_req.Code, nil, TeamLeagueCup.getProtectTimeStartBattleNetCallback_GotoBattle, true, nil)
end
function TeamLeagueCup.getProtectTimeStartBattleNetCallback_GotoBattle(msgType, content)
  if msgType == NetAPIList.tlc_fight_status_ack.Code then
    if content.protect_time > 0 then
      local text_title = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
      local info = GameLoader:GetGameText("LC_MENU_LEAGUE_ATTACK_INFO")
      GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
      GameUIMessageDialog:SetYesButton(TeamLeagueCup.StartBattleProtect)
      GameUIMessageDialog:SetNoButton(nil)
      GameUIMessageDialog:Display(text_title, info)
    else
      TeamLeagueCup:GotoBattle()
    end
    return true
  end
  return false
end
function TeamLeagueCup.StartBattleProtect()
  TeamLeagueCup:GotoBattle()
end
function TeamLeagueCup:GotoBattle()
  local param = {
    server_id = self.currentEnemyInfo.defender.server_id,
    user_id = self.currentEnemyInfo.defender.user_id,
    revenge_id = self.currentEnemyInfo.defender.revenge_id or 0
  }
  DebugOut("param")
  DebugTable(param)
  if GameStateManager:GetCurrentGameState().fightRoundData then
    GameStateManager:GetCurrentGameState().fightRoundData = nil
  end
  NetMessageMgr:SendMsg(NetAPIList.tlc_fight_req.Code, param, TeamLeagueCup.NetcallbackBattle, true)
end
function TeamLeagueCup.NetcallbackBattle(msgType, content)
  if msgType == NetAPIList.tlc_fight_ack.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      return true
    end
    TeamLeagueCup.battleResult = content
    if content.headers and 0 < #content.headers and content.headers[1].report then
      local reportData = content.headers[1].report
      if #reportData.rounds == 0 and GameStateManager:GetCurrentGameState().fightRoundData then
        reportData.rounds = GameStateManager:GetCurrentGameState().fightRoundData
      end
      if reportData then
        GameStateBattlePlay.curBattleType = "teamLeagueCup"
        local GameObjectBattleReplay = LuaObjectManager:GetLuaObject("GameObjectBattleReplay")
        GameObjectBattleReplay.isGroupBattle = true
        GameObjectBattleReplay.curGroupBattleIndex = 1
        GameObjectBattleReplay.GroupBattleReportArr = {}
        GameObjectBattleReplay.GroupBattleReportArr[1] = content
        GameStateBattlePlay:InitBattle(GameStateBattlePlay.MODE_PLAY, content.headers[1].report, 3, nil)
        GameStateManager:SetCurrentGameState(GameStateBattlePlay)
        GameStateBattlePlay:RegisterOverCallback(TeamLeagueCup.PlayerBattleCallback, nil)
      end
    end
    return true
  end
  return false
end
function TeamLeagueCup.PlayerBattleCallback()
  GameUIBattleResult:LoadFlashObject()
  local freport = {}
  freport[1] = TeamLeagueCup.battleResult
  GameUIBattleResult:SetFightReport(freport, 3, nil, true)
  local itemIconS = ""
  local itemNameS = ""
  local itemNumberS = ""
  for k, v in ipairs(TeamLeagueCup.battleResult.awards) do
    local itemName = GameHelper:GetAwardTypeText(v.item_type, v.number)
    local icon = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(v, nil, nil)
    local number = GameHelper:GetAwardCount(v.item_type, v.number, v.no)
    itemIconS = itemIconS .. icon .. "\001"
    itemNameS = itemNameS .. itemName .. "\001"
    itemNumberS = itemNumberS .. number .. "\001"
  end
  local dataTable = {}
  dataTable.rank = TeamLeagueCup.battleResult.result.score_before
  dataTable.rank_after = TeamLeagueCup.battleResult.result.score_after
  dataTable.change_rank = TeamLeagueCup.battleResult.result.score_after - TeamLeagueCup.battleResult.result.score_before
  dataTable.isWin = TeamLeagueCup.battleResult.result.is_win
  dataTable.itemIconS = itemIconS
  dataTable.itemNameS = itemNameS
  dataTable.itemNumberS = itemNumberS
  dataTable.damage = TeamLeagueCup.battleResult.damage
  dataTable.suffer_damage = TeamLeagueCup.battleResult.suffer_damage
  GameUIBattleResult:ShowAdvancedWin(dataTable)
  GameStateManager:SetCurrentGameState(GameStateArena)
  TeamLeagueCup:CleanSearchInfo()
  GameStateArena:AddObject(GameUIBattleResult)
end
