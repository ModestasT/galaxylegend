local GameUIHeroHandbook = LuaObjectManager:GetLuaObject("GameUIHeroHandbook")
local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameStateBattlePlay = GameStateManager.GameStateBattlePlay
local CommanderRanks = {
  [1] = "rank_E",
  [2] = "rank_D",
  [3] = "rank_C",
  [4] = "rank_B",
  [5] = "rank_A",
  [6] = "rank_S",
  [7] = "rank_SS"
}
local ClassType = {
  [1] = "TYPE_7",
  [2] = "TYPE_6",
  [3] = "TYPE_1",
  [4] = "TYPE_2",
  [5] = "TYPE_3",
  [6] = "TYPE_4",
  [7] = "TYPE_5"
}
GameUIHeroHandbook.mShareStoryChecked = {}
GameUIHeroHandbook.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_HANDBOOK_LIKE] = {}
GameUIHeroHandbook.mHeroList = {}
GameUIHeroHandbook.mHeroListByType = {}
GameUIHeroHandbook.mClassType = "all"
GameUIHeroHandbook.mFleetInfos = {}
GameUIHeroHandbook.mLastClassTypeIdx = 1
GameUIHeroHandbook.mLastNameListItemIdx = 1
GameUIHeroHandbook.mLastItemSuggestionPos = "head"
GameUIHeroHandbook.mIsBackFromBattle = false
GameUIHeroHandbook.mHeroLikeState = {}
local _getMainFleetID = function()
  local leaderlist = GameGlobalData:GetData("leaderlist")
  if leaderlist and GameGlobalData.GlobalData.curMatrixIndex then
    return leaderlist.leader_ids[GameGlobalData.GlobalData.curMatrixIndex]
  end
  error("must have number")
  return 1
end
function GameUIHeroHandbook:OnAddToGameState(game_state)
  if GameUtils:GetTutorialHelp() then
    GameUtils:HideTutorialHelp()
  end
  DebugOut("GameUIHeroHandbook:OnAddToGameState")
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  GameUIHeroHandbook:CheckDownloadImage()
  if GameUIHeroHandbook.mIsBackFromBattle then
    GameUIHeroHandbook:InitClassList(GameUIHeroHandbook.mLastClassTypeIdx)
    GameUIHeroHandbook:SetHeroList(GameUIHeroHandbook.mLastNameListItemIdx)
    GameUIHeroHandbook:SetHeroToPos(GameUIHeroHandbook.mLastNameListItemIdx, GameUIHeroHandbook.mLastItemSuggestionPos)
    GameUIHeroHandbook.mIsBackFromBattle = false
  else
    GameUIHeroHandbook.mLastClassTypeIdx = 1
    GameUIHeroHandbook.mLastNameListItemIdx = 1
    GameUIHeroHandbook:RequestHeroLikeState()
    GameUIHeroHandbook:RequestHeroList()
  end
end
function GameUIHeroHandbook:OnEraseFromGameState(game_state)
  self:UnloadFlashObject()
end
function GameUIHeroHandbook:CheckDownloadImage()
  if (ext.crc32.crc32("data2/LAZY_LOAD_domination_bg.png") == "" or ext.crc32.crc32("data2/LAZY_LOAD_domination_bg.png") == nil) and not AutoUpdateInBackground:IsHeroFileUpdatedToServer("data2/LAZY_LOAD_domination_bg.png") then
    self:GetFlashObject():ReplaceTexture("LAZY_LOAD_domination_bg.png", "territorial_map_bg.png")
  end
end
function GameUIHeroHandbook:OnFSCommand(cmd, arg)
  if cmd == "close" then
    GameStateManager.GameStateHeroHandbook:Exit()
  elseif cmd == "btn_help" then
    GameUIHeroHandbook:RequestHeroList()
  elseif cmd == "simulate_fight" then
    GameUIHeroHandbook:RequestSimulateFight(tonumber(arg))
  elseif cmd == "ClassItemClecked" then
    if tonumber(arg) == 1 then
      GameUIHeroHandbook.mClassType = "all"
    else
      GameUIHeroHandbook.mClassType = ClassType[tonumber(arg)]
    end
    GameUIHeroHandbook.mLastClassTypeIdx = tonumber(arg)
    GameUIHeroHandbook:SetHeroList(1)
  elseif cmd == "NeedUpdateHeroNameListItem" then
    local tparam = LuaUtils:string_split(arg, ":")
    local fleetid = tonumber(tparam[1])
    local pos = tonumber(tparam[2])
    GameUIHeroHandbook:UpdateHeroNameListItem(fleetid, pos)
  elseif cmd == "HeroNameItemClicked" then
    local fleetid = tonumber(arg)
    local item
    if "all" == GameUIHeroHandbook.mClassType then
      item = GameUIHeroHandbook.mHeroList[fleetid]
    elseif GameUIHeroHandbook.mHeroListByType[GameUIHeroHandbook.mClassType] then
      item = GameUIHeroHandbook.mHeroListByType[GameUIHeroHandbook.mClassType][fleetid]
    end
    if item then
      GameUIHeroHandbook.mLastNameListItemIdx = fleetid
      GameUIHeroHandbook:SetHeroDetailInfo(item, nil)
    end
    self:GetFlashObject():RejectLazyLoadTexture()
  elseif cmd == "DetailClicked" then
    local fleetid = tonumber(arg)
    GameUIHeroHandbook:ShowHeroDetail(fleetid)
  elseif cmd == "NameSelected" then
    local pos = tonumber(arg)
    GameUIHeroHandbook.mLastNamePos = pos
  elseif cmd == "record_suggestion_pos" then
    GameUIHeroHandbook.mLastItemSuggestionPos = arg
  elseif cmd == "doLike" then
    if not GameUIHeroHandbook.mCurLikedFleetID then
      GameUIHeroHandbook:RequestLikeHero(tonumber(arg))
    end
  elseif cmd == "shareLikeHeroChecked" then
    if self.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_HANDBOOK_LIKE][tonumber(arg)] then
      self.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_HANDBOOK_LIKE][tonumber(arg)] = false
    else
      self.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_HANDBOOK_LIKE][tonumber(arg)] = true
    end
    local flash_obj = self:GetFlashObject()
    if flash_obj then
      flash_obj:InvokeASCallback("_root", "setCheckBox", self.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_HANDBOOK_LIKE][tonumber(arg)], FacebookGraphStorySharer.StoryType.STORY_TYPE_HANDBOOK_LIKE)
    end
  end
end
function GameUIHeroHandbook:GetProgressStringByType(classType)
  local cnt = 0
  local total = 0
  local progressStr = ""
  local heroList = GameUIHeroHandbook.mHeroListByType[classType]
  if classType == ClassType[1] then
    heroList = GameUIHeroHandbook.mHeroList
  end
  if heroList and #heroList > 0 then
    total = #heroList
    for k = 1, #heroList do
      if heroList[k].is_mine then
        cnt = cnt + 1
      end
    end
  end
  progressStr = tostring(cnt) .. "/" .. tostring(total)
  return progressStr
end
function GameUIHeroHandbook:InitClassList(selectedIdx)
  local flashObj = GameUIHeroHandbook:GetFlashObject()
  if flashObj then
    local cnt = 0
    local types = ""
    local progressStr = ""
    local selectedStatus = ""
    for k = 1, #ClassType do
      types = types .. ClassType[k] .. "\001"
      local selected = "unselected"
      if selectedIdx == k then
        selected = "selected"
      end
      selectedStatus = selectedStatus .. selected .. "\001"
      progressStr = progressStr .. GameUIHeroHandbook:GetProgressStringByType(ClassType[k]) .. "\001"
      cnt = cnt + 1
    end
    flashObj:InvokeASCallback("_root", "SetClassList", cnt, types, progressStr, selectedStatus)
  end
end
function GameUIHeroHandbook:RequestHeroLikeState()
  NetMessageMgr:SendMsg(NetAPIList.thumb_up_info_req.Code, nil, self.RequestHeroLikeStateCallback, false, nil)
end
function GameUIHeroHandbook.RequestHeroLikeStateCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.thumb_up_info_req.Code then
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.thumb_up_info_ack.Code then
    if content.fleets_thumbs then
      for i, v in ipairs(content.fleets_thumbs) do
        GameUIHeroHandbook.mHeroLikeState[v.key] = v.value
      end
    end
    return true
  end
  return false
end
function GameUIHeroHandbook:RequestLikeHero(fleetID)
  local requestParam = {}
  requestParam.id = fleetID
  GameUIHeroHandbook.mCurLikedFleetID = fleetID
  DebugOut("GameUIHeroHandbook.mCurLikedFleetID = ", fleetID)
  NetMessageMgr:SendMsg(NetAPIList.thumb_up_req.Code, requestParam, self.RequestLikeHeroCallback, false, nil)
end
function GameUIHeroHandbook.RequestLikeHeroCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.thumb_up_req.Code then
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    GameUIHeroHandbook.mCurLikedFleetID = nil
    return true
  elseif msgType == NetAPIList.thumb_up_ack.Code then
    local item
    DebugOut("GameUIHeroHandbook.mCurLikedFleetID = ", GameUIHeroHandbook.mCurLikedFleetID)
    if "all" == GameUIHeroHandbook.mClassType then
      for k, v in ipairs(GameUIHeroHandbook.mHeroList) do
        if v.id == GameUIHeroHandbook.mCurLikedFleetID then
          item = v
          break
        end
      end
    elseif GameUIHeroHandbook.mHeroListByType[GameUIHeroHandbook.mClassType] then
      for k, v in ipairs(GameUIHeroHandbook.mHeroListByType[GameUIHeroHandbook.mClassType]) do
        if v.id == GameUIHeroHandbook.mCurLikedFleetID then
          item = v
          break
        end
      end
    end
    DebugOut("item = ")
    DebugTable(item)
    if item then
      DebugOut("check show")
      item.thumb_up_num = item.thumb_up_num + 1
      if GameUIHeroHandbook.mHeroLikeState[item.id] == 1 then
      else
        GameUIHeroHandbook.mHeroLikeState[item.id] = 1
        local countryStr = ext.GetDeviceCountry and ext.GetDeviceCountry() or "unknow"
        if GameUIHeroHandbook.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_HANDBOOK_LIKE][item.id] and item.id ~= 1 and item.can_send_story then
          item.can_send_story = false
          local extraInfo = {}
          extraInfo.heroName = GameDataAccessHelper:GetFleetLevelDisplayName(item.id)
          extraInfo.fleetID = item.id
          FacebookGraphStorySharer:ShareStory(FacebookGraphStorySharer.StoryType.STORY_TYPE_HANDBOOK_LIKE, extraInfo)
        else
        end
      end
      GameUIHeroHandbook:SetHeroDetailInfo(item, true)
    end
    GameUIHeroHandbook.mCurLikedFleetID = nil
    return true
  end
  return false
end
function GameUIHeroHandbook:RequestHeroList()
  NetMessageMgr:SendMsg(NetAPIList.all_gd_fleets_req.Code, nil, self.RequestHeroListCallback, true, nil)
end
function GameUIHeroHandbook.RequestHeroListCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.all_gd_fleets_req.Code then
    DebugOut("RequestHeroListCallback err.")
    DebugTable(content)
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.all_gd_fleets_ack.Code then
    DebugOut("RequestHeroListCallback ok.")
    DebugTable(content)
    GameUIHeroHandbook.mHeroList = content.fleets
    GameUIHeroHandbook:SortHeroListByForce()
    GameUIHeroHandbook.mClassType = "all"
    for i, v in ipairs(content.fleets) do
      GameUIHeroHandbook.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_HANDBOOK_LIKE][v.id] = true
    end
    DebugOut("GameUIHeroHandbook.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_HANDBOOK_LIKE] = ")
    DebugTable(GameUIHeroHandbook.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_HANDBOOK_LIKE])
    GameUIHeroHandbook:InitClassList(1)
    GameUIHeroHandbook:SetHeroList(1)
    return true
  end
  return false
end
function GameUIHeroHandbook:ShowHeroDetail(fleetId)
  DebugOut("Pop up hero detail item. " .. tostring(fleetId))
  if GameUIHeroHandbook.mLastNamePos ~= 1 or GameUIHeroHandbook.mClassType ~= "all" then
    if GameUIHeroHandbook.mFleetInfos[fleetId] then
      ItemBox:ShowCommanderDetail2(fleetId, GameUIHeroHandbook.mFleetInfos[fleetId], nil, nil, true, false)
    else
      GameUIHeroHandbook:RequestFleetInfo(fleetId)
    end
  else
    local leaderlist = GameGlobalData:GetData("leaderlist")
    local curMatrix = GameGlobalData.GlobalData.curMatrixIndex
    local tempid = fleetId
    if leaderlist and curMatrix then
      tempid = leaderlist.leader_ids[curMatrix]
    end
    DebugOut("curMatrix:", curMatrix, "tempid")
    DebugTable(leaderlist)
    if GameUIHeroHandbook.mFleetInfos[tempid] then
      ItemBox:ShowCommanderDetail2(tempid, GameUIHeroHandbook.mFleetInfos[tempid], nil, nil, true, true)
    else
      GameUIHeroHandbook:RequestFleetInfo(tempid)
    end
  end
end
function GameUIHeroHandbook:SetHeroDetailInfo(fleetInfo, noClear, pos)
  if fleetInfo == nil then
    return
  end
  DebugOut("SetHeroDetailInfo")
  local commanderInfo
  if nil ~= fleetInfo then
    local fleetId = fleetInfo.id
    local sex = 1
    local basic_info = GameDataAccessHelper:GetCommanderBasicInfo(fleetId)
    local commanderAbility = GameDataAccessHelper:GetCommanderAbility(fleetId)
    local level = GameGlobalData:GetData("levelinfo").level
    local leaderlist = GameGlobalData:GetData("leaderlist")
    if GameUIHeroHandbook.mLastNamePos == 1 and GameUIHeroHandbook.mClassType == "all" then
      commanderInfo = {}
      local tempid = leaderlist.leader_ids[GameGlobalData.GlobalData.curMatrixIndex]
      commanderAbility = GameDataAccessHelper:GetCommanderAbility(tempid)
      basic_info = GameDataAccessHelper:GetCommanderBasicInfo(tempid)
      commanderInfo.fleetId = fleetId
      commanderInfo.rank = GameUtils:GetFleetRankFrame(commanderAbility.Rank, GameUIHeroHandbook.DownloadRankImageCallback)
      commanderInfo.vessleType = GameDataAccessHelper:GetCommanderVesselsType(tempid)
      commanderInfo.head = GameDataAccessHelper:GetFleetAvatar(tempid)
      commanderInfo.shipIcon = GameDataAccessHelper:GetCommanderVesselsImage(tempid)
      commanderInfo.commanderName = GameDataAccessHelper:GetFleetLevelDisplayName(tempid)
      commanderInfo.vessleName = GameLoader:GetGameText("LC_FLEET_" .. commanderInfo.vessleType)
      commanderInfo.skillRangeType = "TYPE_" .. GameDataAccessHelper:GetSkillRangeType(basic_info.SPELL_ID)
      commanderInfo.skillIcon = GameDataAccessHelper:GetSkillIcon(basic_info.SPELL_ID)
      commanderInfo.skillName = GameDataAccessHelper:GetSkillNameText(basic_info.SPELL_ID)
      commanderInfo.skillRangetypeName = GameLoader:GetGameText("LC_MENU_SKILL_RANGE_TYPE_" .. GameDataAccessHelper:GetSkillRangeType(basic_info.SPELL_ID))
      commanderInfo.damageRate = commanderAbility.Damage_Rate
      commanderInfo.defenceRate = commanderAbility.Defence_Rate
      commanderInfo.assistRate = commanderAbility.Assist_Rate
      commanderInfo.story = GameLoader:GetGameText("LC_STORY_STORY_FLEET_" .. tostring(tempid))
      commanderInfo.color = GameDataAccessHelper:GetCommanderColorFrame(tempid)
      commanderInfo.likeNum = fleetInfo.thumb_up_num
      commanderInfo.likeState = (self.mHeroLikeState[fleetId] or 0) + 1
      commanderInfo.fbCheckText = GameLoader:GetGameText("LC_MENU_NEW_FACEBOOK_SHARE_INFO")
      sex = GameDataAccessHelper:GetCommanderSex(tempid)
    else
      commanderInfo = {}
      commanderInfo.fleetId = fleetId
      commanderInfo.rank = GameUtils:GetFleetRankFrame(commanderAbility.Rank, GameUIHeroHandbook.DownloadRankImageCallback)
      commanderInfo.vessleType = GameDataAccessHelper:GetCommanderVesselsType(fleetId)
      commanderInfo.head = GameDataAccessHelper:GetFleetAvatar(fleetId)
      commanderInfo.shipIcon = GameDataAccessHelper:GetCommanderVesselsImage(fleetId)
      local commanderName = GameDataAccessHelper:GetCommanderName(fleetId, 0)
      commanderName = GameLoader:GetGameText("LC_NPC_" .. commanderName)
      commanderName = GameDataAccessHelper:CreateLevelDisplayName(commanderName, 0)
      commanderInfo.commanderName = commanderName
      if fleetId == 1 then
        commanderInfo.commanderName = GameLoader:GetGameText("LC_NPC_NPC_New")
      end
      commanderInfo.vessleName = GameLoader:GetGameText("LC_FLEET_" .. commanderInfo.vessleType)
      commanderInfo.skillRangeType = "TYPE_" .. GameDataAccessHelper:GetSkillRangeType(basic_info.SPELL_ID)
      commanderInfo.skillIcon = GameDataAccessHelper:GetSkillIcon(basic_info.SPELL_ID)
      commanderInfo.skillName = GameDataAccessHelper:GetSkillNameText(basic_info.SPELL_ID)
      commanderInfo.skillRangetypeName = GameLoader:GetGameText("LC_MENU_SKILL_RANGE_TYPE_" .. GameDataAccessHelper:GetSkillRangeType(basic_info.SPELL_ID))
      commanderInfo.damageRate = commanderAbility.Damage_Rate
      commanderInfo.defenceRate = commanderAbility.Defence_Rate
      commanderInfo.assistRate = commanderAbility.Assist_Rate
      commanderInfo.story = GameLoader:GetGameText("LC_STORY_STORY_FLEET_" .. tostring(fleetId))
      commanderInfo.color = GameDataAccessHelper:GetCommanderColorFrame(fleetId)
      commanderInfo.likeNum = fleetInfo.thumb_up_num
      commanderInfo.likeState = (self.mHeroLikeState[fleetId] or 0) + 1
      commanderInfo.fbCheckText = GameLoader:GetGameText("LC_MENU_NEW_FACEBOOK_SHARE_INFO")
      sex = GameDataAccessHelper:GetCommanderSex(fleetId)
    end
    if sex == 1 then
      sex = "man"
    elseif sex == 0 then
      sex = "woman"
    else
      sex = "unknown"
    end
    commanderInfo.sex = sex
  end
  DebugOut("commanderInfo:")
  DebugTable(commanderInfo)
  local shareStoryEnabled = false
  if FacebookGraphStorySharer:IsStoryEnabled(FacebookGraphStorySharer.StoryType.STORY_TYPE_HANDBOOK_LIKE) and fleetInfo and fleetInfo.id ~= 1 and fleetInfo.can_send_story then
    shareStoryEnabled = true
  end
  local flashObj = GameUIHeroHandbook:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "SetHeroDetailInfo", commanderInfo, shareStoryEnabled, self.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_HANDBOOK_LIKE][fleetInfo.id], noClear)
  end
end
function GameUIHeroHandbook.DownloadRankImageCallback(extInfo)
  if GameUIHeroHandbook:GetFlashObject() then
    GameUIHeroHandbook:GetFlashObject():InvokeASCallback("_root", "updateRankImage", extInfo.rank_id)
  end
end
function GameUIHeroHandbook:SortHeroListByForce()
  local sortFunc = function(a, b)
    if not a or not b or not a.force or not b.force or not a.rank or not b.rank or not a.color or not b.color then
      return false
    end
    if a.rank > b.rank then
      return true
    elseif a.rank < b.rank then
      return false
    elseif a.color > b.color then
      return true
    elseif a.color < b.color then
      return false
    elseif a.force > b.force then
      return true
    else
      return false
    end
  end
  GameUIHeroHandbook.mHeroListByType = {}
  if #GameUIHeroHandbook.mHeroList > 0 then
    local mainFleet
    local mainFleetPos = 0
    for k = 1, #GameUIHeroHandbook.mHeroList do
      local fleet = GameUIHeroHandbook.mHeroList[k]
      local commanderAbility = GameDataAccessHelper:GetCommanderAbility(fleet.id)
      fleet.rank = commanderAbility.Rank
      fleet.color = commanderAbility.COLOR
      if fleet.id == _getMainFleetID() then
        mainFleetPos = k
        mainFleet = fleet
      end
    end
    table.sort(GameUIHeroHandbook.mHeroList, sortFunc)
    DebugTable(GameUIHeroHandbook.mHeroList)
    for k = 1, #GameUIHeroHandbook.mHeroList do
      local hero = GameUIHeroHandbook.mHeroList[k]
      local heroType = GameDataAccessHelper:GetCommanderVesselsType(hero.id)
      if nil == GameUIHeroHandbook.mHeroListByType[heroType] then
        GameUIHeroHandbook.mHeroListByType[heroType] = {}
      end
      table.insert(GameUIHeroHandbook.mHeroListByType[heroType], hero)
    end
    if mainFleet then
      table.insert(GameUIHeroHandbook.mHeroList, 1, mainFleet)
    end
  end
  DebugOut("GameUIHeroHandbook.mHeroListByType ")
  DebugTable(GameUIHeroHandbook.mHeroListByType)
end
function GameUIHeroHandbook:SetHeroList(selectedIdx)
  local flashObj = GameUIHeroHandbook:GetFlashObject()
  if flashObj and #GameUIHeroHandbook.mHeroList > 0 then
    local heroCnt = 0
    local defaultDetailHero = 0
    GameUIHeroHandbook.mLastNameListItemIdx = selectedIdx
    if "all" == GameUIHeroHandbook.mClassType then
      heroCnt = #GameUIHeroHandbook.mHeroList
      defaultDetailHero = GameUIHeroHandbook.mHeroList[selectedIdx]
    elseif GameUIHeroHandbook.mHeroListByType[GameUIHeroHandbook.mClassType] then
      heroCnt = #GameUIHeroHandbook.mHeroListByType[GameUIHeroHandbook.mClassType]
      defaultDetailHero = GameUIHeroHandbook.mHeroListByType[GameUIHeroHandbook.mClassType][selectedIdx]
    end
    flashObj:InvokeASCallback("_root", "SetHeroNameList", heroCnt)
    if heroCnt > 0 and defaultDetailHero then
      flashObj:InvokeASCallback("_root", "SetHeroNameListItemSelected", selectedIdx, true)
      GameUIHeroHandbook:SetHeroDetailInfo(defaultDetailHero, nil, selectedIdx)
    else
      GameUIHeroHandbook:SetHeroDetailInfo(nil)
    end
  end
end
function GameUIHeroHandbook:SetHeroToPos(selectedIdx, pos)
  local flashObj = GameUIHeroHandbook:GetFlashObject()
  if not flashObj then
    return
  end
  DebugOut("GameUIHeroHandbook:SetHeroToPos = ", pos)
  flashObj:InvokeASCallback("_root", "SetHeroToPos", selectedIdx, pos)
end
function GameUIHeroHandbook:UpdateHeroNameListItem(itemId, pos)
  local flashObj = GameUIHeroHandbook:GetFlashObject()
  if flashObj then
    local item
    if "all" == GameUIHeroHandbook.mClassType then
      item = GameUIHeroHandbook.mHeroList[itemId]
    elseif GameUIHeroHandbook.mHeroListByType[GameUIHeroHandbook.mClassType] then
      item = GameUIHeroHandbook.mHeroListByType[GameUIHeroHandbook.mClassType][itemId]
    end
    if item then
      local leaderlist = GameGlobalData:GetData("leaderlist")
      local level = GameGlobalData:GetData("levelinfo").level
      local head = ""
      local commanderName = ""
      local commanderColorFr = ""
      local commander_ability = {}
      local rankFram = ""
      if pos == 1 and GameUIHeroHandbook.mClassType == "all" then
        local tempid = leaderlist.leader_ids[GameGlobalData.GlobalData.curMatrixIndex]
        head = GameDataAccessHelper:GetFleetAvatar(tempid)
        commanderName = GameDataAccessHelper:GetFleetLevelDisplayName(tempid)
        commanderColorFr = GameDataAccessHelper:GetCommanderColorFrame(tempid)
        commander_ability = GameDataAccessHelper:GetCommanderAbility(tempid)
        rankFram = GameUtils:GetFleetRankFrame(commander_ability.Rank, GameUIHeroHandbook.DownloadRankListCallback, itemId)
      else
        head = GameDataAccessHelper:GetFleetAvatar(item.id)
        commanderName = GameDataAccessHelper:GetCommanderName(item.id, 0)
        commanderName = GameLoader:GetGameText("LC_NPC_" .. commanderName)
        commanderName = GameDataAccessHelper:CreateLevelDisplayName(commanderName, 0)
        if item.id == 1 then
          commanderName = GameLoader:GetGameText("LC_NPC_NPC_New")
        end
        commanderColorFr = GameDataAccessHelper:GetCommanderColorFrame(item.id)
        commander_ability = GameDataAccessHelper:GetCommanderAbility(item.id)
        rankFram = GameUtils:GetFleetRankFrame(commander_ability.Rank, GameUIHeroHandbook.DownloadRankListCallback, itemId)
      end
      flashObj:InvokeASCallback("_root", "UpdateHeroNameListItem", itemId, item.is_mine, head, GameUIHeroHandbook.mLastNameListItemIdx == itemId or false, commanderName, commanderColorFr, rankFram)
    end
  end
end
function GameUIHeroHandbook.DownloadRankListCallback(extInfo)
  local flashObj = GameUIHeroHandbook:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "updateRankListImage", extInfo.rank_id, extInfo.id)
  end
end
function GameUIHeroHandbook:RequestSimulateFight(fleetId)
  local req = {id = fleetId}
  DebugOut("RequestSimulateFight: " .. fleetId)
  DebugTable(req)
  NetMessageMgr:SendMsg(NetAPIList.fleet_fight_report_req.Code, req, self.RequestSimulateFightCallback, true, nil)
end
function GameUIHeroHandbook.RequestSimulateFightCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.fleet_fight_report_req.Code then
    DebugOut("GameUIHeroHandbook.RequestSimulateFightCallback error")
    DebugTable(content)
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.fleet_fight_report_ack.Code then
    DebugOut("GameUIHeroHandbook.RequestSimulateFightCallback ok")
    DebugTable(content)
    local fight_report = content.report
    GameStateBattlePlay.curBattleType = "handBook"
    GameStateBattlePlay:InitBattle(GameStateBattlePlay.MODE_HISTORY, fight_report)
    GameStateBattlePlay:RegisterOverCallback(function()
      GameUIHeroHandbook.mIsBackFromBattle = true
      GameStateManager:SetCurrentGameState(GameStateManager.GameStateHeroHandbook)
    end)
    GameStateManager:SetCurrentGameState(GameStateBattlePlay)
    return true
  end
  return false
end
function GameUIHeroHandbook:RequestFleetInfo(fleetId)
  local req = {
    fleet_id = fleetId,
    level = 0,
    type = 1,
    req_type = 1
  }
  NetMessageMgr:SendMsg(NetAPIList.fleet_info_req.Code, req, GameUIHeroHandbook.RequestFleetInfoCallback, true, nil)
end
function GameUIHeroHandbook.RequestFleetInfoCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.fleet_info_req.Code then
    DebugOut("GameUIHeroHandbook.RequestFleetInfoCallback error")
    DebugTable(content)
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.fleet_info_ack.Code then
    DebugOut("GameUIHeroHandbook.RequestFleetInfoCallback ok")
    DebugTable(content)
    GameUIHeroHandbook.mFleetInfos[content.fleet_id] = content.fleet_info.fleets[1]
    local showplayer = GameUIHeroHandbook.mLastNamePos == 1 and GameUIHeroHandbook.mClassType == "all"
    ItemBox:ShowCommanderDetail2(content.fleet_id, GameUIHeroHandbook.mFleetInfos[content.fleet_id], nil, nil, true, showplayer)
    return true
  end
  return false
end
if AutoUpdate.isAndroidDevice then
  function GameUIHeroHandbook.OnAndroidBack()
    GameUIHeroHandbook:OnFSCommand("close")
  end
end
