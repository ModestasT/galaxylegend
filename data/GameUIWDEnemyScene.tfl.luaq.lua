local GameUIWDEnemyScene = LuaObjectManager:GetLuaObject("GameUIWDEnemyScene")
local GameWaiting = LuaObjectManager:GetLuaObject("GameWaiting")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameObjectBattleMapBG = LuaObjectManager:GetLuaObject("GameObjectBattleMapBG")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameUIWDStuff = LuaObjectManager:GetLuaObject("GameUIWDStuff")
local GameSetting = LuaObjectManager:GetLuaObject("GameSetting")
local GameStateWD = GameStateManager.GameStateWD
local GameUIBarLeft = LuaObjectManager:GetLuaObject("GameUIBarLeft")
local GameUIBarRight = LuaObjectManager:GetLuaObject("GameUIBarRight")
local GameStateBattlePlay = GameStateManager.GameStateBattlePlay
local GameUIBattleResult = LuaObjectManager:GetLuaObject("GameUIBattleResult")
local GameVipDetailInfoPanel = LuaObjectManager:GetLuaObject("GameVipDetailInfoPanel")
local GameWDAttkBox = LuaObjectManager:GetLuaObject("GameWDAttkBox")
local GameUIWDInBattle = LuaObjectManager:GetLuaObject("GameUIWDInBattle")
function GameUIWDEnemyScene:OnInitGame()
  DebugWD("GameUIWDEnemyScene:OnInitGame()")
end
GameUIWDEnemyScene.bgSize = nil
GameUIWDEnemyScene.sceneSize = nil
GameUIWDEnemyScene.stageSize = nil
GameUIWDEnemyScene.bgMin = nil
GameUIWDEnemyScene.bgMax = nil
GameUIWDEnemyScene.sceneMin = nil
GameUIWDEnemyScene.sceneMax = nil
GameUIWDEnemyScene.bgPos = nil
GameUIWDEnemyScene.scenePos = nil
GameUIWDEnemyScene.defenceID = nil
GameUIWDEnemyScene.targetTime = 0
function GameUIWDEnemyScene:Init()
  local tmpSize = GameObjectBattleMapBG:GetFlashObject():InvokeASCallback("_root", "getBGSize")
  GameUIWDEnemyScene.bgSize = LuaUtils:string_split(tmpSize, "^")[1]
  tmpSize = self:GetFlashObject():InvokeASCallback("_root", "GetSelfSize")
  GameUIWDEnemyScene.sceneSize = LuaUtils:string_split(tmpSize, "^")[1]
  tmpSize = self:GetFlashObject():InvokeASCallback("_root", "GetStageSize")
  GameUIWDEnemyScene.stageSize = LuaUtils:string_split(tmpSize, "^")[1]
  DebugOut("bgSize", GameUIWDEnemyScene.bgSize, GameUIWDEnemyScene.sceneSize, GameUIWDEnemyScene.stageSize)
  GameUIWDEnemyScene.bgMax = 0
  GameUIWDEnemyScene.bgMin = -GameUIWDEnemyScene.bgSize + GameUIWDEnemyScene.stageSize
  GameUIWDEnemyScene.sceneMax = 0
  GameUIWDEnemyScene.sceneMin = GameUIWDEnemyScene.stageSize - GameUIWDEnemyScene.sceneSize
  local bgInitX = -GameUIWDEnemyScene.bgSize / 3
  if bgInitX < GameUIWDEnemyScene.bgMin then
    bgInitX = GameUIWDEnemyScene.bgMin
  end
  GameObjectBattleMapBG:GetFlashObject():InvokeASCallback("_root", "resetBGPos")
  GameObjectBattleMapBG:GetFlashObject():InvokeASCallback("_root", "offsetBGPos", bgInitX, 0, bgInitX, 0)
  GameUIWDEnemyScene.bgPos = bgInitX
  GameUIWDEnemyScene.scenePos = GameUIWDEnemyScene.sceneMin / 2
  self:GetFlashObject():InvokeASCallback("_root", "offset", GameUIWDEnemyScene.scenePos)
  self:GetFlashObject():InvokeASCallback("_root", "init")
  GameGlobalData:RegisterDataChangeCallback("resource", GameUIWDEnemyScene.RefreshSupply)
  GameUIWDEnemyScene.RefreshSupply()
  local userinfo = GameGlobalData:GetData("userinfo")
  local player_main_fleet = GameGlobalData:GetFleetInfo(1)
  local player_avatar = GameUtils:GetPlayerAvatar(nil, player_main_fleet.level)
  local leaderlist = GameGlobalData:GetData("leaderlist")
  local curmatrixindex_m = GameGlobalData.GlobalData.curMatrixIndex
  if leaderlist and curmatrixindex_m then
    player_avatar = GameDataAccessHelper:GetFleetAvatar(leaderlist.leader_ids[curmatrixindex_m], player_main_fleet.level)
  end
  DebugOut("player avatr = ", player_avatar)
  DebugTable(player_main_fleet)
  self:GetFlashObject():InvokeASCallback("_root", "setUserInfo", GameUtils:GetUserDisplayName(userinfo.name), player_avatar)
  GameGlobalData:RegisterDataChangeCallback("vipinfo", GameUIWDEnemyScene.RefreshVipInfo)
  GameUIWDEnemyScene.RefreshVipInfo()
  GameGlobalData:RegisterDataChangeCallback("levelinfo", GameUIWDEnemyScene.RefreshLevelInfo)
  GameUIWDEnemyScene.RefreshLevelInfo()
  GameUIWDEnemyScene:RefreshTarget()
  GameGlobalData:RegisterDataChangeCallback("wd_supply_refresh_time", self.RefreshSupplyCD)
  GameUIWDEnemyScene.RefreshSupplyCD()
  GameUIWDEnemyScene:RefreshDefendHP()
end
function GameUIWDEnemyScene:RefreshDefendHP()
  local obj = GameUIWDEnemyScene:GetFlashObject()
  if obj then
    local percent = GameStateWD.OpponentAllianceDefenceInfo.hp / GameStateWD.OpponentAllianceDefenceInfo.hp_limit
    if GameStateWD.OpponentAllianceDefenceInfo.hp_limit == 0 or GameStateWD.OpponentAllianceDefenceInfo.hp == 0 then
      percent = 0
    else
      percent = math.floor(percent * 100)
      if percent <= 0 then
        percent = 1
      elseif percent > 100 then
        percent = 100
      end
    end
    obj:InvokeASCallback("_root", "setDefendHP", "" .. GameLoader:GetGameText("LC_MENU_WD_BATTLE_FIELD_WALL_TITLE") .. " <font color='#FFCC00'>" .. GameLoader:GetGameText("LC_MENU_Level") .. GameStateWD.OpponentAllianceDefenceInfo.level .. "</font>", percent, "" .. GameUtils.numberConversion(GameStateWD.OpponentAllianceDefenceInfo.hp) .. "/" .. GameUtils.numberConversion(GameStateWD.OpponentAllianceDefenceInfo.hp_limit))
  end
  GameWDAttkBox:RefreshDefenceHP()
end
function GameUIWDEnemyScene:UpdateSupplyCD()
  local obj = GameUIWDEnemyScene:GetFlashObject()
  if obj then
    local leftime = GameUIWDEnemyScene.targetTime - os.time()
    local lefttext = ""
    if leftime > 0 then
      lefttext = GameUtils:formatTimeString(leftime)
    else
    end
    obj:InvokeASCallback("_root", "setSupplyCDTime", lefttext)
  end
end
function GameUIWDEnemyScene.RefreshSupplyCD()
  local refreshTime = GameGlobalData:GetRefreshTime("wd_supply_refresh_time")
  if refreshTime and refreshTime.targetTime then
    GameUIWDEnemyScene.targetTime = refreshTime.targetTime
  elseif refreshTime and refreshTime.wd_next_time > 0 then
    GameUIWDEnemyScene.targetTime = refreshTime.wd_next_time + os.time()
    refreshTime.targetTime = GameUIWDEnemyScene.targetTime
  else
    GameUIWDEnemyScene.targetTime = 0
  end
  GameUIWDEnemyScene:UpdateSupplyCD()
end
function GameUIWDEnemyScene:RefreshTarget()
  if not self:GetFlashObject() or GameStateWD.BothAllianceInfo == nil or #GameStateWD.BothAllianceInfo.infoes == 0 then
    return
  end
  GameWDAttkBox:RefreshTarget()
  self:GetFlashObject():InvokeASCallback("_root", "resetTargets")
  DebugOut("refreshTarget")
  DebugTable(GameStateWD.OpponentAllianceDefenceInfo)
  GameUIWDEnemyScene.defenceID = tonumber(GameStateWD.OpponentAllianceDefenceInfo.leader_id)
  local defenceHP = tonumber(GameStateWD.OpponentAllianceDefenceInfo.leader_hp)
  local members = GameStateWD.BothAllianceInfo.infoes[2].members
  if GameStateWD.BothAllianceInfo and GameStateWD.BothAllianceInfo.infoes and GameStateWD.BothAllianceInfo.infoes[2] and GameStateWD.BothAllianceInfo.infoes[2].members and 0 < #GameStateWD.BothAllianceInfo.infoes[2].members and GameStateWD.OpponentAllianceDefenceInfo and GameStateWD.OpponentAllianceDefenceInfo.leader_id and tonumber(members[1].id) ~= GameUIWDEnemyScene.defenceID then
    local leaderInfo
    for i, v in ipairs(members) do
      if tonumber(v.id) == GameUIWDEnemyScene.defenceID and leaderInfo == nil then
        leaderInfo = LuaUtils:table_rcopy(v)
        table.remove(members, i)
        table.insert(members, 1, leaderInfo)
        break
      end
    end
  end
  local count = 0
  local unkown = false
  local index = 0
  for i = 1, #members do
    DebugOut("member", members[i].id, GameUIWDEnemyScene.defenceID)
    local id = tonumber(members[i].id)
    local battleInfo = GameStateWD:GetUserBattleInfoByID(id)
    local name = GameUtils:GetUserDisplayName(members[i].name)
    local level = members[i].level
    local force = members[i].force
    if 0 < battleInfo.attk_count then
      local isDef = "selected"
      if GameUIWDEnemyScene.defenceID == id then
        if defenceHP == 0 then
          isDef = "defence_leader_broken"
        else
          isDef = "defence_leader_selected"
        end
      end
      local fleetids = ""
      for j = 1, #members[i].fleet_ids do
        DebugOut("members[i].fleetids[j] = ")
        DebugTable(members[i].fleet_ids[j])
        fleetids = fleetids .. GameDataAccessHelper:GetShip(members[i].fleet_ids[j].fleet_id, 0, members[i].fleet_ids[j].levelup) .. "\001"
      end
      self:GetFlashObject():InvokeASCallback("_root", "setTarget", index, id, name, GameUtils.numberConversion(force), level, #members[i].fleet_ids, fleetids, isDef, GameLoader:GetGameText("LC_MENU_ARENA_CAPTION_FORCE"), GameLoader:GetGameText("LC_MENU_WD_BATTLE_FIELD_DEFENCER_TITLE"))
      DebugOut("setTarget", index, id, name, GameUtils.numberConversion(force), level, #members[i].fleet_ids, fleetids, isDef)
    else
      local isDef = "unknown_enemy"
      if GameUIWDEnemyScene.defenceID == id then
        if defenceHP == 0 then
          isDef = "defence_leader_broken"
        else
          isDef = "uknown_leader"
        end
      end
      self:GetFlashObject():InvokeASCallback("_root", "setTargetUnkown", index, id, name, isDef, GameLoader:GetGameText("LC_MENU_ARENA_CAPTION_FORCE"), GameLoader:GetGameText("LC_MENU_WD_BATTLE_FIELD_DEFENCER_TITLE"))
      DebugOut("setTargetUnkown", index, id, name, isDef)
    end
    index = index + 1
  end
end
function GameUIWDEnemyScene.RefreshLevelInfo()
  local levelInfo = GameGlobalData:GetData("levelinfo")
  local obj = GameUIWDEnemyScene:GetFlashObject()
  if obj then
    obj:InvokeASCallback("_root", "setUserLevel", levelInfo.level)
  end
end
function GameUIWDEnemyScene.RefreshVipInfo()
  local curLevel = GameVipDetailInfoPanel:GetVipLevel()
  local obj = GameUIWDEnemyScene:GetFlashObject()
  if obj then
    local m = 0
    local k = 0
    if curLevel > 10 then
      m = math.floor(curLevel / 10)
      k = curLevel - m * 10
    else
      k = curLevel
    end
    obj:InvokeASCallback("_root", "refreshVipInfo", m, k)
  end
end
function GameUIWDEnemyScene.RefreshSupply()
  local resource = GameGlobalData:GetData("resource")
  local obj = GameUIWDEnemyScene:GetFlashObject()
  if obj then
    obj:InvokeASCallback("_root", "setSupply", GameUtils.numberConversion(resource.wd_supply), 32)
  end
end
function GameUIWDEnemyScene:OnAddToGameState()
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  if FlashMemTracker then
    FlashMemTracker.DumpAllFlashMemstate()
  end
  self:Init()
  self:DisplayRankInfo()
end
function GameUIWDEnemyScene:OnEraseFromGameState()
  self:UnloadFlashObject()
end
function GameUIWDEnemyScene:Update(dt)
  local flash_obj = self:GetFlashObject()
  GameUIWDEnemyScene:UpdateSupplyCD()
  if not GameStateWD:IsObjectInState(GameUIBattleResult) and GameStateWD.RestoreAttkBox and not GameStateWD:IsObjectInState(GameWDAttkBox) and GameStateWD.lastEnemyState then
    GameStateWD.RestoreAttkBox = false
    GameStateWD:AddObject(GameWDAttkBox)
  end
  if flash_obj then
    local vipTime = 0
    local vipTimeStr
    if GameVipDetailInfoPanel.mTmpInfoFetchTime and 0 < GameVipDetailInfoPanel.mTmpInfoFetchTime then
      vipTime = GameVipDetailInfoPanel.mTmpInfo.left_time - (os.time() - GameVipDetailInfoPanel.mTmpInfoFetchTime)
    end
    if vipTime > 0 then
      vipTimeStr = GameUtils:formatTimeString(vipTime)
    end
    flash_obj:Update(dt)
    flash_obj:InvokeASCallback("_root", "OnUpdate", dt, vipTimeStr)
    self:DisplayRankInfo()
  end
end
GameUIWDEnemyScene.isNeedPop = false
function GameUIWDEnemyScene:BuySupply()
  local packet = {type = "wd_supply"}
  GameUIWDEnemyScene.isNeedPop = true
  NetMessageMgr:SendMsg(NetAPIList.supply_info_req.Code, packet, GameUIWDEnemyScene.serverCallack, true, nil)
end
function GameUIWDEnemyScene.BuyBattleSupply()
  GameUIWDEnemyScene.isNeedPop = false
  local packet = {type = "wd_supply"}
  NetMessageMgr:SendMsg(NetAPIList.supply_exchange_req.Code, packet, GameUIWDEnemyScene.serverCallack, true, nil)
end
function GameUIWDEnemyScene.serverCallack(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and (content.api == NetAPIList.supply_info_req.Code or content.api == NetAPIList.supply_exchange_req.Code) then
    if content.code ~= 0 then
      local GameVip = LuaObjectManager:GetLuaObject("GameVip")
      GameVip:CheckIsNeedShowVip(content.code)
    end
    return true
  end
  if msgtype == NetAPIList.supply_info_ack.Code then
    do
      local price = content.exchange_cost
      local count = content.count
      DebugOut("price is", price)
      if GameUIWDEnemyScene.isNeedPop then
        local text_content = GameLoader:GetGameText("LC_MENU_BUY_BATTLE_SUPPLY_ASK")
        text_content = string.gsub(text_content, "<credits_num>", price)
        text_content = string.gsub(text_content, "<supply_num>", count)
        local function callback()
          GameUIWDEnemyScene.BuyBattleSupply(price)
        end
        GameUtils:CreditCostConfirm(text_content, callback)
      end
      return true
    end
  end
  return false
end
function GameUIWDEnemyScene:OnFSCommand(cmd, arg)
  DebugWD("GameUIWDEnemyScene:OnFSCommand: ", cmd)
  if cmd == "moveout" then
    GameStateWD:MoveOutEnemyScene()
  elseif cmd == "TouchShield" then
  elseif cmd == "buy" then
    GameUIWDEnemyScene:BuySupply()
  elseif cmd == "reward_released" then
    GameUIWDStuff:Show(GameUIWDStuff.menuType.menu_type_reword)
  elseif cmd == "recruit_released" then
    GameUIWDStuff:Show(GameUIWDStuff.menuType.menu_type_rank)
  elseif cmd == "attk" then
    GameWDAttkBox.isDenfence = false
    if GameUIWDEnemyScene.defenceID == tonumber(arg) then
      GameWDAttkBox.isDenfence = true
    end
    GameWDAttkBox.attkID = tonumber(arg)
    GameStateWD:AddObject(GameWDAttkBox)
  elseif cmd == "attkCore" then
    GameWDAttkBox.isDenfence = false
    GameWDAttkBox.attkID = 2
    GameStateWD:AddObject(GameWDAttkBox)
  elseif cmd == "attkDefence" then
    if GameStateWD.OpponentAllianceDefenceInfo.level > 0 then
      GameWDAttkBox.isDenfence = false
      GameWDAttkBox.attkID = 1
      GameStateWD:AddObject(GameWDAttkBox)
    end
  elseif cmd == "move" then
    local offsetX = tonumber(arg)
    GameUIWDEnemyScene.scenePos = GameUIWDEnemyScene.scenePos + offsetX
    if GameUIWDEnemyScene.scenePos < GameUIWDEnemyScene.sceneMin then
      GameUIWDEnemyScene.scenePos = GameUIWDEnemyScene.sceneMin
    elseif GameUIWDEnemyScene.scenePos > GameUIWDEnemyScene.sceneMax then
      GameUIWDEnemyScene.scenePos = GameUIWDEnemyScene.sceneMax
    end
    self:GetFlashObject():InvokeASCallback("_root", "offset", GameUIWDEnemyScene.scenePos)
    local tmp = GameUIWDEnemyScene.bgPos
    GameUIWDEnemyScene.bgPos = GameUIWDEnemyScene.bgPos + offsetX * GameUIWDEnemyScene.bgMin / GameUIWDEnemyScene.sceneMin
    if GameUIWDEnemyScene.bgPos < GameUIWDEnemyScene.bgMin then
      GameUIWDEnemyScene.bgPos = GameUIWDEnemyScene.bgMin
    elseif GameUIWDEnemyScene.bgPos > GameUIWDEnemyScene.bgMax then
      GameUIWDEnemyScene.bgPos = GameUIWDEnemyScene.bgMax
    end
    GameObjectBattleMapBG:GetFlashObject():InvokeASCallback("_root", "offsetBGPos2", GameUIWDEnemyScene.bgPos, GameUIWDEnemyScene.bgPos - tmp)
  elseif cmd == "Help_released" then
    GameUIWDStuff:Show(GameUIWDStuff.menuType.menu_type_help)
    GameUIWDStuff:SetHelpText(GameLoader:GetGameText("LC_MENU_WD_HELP"))
  end
end
function GameUIWDEnemyScene._OnTimerTick()
  GameUIWDEnemyScene:DisplayRankInfo()
end
function GameUIWDEnemyScene:DisplayRankInfo()
  local flash = GameUIWDEnemyScene:GetFlashObject()
  if not flash then
    return
  end
  if not GameStateWD.WDBaseInfo or GameStateWD.WDBaseInfo.State ~= GameStateWD.WDState.BATTLE then
    return
  end
  if not GameStateWD.BothAllianceInfo then
    return
  end
  local allianceName = GameStateWD.BothAllianceInfo.infoes[1].name .. "\001" .. GameStateWD.BothAllianceInfo.infoes[2].name
  local alliancePoint = GameStateWD:GetAlliancePoints(true) .. "\001" .. GameStateWD:GetAlliancePoints(false)
  local player_info = GameGlobalData:GetUserInfo()
  local myName = GameUtils:GetUserDisplayName(player_info.name)
  local myRank, myPoint = GameUIWDInBattle:GetMyRankAndPoint(tonumber(player_info.player_id))
  flash:InvokeASCallback("_root", "setRankList", allianceName, alliancePoint, myName, "", myPoint, "", "", "")
  return nil
end
