WVEInfoPanelReward = luaClass(nil)
local WVEGameManeger = LuaObjectManager:GetLuaObject("WVEGameManeger")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
local GameUIKrypton = LuaObjectManager:GetLuaObject("GameUIKrypton")
function WVEInfoPanelReward:ctor(wveGameManeger)
  self.mGameManger = wveGameManeger
  self.TAB_STATE = {DAMAGE = 1, KILL = 2}
  self.mCurTab = self.TAB_STATE.KILL
end
function WVEInfoPanelReward:OnFSCommand(cmd, arg)
  if cmd == "ShowRewardPanel" then
    local flashObj = self.mGameManger:GetFlashObject()
    local descText = ""
    if flashObj then
      flashObj:InvokeASCallback("_root", "ShowRewardPanel", descText)
      self.IsShowRewardWindow = true
    end
    self.mCurTab = self.TAB_STATE.KILL
    self:RequestKillReward()
  elseif cmd == "UpdateRewardItemData" then
    self:UpdateRewardItemData(tonumber(arg))
  elseif cmd == "ClickRewardPanelTab1" then
    if self.mCurTab ~= self.TAB_STATE.KILL then
      self.mCurTab = self.TAB_STATE.KILL
      self:RequestKillReward()
    end
  elseif cmd == "ClickRewardPanelTab2" then
    if self.mCurTab ~= self.TAB_STATE.DAMAGE then
      self.mCurTab = self.TAB_STATE.DAMAGE
      self:RequestDamageReward()
    end
  elseif cmd == "awardItemReleased" then
    DebugOut("awardItemReleased:", arg)
    local awardItemIndex, awardItemSubIndex = arg:match("(%d+)\001(%d+)")
    DebugOut(awardItemIndex, ",", awardItemSubIndex)
    local clickedItem = self:GetSubRewardItem(tonumber(awardItemIndex), tonumber(awardItemSubIndex))
    if clickedItem then
      self:ShowItemDetil(clickedItem, clickedItem.item_type)
    end
  elseif cmd == "HideRewardWindow" then
    local flashObj = WVEGameManeger:GetFlashObject()
    if flashObj then
      flashObj:InvokeASCallback("_root", "HideRewardWindow")
      self.IsShowRewardWindow = false
    end
  else
    return false
  end
  return true
end
function WVEInfoPanelReward:GetSubRewardItem(itemIndex, subIndex)
  if self.RewardDatas and self.RewardDatas[itemIndex] and self.RewardDatas[itemIndex].rewards then
    return self.RewardDatas[itemIndex].rewards[subIndex].item
  end
  return nil
end
function WVEInfoPanelReward:ShowItemDetil(item_, item_type)
  if GameHelper:IsResource(item_type) then
    return
  end
  local item = item_
  if item_type == "krypton" then
    if item.level == 0 then
      item.level = 1
    end
    local detail = GameUIKrypton:TryQueryKryptonDetail(item.number, item.level, function(msgType, content)
      local ret = GameUIKrypton.QueryKryptonDetailCallBack(msgType, content)
      if ret then
        local tmp = GameGlobalData:GetKryptonDetail(item.number, item.level)
        if tmp == nil then
          return false
        end
        ItemBox:SetKryptonBox(tmp, item.number)
        local operationTable = {}
        operationTable.btnUnloadVisible = false
        operationTable.btnUpgradeVisible = false
        operationTable.btnUseVisible = false
        operationTable.btnDecomposeVisible = false
        ItemBox:ShowKryptonBox(320, 240, operationTable)
      end
      return ret
    end)
    if detail == nil then
    else
      ItemBox:SetKryptonBox(detail, item.number)
      local operationTable = {}
      operationTable.btnUnloadVisible = false
      operationTable.btnUpgradeVisible = false
      operationTable.btnUseVisible = false
      operationTable.btnDecomposeVisible = false
      ItemBox:ShowKryptonBox(320, 240, operationTable)
    end
  end
  if item_type == "item" then
    item.cnt = 1
    ItemBox:showItemBox("Item", item, tonumber(item.number), 320, 480, 200, 200, "", nil, "", nil)
  end
  if item_type == "fleet" then
    ItemBox:ShowCommanderDetail2(tonumber(item.number))
  end
end
function WVEInfoPanelReward:RequestKillReward()
  local content = {}
  content.rank_type = 1
  NetMessageMgr:SendMsg(NetAPIList.prime_award_req.Code, content, self.RequestRewardCallback, true, nil)
end
function WVEInfoPanelReward:RequestDamageReward()
  local content = {}
  content.rank_type = 2
  NetMessageMgr:SendMsg(NetAPIList.prime_award_req.Code, content, self.RequestRewardCallback, true, nil)
end
function WVEInfoPanelReward.RequestRewardCallback(msgType, content)
  DebugOut("RequestKillRewardCallback:", msgType)
  DebugTable(content)
  if msgType == NetAPIList.prime_award_ack.Code then
    WVEGameManeger.mWVEInfoPanelReward:RecordRewardDatas(content)
    return true
  elseif msgType == NetAPIList.common_ack.Code and content and content.api == NetAPIList.prime_award_req.Code then
    return true
  end
  return false
end
function WVEInfoPanelReward:RecordRewardDatas(content)
  local flashObj = WVEGameManeger:GetFlashObject()
  if not flashObj then
    return
  end
  self.RewardDatas = {}
  for k, v in pairs(content.infoes) do
    local rewardItemData = {}
    if v.begin_rank == v.end_rank then
      rewardItemData.rank = "TOP" .. tostring(v.begin_rank)
    else
      rewardItemData.rank = "TOP" .. tostring(v.begin_rank) .. "~" .. tostring(v.end_rank)
    end
    rewardItemData.rewards = {}
    for rewardK, rewardV in pairs(v.awards) do
      local awardItem = {}
      local count = GameHelper:GetAwardCount(rewardV.item_type, rewardV.number, rewardV.no)
      local icon = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(rewardV, nil, WVEInfoPanelReward.donamicDownloadFinishCallback)
      awardItem.iconFrame = icon
      awardItem.num = GameUtils.numberConversion(count)
      awardItem.item = rewardV
      table.insert(rewardItemData.rewards, awardItem)
    end
    table.insert(self.RewardDatas, rewardItemData)
  end
  DebugOut("RecordRewardDatas:", #content.infoes)
  flashObj:InvokeASCallback("_root", "initRewardListBox", #content.infoes)
end
function WVEInfoPanelReward.donamicDownloadFinishCallback()
  local flashObj = WVEGameManeger:GetFlashObject()
  if not flashObj then
    return
  end
  if WVEGameManeger.mWVEInfoPanelReward.RewardDatas then
    flashObj:InvokeASCallback("_root", "initRewardListBox", #WVEGameManeger.mWVEInfoPanelReward.RewardDatas)
  end
end
function WVEInfoPanelReward:UpdateRewardItemData(itemIndex)
  local flashObj = WVEGameManeger:GetFlashObject()
  if not flashObj then
    return
  end
  if self.RewardDatas and self.RewardDatas[itemIndex] then
    DebugOut("UpdateRewardItemData:", itemIndex)
    DebugTable(self.RewardDatas[itemIndex])
    local data = self.RewardDatas[itemIndex]
    flashObj:InvokeASCallback("_root", "UpdateRewardItemData", itemIndex, data.rewards, data.rank)
  end
end
