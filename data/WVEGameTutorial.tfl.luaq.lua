local WVEGameTutorial = LuaObjectManager:GetLuaObject("WVEGameTutorial")
local GameUICommonDialog = LuaObjectManager:GetLuaObject("GameUICommonDialog")
local WVEGameManeger = LuaObjectManager:GetLuaObject("WVEGameManeger")
local GameStateWVE = GameStateManager.GameStateWVE
local QuestTutorialWVE = TutorialQuestManager.QuestTutorialWVE
WVEGameTutorial.StoryTable = {
  p1 = 1100049,
  p2 = 1100050,
  p3 = 1100051,
  p4 = 1100052
}
WVEGameTutorial.TutorialStep = {
  [1] = "p0",
  [2] = "p1",
  [3] = "p2",
  [4] = "p3"
}
function WVEGameTutorial:OnAddToGameState()
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  self.mCurrentStep = 1
  self.mFinished = self.mFinished or false
end
function WVEGameTutorial:OnEraseFromGameState()
  self:UnloadFlashObject()
  collectgarbage("collect")
end
function WVEGameTutorial:StartTurorail()
  GameUICommonDialog:PlayStory({
    WVEGameTutorial.StoryTable.p1
  }, self.NextStep)
end
function WVEGameTutorial:IsFinishedLocal()
  return self.mFinished
end
function WVEGameTutorial:GotoTutorialStep(step)
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    flash_obj:InvokeASCallback("_root", "PlayerTutorialStep", step)
  end
end
function WVEGameTutorial:OnFSCommand(cmd, arg)
  if cmd == "nextStep" then
    if self.mCurrentStep >= 4 then
      GameStateWVE:EraseObject(WVEGameTutorial)
      GameStateWVE:AddObject(WVEGameManeger)
      QuestTutorialWVE:SetFinish(true)
      self.mFinished = true
      GameStateWVE:ForceCompleteCammandList()
      WVEGameManeger:GotoState(EWVEGameState.STATE_ENTER_GAME)
    else
      self.mCurrentStep = self.mCurrentStep + 1
      self:GotoTutorialStep(self.TutorialStep[self.mCurrentStep])
    end
  elseif cmd == "p2" then
    GameUICommonDialog:PlayStory({
      WVEGameTutorial.StoryTable.p2
    }, self.NextStep)
  elseif cmd == "p3" then
    GameUICommonDialog:PlayStory({
      WVEGameTutorial.StoryTable.p3
    }, self.NextStep)
  elseif cmd == "p4" then
    GameUICommonDialog:PlayStory({
      WVEGameTutorial.StoryTable.p4
    }, self.NextStep)
  end
end
function WVEGameTutorial.NextStep()
  WVEGameTutorial:OnFSCommand("nextStep", WVEGameTutorial.mCurrentStep + 1)
end
