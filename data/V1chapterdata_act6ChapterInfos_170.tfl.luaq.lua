local ChapterInfos_170 = GameData.chapterdata_act6.ChapterInfos_170
ChapterInfos_170[1] = {
  ChaperID = 1,
  ICON = "icon1",
  MapIndex = 1,
  BossPos = 5,
  EntroStory = {6101, 6102},
  AFTER_FINISH = {
    6105,
    6106,
    6107
  }
}
ChapterInfos_170[2] = {
  ChaperID = 2,
  ICON = "icon2",
  MapIndex = 2,
  BossPos = 10,
  EntroStory = {6201},
  AFTER_FINISH = {
    6203,
    6204,
    6205,
    6206
  }
}
ChapterInfos_170[3] = {
  ChaperID = 3,
  ICON = "icon30",
  MapIndex = 3,
  BossPos = 16,
  EntroStory = {6301},
  AFTER_FINISH = {
    6303,
    6304,
    6305,
    6306
  }
}
ChapterInfos_170[4] = {
  ChaperID = 4,
  ICON = "icon400",
  MapIndex = 1,
  BossPos = 18,
  EntroStory = {6401},
  AFTER_FINISH = {6405, 6406}
}
ChapterInfos_170[5] = {
  ChaperID = 5,
  ICON = "icon5",
  MapIndex = 1,
  BossPos = 18,
  EntroStory = {6501},
  AFTER_FINISH = {
    6502,
    6503,
    6504
  }
}
ChapterInfos_170[6] = {
  ChaperID = 6,
  ICON = "icon400",
  MapIndex = 1,
  BossPos = 18,
  EntroStory = {6601},
  AFTER_FINISH = {
    6606,
    6607,
    6608
  }
}
ChapterInfos_170[7] = {
  ChaperID = 7,
  ICON = "icon6",
  MapIndex = 1,
  BossPos = 18,
  EntroStory = {6701, 6702},
  AFTER_FINISH = {
    6711,
    6712,
    6713,
    6714
  }
}
ChapterInfos_170[8] = {
  ChaperID = 8,
  ICON = "icon400",
  MapIndex = 1,
  BossPos = 18,
  EntroStory = {6801},
  AFTER_FINISH = {
    6812,
    6813,
    6814,
    6815,
    6816,
    6817,
    6818,
    6819,
    6820,
    6821,
    6822
  }
}
ChapterInfos_170[9] = {
  ChaperID = 9,
  ICON = "icon5",
  MapIndex = 1,
  BossPos = 18,
  EntroStory = {},
  AFTER_FINISH = {}
}
ChapterInfos_170[10] = {
  ChaperID = 10,
  ICON = "icon5",
  MapIndex = 1,
  BossPos = 18,
  EntroStory = {},
  AFTER_FINISH = {}
}
