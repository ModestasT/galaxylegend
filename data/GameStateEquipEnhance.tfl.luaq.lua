local GameStateEquipEnhance = GameStateManager.GameStateEquipEnhance
local GameStateMainPlanet = GameStateManager.GameStateMainPlanet
local GameFleetInfoBackground = LuaObjectManager:GetLuaObject("GameFleetInfoBackground")
local GameFleetNewEnhance = LuaObjectManager:GetLuaObject("GameFleetNewEnhance")
local GameCommonBackground = LuaObjectManager:GetLuaObject("GameCommonBackground")
local GameUIBuilding = LuaObjectManager:GetLuaObject("GameUIBuilding")
local GameObjectDragger = LuaObjectManager:GetLuaObject("GameObjectDragger")
local GameObjectDraggerItem = GameObjectDragger:NewInstance("bagItem.tfs", false)
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameUIFleetGrowUp = LuaObjectManager:GetLuaObject("GameUIFleetGrowUp")
local GameUIStarSystemPort = LuaObjectManager:GetLuaObject("GameUIStarSystemPort")
local GameUIStarSystemPort_SUB = LuaObjectManager:GetLuaObject("GameUIStarSystemPort_SUB")
local GameFleetEquipment = LuaObjectManager:GetLuaObject("GameFleetEquipment")
local GameFleetEquipmentPop = LuaObjectManager:GetLuaObject("GameFleetEquipmentPop")
GameFleetNewEnhance = GameFleetEquipment
GameStateEquipEnhance.buildingName = "engineering_bay"
GameStateEquipEnhance.isNeedShowEnhanceUI = false
GameStateEquipEnhance.dragItemOffset = -70
GameStateEquipEnhance.isTeamLeague = false
GameStateEquipEnhance.teamLeagueType = 1
SUB_STATE = {STATE_NORMAL = 1, STATE_GROW_UP = 2}
GameStateEquipEnhance.mCurState = SUB_STATE.STATE_NORMAL
function GameStateEquipEnhance:GetPop()
  if not GameFleetEquipmentPop:GetFlashObject() then
    GameFleetEquipmentPop:LoadFlashObject()
    GameStateEquipEnhance:AddObject(GameFleetEquipmentPop)
  end
  return GameFleetEquipmentPop
end
function GameStateEquipEnhance:GoToSubState(subState)
  GameStateEquipEnhance:OnLeaveSubState(GameStateEquipEnhance.mCurState)
  GameStateEquipEnhance.mCurState = subState
  GameStateEquipEnhance:OnEnterSubState(GameStateEquipEnhance.mCurState)
end
function GameStateEquipEnhance:OnEnterSubState(subState)
  if subState == SUB_STATE.STATE_GROW_UP then
    GameStateEquipEnhance:AddObject(GameUIFleetGrowUp)
    GameUIFleetGrowUp:Show()
  elseif subState == SUB_STATE.STATE_NORMAL then
    GameFleetNewEnhance:UpdateFleetLevelInfo()
    GameFleetNewEnhance:GetAllFleetGrowUpDate()
    GameStateEquipEnhance:EraseObject(GameUIFleetGrowUp)
  end
end
function GameStateEquipEnhance:OnLeaveSubState(subState)
end
function GameObjectDraggerItem:OnAddToGameState(state)
  local frame = "item_" .. GameObjectDraggerItem.DraggedItemTYPE
  DebugOut("frame: ", frame)
  self:GetFlashObject():InvokeASCallback("_root", "SetBagItem", frame)
  self:GetFlashObject():InvokeASCallback("_root", "ChangeToBigger")
end
function GameObjectDraggerItem:OnEraseFromGameState(state)
  DebugOut("erase itemDragger from GameFleetNewEnhance and GameFleetInfoUI")
  self.DraggedItemID = -1
  self.unloadItem = false
  self.equipItem = false
  self.dragOnFleet = false
  self.kryptonDestSlot = nil
  self.dragOnBag = false
  self.dragOnCharge = false
end
function GameObjectDraggerItem:OnReachedDestPos()
  DebugOut("GameObjectDraggerItem OnReachedDestPos: ", self.equipItem, self.unloadItem, self.DraggedItemID, self.dragOnFleet)
  DebugOut("GameObjectDraggerItem OnReachedDestPos 222: ", self.dragOnFleet, self.dragOnBag, GameFleetNewEnhance.dragFleetItem, GameFleetNewEnhance.dragBagItem)
  if GameFleetNewEnhance.isDragEquipedItem then
    DebugOut("isDragEquipedItem")
    GameStateEquipEnhance:onEndDragItem()
  end
  if self.DraggedItemID and self.DraggedItemID ~= -1 then
    if self.equipItem then
      GameFleetNewEnhance:ShouldEquip(self.DraggedItemID)
    elseif self.unloadItem then
      GameFleetNewEnhance:UnloadEquip(self.DraggedItemID)
    elseif self.dragOnCharge then
      GameFleetNewEnhance:SetCurrentChargeKrypton(self.DraggedItemID)
      GameStateEquipEnhance:onEndDragItem()
    elseif self.dragOnFleet then
      GameFleetNewEnhance.dragBagKrypton = false
      GameFleetNewEnhance:RefreshCurFleetKrypton()
      if GameFleetNewEnhance.dragFleetItem then
        GameFleetNewEnhance:SwapFleetKryptonPos(self.DraggedItemID, self.kryptonDestSlot)
      elseif GameFleetNewEnhance.dragBagItem then
        GameFleetNewEnhance:EquipKrypton(self.DraggedItemID, self.kryptonDestSlot)
      end
    elseif self.dragOnBag then
      if GameFleetNewEnhance.dragFleetItem then
        GameFleetNewEnhance:EquipOffKrypton(self.DraggedItemID, self.kryptonDestSlot)
      elseif GameFleetNewEnhance.dragBagItem then
        GameFleetNewEnhance:SwapBagKryptonPos(self.DraggedItemID, self.kryptonDestSlot)
      end
    else
      GameStateEquipEnhance:onEndDragItem()
    end
  else
    GameStateEquipEnhance:onEndDragItem()
  end
end
function GameStateEquipEnhance:BeginDragItem(itemId, itemType, initPosX, initPosY, posX, posY)
  if GameFleetNewEnhance.m_subState == 3 or GameFleetNewEnhance.m_subState == 4 then
    return
  end
  DebugOut("BeginDragItem: ", itemId, itemType, posX, posY)
  DebugOut(GameFleetNewEnhance.m_subState)
  GameStateManager:GetCurrentGameState():AddObject(GameObjectDraggerItem)
  GameObjectDraggerItem.DraggedItemID = itemId
  GameObjectDraggerItem.DraggedItemTYPE = itemType
  GameObjectDraggerItem:SetDestPosition(initPosX, initPosY)
  GameObjectDraggerItem.InitPosX = initPosX
  GameObjectDraggerItem.InitPosY = initPosY
  GameObjectDraggerItem:BeginDrag(tonumber(self.dragItemOffset), tonumber(self.dragItemOffset), tonumber(posX), tonumber(posY))
  GameFleetNewEnhance:onBeginDragItem(GameObjectDraggerItem)
end
function GameStateEquipEnhance:OnTouchPressed(x, y)
  if GameStateEquipEnhance:IsObjectInState(GameObjectDraggerItem) and GameObjectDraggerItem:IsAutoMove() or GameUIGlobalScreen.IsShowingLoading then
    return
  end
  GameStateBase.OnTouchPressed(self, x, y)
  DebugOut("GameStateEquipEnhance:OnTouchPressed", x, y)
end
function GameStateEquipEnhance:OnTouchMoved(x, y)
  if GameStateEquipEnhance:IsObjectInState(GameObjectDraggerItem) and GameObjectDraggerItem:IsAutoMove() or GameUIGlobalScreen.IsShowingLoading then
    return
  end
  GameStateBase.OnTouchMoved(self, x, y)
  local dragItemId = GameObjectDraggerItem.DraggedItemID
  if dragItemId and dragItemId ~= -1 then
    GameObjectDraggerItem:UpdateDrag()
  end
  DebugOut("GameStateEquipEnhance:OnTouchMoved", x, y)
end
function GameStateEquipEnhance:OnTouchReleased(x, y)
  if GameStateEquipEnhance:IsObjectInState(GameObjectDraggerItem) and GameObjectDraggerItem:IsAutoMove() or GameUIGlobalScreen.IsShowingLoading then
    return
  end
  GameStateBase.OnTouchReleased(self, x, y)
  local dragItemId = GameObjectDraggerItem.DraggedItemID
  DebugOut("GameStateEquipEnhance:OnTouchReleased", x, y, dragItemId)
  if dragItemId and dragItemId ~= -1 then
    if GameFleetNewEnhance.dragFleetItem or GameFleetNewEnhance.dragBagItem then
      GameStateEquipEnhance:DragKryptonRelease()
    end
    if GameDataAccessHelper:IsItemUsedStrengthen(GameObjectDraggerItem.DraggedItemTYPE) then
      GameObjectDraggerItem:StartAutoMove()
      return
    end
    if GameFleetNewEnhance.isDragEquipedItem then
      if GameFleetNewEnhance.isUnloadEquipedItem then
        DebugOut("isUnloadEquipedItem")
        GameStateEquipEnhance:onEndDragItem()
      else
        DebugOut("StartAutoMove")
        GameObjectDraggerItem:StartAutoMove()
      end
      return
    end
    if GameFleetNewEnhance.dragItemInstance then
      local onequipment = GameFleetNewEnhance:GetFlashObject():InvokeASCallback("_root", "IsOnEquipment")
      DebugOut("onequipment:", type(onequipment), onequipment)
      if onequipment == "true" then
        GameObjectDraggerItem.equipItem = true
        local _, indexInList = GameFleetNewEnhance:IsEquipmentInList(dragItemId)
        if indexInList then
          local itemType = GameFleetNewEnhance.onlyEquip[indexInList].item_type
          local slot = GameDataAccessHelper:GetEquipSlot(itemType)
          local posInfo = GameFleetNewEnhance:GetFlashObject():InvokeASCallback("_root", "GetSlotPos", slot)
          DebugOut("posInfo: ", posInfo)
          local ptPos = LuaUtils:string_split(posInfo, "\001")
          GameObjectDraggerItem:SetDestPosition(tonumber(ptPos[1]), tonumber(ptPos[2]))
        end
      end
    end
    DebugOut("dragItem: ", GameFleetNewEnhance.dragItem, GameFleetNewEnhance.dragItem)
    GameObjectDraggerItem:StartAutoMove()
  end
end
function GameStateEquipEnhance:DragKryptonRelease()
  local posInfo = ""
  if GameFleetNewEnhance:GetFlashObject():InvokeASCallback("_root", "IsOnChargeKrypton") == "true" then
    GameObjectDraggerItem.dragOnCharge = true
    posInfo = GameFleetNewEnhance:GetFlashObject():InvokeASCallback("_root", "GetChargeKryptonPos")
  elseif GameFleetNewEnhance:GetFlashObject():InvokeASCallback("_root", "IsOnFleetKrypton") == "true" then
    GameObjectDraggerItem.dragOnFleet = true
    posInfo = GameFleetNewEnhance:GetFlashObject():InvokeASCallback("_root", "GetFleetKryptonPos")
  elseif GameFleetNewEnhance:GetFlashObject():InvokeASCallback("_root", "IsOnBagKrypton") == "true" then
    if GameFleetNewEnhance.dragBagItem then
      GameObjectDraggerItem.dragOnBag = false
      posInfo = ""
    else
      GameObjectDraggerItem.dragOnBag = true
      posInfo = GameFleetNewEnhance:GetFlashObject():InvokeASCallback("_root", "GetBagKryptonPos")
    end
  end
  DebugOut("GameStateEquipEnhance:DragKryptonRelease: ", posInfo)
  if GameObjectDraggerItem.dragOnCharge or GameObjectDraggerItem.dragOnFleet or GameObjectDraggerItem.dragOnBag then
    local ptPos = LuaUtils:string_split(posInfo, "\001")
    GameObjectDraggerItem:SetDestPosition(tonumber(ptPos[1]), tonumber(ptPos[2]))
    GameObjectDraggerItem.kryptonDestSlot = tonumber(ptPos[3])
  end
end
function GameStateEquipEnhance:onEndDragItem()
  GameFleetNewEnhance:onEndDragItem()
  GameStateEquipEnhance:EraseObject(GameObjectDraggerItem)
end
function GameStateEquipEnhance:OnFocusGain(previousState)
  self.previousState = previousState
  if GameStateEquipEnhance.isNeedShowEnhanceUI then
    GameFleetNewEnhance.isNeedShowEnhanceUI = true
    GameStateEquipEnhance.isNeedShowEnhanceUI = false
  end
  if self.isTeamLeague then
    GameFleetNewEnhance.isTeamLeague = true
    GameFleetNewEnhance.teamLeagueType = self.teamLeagueType
    GameFleetNewEnhance.curTeamIndex = self.teamIndex
  end
  self:AddObject(GameFleetNewEnhance)
  GameObjectDraggerItem:Init()
  GameObjectDraggerItem:LoadFlashObject()
end
function GameStateEquipEnhance:OnFocusLost(newState)
  GameStateEquipEnhance:ResetDefaultFleetAndSlot()
  self:EraseObject(GameFleetNewEnhance)
  GameFleetNewEnhance:UnloadFlashObject()
  if self:IsObjectInState(GameUIStarSystemPort_SUB) then
    self:EraseObject(GameUIStarSystemPort_SUB)
    GameUIStarSystemPort_SUB:UnloadFlashObject()
  end
  if self:IsObjectInState(GameFleetEquipmentPop) then
    self:EraseObject(GameFleetEquipmentPop)
    GameFleetEquipmentPop:UnloadFlashObject()
  end
  self.isTeamLeague = false
  self.teamLeagueType = 1
  self.teamIndex = 1
end
function GameStateEquipEnhance:IsEnhanceEnable()
  return ...
end
function GameStateEquipEnhance:ResetDefaultFleetAndSlot()
  GameFleetNewEnhance.mDefaultFleetIdx = -1
  GameFleetNewEnhance.mDefaultSlot = -1
end
function GameStateEquipEnhance:SetDefaultFleetAndSlot(fleetIdx, slot)
  DebugOut("SetDefaultFleetAndSlot")
  GameFleetNewEnhance.mDefaultFleetIdx = fleetIdx
  GameFleetNewEnhance.mDefaultSlot = slot
end
function GameStateEquipEnhance:EnterEquipEnhance(initTab)
  GameStateManager:SetCurrentGameState(GameStateManager.GameStateEquipEnhance)
end
function GameStateEquipEnhance:EnterTeamLeagueEquipEnhance(formationType, teamIndex)
  self.isTeamLeague = true
  self.teamLeagueType = formationType
  self.teamIndex = teamIndex
  GameStateManager:SetCurrentGameState(GameStateManager.GameStateEquipEnhance)
end
function GameStateEquipEnhance:Update(dt)
  GameStateBase.Update(self, dt)
  if not GameObjectDraggerItem:IsAutoMove() and GameFleetNewEnhance:GetFlashObject() then
    GameFleetNewEnhance:GetFlashObject():InvokeASCallback("_root", "updateEquipGrid", dt)
  end
end
