local NetMessageMgr = NetMessageMgr
local NetAPIList = NetAPIList
local GameTimer = GameTimer
local AlertDataList = AlertDataList
local GameGlobalData = GameGlobalData
local GameUIBarLeft = LuaObjectManager:GetLuaObject("GameUIBarLeft")
local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
local GameUIActivityEvent = LuaObjectManager:GetLuaObject("GameObjectActivityEvent")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
local GameUIPrestigeRankUp = LuaObjectManager:GetLuaObject("GameUIPrestigeRankUp")
local GameUIBarRight = LuaObjectManager:GetLuaObject("GameUIBarRight")
local GameUICommonDialog = LuaObjectManager:GetLuaObject("GameUICommonDialog")
local GameUIBarRight = LuaObjectManager:GetLuaObject("GameUIBarRight")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local GameUIKrypton = LuaObjectManager:GetLuaObject("GameUIKrypton")
local GameVip = LuaObjectManager:GetLuaObject("GameVip")
local GameStateMainPlanet = GameStateManager.GameStateMainPlanet
local GameUIActivityNew = LuaObjectManager:GetLuaObject("GameUIActivityNew")
local GameStateStore = GameStateManager.GameStateStore
local GameGoodsList = LuaObjectManager:GetLuaObject("GameGoodsList")
local GameUIArena = LuaObjectManager:GetLuaObject("GameUIArena")
local GameStateArena = GameStateManager.GameStateArena
local GameObjectMainPlanet = LuaObjectManager:GetLuaObject("GameObjectMainPlanet")
local GameUILab = LuaObjectManager:GetLuaObject("GameUILab")
local GameStateLab = GameStateManager.GameStateLab
local GameUIBarMiddle = LuaObjectManager:GetLuaObject("GameUIBarMiddle")
local GameUIBarRight = LuaObjectManager:GetLuaObject("GameUIBarRight")
local GameStateDaily = GameStateManager.GameStateDaily
local GameUICrusade = require("data1/GameUICrusade.tfl")
local GameStateAlliance = GameStateManager.GameStateAlliance
local GameUIRebuildFleet = LuaObjectManager:GetLuaObject("GameUIRebuildFleet")
local GameUIFactory = LuaObjectManager:GetLuaObject("GameUIFactory")
GameUIActivityEvent.CurrentActivityInfo = nil
GameUIActivityEvent.curEventId = 1
GameUIActivityEvent.background_image_1 = "event_bg_all.png"
GameUIActivityEvent.activity_ui_img = "event_dynamic_all.png"
GameUIActivityEvent.isGatewayRes = nil
function GameUIActivityEvent:OnAddToGameState(game_state)
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  self:RequestEventMenu()
  self:GetFlashObject():InvokeASCallback("_root", "moveIn")
  self:GetFlashObject():InvokeASCallback("_root", "setLocalText", "textActivityOver", GameLoader:GetGameText("LC_MENU_EVENT_OVER_BUTTON"))
end
function GameUIActivityEvent:OnEraseFromGameState()
  DebugOut("GameUIActivityEvent:OnEraseFromGameState")
  self:UnloadFlashObject()
end
local lastUpdatedTime = os.time()
function GameUIActivityEvent:Update(dt)
  local flash_obj = self:GetFlashObject()
  if flash_obj and CurrentActivityInfo then
    flash_obj:Update(dt)
    flash_obj:InvokeASCallback("_root", "onUpdateFrame", dt)
    if os.time() - lastUpdatedTime > 0 then
      lastUpdatedTime = os.time()
      flash_obj:InvokeASCallback("_root", "setTimeText", "")
      if CurrentActivityInfo and CurrentActivityInfo.all_tasks and CurrentActivityInfo.all_tasks[GameUIActivityEvent.curTaskId] and 0 < CurrentActivityInfo.all_tasks[GameUIActivityEvent.curTaskId].end_time - GameUIBarLeft:GetServerTimeUTC() then
        flash_obj:InvokeASCallback("_root", "setTaskActivityTime", GameUtils:formatTimeStringAsPartion2(CurrentActivityInfo.all_tasks[GameUIActivityEvent.curTaskId].end_time - GameUIBarLeft:GetServerTimeUTC()))
      else
        flash_obj:InvokeASCallback("_root", "setTaskTimeOver")
      end
    end
  end
end
function GameUIActivityEvent:OnFSCommand(cmd, arg)
  GameUtils:OnFSCommand(cmd, arg, GameUIActivityEvent)
  if "close_menu" == cmd then
    if GameStateManager:GetCurrentGameState():IsObjectInState(GameUIActivityEvent) then
      GameStateManager:GetCurrentGameState():EraseObject(GameUIActivityEvent)
    end
  elseif "updateTaskItem" == cmd then
    self:UpdateTaskItem(tonumber(arg))
  elseif "selectedTask" == cmd then
    local param = LuaUtils:string_split(arg, "\001")
    self:SelectedTask(tonumber(param[1]), false, tonumber(param[2]))
  elseif "updateEventItem" == cmd then
    self:UpdateEvetnItem(tonumber(arg))
  elseif "selectedSubTask" == cmd then
    self:SelectedSubTask(arg)
  elseif "TreasureBoxClicked" == cmd then
    self:ShowTreasureBoxDetail(tonumber(arg))
  elseif "updateBoxAwardItem" == cmd then
    self:UpdateBoxAwardItem(tonumber(arg))
  elseif "awards_req" == cmd then
    local param = LuaUtils:string_split(arg, "\001")
    self:RequestAwards(tonumber(param[1]), tonumber(param[2]))
  elseif "GotoMenu" == cmd then
    self:TryGotoMenu(arg)
  elseif "itemClicked" == cmd then
    local param = LuaUtils:string_split(arg, "\001")
    self:OnItemClicked(tonumber(param[1]), tonumber(param[2]))
  elseif "getBoxReward" == cmd then
    self:getBoxReward(tonumber(arg))
  elseif "ShowBoxAwardItemDetail" == cmd then
    self:ShowBoxAwardItemDetail(tonumber(arg))
  end
end
function GameUIActivityEvent:getBoxReward(step)
  local param = {
    activity_id = GameUIActivityEvent.curEventId,
    task_id = CurrentActivityInfo.all_tasks[GameUIActivityEvent.curTaskId].task_id,
    step = step
  }
  DebugTable(param)
  NetMessageMgr:SendMsg(NetAPIList.event_progress_award_req.Code, param, getBoxRewardCallback, true)
end
function GameUIActivityEvent:getBoxRewardCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.event_progress_award_req.Code then
    DebugOut(" getBoxRewardCallback ", content.code)
    if content.code == 0 then
    elseif content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  return false
end
function GameUIActivityEvent:RequestEventMenu()
  DebugOut("GameUIActivityEvent:RequestEventMenu", GameUIActivityEvent.curEventId)
  NetMessageMgr:SendMsg(NetAPIList.event_activity_req.Code, {
    id = GameUIActivityEvent.curEventId
  }, self.RequestActivityInfoCallback, true)
end
function GameUIActivityEvent.RequestActivityInfoCallback(msgType, content)
  if msgType == NetAPIList.event_activity_ack.Code then
    if not GameUIActivityEvent:GetFlashObject() then
      GameUIActivityEvent:LoadFlashObject()
      GameUIActivityEvent:RequestEventMenu()
    else
      CurrentActivityInfo = content
      DebugOutPutTable(content, "RequestActivityInfoCallback")
      GameUIActivityEvent.curTaskId = 1
      GameUIActivityEvent.isGatewayRes = not CurrentActivityInfo.localres
      local title = ""
      local Bg_1 = CurrentActivityInfo.background_image
      local UI_img = CurrentActivityInfo.label_img
      local time = ""
      local taskCount = #CurrentActivityInfo.all_tasks
      local showLeftListBgImg = CurrentActivityInfo.left_image
      if GameUIActivityEvent.isGatewayRes and Bg_1 and "" ~= Bg_1 and "undefined" ~= Bg_1 and "event_default_main_bg" ~= Bg_1 then
        Bg_1 = Bg_1 .. ".png"
        local localPath = "data2/" .. DynamicResDownloader:GetFullName(Bg_1, DynamicResDownloader.resType.WELCOME_PIC)
        DebugOut("extInfo.localPath = " .. localPath)
        if DynamicResDownloader:IfResExsit(localPath) then
          GameUIActivityEvent:GetFlashObject():ReplaceTexture(GameUIActivityEvent.background_image_1, Bg_1)
        else
          local extendInfo = {}
          extendInfo.srcRes = GameUIActivityEvent.background_image_1
          extendInfo.curRes = Bg_1
          extendInfo.type = 1
          DynamicResDownloader:AddDynamicRes(Bg_1, DynamicResDownloader.resType.WELCOME_PIC, extendInfo, GameUIActivityEvent.DynamicBackgroundCallback)
        end
      end
      if GameUIActivityEvent.isGatewayRes and UI_img and "" ~= UI_img and "undefined" ~= UI_img and "event_default_main_bg" ~= Bg_1 then
        UI_img = UI_img .. ".png"
        local localPath = "data2/" .. DynamicResDownloader:GetFullName(UI_img, DynamicResDownloader.resType.WELCOME_PIC)
        DebugOut("extInfo.localPath = " .. localPath)
        if DynamicResDownloader:IfResExsit(localPath) then
          GameUIActivityEvent:GetFlashObject():ReplaceTexture(GameUIActivityEvent.activity_ui_img, UI_img)
        else
          local extendInfo = {}
          extendInfo.srcRes = GameUIActivityEvent.activity_ui_img
          extendInfo.curRes = UI_img
          extendInfo.type = 1
          DynamicResDownloader:AddDynamicRes(UI_img, DynamicResDownloader.resType.WELCOME_PIC, extendInfo, GameUIActivityEvent.DynamicBackgroundCallback)
        end
      end
      if Bg_1 == "event_default_main_bg" then
        GameUIActivityEvent.isGatewayRes = false
        UI_img = "event_dynamic_all_2"
      end
      if UI_img == "undefined" then
        UI_img = "event_dynamic_all"
      end
      if GameUIActivityEvent.isGatewayRes == false then
        GameUIActivityEvent:GetFlashObject():ReplaceTexture(GameUIActivityEvent.background_image_1, Bg_1 .. ".png")
        GameUIActivityEvent:GetFlashObject():ReplaceTexture(GameUIActivityEvent.activity_ui_img, UI_img .. ".png")
      end
      GameUIActivityEvent:GetFlashObject():InvokeASCallback("_root", "setLocalText", "textGoto", GameHelper:GetFoatColor(GameUIActivityEvent:GetAreaColor("task_button_goto"), GameLoader:GetGameText("LC_MENU_NEW_SPACE_GOTO_BUTTON")))
      GameUIActivityEvent:GetFlashObject():InvokeASCallback("_root", "setLocalText", "textRecive", GameHelper:GetFoatColor(GameUIActivityEvent:GetAreaColor("task_button_receive"), GameLoader:GetGameText("LC_MENU_SA_RECEIVE_BUTTON")))
      GameUIActivityEvent:GetFlashObject():InvokeASCallback("_root", "setLocalText", "textRecived", GameHelper:GetFoatColor(GameUIActivityEvent:GetAreaColor("task_button_received"), GameLoader:GetGameText("LC_MENU_SA_RECEIVED_INFO")))
      GameUIActivityEvent:GetFlashObject():InvokeASCallback("_root", "setLocalText", "textIntegral", GameHelper:GetFoatColor(GameUIActivityEvent:GetAreaColor("task_points"), GameLoader:GetGameText("LC_MENU_RANK_INTEGRAL_CHAR")))
      GameUIActivityEvent:ShowEventMenu(title, time, taskCount, showLeftListBgImg)
    end
    return true
  else
    return false
  end
  return false
end
function GameUIActivityEvent.DynamicBackgroundCallback(extInfo)
  DebugOut("GameUIActivityEvent.DynamicBackgroundCallback")
  DebugTable(extInfo)
  if GameUIActivityEvent:GetFlashObject() then
    GameUIActivityEvent:GetFlashObject():ReplaceTexture(extInfo.srcRes, extInfo.curRes)
  end
end
function GameUIActivityEvent.DynamicTaskItemBgCallback(extInfo)
  DebugOut("GameUIActivityEvent.DynamicTaskItemBgCallback")
  DebugTable(extInfo)
  local localPath_1 = "data2/" .. DynamicResDownloader:GetFullName(extInfo.imgName, DynamicResDownloader.resType.WELCOME_PIC)
  local localPath_2 = "data2/" .. DynamicResDownloader:GetFullName(extInfo.imgName2, DynamicResDownloader.resType.WELCOME_PIC)
  extInfo.imgName = DynamicResDownloader:IfResExsit(localPath_1) and extInfo.imgName or ""
  extInfo.imgName2 = DynamicResDownloader:IfResExsit(localPath_2) and extInfo.imgName2 or ""
end
function GameUIActivityEvent:ShowEventMenu(title, time, taskCount, showLeftListBgImg)
  DebugOut("GameUIActivityEvent:ShowEventMenu", title, time, taskCount, showLeftListBgImg)
  lastUpdatedTime = os.time()
  local showImgName = ""
  if showLeftListBgImg and "" ~= showLeftListBgImg and "HIDE" ~= showLeftListBgImg and "undefined" ~= showLeftListBgImg then
    showLeftListBgImg = showLeftListBgImg .. ".png"
    local localPath = "data2/" .. DynamicResDownloader:GetFullName(showLeftListBgImg, DynamicResDownloader.resType.WELCOME_PIC)
    if DynamicResDownloader:IfResExsit(localPath) then
      showImgName = showLeftListBgImg
    else
      local extendInfo = {}
      extendInfo.imgName = showLeftListBgImg
      DynamicResDownloader:AddDynamicRes(showLeftListBgImg, DynamicResDownloader.resType.WELCOME_PIC, extendInfo, GameUIActivityEvent.DynamicLeftListBgCallback)
    end
  end
  if "HIDE" == showLeftListBgImg then
    showImgName = showLeftListBgImg
  end
  self:GetFlashObject():InvokeASCallback("_root", "InitMenu", title, time, taskCount, showImgName)
end
function GameUIActivityEvent:UpdateTaskItem(taskId)
  local taskInfo = CurrentActivityInfo.all_tasks[taskId]
  DebugOut("Lua UpdateTaskItem", taskId, taskInfo == nil)
  if taskInfo then
    local text = GameLoader:GetGameText(taskInfo.label)
    local label_clr = self:GetAreaColor("label_button_unselect")
    local label_2_clr = self:GetAreaColor("label_button_selected")
    local label = GameHelper:GetFoatColor(label_clr, text)
    local label_2 = GameHelper:GetFoatColor(label_2_clr, text)
    local isTaskOver = false
    local stat = "locked"
    if taskInfo.start_time < GameUIBarLeft:GetServerTimeUTC() and taskInfo.end_time > GameUIBarLeft:GetServerTimeUTC() then
      stat = "normal"
    end
    if taskId == GameUIActivityEvent.curTaskId then
      stat = "selected"
    end
    if taskInfo.end_time < GameUIBarLeft:GetServerTimeUTC() then
      isTaskOver = true
    end
    local hasNews = self:CheckTaskHasNews(taskInfo, taskId)
    local Bg = ""
    local Bg2 = ""
    local bgFullName = ""
    local bgFullName2 = ""
    if Bg and "" ~= Bg and ".png" ~= Bg and "undefined.png" ~= Bg then
      local localPath = "data2/" .. DynamicResDownloader:GetFullName(Bg, DynamicResDownloader.resType.WELCOME_PIC)
      if DynamicResDownloader:IfResExsit(localPath) then
        DebugOut("extInfo.localPath = " .. localPath)
        bgFullName = Bg
      else
        local extendInfo = {}
        extendInfo.taskId = taskId
        extendInfo.imgName = Bg
        extendInfo.imgName2 = Bg2
        DynamicResDownloader:AddDynamicRes(Bg, DynamicResDownloader.resType.WELCOME_PIC, extendInfo, GameUIActivityEvent.DynamicTaskItemBgCallback)
      end
    end
    if Bg2 and "" ~= Bg2 and ".png" ~= Bg2 and "undefined.png" ~= Bg2 then
      local localPath = "data2/" .. DynamicResDownloader:GetFullName(Bg2, DynamicResDownloader.resType.WELCOME_PIC)
      if DynamicResDownloader:IfResExsit(localPath) then
        DebugOut("extInfo.localPath = " .. localPath)
        bgFullName2 = Bg2
      else
        local extendInfo = {}
        extendInfo.taskId = taskId
        extendInfo.imgName = Bg
        extendInfo.imgName2 = Bg2
        DynamicResDownloader:AddDynamicRes(Bg2, DynamicResDownloader.resType.WELCOME_PIC, extendInfo, GameUIActivityEvent.DynamicTaskItemBgCallback)
      end
    end
    self:GetFlashObject():InvokeASCallback("_root", "SetTaskListItem", taskId, label, label_2, stat, hasNews, isTaskOver, bgFullName, bgFullName2)
  end
end
function GameUIActivityEvent:CheckTaskHasNews(taskInfo, taskId)
  for i = 1, #taskInfo.sub_tasks do
    if taskInfo.sub_tasks[i].status == 0 then
      return true
    end
  end
  for _, pawards in pairs(taskInfo.progress_awards) do
    if pawards.status == 0 then
      return true
    end
  end
  return false
end
function GameUIActivityEvent:MakeGroupOfSubTask(taskId)
  local subTaskGroup = {}
  local sub_tasks = CurrentActivityInfo.all_tasks[taskId].sub_tasks
  for i = 1, #sub_tasks do
    if not subTaskGroup[sub_tasks[i].group] then
      subTaskGroup[sub_tasks[i].group] = {}
    end
    table.insert(subTaskGroup[sub_tasks[i].group], sub_tasks[i])
  end
  CurrentActivityInfo.all_tasks[taskId].subTaskGroup = subTaskGroup
  DebugOut("GameUIActivityEvent:MakeGroupOfSubTask", taskId)
  DebugTable(CurrentActivityInfo.all_tasks[taskId])
end
GameUIActivityEvent.sub_task_info = {}
GameUIActivityEvent.curSubTaskTag = nil
GameUIActivityEvent.curTaskId = 1
function GameUIActivityEvent:SelectedTask(taskId, isRefresh, isLocked)
  DebugTable(CurrentActivityInfo)
  local taskDetail = CurrentActivityInfo.all_tasks[taskId]
  if taskDetail and taskDetail.start_time < GameUIBarLeft:GetServerTimeUTC() then
    local preSelectedIndex = GameUIActivityEvent.curTaskId
    GameUIActivityEvent.curTaskId = taskId
    if isLocked ~= nil and isLocked == 1 then
      GameUIActivityEvent:UpdateTaskItem(taskId)
      if taskId ~= preSelectedIndex then
        GameUIActivityEvent:UpdateTaskItem(preSelectedIndex)
      end
    end
    local progress_awards = taskDetail.progress_awards
    local finishedNum = taskDetail.finished_num
    local totalNum = taskDetail.total_num
    local isTaskOver = taskDetail.end_time < GameUIBarLeft:GetServerTimeUTC()
    local isShowBarBg = CurrentActivityInfo.progressbar_bg ~= "HIDE"
    local timeTextColor = self:GetAreaColor("label_end_time")
    local progressbarTextColor = self:GetAreaColor("processbar_number")
    local task_tabClr = self:GetAreaColor("task_group_tab")
    local showRightImg = CurrentActivityInfo.right_image ~= "HIDE"
    if not taskDetail.subTaskGroup then
      GameUIActivityEvent:MakeGroupOfSubTask(taskId)
      taskDetail = CurrentActivityInfo.all_tasks[GameUIActivityEvent.curTaskId]
    end
    GameUIActivityEvent:GetSubTaskInfo()
    if not isRefresh or not GameUIActivityEvent.curSubTaskTag then
      GameUIActivityEvent.curSubTaskTag = GameUIActivityEvent.sub_task_info[1].key
    end
    DebugOut("GameUIActivityEvent:SelectedTask XX ", totalNum, finishedNum)
    local param = {}
    param.barTotalNum = totalNum
    param.barCurNum = finishedNum
    param.treasureBoxInfo = progress_awards
    param.subTasks = GameUIActivityEvent.sub_task_info
    param.Bg = Bg
    param.subType = GameUIActivityEvent.curSubTaskTag
    param.isTaskOver = isTaskOver
    param.isShowBarBg = isShowBarBg
    param.timeTextColor = timeTextColor
    param.progressbarTextColor = progressbarTextColor
    param.task_tabClr = task_tabClr
    param.showRightImg = showRightImg
    local txtInfo = "LC_MENU_EVENT_TASK_NAME_" .. taskDetail.task_name
    param.txtInfo = GameLoader:GetGameText(txtInfo)
    self:GetFlashObject():InvokeASCallback("_root", "setMainMenuInfo", param)
  elseif taskDetail and taskDetail.start_time > GameUIBarLeft:GetServerTimeUTC() then
    local day = GameUtils:formatTimeStringAsPartion(taskDetail.start_time - GameUIBarLeft:GetServerTimeUTC())
    local infoContent = string.gsub(GameLoader:GetGameText("LC_MENU_EVENT_TASK_LOCKED_TIP"), "<unlockTime>", day)
    DebugOut(" SelectedTask showTip", day, infoContent)
    GameTip:Show(infoContent)
  end
end
function GameUIActivityEvent:GetSubTaskInfo()
  GameUIActivityEvent.sub_task_info = {}
  local taskDetail = CurrentActivityInfo.all_tasks[GameUIActivityEvent.curTaskId]
  for k, v in pairs(taskDetail.subTaskGroup) do
    local subMenuInfo = {}
    subMenuInfo.key = k
    subMenuInfo.title = GameLoader:GetGameText("LC_MENU_EVENT_TASK_GROUP_NAME_" .. k)
    for i = 1, #v do
      if v[i].status == 0 then
        subMenuInfo.hasNews = true
        break
      end
    end
    table.insert(GameUIActivityEvent.sub_task_info, subMenuInfo)
  end
  DebugOutPutTable(GameUIActivityEvent.sub_task_info, "GameUIActivityEvent:GetSubTaskInfo")
end
function GameUIActivityEvent:UpdateEvetnItem(index)
  local subTaskDetail = CurrentActivityInfo.all_tasks[GameUIActivityEvent.curTaskId].subTaskGroup[GameUIActivityEvent.curSubTaskTag]
  local detail = subTaskDetail[index]
  DebugOut("GameUIActivityEvent:UpdateEvetnItem", index)
  DebugTable(subTaskDetail)
  if detail then
    local awards = {}
    for i = 1, 4 do
      awards[i] = {}
      if detail.awards[i] then
        local tmp_type = detail.awards[i].item_type
        local tmp_number = detail.awards[i].number
        local no = detail.awards[i].no
        no = no or 1
        tmp_type, tmp_number = self:getAwardItemDetailInfo(tmp_type, tmp_number, no)
        awards[i].item_type = tmp_type
        awards[i].item_count = no
        awards[i].item_number = tmp_number
        if detail.awards[i].item_type == "item" or detail.awards[i].item_type == "krypton" then
          awards[i].item_number = GameUtils.numberConversion(detail.awards[i].no)
        end
        local theitem = detail.awards[i]
        awards[i].iconframe = GameHelper:GetAwardTypeIconFrameName(theitem.item_type, theitem.number)
        if theitem.item_type == "medal_item" or theitem.item_type == "medal" then
          awards[i].nquality = GameHelper:GetMedalQuality(theitem.item_type, theitem.number)
          awards[i].item_number = GameUtils.numberConversion(detail.awards[i].no)
        end
        awards[i].iconpic = GameHelper:GetGameItemDownloadPng(theitem.item_type, theitem.number)
      else
        awards[i].item_type = "empty"
        awards[i].item_number = 0
        awards[i].item_count = 1
      end
    end
    local status = detail.status + 1
    local totalNum = detail.total_num
    local finishedNum = detail.finished_num
    local point = detail.point
    local awardId = detail.id
    local titleColor = self:GetAreaColor("task_name")
    local title = GameHelper:GetFoatColor(titleColor, GameLoader:GetGameText(detail.title))
    local descColor = self:GetAreaColor("task_description")
    local desc = GameHelper:GetFoatColor(descColor, GameLoader:GetGameText(detail.desc))
    local itemNumColor = self:GetAreaColor("item_number")
    local conditionNumClr = self:GetAreaColor("task_condition_number")
    local Type = detail.type
    local Bg_1 = ""
    local ButtonBg_1 = ""
    local ButtonBg_2 = ""
    local points_bg = ""
    local itemBg = ""
    DebugTable(awards)
    local isTaskOver = CurrentActivityInfo.all_tasks[GameUIActivityEvent.curTaskId].end_time < GameUIBarLeft:GetServerTimeUTC()
    if not self:CheckMissionBgImg(index, status, Bg_1, ButtonBg_1, ButtonBg_2, itemBg, points_bg) then
      Bg_1 = ""
      ButtonBg_1 = ""
      ButtonBg_2 = ""
      points_bg = ""
      itemBg = ""
    end
    self:GetFlashObject():InvokeASCallback("_root", "SetEventListItem", index, finishedNum, totalNum, point, status, title, awardId, desc, Type, awards, isTaskOver, Bg_1, ButtonBg_1, ButtonBg_2, itemBg, points_bg, itemNumColor, conditionNumClr)
  end
end
function GameUIActivityEvent:SelectedSubTask(type)
  GameUIActivityEvent.curSubTaskTag = type
  DebugOut("SelectedSubTask", type)
  local taskDetail = CurrentActivityInfo.all_tasks[GameUIActivityEvent.curTaskId]
  local subTaskDetail = taskDetail.subTaskGroup[GameUIActivityEvent.curSubTaskTag]
  local sortFunc = function(a, b)
    if a.status < b.status then
      return true
    elseif a.status == b.status then
      return a.id < b.id
    elseif a.status > b.status then
      return false
    end
  end
  table.sort(subTaskDetail or {}, sortFunc)
  self:GetFlashObject():InvokeASCallback("_root", "InitEventList", #subTaskDetail)
end
GameUIActivityEvent.SelectedBoxDetail = nil
function GameUIActivityEvent:ShowTreasureBoxDetail(boxIndex)
  SelectedBoxDetail = CurrentActivityInfo.all_tasks[GameUIActivityEvent.curTaskId].progress_awards[boxIndex]
  self:GetFlashObject():InvokeASCallback("_root", "InitBoxAwardsList", #SelectedBoxDetail.awards)
end
function GameUIActivityEvent:UpdateBoxAwardItem(index)
  local awardsList = SelectedBoxDetail.awards[index]
  local tmp_type = awardsList.item_type
  local tmp_number = awardsList.number
  local no = awardsList.no
  no = no or 1
  local isItem = awardsList.item_type == "item"
  tmp_type, tmp_number = self:getAwardItemDetailInfo(tmp_type, tmp_number, no)
  local theitem = awardsList
  tmp_type = GameHelper:GetAwardTypeIconFrameName(theitem.item_type, theitem.number)
  local nquality, iconpic
  if theitem.item_type == "medal_item" or theitem.item_type == "medal" then
    nquality = GameHelper:GetMedalQuality(theitem.item_type, theitem.number)
    iconpic = GameHelper:GetGameItemDownloadPng(theitem.item_type, theitem.number)
    isItem = true
  end
  self:GetFlashObject():InvokeASCallback("_root", "SetBoxAwardsListItem", index, tmp_type, tmp_number, isItem, no, iconpic)
end
GameUIActivityEvent.RequestAwardId = 0
GameUIActivityEvent.RequestAwardTaskId = 0
GameUIActivityEvent.RequestAwardIndex = 0
function GameUIActivityEvent:RequestAwards(awardId, index)
  DebugOut(" GameUIActivityEvent:RequestAwards ", awardId, GameUIActivityEvent.curEventId)
  RequestAwardId = awardId
  RequestAwardTaskId = GameUIActivityEvent.curEventId
  RequestAwardIndex = index
  NetMessageMgr:SendMsg(NetAPIList.event_activity_award_req.Code, {
    id = GameUIActivityEvent.curEventId,
    task_id = awardId
  }, GameUIActivityEvent.RequestAwardsCallback, true)
end
function GameUIActivityEvent.RequestAwardsCallback(msgType, content)
  DebugOut("GameUIActivityEvent.RequestAwardsCallback  ", msgType, content.api)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.event_activity_award_req.Code then
    if content.code == 0 then
      DebugOut(" RequestAwardsCallback  ")
      GameUIActivityEvent:ChangeAwardStat()
    end
    return true
  end
  return false
end
function GameUIActivityEvent:ChangeAwardStat()
  local taskDetail = CurrentActivityInfo.all_tasks[GameUIActivityEvent.curTaskId]
  DebugOut("GameUIActivityEvent:ChangeAwardStat( )")
  DebugTable(taskDetail.sub_tasks)
  for k, v in pairs(taskDetail.sub_tasks) do
    if v.id == RequestAwardId then
      v.status = 2
      break
    end
  end
  DebugOut("GameUIActivityEvent:ChangeAwardStat")
  DebugTable(taskDetail.sub_tasks)
  GameUIActivityEvent:GetSubTaskInfo()
  GameUIActivityEvent:SelectedTask(GameUIActivityEvent.curTaskId, true)
  self:GetFlashObject():InvokeASCallback("_root", "setSubTaskDetail", #CurrentActivityInfo.all_tasks)
  GameUIBarLeft:ReduceNewsCnt(GameUIActivityEvent.curEventId, 1)
end
function GameUIActivityEvent:OnItemClicked(sub_tasks_id, index)
  local sub_tasks = CurrentActivityInfo.all_tasks[GameUIActivityEvent.curTaskId].sub_tasks
  local item
  for i = 1, #sub_tasks do
    if sub_tasks[i].id == sub_tasks_id then
      item = sub_tasks[i].awards[index]
      break
    end
  end
  DebugOut("GameUIActivityEvent:OnItemClicked")
  DebugTable(item)
  if item then
    self:showItemDetil(item, item.item_type)
  else
    print("not find", sub_tasks_id, index, LuaUtils:serializeTable(sub_tasks))
  end
end
function GameUIActivityEvent:RefreshMenu(list)
  if #list == 0 then
    return
  end
  if GameUIActivityEvent:GetFlashObject() then
    local EventGroup
    for i = 1, #list do
      if list[i].id == GameUIActivityEvent.curEventId then
        EventGroup = list[i]
        break
      end
    end
    if EventGroup then
      CurrentActivityInfo = EventGroup
      DebugOut("GameUIActivityEvent:RefreshMenu", GameUIActivityEvent.curTaskId)
      self:GetFlashObject():InvokeASCallback("_root", "setSubTaskDetail", #CurrentActivityInfo.all_tasks)
      GameUIActivityEvent:SelectedTask(GameUIActivityEvent.curTaskId, true)
    end
  end
end
function GameUIActivityEvent:getAwardItemDetailInfo(itemType, number, no)
  DebugActivity("getSignItemDetailInfo", itemType, number, no)
  local itemType = itemType
  local rewards = tonumber(number)
  no = no or 1
  if itemType == "fleet" then
    itemType = GameDataAccessHelper:GetFleetAvatar(rewards)
    rewards = GameLoader:GetGameText("LC_NPC_" .. GameDataAccessHelper:GetCommanderName(rewards))
  elseif itemType == "item" or itemType == "krypton" then
    local isitem = itemType == "item"
    if DynamicResDownloader:IsDynamicStuff(rewards, DynamicResDownloader.resType.PIC) then
      local fullFileName = DynamicResDownloader:GetFullName(rewards .. ".png", DynamicResDownloader.resType.PIC)
      local localPath = "data2/" .. fullFileName
      if DynamicResDownloader:IfResExsit(localPath) then
        ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
        itemType = "item_" .. rewards
      else
        itemType = "temp"
        self:AddDownloadPath(rewards, 0, 0)
      end
    else
      itemType = "item_" .. rewards
    end
    if isitem then
      rewards = GameLoader:GetGameText("LC_MENU_ITEM_CHAR")
    else
      rewards = GameLoader:GetGameText("LC_MENU_KRYPTON_TAB")
    end
  elseif itemType == "pve_supply" then
    rewards = GameLoader:GetGameText("LC_MENU_SUPPLY_CHAR")
  else
    rewards = GameUtils.numberConversion(rewards)
  end
  return itemType, rewards
end
function GameUIActivityEvent:AddDownloadPath(itemID, itemKey, index)
  local resName = itemID .. ".png"
  local extInfo = {}
  extInfo.itemKey = itemKey
  extInfo.index = index
  extInfo.item_id = itemID
  extInfo.localPath = "data2/LAZY_LOAD_dynamic_" .. resName
  DynamicResDownloader:AddDynamicRes(resName, DynamicResDownloader.resType.PIC, extInfo, nil)
end
function GameUIActivityEvent:ShowBoxAwardItemDetail(index)
  local award = SelectedBoxDetail.awards[index]
  if award then
    self:showItemDetil(award, award.item_type)
  end
end
function GameUIActivityEvent:showItemDetil(item_, item_type)
  local item = item_
  if item_type == "krypton" then
    if item.level == 0 then
      item.level = 1
    end
    local detail = GameUIKrypton:TryQueryKryptonDetail(item.number, item.level, function(msgType, content)
      local ret = GameUIKrypton.QueryKryptonDetailCallBack(msgType, content)
      if ret then
        local tmp = GameGlobalData:GetKryptonDetail(item.number, item.level)
        if tmp == nil then
          return false
        end
        ItemBox:SetKryptonBox(tmp, item.number)
        local operationTable = {}
        operationTable.btnUnloadVisible = false
        operationTable.btnUpgradeVisible = false
        operationTable.btnUseVisible = false
        operationTable.btnDecomposeVisible = false
        ItemBox:ShowKryptonBox(320, 1080, operationTable)
      end
      return ret
    end)
    if detail == nil then
    else
      ItemBox:SetKryptonBox(detail, item.number)
      local operationTable = {}
      operationTable.btnUnloadVisible = false
      operationTable.btnUpgradeVisible = false
      operationTable.btnUseVisible = false
      operationTable.btnDecomposeVisible = false
      ItemBox:ShowKryptonBox(320, 1080, operationTable)
    end
  end
  if item_type == "fleet" then
    GameUIFirstCharge:ShowCommanderInfo(tonumber(item.number))
  end
  if item_type == "item" then
    item.cnt = 1
    ItemBox:showItemBox("ChoosableItem", item, tonumber(item.number), 320, 1080, 200, 200, "", nil, "", nil)
  end
  if item_type == "medal_item" then
    item.quality = item.quality or 1
    ItemBox:showItemBox("Medal", item, item.number, 320, 480, 200, 200, "", nil, "", nil)
  end
  if item_type == "medal" then
    ItemBox:ShowMedalDetail(item.number)
  end
end
function GameUIActivityEvent:TryGotoMenu(type)
  local playerLevel = GameGlobalData:GetData("levelinfo").level
  if type == "pay" then
    if not GameGlobalData:FTPVersion() then
      GameVip:showVip(false)
    end
  elseif type == "medalGacha" then
    if not GameGlobalData:GetModuleStatus("medal") then
      GameTip:Show(GameLoader:GetGameText("LC_MENU_MEDAL_LEVEL_ALERT"))
      return
    end
    local toactivity = function()
      GameStateManager:SetCurrentGameState(GameStateManager.GameStateStarSystem.m_previousState)
    end
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateStarSystem)
    local GameUIStarSystemPort = LuaObjectManager:GetLuaObject("GameUIStarSystemPort")
    GameUIStarSystemPort:JumpTo("chouka", toactivity)
  elseif type == "medalStore" then
    if not GameGlobalData:GetModuleStatus("medal") then
      GameTip:Show(GameLoader:GetGameText("LC_MENU_MEDAL_LEVEL_ALERT"))
      return
    end
    local toactivity = function()
      GameStateManager:SetCurrentGameState(GameStateManager.GameStateStarSystem.m_previousState)
    end
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateStarSystem)
    local GameUIStarSystemPort = LuaObjectManager:GetLuaObject("GameUIStarSystemPort")
    GameUIStarSystemPort:JumpTo("shop", toactivity)
  elseif type == "medalboss" then
    if not GameGlobalData:GetModuleStatus("medal") then
      GameTip:Show(GameLoader:GetGameText("LC_MENU_MEDAL_LEVEL_ALERT"))
      return
    end
    local toactivity = function()
      GameStateManager:SetCurrentGameState(GameStateManager.GameStateMainPlanet)
    end
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateStarSystem)
    local GameUIStarSystemPort = LuaObjectManager:GetLuaObject("GameUIStarSystemPort")
    GameUIStarSystemPort:JumpTo("boss", toactivity)
  elseif type == "activity" then
    GameStateManager:GetCurrentGameState():AddObject(GameUIActivityNew)
  elseif type == "store" then
    GameStateManager:SetCurrentGameState(GameStateStore)
  elseif type == "mystery_store" then
    GameGoodsList:SetEnterStoreType(2)
    GameStateManager:SetCurrentGameState(GameStateStore)
  elseif type == "sale_store" then
    if GameUtils:IsModuleUnlock("price_off") then
      GameGoodsList:SetEnterStoreType(3)
      GameStateManager:SetCurrentGameState(GameStateStore)
    else
      self:showCntGotoTip()
    end
  elseif type == "central_system" then
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateTacticsCenter)
  elseif type == "crusade" then
    if GameUtils:IsModuleUnlock("crusade") then
      GameStateLab.mCurSubState = STATE_LAB_SUB_STATE.SUBSTATE_CRUSADE
      GameUICrusade.isFromActivityEvent = true
      GameStateManager:SetCurrentGameState(GameStateManager.GameStateLab)
    else
      self:showCntGotoTip()
    end
  elseif type == "arena" then
    if GameUtils:IsModuleUnlock("arena") then
      GameUIArena:SetArenaType(GameUIArena.ARENA_TYPE.normalArena)
      GameStateManager:SetCurrentGameState(GameStateManager.GameStateArena)
    else
      self:showCntGotoTip()
    end
  elseif type == "tlc" then
    if GameUIArena.teamLeaqueCupEnterData == nil then
      NetMessageMgr:SendMsg(NetAPIList.enter_champion_req.Code, nil, function(msgType, content)
        if msgType == NetAPIList.enter_champion_ack.Code then
          GameUIArena.teamLeaqueCupEnterData = content.tlc_champion
          DebugTable(GameUIArena.teamLeaqueCupEnterData)
          if GameUIArena.teamLeaqueCupEnterData.status ~= 1 then
            self:showCntGotoTip()
          elseif GameUIArena.teamLeaqueCupEnterData.status == 1 then
            GameUIArena.mCurrentArenaType = GameUIArena.ARENA_TYPE.tlc
            DebugOut("testtest_tlc_2")
            GameStateManager:SetCurrentGameState(GameStateManager.GameStateArena)
          end
          return true
        end
        return false
      end, true)
    elseif GameUIArena.teamLeaqueCupEnterData.status ~= 1 then
      self:showCntGotoTip()
    elseif GameUIArena.teamLeaqueCupEnterData.status == 1 then
      GameUIArena.mCurrentArenaType = GameUIArena.ARENA_TYPE.tlc
      GameStateManager:SetCurrentGameState(GameStateManager.GameStateArena)
    end
  elseif type == "tc" then
    if GameUtils:IsModuleUnlock("tc") then
      GameObjectMainPlanet:BuildingClicked("territorial_entry")
    else
      self:showCntGotoTip()
    end
  elseif type == "wd" then
    if GameUtils:IsModuleUnlock("wd") then
      GameObjectMainPlanet:BuildingClicked("world_domination")
    else
      self:showCntGotoTip()
    end
  elseif type == "glc" then
    if GameUtils:IsModuleUnlock("arena") then
      local function _callback(msgType, content)
        if msgType == NetAPIList.enter_champion_ack.Code then
          if content.code ~= 0 then
            GameUIGlobalScreen:ShowAlert("error", content.code, nil)
          end
          if content.world_champion.status ~= 1 then
            GameTip:Show(GameLoader:GetGameText("LC_MENU_LEAGUE_TITLE") .. GameLoader:GetGameText("LC_MENU_WD_LIST_WAITING_BUTTON"))
          else
            GameUIArena:SetArenaType(GameUIArena.ARENA_TYPE.wdc)
            GameStateManager:SetCurrentGameState(GameStateArena)
          end
          return true
        end
        return false
      end
      if playerLevel < 50 then
        GameTip:Show(GameLoader:GetGameText("LC_MENU_TECHUP_REQUIRE_PLAYER_LEVEL"))
      else
        NetMessageMgr:SendMsg(NetAPIList.enter_champion_req.Code, nil, _callback, true)
      end
    else
      self:showCntGotoTip()
    end
  elseif type == "collect" then
    local building = GameGlobalData:GetBuildingInfo("planetary_fortress")
    if building.level > 0 then
      GameUIBarMiddle.m_currentBuildingName = "planetary_fortress"
      GameUIBarMiddle:OnFSCommand("on_function_clicked")
    else
      self:showCntGotoTip()
    end
  elseif type == "officer" then
    local hall = GameGlobalData:GetBuildingInfo("affairs_hall")
    if hall.level > 0 then
      GameUIBarMiddle.m_currentBuildingName = "affairs_hall"
      GameUIBarMiddle:OnFSCommand("on_function_clicked")
    else
      self:showCntGotoTip()
    end
  elseif type == "mine" then
    if GameUtils:IsModuleUnlock("mining") then
      GameStateManager:SetCurrentGameState(GameStateManager.GameStateMineMap)
    else
      self:showCntGotoTip()
    end
  elseif type == "month_card" then
    GameUIActivityNew.mGotoMonthCardDirectly = true
    GameStateManager:GetCurrentGameState():AddObject(GameUIActivityNew)
  elseif type == "fleet" then
    if GameUtils:IsModuleUnlock("fleets") then
      GameUIBarRight:OnFSCommand("FS_CLICK_FLEETS")
    else
      self:showCntGotoTip()
    end
  elseif type == "galactonite" then
    if GameUtils:IsModuleUnlock("krypton") then
      GameUIBarMiddle.m_currentBuildingName = "krypton_center"
      GameUIBarMiddle:OnFSCommand("on_function_clicked")
    else
      self:showCntGotoTip()
    end
  elseif type == "pve_battle" then
    GameUIBarRight:OnFSCommand("FS_CLICK_GALAXY")
  elseif type == "battle_train" then
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateArcane)
  elseif type == "warp_energy" then
    local star = GameGlobalData:GetBuildingInfo("star_portal")
    if star.level > 0 then
      GameUIBarMiddle.m_currentBuildingName = "star_portal"
      GameUIBarMiddle:OnFSCommand("on_function_clicked")
    else
      self:showCntGotoTip()
    end
  elseif type == "tech" then
    if GameUtils:IsModuleUnlock("tech") then
      GameUIBarMiddle.m_currentBuildingName = "tech_lab"
      GameUIBarMiddle:OnFSCommand("on_function_clicked")
    else
      self:showCntGotoTip()
    end
  elseif type == "reform" then
    if GameUIRebuildFleet:IsRebuildVisible(true) then
      GameUIFactory.EnterRebuild = true
      GameStateManager:GetCurrentGameState():AddObject(GameUIFactory)
    else
      self:showCntGotoTip()
    end
  elseif type == "infinite_cosmos" then
    if GameUtils:IsModuleUnlock("infinite_cosmos") then
      GameUILab:OnFSCommand("MenuItemReleased", "infinite_cosmos")
    else
      self:showCntGotoTip()
    end
  elseif type == "colonial" then
    if GameUtils:IsModuleUnlock("colonial") then
      GameObjectMainPlanet:OnFSCommand("buildingClicked", "colonial")
    else
      self:showCntGotoTip()
    end
  elseif type == "elite" then
    local building = GameGlobalData:GetBuildingInfo("commander_academy")
    if building.level > 0 and GameUtils:IsModuleUnlock("elite_challenge") then
      GameUIBarMiddle.m_currentBuildingName = "commander_academy"
      GameUIBarMiddle:OnFSCommand("on_function_clicked")
    else
      self:showCntGotoTip()
    end
  elseif type == "slot" then
    if GameUtils:IsModuleUnlock("slot") then
      GameStateManager:SetCurrentGameState(GameStateManager.GameStateGacha)
    else
      self:showCntGotoTip()
    end
  elseif type == "alliance" then
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateAlliance)
  elseif type == "dexter" then
    GameStateLab.mCurSubState = STATE_LAB_SUB_STATE.SUBSTATE_NORMAL
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateLab)
  elseif type == "medal_main" then
    if not GameGlobalData:GetModuleStatus("medal") then
      GameTip:Show(GameLoader:GetGameText("LC_MENU_MEDAL_LEVEL_ALERT"))
      return
    end
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateStarSystem)
    local GameUIStarSystemPort = LuaObjectManager:GetLuaObject("GameUIStarSystemPort")
    GameUIStarSystemPort:Show()
  elseif type == "xingjingzhisen" then
    local item = GameUILab:GetLibsItemByName("starwar")
    if item and 0 < item.count_down then
      GameStateManager:SetCurrentGameState(GameStateManager.GameStateMiningWorld)
      local GameUIMiningWorld = LuaObjectManager:GetLuaObject("GameUIMiningWorld")
      GameUIMiningWorld.enterDir = 1
      GameUIMiningWorld:Show()
    else
      GameStateManager:SetCurrentGameState(GameStateManager.GameStateMainPlanet)
    end
  end
end
function GameUIActivityEvent:showCntGotoTip()
  local infoContent = GameLoader:GetGameText("LC_MENU_EVENT_FUNCTION_UNACTIVIED_ALERT")
  GameTip:Show(infoContent)
end
function GameUIActivityEvent:CheckTaskInfoImg(imgName, asFuncName)
  DebugOut("GameUIActivityEvent:CheckTaskInfoImg 11 ", imgName)
  DebugOut("GameUIActivityEvent:CheckTaskInfoImg 22 ", asFuncName)
  if imgName and "" ~= imgName and "HIDE" ~= imgName and "undefined" ~= imgName and asFuncName and "" ~= asFuncName then
    imgName = imgName .. ".png"
    local localPath = "data2/" .. DynamicResDownloader:GetFullName(imgName, DynamicResDownloader.resType.WELCOME_PIC)
    if DynamicResDownloader:IfResExsit(localPath) then
      DebugOut("extInfo.localPath = " .. localPath)
      self:GetFlashObject():InvokeASCallback("_root", asFuncName, false, imgName)
    else
      local extendInfo = {}
      extendInfo.imgName = imgName
      extendInfo.asFuncName = asFuncName
      DynamicResDownloader:AddDynamicRes(imgName, DynamicResDownloader.resType.WELCOME_PIC, extendInfo, GameUIActivityEvent.DynamicTaskInfoImgCallback)
    end
  else
    self:GetFlashObject():InvokeASCallback("_root", asFuncName, true, imgName)
  end
end
function GameUIActivityEvent.DynamicTaskInfoImgCallback(extendInfo)
  GameUIActivityEvent:GetFlashObject():InvokeASCallback("_root", extendInfo.asFuncName, false, extendInfo.imgName)
end
function GameUIActivityEvent:CheckTaskInfoCategoryBtnBg(index, imgName)
  if imgName and "" ~= imgName and "undefined" ~= imgName then
    imgName = imgName .. ".png"
    local localPath = "data2/" .. DynamicResDownloader:GetFullName(imgName, DynamicResDownloader.resType.WELCOME_PIC)
    if DynamicResDownloader:IfResExsit(localPath) then
      DebugOut("extInfo.localPath = " .. localPath)
      self:GetFlashObject():InvokeASCallback("_root", "setCategoryBtnBg", false, index, imgName)
    else
      local extendInfo = {}
      extendInfo.imgName = imgName
      extendInfo.index = index
      DynamicResDownloader:AddDynamicRes(imgName, DynamicResDownloader.resType.WELCOME_PIC, extendInfo, GameUIActivityEvent.DynamicTaskInfoCategoryBtnBgCallback)
    end
  elseif not imgName or "" ~= imgName then
    self:GetFlashObject():InvokeASCallback("_root", "setCategoryBtnBg", true, index, imgName)
  end
end
function GameUIActivityEvent.DynamicTaskInfoCategoryBtnBgCallback(extendInfo)
  GameUIActivityEvent:GetFlashObject():InvokeASCallback("_root", "setCategoryBtnBg", false, extendInfo.index, extendInfo.imgName)
end
function GameUIActivityEvent:CheckMissionBgImg(index, stat, Bg_1, ButtonBg_1, ButtonBg_2, itemBg, points_bg)
  local buttonBg = stat == 1 and ButtonBg_2 or ButtonBg_1
  local areaBg = false
  local missionbuttonBg = false
  local missionItemBg = false
  local missionPoints_bg = false
  DebugOut("CheckMissionBgImg CCc", Bg_1, buttonBg, itemBg, points_bg)
  if Bg_1 and "" ~= Bg_1 and "undefined.png" ~= Bg_1 and ".png" ~= Bg_1 then
    local localPath = "data2/" .. DynamicResDownloader:GetFullName(Bg_1, DynamicResDownloader.resType.WELCOME_PIC)
    if DynamicResDownloader:IfResExsit(localPath) then
      areaBg = true
    else
      local extendInfo = {}
      extendInfo.imgName_1 = Bg_1
      extendInfo.imgName_2 = buttonBg
      extendInfo.imgName_3 = itemBg
      extendInfo.imgName_4 = points_bg
      extendInfo.index = index
      DynamicResDownloader:AddDynamicRes(Bg_1, DynamicResDownloader.resType.WELCOME_PIC, extendInfo, GameUIActivityEvent.DynamicMissionBgCallback)
    end
  end
  if buttonBg and "" ~= buttonBg and "undefined.png" ~= buttonBg and ".png" ~= buttonBg then
    local localPath = "data2/" .. DynamicResDownloader:GetFullName(buttonBg, DynamicResDownloader.resType.WELCOME_PIC)
    if DynamicResDownloader:IfResExsit(localPath) then
      missionbuttonBg = true
    else
      local extendInfo = {}
      extendInfo.imgName_1 = Bg_1
      extendInfo.imgName_2 = buttonBg
      extendInfo.imgName_3 = itemBg
      extendInfo.imgName_4 = points_bg
      extendInfo.index = index
      DynamicResDownloader:AddDynamicRes(buttonBg, DynamicResDownloader.resType.WELCOME_PIC, extendInfo, GameUIActivityEvent.DynamicMissionBgCallback)
    end
  end
  if itemBg and "" ~= itemBg and "undefined.png" ~= itemBg and ".png" ~= itemBg then
    local localPath = "data2/" .. DynamicResDownloader:GetFullName(itemBg, DynamicResDownloader.resType.WELCOME_PIC)
    if DynamicResDownloader:IfResExsit(localPath) then
      missionItemBg = true
    else
      local extendInfo = {}
      extendInfo.imgName_1 = Bg_1
      extendInfo.imgName_2 = buttonBg
      extendInfo.imgName_3 = itemBg
      extendInfo.imgName_4 = points_bg
      extendInfo.index = index
      DynamicResDownloader:AddDynamicRes(itemBg, DynamicResDownloader.resType.WELCOME_PIC, extendInfo, GameUIActivityEvent.DynamicMissionBgCallback)
    end
  end
  if points_bg and "" ~= points_bg and "undefined.png" ~= points_bg and ".png" ~= points_bg then
    local localPath = "data2/" .. DynamicResDownloader:GetFullName(points_bg, DynamicResDownloader.resType.WELCOME_PIC)
    if DynamicResDownloader:IfResExsit(localPath) then
      missionPoints_bg = true
    else
      local extendInfo = {}
      extendInfo.imgName_1 = Bg_1
      extendInfo.imgName_2 = buttonBg
      extendInfo.imgName_3 = itemBg
      extendInfo.imgName_4 = points_bg
      extendInfo.index = index
      DynamicResDownloader:AddDynamicRes(points_bg, DynamicResDownloader.resType.WELCOME_PIC, extendInfo, GameUIActivityEvent.DynamicMissionBgCallback)
    end
  end
  return areaBg and missionbuttonBg and missionItemBg and missionPoints_bg
end
function GameUIActivityEvent.DynamicMissionBgCallback(extendInfo)
  DebugOutPutTable(extendInfo, "GameUIActivityEvent.DynamicMissionBgCallback")
  local localPath_1 = "data2/" .. DynamicResDownloader:GetFullName(extendInfo.imgName_1, DynamicResDownloader.resType.WELCOME_PIC)
  local localPath_2 = "data2/" .. DynamicResDownloader:GetFullName(extendInfo.imgName_2, DynamicResDownloader.resType.WELCOME_PIC)
  local localPath_3 = "data2/" .. DynamicResDownloader:GetFullName(extendInfo.imgName_3, DynamicResDownloader.resType.WELCOME_PIC)
  local localPath_4 = "data2/" .. DynamicResDownloader:GetFullName(extendInfo.imgName_4, DynamicResDownloader.resType.WELCOME_PIC)
  extendInfo.imgName_1 = DynamicResDownloader:IfResExsit(localPath_1) and extendInfo.imgName_1 or ""
  extendInfo.imgName_2 = DynamicResDownloader:IfResExsit(localPath_2) and extendInfo.imgName_2 or ""
  extendInfo.imgName_3 = DynamicResDownloader:IfResExsit(localPath_3) and extendInfo.imgName_3 or ""
  extendInfo.imgName_4 = DynamicResDownloader:IfResExsit(localPath_4) and extendInfo.imgName_4 or ""
  GameUIActivityEvent:GetFlashObject():InvokeASCallback("_root", "setMissionBg", extendInfo.index, extendInfo.imgName_1, extendInfo.imgName_2, extendInfo.imgName_3, extendInfo.imgName_4)
end
function GameUIActivityEvent.DynamicLeftListBgCallback(extendInfo)
  GameUIActivityEvent:GetFlashObject():InvokeASCallback("_root", "SetTaskListBgShow", extendInfo.imgName)
end
function GameUIActivityEvent:GetAreaColor(AreaName)
  if CurrentActivityInfo.color and #CurrentActivityInfo.color > 0 then
    for k, v in ipairs(CurrentActivityInfo.color) do
      if v.area_name == AreaName then
        return v.color
      end
    end
  end
  return nil
end
if AutoUpdate.isAndroidDevice then
  function GameUIActivityEvent.OnAndroidBack()
    GameUIActivityEvent:OnFSCommand("close_menu")
  end
end
