local GameStateMainPlanet = GameStateManager.GameStateMainPlanet
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
local GameUISection = LuaObjectManager:GetLuaObject("GameUISection")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local QuestTutorialBuildStar = TutorialQuestManager.QuestTutorialBuildStar
local QuestTutorialStarCharge = TutorialQuestManager.QuestTutorialStarCharge
local QuestTutorialRechargePower = TutorialQuestManager.QuestTutorialRechargePower
local QuestTutorialRecruit = TutorialQuestManager.QuestTutorialRecruit
local QuestTutorialsFirstKillBoss = TutorialQuestManager.QuestTutorialsFirstKillBoss
local GameStateBattleMap = GameStateManager.GameStateBattleMap
local FacebookPopUI = LuaObjectManager:GetLuaObject("FacebookPopUI")
GameUISection.callback_close = nil
function GameUISection:OnAddToGameState()
  DebugOut("GameUISection:OnAddToGameState")
  self:LoadFlashObject()
  self:GetFlashObject():InvokeASCallback("_root", "setTitle", self.m_title)
  self._ActionTimesChangeCallback()
end
function GameUISection:OnEraseFromGameState()
  self:UnloadFlashObject()
  collectgarbage("collect")
end
function GameUISection:OnParentStateGetFocus()
  self:GetFlashObject():InvokeASCallback("_root", "setCloseVisible", GameGlobalData:BattleCanQuit())
  if (QuestTutorialBuildStar:IsActive() or QuestTutorialStarCharge:IsActive() or QuestTutorialRecruit:IsActive()) and not QuestTutorialsFirstKillBoss:IsActive() then
    self:GetFlashObject():InvokeASCallback("_root", "ShowTutorialClose")
  else
    self:GetFlashObject():InvokeASCallback("_root", "HideTutorialClose")
  end
end
function GameUISection:OnFSCommand(cmd, arg)
  if cmd == "close" then
    self:GetFlashObject():InvokeASCallback("_root", "HideTutorialClose")
    local GameObjectAdventure = LuaObjectManager:GetLuaObject("GameObjectAdventure")
    if GameObjectAdventure.tutorialClose then
      AddFlurryEvent("CloseAcademyUI", {}, 1)
      GameObjectAdventure.tutorialClose = nil
    end
    GameObjectAdventure.isNeedGotoEnd = true
    GameObjectAdventure.currentScreenShot = 1
    GameObjectAdventure.m_currentSection = 1
    if GameUISection.callback_close then
      GameUISection.callback_close()
      GameUISection.callback_close = nil
    else
      GameStateManager:SetCurrentGameState(GameStateMainPlanet)
      GameStateBattleMap.m_currentAreaID = nil
    end
    return
  end
  if cmd == "recover_supply_times" then
    self:AddBattleSupply()
    return
  end
  assert(false)
end
function GameUISection.AddBattleSupplyRequtst()
  GameUISection:AddBattleSupply()
end
function GameUISection:AddBattleSupply()
  DebugOut("___\230\137\147\229\141\176 AddBattleSupply", QuestTutorialRechargePower:IsActive())
  if immanentversion == 2 and QuestTutorialRechargePower:IsActive() then
    DebugOut("_1")
    QuestTutorialRechargePower:SetFinish(true)
    DebugOut("_2")
    if not GameUIBarLeft:GetFlashObject() then
      DebugOut("_4")
      GameUIBarLeft:LoadFlashObject()
    end
    DebugOut("_3")
    GameUIBarLeft:GetFlashObject():InvokeASCallback("_root", "HidePowerTip")
  end
  local left_times, total_times = 0, 0
  local battle_supply = GameGlobalData:GetData("battle_supply")
  if battle_supply then
    left_times = battle_supply.current
    total_times = battle_supply.max
    self:RequestPrice()
  end
end
function GameUISection:SetTitle(title)
  DebugOut("GameUISection:SetTitle", title)
  self.m_title = title
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "setTitle", self.m_title)
  end
end
function GameUISection._ActionTimesChangeCallback()
  local flash_obj = GameUISection:GetFlashObject()
  if not flash_obj then
    return
  end
  local left_times, total_times = 0, 0
  local battle_supply = GameGlobalData:GetData("battle_supply")
  if not battle_supply then
    return
  end
  left_times = battle_supply.current
  total_times = battle_supply.max
  flash_obj:InvokeASCallback("_root", "lua2fs_updateSupplyInfo", left_times, total_times)
end
function GameUISection:OnInitGame()
  GameGlobalData:RegisterDataChangeCallback("battle_supply", self._ActionTimesChangeCallback)
end
function GameUISection:RequestPrice()
  local packet = {
    type = "battle_supply"
  }
  GameUISection.needPopBuy = true
  NetMessageMgr:SendMsg(NetAPIList.supply_info_req.Code, packet, GameUISection.serverCallback, true, nil)
end
function GameUISection.BuyBattleSupply(price)
  local resource = GameGlobalData:GetData("resource")
  DebugOut("will buy, price is ", price, resource.credit)
  if price and resource and resource.credit and price > resource.credit then
    local GameVip = LuaObjectManager:GetLuaObject("GameVip")
    GameVip:CheckIsNeedShowVip(8904)
  else
    GameUISection.needPopBuy = false
    local packet = {
      type = "battle_supply"
    }
    NetMessageMgr:SendMsg(NetAPIList.supply_exchange_req.Code, packet, GameUISection.serverCallback, true, nil)
  end
end
function GameUISection.serverCallback(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and (content.api == NetAPIList.supply_info_req.Code or content.api == NetAPIList.supply_exchange_req.Code) then
    if content.code ~= 0 then
      local GameVip = LuaObjectManager:GetLuaObject("GameVip")
      GameVip:CheckIsNeedShowVip(content.code)
    end
    return true
  end
  if msgtype == NetAPIList.supply_info_ack.Code then
    local price = content.exchange_cost
    local count = content.count
    DebugOut("price is", price)
    if GameUISection.needPopBuy then
      local GameUISupplyDialog = LuaObjectManager:GetLuaObject("GameUISupplyDialog")
      GameUISupplyDialog:Show()
    end
    return true
  end
  return false
end
if AutoUpdate.isAndroidDevice then
  function GameUISection.OnAndroidBack()
    GameUISection:OnFSCommand("close")
  end
end
