local GameFleetEquipment = LuaObjectManager:GetLuaObject("GameFleetEquipment")
GameFleetEquipment.kejin = {}
local GameStateEquipEnhance = GameStateManager.GameStateEquipEnhance
local kejin = GameFleetEquipment.kejin
require("FleetMatrix.tfl")
local currentShowBluePrint = 0
kejin.m_currentSelectFleetIndex = 1
local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
function kejin:GetFlashObject()
  return (...), GameFleetEquipment
end
function kejin:Hide()
  GameGlobalData:RemoveDataChangeCallback("fleetinfo", GameFleetEquipment.OnEnhanceFleetinfoChange)
  self:GetFlashObject():InvokeASCallback("_root", "Hide_kejin")
end
function kejin:OnEraseFromGameState()
  GameGlobalData:RemoveDataChangeCallback("fleetinfo", GameFleetEquipment.OnEnhanceFleetinfoChange)
end
local GameStateKrypton = GameStateManager.GameStateKrypton
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameFleetInfoBackground = LuaObjectManager:GetLuaObject("GameFleetInfoBackground")
local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
local GameUICollect = LuaObjectManager:GetLuaObject("GameUICollect")
local QuestTutorialUseKrypton = TutorialQuestManager.QuestTutorialUseKrypton
local QuestTutorialBuildKrypton = TutorialQuestManager.QuestTutorialBuildKrypton
local QuestTutorialEquipKrypton = TutorialQuestManager.QuestTutorialEquipKrypton
local QuestTutorialEnhanceKrypton = TutorialQuestManager.QuestTutorialEnhanceKrypton
local GameUICommonDialog = LuaObjectManager:GetLuaObject("GameUICommonDialog")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local GameWaiting = LuaObjectManager:GetLuaObject("GameWaiting")
local GameUIWDStuff = LuaObjectManager:GetLuaObject("GameUIWDStuff")
local GameUICommonDialog = LuaObjectManager:GetLuaObject("GameUICommonDialog")
local GameVipDetailInfoPanel = LuaObjectManager:GetLuaObject("GameVipDetailInfoPanel")
local GameUIMaskLayer = LuaObjectManager:GetLuaObject("GameUIMaskLayer")
local GameUIMaster = LuaObjectManager:GetLuaObject("GameUIMaster")
local GameUIBattleResult = LuaObjectManager:GetLuaObject("GameUIBattleResult")
kejin.KryptonAutoDecomposeLevel = 1
kejin.KryptonAutoDecomposeLevelMax = 5
kejin.KryptonAutoDecomposeLevelMin = 1
kejin.force = {}
function kejin:OnInitGame()
end
function kejin.RefreshKrypton(arg)
  DebugOut("wwww")
  if kejin:GetFlashObject() then
    DebugOut("Refresh update server krypton")
    kejin:GetFleetKrypton(arg)
    kejin:RequestKryptonInBag()
    GameFleetEquipment.RefreshKryptonRedPoint()
  end
end
function kejin:CheckInitGameData()
  if not self.kryptonSlotLevel then
    self.kryptonSlotLevel = GameDataAccessHelper:GetKryptonUnLockSlot()
  end
end
function kejin.RefreshResource()
  DebugOut("kejin.RefreshResource")
  if not kejin.requestComposeing and GameStateManager:GetCurrentGameState():IsObjectInState(GameFleetEquipment) then
    DebugOut("kejin.RefreshResource___")
    local resource = GameGlobalData:GetData("resource")
    kejin:GetFlashObject():InvokeASCallback("_root", "RefreshResource", GameUtils.numberConversion(resource.money), GameUtils.numberConversion(resource.credit), GameUtils.numberConversion(resource.kenergy))
  end
end
function kejin.RefreshFleets()
  if GameStateManager:GetCurrentGameState():IsObjectInState(GameFleetEquipment) then
    local fleets = GameGlobalData:GetData("fleetinfo").fleets
    kejin.m_currentSelectFleetIndex = kejin.m_currentSelectFleetIndex or 1
    kejin:UpdateSelectedFleetInfo(kejin.m_currentSelectFleetIndex)
  end
end
function kejin:SetInitFleet(index)
  self.initFleetIndex = index
end
function kejin:Init()
  self.autoDecompose = false
  self.batchGacha = false
  self.requestComposeing = false
  self.RefreshResource()
  self.initDecomposeBar = false
  kejin.mCurSortType = 1
  local curLevel = GameVipDetailInfoPanel:GetVipLevel()
  local curPlayerLevel = GameGlobalData:GetData("levelinfo").level
  local isVip = curLevel > 4
  local force = GameLoader:GetGameText("LC_MENU_FRIEND_BATTLE")
  local AutoComposeVisible = self:CheckIsShowAutoCompose()
  self:GetFlashObject():InvokeASCallback("_root", "setAutoComposeVisible", AutoComposeVisible)
  self:UpdateAutoCompose()
  local AutoDecomposeVisible = curLevel >= GameDataAccessHelper:GetVIPLimit("krypton_auto_decompose")
  self:GetFlashObject():InvokeASCallback("_root", "setAutoDecomposeVisible", AutoDecomposeVisible)
  self:UpdateAutoDecompose()
  self:setKryptonGameText()
  local energyLevelUp = GameLoader:GetGameText("LC_MENU_KRYPTON_ENERGY_LEVEL_UP")
  local freeNext = GameLoader:GetGameText("LC_MENU_FREE_KRYPTON_CHAR")
  self:GetFlashObject():InvokeASCallback("_root", "setEnergLevelUp", energyLevelUp)
  self:GetFlashObject():InvokeASCallback("_root", "setButtonEnableWhenAction", true)
  local info = GameLoader:GetGameText("LC_HELP_KRYPTON_INFO_CHAR")
  DebugOut("krypton help", info)
  self:GetFlashObject():InvokeASCallback("_root", "setKryptonHelp", info)
  self:UpdateAutoComposeLvBar()
end
function kejin:CheckIsShowAutoCompose(...)
  local curLevel = GameVipDetailInfoPanel:GetVipLevel()
  local curPlayerLevel = GameGlobalData:GetData("levelinfo").level
  local limitLevel = GameDataAccessHelper:GetLevelLimit("krypton_auto_compose")
  local limitVip = GameDataAccessHelper:GetVIPLimit("krypton_auto_compose")
  if curLevel >= limitVip or curPlayerLevel >= limitLevel then
    return true
  else
    return false
  end
end
function kejin:UpdateAutoCompose()
  self:GetFlashObject():InvokeASCallback("_root", "setAutoComposeEnabled", self.batchGacha)
end
function kejin:UpdateAutoDecompose()
  self:GetFlashObject():InvokeASCallback("_root", "setAutoDecomposeEnabled", self.autoDecompose)
end
function kejin:Clear()
  self.initFleetIndex = 1
  self.equipmentShown = false
end
function kejin:ClearHook()
  self.initFleetIndex = 1
  self.equipmentShown = false
end
function kejin:setKryptonGameText()
  local kryptonPointStr = GameLoader:GetGameText("LC_MENU_KRYPTON_POINT")
  local kryptonLbLockedStr = GameLoader:GetGameText("LC_MENU_LOCKED_CHAR")
  self:GetFlashObject():InvokeASCallback("_root", "setKryptonGameText", kryptonPointStr, kryptonLbLockedStr)
end
function kejin:TryQueryKryptonDetail(kryptonID, kryptonLevel, callback)
  DebugOut("query krypton detail req ", kryptonID)
  local detail = GameGlobalData:GetKryptonDetail(kryptonID, kryptonLevel)
  if detail == nil then
    local req_content = {krypton_id = kryptonID, level = kryptonLevel}
    if callback == nil then
      callback = kejin.QueryKryptonDetailCallBack
    end
    kejin:SendMsgWithMatrix(NetAPIList.krypton_info_req.Code, req_content, callback, true, nil)
  else
    DebugOut("got local krypton detail")
  end
  return detail
end
function kejin.QueryKryptonDetailCallBack(msgType, content)
  DebugOut("krypton info call back")
  DebugOut(msgType)
  DebugTable(conteng)
  if msgType == NetAPIList.krypton_info_ack.Code then
    GameWaiting:HideLoadingScreen()
    GameGlobalData:SetKryptonDetail(tonumber(content.krypton.id), tonumber(content.krypton.level), content.krypton)
    return true
  end
  return false
end
function kejin.OnGlobalLevelChange()
  local levelInfo = GameGlobalData:GetData("levelinfo")
  if levelInfo and tonumber(levelInfo.level) > 29 and not QuestTutorialBuildKrypton:IsActive() and not QuestTutorialBuildKrypton:IsFinished() then
    local krypton = GameGlobalData:GetBuildingInfo("krypton_center")
    if krypton.level > 0 then
      QuestTutorialBuildKrypton:SetFinish(true)
      local fleetkryptons = GameGlobalData:GetData("fleetkryptons")
      local ishavekrypton = false
      for k, v in pairs(fleetkryptons) do
        if v.kryptons and 0 < #v.kryptons then
          ishavekrypton = true
        end
      end
      if ishavekrypton then
        QuestTutorialUseKrypton:SetFinish(true)
      end
    else
      QuestTutorialBuildKrypton:SetActive(true)
    end
  end
end
function kejin:OnAddToGameState()
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  self:Init()
  kejin.m_currentSelectFleetIndex = 1
  self:InitCurrentFleet()
  self:GetFlashObject():InvokeASCallback("_root", "HideTutorialKryptonTab")
  self:GetFlashObject():InvokeASCallback("_root", "HideTutorialLabTab")
  if QuestTutorialUseKrypton:IsActive() then
    if GameStateKrypton.currentTab == GameStateKrypton.KRYPTON then
      local function callback()
        self:GetFlashObject():InvokeASCallback("_root", "ShowTutorialLabTab")
      end
      self:GetFlashObject():InvokeASCallback("_root", "HideTutorialKryptonTab")
      self:GetFlashObject():InvokeASCallback("_root", "HideTutorialBtnAction")
      if not GameUtils:GetTutorialHelp() then
        GameUICommonDialog:PlayStory({100033}, callback)
      end
    elseif GameStateKrypton.currentTab == GameStateKrypton.LAB then
      local function callback()
        self:GetFlashObject():InvokeASCallback("_root", "ShowTutorialBtnAction")
      end
      self:GetFlashObject():InvokeASCallback("_root", "HideTutorialLabTab")
      if not GameUtils:GetTutorialHelp() then
        GameUICommonDialog:PlayStory({100035}, callback)
      end
    elseif GameStateKrypton.currentTab == GameStateKrypton.ADV_LAB then
      local function callback()
        self:GetFlashObject():InvokeASCallback("_root", "ShowTutorialAdvBtnAction")
      end
      self:GetFlashObject():InvokeASCallback("_root", "HideTutorialAdvLabTab")
    end
  elseif QuestTutorialEquipKrypton:IsActive() then
    local function callback()
      self:GetFlashObject():InvokeASCallback("_root", "ShowTutorialEquipKrypton")
    end
    GameUICommonDialog:PlayStory({100037}, callback)
  end
  kejin:CheckKryptonRefineEnterBtnVisible()
  if GameUtils:GetTutorialHelp() and GameUtils:GetTutorialName() == "composeKrypton" then
    kejin:GetHelpTutorialPos()
  end
end
function kejin:RequestKryptonInBag()
  DebugOut("pppuhy RequestKryptonInBag")
  if not GameFleetEquipment.isTeamLeague then
    local function netFailedCallback()
      kejin:SendMsgWithMatrix(NetAPIList.krypton_store_req.Code, nil, kejin.DownloadKryptonBag, true, netFailedCallback)
    end
    kejin:SendMsgWithMatrix(NetAPIList.krypton_store_req.Code, nil, kejin.DownloadKryptonBag, true, netFailedCallback)
  else
    local function netFailedCallback()
      kejin:SendMsgWithMatrix(NetAPIList.tlc_krypton_store_req.Code, nil, kejin.DownloadKryptonBag, true, netFailedCallback)
    end
    kejin:SendMsgWithMatrix(NetAPIList.tlc_krypton_store_req.Code, nil, kejin.tlc_DownloadKryptonBag, true, netFailedCallback)
  end
end
function kejin:UpdateSelectedFleetInfo(fleetIndex)
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  DebugOut("!!!fleetIndex = ", fleetIndex)
  DebugOut("fleet Table")
  DebugTable(fleets)
  if fleets ~= nil then
    local maxFleetIndex = LuaUtils:table_size(fleets)
    if fleetIndex > 0 and fleetIndex <= maxFleetIndex then
      if self:GetFlashObject() ~= nil then
        self:GetFlashObject():InvokeASCallback("_root", "setPage", fleetIndex, maxFleetIndex)
      end
      local content = fleets[fleetIndex]
      GameFleetInfoBackground.currentIndex = fleetIndex
      GameFleetInfoBackground:RefreshCurrentFleet()
      GameFleetInfoBackground:FleetAppear()
      self.m_currentSelectFleetIndex = fleetIndex
      kejin:ShowEquipMent()
      kejin:GetFleetKrypton()
    end
  end
end
function kejin:GetKryptonType(item)
  if item.item_type and item.rank then
    local _type = 50000 + item.item_type * 100 + item.rank
    return _type
  end
  return -1
end
function kejin:Update(dt)
  local flashObj = self:GetFlashObject()
  flashObj:InvokeASCallback("_root", "UpdateGrid", dt)
  if GameFleetEquipment.dragBagKrypton then
    self:UpdataFleetSlotState(self.onSelectBagSlot, false)
  end
end
function kejin:RefreshBagKrypton()
  local cnt = math.ceil(kejin.grid_capacity / 4)
  if cnt > 0 then
    for k = 1, cnt do
      kejin:UpdateBagListItem(k)
    end
  end
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "setKryptonCount", self:GetKryptonCount(), kejin.grid_capacity, string.gsub(GameLoader:GetGameText("LC_MENU_SAVE_FORMATION_INFO_8"), "<number1>", tostring(kejin.grid_capacity)))
  end
end
function kejin:GetKryptonCount()
  local count = 0
  for k, v in pairs(kejin.kryptonBag) do
    if v then
      count = count + 1
    end
  end
  return count
end
function kejin:GetBagSlot()
  local slot = 0
  for i = 1, kejin.grid_capacity do
    local item = self.kryptonBag[i]
    if not item then
      slot = i
      return slot
    end
  end
  return slot
end
function kejin:ShowEquipMent()
  if not self.equipmentShown then
    self.equipmentShown = true
    self:GetFlashObject():InvokeASCallback("_root", "showEquipment")
  end
end
function kejin:HideEquipMent()
  if self.equipmentShown then
    self.equipmentShown = false
    self:GetFlashObject():InvokeASCallback("_root", "hideEquipment")
  end
end
function kejin:GetFleetKrypton(arg)
  DebugOut("GetFleetKrypton__")
  if arg then
    self.m_currentSelectFleetIndex = tonumber(arg)
  end
  DebugOut("GetFleetKrypton = " .. self.m_currentSelectFleetIndex)
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  DebugOutPutTable(fleets, "fleets = ")
  local fleet = fleets[self.m_currentSelectFleetIndex]
  if not fleet then
    return
  end
  local fleetkryptons = GameGlobalData:GetData("fleetkryptons")
  DebugOutPutTable(fleetkryptons, "fleetkryptons ")
  local fleetkrypton = LuaUtils:table_values(LuaUtils:table_filter(fleetkryptons, function(k, v)
    return v.fleet_identity == fleet.identity
  end))[1]
  if fleetkrypton then
    kejin.currentFleetKrypton = fleetkrypton.kryptons
  else
    kejin.currentFleetKrypton = {}
    DebugOut("select fleet's kryptons is nil", self.m_currentSelectFleetIndex)
  end
  DebugOutPutTable(kejin.currentFleetKrypton, "GetFleetKrypton")
  kejin.currentFleetKrypton = kejin.currentFleetKrypton or {}
  kejin.currentFleetKrypton = kejin:ProcessMatrixInfo(kejin.currentFleetKrypton)
  kejin:RefreshCurFleetKrypton()
  kejin:RefreshChargeKrypton()
end
function kejin:InitCurrentFleet()
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  local fleet = fleets[self.m_currentSelectFleetIndex]
  local fleet_name = GameDataAccessHelper:GetFleetLevelDisplayName(fleet.identity, fleet.level)
  DebugOut("!!!!!!!!!!!!!fleet_name = ", fleet_name)
  local iconFrame = GameDataAccessHelper:GetFleetAvatar(fleet.identity, fleet.level)
  local vesselsType = GameDataAccessHelper:GetCommanderVesselsType(fleet.identity, fleet.level)
  local fleetColor = GameDataAccessHelper:GetCommanderColorFrame(fleet.identity, fleet.level)
  if fleet.identity == 1 then
    local tempid = GameFleetEquipment:GetLeaderFleet()
    iconFrame = GameDataAccessHelper:GetFleetAvatar(tempid, fleet.level)
    vesselsType = GameDataAccessHelper:GetCommanderVesselsType(tempid, fleet.level)
    fleetColor = GameDataAccessHelper:GetCommanderColorFrame(tempid, fleet.level)
  end
  self:GetFlashObject():InvokeASCallback("_root", "setFleetKrypton", GameUtils.numberConversion(fleet.krypton), GameUtils.numberConversion(fleet.force), fleet_name, iconFrame, tonumber(fleet.identity), vesselsType, fleetColor)
end
function kejin:RefreshCurFleetKrypton()
  DebugOut("RefreshCurFleetKrypton__")
  self:CheckInitGameData()
  local frame, lockLevel, kryptonLV, canEvalote = "", "", "", ""
  if not self.currentFleetKrypton then
    return
  end
  local building = GameGlobalData:GetBuildingInfo("krypton_center")
  local totalKrypton = 0
  DebugOutPutTable(self.currentFleetKrypton, "self.currentFleetKrypton")
  local resource = GameGlobalData:GetData("resource")
  for i = 1, 8 do
    local item = self.currentFleetKrypton[i]
    if item then
      local id = kejin:GetKryptonType(item)
      if DynamicResDownloader:IsDynamicStuff(id, DynamicResDownloader.resType.PIC) then
        local fullFileName = DynamicResDownloader:GetFullName(id .. ".png", DynamicResDownloader.resType.PIC)
        local localPath = "data2/" .. fullFileName
        if DynamicResDownloader:IfResExsit(localPath) then
          ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
          frame = frame .. "item_" .. id .. "\001"
        else
          frame = frame .. "temp" .. "\001"
          DynamicResDownloader:AddDynamicRes(id .. ".png", DynamicResDownloader.resType.PIC, nil, nil)
        end
      else
        frame = frame .. "item_" .. id .. "\001"
      end
      totalKrypton = item.decompose_kenergy
      kryptonLV = kryptonLV .. GameLoader:GetGameText("LC_MENU_Level") .. item.level .. "\001"
      if resource and resource.kenergy >= item.kenergy_upgrade and not item.max_level then
        canEvalote = canEvalote .. "true" .. "\001"
      else
        canEvalote = canEvalote .. "false" .. "\001"
      end
    else
      frame = frame .. "empty" .. "\001"
      kryptonLV = kryptonLV .. "no" .. "\001"
      canEvalote = canEvalote .. "false" .. "\001"
    end
  end
  for k, v in pairs(self.kryptonSlotLevel) do
    if v.labLevel <= building.level then
      lockLevel = lockLevel .. "no" .. "\001"
    else
      lockLevel = lockLevel .. v.labLevel .. "\001"
    end
  end
  DebugOut("frame, lockLevel,totalKrypton,kryptonLV", frame, lockLevel, totalKrypton, kryptonLV)
  self:GetFlashObject():InvokeASCallback("_root", "RefreshEquipment", frame, lockLevel, totalKrypton, kryptonLV)
  self:GetFlashObject():InvokeASCallback("_root", "SetKryptonEquipArrows", canEvalote)
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  local fleet = fleets[self.m_currentSelectFleetIndex]
  local fleet_name = GameDataAccessHelper:GetFleetLevelDisplayName(fleet.identity, fleet.level)
  local iconFrame = GameDataAccessHelper:GetFleetAvatar(fleet.identity, fleet.level)
  local vesselsType = GameDataAccessHelper:GetCommanderVesselsType(fleet.identity, fleet.level)
  local fleetColor = GameDataAccessHelper:GetCommanderColorFrame(fleet.identity, fleet.level)
  if fleet.identity == 1 then
    local tempid = GameFleetEquipment:GetLeaderFleet()
    iconFrame = GameDataAccessHelper:GetFleetAvatar(tempid, fleet.level)
    vesselsType = GameDataAccessHelper:GetCommanderVesselsType(tempid, fleet.level)
    fleetColor = GameDataAccessHelper:GetCommanderColorFrame(tempid, fleet.level)
  end
  self:GetFlashObject():InvokeASCallback("_root", "setFleetKrypton", GameUtils.numberConversion(fleet.krypton), GameUtils.numberConversion(fleet.force), fleet_name, iconFrame, tonumber(fleet.identity), vesselsType, fleetColor)
end
function kejin.UpdateForce()
  if kejin:GetFlashObject() then
    local fleets = GameGlobalData:GetData("fleetinfo").fleets
    local fleet = fleets[kejin.m_currentSelectFleetIndex]
    DebugOut("fleet.force:" .. fleet.force .. "," .. kejin.m_currentSelectFleetIndex)
    DebugTable(kejin.force)
    local temp = "tempfleetforce" .. fleet.identity
    if not kejin.force[temp] then
      kejin.force[temp] = fleet.force
    end
    local forceChangeNum = tonumber(fleet.force - kejin.force[temp])
    kejin.force[temp] = fleet.force
    local forceChangeNumtemp = GameUtils.numberConversion(math.abs(forceChangeNum))
    kejin:GetFlashObject():InvokeASCallback("_root", "showForceChangeAnimation", forceChangeNum, forceChangeNumtemp)
    kejin.RefreshKrypton()
  end
end
function kejin:SetCurrentChargeKrypton(kryptonId)
  self.currentChargeItemId = kryptonId
  self:RefreshChargeKrypton()
end
function kejin:EquipOffKrypton(kryptonId, destSlot)
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  local identity = fleets[self.m_currentSelectFleetIndex].identity
  for k, v in pairs(self.currentFleetKrypton) do
    if v.id == kryptonId then
      for tk, tv in pairs(v.formation) do
        local system_index = GameFleetEquipment:GetCurMatrixId()
        if tv.formation_id == system_index then
          self.equipOffSrcSlot = tv.slot
        end
      end
    end
  end
  self.equipOffDestSlot = destSlot
  DebugOut("EquipOffKrypton: ", identity, kryptonId, self.equipOffDestSlot, self.equipOffSrcSlot)
  local krypton_equip_off_req_param = {
    fleet_identity = identity,
    krypton_id = kryptonId,
    slot = self.equipOffDestSlot
  }
  local function netFailedCallback()
    kejin:onEndDragItem()
    kejin:RequestKryptonInBag()
  end
  kejin:SendMsgWithMatrix(NetAPIList.krypton_equip_off_req.Code, krypton_equip_off_req_param, kejin.kryptonEquipCallback, true, netFailedCallback)
end
function kejin.UseKrypton()
  kejin:CheckInitGameData()
  local destSlot = 0
  local building = GameGlobalData:GetBuildingInfo("krypton_center")
  local item = kejin.kryptonBag[kejin.onSelectBagSlot]
  local state = kejin:GetCurrentKryptonInFleetKryptonState(item)
  if state == 0 then
    for i = 1, 8 do
      if kejin.kryptonSlotLevel[i].labLevel <= building.level and not kejin.currentFleetKrypton[i] then
        destSlot = i
        break
      end
    end
  end
  if destSlot == 0 then
    DebugOut("state == " .. state)
    for i = 8, 1, -1 do
      if state == 1 then
        if kejin.currentFleetKrypton[i] and kejin:GetTwoKrytponAttribute(item, kejin.currentFleetKrypton[i]) then
          destSlot = i
          DebugOut("destSlot = " .. destSlot)
          break
        end
      elseif kejin.kryptonSlotLevel[i].labLevel <= building.level then
        destSlot = i
      end
    end
  end
  if destSlot > 0 then
    kejin:EquipKrypton(kejin.decomposeId, destSlot)
  end
end
function kejin:EquipKrypton(kryptonId, destSlot)
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  local identity = fleets[self.m_currentSelectFleetIndex].identity
  for k, v in pairs(self.kryptonBag) do
    if v.id == kryptonId then
      self.equipSrcSlot = v.slot
    end
  end
  self.equipDestSlot = destSlot
  DebugOut("EquipKrypton: ", self.equipSrcSlot, " ", self.equipDestSlot)
  local krypton_equip_req_param = {
    fleet_identity = identity,
    krypton_id = kryptonId,
    slot = self.equipDestSlot
  }
  local function netFailedCallback()
    kejin:onEndDragItem()
    kejin:RequestKryptonInBag()
  end
  DebugTable(krypton_equip_req_param)
  kejin:SendMsgWithMatrix(NetAPIList.krypton_equip_req.Code, krypton_equip_req_param, kejin.kryptonEquipCallback, true, netFailedCallback)
end
function kejin:SendMsgWithMatrix(msgType, param, callback, block, failCallback)
  DebugOut("SendMsgWithMatrix:" .. msgType)
  param = param or {}
  local isIndex = false
  for k, v in pairs(param) do
    if k == "formation_id" then
      isIndex = true
      break
    end
  end
  if not isIndex then
    local system_index = GameFleetEquipment:GetCurMatrixId()
    param.formation_id = system_index or 1
  end
  NetMessageMgr:SendMsg(msgType, param, callback, block, failCallback)
  if msgType == NetAPIList.krypton_gacha_one_req.Code then
    GameUtils:RecordForTongdui(msgType, param, callback, block, failCallback)
    local function callback()
      if kejin:GetFlashObject() then
        kejin:GetFlashObject():InvokeASCallback("_root", "setButtonEnableWhenAction", true)
      end
    end
    GameUtils:RecordForTongdui_onuser_cancel(msgType, callback)
    self.requestComposeing = false
  end
end
function kejin:SwapFleetKryptonPos(kryptonId, destSlot)
  self.swapSrcSlot = -1
  local system_index = GameFleetEquipment:GetCurMatrixId()
  for k, v in pairs(self.currentFleetKrypton) do
    if v.id == kryptonId then
      for vk, vv in pairs(v.formation) do
        if vv.formation_id == system_index then
          self.swapSrcSlot = vv.slot
        end
      end
    end
  end
  self.swapDestSlot = destSlot
  local destItem = self.currentFleetKrypton[destSlot]
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  local identity = fleets[self.m_currentSelectFleetIndex].identity
  local krypton_fleet_move_req_param = {
    fleet_identity = identity,
    krypton_id = kryptonId,
    dest_slot = self.swapDestSlot
  }
  DebugOut("krypton_fleet_move_req_param", identity, kryptonId, self.swapDestSlot)
  local function netFailedCallback()
    kejin:onEndDragItem()
    kejin:RequestKryptonInBag()
  end
  kejin:SendMsgWithMatrix(NetAPIList.krypton_fleet_move_req.Code, krypton_fleet_move_req_param, kejin.swapFleetCallback, true, netFailedCallback)
end
function kejin:SwapBagKryptonPos(kryptonId, destSlot)
  self.swapSrcSlot = -1
  for k, v in pairs(self.kryptonBag) do
    if v.id == kryptonId then
      self.swapSrcSlot = v.slot
    end
  end
  self.swapDestSlot = destSlot
  if self.swapSrcSlot == self.swapDestSlot then
    if GameFleetEquipment.dragItemInstance then
      kejin:onEndDragItem()
    end
    return
  end
  local destItem = self.kryptonBag[destSlot]
  DebugOut("destItem: ", destItem)
  local krypton_store_swap_req_param = {
    id_from = kryptonId,
    id_to = destItem and destItem.id or "",
    slot_to = destSlot
  }
  DebugOut("SwapBagKryptonSlot: ", self.swapSrcSlot, self.swapDestSlot)
  DebugOut("SwapBagKryptonPos: ", krypton_store_swap_req_param.id_from, krypton_store_swap_req_param.id_to, krypton_store_swap_req_param.slot_to)
  local function netFailedCallback()
    kejin:onEndDragItem()
    kejin:RequestKryptonInBag()
  end
  kejin:SendMsgWithMatrix(NetAPIList.krypton_store_swap_req.Code, krypton_store_swap_req_param, kejin.swapBagCallback, true, netFailedCallback)
end
function kejin:RefreshChargeKrypton()
  if self.currentChargeItemId then
    local item
    for k, v in pairs(self.kryptonBag) do
      if v.id == self.currentChargeItemId then
        item = v
      end
    end
    if item == nil then
      for k, v in pairs(self.currentFleetKrypton) do
        if v.id == self.currentChargeItemId then
          item = v
        end
      end
    end
    if item then
      if not item.kenergy_upgrade then
        item.kenergy_upgrade = 0
      end
      local frame = ""
      local id = kejin:GetKryptonType(item)
      if DynamicResDownloader:IsDynamicStuff(id, DynamicResDownloader.resType.PIC) then
        local fullFileName = DynamicResDownloader:GetFullName(id .. ".png", DynamicResDownloader.resType.PIC)
        local localPath = "data2/" .. fullFileName
        if DynamicResDownloader:IfResExsit(localPath) then
          ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
          frame = "item_" .. id
        else
          frame = "temp"
          DynamicResDownloader:AddDynamicRes(id .. ".png", DynamicResDownloader.resType.PIC, nil, nil)
        end
      else
        frame = "item_" .. id
      end
      self:GetFlashObject():InvokeASCallback("_root", "setChargeItem", frame, item.kenergy_upgrade)
    end
  else
    self:GetFlashObject():InvokeASCallback("_root", "setChargeItem", "empty", 0)
  end
end
function kejin:BagItemReleased(arg)
  local param = LuaUtils:string_split(arg, "\001")
  local slot = tonumber(param[1])
  local _x, _y, _w, _h = tonumber(param[2]), tonumber(param[3]), tonumber(param[4]), tonumber(param[5])
  local boxType = "Krypton"
  local operateLeftText, operateLeftFunc
  local item = self.kryptonBag[slot]
  local operationTable = {}
  local number = 0
  operateLeftText = GameLoader:GetGameText("LC_MENU_USE_CHAR")
  operateLeftFunc = self.UseKrypton
  operationTable.btnUseVisible = true
  operationTable.btnUpgradeVisible = true
  number = #item.formation
  if item then
    kejin.decomposeId = item.id
    ItemBox:SetKryptonBox(item, kejin:GetKryptonType(item), number)
    ItemBox:ShowKryptonBox(_x, _y, operationTable)
    self:SetCurrentChargeKrypton(item.id)
  end
end
function kejin:UpdataFleetSlotState(kryptonId, isEnd)
  local currentSelectKrypton = self.kryptonBag[kryptonId]
  if currentSelectKrypton then
    self:CheckKryptonWithFleetKrypton(currentSelectKrypton, isEnd)
  end
end
function kejin:CheckKryptonWithFleetKrypton(krypton, isEnd)
  DebugOut("CheckKryptonWithFleetKrypton__")
  self:CheckInitGameData()
  local kryptonLabInfo = GameGlobalData:GetBuildingInfo("krypton_center")
  local state = 0
  if not isEnd then
    state = self:GetCurrentKryptonInFleetKryptonState(krypton)
  end
  local allSlotState = self:GetFleetKryptonState(state, krypton)
  local lockLevel = ""
  for k, v in pairs(self.kryptonSlotLevel) do
    if v.labLevel <= kryptonLabInfo.level then
      lockLevel = lockLevel .. "no" .. "\001"
    else
      lockLevel = lockLevel .. v.labLevel .. "\001"
    end
  end
  DebugOut("state = " .. state)
  DebugOut("allSlotState = " .. allSlotState)
  DebugOut("lockLevelText = " .. lockLevel)
  DebugOutPutTable(kryptonLabInfo, "kryptonLabInfo ")
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "UpdateEquipmentState", allSlotState, lockLevel, GameLoader:GetGameText("LC_MENU_Level"))
  end
end
function kejin:GetFleetKryptonState(state, currentKrypton)
  local slotState = {}
  local slotCount = 8
  if state == 0 then
    for i = 1, slotCount do
      local _state = "no"
      table.insert(slotState, _state)
    end
  elseif state == 1 then
    for i = 1, slotCount do
      local fleetkrypton = self.currentFleetKrypton[i]
      local _state = ""
      if fleetkrypton then
        if self:GetTwoKrytponAttribute(currentKrypton, fleetkrypton) then
          _state = "no"
        else
          _state = "cannot"
        end
      else
        _state = "cannot"
      end
      table.insert(slotState, _state)
    end
  elseif state == 2 then
    for i = 1, slotCount do
      local fleetkrypton = self.currentFleetKrypton[i]
      local _state = ""
      if fleetkrypton then
        if self:GetTwoKrytponAttribute(currentKrypton, fleetkrypton) then
          _state = "not"
        else
          _state = "cannot"
        end
      else
        _state = "cannot"
      end
      table.insert(slotState, _state)
    end
  end
  return ...
end
function kejin:GetCurrentKryptonInFleetKryptonState(currentKrypton)
  local currentkryptonAttributeCount = LuaUtils:table_size(currentKrypton.addon)
  local hasTheSameAttributeSlotCount = 0
  for _, v in pairs(self.currentFleetKrypton) do
    if self:GetTwoKrytponAttribute(currentKrypton, v) then
      hasTheSameAttributeSlotCount = hasTheSameAttributeSlotCount + 1
    end
  end
  return hasTheSameAttributeSlotCount
end
function kejin:GetTwoKrytponAttribute(krypton1, krypton2)
  local krypton1AttributeCount = LuaUtils:table_size(krypton1.addon)
  local count = 0
  for _, v1 in pairs(krypton1.addon) do
    for _, v2 in pairs(krypton2.addon) do
      if v1.type == v2.type then
        count = count + 1
        break
      end
    end
  end
  return count > 0
end
function kejin:FleetItemReleased(arg)
  local param = LuaUtils:string_split(arg, "\001")
  local slot = tonumber(param[1])
  local _x, _y, _w, _h = tonumber(param[2]), tonumber(param[3]), tonumber(param[4]), tonumber(param[5])
  local boxType = "Krypton"
  local item = self.currentFleetKrypton[slot]
  local operateText = GameLoader:GetGameText("LC_MENU_UNLOAD_BUTTON")
  local function operateFunc()
    DebugTable(item)
    if kejin:GetBagSlot() > 0 then
      self:EquipOffKrypton(item.id, kejin:GetBagSlot())
    else
      local tip = GameLoader:GetGameText("LC_ALERT_krypton_store_full")
      GameTip:Show(tip)
    end
  end
  kejin.UnloadKrypton = operateFunc
  local operationTable = {}
  operationTable.btnUnloadVisible = true
  operationTable.btnUpgradeVisible = true
  if item then
    kejin.decomposeId = item.id
    ItemBox:SetKryptonBox(item, kejin:GetKryptonType(item), #item.formation)
    ItemBox:ShowKryptonBox(_x, _y, operationTable)
    self:SetCurrentChargeKrypton(item.id)
  end
end
function kejin.requestDecompose()
  local item
  for k, v in pairs(kejin.kryptonBag) do
    if v.id == kejin.decomposeId then
      item = v
    end
  end
  if not item then
    for k, v in pairs(kejin.currentFleetKrypton) do
      if v.id == kejin.decomposeId then
        item = v
      end
    end
  end
  kejin:GetFlashObject():InvokeASCallback("_root", "decomposeSingleKrypton", item.slot)
  if #item.formation > 0 then
    GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
    GameUIMessageDialog:SetYesButton(kejin.DoRequestDecompose, {})
    local formations = ""
    for i, v in ipairs(item.formation) do
      formations = formations .. FleetMatrix.GetRomaNumber(v.formation_id) .. "/"
    end
    formations = string.sub(formations, 1, -2)
    GameUIMessageDialog:Display(GameLoader:GetGameText("LC_MENU_ALERT_TITLE_DECOMPOSE"), GameLoader:GetGameText("LC_MENU_SAVE_FORMATION_INFO_4") .. formations)
  elseif 1 < item.level or item.rank >= 6 then
    DebugOut("item.level: ", item.level)
    GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
    GameUIMessageDialog:SetYesButton(kejin.DoRequestDecompose, {})
    GameUIMessageDialog:Display(GameLoader:GetGameText("LC_MENU_ALERT_TITLE_DECOMPOSE"), GameLoader:GetGameText("LC_MENU_ALERT_DECOMPOSE"))
  else
    kejin.DoRequestDecompose()
  end
end
function kejin.DoRequestDecompose()
  local krypton_decompose_req_param = {
    krypton_type = 1,
    id = kejin.decomposeId
  }
  if kejin.currentChargeItemId == kejin.decomposeId then
    kejin.currentChargeItemId = nil
  end
  local function netFailedCallback()
    kejin:RequestKryptonInBag()
  end
  kejin:SendMsgWithMatrix(NetAPIList.krypton_decompose_req.Code, krypton_decompose_req_param, kejin.DecomposeCallback, true, netFailedCallback)
end
function kejin:dragKryptonItem(cmd, arg)
  local param = LuaUtils:string_split(arg, "\001")
  local initPosX, initPosY, posX, posY = unpack(param)
  self.dragX = tonumber(posX)
  self.dragY = tonumber(posY)
  self.initPosX = tonumber(initPosX)
  self.initPosY = tonumber(initPosX)
  local pressIndex = -1
  if cmd == "dragFleetItem" then
    pressIndex = self.pressFleetSlot
  elseif cmd == "dragBagItem" then
    pressIndex = self.pressBagSlot
  end
  DebugOut("pressIndex: ", pressIndex)
  if pressIndex ~= -1 then
    local item
    if cmd == "dragFleetItem" then
      item = self.currentFleetKrypton[pressIndex]
    else
      item = self.kryptonBag[pressIndex]
    end
    DebugOut("item: ", item)
    if item then
      if cmd == "dragFleetItem" then
        GameFleetEquipment.dragBagItem = false
        GameFleetEquipment.dragFleetItem = true
      elseif cmd == "dragBagItem" then
        GameFleetEquipment.dragFleetItem = false
        GameFleetEquipment.dragBagItem = true
        GameFleetEquipment.dragBagKrypton = true
      end
      GameStateManager:GetCurrentGameState():BeginDragItem(item.id, kejin:GetKryptonType(item), initPosX, initPosY, posX, posY)
    else
      kejin:onEndDragItem()
    end
  end
end
function kejin:onBeginDragItem(dragitem)
  GameFleetEquipment.dragItemInstance = dragitem
  DebugOut("onBeginDragItem: ", GameFleetEquipment.dragBagItem, GameFleetEquipment.dragFleetItem, self.pressBagSlot, self.pressFleetSlot)
  self:GetFlashObject():InvokeASCallback("_root", "onBeginDragItem", GameFleetEquipment.dragBagItem, GameFleetEquipment.dragFleetItem, self.pressBagSlot, self.pressFleetSlot)
end
function kejin:onEndDragItem()
  DebugOut("kejin:onEndDragItem()")
  GameFleetEquipment.dragBagKrypton = false
  GameFleetEquipment.dragBagItem = false
  GameFleetEquipment.dragFleetItem = false
  GameFleetEquipment.dragItemInstance = nil
  self:GetFlashObject():InvokeASCallback("_root", "onEndDragItem")
  kejin:RefreshBagKrypton()
  kejin:RefreshCurFleetKrypton()
end
function kejin.DoDecomposeAllAnimation()
  local frame = ""
  for i = 1, 12 do
    local item = kejin.kryptonBag[i]
    if item and #item.formation == 0 and item.rank < 3 then
      frame = frame .. "play" .. "\001"
    else
      frame = frame .. "no" .. "\001"
    end
  end
  kejin:DoDecomposeAll()
  kejin:GetFlashObject():InvokeASCallback("_root", "PlayDecomposeAnimation", frame)
  kejin.DecomposeTag = false
end
function kejin:DoDecomposeAll()
  local krypton_decompose_req_param = {
    krypton_type = 3,
    id = kejin.decomposeId
  }
  kejin:SendMsgWithMatrix(NetAPIList.krypton_decompose_req.Code, krypton_decompose_req_param, kejin.DecomposeCallback, true)
end
function kejin:SetKryptonDecomposeLevel(level)
  DebugOut("kejin:SetKryptonDecomposeLevel : " .. level)
  kejin.KryptonAutoDecomposeLevel = level
  local msgPrams = {
    change_num = kejin.KryptonAutoDecomposeLevel
  }
  kejin:SendMsgWithMatrix(NetAPIList.change_auto_decompose_req.Code, msgPrams, nil, false, nil)
  kejin:UpdateAutoComposeLvBar()
end
function kejin:testKrypton(v)
  v.formation = v.formation or {
    [1] = {
      formation_id = 1,
      fleet_id = 1,
      slot = 1
    },
    [2] = {
      formation_id = 2,
      fleet_id = 2,
      slot = 2
    }
  }
end
function kejin:ProcessMatrixInfo(t)
  local r = {}
  local system_index = GameFleetEquipment:GetCurMatrixId()
  for k, v in pairs(t) do
    for tk, tv in pairs(v.formation) do
      if tv.formation_id == system_index then
        r[tv.slot] = v
      end
    end
  end
  DebugOut("ProcessMatrixInfo___", system_index)
  DebugTable(t)
  DebugTable(r)
  return r
end
function kejin:ProcessBagInfo(t)
  local r = {}
  for k, v in pairs(t) do
    r[v.slot] = v
  end
  DebugTable(r)
  return r
end
function kejin.DownloadKryptonBag(msgType, content)
  if msgType == NetAPIList.krypton_store_ack.Code then
    DebugOutPutTable(content, "DownloadKrypton === ")
    kejin.grid_capacity = content.grid_capacity
    kejin.adv_cost = content.gacha_need_credit
    kejin.kryptonBag = content.kryptons
    kejin.kryptonBag = kejin:ProcessBagInfo(kejin.kryptonBag)
    kejin:SetBagList()
    kejin:RefreshkLevel(content.klevel, content.gacha_need_money, content.is_high, content.use_credit_count, content.gacha_need_credit, content.gacha_need_credit == 0)
    return true
  elseif msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.krypton_store_req.Code then
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  end
  return false
end
function kejin.tlc_DownloadKryptonBag(msgType, content)
  if msgType == NetAPIList.tlc_krypton_store_ack.Code then
    DebugOutPutTable(content, "tlc_DownloadKrypton === ")
    kejin.grid_capacity = content.grid_capacity
    kejin.adv_cost = content.gacha_need_credit
    kejin.kryptonBag = content.kryptons
    kejin.kryptonBag = kejin:ProcessBagInfo(kejin.kryptonBag)
    kejin:SetBagList()
    kejin:RefreshkLevel(content.klevel, content.gacha_need_money, content.is_high, content.use_credit_count, content.gacha_need_credit, content.gacha_need_credit == 0)
    return true
  elseif msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.tlc_krypton_store_req.Code then
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  end
  return false
end
function kejin.gachaCallback(msgType, content)
  if msgType == NetAPIList.krypton_gacha_one_ack.Code then
    kejin.actionKryptonMsgType = msgType
    kejin.actionKryptonContent = content
    kejin.adv_cost = content.gacha_need_credit
    kejin:AfterAnimationRefreshBag()
    if QuestTutorialUseKrypton:IsActive() and not GameUtils:GetTutorialHelp() then
      QuestTutorialUseKrypton:SetFinish(true)
      QuestTutorialEquipKrypton:SetActive(true)
      local function callback()
        kejin:GetFlashObject():InvokeASCallback("_root", "ShowTutorialKryptonTab")
      end
      kejin:GetFlashObject():InvokeASCallback("_root", "HideTutorialBtnAction")
      GameUICommonDialog:PlayStory({100036}, callback)
    end
    if GameUtils:GetTutorialHelp() and GameUtils:GetTutorialName() == "composeKrypton" then
      GameUtils:HideTutorialHelp()
    end
    return true
  elseif msgType == NetAPIList.krypton_gacha_some_ack.Code then
    kejin.actionKryptonMsgType = msgType
    kejin.actionKryptonContent = content
    kejin.adv_cost = content.gacha_need_credit
    kejin:AfterAnimationRefreshBag()
    if QuestTutorialUseKrypton:IsActive() and not GameUtils:GetTutorialHelp() then
      QuestTutorialUseKrypton:SetFinish(true)
      QuestTutorialEquipKrypton:SetActive(true)
      local function callback()
        kejin:GetFlashObject():InvokeASCallback("_root", "ShowTutorialKryptonTab")
      end
      kejin:GetFlashObject():InvokeASCallback("_root", "HideTutorialBtnAction")
      GameUICommonDialog:PlayStory({100036}, callback)
    end
    if GameUtils:GetTutorialHelp() and GameUtils:GetTutorialName() == "composeKrypton" then
      GameUtils:HideTutorialHelp()
    end
    return true
  elseif msgType == NetAPIList.common_ack.Code then
    if content.api == NetAPIList.krypton_energy_req.Code or content.api == NetAPIList.krypton_gacha_some_req.Code or content.api == NetAPIList.krypton_gacha_one_req.Code then
      kejin.actionKryptonError = true
      kejin.actionKryptonCode = content.code
      kejin:AfterAnimationRefreshBag()
      if GameUtils:GetTutorialHelp() and GameUtils:GetTutorialName() == "composeKrypton" then
        GameUtils:HideTutorialHelp()
      end
    else
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  return false
end
function kejin.GotoGetMoney()
  local affairBuildInfo = GameGlobalData:GetBuildingInfo("affairs_hall")
  if affairBuildInfo.level >= 1 then
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateAffairInfo)
  else
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateMainPlanet)
    local GameUIBuilding = LuaObjectManager:GetLuaObject("GameUIBuilding")
    GameUIBuilding:DisplayUpgradeDialog("affairs_hall")
  end
end
function kejin.NeedMoreMoney(text)
  GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.Green)
  local cancel = GameLoader:GetGameText("LC_MENU_BUTTON_CAPTION_CANCEL")
  local affairs = GameLoader:GetGameText("LC_MENU_GET_MORE_BUTTON")
  GameUIMessageDialog:SetRightTextButton(cancel)
  GameUIMessageDialog:SetLeftGreenButton(affairs, kejin.GotoGetMoney)
  GameUIMessageDialog:Display("", text)
end
function kejin:AfterAnimationRefreshBag()
  if self.animationAction then
    if self.actionKryptonError then
      local text = AlertDataList:GetTextFromErrorCode(self.actionKryptonCode)
      if tonumber(self.actionKryptonCode) == 130 then
        if not GameUICollect:CheckTutorialCollect(self.actionKryptonCode) then
          kejin.NeedMoreMoney(text)
        end
      else
        local GameVip = LuaObjectManager:GetLuaObject("GameVip")
        GameVip:CheckIsNeedShowVip(self.actionKryptonCode)
      end
      self.actionKryptonError = nil
      self.actionKryptonCode = nil
      self.animationAction = nil
      self.requestComposeing = nil
      self:GetFlashObject():InvokeASCallback("_root", "setButtonEnableWhenAction", true)
      kejin.RefreshResource()
    elseif kejin.actionKryptonMsgType and kejin.actionKryptonContent then
      self.animationAction = nil
      self.requestComposeing = nil
      if kejin.actionKryptonMsgType == NetAPIList.krypton_gacha_one_ack.Code then
        if kejin.actionKryptonContent.money_or_krypton == 0 then
          GameTip:Show(string.format(GameLoader:GetGameText("LC_MENU_KRYPTON_FAIL"), kejin.actionKryptonContent.money), 3000)
        elseif kejin.actionKryptonContent.money_or_krypton == 2 then
          GameTip:Show(string.format(GameLoader:GetGameText("LC_MENU_KRYPTON_DECOMPOSE_ALL"), kejin.actionKryptonContent.increase_kenergy), 3000)
        elseif kejin.actionKryptonContent.money_or_krypton == 3 then
          local numberText = tostring(1000)
          local nameText = GameLoader:GetGameText("LC_MENU_krypton_energy")
          local tipText = string.format(GameLoader:GetGameText("LC_MENU_ITEM_REWARDS_ALERT"), nameText .. "x" .. numberText)
          GameTip:Show(tipText)
        else
          kejin.kryptonBag = LuaUtils:table_values(kejin.kryptonBag)
          table.insert(kejin.kryptonBag, kejin.actionKryptonContent.krypton)
          kejin.kryptonBag = kejin:ProcessBagInfo(kejin.kryptonBag)
          self.animationAction = nil
          self.requestComposeing = nil
          kejin:RefreshBagKrypton()
        end
        kejin:RefreshkLevel(kejin.actionKryptonContent.klevel, kejin.actionKryptonContent.gacha_need_money, kejin.actionKryptonContent.is_high, kejin.actionKryptonContent.use_credit_count, kejin.actionKryptonContent.gacha_need_credit, kejin.actionKryptonContent.gacha_need_credit == 0)
      elseif kejin.actionKryptonMsgType == NetAPIList.krypton_gacha_some_ack.Code then
        kejin.kryptonBag = LuaUtils:table_values(kejin.kryptonBag)
        for k, v in pairs(kejin.actionKryptonContent.kryptons) do
          table.insert(kejin.kryptonBag, v)
        end
        kejin.kryptonBag = kejin:ProcessBagInfo(kejin.kryptonBag)
        kejin:RefreshBagKrypton()
        kejin:RefreshkLevel(kejin.actionKryptonContent.klevel, kejin.actionKryptonContent.gacha_need_money, kejin.actionKryptonContent.is_high, kejin.actionKryptonContent.use_credit_count, kejin.actionKryptonContent.gacha_need_credit, kejin.actionKryptonContent.gacha_need_credit == 0)
        if kejin.autoDecompose then
          local tipText = GameLoader:GetGameText("LC_MENU_KRYPTON_COMOSE_12_DECOMPOSE")
          tipText = string.gsub(tipText, "<kenergy_num>", kejin.actionKryptonContent.increase_kenergy)
          tipText = string.gsub(tipText, "<cubits_num>", kejin.actionKryptonContent.increase_money)
          GameTip:Show(tipText, 3000)
        else
          local tipText = GameLoader:GetGameText("LC_MENU_KRYPTON_COMOSE_12")
          tipText = string.gsub(tipText, "<krypton_num>", LuaUtils:table_size(kejin.actionKryptonContent.kryptons))
          tipText = string.gsub(tipText, "<cubits_num>", kejin.actionKryptonContent.increase_money)
          GameTip:Show(tipText, 3000)
        end
      end
      kejin.RefreshResource()
      self:GetFlashObject():InvokeASCallback("_root", "setButtonEnableWhenAction", true)
      kejin.actionKryptonMsgType = nil
      kejin.actionKryptonContent = nil
    end
  end
end
function kejin:RefreshkLevel(level, gacha_need_money, is_high, adv_level, adv_gacha_need_money, adv_is_high)
  self.currentKlevel = level
  self.currentAdvlevel = adv_level
  local lang = "en"
  if GameSettingData and GameSettingData.Save_Lang then
    lang = GameSettingData.Save_Lang
    if string.find(lang, "ru") == 1 then
      lang = "ru"
    end
  end
  local costText = GameUtils.numberConversion(gacha_need_money)
  local adv_costText = GameUtils.numberConversion(adv_gacha_need_money)
  if not self:GetFlashObject() then
    return
  end
  self:GetFlashObject():InvokeASCallback("_root", "refreshkLevel", self.currentKlevel, costText, is_high, self.currentAdvlevel, adv_costText, adv_is_high, lang)
  for k, v in pairs(kejin:GetShowNumber()) do
    self:GetFlashObject():InvokeASCallback("_root", "setWhichKryptonCanShow", v)
  end
end
function kejin:GetShowNumber()
  if self.currentKlevel > 0 then
    local numberTable = {
      1,
      2,
      3,
      4,
      5,
      6
    }
    local getNumberTbale = {}
    for i = 1, self.currentKlevel + 1 do
      local index = math.random(#numberTable)
      table.insert(getNumberTbale, numberTable[index])
      table.remove(numberTable, index)
    end
    return getNumberTbale
  end
  return nil
end
function kejin.DecomposeCallback(msgType, content)
  kejin.DecomposeTag = false
  if msgType == NetAPIList.krypton_decompose_ack.Code then
    if kejin.decomposeId == "" then
      kejin:RequestKryptonInBag()
    else
      kejin:RequestKryptonInBag()
      kejin.kryptonBag = LuaUtils:table_values(kejin.kryptonBag)
      kejin.kryptonBag = kejin:ProcessBagInfo(kejin.kryptonBag)
    end
    kejin.decomposeId = ""
    local _param = {}
    local param = {}
    _param.item_type = "kenergy"
    _param.number = content.increase_kenergy
    _param.no = 1
    _param.level = 0
    table.insert(param, _param)
    ItemBox:ShowRewardBox(param)
    return true
  elseif msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.krypton_decompose_req.Code then
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  end
  return false
end
function kejin:GetKryptonLevel(itemType)
  local levelName = GameLoader:GetGameText("LC_ITEM_ITEM_NAME_" .. itemType)
  local star, _end = string.find(levelName, "LV")
  levelName = string.sub(levelName, star, string.len(levelName) + 1)
  return levelName
end
function kejin.enhanceKryptonCallback(msgtype, content)
  if msgtype == NetAPIList.krypton_enhance_ack.Code then
    local kry = content.krypton
    local isInFleet = false
    if kry and kry.id then
      for k, v in pairs(kejin.kryptonBag) do
        if v.id == kry.id then
          kejin.kryptonBag[k] = kry
        end
      end
      for k, v in pairs(kejin.currentFleetKrypton) do
        if v.id == kry.id then
          kejin.currentFleetKrypton[k] = kry
          isInFleet = true
        end
      end
      ItemBox:SetKryptonBox(content.krypton, kejin:GetKryptonType(content.krypton), #kry.formation)
      ItemBox:ShowKryptonLevelUp()
    end
    kejin:RefreshBagKrypton()
    kejin:RefreshCurFleetKrypton()
    if not isInFleet then
      kejin:UpdataFleetSlotState(kejin.onSelectBagSlot, false)
    end
    return true
  elseif msgtype == NetAPIList.common_ack.Code then
    if GameFleetEquipment.dragItemInstance then
      GameStateFleetInfo:onEndDragItem()
    end
    local text = AlertDataList:GetTextFromErrorCode(content.code)
    GameTip:Show(text, 3000)
    return true
  end
  return false
end
function kejin.kryptonEquipCallback(msgType, content)
  DebugOut("--------kryptonEquipCallback------------", msgType, content, kejin.equipOffDestSlot, kejin.equipOffSrcSlot, kejin.equipDestSlot)
  if msgType == NetAPIList.krypton_equip_ack.Code then
    local item
    if kejin.equipOffDestSlot ~= -1 then
      item = kejin.kryptonBag[kejin.equipOffDestSlot]
      print(debug.traceback())
      kejin.kryptonBag[kejin.equipOffDestSlot] = kejin.currentFleetKrypton[kejin.equipOffSrcSlot]
      if kejin.kryptonBag[kejin.equipOffDestSlot] then
        kejin.kryptonBag[kejin.equipOffDestSlot].slot = kejin.equipOffDestSlot
      end
    elseif kejin.equipDestSlot ~= -1 then
      item = kejin.currentFleetKrypton[kejin.equipDestSlot]
      kejin.kryptonBag[kejin.equipSrcSlot] = item
      if kejin.kryptonBag[kejin.equipSrcSlot] then
        kejin.kryptonBag[kejin.equipSrcSlot].slot = kejin.equipSrcSlot
      end
      item = nil
    end
    DebugOut("kryptonEquipCallback item: ", item, GameFleetEquipment.dragItemInstance)
    if GameFleetEquipment.dragItemInstance and item then
      GameFleetEquipment.dragItemInstance.DraggedItemID = -1
      local frame = ""
      local id = kejin:GetKryptonType(item)
      if DynamicResDownloader:IsDynamicStuff(id, DynamicResDownloader.resType.PIC) then
        local fullFileName = DynamicResDownloader:GetFullName(id .. ".png", DynamicResDownloader.resType.PIC)
        local localPath = "data2/" .. fullFileName
        if DynamicResDownloader:IfResExsit(localPath) then
          ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
          frame = "item_" .. id
        else
          frame = "temp"
          DynamicResDownloader:AddDynamicRes(id .. ".png", DynamicResDownloader.resType.PIC, nil, nil)
        end
      else
        frame = "item_" .. id
      end
      GameFleetEquipment.dragItemInstance:GetFlashObject():InvokeASCallback("_root", "SetBagItem", frame)
      GameFleetEquipment.dragItemInstance:SetDestPosition(kejin.dragX, kejin.dragY)
      GameFleetEquipment.dragItemInstance:StartAutoMove()
    elseif GameFleetEquipment.dragItemInstance then
      GameStateEquipEnhance:onEndDragItem()
    end
    kejin:RequestKryptonInBag()
    kejin.equipOffDestSlot = -1
    kejin.equipDestSlot = -1
    if QuestTutorialEquipKrypton:IsActive() then
      QuestTutorialEquipKrypton:SetFinish(true)
      QuestTutorialEnhanceKrypton:SetActive(true)
      kejin:GetFlashObject():InvokeASCallback("_root", "HideTutorialEquipKrypton")
    end
    return true
  elseif msgType == NetAPIList.common_ack.Code and (content.api == NetAPIList.krypton_equip_off_req.Code or content.api == NetAPIList.krypton_equip_req.Code) then
    if GameFleetEquipment.dragItemInstance then
      GameStateEquipEnhance:onEndDragItem()
    end
    local text = AlertDataList:GetTextFromErrorCode(content.code)
    GameTip:Show(text, 3000)
    return true
  end
  return false
end
function kejin.swapFleetCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code then
    if content.api == NetAPIList.krypton_fleet_move_req.Code then
      if content.code == 0 then
        local item = kejin.currentFleetKrypton[kejin.swapSrcSlot]
        if GameFleetEquipment.dragItemInstance and item then
          GameFleetEquipment.dragItemInstance.DraggedItemID = -1
          local frame = ""
          local id = kejin:GetKryptonType(item)
          if DynamicResDownloader:IsDynamicStuff(id, DynamicResDownloader.resType.PIC) then
            local fullFileName = DynamicResDownloader:GetFullName(id .. ".png", DynamicResDownloader.resType.PIC)
            local localPath = "data2/" .. fullFileName
            if DynamicResDownloader:IfResExsit(localPath) then
              ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
              frame = "item_" .. id
            else
              frame = "temp"
              DynamicResDownloader:AddDynamicRes(id .. ".png", DynamicResDownloader.resType.PIC, nil, nil)
            end
          else
            frame = "item_" .. id
          end
          GameFleetEquipment.dragItemInstance:GetFlashObject():InvokeASCallback("_root", "SetBagItem", frame)
          GameFleetEquipment.dragItemInstance:SetDestPosition(kejin.dragX, kejin.dragY)
          GameFleetEquipment.dragItemInstance:StartAutoMove()
        elseif GameFleetEquipment.dragItemInstance then
          GameStateEquipEnhance:onEndDragItem()
        end
      else
        GameStateEquipEnhance:onEndDragItem()
        GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      end
      return true
    end
    return false
  end
  return false
end
function kejin.swapBagCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.krypton_store_swap_req.Code then
    if content.code == 0 then
      local item = kejin.kryptonBag[kejin.swapSrcSlot]
      if GameFleetEquipment.dragItemInstance and item then
        GameFleetEquipment.dragItemInstance.DraggedItemID = -1
        local frame = ""
        local id = kejin:GetKryptonType(item)
        if DynamicResDownloader:IsDynamicStuff(id, DynamicResDownloader.resType.PIC) then
          local fullFileName = DynamicResDownloader:GetFullName(id .. ".png", DynamicResDownloader.resType.PIC)
          local localPath = "data2/" .. fullFileName
          if DynamicResDownloader:IfResExsit(localPath) then
            ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
            frame = "item_" .. id
          else
            frame = "temp"
            DynamicResDownloader:AddDynamicRes(id .. ".png", DynamicResDownloader.resType.PIC, nil, nil)
          end
        else
          frame = "item_" .. id
        end
        GameFleetEquipment.dragItemInstance:GetFlashObject():InvokeASCallback("_root", "SetBagItem", frame)
        GameFleetEquipment.dragItemInstance:SetDestPosition(kejin.dragX, kejin.dragY)
        GameFleetEquipment.dragItemInstance:StartAutoMove()
      elseif GameFleetEquipment.dragItemInstance then
        GameStateEquipEnhance:onEndDragItem()
      end
      kejin.kryptonBag[kejin.swapSrcSlot], kejin.kryptonBag[kejin.swapDestSlot] = kejin.kryptonBag[kejin.swapDestSlot], kejin.kryptonBag[kejin.swapSrcSlot]
      if kejin.kryptonBag[kejin.swapSrcSlot] then
        kejin.kryptonBag[kejin.swapSrcSlot].slot = kejin.swapSrcSlot
      end
      kejin.kryptonBag[kejin.swapDestSlot].slot = kejin.swapDestSlot
      kejin.kryptonBag = kejin:ProcessBagInfo(kejin.kryptonBag)
      kejin:RefreshBagKrypton()
    else
      GameStateEquipEnhance:onEndDragItem()
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  return false
end
function kejin.KryptonFragmentCoreNtf(content)
  GameUIGlobalScreen:ShowAlert("error", content.code, nil)
end
if AutoUpdate.isAndroidDevice then
  function kejin.OnAndroidBack()
    if GameUIWDStuff.currentMenu == GameUIWDStuff.menuType.menu_type_help then
      local flashObj = GameUIWDStuff:GetFlashObject()
      if flashObj then
        flashObj:InvokeASCallback("_root", "hideHelp")
      end
    elseif GameStateEquipEnhance.mCurState == SUB_STATE.STATE_GROW_UP then
      GameStateEquipEnhance:GoToSubState(SUB_STATE.STATE_NORMAL)
    elseif GameUIAdjutant.IsShowAdjutantUI then
      GameUIAdjutant:OnFSCommand("close")
    else
      GameFleetNewEnhance:OnFSCommand("closePress")
    end
  end
end
function kejin.Krypton_decompos_change_ntf(content)
  DebugOut("Krypton_decompos_change_ntf : " .. content.change_num)
  kejin.KryptonAutoDecomposeLevelMax = content.max_level
  kejin.KryptonAutoDecomposeLevelMin = content.mini_level
  kejin.KryptonAutoDecomposeLevel = content.change_num
  if not kejin.initDecomposeBar then
    kejin.initDecomposeBar = true
    kejin:UpdateAutoComposeLvBar()
  end
end
function kejin:UpdateAutoComposeLvBar()
  DebugOut("UpdateAutoComposeLvBar")
  if kejin:GetFlashObject() ~= nil then
    local info_parms = kejin.KryptonAutoDecomposeLevelMin .. "\001" .. kejin.KryptonAutoDecomposeLevelMax .. "\001" .. kejin.KryptonAutoDecomposeLevel
    DebugOut(info_parms)
    kejin:GetFlashObject():InvokeASCallback("_root", "setDecomposeLvBar", info_parms)
  end
end
kejin.mCurSortType = 1
function kejin:SortKrypton()
  kejin:RequestSortKryptonInBag()
end
function kejin:RequestSortKryptonInBag()
  local req = {
    type = kejin.mCurSortType
  }
  DebugTable(req)
  if not GameFleetEquipment.isTeamLeague then
    kejin:SendMsgWithMatrix(NetAPIList.krypton_sort_req.Code, req, kejin.RequestSortKryptonInBagCallback, true, nil)
  else
    kejin:SendMsgWithMatrix(NetAPIList.tlc_krypton_sort_req.Code, req, kejin.tlc_RequestSortKryptonInBagCallback, true, nil)
  end
end
function kejin.RequestSortKryptonInBagCallback(msgType, content)
  if msgType == NetAPIList.krypton_sort_ack.Code then
    kejin.grid_capacity = content.grid_capacity
    kejin.adv_cost = content.gacha_need_credit
    DebugOut("~~~__")
    DebugTable(content.kryptons)
    kejin.kryptonBag = content.kryptons
    kejin.kryptonBag = kejin:ProcessBagInfo(kejin.kryptonBag)
    kejin:SetBagList()
    kejin:RefreshkLevel(content.klevel, content.gacha_need_money, content.is_high, content.use_credit_count, content.gacha_need_credit, content.gacha_need_credit == 0)
    kejin.mCurSortType = kejin.mCurSortType + 1
    if kejin.mCurSortType > 2 then
      kejin.mCurSortType = 1
    end
    return true
  elseif msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.krypton_sort_req.Code then
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  end
  return false
end
function kejin.tlc_RequestSortKryptonInBagCallback(msgType, content)
  if msgType == NetAPIList.tlc_krypton_sort_ack.Code then
    kejin.grid_capacity = content.grid_capacity
    kejin.adv_cost = content.gacha_need_credit
    DebugOut("~~~__")
    DebugTable(content.kryptons)
    kejin.kryptonBag = content.kryptons
    kejin.kryptonBag = kejin:ProcessBagInfo(kejin.kryptonBag)
    kejin:SetBagList()
    kejin:RefreshkLevel(content.klevel, content.gacha_need_money, content.is_high, content.use_credit_count, content.gacha_need_credit, content.gacha_need_credit == 0)
    kejin.mCurSortType = kejin.mCurSortType + 1
    if kejin.mCurSortType > 2 then
      kejin.mCurSortType = 1
    end
    return true
  elseif msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.tlc_krypton_sort_req.Code then
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  end
  return false
end
function kejin.UpdateBagListItemCallback(content)
  local baseIdx = tonumber(content.itemId)
  local k = tonumber(content.index)
  local frame = ""
  local level = ""
  local idx = (baseIdx - 1) * 4 + k
  local item = kejin.kryptonBag[idx]
  local percent = 1
  if item then
    local id = kejin:GetKryptonType(item)
    frame = "item_" .. id
    level = GameLoader:GetGameText("LC_MENU_Level") .. item.level
    percent = math.floor(item.refine_exp / item.refine_need_exp * 100) + 1
    if percent > 101 then
      percent = 101
    end
  else
    frame = "empty"
    level = "0"
  end
  local isHide = false
  if idx > kejin.grid_capacity then
    isHide = true
  end
  local one = {
    isHide = isHide,
    fr = frame,
    lv = level,
    progress = percent
  }
  local fourItem = {}
  fourItem[k] = one
  local flashObj = kejin:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "UpdateBagListItem", content.itemId, fourItem[1], fourItem[2], fourItem[3], fourItem[4])
  end
end
function kejin:UpdateBagListItem(itemId)
  local baseIdx = tonumber(itemId)
  local fourItem = {}
  for k = 1, 4 do
    local frame = ""
    local level = ""
    local idx = (baseIdx - 1) * 4 + k
    DebugTable(kejin.kryptonBag)
    local item = kejin.kryptonBag[idx]
    local percent = 1
    if item then
      local id = kejin:GetKryptonType(item)
      if DynamicResDownloader:IsDynamicStuff(id, DynamicResDownloader.resType.PIC) then
        local fullFileName = DynamicResDownloader:GetFullName(id .. ".png", DynamicResDownloader.resType.PIC)
        local localPath = "data2/" .. fullFileName
        if DynamicResDownloader:IfResExsit(localPath) then
          ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
          frame = "item_" .. id
        else
          frame = "temp"
          DynamicResDownloader:AddDynamicRes(id .. ".png", DynamicResDownloader.resType.PIC, {itemId = itemId, index = k}, kejin.UpdateBagListItemCallback)
        end
      else
        frame = "item_" .. id
      end
      level = GameLoader:GetGameText("LC_MENU_Level") .. item.level
      percent = math.floor(item.refine_exp / item.refine_need_exp * 100) + 1
      if percent > 101 then
        percent = 101
      end
    else
      frame = "empty"
      level = "0"
    end
    local isHide = false
    if idx > kejin.grid_capacity then
      isHide = true
    end
    local one = {
      isHide = isHide,
      fr = frame,
      lv = level,
      progress = percent
    }
    fourItem[k] = one
  end
  DebugOut("fourItem")
  DebugTable(fourItem)
  local flashObj = kejin:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "UpdateBagListItem", itemId, fourItem[1], fourItem[2], fourItem[3], fourItem[4])
  end
end
function kejin:SetBagList()
  local flashObj = kejin:GetFlashObject()
  if flashObj then
    local cnt = math.ceil(kejin.grid_capacity / 4)
    DebugOut("kejin:SetBagList()" .. tostring(cnt))
    self:GetFlashObject():InvokeASCallback("_root", "SetBagList", cnt)
    self:GetFlashObject():InvokeASCallback("_root", "setKryptonCount", self:GetKryptonCount(), kejin.grid_capacity, string.gsub(GameLoader:GetGameText("LC_MENU_SAVE_FORMATION_INFO_8"), "<number1>", tostring(kejin.grid_capacity)))
  end
end
function kejin:BagItemClicked(arg)
  local param = LuaUtils:string_split(arg, "\001")
  local idx = (tonumber(param[1]) - 1) * 4 + tonumber(param[2])
  local newArg = tostring(idx) .. "\001" .. param[3] .. "\001" .. param[4] .. "\001" .. param[5] .. "\001" .. param[6]
  kejin:BagItemReleased(newArg)
end
function kejin:RefineCmdHandle(cmd, arg)
  if cmd == "BagItemClicked" then
    kejin:BagItemClicked(arg)
    self:UpdataFleetSlotState(self.onSelectBagSlot, false)
  elseif cmd == "NeedUpdateBagListItem" then
    kejin:UpdateBagListItem(tonumber(arg))
  elseif cmd == "refine_show" then
    kejin:CheckCanEnterRefine()
  elseif cmd == "refine_back" then
    GameFleetInfoBackground:SetRootVisible(true)
    kejin:HideRefinePanel()
  elseif cmd == "refine_closed" then
    kejin.mEnterRefine = false
    kejin:RequestKryptonInBag()
  elseif cmd == "show_target_select_panel" then
    kejin:CheckTutorialRefine1End()
    kejin.mTargetCurClickedIdx = kejin.mTargetCurIdx
    if -1 == kejin.mTargetCurClickedIdx and kejin.mRefineList and #kejin.mRefineList > 0 then
      kejin.mTargetCurClickedIdx = 1
    end
    kejin:SetTargetSelectList()
    kejin:ShowTargetSelectPanel()
  elseif cmd == "close_target_select_panel" then
    kejin:HideTargetSelectPanel()
  elseif cmd == "show_src_select_panel" then
    kejin:CheckTutorialRefine2End()
    kejin:RequestInjectList()
  elseif cmd == "close_src_select_panel" then
    kejin:HideSrcSelectPanel()
  elseif cmd == "NeedUpdateTargetListItem" then
    kejin:UpdateTargetListItem(tonumber(arg))
  elseif cmd == "NeedUpdateSrcListItem" then
    kejin:UpdateSrcListItem(tonumber(arg))
  elseif cmd == "refine_help" then
    GameUIWDStuff:Show(GameUIWDStuff.menuType.menu_type_help)
    GameUIWDStuff:SetHelpText(GameLoader:GetGameText("LC_MENU_REFINE_HELP"))
  elseif cmd == "refine_clicked" then
    kejin:RequestRefine()
  elseif cmd == "pour_clicked" then
    kejin:RequestInject()
  elseif cmd == "refine_add" then
    kejin:RequestPrice()
  elseif cmd == "TargetItemClicked" then
    kejin:TargetItemClicked(arg)
  elseif cmd == "target_choose" then
    kejin:TargetItemSelect()
  elseif cmd == "SrcItemClicked" then
    kejin:SrcItemClicked(arg)
  elseif cmd == "src_choose" then
    kejin:SrcItemSelect()
  elseif cmd == "item_use_clicked" then
    kejin:UseItemClicked()
  elseif cmd == "inject_animation_over" then
    kejin:InjectAnimationOver()
  elseif cmd == "crit_animation_over" then
    kejin:CritAnimationOver()
  end
end
kejin.mEnterRefine = false
kejin.mRefineInfo = nil
kejin.mRefineList = nil
kejin.mRefineNextList = nil
kejin.mInjectList = nil
kejin.mTargetCurClickedIdx = -1
kejin.mTargetCurIdx = -1
kejin.mSrcCurClickedIdx = -1
kejin.mSrcCurIdx = -1
kejin.mIsUseItemOn = false
kejin.mTargetChooseOrUnload = true
kejin.mSrcChooseOrUnload = true
kejin.mCurInjectListForTargetId = ""
kejin.mSrcCurIdxBackup = -1
kejin.mIsCrit = false
kejin.mGotKenergyTip = nil
function kejin:ResetRefineData()
  kejin.mRefineInfo = nil
  kejin.mRefineList = nil
  kejin.mRefineNextList = nil
  kejin.mInjectList = nil
  kejin.mTargetCurClickedIdx = -1
  kejin.mTargetCurIdx = -1
  kejin.mSrcCurClickedIdx = -1
  kejin.mSrcCurIdx = -1
  kejin.mIsUseItemOn = false
  kejin.mTargetChooseOrUnload = true
  kejin.mSrcChooseOrUnload = true
  kejin.mCurInjectListForTargetId = ""
  kejin.mSrcCurIdxBackup = -1
  kejin.mIsCrit = false
  kejin.mGotKenergyTip = nil
end
function kejin:EnterRefine()
  kejin:SetRefinePanelInfo()
  if not kejin.mEnterRefine then
    DebugOut("kejin:EnterRefine")
    kejin.mEnterRefine = true
    GameFleetInfoBackground:SetRootVisible(false)
    kejin:ShowRefinePanel()
  end
  kejin:CheckShowTutorialRefine1()
end
function kejin:ShowRefinePanel()
  local flashObj = kejin:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "ShowRefinePanel")
  end
end
function kejin:HideRefinePanel()
  local flashObj = kejin:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "HideRefinePanel")
  end
end
function kejin:SetRefinePanelInfo()
  local flashObj = kejin:GetFlashObject()
  if flashObj and kejin.mRefineInfo then
    local itemNum = GameUtils.numberConversion(kejin.mRefineInfo.item_count)
    local resource = GameGlobalData:GetData("resource")
    local kryptonNum = GameUtils.numberConversion(resource.kenergy)
    local creditNum = GameUtils.numberConversion(resource.credit)
    flashObj:InvokeASCallback("_root", "SetRefineMoneys", kryptonNum, creditNum, itemNum)
    kejin:SetItemMoneyItemIcon()
  end
  kejin:SetItemCurAndMaxCount()
  kejin:RefreshUseItem()
  kejin:SetCurTargetInfo()
  kejin:SetCurSrcIcon()
end
function kejin.RefreshMoneyIcon()
  kejin:SetItemMoneyItemIcon()
end
function kejin:SetItemMoneyItemIcon()
  local flashObj = kejin:GetFlashObject()
  if flashObj and kejin.mRefineInfo then
    local fr
    local typeId = kejin.mRefineInfo.item_id
    if DynamicResDownloader:IsDynamicStuff(typeId, DynamicResDownloader.resType.PIC) then
      local fullFileName = DynamicResDownloader:GetFullName(typeId .. ".png", DynamicResDownloader.resType.PIC)
      local localPath = "data2/" .. fullFileName
      if DynamicResDownloader:IfResExsit(localPath) then
        ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
        fr = "item_" .. typeId
      else
        fr = "temp"
        DynamicResDownloader:AddDynamicRes(typeId .. ".png", DynamicResDownloader.resType.PIC, nil, kejin.RefreshMoneyIcon)
      end
    else
      fr = "item_" .. typeId
    end
    flashObj:InvokeASCallback("_root", "SetItemMoneyItemIcon", fr)
  end
end
function kejin:ShowTargetSelectPanel()
  kejin:SetTargetClickedInfo(kejin.mTargetCurClickedIdx)
  local flashObj = kejin:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "ShowTargetSelectPanel")
  end
end
function kejin:HideTargetSelectPanel()
  local flashObj = kejin:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "HideTargetSelectPanel")
  end
end
function kejin:SetTargetSelectPanelInfo()
end
function kejin:SetTargetSelectList()
  local flashObj = kejin:GetFlashObject()
  if flashObj then
    local lineCount = math.ceil(#kejin.mRefineList / 3)
    flashObj:InvokeASCallback("_root", "SetTargetList", lineCount)
  end
end
function kejin:UpdateTargetListItem(itemId)
  local flashObj = kejin:GetFlashObject()
  if flashObj then
    local info = {}
    for k = 1, 3 do
      local item = kejin.mRefineList[(itemId - 1) * 3 + k]
      if item then
        local fr
        local lv = GameLoader:GetGameText("LC_MENU_Level") .. item.level
        local typeId = kejin:GetKryptonType(item)
        if DynamicResDownloader:IsDynamicStuff(typeId, DynamicResDownloader.resType.PIC) then
          local fullFileName = DynamicResDownloader:GetFullName(typeId .. ".png", DynamicResDownloader.resType.PIC)
          local localPath = "data2/" .. fullFileName
          if DynamicResDownloader:IfResExsit(localPath) then
            ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
            fr = "item_" .. typeId
          else
            fr = "temp"
            DynamicResDownloader:AddDynamicRes(typeId .. ".png", DynamicResDownloader.resType.PIC, nil, nil)
          end
        else
          fr = "item_" .. typeId
        end
        local isSelected = false
        if (itemId - 1) * 3 + k == kejin.mTargetCurClickedIdx then
          isSelected = true
        end
        local percent = math.floor(item.refine_exp / item.refine_need_exp * 100) + 1
        if percent > 101 then
          percent = 101
        end
        info[k] = {
          fr = fr,
          lv = lv,
          isSelected = isSelected,
          progress = percent
        }
      end
    end
    DebugOut("UpdateTargetListItem ")
    DebugTable(info)
    flashObj:InvokeASCallback("_root", "UpdateTargetListItem", itemId, info[1], info[2], info[3])
  end
end
function kejin:TargetItemClicked(arg)
  local array = LuaUtils:string_split(arg, "\001")
  local oldItemId = -1
  if -1 ~= kejin.mTargetCurClickedIdx then
    oldItemId = math.ceil(kejin.mTargetCurClickedIdx / 3)
  end
  kejin.mTargetCurClickedIdx = (array[1] - 1) * 3 + array[2]
  if -1 ~= oldItemId then
    kejin:UpdateTargetListItem(oldItemId)
  end
  kejin:UpdateTargetListItem(tonumber(array[1]))
  kejin:SetTargetClickedInfo(kejin.mTargetCurClickedIdx)
end
function kejin:SetTargetClickedInfo(idx)
  local flashObj = kejin:GetFlashObject()
  if flashObj then
    local leftName = ""
    local leftIcon
    local leftRank = 0
    local paramText = ""
    local paramNum = ""
    local paramNum2 = ""
    local isInFormation = false
    if kejin.mRefineList and kejin.mRefineList[idx] then
      local item = kejin.mRefineList[idx]
      local typeId = kejin:GetKryptonType(item)
      leftName = GameLoader:GetGameText("LC_ITEM_ITEM_NAME_" .. typeId)
      leftRank = item.rank
      if DynamicResDownloader:IsDynamicStuff(typeId, DynamicResDownloader.resType.PIC) then
        local fullFileName = DynamicResDownloader:GetFullName(typeId .. ".png", DynamicResDownloader.resType.PIC)
        local localPath = "data2/" .. fullFileName
        if DynamicResDownloader:IfResExsit(localPath) then
          ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
          leftIcon = "item_" .. typeId
        else
          leftIcon = "temp"
          DynamicResDownloader:AddDynamicRes(typeId .. ".png", DynamicResDownloader.resType.PIC, nil, nil)
        end
      else
        leftIcon = "item_" .. typeId
      end
      local addon1 = item.addon[1]
      if addon1.type == 20 then
        paramText = GameLoader:GetGameText("LC_MENU_Equip_param_" .. addon1.type)
      else
        paramNum = addon1.value
        paramText = GameLoader:GetGameText("LC_MENU_Equip_param_" .. addon1.type)
        if addon1.type >= 9 and addon1.type <= 14 or addon1.type == 17 then
          paramNum = paramNum / 10 .. "%"
        end
        paramNum = "+" .. paramNum
      end
      isInFormation = 0 < #item.formation
    end
    local rightName = ""
    local rightIcon
    local rightRank = 0
    if kejin.mRefineNextList and kejin.mRefineNextList[idx] then
      local item = kejin.mRefineNextList[idx]
      local typeId = kejin:GetKryptonType(item)
      rightName = GameLoader:GetGameText("LC_ITEM_ITEM_NAME_" .. typeId)
      rightRank = item.rank
      if DynamicResDownloader:IsDynamicStuff(typeId, DynamicResDownloader.resType.PIC) then
        local fullFileName = DynamicResDownloader:GetFullName(typeId .. ".png", DynamicResDownloader.resType.PIC)
        local localPath = "data2/" .. fullFileName
        if DynamicResDownloader:IfResExsit(localPath) then
          ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
          rightIcon = "item_" .. typeId
        else
          rightIcon = "temp"
          DynamicResDownloader:AddDynamicRes(typeId .. ".png", DynamicResDownloader.resType.PIC, nil, nil)
        end
      else
        rightIcon = "item_" .. typeId
      end
      local addon1 = item.addon[1]
      if addon1.type == 20 then
      else
        paramNum2 = addon1.value
        if addon1.type >= 9 and addon1.type <= 14 or addon1.type == 17 then
          paramNum2 = paramNum2 / 10 .. "%"
        end
        paramNum2 = "+" .. paramNum2
      end
    end
    DebugOut("leftName " .. leftName .. " rightName" .. rightName)
    local isJp = false
    if GameSettingData.Save_Lang and GameSettingData.Save_Lang == "jp" then
      isJp = true
    end
    flashObj:InvokeASCallback("_root", "SetTargetPanelRightInfo", paramText, paramNum, paramNum2, leftName, leftIcon, leftRank, rightName, rightIcon, rightRank, isJp, isInFormation)
  end
  kejin:SetTargetChooseBtnText()
end
function kejin:SetTargetChooseBtnText()
  local flashObj = kejin:GetFlashObject()
  if flashObj then
    local btnText = ""
    if -1 ~= kejin.mTargetCurIdx and kejin.mTargetCurClickedIdx == kejin.mTargetCurIdx then
      btnText = GameLoader:GetGameText("LC_MENU_UNLOAD_BUTTON")
      kejin.mTargetChooseOrUnload = false
    else
      btnText = GameLoader:GetGameText("LC_MENU_REFINE_CHOOSE_BUTTON")
      kejin.mTargetChooseOrUnload = true
    end
    flashObj:InvokeASCallback("_root", "SetTargetChooseBtnText", btnText)
  end
end
function kejin:TargetItemSelect()
  if kejin.mTargetChooseOrUnload then
    if kejin.mTargetCurIdx == kejin.mTargetCurClickedIdx then
    else
      kejin.mTargetCurIdx = kejin.mTargetCurClickedIdx
      kejin.mInjectList = nil
      kejin.mSrcCurClickedIdx = -1
      kejin.mSrcCurIdx = -1
      kejin.mSrcCurIdxBackup = -1
      kejin:SetCurTargetInfo()
      kejin:SetCurSrcIcon()
      kejin:CheckShowTutorialRefine2()
    end
    kejin:HideTargetSelectPanel()
  else
    kejin.mTargetCurIdx = -1
    GameSettingData.CurRefineKryptonId = -1
    GameUtils:SaveSettingData()
    kejin.mInjectList = nil
    kejin.mSrcCurClickedIdx = -1
    kejin.mSrcCurIdx = -1
    kejin.mSrcCurIdxBackup = -1
    kejin:SetCurTargetInfo()
    kejin:SetCurSrcIcon()
    kejin:SetTargetClickedInfo(kejin.mTargetCurClickedIdx)
  end
end
function kejin:SetCurTargetIcon()
  local flashObj = kejin:GetFlashObject()
  if flashObj then
    local fr
    local isVisible = false
    if kejin.mRefineList and kejin.mRefineList[kejin.mTargetCurIdx] then
      local item = kejin.mRefineList[kejin.mTargetCurIdx]
      local typeId = kejin:GetKryptonType(item)
      if DynamicResDownloader:IsDynamicStuff(typeId, DynamicResDownloader.resType.PIC) then
        local fullFileName = DynamicResDownloader:GetFullName(typeId .. ".png", DynamicResDownloader.resType.PIC)
        local localPath = "data2/" .. fullFileName
        if DynamicResDownloader:IfResExsit(localPath) then
          ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
          fr = "item_" .. typeId
        else
          fr = "temp"
          DynamicResDownloader:AddDynamicRes(typeId .. ".png", DynamicResDownloader.resType.PIC, nil, nil)
        end
      else
        fr = "item_" .. typeId
      end
      isVisible = true
    end
    flashObj:InvokeASCallback("_root", "SetCurTargetIcon", isVisible, fr)
  end
end
function kejin:SetCurTargetInfo()
  local flashObj = kejin:GetFlashObject()
  if flashObj then
    local percent = 0
    local percentStr = "0.0%"
    local kenergy = "0"
    local isShowAfter = false
    local beforeExp = 0
    local beforeNeedExp = 0
    local afterStr = ""
    local isShowFreeInfo = false
    local isShowItemCountInfo = false
    if kejin.mRefineList and kejin.mRefineList[kejin.mTargetCurIdx] then
      local item = kejin.mRefineList[kejin.mTargetCurIdx]
      beforeExp = item.refine_exp
      beforeNeedExp = item.refine_need_exp
      percent = item.refine_exp / item.refine_need_exp * 100
      if percent > 99.9 and percent < 100 then
        percent = 99.9
      end
      percentStr = string.format("%0.1f", percent) .. "%"
      kenergy = GameUtils.numberConversion(item.refine_need_kenergy)
      if item.rank <= 4 then
        isShowFreeInfo = true
        isShowItemCountInfo = false
      else
        isShowFreeInfo = false
        isShowItemCountInfo = true
      end
      GameSettingData.CurRefineKryptonId = item.id
    else
      isShowFreeInfo = false
      isShowItemCountInfo = false
      GameSettingData.CurRefineKryptonId = -1
    end
    GameUtils:SaveSettingData()
    if kejin.mInjectList and kejin.mInjectList[kejin.mSrcCurIdx] then
      local item = kejin.mInjectList[kejin.mSrcCurIdx]
      local itemExp = 0
      if kejin.mIsUseItemOn then
        itemExp = kejin.mRefineInfo.item_exp
      end
      local afterExp = beforeExp + item.inject_exp + itemExp
      local percent2 = afterExp / beforeNeedExp * 100
      if percent2 > 999.9 then
        percent2 = 999.9
      end
      afterStr = string.format("%0.1f", percent2) .. "%"
      isShowAfter = true
    end
    percent = math.floor(percent)
    flashObj:InvokeASCallback("_root", "SetProgress", percent + 1, percentStr, isShowAfter, afterStr)
    flashObj:InvokeASCallback("_root", "SetRefineBtnKenergy", kenergy)
    flashObj:InvokeASCallback("_root", "SetItemCurAndMaxCountVisible", isShowItemCountInfo)
    flashObj:InvokeASCallback("_root", "SetRefineAddBtnVisible", isShowItemCountInfo and kejin.mRefineInfo.times_can_buy)
    flashObj:InvokeASCallback("_root", "SetFreeInfoVisible", isShowFreeInfo)
  end
  kejin:SetCurTargetIcon()
end
function kejin:ShowSrcSelectPanel()
  kejin:SetSrcClickedInfo(kejin.mSrcCurClickedIdx)
  local flashObj = kejin:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "ShowSrcSelectPanel")
  end
end
function kejin:HideSrcSelectPanel()
  local flashObj = kejin:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "HideSrcSelectPanel")
  end
end
function kejin:SetSrcSelectPanelInfo()
end
function kejin:SetSrcSelectList()
  local flashObj = kejin:GetFlashObject()
  if flashObj then
    local itemCnt = 0
    if kejin.mInjectList then
      itemCnt = math.ceil(#kejin.mInjectList / 3)
    end
    flashObj:InvokeASCallback("_root", "SetSrcList", itemCnt)
  end
end
function kejin:SetChoseButtonStates()
  local buttonState = 0
  if 0 < #self.mInjectList then
    buttonState = 1
  end
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "setChoseButtonStates", buttonState)
  end
end
function kejin:UpdateSrcListItem(itemId)
  local flashObj = kejin:GetFlashObject()
  if kejin.mInjectList and flashObj then
    local info = {}
    for k = 1, 3 do
      local item = kejin.mInjectList[(itemId - 1) * 3 + k]
      if item then
        local fr
        local lv = GameLoader:GetGameText("LC_MENU_Level") .. item.level
        local typeId = kejin:GetKryptonType(item)
        if DynamicResDownloader:IsDynamicStuff(typeId, DynamicResDownloader.resType.PIC) then
          local fullFileName = DynamicResDownloader:GetFullName(typeId .. ".png", DynamicResDownloader.resType.PIC)
          local localPath = "data2/" .. fullFileName
          if DynamicResDownloader:IfResExsit(localPath) then
            ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
            fr = "item_" .. typeId
          else
            fr = "temp"
            DynamicResDownloader:AddDynamicRes(typeId .. ".png", DynamicResDownloader.resType.PIC, nil, nil)
          end
        else
          fr = "item_" .. typeId
        end
        local isSelected = false
        if (itemId - 1) * 3 + k == kejin.mSrcCurClickedIdx then
          isSelected = true
        end
        local percent = math.floor(item.refine_exp / item.refine_need_exp * 100) + 1
        if percent > 101 then
          percent = 101
        end
        info[k] = {
          fr = fr,
          lv = lv,
          isSelected = isSelected,
          progress = percent
        }
      end
    end
    DebugOut("UpdateSrcListItem ")
    DebugTable(info)
    flashObj:InvokeASCallback("_root", "UpdateSrcListItem", itemId, info[1], info[2], info[3])
  end
end
function kejin:SrcItemClicked(arg)
  local array = LuaUtils:string_split(arg, "\001")
  local oldItemId = -1
  if -1 ~= kejin.mSrcCurClickedIdx then
    oldItemId = math.ceil(kejin.mSrcCurClickedIdx / 3)
  end
  kejin.mSrcCurClickedIdx = (array[1] - 1) * 3 + array[2]
  if -1 ~= oldItemId then
    kejin:UpdateSrcListItem(oldItemId)
  end
  kejin:UpdateSrcListItem(tonumber(array[1]))
  kejin:SetSrcClickedInfo(kejin.mSrcCurClickedIdx)
end
function kejin:SetSrcClickedInfo(idx)
  local flashObj = kejin:GetFlashObject()
  if flashObj then
    local nameText = ""
    local iconFr
    local rank = 0
    local isInFormation = false
    if kejin.mInjectList and kejin.mInjectList[idx] then
      local item = kejin.mInjectList[idx]
      local typeId = kejin:GetKryptonType(item)
      nameText = GameLoader:GetGameText("LC_ITEM_ITEM_NAME_" .. typeId)
      rank = item.rank
      if DynamicResDownloader:IsDynamicStuff(typeId, DynamicResDownloader.resType.PIC) then
        local fullFileName = DynamicResDownloader:GetFullName(typeId .. ".png", DynamicResDownloader.resType.PIC)
        local localPath = "data2/" .. fullFileName
        if DynamicResDownloader:IfResExsit(localPath) then
          ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
          iconFr = "item_" .. typeId
        else
          iconFr = "temp"
          DynamicResDownloader:AddDynamicRes(typeId .. ".png", DynamicResDownloader.resType.PIC, nil, nil)
        end
      else
        iconFr = "item_" .. typeId
      end
      isInFormation = 0 < #item.formation
    end
    local isJp = false
    if GameSettingData.Save_Lang and GameSettingData.Save_Lang == "jp" then
      isJp = true
    end
    flashObj:InvokeASCallback("_root", "SetSrcPanelLeftInfo", nameText, iconFr, rank, isJp, isInFormation)
  end
  kejin:SetSrcChooseBtnText()
end
function kejin:SetSrcChooseBtnText()
  local flashObj = kejin:GetFlashObject()
  if flashObj then
    local btnText = ""
    if -1 ~= kejin.mSrcCurIdx and kejin.mSrcCurClickedIdx == kejin.mSrcCurIdx then
      btnText = GameLoader:GetGameText("LC_MENU_UNLOAD_BUTTON")
      kejin.mSrcChooseOrUnload = false
    else
      btnText = GameLoader:GetGameText("LC_MENU_REFINE_CHOOSE_BUTTON")
      kejin.mSrcChooseOrUnload = true
    end
    flashObj:InvokeASCallback("_root", "SetSrcChooseBtnText", btnText)
  end
end
function kejin:SrcItemSelect()
  if kejin.mSrcChooseOrUnload then
    if kejin.mInjectList then
      kejin.mSrcCurIdx = kejin.mSrcCurClickedIdx
      kejin.mSrcCurIdxBackup = kejin.mInjectList[kejin.mSrcCurIdx].id
      kejin:HideSrcSelectPanel()
      kejin:SetCurSrcIcon()
      kejin:SetCurTargetInfo()
    end
  else
    kejin.mSrcCurIdx = -1
    kejin.mSrcCurIdxBackup = -1
    kejin:SetCurSrcIcon()
    kejin:SetCurTargetInfo()
    kejin:SetSrcClickedInfo(kejin.mSrcCurClickedIdx)
  end
end
function kejin:SetCurSrcIcon()
  local flashObj = kejin:GetFlashObject()
  if flashObj then
    local fr
    local isVisible = false
    if kejin.mInjectList and kejin.mInjectList[kejin.mSrcCurIdx] then
      local item = kejin.mInjectList[kejin.mSrcCurIdx]
      local typeId = kejin:GetKryptonType(item)
      if DynamicResDownloader:IsDynamicStuff(typeId, DynamicResDownloader.resType.PIC) then
        local fullFileName = DynamicResDownloader:GetFullName(typeId .. ".png", DynamicResDownloader.resType.PIC)
        local localPath = "data2/" .. fullFileName
        if DynamicResDownloader:IfResExsit(localPath) then
          ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
          fr = "item_" .. typeId
        else
          fr = "temp"
          DynamicResDownloader:AddDynamicRes(typeId .. ".png", DynamicResDownloader.resType.PIC, nil, nil)
        end
      else
        fr = "item_" .. typeId
      end
      isVisible = true
    end
    DebugOut("SetCurSrcIcon " .. tostring(isVisible))
    flashObj:InvokeASCallback("_root", "SetCurSrcIcon", isVisible, fr)
  end
end
function kejin:UseItemClicked()
  if not kejin.mIsUseItemOn and kejin.mRefineInfo and kejin.mRefineInfo.item_count <= 0 then
    local tip = GameLoader:GetGameText("LC_MENU_PROPS_INADEQUATE_INFO")
    GameTip:Show(tip)
    return
  end
  kejin.mIsUseItemOn = not kejin.mIsUseItemOn
  kejin:RefreshUseItem()
  kejin:SetCurTargetInfo()
end
function kejin:RefreshUseItem()
  local flashObj = kejin:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "SetItemUseSelected", kejin.mIsUseItemOn)
  end
end
function kejin:SetItemCurAndMaxCount()
  local flashObj = kejin:GetFlashObject()
  if flashObj then
    local info = ""
    if kejin.mRefineInfo then
      info = tostring(kejin.mRefineInfo.remain_times) .. "/" .. tostring(kejin.mRefineInfo.max_times)
    end
    flashObj:InvokeASCallback("_root", "SetItemCurAndMaxCount", info)
  end
end
function kejin:CheckKryptonRefineEnterBtnVisible()
  local isVisible = false
  local modules = GameGlobalData:GetData("modules_status").modules
  for _, v in pairs(modules) do
    if v.name == "krypton_refine_open" and v.status then
      isVisible = true
      break
    end
  end
  local flashObj = kejin:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "SetEnterRefineBtnVisible", isVisible)
  end
end
function kejin:CheckCanEnterRefine()
  local canEnter = false
  local modules = GameGlobalData:GetData("modules_status").modules
  for _, v in pairs(modules) do
    if v.name == "krypton_refine" and v.status then
      canEnter = true
      break
    end
  end
  if canEnter then
    kejin.mEnterRefine = false
    kejin:ResetRefineData()
    kejin:RequestRefineInfo()
  else
    local tip = GameLoader:GetGameText("LC_MENU_REFINE_UNOPENED_ERROR")
    GameTip:Show(tip)
  end
end
function kejin:PlayInjectAnimation()
  local flashObj = kejin:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "PlayInjectAnimation")
  end
end
function kejin:InjectAnimationOver()
  if kejin.mIsCrit then
    kejin.mIsCrit = false
    kejin:PlayCritAnimation()
  else
    local function callback()
      kejin:ResetRefineData()
      kejin:RequestRefineInfo()
    end
    if kejin.mGotKenergyTip then
      kejin.mGotKenergyTip = nil
    else
      callback()
    end
  end
end
function kejin:PlayCritAnimation()
  local flashObj = kejin:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "PlayCritAnimation")
  end
end
function kejin:CritAnimationOver()
  local function callback()
    kejin:ResetRefineData()
    kejin:RequestRefineInfo()
  end
  if kejin.mGotKenergyTip then
    kejin.mGotKenergyTip = nil
  else
    callback()
  end
end
function kejin:CheckShowTutorialRefine1()
  if not TutorialQuestManager.QuestTutorialRefine:IsFinished() and not TutorialQuestManager.QuestTutorialRefine:IsActive() then
    TutorialQuestManager.QuestTutorialRefine:SetActive(true, false)
    kejin:SetRefineLeftTipVisible(true)
    GameUICommonDialog:PlayStory({1100047}, nil)
  end
end
function kejin:CheckShowTutorialRefine2()
  if TutorialQuestManager.QuestTutorialRefine:IsActive() then
    kejin:SetRefineRightTipVisible(true)
    GameUICommonDialog:PlayStory({1100048}, nil)
  end
end
function kejin:CheckTutorialRefine1End()
  if TutorialQuestManager.QuestTutorialRefine:IsActive() then
    kejin:SetRefineLeftTipVisible(false)
  end
end
function kejin:CheckTutorialRefine2End()
  if TutorialQuestManager.QuestTutorialRefine:IsActive() and not TutorialQuestManager.QuestTutorialRefine:IsFinished() then
    kejin:SetRefineRightTipVisible(false)
    TutorialQuestManager.QuestTutorialRefine:SetActive(false, true)
    TutorialQuestManager.QuestTutorialRefine:SetFinish(true)
  end
end
function kejin:SetRefineLeftTipVisible(isVisible)
  local flashObj = kejin:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "SetRefineLeftTipVisible", isVisible)
  end
end
function kejin:SetRefineRightTipVisible(isVisible)
  local flashObj = kejin:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "SetRefineRightTipVisible", isVisible)
  end
end
function kejin:RequestRefineInfo()
  kejin:SendMsgWithMatrix(NetAPIList.krypton_refine_info_req.Code, nil, self.RequestRefineInfoCallback, true, nil)
end
function kejin.RequestRefineInfoCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.krypton_refine_info_req.Code then
    DebugOut("RequestRefineInfoCallback err.")
    DebugTable(content)
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.krypton_refine_info_ack.Code then
    DebugOut("RequestRefineInfoCallback ok.")
    DebugTable(content)
    kejin.mRefineInfo = content
    kejin:RequestRefineList()
    return true
  end
  return false
end
function kejin:GetIdxInList(list, id)
  local idx = -1
  if list and #list > 0 then
    for k = 1, #list do
      if tostring(list[k].id) == tostring(id) then
        idx = k
        break
      end
    end
  end
  return idx
end
function kejin:RequestRefineList()
  DebugOut("RequestRefineList.")
  kejin:SendMsgWithMatrix(NetAPIList.krypton_refine_list_req.Code, nil, self.RequestRefineListCallback, true, nil)
end
function kejin.RequestRefineListCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.krypton_refine_list_req.Code then
    DebugOut("RequestRefineListCallback err.")
    DebugTable(content)
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.krypton_refine_list_ack.Code then
    DebugOut("RequestRefineListCallback ok.")
    DebugTable(content)
    kejin.mRefineList = content.kryptons
    kejin.mRefineNextList = content.up_kryptons
    if GameSettingData.CurRefineKryptonId then
      DebugOut("GameSettingData.CurRefineKryptonId " .. GameSettingData.CurRefineKryptonId)
      kejin.mTargetCurIdx = kejin:GetIdxInList(kejin.mRefineList, GameSettingData.CurRefineKryptonId)
      if -1 == kejin.mTargetCurIdx then
        GameSettingData.CurRefineKryptonId = -1
        GameUtils:SaveSettingData()
      end
    end
    kejin:EnterRefine()
    return true
  end
  return false
end
function kejin:RequestInjectList()
  if -1 == kejin.mTargetCurIdx or nil == kejin.mRefineList or nil == kejin.mRefineList[kejin.mTargetCurIdx] then
    local tip = GameLoader:GetGameText("LC_MENU_REFINE_KRYPTON_ERROR")
    GameTip:Show(tip)
    return
  end
  if kejin.mRefineList and kejin.mRefineList[kejin.mTargetCurIdx] then
    local item = kejin.mRefineList[kejin.mTargetCurIdx]
    if item.refine_exp >= item.refine_need_exp then
      local tip = GameLoader:GetGameText("LC_MENU_REFINE_FULL_ENERGY_ERROR")
      GameTip:Show(tip)
      return
    end
  end
  local reqId = kejin.mRefineList[kejin.mTargetCurIdx].id
  if reqId == kejin.mCurInjectListForTargetId and kejin.mInjectList then
    kejin.mSrcCurIdx = kejin:GetIdxInList(kejin.mInjectList, kejin.mSrcCurIdxBackup)
    kejin.mSrcCurClickedIdx = kejin.mSrcCurIdx
    if -1 == kejin.mSrcCurClickedIdx and #kejin.mInjectList > 0 then
      kejin.mSrcCurClickedIdx = 1
    end
    DebugTable(kejin.mInjectList)
    DebugOut("kejin.mSrcCurIdx " .. tostring(kejin.mSrcCurIdx) .. " kejin.mSrcCurIdxBackup " .. tostring(kejin.mSrcCurIdxBackup))
    kejin:SetSrcSelectList()
    kejin:ShowSrcSelectPanel()
  else
    kejin.mInjectList = nil
    kejin.mSrcCurIdxBackup = -1
    kejin.mCurInjectListForTargetId = reqId
    DebugOut("RequestInjectList.")
    local req = {
      refined_id = kejin.mCurInjectListForTargetId
    }
    kejin:SendMsgWithMatrix(NetAPIList.krypton_inject_list_req.Code, req, self.RequestInjectListCallback, true, nil)
  end
end
function kejin:RequestRefine()
  DebugOut("RequestRefine.")
  if kejin.mRefineList and kejin.mRefineList[kejin.mTargetCurIdx] then
    local item = kejin.mRefineList[kejin.mTargetCurIdx]
    if item.refine_exp < item.refine_need_exp then
      local tip = GameLoader:GetGameText("LC_MENU_REFINE_CAN_NOT_REFINE_ERROR")
      GameTip:Show(tip)
      return
    end
    kejin.DoRequestRefine(item.id)
  else
    local tip = GameLoader:GetGameText("LC_MENU_REFINE_KRYPTON_ERROR")
    GameTip:Show(tip)
  end
end
function kejin.DoRequestRefine(itemId)
  DebugOut("DoRequestRefine")
  local req = {refine_id = itemId}
  kejin:SendMsgWithMatrix(NetAPIList.krypton_refine_req.Code, req, kejin.RequestRefineCallback, true, nil)
end
function kejin.RequestRefineCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.krypton_refine_req.Code then
    DebugOut("RequestRefineCallback err.")
    DebugTable(content)
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.krypton_refine_ack.Code then
    DebugOut("RequestRefineCallback ok.")
    DebugTable(content)
    local _param = {}
    local param = {}
    _param.item_type = "kenergy"
    _param.number = content.kenergy
    _param.no = 1
    _param.level = 0
    table.insert(param, _param)
    ItemBox:ShowRewardBox(param)
    kejin:ResetRefineData()
    kejin:RequestRefineInfo()
    return true
  end
  return false
end
function kejin:RequestInject()
  if not kejin.mInjectList then
    kejin.DoRequestInject()
    return
  end
  local item = kejin.mInjectList[kejin.mSrcCurIdx]
  if item and #item.formation > 0 then
    GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
    GameUIMessageDialog:SetYesButton(kejin.DoRequestInject)
    local formations = ""
    for i, v in ipairs(item.formation) do
      formations = formations .. FleetMatrix.GetRomaNumber(v.formation_id) .. "/"
    end
    formations = string.sub(formations, 1, -2)
    GameUIMessageDialog:Display(GameLoader:GetGameText("LC_MENU_TITLE_VERIFY"), GameLoader:GetGameText("LC_MENU_SAVE_FORMATION_INFO_6") .. formations)
  else
    kejin.DoRequestInject()
  end
end
function kejin.DoRequestInject()
  DebugOut("RequestInject.")
  if -1 == kejin.mTargetCurIdx or nil == kejin.mRefineList or nil == kejin.mRefineList[kejin.mTargetCurIdx] or nil == kejin.mInjectList or nil == kejin.mInjectList[kejin.mSrcCurIdx] then
    local tip = GameLoader:GetGameText("LC_MENU_REFINE_CONDITION_NOT_MET_ERROR")
    GameTip:Show(tip)
    return
  end
  local isFree = false
  if kejin.mRefineList and kejin.mRefineList[kejin.mTargetCurIdx] then
    local item = kejin.mRefineList[kejin.mTargetCurIdx]
    if item.rank <= 4 then
      isFree = true
    end
  end
  if kejin.mRefineInfo and kejin.mRefineInfo.remain_times and kejin.mRefineInfo.remain_times <= 0 and not isFree then
    local tip = GameLoader:GetGameText("LC_MENU_REFINE_MAX_TIME_ERROR")
    GameTip:Show(tip)
    return
  end
  if kejin.mIsUseItemOn and kejin.mRefineInfo and 0 >= kejin.mRefineInfo.item_count then
    local tip = GameLoader:GetGameText("LC_MENU_PROPS_INADEQUATE_INFO")
    GameTip:Show(tip)
    return
  end
  if kejin.mRefineList and kejin.mRefineList[kejin.mTargetCurIdx] and kejin.mInjectList and kejin.mInjectList[kejin.mSrcCurIdx] then
    local req = {
      injected_id = kejin.mRefineList[kejin.mTargetCurIdx].id,
      inject_id = kejin.mInjectList[kejin.mSrcCurIdx].id,
      with_item = kejin.mIsUseItemOn
    }
    kejin:SendMsgWithMatrix(NetAPIList.krypton_inject_req.Code, req, kejin.RequestInjectCallback, true, nil)
  else
  end
end
function kejin.RequestInjectCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.krypton_inject_req.Code then
    DebugOut("RequestInjectCallback err.")
    DebugTable(content)
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.krypton_inject_ack.Code then
    DebugOut("RequestInjectCallback ok.")
    DebugTable(content)
    local tip = GameLoader:GetGameText("LC_MENU_REFINE_GET_KRYPTON_ENERGY_INFO")
    local _param = {}
    local param = {}
    _param.item_type = "kenergy"
    _param.number = content.kenergy
    _param.no = 1
    _param.level = 0
    table.insert(param, _param)
    ItemBox:ShowRewardBox(param)
    kejin.mIsCrit = content.is_critical
    kejin:PlayInjectAnimation()
    return true
  end
  return false
end
function kejin:RequestPrice()
  local req = {
    price_type = "krypton_inject",
    type = 0
  }
  kejin:SendMsgWithMatrix(NetAPIList.price_req.Code, req, self.RequestPriceCallback, true, nil)
end
function kejin.RequestPriceCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.price_req.Code then
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.price_ack.Code then
    DebugOut("kejin.RequestCrossPriceCallback")
    DebugTable(content)
    local function okCallback()
      kejin:RequestBuy()
    end
    local info = string.format(GameLoader:GetGameText("LC_MENU_REFINE_BUY_INFO"), content.price)
    GameUIGlobalScreen:ShowMessageBox(2, "", info, okCallback, nil)
    return true
  end
  return false
end
function kejin:RequestBuy()
  DebugOut("RequestPour.")
  kejin:SendMsgWithMatrix(NetAPIList.krypton_inject_buy_req.Code, nil, self.RequestBuyCallback, true, nil)
end
function kejin.RequestBuyCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.krypton_inject_buy_req.Code then
    DebugOut("RequestBuyCallback err.")
    DebugTable(content)
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.krypton_inject_buy_ack.Code then
    DebugOut("RequestBuyCallback ok.")
    DebugTable(content)
    if kejin.mRefineInfo then
      kejin.mRefineInfo.remain_times = content.remain_times
      kejin.mRefineInfo.max_times = content.max_times
      kejin:SetItemCurAndMaxCount()
    end
    return true
  end
  return false
end
function kejin:GetHelpTutorialPos(...)
  if GameUtils:GetTutorialHelp() then
    self:GetFlashObject():InvokeASCallback("_root", "GetHelpTutorialPos")
  end
end
function kejin:GetHelpTutorialCompositePos(...)
  if GameUtils:GetTutorialHelp() then
    self:GetFlashObject():InvokeASCallback("_root", "GetHelpTutorialCompositePos")
  end
end
function kejin:OnFSCommand(cmd, arg)
  if cmd == "GotoKryptonMasterPage" then
    local _fleet, index = ZhenXinUI():GetCurFleetContent()
    if _fleet and _fleet.identity then
      DebugOut("GotoKryptonMasterPage~", GameUIMaster.CurrentScope, GameUIMaster.CurrentSystemType, GameUIMaster.CurrentPageType, GameUIMaster.CurMatrixId)
      GameUIMaster.CurrentScope = {
        [1] = tonumber(_fleet.id)
      }
      GameUIMaster.CurrentSystemType = GameUIMaster.MasterSystem.KryptonMaster
      GameUIMaster.CurrentPageType = GameUIMaster.MasterPage.krypton_enchance
      GameUIMaster.CurMatrixId = GameFleetEquipment:GetCurMatrixId()
      GameStateManager:GetCurrentGameState():AddObject(GameUIMaster)
    end
  elseif cmd == "beginFleetPress" then
    self.pressFleetSlot = tonumber(arg)
  elseif cmd == "FleetKryptonItemReleased" then
    self:FleetItemReleased(arg)
  elseif cmd == "endFleetPress" then
    self.pressFleetSlot = -1
  elseif cmd == "beginBagPress" then
    self.pressBagSlot = tonumber(arg)
    self.onSelectBagSlot = tonumber(arg)
  elseif cmd == "BagItemClicked" then
    self:BagItemClicked(arg)
  elseif cmd == "endBagPress" then
    self.pressBagSlot = -1
  elseif cmd == "NeedUpdateBagListItem" then
    self:UpdateBagListItem(tonumber(arg))
  elseif cmd == "drag_bag_item_start" then
    local param = LuaUtils:string_split(arg, "\001")
    local newArg = "" .. param[3] .. "\001" .. param[4] .. "\001" .. param[5] .. "\001" .. param[6]
    self:dragKryptonItem("dragBagItem", newArg)
  elseif cmd == "dragFleetItem" then
    self:dragKryptonItem(cmd, arg)
  elseif cmd == "dragBagItem" then
    self:dragKryptonItem(cmd, arg)
  elseif cmd == "chargePress" then
    if not self.currentChargeItemId then
      return
    end
    local isInFleet, identity = false, -1
    for k, v in pairs(self.currentFleetKrypton) do
      if v.id == self.currentChargeItemId then
        isInFleet = true
      end
    end
    if isInFleet then
      local fleets = GameGlobalData:GetData("fleetinfo").fleets
      identity = fleets[self.m_currentSelectFleetIndex].identity
    end
    local krypton_enhance_req_param = {
      id = self.currentChargeItemId,
      fleet_identity = identity
    }
    self:SendMsgWithMatrix(NetAPIList.krypton_enhance_req.Code, krypton_enhance_req_param, self.enhanceKryptonCallback, true, nil)
  elseif cmd == "gotoBtnClick" then
    DebugOut("currentShowBluePrint", currentShowBluePrint)
    DebugTable(GameData.Item.Keys[currentShowBluePrint])
    local level = GameData.Item.Keys[currentShowBluePrint].REQ_LEVEL
    local GameObjectAdventure = LuaObjectManager:GetLuaObject("GameObjectAdventure")
    if immanentversion170 == 4 or immanentversion170 == 5 then
      GameObjectAdventure:gotoBossAreaDirectly170(currentShowBluePrint)
    else
      GameObjectAdventure:gotoBossAreaDirectly(level)
    end
  elseif cmd == "krypton_sort" then
    kejin:SortKrypton()
  end
  return false
end
function kejin:RefreshKryptonRedPoint()
  print(debug.traceback())
  DebugOut("~~~~~~~")
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  local fleetId = fleets[kejin.m_currentSelectFleetIndex].identity
  local param = {}
  local system_index = GameFleetEquipment:GetCurMatrixId()
  param.matrix_index = system_index
  param.fleet_id = fleetId
  NetMessageMgr:SendMsg(NetAPIList.krypton_redpoint_req.Code, param, kejin.KryptonRedPointCallBack, false, nil)
end
function kejin.KryptonRedPointCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.krypton_redpoint_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.krypton_redpoint_ack.Code then
    if kejin:GetFlashObject() then
      kejin.art_canupgrade = content.show_artfact_red
      DebugOut("RefreshKryptonRedPoint", content.show_artfact_red)
      DebugTable(content)
      kejin:GetFlashObject():InvokeASCallback("_root", "setKryptonBtnRedPoint", content.show_krypton_red)
      kejin:GetFlashObject():InvokeASCallback("_root", "setArtFactRedPoint", content.show_artupgrade_red or content.show_artfact_red)
    end
    return true
  end
  return false
end
