GameGlobalData = {
  GlobalData = {},
  RECORD_VERSION = "1.0",
  m_dataChangeCallbacks = {}
}
GameGlobalData.AttackRangeType = {}
GameGlobalData.AttackRangeType.Monomer = 1
GameGlobalData.AttackRangeType.Horizontal = 2
GameGlobalData.AttackRangeType.Vertical = 3
GameGlobalData.AttackRangeType.Cross = 4
GameGlobalData.AttackRangeType.Plenary = 5
GameGlobalData.AttackRangeType.Back = 6
GameGlobalData.AttackTargetOrder = {}
GameGlobalData.AttackTargetOrder[1] = {
  1,
  4,
  7,
  2,
  5,
  8,
  3,
  6,
  9
}
GameGlobalData.AttackTargetOrder[2] = {
  2,
  5,
  8,
  1,
  4,
  7,
  3,
  6,
  9
}
GameGlobalData.AttackTargetOrder[3] = {
  3,
  6,
  9,
  2,
  5,
  8,
  1,
  4,
  7
}
GameGlobalData.AttackTargetOrderBack = {}
GameGlobalData.AttackTargetOrderBack[1] = {
  7,
  4,
  1,
  8,
  5,
  2,
  9,
  6,
  3
}
GameGlobalData.AttackTargetOrderBack[2] = {
  8,
  5,
  2,
  7,
  4,
  1,
  9,
  6,
  3
}
GameGlobalData.AttackTargetOrderBack[3] = {
  9,
  6,
  3,
  8,
  5,
  2,
  7,
  4,
  1
}
GameGlobalData.FeatureSwitch = {}
GameGlobalData.isLogin = false
GameGlobalData.Purchase_tobeVerify = {}
GameGlobalData.forRequestActivityInfo = false
GameGlobalData.max_level = 80
GameGlobalData.max_recruit = 9
GameGlobalData.starCraftData = nil
GameGlobalData.redPointStates = nil
function GameGlobalData:building_remapname(tbuildings)
  if not tbuildings then
    return
  end
  local tmap = {
    empire_headquarters = "Command_post"
  }
  for _, data_new in pairs(tbuildings) do
    if tmap[data_new.name] then
      data_new.name = tmap[data_new.name]
    end
  end
end
function GameGlobalData:reConstructTable(dest, content)
  for k, v in pairs(content) do
    if type(v) == "table" then
      if dest[k] == nil then
        dest[k] = {}
      end
      for tk, tv in pairs(v) do
        if type(tv) == "table" then
          dest[k][tk] = {}
          self:reConstructTable(dest[k][tk], tv)
        else
          dest[k][tk] = tv
        end
      end
    else
      dest[k] = v
    end
  end
end
function GameGlobalData:UpdateGameData(destKey, content)
  if type(content) == "table" then
    DebugOut("table")
    if destKey == "" then
      DebugOut("destKey")
      self:reConstructTable(self.GlobalData, content)
    else
      if self.GlobalData[destKey] == nil then
        DebugOut("nil")
        self.GlobalData[destKey] = {}
      end
      self:reConstructTable(self.GlobalData[destKey], content)
    end
  else
    self.GlobalData[destKey] = content
  end
  if self.GlobalData.buildings then
    GameGlobalData:building_remapname(self.GlobalData.buildings.buildings)
  end
end
function GameGlobalData:GetData(path)
  local t = self.GlobalData[path]
  return t
end
function GameGlobalData:GetUserInfo()
  return self.GlobalData.userinfo
end
function GameGlobalData:GetNumMaxSkills()
  return 6
end
function GameGlobalData:CheckEnterSection(areaId, sectionId)
  local progress_info = self.GlobalData.progress
  local completedareaId = progress_info.act
  local completedSectionId = progress_info.chapter
  if areaId < completedareaId or areaId == completedareaId and sectionId <= completedSectionId then
    return true
  end
  return false
end
function GameGlobalData:ComputePlayerBattleForce()
  local totalForceCount = 0
  for _, dataCell in ipairs(GameGlobalData.GlobalData.matrix.cells) do
    if 0 < dataCell.fleet_identity then
      totalForceCount = totalForceCount + self:GetFleetInfo(dataCell.fleet_identity).force
    end
  end
  return totalForceCount
end
function GameGlobalData:GetKryptonDetail(kryptonID, kryptonLevel)
  if self.GlobalData.kryptonDetails == nil then
    self.GlobalData.kryptonDetails = {}
  end
  local key = "" .. kryptonID .. "lv " .. kryptonLevel
  return self.GlobalData.kryptonDetails[key]
end
function GameGlobalData:SetKryptonDetail(kryptonID, kryptonLevel, kryptonDetail)
  if self.GlobalData.kryptonDetails == nil then
    self.GlobalData.kryptonDetails = {}
  end
  local key = "" .. kryptonID .. "lv " .. kryptonLevel
  kryptonDetail.identity = kryptonID
  self.GlobalData.kryptonDetails[key] = kryptonDetail
end
function GameGlobalData:GetFleetDetail(fleetID)
  if self.GlobalData.fleetDetails == nil then
    self.GlobalData.fleetDetails = {}
  end
  return self.GlobalData.fleetDetails[fleetID]
end
function GameGlobalData:SetFleetDetail(fleetID, fleetDetail)
  if self.GlobalData.fleetDetails == nil then
    self.GlobalData.fleetDetails = {}
  end
  fleetDetail.identity = fleetID
  self.GlobalData.fleetDetails[fleetID] = fleetDetail
end
function GameGlobalData:GetFleetInfo(fleetID)
  if self.GlobalData.fleetinfo ~= nil then
    local fleets = self.GlobalData.fleetinfo.fleets
    for i, v in ipairs(fleets) do
      if v and v.identity == fleetID then
        return v, i
      end
    end
  end
  return nil
end
function GameGlobalData:RequestFleetDisplayInfo(fleetList, callback)
  local content = {}
  if fleetList == nil then
    local fleetsInfo = GameGlobalData.GlobalData.fleetinfo.fleets
    local fleetsParam = {}
    for i, fleet in ipairs(fleetsInfo) do
      local showParam = {}
      showParam.fleet_id = fleet.identity
      showParam.level = -1
      table.insert(fleetsParam, showParam)
    end
    content.fleets = fleetsParam
  else
    content.fleets = fleetList
  end
  if callback == nil then
    callback = GameGlobalData.RequestFleetDisplayInfoCallback
  end
  NetMessageMgr:SendMsg(NetAPIList.fleet_show_req.Code, content, callback, false, nil)
end
function GameGlobalData.RequestFleetDisplayInfoCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.fleet_show_req.Code then
    return true
  elseif msgType == NetAPIList.fleet_show_ack.Code then
    if GameGlobalData.GlobalData.fleetinfo.fleetsDisplay == nil then
      GameGlobalData.GlobalData.fleetinfo.fleetsDisplay = {}
    end
    local fleetsDisplay = GameGlobalData.GlobalData.fleetinfo.fleetsDisplay
    for i, v in ipairs(content.fleets) do
      fleetsDisplay[v.fleet_id] = v
    end
    return true
  end
  return false
end
function GameGlobalData:GetFleetDisplayerInfo(fleetID)
  if self.GlobalData.fleetinfo.fleetsDisplay then
    return self.GlobalData.fleetinfo.fleetsDisplay[fleetID]
  end
  return nil
end
function GameGlobalData:SetFleetDisplayerInfo(fleetDisplayInfo)
  self.GlobalData.fleetinfo.fleetsDisplay[fleetDisplayInfo.fleet_id] = fleetDisplayInfo
end
function GameGlobalData:UpdateFleetDisplayerInfo(content)
  if GameGlobalData.GlobalData.fleetinfo.fleetsDisplay == nil then
    GameGlobalData.GlobalData.fleetinfo.fleetsDisplay = {}
  end
  local fleetsDisplay = GameGlobalData.GlobalData.fleetinfo.fleetsDisplay
  for i, v in ipairs(content.fleets) do
    fleetsDisplay[v.fleet_id] = v
  end
end
function GameGlobalData:GetFleetKrypton(fleetID)
  if self.GlobalData.fleetkryptons ~= nil then
    local fleetkryptons = self.GlobalData.fleetkryptons
    for i, v in ipairs(fleetkryptons) do
      if v and v.fleet_identity == fleetID then
        return v, i
      end
    end
  end
  return nil
end
function GameGlobalData:GetEquipment(equipId)
  if self.GlobalData.equipments ~= nil then
    local equipments = self.GlobalData.equipments
    for i, v in ipairs(equipments) do
      if v and v.equip_id == equipId then
        return v, i
      end
    end
  end
  return nil
end
function GameGlobalData:GetFleetInfoWithUDID(udid)
  local fleets = self.GlobalData.fleetinfo.fleets
  for _, v in ipairs(fleets) do
    if v.id == udid then
      return v
    end
  end
  return nil
end
local buildingsProperty = {
  krypton_center = 3,
  commander_academy = 100,
  affairs_hall = 5,
  factory = 6,
  tech_lab = 4,
  star_portal = 100,
  engineering_bay = 1,
  planetary_fortress = 2
}
function GameGlobalData:GetLowestUpgradableBuilding()
  if not self.GlobalData or not self.GlobalData.buildings or not self.GlobalData.buildings.buildings then
    return nil
  end
  local levelInfo = GameGlobalData:GetData("levelinfo")
  if levelInfo == nil then
    return nil
  end
  local lowestBuildingName = ""
  local lowestLevel = 100000
  local property = 100000
  if self.GlobalData.buildings.buildings ~= nil then
    if immanentversion == 1 then
      for i, v in ipairs(self.GlobalData.buildings.buildings) do
        if v and lowestLevel > v.level and v.level < v.max_level and levelInfo.level >= v.require_user_level then
          lowestLevel = v.level
          lowestBuildingName = v.name
        end
      end
    elseif immanentversion == 2 then
      for i, v in ipairs(self.GlobalData.buildings.buildings) do
        if v and v.level < v.max_level and levelInfo.level >= v.require_user_level then
          if v.level == 0 then
            lowestBuildingName = v.name
            break
          else
            local vPropery = buildingsProperty[v.name]
            if property > vPropery then
              property = vPropery
              lowestBuildingName = v.name
            end
          end
        end
      end
    end
  end
  return lowestBuildingName
end
function GameGlobalData:GetBuildingInfo(buildingName)
  DebugOut("GetBuildingInfo")
  DebugTable(self.GlobalData.buildings)
  DebugTable(self.GlobalData.buildings.buildings)
  if not self.GlobalData or not self.GlobalData.buildings or not self.GlobalData.buildings.buildings then
    return nil
  end
  if self.GlobalData.buildings.buildings ~= nil then
    for i, v in ipairs(self.GlobalData.buildings.buildings) do
      if v and v.name == buildingName then
        return v
      end
    end
  end
  return nil
end
function GameGlobalData:fix_BuildingCanEnter(buildingName)
  local building = GameGlobalData:GetBuildingInfo(buildingName)
  if building and building.level > 0 then
    return true
  end
  return false
end
function GameGlobalData:BuildingCanEnter(buildingName)
  local building = GameGlobalData:GetBuildingInfo(buildingName)
  if building and (building.level > 0 or building.status == 1) then
    return true
  end
  return false
end
function GameGlobalData:GetBuildingCDTimes()
  if not self.GlobalData.cdtimes then
    return nil
  end
  local buildCDTimes = {}
  for _, v in pairs(self.GlobalData.cdtimes.cdtimes) do
    if v.cd_type == 1 then
      table.insert(buildCDTimes, v)
    end
  end
  return buildCDTimes
end
function GameGlobalData:GetEnhanceCDTime()
  if not self.GlobalData.cdtimes then
    return nil
  end
  if not self.GlobalData.cdtimes.cdtimes then
    return nil
  end
  for _, v in pairs(self.GlobalData.cdtimes.cdtimes) do
    if v.cd_type == 3 then
      return v
    end
  end
  return nil
end
function GameGlobalData:GetCollectionCDTime()
  if not self.GlobalData.cdtimes then
    return nil
  end
  for _, v in pairs(self.GlobalData.cdtimes.cdtimes) do
    if v.cd_type == 4 then
      return v
    end
  end
  return nil
end
function GameGlobalData:GetArenaCDTime()
  if not self.GlobalData.cdtimes then
    return nil
  end
  for _, v in pairs(self.GlobalData.cdtimes.cdtimes) do
    if v.cd_type == 5 then
      return v
    end
  end
  return nil
end
function GameGlobalData:GetTrickyCDTime()
  if not self.GlobalData.cdtimes then
    return nil
  end
  for _, v in pairs(self.GlobalData.cdtimes.cdtimes) do
    if v.cd_type == 6 then
      return v
    end
  end
  return nil
end
function GameGlobalData:GetRecruitCDTime()
  if not self.GlobalData.cdtimes then
    return nil
  end
  for _, v in pairs(self.GlobalData.cdtimes.cdtimes) do
    if v.cd_type == 9 then
      return v
    end
  end
  return nil
end
function GameGlobalData:NeedShowDurabilityTooLowTips()
  if self:GetData("fleetinfo") then
    local user_commander_list = self:GetData("fleetinfo").fleets
    for _, v in pairs(user_commander_list) do
      if v.damage < 50 then
        return true
      end
    end
  end
  return false
end
function GameGlobalData:GetTechInfo(index)
  DebugOut("index = ", index)
  for _, v in ipairs(self.GlobalData.techniques) do
    DebugTable(v)
    if v.id == index then
      return v
    end
  end
  return nil
end
function GameGlobalData:UpdateFleetInfoAfterBattle(fleets)
  for _, data_new in pairs(fleets) do
    for _, data_last in pairs(self.GlobalData.fleetinfo.fleets) do
      if data_new.id == data_last.identity then
        data_last.damage = data_new.damage
        break
      end
    end
  end
end
function GameGlobalData:RemoveData(path, func)
  local t = self.GlobalData[path]
  if not t then
    return
  end
  for k, v in pairs(t) do
    if func(k, v) then
      t[k] = nil
    end
  end
end
function GameGlobalData:RegisterDataChangeCallback(key, callback)
  DebugOut("register datachange callback for key ", key)
  local callbacksForKey = self.m_dataChangeCallbacks[key]
  if callbacksForKey == nil then
    self.m_dataChangeCallbacks[key] = {}
    callbacksForKey = self.m_dataChangeCallbacks[key]
  end
  for i, v in pairs(callbacksForKey) do
    if v == callback then
      DebugOut("dupicated callback ", callback, "for key ", key)
      return
    end
  end
  table.insert(callbacksForKey, callback)
end
function GameGlobalData:RemoveDataChangeCallback(key, callback)
  local callbacksForKey = self.m_dataChangeCallbacks[key]
  if callbacksForKey then
    for i, v in pairs(callbacksForKey) do
      if v == callback then
        table.remove(callbacksForKey, i)
        break
      end
    end
  end
end
local netDataProcessTable = {}
local SortFleetWithMatrix = function()
  local fleets = GameGlobalData.GlobalData.fleetinfo.fleets
  local fleets_matrix = GameGlobalData:GetData("matrix").cells
  if fleets and fleets_matrix then
    local fleetsInMatrix = {}
    local fleetsNotInMatrix = {}
    for _, fleet in ipairs(fleets) do
      if GameUtils:IsInMatrix(fleet.identity) then
        table.insert(fleetsInMatrix, fleet)
      else
        table.insert(fleetsNotInMatrix, fleet)
      end
    end
    table.sort(fleetsInMatrix, function(a, b)
      return a.identity < b.identity
    end)
    table.sort(fleetsNotInMatrix, function(a, b)
      return a.identity < b.identity
    end)
    fleets = {}
    LuaUtils:table_imerge(fleets, fleetsInMatrix)
    LuaUtils:table_imerge(fleets, fleetsNotInMatrix)
    GameGlobalData.GlobalData.fleetinfo.fleets = fleets
  end
end
function netDataProcessTable.fleetinfo()
  if nil ~= GameGlobalData.GlobalData.fleetinfo.fleets then
    local fleet_array = GameGlobalData.GlobalData.fleetinfo.fleets
    DebugOutPutTable(fleet_array, "fleet_array")
    SortFleetWithMatrix()
    for _, data_fleet in ipairs(fleet_array) do
      table.sort(data_fleet.spells, function(skill_prev, skill_next)
        return skill_prev < skill_next
      end)
      DebugOutPutTable(data_fleet.spells, "spells")
    end
  end
end
function netDataProcessTable.matrix()
  SortFleetWithMatrix()
end
local levelInfoStack = {}
function GameGlobalData:GetLastLevelInfo()
  return levelInfoStack[1]
end
function netDataProcessTable.levelinfo()
  local level_info = GameGlobalData.GlobalData.levelinfo
  if level_info then
    if #levelInfoStack < 2 then
      table.insert(levelInfoStack, LuaUtils:table_rcopy(level_info))
    else
      levelInfoStack[1] = levelInfoStack[2]
      levelInfoStack[2] = LuaUtils:table_rcopy(level_info)
    end
    DebugOutPutTable(levelInfoStack, "levelInfoStack")
    GameGlobalData:CheckNeedShowGotoStore()
    if level_info.last_level then
      level_info.up_level = level_info.level - level_info.last_level
    else
      level_info.up_level = 0
    end
    level_info.last_level = level_info.level
  end
end
function GameGlobalData:RefreshData(key)
  DebugOut("globalData changed ", key)
  local dataCallbackTable
  if key then
    dataCallbackTable = {}
    dataCallbackTable[key] = self.m_dataChangeCallbacks[key]
    if not dataCallbackTable[key] then
      GameUtils:Warning("unknow data key")
    end
  else
    dataCallbackTable = self.m_dataChangeCallbacks
  end
  for key_callback, table_callback in pairs(dataCallbackTable) do
    local before_process = netDataProcessTable[key_callback]
    DebugOut("before_process", before_process)
    if before_process then
      before_process()
    end
    for _, callback_func in ipairs(table_callback) do
      DebugOut("callback_func", callback_func)
      callback_func()
    end
  end
end
function GameGlobalData.RefreshUniversalData(content)
  DebugOutPutTable(content, "GameGlobalData.RefreshUniversalData")
  for k, v in pairs(content.map) do
    GameGlobalData:UpdateGameData(content.map[k].str_key, content.map[k].str_value)
    GameGlobalData:RefreshData(content.map[k].str_key)
  end
end
function GameGlobalData.UpdateResource(content)
  DebugOut("GameGlobalData:UpdateResource")
  local before_update = GameGlobalData.GlobalData.resource
  if before_update and before_update.prestige == 0 and content.prestige > 0 and immanentversion == 1 then
    local GameNewMenuItem = LuaObjectManager:GetLuaObject("GameNewMenuItem")
    GameNewMenuItem:Push("prestige")
    if not TutorialQuestManager.QuestTutorialPrestige:IsActive() and not TutorialQuestManager.QuestTutorialPrestige:IsFinished() then
      TutorialQuestManager.QuestTutorialPrestige:SetActive(true)
      local GameUIBarRight = LuaObjectManager:GetLuaObject("GameUIBarRight")
      GameUIBarRight:CheckQuestTutorial()
    end
  end
  GameGlobalData:UpdateGameData("resource", content)
  GameGlobalData:RefreshData("resource")
end
function GameGlobalData.UpdateLeaderList(content)
  DebugOut("GameGlobalData.UpdateLeaderList")
  GameGlobalData:UpdateGameData("leaderlist", content)
  GameGlobalData:RefreshData("leaderlist")
  local curmatrixindex = GameGlobalData.GlobalData.curMatrixIndex
  if curmatrixindex ~= nil then
    local GameUIBarLeft = LuaObjectManager:GetLuaObject("GameUIBarLeft")
    if GameStateManager:GetCurrentGameState():IsObjectInState(GameUIBarLeft) then
      assert(content.leader_ids[curmatrixindex], "current index is " .. curmatrixindex)
      GameUIBarLeft:updateUserAvatar(content.leader_ids[curmatrixindex])
    end
  end
end
function GameGlobalData.UpdateCDTime(content)
  DebugOut("GameGlobalData:UpdateCDTime")
  local cdTimesTable = GameGlobalData.GlobalData.cdtimes
  if not cdTimesTable then
    GameGlobalData:UpdateGameData("cdtimes", content.cdtimes)
  else
    local cdTimes = GameGlobalData.GlobalData.cdtimes.cdtimes
    if not cdTimes then
      GameGlobalData:UpdateGameData("cdtimes", content.cdtimes)
    else
      for i, v in pairs(content.cdtimes) do
        local exist = false
        for _i, _v in pairs(cdTimes) do
          if _v.id == v.id then
            _v.cd_type = v.cd_type
            _v.status = v.status
            _v.content = v.content
            _v.left_time = v.left_time
            _v.can_do = v.can_do
            exist = true
            break
          end
        end
        if not exist then
          DebugOut("table.insert(cdTimes, v)")
          DebugTable(v)
          table.insert(cdTimes, v)
        end
      end
    end
  end
  GameGlobalData:RefreshData("cdtimes")
  return true
end
function GameGlobalData.UpdateFleets(content)
  DebugOut("GameGlobalData.UpdateFleets")
  DebugTable(content)
  local fleetsParam = {}
  for k, v in pairs(content.fleets) do
    local item, itemK = GameGlobalData:GetFleetInfo(v.identity)
    if item then
      GameGlobalData.GlobalData.fleetinfo.fleets[itemK] = v
    else
      table.insert(GameGlobalData.GlobalData.fleetinfo.fleets, v)
      local showParam = {}
      showParam.fleet_id = v.identity
      showParam.level = -1
      table.insert(fleetsParam, showParam)
    end
  end
  GameGlobalData:RefreshData("fleetinfo")
  if #fleetsParam > 0 then
    GameGlobalData:RequestFleetDisplayInfo(fleetsParam, nil)
  end
end
function GameGlobalData.UpdateFleetKrypton(content)
  DebugOut("GameGlobalData.UpdateFleetKrypton")
  DebugTable(content)
  for k, v in pairs(content.fleetkryptons) do
    local item, itemK = GameGlobalData:GetFleetKrypton(v.fleet_identity)
    if item then
      GameGlobalData.GlobalData.fleetkryptons[itemK] = v
    else
      if GameGlobalData.GlobalData.fleetkryptons == nil then
        GameGlobalData.GlobalData.fleetkryptons = {}
      end
      table.insert(GameGlobalData.GlobalData.fleetkryptons, v)
    end
  end
  DebugTable(GameGlobalData.GlobalData.fleetkryptons)
  GameGlobalData:RefreshData("fleetkryptons")
end
function GameGlobalData.UpdateEquipments(content)
  DebugOut("GameGlobalData.UpdateEquipments")
  local deleteList = content.delete_list or {}
  local updateList = content.update_list or {}
  local equipments = GameGlobalData.GlobalData.equipments
  for k, v in pairs(deleteList) do
    for tk, tv in pairs(equipments) do
      if v == tv.equip_id then
        table.remove(equipments, tk)
        break
      end
    end
  end
  for k, v in pairs(updateList) do
    local item, itemK = GameGlobalData:GetEquipment(v.equip_id)
    if item then
      GameGlobalData.GlobalData.equipments[itemK] = v
    else
      table.insert(GameGlobalData.GlobalData.equipments, v)
    end
  end
  GameGlobalData:RefreshData("equipments")
end
function GameGlobalData.UpdateWdcPlayerInfo(content)
  DebugOut("GameGlobalData.UpdateWdcPlayerInfo")
  DebugTable(content)
  GameGlobalData.GlobalData.wdcPlayerInfo = content.info
  GameGlobalData:RefreshData("wdcPlayerInfo")
  DebugTable(GameGlobalData:GetData("wdcPlayerInfo"))
end
function GameGlobalData.UpdateTlcPlayerInfo(content)
  DebugOut("GameGlobalData.UpdateTlcPlayerInfo")
  DebugTable(content)
  GameGlobalData.GlobalData.tlcPlayerInfo = content.info
  GameGlobalData:RefreshData("tlcPlayerInfo")
  DebugTable(GameGlobalData:GetData("tlcPlayerInfo"))
end
function GameGlobalData._SupplyPointNotifyHandler(content)
  local key_refresh = content.type
  if content.type == "battle_supply" then
    key_refresh = "battle_supply"
  elseif content.type == "ace_combat_supply" then
    key_refresh = "ace_combat_supply"
  elseif content.type == "pvp_supply" then
    key_refresh = "pvp_supply"
  elseif content.type == "laba_supply" then
    key_refresh = "laba_supply"
  elseif content.type == "wd_supply" then
    key_refresh = "wd_supply"
  elseif content.type == "wdc_supply" then
    key_refresh = "wdc_supply"
  elseif content.type == "tlc_supply" then
    key_refresh = "tlc_supply"
  else
    assert(false)
  end
  local table_update = GameGlobalData.GlobalData[key_refresh]
  if not table_update then
    GameGlobalData.GlobalData[key_refresh] = {}
    table_update = GameGlobalData.GlobalData[key_refresh]
  end
  table_update.max = content.max
  table_update.current = content.current
  GameGlobalData:RefreshData(key_refresh)
end
function GameGlobalData.NotifySupplyLootStatusHandler(content)
  GameGlobalData.GlobalData.supply_loot = content
  local GameUIBarLeft = LuaObjectManager:GetLuaObject("GameUIBarLeft")
  GameUIBarLeft:UpdateSupplyLoot()
end
function GameGlobalData.NotifySupplyLootHandler(content)
  local GameTip = LuaObjectManager:GetLuaObject("GameTip")
  local infoContent = ""
  if content.technique > 0 then
    infoContent = GameLoader:GetGameText("LC_MENU_REWARD_INFO")
    infoContent = string.gsub(infoContent, "%[EVENT%]", GameLoader:GetGameText("LC_MENU_PRESTIGE_DAILY_REWARDS_TITLE"))
    infoContent = string.gsub(infoContent, "%[TYPE%]", GameLoader:GetGameText("LC_MENU_TECHNIQUE_CHAR"))
    infoContent = string.gsub(infoContent, "%[NUMBER%]", content.technique)
    GameTip:Show(infoContent)
  end
  if 0 < content.laba_supply then
    infoContent = GameLoader:GetGameText("LC_MENU_REWARD_INFO")
    infoContent = string.gsub(infoContent, "%[EVENT%]", GameLoader:GetGameText("LC_MENU_PRESTIGE_DAILY_REWARDS_TITLE"))
    infoContent = string.gsub(infoContent, "%[TYPE%]", GameLoader:GetGameText("LC_MENU_DEXTER_COIN"))
    infoContent = string.gsub(infoContent, "%[NUMBER%]", content.laba_supply)
    GameTip:Show(infoContent)
  end
  if 0 < content.battle_supply then
    infoContent = GameLoader:GetGameText("LC_MENU_REWARD_INFO")
    infoContent = string.gsub(infoContent, "%[EVENT%]", GameLoader:GetGameText("LC_MENU_PRESTIGE_DAILY_REWARDS_TITLE"))
    infoContent = string.gsub(infoContent, "%[TYPE%]", GameLoader:GetGameText("LC_MENU_SUPPLY_CHAR"))
    infoContent = string.gsub(infoContent, "%[NUMBER%]", content.battle_supply)
    GameTip:Show(infoContent)
  end
  if 0 < content.money then
    infoContent = GameLoader:GetGameText("LC_MENU_REWARD_INFO")
    infoContent = string.gsub(infoContent, "%[EVENT%]", GameLoader:GetGameText("LC_MENU_PRESTIGE_DAILY_REWARDS_TITLE"))
    infoContent = string.gsub(infoContent, "%[TYPE%]", GameLoader:GetGameText("LC_MENU_LOOT_CUBIT"))
    infoContent = string.gsub(infoContent, "%[NUMBER%]", content.money)
    GameTip:Show(infoContent)
  end
end
function GameGlobalData.UpdateProgress(content)
  GameGlobalData.GlobalData.progress.act = content.act
  GameGlobalData.GlobalData.progress.chapter = content.chapter
  GameGlobalData.GlobalData.progress.finish_count = content.finish_count
  GameGlobalData.GlobalData.progress.all_count = content.all_count
  GameGlobalData.GlobalData.progress.adv_chapter = content.adv_chapter
  GameGlobalData:RefreshData("progress")
end
function GameGlobalData.UpdateProgressTemp(content)
  GameGlobalData.GlobalData.progress.finish_count = content.finish_count
  GameGlobalData.GlobalData.progress.all_count = content.all_count
  GameGlobalData:RefreshData("progress")
end
function GameGlobalData.ChangeNameNtf(content)
  local GameTip = LuaObjectManager:GetLuaObject("GameTip")
  local textID = GameLoader:GetGameText("LC_MENU_USE_ITEM_98_SUCCESS")
  GameTip:Show(textID, 3000)
  GameGlobalData.UpdateUserInfo(content)
end
function GameGlobalData.LeaderInfoNtf(content)
  DebugOut("LeaderInfoNtf")
  GameGlobalData.UpdateLeaderList(content)
end
function GameGlobalData.UpdateUserInfo(content)
  DebugOut("GameGlobalData.UpdateUserInfo")
  DebugTable(content)
  GameGlobalData:UpdateGameData("userinfo", content)
  local LoginInfo = GameUtils:GetLoginInfo()
  if LoginInfo then
    LoginInfo.PlayerInfo.name = content.name
  end
  GameSettingData.LastLogin = GameUtils:GetLoginInfo()
  GameUtils:SaveSettingData()
  GameGlobalData:RefreshData("userinfo")
end
function GameGlobalData.UpdateLevel(content)
  DebugOut("GameGlobalData.UpdateLevel")
  DebugTable(content)
  GameGlobalData:UpdateGameData("levelinfo", content)
  GameGlobalData:RefreshData("levelinfo")
end
function GameGlobalData.UpdateBuildings(content)
  DebugOut("GameGlobalData.UpdateBuildings")
  DebugTable(content)
  local local_buildings = GameGlobalData.GlobalData.buildings.buildings
  GameGlobalData:building_remapname(content.buildings)
  for _, data_new in pairs(content.buildings) do
    local needInsert = true
    for i, data_local in pairs(local_buildings) do
      if data_local.name == data_new.name then
        local_buildings[i] = data_new
        needInster = false
        break
      end
    end
    if needInster then
      table.insert(local_buildings, data_new)
    end
  end
  GameGlobalData:RefreshData("buildings")
end
function GameGlobalData.UpdateOffline(content)
  DebugOut("GameGlobalData.UpdateOffline")
  GameGlobalData.GlobalData.offline_info = content
  GameGlobalData:RefreshData("offline_info")
end
function GameGlobalData._UserMatrixNotifyHandler(content)
  DebugOut("_UserMatrixNotifyHandler")
  DebugTable(content)
  GameGlobalData.GlobalData.matrix = content
  for _, cell_data in ipairs(GameGlobalData.GlobalData.matrix.cells) do
    if cell_data.fleet_identity == 1 then
      commander_data = GameGlobalData:GetFleetInfo(1)
      commander_data.active_spell = cell_data.fleet_spell
      break
    end
  end
  GameGlobalData:RefreshData("matrix")
end
GameGlobalData._achievementNotifyStack = {}
function GameGlobalData._AchievementCompleteNotifyHandler(content)
  if IPlatformExt.isAchievement_support() then
    local AchievementPlatformTable = AchievementPlatformMap[content.id] or {}
    local AchievementPlatformID = AchievementPlatformTable[IPlatformExt.getConfigValue("PlatformName")] or "fucking_id_" .. v.id
    IPlatformExt.unlock_Achievement(AchievementPlatformID)
  end
  table.insert(GameGlobalData._achievementNotifyStack, content)
end
function GameGlobalData:CheckAchievementDisplay()
  if #GameGlobalData._achievementNotifyStack > 0 then
    local achievementNotifyInfo = GameGlobalData._achievementNotifyStack[1]
    table.remove(GameGlobalData._achievementNotifyStack, 1)
    local GameUIAchievement = LuaObjectManager:GetLuaObject("GameUIAchievement")
    local achievement_name = GameUIAchievement:GetAchievementTitle(achievementNotifyInfo.id)
    local text_info = GameLoader:GetGameText("LC_MENU_ACHIEVEMENT_COMPLETE_NOTIFY")
    text_info = text_info .. achievement_name
    local GameUIPrestigeRankUp = LuaObjectManager:GetLuaObject("GameUIPrestigeRankUp")
    GameUIPrestigeRankUp:ShowAchievement(achievementNotifyInfo)
  end
end
function GameGlobalData:CheckNeedShowGotoStore()
end
function GameGlobalData.OpenUrl()
  local appStoreUrl = ext.QueryAppConfig("AppStoreUrl")
  ext.http.openURL(appStoreUrl)
end
function GameGlobalData.OpenAccountL20Url()
  local accountLevel20Url = ext.QueryAppConfig("Account_Level20_Url")
  ext.http.openURL(accountLevel20Url)
end
function GameGlobalData.RefreshTask(content)
  DebugOut("GameGlobalData.RefreshTask")
  GameGlobalData.GlobalData.task_statistic = content
  GameGlobalData:RefreshData("task_statistic")
  return true
end
function GameGlobalData.RefreshTodayTask(content)
  DebugOut("GameGlobalData.RefreshTodayTask")
  GameGlobalData.GlobalData.today_task_statistic = content
  GameGlobalData:RefreshData("today_task_statistic")
  return true
end
function GameGlobalData.RefreshUnreadMail(content)
  local key_refresh = "unmailinfo"
  local mail_info = GameGlobalData.GlobalData[key_refresh]
  mail_info.count = content.count
  GameGlobalData:RefreshData("unmailinfo")
end
function GameGlobalData.RefreshVip(content)
  DebugOut("GameGlobalData.RefreshVip")
  DebugTable(content)
  GameGlobalData.GlobalData.vipinfo = content
  GameGlobalData:RefreshData("vipinfo")
end
function GameGlobalData.RefreshNewsAndAct(content)
  DebugNews("RefreshNewsAndAct")
  DebugTable(content)
  GameGlobalData.GlobalData.notice_and_act = content
  GameGlobalData:RefreshData("notice_and_act")
end
function GameGlobalData:GetConfigValueFromContent(content, key)
  for _, v in ipairs(content.configs) do
    if v.config_attr == key then
      return v.config_value
    end
  end
  return nil
end
function GameGlobalData:UpdateGameConfig(config_key, config_value)
  if not GameGlobalData.GlobalData.configs then
    GameGlobalData.GlobalData.configs = {}
  end
  GameGlobalData.GlobalData.configs[config_key] = config_value
end
function GameGlobalData:GetGameConfig(config_key)
  if GameGlobalData.GlobalData.configs then
    return GameGlobalData.GlobalData.configs[config_key]
  end
  return nil
end
function GameGlobalData:GetModuleStatus(module_name)
  if not GameGlobalData.GlobalData.modules_status or not GameGlobalData.GlobalData.modules_status.modules then
    return nil
  end
  for _, data in ipairs(GameGlobalData.GlobalData.modules_status.modules) do
    if data.name == module_name then
      return data.status
    end
  end
  assert(false)
  return nil
end
function GameGlobalData.RefreshMenuStatus(content)
  DebugOut("module_status_ntf")
  GameGlobalData.GlobalData.modules_status = content
  GameGlobalData:RefreshData("modules_status")
end
function GameGlobalData:isIDAuth()
  DebugOut("fortp:")
  DebugTable(GameGlobalData.GlobalData.id_status)
  if GameGlobalData.GlobalData.id_status ~= nil and GameGlobalData.GlobalData.id_status.isAuth == true then
    return true
  else
    return false
  end
end
function GameGlobalData:GetIDConfirmdata()
  return GameGlobalData.GlobalData.id_status
end
function GameGlobalData.UpdateIdStatus(content)
  GameGlobalData.GlobalData.id_status = content
  if GameUtils:IsChinese() and not GameUtils:isOlderThan18(content.id) then
    local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
    GameUIGlobalScreen:ShowMessageBox(1, "\233\128\154\231\159\165", GameLoader:GetGameText("LC_MENU_POM_tips_login_fail"), function()
      GameUtils:RestartGame(200)
    end)
  end
  if GameUtils:IsChinese() and GameGlobalData.GlobalData.id_status.isAuth then
    local curloginInfo = GameUtils:GetLoginInfo()
    if GameSettingData and GameSettingData.PassportTable and curloginInfo then
      for _, AccountLocalInfo in ipairs(GameSettingData.PassportTable) do
        local localAccType = AccountLocalInfo.accType or GameUtils.AccountType.mail
        local loginAccType = curloginInfo.AccountInfo.accType or GameUtils.AccountType.mail
        if curloginInfo.AccountInfo and curloginInfo.AccountInfo.passport == AccountLocalInfo.passport and loginAccType == localAccType and (AccountLocalInfo.ChinaIDAuth == nil or AccountLocalInfo.ChinaIDAuth == false) then
          AccountLocalInfo.ChinaIDAuth = true
          GameUtils:SaveSettingData()
        end
      end
    end
  end
end
function GameGlobalData.mulMatrixBlankChangedNtf(content)
  DebugOut("mulMatrixBlankChangedNtf", content.blank)
  GameGlobalData.GlobalData.maxAvailableMatrix = content.blank
  GameGlobalData:RefreshData("maxAvailableMatrix")
end
function GameGlobalData.UserPVPAckCallback(content)
  local user_info = GameGlobalData:GetUserInfo()
  DebugOutPutTable(content, "userChallengeAck")
end
function GameGlobalData.AttributesChangeCallback(content)
  for _, v_attr in ipairs(content.attributes) do
    for index_fleet, v_fleet in ipairs(GameGlobalData.GlobalData.fleetinfo.fleets) do
      v_fleet[v_attr.key] = v_fleet[v_attr.key] + v_attr.value
    end
  end
end
function GameGlobalData.PrestigeRankUpNotifyCallback(content)
  local GameUIPrestigeRankUp = LuaObjectManager:GetLuaObject("GameUIPrestigeRankUp")
  GameUIPrestigeRankUp:PushRankUpData(content.complete_prestige)
end
function GameGlobalData.PlayerWdcRankUpCallback(content)
  local GameUIPrestigeRankUp = LuaObjectManager:GetLuaObject("GameUIPrestigeRankUp")
  GameUIPrestigeRankUp:PushWdcPlayerRankUpdate(content)
end
function GameGlobalData.PlayerTlcRankUpCallback(content)
  local GameUIPrestigeRankUp = LuaObjectManager:GetLuaObject("GameUIPrestigeRankUp")
  GameUIPrestigeRankUp:PushTlcPlayerRankUpdate(content)
end
function GameGlobalData.MineCompleteNofity(content)
  DebugOut("GameGlobalData.MineCompleteNofity ")
  local info_title = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
  local info_content = GameLoader:GetGameText("LC_MENU_MINING_REWARD_CHAR")
  local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
  local prestige = 0
  local money = 0
  local kenergy = 0
  local moneyStr = ""
  local prestigeStr = ""
  local kenergyStr = ""
  local extraStr = ""
  for i = 1, #content.basic_award do
    if content.basic_award[i].item_type == "money" then
      money = content.basic_award[i].number
      moneyStr = GameHelper:GetAwardText(content.basic_award[i].item_type, content.basic_award[i].number, content.basic_award[i].no)
    elseif content.basic_award[i].item_type == "prestige" then
      prestige = content.basic_award[i].number
      prestigeStr = GameHelper:GetAwardText(content.basic_award[i].item_type, content.basic_award[i].number, content.basic_award[i].no)
    elseif content.basic_award[i].item_type == "kenergy" then
      kenergy = content.basic_award[i].number
      kenergyStr = GameHelper:GetAwardText(content.basic_award[i].item_type, content.basic_award[i].number, content.basic_award[i].no)
    else
      extraStr = extraStr .. " " .. GameHelper:GetAwardText(content.basic_award[i].item_type, content.basic_award[i].number, content.basic_award[i].no)
    end
  end
  if money > 0 then
    info_content = info_content .. " " .. moneyStr
  end
  if prestige > 0 then
    info_content = info_content .. " " .. prestigeStr
  end
  if kenergy > 0 then
    info_content = info_content .. " " .. kenergyStr
  end
  if content.extra_techniques ~= 0 or content.extra_credit ~= 0 then
    info_content = info_content .. "\n" .. GameLoader:GetGameText("LC_MENU_MINING_ADDITIONAL_REWARD_CHAR")
    if content.extra_technique ~= 0 then
      local extra_info = GameLoader:GetGameText("LC_MENU_MINING_ADDITIONAL_REWARD_TECH_CHAR")
      extra_info = string.format(extra_info, content.extra_techniques)
      info_content = info_content .. " " .. extra_info
    end
    if content.extra_credit ~= 0 then
      local extra_info = GameLoader:GetGameText("LC_MENU_MINING_ADDITIONAL_REWARD_CREDIT_CHAR")
      extra_info = string.format(extra_info, content.extra_credit)
      info_content = info_content .. " " .. extra_info
    end
    info_content = info_content .. " " .. extraStr
  else
    info_content = info_content .. "\n" .. GameLoader:GetGameText("LC_MENU_MINING_ADDITIONAL_REWARD_CHAR")
    info_content = info_content .. " " .. extraStr
  end
  local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
  GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.OK)
  GameUIMessageDialog:Display(info_title, info_content)
  local gameObjectMineMap = LuaObjectManager:GetLuaObject("GameObjectMineMap")
  gameObjectMineMap.miningComplete()
end
function GameGlobalData:CanShowGalaxyButton()
  if GameStateManager:GetCurrentGameState() == GameStateManager.GameStateMainPlanet then
    return true
  else
    return false
  end
end
function GameGlobalData:BattleCanQuit()
  do return true end
  if self.GlobalData.progress then
    local progress_info = self.GlobalData.progress
    return progress_info.act > 1 or 1 < progress_info.chapter
  else
    return false
  end
end
function GameGlobalData.RefreshTimeNotifyHandler(content)
  if not GameGlobalData.GlobalData.RefreshTimes then
    GameGlobalData.GlobalData.RefreshTimes = {}
  end
  if not content.flag then
    content.time_type = "wd_supply_refresh_time"
  else
    content.time_type = "battle_supply_refresh_time"
  end
  DebugOut("refresh cd time")
  DebugTable(content)
  local oldData = false
  for _, refreshTimeData in ipairs(GameGlobalData.GlobalData.RefreshTimes) do
    if refreshTimeData.time_type == content.time_type then
      oldData = refreshTimeData
      break
    end
  end
  if oldData then
    if content.flag then
      oldData.next_time = content.next_time
    else
      oldData.targetTime = nil
      oldData.wd_next_time = content.wd_next_time
    end
  else
    table.insert(GameGlobalData.GlobalData.RefreshTimes, content)
  end
  GameGlobalData:RefreshData(content.time_type)
end
function GameGlobalData.ArcaneInfoNotifyHandler(content)
  if content.refresh_time then
    content.refresh_time = content.refresh_time * 1000
  end
  local GameStateArcane = GameStateManager.GameStateArcane
  GameStateArcane:SetArcaneInfo(content)
  GameGlobalData:RefreshData("arcane_info")
end
function GameGlobalData:GetRefreshTime(timeType)
  if GameGlobalData.GlobalData.RefreshTimes then
    for _, refreshTimeData in ipairs(GameGlobalData.GlobalData.RefreshTimes) do
      if refreshTimeData.time_type == timeType then
        return refreshTimeData
      end
    end
  end
  return nil
end
function GameGlobalData:FTPVersion()
  return false
end
function GameGlobalData.UpdateQuestState(content)
  GameGlobalData.GlobalData.user_quest = content
  GameGlobalData:RefreshData("user_quest")
end
GameGlobalData.MemberPositionType = {}
GameGlobalData.MemberPositionType.normal = 0
GameGlobalData.MemberPositionType.vice_chairman = 1
GameGlobalData.MemberPositionType.chairman = 2
function GameGlobalData:MemberPositionName(position)
  if position == self.MemberPositionType.normal then
    return ...
  elseif position == self.MemberPositionType.vice_chairman then
    return ...
  elseif position == self.MemberPositionType.chairman then
    return ...
  end
  return nil
end
function GameGlobalData:GetCommanderTypeWithIdentity(commanderIdentity)
  if commanderIdentity >= 2 and commanderIdentity <= 4 then
    return "TYPE_1"
  elseif commanderIdentity >= 200 and commanderIdentity <= 300 then
    return "TYPE_3"
  elseif commanderIdentity >= 500 and commanderIdentity <= 600 then
    return "TYPE_4"
  else
    return "TYPE_2"
  end
end
function GameGlobalData:UpdateItemCountGameData(content)
  if content == nil or content.items == nil or #content.items == 0 then
    return
  end
  DebugTable(content.items)
  for j = 1, #content.items do
    for i, v in ipairs(GameGlobalData.GlobalData.item_count.items) do
      DebugOut("UpdateItemCountGameData------>")
      DebugTable(v)
      DebugTable(content.items[j])
      if v.item_id == content.items[j].item_id then
        table.remove(GameGlobalData.GlobalData.item_count.items, i)
        break
      end
    end
    table.insert(GameGlobalData.GlobalData.item_count.items, content.items[j])
  end
end
function GameGlobalData.ItemCountChangeHandler(content)
  DebugOut("GameGameGlobalData.ItemCountChangeHandler")
  DebugTable(content)
  if GameGlobalData.GlobalData.item_count == nil then
    DebugOut("new")
    GameGlobalData:UpdateGameData("item_count", content)
  else
    DebugOut("modify")
    GameGlobalData:UpdateItemCountGameData(content)
  end
  local resource = GameGlobalData:GetData("item_count")
  DebugTable(resource)
  GameGlobalData:RefreshData("item_count")
end
function GameGlobalData:GetItemCount(itemType)
  if GameGlobalData.GlobalData.item_count then
    for i, v in ipairs(GameGlobalData.GlobalData.item_count.items) do
      if v.item_id == itemType then
        return v.item_no
      end
    end
  end
  return 0
end
function GameGlobalData.UpdateCuponAndClassifyDate(content)
  GameGlobalData.GlobalData.CuponChoices = content.store_coupons
  GameGlobalData.GlobalData.ClassifyChoices = content.equip_enhant_items
end
function GameGlobalData:GetCuponChoices()
  local ret = {}
  DebugOut("GetCuponChoices")
  local GameItemBag = LuaObjectManager:GetLuaObject("GameItemBag")
  DebugTable(GameGlobalData.GlobalData.CuponChoices)
  if GameGlobalData.GlobalData.CuponChoices == nil then
    return nil
  end
  if GameItemBag == nil or GameItemBag.itemInBag == nil then
    return nil
  end
  for k, v in ipairs(GameGlobalData.GlobalData.CuponChoices) do
    for kItem, vItem in pairs(GameItemBag.itemInBag) do
      if vItem.item_type == v.item_id and vItem.cnt >= 1 then
        local item = {}
        item.item_type = "item"
        item.number = v.item_id
        item.no = vItem.cnt
        item.level = 0
        item.discount = v.discount
        table.insert(ret, item)
      end
    end
  end
  return ret
end
function GameGlobalData:GetClassifyChoices()
  local ret = {}
  if GameGlobalData.GlobalData.ClassifyChoices == nil then
    return ret
  end
  for k, v in ipairs(GameGlobalData.GlobalData.ClassifyChoices) do
    local item = {}
    item.item_type = "item"
    item.number = v
    item.no = 1
    item.level = 0
    table.insert(ret, item)
  end
  return ret
end
function GameGlobalData.AllianceResourceChangeHandler(content)
  DebugOut("GameGlobalData.AllianceResourceChangeHandler(content)t)")
  DebugTable(content)
  if content == nil or content.resource == nil then
    return
  end
  if GameGlobalData.GlobalData.alliance_resource == nil then
    GameGlobalData.GlobalData.alliance_resource = {}
    GameGlobalData.GlobalData.alliance_resource.resource = {}
  end
  GameGlobalData.GlobalData.alliance_resource.resource = LuaUtils:table_rcopy(content.resource)
  GameGlobalData:RefreshData("alliance_resource")
end
function GameGlobalData.ChangeMaxLevelNtf(content)
  DebugOutPutTable(content, "ChangeMaxLevelNtf")
  GameGlobalData.max_level = content.max
  if content.max_recruit then
    GameGlobalData.max_recruit = content.max_recruit
  end
end
function GameGlobalData.AdjutantMaxCountNtfHandler(content)
  DebugOut("AdjutantMaxCountNtfHandler")
  DebugTable(content)
  GameGlobalData:UpdateGameData("adjutant_max_count", content.num)
  GameGlobalData:UpdateGameData("adjutant_unlock_level", content.level)
end
function GameGlobalData.RemodelInfoNtf(content)
  DebugOut("RemodelInfoNtf")
  DebugTable(content.remodel_info_ntf)
  GameGlobalData.GlobalData.remodel_info = {}
  GameGlobalData:UpdateGameData("remodel_info", content.remodel_info_ntf)
  GameGlobalData:RefreshData("remodel_info")
end
function GameGlobalData.RemodelHelpInfoNtf(content)
  DebugOut("RemodelHelpInfoNtf")
  DebugTable(content)
  GameGlobalData.GlobalData.remodel_help = {}
  GameGlobalData:UpdateGameData("remodel_help", content.other_info)
  DebugTable(GameGlobalData:GetData("remodel_help"))
  GameGlobalData:RefreshData("remodel_help")
end
function GameGlobalData.PushButtonInfoNtf(content)
  DebugOut("PushButtonInfoNtf:")
  DebugTable(content)
  GameGlobalData:UpdateGameData("push_button_info", content.ntf)
  GameGlobalData:RefreshData("push_button_info")
end
function GameGlobalData.WVEPrimeRankInfoNtf(content)
  DebugOut("WVEPrimeRankInfoNtf:")
  DebugTable(content)
  GameGlobalData:UpdateGameData("prime_rank_info", content.ranks)
  GameGlobalData:RefreshData("prime_rank_info")
end
function GameGlobalData.FeatureSwitchNtf(content)
  GameGlobalData.FeatureSwitch = content.ltv_ajusts
  GameUtils:printByAndroid("GameGlobalData.FeatureSwitch = ")
  GameUtils:DebugOutTableInAndroid(GameGlobalData.FeatureSwitch)
end
function GameGlobalData:GetFeatureSwitchState(featureType)
  for i, v in ipairs(GameGlobalData.FeatureSwitch) do
    if v and v.type == featureType then
      if v.status == "true" then
        return true
      else
        return false
      end
    end
  end
  return false
end
function GameGlobalData.OnButtonsStatusChange(content)
  DebugOut("GameGlobalData.OnButtonsStatusChange")
  GameGlobalData.ButtonsStatus = content.buttons
  GameGlobalData.showIndex = "1:2:3:4:5:6:7:8:9"
  GameGlobalData:RefreshData("buttons_change")
end
function GameGlobalData.OnStarCraftNtf(content)
  GameGlobalData.starCraftData = content
  GameGlobalData.starCraftData.curLocalTime = os.time()
  local GameUIStarCraftEnter = LuaObjectManager:GetLuaObject("GameUIStarCraftEnter")
  if GameStateManager:GetCurrentGameState():IsObjectInState(GameUIStarCraftEnter) then
    GameUIStarCraftEnter:UpdateData()
  end
  local GameObjectMainPlanet = LuaObjectManager:GetLuaObject("GameObjectMainPlanet")
  if GameStateManager:GetCurrentGameState():IsObjectInState(GameObjectMainPlanet) then
    GameObjectMainPlanet:UpdateWorldStatus()
  end
  if GameStateManager:GetCurrentGameState() == GameStateManager.GameStateStarCraft and GameGlobalData.starCraftData.open == false and GameGlobalData.starCraftData.type == 2 then
    local GameUIStarCraftMap = LuaObjectManager:GetLuaObject("GameUIStarCraftMap")
    GameUIStarCraftMap:OnFSCommand("closeMap", "")
    local GameTip = LuaObjectManager:GetLuaObject("GameTip")
    local endText = GameLoader:GetGameText("LC_MENU_TC_ALREADY_ENDED")
    GameTip:Show(endText)
    DebugOut("asjkdakjsdj")
  end
end
function GameGlobalData:GetRedPointStatByKey(key)
  if GameGlobalData.redPointStates then
    for k, v in pairs(GameGlobalData.redPointStates.states) do
      if v.key == key then
        return v.value
      end
    end
  end
  return 0
end
function GameGlobalData:SetRedPointStatByKey(key, value)
  if GameGlobalData.redPointStates then
    for k, v in pairs(GameGlobalData.redPointStates.states) do
      if v.key == key then
        v.value = value
        return
      end
    end
  end
  table.insert(GameGlobalData.redPointStates.states, {key = key, value = value})
end
function GameGlobalData.OnRedPointtNtf(content)
  DebugOut("OnRedPointtNtf")
  DebugTable(content)
  DebugOut("OnRedPointtNtf_2")
  DebugTable(GameGlobalData.redPointStates)
  if GameGlobalData.redPointStates == nil then
    GameGlobalData.redPointStates = content
  else
    for k, v in pairs(content.states) do
      GameGlobalData:SetRedPointStatByKey(v.key, v.value)
    end
  end
  DebugOut("OnRedPointtNtf_3")
  DebugTable(GameGlobalData.redPointStates)
  local GameFleetNewEnhance = LuaObjectManager:GetLuaObject("GameFleetNewEnhance")
  local GameUIBarRight = LuaObjectManager:GetLuaObject("GameUIBarRight")
  local GameObjectPlayerMatrix = LuaObjectManager:GetLuaObject("GameObjectPlayerMatrix")
  local GameUIBarLeft = LuaObjectManager:GetLuaObject("GameUIBarLeft")
  local GameUIFairArenaEnter = LuaObjectManager:GetLuaObject("GameUIFairArenaEnter")
  local GameUILab = LuaObjectManager:GetLuaObject("GameUILab")
  local GameObjectTacticsCenter = LuaObjectManager:GetLuaObject("GameObjectTacticsCenter")
  local GameUIStarSystemPort = LuaObjectManager:GetLuaObject("GameUIStarSystemPort")
  GameUIBarRight:RefreshRedPoint()
  GameObjectPlayerMatrix:RefreshRedPoint()
  GameFleetNewEnhance:RefreshRedPoint()
  GameUIBarLeft:RefreshRedPoint()
  GameUIFairArenaEnter:RefreshRedPoint()
  GameUILab:RefreshRedPoint()
  GameObjectTacticsCenter:RefreshRedPoint()
  GameUIStarSystemPort:RefreshRedPoint()
end
function GameGlobalData.FTENtf(content)
  GameGlobalData.FakeBattleIndex = content.id
end
function GameGlobalData.OnMedalListNTF(content)
  local GameFleetEquipment = LuaObjectManager:GetLuaObject("GameFleetEquipment")
  GameFleetEquipment.xunzhang:OnMedalListNTF(content)
  local GameUIStarSystemPort = LuaObjectManager:GetLuaObject("GameUIStarSystemPort")
  GameUIStarSystemPort:OnMedalListNTF(content)
end
function GameGlobalData.OnAchiRedNTF(content)
  DebugOut("OnAchiRedNTF")
  local GameUIStarSystemHandbook = LuaObjectManager:GetLuaObject("GameUIStarSystemHandbook")
  GameUIStarSystemHandbook:OnAchiRedNTF(content)
end
function GameGlobalData.OnHonorCollectionNTF(content)
  DebugOut("OnHonorCollectionNTF")
  local GameUIStarSystemHandbook = LuaObjectManager:GetLuaObject("GameUIStarSystemHandbook")
  GameUIStarSystemHandbook:OnHonorCollectionNTF(content)
end
function GameGlobalData.Ntf_exchange_regulation(content)
  GameGlobalData.CostCreditTable = {}
  GameGlobalData.minCostCredit = 6
  for k, v in pairs(content.regulation) do
    assert(tonumber(v.str_value), v.str_value)
    GameGlobalData.CostCreditTable[v.str_key] = tonumber(v.str_value)
    if v.str_key == "min_need" then
      GameGlobalData.minCostCredit = tonumber(v.str_value)
    end
  end
end
function GameGlobalData.GetCostCredit_a_b(gameitem)
  local k
  if gameitem.item_type == "item" then
    k = tostring(gameitem.number)
  else
    k = gameitem.item_type
  end
  assert(GameGlobalData.CostCreditTable[k], "not find " .. k)
  return GameGlobalData.CostCreditTable[k], 0
end
function GameGlobalData.GetMinCostCredit()
  return GameGlobalData.minCostCredit
end
function GameGlobalData:GetFakeBattleIndex()
  return GameGlobalData.FakeBattleIndex
end
local tfuncs = {}
function tfuncs.memlow(strparam)
  local warn_info = {
    device_name = ext.GetDeviceName(),
    device_real_name = ext.GetDeviceName(),
    udid = ext.GetIOSOpenUdid(),
    player_id = GameGlobalData:GetData("userinfo") and GameGlobalData:GetData("userinfo").player_id or -1,
    level = GameGlobalData:GetData("levelinfo") and GameGlobalData:GetData("levelinfo").level or 0,
    info_type = "Warning",
    info_code = "memlow"
  }
  NetMessageMgr:SendMsg(NetAPIList.warn_req.Code, warn_info, nil, false, nil)
end
function tfuncs.touchevent(strparam)
  if strparam ~= nil then
    local tstr = LuaUtils:string_split(strparam, ";")
    if #tstr == 4 then
      GameStateManager:OnCommonTouchEvent(tonumber(tstr[1]), tonumber(tstr[2]), tonumber(tstr[3]), tonumber(tstr[4]))
    end
  end
end
function tfuncs.RecordFileOpenError(filename)
  DebugOut("RecordFileOpenError")
  if AutoUpdate.isTestGW then
    ext.showAlert("open file fail/error:" .. filename, 0)
  end
  local ext_info = {
    [1] = {
      str_key = "FileOpenFail",
      str_value = filename
    }
  }
  GameUtils:SavaUserStatistics(ext_info, true)
end
function GlobalFunc(strname, strparam)
  if tfuncs[strname] then
    tfuncs[strname](strparam)
  end
end
function GameGlobalData.OnFightRoundNtf(content)
  local GameObjectBattleReplay = LuaObjectManager:GetLuaObject("GameObjectBattleReplay")
  DebugOut("OnFightRoundNtf", #content.rounds, GameObjectBattleReplay.isGroupBattle)
  DebugTable(content)
  if not GameObjectBattleReplay.isGroupBattle then
    local curStat = GameStateManager:GetCurrentGameState()
    if not curStat.fightRoundData then
      curStat.fightRoundData = {}
    end
    for k, v in ipairs(content.rounds) do
      curStat.fightRoundData[v.round_cnt] = v
    end
  else
    for _, group in ipairs(GameObjectBattleReplay.GroupBattleReportArr) do
      for _, roundDetail in ipairs(content.rounds) do
        for k, v in ipairs(group.headers) do
          if #v.report.rounds ~= v.round_cnt then
            v.report.rounds[roundDetail.round_cnt] = roundDetail
            break
          end
        end
      end
    end
    DebugOutPutTable(GameObjectBattleReplay.GroupBattleReportArr, "OnFightRoundNtfGameObjectBattleReplay.GroupBattleReportArr")
  end
end
function GameGlobalData.OnFleetMedalRedPointNTF(content)
  GameGlobalData.FleetMedalRedPointData = content
  GameGlobalData:RefreshData("FleetMedalRedPoint")
end
function GameGlobalData:GetFleetMedalRedPointDataByFormation(formationId, fleetId)
  if not GameGlobalData.FleetMedalRedPointData then
    return nil
  end
  for k, v in ipairs(GameGlobalData.FleetMedalRedPointData.formations) do
    if v.formation_id == formationId and v.fleet_id == fleetId then
      return v.red_points
    end
  end
  return nil
end
function GameGlobalData.OnFleetMedalButtonRedPointNTF(content)
  GameGlobalData.FleetMedalButtonRedPointData = content
  GameGlobalData:RefreshData("FleetMedalButtonRedPoint")
end
function GameGlobalData.onFPS_switch_ntf(content)
  GameGlobalData.needFPSFte = content.status == 1
end
function GameGlobalData:GetFleetMedalButtonStat(formationId, fleetId)
  if not GameGlobalData.FleetMedalButtonRedPointData then
    return false
  end
  for k, v in ipairs(GameGlobalData.FleetMedalButtonRedPointData.btns) do
    if v.formation_id == formationId and v.fleet_id == fleetId then
      return v.red_point == 1
    end
  end
  return false
end
