require("GameTextEdit.tfl")
local GameUILargeMapAlliance = LuaObjectManager:GetLuaObject("GameUILargeMapAlliance")
local GameUILargeMap = LuaObjectManager:GetLuaObject("GameUILargeMap")
local GameUILargeMapUI = LuaObjectManager:GetLuaObject("GameUILargeMapUI")
local GameStateLargeMapAlliance = GameStateManager.GameStateLargeMapAlliance
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameUIPlayerView = LuaObjectManager:GetLuaObject("GameUIPlayerView")
local GameMail = LuaObjectManager:GetLuaObject("GameMail")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameStateBattlePlay = GameStateManager.GameStateBattlePlay
local GameObjectBattleReplay = LuaObjectManager:GetLuaObject("GameObjectBattleReplay")
local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
local GameUIChat = LuaObjectManager:GetLuaObject("GameUIChat")
local GameVip = LuaObjectManager:GetLuaObject("GameVip")
local GameWaiting = LuaObjectManager:GetLuaObject("GameWaiting")
local is_SetDissolveTimeZero = false
GameUILargeMapAlliance.ALLIANCE_LIST = 1
GameUILargeMapAlliance.ALLIANCE_FUNCTION = 2
GameUILargeMapAlliance.baseData = nil
GameUILargeMapAlliance.curShowType = GameUILargeMapAlliance.ALLIANCE_LIST
GameUILargeMapAlliance.curTab = 1
GameUILargeMapAlliance.nearAllianceData = nil
GameUILargeMapAlliance.allianceAffaisData = nil
GameUILargeMapAlliance.allianceArmyData = nil
GameUILargeMapAlliance.allianceArmySelectedData = nil
GameUILargeMapAlliance.allianceSeletcedOnline = false
GameUILargeMapAlliance.allianceInviteData = nil
GameUILargeMapAlliance.allianceInviteSearchData = nil
GameUILargeMapAlliance.allianceRecruitOptionData = nil
GameUILargeMapAlliance.curRatifyTab = 1
GameUILargeMapAlliance.allianceRatifyJoinApplyData = nil
GameUILargeMapAlliance.allianceRatifyDutyApplyData = nil
GameUILargeMapAlliance.allianceDipcomacyData = nil
GameUILargeMapAlliance.allianceDipcomacyDragList = {}
GameUILargeMapAlliance.allianceLogTab = 1
GameUILargeMapAlliance.allianceLogData = nil
function GameUILargeMapAlliance:ShowByType(baseData, tab)
  GameUILargeMapAlliance.baseData = baseData
  if GameUILargeMapAlliance.baseData.alliance_info.id == "" then
    GameUILargeMapAlliance.curShowType = GameUILargeMapAlliance.ALLIANCE_LIST
  else
    GameUILargeMapAlliance.curShowType = GameUILargeMapAlliance.ALLIANCE_FUNCTION
    GameUILargeMapAlliance.curTab = tab and tab or 1
  end
  GameStateManager:SetCurrentGameState(GameStateLargeMapAlliance)
end
function GameUILargeMapAlliance.PlayerExitAllianceNtf(content)
  local userInfo = GameGlobalData:GetUserInfo()
  if userInfo then
    for k, v in ipairs(content.players) do
      if v == userInfo.player_id then
        GameUIChat.largemapCountryNum = 0
        break
      end
    end
  end
  if GameUILargeMap.MyPlayerData then
    for k, v in ipairs(content.players) do
      if v == GameUILargeMap.MyPlayerData.player_id then
        GameUILargeMap:ExitAllianceChatChannel()
        if GameUILargeMap:GetFlashObject() then
          GameUILargeMap:ClearMassLines()
          GameUILargeMap:ClearBlockVision()
        end
        break
      end
    end
  end
  if GameUILargeMap.MyPlayerData then
    for k, v in ipairs(content.players) do
      if v == GameUILargeMap.MyPlayerData.player_id and (GameUILargeMapUI.curShowType == GameUILargeMapUI.TYPE_MASS_MEMBERS or GameUILargeMapUI.curShowType == GameUILargeMapUI.TYPE_MASS_FLEETS) then
        GameUILargeMapUI:Hide()
      end
    end
  end
  if GameUILargeMapAlliance:GetFlashObject() then
    for k, v in ipairs(content.players) do
      if v == GameUILargeMapAlliance.baseData.player_id then
        if GameUILargeMapAlliance.curShowType ~= GameUILargeMapAlliance.ALLIANCE_LIST then
          GameUILargeMapAlliance:GenerateExitAllianceData()
          GameUILargeMapAlliance.allianceArmyData = nil
          if GameUIMessageDialog.Usage == "Alliance" and GameUIMessageDialog:GetFlashObject() then
            GameUIMessageDialog:GetFlashObject():InvokeASCallback("_root", "hideMessageBox")
          end
        end
        return
      end
      if GameUILargeMapAlliance.allianceArmyData then
        for h, s in ipairs(GameUILargeMapAlliance.allianceArmyData.memberList) do
          if s.id == v then
            table.remove(GameUILargeMapAlliance.allianceArmyData.memberList, h)
            break
          end
        end
        GameUILargeMapAlliance.allianceArmyData.memberCount = #GameUILargeMapAlliance.allianceArmyData.memberList
      end
    end
    if GameUILargeMapAlliance.allianceArmyData and GameUILargeMapAlliance.curTab == 2 then
      GameUILargeMapAlliance:FilterAllianceMember(GameUILargeMapAlliance.allianceSeletcedOnline)
    end
  end
end
function GameUILargeMapAlliance:RefreshUI()
  if GameUILargeMapAlliance.curShowType == GameUILargeMapAlliance.ALLIANCE_FUNCTION and GameUILargeMapAlliance.curTab == 2 and GameUILargeMapAlliance.allianceArmyData then
    GameUILargeMapAlliance:ReqAllianceArmyData()
  end
end
function GameUILargeMapAlliance:OnInitGame()
end
function GameUILargeMapAlliance:OnAddToGameState()
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  local data = {}
  data.AffairText = GameLoader:GetGameText("LC_MENU_MAP_ARMY_INTERNAL_AFFAIRS")
  self:GetFlashObject():InvokeASCallback("_root", "InitFlashObject", data)
  self:GetFlashObject():InvokeASCallback("_root", "SetShowType", GameUILargeMapAlliance.curShowType)
  local tabStatus = {}
  tabStatus.affais_enable = true
  tabStatus.army_enable = true
  tabStatus.diplomacy_enable = false
  tabStatus.log_enable = false
  tabStatus.smallmap_enable = false
  self:GetFlashObject():InvokeASCallback("_root", "SetTabStatus", tabStatus)
  self:GetFlashObject():InvokeASCallback("_root", "SetTab", GameUILargeMapAlliance.curTab)
  GameUILargeMapAlliance:ReqAllianceData()
end
function GameUILargeMapAlliance:OnEraseFromGameState()
  DebugOut("GameUILargeMapAlliance:OnEraseFromGameState")
  self:UnloadFlashObject()
  GameUILargeMapAlliance.curRatifyTab = 1
  GameUILargeMapAlliance.baseData = nil
  GameUILargeMapAlliance.nearAllianceData = nil
  GameUILargeMapAlliance.allianceAffaisData = nil
  GameUILargeMapAlliance.allianceArmyData = nil
  GameUILargeMapAlliance.allianceArmySelectedData = nil
  GameUILargeMapAlliance.allianceInviteData = nil
  GameUILargeMapAlliance.allianceInviteSearchData = nil
  GameUILargeMapAlliance.allianceRecruitOptionData = nil
  GameUILargeMapAlliance.allianceRatifyJoinApplyData = nil
  GameUILargeMapAlliance.allianceRatifyDutyApplyData = nil
  GameUILargeMapAlliance.allianceDipcomacyData = nil
  GameUILargeMapAlliance.allianceLogTab = 1
end
function GameUILargeMapAlliance:ReqAllianceData()
  if GameUILargeMapAlliance.curShowType == GameUILargeMapAlliance.ALLIANCE_LIST then
    self:GetFlashObject():InvokeASCallback("_root", "ShowFoundAll")
    GameUILargeMapAlliance:ReqAllianceListData()
  else
    self:GetFlashObject():InvokeASCallback("_root", "ShowFunctionAll")
    if GameUILargeMapAlliance.curTab == 1 then
      GameUILargeMapAlliance:ReqAllianceAffaisData()
    elseif GameUILargeMapAlliance.curTab == 2 then
      GameUILargeMapAlliance:ReqAllianceArmyData()
    elseif GameUILargeMapAlliance.curTab == 3 then
      GameUILargeMapAlliance:ReqAllianceDiplomacyData()
    elseif GameUILargeMapAlliance.curTab == 4 then
      GameUILargeMapAlliance:ReqAllianceLogData()
    elseif GameUILargeMapAlliance.curTab == 5 then
      GameUILargeMapAlliance:ReqSmallMapData()
    end
  end
end
function GameUILargeMapAlliance:ReqAllianceListData()
  NetMessageMgr:SendMsg(NetAPIList.large_map_near_alliance_req.Code, nil, GameUILargeMapAlliance.LargeMapAllianceListCallBack, true, nil)
end
function GameUILargeMapAlliance.LargeMapAllianceListCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.large_map_near_alliance_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.large_map_near_alliance_ack.Code then
    GameUILargeMapAlliance:GenerateAllianceListData(content)
    GameUILargeMapAlliance:SetAllianceListUI(GameUILargeMapAlliance.nearAllianceData)
    return true
  end
  return false
end
function GameUILargeMapAlliance:GenerateAllianceListData(content)
  local data = {}
  data.createPrice = GameHelper:GetAwardCount(content.create_consume.item_type, content.create_consume.number, content.create_consume.no)
  data.applyAlliance = {}
  for k, v in ipairs(content.apply_alliances) do
    data.applyAlliance[v] = true
  end
  data.allianceList = {}
  for k, v in ipairs(content.alliance_list) do
    local item = {}
    item = v
    item.applyText = GameLoader:GetGameText("LC_MENU_MAP_ARMY_APPLICATION")
    item.haveApplyText = GameLoader:GetGameText("LC_MENU_MAP_ARMY_ALREADY_APPLY")
    item.haveApply = data.applyAlliance[v.id]
    table.insert(data.allianceList, item)
  end
  data.allianceCount = #data.allianceList
  GameUILargeMapAlliance.nearAllianceData = data
end
function GameUILargeMapAlliance:SetAllianceListUI(data)
  if GameUILargeMapAlliance:GetFlashObject() then
    GameUILargeMapAlliance:GetFlashObject():InvokeASCallback("_root", "SetSearchAllianceUI", data)
    GameWaiting:HideLoadingScreen()
    is_SetDissolveTimeZero = false
  end
end
function GameUILargeMapAlliance:ReqSearchAllianceData(str)
  local param = {}
  param.search_name = str
  NetMessageMgr:SendMsg(NetAPIList.large_map_search_alliance_req.Code, param, GameUILargeMapAlliance.LargeMapSearchAllianceCallBack, true, nil)
end
function GameUILargeMapAlliance.LargeMapSearchAllianceCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.large_map_search_alliance_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.large_map_search_alliance_ack.Code then
    GameUILargeMapAlliance:GenerateAllianceListData(content)
    GameUILargeMapAlliance:SetAllianceListUI(GameUILargeMapAlliance.nearAllianceData)
    return true
  end
  return false
end
function GameUILargeMapAlliance:OnFSCommand(cmd, arg)
  GameTextEdit:fscommand(self:GetFlashObject(), cmd, arg)
  if cmd == "close" then
    GameStateManager:SetCurrentGameState(GameStateLargeMapAlliance.m_preState)
  elseif cmd == "CreateAlliance" then
    local name = string.gsub(arg, " ", "")
    name = string.gsub(arg, "\n", "")
    if name == "" then
      return
    end
    GameUILargeMapAlliance:CreateAlliance(name)
  elseif cmd == "ExitAlliance" then
    if GameUILargeMapAlliance.baseData.alliance_rank == 0 then
      local function callback()
        GameUILargeMapAlliance:DissolveAlliance(arg)
      end
      local text_title = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
      GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.Caption)
      GameUIMessageDialog:SetLeftTextButton(GameLoader:GetGameText("LC_MENU_GET_RIGHT_VERSION_CANCEL"), nil, {})
      GameUIMessageDialog:SetRightTextButton(GameLoader:GetGameText("LC_MENU_TITLE_VERIFY"), callback, {})
      GameUIMessageDialog:Display(text_title, GameLoader:GetGameText("LC_MENU_MAP_CONFIRM_DISSOLUTION"))
      GameUIMessageDialog.Usage = "Alliance"
    else
      local function callback()
        GameUILargeMapAlliance:ExitAlliance(arg)
      end
      local text_title = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
      GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.Caption)
      GameUIMessageDialog:SetLeftTextButton(GameLoader:GetGameText("LC_MENU_GET_RIGHT_VERSION_CANCEL"), nil, {})
      GameUIMessageDialog:SetRightTextButton(GameLoader:GetGameText("LC_MENU_TITLE_VERIFY"), callback, {})
      GameUIMessageDialog:Display(text_title, GameLoader:GetGameText("LC_MENU_MAP_CONFIRM_QUIT_LEGION"))
      GameUIMessageDialog.Usage = "Alliance"
    end
  elseif cmd == "JoinAppleAlliance" then
    GameUILargeMapAlliance:JoinAlliance(arg)
  elseif cmd == "update_alliance_item" then
    local itemData = GameUILargeMapAlliance.nearAllianceData.allianceList[tonumber(arg)]
    if GameUILargeMapAlliance:GetFlashObject() then
      GameUILargeMapAlliance:GetFlashObject():InvokeASCallback("_root", "UpdateAllianceItem", tonumber(arg), itemData)
    end
  elseif cmd == "TouchTab" then
    if GameUILargeMapAlliance.curTab ~= tonumber(arg) then
      GameUILargeMapAlliance.curTab = tonumber(arg)
      self:GetFlashObject():InvokeASCallback("_root", "SetTab", GameUILargeMapAlliance.curTab)
      if GameUILargeMapAlliance.curTab == 1 then
        GameUILargeMapAlliance:ReqAllianceAffaisData()
      elseif GameUILargeMapAlliance.curTab == 2 then
        GameUILargeMapAlliance:ReqAllianceArmyData()
      elseif GameUILargeMapAlliance.curTab == 3 then
        GameUILargeMapAlliance:ReqAllianceDiplomacyData()
      elseif GameUILargeMapAlliance.curTab == 4 then
        GameUILargeMapAlliance:ReqAllianceLogData()
      elseif GameUILargeMapAlliance.curTab == 5 then
        GameUILargeMapAlliance:ReqSmallMapData()
      end
    end
  elseif cmd == "SelectMemberOnline" then
    GameUILargeMapAlliance.allianceSeletcedOnline = true
    GameUILargeMapAlliance:FilterAllianceMember(true)
  elseif cmd == "SelectMemberAll" then
    GameUILargeMapAlliance.allianceSeletcedOnline = false
    GameUILargeMapAlliance:FilterAllianceMember(false)
  elseif cmd == "update_member_item" then
    local itemData = GameUILargeMapAlliance.allianceArmyData.memberList[tonumber(arg)]
    if GameUILargeMapAlliance.allianceArmySelectedData then
      itemData = GameUILargeMapAlliance.allianceArmySelectedData.memberList[tonumber(arg)]
    end
    if GameUILargeMapAlliance:GetFlashObject() then
      GameUILargeMapAlliance:GetFlashObject():InvokeASCallback("_root", "UpdateAllianceMemberItem", tonumber(arg), itemData)
    end
  elseif cmd == "InviteAlliancePop" then
    GameUILargeMapAlliance:ReqAllianceInviteData()
  elseif cmd == "SearchPlayer" then
    local name = string.gsub(arg, " ", "")
    name = string.gsub(arg, "\n", "")
    if name == "" then
      GameUILargeMapAlliance.allianceInviteSearchData = nil
      GameUILargeMapAlliance:SetAllianceInvitePopUI(GameUILargeMapAlliance.allianceInviteData)
    else
      GameUILargeMapAlliance:ReqAllianceInviteSearchData(name)
    end
  elseif cmd == "InviteJoin" then
    local playerId = arg
    GameUILargeMapAlliance:ReqAllianceInvite(playerId)
  elseif cmd == "RecruitOptionPop" then
    local allianceID = GameUILargeMapAlliance.baseData.alliance_info.id
    GameUILargeMapAlliance:ReqAllianceRecruitOptionData(allianceID)
  elseif cmd == "CloseRecruitOption" then
    local needLevel, needForce, needExploit, enableRatify = unpack(LuaUtils:string_split(arg, ":"))
    enableRatify = enableRatify == "state_on" and true or false
    local allianceID = GameUILargeMapAlliance.baseData.alliance_info.id
    if needLevel == "" then
      needLevel = 1 or needLevel
    end
    if needForce == "" then
      needForce = 0 or needForce
    end
    if needExploit == "" then
      needExploit = 0 or needExploit
    end
    GameUILargeMapAlliance:ReqAllianceRecruitOptionUpdate(allianceID, enableRatify, needLevel, needForce, needExploit)
  elseif cmd == "RatifyPop" then
    GameUILargeMapAlliance:ReqRatifyData(GameUILargeMapAlliance.curRatifyTab)
    GameUILargeMapAlliance:HideAllianceArmyUI()
    GameUILargeMapAlliance:ShowDutyUIByTab(GameUILargeMapAlliance.curRatifyTab)
  elseif cmd == "TouchRatifyTab" then
    if GameUILargeMapAlliance.curRatifyTab ~= tonumber(arg) then
      GameUILargeMapAlliance.curRatifyTab = tonumber(arg)
    end
    self:GetFlashObject():InvokeASCallback("_root", "SetRatifyTab", GameUILargeMapAlliance.curRatifyTab)
    GameUILargeMapAlliance:ReqRatifyData(GameUILargeMapAlliance.curRatifyTab)
  elseif cmd == "update_player_item" then
    local itemData = GameUILargeMapAlliance.allianceInviteData.playerList[tonumber(arg)]
    if GameUILargeMapAlliance:GetFlashObject() then
      GameUILargeMapAlliance:GetFlashObject():InvokeASCallback("_root", "UpdateAllianceInviteMemberItem", tonumber(arg), itemData)
    end
  elseif cmd == "update_join_apply_item" then
    local itemData = GameUILargeMapAlliance.allianceRatifyJoinApplyData.joinApplyList[tonumber(arg)]
    if GameUILargeMapAlliance:GetFlashObject() then
      GameUILargeMapAlliance:GetFlashObject():InvokeASCallback("_root", "UpdateJoinApplyItem", tonumber(arg), itemData)
    end
  elseif cmd == "update_duty_apply_item" then
    local itemData = GameUILargeMapAlliance.allianceRatifyDutyApplyData.dutyApplyList[tonumber(arg)]
    if GameUILargeMapAlliance:GetFlashObject() then
      GameUILargeMapAlliance:GetFlashObject():InvokeASCallback("_root", "UpdateDutyApplyItem", tonumber(arg), itemData)
    end
  elseif cmd == "AgreeRatifyApply" then
    local playerId = arg
    GameUILargeMapAlliance:ReqRatifyHandle(playerId, GameUILargeMapAlliance.curRatifyTab, true)
  elseif cmd == "CancelRatifyApply" then
    local playerId = arg
    GameUILargeMapAlliance:ReqRatifyHandle(playerId, GameUILargeMapAlliance.curRatifyTab, false)
  elseif cmd == "AllAgreeRatify" then
    GameUILargeMapAlliance:ReqRatifyHandle("", GameUILargeMapAlliance.curRatifyTab, true)
  elseif cmd == "AllCancelRatify" then
    GameUILargeMapAlliance:ReqRatifyHandle("", GameUILargeMapAlliance.curRatifyTab, false)
  elseif cmd == "ApplyRolePop" then
    GameUILargeMapAlliance:SetApplyDutyPopUI()
    GameUILargeMapAlliance:ShowApplyDutyPop()
  elseif cmd == "ApplyDuty" then
    local playerId, rank = unpack(LuaUtils:string_split(arg, ":"))
    GameUILargeMapAlliance:ReqApplyDuty(playerId, applyRank)
  elseif cmd == "ShowPlayerDetail" then
    GameUILargeMapAlliance:ViewMemberInfo(arg)
  elseif cmd == "SendMail" then
    GameUILargeMapAlliance:SendMemberMail(arg)
  elseif cmd == "Promotion" then
    GameUILargeMapAlliance:ReqMemberPromotion(arg)
  elseif cmd == "Demotion" then
    GameUILargeMapAlliance:ReqAllianceDemotion(arg)
  elseif cmd == "Exchange" then
    GameUILargeMapAlliance:ReqAllianceExchange(arg)
  elseif cmd == "Expel" then
    GameUILargeMapAlliance:ReqAllianceExpel(arg)
  elseif cmd == "ShowFunPop" then
    local playerId, rank = unpack(LuaUtils:string_split(arg, ":"))
    GameUILargeMapAlliance:SetFunPop(playerId, tonumber(rank))
  elseif cmd == "RatifyBack" then
    GameUILargeMapAlliance:ReqAllianceArmyData()
  elseif cmd == "update_diplomacy_battome_item" then
    local itemData = GameUILargeMapAlliance.allianceDipcomacyData.batToMeList[tonumber(arg)]
    if GameUILargeMapAlliance:GetFlashObject() then
      GameUILargeMapAlliance:GetFlashObject():InvokeASCallback("_root", "UpdateDiplomacyBatToMe", tonumber(arg), itemData)
    end
  elseif cmd == "update_diplomacy_battoother_item" then
    local itemData = GameUILargeMapAlliance.allianceDipcomacyData.batToOtherList[tonumber(arg)]
    if GameUILargeMapAlliance:GetFlashObject() then
      GameUILargeMapAlliance:GetFlashObject():InvokeASCallback("_root", "UpdateDiplomacyBatToOther", tonumber(arg), itemData)
    end
  elseif cmd == "update_diplomacy_allarmy_item" then
    local itemData = GameUILargeMapAlliance.allianceDipcomacyData.allAllianceList[tonumber(arg)]
    if GameUILargeMapAlliance:GetFlashObject() then
      GameUILargeMapAlliance:GetFlashObject():InvokeASCallback("_root", "UpdateDiplomacyAllArmy", tonumber(arg), itemData)
    end
  elseif cmd == "DragDiplocamyItem" then
    local speed, itemType, allianceId, itemIndex = unpack(LuaUtils:string_split(arg, "|"))
    DebugOut("DragDiplocamyItem value:", speed, itemType, allianceId)
    if itemType == "allarmy" and tonumber(speed) < -2 and GameUILargeMapAlliance.baseData.alliance_rank <= 10 then
      GameUILargeMapAlliance:CreateDragItem("allarmy", "battoother", tonumber(itemIndex), allianceId)
      GameUILargeMapAlliance:RemoveDipcomacyItem("allarmy", allianceId)
      GameUILargeMapAlliance:UpdateDipcomacyList("allarmy")
    elseif itemType == "battoother" and tonumber(speed) > 2 and GameUILargeMapAlliance.baseData.alliance_rank <= 10 then
      GameUILargeMapAlliance:CreateDragItem("battoother", "allarmy", tonumber(itemIndex), allianceId)
      GameUILargeMapAlliance:RemoveDipcomacyItem("battoother", allianceId)
      GameUILargeMapAlliance:UpdateDipcomacyList("battoother")
    elseif itemType == "battome" and tonumber(speed) > 2 and GameUILargeMapAlliance.baseData.alliance_rank <= 10 then
      GameUILargeMapAlliance:CreateDragItem("battome", "battoother", tonumber(itemIndex), allianceId)
    end
  elseif cmd == "dragFinish" then
    GameUILargeMapAlliance:onDipcomacyDragFinish(tonumber(arg))
  elseif cmd == "ModifyNotice" then
    GameUILargeMapAlliance:onModifyNotice(arg)
  elseif cmd == "AdjustRate" then
    local rate, expand = unpack(LuaUtils:string_split(arg, "|"))
    GameUILargeMapAlliance:onAdjustRate(tonumber(rate), tonumber(expand))
  elseif cmd == "update_log_item" then
    local itemData = GameUILargeMapAlliance.allianceLogData.logList[tonumber(arg)]
    if GameUILargeMapAlliance:GetFlashObject() then
      GameUILargeMapAlliance:GetFlashObject():InvokeASCallback("_root", "UpdateLogItem", tonumber(arg), itemData)
    end
  elseif cmd == "TouchLogTab" then
    if GameUILargeMapAlliance.allianceLogTab ~= tonumber(arg) then
      GameUILargeMapAlliance.allianceLogTab = tonumber(arg)
      GameUILargeMapAlliance:ReqAllianceLogData()
    end
  elseif cmd == "PlayReport" then
    GameUILargeMapAlliance:onLogReport(arg)
  elseif cmd == "SearchAlliance" then
    if arg == "" then
      GameUILargeMapAlliance:ReqAllianceListData()
    else
      GameUILargeMapAlliance:ReqSearchAllianceData(arg)
    end
  end
end
function GameUILargeMapAlliance:CreateAlliance(name)
  local param = {}
  param.name = name
  NetMessageMgr:SendMsg(NetAPIList.large_map_create_alliance_req.Code, param, GameUILargeMapAlliance.LargeMapCreateAllianceCallBack, true, nil)
end
function GameUILargeMapAlliance.LargeMapCreateAllianceCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.large_map_create_alliance_req.Code then
    if content.code ~= 0 then
      GameVip:CheckIsNeedShowVip(content.code)
    end
    return true
  elseif msgType == NetAPIList.large_map_create_alliance_ack.Code then
    GameUILargeMapAlliance.curTab = 1
    if GameUILargeMapAlliance:GetFlashObject() then
      GameUILargeMapAlliance:GetFlashObject():InvokeASCallback("_root", "SetTab", GameUILargeMapAlliance.curTab)
    end
    GameUILargeMapAlliance:GenerateCreateAllianceData(content)
    GameUILargeMapAlliance:HideCreateAlliancePop()
    GameTip:Show(GameLoader:GetGameText("LC_MENU_MAP_CREATE_ALLIANCE_SUCCESS"))
    return true
  end
  return false
end
function GameUILargeMapAlliance:GenerateCreateAllianceData(content)
  GameUILargeMapAlliance.baseData.alliance_info = content
  GameUILargeMapAlliance.curShowType = GameUILargeMapAlliance.ALLIANCE_FUNCTION
  self:GetFlashObject():InvokeASCallback("_root", "SetShowType", GameUILargeMapAlliance.curShowType)
  GameUILargeMapAlliance:ReqAllianceData()
end
function GameUILargeMapAlliance:HideCreateAlliancePop()
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "HideCreateAlliancePop")
  end
end
function GameUILargeMapAlliance:JoinAlliance(id)
  local param = {}
  param.id = id
  NetMessageMgr:SendMsg(NetAPIList.large_map_join_alliance_req.Code, param, GameUILargeMapAlliance.LargeMapJoinAllianceCallBack, true, nil)
end
function GameUILargeMapAlliance.LargeMapJoinAllianceCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.large_map_join_alliance_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.large_map_join_alliance_ack.Code then
    GameUILargeMapAlliance.curTab = 1
    if GameUILargeMapAlliance:GetFlashObject() then
      GameUILargeMapAlliance:GetFlashObject():InvokeASCallback("_root", "SetTab", GameUILargeMapAlliance.curTab)
    end
    GameUILargeMapAlliance:GenerateJoinAllianceData(content)
    return true
  end
  return false
end
function GameUILargeMapAlliance:GenerateJoinAllianceData(content)
  if content.apply_status == 1 then
    GameUILargeMapAlliance.nearAllianceData.applyAlliance[content.alliance_info.id] = true
    for k, v in pairs(GameUILargeMapAlliance.nearAllianceData.allianceList) do
      if v.id == content.alliance_info.id then
        v.haveApply = true
        GameUILargeMapAlliance:OnFSCommand("update_alliance_item", k)
        return
      end
    end
  else
    GameUILargeMapAlliance.baseData.alliance_info = content.alliance_info
    GameUILargeMapAlliance.curShowType = GameUILargeMapAlliance.ALLIANCE_FUNCTION
    self:GetFlashObject():InvokeASCallback("_root", "SetShowType", GameUILargeMapAlliance.curShowType)
    GameUILargeMapAlliance:ReqAllianceData()
  end
end
function GameUILargeMapAlliance:ExitAlliance(id)
  local param = {}
  param.id = id
  NetMessageMgr:SendMsg(NetAPIList.large_map_exit_alliance_req.Code, param, GameUILargeMapAlliance.LargeMapExitAllianceCallBack, true, nil)
end
function GameUILargeMapAlliance.LargeMapExitAllianceCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.large_map_exit_alliance_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.large_map_exit_alliance_ack.Code then
    GameUILargeMapAlliance:GenerateExitAllianceData(content)
    return true
  end
  return false
end
function GameUILargeMapAlliance:DissolveAlliance(id)
  local param = {}
  param.id = id
  NetMessageMgr:SendMsg(NetAPIList.large_map_dissolve_alliance_req.Code, param, GameUILargeMapAlliance.LargeMapDissolveAllianceCallBack, true, nil)
end
function GameUILargeMapAlliance.LargeMapDissolveAllianceCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.large_map_dissolve_alliance_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.large_map_dissolve_alliance_ack.Code then
    GameUILargeMapAlliance:GenerateExitAllianceData(content)
    return true
  end
  return false
end
function GameUILargeMapAlliance:GenerateExitAllianceData(content)
  if is_SetDissolveTimeZero == false then
    GameWaiting:ShowLoadingScreen()
    is_SetDissolveTimeZero = true
  end
  GameUILargeMapAlliance.baseData.alliance_info.id = ""
  GameUILargeMapAlliance.baseData.alliance_info.name = ""
  GameUILargeMapAlliance.baseData.alliance_info.rate = 0
  GameUILargeMapAlliance.baseData.alliance_info.block_count = 0
  GameUILargeMapAlliance.baseData.alliance_info.people_count = 0
  GameUILargeMapAlliance.baseData.alliance_info.total_fleet = 0
  GameUILargeMapAlliance.baseData.alliance_info.online_fleet = 0
  GameUILargeMapAlliance.baseData.alliance_rank = 20
  GameUILargeMapAlliance.curShowType = GameUILargeMapAlliance.ALLIANCE_LIST
  self:GetFlashObject():InvokeASCallback("_root", "SetShowType", GameUILargeMapAlliance.curShowType)
  GameUILargeMapAlliance:ReqAllianceData()
end
function GameUILargeMapAlliance:UpdateAllAllianceList()
  local flashObj = GameUILargeMapAlliance:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "UpdateAllAllianceList")
  end
end
function GameUILargeMapAlliance:ReqAllianceAffaisData()
  NetMessageMgr:SendMsg(NetAPIList.large_map_alliance_affais_req.Code, nil, GameUILargeMapAlliance.LargeMapAllianceAffaisCallBack, true, nil)
end
function GameUILargeMapAlliance.LargeMapAllianceAffaisCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.large_map_alliance_affais_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.large_map_alliance_affais_ack.Code then
    GameUILargeMapAlliance:GenerateAllianceAffaisData(content)
    GameUILargeMapAlliance:SetAllianceAffaisUI(GameUILargeMapAlliance.allianceAffaisData)
    return true
  end
  return false
end
function GameUILargeMapAlliance:GenerateAllianceAffaisData(content)
  local data = {}
  data.allianceName = content.alliance_info.name
  if content.alliance_info.block_count == 0 then
    data.allianceName = data.allianceName
  end
  data.allianceNotice = content.alliance_notice
  data.myAllianceRank = GameUILargeMapAlliance.baseData.alliance_rank
  data.rate = content.alliance_info.rate
  data.expand = content.alliance_info.expend
  data.blockCount = content.alliance_info.block_count
  data.info1 = GameLoader:GetGameText("LC_MENU_MAP_ARMY_TAX") .. ":" .. content.alliance_info.rate .. "%"
  data.info1 = data.info1 .. "\n" .. GameLoader:GetGameText("LC_MENU_MAP_ARMY_PERMANENT_RESIDENTS") .. ":" .. content.alliance_info.people_count
  data.info1 = data.info1 .. "\n" .. GameLoader:GetGameText("LC_MENU_MAP_ARMY_TERRITORIAL_SCOPE") .. ":" .. content.alliance_info.block_count
  data.info2 = GameLoader:GetGameText("LC_MENU_MAP_ARMY_MILITARY_EXPENDITURE") .. ":" .. content.alliance_info.expend .. "%"
  data.info2 = data.info2 .. "\n" .. GameLoader:GetGameText("LC_MENU_MAP_ARMY_FLEET_NUMBER") .. ":" .. content.alliance_info.total_fleet
  data.info2 = data.info2 .. "\n" .. GameLoader:GetGameText("LC_MENU_MAP_ARMY_FORTRESS_PLANET_NUMBER") .. ":" .. content.alliance_info.core_star
  data.res = {}
  for k, v in pairs(content.alliance_res or {}) do
    local item = {}
    item.frame = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(v)
    item.count = GameHelper:GetAwardCount(v.item_type, v.number, v.no)
    data.res[#data.res + 1] = item
  end
  data.grantTime = content.grant_time
  data.grantBaseTime = os.time()
  data.grantTimeStr = GameUtils:formatTimeString(data.grantTime)
  data.grantTimeStr = ""
  data.enableRun = true
  data.dissolveTime = content.dissolve_time
  data.dissolveBaseTime = os.time()
  data.dissolveTimeStr = 0 < data.dissolveTime and GameUtils:formatTimeString(data.dissolveTime) or ""
  data.enableDissRun = true
  if 0 < data.blockCount then
    data.dissolveTimeStr = ""
  end
  GameUILargeMapAlliance.allianceAffaisData = data
end
function GameUILargeMapAlliance:SetAllianceAffaisUI(data)
  local flashObj = GameUILargeMapAlliance:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "SetAffaisUI", data)
    GameUILargeMapAlliance:ReqAllianceArmyData()
  end
end
function GameUILargeMapAlliance:onModifyNotice(str)
  local param = {}
  param.alliance_id = GameUILargeMapAlliance.baseData.alliance_info.id
  param.notice = str
  NetMessageMgr:SendMsg(NetAPIList.large_map_alliance_modify_notice_req.Code, param, GameUILargeMapAlliance.LargeMapAllianceAffaisNoticeCallBack, true, nil)
end
function GameUILargeMapAlliance.LargeMapAllianceAffaisNoticeCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.large_map_alliance_modify_notice_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.large_map_alliance_modify_notice_ack.Code then
    GameUILargeMapAlliance:GenerateAllianceAffaisData(content)
    GameUILargeMapAlliance:SetAllianceAffaisUI(GameUILargeMapAlliance.allianceAffaisData)
    return true
  end
  return false
end
function GameUILargeMapAlliance:onAdjustRate(rate, expend)
  local param = {}
  param.alliance_id = GameUILargeMapAlliance.baseData.alliance_info.id
  param.rate = rate
  param.expend = expend
  NetMessageMgr:SendMsg(NetAPIList.large_map_alliance_adjust_req.Code, param, GameUILargeMapAlliance.LargeMapAllianceAffaisAdjustCallBack, true, nil)
end
function GameUILargeMapAlliance.LargeMapAllianceAffaisAdjustCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.large_map_alliance_adjust_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.large_map_alliance_adjust_ack.Code then
    GameUILargeMapAlliance:GenerateAllianceAffaisData(content)
    GameUILargeMapAlliance:SetAllianceAffaisUI(GameUILargeMapAlliance.allianceAffaisData)
    return true
  end
  return false
end
function GameUILargeMapAlliance:HideAllianceArmyUI()
  local flashObj = GameUILargeMapAlliance:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "HideArmyUI")
  end
end
function GameUILargeMapAlliance:ReqAllianceArmyData()
  NetMessageMgr:SendMsg(NetAPIList.large_map_alliance_army_req.Code, nil, GameUILargeMapAlliance.LargeMapAllianceArmyCallBack, true, nil)
end
function GameUILargeMapAlliance.LargeMapAllianceArmyCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.large_map_alliance_army_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.large_map_alliance_army_ack.Code then
    GameUILargeMapAlliance:GenerateAllianceArmyData(content)
    GameUILargeMapAlliance:FilterAllianceMember(GameUILargeMapAlliance.allianceSeletcedOnline)
    return true
  end
  return false
end
function GameUILargeMapAlliance:GenerateAllianceArmyData(content)
  local data = {}
  data.allianceInfo = content.alliance_info
  data.memberCount = #content.member_list
  data.memberList = {}
  data.selfRank = GameUILargeMapAlliance.baseData.alliance_rank
  data.exitAllianceText = GameLoader:GetGameText("LC_MENU_MAP_QUIT_LEGION")
  data.dissolveAllianceText = GameLoader:GetGameText("LC_MENU_MAP_ARMY_DISSOLUTION_OF_CORPS")
  data.RecuitOptionText = GameLoader:GetGameText("LC_MENU_MAP_ARMY_RECRUITMENT_OPTIONS")
  data.ApplyListText = GameLoader:GetGameText("LC_MENU_MAP_ARMY_APPROVE_THE_LIST")
  data.AllMailText = GameLoader:GetGameText("LC_MENU_MAP_ARMY_ALL_MAIL")
  for k, v in ipairs(content.member_list or {}) do
    local item = {}
    item.id = v.id
    item.name = v.name
    item.rank = v.rank
    item.order = v.is_online and v.rank or 1000 + v.rank
    item.rankText = GameLoader:GetGameText("LC_MENU_MAP_ARMY_RANK_" .. v.rank)
    item.showFunc = GameUILargeMapAlliance.baseData.player_id ~= v.id
    item.level = v.level
    item.force = v.force
    item.forceText = GameUtils:formatNumber(v.force)
    item.lastLoginText = GameUtils:formatTimeStringAsAgoStyle(v.last_login)
    item.isOnline = v.is_online
    item.isOnlineText = GameLoader:GetGameText("LC_MENU_ALLIANCE_ONLINE_INFO")
    data.memberList[#data.memberList + 1] = item
  end
  GameUILargeMapAlliance.allianceArmyData = data
end
function GameUILargeMapAlliance:FilterAllianceMember(isOnline)
  local data = {}
  if isOnline then
    data.allianceInfo = GameUILargeMapAlliance.allianceArmyData.allianceInfo
    data.memberList = {}
    for k, v in ipairs(GameUILargeMapAlliance.allianceArmyData.memberList) do
      local item = v
      if isOnline and v.isOnline == isOnline then
        data.memberList[#data.memberList + 1] = item
      end
    end
    data.memberCount = #data.memberList
    GameUILargeMapAlliance.allianceArmySelectedData = data
  else
    data = GameUILargeMapAlliance.allianceArmyData
  end
  data.selfRank = GameUILargeMapAlliance.baseData.alliance_rank
  data.exitAllianceText = GameLoader:GetGameText("LC_MENU_MAP_QUIT_LEGION")
  data.dissolveAllianceText = GameLoader:GetGameText("LC_MENU_MAP_ARMY_DISSOLUTION_OF_CORPS")
  data.RecuitOptionText = GameLoader:GetGameText("LC_MENU_MAP_ARMY_RECRUITMENT_OPTIONS")
  data.ApplyListText = GameLoader:GetGameText("LC_MENU_MAP_ARMY_APPROVE_THE_LIST")
  data.AllMailText = GameLoader:GetGameText("LC_MENU_MAP_ARMY_ALL_MAIL")
  table.sort(data.memberList, function(a, b)
    return a.order < b.order
  end)
  GameUILargeMapAlliance.allianceArmySelectedData = data
  GameUILargeMapAlliance:SetAllianceArmyUI(data)
end
function GameUILargeMapAlliance:SetAllianceArmyUI(data)
  local flashObj = GameUILargeMapAlliance:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "SetArmyUI", data)
  end
end
function GameUILargeMapAlliance:ReqAllianceInviteData()
  NetMessageMgr:SendMsg(NetAPIList.large_map_alliance_invite_data_req.Code, nil, GameUILargeMapAlliance.LargeMapAllianceInviteDataCallBack, true, nil)
end
function GameUILargeMapAlliance.LargeMapAllianceInviteDataCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.large_map_alliance_invite_data_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.large_map_alliance_invite_data_ack.Code then
    GameUILargeMapAlliance:GenerateAllianceInviteData(content)
    GameUILargeMapAlliance:SetAllianceInvitePopUI(GameUILargeMapAlliance.allianceInviteData)
    GameUILargeMapAlliance:ShowInvitePop()
    return true
  end
  return false
end
function GameUILargeMapAlliance:GenerateAllianceInviteData(content)
  local data = {}
  data.allianceInfo = content.alliance_info
  data.searchStr = ""
  data.playerCount = #content.player_list
  data.playerList = {}
  for k, v in ipairs(content.player_list or {}) do
    local item = {}
    item = v
    item.forceText = GameUtils:formatNumber(v.force)
    item.inviteText = GameLoader:GetGameText("")
    item.enableInvite = true
    data.playerList[#data.playerList + 1] = item
  end
  GameUILargeMapAlliance.allianceInviteData = data
end
function GameUILargeMapAlliance:SetAllianceInvitePopUI(data)
  local flashObj = GameUILargeMapAlliance:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "SetInvitePopUI", data)
  end
end
function GameUILargeMapAlliance:ReqAllianceInviteSearchData(str)
  local param = {}
  param.search_name = str
  NetMessageMgr:SendMsg(NetAPIList.large_map_alliance_player_search_req.Code, param, GameUILargeMapAlliance.LargeMapAllianceInviteSearchCallBack, true, nil)
end
function GameUILargeMapAlliance.LargeMapAllianceInviteSearchCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.large_map_alliance_player_search_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.large_map_alliance_player_search_ack.Code then
    GameUILargeMapAlliance:GenerateAllianceInviteSearchData(content)
    GameUILargeMapAlliance:SetAllianceInvitePopUI(GameUILargeMapAlliance.allianceInviteSearchData)
    return true
  end
  return false
end
function GameUILargeMapAlliance:GenerateAllianceInviteSearchData(content)
  local data = {}
  data.allianceInfo = GameUILargeMapAlliance.allianceInviteData.alliance_info
  data.searchStr = content.search_name
  data.playerCount = #content.player_list
  data.playerList = {}
  for k, v in ipairs(content.player_list or {}) do
    local item = {}
    item = v
    item.forceText = GameUtils:formatNumber(v.force)
    item.inviteText = GameLoader:GetGameText("")
    item.enableInvite = true
    data.playerList[#data.playerList + 1] = item
  end
  GameUILargeMapAlliance.allianceInviteSearchData = data
end
function GameUILargeMapAlliance:ReqAllianceInvite(playerid)
  local param = {}
  param.player_id = playerid
  NetMessageMgr:SendMsg(NetAPIList.large_map_alliance_invite_req.Code, param, GameUILargeMapAlliance.LargeMapAllianceInviteCallBack, true, nil)
end
function GameUILargeMapAlliance.LargeMapAllianceInviteCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.large_map_alliance_invite_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.large_map_alliance_invite_ack.Code then
    GameUILargeMapAlliance:UpdateAllianceInvite(content)
    return true
  end
  return false
end
function GameUILargeMapAlliance:UpdateAllianceInvite(content)
  if content.is_success == true then
    if GameUILargeMapAlliance.allianceInviteSearchData then
      for k, v in pairs(GameUILargeMapAlliance.allianceInviteSearchData.playerList) do
        if v.id == content.player_id then
          v.enableInvite = false
          GameUILargeMapAlliance:OnFSCommand("update_player_item", k)
          break
        end
      end
    end
    for k, v in pairs(GameUILargeMapAlliance.allianceInviteData.playerList) do
      if v.id == content.player_id then
        v.enableInvite = false
        if GameUILargeMapAlliance.allianceInviteSearchData == nil then
          GameUILargeMapAlliance:OnFSCommand("update_player_item", k)
        end
        break
      end
    end
  end
end
function GameUILargeMapAlliance:ShowInvitePop()
  local flashObj = GameUILargeMapAlliance:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "ShowInvitePop")
  end
end
function GameUILargeMapAlliance:ReqAllianceRecruitOptionData(allianceId)
  local param = {}
  param.alliance_id = allianceId
  NetMessageMgr:SendMsg(NetAPIList.large_map_alliance_recruit_option_req.Code, param, GameUILargeMapAlliance.LargeMapAllianceRecruitOptionCallBack, true, nil)
end
function GameUILargeMapAlliance.LargeMapAllianceRecruitOptionCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.large_map_alliance_recruit_option_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.large_map_alliance_recruit_option_ack.Code then
    GameUILargeMapAlliance:GenerateAllianceRecruitOptionData(content)
    GameUILargeMapAlliance:SetRecruitOptionUI(GameUILargeMapAlliance.allianceRecruitOptionData)
    GameUILargeMapAlliance:ShowRecruitOptionUI()
    return true
  end
  return false
end
function GameUILargeMapAlliance:GenerateAllianceRecruitOptionData(content)
  local data = {}
  data.switchStatus = content.enable_ratify and "state_on" or "state_off"
  data.needLevel = content.need_level
  data.needForce = content.need_force
  data.needExploit = content.need_exploit
  GameUILargeMapAlliance.allianceRecruitOptionData = data
end
function GameUILargeMapAlliance:SetRecruitOptionUI(data)
  local flashObj = GameUILargeMapAlliance:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "SetRecruitOptionUI", data)
  end
end
function GameUILargeMapAlliance:ShowRecruitOptionUI()
  local flashObj = GameUILargeMapAlliance:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "ShowRecruitOptionPop")
  end
end
function GameUILargeMapAlliance:ReqAllianceRecruitOptionUpdate(allianceId, enableRatify, needLevel, needForce, needExploit)
  local param = {}
  param.alliance_id = allianceId
  param.enable_ratify = enableRatify
  param.need_level = tonumber(needLevel) or 1
  param.need_force = tonumber(needForce) or 0
  param.need_exploit = tonumber(needExploit) or 0
  NetMessageMgr:SendMsg(NetAPIList.large_map_alliance_recruit_option_update_req.Code, param, nil, false, nil)
end
function GameUILargeMapAlliance:ReqRatifyData(tab)
  local allianceID = GameUILargeMapAlliance.baseData.alliance_info.id
  if tab == 1 then
    GameUILargeMapAlliance:ReqAllianceRatifyJoinApplyData(allianceID)
  else
    GameUILargeMapAlliance:ReqAllianceRatifyDutyApplyData(allianceID)
  end
end
function GameUILargeMapAlliance:ShowDutyUIByTab(tab)
  local flashObj = GameUILargeMapAlliance:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "SetRatifyTab", tab)
    flashObj:InvokeASCallback("_root", "ShowRatifyUI")
  end
end
function GameUILargeMapAlliance:ReqAllianceRatifyJoinApplyData(allianceId)
  local param = {}
  param.alliance_id = allianceId
  NetMessageMgr:SendMsg(NetAPIList.large_map_alliance_ratify_join_data_req.Code, param, GameUILargeMapAlliance.LargeMapAllianceRatifyJoinDataCallBack, true, nil)
end
function GameUILargeMapAlliance.LargeMapAllianceRatifyJoinDataCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.large_map_alliance_ratify_join_data_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.large_map_alliance_ratify_join_data_ack.Code then
    GameUILargeMapAlliance:GenerateAllianceRatifyJoinData(content)
    GameUILargeMapAlliance:SetRatifyJoinApplyUI(GameUILargeMapAlliance.allianceRatifyJoinApplyData)
    return true
  end
  return false
end
function GameUILargeMapAlliance:GenerateAllianceRatifyJoinData(content)
  local data = {}
  data.joinApplyCount = #content.apply_list
  data.joinApplyList = {}
  for k, v in ipairs(content.apply_list) do
    local item = {}
    item.id = v.id
    item.playerId = v.player_id
    item.name = v.name
    item.level = v.level
    item.forceText = GameUtils:formatNumber(v.force)
    item.lastLoginText = GameUtils:formatTimeStringAsAgoStyle(v.last_login)
    item.agreeText = GameLoader:GetGameText("LC_MENU_MAP_ARMY_APPROVAL")
    item.cancelText = GameLoader:GetGameText("LC_MENU_MAP_ARMY_REFUSE")
    data.joinApplyList[#data.joinApplyList + 1] = item
  end
  GameUILargeMapAlliance.allianceRatifyJoinApplyData = data
end
function GameUILargeMapAlliance:SetRatifyJoinApplyUI(data)
  local flashObj = GameUILargeMapAlliance:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "SetRatifyJoinApplyUI", data)
  end
end
function GameUILargeMapAlliance:ReqAllianceRatifyDutyApplyData(allianceId)
  local param = {}
  param.alliance_id = allianceId
  NetMessageMgr:SendMsg(NetAPIList.large_map_alliance_ratify_duty_data_req.Code, param, GameUILargeMapAlliance.LargeMapAllianceRatifyDutyDataCallBack, true, nil)
end
function GameUILargeMapAlliance.LargeMapAllianceRatifyDutyDataCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.large_map_alliance_ratify_duty_data_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.large_map_alliance_ratify_duty_data_ack.Code then
    GameUILargeMapAlliance:GenerateAllianceRatifyDutyData(content)
    GameUILargeMapAlliance:SetRatifyDutyApplyUI(GameUILargeMapAlliance.allianceRatifyDutyApplyData)
    return true
  end
  return false
end
function GameUILargeMapAlliance:GenerateAllianceRatifyDutyData(content)
  local data = {}
  data.dutyApplyCount = #content.apply_list
  data.dutyApplyList = {}
  for k, v in ipairs(content.apply_list) do
    local item = {}
    item.id = v.id
    item.playerId = v.player_id
    item.name = v.name
    item.level = v.level
    item.forceText = GameUtils:formatNumber(v.force)
    item.rankText = GameLoader:GetGameText("LC_MENU_MAP_ARMY_RANK_" .. v.rank)
    item.dstRankText = GameLoader:GetGameText("LC_MENU_MAP_ARMY_RANK_" .. v.apply_rank)
    item.lastLoginText = GameUtils:formatTimeStringAsAgoStyle(v.last_login)
    item.agreeText = GameLoader:GetGameText("LC_MENU_MAP_ARMY_APPROVAL")
    item.cancelText = GameLoader:GetGameText("LC_MENU_MAP_ARMY_REFUSE")
    data.dutyApplyList[#data.dutyApplyList + 1] = item
  end
  GameUILargeMapAlliance.allianceRatifyDutyApplyData = data
end
function GameUILargeMapAlliance:SetRatifyDutyApplyUI(data)
  local flashObj = GameUILargeMapAlliance:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "SetRatifyDutyApplyUI", data)
  end
end
function GameUILargeMapAlliance:ReqRatifyHandle(id, applyType, status)
  local idlist = {}
  if id == "" and GameUILargeMapAlliance.allianceRatifyJoinApplyData then
    for k, v in pairs(GameUILargeMapAlliance.allianceRatifyJoinApplyData.joinApplyList) do
      idlist[#idlist + 1] = v.id
    end
  else
    idlist[1] = id
  end
  local param = {}
  param.id = idlist
  param.apply_type = applyType
  param.status = status
  NetMessageMgr:SendMsg(NetAPIList.large_map_alliance_ratify_handle_req.Code, param, GameUILargeMapAlliance.LargeMapAllianceRatifyHandleCallBack, true, nil)
end
function GameUILargeMapAlliance.LargeMapAllianceRatifyHandleCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.large_map_alliance_ratify_handle_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.large_map_alliance_ratify_handle_ack.Code then
    if content.apply_type == 1 then
      GameUILargeMapAlliance:GenerateAllianceRatifyJoinData(content.apply_info)
      GameUILargeMapAlliance:SetRatifyJoinApplyUI(GameUILargeMapAlliance.allianceRatifyJoinApplyData)
    else
      GameUILargeMapAlliance:GenerateAllianceRatifyDutyData(content.apply_info)
      GameUILargeMapAlliance:SetRatifyDutyApplyUI(GameUILargeMapAlliance.allianceRatifyDutyApplyData)
    end
    return true
  end
  return false
end
function GameUILargeMapAlliance:ShowApplyDutyPop()
  local flashObj = GameUILargeMapAlliance:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "ShowApplyDutyPop")
  end
end
function GameUILargeMapAlliance:SetApplyDutyPopUI()
  local flashObj = GameUILargeMapAlliance:GetFlashObject()
  if flashObj then
    local data = {}
    data.id = GameUILargeMapAlliance.baseData.player_id
    data.rank = GameUILargeMapAlliance.baseData.alliance_rank
    flashObj:InvokeASCallback("_root", "SetApplyDutyPopUI", data)
  end
end
function GameUILargeMapAlliance:ReqApplyDuty(playerId, applyRank)
  local param = {}
  param.player_id = playerId
  param.apply_rank = tonumber(applyRank)
  NetMessageMgr:SendMsg(NetAPIList.large_map_alliance_apply_duty_req.Code, param, GameUILargeMapAlliance.LargeMapAllianceApplyDutyCallBack, true, nil)
end
function GameUILargeMapAlliance.LargeMapAllianceApplyDutyCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.large_map_alliance_apply_duty_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    else
      GameTip:Show("ok")
    end
    return true
  end
  return false
end
function GameUILargeMapAlliance:ViewMemberInfo(playerid)
  GameUIPlayerView:UpdateWithPlayerID("LM" .. playerid)
end
function GameUILargeMapAlliance:SendMemberMail(playerid)
  local name = ""
  for k, v in pairs(GameUILargeMapAlliance.allianceArmyData.memberList) do
    if v.id == playerid then
      name = v.name
    end
  end
  GameStateManager:GetCurrentGameState():AddObject(GameMail)
  GameMail:ShowBox(GameMail.MAILBOX_WRITE_TAB)
  GameMail:SetEnableChangeName(false)
  GameMail:SetSendSuffix("|LM" .. playerid)
  GameMail:SetWrite(name, "", "")
end
function GameUILargeMapAlliance:ReqMemberPromotion(playerId)
  local param = {}
  param.player_id = playerId
  NetMessageMgr:SendMsg(NetAPIList.large_map_alliance_promotion_req.Code, param, GameUILargeMapAlliance.LargeMapAlliancePromotionCallBack, true, nil)
end
function GameUILargeMapAlliance.LargeMapAlliancePromotionCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.large_map_alliance_promotion_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.large_map_alliance_promotion_ack.Code then
    GameUILargeMapAlliance:GenerateAllianceDemotionData(content)
    GameUILargeMapAlliance:UpdateAllianceArmyMemberData()
    return true
  end
  return false
end
function GameUILargeMapAlliance:ReqAllianceDemotion(playerId)
  local param = {}
  param.player_id = playerId
  NetMessageMgr:SendMsg(NetAPIList.large_map_alliance_demotion_req.Code, param, GameUILargeMapAlliance.LargeMapAllianceDemotionCallBack, true, nil)
end
function GameUILargeMapAlliance.LargeMapAllianceDemotionCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.large_map_alliance_demotion_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.large_map_alliance_demotion_ack.Code then
    GameUILargeMapAlliance:GenerateAllianceDemotionData(content)
    GameUILargeMapAlliance:UpdateAllianceArmyMemberData()
    return true
  end
  return false
end
function GameUILargeMapAlliance:GenerateAllianceDemotionData(content)
  for k, v in pairs(GameUILargeMapAlliance.allianceArmyData.memberList) do
    if v.id == content.id then
      v.order = v.order - v.rank
      v.rank = content.rank
      v.order = v.order + v.rank
      v.rankText = GameLoader:GetGameText("LC_MENU_MAP_ARMY_RANK_" .. v.rank)
      break
    end
  end
  if GameUILargeMapAlliance.allianceArmySelectedData then
    for k, v in pairs(GameUILargeMapAlliance.allianceArmySelectedData.memberList) do
      if v.id == content.id then
        v.order = v.order - v.rank
        v.rank = content.rank
        v.order = v.order + v.rank
        v.rankText = GameLoader:GetGameText("LC_MENU_MAP_ARMY_RANK_" .. v.rank)
        break
      end
    end
  end
end
function GameUILargeMapAlliance:ReqAllianceExchange(playerId)
  local param = {}
  param.player_id = playerId
  NetMessageMgr:SendMsg(NetAPIList.large_map_alliance_exchange_req.Code, param, GameUILargeMapAlliance.LargeMapAllianceExchangeCallBack, true, nil)
end
function GameUILargeMapAlliance.LargeMapAllianceExchangeCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.large_map_alliance_exchange_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.large_map_alliance_exchange_ack.Code then
    GameUILargeMapAlliance:GenerateAllianceExchangeData(content)
    GameUILargeMapAlliance:FilterAllianceMember(GameUILargeMapAlliance.allianceSeletcedOnline)
    return true
  end
  return false
end
function GameUILargeMapAlliance:GenerateAllianceExchangeData(content)
  for k, v in pairs(GameUILargeMapAlliance.allianceArmyData.memberList) do
    if v.id == content.my_alliance_info.id then
      v.order = v.order - v.rank
      v.rank = content.my_alliance_info.rank
      v.order = v.order + v.rank
      v.rankText = GameLoader:GetGameText("LC_MENU_MAP_ARMY_RANK_" .. v.rank)
    elseif v.id == content.other_alliance_info.id then
      v.rank = content.other_alliance_info.rank
      v.rankText = GameLoader:GetGameText("LC_MENU_MAP_ARMY_RANK_" .. v.rank)
    end
  end
  if GameUILargeMapAlliance.allianceArmySelectedData then
    for k, v in pairs(GameUILargeMapAlliance.allianceArmySelectedData.memberList) do
      if v.id == content.my_alliance_info.id then
        v.order = v.order - v.rank
        v.rank = content.my_alliance_info.rank
        v.order = v.order + v.rank
        v.rankText = GameLoader:GetGameText("LC_MENU_MAP_ARMY_RANK_" .. v.rank)
      elseif v.id == content.other_alliance_info.id then
        v.order = v.order - v.rank
        v.rank = content.other_alliance_info.rank
        v.order = v.order + v.rank
        v.rankText = GameLoader:GetGameText("LC_MENU_MAP_ARMY_RANK_" .. v.rank)
      end
    end
  end
  GameUILargeMapAlliance.baseData.alliance_rank = content.my_alliance_info.rank
end
function GameUILargeMapAlliance:ReqAllianceExpel(playerId)
  local param = {}
  param.player_id = playerId
  NetMessageMgr:SendMsg(NetAPIList.large_map_alliance_expel_req.Code, param, GameUILargeMapAlliance.LargeMapAllianceExpelCallBack, true, nil)
end
function GameUILargeMapAlliance.LargeMapAllianceExpelCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.large_map_alliance_expel_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.large_map_alliance_expel_ack.Code then
    GameUILargeMapAlliance:GenerateAllianceExpelData(content)
    if content.is_success then
      GameUILargeMapAlliance:FilterAllianceMember(GameUILargeMapAlliance.allianceSeletcedOnline)
      GameTip:Show(GameLoader:GetGameText("LC_MENU_MAP_LEGION_FIRE_SUCCESS"))
    end
    return true
  end
  return false
end
function GameUILargeMapAlliance:GenerateAllianceExpelData(content)
  if content.is_success then
    for k, v in pairs(GameUILargeMapAlliance.allianceArmyData.memberList) do
      if v.id == content.player_id then
        table.remove(GameUILargeMapAlliance.allianceArmyData.memberList, k)
      end
    end
    GameUILargeMapAlliance.allianceArmyData.memberCount = #GameUILargeMapAlliance.allianceArmyData.memberList
    if GameUILargeMapAlliance.allianceArmySelectedData then
      for k, v in pairs(GameUILargeMapAlliance.allianceArmySelectedData.memberList) do
        if v.id == content.player_id then
          table.remove(GameUILargeMapAlliance.allianceArmySelectedData.memberList, k)
        end
      end
    end
  end
end
function GameUILargeMapAlliance:UpdateAllianceArmyMemberData()
  local flashObj = GameUILargeMapAlliance:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "UpdateAllianceAllMemberItem")
  end
end
function GameUILargeMapAlliance:SetFunPop(playerId, rank)
  local data = {}
  data.playerId = playerId
  data.popType = "p3"
  data.viewText = GameLoader:GetGameText("LC_MENU_MAP_ARMY_TO_VIEW")
  data.mailText = GameLoader:GetGameText("LC_MENU_MAP_ARMY_SEND_EMAIL")
  data.promotionText = GameLoader:GetGameText("LC_MENU_MAP_ARMY_PROMOTION")
  data.demotedText = GameLoader:GetGameText("LC_MENU_MAP_ARMY_DEMOTED")
  data.transferText = GameLoader:GetGameText("LC_MENU_MAP_ARMY_TRANSFER")
  data.dismissedText = GameLoader:GetGameText("LC_MENU_MAP_ARMY_DISMISSED")
  data.promotionEnabled = rank == 20
  data.demotionEnabled = rank < 20
  if rank <= GameUILargeMapAlliance.baseData.alliance_rank then
    data.popType = "p3"
  elseif rank > GameUILargeMapAlliance.baseData.alliance_rank and GameUILargeMapAlliance.baseData.alliance_rank ~= 0 then
    data.popType = "p2"
  else
    data.popType = "p1"
  end
  if GameUILargeMapAlliance:GetFlashObject() then
    GameUILargeMapAlliance:GetFlashObject():InvokeASCallback("_root", "SetSubMenuPopUI", data)
    GameUILargeMapAlliance:GetFlashObject():InvokeASCallback("_root", "ShowSubMenuPop")
  end
end
function GameUILargeMapAlliance:ReqAllianceDiplomacyData()
  NetMessageMgr:SendMsg(NetAPIList.large_map_alliance_diplomacy_req.Code, nil, GameUILargeMapAlliance.LargeMapAllianceDiplomacyCallBack, true, nil)
end
function GameUILargeMapAlliance.LargeMapAllianceDiplomacyCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.large_map_alliance_diplomacy_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.large_map_alliance_diplomacy_ack.Code then
    GameUILargeMapAlliance:GenerateAllianceDiplomacyData(content)
    GameUILargeMapAlliance:SetDiplomacyUI(GameUILargeMapAlliance.allianceDipcomacyData)
    return true
  end
  return false
end
function GameUILargeMapAlliance:GenerateAllianceDiplomacyData(content)
  local data = {}
  data.batToMeList = {}
  for k, v in pairs(content.bat_to_me) do
    local item = {}
    item.id = v.id
    item.allianceName = v.name
    data.batToMeList[#data.batToMeList + 1] = item
  end
  data.batToOtherList = {}
  for k, v in pairs(content.bat_to_other) do
    local item = {}
    item.id = v.id
    item.allianceName = v.name
    data.batToOtherList[#data.batToOtherList + 1] = item
  end
  data.allAllianceList = {}
  for k, v in pairs(content.all_army) do
    local item = {}
    item.id = v.id
    item.allianceName = v.name
    data.allAllianceList[#data.allAllianceList + 1] = item
  end
  GameUILargeMapAlliance.allianceDipcomacyData = data
end
function GameUILargeMapAlliance:SetDiplomacyUI(data)
  if GameUILargeMapAlliance:GetFlashObject() then
    GameUILargeMapAlliance:GetFlashObject():InvokeASCallback("_root", "SetDiplomacyUI", data)
  end
end
function GameUILargeMapAlliance:CreateDragItem(curType, dstType, itemIndex, allianceId)
  local itemData
  if curType == "allarmy" then
    itemData = LuaUtils:table_copy(GameUILargeMapAlliance.allianceDipcomacyData.allAllianceList[itemIndex])
  elseif curType == "battoother" then
    itemData = LuaUtils:table_copy(GameUILargeMapAlliance.allianceDipcomacyData.batToOtherList[itemIndex])
  elseif curType == "battome" then
    itemData = LuaUtils:table_copy(GameUILargeMapAlliance.allianceDipcomacyData.batToMeList[itemIndex])
  end
  local dragObj = {}
  dragObj.curType = curType
  dragObj.dstType = dstType
  dragObj.dragId = #GameUILargeMapAlliance.allianceDipcomacyDragList + 1
  dragObj.allianceId = allianceId
  dragObj.itemData = itemData
  GameUILargeMapAlliance.allianceDipcomacyDragList[#GameUILargeMapAlliance.allianceDipcomacyDragList + 1] = dragObj
  if GameUILargeMapAlliance:GetFlashObject() then
    GameUILargeMapAlliance:GetFlashObject():InvokeASCallback("_root", "CreateDragItem", curType, dstType, dragObj.dragId, itemIndex, itemData)
  end
end
function GameUILargeMapAlliance:RemoveDipcomacyItem(curType, allianceId)
  local list
  if curType == "allarmy" then
    list = GameUILargeMapAlliance.allianceDipcomacyData.allAllianceList
  elseif curType == "battoother" then
    list = GameUILargeMapAlliance.allianceDipcomacyData.batToOtherList
  elseif curType == "battome" then
    list = GameUILargeMapAlliance.allianceDipcomacyData.batToMeList
  end
  for k, v in pairs(list or {}) do
    if v.id == allianceId then
      table.remove(list, k)
      break
    end
  end
end
function GameUILargeMapAlliance:UpdateDipcomacyList(curType)
  if curType == "allarmy" then
    if GameUILargeMapAlliance:GetFlashObject() then
      GameUILargeMapAlliance:GetFlashObject():InvokeASCallback("_root", "SetDiplomacyAllArmy", GameUILargeMapAlliance.allianceDipcomacyData.allAllianceList)
    end
  elseif curType == "battoother" then
    if GameUILargeMapAlliance:GetFlashObject() then
      GameUILargeMapAlliance:GetFlashObject():InvokeASCallback("_root", "SetDiplomacyBatToOther", GameUILargeMapAlliance.allianceDipcomacyData.batToOtherList)
    end
  elseif curType == "battome" and GameUILargeMapAlliance:GetFlashObject() then
    GameUILargeMapAlliance:GetFlashObject():InvokeASCallback("_root", "SetDiplomacyBatToMe", GameUILargeMapAlliance.allianceDipcomacyData.batToMeList)
  end
end
function GameUILargeMapAlliance:FindSameAlliance(searchList, allianceId)
  for k, v in pairs(searchList or {}) do
    if v.id == allianceId then
      return true
    end
  end
  return false
end
function GameUILargeMapAlliance:onDipcomacyDragFinish(dragId)
  local dragObj
  for k, v in pairs(GameUILargeMapAlliance.allianceDipcomacyDragList) do
    if v.dragId == dragId then
      dragObj = v
      table.remove(GameUILargeMapAlliance.allianceDipcomacyDragList, k)
      break
    end
  end
  if dragObj then
    if dragObj.dstType == "allarmy" and not GameUILargeMapAlliance:FindSameAlliance(GameUILargeMapAlliance.allianceDipcomacyData.allAllianceList, dragObj.itemData.id) then
      GameUILargeMapAlliance.allianceDipcomacyData.allAllianceList[#GameUILargeMapAlliance.allianceDipcomacyData.allAllianceList + 1] = dragObj.itemData
    elseif dragObj.dstType == "battoother" and not GameUILargeMapAlliance:FindSameAlliance(GameUILargeMapAlliance.allianceDipcomacyData.batToOtherList, dragObj.itemData.id) then
      GameUILargeMapAlliance.allianceDipcomacyData.batToOtherList[#GameUILargeMapAlliance.allianceDipcomacyData.batToOtherList + 1] = dragObj.itemData
    elseif dragObj.dstType == "battome" and not GameUILargeMapAlliance:FindSameAlliance(GameUILargeMapAlliance.allianceDipcomacyData.batToMeList, dragObj.itemData.id) then
      GameUILargeMapAlliance.allianceDipcomacyData.batToMeList[#GameUILargeMapAlliance.allianceDipcomacyData.batToMeList + 1] = dragObj.itemData
    end
    GameUILargeMapAlliance:UpdateDipcomacyList(dragObj.dstType)
    GameUILargeMapAlliance:ReqDipcomacyExchange(dragObj.curType, dragObj.dstType, dragObj.allianceId)
  end
end
function GameUILargeMapAlliance:ReqDipcomacyExchange(curType, dstType, allianceId)
  local param = {}
  param.alliance_id = allianceId
  param.cur_type = curType
  param.dst_type = dstType
  NetMessageMgr:SendMsg(NetAPIList.large_map_alliance_diplomacy_exchange_req.Code, param, GameUILargeMapAlliance.LargeMapAllianceDiplomacyExchangeCallBack, false, nil)
end
function GameUILargeMapAlliance.LargeMapAllianceDiplomacyExchangeCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.large_map_alliance_diplomacy_exchange_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  return false
end
function GameUILargeMapAlliance:UpdateAffairTime()
  if GameUILargeMapAlliance.allianceAffaisData and GameUILargeMapAlliance.curTab == 1 then
    local leftTime = GameUILargeMapAlliance.allianceAffaisData.grantTime - (os.time() - GameUILargeMapAlliance.allianceAffaisData.grantBaseTime)
    if leftTime < 0 then
      leftTime = 0
    end
    GameUILargeMapAlliance.allianceAffaisData.grantTimeStr = GameUtils:formatTimeString(leftTime)
    GameUILargeMapAlliance.allianceAffaisData.grantTimeStr = ""
    GameUILargeMapAlliance:GetFlashObject():InvokeASCallback("_root", "SetAffairGrantTime", GameUILargeMapAlliance.allianceAffaisData.grantTimeStr)
    if leftTime == 0 and GameUILargeMapAlliance.allianceAffaisData.enableRun then
      GameUILargeMapAlliance.allianceAffaisData.enableRun = false
      GameUILargeMapAlliance:ReqAllianceAffaisData()
    end
    local leftTime2 = GameUILargeMapAlliance.allianceAffaisData.dissolveTime - (os.time() - GameUILargeMapAlliance.allianceAffaisData.dissolveBaseTime)
    if leftTime2 < 0 then
      leftTime2 = 0
    end
    GameUILargeMapAlliance.allianceAffaisData.dissolveTimeStr = leftTime2 > 0 and GameUtils:formatTimeString(leftTime2) or ""
    local exiltText = "(" .. GameLoader:GetGameText("LC_MENU_EXILE") .. ")"
    if 0 < GameUILargeMapAlliance.allianceAffaisData.blockCount then
      GameUILargeMapAlliance.allianceAffaisData.dissolveTimeStr = ""
      exiltText = ""
    end
    GameUILargeMapAlliance:GetFlashObject():InvokeASCallback("_root", "SetDissolveTime", GameUILargeMapAlliance.allianceAffaisData.dissolveTimeStr, exiltText)
    if leftTime2 == 0 and GameUILargeMapAlliance.allianceAffaisData.blockCount == 0 and GameUILargeMapAlliance.allianceAffaisData.enableDissRun then
      GameUILargeMapAlliance.allianceAffaisData.enableDissRun = false
      if is_SetDissolveTimeZero == false then
        GameWaiting:ShowLoadingScreen()
        is_SetDissolveTimeZero = true
      end
    end
  end
end
function GameUILargeMapAlliance:ReqAllianceLogData()
  local param = {}
  param.log_type = GameUILargeMapAlliance.allianceLogTab
  NetMessageMgr:SendMsg(NetAPIList.large_map_log_req.Code, param, GameUILargeMapAlliance.LargeMapAllianceLogDataCallBack, false, nil)
end
function GameUILargeMapAlliance.LargeMapAllianceLogDataCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.large_map_log_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.large_map_log_ack.Code then
    GameUILargeMapAlliance:GenerateAllianceLogData(content)
    GameUILargeMapAlliance:SetLogTab(content.log_type)
    GameUILargeMapAlliance:SetAllianceLogUI(GameUILargeMapAlliance.allianceLogData)
    return true
  end
  return false
end
function GameUILargeMapAlliance:GenerateAllianceLogData(content)
  GameUILargeMapAlliance.allianceLogTab = content.log_type
  local data = {}
  data.logType = content.log_type
  data.logList = {}
  for k, v in ipairs(content.log_list) do
    local item = {}
    item.id = v.id
    item.reportId = v.report_id
    item.logStr = v.log_str
    item.timeStr = GameUtils:formatTimeStringAsAgoStyle(v.time)
    data.logList[#data.logList + 1] = item
  end
  GameUILargeMapAlliance.allianceLogData = data
end
function GameUILargeMapAlliance:SetAllianceLogUI(data)
  if GameUILargeMapAlliance:GetFlashObject() then
    GameUILargeMapAlliance:GetFlashObject():InvokeASCallback("_root", "SetLogData", data)
  end
end
function GameUILargeMapAlliance:SetLogTab(tab)
  if GameUILargeMapAlliance:GetFlashObject() then
    GameUILargeMapAlliance:GetFlashObject():InvokeASCallback("_root", "SetLogTab", tab)
  end
end
function GameUILargeMapAlliance:onLogReport(reportId)
  local function _callback(msgtype, content)
    if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.large_map_log_battle_req.Code then
      if content.code ~= 0 then
        GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      else
        local battleplay = GameStateManager.GameStateBattlePlay
        GameStateBattlePlay.curBattleType = "largemap"
        if GameObjectBattleReplay.GroupBattleReportArr[1] and 0 < #GameObjectBattleReplay.GroupBattleReportArr[1].headers then
          battleplay:InitBattle(battleplay.MODE_HISTORY, GameObjectBattleReplay.GroupBattleReportArr[1].headers[1].report)
          GameStateBattlePlay:RegisterOverCallback(function()
            GameStateManager:SetCurrentGameState(GameStateLargeMap)
            GameUILargeMapAlliance:ShowByType(GameUILargeMapAlliance.baseData, 4)
          end, nil)
          GameStateManager:SetCurrentGameState(battleplay)
        end
      end
      return true
    end
    return false
  end
  local param = {}
  param.report_id = reportId
  NetMessageMgr:SendMsg(NetAPIList.large_map_log_battle_req.Code, param, _callback, true, nil)
end
function GameUILargeMapAlliance:ReqSmallMapData()
  NetMessageMgr:SendMsg(NetAPIList.large_map_small_map_req.Code, nil, GameUILargeMapAlliance.LargeMapSmallMapCallBack, false, nil)
end
function GameUILargeMapAlliance.LargeMapSmallMapCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.large_map_small_map_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.large_map_small_map_ack.Code then
    GameUILargeMapAlliance:SetSmallData(content)
    return true
  end
  return false
end
function GameUILargeMapAlliance:SetSmallData(data)
  if GameUILargeMapAlliance:GetFlashObject() then
    local str = GameLoader:GetGameText("LC_MENU_MAP_ARMY_MINIMAP_REFRESHTIME")
    data.tipText = string.gsub(str, "<number1>", tostring(data.refresh_min))
    GameUILargeMapAlliance:GetFlashObject():InvokeASCallback("_root", "SetMapData", data)
  end
end
function GameUILargeMapAlliance:Update(dt)
  local flashObj = GameUILargeMapAlliance:GetFlashObject()
  if flashObj then
    flashObj:Update(dt)
    flashObj:InvokeASCallback("_root", "OnUpdate", dt)
    GameUILargeMapAlliance:UpdateAffairTime()
  end
end
if AutoUpdate.isAndroidDevice then
  function GameUILargeMapAlliance.OnAndroidBack()
    GameUILargeMapAlliance:OnFSCommand("close")
  end
end
