ProductIdentifyList = {
  AndroidGpIds = {},
  sectionOriginal = {},
  sectionA = {},
  sectionB = {},
  sectionC = {},
  mUdidHash = nil,
  mSection = nil,
  mSetGroupOpen = false
}
local sectionTable = {
  [1] = "sectionA",
  [2] = "sectionB",
  [3] = "sectionC"
}
function ProductIdentifyList:InitProduct()
  self.AndroidGpIds.sectionOriginal = {}
  self.AndroidGpIds.sectionA = {}
  self.AndroidGpIds.sectionB = {}
  self.AndroidGpIds.sectionC = {}
  self.AndroidGpIds.sectionOriginal.product1 = "ge2_120_credits_an"
  self.AndroidGpIds.sectionOriginal.product2 = "ge2_500_credits_an"
  self.AndroidGpIds.sectionOriginal.product3 = "ge2_1200_credits_an"
  self.AndroidGpIds.sectionOriginal.product4 = "ge2_2500_credits_an"
  self.AndroidGpIds.sectionOriginal.product5 = "ge2_14000_credits_an"
  self.AndroidGpIds.sectionOriginal.product6 = "ge2_6500_credits_an"
  self.AndroidGpIds.sectionOriginal.product7 = "ge2_credits_package_120_an"
  self.AndroidGpIds.sectionOriginal.product8 = "ge2_credits_package_600_an"
  self.AndroidGpIds.sectionOriginal.product9 = "ge2_credits_package_1250_an"
  self.AndroidGpIds.sectionOriginal.product10 = "ge2_credits_package_2750_an"
  self.AndroidGpIds.sectionOriginal.product11 = "ge2_credits_package_7000_an"
  self.AndroidGpIds.sectionOriginal.product12 = "ge2_credits_package_15000_an"
  self.AndroidGpIds.sectionA.product1 = "ge2_500_credits_an_a"
  self.AndroidGpIds.sectionA.product2 = "ge2_1200_credits_an_a"
  self.AndroidGpIds.sectionA.product3 = "ge2_2500_credits_an_a"
  self.AndroidGpIds.sectionA.product4 = "ge2_14000_credits_an_a"
  self.AndroidGpIds.sectionA.product5 = "ge2_6500_credits_an_a"
  self.AndroidGpIds.sectionA.product6 = "ge2_credits_package_120_an_a"
  self.AndroidGpIds.sectionA.product7 = "ge2_credits_package_600_an_a"
  self.AndroidGpIds.sectionA.product8 = "ge2_credits_package_1250_an_a"
  self.AndroidGpIds.sectionA.product9 = "ge2_credits_package_2750_an_a"
  self.AndroidGpIds.sectionA.product10 = "ge2_credits_package_7000_an_a"
  self.AndroidGpIds.sectionA.product11 = "ge2_credits_package_15000_an_a"
  self.AndroidGpIds.sectionB.product1 = "ge2_500_credits_an_b"
  self.AndroidGpIds.sectionB.product2 = "ge2_1200_credits_an_b"
  self.AndroidGpIds.sectionB.product3 = "ge2_2500_credits_an_b"
  self.AndroidGpIds.sectionB.product4 = "ge2_14000_credits_an_b"
  self.AndroidGpIds.sectionB.product5 = "ge2_6500_credits_an_b"
  self.AndroidGpIds.sectionB.product6 = "ge2_credits_package_120_an_b"
  self.AndroidGpIds.sectionB.product7 = "ge2_credits_package_600_an_b"
  self.AndroidGpIds.sectionB.product8 = "ge2_credits_package_1250_an_b"
  self.AndroidGpIds.sectionB.product9 = "ge2_credits_package_2750_an_b"
  self.AndroidGpIds.sectionB.product10 = "ge2_credits_package_7000_an_b"
  self.AndroidGpIds.sectionB.product11 = "ge2_credits_package_15000_an_b"
  self.AndroidGpIds.sectionC.product1 = "ge2_500_credits_an_c"
  self.AndroidGpIds.sectionC.product2 = "ge2_1200_credits_an_c"
  self.AndroidGpIds.sectionC.product3 = "ge2_2500_credits_an_c"
  self.AndroidGpIds.sectionC.product4 = "ge2_14000_credits_an_c"
  self.AndroidGpIds.sectionC.product5 = "ge2_6500_credits_an_c"
  self.AndroidGpIds.sectionC.product6 = "ge2_credits_package_120_an_c"
  self.AndroidGpIds.sectionC.product7 = "ge2_credits_package_600_an_c"
  self.AndroidGpIds.sectionC.product8 = "ge2_credits_package_1250_an_c"
  self.AndroidGpIds.sectionC.product9 = "ge2_credits_package_2750_an_c"
  self.AndroidGpIds.sectionC.product10 = "ge2_credits_package_7000_an_c"
  self.AndroidGpIds.sectionC.product11 = "ge2_credits_package_15000_an_c"
end
function ProductIdentifyList:GetUDIDHash()
  if mUdidHash then
    return mUdidHash
  end
  mUdidHash = ext.ENC1(ext.GetIOSOpenUdid())
  return mUdidHash
end
function ProductIdentifyList:GetSecTionIndex(udidHash)
  local hashLen = string.len(udidHash)
  if hashLen >= 4 then
    local modStr = string.sub(udidHash, hashLen - 4, hashLen)
    local modNumber = tonumber(modStr, 16)
    return modNumber % 3 + 1
  end
  return nil
end
function ProductIdentifyList:IsSupportSetProductList()
  return AutoUpdate.isSupportSetProductList
end
function ProductIdentifyList:IsSetGroupOpen()
  return ProductIdentifyList.mSetGroupOpen
end
function ProductIdentifyList:GetSectionNumber()
  local hash = self:GetUDIDHash()
  local sectionIndex = self:GetSecTionIndex(hash)
  return sectionIndex
end
function ProductIdentifyList:GetSection()
  if AutoUpdate.isSupportSetProductList and mSetGroupOpen then
    local hash = self:GetUDIDHash()
    local sectionIndex = self:GetSecTionIndex(hash)
    DebugOut("sectionIndex = ", sectionIndex)
    if sectionIndex then
      mSection = sectionTable[sectionIndex]
      DebugOut("mSection = ", mSection)
    else
      mSection = "sectionOriginal"
    end
    return mSection
  else
    mSection = "sectionOriginal"
    return mSection
  end
end
function ProductIdentifyList:SetProductList()
  if not AutoUpdate.isAndroidDevice then
    return
  end
  if not AutoUpdate.isSupportSetProductList then
    return
  end
  local productList = self.AndroidGpIds[self:GetSection()]
  local ret = ext.json.generate(productList)
  IPlatformExt.setUpProductList(ext.json.generate(productList))
end
