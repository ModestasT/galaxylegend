WVEInfoPanelPowerUp = luaClass(nil)
local WVEGameManeger = LuaObjectManager:GetLuaObject("WVEGameManeger")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
function WVEInfoPanelPowerUp:ctor(wveGameManeger)
  self.mGameManger = wveGameManeger
end
function WVEInfoPanelPowerUp:Init()
  local flashObj = self.mGameManger:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "ShowPowerUpBtn")
  end
end
function WVEInfoPanelPowerUp:OnFSCommand(cmd, arg)
  local flashObj = self.mGameManger:GetFlashObject()
  if cmd == "click_power_up" then
    NetMessageMgr:SendMsg(NetAPIList.prime_increase_power_info_req.Code, nil, self.RequestPowerUpInfoCallback, true, nil)
  elseif cmd == "cancel_power_up" then
    if flashObj then
      flashObj:InvokeASCallback("_root", "HidePowerUpPanel")
    end
  elseif cmd == "ok_power_up" then
    if flashObj then
      flashObj:InvokeASCallback("_root", "HidePowerUpPanel")
    end
    self:RequestPowerUp()
  else
    return false
  end
  return true
end
function WVEInfoPanelPowerUp:RequestPowerUp()
  local userResource = GameGlobalData:GetData("resource")
  if userResource and userResource.credit < WVEGameManeger.powerup_credit then
    GameTip:Show(GameLoader:GetGameText("LC_MENU_NOT_ENOUGH_CREDIT"))
  else
    NetMessageMgr:SendMsg(NetAPIList.prime_increase_power_req.Code, nil, self.RequestPowerUpCallback, false, nil)
  end
end
function WVEInfoPanelPowerUp:SetPowerUpBtnCreditNum(num)
  local flashObj = self.mGameManger:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "SetPowerUpBtnCreditNum", tonumber(num))
  end
end
function WVEInfoPanelPowerUp.RequestPowerUpInfoCallback(msgType, content)
  DebugOut("RequestPowerUpInfoCallback:", msgType)
  DebugTable(content)
  DebugOut(NetAPIList.prime_increase_power_info_ack.Code)
  if msgType == NetAPIList.prime_increase_power_info_ack.Code then
    DebugOut("SetPowerUpInfo")
    WVEGameManeger.mWVEInfoPanelPowerUp:SetPowerUpInfo(content)
    return true
  elseif msgType == NetAPIList.common_ack.Code and content and content.api == NetAPIList.prime_increase_power_info_req.Code then
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  end
  return false
end
function WVEInfoPanelPowerUp.RequestPowerUpCallback(msgType, content)
  DebugOut("RequestPowerUpCallback", msgType)
  DebugTable(content)
  if msgType == NetAPIList.common_ack.Code and content and content.api == NetAPIList.prime_increase_power_req.Code then
    if content.code == 0 then
      GameUtils:ShowTips(GameLoader:GetGameText("LC_MENU_WVE_POWER_UP_INFO"))
    else
      DebugOut("RequestPowerUpCallback0:", content.code)
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  return false
end
function WVEInfoPanelPowerUp:UpdatePowerUpTime(myInfo)
  DebugOut("UpdatePowerUpTime")
  DebugTable(myInfo)
  local leftTime = tonumber(myInfo.power_left_time)
  local flashObj = self.mGameManger:GetFlashObject()
  if leftTime and leftTime > 0 then
    local powerUp = self:CalculatePowerUp(myInfo.change_list)
    self.PowerUpLeftTime = leftTime + os.time()
    DebugOut("UpdatePowerUpTime:", powerUp, ",", leftTime)
    if flashObj then
      flashObj:InvokeASCallback("_root", "ShowPowerUpTime")
      flashObj:InvokeASCallback("_root", "SetPowerUpTimePanelInfo", GameLoader:GetGameText("LC_MENU_FORCE_CHAR") .. "+" .. powerUp .. "%", GameUtils:formatTimeString(leftTime))
    end
  else
    self.PowerUpLeftTime = nil
    if flashObj then
      flashObj:InvokeASCallback("_root", "ShowPowerUpBtn")
    end
  end
end
function WVEInfoPanelPowerUp:Update(dt)
  local flashObj = self.mGameManger:GetFlashObject()
  if flashObj and self.PowerUpLeftTime then
    if self.PowerUpLeftTime - os.time() > 0 then
      flashObj:InvokeASCallback("_root", "UpdatePowerUpLeftTime", GameUtils:formatTimeString(self.PowerUpLeftTime - os.time()))
    else
      flashObj:InvokeASCallback("_root", "ShowPowerUpBtn")
      self.PowerUpLeftTime = nil
    end
  end
end
function WVEInfoPanelPowerUp:CalculatePowerUp(powerUpList)
  local count = 0
  for k, v in pairs(powerUpList) do
    count = count + tonumber(v.change_percent)
  end
  count = count / #powerUpList
  return count
end
function WVEInfoPanelPowerUp:SetPowerUpInfo(content)
  local flashObj = self.mGameManger:GetFlashObject()
  DebugOut("SetPowerUpInfo start", flashObj)
  if not flashObj or not content then
    return
  end
  local data = {}
  for k, v in pairs(content.change_list) do
    local itemData = {}
    itemData.propertyName = GameLoader:GetGameText("LC_MENU_Equip_param_" .. v.id)
    itemData.propertyValue = v.change_percent
    table.insert(data, itemData)
  end
  local desc = string.format(GameLoader:GetGameText("LC_MENU_PRIMUS_INVADE_POWER_UP_INFO"), self.mGameManger.powerup_credit)
  flashObj:InvokeASCallback("_root", "SetPowerUpPanelInfo", desc, data)
  flashObj:InvokeASCallback("_root", "ShowPowerUpPanel")
  DebugOut("SetPowerUpInfo END")
end
