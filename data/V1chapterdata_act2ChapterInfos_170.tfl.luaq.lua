local ChapterInfos_170 = GameData.chapterdata_act2.ChapterInfos_170
ChapterInfos_170[1] = {
  ChaperID = 1,
  ICON = "icon1",
  MapIndex = 1,
  BossPos = 5,
  EntroStory = {2101, 2102},
  AFTER_FINISH = {
    2105,
    2106,
    2107,
    2108,
    2109,
    2110,
    2111
  }
}
ChapterInfos_170[2] = {
  ChaperID = 2,
  ICON = "icon2",
  MapIndex = 2,
  BossPos = 10,
  EntroStory = {2201},
  AFTER_FINISH = {
    2209,
    2210,
    2211,
    2212
  }
}
ChapterInfos_170[3] = {
  ChaperID = 3,
  ICON = "icon30",
  MapIndex = 3,
  BossPos = 16,
  EntroStory = {
    2301,
    2302,
    2303
  },
  AFTER_FINISH = {
    2311,
    2312,
    2313,
    2314,
    2315
  }
}
ChapterInfos_170[4] = {
  ChaperID = 4,
  ICON = "icon400",
  MapIndex = 1,
  BossPos = 18,
  EntroStory = {2401, 2402},
  AFTER_FINISH = {
    2403,
    2404,
    2405
  }
}
ChapterInfos_170[5] = {
  ChaperID = 5,
  ICON = "icon5",
  MapIndex = 1,
  BossPos = 18,
  EntroStory = {2501},
  AFTER_FINISH = {2503, 2504}
}
ChapterInfos_170[6] = {
  ChaperID = 6,
  ICON = "icon400",
  MapIndex = 1,
  BossPos = 18,
  EntroStory = {2601},
  AFTER_FINISH = {
    2606,
    2607,
    2608
  }
}
ChapterInfos_170[7] = {
  ChaperID = 7,
  ICON = "icon6",
  MapIndex = 1,
  BossPos = 18,
  EntroStory = {2701, 2702},
  AFTER_FINISH = {
    2707,
    2708,
    2709
  }
}
ChapterInfos_170[8] = {
  ChaperID = 8,
  ICON = "icon400",
  MapIndex = 1,
  BossPos = 18,
  EntroStory = {2801, 2802},
  AFTER_FINISH = {
    2806,
    2807,
    2808
  }
}
ChapterInfos_170[9] = {
  ChaperID = 9,
  ICON = "icon5",
  MapIndex = 1,
  BossPos = 18,
  EntroStory = {},
  AFTER_FINISH = {}
}
ChapterInfos_170[10] = {
  ChaperID = 10,
  ICON = "icon5",
  MapIndex = 1,
  BossPos = 18,
  EntroStory = {},
  AFTER_FINISH = {}
}
