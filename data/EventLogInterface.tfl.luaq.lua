EventLogInterface = {}
function EventLogInterface:SendAppsFlyerEvent(eventType, param)
  if ext.AppsFlyerEvent then
    ext.AppsFlyerEvent(eventType, param)
  end
end
function EventLogInterface:SendFBLogEvent(eventType, param)
  if ext.socialShare.FB_LogEvent and Facebook:IsFacebookEnabled() then
    ext.socialShare.FB_LogEvent(eventType, param)
  end
end
function EventLogInterface.eventLogNtf(content)
  DebugOut("EventLogInterface.eventLogNtf")
  DebugTable(content)
  if content.event_type == 1 then
    EventLogInterface:SendAppsFlyerEvent(content.event_name, content.event_param)
  elseif content.event_type == 2 then
    EventLogInterface:SendFBLogEvent(content.event_name, content.event_param)
  else
    EventLogInterface:SendAppsFlyerEvent(content.event_name, content.event_param)
    EventLogInterface:SendFBLogEvent(content.event_name, content.event_param)
  end
end
