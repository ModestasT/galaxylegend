local GameStateMainPlanet = GameStateManager.GameStateMainPlanet
local GameUICollect = LuaObjectManager:GetLuaObject("GameUICollect")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameUIBuilding = LuaObjectManager:GetLuaObject("GameUIBuilding")
local QuestTutorialTax = TutorialQuestManager.QuestTutorialTax
local QuestTutorialBattleMap = TutorialQuestManager.QuestTutorialBattleMap
local QuestTutorialEngineer = TutorialQuestManager.QuestTutorialEngineer
local GameObjectMainPlanet = LuaObjectManager:GetLuaObject("GameObjectMainPlanet")
local GameUICommonDialog = LuaObjectManager:GetLuaObject("GameUICommonDialog")
local GameUIBarRight = LuaObjectManager:GetLuaObject("GameUIBarRight")
local QuestTutorialCollect = TutorialQuestManager.QuestTutorialCollect
local GameVipDetailInfoPanel = LuaObjectManager:GetLuaObject("GameVipDetailInfoPanel")
local FacebookPopUI = LuaObjectManager:GetLuaObject("FacebookPopUI")
GameUICollect.buildingName = "planetary_fortress"
function GameUICollect:OnInitGame()
  GameGlobalData:RegisterDataChangeCallback("resource", GameUICollect._UpdateResourceListener)
end
local crit_times = 1
function GameUICollect:ShowCollect()
  self:LoadFlashObject()
  GameStateManager:GetCurrentGameState():AddObject(self)
end
function GameUICollect:DoColloectTutorial()
  if self.finishTaxTutorial then
    AddFlurryEvent("TutorialCityHallTax_SelectTax2", {}, 2)
  end
  if QuestTutorialTax:IsActive() then
    QuestTutorialTax:SetFinish(true)
    self.finishTaxTutorial = true
    if immanentversion == 2 then
      AddFlurryEvent("TutorialCityHallTax_SelectTax", {}, 2)
      local function callback()
        self:GetFlashObject():InvokeASCallback("_root", "HideTutorialTax")
        if GameObjectMainPlanet:GetFlashObject() then
          GameObjectMainPlanet:GetFlashObject():InvokeASCallback("_root", "HideTutorialCityHall")
        end
        if not QuestTutorialEngineer:IsActive() and not QuestTutorialEngineer:IsFinished() then
          QuestTutorialEngineer:SetActive(true)
        end
      end
      GameUICommonDialog:PlayStory({51109, 51110}, callback)
    elseif immanentversion == 1 then
      self:GetFlashObject():InvokeASCallback("_root", "ShowTutorialClose")
      self:GetFlashObject():InvokeASCallback("_root", "HideTutorialTax")
      if GameObjectMainPlanet:GetFlashObject() then
        GameObjectMainPlanet:GetFlashObject():InvokeASCallback("_root", "HideTutorialCityHall")
      end
    end
  end
end
function GameUICollect:OnFSCommand(cmd, arg)
  if cmd == "normal_collect" then
    self:CheckShowTutorial()
    NetMessageMgr:SendMsg(NetAPIList.revenue_do_req.Code, {times = 1}, self.serverCallback, false, nil)
    return
  end
  if cmd == "force_collect" then
    local costCredit = tonumber(arg)
    local function force_levy_callback()
      NetMessageMgr:SendMsg(NetAPIList.revenue_exchange_req.Code, nil, self.serverCallback, false)
    end
    local info_content = string.format(GameLoader:GetGameText("LC_MENU_FORCE_COLLECTION_CHAR"), costCredit)
    GameUtils:CreditCostConfirm(info_content, force_levy_callback)
    return
  end
  if cmd == "close" then
    if GameUICollect.finishTaxTutorial then
      GameUICollect.finishTaxTutorial = nil
      AddFlurryEvent("TutorialCityHallTax_Close", {}, 2)
    end
    self:MoveOut()
  elseif cmd == "animation_over_moveout" then
    GameStateManager:GetCurrentGameState():EraseObject(self)
  elseif cmd == "collect_all" then
    if self:isOneKeyCollectUseable() or Facebook:IsBindFacebook() then
      self:CheckShowTutorial()
      GameUICollect:DoColloectTutorial()
      self:OneKeyCollect()
    elseif Facebook:IsFacebookEnabled() then
      if FacebookPopUI.mFeatureSwitch[FACEBOOKFEATURESWITCH.SWITCH_COLLECT] and FacebookPopUI.mFeatureSwitch[FACEBOOKFEATURESWITCH.SWITCH_COLLECT] == 1 then
        FacebookPopUI.mLogInAwardCallback = self.CheckUnlockCollectAll
        FacebookPopUI:ShowFacebookMenu(FACEBOOKMENUTYPE.TYPE_MENU_LOGIN)
        FacebookPopUI:SetBindAwardType(FACEBOOKBINDAWARDTYPE.AWARD_UNLOCK_COLLECTALL)
        if not ext.GetDeviceCountry or not ext.GetDeviceCountry() then
          local countryStr = "unknow"
        end
      else
        self:tipVipLevelNotEnoughForOneKeyCollect()
      end
    else
      self:tipVipLevelNotEnoughForOneKeyCollect()
    end
  elseif cmd == "animation_over_collect" and self.CollectAnimationOverCallback then
    self.CollectAnimationOverCallback()
  end
end
function GameUICollect:MoveIn()
  self:IsTutorialCollectActive()
  local obj_flash = self:GetFlashObject()
  obj_flash:InvokeASCallback("_root", "lua2fs_animationMoveIn")
end
function GameUICollect:MoveOut()
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "lua2fs_animationMoveOut")
end
function GameUICollect:IsTutorialCollectActive()
  if QuestTutorialCollect:IsActive() then
    local function callback()
      self:GetFlashObject():InvokeASCallback("_root", "ShowTutorialTax")
    end
    GameUICommonDialog:PlayStory({100017}, callback)
  end
end
function GameUICollect:isOneKeyCollectUseable()
  local curLevel = GameVipDetailInfoPanel:GetVipLevel()
  local viplevellimit = self:getOneKeyCollectVipLevelLimit()
  local curPlayerLevel = GameGlobalData:GetData("levelinfo").level
  local curPlayerLevelLimit = GameDataAccessHelper:GetLevelLimit("one_key_collect")
  return curLevel >= viplevellimit and curPlayerLevel >= curPlayerLevelLimit
end
function GameUICollect:tipVipLevelNotEnoughForOneKeyCollect()
  local strTip = GameLoader:GetGameText("LC_MENU_UNLOCK_LEVEL_OR_VIP_INFO")
  local viplevellimit = self:getOneKeyCollectVipLevelLimit()
  local playerlevellimit = GameDataAccessHelper:GetLevelLimit("one_key_collect")
  strTip = string.gsub(strTip, "<playerLevel_num>", playerlevellimit)
  strTip = string.gsub(strTip, "<vipLevel_num>", viplevellimit)
  GameUtils:ShowTips(strTip)
end
function GameUICollect:getOneKeyCollectVipLevelLimit()
  return ...
end
function GameUICollect:OneKeyCollect()
  NetMessageMgr:SendMsg(NetAPIList.revenue_do_req.Code, {times = 1}, self.OneKeyCollectCallback, true, nil)
end
function GameUICollect:AnimationCrit(taxes, crit_times)
  local flash_obj = self:GetFlashObject()
  if not flash_obj then
    return
  end
  if crit_times and crit_times > 1 then
    taxes = crit_times * taxes
  end
  flash_obj:InvokeASCallback("_root", "lua2fs_animationCrit", taxes, crit_times)
end
function GameUICollect:UpdateRevenueInfo(revenue_info)
  DebugOut("GameUICollect:UpdateRevenueInfo")
  if GameUICollect.AddCollectionNotification then
    local allCDTimes = (revenue_info.total_times - revenue_info.left_times) * 3600
    DebugOut("allCDTimes")
    DebugOut(allCDTimes)
    GameUICollect.AddCollectionNotification(allCDTimes)
  end
  self._lastRevenueData = revenue_info
  local obj_flash = self:GetFlashObject()
  if not obj_flash then
    return
  end
  obj_flash:InvokeASCallback("_root", "lua2fs_updateLeftTimes", revenue_info.left_times, revenue_info.total_times)
  local text_cubit_collect = tostring(revenue_info.money)
  obj_flash:SetText("_main.tax_info.num_taxes", text_cubit_collect)
  obj_flash:SetText("_main.btn_force_levy.cost_credit.textbox", tostring(revenue_info.exchange_cost))
  local leftTime = revenue_info.left_time * 1000
  self._timeRecover = ext.getSysTime() + leftTime
  self:UpdateRecoverTimeInfo(math.floor(leftTime / 1000))
end
function GameUICollect:UpdateResourceInfo()
  local flash_obj = self:GetFlashObject()
  local data_resource = GameGlobalData:GetData("resource")
  flash_obj:SetText("_main.tax_info.num_credit", GameUtils.numberConversion(data_resource.credit))
  flash_obj:SetText("_main.tax_info.num_cubit", GameUtils.numberConversion(data_resource.money))
end
function GameUICollect:Visible(is_visible, mc_path)
  mc_path = mc_path or -1
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "lua2fs_setVisible", mc_path, is_visible)
end
function GameUICollect:OnEraseFromGameState()
  if QuestTutorialTax:IsActive() then
    self:GetFlashObject():InvokeASCallback("_root", "HideTutorialTax")
  end
  if self.finishTaxTutorial then
    self.finishTaxTutorial = nil
  end
  FacebookPopUI.mLogInAwardCallback = nil
  self:GetFlashObject():InvokeASCallback("_root", "HideTutorialClose")
  GameStateMainPlanet:CheckShowTutorial()
  self:UnloadFlashObject()
  GameStateMainPlanet:CheckShowQuestInMainPlanet()
end
function GameUICollect:OnAddToGameState(game_state)
  self:Visible(false)
  if self:isOneKeyCollectUseable() or Facebook:IsBindFacebook() then
    self:GetFlashObject():InvokeASCallback("_root", "SetOneKeyCollectBtnUnlock")
  else
    self:GetFlashObject():InvokeASCallback("_root", "SetOneKeyCollectBtnLock")
  end
  NetMessageMgr:SendMsg(NetAPIList.revenue_info_req.Code, null, GameUICollect.serverCallback, true)
  self.CollectAnimationOverCallback = nil
end
function GameUICollect.CheckUnlockCollectAll()
  if GameStateManager:GetCurrentGameState():IsObjectInState(GameUICollect) and GameUICollect:GetFlashObject() then
    if GameUICollect:isOneKeyCollectUseable() or Facebook:IsBindFacebook() then
      DebugOut("SetOneKeyCollectBtnUnlock")
      GameUICollect:GetFlashObject():InvokeASCallback("_root", "SetOneKeyCollectBtnUnlock")
    else
      DebugOut("SetOneKeyCollectBtnLock lock")
      GameUICollect:GetFlashObject():InvokeASCallback("_root", "SetOneKeyCollectBtnLock")
    end
  end
end
function GameUICollect:CheckShowTutorial()
  if QuestTutorialCollect:IsActive() and immanentversion == 1 then
    self:GetFlashObject():InvokeASCallback("_root", "ShowTutorialClose")
  end
  if not QuestTutorialCollect:IsFinished() then
    QuestTutorialCollect:SetFinish(true)
  end
end
function GameUICollect:UpdateRecoverTimeInfo(timeLeft)
  local obj_flash = self:GetFlashObject()
  if not obj_flash then
    return
  end
  obj_flash:SetText("_main.tax_info.recover_time", GameUtils:formatTimeString(timeLeft))
end
function GameUICollect:Update(dt)
  local obj_flash = self:GetFlashObject()
  obj_flash:InvokeASCallback("_root", "onUpdateFrame", dt)
  if self._timeRecover then
    local timeLeft = self._timeRecover - ext.getSysTime()
    if timeLeft <= 0 then
      timeLeft = 0
      self._timeRecover = nil
    end
    self:UpdateRecoverTimeInfo(math.floor(timeLeft / 1000))
  end
  obj_flash:Update(dt)
end
function GameUICollect:RequestInfo()
  local building = GameGlobalData:GetBuildingInfo(self.buildingName)
  if building and building.level > 0 then
    NetMessageMgr:SendMsg(NetAPIList.revenue_info_req.Code, null, GameUICollect.serverCallback, true, nil)
  elseif building and 0 < building.status then
    GameUIBuilding:DisplayUpgradeDialog(self.buildingName)
  end
end
function GameUICollect:SetLevyAllEnabled(enabled)
  self:GetFlashObject():InvokeASCallback("_root", "setLevyAllEnabled", enabled)
end
function GameUICollect.serverCallback(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and (content.api == NetAPIList.revenue_info_req.Code or content.api == NetAPIList.revenue_do_req.Code or content.api == NetAPIList.revenue_exchange_req.Code) then
    local GameVip = LuaObjectManager:GetLuaObject("GameVip")
    GameVip:CheckIsNeedShowVip(content.code, true)
    return true
  end
  if msgtype == NetAPIList.revenue_info_ack.Code then
    GameUICollect:MoveIn()
    GameUICollect:UpdateRevenueInfo(content)
    GameUICollect:UpdateResourceInfo()
    if QuestTutorialTax:IsActive() then
      if immanentversion == 1 then
        local function callback()
          GameUICollect:GetFlashObject():InvokeASCallback("_root", "ShowTutorialTax")
        end
        GameUICommonDialog:PlayStory({6}, callback)
      elseif immanentversion == 2 then
        GameUICollect:GetFlashObject():InvokeASCallback("_root", "ShowTutorialTax")
      end
    end
    return true
  end
  if msgtype == NetAPIList.revenue_do_ack.Code then
    GameUICollect:UpdateRevenueInfo(content.revenue)
    GameUICollect:AnimationCrit(content.revenue.money, content.more)
    GameUICollect:DoColloectTutorial()
    return true
  end
  return false
end
function GameUICollect._UpdateResourceListener()
  if GameStateManager:GetCurrentGameState() == GameStateMainPlanet and GameStateMainPlanet:IsObjectInState(GameUICollect) then
    GameUICollect:UpdateResourceInfo()
  end
end
function GameUICollect:CheckTutorialCollect(code)
  local money_code = tonumber(code)
  if money_code == 8604 or money_code == 2001 or money_code == 130 or money_code == 709 then
    local GameUICommonDialog = LuaObjectManager:GetLuaObject("GameUICommonDialog")
    local levelInfo = GameGlobalData:GetData("levelinfo")
    if not QuestTutorialCollect:IsFinished() and levelInfo.level <= 18 then
      QuestTutorialCollect:SetActive(true)
      local callback = function()
        GameStateManager:SetCurrentGameState(GameStateManager.GameStateMainPlanet)
      end
      GameUICommonDialog:PlayStory({100001}, callback)
      return true
    end
    return false
  end
  return false
end
function GameUICollect.OneKeyCollectCallback(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.revenue_do_req.Code then
    local GameVip = LuaObjectManager:GetLuaObject("GameVip")
    GameVip:CheckIsNeedShowVip(content.code)
    return true
  end
  if msgtype == NetAPIList.revenue_do_ack.Code then
    if GameUICollect:GetFlashObject() then
      GameUICollect:UpdateRevenueInfo(content.revenue)
      GameUICollect:AnimationCrit(content.revenue.money, content.more)
      if GameUICollect._lastRevenueData.left_times > 0 then
        GameUICollect:OneKeyCollect()
      else
        GameUICollect:DoColloectTutorial()
      end
    end
    return true
  end
  return false
end
if AutoUpdate.isAndroidDevice then
  function GameUICollect.OnAndroidBack()
    GameUICollect:MoveOut()
  end
end
