local enhance = GameData.medal_enhance.enhance
table.insert(enhance, {
  type = 100001,
  rank = 0,
  level_limit = 5,
  consume = "[{medal,2},{9001,2},{9002,0}]"
})
table.insert(enhance, {
  type = 101001,
  rank = 1,
  level_limit = 10,
  consume = "[{medal,5},{9001,2},{9002,1}]"
})
table.insert(enhance, {
  type = 102001,
  rank = 2,
  level_limit = 15,
  consume = "[{medal,8},{9001,2},{9002,2}]"
})
table.insert(enhance, {
  type = 103001,
  rank = 3,
  level_limit = 20,
  consume = "[{medal,10},{9001,6},{9002,2}]"
})
table.insert(enhance, {
  type = 104001,
  rank = 4,
  level_limit = 25,
  consume = "[{medal,12},{9001,6},{9002,4}]"
})
table.insert(enhance, {
  type = 105001,
  rank = 5,
  level_limit = 30,
  consume = "[{medal,15},{9001,8},{9002,6}]"
})
table.insert(enhance, {
  type = 106001,
  rank = 6,
  level_limit = 30,
  consume = "[]"
})
table.insert(enhance, {
  type = 100002,
  rank = 0,
  level_limit = 5,
  consume = "[{medal,2},{9001,2},{9002,0}]"
})
table.insert(enhance, {
  type = 101002,
  rank = 1,
  level_limit = 10,
  consume = "[{medal,5},{9001,2},{9002,1}]"
})
table.insert(enhance, {
  type = 102002,
  rank = 2,
  level_limit = 15,
  consume = "[{medal,8},{9001,2},{9002,2}]"
})
table.insert(enhance, {
  type = 103002,
  rank = 3,
  level_limit = 20,
  consume = "[{medal,10},{9001,6},{9002,2}]"
})
table.insert(enhance, {
  type = 104002,
  rank = 4,
  level_limit = 25,
  consume = "[{medal,12},{9001,6},{9002,4}]"
})
table.insert(enhance, {
  type = 105002,
  rank = 5,
  level_limit = 30,
  consume = "[{medal,15},{9001,8},{9002,6}]"
})
table.insert(enhance, {
  type = 106002,
  rank = 6,
  level_limit = 30,
  consume = "[]"
})
table.insert(enhance, {
  type = 100003,
  rank = 0,
  level_limit = 5,
  consume = "[{medal,2},{9001,2},{9002,0}]"
})
table.insert(enhance, {
  type = 101003,
  rank = 1,
  level_limit = 10,
  consume = "[{medal,5},{9001,2},{9002,1}]"
})
table.insert(enhance, {
  type = 102003,
  rank = 2,
  level_limit = 15,
  consume = "[{medal,8},{9001,2},{9002,2}]"
})
table.insert(enhance, {
  type = 103003,
  rank = 3,
  level_limit = 20,
  consume = "[{medal,10},{9001,6},{9002,2}]"
})
table.insert(enhance, {
  type = 104003,
  rank = 4,
  level_limit = 25,
  consume = "[{medal,12},{9001,6},{9002,4}]"
})
table.insert(enhance, {
  type = 105003,
  rank = 5,
  level_limit = 30,
  consume = "[{medal,15},{9001,8},{9002,6}]"
})
table.insert(enhance, {
  type = 106003,
  rank = 6,
  level_limit = 30,
  consume = "[]"
})
table.insert(enhance, {
  type = 100004,
  rank = 0,
  level_limit = 5,
  consume = "[{medal,2},{9001,2},{9002,0}]"
})
table.insert(enhance, {
  type = 101004,
  rank = 1,
  level_limit = 10,
  consume = "[{medal,5},{9001,2},{9002,1}]"
})
table.insert(enhance, {
  type = 102004,
  rank = 2,
  level_limit = 15,
  consume = "[{medal,8},{9001,2},{9002,2}]"
})
table.insert(enhance, {
  type = 103004,
  rank = 3,
  level_limit = 20,
  consume = "[{medal,10},{9001,6},{9002,2}]"
})
table.insert(enhance, {
  type = 104004,
  rank = 4,
  level_limit = 25,
  consume = "[{medal,12},{9001,6},{9002,4}]"
})
table.insert(enhance, {
  type = 105004,
  rank = 5,
  level_limit = 30,
  consume = "[{medal,15},{9001,8},{9002,6}]"
})
table.insert(enhance, {
  type = 106004,
  rank = 6,
  level_limit = 30,
  consume = "[]"
})
table.insert(enhance, {
  type = 100005,
  rank = 0,
  level_limit = 5,
  consume = "[{medal,2},{9001,2},{9002,0}]"
})
table.insert(enhance, {
  type = 101005,
  rank = 1,
  level_limit = 10,
  consume = "[{medal,5},{9001,2},{9002,1}]"
})
table.insert(enhance, {
  type = 102005,
  rank = 2,
  level_limit = 15,
  consume = "[{medal,8},{9001,2},{9002,2}]"
})
table.insert(enhance, {
  type = 103005,
  rank = 3,
  level_limit = 20,
  consume = "[{medal,10},{9001,6},{9002,2}]"
})
table.insert(enhance, {
  type = 104005,
  rank = 4,
  level_limit = 25,
  consume = "[{medal,12},{9001,6},{9002,4}]"
})
table.insert(enhance, {
  type = 105005,
  rank = 5,
  level_limit = 30,
  consume = "[{medal,15},{9001,8},{9002,6}]"
})
table.insert(enhance, {
  type = 106005,
  rank = 6,
  level_limit = 30,
  consume = "[]"
})
table.insert(enhance, {
  type = 200001,
  rank = 0,
  level_limit = 5,
  consume = "[{medal,2},{9001,3},{9002,2}]"
})
table.insert(enhance, {
  type = 201001,
  rank = 1,
  level_limit = 10,
  consume = "[{medal,5},{9001,6},{9002,4}]"
})
table.insert(enhance, {
  type = 202001,
  rank = 2,
  level_limit = 15,
  consume = "[{medal,8},{9001,15},{9002,5}]"
})
table.insert(enhance, {
  type = 203001,
  rank = 3,
  level_limit = 20,
  consume = "[{medal,10},{9001,22},{9002,8}]"
})
table.insert(enhance, {
  type = 204001,
  rank = 4,
  level_limit = 25,
  consume = "[{medal,12},{9001,26},{9002,12}]"
})
table.insert(enhance, {
  type = 205001,
  rank = 5,
  level_limit = 30,
  consume = "[{medal,15},{9001,35},{9002,20}]"
})
table.insert(enhance, {
  type = 206001,
  rank = 6,
  level_limit = 30,
  consume = "[]"
})
table.insert(enhance, {
  type = 200002,
  rank = 0,
  level_limit = 5,
  consume = "[{medal,2},{9001,3},{9002,2}]"
})
table.insert(enhance, {
  type = 201002,
  rank = 1,
  level_limit = 10,
  consume = "[{medal,5},{9001,6},{9002,4}]"
})
table.insert(enhance, {
  type = 202002,
  rank = 2,
  level_limit = 15,
  consume = "[{medal,8},{9001,15},{9002,5}]"
})
table.insert(enhance, {
  type = 203002,
  rank = 3,
  level_limit = 20,
  consume = "[{medal,10},{9001,22},{9002,8}]"
})
table.insert(enhance, {
  type = 204002,
  rank = 4,
  level_limit = 25,
  consume = "[{medal,12},{9001,26},{9002,12}]"
})
table.insert(enhance, {
  type = 205002,
  rank = 5,
  level_limit = 30,
  consume = "[{medal,15},{9001,35},{9002,20}]"
})
table.insert(enhance, {
  type = 206002,
  rank = 6,
  level_limit = 30,
  consume = "[]"
})
table.insert(enhance, {
  type = 200003,
  rank = 0,
  level_limit = 5,
  consume = "[{medal,2},{9001,3},{9002,2}]"
})
table.insert(enhance, {
  type = 201003,
  rank = 1,
  level_limit = 10,
  consume = "[{medal,5},{9001,6},{9002,4}]"
})
table.insert(enhance, {
  type = 202003,
  rank = 2,
  level_limit = 15,
  consume = "[{medal,8},{9001,15},{9002,5}]"
})
table.insert(enhance, {
  type = 203003,
  rank = 3,
  level_limit = 20,
  consume = "[{medal,10},{9001,22},{9002,8}]"
})
table.insert(enhance, {
  type = 204003,
  rank = 4,
  level_limit = 25,
  consume = "[{medal,12},{9001,26},{9002,12}]"
})
table.insert(enhance, {
  type = 205003,
  rank = 5,
  level_limit = 30,
  consume = "[{medal,15},{9001,35},{9002,20}]"
})
table.insert(enhance, {
  type = 206003,
  rank = 6,
  level_limit = 30,
  consume = "[]"
})
table.insert(enhance, {
  type = 200004,
  rank = 0,
  level_limit = 5,
  consume = "[{medal,2},{9001,3},{9002,2}]"
})
table.insert(enhance, {
  type = 201004,
  rank = 1,
  level_limit = 10,
  consume = "[{medal,5},{9001,6},{9002,4}]"
})
table.insert(enhance, {
  type = 202004,
  rank = 2,
  level_limit = 15,
  consume = "[{medal,8},{9001,15},{9002,5}]"
})
table.insert(enhance, {
  type = 203004,
  rank = 3,
  level_limit = 20,
  consume = "[{medal,10},{9001,22},{9002,8}]"
})
table.insert(enhance, {
  type = 204004,
  rank = 4,
  level_limit = 25,
  consume = "[{medal,12},{9001,26},{9002,12}]"
})
table.insert(enhance, {
  type = 205004,
  rank = 5,
  level_limit = 30,
  consume = "[{medal,15},{9001,35},{9002,20}]"
})
table.insert(enhance, {
  type = 206004,
  rank = 6,
  level_limit = 30,
  consume = "[]"
})
table.insert(enhance, {
  type = 200005,
  rank = 0,
  level_limit = 5,
  consume = "[{medal,2},{9001,3},{9002,2}]"
})
table.insert(enhance, {
  type = 201005,
  rank = 1,
  level_limit = 10,
  consume = "[{medal,5},{9001,6},{9002,4}]"
})
table.insert(enhance, {
  type = 202005,
  rank = 2,
  level_limit = 15,
  consume = "[{medal,8},{9001,15},{9002,5}]"
})
table.insert(enhance, {
  type = 203005,
  rank = 3,
  level_limit = 20,
  consume = "[{medal,10},{9001,22},{9002,8}]"
})
table.insert(enhance, {
  type = 204005,
  rank = 4,
  level_limit = 25,
  consume = "[{medal,12},{9001,26},{9002,12}]"
})
table.insert(enhance, {
  type = 205005,
  rank = 5,
  level_limit = 30,
  consume = "[{medal,15},{9001,35},{9002,20}]"
})
table.insert(enhance, {
  type = 206005,
  rank = 6,
  level_limit = 30,
  consume = "[]"
})
table.insert(enhance, {
  type = 300001,
  rank = 0,
  level_limit = 5,
  consume = "[{medal,1},{9001,5},{9002,2}]"
})
table.insert(enhance, {
  type = 301001,
  rank = 1,
  level_limit = 10,
  consume = "[{medal,2},{9001,10},{9002,4}]"
})
table.insert(enhance, {
  type = 302001,
  rank = 2,
  level_limit = 15,
  consume = "[{medal,2},{9001,20},{9002,12}]"
})
table.insert(enhance, {
  type = 303001,
  rank = 3,
  level_limit = 20,
  consume = "[{medal,2},{9001,30},{9002,16}]"
})
table.insert(enhance, {
  type = 304001,
  rank = 4,
  level_limit = 25,
  consume = "[{medal,3},{9001,30},{9002,30}]"
})
table.insert(enhance, {
  type = 305001,
  rank = 5,
  level_limit = 30,
  consume = "[{medal,3},{9001,50},{9002,41}]"
})
table.insert(enhance, {
  type = 306001,
  rank = 6,
  level_limit = 30,
  consume = "[]"
})
table.insert(enhance, {
  type = 300002,
  rank = 0,
  level_limit = 5,
  consume = "[{medal,1},{9001,5},{9002,2}]"
})
table.insert(enhance, {
  type = 301002,
  rank = 1,
  level_limit = 10,
  consume = "[{medal,2},{9001,10},{9002,4}]"
})
table.insert(enhance, {
  type = 302002,
  rank = 2,
  level_limit = 15,
  consume = "[{medal,2},{9001,20},{9002,12}]"
})
table.insert(enhance, {
  type = 303002,
  rank = 3,
  level_limit = 20,
  consume = "[{medal,2},{9001,30},{9002,16}]"
})
table.insert(enhance, {
  type = 304002,
  rank = 4,
  level_limit = 25,
  consume = "[{medal,3},{9001,30},{9002,30}]"
})
table.insert(enhance, {
  type = 305002,
  rank = 5,
  level_limit = 30,
  consume = "[{medal,3},{9001,50},{9002,41}]"
})
table.insert(enhance, {
  type = 306002,
  rank = 6,
  level_limit = 30,
  consume = "[]"
})
table.insert(enhance, {
  type = 300003,
  rank = 0,
  level_limit = 5,
  consume = "[{medal,1},{9001,5},{9002,2}]"
})
table.insert(enhance, {
  type = 301003,
  rank = 1,
  level_limit = 10,
  consume = "[{medal,2},{9001,10},{9002,4}]"
})
table.insert(enhance, {
  type = 302003,
  rank = 2,
  level_limit = 15,
  consume = "[{medal,2},{9001,20},{9002,12}]"
})
table.insert(enhance, {
  type = 303003,
  rank = 3,
  level_limit = 20,
  consume = "[{medal,2},{9001,30},{9002,16}]"
})
table.insert(enhance, {
  type = 304003,
  rank = 4,
  level_limit = 25,
  consume = "[{medal,3},{9001,30},{9002,30}]"
})
table.insert(enhance, {
  type = 305003,
  rank = 5,
  level_limit = 30,
  consume = "[{medal,3},{9001,50},{9002,41}]"
})
table.insert(enhance, {
  type = 306003,
  rank = 6,
  level_limit = 30,
  consume = "[]"
})
table.insert(enhance, {
  type = 300004,
  rank = 0,
  level_limit = 5,
  consume = "[{medal,1},{9001,5},{9002,2}]"
})
table.insert(enhance, {
  type = 301004,
  rank = 1,
  level_limit = 10,
  consume = "[{medal,2},{9001,10},{9002,4}]"
})
table.insert(enhance, {
  type = 302004,
  rank = 2,
  level_limit = 15,
  consume = "[{medal,2},{9001,20},{9002,12}]"
})
table.insert(enhance, {
  type = 303004,
  rank = 3,
  level_limit = 20,
  consume = "[{medal,2},{9001,30},{9002,16}]"
})
table.insert(enhance, {
  type = 304004,
  rank = 4,
  level_limit = 25,
  consume = "[{medal,3},{9001,30},{9002,30}]"
})
table.insert(enhance, {
  type = 305004,
  rank = 5,
  level_limit = 30,
  consume = "[{medal,3},{9001,50},{9002,41}]"
})
table.insert(enhance, {
  type = 306004,
  rank = 6,
  level_limit = 30,
  consume = "[]"
})
table.insert(enhance, {
  type = 300005,
  rank = 0,
  level_limit = 5,
  consume = "[{medal,1},{9001,5},{9002,2}]"
})
table.insert(enhance, {
  type = 301005,
  rank = 1,
  level_limit = 10,
  consume = "[{medal,2},{9001,10},{9002,4}]"
})
table.insert(enhance, {
  type = 302005,
  rank = 2,
  level_limit = 15,
  consume = "[{medal,2},{9001,20},{9002,12}]"
})
table.insert(enhance, {
  type = 303005,
  rank = 3,
  level_limit = 20,
  consume = "[{medal,2},{9001,30},{9002,16}]"
})
table.insert(enhance, {
  type = 304005,
  rank = 4,
  level_limit = 25,
  consume = "[{medal,3},{9001,30},{9002,30}]"
})
table.insert(enhance, {
  type = 305005,
  rank = 5,
  level_limit = 30,
  consume = "[{medal,3},{9001,50},{9002,41}]"
})
table.insert(enhance, {
  type = 306005,
  rank = 6,
  level_limit = 30,
  consume = "[]"
})
table.insert(enhance, {
  type = 400001,
  rank = 0,
  level_limit = 5,
  consume = "[{medal,1},{9001,5},{9002,4}]"
})
table.insert(enhance, {
  type = 401001,
  rank = 1,
  level_limit = 10,
  consume = "[{medal,1},{9001,10},{9002,8}]"
})
table.insert(enhance, {
  type = 402001,
  rank = 2,
  level_limit = 15,
  consume = "[{medal,2},{9001,20},{9002,10}]"
})
table.insert(enhance, {
  type = 403001,
  rank = 3,
  level_limit = 20,
  consume = "[{medal,2},{9001,30},{9002,18}]"
})
table.insert(enhance, {
  type = 404001,
  rank = 4,
  level_limit = 25,
  consume = "[{medal,3},{9001,30},{9002,30}]"
})
table.insert(enhance, {
  type = 405001,
  rank = 5,
  level_limit = 30,
  consume = "[{medal,3},{9001,50},{9002,58}]"
})
table.insert(enhance, {
  type = 406001,
  rank = 6,
  level_limit = 30,
  consume = "[]"
})
table.insert(enhance, {
  type = 400002,
  rank = 0,
  level_limit = 5,
  consume = "[{medal,1},{9001,5},{9002,4}]"
})
table.insert(enhance, {
  type = 401002,
  rank = 1,
  level_limit = 10,
  consume = "[{medal,1},{9001,10},{9002,8}]"
})
table.insert(enhance, {
  type = 402002,
  rank = 2,
  level_limit = 15,
  consume = "[{medal,2},{9001,20},{9002,10}]"
})
table.insert(enhance, {
  type = 403002,
  rank = 3,
  level_limit = 20,
  consume = "[{medal,2},{9001,30},{9002,18}]"
})
table.insert(enhance, {
  type = 404002,
  rank = 4,
  level_limit = 25,
  consume = "[{medal,3},{9001,30},{9002,30}]"
})
table.insert(enhance, {
  type = 405002,
  rank = 5,
  level_limit = 30,
  consume = "[{medal,3},{9001,50},{9002,58}]"
})
table.insert(enhance, {
  type = 406002,
  rank = 6,
  level_limit = 30,
  consume = "[]"
})
table.insert(enhance, {
  type = 400003,
  rank = 0,
  level_limit = 5,
  consume = "[{medal,1},{9001,5},{9002,4}]"
})
table.insert(enhance, {
  type = 401003,
  rank = 1,
  level_limit = 10,
  consume = "[{medal,1},{9001,10},{9002,8}]"
})
table.insert(enhance, {
  type = 402003,
  rank = 2,
  level_limit = 15,
  consume = "[{medal,2},{9001,20},{9002,10}]"
})
table.insert(enhance, {
  type = 403003,
  rank = 3,
  level_limit = 20,
  consume = "[{medal,2},{9001,30},{9002,18}]"
})
table.insert(enhance, {
  type = 404003,
  rank = 4,
  level_limit = 25,
  consume = "[{medal,3},{9001,30},{9002,30}]"
})
table.insert(enhance, {
  type = 405003,
  rank = 5,
  level_limit = 30,
  consume = "[{medal,3},{9001,50},{9002,58}]"
})
table.insert(enhance, {
  type = 406003,
  rank = 6,
  level_limit = 30,
  consume = "[]"
})
table.insert(enhance, {
  type = 400004,
  rank = 0,
  level_limit = 5,
  consume = "[{medal,1},{9001,5},{9002,4}]"
})
table.insert(enhance, {
  type = 401004,
  rank = 1,
  level_limit = 10,
  consume = "[{medal,1},{9001,10},{9002,8}]"
})
table.insert(enhance, {
  type = 402004,
  rank = 2,
  level_limit = 15,
  consume = "[{medal,2},{9001,20},{9002,10}]"
})
table.insert(enhance, {
  type = 403004,
  rank = 3,
  level_limit = 20,
  consume = "[{medal,2},{9001,30},{9002,18}]"
})
table.insert(enhance, {
  type = 404004,
  rank = 4,
  level_limit = 25,
  consume = "[{medal,3},{9001,30},{9002,30}]"
})
table.insert(enhance, {
  type = 405004,
  rank = 5,
  level_limit = 30,
  consume = "[{medal,3},{9001,50},{9002,58}]"
})
table.insert(enhance, {
  type = 406004,
  rank = 6,
  level_limit = 30,
  consume = "[]"
})
table.insert(enhance, {
  type = 400005,
  rank = 0,
  level_limit = 5,
  consume = "[{medal,1},{9001,5},{9002,4}]"
})
table.insert(enhance, {
  type = 401005,
  rank = 1,
  level_limit = 10,
  consume = "[{medal,1},{9001,10},{9002,8}]"
})
table.insert(enhance, {
  type = 402005,
  rank = 2,
  level_limit = 15,
  consume = "[{medal,2},{9001,20},{9002,10}]"
})
table.insert(enhance, {
  type = 403005,
  rank = 3,
  level_limit = 20,
  consume = "[{medal,2},{9001,30},{9002,18}]"
})
table.insert(enhance, {
  type = 404005,
  rank = 4,
  level_limit = 25,
  consume = "[{medal,3},{9001,30},{9002,30}]"
})
table.insert(enhance, {
  type = 405005,
  rank = 5,
  level_limit = 30,
  consume = "[{medal,3},{9001,50},{9002,58}]"
})
table.insert(enhance, {
  type = 406005,
  rank = 6,
  level_limit = 30,
  consume = "[]"
})
table.insert(enhance, {
  type = 500001,
  rank = 0,
  level_limit = 5,
  consume = "[{medal,1},{9001,5},{9002,10}]"
})
table.insert(enhance, {
  type = 501001,
  rank = 1,
  level_limit = 10,
  consume = "[{medal,1},{9001,10},{9002,20}]"
})
table.insert(enhance, {
  type = 502001,
  rank = 2,
  level_limit = 15,
  consume = "[{medal,2},{9001,20},{9002,16}]"
})
table.insert(enhance, {
  type = 503001,
  rank = 3,
  level_limit = 20,
  consume = "[{medal,2},{9001,30},{9002,24}]"
})
table.insert(enhance, {
  type = 504001,
  rank = 4,
  level_limit = 25,
  consume = "[{medal,2},{9001,30},{9002,48}]"
})
table.insert(enhance, {
  type = 505001,
  rank = 5,
  level_limit = 30,
  consume = "[{medal,3},{9001,50},{9002,64}]"
})
table.insert(enhance, {
  type = 506001,
  rank = 6,
  level_limit = 30,
  consume = "[]"
})
table.insert(enhance, {
  type = 500002,
  rank = 0,
  level_limit = 5,
  consume = "[{medal,1},{9001,5},{9002,10}]"
})
table.insert(enhance, {
  type = 501002,
  rank = 1,
  level_limit = 10,
  consume = "[{medal,1},{9001,10},{9002,20}]"
})
table.insert(enhance, {
  type = 502002,
  rank = 2,
  level_limit = 15,
  consume = "[{medal,2},{9001,20},{9002,16}]"
})
table.insert(enhance, {
  type = 503002,
  rank = 3,
  level_limit = 20,
  consume = "[{medal,2},{9001,30},{9002,24}]"
})
table.insert(enhance, {
  type = 504002,
  rank = 4,
  level_limit = 25,
  consume = "[{medal,2},{9001,30},{9002,48}]"
})
table.insert(enhance, {
  type = 505002,
  rank = 5,
  level_limit = 30,
  consume = "[{medal,3},{9001,50},{9002,64}]"
})
table.insert(enhance, {
  type = 506002,
  rank = 6,
  level_limit = 30,
  consume = "[]"
})
table.insert(enhance, {
  type = 500003,
  rank = 0,
  level_limit = 5,
  consume = "[{medal,1},{9001,5},{9002,10}]"
})
table.insert(enhance, {
  type = 501003,
  rank = 1,
  level_limit = 10,
  consume = "[{medal,1},{9001,10},{9002,20}]"
})
table.insert(enhance, {
  type = 502003,
  rank = 2,
  level_limit = 15,
  consume = "[{medal,2},{9001,20},{9002,16}]"
})
table.insert(enhance, {
  type = 503003,
  rank = 3,
  level_limit = 20,
  consume = "[{medal,2},{9001,30},{9002,24}]"
})
table.insert(enhance, {
  type = 504003,
  rank = 4,
  level_limit = 25,
  consume = "[{medal,2},{9001,30},{9002,48}]"
})
table.insert(enhance, {
  type = 505003,
  rank = 5,
  level_limit = 30,
  consume = "[{medal,3},{9001,50},{9002,64}]"
})
table.insert(enhance, {
  type = 506003,
  rank = 6,
  level_limit = 30,
  consume = "[]"
})
table.insert(enhance, {
  type = 500004,
  rank = 0,
  level_limit = 5,
  consume = "[{medal,1},{9001,5},{9002,10}]"
})
table.insert(enhance, {
  type = 501004,
  rank = 1,
  level_limit = 10,
  consume = "[{medal,1},{9001,10},{9002,20}]"
})
table.insert(enhance, {
  type = 502004,
  rank = 2,
  level_limit = 15,
  consume = "[{medal,2},{9001,20},{9002,16}]"
})
table.insert(enhance, {
  type = 503004,
  rank = 3,
  level_limit = 20,
  consume = "[{medal,2},{9001,30},{9002,24}]"
})
table.insert(enhance, {
  type = 504004,
  rank = 4,
  level_limit = 25,
  consume = "[{medal,2},{9001,30},{9002,48}]"
})
table.insert(enhance, {
  type = 505004,
  rank = 5,
  level_limit = 30,
  consume = "[{medal,3},{9001,50},{9002,64}]"
})
table.insert(enhance, {
  type = 506004,
  rank = 6,
  level_limit = 30,
  consume = "[]"
})
table.insert(enhance, {
  type = 500005,
  rank = 0,
  level_limit = 5,
  consume = "[{medal,1},{9001,5},{9002,10}]"
})
table.insert(enhance, {
  type = 501005,
  rank = 1,
  level_limit = 10,
  consume = "[{medal,1},{9001,10},{9002,20}]"
})
table.insert(enhance, {
  type = 502005,
  rank = 2,
  level_limit = 15,
  consume = "[{medal,2},{9001,20},{9002,16}]"
})
table.insert(enhance, {
  type = 503005,
  rank = 3,
  level_limit = 20,
  consume = "[{medal,2},{9001,30},{9002,24}]"
})
table.insert(enhance, {
  type = 504005,
  rank = 4,
  level_limit = 25,
  consume = "[{medal,2},{9001,30},{9002,48}]"
})
table.insert(enhance, {
  type = 505005,
  rank = 5,
  level_limit = 30,
  consume = "[{medal,3},{9001,50},{9002,64}]"
})
table.insert(enhance, {
  type = 506005,
  rank = 6,
  level_limit = 30,
  consume = "[]"
})
