require("data1/NetProtocalPaser.tfl")
local NetAPIList = NetAPIList
local GameSetting = LuaObjectManager:GetLuaObject("GameSetting")
NetMessageMgr = {}
NetMessageMgr.CallbackList = {}
NetMessageMgr.CallbacksWhenReconnect = {}
NetMessageMgr.HEARTBEATT_INTERVAL = 60000
NetMessageMgr.HeartbeatTimer = -1
NetMessageMgr.ReConnectCallback = nil
NetMessageMgr.ReConnectSuccessCallList = {}
NetMessageMgr.MsgCache = {}
NetMessageMgr.HeaderSize = 2
NetMessageMgr.PendSendMsg = {}
function NetMessageMgr:AddReconnectSuccessCall(call)
  if nil == NetMessageMgr.ReConnectSuccessCallList then
    NetMessageMgr.ReConnectSuccessCallList = {}
  end
  table.insert(NetMessageMgr.ReConnectSuccessCallList, call)
end
function NetMessageMgr:InitNet(callback)
  local serverAdd, port, domain_port
  local login_info = GameUtils:GetLoginInfo()
  local server_info = login_info.ServerInfo
  if server_info then
    serverAdd = server_info.ip
    port = server_info.port
    domain_port = server_info.domain_port
  else
    local infoContent = "can not find a valid server"
    local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
    local title = GameLoader:GetGameText("LC_MENU_NOTIFICATION_CHAR")
    local GameSetting = LuaObjectManager:GetLuaObject("GameSetting")
    GameSetting:showContactUsDialogForNetError(title, infoContent, function()
      GameUtils:RestartGame(200)
    end)
    if callback then
      callback(-1)
    end
    return false
  end
  if server_info.is_in_maintain then
    NetMessageMgr:ShowNotice()
    if callback then
      callback(-1)
    end
    return false
  end
  if GameUtils:GetNeedSpeed() and server_info.is_mix then
    server_info.is_mix = true
    local localAppid = ext.GetBundleIdentifier()
    if AutoUpdate.localAppVersion < 10607 then
      GameUtils:ShowVersonError(localAppid)
      if callback then
        callback(-1)
      end
      return false
    end
  else
    server_info.is_mix = false
  end
  if ext.socket.setHeaderSize then
    ext.socket.setHeaderSize(2)
  end
  NetMessageMgr.HeaderSize = 2
  if server_info.is_mix and server_info.domain ~= "" and domain_port then
    DebugOut("trying login to server : " .. server_info.domain .. " with port(" .. tostring(domain_port) .. ")" .. "needEncrypt")
    if server_info.domain and ext.socket.connectToServer(server_info.domain, domain_port, 1) then
      DebugOut("login success ...")
      if not login_info.PlayerInfo then
        DebugOut("[udid] \232\191\158\230\142\165\230\156\141\229\138\161\229\153\168\229\174\140\230\136\144")
        self:SendUdidStepReq(1)
      end
      if callback then
        callback(0)
      end
      return true
    elseif server_info.domain2 and ext.socket.connectToServer(server_info.domain2, domain_port, 1) then
      DebugOut("login success ...")
      if not login_info.PlayerInfo then
        DebugOut("[udid] \232\191\158\230\142\165\230\156\141\229\138\161\229\153\168\229\174\140\230\136\144")
        self:SendUdidStepReq(1)
      end
      if callback then
        callback(0)
      end
      return true
    else
      if callback then
        callback(-1)
      end
      return false
    end
  else
    DebugOut("trying login to server : " .. serverAdd .. " with port(" .. tostring(port) .. ")" .. "needEncrypt")
    if g_SaveNewFteData then
      ext.sendNewFteData("FTE_TrySocketConnectStarted")
    end
    if ext.socket.connectToServer(serverAdd, port, 1) then
      DebugOut("login success ...")
      if not login_info.PlayerInfo then
        DebugOut("[udid] \232\191\158\230\142\165\230\156\141\229\138\161\229\153\168\229\174\140\230\136\144")
        self:SendUdidStepReq(1)
      end
      if g_SaveNewFteData then
        ext.sendNewFteData("FTE_TrySocketConnectResultSuccessed")
      end
      if callback then
        callback(0)
      end
      return true
    else
      if callback then
        callback(-1)
      end
      return false
    end
  end
end
function NetMessageMgr:ServerInMaintaining()
  local login_info = GameUtils:GetLoginInfo()
  local server_info = login_info.ServerInfo
  local serverName = " " .. server_info.name or server_info.logic_id or ""
  local infoContent = GameLoader:GetGameText("LC_MENU_SERVER_UNDER_MAINTENANCE_CHAR") .. serverName
  local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
  local GameWaiting = LuaObjectManager:GetLuaObject("GameWaiting")
  local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
  local title = GameLoader:GetGameText("LC_MENU_NOTIFICATION_CHAR")
  local GameSetting = LuaObjectManager:GetLuaObject("GameSetting")
  GameSetting:showContactUsDialogForNetError(title, infoContent, function()
    GameUtils:RestartGame(200)
  end)
end
function NetMessageMgr:ShowNotice()
  local requestType = "servers/" .. GameUtils:GetActiveServerInfo().id .. "/server_notice"
  local requestParam = {
    lang = GameSettingData.Save_Lang
  }
  GameUtils:HTTP_SendRequest(requestType, requestParam, NetMessageMgr.RequestNewsCallback, true, GameUtils.httpRequestFailedCallback, "GET", "notencrypt")
end
function NetMessageMgr.RequestNewsCallback(content)
  DebugOut("notice = ")
  DebugTable(content)
  local notice
  for i, v in pairs(content) do
    if string.find(v.notice_type, "announce_close_server") then
      notice = v
    end
  end
  if notice then
    local GameUIStopNotice = LuaObjectManager:GetLuaObject("GameUIStopNotice")
    GameUIStopNotice:Show(notice)
  else
    NetMessageMgr:ServerInMaintaining()
  end
  return true
end
function NetMessageMgr:SendFlurryReq(eventName)
  DebugOut("NetMessageMgr:SendFlurryReq(" .. eventName .. ")")
  local udid_track_req = {
    market = ext.GetChannel(),
    terminal = ext.GetDeviceName(),
    udid = ext.GetIOSOpenUdid(),
    track = eventName
  }
  local function failedCallback()
    NetMessageMgr:SendMsg(NetAPIList.udid_track_req.Code, udid_track_req, nil, false, failedCallback)
  end
  NetMessageMgr:SendMsg(NetAPIList.udid_track_req.Code, udid_track_req, nil, false, failedCallback)
end
function NetMessageMgr:SendProtocalVersion()
  local param = {
    version = AutoUpdate.localAppVersion
  }
  local function callback(msgType, content)
    if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.protocal_version_req.Code then
      DebugOut("sey header")
      if ext.socket.setHeaderSize then
        ext.socket.setHeaderSize(4)
      end
      NetMessageMgr.HeaderSize = 4
      return true
    end
    return false
  end
  local function _failCallBack()
    NetMessageMgr:SendMsg(NetAPIList.protocal_version_req.Code, param, callback, false, _failCallBack)
  end
  NetMessageMgr:SendMsg(NetAPIList.protocal_version_req.Code, param, callback, false, _failCallBack)
end
function NetMessageMgr:SendUdidStepReq(step)
  DebugOut("NetMessageMgr:SendUdidStepReq(" .. step .. ")")
  local udid_step_req = {
    market = ext.GetChannel(),
    terminal = ext.GetDeviceName(),
    udid = ext.GetIOSOpenUdid(),
    step = step
  }
  DebugTable(udid_step_req)
  NetMessageMgr:SendMsg(NetAPIList.udid_step_req.Code, udid_step_req, nil, false, nil)
end
function NetMessageMgr:SendLoginLog()
  NetMessageMgr:SendMsg(NetAPIList.client_login_ok_req.Code, nil, nil, false, nil)
end
function NetMessageMgr.ServerKickNotifyHandler(content)
  if content.action then
    local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
    local title = GameLoader:GetGameText("LC_MENU_NOTIFICATION_CHAR")
    local serverName = " " .. GameUtils:GetActiveServerInfo().name or GameUtils:GetActiveServerInfo().logic_id or ""
    local content = GameLoader:GetGameText("LC_MENU_SERVER_KICK_YOU_OUT_CHAR") .. serverName
    local GameSetting = LuaObjectManager:GetLuaObject("GameSetting")
    GameSetting:showContactUsDialogForNetError(title, content, function()
      GameUtils:RestartGame(200)
    end)
  else
    assert(false)
  end
end
function NetMessageMgr.HandleKeyList(content)
  DebugOut("==========handle keylist =========")
  DebugTable(content)
  local seed = content.keys[1]
  ext.socket.initPike(seed, seed)
end
function NetMessageMgr.HandleQihooID(content)
  GameUtils:printByAndroid("NetMessageMgr.HandleQihooID")
  if content then
    GameUtils:DebugOutTableInAndroid(content)
    IPlatformExt.setQihooID(ext.json.generate(content))
  end
end
function NetMessageMgr:InitMsgHandle()
  self.NetMsgHandles = {}
  local MsgHandles = self.NetMsgHandles
  local GameUIBarLeft = LuaObjectManager:GetLuaObject("GameUIBarLeft")
  local GameUIChat = LuaObjectManager:GetLuaObject("GameUIChat")
  local GameItemBag = LuaObjectManager:GetLuaObject("GameItemBag")
  local GameObjectLevelUp = LuaObjectManager:GetLuaObject("GameObjectLevelUp")
  local NewsTip = LuaObjectManager:GetLuaObject("NewsTip")
  local GameObjectMineMap = LuaObjectManager:GetLuaObject("GameObjectMineMap")
  local GameObjectGacha = LuaObjectManager:GetLuaObject("GameObjectGacha")
  local GameUILab = LuaObjectManager:GetLuaObject("GameUILab")
  local GameObjectCosmicExpedition = LuaObjectManager:GetLuaObject("GameObjectCosmicExpedition")
  local GameUIUserAlliance = LuaObjectManager:GetLuaObject("GameUIUserAlliance")
  local GameUIBarLeft = LuaObjectManager:GetLuaObject("GameUIBarLeft")
  local GameUIFirstCharge = LuaObjectManager:GetLuaObject("GameUIFirstCharge")
  local GameUIActivity = LuaObjectManager:GetLuaObject("GameUIActivity")
  local GameUIKrypton = LuaObjectManager:GetLuaObject("GameUIKrypton")
  local GameStateWorldBoss = GameStateManager.GameStateWorldBoss
  local GameUIColonial = LuaObjectManager:GetLuaObject("GameUIColonial")
  local GameUIBattleResult = LuaObjectManager:GetLuaObject("GameUIBattleResult")
  local GameUIWDDefence = LuaObjectManager:GetLuaObject("GameUIWDDefence")
  local GameQuestMenu = LuaObjectManager:GetLuaObject("GameQuestMenu")
  local GameStateWD = GameStateManager.GameStateWD
  local GameUIActivityNew = LuaObjectManager:GetLuaObject("GameUIActivityNew")
  local GameVip = LuaObjectManager:GetLuaObject("GameVip")
  local GameObjectAdventure = LuaObjectManager:GetLuaObject("GameObjectAdventure")
  local GameObjectMainPlanet = LuaObjectManager:GetLuaObject("GameObjectMainPlanet")
  local GameUIRebuildFleet = LuaObjectManager:GetLuaObject("GameUIRebuildFleet")
  local GameUIConsortium = LuaObjectManager:GetLuaObject("GameUIConsortium")
  local GameUIPrestigeRankUp = LuaObjectManager:GetLuaObject("GameUIPrestigeRankUp")
  local GameUIArena = LuaObjectManager:GetLuaObject("GameUIArena")
  local GameUIRechargePush = LuaObjectManager:GetLuaObject("GameUIRechargePush")
  local GameUIWebView = LuaObjectManager:GetLuaObject("GameUIWebView")
  local GameUISevenBenefit = LuaObjectManager:GetLuaObject("GameUISevenBenefit")
  local GameUIDice = LuaObjectManager:GetLuaObject("GameUIDice")
  local GameUIMaster = LuaObjectManager:GetLuaObject("GameUIMaster")
  local GameUIMiningWars = LuaObjectManager:GetLuaObject("GameUIMiningWars")
  local GameUIMiningWorld = LuaObjectManager:GetLuaObject("GameUIMiningWorld")
  local GameGoodsList = LuaObjectManager:GetLuaObject("GameGoodsList")
  local GameStateBattlePlay = GameStateManager.GameStateBattlePlay
  local GameUIScratchGacha = LuaObjectManager:GetLuaObject("GameUIScratchGacha")
  local GameUIStarSystemMedalEquip = LuaObjectManager:GetLuaObject("GameUIStarSystemPort").medalEquip
  local GameUILargeMap = LuaObjectManager:GetLuaObject("GameUILargeMap")
  local GameUILargeMapUI = LuaObjectManager:GetLuaObject("GameUILargeMapUI")
  local GameUILargeMapPop = LuaObjectManager:GetLuaObject("GameUILargeMapPop")
  local GameUILargeMapAlliance = LuaObjectManager:GetLuaObject("GameUILargeMapAlliance")
  local GameObjectClimbTower = LuaObjectManager:GetLuaObject("GameObjectClimbTower")
  local GameFleetEquipment = LuaObjectManager:GetLuaObject("GameFleetEquipment")
  local GameObjectFirstPayPush = LuaObjectManager:GetLuaObject("GameObjectFirstPayPush")
  local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
  MsgHandles[NetAPIList.keylist_ntf.Code] = NetMessageMgr.HandleKeyList
  MsgHandles[NetAPIList.login_token_ntf.Code] = NetMessageMgr.HandleQihooID
  MsgHandles[NetAPIList.chat_ntf.Code] = NetMessageHandler.ChatMessageNotifyHandler
  MsgHandles[NetAPIList.chat_error_ntf.Code] = GameUIChat.ChatFailedNotifyHandler
  MsgHandles[NetAPIList.guide_progress_ntf.Code] = TutorialQuestManager.UpdateTutorial
  MsgHandles[NetAPIList.cd_time_ntf.Code] = GameGlobalData.UpdateCDTime
  MsgHandles[NetAPIList.resource_ntf.Code] = GameGlobalData.UpdateResource
  MsgHandles[NetAPIList.alliance_info_ntf.Code] = GameStateManager.GameStateAlliance.AllianceInfoNotifyHandler
  MsgHandles[NetAPIList.achievement_finish_ntf.Code] = GameGlobalData._AchievementCompleteNotifyHandler
  MsgHandles[NetAPIList.level_ntf.Code] = GameGlobalData.UpdateLevel
  MsgHandles[NetAPIList.fleets_ntf.Code] = GameGlobalData.UpdateFleets
  MsgHandles[NetAPIList.fleet_kryptons_ntf.Code] = GameGlobalData.UpdateFleetKrypton
  MsgHandles[NetAPIList.equipments_update_ntf.Code] = GameGlobalData.UpdateEquipments
  MsgHandles[NetAPIList.supply_ntf.Code] = GameGlobalData._SupplyPointNotifyHandler
  MsgHandles[NetAPIList.progress_ntf.Code] = GameGlobalData.UpdateProgress
  MsgHandles[NetAPIList.buildings_ntf.Code] = GameGlobalData.UpdateBuildings
  MsgHandles[NetAPIList.offline_changed_ntf.Code] = GameGlobalData.UpdateOffline
  MsgHandles[NetAPIList.fleet_matrix_ntf.Code] = GameGlobalData._UserMatrixNotifyHandler
  MsgHandles[NetAPIList.adventure_chapter_status_ntf.Code] = GameObjectAdventure.AdvChapterStatusNtfHandler
  MsgHandles[NetAPIList.task_finished_ntf.Code] = GameGlobalData.RefreshTask
  MsgHandles[NetAPIList.today_task_ntf.Code] = GameGlobalData.RefreshTodayTask
  MsgHandles[NetAPIList.mail_unread_ntf.Code] = GameGlobalData.RefreshUnreadMail
  MsgHandles[NetAPIList.vip_info_ntf.Code] = GameGlobalData.RefreshVip
  MsgHandles[NetAPIList.bag_grid_ntf.Code] = GameItemBag.UpdateItemInBag
  MsgHandles[NetAPIList.server_kick_user_ntf.Code] = NetMessageMgr.ServerKickNotifyHandler
  MsgHandles[NetAPIList.module_status_ntf.Code] = GameGlobalData.RefreshMenuStatus
  MsgHandles[NetAPIList.attributes_change_ntf.Code] = GameGlobalData.AttributesChangeCallback
  MsgHandles[NetAPIList.quest_ntf.Code] = GameGlobalData.UpdateQuestState
  MsgHandles[NetAPIList.prestige_complete_ntf.Code] = GameGlobalData.PrestigeRankUpNotifyCallback
  MsgHandles[NetAPIList.loud_cast_ntf.Code] = NewsTip.LoadCastNotify
  MsgHandles[NetAPIList.mine_info_ntf.Code] = GameObjectMineMap.MineInfoNotifyHandler
  MsgHandles[NetAPIList.mine_complete_ntf.Code] = GameGlobalData.MineCompleteNofity
  MsgHandles[NetAPIList.laba_ntf.Code] = GameObjectGacha.GachaInfoNotifyHandler
  MsgHandles[NetAPIList.laba_times_ntf.Code] = GameObjectGacha.GachaTimesChangeHandler
  MsgHandles[NetAPIList.laba_type_ntf.Code] = GameObjectGacha.LabaAwardPoolNtf
  MsgHandles[NetAPIList.dexter_libs_ntf.Code] = GameUILab.DexterLibsNotifyHandler
  MsgHandles[NetAPIList.supply_loot_ack.Code] = GameGlobalData.NotifySupplyLootStatusHandler
  MsgHandles[NetAPIList.supply_loot_ntf.Code] = GameGlobalData.NotifySupplyLootHandler
  MsgHandles[NetAPIList.refresh_time_ntf.Code] = GameGlobalData.RefreshTimeNotifyHandler
  MsgHandles[NetAPIList.ac_ntf.Code] = GameGlobalData.ArcaneInfoNotifyHandler
  MsgHandles[NetAPIList.gateway_info_ntf.Code] = GameGlobalData.RefreshNewsAndAct
  MsgHandles[NetAPIList.block_devil_ntf.Code] = GameObjectCosmicExpedition.RefreshPlayerInfo
  MsgHandles[NetAPIList.block_devil_complete_ntf.Code] = GameObjectCosmicExpedition.GetExploreReward
  MsgHandles[NetAPIList.alliance_level_info_ntf.Code] = GameUIUserAlliance.RefreshAllianceLevelInfo
  MsgHandles[NetAPIList.login_first_ntf.Code] = GameUIBarLeft.ShowActivity
  MsgHandles[NetAPIList.award_count_ntf.Code] = GameUIBarLeft.awardCountNtfHandler
  MsgHandles[NetAPIList.event_activity_ntf.Code] = GameUIBarLeft.activityEventInfoNtfHandler
  MsgHandles[NetAPIList.login_time_current_ntf.Code] = GameUIBarLeft.UpdateServerTime
  MsgHandles[NetAPIList.first_charge_ntf.Code] = GameUIFirstCharge.NetCallbackGetData
  MsgHandles[NetAPIList.active_gifts_ntf.Code] = GameUIBarLeft.onActiveGiftsNTF
  MsgHandles[NetAPIList.world_boss_info_ntf.Code] = GameStateWorldBoss.WorldBossInfoNotifyHandler
  MsgHandles[NetAPIList.promotions_ntf.Code] = GameUIActivityNew.getPromotionsActivity
  MsgHandles[NetAPIList.promotion_award_ntf.Code] = GameUIBarLeft.GetSpecialActivityIsReward
  MsgHandles[NetAPIList.activities_button_status_ntf.Code] = GameUIBarLeft.GetIsNewActivity
  MsgHandles[NetAPIList.exp_add_item_ntf.Code] = GameUIBarLeft.ExpPropsNotifyHandler
  MsgHandles[NetAPIList.items_count_ntf.Code] = GameGlobalData.ItemCountChangeHandler
  MsgHandles[NetAPIList.thanksgiven_champion_ntf.Code] = GameUIChat.ThanksGivenChampionNotifyHandler
  MsgHandles[NetAPIList.item_use_award_ntf.Code] = GameItemBag.UseItemNotifyHandler
  MsgHandles[NetAPIList.colony_info_ntf.Code] = GameUIColonial.ColonyInfoNotifyHandler
  MsgHandles[NetAPIList.colony_exp_ntf.Code] = GameStateManager.GameStateColonization.ColonyExpChangeNotifyHandler
  MsgHandles[NetAPIList.colony_times_ntf.Code] = GameStateManager.GameStateColonization.ColonyTimesChangeNotifyHandler
  MsgHandles[NetAPIList.colony_logs_ntf.Code] = GameUIColonial.ColonyLogChangeNotifyHandler
  MsgHandles[NetAPIList.yys_ntf.Code] = GameUIBarLeft.YysNtfCallback
  MsgHandles[NetAPIList.common_ntf.Code] = NetMessageMgr.CommonNtfHandler
  MsgHandles[NetAPIList.battle_rate_ntf.Code] = GameUIBattleResult.BattleRateNtfCall
  MsgHandles[NetAPIList.alliance_resource_ntf.Code] = GameGlobalData.AllianceResourceChangeHandler
  MsgHandles[NetAPIList.alliance_defence_ntf.Code] = GameUIWDDefence.OnDefenceChangeHandler
  MsgHandles[NetAPIList.alliance_activity_times_ntf.Code] = GameUIUserAlliance.OnActivityTImesChangeHandler
  MsgHandles[NetAPIList.alliance_domination_ntf.Code] = GameStateManager.GameStateWD.OnBothAllianceInfoChange
  MsgHandles[NetAPIList.alliance_domination_battle_ntf.Code] = GameStateManager.GameStateWD.OnBattleResultChange
  MsgHandles[NetAPIList.alliance_domination_defence_ntf.Code] = GameStateManager.GameStateWD.OnOpponentDefenceInfoChange
  MsgHandles[NetAPIList.alliance_domination_total_rank_ntf.Code] = GameStateManager.GameStateWD.OnScoreChange
  MsgHandles[NetAPIList.domination_buffer_of_rank_ntf.Code] = GameStateManager.GameStateAlliance.onBufferChange
  MsgHandles[NetAPIList.activity_task_list_ntf.Code] = GameQuestMenu.ActivityQuestListNtf
  MsgHandles[NetAPIList.ip_ntf.Code] = GameSetting.OnIPAddrNtf
  MsgHandles[NetAPIList.orders_ntf.Code] = GameVip.ordersNtf
  MsgHandles[NetAPIList.mulmatrix_blank_ntf.Code] = GameGlobalData.mulMatrixBlankChangedNtf
  MsgHandles[NetAPIList.remodel_info_ntf.Code] = GameGlobalData.RemodelInfoNtf
  MsgHandles[NetAPIList.remodel_help_ntf.Code] = GameGlobalData.RemodelHelpInfoNtf
  MsgHandles[NetAPIList.remodel_push_ntf.Code] = GameUIRebuildFleet.RemodelPushInfoNtf
  MsgHandles[NetAPIList.remodel_event_ntf.Code] = GameUIBarLeft.RebuildEntranceNtf
  MsgHandles[NetAPIList.financial_amount_ntf.Code] = GameUIConsortium.GetAcountOfGiftFromFriendsNtf
  MsgHandles[NetAPIList.facebook_friends_ntf.Code] = GameUIConsortium.GetFacebookDataChangeNtf
  MsgHandles[NetAPIList.financial_ntf.Code] = GameUIConsortium.GetConsortiumInfoNtf
  MsgHandles[NetAPIList.push_button_ntf.Code] = GameGlobalData.PushButtonInfoNtf
  MsgHandles[NetAPIList.wdc_status_ntf.Code] = GameUIArena.wdcEndNtf
  MsgHandles[NetAPIList.tlc_status_ntf.Code] = GameUIArena.tlcEndNtf
  MsgHandles[NetAPIList.credit_exchange_ntf.Code] = GameUtils.NtfTongdui
  MsgHandles[NetAPIList.mul_credit_exchange_ntf.Code] = GameUtils.NtfMultiTongdui
  MsgHandles[NetAPIList.exchange_regulation_ntf.Code] = GameGlobalData.Ntf_exchange_regulation
  MsgHandles[NetAPIList.normal_ntf.Code] = GameGlobalData.RefreshUniversalData
  MsgHandles[NetAPIList.medal_list_ntf.Code] = GameGlobalData.OnMedalListNTF
  MsgHandles[NetAPIList.medal_glory_collection_ntf.Code] = GameGlobalData.OnHonorCollectionNTF
  MsgHandles[NetAPIList.medal_glory_achievement_red_point_ntf.Code] = GameGlobalData.OnAchiRedNTF
  MsgHandles[NetAPIList.pay_record_ntf.Code] = GameVip.payRecordNtf
  MsgHandles[NetAPIList.change_name_ntf.Code] = GameGlobalData.ChangeNameNtf
  MsgHandles[NetAPIList.leader_info_ntf.Code] = GameGlobalData.LeaderInfoNtf
  MsgHandles[NetAPIList.change_auto_decompose_ntf.Code] = GameUIKrypton.Krypton_decompos_change_ntf
  MsgHandles[NetAPIList.open_box_recent_ntf.Code] = GameUIActivityNew.RecentChestNtfHandler
  MsgHandles[NetAPIList.dna_gacha_ntf.Code] = GameUIcomboGacha.RecentChestNtfHandler
  MsgHandles[NetAPIList.dice_board_ntf.Code] = GameUIDice.RecentChestNtfHandler
  MsgHandles[NetAPIList.pay_animation_ntf.Code] = GameUIBarLeft.PayAnimationNtfHandler
  MsgHandles[NetAPIList.contention_hit_ntf.Code] = NewsTip.StarCraftHitNotify
  MsgHandles[NetAPIList.mycard_error_ntf.Code] = NewsTip.MycardPayNotify
  MsgHandles[NetAPIList.special_efficacy_ntf.Code] = NetMessageHandler.SpecialEfficacyNotifyHandler
  MsgHandles[NetAPIList.chat_channel_switch_ntf.Code] = GameUIChat.ChatChannelSwitchNtf
  MsgHandles[NetAPIList.max_level_ntf.Code] = GameGlobalData.ChangeMaxLevelNtf
  MsgHandles[NetAPIList.adjutant_max_ntf.Code] = GameGlobalData.AdjutantMaxCountNtfHandler
  MsgHandles[NetAPIList.client_version_ntf.Code] = GameStateManager.GameStateMainPlanet.ClientVersionNtfHandler
  MsgHandles[NetAPIList.version_code_ntf.Code] = GameStateManager.GameStateMainPlanet.CheckNeedNormalStartNtfHandler
  MsgHandles[NetAPIList.prestige_info_ntf.Code] = GameUIPrestigeRankUp.PrestigeInfoNtfNetCallback
  MsgHandles[NetAPIList.levelup_unlock_ntf.Code] = GameObjectLevelUp.lockLevelupInfo
  MsgHandles[NetAPIList.champion_promote_ntf.Code] = GameUIArena.BreakthroughNtfHandler
  MsgHandles[NetAPIList.equip_push_ntf.Code] = GameUIBarLeft.NewEquipNtf
  MsgHandles[NetAPIList.pay_push_pop_ntf.Code] = GameUIRechargePush.PayPushNtf
  MsgHandles[NetAPIList.url_push_ntf.Code] = GameUIWebView.PlayerRecall
  MsgHandles[NetAPIList.seven_day_ntf.Code] = GameUISevenBenefit.SevendayNtfCallback
  MsgHandles[NetAPIList.daily_benefits_ntf.Code] = GameUISevenBenefit.updateDailyBenefitsNtf
  MsgHandles[NetAPIList.wdc_awards_ntf.Code] = GameUIArena.RefreshGradeReward
  MsgHandles[NetAPIList.tlc_awards_ntf.Code] = GameUIArena.RefreshGradeReward_tlc
  MsgHandles[NetAPIList.ltv_ajust_ntf.Code] = GameGlobalData.FeatureSwitchNtf
  MsgHandles[NetAPIList.user_bag_info_ntf.Code] = GameItemBag.RefreshBagSize
  MsgHandles[NetAPIList.bugly_data_ntf.Code] = NetMessageMgr.SetBugly
  MsgHandles[NetAPIList.wdc_info_ntf.Code] = GameGlobalData.UpdateWdcPlayerInfo
  MsgHandles[NetAPIList.tlc_info_ntf.Code] = GameGlobalData.UpdateTlcPlayerInfo
  MsgHandles[NetAPIList.wdc_rankup_ntf.Code] = GameGlobalData.PlayerWdcRankUpCallback
  MsgHandles[NetAPIList.tlc_rankup_ntf.Code] = GameGlobalData.PlayerTlcRankUpCallback
  MsgHandles[NetAPIList.wdc_rank_board_ntf.Code] = GameUIArena.getRankListData
  MsgHandles[NetAPIList.tlc_rank_board_ntf.Code] = GameUIArena.getRankListData_tlc
  MsgHandles[NetAPIList.speed_resource_ntf.Code] = GameUIActivityNew.updateSpeedResource
  MsgHandles[NetAPIList.star_craft_data_ntf.Code] = GameGlobalData.OnStarCraftNtf
  MsgHandles[NetAPIList.red_point_ntf.Code] = GameGlobalData.OnRedPointtNtf
  MsgHandles[NetAPIList.master_ntf.Code] = GameUIMaster.OnMasterAchievementNTF
  MsgHandles[NetAPIList.mining_wars_change_ntf.Code] = GameUIMiningWars.statusChangeNtf
  MsgHandles[NetAPIList.mining_reward_ntf.Code] = GameUIMiningWorld.rewardsNtf
  MsgHandles[NetAPIList.main_city_ntf.Code] = GameGlobalData.OnButtonsStatusChange
  MsgHandles[NetAPIList.event_log_ntf.Code] = EventLogInterface.eventLogNtf
  MsgHandles[NetAPIList.fte_ntf.Code] = GameGlobalData.FTENtf
  MsgHandles[NetAPIList.user_id_status_ntf.Code] = GameGlobalData.UpdateIdStatus
  MsgHandles[NetAPIList.item_classify_ntf.Code] = GameGlobalData.UpdateCuponAndClassifyDate
  MsgHandles[NetAPIList.forces_ntf.Code] = GameUIMiningWars.ForcesNtf
  MsgHandles[NetAPIList.price_off_store_ntf.Code] = GameGoodsList.PriceOffStoreNTF
  MsgHandles[NetAPIList.fte_gift_ntf.Code] = GameGoodsList.FTEGiftNTF
  MsgHandles[NetAPIList.group_fight_ntf.Code] = GameStateBattlePlay.OnGroupFightReportNTF
  MsgHandles[NetAPIList.scratch_board_ntf.Code] = GameUIScratchGacha.OnBoardNtf
  MsgHandles[NetAPIList.large_map_route_ntf.Code] = GameUILargeMap.LargeMapRouteNtf
  MsgHandles[NetAPIList.large_map_block_update_ntf.Code] = GameUILargeMap.LargeMapBlockNtf
  MsgHandles[NetAPIList.large_map_event_ntf.Code] = GameUILargeMap.LargeMapEventNtf
  MsgHandles[NetAPIList.medal_fleet_formation_ntf.Code] = GameUIStarSystemMedalEquip.OnMedalFormationNtf
  MsgHandles[NetAPIList.fight_report_round_ntf.Code] = GameGlobalData.OnFightRoundNtf
  MsgHandles[NetAPIList.hero_union_unlock_ntf.Code] = GameUIPrestigeRankUp.HeroUnionUnlockNtf
  MsgHandles[NetAPIList.medal_fleet_red_point_ntf.Code] = GameGlobalData.OnFleetMedalRedPointNTF
  MsgHandles[NetAPIList.medal_fleet_btn_red_point_ntf.Code] = GameGlobalData.OnFleetMedalButtonRedPointNTF
  MsgHandles[NetAPIList.large_map_my_info_ntf.Code] = GameUILargeMap.LargeMapMyinfoNtf
  MsgHandles[NetAPIList.large_map_remove_mass_player_ntf.Code] = GameUILargeMapUI.RemoveMassPlayerNtf
  MsgHandles[NetAPIList.large_map_mass_invite_ntf.Code] = GameUILargeMapPop.LargeMapInviteNtf
  MsgHandles[NetAPIList.large_map_add_mass_player_ntf.Code] = GameUILargeMapUI.AddMassPlayerNtf
  MsgHandles[NetAPIList.large_map_alliance_player_exit_ntf.Code] = GameUILargeMapAlliance.PlayerExitAllianceNtf
  MsgHandles[NetAPIList.fps_switch_ntf.Code] = GameGlobalData.onFPS_switch_ntf
  MsgHandles[NetAPIList.large_map_mass_attack_line_ntf.Code] = GameUILargeMap.LargeMapLineNtf
  MsgHandles[NetAPIList.large_map_vision_ntf.Code] = GameUILargeMap.LargeMapVisionNtf
  MsgHandles[NetAPIList.large_map_vision_change_ntf.Code] = GameUILargeMap.LargeMapVisionChangeNtf
  MsgHandles[NetAPIList.expedition_task_update_ntf.Code] = GameObjectClimbTower.MilestoneReward_ExpNtf
  MsgHandles[NetAPIList.expedition_top3_update_ntf.Code] = GameObjectClimbTower.LeaderBoard_ExpNtf
  MsgHandles[NetAPIList.tlc_matrix_change_ntf.Code] = GameFleetEquipment.zhenxin_teamleague.UpdateMatrix
  MsgHandles[NetAPIList.pop_firstpay_on_lvup_ntf.Code] = GameObjectFirstPayPush.FirstPayPushNTF
  MsgHandles[NetAPIList.gcs_point_update_ntf.Code] = GameHelper.GCS_PointUpdateNTF
  MsgHandles[NetAPIList.gcs_buy_update_ntf.Code] = GameHelper.GCS_BuyCardUpdateNTF
end
function NetMessageMgr:RegisterMsgHandler(keyCode, handler)
  local MsgHandles = self.NetMsgHandles
  MsgHandles[keyCode] = handler
end
function NetMessageMgr:RemoveMsgHandler(keyCode)
  local MsgHandles = self.NetMsgHandles
  MsgHandles[keyCode] = nil
end
function NetMessageMgr:RegisterReconnectHandler(handler)
  NetMessageMgr.ReConnectCallback = handler
end
function NetMessageMgr.SetBugly(content)
  if ext.setBugly then
    if content.is_open then
      ext.setBugly(1)
    else
      ext.setBugly(0)
    end
  end
  if ext.setBuglyLuaError then
    if content.lua_error_open then
      ext.setBuglyLuaError(1)
    else
      ext.setBuglyLuaError(0)
    end
  end
  NetMessageMgr:SetFrame60AndHalfProtrait(content)
end
function NetMessageMgr:SetFrame60AndHalfProtrait(content)
  GameSettingData.frame60_open = content.frame60_open
  GameSettingData.half_body_open = content.half_body_open
  GameUtils:SaveSettingData()
end
function NetMessageMgr.CommonNtfHandler(content)
  if content.code == 110912 then
    local GameSetting = LuaObjectManager:GetLuaObject("GameSetting")
    GameSetting:SetUnlockNewFleet(true)
  else
    local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
  end
end
function NetMessageMgr:OnConnectFailed(codeId)
  local GameWaiting = LuaObjectManager:GetLuaObject("GameWaiting")
  GameWaiting:HideLoadingScreen()
  local errorCode = {
    [0] = "create socket failed",
    [1] = "connect to server ",
    [2] = "select error ",
    [3] = "recv zero byte ",
    [4] = "sent zero byte"
  }
  local Error = "Connect Error " .. errorCode[codeId]
  if GameStateManager.GameStateLoading:IsLoadFinished() then
    local serverName = " " .. GameUtils:GetActiveServerInfo().name or GameUtils:GetActiveServerInfo().id or ""
    local errorText = GameLoader:GetGameText("LC_ALERT_server_ask_for_offline") .. serverName
    local title = GameLoader:GetGameText("LC_MENU_CONNECTION_ERROR") or "Net Error"
    local yesCallBack = function()
      local callback = function()
        GameUtils:RestartGame(200)
      end
      GameUtils:SyncServerInfo(callback)
    end
    GameSetting:showContactUsDialogForNetError(title, errorText, yesCallBack)
  else
    if ext.socket.setHeaderSize then
      ext.socket.setHeaderSize(2)
    end
    NetMessageMgr.HeaderSize = 2
    ext.socket.reconnectToServer()
  end
  local deviceType = ext.GetDeviceFullName()
  if deviceType == "android" then
    deviceType = ext.GetDeviceVersion()
  end
  ext.addUmengEvent("OFFLINE_COUNT", {
    lang = ext.GetDeviceLanguage(),
    deviceType = deviceType
  })
  self.HeartbeatTimer = -1
end
NetMessageMgr.ReConnect = false
function NetMessageMgr:OnTryingReconnect()
  DebugOut("-----OnTryingReconnect----- ")
  if not self.m_isTryingToReconnect then
    local GameWaiting = LuaObjectManager:GetLuaObject("GameWaiting")
    GameWaiting:ShowLoadingScreen(true)
  end
  self.m_isTryingToReconnect = true
  for i, v in ipairs(self.CallbackList) do
    if v then
      table.insert(self.CallbacksWhenReconnect, v.NetFailedCallback)
    end
  end
  self.CallbackList = {}
  NetMessageMgr.ReConnect = true
  NetMessageMgr.ReConnectTime = 0
end
function NetMessageMgr:OnLoginFinish()
  DebugOut("=========OnLoginFinish========")
  for i, v in ipairs(self.CallbacksWhenReconnect) do
    v()
  end
  self.CallbacksWhenReconnect = {}
  if NetMessageMgr.ReConnect then
    local playerList = {}
    local requestParam = {notifies = playerList}
    NetMessageMgr:SendMsg(NetAPIList.alliance_notifies_req.Code, requestParam, nil, false, nil)
    if NetMessageMgr.ReConnectCallback then
      NetMessageMgr.ReConnectCallback()
    end
    local GameUIChat = LuaObjectManager:GetLuaObject("GameUIChat")
    GameUIChat:ClearChatMessageOnLogIn()
    NetMessageMgr:SendMsg(NetAPIList.chat_history_async_req.Code, nil, nil, false, nil)
    NetMessageMgr.ReConnect = false
    if NetMessageMgr.ReConnectSuccessCallList then
      for k, v in pairs(NetMessageMgr.ReConnectSuccessCallList) do
        if v then
          v()
        end
      end
    end
    NetMessageMgr.ReConnectSuccessCallList = {}
  end
end
function NetMessageMgr:OnConnectToServer()
  DebugOut("-----OnGetDeviceConfig------")
  if self.m_isTryingToReconnect then
    self.m_isTryingToReconnect = false
    local GameWaiting = LuaObjectManager:GetLuaObject("GameWaiting")
    GameWaiting:HideLoadingScreen()
    local GameStateLoading = GameStateManager.GameStateLoading
    GameStateLoading:CheckNewVersion()
    GameStateLoading:Login()
  end
  self.HeartbeatTimer = self.HEARTBEATT_INTERVAL
  AddFlurryEvent("StartGame", {}, 1)
end
function NetMessageMgr:ParseMessage(msg)
  local msgSize = 0
  if NetMessageMgr.HeaderSize == 4 then
    msgSize = msg:readUInt()
  else
    msgSize = msg:readUShort()
  end
  local msgType = msg:readUShort()
  local time = os.time()
  local timeStr = os.date("%H:%M:%S", time)
  DebugOut("lua handle message, pack size is" .. msgSize .. " type is " .. msgType .. ", @time: " .. timeStr)
  local content = Paser:parseMessage(msg, msgType)
  self:DispatchMessage(msgType, content)
  if msgType == 10000 and AutoUpdate.localAppVersion >= 10904 then
    NetMessageMgr:SendProtocalVersion()
  end
end
function NetMessageMgr:RemoveMsg(msgType)
  for k, v in pairs(self.CallbackList) do
    if v.MsgType == msgType then
      if v.IsWaiting then
        local GameWaiting = LuaObjectManager:GetLuaObject("GameWaiting")
        GameWaiting:HideLoadingScreen()
      end
      table.remove(self.CallbackList, k)
      return v
    end
  end
end
function NetMessageMgr:DealCommonAck(msgType, content)
  if msgType == NetAPIList.common_ack.Code then
    if content.code == 0 then
      return false
    end
    if content.code == 50111 then
      for k, v in pairs(self.CallbackList) do
        if v.IsWaiting then
          local GameWaiting = LuaObjectManager:GetLuaObject("GameWaiting")
          GameWaiting:HideLoadingScreen()
          break
        end
      end
      if GameStateManager.GameStateMainPlanet ~= GameStateManager:GetCurrentGameState() then
        GameStateManager:SetCurrentGameState(GameStateManager.GameStateMainPlanet)
      end
      do
        local callback = function()
          local localAppid = ext.GetBundleIdentifier()
          if GameUtils.AppDownLoadURL[localAppid] then
            ext.http.openURL(GameUtils.AppDownLoadURL[localAppid])
          elseif GameUtils.WorldPack[localAppid] then
            ext.http.openURL("http://tap4fun.com/en/game/7")
          else
            ext.http.openURL("http://yh.t4f.cn/")
          end
        end
        local cancelcallback = function()
        end
        local text_title = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
        local updateText = GameLoader:GetGameText("LC_MENU_TIP_MESSAGE_TOOLANG")
        local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
        GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
        GameUIMessageDialog:SetYesButton(callback)
        GameUIMessageDialog:SetNoButton(cancelcallback)
        GameUIMessageDialog:Display(text_title, updateText)
      end
      do break end
      do
        local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
        GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      end
      return true
    end
  end
  return false
end
NetMessageMgr.CallAfterCallbackHandledFunc = nil
function NetMessageMgr:DispatchMessage(msgType, content)
  DebugOut("dispatch msg: type is " .. msgType)
  if AutoUpdate.isDeveloper then
    DebugOut("content " .. LuaUtils:serializeTable(content))
  end
  local attpTime = os.time()
  for i, v in ipairs(self.CallbackList) do
    local result
    result, isHandled = pcall(v.CallbackFunc, msgType, content)
    if not result then
      if AutoUpdate.isDeveloper then
        print("luaerror dispatch " .. tostring(msgType) .. LuaUtils:serializeTable(content))
        error("luaerror dispatch " .. tostring(isHandled))
      end
      isHandled = true
    end
    if isHandled ~= nil or v.MsgType then
    end
    assert(isHandled ~= nil)
    if isHandled then
      if i ~= 1 then
        DebugOut("fuck error: the order of msgs from sever do not match the order client send ")
        assert(0)
      end
      if v.IsWaiting then
        local GameWaiting = LuaObjectManager:GetLuaObject("GameWaiting")
        GameWaiting:HideLoadingScreen()
      end
      if msgType == NetAPIList.pay_sign_ack.Code or msgType == NetAPIList.sign_activity_ack.Code then
        do
          local sendData = {}
          sendData.time_consuming = ext.getSysTime() - v.SendTime
          sendData.device_type = ext.GetPlatform()
          sendData.created_at = tostring(os.date())
          sendData.open_udid = ext.GetIOSOpenUdid()
          sendData.user_id = ext.T4FGetUserID()
          sendData.ip = ""
          DebugOut("Upload Time To Server:" .. tostring(spendTime))
          local function netFailedCallback()
            NetMessageMgr:SendMsg(NetAPIList.client_req_stat_req.Code, sendData, nil, false, netFailedCallback)
          end
          NetMessageMgr:SendMsg(NetAPIList.client_req_stat_req.Code, sendData, nil, false, netFailedCallback)
        end
      end
      DebugOut(string.format("%s msg send-receive used time:%d,client deal use time:%d", v.MsgType, attpTime - v.SendLocalTime, os.time() - attpTime))
      table.remove(self.CallbackList, i)
      if NetMessageMgr.CallAfterCallbackHandledFunc ~= nil then
        local func = NetMessageMgr.CallAfterCallbackHandledFunc
        NetMessageMgr.CallAfterCallbackHandledFunc = nil
        func()
      end
      return
    end
  end
  if NetMessageMgr:DealCommonAck(msgType, content) then
    return
  end
  local msgHandle = self.NetMsgHandles[msgType]
  if msgHandle == nil then
    DebugOut("ERROR: no msg handle for msgType: " .. msgType)
    return
  end
  msgHandle(content)
end
function NetMessageMgr:Update(dt)
  if self.m_isTryingToReconnect then
    self.ReConnectTime = self.ReConnectTime + dt
    if self.ReConnectTime > 120000 then
      self.ReConnectTime = 0
      self.m_isTryingToReconnect = false
      local serverName = " " .. GameUtils:GetActiveServerInfo().name or GameUtils:GetActiveServerInfo().id or ""
      local errorText = GameLoader:GetGameText("LC_ALERT_server_ask_for_offline") .. serverName
      local title = GameLoader:GetGameText("LC_MENU_CONNECTION_ERROR") or "Net Error"
      GameSetting:showContactUsDialogForNetError(title, errorText, function()
        GameUtils:RestartGame(200)
      end)
    end
  end
  ext.socket.updateNetMsg(dt)
  if ext.socket.hasPendingMsg() then
    local newMsg = CNetMsg:new()
    newMsg:LoadMsg()
    self:ParseMessage(newMsg)
  end
  if self.HeartbeatTimer ~= -1 then
    self.HeartbeatTimer = self.HeartbeatTimer - dt
    if 0 >= self.HeartbeatTimer then
      self.HeartbeatTimer = self.HEARTBEATT_INTERVAL
      NetMessageMgr:SendMsg(NetAPIList.heart_beat_req.Code, nil, nil, false)
    end
  end
  if GameGlobalData.isLogin then
    for i, v in ipairs(NetMessageMgr.MsgCache) do
      NetMessageMgr:SendMsg(v.msgType, v.content, v.callback, v.isWaiting, v.netFailedCallback)
      table.remove(NetMessageMgr.MsgCache, 1)
      break
    end
  end
  if AutoUpdate.localAppVersion >= 10904 and NetMessageMgr.HeaderSize == 4 and 0 < #NetMessageMgr.PendSendMsg then
    DebugOut("send pending msg")
    for i, v in ipairs(NetMessageMgr.PendSendMsg) do
      NetMessageMgr:SendMsg(v.msgType, v.content, v.callback, v.isWaiting, v.netFailedCallback)
      table.remove(NetMessageMgr.PendSendMsg, 1)
      break
    end
  end
end
function NetMessageMgr:SendMsg(msgType, content, callback, isWaiting, netFailedCallback)
  DebugTime("SendMsg, msgType : ", msgType, NetAPIList.nameToMsgType["MSGTYPE" .. msgType], LuaUtils:serializeTable(content or {}), callback, isWaiting)
  DebugTraceStack()
  if AutoUpdate.localAppVersion >= 10904 and NetMessageMgr.HeaderSize == 2 and msgType ~= NetAPIList.protocal_version_req.Code then
    local cacheMsg = {}
    cacheMsg.msgType = msgType
    cacheMsg.content = content
    cacheMsg.callback = callback
    cacheMsg.isWaiting = isWaiting
    cacheMsg.netFailedCallback = netFailedCallback
    table.insert(NetMessageMgr.PendSendMsg, cacheMsg)
    return
  end
  if not GameGlobalData.isLogin and msgType ~= NetAPIList.user_login_req.Code and msgType ~= NetAPIList.user_register_req.Code and msgType ~= NetAPIList.warn_req.Code and msgType ~= NetAPIList.client_fps_req.Code and msgType ~= NetAPIList.udid_step_req.Code and msgType ~= NetAPIList.protocal_version_req.Code then
    local cacheMsg = {}
    cacheMsg.msgType = msgType
    cacheMsg.content = content
    cacheMsg.callback = callback
    cacheMsg.isWaiting = isWaiting
    cacheMsg.netFailedCallback = netFailedCallback
    table.insert(NetMessageMgr.MsgCache, cacheMsg)
    return
  end
  local newMsg = CNetMsg:new()
  newMsg:CreateNewMessage(NetMessageMgr.HeaderSize)
  Paser:formatMessage(newMsg, msgType, content)
  newMsg:sendMsg()
  if callback ~= nil then
    local newData = {}
    newData.CallbackFunc = callback
    newData.IsWaiting = isWaiting
    newData.SendTime = ext.getSysTime()
    newData.SendLocalTime = os.time()
    newData.NetFailedCallback = netFailedCallback
    newData.MsgType = msgType
    table.insert(self.CallbackList, newData)
    DebugOut("check isWaiting")
    if isWaiting then
      local GameWaiting = LuaObjectManager:GetLuaObject("GameWaiting")
      GameWaiting:ShowLoadingScreen()
    end
  end
end
