local GameUIMiningWorld = LuaObjectManager:GetLuaObject("GameUIMiningWorld")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameUIMiningWars = LuaObjectManager:GetLuaObject("GameUIMiningWars")
local GameStateBattlePlay = GameStateManager.GameStateBattlePlay
local GameUIWDStuff = LuaObjectManager:GetLuaObject("GameUIWDStuff")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local GameStateMiningWorld = GameStateManager.GameStateMiningWorld
local GameObjectBattleReplay = LuaObjectManager:GetLuaObject("GameObjectBattleReplay")
GameUIMiningWorld.basicData = nil
GameUIMiningWorld.logsData = nil
GameUIMiningWorld.searchData = nil
GameUIMiningWorld.baseContent = nil
GameUIMiningWorld.curUI = "main"
GameUIMiningWorld.onAddStateCallBack = nil
GameUIMiningWorld.enterDir = 0
function GameUIMiningWorld.rewardsNtf(content)
  if GameStateManager:GetCurrentGameState() == GameStateManager.GameStateMiningWorld then
    ItemBox:ShowRewardBox(content.rewards)
    if GameUIMiningWorld:GetFlashObject() then
      GameUIMiningWorld:ReqMiningWorldData()
    end
  end
end
function GameUIMiningWorld:OnInitGame()
end
function GameUIMiningWorld:Show()
  GameUIMiningWorld:ReqMiningWorldData()
  GameStateManager:GetCurrentGameState():AddObject(GameUIMiningWorld)
end
function GameUIMiningWorld:OnAddToGameState()
  DebugOut("GameUIMiningWorld:OnAddToGameState")
  self:LoadFlashObject()
  GameTimer:Add(self._OnTimerTick, 1000)
  local data = {}
  data.PLANETCRAFT_TODAY_REMAINING = GameLoader:GetGameText("LC_MENU_PLANETCRAFT_TODAY_REMAINING")
  data.mapName1 = GameLoader:GetGameText("LC_MENU_PLANETCRAFT_MAP_1")
  data.mapName2 = GameLoader:GetGameText("LC_MENU_PLANETCRAFT_MAP_2")
  data.mapName3 = GameLoader:GetGameText("LC_MENU_PLANETCRAFT_MAP_3")
  data.mapName4 = GameLoader:GetGameText("LC_MENU_PLANETCRAFT_MAP_4")
  data.mapName5 = GameLoader:GetGameText("LC_MENU_PLANETCRAFT_MAP_5")
  data.mapName6 = GameLoader:GetGameText("LC_MENU_PLANETCRAFT_MAP_6")
  GameUIMiningWorld:GetFlashObject():InvokeASCallback("_root", "InitFlashUI", data)
  GameUIMiningWorld:MoveIn()
  if GameUIMiningWorld.onAddStateCallBack then
    GameUIMiningWorld.onAddStateCallBack()
    GameUIMiningWorld.onAddStateCallBack = nil
  end
end
function GameUIMiningWorld:OnEraseFromGameState()
  GameUIMiningWorld.basicData = nil
  self:UnloadFlashObject()
  collectgarbage("collect")
end
function GameUIMiningWorld:OnFSCommand(cmd, arg)
  if cmd == "close" then
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateMainPlanet)
  elseif cmd == "TouchMapBlock" then
    local id, unlock = unpack(LuaUtils:string_split(arg, ":"))
    GameUIMiningWars.curMapId = tonumber(id)
    if unlock == "true" then
      GameUIMiningWars:Show()
      GameStateManager:GetCurrentGameState():EraseObject(GameUIMiningWorld)
    else
      GameTip:Show(GameLoader:GetGameText("LC_MENU_PLANETCRAFT_NOT_OPEN"))
    end
  elseif cmd == "LookLog" then
    GameUIMiningWorld:ReqLogs()
  elseif cmd == "SearchPE" then
    GameUIMiningWorld:ReqSearchData()
  elseif cmd == "Touch_PE" then
    local starid, playerid, logic_id, block = unpack(LuaUtils:string_split(arg, ":"))
    GameUIMiningWorld:OnBattlePE(tonumber(starid), tonumber(playerid), tonumber(logic_id), false, tonumber(block))
  elseif cmd == "update_log_item" then
    GameUIMiningWorld:UpdateLogItem(tonumber(arg))
  elseif cmd == "update_search_item" then
    GameUIMiningWorld:UpdateSearchItem(tonumber(arg))
  elseif cmd == "closeUI" then
    GameUIMiningWorld.curUI = "main"
  elseif cmd == "OpenUI" then
    GameUIMiningWorld.curUI = arg
  elseif cmd == "report" then
    GameUIMiningWorld:onReport(arg)
  elseif cmd == "add_count" then
    GameUIMiningWorld:OnBuyCount()
  elseif cmd == "help" then
    GameUIWDStuff:Show(GameUIWDStuff.menuType.menu_type_help)
    GameUIWDStuff:SetHelpText(GameLoader:GetGameText("LC_MENU_PLANETCRAFT_HELP_INFO"))
  end
end
function GameUIMiningWorld:ReqMiningWorldData()
  local function callback(msgtype, content)
    if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.mining_world_req.Code then
      if content.code ~= 0 then
        GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      end
      return true
    end
    if msgtype == NetAPIList.mining_world_ack.Code then
      GameUIMiningWorld:GenerateData(content)
      GameUIMiningWorld:SetBasicData()
      return true
    end
    return false
  end
  DebugOut("GameUIMiningWorld:ReqMiningWorldData")
  local param = {}
  param.flag = GameUtils:GetFlagByCountryCode()
  param.dir = GameUIMiningWorld.enterDir
  GameUIMiningWorld.enterDir = 0
  NetMessageMgr:SendMsg(NetAPIList.mining_world_req.Code, param, callback, true, nil)
end
function GameUIMiningWorld:GenerateData(content)
  GameUIMiningWorld.baseContent = content
  local data = {}
  data.searchLeftTime = content.map_blocks[1].search_left_time
  data.baseTime = os.time()
  data.leftCount = content.map_blocks[1].left_count
  data.buyCount = content.map_blocks[1].buy_count
  data.buyCredit = content.map_blocks[1].buy_count_credit
  data.maxCount = content.map_blocks[1].max_count
  data.PLANETCRAFT_TODAY_REMAINING = GameLoader:GetGameText("LC_MENU_PLANETCRAFT_TODAY_REMAINING")
  data.mapBlocks = {}
  for k, v in pairs(content.map_blocks or {}) do
    v.name = GameLoader:GetGameText("LC_MENU_PLANETCRAFT_MAP_" .. v.id)
    data.mapBlocks[#data.mapBlocks + 1] = v
  end
  GameUIMiningWorld.basicData = data
end
function GameUIMiningWorld:SetBasicData()
  local data = {}
  local flashObj = GameUIMiningWorld:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "SetBasicData", GameUIMiningWorld.basicData)
    GameUIMiningWorld._OnTimerTick()
  end
end
function GameUIMiningWorld:MoveIn()
  local flashObj = GameUIMiningWorld:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "MoveIn")
  end
end
function GameUIMiningWorld._OnTimerTick()
  if GameUIMiningWorld.basicData then
    local left = GameUIMiningWorld.basicData.searchLeftTime - (os.time() - GameUIMiningWorld.basicData.baseTime)
    local str = ""
    if left <= 0 then
      left = 0
    else
      str = GameUtils:formatTimeStringAsTwitterStyle3(left)
    end
    if GameUIMiningWorld:GetFlashObject() then
      GameUIMiningWorld:GetFlashObject():InvokeASCallback("_root", "setUpdateTime", str)
    end
  end
  if GameStateMiningWorld:IsObjectInState(GameUIMiningWorld) then
    return 1000
  end
  return nil
end
function GameUIMiningWorld:ReqLogs()
  NetMessageMgr:SendMsg(NetAPIList.mining_world_log_req.Code, nil, GameUIMiningWorld.logsCallBack, true, nil)
end
function GameUIMiningWorld.logsCallBack(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.mining_world_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgtype == NetAPIList.mining_world_log_ack.Code then
    GameUIMiningWorld:GenerateLogsData(content)
    GameUIMiningWorld:SetLogsData()
    return true
  end
  return false
end
function GameUIMiningWorld:GenerateLogsData(content)
  local data = {}
  data.logs = {}
  for k, v in ipairs(content.logs or {}) do
    local item = {}
    item.time = GameUtils:formatTimeStringAsTwitterStyle(v.time)
    item.desc = v.desc
    item.reportId = v.report_id
    data.logs[#data.logs + 1] = item
  end
  GameUIMiningWorld.logsData = data
end
function GameUIMiningWorld:SetLogsData()
  GameUIMiningWorld:GetFlashObject():InvokeASCallback("_root", "SetLogsData", #GameUIMiningWorld.logsData.logs, GameLoader:GetGameText("LC_MENU_FST_log"))
  GameUIMiningWorld:GetFlashObject():InvokeASCallback("_root", "MoveIN_Log")
end
function GameUIMiningWorld:UpdateLogItem(index)
  local log = GameUIMiningWorld.logsData.logs[index]
  GameUIMiningWorld:GetFlashObject():InvokeASCallback("_root", "UpdateLogItem", index, log)
end
function GameUIMiningWorld:ReqSearchData()
  local leftTime = GameUIMiningWorld.basicData.searchLeftTime - (os.time() - GameUIMiningWorld.basicData.baseTime)
  if leftTime > 0 then
    local str = GameLoader:GetGameText("LC_MENU_EVENT_COLONIAL_NEXT_REQUIRE_CHAR") .. GameUtils:formatTimeStringAsPartion2(leftTime)
    GameTip:Show(str)
    return
  end
  NetMessageMgr:SendMsg(NetAPIList.mining_world_pe_req.Code, nil, GameUIMiningWorld.searchPECallBack, true, nil)
end
function GameUIMiningWorld.searchPECallBack(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.mining_world_pe_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgtype == NetAPIList.mining_world_pe_ack.Code then
    GameUIMiningWorld:GenerateSearchData(content)
    GameUIMiningWorld:SetSearchData()
    return true
  end
  return false
end
function GameUIMiningWorld:GenerateSearchData(content)
  local data = {}
  data.peList = {}
  for k, v in ipairs(content.pe_list) do
    local item = {}
    item.time = GameUtils:formatTimeStringAsTwitterStyle(v.time)
    item.starLevel = v.star_level
    item.starFrame = tonumber(v.star_frame) or 1
    item.desc = v.desc
    item.playerId = v.player_id
    item.logicId = v.logic_id
    item.starId = v.star_id
    item.pos = v.pos
    item.block = v.block
    data.peList[#data.peList + 1] = item
  end
  GameUIMiningWorld.searchData = data
  GameUIMiningWorld.basicData.searchLeftTime = content.search_left_time
  GameUIMiningWorld.basicData.baseTime = os.time()
end
function GameUIMiningWorld:SetSearchData()
  GameUIMiningWorld:GetFlashObject():InvokeASCallback("_root", "SetSearchData", #GameUIMiningWorld.searchData.peList, GameLoader:GetGameText("LC_MENU_PLANETCRAFT_SCAN_NORESULT"))
  GameUIMiningWorld:GetFlashObject():InvokeASCallback("_root", "MoveIN_PE")
end
function GameUIMiningWorld:UpdateSearchItem(index)
  local pe = GameUIMiningWorld.searchData.peList[index]
  GameUIMiningWorld:GetFlashObject():InvokeASCallback("_root", "UpdateSearchItem", index, pe)
end
function GameUIMiningWorld:GetPosByStarID(id)
  for k, v in pairs(GameUIMiningWorld.searchData.peList) do
    if v.starId == id then
      return v.pos
    end
  end
  return 1
end
function GameUIMiningWorld:OnBattlePE(starId, playerid, logicId, useCredit, block)
  local curflag = GameUtils:GetFlagByCountryCode()
  local param = {}
  param.player_id = playerid
  param.id = starId
  param.logic_id = logicId
  param.use_credit = useCredit or false
  param.flag = curflag
  param.pos = GameUIMiningWorld:GetPosByStarID(starId)
  param.is_intrusion = true
  GameUIMiningWorld.curStarID = starId
  GameUIMiningWorld.curPlayerID = playerid
  GameUIMiningWorld.curLogicID = logicId
  GameUIMiningWorld.curBlock = block
  if GameStateManager:GetCurrentGameState().fightRoundData then
    GameStateManager:GetCurrentGameState().fightRoundData = nil
  end
  NetMessageMgr:SendMsg(NetAPIList.mining_wars_battle_req.Code, param, GameUIMiningWorld.battlePECallBack, true, nil)
end
function GameUIMiningWorld.battlePECallBack(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.mining_wars_battle_req.Code then
    if content.code ~= 0 then
      if content.code == 49210 then
        local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
        GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
        GameUIMessageDialog:SetYesButton(function()
          local param = {
            block = GameUIMiningWorld.curBlock
          }
          NetMessageMgr:SendMsg(NetAPIList.mining_wars_buy_count_req.Code, param, GameUIMiningWorld.buyCountCallBack, true, nil)
        end)
        local credit = GameUIMiningWorld.baseContent.map_blocks[GameUIMiningWorld.curBlock].buy_count_credit
        local contentText = GameLoader:GetGameText("LC_ALERT_sw_no_remaining_plunder_times") .. "\n" .. GameLoader:GetGameText("LC_MENU_PLANETCRAFT_COST_TIP_RUSH")
        contentText = string.gsub(contentText, "<credits_num>", tostring(credit))
        local titleText = GameLoader:GetGameText("LC_MENU_LOCAL_TEXT_SETTING_PAYREMIND")
        GameUIMessageDialog:Display(titleText, contentText)
      else
        local GameVip = LuaObjectManager:GetLuaObject("GameVip")
        GameVip:CheckIsNeedShowVip(content.code)
      end
    end
    return true
  elseif msgtype == NetAPIList.mining_wars_battle_ack.Code then
    if 0 < content.error_code then
      local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
      GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
      GameUIMessageDialog:SetYesButton(function()
        GameUIMiningWorld:OnBattlePE(GameUIMiningWorld.curStarID, GameUIMiningWorld.curPlayerID, GameUIMiningWorld.curLogicID, true, GameUIMiningWorld.curBlock)
        GameUIMiningWorld.curStarID = nil
        GameUIMiningWorld.curPlayerID = nil
        GameUIMiningWorld.curLogicID = nil
        GameUIMiningWorld.curBlock = nil
      end)
      local contentText = GameLoader:GetGameText("LC_MENU_LACK_OF_RESOURCES")
      local name = GameLoader:GetGameText("LC_MENU_LEAGUE_REVENGE")
      local titleText = GameLoader:GetGameText("LC_MENU_LOCAL_TEXT_SETTING_PAYREMIND")
      contentText = string.gsub(contentText, "<name1>", name)
      contentText = string.gsub(contentText, "<name2>", tostring(content.switch_credit))
      GameUIMessageDialog:Display(titleText, contentText)
      return true
    end
    if #content.report.rounds == 0 and GameStateManager:GetCurrentGameState().fightRoundData then
      content.report.rounds = GameStateManager:GetCurrentGameState().fightRoundData
    end
    GameUIMiningWorld:onBattle(content.report, content.success, content.battle_result)
    return true
  end
  return false
end
function GameUIMiningWorld:onBattle(report, success, battleResult)
  local battle_report_data = report
  if GameObjectBattleReplay.GroupBattleReportArr[1] and #GameObjectBattleReplay.GroupBattleReportArr[1].headers > 0 then
    battle_report_data = GameObjectBattleReplay.GroupBattleReportArr[1].headers[1].report
  else
    error("no battle report")
  end
  if battle_report_data then
    local lastGameState = GameStateManager:GetCurrentGameState()
    GameStateBattlePlay.curBattleType = "starwar"
    GameStateBattlePlay:InitBattle(GameStateBattlePlay.MODE_PLAY, battle_report_data)
    GameStateBattlePlay:RegisterOverCallback(function()
      local GameUIBattleResult = LuaObjectManager:GetLuaObject("GameUIBattleResult")
      GameUIBattleResult:LoadFlashObject()
      GameUIBattleResult:SetFightReport(GameObjectBattleReplay.GroupBattleReportArr, nil, nil)
      local data = {}
      local lang = "en"
      if GameSettingData and GameSettingData.Save_Lang then
        lang = GameSettingData.Save_Lang
        if string.find(lang, "ru") == 1 then
          lang = "ru"
        elseif GameUtils:IsChinese() and GameUtils:IsSimplifiedChinese() then
          lang = "zh"
        end
      end
      data.lang = lang
      data.isChinese = GameUtils:IsChinese()
      data.isWin = battleResult.is_win
      data.starFrame = tonumber(battleResult.star_frame)
      data.consumeList = {}
      for k, v in pairs(battleResult.consume_item or {}) do
        local item = {}
        item.name = GameHelper:GetAwardText(v.item_type, v.number, v.no)
        item.count = GameHelper:GetAwardCount(v.item_type, v.number, v.no)
        item.frame = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(v)
        item.displayCount = GameUtils.numberConversion(math.floor(item.count))
        item.model = v
        data.consumeList[#data.consumeList + 1] = item
      end
      GameUIBattleResult:SetStarWarResult(data)
      local function callback()
        GameStateManager:GetCurrentGameState():AddObject(GameUIBattleResult)
      end
      GameUIMiningWorld.onAddStateCallBack = callback
      GameStateManager:SetCurrentGameState(GameStateManager.GameStateMiningWorld)
      GameUIMiningWorld:Show()
    end, nil)
    GameStateManager:GetCurrentGameState():EraseObject(GameUIMiningWorld)
    GameStateManager:SetCurrentGameState(GameStateBattlePlay)
  end
end
function GameUIMiningWorld:onReport(report_id)
  local param = {}
  param.id = report_id
  GameStateBattlePlay.report_id = report_id
  if GameStateManager:GetCurrentGameState().fightRoundData then
    GameStateManager:GetCurrentGameState().fightRoundData = nil
  end
  NetMessageMgr:SendMsg(NetAPIList.mining_battle_report_req.Code, param, GameUIMiningWorld.battleReportCallBack, true, nil)
end
function GameUIMiningWorld.battleReportCallBack(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.mining_battle_report_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgtype == NetAPIList.mining_battle_report_ack.Code then
    local battle_report_data = content.report
    local battleResult = content.battle_result
    if battle_report_data then
      if #battle_report_data.rounds == 0 and GameStateManager:GetCurrentGameState().fightRoundData then
        battle_report_data.rounds = GameStateManager:GetCurrentGameState().fightRoundData
      end
      local lastGameState = GameStateManager:GetCurrentGameState()
      GameStateBattlePlay.curBattleType = "starwar"
      GameStateBattlePlay:InitBattle(GameStateBattlePlay.MODE_PLAY, battle_report_data)
      GameStateBattlePlay:RegisterOverCallback(function()
        GameStateManager:SetCurrentGameState(GameStateManager.GameStateMiningWorld)
        GameUIMiningWorld:Show()
      end, nil)
      GameStateManager:SetCurrentGameState(GameStateBattlePlay)
    end
    return true
  end
  return false
end
function GameUIMiningWorld:OnBuyCount()
  local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
  GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
  GameUIMessageDialog:SetYesButton(function()
    local param = {block = 1}
    NetMessageMgr:SendMsg(NetAPIList.mining_wars_buy_count_req.Code, param, GameUIMiningWorld.buyCountCallBack, true, nil)
  end)
  local contentText = GameLoader:GetGameText("LC_MENU_PLANETCRAFT_COST_TIP_RUSH")
  local titleText = GameLoader:GetGameText("LC_MENU_LOCAL_TEXT_SETTING_PAYREMIND")
  contentText = string.gsub(contentText, "<credits_num>", tostring(GameUIMiningWorld.basicData.buyCredit))
  GameUIMessageDialog:Display(titleText, contentText)
end
function GameUIMiningWorld.buyCountCallBack(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.mining_wars_buy_count_req.Code then
    if content.code ~= 0 then
      local GameVip = LuaObjectManager:GetLuaObject("GameVip")
      GameVip:CheckIsNeedShowVip(content.code)
    end
    return true
  elseif msgtype == NetAPIList.mining_wars_buy_count_ack.Code then
    GameUIMiningWorld.basicData.leftCount = content.left_count
    GameUIMiningWorld.basicData.buyCredit = content.next_buy_credit
    local data = {}
    data.leftCount = GameUIMiningWorld.basicData.leftCount
    data.PLANETCRAFT_TODAY_REMAINING = GameLoader:GetGameText("LC_MENU_PLANETCRAFT_TODAY_REMAINING")
    GameUIMiningWorld:GetFlashObject():InvokeASCallback("_root", "setLeftCount", data)
    return true
  end
  return false
end
function GameUIMiningWorld:req(dt)
end
function GameUIMiningWorld:Update(dt)
  local flashObj = GameUIMiningWorld:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "onUpdateFrame", dt)
    flashObj:Update(dt)
  end
end
if AutoUpdate.isAndroidDevice then
  function GameUIMiningWorld.OnAndroidBack()
    if not GameUIMiningWorld:GetFlashObject() then
      return
    end
    if GameUIMiningWorld.curUI == "main" then
      GameUIMiningWorld:GetFlashObject():InvokeASCallback("_root", "MoveOut")
    elseif GameUIMiningWorld.curUI == "log" then
      GameUIMiningWorld:GetFlashObject():InvokeASCallback("_root", "MoveOut_Log")
    elseif GameUIMiningWorld.curUI == "pe" then
      GameUIMiningWorld:GetFlashObject():InvokeASCallback("_root", "MoveOut_PE")
    end
  end
end
