local GameStateAlliance = GameStateManager.GameStateAlliance
local GameMail = LuaObjectManager:GetLuaObject("GameMail")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameUIUserAlliance = LuaObjectManager:GetLuaObject("GameUIUserAlliance")
local GameUIPlayerView = LuaObjectManager:GetLuaObject("GameUIPlayerView")
local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameObjectAllianceDonation = LuaObjectManager:GetLuaObject("GameObjectAllianceDonation")
local GameObjectCosmicExpedition = LuaObjectManager:GetLuaObject("GameObjectCosmicExpedition")
local GameUIWDDefence = LuaObjectManager:GetLuaObject("GameUIWDDefence")
local GameSetting = LuaObjectManager:GetLuaObject("GameSetting")
local GameUIAllianceInfo = LuaObjectManager:GetLuaObject("GameUIAllianceInfo")
local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
local GameUIFriend = LuaObjectManager:GetLuaObject("GameUIFriend")
local GameUIUnalliance = LuaObjectManager:GetLuaObject("GameUIUnalliance")
local GameUIFactory = LuaObjectManager:GetLuaObject("GameUIFactory")
local GameUIRebuildFleet = LuaObjectManager:GetLuaObject("GameUIRebuildFleet")
local ActivityTypeTable = {}
ActivityTypeTable.Donation = 1
ActivityTypeTable.CosmicExpedition = 2
ActivityTypeTable.WDDefence = 3
ActivityTypeTable.Rebuild = 4
GameUIUserAlliance.mShareStoryChecked = {}
GameUIUserAlliance.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_STARALLIANCE_MODIFY_NOTE] = true
GameUIUserAlliance.mCurMemo = nil
GameUIUserAlliance.MemberRankType = {Normal = 0, DefenseLeader = 1001}
GameUIUserAlliance.mIsSwitchOn = true
GameUIUserAlliance.mCurLimitLv = 0
function GameUIUserAlliance:OnAddToGameState(game_state)
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  GameGlobalData:RegisterDataChangeCallback("alliance_resource", self.RefreshResource)
  local alliances_info = {}
  alliances_info.data = {}
  alliances_info.total = 0
  alliances_info.index = 0
  alliances_info.perpage = 20
  alliances_info.keyword = ""
  self.alliances_info = alliances_info
  self:UpdateAlliancesList(0)
  self:UpdateRecordsList(0)
  self:ClearLocalDataAllianceInfo()
  self:ClearLocalDataMembers()
  self:ClearLocalDataRecords()
  self:ClearLocalDataAudits()
  local active_tab = self:GetActiveTab()
  if active_tab == "" then
    active_tab = "main"
  end
  self:SelectTab(active_tab)
  self:FetchActivities(false)
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    local unlock = false
    local moduleStatus = GameGlobalData:GetData("modules_status").modules
    for k, v in pairs(moduleStatus) do
      if "friends" == v.name then
        unlock = v.status
      end
    end
    flash_obj:InvokeASCallback("_root", "SetContactBtnVisible", unlock, GameLoader:GetGameText("LC_MENU_ALLIANCE_RECRUIT_PLAYER_BUTTON"))
  end
end
function GameUIUserAlliance:OnEraseFromGameState(game_state)
  GameUIUserAlliance:UnloadFlashObject()
  GameUIUserAlliance.isOnShowEditBox = nil
  GameUIUserAlliance.isOnShowPopWindow = nil
  GameUIUserAlliance.isOnShowAudits = nil
end
function GameUIUserAlliance:ClearLocalDataAllianceInfo()
end
function GameUIUserAlliance:ClearLocalDataMembers()
  self.members_info = {}
  self.members_info.data = {}
  self.members_info.index = 0
  self.members_info.total = 0
  self.members_info.perpage = 50
end
function GameUIUserAlliance:ClearLocalDataAudits()
  self.audits_info = {}
  self.audits_info.data = {}
  self.audits_info.index = 0
  self.audits_info.total = 0
  self.audits_info.perpage = 50
end
function GameUIUserAlliance:ClearLocalDataRecords()
  self.records_info = {}
  self.records_info.data = {}
  self.records_info.index = 0
  self.records_info.total = 0
  self.records_info.perpage = 50
end
function GameUIUserAlliance:ClearLocalDataActivities()
  self.ActivitiesTable = {}
end
function GameUIUserAlliance:OnFSCommand(cmd, arg)
  if cmd == "switch_tabpage" then
    self:SwitchTabPage(tonumber(arg))
  elseif cmd == "select_tab" then
    self:SelectTab(arg)
  elseif cmd == "delete_alliance" then
    local text_title = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
    local text_content = GameLoader:GetGameText("LC_MENU_CONTENT_VERIFY_DELETE_ALLIANCE")
    GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
    GameUIMessageDialog:SetYesButton(function()
      GameUIUserAlliance:DeleteAlliance()
    end)
    GameUIMessageDialog:Display(text_title, text_content)
  elseif cmd == "on_erase" then
    GameStateAlliance:Quit()
  elseif cmd == "update_member_item" then
    self:SetMemberItem(tonumber(arg))
  elseif cmd == "update_audit_item" then
    self:SetAuditItem(tonumber(arg))
  elseif cmd == "select_member" then
    self:SelectMember(tonumber(arg))
  elseif cmd == "update_activity_item" then
    self:UpdateActivityItem(tonumber(arg))
  elseif cmd == "selectActivity" then
    self:SelectActivity(tonumber(arg))
  elseif cmd == "update_apply_item" then
    self:UpdateApplyItem(tonumber(arg))
  elseif cmd == "update_record_item" then
    self:UpdateRecordItem(tonumber(arg))
  elseif cmd == "view_player" then
    self:ViewMemberInfo(tonumber(arg))
  elseif cmd == "add_friend" then
    self:AddMemberFriend(tonumber(arg))
  elseif cmd == "send_mail" then
    self:SendMemberMail(tonumber(arg))
  elseif cmd == "group_mail_released" then
    self:SendAllMemberMail()
  elseif cmd == "demote_vicechairman" then
    local text_title = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
    local text_content = GameLoader:GetGameText("LC_MENU_CONTENT_VERIFY_ALLIANCE_DEMOTE")
    GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
    GameUIMessageDialog:SetYesButton(GameUIUserAlliance.DemoteMemberPosition, {
      self,
      tonumber(arg)
    })
    GameUIMessageDialog:Display(text_title, text_content)
  elseif cmd == "promote_member" then
    local text_title = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
    local text_content = GameLoader:GetGameText("LC_MENU_CONTENT_VERIFY_ALLIANCE_PROMOTE")
    GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
    GameUIMessageDialog:SetYesButton(GameUIUserAlliance.PromoteMemberPosition, {
      self,
      tonumber(arg)
    })
    GameUIMessageDialog:Display(text_title, text_content)
  elseif cmd == "demise_alliance" then
    local memberInfo = self.members_info.data[tonumber(arg)]
    local text_title = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
    local text_content = GameLoader:GetGameText("LC_MENU_CONTENT_VERIFY_ALLIANCE_DEMISE")
    text_content = string.format(text_content, GameUtils:CutOutUsernameByLastDot(memberInfo.name))
    GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
    GameUIMessageDialog:SetYesButton(GameUIUserAlliance.TransferAlliance, {
      self,
      tonumber(arg)
    })
    GameUIMessageDialog:Display(text_title, text_content)
  elseif cmd == "kick_member" then
    local text_title = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
    local text_content = GameLoader:GetGameText("LC_MENU_CONTENT_VERIFY_ALLIANCE_KICKOUT")
    GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
    GameUIMessageDialog:SetYesButton(GameUIUserAlliance.KickOutMember, {
      self,
      tonumber(arg)
    })
    GameUIMessageDialog:Display(text_title, text_content)
  elseif cmd == "modify_alliance_memo" then
    GameUIUserAlliance:ModifyAllianceDesc(arg)
  elseif cmd == "agree_join_apply" then
    self:RatifyJoinApply(tonumber(arg))
  elseif cmd == "disagree_join_apply" then
    self:RefuseJoinApply(tonumber(arg))
  elseif cmd == "refuse_all_audits" then
    local content = {
      ids = {}
    }
    NetMessageMgr:SendMsg(NetAPIList.alliance_refuse_req.Code, content, GameUIUserAlliance.NetCallbackRefuse, false, nil)
  elseif cmd == "ratify_all_audits" then
    local content = {
      ids = {}
    }
    NetMessageMgr:SendMsg(NetAPIList.alliance_ratify_req.Code, content, GameUIUserAlliance.NetCallbackRatify, false, nil)
  elseif cmd == "quit_alliance" then
    local text_title = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
    local text_content = ""
    local user_alliance = GameGlobalData:GetData("alliance")
    text_content = GameLoader:GetGameText("LC_MENU_CONTENT_VERIFY_QUIT_ALLIANCE")
    GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
    GameUIMessageDialog:SetYesButton(GameUIUserAlliance.QuitAlliance, {GameUIUserAlliance})
    GameUIMessageDialog:Display(text_title, text_content)
  elseif cmd == "view_audits" then
    self.isOnShowAudits = true
    self:UpdateAuditsList(0)
  elseif cmd == "edit_alliance_desc" then
    self:ModifyAllianceDesc(arg)
  elseif cmd == "desc_editbox_move_out_over" then
    GameUIUserAlliance.isOnShowEditBox = nil
  elseif cmd == "desc_editbox_move_in" then
    GameUIUserAlliance.isOnShowEditBox = true
    local shareStoryEnabled = false
    if FacebookGraphStorySharer:IsStoryEnabled(FacebookGraphStorySharer.StoryType.STORY_TYPE_STARALLIANCE_MODIFY_NOTE) then
      shareStoryEnabled = true
    end
    local flash_obj = self:GetFlashObject()
    if flash_obj then
      flash_obj:InvokeASCallback("_root", "descEditboxShareEnable", shareStoryEnabled, self.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_STARALLIANCE_MODIFY_NOTE])
    end
  elseif cmd == "update_alliance_item" then
    self:UpdateAllianceItem(tonumber(arg))
  elseif cmd == "select_alliance" then
    local index_alliance = tonumber(arg)
    self:SelectAlliance(index_alliance)
  elseif cmd == "search_alliance" then
    GameUIUserAlliance.alliances_info.keyword = arg
    GameUIUserAlliance:UpdateAlliancesList(0)
  elseif cmd == "flag_released" then
    local user_alliance = self.alliance_info
    local user_alliance2 = GameGlobalData:GetData("alliance")
    if user_alliance.flag ~= 0 then
      GameTip:Show(GameLoader:GetGameText("LC_MENU_ALLIANCE_FLAG_LOCKED_ALERT"))
    elseif user_alliance2.position ~= GameGlobalData.MemberPositionType.chairman then
      GameTip:Show(GameLoader:GetGameText("LC_ALERT_alliance_flag_modification_permission_not_owned"))
    else
      self:RefreshFlagList()
      self:ShowFlagSelect()
    end
  elseif cmd == "needUpdateFlagItem" then
    self:UpdateFlagItem(arg)
  elseif cmd == "flagItemReleased" then
    self:SetFlagItem(arg)
  elseif cmd == "clickedDominationBuffer" then
    local content = GameLoader:GetGameText("LC_MENU_WD_BUFF_DESC_" .. GameStateAlliance.dominationBuffer)
    GameTip:Show(content)
  elseif cmd == "btn_contacts" then
    NetMessageMgr:SendMsg(NetAPIList.friends_req.Code, null, GameUIFriend.friendListCallback, true, nil)
  elseif cmd == "switch_click" then
    DebugOut("GameUIUserAlliance.mIsSwitchOn : " .. tostring(GameUIUserAlliance.mIsSwitchOn))
    if GameUIUserAlliance.mIsSwitchOn then
      GameUIUserAlliance:TurnOff()
      GameUIUserAlliance.mIsSwitchOn = false
    else
      GameUIUserAlliance:TurnOn()
      GameUIUserAlliance.mIsSwitchOn = true
    end
  elseif cmd == "opt_btn_confirm" then
    GameUIUserAlliance:SendApplyAllianceLimit(tonumber(arg))
  elseif cmd == "btn_option" then
    GameUIUserAlliance:ShowAllianceSetting()
  elseif cmd == "shareAllianceInfoChecked" then
    if self.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_STARALLIANCE_MODIFY_NOTE] then
      self.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_STARALLIANCE_MODIFY_NOTE] = false
    else
      self.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_STARALLIANCE_MODIFY_NOTE] = true
    end
    local flash_obj = self:GetFlashObject()
    if flash_obj then
      flash_obj:InvokeASCallback("_root", "setCheckBox", self.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_STARALLIANCE_MODIFY_NOTE], FacebookGraphStorySharer.StoryType.STORY_TYPE_STARALLIANCE_MODIFY_NOTE)
    end
  end
  GameTextEdit:fscommand(self:GetFlashObject(), cmd, arg)
end
function GameUIUserAlliance:InitFlashObject()
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "onFlashObjectLoaded")
  flash_obj:InvokeASCallback("_root", "initAlliancesList")
  GameUIUserAlliance.InitDominationBuffer()
end
function GameUIUserAlliance.InitDominationBuffer()
  DebugOut("please_fuck_InitDominationBuffer_3q")
  if not GameUIUserAlliance:GetFlashObject() then
    return
  end
  if GameStateAlliance.dominationBuffer and GameStateAlliance.dominationBuffer ~= "" then
    local pngName = "wd_icon_buff_" .. GameStateAlliance.dominationBuffer .. ".png"
    local localPath = "data2/" .. pngName
    local replaceName = "alliance_buff.png"
    if DynamicResDownloader:IfResExsit(localPath) then
      GameUIUserAlliance:GetFlashObject():InvokeASCallback("_root", "setDominationBuffer")
      GameUIUserAlliance:GetFlashObject():ReplaceTexture(replaceName, pngName)
    else
      local extInfo = {}
      extInfo.pngName = pngName
      extInfo.localPath = localPath
      extInfo.replaceName = replaceName
      DynamicResDownloader:AddDynamicRes(pngName, DynamicResDownloader.resType.FESTIVAL_BANNER, extInfo, GameUIUserAlliance.bannerDownloaderCallback)
    end
  end
end
function GameUIUserAlliance.bannerDownloaderCallback(extInfo)
  if DynamicResDownloader:IfResExsit(extInfo.localPath) and GameUIUserAlliance:GetFlashObject() then
    GameUIUserAlliance:GetFlashObject():InvokeASCallback("_root", "setDominationBuffer")
    GameUIUserAlliance:GetFlashObject():ReplaceTexture(extInfo.replaceName, extInfo.pngName)
    if extInfo.callback then
      extInfo.callback()
    end
  end
end
function GameUIUserAlliance:SetTabEnabled(nameTab, isEnabled)
  self:GetFlashObject():InvokeASCallback("_root", "setTabItemEnabled", nameTab, isEnabled)
end
function GameUIUserAlliance:SetVisible(nameObject, isVisible)
  nameObject = nameObject or -1
  self:GetFlashObject():InvokeASCallback("_root", "setVisible", nameObject, isVisible)
end
function GameUIUserAlliance:GetMemberWithUserIndex(user_id)
  for i, v in ipairs(self._Members) do
    if v.id == user_id then
      return v, i
    end
  end
  return nil
end
function GameUIUserAlliance:UpdateMembersData(index_page)
  local data_alliance = GameGlobalData:GetData("alliance")
  local content = {
    alliance_id = data_alliance.id,
    index = index_page - 1,
    size = self._numMembersPage
  }
  local function netFailedCallback()
    NetMessageMgr:SendMsg(NetAPIList.alliance_members_req.Code, content, self.serverCallback, true, netFailedCallback)
  end
  NetMessageMgr:SendMsg(NetAPIList.alliance_members_req.Code, content, self.serverCallback, true, netFailedCallback)
end
function GameUIUserAlliance:UpdateMembersList(page_index)
  if page_index and page_index >= 0 then
    self.members_info.index = page_index
    local function netCallProcess()
      local alliance_info = GameGlobalData:GetData("alliance")
      local content = {
        alliance_id = alliance_info.id,
        index = self.members_info.index,
        size = self.members_info.perpage
      }
      NetMessageMgr:SendMsg(NetAPIList.alliance_members_req.Code, content, self.NetCallbackMembers, true, netCallProcess)
    end
    netCallProcess()
  else
    local user_alliance = GameGlobalData:GetData("alliance")
    local is_visible = false
    if user_alliance.position == GameGlobalData.MemberPositionType.chairman then
      is_visible = true
    end
    if self:GetFlashObject() then
      self:GetFlashObject():InvokeASCallback("_root", "syncListboxItemCount", "member", #self.members_info.data)
    end
    self:SelectMember(-1)
  end
end
function GameUIUserAlliance:SetMemberItem(index)
  local member_data = self.members_info.data[index]
  DebugWD("SetMemberItem")
  DebugWDTable(member_data)
  if member_data then
    local position_text = GameGlobalData:MemberPositionName(member_data.position)
    self:GetFlashObject():InvokeASCallback("_root", "setMemberItem", index, GameUtils:CutOutUsernameByLastDot(member_data.name), member_data.level, position_text, member_data.rank, member_data.contribution, member_data.last_login, member_data.military_rank == GameUIUserAlliance.MemberRankType.DefenseLeader)
  end
end
function GameUIUserAlliance:UpdateActivityInfo()
  if self.alliance_info then
    local user_alliance = self.alliance_info
    GameUIUserAlliance.SetAllianceLevelInfo(user_alliance.level_info)
  end
  if self.ActivitiesTable and self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "syncActivitiesItemCount", #self.ActivitiesTable)
  end
end
function GameUIUserAlliance:UpdateActivityItem(indexItem)
  local dataActivity = self.ActivitiesTable[indexItem]
  local dataString = -1
  if dataActivity then
    local infoActivity = GameDataAccessHelper:GetAllianceActivityInfo(dataActivity.id)
    local dataTable = {
      infoActivity.id,
      infoActivity.icon,
      GameLoader:GetGameText(infoActivity.actTitle),
      GameLoader:GetGameText(infoActivity.actContent),
      tostring(dataActivity.left_times)
    }
    dataString = table.concat(dataTable, "\001")
  end
  self:GetFlashObject():InvokeASCallback("_root", "setActivityItem", indexItem, dataString)
end
function GameUIUserAlliance:SelectActivity(indexItem)
  local dataActivity = self.ActivitiesTable[indexItem]
  assert(dataActivity)
  if dataActivity.status ~= 0 then
    local tipText = string.format(GameLoader:GetGameText("LC_MENU_ALLIANCE_FUNCTION_LOCKED_CHAR"), dataActivity.level_limit)
    GameTip:Show(tipText)
    return
  end
  if dataActivity.id == ActivityTypeTable.Donation then
    GameStateAlliance:AddObject(GameObjectAllianceDonation)
  elseif dataActivity.id == ActivityTypeTable.CosmicExpedition then
    GameStateAlliance:AddObject(GameObjectCosmicExpedition)
  elseif dataActivity.id == ActivityTypeTable.WDDefence then
    GameStateAlliance:AddObject(GameUIWDDefence)
  elseif dataActivity.id == ActivityTypeTable.Rebuild then
    if GameUIRebuildFleet:IsRebuildVisible(true) then
      GameStateAlliance:AddObject(GameUIFactory)
      GameUIFactory.EnterRebuild = true
      GameUIFactory.EraseFlashObjectCallback = GameUIUserAlliance.FetchActivities
    end
  else
    assert(false)
  end
end
function GameUIUserAlliance:NeedUpdateMemberItem()
  self:GetFlashObject():InvokeASCallback("_root", "needUpdateVisibleMemberItem")
end
function GameUIUserAlliance:UpdateAuditsList(page_index)
  local data_alliance = GameGlobalData:GetData("alliance")
  if page_index and page_index >= 0 then
    self.audits_info.index = page_index
    local function netCallProcess()
      local content = {
        alliance_id = data_alliance.id,
        index = self.audits_info.index,
        size = self.audits_info.perpage
      }
      NetMessageMgr:SendMsg(NetAPIList.alliance_aduits_req.Code, content, self.NetCallbackAudits, true, netCallProcess)
    end
    netCallProcess()
  else
    self:GetFlashObject():InvokeASCallback("_root", "syncListboxItemCount", "audit", #self.audits_info.data)
  end
end
function GameUIUserAlliance:SetAuditItem(index)
  local audit_data = self.audits_info.data[index]
  if audit_data then
    self:GetFlashObject():InvokeASCallback("_root", "setAuditItem", index, audit_data.rank, GameUtils:CutOutUsernameByLastDot(audit_data.name), audit_data.level, audit_data.applied_at, GameLoader:GetGameText("LC_MENU_ALLIANCE_AGREE_INFO"), GameLoader:GetGameText("LC_MENU_ALLIANCE_DISAGREE_INFO"))
  end
end
function GameUIUserAlliance:UpdateRecordsList(index)
  if index and index >= 0 then
    local user_alliance = GameGlobalData:GetData("alliance")
    local content = {
      id = user_alliance.id
    }
    NetMessageMgr:SendMsg(NetAPIList.alliance_logs_req.Code, content, self.NetCallbackLogs, true, nil)
  else
    local flash_obj = self:GetFlashObject()
    if flash_obj ~= nil then
      flash_obj:InvokeASCallback("_root", "syncListboxItemCount", "record", #GameUIUserAlliance.records_info.data)
    end
  end
end
function GameUIUserAlliance:UpdateRecordItem(index)
  local record_data = self.records_info.data[index]
  if record_data then
    local msg_content = self:GetLogStringWithTypeAndParams(record_data.type, record_data.params)
    local time_ago = record_data.time
    self:GetFlashObject():InvokeASCallback("_root", "setRecordItem", index, msg_content, time_ago)
  end
end
function GameUIUserAlliance:RatifyJoinApply(index)
  local flash_obj = self:GetFlashObject()
  local data_member = self.audits_info.data[index]
  local content = {
    ids = {}
  }
  table.insert(content.ids, data_member.id)
  NetMessageMgr:SendMsg(NetAPIList.alliance_ratify_req.Code, content, GameUIUserAlliance.NetCallbackRatify, true, nil)
end
function GameUIUserAlliance:RefuseJoinApply(index)
  local flash_obj = self:GetFlashObject()
  local data_member = self.audits_info.data[index]
  local content = {
    ids = {}
  }
  table.insert(content.ids, data_member.id)
  NetMessageMgr:SendMsg(NetAPIList.alliance_refuse_req.Code, content, GameUIUserAlliance.NetCallbackRefuse, true, nil)
end
function GameUIUserAlliance:PromoteMemberPosition(index)
  local data_member = self.members_info.data[index]
  local content = {
    user_id = data_member.id
  }
  NetMessageMgr:SendMsg(NetAPIList.alliance_promote_req.Code, content, self.NetCallbackPromote, false, nil)
end
function GameUIUserAlliance:DemoteMemberPosition(index)
  local data_member = self.members_info.data[index]
  local content = {
    user_id = data_member.id
  }
  NetMessageMgr:SendMsg(NetAPIList.alliance_demote_req.Code, content, self.NetCallbackDemoted, false, nil)
end
function GameUIUserAlliance:KickOutMember(index)
  local data_member = self.members_info.data[index]
  local content = {
    user_id = data_member.id
  }
  NetMessageMgr:SendMsg(NetAPIList.alliance_kick_req.Code, content, self.NetCallbackKick, false, nil)
end
function GameUIUserAlliance:TransferAlliance(index)
  local data_member = self.members_info.data[index]
  local content = {
    user_id = data_member.id
  }
  NetMessageMgr:SendMsg(NetAPIList.alliance_transfer_req.Code, content, self.NetCallbackDemise, true, nil)
end
function GameUIUserAlliance:ViewMemberInfo(index)
  local data_member = self.members_info.data[index]
  GameUIPlayerView:UpdateWithPlayerID(data_member.id)
end
function GameUIUserAlliance:SendMemberMail(index)
  local data_member = self.members_info.data[index]
  GameStateAlliance:AddObject(GameMail)
  GameMail:ShowBox(GameMail.MAILBOX_WRITE_TAB)
  GameMail:SetWrite(data_member.name, "", "")
end
function GameUIUserAlliance:SendAllMemberMail(index)
  GameStateAlliance:AddObject(GameMail)
  GameMail:ShowBox(GameMail.MAILBOX_WRITE_TAB)
  GameMail:setCurrentMailType("alliance")
end
function GameUIUserAlliance:AddMemberFriend(index)
  local data_member = self.members_info.data[index]
  local content = {
    id = data_member.id,
    name = data_member.name
  }
  NetMessageMgr:SendMsg(NetAPIList.add_friend_req.Code, content, self.NetCallbackAddFriend, false, nil)
end
function GameUIUserAlliance:ModifyAllianceDesc(desc_info)
  local packet = {content = desc_info}
  NetMessageMgr:SendMsg(NetAPIList.alliance_memo_req.Code, packet, self.NetCallbackMemo, true, nil)
end
function GameUIUserAlliance:QuitAlliance()
  NetMessageMgr:SendMsg(NetAPIList.alliance_quit_req.Code, nil, GameUIUserAlliance.NetCallbackQuit, true, nil)
end
function GameUIUserAlliance:DeleteAlliance()
  NetMessageMgr:SendMsg(NetAPIList.alliance_delete_req.Code, nil, GameUIUserAlliance.NetCallbackDelete, true, nil)
end
function GameUIUserAlliance:GetActiveTab()
  return ...
end
function GameUIUserAlliance:SelectTab(tab_name)
  local last_active_tab = self:GetActiveTab()
  self:GetFlashObject():InvokeASCallback("_root", "selectTabItem", tab_name)
  self:SelectMember(-1)
  if last_active_tab == "main" then
    self.isOnShowAudits = nil
    self:ClearLocalDataAllianceInfo()
  elseif last_active_tab == "member" then
    self:ClearLocalDataMembers()
  elseif last_active_tab == "function" then
    self:ClearLocalDataActivities()
  elseif last_active_tab == "record" then
    self:ClearLocalDataRecords()
  elseif last_active_tab == "list" then
  end
  if tab_name == "main" then
    do
      local data_alliance = GameGlobalData:GetData("alliance")
      local function netCallProcess()
        local content = {
          id = data_alliance.id
        }
        NetMessageMgr:SendMsg(NetAPIList.alliance_info_req.Code, content, GameUIUserAlliance.NetCallbackAllianceInfoMine, true, netCallProcess)
      end
      netCallProcess()
    end
  elseif tab_name == "member" then
    self:UpdateMembersList(0)
  elseif tab_name == "function" then
    self:FetchActivities(true)
  elseif tab_name == "record" then
    self:UpdateRecordsList(0)
  elseif tab_name == "audit" then
    self:UpdateAuditsList(0)
  elseif tab_name == "list" then
  else
    assert(false)
  end
end
function GameUIUserAlliance:FetchActivities(forSelectTab)
  local function netCall()
    local userAllianceInfo = GameGlobalData:GetData("alliance")
    local content = {
      alliance_id = userAllianceInfo.id
    }
    NetMessageMgr:SendMsg(NetAPIList.alliance_activities_req.Code, content, GameUIUserAlliance.NetCallbackActivities, true, netCall)
  end
  netCall()
end
function GameUIUserAlliance:SelectMember(index)
  if tonumber(index) == -1 then
    GameUIUserAlliance.isOnShowPopWindow = nil
  else
    GameUIUserAlliance.isOnShowPopWindow = true
  end
  local data_userinfo = GameGlobalData:GetData("userinfo")
  local data_alliance = GameGlobalData:GetData("alliance")
  local data_member = self.members_info.data[index]
  local from_position = -1
  local to_position = -1
  if data_member then
    if data_member.id == data_userinfo.player_id then
      return
    end
    from_position = data_alliance.position
    to_position = data_member.position
  end
  self:GetFlashObject():InvokeASCallback("_root", "selectMemberItem", index)
  self:GetFlashObject():InvokeASCallback("_root", "showOperationMenu", from_position, to_position)
end
function GameUIUserAlliance:UpdateUserAllianceInfo()
  local data_alliance = GameGlobalData:GetData("alliance")
  if self.alliance_info then
    local user_alliance = self.alliance_info
    local flash_obj = self:GetFlashObject()
    local enable_editinfo
    if data_alliance.position == GameGlobalData.MemberPositionType.chairman then
      enable_editinfo = true
    end
    local money = ""
    local brick = ""
    local alliance_res = GameGlobalData:GetData("alliance_resource")
    if alliance_res ~= nil and alliance_res.resource ~= nil and alliance_res.resource.money ~= nil and alliance_res.resource.brick ~= nil then
      money = alliance_res.resource.money
      brick = alliance_res.resource.brick
    end
    if flash_obj ~= nil then
      flash_obj:InvokeASCallback("_root", "setAllianceBaseInfo", user_alliance.rank, user_alliance.name, user_alliance.members_count, user_alliance.members_max, GameUtils:CutOutUsernameByLastDot(user_alliance.creator_name), money, brick, GameUtils:GetFlagFrameName(user_alliance.flag), data_alliance.position == GameGlobalData.MemberPositionType.chairman)
    end
    GameUIUserAlliance.SetAllianceLevelInfo(user_alliance.level_info)
    GameUIUserAlliance.mCurMemo = user_alliance.memo
    if flash_obj ~= nil then
      flash_obj:InvokeASCallback("_root", "setAllianceDesc", user_alliance.memo)
    end
    self:UpdateControlType()
  else
    local function netCallProcess()
      local content = {
        id = data_alliance.id
      }
      NetMessageMgr:SendMsg(NetAPIList.alliance_info_req.Code, content, GameUIUserAlliance.NetCallbackAllianceInfoMine, true, netCallProcess)
    end
    netCallProcess()
  end
end
function GameUIUserAlliance:UpdateControlType()
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    local data_alliance = GameGlobalData:GetData("alliance")
    flash_obj:InvokeASCallback("_root", "setAllianceControlType", data_alliance.position)
  end
end
function GameUIUserAlliance:GetRollTextString()
  local max = math.min(#self.records_info.data, 6)
  local rollText = ""
  for i = 1, max do
    local record_data = self.records_info.data[i]
    local msg_content = self:GetLogStringWithTypeAndParams(record_data.type, record_data.params)
    rollText = rollText .. msg_content .. "   "
    if i < max then
      rollText = rollText .. "   "
    end
  end
  return rollText
end
function GameUIUserAlliance:SetRollText()
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    DebugWD("SetRollText", rollText)
    flash_obj:InvokeASCallback("_root", "setRollTextInfo", self:GetRollTextString())
  end
end
function GameUIUserAlliance.RefreshAllianceLevelInfo(content)
  GameUIUserAlliance.SetAllianceLevelInfo(content)
end
function GameUIUserAlliance.SetAllianceLevelInfo(level_info)
  local flash_obj = GameUIUserAlliance:GetFlashObject()
  if not flash_obj then
    return
  end
  DebugOut("SetAllianceLevelInfo")
  DebugTable(level_info)
  flash_obj:InvokeASCallback("_root", "setAllianceLevelInfo", GameLoader:GetGameText("LC_MENU_Level") .. level_info.level, level_info.experience, level_info.uplevel_experience, GameUtils.numberConversion(level_info.experience), GameUtils.numberConversion(level_info.uplevel_experience))
end
function GameUIUserAlliance:Update(dt)
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    flash_obj:Update(dt)
    flash_obj:InvokeASCallback("_root", "onUpdateFrame", dt)
    flash_obj:InvokeASCallback("_root", "onUpdateAllianceDescFrame", dt)
    flash_obj:InvokeASCallback("_root", "updateRollTextPos", dt)
    flash_obj:InvokeASCallback("_root", "onUpdateFrameAllianceList", dt)
    flash_obj:InvokeASCallback("_root", "OnFlagUpdate")
    flash_obj:InvokeASCallback("_root", "onUpdateEditboxFrame", dt)
  end
end
function GameUIUserAlliance:GetLogStringWithTypeAndParams(log_type, params)
  do break end
  DebugWD("GetLogStringWithTypeAndParams", log_type)
  DebugWDTable(params)
  if not self.FormatLogStringTable then
    self.FormatLogStringTable = {}
    self.FormatLogStringTable[2] = function(param_array)
      local log_string = GameLoader:GetGameText("LC_MENU_MSG_JOIN_ALLIANCE_CHAR")
      log_string = string.format(log_string, GameUtils:CutOutUsernameByLastDot(param_array[1]))
      return log_string
    end
    self.FormatLogStringTable[3] = function(param_array)
      local log_string = GameLoader:GetGameText("LC_MENU_MSG_LEAVE_ALLIANCE_CHAR")
      log_string = string.format(log_string, GameUtils:CutOutUsernameByLastDot(param_array[1]))
      return log_string
    end
    self.FormatLogStringTable[4] = function(param_array)
      local log_string = GameLoader:GetGameText("LC_MENU_MSG_ALLIANCE_UP_CHAR")
      log_string = string.gsub(log_string, "<chairman_char>", GameUtils:CutOutUsernameByLastDot(param_array[1]))
      log_string = string.gsub(log_string, "<playerName_char>", GameUtils:CutOutUsernameByLastDot(param_array[2]))
      return log_string
    end
    self.FormatLogStringTable[5] = function(param_array)
      local log_string = GameLoader:GetGameText("LC_MENU_MSG_ALLIANCE_DOWN_CHAR")
      log_string = string.gsub(log_string, "<chairman_char>", GameUtils:CutOutUsernameByLastDot(param_array[1]))
      log_string = string.gsub(log_string, "<playerName_char>", GameUtils:CutOutUsernameByLastDot(param_array[2]))
      return log_string
    end
    self.FormatLogStringTable[6] = function(param_array)
      local log_string = GameLoader:GetGameText("LC_MENU_MSG_ALLIANCE_CHANGE_LEADER")
      log_string = string.gsub(log_string, "<chairman_char>", GameUtils:CutOutUsernameByLastDot(param_array[1]))
      log_string = string.gsub(log_string, "<playerName_char>", GameUtils:CutOutUsernameByLastDot(param_array[2]))
      return log_string
    end
    self.FormatLogStringTable[7] = function(param_array)
      local log_string = GameLoader:GetGameText("LC_MENU_MEMBER_PRESTIGE_CHAR")
      log_string = string.gsub(log_string, "<playerName>", GameUtils:CutOutUsernameByLastDot(param_array[1]))
      log_string = string.gsub(log_string, "<prestige_num>", tonumber(param_array[2]))
      log_string = string.gsub(log_string, "<alliexp_num>", tonumber(param_array[2]))
      return log_string
    end
    self.FormatLogStringTable[8] = function(param_array)
      local log_string = GameLoader:GetGameText("LC_MENU_ALLIANCE_DONATE_CONSTRUCTED_CHAR")
      log_string = string.gsub(log_string, "<PlayerName_char>", GameUtils:CutOutUsernameByLastDot(param_array[1]))
      log_string = string.gsub(log_string, "<AllienceExp_char>", tonumber(param_array[2]))
      return log_string
    end
    self.FormatLogStringTable[9] = function(param_array)
      local log_string = GameLoader:GetGameText("LC_MENU_RECORD_DEFDONATION_ALERT")
      log_string = string.gsub(log_string, "<playerName>", GameUtils:CutOutUsernameByLastDot(param_array[1]))
      log_string = string.gsub(log_string, "<material_num>", tonumber(param_array[2]))
      log_string = string.gsub(log_string, "<contribution_num>", tonumber(param_array[2]))
      return log_string
    end
    self.FormatLogStringTable[10] = function(param_array)
      local log_string = GameLoader:GetGameText("LC_MENU_RECORD_COSMIC_EXPEDITION_ALERT")
      local name = ""
      for i, v in ipairs(param_array) do
        name = name .. GameUtils:CutOutUsernameByLastDot(v) .. " "
      end
      log_string = string.format(log_string, name)
      return log_string
    end
    self.FormatLogStringTable[11] = function(param_array)
      local log_string = GameLoader:GetGameText("LC_MENU_RECORD_APPOINT_DEFENCER_ALERT")
      log_string = string.format(log_string, GameUtils:CutOutUsernameByLastDot(param_array[1]))
      return log_string
    end
    self.FormatLogStringTable[12] = function(param_array)
      local log_string = GameLoader:GetGameText("LC_MENU_RECORD_SUCCESS_ALERT")
      log_string = string.format(log_string, tonumber(param_array[2]))
      return log_string
    end
  end
  return (...), params, log_type
end
function GameUIUserAlliance:RefreshResource()
  local flash_obj = GameUIUserAlliance:GetFlashObject()
  if flash_obj then
    local alliance_res = GameGlobalData:GetData("alliance_resource")
    if alliance_res ~= nil and alliance_res.resource ~= nil and alliance_res.resource.money ~= nil and alliance_res.resource.brick ~= nil then
      flash_obj:InvokeASCallback("_root", "updateAllianceResource", alliance_res.resource.money, alliance_res.resource.brick)
    else
      flash_obj:InvokeASCallback("_root", "updateAllianceResource", "", "")
    end
  end
end
function GameUIUserAlliance:UpdateFunctionBtnState()
  local flash_obj = GameUIUserAlliance:GetFlashObject()
  if flash_obj then
    local needHightlight = false
    local dataActivity = self.ActivitiesTable
    if dataActivity ~= nil or #dataActivity == 0 then
      for i, v in ipairs(dataActivity) do
        if 0 < v.left_times then
          needHightlight = true
          break
        end
      end
    end
    flash_obj:InvokeASCallback("_root", "setEventBtnState", needHightlight)
  end
end
function GameUIUserAlliance.NetCallbackAllianceInfoMine(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.alliance_info_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  if msgtype == NetAPIList.alliance_info_ack.Code then
    DebugWD("NetCallbackAllianceInfoMine")
    DebugWDTable(content)
    GameUIUserAlliance.alliance_info = content.alliance
    GameUIUserAlliance:UpdateUserAllianceInfo()
    local playerList = {}
    local requestParam = {notifies = playerList}
    NetMessageMgr:SendMsg(NetAPIList.alliance_notifies_req.Code, requestParam, nil, false, nil)
    return true
  end
  return false
end
function GameUIUserAlliance.NetCallbackMembers(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.alliance_members_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  if msgtype == NetAPIList.alliance_members_ack.Code then
    local members_info = GameUIUserAlliance.members_info
    members_info.data = content.members
    members_info.total = content.total
    members_info.index = content.index
    GameUIUserAlliance:UpdateMembersList()
    return true
  end
  return false
end
function GameUIUserAlliance.NetCallbackAudits(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  if msgtype == NetAPIList.alliance_aduits_ack.Code then
    local audits_info = GameUIUserAlliance.audits_info
    audits_info.data = content.members
    audits_info.total = content.total
    audits_info.index = content.index
    GameUIUserAlliance:UpdateAuditsList()
    return true
  end
  return false
end
function GameUIUserAlliance.NetCallbackQuit(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.alliance_quit_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  if msgtype == NetAPIList.alliance_quit_ack.Code then
    local data_alliance = GameGlobalData:GetData("alliance")
    data_alliance.id = ""
    data_alliance.position = nil
    local tip_text = GameLoader:GetGameText("LC_MENU_TIP_ALLIANCE_QUIT")
    tip_text = string.format(tip_text, GameUIUserAlliance.alliance_info.name)
    GameTip:Show(tip_text)
    GameStateAlliance:CheckUserAllianceStatus()
    local GameUIChat = LuaObjectManager:GetLuaObject("GameUIChat")
    GameUIChat.allianceNum = 0
    GameUIChat.MessageStack.alliance = {}
    return true
  end
  if msgtype == NetAPIList.alliance_quit_fail_ack.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  return false
end
function GameUIUserAlliance.NetCallbackDelete(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.alliance_delete_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  if msgtype == NetAPIList.alliance_delete_fail_ack.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  if msgtype == NetAPIList.alliance_delete_ack.Code then
    local data_alliance = GameGlobalData:GetData("alliance")
    data_alliance.id = ""
    data_alliance.position = nil
    return true
  end
  return false
end
function GameUIUserAlliance.NetCallbackMail(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.mail_send_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content, nil)
    end
    return true
  end
  return false
end
function GameUIUserAlliance.NetCallbackRatify(msgtype, content)
  DebugWD("NetCallbackRatify")
  DebugWD(msgtype)
  DebugWDTable(content)
  if msgtype == NetAPIList.alliance_ratify_ack.Code then
    local needShowError = 0
    local data_userinfo = GameGlobalData:GetData("userinfo")
    local receivers = {}
    for i, v in ipairs(content.result) do
      if v.code == 0 then
        table.insert(receivers, v.user_name)
      else
        needShowError = v.code
      end
    end
    if #receivers > 0 then
      local mail_title = GameLoader:GetGameText("LC_MENU_TITLE_ALLIANCE_MAIL_RATIFY")
      local mail_content = GameLoader:GetGameText("LC_MENU_CONTENT_ALLIANCE_MAIL_RATIFY")
      mail_content = string.gsub(mail_content, "<playerName_char>", GameUtils:CutOutUsernameByLastDot(data_userinfo.name))
      mail_content = string.gsub(mail_content, "<alliance_char>", GameUIUserAlliance.alliance_info.name)
      local packet_mail = {
        recevier_names = receivers,
        title = mail_title,
        content = mail_content
      }
      NetMessageMgr:SendMsg(NetAPIList.mail_send_req.Code, packet_mail, GameUIUserAlliance.NetCallbackMail, false, nil)
      GameUIUserAlliance:UpdateAuditsList(0)
    end
    if needShowError ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", needShowError, nil)
    end
    return true
  end
  if msgtype == NetAPIList.alliance_refuse_result.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.alliance_ratify_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  return false
end
function GameUIUserAlliance.NetCallbackRefuse(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.alliance_refuse_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  if msgtype == NetAPIList.alliance_refuse_ack.Code then
    GameUIUserAlliance:UpdateAuditsList(0)
    return true
  end
  return false
end
function GameUIUserAlliance.NetCallbackKick(msgtype, content)
  if msgtype == NetAPIList.alliance_kick_ack.Code then
    local text_tip = GameLoader:GetGameText("LC_MENU_TIP_ALLIANCE_KICKOUT")
    text_tip = string.format(text_tip, content.user_name)
    GameTip:Show(text_tip, 3000)
    local data_alliance = GameGlobalData:GetData("alliance")
    local data_userinfo = GameGlobalData:GetData("userinfo")
    local mail_title = GameLoader:GetGameText("LC_MENU_TITLE_ALLIANCE_MAIL_KICKOUT")
    local mail_content = GameLoader:GetGameText("LC_MENU_CONTENT_ALLIANCE_MAIL_KICKOUT")
    mail_content = string.gsub(mail_content, "<playerName_char>", GameUtils:CutOutUsernameByLastDot(data_userinfo.name))
    mail_content = string.gsub(mail_content, "<alliance_char>", GameUIUserAlliance.alliance_info.name)
    local packet_mail = {
      recevier_names = {
        content.user_name
      },
      title = mail_title,
      content = mail_content
    }
    NetMessageMgr:SendMsg(NetAPIList.mail_send_req.Code, packet_mail, GameUIUserAlliance.NetCallbackMail, false, nil)
    GameUIUserAlliance:SelectMember(-1)
    GameUIUserAlliance:UpdateMembersList(0)
    return true
  end
  if msgtype == NetAPIList.alliance_kick_fail_ack.Code then
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  end
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.alliance_kick_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  return false
end
function GameUIUserAlliance.NetCallbackPromote(msgtype, content)
  if msgtype == NetAPIList.alliance_promote_ack.Code then
    GameUIUserAlliance:SelectMember(-1)
    GameUIUserAlliance:UpdateMembersList(0)
    return true
  elseif msgtype == NetAPIList.alliance_promote_fail_ack.Code then
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  end
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.alliance_promote_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  return false
end
function GameUIUserAlliance.NetCallbackDemoted(msgtype, content)
  if msgtype == NetAPIList.alliance_demote_ack.Code then
    GameUIUserAlliance:SelectMember(-1)
    GameUIUserAlliance:UpdateMembersList(0)
    return true
  end
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.alliance_demote_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  return false
end
function GameUIUserAlliance.NetCallbackDemise(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.alliance_transfer_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  if msgtype == NetAPIList.alliance_transfer_ack.Code then
    GameUIUserAlliance:SelectMember(-1)
    local user_alliance = GameGlobalData:GetData("alliance")
    user_alliance.position = 0
    GameUIUserAlliance:UpdateMembersList(0)
    GameUIUserAlliance:UpdateControlType()
    return true
  end
  return false
end
function GameUIUserAlliance.NetCallbackMemo(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.alliance_memo_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  if msgtype == NetAPIList.alliance_memo_ack.Code then
    GameUIUserAlliance.alliance_info = content.alliance
    if content.alliance.memo ~= GameUIUserAlliance.mCurMemo then
      GameUIUserAlliance.mCurMemo = content.alliance.memo
      do
        local countryStr = ext.GetDeviceCountry and ext.GetDeviceCountry() or "unknow"
        if GameUIUserAlliance.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_STARALLIANCE_MODIFY_NOTE] then
          local extraInfo = {}
          extraInfo.allianceNote = content.alliance.memo or ""
          DebugOut("content.memo = ", content.alliance.memo)
          if content.alliance.memo ~= "" then
            FacebookGraphStorySharer:ShareStory(FacebookGraphStorySharer.StoryType.STORY_TYPE_STARALLIANCE_MODIFY_NOTE, extraInfo)
          end
        end
      end
    else
    end
    GameUIUserAlliance:UpdateUserAllianceInfo()
    return true
  end
  return false
end
function GameUIUserAlliance.NetCallbackAddFriend(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.add_friend_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  if msgtype == NetAPIList.add_friend_ack.Code then
    GameTip:Show(GameLoader:GetGameText("LC_MENU_ADD_FRIEND_SUCC"))
    return true
  end
  return false
end
function GameUIUserAlliance.NetCallbackLogs(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.alliance_logs_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  if msgtype == NetAPIList.alliance_logs_ack.Code then
    DebugOut("NetCallbackLogs")
    DebugTable(content)
    GameUIUserAlliance.records_info.data = content.logs
    table.sort(GameUIUserAlliance.records_info.data, function(a, b)
      return a.time < b.time
    end)
    GameUIUserAlliance:UpdateRecordsList()
    GameUIUserAlliance:SetRollText()
    return true
  end
  return false
end
function GameUIUserAlliance.NetCallbackActivities(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.alliance_activities_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  if msgType == NetAPIList.alliance_activities_ack.Code then
    DebugOut("NetCallbackActivities")
    DebugTable(content)
    GameUIUserAlliance.ActivitiesTable = content.activities
    table.sort(GameUIUserAlliance.ActivitiesTable, function(a, b)
      return a.id < b.id
    end)
    GameUIUserAlliance:UpdateActivityInfo()
    GameUIUserAlliance:UpdateFunctionBtnState()
    return true
  end
  return false
end
if AutoUpdate.isAndroidDevice then
  function GameUIUserAlliance.OnAndroidBack()
    local logMessage = "GameUIUserAlliance:" .. tostring(GameUIUserAlliance.isOnShowEditBox) .. "," .. tostring(GameUIUserAlliance.isOnShowPopWindow) .. "," .. tostring(GameUIUserAlliance.isOnShowAudits)
    GameUtils:printByAndroid(logMessage)
    if GameUIUserAlliance.isOnShowEditBox then
      GameUIUserAlliance:GetFlashObject():InvokeASCallback("_root", "descEditboxMoveOut")
    elseif GameUIUserAlliance.isOnShowPopWindow then
      GameUIUserAlliance:SelectMember(-1)
    elseif GameUIUserAlliance.isOnShowAudits then
      GameUIUserAlliance:GetFlashObject():InvokeASCallback("_root", "aduitMoveOut")
    else
      GameStateAlliance:Quit()
    end
  end
end
function GameUIUserAlliance:UpdateAlliancesList(page_index)
  local alliances_info = self.alliances_info
  if page_index and page_index >= 0 then
    local function netCallProcess()
      local packet = {
        keyword = alliances_info.keyword,
        index = alliances_info.index,
        size = alliances_info.perpage
      }
      NetMessageMgr:SendMsg(NetAPIList.alliances_req.Code, packet, self.NetCallbackAlliances, true)
    end
    netCallProcess()
  else
    local flash_obj = self:GetFlashObject()
    if flash_obj ~= nil then
      self:GetFlashObject():InvokeASCallback("_root", "setAlliancesList", #self.alliances_info.data, self.alliances_info.total)
    end
  end
end
function GameUIUserAlliance:UpdateAllianceItem(index)
  local alliances_info = self.alliances_info
  local data_alliance = alliances_info.data[index]
  local flash_obj = self:GetFlashObject()
  if flash_obj ~= nil then
    flash_obj:InvokeASCallback("_root", "setAllianceItem", index, data_alliance.rank, GameUtils:CutOutUsernameByLastDot(data_alliance.name), data_alliance.level_info.level, GameUtils:CutOutUsernameByLastDot(data_alliance.creator_name), data_alliance.members_count, data_alliance.members_max)
  end
end
function GameUIUserAlliance:SelectAlliance(index)
  local data_alliance = GameUIUserAlliance.alliances_info.data[index]
  if data_alliance then
    self:GetFlashObject():InvokeASCallback("_root", "selectAllianceItem", index)
    GameUIAllianceInfo:SetAllianceData(data_alliance)
    GameUIAllianceInfo:SetApplyStatus(-1)
    GameStateAlliance:AddObject(GameUIAllianceInfo)
  end
end
function GameUIUserAlliance.NetCallbackAlliances(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.alliances_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  if msgtype == NetAPIList.alliances_ack.Code then
    local alliances_info = GameUIUserAlliance.alliances_info
    alliances_info.data = content.alliances
    alliances_info.total = content.total
    alliances_info.index = content.index
    table.sort(alliances_info.data, function(a, b)
      return a.rank < b.rank
    end)
    GameUIUserAlliance:UpdateAlliancesList()
    return true
  end
  return false
end
function GameUIUserAlliance.OnActivityTImesChangeHandler(content)
  DebugWD("GameUIUserAlliance.OnActivityTImesChangeHandler")
  DebugWDTable(content)
  if GameUIUserAlliance.ActivitiesTable ~= nil and #GameUIUserAlliance.ActivitiesTable > 0 then
    for i, v in ipairs(GameUIUserAlliance.ActivitiesTable) do
      for j, k in ipairs(content.times) do
        if v.id == k.id then
          v.left_times = k.left_times
        end
      end
    end
  end
  GameUIUserAlliance:ChangeActivityTimes()
end
function GameUIUserAlliance:ChangeActivityTimes()
  GameUIUserAlliance:UpdateActivityInfo()
  GameUIUserAlliance:UpdateFunctionBtnState()
end
function GameUIUserAlliance:ShowFlagSelect()
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    flash_obj:InvokeASCallback("_root", "showFlagSelect", GameLoader:GetGameText("LC_MENU_ALLIANCE_FLAG_TITLE"))
  end
end
GameUIUserAlliance.flagItemList = nil
function GameUIUserAlliance:BuildFlagList()
  GameUIUserAlliance.flagItemList = nil
  GameUIUserAlliance.flagItemList = {}
  local bundleIdentifier = ext.GetBundleIdentifier()
  local flag = GameData.alliance.flag
  local col = "FLAG_WORLD"
  if bundleIdentifier == AutoUpdate.bundleIdentifierType.BUNDLE_IDENTIFIER_NORMAL_CN or bundleIdentifier == AutoUpdate.bundleIdentifierType.BUNDLE_IDENTIFIER_NORMAL_ANDROID_LOCAL or bundleIdentifier == AutoUpdate.bundleIdentifierType.BUNDLE_IDENTIFIER_NORMAL_ANDROID_PLATFORM_QIHOO then
    col = "FLAG_CN"
  end
  for i = 1, #flag do
    table.insert(GameUIUserAlliance.flagItemList, flag[i][col])
  end
end
function GameUIUserAlliance:RefreshFlagList()
  if GameUIUserAlliance.flagItemList == nil then
    self:BuildFlagList()
  end
  self:GetFlashObject():InvokeASCallback("_root", "clearFlagListItem")
  if GameUIUserAlliance.flagItemList ~= nil then
    local itemCount = math.ceil(#GameUIUserAlliance.flagItemList / 6)
    self:GetFlashObject():InvokeASCallback("_root", "initFlagListItem", itemCount)
    for i = 1, itemCount do
      self:GetFlashObject():InvokeASCallback("_root", "addFlagListItem", i)
    end
    self:GetFlashObject():InvokeASCallback("_root", "SetArrowInitVisible")
  end
end
function GameUIUserAlliance:UpdateFlagItem(Id)
  local itemKey = tonumber(Id)
  local flag, name = "", ""
  for k, v in ipairs(GameUIUserAlliance.flagItemList) do
    if math.ceil(k / 6) == itemKey then
      DebugWD("print flag items")
      DebugWD(v)
      name = name .. GameLoader:GetGameText("LC_TECH_FLAG_" .. v) .. "\001"
      flag = flag .. GameUtils:GetFlagFrameName(v) .. "\001"
    end
  end
  DebugWD("UpdateFlagItem: ", itemKey, name, flag)
  self:GetFlashObject():InvokeASCallback("_root", "setFlagItem", itemKey, flag, name)
end
function GameUIUserAlliance:SetFlagItem(Id)
  local itemKey = tonumber(Id)
  DebugWD("SetFlagItem")
  DebugWD(itemKey)
  local text_title = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
  local text_content = string.format(GameLoader:GetGameText("LC_MENU_ALLIANCE_FLAG_CONFIRM_ALERT"), GameLoader:GetGameText("LC_TECH_FLAG_" .. GameUIUserAlliance.flagItemList[itemKey]))
  GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
  GameUIMessageDialog:SetYesButton(function()
    GameUIUserAlliance:SendFLagReg(GameUIUserAlliance.flagItemList[itemKey])
  end)
  GameUIMessageDialog:Display(text_title, text_content)
end
GameUIUserAlliance.SetFlag = 0
function GameUIUserAlliance:SendFLagReg(Id)
  local content = {flag = Id}
  GameUIUserAlliance.SetFlag = Id
  NetMessageMgr:SendMsg(NetAPIList.alliance_flag_req.Code, content, GameUIUserAlliance.SetFlagCallback, true, nil)
end
function GameUIUserAlliance.SetFlagCallback(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.alliance_flag_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    else
      GameUIGlobalScreen:ShowAlert("success", content.code, nil)
      local user_alliance = GameUIUserAlliance.alliance_info
      user_alliance.flag = GameUIUserAlliance.SetFlag
      GameUIUserAlliance:UpdateUserAllianceInfo()
    end
    return true
  end
  return false
end
function GameUIUserAlliance:SendApplyAllianceLimit(limitLv)
  if limitLv and limitLv == 0 then
    limitLv = 1
  end
  if GameUIUserAlliance.mIsSwitchOn then
    limitLv = 0
  end
  if nil == limitLv or limitLv < 0 then
    return
  end
  GameUIUserAlliance.mCurLimitLv = limitLv
  local req = {apply_limit = limitLv}
  DebugOutPutTable(req, "SendApplyAllianceLimit")
  NetMessageMgr:SendMsg(NetAPIList.apply_limit_update_req.Code, req, GameUIUserAlliance.SendApplyAllianceLimitCallback, true, nil)
end
function GameUIUserAlliance.SendApplyAllianceLimitCallback(msgType, content)
  DebugOutPutTable(content, "SendApplyAllianceLimitCallback")
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.apply_limit_update_req.Code then
    if content.code == 0 then
      GameUIUserAlliance.alliance_info.apply_limit = GameUIUserAlliance.mCurLimitLv
      local flashObj = GameUIUserAlliance:GetFlashObject()
      if flashObj then
        local inputText = tostring(GameUIUserAlliance.mCurLimitLv)
        if GameUIUserAlliance.mIsSwitchOn then
          inputText = "1"
        end
        flashObj:InvokeASCallback("_root", "SetDefInputText", inputText)
      end
    elseif 5 == content.code then
      GameTip:Show(GameLoader:GetGameText("LC_MENU_ALLIANCE_LEVEL_ALERT_INFO"))
    else
      local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  return false
end
function GameUIUserAlliance:TurnOn()
  local flashObj = self:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "TurnOn")
  end
end
function GameUIUserAlliance:TurnOff()
  local flashObj = self:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "TurnOff")
  end
end
function GameUIUserAlliance:ShowAllianceSetting()
  local flashObj = self:GetFlashObject()
  if flashObj then
    GameUIUserAlliance.mCurLimitLv = GameUIUserAlliance.alliance_info.apply_limit
    GameUIUserAlliance.mIsSwitchOn = GameUIUserAlliance.mCurLimitLv == 0
    local inputText = tostring(GameUIUserAlliance.mCurLimitLv)
    if GameUIUserAlliance.mIsSwitchOn then
      inputText = "1"
    end
    flashObj:InvokeASCallback("_root", "SetOnOf", GameUIUserAlliance.mIsSwitchOn)
    flashObj:InvokeASCallback("_root", "SetDefInputText", inputText)
    flashObj:InvokeASCallback("_root", "ShowAllianceSetting")
  end
end
