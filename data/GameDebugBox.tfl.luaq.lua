local GameDebugBox = LuaObjectManager:GetLuaObject("GameDebugBox")
local GameStateGlobalState = GameStateManager.GameStateGlobalState
function GameDebugBox:OnAddToGameState()
  self:LoadFlashObject()
end
function GameDebugBox:OnEraseFromGameState()
  self:UnloadFlashObject()
end
function GameDebugBox:SetText(text)
  self:GetFlashObject():InvokeASCallback("_root", "setShow")
  self:GetFlashObject():InvokeASCallback("_root", "setTextContent", text)
end
function GameDebugBox:ShowText()
  self:GetFlashObject():InvokeASCallback("_root", "setShow")
end
function GameDebugBox:HideText()
  self:GetFlashObject():InvokeASCallback("_root", "setHide")
end
