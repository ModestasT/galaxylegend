local basic = GameData.equip_enhance.basic
table.insert(basic, {
  EQUIP_TYPE = 100001,
  EQUIP_VAL1 = 16,
  EQUIP_VAL2 = 1
})
table.insert(basic, {
  EQUIP_TYPE = 100002,
  EQUIP_VAL1 = 11,
  EQUIP_VAL2 = 0
})
table.insert(basic, {
  EQUIP_TYPE = 100003,
  EQUIP_VAL1 = 8,
  EQUIP_VAL2 = 0
})
table.insert(basic, {
  EQUIP_TYPE = 100004,
  EQUIP_VAL1 = 8,
  EQUIP_VAL2 = 0
})
table.insert(basic, {
  EQUIP_TYPE = 100005,
  EQUIP_VAL1 = 8,
  EQUIP_VAL2 = 0
})
table.insert(basic, {
  EQUIP_TYPE = 100006,
  EQUIP_VAL1 = 5,
  EQUIP_VAL2 = 0
})
table.insert(basic, {
  EQUIP_TYPE = 100011,
  EQUIP_VAL1 = 16,
  EQUIP_VAL2 = 1
})
table.insert(basic, {
  EQUIP_TYPE = 100012,
  EQUIP_VAL1 = 8,
  EQUIP_VAL2 = 0
})
table.insert(basic, {
  EQUIP_TYPE = 100013,
  EQUIP_VAL1 = 8,
  EQUIP_VAL2 = 0
})
table.insert(basic, {
  EQUIP_TYPE = 100014,
  EQUIP_VAL1 = 8,
  EQUIP_VAL2 = 0
})
table.insert(basic, {
  EQUIP_TYPE = 100015,
  EQUIP_VAL1 = 8,
  EQUIP_VAL2 = 0
})
table.insert(basic, {
  EQUIP_TYPE = 100016,
  EQUIP_VAL1 = 5,
  EQUIP_VAL2 = 0
})
table.insert(basic, {
  EQUIP_TYPE = 100021,
  EQUIP_VAL1 = 20,
  EQUIP_VAL2 = 2
})
table.insert(basic, {
  EQUIP_TYPE = 100022,
  EQUIP_VAL1 = 16,
  EQUIP_VAL2 = 0
})
table.insert(basic, {
  EQUIP_TYPE = 100023,
  EQUIP_VAL1 = 12,
  EQUIP_VAL2 = 0
})
table.insert(basic, {
  EQUIP_TYPE = 100024,
  EQUIP_VAL1 = 12,
  EQUIP_VAL2 = 0
})
table.insert(basic, {
  EQUIP_TYPE = 100025,
  EQUIP_VAL1 = 12,
  EQUIP_VAL2 = 0
})
table.insert(basic, {
  EQUIP_TYPE = 100026,
  EQUIP_VAL1 = 7,
  EQUIP_VAL2 = 0
})
table.insert(basic, {
  EQUIP_TYPE = 100031,
  EQUIP_VAL1 = 40,
  EQUIP_VAL2 = 3
})
table.insert(basic, {
  EQUIP_TYPE = 100032,
  EQUIP_VAL1 = 28,
  EQUIP_VAL2 = 0
})
table.insert(basic, {
  EQUIP_TYPE = 100033,
  EQUIP_VAL1 = 20,
  EQUIP_VAL2 = 0
})
table.insert(basic, {
  EQUIP_TYPE = 100034,
  EQUIP_VAL1 = 20,
  EQUIP_VAL2 = 0
})
table.insert(basic, {
  EQUIP_TYPE = 100035,
  EQUIP_VAL1 = 20,
  EQUIP_VAL2 = 0
})
table.insert(basic, {
  EQUIP_TYPE = 100036,
  EQUIP_VAL1 = 12,
  EQUIP_VAL2 = 0
})
table.insert(basic, {
  EQUIP_TYPE = 100041,
  EQUIP_VAL1 = 80,
  EQUIP_VAL2 = 5
})
table.insert(basic, {
  EQUIP_TYPE = 100042,
  EQUIP_VAL1 = 56,
  EQUIP_VAL2 = 0
})
table.insert(basic, {
  EQUIP_TYPE = 100043,
  EQUIP_VAL1 = 40,
  EQUIP_VAL2 = 0
})
table.insert(basic, {
  EQUIP_TYPE = 100044,
  EQUIP_VAL1 = 40,
  EQUIP_VAL2 = 0
})
table.insert(basic, {
  EQUIP_TYPE = 100045,
  EQUIP_VAL1 = 40,
  EQUIP_VAL2 = 0
})
table.insert(basic, {
  EQUIP_TYPE = 100046,
  EQUIP_VAL1 = 24,
  EQUIP_VAL2 = 0
})
table.insert(basic, {
  EQUIP_TYPE = 100051,
  EQUIP_VAL1 = 160,
  EQUIP_VAL2 = 9
})
table.insert(basic, {
  EQUIP_TYPE = 100052,
  EQUIP_VAL1 = 112,
  EQUIP_VAL2 = 0
})
table.insert(basic, {
  EQUIP_TYPE = 100053,
  EQUIP_VAL1 = 80,
  EQUIP_VAL2 = 0
})
table.insert(basic, {
  EQUIP_TYPE = 100054,
  EQUIP_VAL1 = 80,
  EQUIP_VAL2 = 0
})
table.insert(basic, {
  EQUIP_TYPE = 100055,
  EQUIP_VAL1 = 80,
  EQUIP_VAL2 = 0
})
table.insert(basic, {
  EQUIP_TYPE = 100056,
  EQUIP_VAL1 = 48,
  EQUIP_VAL2 = 0
})
table.insert(basic, {
  EQUIP_TYPE = 100061,
  EQUIP_VAL1 = 320,
  EQUIP_VAL2 = 15
})
table.insert(basic, {
  EQUIP_TYPE = 100062,
  EQUIP_VAL1 = 224,
  EQUIP_VAL2 = 0
})
table.insert(basic, {
  EQUIP_TYPE = 100063,
  EQUIP_VAL1 = 160,
  EQUIP_VAL2 = 0
})
table.insert(basic, {
  EQUIP_TYPE = 100064,
  EQUIP_VAL1 = 160,
  EQUIP_VAL2 = 0
})
table.insert(basic, {
  EQUIP_TYPE = 100065,
  EQUIP_VAL1 = 160,
  EQUIP_VAL2 = 0
})
table.insert(basic, {
  EQUIP_TYPE = 100066,
  EQUIP_VAL1 = 96,
  EQUIP_VAL2 = 0
})
table.insert(basic, {
  EQUIP_TYPE = 100071,
  EQUIP_VAL1 = 640,
  EQUIP_VAL2 = 25
})
table.insert(basic, {
  EQUIP_TYPE = 100072,
  EQUIP_VAL1 = 448,
  EQUIP_VAL2 = 0
})
table.insert(basic, {
  EQUIP_TYPE = 100073,
  EQUIP_VAL1 = 320,
  EQUIP_VAL2 = 0
})
table.insert(basic, {
  EQUIP_TYPE = 100074,
  EQUIP_VAL1 = 320,
  EQUIP_VAL2 = 0
})
table.insert(basic, {
  EQUIP_TYPE = 100075,
  EQUIP_VAL1 = 320,
  EQUIP_VAL2 = 0
})
table.insert(basic, {
  EQUIP_TYPE = 100076,
  EQUIP_VAL1 = 192,
  EQUIP_VAL2 = 0
})
table.insert(basic, {
  EQUIP_TYPE = 100081,
  EQUIP_VAL1 = 1280,
  EQUIP_VAL2 = 45
})
table.insert(basic, {
  EQUIP_TYPE = 100082,
  EQUIP_VAL1 = 896,
  EQUIP_VAL2 = 0
})
table.insert(basic, {
  EQUIP_TYPE = 100083,
  EQUIP_VAL1 = 640,
  EQUIP_VAL2 = 0
})
table.insert(basic, {
  EQUIP_TYPE = 100084,
  EQUIP_VAL1 = 640,
  EQUIP_VAL2 = 0
})
table.insert(basic, {
  EQUIP_TYPE = 100085,
  EQUIP_VAL1 = 640,
  EQUIP_VAL2 = 0
})
table.insert(basic, {
  EQUIP_TYPE = 100086,
  EQUIP_VAL1 = 384,
  EQUIP_VAL2 = 0
})
table.insert(basic, {
  EQUIP_TYPE = 100091,
  EQUIP_VAL1 = 2560,
  EQUIP_VAL2 = 79
})
table.insert(basic, {
  EQUIP_TYPE = 100092,
  EQUIP_VAL1 = 1792,
  EQUIP_VAL2 = 0
})
table.insert(basic, {
  EQUIP_TYPE = 100093,
  EQUIP_VAL1 = 1280,
  EQUIP_VAL2 = 0
})
table.insert(basic, {
  EQUIP_TYPE = 100094,
  EQUIP_VAL1 = 1280,
  EQUIP_VAL2 = 0
})
table.insert(basic, {
  EQUIP_TYPE = 100095,
  EQUIP_VAL1 = 1280,
  EQUIP_VAL2 = 0
})
table.insert(basic, {
  EQUIP_TYPE = 100096,
  EQUIP_VAL1 = 768,
  EQUIP_VAL2 = 0
})
