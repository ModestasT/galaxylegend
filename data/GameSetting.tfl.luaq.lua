local GameSetting = LuaObjectManager:GetLuaObject("GameSetting")
local GameStateSetting = GameStateManager.GameStateSetting
local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameUIBarLeft = LuaObjectManager:GetLuaObject("GameUIBarLeft")
local FlurryStepName = "FlurryEventStepV001"
local GameAccountSelector = LuaObjectManager:GetLuaObject("GameAccountSelector")
local GameWaiting = LuaObjectManager:GetLuaObject("GameWaiting")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local GamePushNotification = LuaObjectManager:GetLuaObject("GamePushNotification")
local GameUIWDStuff = LuaObjectManager:GetLuaObject("GameUIWDStuff")
local GameVipDetailInfoPanel = LuaObjectManager:GetLuaObject("GameVipDetailInfoPanel")
local GameUIPlayerDetailInfo = LuaObjectManager:GetLuaObject("GameUIPlayerDetailInfo")
local NetMessageMgr = NetMessageMgr
local NetAPIList = NetAPIList
local GameUIChat = LuaObjectManager:GetLuaObject("GameUIChat")
GameSetting.DataType = {}
GameSetting.DataType.FriendList = 1
GameSetting.DataType.SearchFriendResult = 2
GameSetting.DataType.NoticeList = 3
GameSetting.DataType.BlockList = 4
GameSetting.DataType.tangoFriend = 5
GameSetting._current_list_mode = GameSetting.DataType.FriendList
GameSetting.tangoGooglePlayUrl = "www.tango.me/install/?src=sdk-android"
local SERVER_BRAG_URL = "galaxylegend4t://brag"
local IMAGE_URL = "https://a.portal-platform.tap4fun.com/gl/url_get/dev/dev1//images/gl_tango.jpg"
local INVITATION_URL = "galaxylegend4t://invitation"
GameSetting.mAccountBindInfo = nil
GameSetting.mT4FAccount = nil
GameSetting.mFBAccount = nil
GameSetting.curSubindex = 1
function GameSetting:OnFSCommand(cmd, arg)
  if cmd == "select_item" then
    self:SelectItem(arg)
  elseif cmd == "showHelpContent" then
    GameSetting.curSubindex = tonumber(arg)
    self:UpdateHelpData(tonumber(arg))
  elseif cmd == "GotoFacebook" then
    ext.http.openURL("https://www.facebook.com/galaxylegendofficial/")
  elseif cmd == "GotoForm" then
    ext.http.openURL("http://galaxylegend.tap4fun.com/forum/forum.php")
  elseif cmd == "GotoHomepage" then
    ext.http.openURL("http://yh.t4f.cn/")
  elseif cmd == "ShowWhich" then
    local faqtxt = GameLoader:GetGameText("LC_MENU_Contact_Title_FAQ")
    local fbtxt = GameLoader:GetGameText("LC_MENU_Contact_Title_Facebook")
    local formtxt = GameLoader:GetGameText("LC_MENU_Contact_Title_Forum")
    local contacttxt = GameLoader:GetGameText("LC_MENU_Contact_Title_Support")
    local suggesttxt = GameLoader:GetGameText("LC_MENU_Contact_Title_Suggestion")
    local hometxt = GameLoader:GetGameText("LC_MENU_Contact_Title_HomePage")
    if GameSettingData.Save_Lang == "cn" then
      self:GetFlashObject():InvokeASCallback("_root", "ShowHelpBar", true, faqtxt, fbtxt, formtxt, contacttxt, suggesttxt, hometxt)
    else
      self:GetFlashObject():InvokeASCallback("_root", "ShowHelpBar", false, faqtxt, fbtxt, formtxt, contacttxt, suggesttxt, hometxt)
    end
  elseif cmd == "update_help_item" then
    self:SetHelpItem(tonumber(arg))
  elseif cmd == "switch_turn_on" then
    if arg == "music" then
      GameSettingData.EnableMusic = true
      if GameUtils:GetActiveMusic() then
        local musicPath = GameUtils:GetActiveMusic()
        GameUtils.ActiveMusic = ""
        GameUtils:PlayMusic(musicPath)
      end
    elseif arg == "sound" then
      GameSettingData.EnableSound = true
    elseif arg == "pay_remind" then
      GameSettingData.EnablePayRemind = true
    elseif arg == "buildingName" then
      GameSettingData.EnableBuildingName = true
    elseif arg == "vipColor" then
      GameSettingData.EnableChatEffect = true
    end
  elseif cmd == "switch_turn_off" then
    if arg == "music" then
      GameSettingData.EnableMusic = false
      ext.audio.stopBackgroundMusic()
    elseif arg == "sound" then
      GameSettingData.EnableSound = false
    elseif arg == "pay_remind" then
      GameSettingData.EnablePayRemind = false
    elseif arg == "buildingName" then
      GameSettingData.EnableBuildingName = false
    elseif arg == "vipColor" then
      GameSettingData.EnableChatEffect = false
    end
  elseif cmd == "push_turn_on" then
    GamePushNotification.ChangePushSetting(arg, true)
  elseif cmd == "push_turn_off" then
    GamePushNotification.ChangePushSetting(arg, false)
  elseif cmd == "tap4fun_account_register" then
    local GameUITap4funAccount = LuaObjectManager:GetLuaObject("GameUITap4funAccount")
    GameStateManager:GetCurrentGameState():AddObject(GameUITap4funAccount)
  elseif cmd == "tap4fun_account_switch" then
    local GameUITap4funAccount = LuaObjectManager:GetLuaObject("GameUITap4funAccount")
    GameStateManager:GetCurrentGameState():AddObject(GameUITap4funAccount)
  elseif cmd == "select_help_item" then
    self:SelectHelpItem(tonumber(arg))
  elseif cmd == "select_language" then
    if GameSettingData and (GameSettingData.Save_Lang == "ido" or GameSettingData.Save_Lang == "tr") then
      GameSettingData.isSupportTrAndIdo = true
    end
    local theAppVersion
    if not AutoUpdate.localAppVersion then
      local _, _, AppVer1, AppVer2, AppVer3 = string.find(ext.GetAppVersion(), "taptap taptap (%d+).(%d+).(%d+)")
      theAppVersion = tonumber(AppVer3) + 100 * tonumber(AppVer2) + 10000 * tonumber(AppVer1)
    else
      theAppVersion = AutoUpdate.localAppVersion
    end
    if (arg == "tr" or arg == "ido") and theAppVersion < 10703 and not GameSettingData.isSupportTrAndIdo then
      local isSupportTrAndIdo = false
      local updateText
      if ext.GetBundleIdentifier() == "com.tap4fun.galaxyempire2" then
        isSupportTrAndIdo = true
      elseif ext.GetBundleIdentifier() == "com.tap4fun.galaxyempire2.chinese" then
        isSupportTrAndIdo = true
      elseif ext.GetBundleIdentifier() == "com.tap4fun.galaxyempire2_android" then
        isSupportTrAndIdo = true
      elseif ext.GetBundleIdentifier() == "com.ts.galaxyempire2_android_global" then
        isSupportTrAndIdo = true
      elseif ext.GetBundleIdentifier() == "com.tap4fun.galaxyempire2_android_amazon" then
        isSupportTrAndIdo = true
      elseif ext.GetBundleIdentifier() == "com.tap4fun.galaxyempire2_android_cafe" then
        isSupportTrAndIdo = true
      elseif ext.GetBundleIdentifier() == "com.tap4fun.galaxyempire2_android_local" then
        isSupportTrAndIdo = true
      elseif ext.GetBundleIdentifier() == "com.tap4fun.galaxyempire2_android.platform.qihoo360" then
        isSupportTrAndIdo = true
      elseif ext.GetBundleIdentifier() == "com.tap4fun.galaxyempire2_android.platform.uc" then
        isSupportTrAndIdo = true
      else
        isSupportTrAndIdo = false
      end
      if isSupportTrAndIdo then
        if arg == "tr" then
          updateText = "LC_MENU_UPDATE_VERSION_TURKEY"
        else
          updateText = "LC_MENU_UPDATE_VERSION_INDONESIA"
        end
        do
          local localAppid = ext.GetBundleIdentifier()
          if localAppid then
            local function callback()
              if GameUtils.WorldPack[localAppid] then
                ext.http.openURL("http://tap4fun.com/en/game/7")
              else
                ext.http.openURL(GameUtils.AppDownLoadURL[localAppid])
              end
            end
            local text_title = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
            GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.Caption)
            GameUIMessageDialog:SetLeftTextButton(GameLoader:GetGameText("LC_MENU_GET_RIGHT_VERSION_CANCEL"), nil, {})
            GameUIMessageDialog:SetRightTextButton(GameLoader:GetGameText("LC_MENU_GET_RIGHT_VERSION_CONFIRM"), callback, {})
            GameUIMessageDialog:Display(text_title, GameLoader:GetGameText(updateText))
          end
        end
      else
        if arg == "tr" then
          updateText = "LC_MENU_CANT_IN_TURKEY"
        else
          updateText = "LC_MENU_CANT_IN_INDONESIA"
        end
        local text_title = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
        GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.OK)
        GameUIMessageDialog:Display(text_title, GameLoader:GetGameText(updateText))
      end
    else
      GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
      GameUIMessageDialog:SetYesButton(function()
        GameSettingData.Save_Lang = arg
        GameUtils:SaveSettingData()
        GameUtils:RestartGame(200, self:GetFlashObject())
      end)
      GameUIMessageDialog:SetNoButton(function()
        GameSetting:UpdateLanguageSettingData()
      end)
      local textTitle = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
      local textInfo = GameLoader:GetGameText("LC_MENU_SURE_SWITCH_LANGUAGE")
      GameUIMessageDialog:Display(textTitle, textInfo)
    end
  elseif cmd == "close_menu" then
    GameStateManager:SetCurrentGameState(GameStateSetting:GetPreState())
    if self._closeCallback then
      self._closeCallback()
      self._closeCallback = nil
    end
  elseif cmd == "contact_us" then
    local LoginInfo = GameUtils:GetLoginInfo()
    local theAppVersion
    if not AutoUpdate.localAppVersion then
      local _, _, AppVer1, AppVer2, AppVer3 = string.find(ext.GetAppVersion(), "(%d+).(%d+).(%d+)")
      theAppVersion = tonumber(AppVer3) + 100 * tonumber(AppVer2) + 10000 * tonumber(AppVer1)
    else
      theAppVersion = AutoUpdate.localAppVersion
    end
    if ext.GetPlatform() == "iOS" and GameUtils:GetLeftNumFromOSVersion(ext.GetOSVersion()) and GameUtils:GetLeftNumFromOSVersion(ext.GetOSVersion()) < 7 then
      local text_title = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
      GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.OK)
      GameUIMessageDialog:Display(text_title, GameLoader:GetGameText("LC_MENU_IOS_VERSION_LOWER"))
    elseif theAppVersion < 10706 then
      local isSupporthelpshift = false
      local updateText
      if ext.GetBundleIdentifier() == "com.tap4fun.galaxyempire2" then
        isSupporthelpshift = true
      elseif ext.GetBundleIdentifier() == "com.ts.spacelancer" then
        isSupporthelpshift = true
      elseif ext.GetBundleIdentifier() == "com.tap4fun.galaxyempire2.chinese" then
        isSupporthelpshift = true
      elseif ext.GetBundleIdentifier() == "com.tap4fun.galaxyempire2_android" then
        isSupporthelpshift = true
      elseif ext.GetBundleIdentifier() == "com.ts.galaxyempire2_android_global" then
        isSupporthelpshift = true
      elseif ext.GetBundleIdentifier() == "com.tap4fun.galaxyempire2_android_amazon" then
        isSupporthelpshift = true
      elseif ext.GetBundleIdentifier() == "com.tap4fun.galaxyempire2_android_cafe" then
        isSupporthelpshift = true
      elseif ext.GetBundleIdentifier() == "com.tap4fun.galaxyempire2_android_local" then
        isSupporthelpshift = true
      elseif ext.GetBundleIdentifier() == "com.tap4fun.galaxyempire2_android.platform.qihoo360" then
        isSupporthelpshift = true
      elseif ext.GetBundleIdentifier() == "com.tap4fun.galaxyempire2_android.platform.uc" then
        isSupporthelpshift = true
      else
        isSupporthelpshift = false
      end
      if isSupporthelpshift then
        updateText = "LC_MENU_CS_UPDATE_TO_NEW_VERSION"
        do
          local localAppid = ext.GetBundleIdentifier()
          if localAppid then
            local function callback()
              ext.http.openURL(GameUtils.AppDownLoadURL[localAppid])
            end
            local text_title = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
            GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
            GameUIMessageDialog:SetYesButton(callback)
            GameUIMessageDialog:Display(text_title, GameLoader:GetGameText(updateText))
          end
        end
      else
        updateText = "LC_MENU_CS_CAN_NOT_CONNECT"
        local text_title = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
        GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.OK)
        GameUIMessageDialog:Display(text_title, GameLoader:GetGameText(updateText))
      end
    else
      local PlayerID = GameGlobalData:GetUserInfo().player_id
      local PlayerName = GameGlobalData:GetUserInfo().name
      local ServerName = GameUtils:GetActiveServerInfo().name
      local servinfo = GameUtils:GetActiveServerInfo()
      local ServerID = GameUtils:GetActiveServerInfo().id
      local IP = GameSetting.localIP or ""
      local Serveridentifier = GameUtils:GetActiveServerInfo().logic_id
      DebugOut("serverinfo:")
      DebugTable(servinfo)
      DebugOut("ServerName:" .. ServerName)
      DebugOut("serverID:" .. ServerID)
      local contactid = Serveridentifier .. "/" .. PlayerID .. "/" .. PlayerName
      GameUtils:printByAndroid("  contactid:" .. contactid .. "  level:" .. GameGlobalData:GetData("vipinfo").level .. "  PlayerName:" .. PlayerName .. "  ext.GetAppVersion(): " .. ext.GetAppVersion() .. "openudid:" .. ext.GetIOSOpenUdid())
      ext.http.openFAQ(contactid, GameGlobalData:GetData("vipinfo").level, 1000, PlayerName, ext.GetAppVersion(), "faq", ext.GetIOSOpenUdid(), ServerName, IP)
    end
  elseif cmd == "clickReportID" then
    self:SendReport("Account & Login Issues")
  elseif cmd == "clickReportIAP" then
    self:SendReport("Purchasing Issues")
  elseif cmd == "clickReportBUG" then
    self:SendReport("Bug Report")
  elseif cmd == "clickReportOther" then
    self:SendReport("Other Issues")
  elseif cmd == "view_forum" then
    local user_info = GameGlobalData:GetUserInfo()
    local key, secret = GameUtils:GetUserVoiceKeyAndSecret()
    local email = string.format("%s@gl.com", GameUtils:GetUserDisplayName(user_info.name))
    local user_name = GameUtils:GetUserDisplayName(user_info.name)
    local udid = GameUtils:GetUserDisplayName(user_info.name) .. "gl"
    ext.ShowUserVoice("https://tap4fun.uservoice.com", key, secret, email, user_name, udid)
  elseif cmd == "onLogoutWechat" then
    GameSettingData.wechat_refrshtoken = ""
    GameUtils:SaveSettingData()
    GameUtils.PlatfromExtLogout_Standard(self:GetFlashObject())
  elseif cmd == "restart_game" then
    if IPlatformExt.getConfigValue("UseExtLogin") == "True" then
      if IPlatformExt.getConfigValue("PlatformName") == "taptap" then
        GameUtils:RestartGame(200, self:GetFlashObject())
      else
        GameUtils.PlatfromExtLogout_Standard(self:GetFlashObject())
      end
    elseif GameUtils:IsQihooApp() then
      GameUtils:printByAndroid("-----------restart_game------------")
      GameSettingData.qihooSwitch = "true"
      GameSettingData.LastLogin = nil
      GameUtils:SaveSettingData()
      GameUtils:RestartGame(200, self:GetFlashObject())
    else
      GameUtils:RestartGame(200, self:GetFlashObject())
    end
  elseif cmd == "PassportString" then
    self.PassportString = arg
    if self.PassportString then
      self.PassportString = LuaUtils:string_trim(self.PassportString)
    end
  elseif cmd == "PasswordString" then
    self.PasswordString = arg
  elseif cmd == "PasswordConfirmString" then
    self.PasswordConfirmString = arg
  elseif cmd == "IsAgree" then
    self.IsAgree = tonumber(arg)
  elseif cmd == "AccountRegister" then
    local loginInfo = GameUtils:GetLoginInfo()
    if IPlatformExt.getConfigValue("UseExtLogin") == "True" or GameUtils:IsQihooApp() then
      self:AccountRegisterDelegate()
    elseif not loginInfo.AccountInfo or loginInfo.AccountInfo.accType == GameUtils.AccountType.guest then
      self:AccountRegisterDelegate()
    elseif GameUtils:IsFacebookAccount(loginInfo.AccountInfo) then
      self:AccountRegister()
    end
  elseif cmd == "close_registerBox" then
  elseif cmd == "quickRegistBtnCliked" then
  elseif cmd == "correction" then
    print("luacorrection:")
    GameSetting:getCorrentionData()
  elseif cmd == "close_correction" then
  elseif cmd == "correction_submit" then
    GameSetting:correctionSubmit(arg)
  elseif cmd == "correction_next" then
    GameSetting:correctionNext(arg)
  elseif cmd == "correction_copy" then
    GameSetting:correctionCopy(arg)
  elseif cmd == "btn_t4f" then
    GameSetting:LoginT4FBtnHandler()
  elseif cmd == "login_t4f" then
    GameSetting:CheckTap4funAccountValid()
  elseif cmd == "btn_fb" then
    GameSetting:LoginFacebookBtnHandler()
  elseif cmd == "login_btnClose" then
    GameSetting:HideLoginUI()
  elseif cmd == "NeedUpdateRoleListItem" then
    GameSetting:UpdateRoleListItem(tonumber(arg))
  elseif cmd == "btn_arrow" then
    if arg == "show" then
      GameSetting:SetRoleList()
      GameSetting:ShowRoleList()
    else
      GameSetting:HideRoleList()
    end
  elseif cmd == "RoleItemClicked" then
    GameSetting:OnRoleItemClicked(arg)
  elseif cmd == "use_suggestion" then
    GameSetting:initSuggestionData()
  elseif cmd == "suggestion_close" then
    GameSetting:HideUISuggestion()
  elseif cmd == "suggestion_tab" then
    GameSetting:SuggestionTab(tonumber(arg))
  elseif cmd == "submi_my_suggestion_ui" then
    GameSetting.clickMySuggestion = true
    GameSetting:SubmitMySuggestionUI()
  elseif cmd == "back_suggestion" then
  elseif cmd == "back_suggestion_ui" then
  elseif cmd == "updateItemDate" then
    GameSetting:UpdateSuggestionItemData(tonumber(arg))
  elseif cmd == "head_page" then
    GameSetting:GetPageData(tonumber(arg), 1)
  elseif cmd == "before_page" then
    GameSetting:GetPageData(tonumber(arg), GameSetting.currentPage - 1)
  elseif cmd == "select_page" then
    GameSetting:initPageList()
  elseif cmd == "after_page" then
    GameSetting:GetPageData(tonumber(arg), GameSetting.currentPage + 1)
  elseif cmd == "end_page" then
    GameSetting:GetPageData(tonumber(arg), GameSetting.currentSugPageAll)
  elseif cmd == "updatePageItemDate" then
    GameSetting:updatePageItemDate(arg)
  elseif cmd == "select_page_number" then
    local param = LuaUtils:string_split(arg, "\001")
    GameSetting:GetPageData(tonumber(param[1]), tonumber(param[2]))
  elseif cmd == "suggestionListitem" then
    GameSetting:showSuggestionInfo(tonumber(arg))
  elseif cmd == "feedback" then
    if GameSetting.clickMySuggestion then
      GameSetting:FeedbackMySuggestion(arg)
    else
      GameSetting:EditMySuggestion(arg)
    end
  elseif cmd == "edit_back_suggestion" then
    GameSetting:BackSuggestion(arg)
  elseif cmd == "support" then
    local param = LuaUtils:string_split(arg, "\001")
    if tonumber(param[2]) == 1 then
      GameSetting:SupportTheSuggestion(tonumber(param[1]))
    end
    if tonumber(param[2]) == 2 then
      GameSetting:CancelSupportTheSuggestion(tonumber(param[1]))
    end
  elseif cmd == "edit" then
    GameSetting.clickMySuggestion = false
    GameSetting:ShouEditMySuggestion(tonumber(arg))
  elseif cmd == "delete" then
    GameSetting:DeleteMySuggestion(tonumber(arg))
  elseif cmd == "suggestion_help" then
    GameUIWDStuff:Show(GameUIWDStuff.menuType.menu_type_help)
    GameUIWDStuff:SetHelpText(GameLoader:GetGameText("LC_MENU_SUGGESTION_HELP"))
  elseif cmd == "suggestion_translate" then
    GameSetting:SuggestionTranslate()
  end
  GameUtils:printByAndroid("-------<<<<<<    ..GameSetting:OnFSCommand.. >>>>>>----cmd--" .. cmd .. ",arg= " .. arg)
  if "Close_Friend_Windows" == cmd then
  elseif "Friend_List_Clicked" == cmd then
    GameSetting._current_list_mode = GameSetting.DataType.FriendList
    NetMessageMgr:SendMsg(NetAPIList.friends_req.Code, null, self.friendListCallback, true, nil)
  elseif "Add_Friend_Clicked" == cmd then
    GameSetting._current_list_mode = GameSetting.DataType.SearchFriendResult
    GameSetting:GetFlashObject():InvokeASCallback("_root", "SetSelectedPanel", GameSetting.DataType.SearchFriendResult, true)
  elseif "Notice_Clicked" == cmd then
    GameSetting._current_list_mode = GameSetting.DataType.NoticeList
    local function netFailedCallback()
      NetMessageMgr:SendMsg(NetAPIList.messages_req.Code, nil, self.loadNoticeCallback, true, netFailedCallback)
    end
    NetMessageMgr:SendMsg(NetAPIList.messages_req.Code, nil, self.loadNoticeCallback, true, netFailedCallback)
  elseif "Block_List_Clicked" == cmd then
    GameSetting._current_list_mode = GameSetting.DataType.BlockList
    self.blockClicked = true
    NetMessageMgr:SendMsg(NetAPIList.friends_req.Code, null, self.friendListCallback, true, nil)
  elseif "Search_Friend_Clicked" == cmd then
    GameSetting:SearchFriend(arg)
  elseif "Show_Pop_Up_Menu" == cmd then
    GameSetting.isOnOperation = true
    local ptPos = LuaUtils:string_split(arg, "\001")
    local item_index = tonumber(ptPos[1])
    local dataInfo = self:GetDataInfo(item_index, ptPos[2])
    self.current_Arg = arg
    local name = GameUtils:GetUserDisplayName(dataInfo.name)
    self:GetFlashObject():InvokeASCallback("_root", "ShowPopupMenu", name, arg)
  elseif "NeedUpdateItemData" == cmd then
    GameSetting:UpdateItemDetailDate(arg)
  elseif "move_out_operation" == cmd then
    GameSetting.isOnOperation = nil
  elseif "Add_Friend" == cmd then
    GameSetting:AddFriendToPlayer(arg)
  elseif "showNormalFriend" == cmd then
    GameSetting.isInviteAll = false
    GameSetting:SetNormalFriend()
  elseif "showTangoFriend" == cmd then
    GameSetting.isInviteAll = false
    GameSetting:SetTangoFriend()
  elseif "tango_invite_choose" == cmd then
    GameSetting:chooseTangoFriend(tonumber(arg))
  elseif "choose_all_tangofriend" == cmd then
    GameSetting:updateTangoFriendALlChoose()
  elseif "invite_all_tangofriend" == cmd then
    GameSetting:InviteAllTangeFriend()
  elseif "tango_invite_clicled" == cmd then
    GameSetting:InviteTangoFriend(tonumber(arg))
  end
  if "closeQuestCenter" == cmd then
  end
  if "unlock_ban_friend" == cmd then
    self:UnlockBanFriend(arg)
  end
  if cmd == "operation" then
    if arg == "view" then
      self:OperationView()
    elseif arg == "whisper" then
      self:OperationWhisper()
    elseif arg == "block" then
      self:OperationBlock()
    elseif arg == "delete" then
      self:OperationDelete()
    else
      assert(false)
    end
  end
  if cmd == "animation_over_moveout" then
    self:GetFlashObject():InvokeASCallback("_root", "hidePopMenu")
  end
  if cmd == "view_player_detail" then
    self:OperationView(arg)
  end
  GameTextEdit:fscommand(self:GetFlashObject(), cmd, arg)
end
function GameSetting.SetPushSwitchStatus(content)
  if not content then
    return
  end
  DebugOut("SetPushSwitchStatus:")
  DebugTable(content)
  local status_switch = "on"
  for k, v in pairs(content) do
    DebugOut(k, v)
    if v > 0 then
      status_switch = "on"
    else
      status_switch = "off"
    end
    if GameSetting:GetFlashObject() then
      GameSetting:GetFlashObject():InvokeASCallback("_root", "setPushSwitchStatus", k, status_switch)
    end
  end
end
function GameSetting.OnIPAddrNtf(content)
  GameSetting.localIP = content.ip
end
function GameSetting:SendReport(reportType)
  ext.dofile("data1/EngineDefault.tfl")
  local resource = GameGlobalData:GetData("resource")
  local logininfo = GameUtils:GetLoginInfo()
  local userinfo = GameGlobalData:GetData("userinfo")
  local vipLevel = GameGlobalData:GetData("vipinfo").level
  local version = ext.GetAppVersion()
  local openUDID = ext.GetIOSOpenUdid()
  local username = userinfo.name
  local receiver = "support@contact.tap4fun.com"
  local catagory = reportType
  local lang = ext.GetDeviceLanguage()
  if lang == "zh_Hans" then
    lang = "zh-Hans"
  elseif lang == "zh_Hant" then
    lang = "zh-Hant"
  end
  local channel = ext.GetChannel()
  local DeviceType = ext.GetDeviceName()
  local osVersion = ext.GetOSVersion()
  local serverName = logininfo.ServerInfo.name or ""
  local version = ext.GetAppVersion()
  local account = ""
  if GameSettingData and GameSettingData.Tap4funAccount then
    account = GameSettingData.Tap4funAccount.passport
  end
  if resource and resource.total_amount and resource.total_amount > 0 then
    receiver = "vip@contact.tap4fun.com"
  end
  local preTitle = "DO NOT DELETE"
  if GameSettingData.Save_Lang == "cn" then
    preTitle = "\232\175\183\229\139\191\229\136\160\233\153\164"
  end
  local UTCTime = os.date("%Y-%m-%d %H:%M:%S", os.time(os.date("!*t")))
  local ServerTime = ""
  if GameUIBarLeft:GetServerTime() then
    ServerTime = os.date("%Y-%m-%d %H:%M:%S", GameUIBarLeft:GetServerTime())
  end
  local localTome = os.date("%Y-%m-%d %H:%M:%S", os.time())
  local title = string.format("GalaxyLegend %s [%s]", reportType, ext.GetPlatform())
  local content = string.format([[
 
                                    
                                    
                                    
                                    
                                    
                                    
    ------ %s -------               
    Server Time: %s                 
    Local Time: %s                  
    UTC Time: %s                    
    Game: %s                        
    Version:%s                      
    Account:%s                      
    Username:%s                     
    Server:%s                       
    OpenUDID:%s                     
    Category:%s                     
    Language:%s                     
    Channel:%s                      
    Device Type:%s                  
    OS Version:%s                   
    CS:%d                           
    ]], preTitle, ServerTime, localTome, UTCTime, "GalaxyLegend", version, account, username, serverName, openUDID, catagory, lang, channel, DeviceType, osVersion, EngineDefault.GetVIPLevel(resource.total_amount))
  if vipLevel > 0 then
    content = content .. " hashcode: " .. vipLevel
  end
  if not ext.sysmail.SendMail(receiver, title, content) then
    local okCallback = function()
      if ext.sysmail.OpenMailApp then
        ext.sysmail.OpenMailApp()
      end
    end
    GameUIGlobalScreen:ShowMessageBox(GameUIGlobalScreen.MessageBoxType_YesNo, GameLoader:GetGameText("LC_MENU_NOTIFICATION_CHAR"), GameLoader:GetGameText("LC_MENU_BIND_EMAIL_ACC"), okCallback, nil)
  end
end
function GameSetting:SelectItem(nameTab)
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  self:GetFlashObject():InvokeASCallback("_root", "selectItem", nameTab)
  if nameTab == "sound" then
    self:UpdateSoundSettingData()
  elseif nameTab == "userid" then
    self:UpdateUserIDSettingData()
  elseif nameTab == "help" then
    self.curSubindex = 1
  elseif nameTab == "language" then
    self:UpdateLanguageSettingData()
  elseif nameTab == "about" then
    self:UpdateAboutData()
  elseif nameTab == "push" then
    self:SetPushBtnText()
    self.SetPushSwitchStatus(GameGlobalData:GetData("push_button_info"))
  elseif nameTab == "friends" then
    NetMessageMgr:SendMsg(NetAPIList.friends_req.Code, null, GameSetting.friendListCallback, true, nil)
  else
    assert(false)
  end
end
function GameSetting:SetPushBtnText()
  local all, activity, building, arena, fleet, colonial, tax, mine, alliance
  all = GameLoader:GetGameText("LC_MENU_LOCAL_TEXT_PUSH_MESSAGE")
  activity = GameLoader:GetGameText("LC_MENU_LOCAL_TEXT_PUSH_ACTIVITY")
  building = GameLoader:GetGameText("LC_MENU_LOCAL_TEXT_PUSH_BUILDING")
  arena = GameLoader:GetGameText("LC_MENU_LOCAL_TEXT_PUSH_ARENA")
  fleet = GameLoader:GetGameText("LC_MENU_LOCAL_TEXT_PUSH_FLEET")
  colonial = GameLoader:GetGameText("LC_MENU_LOCAL_TEXT_PUSH_COLONIAL")
  tax = GameLoader:GetGameText("LC_MENU_LOCAL_TEXT_PUSH_TAX")
  mine = GameLoader:GetGameText("LC_MENU_LOCAL_TEXT_PUSH_MINE")
  alliance = GameLoader:GetGameText("LC_MENU_LOCAL_TEXT_PUSH_ALLIANCE")
  self:GetFlashObject():InvokeASCallback("_root", "SetPushDesc", all, activity, building, arena, fleet, colonial, tax, mine, alliance)
end
function GameSetting:OnAddToGameState(gameState)
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  GameGlobalData:RegisterDataChangeCallback("new_friend_cnt", GameSetting.RefreshNewFriendsCnt)
  GameSetting:CheckDownloadImage()
  if self:GetActiveItem() == "" then
    self:SelectItem("userid")
  end
  self:MoveIn()
  GameSetting:RequestBindReward()
  GameSetting:CheckFacebookEnable()
  if AutoUpdate.isAndroidDevice then
    self:GetFlashObject():InvokeASCallback("_root", "setIsTangoApp", GameUtils:IsTangoAPP())
  else
    self:GetFlashObject():InvokeASCallback("_root", "setIsTangoApp", GameUtils:IsLoginTangoAccount())
  end
  if GameSettingData and GameSettingData.Save_Lang then
    local lang = GameSettingData.Save_Lang
    if string.find(lang, "ru") == 1 then
      self:GetFlashObject():InvokeASCallback("_root", "setAboutLogoFrame", "ru")
    else
      self:GetFlashObject():InvokeASCallback("_root", "setAboutLogoFrame", "en")
    end
  end
  GameSetting.RefreshNewFriendsCnt()
end
function GameSetting:CheckDownloadImage()
  DebugOut("begin check download")
  if (ext.crc32.crc32("data2/LAZY_LOAD_setting_ui_bg_01.png") == "" or ext.crc32.crc32("data2/LAZY_LOAD_setting_ui_bg_01.png") == nil) and not AutoUpdateInBackground:IsHeroFileUpdatedToServer("data2/LAZY_LOAD_setting_ui_bg_01.png") then
    DebugOut("begin check download replace!")
    self:GetFlashObject():ReplaceTexture("LAZY_LOAD_setting_ui_bg_01.png", "territorial_map_bg.png")
  end
end
function GameSetting:GetActiveItem()
  return ...
end
function GameSetting:OnEraseFromGameState()
  GameUtils:SaveSettingData()
  self:UnloadFlashObject()
end
function GameSetting:MoveIn()
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "animationMoveIn")
end
function GameSetting:MoveOut()
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "animationMoveOut")
end
function GameSetting:UpdateSoundSettingData()
  self:GetFlashObject():InvokeASCallback("_root", "setSwitchStatus", "music", GameSettingData.EnableMusic and "on" or "off")
  self:GetFlashObject():InvokeASCallback("_root", "setSwitchStatus", "sound", GameSettingData.EnableSound and "on" or "off")
  if GameSettingData.EnablePayRemind == nil then
    GameSettingData.EnablePayRemind = true
  end
  if GameSettingData.EnableBuildingName == nil then
    GameSettingData.EnableBuildingName = true
  end
  if GameSettingData.EnableChatEffect == nil then
    GameSettingData.EnableChatEffect = true
  end
  self:GetFlashObject():InvokeASCallback("_root", "setSwitchStatus", "pay_remind", GameSettingData.EnablePayRemind and "on" or "off")
  self:GetFlashObject():InvokeASCallback("_root", "setSwitchStatus", "buildingName", GameSettingData.EnableBuildingName and "on" or "off")
  self:GetFlashObject():InvokeASCallback("_root", "setSwitchStatus", "vipColor", GameSettingData.EnableChatEffect and "on" or "off")
  local vipLevel = GameGlobalData:GetData("vipinfo").level
  self:GetFlashObject():InvokeASCallback("_root", "setVipColorSwitchVisible", vipLevel >= 10)
end
function GameSetting:UpdateUserIDSettingData()
  local LoginInfo = GameUtils:GetLoginInfo()
  DebugOut("GameSetting:UpdateUserIDSettingData")
  DebugOutPutTable(LoginInfo, "LoginInfo")
  local DataTable = {}
  local IndexData = 0
  IndexData = IndexData + 1
  DataTable[IndexData] = ""
  if LoginInfo.AccountInfo then
    if GameUtils:IsFacebookAccount(LoginInfo.AccountInfo) or GameUtils:IsLoginTangoAccount() then
      DataTable[IndexData] = LoginInfo.AccountInfo.displayName or ""
    else
      DataTable[IndexData] = LoginInfo.AccountInfo.passport or ""
    end
  end
  IndexData = IndexData + 1
  DataTable[IndexData] = LoginInfo.ServerInfo.name
  IndexData = IndexData + 1
  DataTable[IndexData] = "normal"
  IndexData = IndexData + 1
  DataTable[IndexData] = LoginInfo.PlayerInfo.name
  self:GetFlashObject():InvokeASCallback("_root", "setLoginInfo", table.concat(DataTable, "\001"))
  GameSetting:CheckAccountInfo()
end
function GameSetting:UpdateLanguageSettingData()
  DebugOut("GameSetting:UpdateLanguageSettingData", GameUtils:GetSaveLang())
  local isShowHongKongFlag = GameUtils:GetSaveLang() ~= "cn" and GameUtils:GetIsCNBundleType() == false
  self:GetFlashObject():InvokeASCallback("_root", "setShowHongKongFlag", isShowHongKongFlag)
  self:GetFlashObject():InvokeASCallback("_root", "setLanguageSelected", GameSettingData.Save_Lang)
  local disableTable = {}
  for _, languageName in ipairs(disableTable) do
    self:GetFlashObject():InvokeASCallback("_root", "setLanguageEnabled", languageName, false)
    self:GetFlashObject():SetCustomEffect("_root.main.language_page.language_" .. languageName, "Gray", true)
  end
end
function GameSetting.RefreshNewFriendsCnt()
  local number = GameGlobalData:GetData("new_friend_cnt")
  DebugOut("GameSetting.RefreshNewFriendsCnt", number)
  if number and GameSetting:GetFlashObject() then
    GameSetting:GetFlashObject():InvokeASCallback("_root", "SetFriedsNewShow", tonumber(number) > 0)
    GameSetting:GetFlashObject():InvokeASCallback("_root", "SetNoticeCount", tonumber(number))
  end
end
function GameSetting:SetDeviceFullName(name)
  GameSettingData.DeviceFullName = name
end
function GameSetting:SetDeviceRealName(name)
  GameSettingData.DeviceRealName = name
end
function GameSetting:SetGPURenderer(name)
  GameSettingData.GPURenderer = name
end
function GameSetting:SetGPUVendor(name)
  GameSettingData.GPUVendor = name
end
function GameSetting:SetCPUHZ(number)
  GameSettingData.CPUHZ = number
end
function GameSetting:SetMaxMemory(number)
  GameSettingData.MaxMemory = number
end
function GameSetting:SelectLanguage(language)
  GameSettingData.Save_Lang = language
  GameUtils:SaveSettingData()
  GameUtils:RestartGame(200, self:GetFlashObject())
end
function GameSetting:GetSavedLanguage()
  if GameSettingData and GameSettingData.Save_Lang then
    return GameSettingData.Save_Lang
  end
  return "en"
end
function GameSetting:SaveChapterEnter(combinedSectionID)
  GameSettingData.m_lastSectionShowEnterDialog = combinedSectionID
  GameUtils:SaveSettingData()
end
function GameSetting:SetUnlockNewFleet(isHaveNewFleet)
  if GameSettingData.m_newFleetUnlock ~= isHaveNewFleet then
    GameSettingData.m_newFleetUnlock = isHaveNewFleet
    GameUtils:SaveSettingData()
  end
end
function GameSetting:SaveDownloadFileName(fileName)
  local crc = ext.crc32.crc32(fileName)
  if GameSettingData["File:" .. fileName] ~= crc then
    GameSettingData["File:" .. fileName] = crc
    ext.UpdateAutoUpdateFile(fileName, crc, 0, 0)
    GameUtils:SaveSettingData()
  end
end
function GameSetting:GetFlurryEventStep()
  if not GameSettingData[FlurryStepName] then
    GameSettingData[FlurryStepName] = 1
    GameUtils:SaveSettingData()
  end
  return GameSettingData[FlurryStepName]
end
function GameSetting:SaveFlurryEventSend(flurryName, newStep)
  if immanentversion == 1 then
    GameSettingData["Flurry" .. flurryName] = true
    if newStep and (not GameSettingData[FlurryStepName] or newStep + 1 > GameSettingData[FlurryStepName]) then
      GameSettingData[FlurryStepName] = newStep + 1
    end
  elseif immanentversion == 2 then
    GameSettingData["Flurry2" .. flurryName] = true
  else
    assert(false)
  end
  GameUtils:SaveSettingData()
end
function GameSetting:CheckStopRecordFlurryEvent()
  if GameSettingData.FlurryStateStop == nil then
    GameSettingData.FlurryStateStop = false
    GameUtils:SaveSettingData()
  elseif GameSettingData.FlurryStateStop == false then
    GameSettingData.FlurryStateStop = true
    GameUtils:SaveSettingData()
  end
end
function GameSetting:IsCanSendFlurryEvent(flurryName)
  if immanentversion == 1 then
    return not GameSettingData["Flurry" .. flurryName]
  elseif immanentversion == 2 then
    return not GameSettingData["Flurry2" .. flurryName]
  end
  assert(false)
end
function GameSetting:IsDownloadFileExsit(fileName)
  local crc = ext.crc32.crc32(fileName)
  DebugActivity("IsDownloadFileExsit: " .. (GameSettingData["File:" .. fileName] or "nil ") .. " vs " .. crc)
  return GameSettingData["File:" .. fileName] == crc
end
function GameSetting:UpdateAboutData()
  local about_list = GameLoader:GetGameText("LC_MENU_ABOUT_US_CHAR")
  local appVersion = ext.GetAppVersion()
  local svn = AutoUpdate.localVersion
  about_list = string.format("Version %s[%s]", appVersion, svn) .. about_list
  self:GetFlashObject():InvokeASCallback("_root", "setLocalText", "ABOUT_LIST", about_list)
  self:GetFlashObject():InvokeASCallback("_root", "resetAboutList")
  if AutoUpdate.isDeveloper and Extra_text ~= nil and Extra_text.version ~= nil then
    DebugOut("Extra Text Version = " .. Extra_text.version)
  end
end
function GameSetting:UpdateHelpData(index)
  local help_table = GameDataAccessHelper:GetHelpItemTable_new(index)
  self:GetFlashObject():InvokeASCallback("_root", "setHelpList", #help_table, index)
end
function GameSetting:SetHelpItem(item_index)
  local help_data = GameDataAccessHelper:GetHelpItemTable_new(self.curSubindex)[item_index].ID
  local help_title = GameDataAccessHelper:GetHelpItemTitle(help_data)
  DebugOut("help_title = " .. help_title)
  self:GetFlashObject():InvokeASCallback("_root", "setHelpItem", item_index, help_title)
end
function GameSetting:SelectHelpItem(item_index)
  local help_data = GameDataAccessHelper:GetHelpItemTable_new(self.curSubindex)[item_index].ID
  local help_title = GameDataAccessHelper:GetHelpItemTitle(help_data)
  local help_content = GameDataAccessHelper:GetHelpItemContent(help_data)
  self:GetFlashObject():InvokeASCallback("_root", "showHelpContent", help_title, help_content)
end
function GameSetting:Update(dt)
  local flash_obj = self:GetFlashObject()
  flash_obj:Update(dt)
  flash_obj:InvokeASCallback("_root", "onUpdateFrame", dt)
  flash_obj:InvokeASCallback("_root", "UpdateDataList", dt)
end
function GameSetting:InitFlashObject()
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "setLocalText", "CHOOSE_SERVER", GameLoader:GetGameText("LC_MENU_CHOOSE_SERVER"))
  flash_obj:InvokeASCallback("_root", "setLocalText", "CURRENT_SERVER", GameLoader:GetGameText("LC_MENU_CURRENT_SERVER"))
  flash_obj:InvokeASCallback("_root", "setLocalText", "SERVER_STATUS_NORMAL", GameLoader:GetGameText("LC_MENU_SERVER_STATUS_NORMAL"))
  flash_obj:InvokeASCallback("_root", "setLocalText", "SERVER_STATUS_HOT", GameLoader:GetGameText("LC_MENU_SERVER_STATUS_HOT"))
  flash_obj:InvokeASCallback("_root", "setLocalText", "SERVER_STATUS_FULL", GameLoader:GetGameText("LC_MENU_SERVER_STATUS_FULL"))
  flash_obj:InvokeASCallback("_root", "setLocalText", "text_userid", GameLoader:GetGameText("LC_MENU_LOCAL_TEXT_USERID"))
  flash_obj:InvokeASCallback("_root", "setLocalText", "text_help", GameLoader:GetGameText("LC_MENU_Contact_Title_Contact"))
  flash_obj:InvokeASCallback("_root", "setLocalText", "text_friends", GameLoader:GetGameText("LC_MENU_CONTACTS_BUTTON_NEW"))
  flash_obj:InvokeASCallback("_root", "setLocalText", "text_push", GameLoader:GetGameText("LC_MENU_LOCAL_TEXT_PUSH"))
  flash_obj:InvokeASCallback("_root", "setLocalText", "text_sound", GameLoader:GetGameText("LC_MENU_SYSTEM_NEW_NAME"))
  flash_obj:InvokeASCallback("_root", "setLocalText", "text_language", GameLoader:GetGameText("LC_MENU_LOCAL_TEXT_LANGUAGE"))
  flash_obj:InvokeASCallback("_root", "setLocalText", "text_about", GameLoader:GetGameText("LC_MENU_LOCAL_TEXT_ABOUT"))
  local FlashObject = self:GetFlashObject()
  if FlashObject then
    FlashObject:InvokeASCallback("_root", "setLocalText", "LC_MENU_LOGIN_NEW_ACCOUNT", GameLoader:GetGameText("LC_MENU_LOGIN_NEW_ACCOUNT"))
    FlashObject:InvokeASCallback("_root", "setLocalText", "LC_MENU_LOGIN_EDIT_BUTTON", GameLoader:GetGameText("LC_MENU_LOGIN_EDIT_BUTTON"))
    FlashObject:InvokeASCallback("_root", "setLocalText", "LC_MENU_WORD_CHARACTER_INFO", GameLoader:GetGameText("LC_MENU_WORD_CHARACTER_INFO"))
    FlashObject:InvokeASCallback("_root", "setLocalText", "LC_MENU_PASSWORD_CHARACTER_INFO", GameLoader:GetGameText("LC_MENU_PASSWORD_CHARACTER_INFO"))
  end
end
if AutoUpdate.isAndroidDevice then
  function GameSetting.OnAndroidBack()
    if GameSetting.IsShowSuggestionWindow then
      if GameSetting.IsShowSubmitSuggestionWindow then
        GameSetting:BackSuggestion("\002\001\002")
      elseif GameUIWDStuff.menuType.menu_type_help == GameUIWDStuff.currentMenu then
        local flash_obj = GameUIWDStuff:GetFlashObject()
        if flash_obj then
          flash_obj:InvokeASCallback("_root", "hideHelp")
        end
      else
        GameSetting:OnFSCommand("suggestion_close")
      end
    else
      GameSetting:MoveOut()
    end
  end
end
function GameSetting:AccountRegisterDelegate()
  DebugOut("GameSetting:AccountRegisterDelegate")
  GameAccountSelector.isRegistByDelegate = true
  GameAccountSelector.PassportString = self.PassportString
  GameAccountSelector.PasswordString = self.PasswordString
  GameAccountSelector.PasswordConfirmString = self.PasswordConfirmString
  GameAccountSelector.IsAgree = self.IsAgree
  local LoginInfo = GameUtils:GetLoginInfo()
  DebugOutPutTable(LoginInfo, "LoginInfo")
  GameAccountSelector.LoginInfoEditing = GameUtils:GetLoginInfo()
  GameAccountSelector:AccountRegister()
end
function GameSetting:bindNewAccountComplete()
  GameSetting:RequestGetBindReward(GameUtils.AccountType.mail)
  DebugOut("bindNewAccountComplete")
  GameSetting:HideRegisterUI()
  GameSetting:UpdateUserIDSettingData()
end
function GameSetting:getCorrentionData()
  local infoURL = "trans/get_trans_list?user_id=" .. GameGlobalData:GetUserInfo().player_id .. "&server_id=" .. GameUtils:GetActiveServerInfo().logic_id .. "&locale=" .. GameSettingData.Save_Lang
  DebugOut("correntionDataURL: " .. infoURL)
  print("correntionDataURL:")
  if GameSetting:IsChinese() then
    DebugOut("https://d.portal-platform.t4f.cn:10616/")
    GameUtils:SetHTTPServer("https://d.portal-platform.t4f.cn:10616/")
  else
    DebugOut("https://d.portal-platform.t4f.cn:10616/")
    GameUtils:SetHTTPServer("https://origin-oregon-01.portal-platform.tap4fun.com:10616/")
  end
  GameUtils:HTTP_SendRequest(infoURL, nil, GameSetting.correctionDatainfo, true, GameSetting.httpRequestFailedCallback, "GET", "notencrypt")
end
function GameSetting.correctionDatainfo(content)
  DebugOut("correctionData callback")
  GameUtils:printTable(content)
  GameSetting.correctionData = {}
  GameSetting.correctionIndex = 1
  GameSetting.correctionData = content.keys
  GameSetting.correctionCount = content.trans_count
  GameUtils:printTable(GameSetting.correctionData)
  if next(GameSetting.correctionData) == nil then
    return
  end
  GameSetting:GetFlashObject():InvokeASCallback("_root", "showCorrection")
  GameSetting:updataCorrection(GameSetting.correctionData, GameSetting.correctionIndex)
end
function GameSetting.httpRequestFailedCallback(status_code, content)
  local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
  local serverName = " "
  serverName = not GameUtils:GetActiveServerInfo() or serverName .. GameUtils:GetActiveServerInfo().name or GameUtils:GetActiveServerInfo().id or ""
  local errorText = GameLoader:GetGameText("LC_MENU_CONNECT_ERROR") .. serverName
  GameSetting:showContactUsDialogForNetError(GameLoader:GetGameText("LC_MENU_ERROR_TITLE"), errorText)
end
function GameSetting:updataCorrection(correction_data, index)
  local text = GameLoader:GetGameText(correction_data[index])
  local info = GameLoader:GetGameText("LC_MENU_CORRECTION_COUNT_INFO")
  local defalutText = GameLoader:GetGameText("LC_MENU_PLAYER_SUGGESTION_INFO")
  info = string.format(info, tostring(GameSetting.correctionCount))
  DebugOut("showtext:" .. text)
  self:GetFlashObject():InvokeASCallback("_root", "updateCorrectionData", text, info, defalutText)
end
function GameSetting:correctionNext(nextString)
  DebugOut("correction next::" .. nextString)
  local arg = LuaUtils:string_split(nextString, "\001")
  if arg[2] == "\002" or GameSetting:correctionCompareString(arg[1], arg[2]) then
    GameSetting:correctionNextCommand()
  else
    GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
    GameUIMessageDialog:SetYesButton(function()
      GameSetting:correctionNextCommand()
    end)
    GameUIMessageDialog:SetNoButton(function()
      GameSetting:correctionSubmitSend(false)
    end)
    local textTitle = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
    local textInfo = GameLoader:GetGameText("LC_MENU_CORRECTION_INFO_5")
    GameUIMessageDialog:Display(textTitle, textInfo)
  end
end
function GameSetting:correctionSubmit(nowString)
  DebugOut("correction submit  " .. nowString)
  local arg = LuaUtils:string_split(nowString, "\001")
  local gameTip = LuaObjectManager:GetLuaObject("GameTip")
  if arg[2] == "\002" or GameSetting:correctionCompareString(arg[1], arg[2]) then
    gameTip:Show(GameLoader:GetGameText("LC_MENU_CORRECTION_INFO_1"))
  else
    GameSetting.correctionText = arg[2]
    GameSetting:correctionSubmitSend(false)
  end
end
function GameSetting:correctionCompareString(nowString, nextString)
  if nowString == nextString then
    return true
  end
  return false
end
function GameSetting:correctionSubmitCommand()
  DebugOut("submit command")
  DebugTable(GameSetting.correctionData)
  GameSetting.correctionIndex = GameSetting.correctionIndex + 1
  if GameSetting.correctionIndex > #GameSetting.correctionData then
    GameSetting:getCorrentionData()
  else
    GameSetting:updataCorrection(GameSetting.correctionData, GameSetting.correctionIndex)
  end
end
function GameSetting:correctionNextCommand(...)
  DebugOut("next command")
  DebugTable(GameSetting.correctionData)
  GameSetting.correctionIndex = GameSetting.correctionIndex + 1
  if GameSetting.correctionIndex > #GameSetting.correctionData then
    GameSetting:getCorrentionData()
  else
    GameSetting:updataCorrection(GameSetting.correctionData, GameSetting.correctionIndex)
  end
end
function GameSetting:correctionSubmitSend(boverwirte)
  local data = {
    user_id = GameGlobalData:GetUserInfo().player_id,
    user_name = GameGlobalData:GetUserInfo().name,
    server_id = GameUtils:GetActiveServerInfo().logic_id,
    locale = GameSettingData.Save_Lang,
    key = GameSetting.correctionData[GameSetting.correctionIndex],
    text = GameSetting.correctionText,
    overwrite = tostring(boverwirte)
  }
  GameUtils:SetHTTPPostData(ext.json.generate(data), nil)
  if GameSetting:IsChinese() then
    GameUtils:SetHTTPServer("https://d.portal-platform.t4f.cn:10616/")
  else
    GameUtils:SetHTTPServer("https://origin-oregon-01.portal-platform.tap4fun.com:10616/")
  end
  GameUtils:HTTP_SendRequest("trans/do_trans", nil, GameSetting.correctionSubmitSendCallBack, true, GameSetting.httpRequestFailedCallback, "POST")
end
function GameSetting.correctionSubmitSendCallBack(content)
  DebugOut("correctionSubmitSendCallBack:")
  local gameTip = LuaObjectManager:GetLuaObject("GameTip")
  if content.result then
    GameSetting.correctionCount = GameSetting.correctionCount + 1
    GameSetting:correctionSubmitCommand()
  elseif content.code == 1 then
    gameTip:Show(GameLoader:GetGameText("LC_MENU_CORRECTION_INFO_3"))
  elseif content.code == 2 then
    GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
    GameUIMessageDialog:SetYesButton(function()
      GameSetting:correctionSubmitSend(true)
    end)
    GameUIMessageDialog:SetNoButton(function()
      GameSetting:correctionNextCommand()
    end)
    local textTitle = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
    local textInfo = GameLoader:GetGameText("LC_MENU_CORRECTION_INFO_4")
    GameUIMessageDialog:Display(textTitle, textInfo)
  elseif content.code == 3 then
    gameTip:Show(GameLoader:GetGameText("LC_MENU_CORRECTION_INFO_6"))
  elseif content.code == 4 then
    gameTip:Show(GameLoader:GetGameText("LC_MENU_CORRECTION_INFO_7"))
  end
end
function GameSetting:correctionCopy(text)
  DebugOut("correction submit  " .. text)
  local arg = LuaUtils:string_split(text, "\001")
  if arg[2] == "\002" or GameSetting:correctionCompareString(arg[1], arg[2]) then
    self:GetFlashObject():InvokeASCallback("_root", "correctionCopy")
  else
    GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
    GameUIMessageDialog:SetYesButton(function()
      self:GetFlashObject():InvokeASCallback("_root", "correctionCopy")
    end)
    GameUIMessageDialog:SetNoButton(function()
    end)
    local textTitle = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
    local textInfo = GameLoader:GetGameText("LC_MENU_CORRECTION_INFO_2")
    GameUIMessageDialog:Display(textTitle, textInfo)
  end
end
function GameSetting:IsChinese()
  return (...), GameUtils
end
function GameSetting:CheckAccountInfo()
  local loginInfo = GameUtils:GetLoginInfo()
  DebugOut("CheckAccountInfo")
  DebugTable(loginInfo)
  if IPlatformExt.getConfigValue("UseExtLogin") == "True" or GameUtils:IsQihooApp() then
    GameUtils:RequestAccountPlayerTable(loginInfo.AccountInfo, self.RequestAccountPlayerTableCallback, self.RequestAccountPlayerTableErrorCallback)
  elseif not loginInfo.AccountInfo or loginInfo.AccountInfo.accType == GameUtils.AccountType.guest then
    local info = {isBinded = false}
    GameSetting:SetT4FLoginBtnInfo(info)
    GameSetting:SetFBLoginBtnInfo(info)
    local account = {
      accType = GameUtils.AccountType.guest,
      passport = "",
      password = ""
    }
    GameUtils:RequestAccountPlayerTable(account, self.RequestAccountPlayerTableCallback, self.RequestAccountPlayerTableErrorCallback)
  elseif GameUtils:IsFacebookAccount(loginInfo.AccountInfo) then
    GameUtils:RequestAccountPlayerTable(loginInfo.AccountInfo, self.RequestAccountPlayerTableCallback, self.RequestAccountPlayerTableErrorCallback)
  else
    GameUtils:RequestAccountPlayerTable(loginInfo.AccountInfo, self.RequestAccountPlayerTableCallback, self.RequestAccountPlayerTableErrorCallback)
  end
end
function GameSetting.RequestAccountPlayerTableCallback(content)
  GameSetting.mAccountBindInfo = content
  Facebook.mBindInfo = content
  local curPlayerId = ""
  if GameSettingData.LastLogin and GameSettingData.LastLogin.PlayerInfo then
    curPlayerId = tostring(GameSettingData.LastLogin.PlayerInfo.slogic_id) .. GameSettingData.LastLogin.PlayerInfo.name
  end
  local function sortFunc(a, b)
    if tostring(curPlayerId) == tostring(a.role.slogic_id) .. a.role.name then
      return true
    end
    if tostring(curPlayerId) == tostring(b.role.slogic_id) .. b.role.name then
      return false
    end
    if a.role.level >= b.role.level then
      return true
    else
      return false
    end
  end
  LuaUtils:BubbleSort(GameSetting.mAccountBindInfo.role_list, sortFunc)
  local loginInfo = GameUtils:GetLoginInfo()
  local t4fInfo = {isBinded = false}
  local fbInfo = {isBinded = false}
  if IPlatformExt.getConfigValue("UseExtLogin") == "True" or GameUtils:IsQihooApp() then
  elseif not loginInfo.AccountInfo or loginInfo.AccountInfo.accType == GameUtils.AccountType.guest then
  elseif GameUtils:IsFacebookAccount(loginInfo.AccountInfo) then
    fbInfo = {
      isBinded = true,
      passport = loginInfo.AccountInfo.passport
    }
    Facebook.mIsBindFacebookAccount = true
  else
    t4fInfo = {
      isBinded = true,
      passport = loginInfo.AccountInfo.passport
    }
  end
  if content.multi_acc and #content.multi_acc > 0 then
    for k, v in ipairs(content.multi_acc) do
      if v.bind_type == GameUtils.AccountType.mail then
        t4fInfo.isBinded = true
        t4fInfo.passport = v.bind_passport
      elseif v.bind_type == GameUtils.AccountType.facebook then
        fbInfo.isBinded = true
        fbInfo.passport = v.bind_passport
        Facebook.mIsBindFacebookAccount = true
      end
      if v.origin_type == GameUtils.AccountType.mail then
        t4fInfo.isBinded = true
        t4fInfo.passport = v.origin_passport
      elseif v.origin_type == GameUtils.AccountType.facebook then
        fbInfo.isBinded = true
        fbInfo.passport = v.origin_passport
        Facebook.mIsBindFacebookAccount = true
      end
    end
  else
    local info = {isBinded = false}
    GameSetting:SetT4FLoginBtnInfo(info)
    GameSetting:SetFBLoginBtnInfo(info)
  end
  if fbInfo.isBinded then
    local function getNameCallback(fbName)
      fbInfo.passport = fbName
      GameSetting:SetFBLoginBtnInfo(fbInfo)
    end
    local fbName = Facebook:GetFacebookUserNameById(fbInfo.passport, getNameCallback)
    if nil == fbName or "" == fbName then
      fbName = GameSetting:GetFBDisplayFromLoc(fbInfo.passport)
    end
    if fbName then
      fbInfo.passport = fbName
    end
  end
  GameSetting:SetT4FLoginBtnInfo(t4fInfo)
  GameSetting:SetFBLoginBtnInfo(fbInfo)
end
function GameSetting:GetFBDisplayFromLoc(fbId)
  if GameSettingData.PassportTable then
    for k, v in pairs(GameSettingData.PassportTable) do
      if fbId == v.passport then
        return v.displayName
      end
    end
  end
  return nil
end
function GameSetting.RequestAccountPlayerTableErrorCallback()
  local info = {isBinded = true, passport = ""}
  GameSetting:SetT4FLoginBtnInfo(info)
  GameSetting:SetFBLoginBtnInfo(info)
  GameSetting:CheckAccountInfo()
end
function GameSetting:SetT4FLoginBtnInfo(info)
  local flashObj = GameSetting:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "SetT4FLoginBtnInfo", info)
  end
end
function GameSetting:SetFBLoginBtnInfo(info)
  local flashObj = GameSetting:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "SetFBLoginBtnInfo", info)
  end
end
function GameSetting:SetBindRewardInfo(t4f_game_items, fb_game_items)
  local flashObj = GameSetting:GetFlashObject()
  if flashObj then
    local t4fRewardCredit = 0
    local t4fRewardSupply = 0
    for i, v in ipairs(t4f_game_items) do
      if v.item_type == "credit" then
        t4fRewardCredit = v.number
      elseif v.item_type == "pve_supply" then
        t4fRewardSupply = v.number
      end
    end
    local fbRwardCredit = 0
    local fbRwardSupply = 0
    for i, v in ipairs(fb_game_items) do
      if v.item_type == "credit" then
        fbRwardCredit = v.number
      elseif v.item_type == "pve_supply" then
        fbRwardSupply = v.number
      end
    end
    flashObj:InvokeASCallback("_root", "SetT4FLoginBtnReward", t4fRewardCredit, t4fRewardSupply)
    flashObj:InvokeASCallback("_root", "SetFBLoginBtnReward", fbRwardCredit, fbRwardSupply)
  end
end
function GameSetting:LoginT4FBtnHandler()
  GameSetting:ShowRegisterUI()
end
function GameSetting:LoginFacebookBtnHandler()
  GameSetting:FacebookLogin()
end
function GameSetting:CheckTap4funAccountValid()
  local info = GameSetting:GetFlashObject():InvokeASCallback("_root", "GetLoginInfo")
  DebugOut("input account : " .. info)
  DebugOut("ext.GetIOSOpenUdid() " .. ext.GetIOSOpenUdid())
  local acc = LuaUtils:string_split(info, "\001")
  local accountInfo = {}
  accountInfo.passport = acc[1]
  accountInfo.password = acc[2]
  if nil == accountInfo.password then
    accountInfo.password = ""
  end
  GameSetting.mT4FAccount = accountInfo
  GameUtils:Tap4funAccountValidate(accountInfo, GameSetting.CheckTap4funAccountValidCallback)
end
function GameSetting:BindTap4funAccount()
  local loginInfo = GameUtils:GetLoginInfo()
  if IPlatformExt.getConfigValue("UseExtLogin") == "True" or GameUtils:IsQihooApp() then
  elseif not loginInfo.AccountInfo or loginInfo.AccountInfo.accType == GameUtils.AccountType.guest then
    if loginInfo.PlayerInfo then
      GameUtils:Tap4funAccountBind(GameSetting.mT4FAccount, loginInfo.PlayerInfo.name, GameSetting.Tap4funAccountBindRoleCallback, GameSetting.Tap4funAccountBindRoleError)
    end
  else
    if GameUtils:IsFacebookAccount(loginInfo.AccountInfo) then
      DebugOut("loginInfo.AccountInfo = ")
      DebugTable(loginInfo.AccountInfo)
      GameSetting:BindAccount(GameUtils.AccountType.facebook, loginInfo.AccountInfo.passport, loginInfo.AccountInfo.password, GameUtils.AccountType.mail, GameSetting.mT4FAccount.passport, GameSetting.mT4FAccount.password)
    else
    end
  end
end
function GameSetting.CheckTap4funAccountValidCallback(content)
  GameWaiting:HideLoadingScreen()
  GameSetting:HideLoginUI()
  if content.is_valid then
    local function onInfoOver()
      GameSetting:BindTap4funAccount()
    end
    GameTip:Show(GameLoader:GetGameText("LC_MENU_ACCOUNT_CHEAK_RIGHT_CHAR"), 3000, onInfoOver)
  else
    GameTip:Show(GameLoader:GetGameText("LC_MENU_WRONG_USER_NAME_CHAR"))
    GameSetting:ShowRegisterUI()
  end
end
function GameSetting.Tap4funAccountBindRoleCallback(content)
  GameWaiting:HideLoadingScreen()
  local function onInfoOver()
    GameSetting:CheckAccountInfo()
  end
  local tip = GameLoader:GetGameText("LC_MENU_BINDING_ACCOUNT_SUCCESS_CHAR")
  if content.error == "ok" then
    GameUtils:AddAccountLocalData(GameSetting.mT4FAccount)
    GameUtils:AddAccountLocalData(GameSetting.mT4FAccount)
    local loginInfo = GameUtils:GetLoginInfo()
    loginInfo.AccountInfo = GameSetting.mT4FAccount
    GameUtils:SetLoginInfo(loginInfo)
    GameSettingData.LastLogin.AccountInfo = GameSetting.mT4FAccount
    GameUtils:SaveSettingData()
    GameSetting:RequestGetBindReward(GameUtils.AccountType.mail)
  elseif content.error then
    tip = GameLoader:GetGameText("LC_ALERT_" .. content.error)
    tip = tip or GameLoader:GetGameText("LC_MENU_BINDING_ACCOUNT_FAIL_CHAR")
  end
  GameTip:Show(tip, 3000, onInfoOver)
end
function GameSetting.Tap4funAccountBindRoleError(content)
  DebugOut("GameSetting.AccountBindError")
  DebugTable(content)
end
function GameSetting:ShowLoginUI()
  local flashObj = GameSetting:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "ShowLoginUI")
  end
end
function GameSetting:HideLoginUI()
  local flashObj = GameSetting:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "HideLoginUI")
  end
end
function GameSetting:ShowRegisterUI()
  local flashObj = GameSetting:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "ShowRegisterUI")
  end
end
function GameSetting:HideRegisterUI()
  local flashObj = GameSetting:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "hideAccountRegister")
  end
end
function GameSetting:FacebookLogin()
  Facebook:Login(GameSetting.FacebookLoginCallback)
end
function GameSetting.FacebookLoginCallback(success, idOrError, token, username, email)
  if not success then
    DebugOut("GameSetting.FacebookLoginCallback failed.")
  end
  if success then
    GameSetting.mFBAccount = {
      passport = idOrError,
      password = token,
      accType = GameUtils.AccountType.facebook,
      displayName = username
    }
    GameSetting:CheckFacebookAccountValid()
  else
  end
end
function GameSetting.CheckFacebookAccountValidFailedCallback(status_code, content)
  Facebook:Reset()
  local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
  local errorText = GameLoader:GetGameText("LC_MENU_CONNECT_ERROR")
  GameSetting:showContactUsDialogForNetError(GameLoader:GetGameText("LC_MENU_ERROR_TITLE"), errorText)
end
function GameSetting:CheckFacebookAccountValid()
  DebugOut("GameSetting:CheckFacebookAccountValid")
  local accountInfo = {}
  accountInfo.passport = GameSetting.mFBAccount.passport
  accountInfo.password = GameSetting.mFBAccount.password
  if nil == accountInfo.password then
    accountInfo.password = ""
  end
  GameUtils:FacebookAccountValidate(accountInfo, GameSetting.CheckFacebookAccountValidCallback, GameSetting.CheckFacebookAccountValidFailedCallback)
end
function GameSetting.CheckFacebookAccountValidCallback(content)
  GameWaiting:HideLoadingScreen()
  if content.is_valid then
    local function onInfoOver()
      local loginInfo = GameUtils:GetLoginInfo()
      if IPlatformExt.getConfigValue("UseExtLogin") == "True" or GameUtils:IsQihooApp() then
      elseif not loginInfo.AccountInfo or loginInfo.AccountInfo.accType == GameUtils.AccountType.guest then
        if loginInfo.PlayerInfo then
          GameUtils:FacebookAccountBind(GameUtils.AccountType.facebook, GameSetting.mFBAccount, loginInfo.PlayerInfo.name, GameSetting.FacebookAccountBindRoleCallback, GameSetting.FacebookAccountBindRoleErrorCallback)
        end
      elseif GameUtils:IsFacebookAccount(loginInfo.AccountInfo) then
      else
        GameSetting:BindAccount(GameUtils.AccountType.mail, loginInfo.AccountInfo.passport, loginInfo.AccountInfo.password, GameUtils.AccountType.facebook, GameSetting.mFBAccount.passport, GameSetting.mFBAccount.password)
      end
    end
    GameTip:Show(GameLoader:GetGameText("LC_MENU_ACCOUNT_CHEAK_RIGHT_CHAR"), 3000, onInfoOver)
  else
    GameTip:Show(GameLoader:GetGameText("LC_MENU_WRONG_USER_NAME_CHAR"))
  end
end
function GameSetting.FacebookAccountBindRoleCallback(content)
  GameWaiting:HideLoadingScreen()
  local function onInfoOver()
    GameSetting:CheckAccountInfo()
  end
  local fbBindFailed = false
  local tip = GameLoader:GetGameText("LC_MENU_BINDING_ACCOUNT_SUCCESS_CHAR")
  if content.error == "ok" then
    Facebook:SendFacebookPassport(GameSetting.mFBAccount.passport)
    local NewAccountData = {}
    NewAccountData.passport = GameSetting.mFBAccount.passport
    NewAccountData.password = GameSetting.mFBAccount.password
    NewAccountData.accType = GameUtils.AccountType.facebook
    NewAccountData.displayName = GameSetting.mFBAccount.displayName
    GameUtils:AddAccountLocalData(NewAccountData)
    local loginInfo = GameUtils:GetLoginInfo()
    loginInfo.AccountInfo = NewAccountData
    GameUtils:SetLoginInfo(loginInfo)
    GameSettingData.LastLogin.AccountInfo = NewAccountData
    GameUtils:SaveSettingData()
    GameSetting:RequestGetBindReward(GameUtils.AccountType.facebook)
    if GameSetting.mDelegateBindSuccessCallback then
      local ret = GameSetting.mDelegateBindSuccessCallback()
      if ret then
        fbBindFailed = true
      end
      GameSetting.mDelegateBindSuccessCallback = nil
    end
  elseif content.error then
    Facebook:Reset()
    tip = GameLoader:GetGameText("LC_ALERT_" .. content.error)
    tip = tip or GameLoader:GetGameText("LC_MENU_BINDING_ACCOUNT_FAIL_CHAR")
    if GameSetting.mDelegateBindFailedCallback then
      GameSetting.mDelegateBindFailedCallback()
      GameSetting.mDelegateBindFailedCallback = nil
    else
      fbBindFailed = true
      GameSetting:ShowChangeAccountDialog()
    end
  end
  if not fbBindFailed then
    GameTip:Show(tip, 3000, onInfoOver)
  end
end
function GameSetting.FacebookAccountBindRoleErrorCallback(content)
  Facebook:Reset()
  DebugOut("GameSetting.AccountBindError")
  DebugTable(content)
end
function GameSetting.BindAccountSuccessCallback(content)
  DebugOut("GameSetting.BindAccountSuccessCallback")
  DebugTable(content)
  GameWaiting:HideLoadingScreen()
  local function onInfoOver()
    GameSetting:CheckAccountInfo()
  end
  local tip
  local fbBindFailed = false
  if content.status == "success" then
    if GameSetting.mFBAccount then
      Facebook:SendFacebookPassport(GameSetting.mFBAccount.passport)
    end
    GameSetting:HideRegisterUI()
    GameSetting:RequestGetBindReward(GameSetting.mBindAccType)
    tip = GameLoader:GetGameText("LC_MENU_BINDING_ACCOUNT_SUCCESS_CHAR")
    if GameSetting.mDelegateBindSuccessCallback then
      local ret = GameSetting.mDelegateBindSuccessCallback()
      if ret then
        fbBindFailed = true
      end
      GameSetting.mDelegateBindSuccessCallback = nil
    end
  else
    tip = GameLoader:GetGameText("LC_ALERT_" .. content.status)
    tip = tip or GameLoader:GetGameText("LC_MENU_BINDING_ACCOUNT_FAIL_CHAR")
    if GameSetting.mDelegateBindFailedCallback then
      if GameSetting.mDelegateBindFailedCallback() then
        fbBindFailed = true
      end
      GameSetting.mDelegateBindFailedCallback = nil
    else
      fbBindFailed = true
      GameSetting:ShowChangeAccountDialog()
    end
  end
  if not fbBindFailed then
    GameTip:Show(tip, 3000, onInfoOver)
  end
end
function GameSetting.BindAccountFailedCallback(status_code, content)
  DebugOut("GameSetting.BindAccountFailedCallback")
  DebugTable(content)
  local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
  local errorText = "status_code " .. status_code
  GameUIGlobalScreen:ShowMessageBox(1, "ERROR", errorText, nil)
  if GameSetting.mDelegateBindFailedCallback then
    GameSetting.mDelegateBindFailedCallback()
    GameSetting.mDelegateBindFailedCallback = nil
  end
end
function GameSetting:RequestBindReward()
  DebugOut("RequestBindReward ")
  NetMessageMgr:SendMsg(NetAPIList.multi_acc_req.Code, nil, self.RequestBindRewardCallback, true, nil)
end
function GameSetting.RequestBindRewardCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.multi_acc_req.Code then
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.multi_acc_ack.Code then
    DebugOut("GameSetting.RequestBindRewardCallback")
    DebugTable(content)
    local t4f_game_items, fb_game_items
    for k, v in pairs(content.loot) do
      if GameUtils.AccountType.mail == v.acc_type then
        t4f_game_items = v.items
      elseif GameUtils.AccountType.facebook == v.acc_type then
        fb_game_items = v.items
      end
    end
    GameSetting:SetBindRewardInfo(t4f_game_items, fb_game_items)
    return true
  end
  return false
end
function GameSetting:RequestGetBindReward(accType)
  DebugOut("GameSetting:RequestGetBindReward " .. tostring(accType))
  if accType and type(accType) == "number" then
    DebugOut("RequestGetBindReward ")
    local req = {i = accType}
    NetMessageMgr:SendMsg(NetAPIList.multi_acc_loot_req.Code, req, nil, true, nil)
  end
end
function GameSetting:ShowRoleList()
  local flashObj = GameSetting:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "ShowRoleList")
  end
end
function GameSetting:HideRoleList()
  local flashObj = GameSetting:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "HideRoleList")
  end
end
function GameSetting:SetRoleList()
  if GameSetting.mAccountBindInfo and GameSetting.mAccountBindInfo.role_list then
    local flashObj = GameSetting:GetFlashObject()
    if flashObj then
      local itemCount = math.ceil(#GameSetting.mAccountBindInfo.role_list / 2)
      flashObj:InvokeASCallback("_root", "SetRoleList", itemCount)
    end
  end
end
function GameSetting:UpdateRoleListItem(itemId)
  local flashObj = GameSetting:GetFlashObject()
  if flashObj then
    local itemL = GameSetting.mAccountBindInfo.role_list[(itemId - 1) * 2 + 1]
    local itemR = GameSetting.mAccountBindInfo.role_list[(itemId - 1) * 2 + 2]
    local curPlayerId
    if GameSettingData.LastLogin and GameSettingData.LastLogin.PlayerInfo then
      curPlayerId = tostring(GameSettingData.LastLogin.PlayerInfo.slogic_id) .. GameSettingData.LastLogin.PlayerInfo.name
    end
    local highLightL = false
    local serverNameL, playerNameL
    local serverStopStatusL = false
    local highLightR = false
    local serverNameR, playerNameR
    local serverStopStatusR = false
    if curPlayerId then
      DebugOut("curPlayerId " .. tostring(curPlayerId))
    end
    DebugTable(GameGlobalData:GetData("userinfo"))
    DebugTable(GameSetting.mAccountBindInfo.server_list)
    if itemL then
      DebugTable(itemL)
      serverNameL = itemL.server.name
      serverStopStatusL = itemL.server.is_in_maintain
      local lv = itemL.role.level or ""
      if itemL.role.level then
        lv = GameLoader:GetGameText("LC_MENU_Level") .. lv
      end
      playerNameL = GameUtils:CutOutUsernameByLastDot(itemL.role.name) .. lv
      if curPlayerId and tostring(curPlayerId) == tostring(itemL.role.slogic_id) .. itemL.role.name then
        highLightL = true
      end
    end
    if itemR then
      DebugTable(itemR)
      serverNameR = itemR.server.name
      serverStopStatusR = itemL.server.is_in_maintain
      local lv = itemR.role.level or ""
      if itemR.role.level then
        lv = GameLoader:GetGameText("LC_MENU_Level") .. lv
      end
      playerNameR = GameUtils:CutOutUsernameByLastDot(itemR.role.name) .. lv
      if curPlayerId and tostring(curPlayerId) == tostring(itemR.slogic_id) .. itemR.role.name then
        highLightR = true
      end
    end
    flashObj:InvokeASCallback("_root", "UpdateRoleListItem", itemId, highLightL, serverNameL, playerNameL, highLightR, serverNameR, playerNameR, serverStopStatusL, serverStopStatusR)
  end
end
function GameSetting:OnRoleItemClicked(arg)
  if not GameSettingData.LastLogin then
    return
  end
  DebugOut("GameSetting:OnRoleItemClicked")
  DebugTable(GameSetting.mAccountBindInfo)
  DebugTable(GameSettingData.LastLogin)
  local param = LuaUtils:string_split(arg, "\001")
  local idx = (tonumber(param[1]) - 1) * 2 + tonumber(param[2])
  local item = GameSetting.mAccountBindInfo.role_list[idx]
  local curPlayerId
  if GameSettingData.LastLogin.PlayerInfo then
    curPlayerId = tostring(GameSettingData.LastLogin.PlayerInfo.slogic_id) .. GameSettingData.LastLogin.PlayerInfo.name
  end
  if item then
    if curPlayerId and tostring(curPlayerId) == tostring(item.role.slogic_id) .. item.role.name then
      return
    end
    local registAppid = item.register_appid
    local localAppid = ext.GetBundleIdentifier()
    if registAppid and registAppid ~= "" and GameUtils:CanLoginByAppid(registAppid, localAppid) == false then
      GameUtils:ShowErrorLoginInfo(registAppid, localAppid)
      return
    end
    if item.server.is_mix and AutoUpdate.localAppVersion < 10607 and (localAppid == AutoUpdate.bundleIdentifierType.BUNDLE_IDENTIFIER_NORMAL_CN or localAppid == AutoUpdate.bundleIdentifierType.BUNDLE_IDENTIFIER_NORMAL_ANDROID_LOCAL or localAppid == AutoUpdate.bundleIdentifierType.BUNDLE_IDENTIFIER_NORMAL_ANDROID_PLATFORM_QIHOO) then
      GameUtils:ShowVersonError(localAppid)
      return
    end
    local function okCallback()
      GameSettingData.LastLogin.PlayerInfo = item.role
      GameSettingData.LastLogin.ServerInfo = item.server
      GameSettingData.LastLogin.RegistAppId = item.register_appid or ""
      GameSettingData.LastLogin.isCreateActor = item.change_name or 1
      GameUtils:SaveSettingData()
      GameUtils:RestartGame(200, GameSetting:GetFlashObject())
    end
    local cancelCallback = function()
    end
    local tip = GameLoader:GetGameText("LC_MENU_LOGIN_CHOOSE_PLAYER_INFO")
    GameUIGlobalScreen:ShowMessageBox(2, "", tip, okCallback, cancelCallback)
  end
end
function GameSetting:ReplaceFacebookIcon(fbId)
  if fbId then
    local function callback(fbId, filename)
      local flashObj = GameSetting:GetFlashObject()
      if flashObj then
        local orgTexture = "fbicon.png"
        DebugOut("orgTexture " .. orgTexture .. " filename " .. filename)
        flashObj:ReplaceTexture(orgTexture, filename)
      end
    end
    Facebook:GetHeadIcon(fbId, callback)
  end
end
function GameSetting:CheckFacebookEnable()
  local flashObj = GameSetting:GetFlashObject()
  if flashObj then
    local fbEnable = Facebook:IsFacebookEnabled()
    flashObj:InvokeASCallback("_root", "SetFacebookEnable", fbEnable)
    local bindTap4funEnable = true
    if ext.GetBundleIdentifier() == "com.tap4fun.galaxyempire2_android.platform.BD" then
      bindTap4funEnable = false
    elseif ext.GetBundleIdentifier() == "com.tap4fun.galaxyempire2_android.platform.qihoo360" then
      bindTap4funEnable = false
    elseif ext.GetBundleIdentifier() == "com.tap4fun.galaxyempire2_android.platform.uc" then
      bindTap4funEnable = false
    elseif ext.GetBundleIdentifier() == "com.tap4fun.galaxyempire2_android_taptap" then
      bindTap4funEnable = false
      flashObj:InvokeASCallback("_root", "SetTaptap", "logout")
    end
    if not bindTap4funEnable then
      flashObj:InvokeASCallback("_root", "SetT4FBindBtnDisable")
    end
  end
end
GameSetting.mDelegateBindSuccessCallback = nil
GameSetting.mDelegateBindFailedCallback = nil
GameSetting.mBindAccType = nil
function GameSetting:BindAccount(origin_type, origin_passport, origin_password, bind_type, bind_passport, bind_password, successCallback, failedCallback)
  GameSetting.mBindAccType = bind_type
  GameUtils:BindAccount(origin_type, origin_passport, origin_password, bind_type, bind_passport, bind_password, GameSetting.BindAccountSuccessCallback, GameSetting.BindAccountFailedCallback)
end
function GameSetting:FindFBAccountRole()
  local accountPlayerTable
  local function ChangeAccount()
    local curServerId = tostring(GameUtils:GetActiveServerInfo().logic_id)
    local serverInfo, accountInfo, playerInfo
    if curServerId and accountPlayerTable then
      for k, v in pairs(accountPlayerTable.role_list) do
        if curServerId == tostring(v.role.logic_id) then
          serverInfo = v.server
          playerInfo = v.role
          break
        end
      end
    end
    if accountPlayerTable and nil == playerInfo and #accountPlayerTable.role_list > 0 then
      serverInfo = accountPlayerTable.role_list[1].server
      playerInfo = accountPlayerTable.role_list[1].role
    end
    accountInfo = GameSetting.mFBAccount
    if serverInfo and accountInfo and playerInfo then
      local NewAccountData = {}
      NewAccountData.passport = GameSetting.mFBAccount.passport
      NewAccountData.password = GameSetting.mFBAccount.password
      NewAccountData.accType = GameUtils.AccountType.facebook
      NewAccountData.displayName = GameSetting.mFBAccount.displayName
      GameUtils:AddAccountLocalData(NewAccountData)
      GameSettingData.LastLogin.PlayerInfo = playerInfo
      GameSettingData.LastLogin.ServerInfo = serverInfo
      GameSettingData.LastLogin.AccountInfo = accountInfo
      GameUtils:SaveSettingData()
      DebugOut("set GameSettingData.LastLogin")
      DebugTable(GameSettingData.LastLogin)
      GameUtils:RestartGame(200, GameSetting:GetFlashObject())
    end
  end
  local function accountPlayerTableSuccessCallback(content)
    DebugOut("accountPlayerTableSuccessCallback")
    DebugOut(accountPlayerTableSuccessCallback)
    DebugTable(content)
    accountPlayerTable = content
    ChangeAccount()
  end
  local accountPlayerTableFailedCallback = function()
    DebugOut("accountPlayerTableFailedCallback")
  end
  local AccountTable = {}
  table.insert(AccountTable, GameSetting.mFBAccount.passport)
  GameUtils:RequestAccountPlayerTable(GameSetting.mFBAccount, accountPlayerTableSuccessCallback, accountPlayerTableFailedCallback)
end
function GameSetting:ShowChangeAccountDialog()
  local function okCallback()
    Facebook:Reset()
    GameSetting:FindFBAccountRole()
  end
  local cancelCallback = function()
    Facebook:Reset()
  end
  local tip = GameLoader:GetGameText("LC_MENU_FACEBOOK_REPEAT_ID_ALERT")
  GameUIGlobalScreen:ShowMessageBox(2, "", tip, okCallback, cancelCallback)
end
function GameSetting:AccountRegister()
  DebugOut("GameSetting:AccountRegister")
  if self.IsAgree == 0 then
    GameTip:Show(GameLoader:GetGameText("LC_MENU_PROTOCOL_CONFIRM_ALERT_CHAR"))
    return
  end
  if not self.PassportString or self.PassportString == "" then
    GameTip:Show(GameLoader:GetGameText("LC_MENU_ACCOUNT_CAN_NOT_EMPTY_CHAR"))
    return
  end
  if not self.PasswordString or self.PasswordString == "" then
    GameTip:Show(GameLoader:GetGameText("LC_MENU_PASSWORD_CAN_NOT_EMPTY_CHAR"))
    return
  end
  if not self.PasswordConfirmString or self.PasswordConfirmString == "" then
    GameTip:Show(GameLoader:GetGameText("LC_MENU_PASSWORD_CONFIRM_EMPTY_CHAR"))
    return
  end
  if self.PasswordConfirmString ~= self.PasswordString then
    GameTip:Show(GameLoader:GetGameText("LC_MENU_PASSWORD_CONFIRM_ERRO_CHAR"))
    return
  end
  if GameUtils:CheckSpaceCharFromString(self.PasswordString) then
    GameTip:Show(GameLoader:GetGameText("LC_ALERT_illegal_character_password"))
    return
  end
  local AccountInfo = {}
  AccountInfo.passport = self.PassportString
  AccountInfo.password = self.PasswordString
  if GameAccountSelector:CheckAccountFormat(AccountInfo) then
    GameWaiting:ShowLoadingScreen()
    GameUtils:Tap4funAccountRegister(AccountInfo, self.AccountRegisterCallback)
  end
end
function GameSetting.AccountRegisterCallback(content)
  DebugOutPutTable(content, "AccountRegisterCallback")
  GameWaiting:HideLoadingScreen()
  if content.is_valid then
    local AccountInfo = {}
    AccountInfo.passport = GameSetting.PassportString
    AccountInfo.password = GameSetting.PasswordString
    GameUtils:AddAccountLocalData(AccountInfo)
    GameSetting.mT4FAccount = AccountInfo
    local function onInfoOver()
      GameSetting:BindTap4funAccount()
    end
    GameTip:Show(GameLoader:GetGameText("LC_MENU_REG_SUCCESS_CHAR"), 2000, onInfoOver)
  else
    local tipInfo = GameLoader:GetGameText("LC_ALERT_" .. content.error)
    DebugOut("show alert", tipInfo)
    if tipInfo then
      GameTip:Show(tipInfo)
    else
      DebugOut("unknown content.error", content.error)
      GameTip:Show("LC_ALERT_passport_not_valid")
    end
  end
end
GameSetting.currentSugTab = 1
GameSetting.currentPage = 1
GameSetting.currentSugPageAll = 1
GameSetting.currentShowInfoId = 0
GameSetting.clickMySuggestion = false
GameSetting.backsuggestionflag = false
GameSetting.currentListboxData = {}
GameSetting.suggestion_hot = {}
GameSetting.suggestion_project = {}
GameSetting.suggestion_mine = {}
GameSetting.editData = {}
GameSetting.editData.title = ""
GameSetting.editData.desc = ""
GameSetting.lastTitle = {}
GameSetting.lastContent = {}
GameSetting.saveSuccessFlag = false
function GameSetting:ShowUISuggestion()
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "MoveInSuggestionAnimation")
  end
end
function GameSetting:HideUISuggestion()
  GameSetting.suggestion_hot = {}
  GameSetting.suggestion_project = {}
  GameSetting.suggestion_mine = {}
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "MoveOutSuggestionAnimation")
    self.IsShowSuggestionWindow = false
  end
end
function GameSetting:initSuggestionData()
  if ext.is16x9() then
    GameSetting.pagecount = 6
  elseif ext.is4x3() then
    GameSetting.pagecount = 8
  else
    GameSetting.pagecount = 6
  end
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "initSuggestionItemStatus", GameSetting.pagecount)
  end
  GameSetting:SuggestionTab(GameSetting.currentSugTab)
  GameSetting:ShowUISuggestion()
  GameSetting.IsShowSuggestionWindow = true
end
function GameSetting:SuggestionTab(index)
  GameSetting.currentSugTab = index
  GameSetting.currentPage = 1
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "SelectSuggestionTab", GameSetting.currentSugTab, GameSetting.pagecount)
  end
  GameSetting:SetListboxData(GameSetting.currentSugTab, GameSetting.currentPage)
end
function GameSetting:SubmitMySuggestionUI()
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "ShowCreateMySuggestion")
  end
  DebugOut("IsShowSubmitSuggestionWindow:true")
  self.IsShowSubmitSuggestionWindow = true
end
function GameSetting:SetListboxData(tab_index, page)
  if page < 1 then
    page = 1
  elseif page > GameSetting.currentSugPageAll then
    page = GameSetting.currentSugPageAll
  end
  GameSetting.suggestion_mine = {}
  GameSetting.currentPage = page
  if tab_index == 1 then
    if GameSetting.suggestion_hot[GameSetting.currentPage] and #GameSetting.suggestion_hot[GameSetting.currentPage] > 0 then
      GameSetting.currentListboxData = GameSetting.suggestion_hot[GameSetting.currentPage]
      GameSetting.currentSugPageAll = GameSetting.suggestion_hot.count
      GameSetting:UpdateItemDate()
    else
      GameSetting:GetHotSuggestionData(GameSetting.currentPage, GameSetting.pagecount)
    end
  end
  if tab_index == 2 then
    if GameSetting.suggestion_project[GameSetting.currentPage] and 0 < #GameSetting.suggestion_project[GameSetting.currentPage] then
      GameSetting.currentListboxData = GameSetting.suggestion_project[GameSetting.currentPage]
      GameSetting.currentSugPageAll = GameSetting.suggestion_project.count
      GameSetting:UpdateItemDate()
    else
      GameSetting:GetAcceptedSuggestion(GameSetting.currentPage, GameSetting.pagecount)
    end
  end
  if tab_index == 3 then
    if GameSetting.suggestion_mine[GameSetting.currentPage] and #GameSetting.suggestion_mine[GameSetting.currentPage] > 0 then
      GameSetting.currentListboxData = GameSetting.suggestion_mine[GameSetting.currentPage]
      GameSetting.currentSugPageAll = GameSetting.suggestion_mine.count
      GameSetting:UpdateItemDate()
    else
      GameSetting:GetMyselfSuggestion(GameSetting.currentPage, GameSetting.pagecount)
    end
  end
end
function GameSetting:GetPageData(tab_index, page)
  if page < 1 then
    page = 1
  elseif page > GameSetting.currentSugPageAll then
    page = GameSetting.currentSugPageAll
  end
  GameSetting.currentPage = page
  if tab_index == 1 then
    if GameSetting.suggestion_hot[GameSetting.currentPage] and #GameSetting.suggestion_hot[GameSetting.currentPage] > 0 then
      GameSetting.currentListboxData = GameSetting.suggestion_hot[GameSetting.currentPage]
      GameSetting.currentSugPageAll = GameSetting.suggestion_hot.count
      GameSetting:UpdateItemDate()
    else
      GameSetting:GetHotSuggestionData(GameSetting.currentPage, GameSetting.pagecount)
    end
  end
  if tab_index == 2 then
    if GameSetting.suggestion_project[GameSetting.currentPage] and 0 < #GameSetting.suggestion_project[GameSetting.currentPage] then
      GameSetting.currentListboxData = GameSetting.suggestion_project[GameSetting.currentPage]
      GameSetting.currentSugPageAll = GameSetting.suggestion_project.count
      GameSetting:UpdateItemDate()
    else
      GameSetting:GetAcceptedSuggestion(GameSetting.currentPage, GameSetting.pagecount)
    end
  end
  if tab_index == 3 then
    if GameSetting.suggestion_mine[GameSetting.currentPage] and 0 < #GameSetting.suggestion_mine[GameSetting.currentPage] then
      GameSetting.currentListboxData = GameSetting.suggestion_mine[GameSetting.currentPage]
      GameSetting.currentSugPageAll = GameSetting.suggestion_mine.count
      GameSetting:UpdateItemDate()
    else
      GameSetting:GetMyselfSuggestion(GameSetting.currentPage, GameSetting.pagecount)
    end
  end
end
function GameSetting:UpdateSuggestionItemData(item_id)
  DebugOut("GameSetting:UpdateSuggestionItemData:")
  DebugTable(GameSetting.currentListboxData)
  DebugOut(item_id)
  local itemDate = GameSetting.currentListboxData[item_id]
  if itemDate then
    DebugOut("itemdata:")
    DebugTable(itemDate)
    local text = GameSetting:GetSuggestionItemData(itemDate)
    DebugOut("text:" .. text)
    if self:GetFlashObject() then
      self:GetFlashObject():InvokeASCallback("_root", "updateItemDate", GameSetting.currentSugTab, item_id, text)
    end
  end
end
function GameSetting:GetSuggestionItemData(itemDate)
  local text = ""
  text = text .. itemDate.id .. "\001"
  text = text .. itemDate.title .. "\001"
  text = text .. GameUtils:CutOutUsernameByLastDot(itemDate.user_name) .. "\001"
  local param_1 = LuaUtils:string_split(itemDate.created_at .. "-", "-")
  DebugOut("param_1[3]:" .. param_1[3])
  local param_2 = LuaUtils:string_split(param_1[3], "T")
  text = text .. param_1[2] .. "." .. param_2[1] .. "\001"
  text = text .. itemDate.support_count .. "\001"
  text = text .. GameSetting:GetSuggestionStatus(itemDate.status) .. "\001"
  text = text .. GameSetting.currentPage .. "\001"
  text = text .. GameSetting.currentSugPageAll .. "\001"
  text = text .. itemDate.status .. "\001"
  text = text .. tostring(itemDate.supported) .. "\001"
  text = text .. GameSetting.pagecount .. "\001"
  text = text .. tostring(itemDate.comment) .. "\001"
  return text
end
function GameSetting:GetSuggestionInfo(itemDate)
  DebugTable(itemDate)
  local text = ""
  text = text .. GameUtils:CutOutUsernameByLastDot(itemDate.user_name) .. "\001"
  text = text .. itemDate.slogic_name .. "\001"
  text = text .. itemDate.title .. "\001"
  text = text .. itemDate.desc .. "\001"
  text = text .. tostring(itemDate.status)
  return text
end
function GameSetting:initSuggestionPageInfo()
  local text = ""
  text = text .. GameSetting.currentPage .. "\001"
  text = text .. GameSetting.currentSugPageAll
  return text
end
function GameSetting:GetTranslate(itemDate)
  local text = ""
  return text
end
function GameSetting:GetSuggestionStatus(id)
  local status = ""
  if id == 0 then
    status = GameLoader:GetGameText("LC_MENU_SUGGESTION_STATUS_INFO_1")
  elseif id == 3 or id == 2 or id == 5 or id == 4 then
    status = GameLoader:GetGameText("LC_MENU_SUGGESTION_STATUS_INFO_3")
  elseif id == 1 then
    status = GameLoader:GetGameText("LC_MENU_SUGGESTION_STATUS_INFO_3")
  elseif id == 6 then
    status = GameLoader:GetGameText("LC_MENU_SUGGESTION_STATUS_INFO_6")
  end
  return status
end
function GameSetting:UpdateItemDate()
  if self:GetFlashObject() then
    local text = GameSetting:initSuggestionPageInfo()
    self:GetFlashObject():InvokeASCallback("_root", "HideAllItem", text, GameSetting.pagecount)
  end
  for i = 1, #GameSetting.currentListboxData do
    GameSetting:UpdateSuggestionItemData(i)
  end
end
function GameSetting:initPageList()
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "initListPageBoxTab", GameSetting.currentSugPageAll)
    self:GetFlashObject():InvokeASCallback("_root", "ShowPageListBox")
  end
end
function GameSetting:updatePageItemDate(arg)
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "updatePageItemDate", arg)
  end
end
function GameSetting:showSuggestionInfo(arg)
  GameSetting.currentShowInfoId = arg
  local itemData = GameSetting.currentListboxData[arg]
  local text = GameSetting:GetSuggestionInfo(itemData)
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "ShowSuggestionInfo", text)
  end
end
GameSetting.messageTranslate = nil
function GameSetting:SuggestionTranslate()
  DebugOut("GameSetting:SuggestionTranslate:")
  local itemData = GameSetting.currentListboxData[GameSetting.currentShowInfoId]
  if nil ~= GameSetting.messageTranslate then
    GameSetting.messageTranslate = nil
    if GameSetting:GetFlashObject() then
      GameSetting:GetFlashObject():InvokeASCallback("_root", "SetTranslateTitleAndContent", itemData.title, itemData.desc)
    end
  else
    GameSetting.messageTranslate = {}
    GameUtils:Translate(itemData.title, GameSetting.TranslateTitleCallback)
    GameUtils:Translate(itemData.desc, GameSetting.TranslateContentCallback)
  end
end
function GameSetting.TranslateTitleCallback(success, result)
  DebugOut("GameSetting.TranslateTitleCallback")
  if success then
    DebugOut("Translate result : ")
    DebugTable(result)
    GameSetting.messageTranslate.translateTitle = result.translatedText
    if GameSetting.messageTranslate.translateContent then
      DebugOut("title content:" .. GameSetting.messageTranslate.translateTitle .. " " .. GameSetting.messageTranslate.translateContent)
      GameSetting:GetFlashObject():InvokeASCallback("_root", "SetTranslateTitleAndContent", GameSetting.messageTranslate.translateTitle, GameSetting.messageTranslate.translateContent)
    end
  else
    DebugOut("Translate failed, errText : ")
  end
end
function GameSetting.TranslateContentCallback(success, result)
  DebugOut("GameSetting.TranslateContentCallback")
  if success then
    DebugOut("Translate result : ")
    DebugTable(result)
    GameSetting.messageTranslate.translateContent = result.translatedText
    if GameSetting.messageTranslate.translateTitle then
      DebugOut("title content:" .. GameSetting.messageTranslate.translateTitle .. " " .. GameSetting.messageTranslate.translateContent)
      if GameSetting:GetFlashObject() then
        GameSetting:GetFlashObject():InvokeASCallback("_root", "SetTranslateTitleAndContent", GameSetting.messageTranslate.translateTitle, GameSetting.messageTranslate.translateContent)
      end
    end
  else
    DebugOut("Translate failed, errText : ")
  end
end
function GameSetting:ShouEditMySuggestion(arg)
  local itemData = GameSetting.currentListboxData[arg]
  GameSetting.editData = itemData
  GameSetting.editIndex = itemData.id
  local text = GameSetting:GetSuggestionInfo(itemData)
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "ShowEditMySuggestion", text)
  end
end
function GameSetting:GetHotSuggestionData(pageid, pagecount)
  local infoURL = "feedback/get_hot?page=" .. pageid .. "&per_page=" .. pagecount .. "&user_id=" .. GameGlobalData:GetUserInfo().player_id
  if GameSetting:IsChinese() then
    GameUtils:SetHTTPServer("https://d.portal-platform.t4f.cn:10616/")
  else
    GameUtils:SetHTTPServer("https://origin-oregon-01.portal-platform.tap4fun.com:10616/")
  end
  GameUtils:HTTP_SendRequest(infoURL, nil, GameSetting.GetHotSuggestionDataCallback, true, GameSetting.httpRequestFailedCallback, "GET", "notencrypt")
end
function GameSetting.GetHotSuggestionDataCallback(content)
  DebugTable(content)
  GameSetting.suggestion_hot[tonumber(content.page)] = content.data
  if content.count == 0 then
    GameSetting.currentSugPageAll = 1
  else
    GameSetting.currentSugPageAll = math.ceil(content.count / GameSetting.pagecount)
  end
  GameSetting.currentListboxData = content.data
  GameSetting.suggestion_hot.count = GameSetting.currentSugPageAll
  GameSetting:UpdateItemDate()
end
function GameSetting:SupportTheSuggestion(index)
  GameSetting.supportIndex = index
  local item = GameSetting.currentListboxData[index]
  if item then
    DebugTable(item)
    local infoURL = "feedback/support?id=" .. item.id .. "&user_id=" .. GameGlobalData:GetUserInfo().player_id
    if GameSetting:IsChinese() then
      GameUtils:SetHTTPServer("https://d.portal-platform.t4f.cn:10616/")
    else
      GameUtils:SetHTTPServer("https://origin-oregon-01.portal-platform.tap4fun.com:10616/")
    end
    GameUtils:HTTP_SendRequest(infoURL, nil, GameSetting.SupportTheSuggestionCallback, true, GameSetting.httpRequestFailedCallback, "GET", "notencrypt")
  end
end
function GameSetting.SupportTheSuggestionCallback(content)
  if content.result and GameSetting:GetFlashObject() then
    local item = GameSetting.currentListboxData[GameSetting.supportIndex]
    item.support_count = item.support_count + 1
    item.supported = true
    GameSetting:GetFlashObject():InvokeASCallback("_root", "SupportSuggestion", GameSetting.supportIndex, item.support_count)
  else
  end
end
function GameSetting:CancelSupportTheSuggestion(index)
  GameSetting.cancel_support_index = index
  local item = GameSetting.currentListboxData[index]
  if item then
    DebugTable(item)
    local infoURL = "feedback/cancel_support?id=" .. item.id .. "&user_id=" .. GameGlobalData:GetUserInfo().player_id
    if GameSetting:IsChinese() then
      GameUtils:SetHTTPServer("https://d.portal-platform.t4f.cn:10616/")
    else
      GameUtils:SetHTTPServer("https://origin-oregon-01.portal-platform.tap4fun.com:10616/")
    end
    GameUtils:HTTP_SendRequest(infoURL, nil, GameSetting.CancelSupportTheSuggestionCallback, true, GameSetting.httpRequestFailedCallback, "GET", "notencrypt")
  end
end
function GameSetting.CancelSupportTheSuggestionCallback(content)
  if content.result and GameSetting:GetFlashObject() then
    local item = GameSetting.currentListboxData[GameSetting.cancel_support_index]
    item.support_count = item.support_count - 1
    item.supported = false
    GameSetting:GetFlashObject():InvokeASCallback("_root", "CancelSuggestion", GameSetting.cancel_support_index, item.support_count)
  else
  end
end
function GameSetting:GetMyselfSuggestion(pageid, pagecount)
  local infoURL = "feedback/get_myself?slogic_id=" .. GameUtils:GetActiveServerInfo().logic_id .. "&user_id=" .. GameGlobalData:GetUserInfo().player_id .. "&page=" .. pageid .. "&per_page=" .. pagecount
  if GameSetting:IsChinese() then
    GameUtils:SetHTTPServer("https://d.portal-platform.t4f.cn:10616/")
  else
    GameUtils:SetHTTPServer("https://origin-oregon-01.portal-platform.tap4fun.com:10616/")
  end
  GameUtils:HTTP_SendRequest(infoURL, nil, GameSetting.GetMyselfSuggestionCallback, true, GameSetting.httpRequestFailedCallback, "GET", "notencrypt")
end
function GameSetting.GetMyselfSuggestionCallback(content)
  DebugTable(content)
  GameSetting.suggestion_mine[tonumber(content.page)] = content.data
  if content.count == 0 then
    GameSetting.currentSugPageAll = 1
  else
    GameSetting.currentSugPageAll = math.ceil(content.count / GameSetting.pagecount)
  end
  GameSetting.currentListboxData = content.data
  GameSetting.suggestion_mine.count = GameSetting.currentSugPageAll
  GameSetting:UpdateItemDate()
end
function GameSetting:GetAcceptedSuggestion(pageid, pagecount)
  local infoURL = "feedback/get_accepted?page=" .. pageid .. "&per_page=" .. pagecount .. "&user_id=" .. GameGlobalData:GetUserInfo().player_id
  if GameSetting:IsChinese() then
    GameUtils:SetHTTPServer("https://d.portal-platform.t4f.cn:10616/")
  else
    GameUtils:SetHTTPServer("https://origin-oregon-01.portal-platform.tap4fun.com:10616/")
  end
  GameUtils:HTTP_SendRequest(infoURL, nil, GameSetting.GetAcceptedSuggestionCallback, true, GameSetting.httpRequestFailedCallback, "GET", "notencrypt")
end
function GameSetting.GetAcceptedSuggestionCallback(content)
  DebugTable(content)
  GameSetting.suggestion_project[tonumber(content.page)] = content.data
  if content.count == 0 then
    DebugOut("currentSugPageAll:---0")
    GameSetting.currentSugPageAll = 1
  else
    GameSetting.currentSugPageAll = math.ceil(content.count / GameSetting.pagecount)
  end
  DebugOut("currentSugPageAll:")
  DebugOut(GameSetting.currentSugPageAll)
  GameSetting.currentListboxData = content.data
  GameSetting.suggestion_project.count = GameSetting.currentSugPageAll
  GameSetting:UpdateItemDate()
end
function GameSetting:FeedbackMySuggestion(arg)
  DebugOut(arg)
  local param = LuaUtils:string_split(arg, "\001")
  local param_text1 = LuaUtils:string_trim(param[1])
  local param_text2 = LuaUtils:string_trim(param[2])
  DebugOut("param_text1:" .. param_text1 .. "," .. "param_text2:" .. param_text2)
  if param_text1 == "\002" or param_text2 == "\002" then
    GameTip:Show(GameLoader:GetGameText("LC_MENU_SUGGESTION_NULL_ALERT"))
    return
  end
  local param_text1_send = LuaUtils:string_split(param[1], "\002")
  local param_text2_send = LuaUtils:string_split(param[2], "\002")
  GameSetting.lastTitle = param_text1_send[1]
  GameSetting.lastContent = param_text2_send[1]
  if GameSetting.saveSuccessFlag then
    if GameSetting.saveItem then
      local data = {
        id = GameSetting.saveItem.id,
        title = param_text1_send[1],
        desc = param_text1_send[1]
      }
      GameUtils:SetHTTPPostData(ext.json.generate(data), nil)
      if GameSetting:IsChinese() then
        GameUtils:SetHTTPServer("https://d.portal-platform.t4f.cn:10616/")
      else
        GameUtils:SetHTTPServer("https://origin-oregon-01.portal-platform.tap4fun.com:10616/")
      end
      GameUtils:HTTP_SendRequest("feedback/edit", nil, GameSetting.EditMySuggestionCallbcak, true, GameSetting.httpRequestFailedCallback, "POST")
    end
  else
    local data = {
      user_id = GameGlobalData:GetUserInfo().player_id,
      user_name = GameGlobalData:GetUserInfo().name,
      slogic_id = GameUtils:GetActiveServerInfo().logic_id,
      slogic_name = GameUtils:GetActiveServerInfo().name,
      title = param_text1_send[1],
      desc = param_text2_send[1],
      locale = GameSettingData.Save_Lang
    }
    GameUtils:SetHTTPPostData(ext.json.generate(data), nil)
    if GameSetting:IsChinese() then
      GameUtils:SetHTTPServer("https://d.portal-platform.t4f.cn:10616/")
    else
      GameUtils:SetHTTPServer("https://origin-oregon-01.portal-platform.tap4fun.com:10616/")
    end
    GameUtils:HTTP_SendRequest("feedback", nil, GameSetting.FeedbackMySuggestionCallback, true, GameSetting.httpRequestFailedCallback, "POST")
  end
end
function GameSetting.FeedbackMySuggestionCallback(content)
  if content.result then
    GameSetting.saveSuccessFlag = true
    GameTip:Show(GameLoader:GetGameText("LC_MENU_SUGGESTION_SAVE_SUCCESS"))
    GameSetting.saveItem = content.msg
    if GameSetting.currentSugTab == 3 then
      GameSetting:GetMyselfSuggestion(GameSetting.currentPage, GameSetting.pagecount)
    end
  else
    GameTip:Show(GameLoader:GetGameText("LC_MENU_SUGGESTION_SAVE_FAIL"))
  end
  if GameSetting.backsuggestionflag then
    GameSetting.backsuggestionflag = false
    GameSetting.saveSuccessFlag = false
    if GameSetting:GetFlashObject() then
      GameSetting:GetFlashObject():InvokeASCallback("_root", "HideMySuggestionUI")
      GameSetting.IsShowSubmitSuggestionWindow = false
    end
  end
end
function GameSetting:BackSuggestion(arg)
  DebugOut(arg)
  GameSetting.backsuggestionflag = true
  local param = LuaUtils:string_split(arg, "\001")
  local param_text1 = LuaUtils:string_trim(param[1])
  local param_text2 = LuaUtils:string_trim(param[2])
  DebugOut("param_text1:" .. param_text1 .. "," .. "param_text2:" .. param_text2)
  local param_text1_send = LuaUtils:string_split(param[1], "\002")
  local param_text2_send = LuaUtils:string_split(param[2], "\002")
  if param_text1 == "\002" or param_text2 == "\002" or param_text1_send[1] == GameSetting.editData.title and param_text2_send[1] == GameSetting.editData.desc or GameSetting.lastTitle == param_text1_send[1] and GameSetting.lastContent == param_text2_send[1] then
    GameSetting.backsuggestionflag = false
    GameSetting.saveSuccessFlag = false
    if self:GetFlashObject() then
      self:GetFlashObject():InvokeASCallback("_root", "HideMySuggestionUI")
      GameSetting.IsShowSubmitSuggestionWindow = false
    end
    GameSetting.editData = {}
    GameSetting.editData.title = ""
    GameSetting.editData.desc = ""
  else
    GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
    GameUIMessageDialog:SetYesButton(function()
      if GameSetting.clickMySuggestion then
        GameSetting:FeedbackMySuggestion(arg)
      else
        GameSetting:EditMySuggestion(arg)
      end
      GameSetting.editData = {}
      GameSetting.editData.title = ""
      GameSetting.editData.desc = ""
    end)
    GameUIMessageDialog:SetNoButton(function()
      GameSetting.backsuggestionflag = false
      GameSetting.saveSuccessFlag = false
      if self:GetFlashObject() then
        self:GetFlashObject():InvokeASCallback("_root", "HideMySuggestionUI")
        GameSetting.IsShowSubmitSuggestionWindow = false
      end
      GameSetting.editData = {}
      GameSetting.editData.title = ""
      GameSetting.editData.desc = ""
    end)
    local textTitle = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
    local textInfo = GameLoader:GetGameText("LC_MENU_SUGGESTION_SAVE_ALERT")
    GameUIMessageDialog:Display(textTitle, textInfo)
  end
end
function GameSetting:DeleteMySuggestion(index)
  GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
  GameUIMessageDialog:SetYesButton(function()
    local itemData = GameSetting.currentListboxData[index]
    local infoURL = "feedback/delete?id=" .. itemData.id
    if GameSetting:IsChinese() then
      GameUtils:SetHTTPServer("https://d.portal-platform.t4f.cn:10616/")
    else
      GameUtils:SetHTTPServer("https://origin-oregon-01.portal-platform.tap4fun.com:10616/")
    end
    GameUtils:HTTP_SendRequest(infoURL, nil, GameSetting.DeleteMySuggestionCallback, true, GameSetting.httpRequestFailedCallback, "GET", "notencrypt")
  end)
  GameUIMessageDialog:SetNoButton(function()
  end)
  local textTitle = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
  local textInfo = GameLoader:GetGameText("LC_MENU_SUGGESTION_DELETE_INFO")
  GameUIMessageDialog:Display(textTitle, textInfo)
end
function GameSetting.DeleteMySuggestionCallback(content)
  if content.result then
    GameSetting:GetMyselfSuggestion(GameSetting.currentPage, GameSetting.pagecount)
  else
  end
end
function GameSetting:EditMySuggestion(arg)
  local param = LuaUtils:string_split(arg, "\001")
  local param_text1 = LuaUtils:string_trim(param[1])
  local param_text2 = LuaUtils:string_trim(param[2])
  DebugOut("param_text1:" .. param_text1 .. "," .. "param_text2:" .. param_text2)
  if param_text1 == "\002" or param_text2 == "\002" then
    GameTip:Show(GameLoader:GetGameText("LC_MENU_SUGGESTION_NULL_ALERT"))
    return
  end
  local param_text1_send = LuaUtils:string_split(param[1], "\002")
  local param_text2_send = LuaUtils:string_split(param[2], "\002")
  GameSetting.lastTitle = param_text1_send[1]
  GameSetting.lastContent = param_text2_send[1]
  local itemData = GameSetting.currentListboxData[index]
  local data = {
    id = GameSetting.editIndex,
    title = param_text1_send[1],
    desc = param_text2_send[1]
  }
  GameUtils:SetHTTPPostData(ext.json.generate(data), nil)
  if GameSetting:IsChinese() then
    GameUtils:SetHTTPServer("https://d.portal-platform.t4f.cn:10616/")
  else
    GameUtils:SetHTTPServer("https://origin-oregon-01.portal-platform.tap4fun.com:10616/")
  end
  GameUtils:HTTP_SendRequest("feedback/edit", nil, GameSetting.EditMySuggestionCallbcak, true, GameSetting.httpRequestFailedCallback, "POST")
end
function GameSetting.EditMySuggestionCallbcak(content)
  if content.result then
    GameTip:Show(GameLoader:GetGameText("LC_MENU_SUGGESTION_SAVE_SUCCESS"))
    if GameSetting.currentSugTab == 3 then
      GameSetting:GetMyselfSuggestion(GameSetting.currentPage, GameSetting.pagecount)
    end
  else
    GameTip:Show(GameLoader:GetGameText("LC_MENU_SUGGESTION_SAVE_FAIL"))
  end
  if GameSetting.backsuggestionflag then
    GameSetting.backsuggestionflag = false
    if GameSetting:GetFlashObject() then
      GameSetting:GetFlashObject():InvokeASCallback("_root", "HideMySuggestionUI")
      GameSetting.IsShowSubmitSuggestionWindow = false
    end
  end
end
function GameSetting.RestartGameOrReconnectToServer()
  if not GameGlobalData.isLogin then
    GameUtils:RestartGame(0)
    return
  end
  if AutoUpdate.isQuickStartGame then
    GameUtils:RestartGame(0)
  else
    if ext.socket.setHeaderSize then
      ext.socket.setHeaderSize(2)
    end
    NetMessageMgr.HeaderSize = 2
    ext.socket.reconnectToServer()
  end
end
function GameSetting._createCommonDetailUrl(gameParamValue)
  local vipInfo = GameGlobalData:GetData("vipinfo")
  local resource = GameGlobalData:GetData("resource")
  local vip_level = -1
  if resource then
    vip_level = EngineDefault.GetVIPLevel(resource.total_amount)
  end
  ext.dofile("data1/EngineDefault.tfl")
  local FAQUrl = "http://support.tap4fun.com/check?"
  local utc_time = os.date("%Y-%m-%d %H:%M:%S", os.time())
  local game = gameParamValue
  local version = ext.GetAppVersion()
  local openudid = ext.GetIOSOpenUdid()
  local username = "1"
  local userInfo = GameGlobalData:GetData("userinfo")
  if userInfo then
    username = userInfo.name
  end
  local server = ""
  if GameUtils:GetLoginInfo() ~= nil and GameUtils:GetLoginInfo().ServerInfo ~= nil then
    server = GameUtils:GetLoginInfo().ServerInfo.name or ""
  end
  local language = ext.GetDeviceLanguage()
  if language == "zh_Hans" or string.find(language, "zh-Hans") or string.find(language, "zh_Hans") then
    language = "zh-Hans"
  elseif language == "zh_Hant" or string.find(language, "zh-Hant") or string.find(language, "zh_Hant") then
    language = "zh-Hant"
  end
  local device_type = ext.GetDeviceName()
  local os_version = ext.GetOSVersion()
  local HashCode = 0
  if vipinfo then
    HashCode = vipInfo.level
  end
  local IP = GameSetting.localIP or ""
  local lastHttpRequestUrl = GameUtils.lastRequestRecord.url
  local lastHttpRequestStatusCode = GameUtils.lastRequestRecord.statusCode
  FAQUrl = FAQUrl .. "utc_time=" .. ext.ENC3(utc_time) .. "&game=" .. ext.ENC3(game) .. "&version=" .. ext.ENC3(version) .. "&openudid=" .. ext.ENC3(openudid) .. "&username=" .. ext.ENC3(username) .. "&server=" .. ext.ENC3(server) .. "&language=" .. ext.ENC3(language) .. "&device_type=" .. ext.ENC3(device_type) .. "&os_version=" .. ext.ENC3(os_version) .. "&vip_level=" .. ext.ENC3(vip_level) .. "&HashCode=" .. ext.ENC3(HashCode) .. "&IP=" .. ext.ENC3(IP) .. "&CS=" .. ext.ENC3(vip_level)
  return FAQUrl
end
function GameSetting.openTpa4funContactUsPage()
  local detailUrl = GameSetting._createCommonDetailUrl("gl")
  ext.http.openURL(detailUrl)
end
function GameSetting.openGLLoginAndNetErrorPage()
  local theAppVersion
  if not AutoUpdate.localAppVersion then
    local _, _, AppVer1, AppVer2, AppVer3 = string.find(ext.GetAppVersion(), "(%d+).(%d+).(%d+)")
    theAppVersion = tonumber(AppVer3) + 100 * tonumber(AppVer2) + 10000 * tonumber(AppVer1)
  else
    theAppVersion = AutoUpdate.localAppVersion
  end
  local IP = GameSetting.localIP or ""
  local server = "1"
  if GameSettingData and GameSettingData.LastLogin and GameSettingData.LastLogin.ServerInfo and GameSettingData.LastLogin.ServerInfo.name then
    server = GameSettingData.LastLogin.ServerInfo.name
  end
  if ext.GetPlatform() == "iOS" and GameUtils:GetLeftNumFromOSVersion(ext.GetOSVersion()) and GameUtils:GetLeftNumFromOSVersion(ext.GetOSVersion()) < 7 then
    local text_title = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
    GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.OK)
    GameUIMessageDialog:Display(text_title, GameLoader:GetGameText("LC_MENU_IOS_VERSION_LOWER"))
  elseif theAppVersion < 10706 then
    local isSupporthelpshift = false
    local updateText
    if ext.GetBundleIdentifier() == "com.tap4fun.galaxyempire2" then
      isSupporthelpshift = true
    elseif ext.GetBundleIdentifier() == "com.tap4fun.galaxyempire2.chinese" then
      isSupporthelpshift = true
    elseif ext.GetBundleIdentifier() == "com.tap4fun.galaxyempire2_android" then
      isSupporthelpshift = true
    elseif ext.GetBundleIdentifier() == "com.ts.galaxyempire2_android_global" then
      isSupporthelpshift = true
    elseif ext.GetBundleIdentifier() == "com.tap4fun.galaxyempire2_android_amazon" then
      isSupporthelpshift = true
    elseif ext.GetBundleIdentifier() == "com.tap4fun.galaxyempire2_android_cafe" then
      isSupporthelpshift = true
    elseif ext.GetBundleIdentifier() == "com.tap4fun.galaxyempire2_android_local" then
      isSupporthelpshift = true
    elseif ext.GetBundleIdentifier() == "com.tap4fun.galaxyempire2_android.platform.qihoo360" then
      isSupporthelpshift = true
    elseif ext.GetBundleIdentifier() == "com.tap4fun.galaxyempire2_android.platform.uc" then
      isSupporthelpshift = true
    else
      isSupporthelpshift = false
    end
    if isSupporthelpshift then
      do
        local localAppid = ext.GetBundleIdentifier()
        if localAppid then
          local function callback()
            ext.http.openURL(GameUtils.AppDownLoadURL[localAppid])
          end
          local text_title = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
          GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
          GameUIMessageDialog:SetYesButton(callback)
          GameUIMessageDialog:Display(text_title, GameLoader:GetGameText("LC_MENU_CS_UPDATE_TO_NEW_VERSION"))
        end
      end
    else
      local text_title = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
      GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.OK)
      GameUIMessageDialog:Display(text_title, GameLoader:GetGameText("LC_MENU_CS_CAN_NOT_CONNECT"))
    end
  else
    ext.http.openFAQ("-1", 0, 0, "", ext.GetAppVersion(), "Login error", ext.GetIOSOpenUdid(), server, IP)
  end
end
function GameSetting:showContactUsDialogForNetError(title, errorInfo, okCallback)
  self:showContactUsDialog(title, errorInfo, GameSetting.openGLLoginAndNetErrorPage, okCallback)
end
function GameSetting:showContactUsDialog(title, errorInfo, contactCallback, okCallback)
  title = string.match(title, "^$") and (GameLoader:GetGameText("LC_MENU_ERROR_TITLE") or "error")
  local info = errorInfo or ""
  local textOK = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY") or "confirm"
  local textContactUs = GameLoader:GetGameText("LC_MENU_LOCAL_TEXT_CONTACTUS") or "contact_us"
  GameUIMessageDialog:SetLeftTextButton(textOK, function()
    if okCallback then
      okCallback()
    end
  end)
  GameUIMessageDialog:SetRightTextButton(textContactUs, function()
    local theAppVersion
    if not AutoUpdate.localAppVersion then
      local _, _, AppVer1, AppVer2, AppVer3 = string.find(ext.GetAppVersion(), "(%d+).(%d+).(%d+)")
      theAppVersion = tonumber(AppVer3) + 100 * tonumber(AppVer2) + 10000 * tonumber(AppVer1)
    else
      theAppVersion = AutoUpdate.localAppVersion
    end
    local IP = GameSetting.localIP or ""
    local server = "1"
    if GameSettingData and GameSettingData.LastLogin and GameSettingData.LastLogin.ServerInfo and GameSettingData.LastLogin.ServerInfo.name then
      server = GameSettingData.LastLogin.ServerInfo.name
    end
    if ext.GetPlatform() == "iOS" and GameUtils:GetLeftNumFromOSVersion(ext.GetOSVersion()) and GameUtils:GetLeftNumFromOSVersion(ext.GetOSVersion()) < 7 then
      local text_title = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
      GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.OK)
      GameUIMessageDialog:Display(text_title, GameLoader:GetGameText("LC_MENU_IOS_VERSION_LOWER"))
    elseif theAppVersion < 10706 then
      local isSupporthelpshift = false
      if ext.GetBundleIdentifier() == "com.tap4fun.galaxyempire2" then
        isSupporthelpshift = true
      elseif ext.GetBundleIdentifier() == "com.ts.spacelancer" then
        isSupporthelpshift = true
      elseif ext.GetBundleIdentifier() == "com.tap4fun.galaxyempire2.chinese" then
        isSupporthelpshift = true
      elseif ext.GetBundleIdentifier() == "com.tap4fun.galaxyempire2_android" then
        isSupporthelpshift = true
      elseif ext.GetBundleIdentifier() == "com.ts.galaxyempire2_android_global" then
        isSupporthelpshift = true
      elseif ext.GetBundleIdentifier() == "com.tap4fun.galaxyempire2_android_amazon" then
        isSupporthelpshift = true
      elseif ext.GetBundleIdentifier() == "com.tap4fun.galaxyempire2_android_cafe" then
        isSupporthelpshift = true
      elseif ext.GetBundleIdentifier() == "com.tap4fun.galaxyempire2_android_local" then
        isSupporthelpshift = true
      elseif ext.GetBundleIdentifier() == "com.tap4fun.galaxyempire2_android.platform.qihoo360" then
        isSupporthelpshift = true
      elseif ext.GetBundleIdentifier() == "com.tap4fun.galaxyempire2_android.platform.uc" then
        isSupporthelpshift = true
      else
        isSupporthelpshift = false
      end
      if isSupporthelpshift then
        do
          local localAppid = ext.GetBundleIdentifier()
          if localAppid then
            local function callback()
              ext.http.openURL(GameUtils.AppDownLoadURL[localAppid])
            end
            local text_title = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
            GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
            GameUIMessageDialog:SetYesButton(callback)
            GameUIMessageDialog:Display(text_title, GameLoader:GetGameText("LC_MENU_CS_UPDATE_TO_NEW_VERSION"))
          end
        end
      else
        local text_title = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
        GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.OK)
        GameUIMessageDialog:Display(text_title, GameLoader:GetGameText("LC_MENU_CS_CAN_NOT_CONNECT"))
      end
    else
      ext.http.openFAQ("-1", 0, 0, "", ext.GetAppVersion(), "Login error", ext.GetIOSOpenUdid(), server, IP)
    end
  end)
  GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.Caption)
  GameUIMessageDialog:Display(title, info)
end
function GameSetting:showNoticeDialog(title, noticeInfo, okCallback, noticeType)
  local info = noticeInfo
  GameUIMessageDialog:SetNoticeButton(okCallback, noticeType)
  GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.Notice)
  if noticeType == "announce_close_server_html" then
    local pos = GameUIMessageDialog:GetElementPosAndSize("_root.help_pop.info_box.textPanel.maskbox")
    GameUIMessageDialog:CreateWebView(info, pos[1], pos[2], pos[3], pos[4])
    GameUIMessageDialog:Display(title, "")
  else
    GameUIMessageDialog:Display(title, info)
  end
end
function GameSetting:ClearData()
  self.FriendListData = nil
  self.SearchFriendResultData = nil
  self.NoticeListData = nil
  self.BlockListData = nil
  GameSetting._current_list_mode = GameSetting.DataType.FriendList
end
function GameSetting:InitFriendList(friend_list_data)
  self.FriendListData = friend_list_data
  GameSetting:SetNormalFriend()
end
function GameSetting:SetNormalFriend()
  if self:GetFlashObject() then
    GameSetting:GetFlashObject():InvokeASCallback("_root", "SetSelectedPanel", GameSetting.DataType.FriendList, true)
    self:GetFlashObject():InvokeASCallback("_root", "InitDataList", #self.FriendListData, GameSetting.DataType.FriendList, GameLoader:GetGameText("LC_MENU_NO_FRIEND"))
  end
end
function GameSetting:SetTangoFriend()
  if not GameUtils:IsLoginTangoAccount() then
    if AutoUpdate.isAndroidDevice and GameUtils:IsTangoAPP() and GameUtils.IsNotInstallTango then
      ext.http.openURL(GameSetting.tangoGooglePlayUrl)
    end
    return
  end
  if not GameSetting.tangoFriendList then
    GameSetting.tangoFriendList = {}
    GameSetting:GetTangoFriendList()
  else
    GameSetting:ShowTangoFriendList()
  end
end
function GameSetting.friendListCallback(msgType, content)
  if not GameSetting:GetFlashObject() then
    GameSetting:LoadFlashObject()
  end
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.friends_req.Code then
    assert(content.code ~= 0)
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.friends_ack.Code then
    if GameSetting.blockClicked then
      GameSetting.BlockListData = content.ban_friends
      GameSetting:GetFlashObject():InvokeASCallback("_root", "SetSelectedPanel", GameSetting.DataType.BlockList, true)
      GameSetting:GetFlashObject():InvokeASCallback("_root", "InitDataList", #GameSetting.BlockListData, GameSetting.DataType.BlockList, GameLoader:GetGameText("LC_MENU_NO_BLOCK_FRIEND"))
      GameSetting.blockClicked = false
    else
      GameSetting:InitFriendList(content.friends)
      GameSetting.BlockListData = content.ban_friends
    end
    GameSetting:GetFlashObject():InvokeASCallback("_root", "ShowFriendMenu")
    return true
  end
  return false
end
function GameSetting:ShowAddFriendUIAndFetchFriends()
  NetMessageMgr:SendMsg(NetAPIList.friends_req.Code, null, self.fetchFriendListCallback, true, nil)
end
function GameSetting.fetchFriendListCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.friends_req.Code then
    assert(content.code ~= 0)
    GameSetting:ShowAddFriendUI()
    return true
  elseif msgType == NetAPIList.friends_ack.Code then
    GameSetting:InitFriendList(content.friends)
    GameSetting:ShowAddFriendUI()
    return true
  end
  return false
end
function GameSetting:ShowAddFriendUI()
  if not GameSetting:GetFlashObject() then
    GameSetting:LoadFlashObject()
  end
  GameSetting._current_list_mode = GameSetting.DataType.SearchFriendResult
end
function GameSetting:GetTangoFriendList()
  GameWaiting:ShowLoadingScreen()
  ext.socialNetwork.getContactsProfile(function(errStr, friendProfiles)
    GameWaiting:HideLoadingScreen()
    if errStr then
      GameUtils:ShowTips(errStr)
    end
    if friendProfiles then
      GameUtils:printByAndroid("print---------------friendProfiles-------")
      GameUtils:DebugOutTableInAndroid(friendProfiles)
      GameSetting.tangoFriendList = friendProfiles
      GameSetting:GetCurrentPlatformTangoID()
      if not GameSetting.InviteTangoIDList then
        GameSetting:ReqInvitedTangoFriendList()
      end
    end
  end)
end
function GameSetting:ShowTangoFriendList()
  if not GameSetting.tangoFriendList then
    return
  end
  GameSetting:GetFlashObject():InvokeASCallback("_root", "SetSelectedPanel", GameSetting.DataType.tangoFriend, true)
  self:GetFlashObject():InvokeASCallback("_root", "InitDataList", #GameSetting.tangoFriendList, GameSetting.DataType.tangoFriend, GameLoader:GetGameText("LC_MENU_NO_FRIEND"))
end
function GameSetting:ReqInvitedTangoFriendList()
  local friends_tango = {}
  for k, v in pairs(self.tangoFriendList) do
    table.insert(friends_tango, v.tangoID)
  end
  local content = {
    tangoid = GameUtils.m_myProfile.tangoID,
    friends = friends_tango
  }
  NetMessageMgr:SendMsg(NetAPIList.tango_invited_friends_req.Code, content, GameSetting.ReqInvitedTangoFriendListCallback, true, nil)
end
function GameSetting.ReqInvitedTangoFriendListCallback(msgType, content)
  GameUtils:printByAndroid("fuck----ReqInvitedTangoFriendListCallback------msgType," .. msgType)
  if content then
    GameUtils:DebugOutTableInAndroid(content)
  end
  if msgType == NetAPIList.tango_invited_friends_ack.Code then
    GameSetting.InviteTangoIDList = content.invited_friends
    GameSetting.RegisteredTangoFriends = content.registered_friends
    if not GameSetting.InviteTangoIDList then
      GameSetting.InviteTangoIDList = {}
    end
    if not GameSetting.RegisteredTangoFriends then
      GameSetting.RegisteredTangoFriends = {}
    end
    for i = #GameSetting.tangoFriendList, 1, -1 do
      if #GameSetting.InviteTangoIDList > 0 then
        for k1, v1 in pairs(GameSetting.InviteTangoIDList) do
          if GameSetting.tangoFriendList[i] and GameSetting.tangoFriendList[i].tangoID and v1 and tostring(GameSetting.tangoFriendList[i].tangoID) == tostring(v1) then
            GameSetting.tangoFriendList[i].Invited = true
          end
        end
      end
      if #GameSetting.RegisteredTangoFriends > 0 then
        for k2, v2 in pairs(GameSetting.RegisteredTangoFriends) do
          if GameSetting.tangoFriendList[i] and GameSetting.tangoFriendList[i].tangoID and v2 and tostring(GameSetting.tangoFriendList[i].tangoID) == tostring(v2) then
            table.remove(GameSetting.tangoFriendList, i)
          end
        end
      end
    end
    GameSetting:ShowTangoFriendList()
    return true
  elseif msgType == NetAPIList.common_ack.Code and (content.api == NetAPIList.tango_invited_friends_req.Code or content.api == NetAPIList.tango_invite_req.Code) then
    GameTip:Show(AlertDataList:GetTextFromErrorCode(content.code), 3000)
    if content.api == NetAPIList.tango_invite_req.Code then
      if GameSetting.tangoFriendList and #GameSetting.tangoFriendList > 0 then
        for h = 1, #GameSetting.tangoFriendList do
          for i = 1, #GameSetting.sendTangoInviteList do
            if GameSetting.tangoFriendList[h].tangoID == GameSetting.sendTangoInviteList[i] then
              GameSetting.tangoFriendList[h].Invited = true
              GameSetting.tangoFriendList[h].boxClicked = false
            end
          end
          GameSetting:updateTangoFriendItem(h, true)
        end
      end
      GameSetting.sendTangoInviteList = {}
    end
    return true
  end
  return false
end
function GameSetting:GetCurrentPlatformTangoID()
  if not GameSetting.tangoFriendList then
    return
  end
  for i = 1, #GameSetting.tangoFriendList do
    GameSetting.tangoFriendList[i].boxClicked = false
    GameSetting.tangoFriendList[i].Invited = false
  end
end
function GameSetting:GetCanInviteTangoList()
end
function GameSetting.searchFriendCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.friend_search_req.Code then
    assert(content.code ~= 0)
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.friend_search_ack.Code then
    GameSetting.SearchFriendResultData = content.friends
    if #GameSetting.SearchFriendResultData < 1 then
      local tipText = GameLoader:GetGameText("LC_MENU_FRIEND_SEARCH_NO_RESULT")
      GameTip:Show(tipText)
    end
    GameSetting:GetFlashObject():InvokeASCallback("_root", "InitDataList", #GameSetting.SearchFriendResultData, GameSetting.DataType.SearchFriendResult, GameLoader:GetGameText("LC_MENU_NOT_FOUND_FRIEND"))
    GameSetting:GetFlashObject():InvokeASCallback("_root", "SetSelectedPanel", GameSetting.DataType.SearchFriendResult, true)
    return true
  end
  return false
end
function GameSetting.loadNoticeCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.messages_req.Code then
    assert(content.code ~= 0)
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.messages_ack.Code then
    GameSetting.NoticeListData = content.messages
    local noticeShownList = {}
    for i, v in ipairs(GameSetting.NoticeListData) do
      local isMyFriend = GameSetting:IsInFriendsList(v.user_id)
      local isMyBlockedFriend = GameSetting:IsInBlockedFriendsList(v.user_id)
      if not isMyFriend and not isMyBlockedFriend then
        table.insert(noticeShownList, v)
      end
    end
    GameGlobalData:UpdateGameData("new_friend_cnt", 0)
    GameGlobalData:RefreshData("new_friend_cnt")
    GameSetting.NoticeListData = noticeShownList
    GameSetting:GetFlashObject():InvokeASCallback("_root", "SetSelectedPanel", GameSetting.DataType.NoticeList, true)
    GameSetting:GetFlashObject():InvokeASCallback("_root", "InitDataList", #GameSetting.NoticeListData, GameSetting.DataType.NoticeList, GameLoader:GetGameText("LC_MENU_NO_FRIEND_MSG"))
    return true
  end
  return false
end
function GameSetting.deleteNoticeCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.message_del_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    else
      if GameSetting.NoticeListData then
        table.remove(GameSetting.NoticeListData, GameSetting.waitForDeleteMsgIndex)
      end
      GameSetting:GetFlashObject():InvokeASCallback("_root", "SetSelectedPanel", GameSetting.DataType.NoticeList, true)
      GameSetting:GetFlashObject():InvokeASCallback("_root", "InitDataList", #GameSetting.NoticeListData, GameSetting.DataType.NoticeList, GameLoader:GetGameText("LC_MENU_NO_FRIEND_MSG"))
    end
    return true
  end
  return false
end
function GameSetting:SearchFriend(searchFriendName)
  if searchFriendName == nil then
    return
  elseif string.len(searchFriendName) == 0 then
    return
  elseif string.len(searchFriendName) > 256 then
    searchFriendName = string.sub(searchFriendName, 1, 256)
  end
  local friend_search_req_data = {name = searchFriendName}
  NetMessageMgr:SendMsg(NetAPIList.friend_search_req.Code, friend_search_req_data, self.searchFriendCallback, false, nil)
end
function GameSetting:AddFriend(friendData)
  if friendData and self.FriendListData then
    table.insert(self.FriendListData, friendData)
  end
end
function GameSetting:DeleteFriendNotice()
  NetMessageMgr:SendMsg(NetAPIList.friends_req.Code, null, GameSetting.friendListCallback, true, nil)
end
function GameSetting.friendDetailCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.code == NetAPIList.common_ack.Code and content.api == NetAPIList.friend_ban_req.Code then
    if GameSetting.current_Arg then
      local ptPos = LuaUtils:string_split(GameSetting.current_Arg, "\001")
      local dataInfo = GameSetting:GetDataInfo(tonumber(ptPos[1]), tonumber(ptPos[2]))
      local content = string.format(GameLoader:GetGameText("LC_MENU_ADD_BLOCK_CHAR"), GameUtils:GetUserDisplayName(dataInfo.name))
      GameTip:Show(content)
      NetMessageMgr:SendMsg(NetAPIList.friends_req.Code, null, GameSetting.friendListCallback, true, nil)
    end
    return true
  elseif msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.friend_ban_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  return false
end
function GameSetting.friendBanDelCallback(msgType, content)
  if content.code == 0 then
    if GameSetting.clickBlockListIndex then
      GameSetting.blockClicked = true
      NetMessageMgr:SendMsg(NetAPIList.friends_req.Code, null, GameSetting.friendListCallback, true, nil)
      return true
    end
  else
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  end
  return false
end
function GameSetting.DeleteFriendCallback(msgType, content)
  if content.code == 0 and content.api == 46 then
    GameTip:Show(GameLoader:GetGameText("LC_MENU_DELETE_FRIEND_SUCCES_CHAR"))
    GameSetting:GetFlashObject():InvokeASCallback("_root", "operationMoveOut")
    GameSetting:DeleteFriendNotice()
    return true
  else
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  end
  return false
end
function GameSetting.addFriendCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.add_friend_req.Code then
    assert(content.code ~= 0)
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.add_friend_ack.Code then
    GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.OK)
    GameUIMessageDialog:Display(GameLoader:GetGameText("LC_MENU_TITLE_SUCCESS_INFO"), GameLoader:GetGameText("LC_MENU_ADD_FRIEND_SUCC"))
    GameSetting:AddFriend(content.friend)
    GameSetting.waitForDeleteMsgIndex = nil
    if GameSetting.NoticeListData then
      for i, v in ipairs(GameSetting.NoticeListData) do
        if v.user_id == content.friend.id then
          local msgContent = {}
          msgContent.id = v.id
          GameSetting.waitForDeleteMsgIndex = i
          DebugOut("msgContent = ")
          DebugTable(msgContent)
          NetMessageMgr:SendMsg(NetAPIList.message_del_req.Code, msgContent, GameSetting.deleteNoticeCallback, true, nil)
        end
      end
    end
    if GameSetting._current_list_mode == GameSetting.DataType.NoticeList then
      local function netFailedCallback()
        NetMessageMgr:SendMsg(NetAPIList.messages_req.Code, nil, GameSetting.loadNoticeCallback, true, netFailedCallback)
      end
      NetMessageMgr:SendMsg(NetAPIList.messages_req.Code, nil, GameSetting.loadNoticeCallback, true, netFailedCallback)
    end
    return true
  end
  return false
end
function GameSetting:GetDataInfo(item_index, DataType)
  local dataInfo
  if GameSetting.DataType.FriendList == tonumber(DataType) then
    dataInfo = self.FriendListData[item_index]
    return dataInfo
  elseif GameSetting.DataType.SearchFriendResult == tonumber(DataType) then
    dataInfo = self.SearchFriendResultData[item_index]
    return dataInfo
  elseif GameSetting.DataType.BlockList == tonumber(DataType) then
    dataInfo = self.BlockListData[item_index]
    return dataInfo
  end
  return dataInfo
end
function GameSetting:OperationView(arg)
  if self.current_Arg then
    local ptPos = LuaUtils:string_split(self.current_Arg, "\001")
    local dataInfo = self:GetDataInfo(tonumber(ptPos[1]), tonumber(ptPos[2]))
    local coordinate = LuaUtils:string_split(arg, "\001")
    GameUIPlayerDetailInfo:Show(dataInfo.id, tonumber(coordinate[1]), tonumber(coordinate[2]))
  end
end
function GameSetting:OperationWhisper()
  local ptPos = LuaUtils:string_split(self.current_Arg, "\001")
  local dataInfo = self:GetDataInfo(tonumber(ptPos[1]), tonumber(ptPos[2]))
  GameUIChat:LoadFlashObject()
  GameStateManager:GetCurrentGameState():AddObject(GameUIChat)
  GameUIChat:MoveOutOperation()
  GameUIChat.last_receiver = dataInfo.name
  GameUIChat:SelectChannel("whisper")
end
function GameSetting:OperationBlock()
  if GameSetting._current_list_mode == GameSetting.DataType.BlockList then
    return
  end
  local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
  GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
  local function AddtoBlacklistCallback()
    if self.current_Arg then
      local ptPos = LuaUtils:string_split(self.current_Arg, "\001")
      local dataInfo = self:GetDataInfo(tonumber(ptPos[1]), tonumber(ptPos[2]))
      local content = {
        id = dataInfo.id,
        name = dataInfo.name
      }
      NetMessageMgr:SendMsg(NetAPIList.friend_ban_req.Code, content, self.friendDetailCallback, true, nil)
    end
  end
  GameUIMessageDialog:SetYesButton(AddtoBlacklistCallback, {})
  local info_title = ""
  local info_content = GameLoader:GetGameText("LC_MENU_BLOCK_CONFIRM_ALERT")
  GameUIMessageDialog:Display(info_title, info_content)
end
function GameSetting:OperationDelete()
  if self._current_list_mode ~= GameSetting.DataType.FriendList then
    return
  end
  local ptPos = LuaUtils:string_split(self.current_Arg, "\001")
  local dataInfo = self:GetDataInfo(tonumber(ptPos[1]), tonumber(ptPos[2]))
  local del_friend_req_content = {
    id = dataInfo.id
  }
  NetMessageMgr:SendMsg(NetAPIList.del_friend_req.Code, del_friend_req_content, self.DeleteFriendCallback, false)
end
function GameSetting:UnlockBanFriend(arg)
  self.clickBlockListIndex = tonumber(arg)
  local friend_ban_del_req = {
    id = self.BlockListData[tonumber(arg)].id
  }
  NetMessageMgr:SendMsg(NetAPIList.friend_ban_del_req.Code, friend_ban_del_req, self.friendBanDelCallback, false)
end
function GameSetting:AddFriendToPlayer(arg)
  local ptPos = LuaUtils:string_split(arg, "\001")
  local data_index = tonumber(ptPos[1])
  local data_index = tonumber(ptPos[1])
  local dataInfo, add_friend_req_content
  if GameSetting.DataType.SearchFriendResult == tonumber(ptPos[2]) then
    dataInfo = self.SearchFriendResultData[data_index]
    add_friend_req_content = {
      id = dataInfo.id,
      name = dataInfo.name
    }
  else
    dataInfo = self.NoticeListData[tonumber(data_index)]
    add_friend_req_content = {
      id = dataInfo.user_id,
      name = dataInfo.user_name
    }
    self.addMessageId = dataInfo.id
    self.add_frend_id = dataInfo.user_id
  end
  NetMessageMgr:SendMsg(NetAPIList.add_friend_req.Code, add_friend_req_content, self.addFriendCallback, true, nil)
end
function GameSetting:UpdateItemDetailDate(arg)
  local ptPos = LuaUtils:string_split(arg, "\001")
  local data_index = tonumber(ptPos[1])
  local dataInfo
  GameUtils:printByAndroid(" UpdateItemDetailDate ---data_index-> " .. data_index .. ",tonumber(ptPos[2])=" .. tonumber(ptPos[2]))
  if GameSetting.DataType.FriendList == tonumber(ptPos[2]) then
    dataInfo = self.FriendListData[data_index]
    if dataInfo then
      local name = GameUtils:GetUserDisplayName(dataInfo.name)
      local offlineText = GameLoader:GetGameText("LC_MENU_ALLIANCE_OFFLINE_INFO")
      local onlineText = GameLoader:GetGameText("LC_MENU_ALLIANCE_ONLINE_INFO")
      local avatar = ""
      if dataInfo.icon == 0 or dataInfo.icon == 1 then
        avatar = GameUtils:GetPlayerAvatarWithSex(dataInfo.sex, dataInfo.main_fleet_level)
      else
        avatar = GameDataAccessHelper:GetFleetAvatar(dataInfo.icon, dataInfo.main_fleet_level)
      end
      DebugTable(dataInfo)
      DebugOut("FriendList:", avatar)
      self:GetFlashObject():InvokeASCallback("_root", "UpdateFriendItemData", data_index, avatar, name, dataInfo.level, dataInfo.online, tonumber(ptPos[2]), onlineText, offlineText)
    end
  elseif GameSetting.DataType.SearchFriendResult == tonumber(ptPos[2]) then
    dataInfo = self.SearchFriendResultData[data_index]
    local addText = GameLoader:GetGameText("LC_MENU_ADD")
    if dataInfo then
      local name = GameUtils:GetUserDisplayName(dataInfo.name)
      local offlineText = GameLoader:GetGameText("LC_MENU_ALLIANCE_OFFLINE_INFO")
      local onlineText = GameLoader:GetGameText("LC_MENU_ALLIANCE_ONLINE_INFO")
      local avatar = ""
      if dataInfo.icon == 0 or dataInfo.icon == 1 then
        avatar = GameUtils:GetPlayerAvatarWithSex(dataInfo.sex, dataInfo.main_fleet_level)
      else
        avatar = GameDataAccessHelper:GetFleetAvatar(dataInfo.icon, dataInfo.main_fleet_level)
      end
      DebugTable(dataInfo)
      DebugOut("FriendList:", avatar)
      self:GetFlashObject():InvokeASCallback("_root", "UpdateItemData", data_index, avatar, name, dataInfo.level, dataInfo.online, addText, tonumber(ptPos[2]), onlineText, offlineText)
    end
  elseif GameSetting.DataType.NoticeList == tonumber(ptPos[2]) then
    dataInfo = self.NoticeListData[data_index]
    if dataInfo then
      local name = GameUtils:GetUserDisplayName(dataInfo.user_name)
      local text = string.format(GameLoader:GetGameText("LC_MENU_ADD_AS_FRIEND"), name)
      local isMyFriend = self:IsInFriendsList(dataInfo.user_id)
      local addText = GameLoader:GetGameText("LC_MENU_ADD")
      self:GetFlashObject():InvokeASCallback("_root", "UpdateNoticeItemData", data_index, text, tonumber(ptPos[2]), isMyFriend, addText)
    end
  elseif GameSetting.DataType.BlockList == tonumber(ptPos[2]) then
    dataInfo = self.BlockListData[data_index]
    local cancelText = GameLoader:GetGameText("LC_MENU_BUTTON_CAPTION_CANCEL")
    if dataInfo then
      local name = GameUtils:GetUserDisplayName(dataInfo.name)
      local offlineText = GameLoader:GetGameText("LC_MENU_ALLIANCE_OFFLINE_INFO")
      local onlineText = GameLoader:GetGameText("LC_MENU_ALLIANCE_ONLINE_INFO")
      local avatar = ""
      if dataInfo.icon == 0 or dataInfo.icon == 1 then
        avatar = GameUtils:GetPlayerAvatarWithSex(dataInfo.sex, dataInfo.main_fleet_level)
      else
        avatar = GameDataAccessHelper:GetFleetAvatar(dataInfo.icon, dataInfo.main_fleet_level)
      end
      DebugTable(dataInfo)
      DebugOut("FriendList:", avatar)
      self:GetFlashObject():InvokeASCallback("_root", "UpdateItemData", data_index, avatar, name, dataInfo.level, dataInfo.online, cancelText, tonumber(ptPos[2]), onlineText, offlineText)
    end
  elseif GameSetting.DataType.tangoFriend == tonumber(ptPos[2]) then
    GameSetting:updateTangoFriendItem(data_index)
  end
end
function GameSetting:updateTangoFriendItem(data_index, isNotUpPic)
  dataInfo = self.tangoFriendList[data_index]
  local ReplaceTextureName = "tango_avatar_" .. tostring(data_index % 10)
  if dataInfo then
    do
      local name = GameUtils:GetUserDisplayName(dataInfo.fullName)
      local url = dataInfo.profileImageUrl
      if string.len(url) < 1 then
        url = "http://sdk.tango.me/assets/t/tango/sdk/profile_placeholder_2x.png"
      end
      local picName = "tango_pic_" .. dataInfo.tangoID .. ".png"
      if not isNotUpPic then
        if GameSetting:IsDownloadFileExsit("data2/" .. picName) then
          ext.UpdateAutoUpdateFile("data2/" .. picName, 0, 0, 0)
          if GameSetting:GetFlashObject() then
            GameSetting:GetFlashObject():ReplaceTexture(ReplaceTextureName, picName)
          end
        else
          ext.http.requestDownload({
            url,
            method = "GET",
            localpath = "data2/" .. picName,
            callback = function(statusCode, content, errstr)
              if errstr ~= nil or statusCode ~= 200 then
                GameUtils:printByAndroid("get file failed")
                return
              end
              ext.ScaleImageFile("data2/" .. picName, 80, 80)
              ext.UpdateAutoUpdateFile(content, 0, 0, 0)
              if GameSetting:GetFlashObject() then
                GameSetting:GetFlashObject():ReplaceTexture(ReplaceTextureName, picName)
              end
              GameSetting:SaveDownloadFileName("data2/" .. picName)
            end
          })
        end
      end
      local inviteText = GameLoader:GetGameText("LC_MENU_INVITE")
      self:GetFlashObject():InvokeASCallback("_root", "UpdateTangoItemData", data_index, name, dataInfo.boxClicked, GameSetting.DataType.tangoFriend, inviteText, GameSetting.tangoFriendList[data_index].Invited)
    end
  end
end
function GameSetting:chooseTangoFriend(data_index)
  local index = tonumber(data_index)
  if not GameSetting.tangoFriendList[index].Invited then
    GameSetting.tangoFriendList[index].boxClicked = not GameSetting.tangoFriendList[index].boxClicked
  else
    GameSetting.tangoFriendList[index].boxClicked = false
  end
  GameSetting:updateTangoFriendItem(index)
end
function GameSetting:updateTangoFriendALlChoose()
  if #GameSetting.tangoFriendList > 0 then
    GameSetting.isInviteAll = not GameSetting.isInviteAll
    for i = 1, #GameSetting.tangoFriendList do
      if not GameSetting.tangoFriendList[i].Invited then
        GameSetting.tangoFriendList[i].boxClicked = GameSetting.isInviteAll
      else
        GameSetting.tangoFriendList[i].boxClicked = false
      end
      GameSetting:updateTangoFriendItem(i, true)
    end
  end
end
function GameSetting:InviteAllTangeFriend()
  GameSetting:GetChooseTangoFriend()
  GameSetting:SendInviteTangoFriend()
end
function GameSetting:InviteTangoFriend(data_index)
  GameSetting.sendTangoInviteList = {}
  table.insert(GameSetting.sendTangoInviteList, GameSetting.tangoFriendList[data_index].tangoID)
  GameSetting:SendInviteTangoFriend()
end
function GameSetting:GetChooseTangoFriend()
  GameSetting.sendTangoInviteList = {}
  for i = 1, #GameSetting.tangoFriendList do
    if GameSetting.tangoFriendList[i].boxClicked and not GameSetting.tangoFriendList[i].Invited then
      table.insert(GameSetting.sendTangoInviteList, self.tangoFriendList[i].tangoID)
    end
  end
  return GameSetting.sendTangoInviteList
end
function GameSetting:SendInviteTangoFriend()
  if GameUtils.m_myProfile and GameSetting.sendTangoInviteList and #GameSetting.sendTangoInviteList > 0 then
    local content = {
      tangoid = GameUtils.m_myProfile.tangoID,
      friends = GameSetting.sendTangoInviteList
    }
    NetMessageMgr:SendMsg(NetAPIList.tango_invite_req.Code, content, GameSetting.ReqInvitedTangoFriendListCallback, true, nil)
    local subText = "Tap to play!"
    local serverName = GameUtils:GetLoginInfo().ServerInfo.name or " "
    local text = string.format(GameLoader:GetGameText("LC_MENU_ANGO_INVITE_TEXT"), serverName)
    local succCallback = function()
    end
    self:TangoSendAction(GameSetting.sendTangoInviteList, text, IMAGE_URL, INVITATION_URL, subText, succCallback)
  end
end
function GameSetting:TangoSendAction(t, text, imageUrl, linkUrl, subText, succCallback, failedCallback)
  if #t < 1 then
    return
  end
  ext.socialNetwork.sendActionMessage(t, text, imageUrl, linkUrl, subText, function(errStr)
    if errStr then
      if failedCallback then
        failedCallback()
      end
      GameUtils:ShowTips(errStr)
    else
      succCallback()
    end
  end)
end
function GameSetting:IsInFriendsList(dataInfo_id)
  local isMyFriend = false
  if self.FriendListData then
    for k, v in pairs(self.FriendListData) do
      if v.id == dataInfo_id then
        isMyFriend = true
      end
    end
  end
  return isMyFriend
end
function GameSetting:IsInBlockedFriendsList(dataInfo_id)
  local isMyFriend = false
  if self.BlockListData then
    for k, v in pairs(self.BlockListData) do
      if v.id == dataInfo_id then
        isMyFriend = true
      end
    end
  end
  return isMyFriend
end
