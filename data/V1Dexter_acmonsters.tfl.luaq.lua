local monsters = GameData.Dexter_ac.monsters
monsters[5200011] = {
  ID = 5200011,
  vessels = 1,
  Avatar = "head23",
  Ship = "ship37"
}
monsters[5200012] = {
  ID = 5200012,
  vessels = 2,
  Avatar = "head23",
  Ship = "ship37"
}
monsters[5200013] = {
  ID = 5200013,
  vessels = 3,
  Avatar = "head23",
  Ship = "ship37"
}
monsters[5200014] = {
  ID = 5200014,
  vessels = 4,
  Avatar = "head23",
  Ship = "ship37"
}
monsters[5200015] = {
  ID = 5200015,
  vessels = 5,
  Avatar = "head23",
  Ship = "ship37"
}
monsters[5200016] = {
  ID = 5200016,
  vessels = 6,
  Avatar = "head36",
  Ship = "ship11"
}
monsters[5200021] = {
  ID = 5200021,
  vessels = 1,
  Avatar = "head26",
  Ship = "ship38"
}
monsters[5200022] = {
  ID = 5200022,
  vessels = 2,
  Avatar = "head26",
  Ship = "ship38"
}
monsters[5200023] = {
  ID = 5200023,
  vessels = 3,
  Avatar = "head26",
  Ship = "ship38"
}
monsters[5200024] = {
  ID = 5200024,
  vessels = 4,
  Avatar = "head26",
  Ship = "ship38"
}
monsters[5200025] = {
  ID = 5200025,
  vessels = 5,
  Avatar = "head26",
  Ship = "ship38"
}
monsters[5200026] = {
  ID = 5200026,
  vessels = 6,
  Avatar = "head36",
  Ship = "ship14"
}
monsters[5200031] = {
  ID = 5200031,
  vessels = 1,
  Avatar = "head25",
  Ship = "ship47"
}
monsters[5200032] = {
  ID = 5200032,
  vessels = 2,
  Avatar = "head25",
  Ship = "ship47"
}
monsters[5200033] = {
  ID = 5200033,
  vessels = 3,
  Avatar = "head25",
  Ship = "ship47"
}
monsters[5200034] = {
  ID = 5200034,
  vessels = 4,
  Avatar = "head25",
  Ship = "ship47"
}
monsters[5200035] = {
  ID = 5200035,
  vessels = 5,
  Avatar = "head25",
  Ship = "ship47"
}
monsters[5200036] = {
  ID = 5200036,
  vessels = 6,
  Avatar = "head36",
  Ship = "ship25"
}
monsters[5200041] = {
  ID = 5200041,
  vessels = 1,
  Avatar = "head13",
  Ship = "ship2"
}
monsters[5200042] = {
  ID = 5200042,
  vessels = 2,
  Avatar = "head13",
  Ship = "ship2"
}
monsters[5200043] = {
  ID = 5200043,
  vessels = 3,
  Avatar = "head13",
  Ship = "ship2"
}
monsters[5200044] = {
  ID = 5200044,
  vessels = 4,
  Avatar = "head13",
  Ship = "ship2"
}
monsters[5200045] = {
  ID = 5200045,
  vessels = 5,
  Avatar = "head13",
  Ship = "ship2"
}
monsters[5200046] = {
  ID = 5200046,
  vessels = 6,
  Avatar = "head36",
  Ship = "ship12"
}
monsters[5200051] = {
  ID = 5200051,
  vessels = 1,
  Avatar = "head12",
  Ship = "ship49"
}
monsters[5200052] = {
  ID = 5200052,
  vessels = 2,
  Avatar = "head12",
  Ship = "ship49"
}
monsters[5200053] = {
  ID = 5200053,
  vessels = 3,
  Avatar = "head12",
  Ship = "ship49"
}
monsters[5200054] = {
  ID = 5200054,
  vessels = 4,
  Avatar = "head12",
  Ship = "ship49"
}
monsters[5200055] = {
  ID = 5200055,
  vessels = 5,
  Avatar = "head12",
  Ship = "ship49"
}
monsters[5200056] = {
  ID = 5200056,
  vessels = 6,
  Avatar = "head36",
  Ship = "ship24"
}
monsters[5200061] = {
  ID = 5200061,
  vessels = 1,
  Avatar = "head23",
  Ship = "ship5"
}
monsters[5200062] = {
  ID = 5200062,
  vessels = 2,
  Avatar = "head23",
  Ship = "ship5"
}
monsters[5200063] = {
  ID = 5200063,
  vessels = 3,
  Avatar = "head23",
  Ship = "ship5"
}
monsters[5200064] = {
  ID = 5200064,
  vessels = 4,
  Avatar = "head23",
  Ship = "ship5"
}
monsters[5200065] = {
  ID = 5200065,
  vessels = 5,
  Avatar = "head23",
  Ship = "ship5"
}
monsters[5200066] = {
  ID = 5200066,
  vessels = 6,
  Avatar = "head36",
  Ship = "ship24"
}
monsters[5200071] = {
  ID = 5200071,
  vessels = 1,
  Avatar = "head26",
  Ship = "ship42"
}
monsters[5200072] = {
  ID = 5200072,
  vessels = 2,
  Avatar = "head26",
  Ship = "ship42"
}
monsters[5200073] = {
  ID = 5200073,
  vessels = 3,
  Avatar = "head26",
  Ship = "ship42"
}
monsters[5200074] = {
  ID = 5200074,
  vessels = 4,
  Avatar = "head26",
  Ship = "ship42"
}
monsters[5200075] = {
  ID = 5200075,
  vessels = 5,
  Avatar = "head26",
  Ship = "ship42"
}
monsters[5200076] = {
  ID = 5200076,
  vessels = 6,
  Avatar = "head36",
  Ship = "ship25"
}
monsters[5200081] = {
  ID = 5200081,
  vessels = 1,
  Avatar = "head25",
  Ship = "ship43"
}
monsters[5200082] = {
  ID = 5200082,
  vessels = 2,
  Avatar = "head25",
  Ship = "ship43"
}
monsters[5200083] = {
  ID = 5200083,
  vessels = 3,
  Avatar = "head25",
  Ship = "ship43"
}
monsters[5200084] = {
  ID = 5200084,
  vessels = 4,
  Avatar = "head25",
  Ship = "ship43"
}
monsters[5200085] = {
  ID = 5200085,
  vessels = 5,
  Avatar = "head25",
  Ship = "ship43"
}
monsters[5200086] = {
  ID = 5200086,
  vessels = 6,
  Avatar = "head36",
  Ship = "ship16"
}
monsters[5200091] = {
  ID = 5200091,
  vessels = 1,
  Avatar = "head13",
  Ship = "ship41"
}
monsters[5200092] = {
  ID = 5200092,
  vessels = 2,
  Avatar = "head13",
  Ship = "ship41"
}
monsters[5200093] = {
  ID = 5200093,
  vessels = 3,
  Avatar = "head13",
  Ship = "ship41"
}
monsters[5200094] = {
  ID = 5200094,
  vessels = 4,
  Avatar = "head13",
  Ship = "ship41"
}
monsters[5200095] = {
  ID = 5200095,
  vessels = 5,
  Avatar = "head13",
  Ship = "ship41"
}
monsters[5200096] = {
  ID = 5200096,
  vessels = 6,
  Avatar = "head36",
  Ship = "ship20"
}
monsters[5200101] = {
  ID = 5200101,
  vessels = 1,
  Avatar = "head12",
  Ship = "ship41"
}
monsters[5200102] = {
  ID = 5200102,
  vessels = 2,
  Avatar = "head12",
  Ship = "ship42"
}
monsters[5200103] = {
  ID = 5200103,
  vessels = 3,
  Avatar = "head12",
  Ship = "ship43"
}
monsters[5200104] = {
  ID = 5200104,
  vessels = 4,
  Avatar = "head12",
  Ship = "ship44"
}
monsters[5200105] = {
  ID = 5200105,
  vessels = 5,
  Avatar = "head12",
  Ship = "ship45"
}
monsters[5200106] = {
  ID = 5200106,
  vessels = 6,
  Avatar = "head36",
  Ship = "ship23"
}
monsters[5200111] = {
  ID = 5200111,
  vessels = 1,
  Avatar = "head23",
  Ship = "ship24"
}
monsters[5200112] = {
  ID = 5200112,
  vessels = 2,
  Avatar = "head23",
  Ship = "ship24"
}
monsters[5200113] = {
  ID = 5200113,
  vessels = 3,
  Avatar = "head23",
  Ship = "ship24"
}
monsters[5200114] = {
  ID = 5200114,
  vessels = 4,
  Avatar = "head23",
  Ship = "ship24"
}
monsters[5200115] = {
  ID = 5200115,
  vessels = 5,
  Avatar = "head23",
  Ship = "ship24"
}
monsters[5200116] = {
  ID = 5200116,
  vessels = 6,
  Avatar = "head36",
  Ship = "ship23"
}
monsters[5200121] = {
  ID = 5200121,
  vessels = 1,
  Avatar = "head26",
  Ship = "ship25"
}
monsters[5200122] = {
  ID = 5200122,
  vessels = 2,
  Avatar = "head26",
  Ship = "ship25"
}
monsters[5200123] = {
  ID = 5200123,
  vessels = 3,
  Avatar = "head26",
  Ship = "ship25"
}
monsters[5200124] = {
  ID = 5200124,
  vessels = 4,
  Avatar = "head26",
  Ship = "ship25"
}
monsters[5200125] = {
  ID = 5200125,
  vessels = 5,
  Avatar = "head26",
  Ship = "ship25"
}
monsters[5200126] = {
  ID = 5200126,
  vessels = 6,
  Avatar = "head36",
  Ship = "ship24"
}
monsters[5200131] = {
  ID = 5200131,
  vessels = 1,
  Avatar = "head23",
  Ship = "ship24"
}
monsters[5200132] = {
  ID = 5200132,
  vessels = 2,
  Avatar = "head23",
  Ship = "ship24"
}
monsters[5200133] = {
  ID = 5200133,
  vessels = 3,
  Avatar = "head23",
  Ship = "ship24"
}
monsters[5200134] = {
  ID = 5200134,
  vessels = 4,
  Avatar = "head23",
  Ship = "ship24"
}
monsters[5200135] = {
  ID = 5200135,
  vessels = 5,
  Avatar = "head23",
  Ship = "ship24"
}
monsters[5200136] = {
  ID = 5200136,
  vessels = 6,
  Avatar = "head36",
  Ship = "ship23"
}
monsters[5200141] = {
  ID = 5200141,
  vessels = 1,
  Avatar = "head26",
  Ship = "ship25"
}
monsters[5200142] = {
  ID = 5200142,
  vessels = 2,
  Avatar = "head26",
  Ship = "ship25"
}
monsters[5200143] = {
  ID = 5200143,
  vessels = 3,
  Avatar = "head26",
  Ship = "ship25"
}
monsters[5200144] = {
  ID = 5200144,
  vessels = 4,
  Avatar = "head26",
  Ship = "ship25"
}
monsters[5200145] = {
  ID = 5200145,
  vessels = 5,
  Avatar = "head26",
  Ship = "ship25"
}
monsters[5200146] = {
  ID = 5200146,
  vessels = 6,
  Avatar = "head36",
  Ship = "ship24"
}
