local GameObjectBattleReplay = LuaObjectManager:GetLuaObject("GameObjectBattleReplay")
local GameStateBattlePlay = GameStateManager.GameStateBattlePlay
local GameObjectShakeScreen = LuaObjectManager:GetLuaObject("GameObjectShakeScreen")
local GameObjectBattleBG = LuaObjectManager:GetLuaObject("GameObjectBattleBG")
local GameObjectFakeBattle = LuaObjectManager:GetLuaObject("GameObjectFakeBattle")
local GameUIHalfPortrait = LuaObjectManager:GetLuaObject("GameUIHalfPortrait")
if GameSettingData.half_body_open == nil then
  GameSettingData.half_body_open = true
end
GameStateBattlePlay.isPlayEffect = GameSettingData.half_body_open and GameSettingData.frame60_open and g_isMemoryOver2GDevice
local bundleIdentifier = ext.GetBundleIdentifier()
if bundleIdentifier == "com.tap4fun.galaxyempire2_android.platform.qihoo360" or bundleIdentifier == "com.tap4fun.galaxyempire2_android.platform.uc" or bundleIdentifier == "com.tap4fun.galaxyempire2_android_cafe" then
  GameStateBattlePlay.isPlayEffect = false
end
DebugOut(" GameStateBattlePlay.isPlayEffect ", GameSettingData.frame60_open, g_isMemoryOver2GDevice, GameSettingData.half_body_open)
GameStateBattlePlay.MODE_PLAY = 1
GameStateBattlePlay.MODE_REPLAY = 2
GameStateBattlePlay.MODE_HISTORY = 3
function GameStateBattlePlay:InitGameState()
end
function GameStateBattlePlay:SetReplayMode(mode)
  self.m_replayMode = mode
  self.m_currentAreaID = nil
end
function GameStateBattlePlay:GetReplayMode()
  return self.m_replayMode
end
function GameStateBattlePlay:InitBattle(mode, battleResult, areaID, battleID)
  if DebugConfig.isDebugBattllePlayLoad then
    GameStateManager.GameStateDebug:SwitchTimeStart()
  end
  self:SetReplayMode(mode)
  self.m_fakeBattleID = TutorialQuestManager:GetTurialFakeBattleID(areaID, battleID)
  if self.m_fakeBattleID then
    local battleData = TutorialQuestManager:GetTurialFakeBattleData(self.m_fakeBattleID)
    TutorialQuestManager:SysFakeBattleInfo(battleResult, battleData)
    GameObjectBattleReplay:SetBattleData(battleData, areaID, battleID)
  else
    GameObjectBattleReplay:SetBattleData(battleResult, areaID, battleID)
  end
  DebugOut("battle Start result")
  self.m_currentAreaID = areaID
  self.m_currentBattleID = battleID
end
function GameStateBattlePlay:PlayEffect(isLeft, head, ship, banner, avatar, shipName)
  if GameStateBattlePlay.isPlayEffect then
    GameUIHalfPortrait:PlayEffect(isLeft, head, ship, banner, avatar, shipName)
    return true
  end
  return false
end
function GameStateBattlePlay:OnEffectEnd()
  GameObjectBattleReplay:OnEffectEnd()
end
function GameStateBattlePlay:OnFocusGain(previousState)
  DebugOutBattlePlay("GameStateBattlePlay:OnFocusGain")
  DeviceAutoConfig.StartStatisticsInfo("BattlePlayLoadTime")
  DeviceAutoConfig.StartStatisticsInfo("BattlePlayAllLoadTime")
  local isPlayFakeBattle = TutorialQuestManager:GetTurialFakeBattleID()
  self.m_preState = previousState
  GameObjectBattleBG:LoadBattleBG(self.m_currentAreaID)
  GameObjectBattleReplay:LoadFlashObject()
  GameObjectShakeScreen:LoadFlashObject()
  if isPlayFakeBattle then
    GameObjectFakeBattle:LoadFlashObject()
  end
  self:AddObject(GameObjectBattleBG)
  self:AddObject(GameObjectBattleReplay)
  if isPlayFakeBattle then
    self:AddObject(GameObjectFakeBattle)
  end
  self:AddObject(GameObjectShakeScreen)
  if GameStateBattlePlay.isPlayEffect then
    self:AddObject(GameUIHalfPortrait)
  end
  AddFlurryEvent("StartFirstBattle", {}, 1)
  DeviceAutoConfig.EndStatisticsInfo("BattlePlayLoadTime")
  DeviceAutoConfig.StartStatisticsInfo("BattlePlayPreLoadTime")
end
function GameStateBattlePlay:OnFocusLost(newState)
  DebugOutBattlePlay("GameStateBattlePlay:OnFocusLost")
  self.m_preState = nil
  self:EraseObject(GameObjectBattleBG)
  self:EraseObject(GameObjectBattleReplay)
  local isPlayFakeBattle = TutorialQuestManager:GetTurialFakeBattleID()
  if isPlayFakeBattle then
    self:EraseObject(GameObjectFakeBattle)
  end
  self:EraseObject(GameObjectShakeScreen)
  GameObjectShakeScreen:ResetShake()
  GameObjectShakeScreen:UnloadFlashObject()
  if GameStateBattlePlay.isPlayEffect then
    self:EraseObject(GameUIHalfPortrait)
  end
  self.m_isReplayMode = self.MODE_PLAY
  GameStateBattlePlay.report_id = nil
  AddFlurryEvent("EndFirstBattle", {}, 1)
end
function GameStateBattlePlay:RegisterOverCallback(func, args)
  self._battleOverCallback = {}
  self._battleOverCallback.func = func
  self._battleOverCallback.args = args
end
function GameStateBattlePlay:ExecuteOverCallback()
  if self._battleOverCallback then
    local callback = self._battleOverCallback.func
    local args = self._battleOverCallback.args
    if args and #args > 0 then
      callback(unpack(args))
    else
      callback(args)
    end
    self._battleOverCallback = nil
  end
end
function GameStateBattlePlay.onBufferChange(content)
  GameStateBattlePlay.dominationBuffer = content.icon
end
function GameStateBattlePlay.OnGroupFightReportNTF(content)
  DebugOutPutTable(content, "OnGroupFightReportNTF")
  if not GameObjectBattleReplay.GroupBattleReportArr then
    GameObjectBattleReplay.GroupBattleReportArr = {}
  end
  table.insert(GameObjectBattleReplay.GroupBattleReportArr, content)
  GameObjectBattleReplay.isGroupBattle = true
  GameObjectBattleReplay.curGroupBattleIndex = 1
end
