local GameUIFactory = LuaObjectManager:GetLuaObject("GameUIFactory")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local QuestTutorialUseFactory = TutorialQuestManager.QuestTutorialUseFactory
local QuestTutorialRepairFleet = TutorialQuestManager.QuestTutorialRepairFleet
local QuestTutorialBuildFactory = TutorialQuestManager.QuestTutorialBuildFactory
local GameUICommonDialog = LuaObjectManager:GetLuaObject("GameUICommonDialog")
local GameUIBarRight = LuaObjectManager:GetLuaObject("GameUIBarRight")
local GameStateMainPlanet = GameStateManager.GameStateMainPlanet
local GameUIRebuildFleet = LuaObjectManager:GetLuaObject("GameUIRebuildFleet")
local QuestTutorialComboGacha = TutorialQuestManager.QuestTutorialComboGacha
local GameUIActivityNew = LuaObjectManager:GetLuaObject("GameUIActivityNew")
local GameUIWDStuff = LuaObjectManager:GetLuaObject("GameUIWDStuff")
local QuestTutorialFactoryRemodel = TutorialQuestManager.QuestTutorialFactoryRemodel
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
GameUIFactory.buildingName = "factory"
function GameUIFactory:OnInitGame()
  GameGlobalData:RegisterDataChangeCallback("fleetinfo", GameUIFactory._CommanderInfoListener)
  GameGlobalData:RegisterDataChangeCallback("levelinfo", GameUIFactory.OnGlobalLevelChange)
end
function GameUIFactory:OnFSCommand(cmd, arg)
  if cmd == "update_commander_item" then
    self:UpdateCommanderItem(tonumber(arg))
  elseif cmd == "select_commander" then
    self:SelectCommander(tonumber(arg))
  elseif cmd == "repair_commander_vessels" then
    self:RepairCommanderVessels(tonumber(arg))
  elseif cmd == "repair_all_commander_vessels" then
    self:RepairAllCommanderVessels(tonumber(arg))
  elseif cmd == "close_all" then
    GameStateManager:GetCurrentGameState():EraseObject(self)
    if GameUIFactory.battle_failed then
      local param = {cli_data = "remodel"}
      NetMessageMgr:SendMsg(NetAPIList.battle_rate_req.Code, param, nil, false, nil)
      GameUIFactory.battle_failed = nil
    end
  elseif cmd == "click_help" then
    GameUIRebuildFleet:ShowHelpWindow()
  elseif cmd == "show_infobar" then
    GameUIRebuildFleet:ShowInfobar(tonumber(arg))
    GameUIRebuildFleet.IsShowInfoBarWindow = true
  elseif cmd == "upgrade_fleetpart" then
    GameUIRebuildFleet:AskForUpgrade()
    if immanentversion170 == 4 or immanentversion170 == 5 then
      QuestTutorialFactoryRemodel:SetFinish(true, false)
      self:GetFlashObject():InvokeASCallback("_root", "SetRemodelTutorialActive", QuestTutorialFactoryRemodel:IsActive())
      self:GetFlashObject():InvokeASCallback("_root", "ShowRebuildItemTutorial", 1, false, 1)
      GameUICommonDialog:PlayStory({1100095}, function()
        if self:GetFlashObject() then
          self:GetFlashObject():InvokeASCallback("_root", "ShowCloseTip", true)
        end
      end)
    end
  elseif cmd == "finish_upgrade" then
    GameUIRebuildFleet:AskForFinishUpgrade(tonumber(arg))
  elseif cmd == "click_help_others" then
    GameUIRebuildFleet:ShowHelpOthersWindow()
  elseif cmd == "click_ask_for_help" then
    GameUIRebuildFleet:AskForHelp()
  elseif cmd == "accelerate_all" then
    GameUIRebuildFleet:TryAccelerateAll()
  elseif cmd == "accelerate_self" then
    GameUIRebuildFleet:AskForHelp()
  elseif cmd == "accelerate_other" then
    GameUIRebuildFleet:AccelerateOther(tonumber(arg))
  elseif cmd == "update_help_message_item" then
    GameUIRebuildFleet:SetHelpMessageItem(tonumber(arg))
  elseif cmd == "update_accelerate_item" then
    GameUIRebuildFleet:SetAccelerateItemData(tonumber(arg))
  elseif cmd == "selected_rebuild" then
    GameUIRebuildFleet:CheckVisible(GameUIRebuildFleet.Entrance.FactoryInner)
  elseif cmd == "close_rebuild_accelerate" then
    GameUIRebuildFleet.IsShowHelpOthersWindow = false
  elseif cmd == "CloseFleetPartWindow" then
    GameUIRebuildFleet.IsShowInfoBarWindow = false
  elseif cmd == "shareRebuildHelpCheckClicked" then
    if GameUIRebuildFleet.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_REBUILDSHIP_REQUET_HELP] then
      GameUIRebuildFleet.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_REBUILDSHIP_REQUET_HELP] = false
    else
      GameUIRebuildFleet.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_REBUILDSHIP_REQUET_HELP] = true
    end
    local flash_obj = self:GetFlashObject()
    if flash_obj then
      flash_obj:InvokeASCallback("_root", "setCheckBox", GameUIRebuildFleet.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_REBUILDSHIP_REQUET_HELP], FacebookGraphStorySharer.StoryType.STORY_TYPE_REBUILDSHIP_REQUET_HELP)
    end
  elseif cmd == "gotoRebuildPage" then
    local page = tonumber(arg)
    if page == 2 and not GameUIRebuildFleet:IsFirstRebuildPageOver() then
      local message = GameLoader:GetGameText("LC_MENU_REBUILD_NEXT_PAGE_ALERT")
      GameTip:Show(message)
    elseif page == 3 and not GameUIRebuildFleet:IsSecondRebuildPageOver() then
      local message = GameLoader:GetGameText("LC_MENU_REBUILD_NEXT_PAGE_ALERT")
      GameTip:Show(message)
    else
      local flash_obj = self:GetFlashObject()
      if flash_obj then
        flash_obj:InvokeASCallback("_root", "switchRebuildPage", page)
      end
    end
  else
    assert(false)
  end
end
function GameUIFactory:AnimationMoveInAll()
  self:GetFlashObject():InvokeASCallback("_root", "ShowRepairWindowDirectly")
end
function GameUIFactory.OnGlobalLevelChange()
  DebugOut("GameUIFactory.OnGlobalLevelChange")
  local levelInfo = GameGlobalData:GetData("levelinfo")
  if levelInfo and tonumber(levelInfo.level) > 14 and not QuestTutorialBuildFactory:IsFinished() and not QuestTutorialBuildFactory:IsActive() then
    local factory = GameGlobalData:GetBuildingInfo("factory")
    if factory.level > 0 then
      QuestTutorialBuildFactory:SetFinish(true)
      if (immanentversion170 == 4 or immanentversion170 == 5) and not QuestTutorialFactoryRemodel:IsFinished() and not QuestTutorialFactoryRemodel:IsActive() then
        GameUICommonDialog:PlayStory({1100094}, function()
          QuestTutorialFactoryRemodel:SetActive(true, false)
        end)
      end
    elseif not QuestTutorialBuildFactory:IsFinished() then
      QuestTutorialBuildFactory:SetActive(true)
    end
  end
end
function GameUIFactory:OnAddToGameState(new_parent)
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  GameUIRebuildFleet:OnAddToGameState()
  self:UpdateCommanderList()
  self:SelectCommander(1)
  self:UpdateTotalRepairInfo()
  self:CheckTutorialQuest()
  self:AnimationMoveInAll()
  GameUIFactory.tmp_tag = 1
  if self.EnterRebuild then
    GameUIRebuildFleet:CheckVisible(GameUIRebuildFleet.Entrance.Alliance)
  end
  DebugOut("OnAddToGameState GameUIFactory")
end
function GameUIFactory:OnEraseFromGameState()
  self.EnterRebuild = false
  if self.EraseFlashObjectCallback then
    self.EraseFlashObjectCallback()
  end
  GameStateMainPlanet:CheckShowQuestInMainPlanet()
  if immanentversion == 2 and GameUIFactory.tmp_tag == 2 then
    local function callback()
      GameUIBarRight:GetFlashObject():InvokeASCallback("_root", "ShowTutorialBattleMapBtn")
    end
    GameUICommonDialog:ForcePlayStory({100043}, callback)
  end
  GameUIFactory:UnloadFlashObject()
end
function GameUIFactory:UpdateCommanderList()
  local user_commander_list = GameGlobalData:GetData("fleetinfo").fleets
  DebugOut("UpdateCommanderList in lua")
  self:GetFlashObject():InvokeASCallback("_root", "updateCommanderList", #user_commander_list)
end
function GameUIFactory:CheckTutorialQuest()
  DebugOut("GameUIFactory:CheckTutorialQuest")
  if immanentversion == 2 then
    if QuestTutorialUseFactory:IsActive() and not QuestTutorialUseFactory:IsFinished() or QuestTutorialRepairFleet:IsActive() and not QuestTutorialRepairFleet:IsFinished() then
      GameUICommonDialog:ForcePlayStory({100041})
    end
  elseif immanentversion == 1 then
    if QuestTutorialUseFactory:IsActive() and not QuestTutorialUseFactory:IsFinished() then
      QuestTutorialUseFactory:SetFinish(true)
    end
    if QuestTutorialRepairFleet:IsActive() and not QuestTutorialRepairFleet:IsFinished() then
      GameUICommonDialog:ForcePlayStory({100040, 100041}, nil)
    end
  end
  if immanentversion170 == 4 or immanentversion170 == 5 then
    self:GetFlashObject():InvokeASCallback("_root", "SetRemodelTutorialActive", QuestTutorialFactoryRemodel:IsActive())
  end
end
function GameUIFactory:UpdateCommanderItem(commander_index)
  local user_commander_list = GameGlobalData:GetData("fleetinfo").fleets
  local commander_data = user_commander_list[commander_index]
  local commander_name = GameDataAccessHelper:GetFleetLevelDisplayName(commander_data.identity, commander_data.level)
  local tempid = commander_data.identity
  if tempid == 1 then
    local leaderlist = GameGlobalData:GetData("leaderlist")
    local curMatrixIndex = GameGlobalData.GlobalData.curMatrixIndex
    curMatrixIndex = curMatrixIndex or 1
    tempid = leaderlist.leader_ids[curMatrixIndex]
  end
  if tempid then
    local commander_avatar = GameDataAccessHelper:GetFleetAvatar(tempid, commander_data.level)
    local commander_vesselstype = GameDataAccessHelper:GetCommanderVesselsType(tempid, commander_data.level)
    local durability_percent = commander_data.damage
    self:GetFlashObject():InvokeASCallback("_root", "updateCommanderItem", commander_index, commander_name, commander_avatar, commander_vesselstype, durability_percent)
  end
end
function GameUIFactory:SelectCommander(commander_index)
  local user_commander_list = GameGlobalData:GetData("fleetinfo").fleets
  local commander_data = user_commander_list[commander_index]
  local tempid = commander_data.identity
  if tempid == 1 then
    local leaderlist = GameGlobalData:GetData("leaderlist")
    local curMatrixIndex = GameGlobalData.GlobalData.curMatrixIndex
    curMatrixIndex = curMatrixIndex or 1
    tempid = leaderlist.leader_ids[curMatrixIndex]
  end
  if tempid then
    local commander_vessels = GameDataAccessHelper:GetCommanderVesselsImage(tempid, commander_data.level)
    local commander_durability = commander_data.damage
    local commander_repaircost = commander_data.repair_money
    self:GetFlashObject():InvokeASCallback("_root", "updateSelectedCommander", commander_index, commander_vessels, commander_durability, commander_repaircost)
  end
end
function GameUIFactory:UpdateTotalRepairInfo()
  local average_durability = 0
  local total_repaircost = 0
  local user_commander_list = GameGlobalData:GetData("fleetinfo").fleets
  for _, commander_data in ipairs(user_commander_list) do
    average_durability = average_durability + commander_data.damage
    total_repaircost = total_repaircost + commander_data.repair_money
  end
  average_durability = math.floor(average_durability / #user_commander_list)
  self:GetFlashObject():InvokeASCallback("_root", "updateTotalRepairInfo", average_durability, total_repaircost)
end
function GameUIFactory:NeedUpdateData()
  self:GetFlashObject():InvokeASCallback("_root", "needUpdateData")
end
function GameUIFactory:RepairFleet(commander_index)
  local data_commander = self:GetCommanderInfo(commander_status, commander_index)
  local content = {
    identity = data_commander.identity
  }
  NetMessageMgr:SendMsg(NetAPIList.fleet_repair_req.Code, content, GameStateFactory.serverCallback, true, nil)
  GameUtils:RecordForTongdui(NetAPIList.fleet_repair_req.Code, content, GameStateFactory.serverCallback, true, nil)
end
function GameUIFactory:RepairCommanderVessels(commander_index)
  local user_commander_list = GameGlobalData:GetData("fleetinfo").fleets
  local commander_data = user_commander_list[commander_index]
  local net_packet = {
    identity = commander_data.identity
  }
  NetMessageMgr:SendMsg(NetAPIList.fleet_repair_req.Code, net_packet, self._NetMessageCallback, true, nil)
  GameUtils:RecordForTongdui(NetAPIList.fleet_repair_req.Code, net_packet, self._NetMessageCallback, true, nil)
  if immanentversion == 2 then
    if QuestTutorialRepairFleet:IsActive() then
      QuestTutorialRepairFleet:SetFinish(true)
    end
    if QuestTutorialUseFactory:IsActive() then
      QuestTutorialUseFactory:SetFinish(true)
      GameUIFactory.tmp_tag = 2
    end
  end
end
function GameUIFactory:RepairAllCommanderVessels()
  local net_packet = {onserver = 1}
  NetMessageMgr:SendMsg(NetAPIList.fleet_repair_all_req.Code, net_packet, self._NetMessageCallback, true, nil)
  GameUtils:RecordForTongdui(NetAPIList.fleet_repair_all_req.Code, net_packet, self._NetMessageCallback, true, nil)
  if immanentversion == 2 then
    if QuestTutorialRepairFleet:IsActive() then
      QuestTutorialRepairFleet:SetFinish(true)
    end
    if QuestTutorialUseFactory:IsActive() then
      QuestTutorialUseFactory:SetFinish(true)
      GameUIFactory.tmp_tag = 2
    end
  end
end
function GameUIFactory._CommanderInfoListener()
  if GameStateManager:GetCurrentGameState():IsObjectInState(GameUIFactory) then
    GameUIFactory:NeedUpdateData()
    GameUIFactory:UpdateTotalRepairInfo()
  end
end
function GameUIFactory:Update(dt)
  local flash_obj = self:GetFlashObject()
  flash_obj:Update(dt)
  flash_obj:InvokeASCallback("_root", "onUpdateFrame", dt)
  GameUIRebuildFleet:Update()
end
function GameUIFactory._NetMessageCallback(msgtype, content)
  DebugOut("GameUIFactory._NetMessageCallback", msgtype)
  if msgtype == NetAPIList.common_ack.Code and (content.api == NetAPIList.fleet_repair_req.Code or content.api == NetAPIList.fleet_repair_all_req.Code) then
    DebugTable(content)
    if content.code == 0 then
      if (content.api == NetAPIList.fleet_repair_req.Code or content.api == NetAPIList.fleet_repair_all_req.Code) and immanentversion == 1 and QuestTutorialRepairFleet:IsActive() then
        GameUICommonDialog:ForcePlayStory({100042, 100043}, nil)
        DebugOut("QuestTutorialRepairFleet:SetFinish(true)")
        QuestTutorialRepairFleet:SetFinish(true)
      end
    else
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  return false
end
if AutoUpdate.isAndroidDevice then
  function GameUIFactory.OnAndroidBack()
    if GameUIRebuildFleet.IsShowHelpOthersWindow then
      GameUIFactory:GetFlashObject():InvokeASCallback("_root", "CloseAccelerateWindow")
    elseif GameUIWDStuff.menuType.menu_type_help == GameUIWDStuff.currentMenu then
      local flash_obj = GameUIWDStuff:GetFlashObject()
      if flash_obj then
        flash_obj:InvokeASCallback("_root", "hideHelp")
      end
    elseif GameUIRebuildFleet.IsShowInfoBarWindow then
      GameUIFactory:GetFlashObject():InvokeASCallback("_root", "CloseFleetPartWindow")
    else
      GameUIFactory:OnFSCommand("close_all")
    end
  end
end
