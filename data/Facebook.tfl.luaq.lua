FACEBOOK_ACTION_STATUS = {
  IDLE = 1,
  DOING = 2,
  SUCCESS = 3,
  FAILED
}
FACEBOOK_ACTION_TYPE = {
  LOGIN = 1,
  SHARE_TEXT = 2,
  SHARE_LINK = 3,
  GET_FRIEND_LIST = 4,
  INVITE_FRIEND = 5,
  SEND_GIFT_MSG = 6,
  GET_INVITEABLE_FRIEND_LIST = 7,
  SHARE_STORY = 8,
  SHARE_STORY_TEMPER = 9
}
Facebook = {
  mLoginStatus = FACEBOOK_ACTION_STATUS.IDLE,
  mUserId = "",
  mToken = "",
  mUserName = "",
  mEmail = "",
  mError = nil,
  mUseFriendFunction = false,
  mLoginCallback = nil,
  mGetFriendListCallback = nil,
  mGetInviteableFriendListCallback = nil,
  mShareTextCallback = nil,
  mShareLinkCallback = nil,
  mShareStoryCallback = nil,
  mInviteFriendsCallback = nil,
  mSendGiftMsgToFriendCallback = nil,
  mActionList = {},
  mActionStatus = FACEBOOK_ACTION_STATUS.IDLE,
  mImgUrl = nil,
  mIsBindFacebookAccount = false,
  mbindFacebookId = nil,
  mBindInfo = nil,
  mNeedCheckValid = false,
  mLogInV2 = false
}
function Facebook.LoginCallback(success, idOrError, token, username, email)
  if success then
    Facebook.mError = nil
    Facebook.mUserId = idOrError
    Facebook.mToken = token
    Facebook.mUserName = username
    Facebook.mEmail = email
    print(idOrError)
    print(token)
    Facebook.mLoginStatus = FACEBOOK_ACTION_STATUS.SUCCESS
    Facebook:SaveFacebookInfo(idOrError, username)
  else
    DebugOut("idOrError 111 = ", idOrError)
    Facebook.mError = idOrError
    Facebook.mLoginStatus = FACEBOOK_ACTION_STATUS.IDLE
    Facebook:ClearActionList()
  end
  Facebook:SetStatus(FACEBOOK_ACTION_STATUS.IDLE)
  print("Facebook.LoginCallback " .. tostring(success))
  if success and Facebook.mNeedCheckValid then
    DebugOut("111111111")
    if Facebook.mLogInV2 then
      valid = Facebook:CheckFacebookAccountValiedV2(idOrError, token, username, email)
    else
      valid = Facebook:CheckFacebookAccountValied(idOrError, token, username, email)
    end
    DebugOut("22222222")
    if not valid then
      print("Facebook.LoginCallback ckeck valid failed.")
      Facebook:ClearActionList()
      Facebook.mLoginStatus = FACEBOOK_ACTION_STATUS.IDLE
      return
    end
  end
  if Facebook.mLoginCallback then
    DebugOut("gogogogogogogo111")
    Facebook.mLoginCallback(success, idOrError, token, username, email)
    Facebook.mLoginCallback = nil
  end
end
function Facebook.GetFriendListCallback(success, friendListStr)
  Facebook:SetStatus(FACEBOOK_ACTION_STATUS.IDLE)
  if success then
    print(friendListStr)
  else
  end
  print("Facebook.GetFriendList " .. tostring(success))
  local friends = ext.json.parse(friendListStr)
  DebugOut("friends = ", friends)
  DebugTable(friends)
  if Facebook.mGetFriendListCallback then
    Facebook.mGetFriendListCallback(success, friends)
    Facebook.mGetFriendListCallback = nil
  end
end
function Facebook.ShareTextCallback(success)
  Facebook:SetStatus(FACEBOOK_ACTION_STATUS.IDLE)
  if success then
  else
    Facebook.mError = idOrError
  end
  print("Facebook.ShareTextCallback " .. tostring(success))
  if Facebook.mShareTextCallback then
    Facebook.mShareTextCallback(success)
    Facebook.mShareTextCallback = nil
  end
end
function Facebook.ShareLinkCallback(success, msg)
  Facebook:SetStatus(FACEBOOK_ACTION_STATUS.IDLE)
  if success then
  else
    Facebook.mError = idOrError
    if msg then
      local startPos, endPos = string.find(msg, "error_code")
      if startPos then
        Facebook.mLoginStatus = FACEBOOK_ACTION_STATUS.IDLE
      end
    end
  end
  DebugOut(msg)
  print("Facebook.ShareLinkCallback " .. tostring(success))
  if Facebook.mShareLinkCallback then
    Facebook.mShareLinkCallback(success)
    Facebook.mShareLinkCallback = nil
  end
end
function Facebook.ShareStoryCallback(success, msg)
  if success then
  else
  end
  DebugOut(msg)
  print("Facebook.mShareStoryCallback " .. tostring(success))
  if Facebook.mShareStoryCallback then
    Facebook.mShareStoryCallback(success)
    Facebook.mShareStoryCallback = nil
  else
    Facebook:SetStatus(FACEBOOK_ACTION_STATUS.IDLE)
  end
end
function Facebook.InviteFriendsCallback(success, msg)
  Facebook:SetStatus(FACEBOOK_ACTION_STATUS.IDLE)
  if success then
  else
    Facebook.mError = idOrError
    if msg then
      local startPos, endPos = string.find(msg, "error_code")
      if startPos then
        local isUserCancel = false
        local params = LuaUtils:string_split(msg, "&")
        for k, v in pairs(params) do
          if v then
            params2 = LuaUtils:string_split(v, "=")
            if params2 and #params2 == 2 and "error_code" == params2[1] and "4201" == params2[2] then
              isUserCancel = true
            end
          end
        end
        if not isUserCancel then
          Facebook.mLoginStatus = FACEBOOK_ACTION_STATUS.IDLE
        end
      end
    end
  end
  DebugOut(msg)
  print("Facebook.InviteFriendsCallback " .. tostring(success))
  if Facebook.mInviteFriendsCallback then
    Facebook.mInviteFriendsCallback(success)
    Facebook.mInviteFriendsCallback = nil
  end
end
function Facebook.SendGiftMsgToFriendCallback(success, msg)
  Facebook:SetStatus(FACEBOOK_ACTION_STATUS.IDLE)
  if success then
  else
  end
  DebugOut(msg)
  print("Facebook.SendGiftMsgToFriendCallback " .. tostring(success))
  if Facebook.mSendGiftMsgToFriendCallback then
    Facebook.mSendGiftMsgToFriendCallback(success)
    Facebook.mSendGiftMsgToFriendCallback = nil
  end
end
function Facebook:Reset()
  Facebook:SetStatus(FACEBOOK_ACTION_STATUS.IDLE)
  Facebook.mActionList = {}
  Facebook.mLoginStatus = FACEBOOK_ACTION_STATUS.IDLE
  Facebook.mHeadMap = {}
end
function Facebook:SetupFacebookInfo(success, idOrError, token, username, email)
  Facebook.mError = nil
  Facebook.mUserId = idOrError
  Facebook.mToken = token
  Facebook.mUserName = username
  Facebook.mEmail = email
end
function Facebook:SetStatus(status)
  Facebook.mActionStatus = status
end
function Facebook:GetStatus()
  return Facebook.mActionStatus
end
function Facebook:ClearActionList()
  if Facebook.mActionList and #Facebook.mActionList > 0 then
    for k, v in pairs(Facebook.mActionList) do
      if v.data.callback then
        v.data.callback(false)
      end
    end
  end
  Facebook.mActionList = {}
end
function Facebook:AddAction(action, pos)
  DebugOut("AddAction :")
  DebugTable(action)
  if not Facebook.mActionList then
    Facebook.mActionList = {}
  end
  if pos and pos > 0 and pos <= #Facebook.mActionList then
    table.insert(Facebook.mActionList, pos, action)
  else
    table.insert(Facebook.mActionList, action)
  end
end
function Facebook:IsBindFacebook()
  return Facebook.mIsBindFacebookAccount
end
function Facebook:LoginWithNoUI()
  if not Facebook:IsSupportFacebook() then
    Facebook:ShowDownloadTip()
    return
  end
  if ext.socialShare.FB_LoginWithNoUI then
    ext.socialShare.FB_LoginWithNoUI()
    return
  elseif AutoUpdate.isAndroidDevice and ext.socialShare and ext.socialShare.FB_IsLogin and ext.socialShare.FB_IsLogin() then
    ext.socialShare.FB_Login()
  end
end
function Facebook:Login(callback, checkValid, useV2)
  if not Facebook:IsSupportFacebook() then
    Facebook:ShowDownloadTip()
    return
  end
  if useV2 then
    Facebook.mLogInV2 = true
  else
    Facebook.mLogInV2 = false
  end
  if Facebook:GetStatus() == FACEBOOK_ACTION_STATUS.DOING then
    Facebook:SetStatus(FACEBOOK_ACTION_STATUS.IDLE)
  end
  local isDo = false
  if Facebook:GetStatus() ~= FACEBOOK_ACTION_STATUS.DOING then
    Facebook.mNeedCheckValid = checkValid
    Facebook:SetStatus(FACEBOOK_ACTION_STATUS.DOING)
    Facebook.mLoginStatus = FACEBOOK_ACTION_STATUS.DOING
    isDo = true
  end
  local function doLogin()
    if isDo then
      Facebook.mLoginCallback = callback
      ext.socialShare.FB_Login()
    end
  end
  if checkValid and not Facebook:IsBindFacebook() then
    DebugOut("Need bind facebook account.")
    local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
    local function okCallback()
      doLogin()
    end
    local cancelCallback = function()
      Facebook:Reset()
    end
    local tip = GameLoader:GetGameText("LC_MENU_FACEBOOK_NOT_BOUND_ACCOUNT")
    GameUIGlobalScreen:ShowMessageBox(2, "", tip, okCallback, cancelCallback)
  else
    doLogin()
  end
end
function Facebook:LoginV2(callback, checkValid)
  if not Facebook:IsSupportFacebook() then
    Facebook:ShowDownloadTip()
    return
  end
  Facebook.mLogInV2 = true
  if Facebook:GetStatus() == FACEBOOK_ACTION_STATUS.DOING then
    Facebook:SetStatus(FACEBOOK_ACTION_STATUS.IDLE)
  end
  local isDo = false
  if Facebook:GetStatus() ~= FACEBOOK_ACTION_STATUS.DOING then
    Facebook.mNeedCheckValid = checkValid
    Facebook:SetStatus(FACEBOOK_ACTION_STATUS.DOING)
    Facebook.mLoginStatus = FACEBOOK_ACTION_STATUS.DOING
    isDo = true
  end
  local function doLogin()
    if isDo then
      DebugOut("do logint!!!")
      Facebook.mLoginCallback = callback
      ext.socialShare.FB_Login()
    end
  end
  if checkValid then
    DebugOut("Need bind facebook account.")
    doLogin()
  else
    doLogin()
  end
end
function Facebook:Logout()
end
function Facebook:GetFriendList(callback)
  if not Facebook:IsFriendFunctionEnabled() then
    return
  end
  if not Facebook:IsSupportFacebook() then
    Facebook:ShowDownloadTip()
    return
  end
  if Facebook.mLoginStatus == FACEBOOK_ACTION_STATUS.FAILED then
    return
  elseif Facebook.mLoginStatus == FACEBOOK_ACTION_STATUS.IDLE then
    Facebook:Login(nil, true)
  end
  if Facebook:GetStatus() ~= FACEBOOK_ACTION_STATUS.DOING then
    Facebook:SetStatus(FACEBOOK_ACTION_STATUS.DOING)
    Facebook.mGetFriendListCallback = callback
    ext.socialShare.FB_GetFriendList()
  else
    local action = {
      actionType = FACEBOOK_ACTION_TYPE.GET_FRIEND_LIST,
      data = {callback = callback}
    }
    Facebook:AddAction(action)
  end
end
function Facebook:GetCanFetchInvitableFriendList()
  if not Facebook:IsFriendFunctionEnabled() then
    return false
  end
  if ext.socialShare.FB_GetInvitableFriends then
    return true
  end
  return false
end
function Facebook:GetInvitableFriendList(excludeFriends, callback)
  if not Facebook:IsFriendFunctionEnabled() then
    return
  end
  if not Facebook:IsSupportFacebook() then
    Facebook:ShowDownloadTip()
    return
  end
  if not Facebook:GetCanFetchInvitableFriendList() then
    return
  end
  if Facebook.mLoginStatus == FACEBOOK_ACTION_STATUS.FAILED then
    return
  elseif Facebook.mLoginStatus == FACEBOOK_ACTION_STATUS.IDLE then
    Facebook:Login(nil, true)
  end
  local excludeFriendsStr = ""
  if excludeFriends then
    excludeFriendsStr = table.concat(excludeFriends, ",")
  end
  if Facebook:GetStatus() ~= FACEBOOK_ACTION_STATUS.DOING then
    Facebook:SetStatus(FACEBOOK_ACTION_STATUS.DOING)
    Facebook.mGetFriendListCallback = callback
    ext.socialShare.FB_GetInvitableFriends(excludeFriendsStr)
  else
    local action = {
      actionType = FACEBOOK_ACTION_TYPE.GET_INVITEABLE_FRIEND_LIST,
      data = {excludeFriends = excludeFriends, callback = callback}
    }
    Facebook:AddAction(action)
  end
end
function Facebook:ShareText(text, callback)
  if not Facebook:IsSupportFacebook() then
    Facebook:ShowDownloadTip()
    return
  end
  if Facebook.mLoginStatus == FACEBOOK_ACTION_STATUS.FAILED then
    return
  elseif Facebook.mLoginStatus == FACEBOOK_ACTION_STATUS.IDLE then
    Facebook:Login(nil, true)
  end
  if Facebook:GetStatus() ~= FACEBOOK_ACTION_STATUS.DOING then
    Facebook:SetStatus(FACEBOOK_ACTION_STATUS.DOING)
    Facebook.mShareTextCallback = callback
    ext.socialShare.FB_ShareText(text)
  else
    local action = {
      actionType = FACEBOOK_ACTION_TYPE.SHARE_TEXT,
      data = {text = text, callback = callback}
    }
    Facebook:AddAction(action)
  end
end
function Facebook:ShareLink(gameUrl, title, subTitle, describe, picUrl, callback)
  if not Facebook:IsSupportFacebook() then
    Facebook:ShowDownloadTip()
    return
  end
  local localAppid = ext.GetBundleIdentifier()
  local isAndroidLoc = string.find(localAppid, "android")
  if AutoUpdate.localAppVersion < 10900 then
    local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
    if isAndroidLoc then
      local function callback()
        ext.http.openURL(GameUtils.AppDownLoadURL[localAppid])
      end
      local text_title = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
      GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.Caption)
      GameUIMessageDialog:SetLeftTextButton(GameLoader:GetGameText("LC_MENU_GET_RIGHT_VERSION_CANCEL"), nil, {})
      GameUIMessageDialog:SetRightTextButton(GameLoader:GetGameText("LC_MENU_GET_RIGHT_VERSION_CONFIRM"), callback, {})
      GameUIMessageDialog:Display(text_title, GameLoader:GetGameText("LC_MENU_SHARE_VERSION_UPDATE_NOTICE"))
    else
      local function callback()
        if GameUtils.WorldPack[localAppid] then
          ext.http.openURL("http://tap4fun.com/en/game/7")
        else
          ext.http.openURL("http://yh.t4f.cn/")
        end
      end
      local text_title = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
      GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.Caption)
      GameUIMessageDialog:SetLeftTextButton(GameLoader:GetGameText("LC_MENU_GET_RIGHT_VERSION_CANCEL"), nil, {})
      GameUIMessageDialog:SetRightTextButton(GameLoader:GetGameText("LC_MENU_GET_RIGHT_VERSION_CONFIRM"), callback, {})
      GameUIMessageDialog:Display(text_title, GameLoader:GetGameText("LC_MENU_SHARE_VERSION_UPDATE_NOTICE"))
    end
    return
  end
  if Facebook.mLoginStatus == FACEBOOK_ACTION_STATUS.FAILED then
    return
  elseif Facebook.mLoginStatus == FACEBOOK_ACTION_STATUS.IDLE then
    Facebook:Login(nil, true)
  end
  if Facebook:GetStatus() ~= FACEBOOK_ACTION_STATUS.DOING then
    Facebook:SetStatus(FACEBOOK_ACTION_STATUS.DOING)
    Facebook.mShareLinkCallback = callback
    ext.socialShare.FB_ShareLink(gameUrl, title, subTitle, describe, picUrl)
  else
    local action = {
      actionType = FACEBOOK_ACTION_TYPE.SHARE_LINK,
      data = {
        gameUrl = gameUrl,
        title = title,
        subTitle = subTitle,
        describe = describe,
        picUrl = picUrl,
        callback = callback
      }
    }
    Facebook:AddAction(action)
  end
end
function Facebook:InviteFriends(inviteTitle, inviteMsg, friendsList, callback)
  if not Facebook:IsFriendFunctionEnabled() then
    return
  end
  if not Facebook:IsSupportFacebook() then
    Facebook:ShowDownloadTip()
    return
  end
  if Facebook.mLoginStatus == FACEBOOK_ACTION_STATUS.FAILED then
    return
  elseif Facebook.mLoginStatus == FACEBOOK_ACTION_STATUS.IDLE then
    Facebook:Login(nil, true)
  end
  local friendsListStr = ""
  if friendsList then
    friendsListStr = table.concat(friendsList, ",")
  end
  if Facebook:GetStatus() ~= FACEBOOK_ACTION_STATUS.DOING then
    Facebook.mInviteFriendsCallback = callback
    ext.socialShare.FB_InviteFriends(Facebook.mUserId, Facebook.mToken, inviteTitle, inviteMsg, friendsListStr)
  else
    local action = {
      actionType = FACEBOOK_ACTION_TYPE.INVITE_FRIEND,
      data = {
        inviteTitle = inviteTitle,
        inviteMsg = inviteMsg,
        inviteFriendsList = friendsList,
        callback = callback
      }
    }
    Facebook:AddAction(action)
  end
end
function Facebook:SendGiftMsgToFriend(inviteTitle, inviteMsg, friendIdList, callback)
  if not Facebook:IsFriendFunctionEnabled() then
    return
  end
  if not Facebook:IsSupportFacebook() then
    Facebook:ShowDownloadTip()
    return
  end
  if Facebook.mLoginStatus == FACEBOOK_ACTION_STATUS.FAILED then
    return
  elseif Facebook.mLoginStatus == FACEBOOK_ACTION_STATUS.IDLE then
    Facebook:Login(nil, true)
  end
  if Facebook:GetStatus() ~= FACEBOOK_ACTION_STATUS.DOING then
    if friendIdList and #friendIdList > 0 then
      local friendIds = ""
      for i = 1, #friendIdList do
        friendIds = friendIds .. friendIdList[i]
        if i < #friendIdList then
          friendIds = friendIds .. "\001"
        end
      end
      DebugTable(friendIdList)
      DebugOut("friendIds " .. friendIds)
      Facebook.mSendGiftMsgToFriendCallback = callback
      ext.socialShare.FB_SendGift(inviteTitle, inviteMsg, "1629062513983940", friendIds)
    else
      DebugOut("friendIdList is nil or empty.")
      if callback then
        callback(false)
      end
    end
  else
    local action = {
      actionType = FACEBOOK_ACTION_TYPE.SEND_GIFT_MSG,
      data = {
        inviteTitle = inviteTitle,
        inviteMsg = inviteMsg,
        friendIdList = friendIdList,
        callback = callback
      }
    }
    Facebook:AddAction(action)
  end
end
function Facebook:IsHasHeadIconFile(fbId)
  if Facebook.mHeadMap and Facebook.mHeadMap[fbId] then
    return true
  else
    return false
  end
end
function Facebook:GetHeadIcon(fbId, callback)
  local fileName = "fb2" .. tostring(fbId) .. ".png"
  local localPath = "data2/fb2" .. tostring(fbId) .. ".png"
  local url = "https://graph.facebook.com/" .. tostring(fbId) .. "/picture?width=80&height=80"
  if Facebook.mHeadMap and Facebook.mHeadMap[fbId] then
    ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
    if callback then
      callback(fbId, fileName)
    end
    return true
  end
  ext.http.requestDownload({
    url,
    method = "GET",
    localpath = localPath,
    progressCallback = function(percent)
    end,
    callback = function(statusCode, filename, errstr)
      print("downloading filename ", filename)
      if 200 == statusCode and (nil == errstr or "" == errstr) then
        ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
        if not Facebook.mHeadMap then
          Facebook.mHeadMap = {}
        end
        Facebook.mHeadMap[fbId] = fileName
        if callback then
          callback(fbId, fileName)
        end
      else
        DebugOut("Download facebook head icon failed. ")
      end
    end
  })
end
function Facebook:Update()
  if Facebook.mActionStatus ~= FACEBOOK_ACTION_STATUS.DOING and #Facebook.mActionList > 0 then
    local action = Facebook.mActionList[1]
    if action.actionType == FACEBOOK_ACTION_TYPE.LOGIN then
      Facebook:Login(nil, true)
    elseif action.actionType == FACEBOOK_ACTION_TYPE.SHARE_TEXT then
      Facebook:ShareText(action.data.text, action.data.callback)
    elseif action.actionType == FACEBOOK_ACTION_TYPE.SHARE_LINK then
      Facebook:ShareLink(action.data.gameUrl, action.data.title, action.data.subTitle, action.data.describe, action.data.picUrl, action.data.callback)
    elseif action.actionType == FACEBOOK_ACTION_TYPE.GET_FRIEND_LIST then
      Facebook:GetFriendList(action.data.callback)
    elseif action.actionType == FACEBOOK_ACTION_TYPE.GET_INVITEABLE_FRIEND_LIST then
      Facebook:GetInvitableFriendList(action.data.excludeFriends, action.data.callback)
    elseif action.actionType == FACEBOOK_ACTION_TYPE.INVITE_FRIEND then
      Facebook:InviteFriends(action.data.inviteTitle, action.data.inviteMsg, action.data.inviteFriendsList, action.data.callback)
    elseif action.actionType == FACEBOOK_ACTION_TYPE.SEND_GIFT_MSG then
      Facebook:SendGiftMsgToFriend(action.data.inviteTitle, action.data.inviteMsg, action.data.friendIdList, action.data.callback)
    elseif action.actionType == FACEBOOK_ACTION_TYPE.SHARE_STORY then
      Facebook:ShareGraphStory(action.data.objectTypeWithNamespace, action.data.objectType, action.data.ActionType, action.data.storyTitle, action.data.storeDesc, action.data.imgUrl, action.data.callback)
    elseif action.actionType == FACEBOOK_ACTION_TYPE.SHARE_STORY_TEMPER then
      Facebook:ShareGraphStoryTemper(action.data.objectTypeWithNamespace, action.data.objectType, action.data.ActionType, action.data.storyTitle, action.data.storeDesc, action.data.imgUrl, action.data.callback)
    end
    table.remove(Facebook.mActionList, 1)
  end
end
function Facebook:IsSupportFacebook()
  if IPlatformExt.getConfigValue("PlatformName") == "Huawei" then
    return false
  end
  if ext.GetPlatform() == "Win32" then
    return false
  end
  if ext.socialShare.IsSupportFacebook and ext.socialShare.IsSupportFacebook() then
    return true
  end
  return false
end
function Facebook:IsFacebookEnabled()
  if IPlatformExt.getConfigValue("PlatformName") == "Huawei" then
    return false
  end
  local isSysNotSupport = ext.GetPlatform() == "iOS" and GameUtils:GetLeftNumFromOSVersion(ext.GetOSVersion()) and GameUtils:GetLeftNumFromOSVersion(ext.GetOSVersion()) < 6
  return not isSysNotSupport and not GameUtils:IsChinese() and not GameUtils:IsSpaceLancer() and IPlatformExt.getConfigValue("UseExtLogin") ~= "True" and not GameUtils:IsQihooApp()
end
function Facebook:IsFriendFunctionEnabled()
  return self.mUseFriendFunction
end
function Facebook:IsLoginedIn()
  if not Facebook:IsSupportFacebook() then
    return false
  end
  if ext.socialShare and ext.socialShare.FB_IsLogin then
    local islogin = ext.socialShare.FB_IsLogin()
    if not islogin then
      Facebook.mLoginStatus = FACEBOOK_ACTION_STATUS.IDLE
    end
  else
    Facebook.mLoginStatus = FACEBOOK_ACTION_STATUS.IDLE
  end
  return Facebook.mLoginStatus == FACEBOOK_ACTION_STATUS.SUCCESS
end
function Facebook:HasPublishPermission()
