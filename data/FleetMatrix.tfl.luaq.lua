local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameFleetInfoBackground = LuaObjectManager:GetLuaObject("GameFleetInfoBackground")
local lastIndex = 1
FleetMatrix = {}
FleetMatrix.matrixs = {}
FleetMatrix.matrixGetCallback = nil
FleetMatrix.matrixSwitchCallback = nil
FleetMatrix.matrixBuyCallback = nil
FleetMatrix.matrixSaveCallback = nil
FleetMatrix.matrixSystemSwitchCallback = {}
FleetMatrix.blank_num = 1
FleetMatrix.max_blank = 6
FleetMatrix.cur_index = 1
FleetMatrix.switch_index = 1
FleetMatrix.system_index = nil
FleetMatrix.matrixs_purpose = {}
FleetMatrix.fleetinfo = {}
FleetMatrix.MATRIX_TYPE_NONE = 0
FleetMatrix.MATRIX_TYPE_CHAMPION = 1
FleetMatrix.MATRIX_TYPE_BURNING = 2
FleetMatrix.MATRIX_TYPE_BUDO = 3
FleetMatrix.MATRIX_TYPE_WORLD_CHAMPION = 4
FleetMatrix.Matrix_Type = 0
FleetMatrix.cur_type = 0
local switchArg
local isSwitch = false
function FleetMatrix:Init()
  FleetMatrix.matrixs_in_as = {}
end
function FleetMatrix:SetTypeMatrix(type)
  if type ~= FleetMatrix.MATRIX_TYPE_NONE then
    DebugOut("SetTypeMatrix:" .. tostring(type))
    NetMessageMgr:SendMsg(NetAPIList.change_matrix_type_req.Code, {
      matrix = {
        key = type,
        value = self.system_index
      }
    }, FleetMatrix.SetTypeMatrixAck, true)
  end
end
function FleetMatrix.SetTypeMatrixAck(msgType, content)
  if msgType == NetAPIList.change_matrix_type_ack.Code then
    if content.code == 0 then
      FleetMatrix.matrixs_purpose = {}
      for k, v in pairs(content.matrixs_purpose) do
        FleetMatrix.matrixs_purpose[v.key] = v.value
      end
    end
    return true
  end
  return false
end
function FleetMatrix:GetMatrix(type, index)
  if type ~= FleetMatrix.MATRIX_TYPE_NONE then
    local matrixIndex = FleetMatrix.matrixs_purpose[type]
    if matrixIndex then
      return FleetMatrix.matrixs[matrixIndex]
    end
  end
  return FleetMatrix.matrixs[index]
end
function FleetMatrix:getCurrentMartix(type, index)
  return ...
end
function FleetMatrix:getCurrentMartixIndexGot(type, index)
  local matrixIndex = FleetMatrix.matrixs_purpose[type]
  if matrixIndex then
    return FleetMatrix.matrixs[matrixIndex].index
  end
  return 0
end
function FleetMatrix:getCurrentMartixIndex(type, index)
  return FleetMatrix:getCurrentMartix(type, index).index
end
function FleetMatrix:GetMatrixsReq(callback, type)
  FleetMatrix.matrixGetCallback = callback
  FleetMatrix.cur_type = type or FleetMatrix.MATRIX_TYPE_NONE
  local requestFailedCallback
  function requestFailedCallback()
    DebugOut("FleetMatrix:GetMatrixsReq", requestFailedCallback)
    local param = {type = 0}
    if GameStateManager:GetCurrentGameState() == GameStateManager.GameStateFormation then
      param.type = 0
    end
    NetMessageMgr:SendMsg(NetAPIList.mulmatrix_get_req.Code, param, FleetMatrix.GetMatrixsAck, true, requestFailedCallback)
  end
  requestFailedCallback()
end
function FleetMatrix.GetRomaNumber(index)
  if index == 1 then
    return "I"
  elseif index == 2 then
    return "II"
  elseif index == 3 then
    return "III"
  elseif index == 4 then
    return "IV"
  elseif index == 5 then
    return "V"
  elseif index == 6 then
    return "VI"
  end
  if index == 101 then
    return ...
  elseif index == 102 then
    return ...
  elseif index == 103 then
    return ...
  elseif index == 104 then
    return ...
  elseif index == 105 then
    return ...
  elseif index == 106 then
    return ...
  end
end
function FleetMatrix.GetMatrixsAck(msgType, content)
  DebugOut("!!!  ~~ FleetMatrix.GetMatrixsAck")
  if msgType == NetAPIList.mulmatrix_get_ack.Code then
    FleetMatrix:Init()
    FleetMatrix.blank_num = content.blank_num
    FleetMatrix.max_blank = content.max_blank
    FleetMatrix.matrixs = {}
    FleetMatrix.matrixs_purpose = {}
    for k, v in pairs(content.matrixs) do
      FleetMatrix.matrixs[v.index] = v
    end
    table.sort(FleetMatrix.matrixs, function(v1, v2)
      return v1.index < v2.index
    end)
    FleetMatrix.matrixs_in_as = {}
    FleetMatrix.cur_index = content.cur_index
    for k, v in ipairs(FleetMatrix.matrixs) do
      local as_matrix = {}
      as_matrix.id = k
      if v.name == "" then
        v.name = GameLoader:GetGameText("LC_MENU_FORMATION_BUTTON") .. tostring(k)
        v.isChange = true
      end
      as_matrix.diplayName = v.name
      as_matrix.name = FleetMatrix.GetRomaNumber(k) .. ":" .. v.name
      table.insert(FleetMatrix.matrixs_in_as, as_matrix)
    end
    for k, v in pairs(content.matrixs_purpose) do
      FleetMatrix.matrixs_purpose[v.key] = v.value
    end
    FleetMatrix.system_index = FleetMatrix.system_index or FleetMatrix:getCurrentMartixIndex(FleetMatrix.cur_type, FleetMatrix.cur_index)
    FleetMatrix:setCurrentMatrix(FleetMatrix.cur_index)
    if FleetMatrix.matrixGetCallback then
      FleetMatrix.matrixGetCallback()
    end
    return true
  end
  return false
end
function FleetMatrix:getMartixByAsIndex(index)
  return self.matrixs[self.matrixs_in_as[index].id]
end
function FleetMatrix:SwitchMatrixSystem(code, arg, index, callback, failcallback)
  FleetMatrix.matrixSystemSwitchCallback[1] = callback
  FleetMatrix.matrixSystemSwitchCallback[2] = failcallback
  FleetMatrix.switch_index = index
  GameGlobalData.GlobalData.curMatrixIndex = index
  NetMessageMgr:SendMsg(code, arg, FleetMatrix.SwitchMatrixSystemAck, true)
end
function FleetMatrix.SwitchMatrixSystemAck(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.switch_formation_req.Code then
    GameTip:Show(string.gsub(GameLoader:GetGameText("LC_MENU_SAVE_FORMATION_INFO_7"), "<number1>", content.code))
    if FleetMatrix.matrixSystemSwitchCallback[2] then
      FleetMatrix.matrixSystemSwitchCallback[2]()
    end
    return true
  elseif msgType == NetAPIList.switch_formation_ack.Code then
    FleetMatrix.system_index = FleetMatrix.switch_index
    GameGlobalData.GlobalData.fleetinfo.fleets = {}
    GameGlobalData.GlobalData.fleetinfo = content.fleet
    FleetMatrix.SortFleetWithMatrix()
    GameFleetInfoBackground.OnChangeFleetInfo()
    GameGlobalData.GlobalData.fleetkryptons = {}
    GameGlobalData.UpdateFleetKrypton(content)
    if FleetMatrix.matrixSystemSwitchCallback[1] then
      FleetMatrix.matrixSystemSwitchCallback[1]()
    end
    FleetMatrix:setCurrentMatrix(FleetMatrix.system_index)
    if content.code ~= 0 and FleetMatrix.matrixSystemSwitchCallback[2] then
      FleetMatrix.matrixSystemSwitchCallback[2]()
    end
    return true
  end
  return false
end
function FleetMatrix.SortFleetWithMatrix()
  local fleets = GameGlobalData.GlobalData.fleetinfo.fleets
  local fleets_matrix = GameGlobalData:GetData("matrix").cells
  if fleets and fleets_matrix then
    local fleetsInMatrix = {}
    local fleetsNotInMatrix = {}
    for _, fleet in ipairs(fleets) do
      if GameUtils:IsInMatrix(fleet.identity) then
        table.insert(fleetsInMatrix, fleet)
      else
        table.insert(fleetsNotInMatrix, fleet)
      end
    end
    table.sort(fleetsInMatrix, function(a, b)
      return a.identity < b.identity
    end)
    table.sort(fleetsNotInMatrix, function(a, b)
      return a.identity < b.identity
    end)
    fleets = {}
    LuaUtils:table_imerge(fleets, fleetsInMatrix)
    LuaUtils:table_imerge(fleets, fleetsNotInMatrix)
    GameGlobalData.GlobalData.fleetinfo.fleets = fleets
  end
end
function FleetMatrix:GetActiviteSpell(commander_id)
  local curMatrix = FleetMatrix.matrixs[FleetMatrix.cur_index]
  DebugOut("curMatrix")
  DebugTable(curMatrix)
  for k, v in pairs(curMatrix.cells) do
    if v.fleet_identity == commander_id then
      return v.fleet_spell
    end
  end
end
function FleetMatrix:SetCurrentSkill(commander_id, skill_id)
  local curMatrix = FleetMatrix.matrixs[FleetMatrix.cur_index]
  for k, v in pairs(curMatrix.cells) do
    if v.fleet_identity == commander_id then
      v.fleet_spell = skill_id
    end
  end
  curMatrix.isChange = true
end
function FleetMatrix:setCurrentMatrix(index)
  local matrix = self:getCurrentMartix(FleetMatrix.cur_type, index)
  GameGlobalData.GlobalData.matrix.cells = matrix.cells
  GameGlobalData.GlobalData.matrix.id = matrix.index
  GameGlobalData.GlobalData.matrix.name = matrix.name
end
function FleetMatrix:SwitchMatrixClient(index, callback)
  FleetMatrix.system_index = index
  FleetMatrix:setCurrentMatrix(FleetMatrix.system_index)
  callback()
end
function FleetMatrix:SwitchMatrixReq(index, callback, switch, arg)
  isSwitch = switch
  FleetMatrix.switch_index = index
  switchArg = arg
  FleetMatrix.matrixSwitchCallback = callback
  GameGlobalData.GlobalData.curMatrixIndex = index
  NetMessageMgr:SendMsg(NetAPIList.mulmatrix_switch_req.Code, {id = index, switch = switch}, FleetMatrix.SwitchMatrixAck, true, nil)
end
function FleetMatrix.SwitchMatrixAck(msgType, content)
  if msgType == NetAPIList.mulmatrix_switch_ack.Code then
    if isSwitch then
      FleetMatrix.cur_index = FleetMatrix.switch_index
    else
      FleetMatrix.system_index = FleetMatrix.switch_index
    end
    FleetMatrix:setCurrentMatrix(FleetMatrix.switch_index)
    if FleetMatrix.matrixSwitchCallback then
      FleetMatrix.matrixSwitchCallback(switchArg)
    end
    return true
  end
  return false
end
function FleetMatrix:RenameMatrix(newName)
  local matrix = FleetMatrix.matrixs[FleetMatrix.cur_index]
  matrix.name = newName
  matrix.isChange = true
end
function FleetMatrix:CheckMatrixChanged()
  local martix = FleetMatrix.matrixs[FleetMatrix.cur_index]
  return martix and martix.isChange
end
function FleetMatrix:SaveMatrix(matrix)
  NetMessageMgr:SendMsg(NetAPIList.mulmatrix_save_req.Code, matrix, nil, false)
  matrix.isChange = false
end
function FleetMatrix:SaveMatrixChanged(isNeedBlock, callback)
  FleetMatrix.matrixSaveCallback = callback
  if FleetMatrix:CheckMatrixChanged() then
    local curMatrix = FleetMatrix.matrixs[FleetMatrix.cur_index]
    curMatrix.isChange = false
    DebugOutPutTable(curMatrix.cells, "saveCurrentMatrix")
    NetMessageMgr:SendMsg(NetAPIList.mulmatrix_save_req.Code, curMatrix, FleetMatrix.SaveMatrixAck, isNeedBlock)
  elseif callback then
    callback()
  end
end
function FleetMatrix.SaveMatrixAck(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.mulmatrix_save_req.Code then
    if content.code ~= 0 then
      local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    elseif FleetMatrix.matrixSaveCallback then
      FleetMatrix.matrixSaveCallback()
    end
    return true
  end
  return false
end
function FleetMatrix:BuyMatrixDialogReq(callback, lastIndex)
  FleetMatrix.matrixBuyCallback = callback
  NetMessageMgr:SendMsg(NetAPIList.mulmatrix_price_req.Code, nil, FleetMatrix.GetMatrixPriceAck, true)
end
function FleetMatrix.GetMatrixPriceAck(msgType, content)
  if msgType == NetAPIList.mulmatrix_price_ack.Code then
    DebugOutPutTable(content, "mulmatrix_price_ack")
    local lastLevelInfo = GameGlobalData:GetLastLevelInfo()
    DebugOut("level ", lastLevelInfo.level)
    local res = GameGlobalData:GetData("resource")
    local prestigeLevel = res.prestige
    DebugOut("prestigeLevel", prestigeLevel)
    local isMeetPrestigeCondition = true
    local isMeetLevelCondition = true
    local tips = ""
    for i, v in pairs(content.conditions) do
      if v.type == "prestige_level" and v.value > 0 then
        DebugOut("prestige_level require:", v.value)
        if prestigeLevel < v.value then
          isMeetPrestigeCondition = false
          local prestige_name = GameDataAccessHelper:GetMilitaryRankName(v.value)
          tips = tips .. string.format(GameLoader:GetGameText("LC_MENU_FORMATION_LIMIT_PRESTIGE"), prestige_name)
          tips = tips .. " "
        end
      elseif v.type == "level" and v.value > 0 then
        DebugOut("level require:", v.value)
        if lastLevelInfo.level < v.value then
          isMeetLevelCondition = false
          tips = tips .. string.format(GameLoader:GetGameText("LC_MENU_FORMATION_LIMIT_LEVEL"), v.value)
        end
      end
    end
    DebugOut("isMeetCondition", isMeetPrestigeCondition, isMeetLevelCondition, content.credit_cost, content.money_cost)
    if isMeetLevelCondition and isMeetPrestigeCondition then
      local info = ""
      if 0 < content.credit_cost then
        info = info .. string.format(GameLoader:GetGameText("LC_MENU_FORMATION_PAY_CREDIT"), content.credit_cost)
        info = info .. " "
      end
      if 0 < content.money_cost then
        info = info .. string.format(GameLoader:GetGameText("LC_MENU_FORMATION_PAY_MONEY"), content.money_cost)
      end
      local callback = function()
        FleetMatrix:BuyMatrixReq()
      end
      local cancal = function()
        FleetMatrix.matrixBuyCallback(false)
      end
      local text_title = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
      GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
      GameUIMessageDialog:SetYesButton(callback)
      GameUIMessageDialog:SetNoButton(cancal)
      GameUIMessageDialog:Display(text_title, info)
    else
      FleetMatrix.matrixBuyCallback(false)
      GameTip:Show(tips)
    end
    return true
  end
  return false
end
function FleetMatrix:BuyMatrixReq()
  NetMessageMgr:SendMsg(NetAPIList.mulmatrix_buy_req.Code, nil, FleetMatrix.BuyMatrixAck, true, nil)
end
function FleetMatrix.BuyMatrixAck(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.mulmatrix_buy_req.Code then
    if content.code ~= 0 then
      if content.code == 1000014 then
        local GameVip = LuaObjectManager:GetLuaObject("GameVip")
        GameVip:CheckIsNeedShowVip(content.code)
      elseif content.code == 1000016 then
        local text = AlertDataList:GetTextFromErrorCode(1000016)
        local GameUIKrypton = LuaObjectManager:GetLuaObject("GameUIKrypton")
        GameUIKrypton.NeedMoreMoney(text)
      end
      FleetMatrix.matrixBuyCallback(false)
    else
      FleetMatrix.blank_num = FleetMatrix.blank_num + 1
      FleetMatrix.matrixs[FleetMatrix.blank_num] = LuaUtils:table_rcopy(FleetMatrix.matrixs[1])
      FleetMatrix.matrixs[FleetMatrix.blank_num].isChange = true
      FleetMatrix.matrixs[FleetMatrix.blank_num].type = 0
      FleetMatrix.matrixs[FleetMatrix.blank_num].index = FleetMatrix.blank_num
      FleetMatrix.matrixs[FleetMatrix.blank_num].name = GameLoader:GetGameText("LC_MENU_FORMATION_BUTTON") .. FleetMatrix.blank_num
      FleetMatrix:SwitchMatrixReq(FleetMatrix.blank_num, FleetMatrix.matrixBuyCallback, true, true)
    end
    return true
  end
  return false
end
function FleetMatrix:SetMatrix(position, cell)
  DebugOut("SetMatrix:" .. position .. "=>" .. cell)
  local cells = FleetMatrix.matrixs[self.cur_index].cells
  cells[position].fleet_identity = cell
  FleetMatrix.matrixs[self.cur_index].isChange = true
end
function FleetMatrix:SwapMatrix(oldPosition, newPosition)
  DebugOut("SwapMatrix:" .. oldPosition .. "==>" .. newPosition)
  local cells = FleetMatrix.matrixs[self.cur_index].cells
  local tmp_fleet_identity = cells[oldPosition].fleet_identity
  local tmp_fleet_spell = cells[oldPosition].fleet_spell
  cells[oldPosition].fleet_identity = cells[newPosition].fleet_identity
  cells[oldPosition].fleet_spell = cells[newPosition].fleet_spell
  cells[newPosition].fleet_identity = tmp_fleet_identity
  cells[newPosition].fleet_spell = tmp_fleet_spell
  FleetMatrix.matrixs[self.cur_index].isChange = true
end
