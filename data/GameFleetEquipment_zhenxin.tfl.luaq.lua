local GameFleetEquipment = LuaObjectManager:GetLuaObject("GameFleetEquipment")
GameFleetEquipment.zhenxin = {}
local zhenxin = GameFleetEquipment.zhenxin
local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
local GameUIAdjutant = LuaObjectManager:GetLuaObject("GameUIAdjutant")
local GameUIMaster = LuaObjectManager:GetLuaObject("GameUIMaster")
local GameFleetEquipmentPop = LuaObjectManager:GetLuaObject("GameFleetEquipmentPop")
local GameStateFormation = GameStateManager.GameStateFormation
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local TutorialAdjutantManager = LuaObjectManager:GetLuaObject("TutorialAdjutantManager")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local GameStateDaily = GameStateManager.GameStateDaily
require("FleetMatrix.tfl")
zhenxin.CurSelectFleet = 1
local CurMatrixIndex = GameGlobalData.GlobalData.curMatrixIndex
local isRequestMatrixInfo = false
local FormationFleets = {}
local QuestTutorialBattleMapAfterRecruit = TutorialQuestManager.QuestTutorialBattleMapAfterRecruit
zhenxin.battle_matrix = {}
zhenxin.AdjutantButtoState = {
  unlocked = 0,
  locked = 1,
  can = 2,
  have = 3
}
local needChangeCurSelectShip = true
function zhenxin:OnAddToGameState()
  GameGlobalData:RegisterDataChangeCallback("fleetinfo", zhenxin.OnEnhanceFleetinfoChange)
  GameGlobalData:RegisterDataChangeCallback("matrix", zhenxin.matrixUpdate)
  GameGlobalData:RegisterDataChangeCallback("leaderlist", zhenxin.OnEnhanceFleetinfoChange)
  DebugOut("zhenxin:~~~~~~~~~~~~~~~~~~~~~~~~~OnAddToGameState")
end
function zhenxin:OnEraseFromGameState()
  zhenxin.CurSelectFleet = 1
  zhenxin.newSelectedFleetID = nil
  GameGlobalData:RemoveDataChangeCallback("fleetinfo", zhenxin.OnEnhanceFleetinfoChange)
  GameGlobalData:RemoveDataChangeCallback("matrix", zhenxin.matrixUpdate)
  GameGlobalData:RemoveDataChangeCallback("leaderlist", zhenxin.OnEnhanceFleetinfoChange)
  DebugOut("zhenxin:~~~~~~~~~~~~~~~~~~~~~~~~~OnEraseFromGameState")
  isRequestMatrixInfo = false
end
function zhenxin:OnInitGame()
  DebugOut("zhenxin:~~~~~~~~~~~~~~~~~~~~~~~~~ OnInitGame")
  GameGlobalData:RegisterDataChangeCallback("fleetinfo", zhenxin.OnEnhanceFleetinfoChange)
  GameGlobalData:RegisterDataChangeCallback("matrix", zhenxin.matrixUpdate)
  GameGlobalData:RegisterDataChangeCallback("leaderlist", zhenxin.OnEnhanceFleetinfoChange)
end
function zhenxin:GetFlashObject()
  return (...), GameFleetEquipment
end
function zhenxin:OnFSCommand(cmd, arg)
  if cmd == "switchFormation" then
    local GameStatePlayerMatrix = GameStateManager.GameStatePlayerMatrix
    if GameStatePlayerMatrix.basePrveState == nil and GameStateManager.GameStateEquipEnhance.basePrveState == nil then
      GameStateManager.GameStateEquipEnhance.basePrveState = GameStateManager.GameStateEquipEnhance.previousState
    end
    GameStateManager:SetCurrentGameState(GameStatePlayerMatrix)
    return true
  elseif cmd == "FormationFleetClicked" then
    local param = LuaUtils:string_split(arg, "\001")
    local identity = tonumber(param[1])
    local idx = tonumber(param[2])
    if identity == 0 then
    elseif identity > 0 then
      self.CurSelectFleet = idx
      self:OnFleetSelected()
    else
      local tip = GameLoader:GetGameText("LC_ALERT_fleet_count_over_max_new")
      local function okCallback()
        GameHelper:SetEnterPage(GameHelper.PagePrestige)
        GameStateManager:SetCurrentGameState(GameStateDaily)
      end
      local cancelCallback = function()
      end
      DebugOut("GameUIGlobalScreen3")
      GameUIGlobalScreen:ShowMessageBox(2, "", tip, okCallback, cancelCallback)
    end
    return true
  elseif cmd == "AdjutantClicked" then
    local fleetId, index = self:GetCurFleetContent().identity, nil
    GameUIAdjutant.mFleetIdBeforeEnter = fleetId
    GameUIAdjutant:EnterAdjutantUI()
    GameUIAdjutant.IsShowAdjutantUI = true
    return true
  elseif cmd == "showFleetPopView" then
    local fleet, index = self:GetCurFleetContent()
    local leaderlist = GameGlobalData:GetData("leaderlist")
    local curMatrixIndex = GameGlobalData.GlobalData.curMatrixIndex
    if leaderlist and curMatrixIndex and fleet.identity == 1 then
      ItemBox:ShowCommanderDetail2(leaderlist.leader_ids[curMatrixIndex], fleet, 1)
    else
      ItemBox:ShowCommanderDetail2(fleet.identity, fleet, 1)
    end
    return true
  elseif cmd == "GotoMasterPage" then
    local fleet, index = self:GetCurFleetContent()
    if fleet and fleet.identity then
      GameUIMaster.CurrentScope = {
        [1] = fleet.id
      }
      GameUIMaster.CurrentSystemType = GameUIMaster.MasterSystem.EquipMaster
      GameUIMaster.CurrentPageType = GameUIMaster.MasterPage.equip_enchance
      GameUIMaster.CurMatrixId = CurMatrixIndex
      GameStateManager:GetCurrentGameState():AddObject(GameUIMaster)
    end
    return true
  else
    if cmd == "changeShip" then
      zhenxin:OnChangeShip()
    else
    end
  end
  return false
end
function zhenxin.GetMatrixsAck()
  zhenxin.battle_matrix = {}
  zhenxin.validFleetSlot = GameGlobalData:GetData("matrix").count
  for _, v in ipairs(GameGlobalData:GetData("matrix").cells) do
    zhenxin.battle_matrix[v.cell_id] = v.fleet_identity
  end
  CurMatrixIndex = GameGlobalData:GetData("matrix").id
  DebugOutPutTable(GameGlobalData:GetData("matrix"), "GetMatrixsAck")
  isRequestMatrixInfo = true
  if GameFleetEquipment.currentSelect == "xunzhang" or GameFleetEquipment.currentSelect == "zhuangbei" then
    zhenxin:InitMenu()
  end
end
function sortZhenxinFleet(a, b)
  if a.commander_vesselstype == "TYPE_6" and b.commander_vesselstype ~= "TYPE_6" then
    return true
  elseif a.commander_vesselstype ~= "TYPE_6" and b.commander_vesselstype == "TYPE_6" then
    return false
  else
    return a.force > b.force
  end
end
function zhenxin:InitMenu()
  DebugOut("zhenxin:InitMenu()", isRequestMatrixInfo)
  if not isRequestMatrixInfo then
    FleetMatrix:GetMatrixsReq(zhenxin.GetMatrixsAck, FleetMatrix.Matrix_Type)
  else
    self:CheckFormationShow()
    FormationFleets = {}
    for k, v in pairs(zhenxin.battle_matrix) do
      local fleetDetail = {}
      if v > 0 then
        local commander_data = GameGlobalData:GetFleetInfo(v)
        local tempid = v
        local commander_level = 0
        if v == 1 then
          DebugOut("CurMatrixIndex", CurMatrixIndex)
          tempid = GameGlobalData:GetData("leaderlist").leader_ids[CurMatrixIndex] or 1
        end
        if commander_data == nil then
          if tempid == 1 then
            commander_data = {}
            commander_data.identity = 1
          end
          DebugOut("error:id_commander")
          return
        else
          commander_level = commander_data.level
        end
        local ability = GameDataAccessHelper:GetCommanderAbility(tempid, commander_level)
        fleetDetail.commander_identity = commander_data.identity
        fleetDetail.commanderType = GameGlobalData:GetCommanderTypeWithIdentity(tempid)
        fleetDetail.commander_avatar = GameDataAccessHelper:GetFleetAvatar(tempid, commander_level)
        fleetDetail.commander_vessels = GameDataAccessHelper:GetCommanderVesselsImage(tempid, commander_level, false)
        fleetDetail.commander_vesselstype = GameDataAccessHelper:GetCommanderVesselsType(tempid, commander_level)
        fleetDetail.commander_color = GameDataAccessHelper:GetCommanderColorFrame(tempid, commander_level)
        fleetDetail.commander_sex = GameDataAccessHelper:GetCommanderSex(tempid)
        fleetDetail.commander_rank = GameUtils:GetFleetRankFrame(ability.Rank, zhenxin.DownloadRankImageCallback, #FormationFleets + 1)
        fleetDetail.force = commander_data.force
        table.insert(FormationFleets, fleetDetail)
      end
    end
    table.sort(FormationFleets, sortZhenxinFleet)
    for i = 1, 5 do
      if not FormationFleets[i] then
        if i <= self.validFleetSlot or i == 2 then
          local fleetDetail = {}
          fleetDetail.commander_identity = 0
          fleetDetail.force = 0
          FormationFleets[i] = fleetDetail
        else
          local fleetDetail = {}
          fleetDetail.commander_identity = -1
          fleetDetail.force = -1
          FormationFleets[i] = fleetDetail
        end
      end
    end
    if zhenxin.newSelectedFleetID then
      for k, v in ipairs(FormationFleets) do
        if v.commander_identity == zhenxin.newSelectedFleetID then
          zhenxin.CurSelectFleet = k
          zhenxin.newSelectedFleetID = nil
          break
        end
      end
    end
    local flashObj = self:GetFlashObject()
    if flashObj then
      DebugOutPutTable(FormationFleets, "FormationFleetsxx:" .. self.CurSelectFleet)
      flashObj:InvokeASCallback("_root", "SetFormationFleetInfo", FormationFleets, self.CurSelectFleet)
      self:SetFleetDetailMenu()
    end
  end
end
function zhenxin:GetCurFleetContent()
  if FormationFleets == nil or FormationFleets[self.CurSelectFleet] == nil then
    return
  end
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  local content, idx
  for k, v in ipairs(fleets) do
    if v.identity == FormationFleets[self.CurSelectFleet].commander_identity then
      content = v
      idx = k
      break
    end
  end
  return content
end
function zhenxin:SetFleetDetailMenu()
  local content, index = self:GetCurFleetContent()
  local leaderlist = GameGlobalData:GetData("leaderlist")
  local CurMatrixIndex = GameGlobalData.GlobalData.curMatrixIndex
  DebugOut("RefreshFleetAvatar:" .. content.identity)
  if not content then
    return
  end
  TutorialAdjutantManager:StartTutorialAdjutantBind_NewEnhance(content.identity)
  local iconFrame = GameDataAccessHelper:GetFleetAvatar(content.identity, content.level)
  local fleetName = GameDataAccessHelper:GetFleetLevelDisplayName(content.identity, content.level)
  local level = GameLoader:GetGameText("LC_MENU_Level") .. GameGlobalData:GetData("levelinfo").level
  local vessleType = GameDataAccessHelper:GetCommanderVesselsType(content.identity, content.level)
  local force = GameUtils.numberConversion(content.force)
  local fleetID = content.identity
  local fleetIcon = GameDataAccessHelper:GetShip(content.identity, nil, content.level)
  local fleetColor = GameDataAccessHelper:GetCommanderColorFrame(fleetID, content.level)
  local forceTitle = GameLoader:GetGameText("LC_MENU_FORCE_CHAR")
  local basic_info = GameDataAccessHelper:GetCommanderBasicInfo(fleetID, content.level)
  local skill = basic_info.SPELL_ID
  local skillRangeType = "TYPE_" .. GameDataAccessHelper:GetSkillRangeType(skill)
  local ability = GameDataAccessHelper:GetCommanderAbility(fleetID, content.level)
  local fleetRank = GameUtils:GetFleetRankFrame(ability.Rank, zhenxin.UpdateRankImageDownLoadCallback)
  local sex = GameDataAccessHelper:GetCommanderSex(fleetID)
  if content.identity == 1 and leaderlist and CurMatrixIndex then
    local tempid = leaderlist.leader_ids[CurMatrixIndex]
    iconFrame = GameDataAccessHelper:GetFleetAvatar(tempid, content.level)
    fleetName = GameDataAccessHelper:GetFleetLevelDisplayName(tempid, content.level)
    vessleType = GameDataAccessHelper:GetCommanderVesselsType(tempid, content.level)
    fleetColor = GameDataAccessHelper:GetCommanderColorFrame(tempid, content.level)
    basic_info = GameDataAccessHelper:GetCommanderBasicInfo(tempid, content.level)
    skill = basic_info.SPELL_ID
    skillRangeType = "TYPE_" .. GameDataAccessHelper:GetSkillRangeType(skill)
    ability = GameDataAccessHelper:GetCommanderAbility(tempid, content.level)
    fleetRank = GameUtils:GetFleetRankFrame(ability.Rank, zhenxin.UpdateRankImageDownLoadCallback)
    sex = GameDataAccessHelper:GetCommanderSex(tempid)
    fleetIcon = GameDataAccessHelper:GetShip(leaderlist.leader_ids[CurMatrixIndex], nil, content.level)
  end
  if sex == 1 then
    sex = "man"
  elseif sex == 0 then
    sex = "woman"
  else
    sex = "unknown"
  end
  DebugOut("SetFleetAvatar:")
  DebugOut(fleetIcon, fleetName, iconFrame, level, vessleType, forceTitle, force, fleetID, fleetColor, skillRangeType, fleetRank)
  self:GetFlashObject():InvokeASCallback("_root", "SetFleetAvatar", fleetName, iconFrame, level, vessleType, forceTitle, force, fleetID, fleetColor, skillRangeType, fleetRank, sex, fleetIcon)
  self:SetAdjutant(content)
  if GameFleetEquipment.currentSelect == "zhuangbei" or GameFleetEquipment.currentSelect == "xunzhang" or GameFleetEquipment.currentSelect == "kejin" then
    self:GetFlashObject():InvokeASCallback("_root", "ShowShipDetailInfoMenu")
  end
  if GameFleetEquipment.currentSelect == "zhuangbei" or GameFleetEquipment.currentSelect == "xunzhang" then
    self:GetFlashObject():InvokeASCallback("_root", "ShowDetailFleet")
  end
end
function zhenxin:SetAdjutant(content)
  local adjutantIconFrame = ""
  local adjutantRank = ""
  local curAdjutantButton = self:GetAdjutantButtonState(content)
  if curAdjutantButton == zhenxin.AdjutantButtoState.have then
    local adjutant = self:GetAdjutant(content.cur_adjutant)
    DebugOutPutTable(adjutant, "SetAdjutantfsf")
    if adjutant then
      adjutantIconFrame = GameDataAccessHelper:GetFleetAvatar(adjutant.identity, adjutant.level)
      local ability = GameDataAccessHelper:GetCommanderAbility(adjutant.identity, adjutant.level)
      adjutantRank = GameUtils:GetFleetRankFrame(ability.Rank, zhenxin.DownloadAdjuantRankImageCallback)
    end
  end
  self:GetFlashObject():InvokeASCallback("_root", "setAdjutantButtonState", curAdjutantButton, adjutantIconFrame, adjutantRank)
  if self:FleetInMatrix(content.identity) then
    DebugOut("enheng?", curAdjutantButton)
    self:GetFlashObject():InvokeASCallback("_root", "setAdjutantRedpoint", curAdjutantButton == zhenxin.AdjutantButtoState.can)
  else
    self:GetFlashObject():InvokeASCallback("_root", "setAdjutantRedpoint", false)
  end
end
function zhenxin:SetAdjutantRedpoint(isShow)
  local flashObj = self:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "setAdjutantRedpoint", false)
  end
end
function zhenxin:GetAdjutant(identity)
  local adjutant
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  DebugOutPutTable(zhenxin.DismissFleet, "zhenxin:GetAdjutant( " .. identity)
  for _, v in pairs(fleets) do
    if v.identity == identity then
      adjutant = v
      break
    end
  end
  for _, v in pairs(zhenxin.DismissFleet) do
    if v.identity == identity then
      adjutant = v
      break
    end
  end
  if adjutant == nil then
    DebugOut("error: adjutant is nil")
  end
  return adjutant
end
function zhenxin:GetAdjutantButtonState(content)
  local currentState = ""
  local playerLelevinfo = GameGlobalData:GetData("levelinfo")
  if playerLelevinfo.level < 45 then
    currentState = zhenxin.AdjutantButtoState.unlocked
    return currentState
  end
  if content.cur_adjutant > 1 and zhenxin:GetAdjutant(content.cur_adjutant) then
    currentState = zhenxin.AdjutantButtoState.have
  else
    if #content.can_adjutant > 0 and zhenxin:GetAdjutantStateFr(content.can_adjutant) then
      currentState = zhenxin.AdjutantButtoState.can
    else
      currentState = zhenxin.AdjutantButtoState.locked
    end
    if zhenxin:GetMajorId(content.identity) then
      currentState = zhenxin.AdjutantButtoState.locked
    end
  end
  DebugOut("GetAdjutantButtonState = " .. currentState)
  return currentState
end
function zhenxin:GetAdjutantStateFr(adjutants)
  local matrixCells = GameGlobalData:GetData("matrix").cells
  DebugOut("GetAdjutantStateFr = ")
  DebugTable(adjutants)
  DebugTable(matrixCells)
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  if not zhenxin.DismissFleet then
    zhenxin:GetDismissFleet()
    return nil
  end
  if matrixCells and #matrixCells > 0 then
    for _, v in ipairs(adjutants) do
      local stateMatrix = false
      local stateAdjutantsByother = false
      local stateHaveAdjutant = false
      for _, m in ipairs(matrixCells) do
        if v == m.fleet_identity then
          stateMatrix = true
        end
      end
      for _, ajutant in ipairs(zhenxin.DismissFleet) do
        if ajutant.cur_adjutant == v then
          stateAdjutantsByother = true
        end
        if ajutant.identity == v and ajutant.cur_adjutant > 1 then
          stateHaveAdjutant = true
        end
      end
      for _, ajutant in ipairs(fleets) do
        if ajutant.cur_adjutant == v then
          stateAdjutantsByother = true
        end
        if ajutant.identity == v and ajutant.cur_adjutant > 1 then
          stateHaveAdjutant = true
        end
      end
      if not stateMatrix and not stateAdjutantsByother and not stateHaveAdjutant then
        return true
      end
    end
  end
  return false
end
function zhenxin:GetMajorId(identity)
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  for _, v in ipairs(fleets) do
    if v.cur_adjutant == identity then
      return true
    end
  end
  return false
end
function zhenxin:FleetInMatrix(fleetid)
  local matrix = GameGlobalData:GetData("matrix").cells
  DebugOut("matrix::")
  DebugTable(matrix)
  for k, v in pairs(matrix or {}) do
    if v.fleet_identity == fleetid then
      return true
    end
  end
  return false
end
function zhenxin:GetDismissFleet()
  zhenxin.DismissFleet = {}
  NetMessageMgr:SendMsg(NetAPIList.fleet_dismiss_req.Code, nil, zhenxin.AllFleetDataCallback, true, nil)
end
function zhenxin.AllFleetDataCallback(msgType, content)
  if NetAPIList.fleet_dismiss_ack.Code == msgType then
    DebugOut("AllFleetDataCallback:")
    zhenxin.DismissFleet = content.fleets
    if #FormationFleets > 0 then
      local fleet, index = zhenxin:GetCurFleetContent()
      zhenxin:UpdateAdjutantState(fleet.identity, fleet.cur_adjutant, fleet)
    end
    return true
  end
  return false
end
function zhenxin:UpdateAdjutantState(fleetid, curAdjutant, content)
  local adjutantIconFrame = ""
  local adjutantRank = ""
  local curAdjutantButton = self:GetAdjutantButtonState(content)
  if curAdjutantButton == zhenxin.AdjutantButtoState.have then
    local adjutant = self:GetAdjutant(curAdjutant)
    if adjutant then
      adjutantIconFrame = GameDataAccessHelper:GetFleetAvatar(adjutant.identity, adjutant.level)
      local ability = GameDataAccessHelper:GetCommanderAbility(adjutant.identity, adjutant.level)
      adjutantRank = GameUtils:GetFleetRankFrame(ability.Rank, zhenxin.DownloadRankImageCallback)
    end
  end
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "setAdjutantButtonState", curAdjutantButton, adjutantIconFrame, adjutantRank)
    if self:FleetInMatrix(fleetid) then
      DebugOut("enheng?", curAdjutantButton)
      self:GetFlashObject():InvokeASCallback("_root", "setAdjutantRedpoint", curAdjutantButton == zhenxin.AdjutantButtoState.can and not self:IsMaxAdjutantNow())
    else
      self:GetFlashObject():InvokeASCallback("_root", "setAdjutantRedpoint", false)
    end
  end
end
function zhenxin:IsMaxAdjutantNow()
  local adjutantUnlockCnt = GameGlobalData:GetData("adjutant_max_count")
  local nowAdjutantNum = 0
  local fleet_info = GameGlobalData:GetData("fleetinfo").fleets
  for k, v in ipairs(fleet_info) do
    if GameUtils:IsInMatrix(v.identity) and 0 < v.cur_adjutant then
      nowAdjutantNum = nowAdjutantNum + 1
    end
  end
  DebugOut("IsMaxAdjutantNow", nowAdjutantNum, adjutantUnlockCnt)
  return nowAdjutantNum == adjutantUnlockCnt
end
function zhenxin:OnFleetSelected()
  local fleetInfo = GameFleetEquipment.zhenxin:GetCurFleetContent()
  GameFleetEquipment.currentforce = fleetInfo.force
  local fleet_info = GameGlobalData:GetData("fleetinfo").fleets
  local fleetidx
  for k, v in ipairs(fleet_info) do
    if v.identity == fleetInfo.identity then
      fleetidx = k
      break
    end
  end
  assert(fleetidx, "must have " .. fleetInfo.identity)
  self:SetFleetDetailMenu()
  if not GameFleetEquipment.currentSelect or GameFleetEquipment.currentSelect == "zhuangbei" then
    GameFleetEquipment.zhuangbei:Init(fleetInfo, fleetidx, true)
    GameFleetEquipment.currentSelect = "zhuangbei"
  elseif GameFleetEquipment.currentSelect == "kejin" then
    self:GetFlashObject():InvokeASCallback("_root", "enterKrypton")
    GameFleetEquipment.kejin.RefreshKrypton(fleetidx)
  elseif GameFleetEquipment.currentSelect == "shenjie" then
    GameFleetEquipment.shenjie:Init(fleetInfo, fleetidx)
  elseif GameFleetEquipment.currentSelect == "xunzhang" then
    GameFleetEquipment:OnFSCommand("ShowMedalMenu")
  elseif GameFleetEquipment.currentSelect == "chuancang" then
    GameFleetEquipment:enterArtifactByID(fleetInfo)
  end
  if GameFleetEquipment.currentSelect ~= "zhuangbei" then
    GameFleetEquipment.zhuangbei:Init(fleetInfo, fleetidx, false)
  end
  GameFleetEquipment.shenjie:UpdateGrowUpState(fleetInfo.identity)
  if GameFleetEquipment.currentSelect == "kejin" or GameFleetEquipment.currentSelect == "chuancang" then
    GameFleetEquipment:RefreshKryptonRedPoint()
  end
  GameFleetEquipment.xunzhang:SetMedalButtonNews()
end
function zhenxin:OnChangeFleet(newfleetID)
  local fleetDetail, index = self:GetCurFleetContent()
  local fleetId = fleetDetail.identity
  needChangeCurSelectShip = false
  if fleetId == 1 then
    local param = {}
    param.id = newfleetID
    NetMessageMgr:SendMsg(NetAPIList.change_leader_req.Code, param, zhenxin.ChangeFleetCallBack, false)
  else
    zhenxin.newSelectedFleetID = newfleetID
    zhenxin.activeNewSelectedFleet = false
    local param = {}
    param.matrix_index = CurMatrixIndex
    param.old_fleet_id = fleetId
    param.new_fleet_id = newfleetID
    NetMessageMgr:SendMsg(NetAPIList.change_fleets_req.Code, param, zhenxin.ChangeFleetCallBack, true, nil)
  end
end
function zhenxin.ChangeFleetCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and (content.api == NetAPIList.change_fleets_req.Code or content.api == NetAPIList.change_leader_req.Code) then
    if content.code ~= 0 then
      zhenxin.newSelectedFleetID = nil
      zhenxin.activeNewSelectedFleet = false
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  return false
end
function zhenxin.matrixUpdate()
  DebugOut("zhenxin.matrixUpdate")
  zhenxin:GetDismissFleet()
  CurMatrixIndex = GameGlobalData.GlobalData.curMatrixIndex
  if needChangeCurSelectShip then
    zhenxin.CurSelectFleet = 1
  end
  zhenxin.GetMatrixsAck()
  needChangeCurSelectShip = true
  if not zhenxin:GetFlashObject() then
    return
  end
  zhenxin:OnFleetSelected()
  GameFleetEquipment.kejin:RequestKryptonInBag()
  GameFleetEquipment.zhuangbei:UpdateSelectedFleetInfo()
end
function zhenxin.OnEnhanceFleetinfoChange()
  DebugOut("zhenxin.OnEnhanceFleetinfoChange ===============")
  if zhenxin:GetFlashObject() then
    zhenxin.newSelectedFleetID = FormationFleets[zhenxin.CurSelectFleet].commander_identity
    zhenxin:InitMenu()
    zhenxin:GetDismissFleet()
  end
end
function zhenxin:OnChangeShip()
  local content, index = self:GetCurFleetContent()
  local fleetId = content.identity
  local param = {}
  param.matrix_index = CurMatrixIndex
  param.fleet_id = fleetId
  NetMessageMgr:SendMsg(NetAPIList.change_fleets_data_req.Code, param, zhenxin.ChangeFleetDataCallBack, true, nil)
end
function zhenxin:CheckFormationShow()
  local rightMenu = GameGlobalData:GetData("modules_status").modules
  local unlock = false
  for k, v in pairs(rightMenu) do
    if v.name == "hire" then
      unlock = v.status
      break
    end
  end
  local flashObj = self:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "setFormationShow", unlock)
  end
end
function zhenxin.ChangeFleetDataCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.change_fleets_data_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.change_fleets_data_ack.Code then
    GameFleetEquipmentPop:zhenxin_showChangeFleetPop(content)
    return true
  end
  return false
end
function zhenxin:UpdateRankImageDownLoadCallback(extInfo)
  if zhenxin:GetFlashObject() and extInfo then
    zhenxin:GetFlashObject():InvokeASCallback("_root", "updateFleetRankImage", extInfo.rank_id)
  end
end
function zhenxin.DownloadRankImageCallback(extInfo)
  if zhenxin:GetFlashObject() and extInfo then
    zhenxin:GetFlashObject():InvokeASCallback("_root", "updateRankImage", extInfo.rank_id, extInfo.id)
  end
end
function zhenxin.DownloadAdjuantRankImageCallback(extInfo)
  if zhenxin:GetFlashObject() and extInfo then
    zhenxin:GetFlashObject():InvokeASCallback("_root", "updateAdjutantRankImage", extInfo.rank_id)
  end
end
