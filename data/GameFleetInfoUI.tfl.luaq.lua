local GameStateFleetInfo = GameStateManager.GameStateFleetInfo
local GameStateEquipEnhance = GameStateManager.GameStateEquipEnhance
local GameFleetInfoUI = LuaObjectManager:GetLuaObject("GameFleetInfoUI")
local GameFleetInfoBackground = LuaObjectManager:GetLuaObject("GameFleetInfoBackground")
local GameFleetInfoBag = LuaObjectManager:GetLuaObject("GameFleetInfoBag")
local GameFleetInfoTabBar = LuaObjectManager:GetLuaObject("GameFleetInfoTabBar")
local NetMessageMgr = NetMessageMgr
local NetAPIList = NetAPIList
local AlertDataList = AlertDataList
local GameGlobalData = GameGlobalData
local GameStateBattleMap = GameStateManager.GameStateBattleMap
local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
local GameFleetEnhance = LuaObjectManager:GetLuaObject("GameFleetEnhance")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameItemBag = LuaObjectManager:GetLuaObject("GameItemBag")
local GameUICommonDialog = LuaObjectManager:GetLuaObject("GameUICommonDialog")
local QuestTutorialsChangeFleets1 = TutorialQuestManager.QuestTutorialsChangeFleets1
local QuestTutorialsChangeFleets = TutorialQuestManager.QuestTutorialsChangeFleets
GameFleetInfoUI.lastForce = 0
local QuestTutorialEquip = TutorialQuestManager.QuestTutorialEquip
local QuestTutorialEquipAllByOneKey = TutorialQuestManager.QuestTutorialEquipAllByOneKey
local QuestTutorialSkill = TutorialQuestManager.QuestTutorialSkill
local GameUIFleetGrowUp = LuaObjectManager:GetLuaObject("GameUIFleetGrowUp")
function GameFleetInfoUI:OnInitGame()
  GameGlobalData:RegisterDataChangeCallback("fleetinfo", GameFleetInfoUI.refreshFleets)
  self.currentSlectedIndex = -1
  self.curFleetEquipments = {}
end
GameFleetInfoUI.forceEquip = false
function GameFleetInfoUI:Init()
  self.lastReleasedIndex = -1
  self.pressIndex = -1
  self.clicked = false
  self.doubClicked = false
  self.dragItem = false
  self.avatarBright = false
  GameFleetInfoUI.all_need_equip = {}
  self.currentSlectedIndex = math.max(self.currentSlectedIndex, 1)
  self:UpdateSelectedFleetInfo(self.currentSlectedIndex)
  if self:GetFlashObject() then
    if QuestTutorialEquipAllByOneKey:IsActive() then
      self:GetFlashObject():InvokeASCallback("_root", "SetEquipAllAnimVisible", true)
    else
      self:GetFlashObject():InvokeASCallback("_root", "SetEquipAllAnimVisible", false)
    end
  end
end
function GameFleetInfoUI:OnAddToGameState()
  self:LoadFlashObject()
  self:Init()
end
function GameFleetInfoUI:OnEraseFromGameState()
  self:Clear()
  self:GetFlashObject():InvokeASCallback("_root", "disableEquip")
  self:UnloadFlashObject()
  GameUIFleetGrowUp:UnloadFlashObject()
end
function GameFleetInfoUI:Clear()
  self.curFleetEquipments = {}
  self.currentSlectedIndex = -1
  self.equipmentShown = false
  self.lastForce = 0
  GameFleetInfoUI.all_need_equip = {}
  GameFleetInfoUI.forceEquip = nil
end
function GameFleetInfoUI.refreshFleets()
  DebugOut("GameFleetInfoUI.refreshFleets")
  if GameStateManager:GetCurrentGameState():IsObjectInState(GameFleetInfoUI) then
    GameFleetInfoUI:UpdateSelectedFleetInfo(GameFleetInfoUI.currentSlectedIndex)
  end
end
function GameFleetInfoUI:UpdateItemInFleet(content)
  if content.bag_type == GameFleetInfoBag.BAG_OWNER_FLEET then
    local fleets = GameGlobalData:GetData("fleetinfo").fleets
    local fleet = fleets[self.currentSlectedIndex]
    if fleet and content.owner == fleet.id then
      for k, v in pairs(content.grids) do
        if v.item_type == 0 then
          self.curFleetEquipments[v.pos] = nil
        else
          self.curFleetEquipments[v.pos] = v
        end
      end
      self:RefreshCurFleetEquipment()
    end
  end
end
function GameFleetInfoUI:UpdateFleetInfoWithContent(content)
  DebugOut("UpdateFleetInfoWithContent")
  if self:GetFlashObject() ~= nil then
    local iconFrame = GameDataAccessHelper:GetFleetAvatar(content.identity)
    local name = GameLoader:GetGameText("LC_NPC_" .. GameDataAccessHelper:GetCommanderName(content.identity))
    if content.identity == 1 then
      local userinfo = GameGlobalData:GetUserInfo()
      name = GameUtils:GetUserDisplayName(userinfo.name)
    end
    local level = GameLoader:GetGameText("LC_MENU_Level") .. GameGlobalData:GetData("levelinfo").level
    local damage = tostring(content.damage)
    local weapon = GameDataAccessHelper:GetSkillNameText(content.active_spell)
    local commonAttack = GameUtils.numberConversion(content.ph_attack)
    local atkText = GameLoader:GetGameText("LC_MENU_PHATK_COLON")
    if GameDataAccessHelper:IsEnergyAttack(content.identity) then
      commonAttack = GameUtils.numberConversion(content.en_attack)
      atkText = GameLoader:GetGameText("LC_MENU_ENATK_COLON")
    end
    local desText = "des"
    if self.lastForce == 0 then
      self.lastForce = content.force
    else
      local changeforce = content.force - self.lastForce
      if changeforce > 0 then
        GameFleetInfoUI:CheckTotorialEquip()
      end
      self.lastForce = content.force
      self:GetFlashObject():InvokeASCallback("_root", "showForceChangeAnimation", changeforce)
      if QuestTutorialsChangeFleets1:IsActive() and changeforce > 0 then
        QuestTutorialsChangeFleets1:SetFinish(true)
        GameFleetInfoBag:GetFlashObject():InvokeASCallback("_root", "HideTutorialAnimTip")
      end
    end
    local vesselsType = "TYPE_" .. tostring(content.vessels)
    local vesselsText = GameLoader:GetGameText("LC_FLEET_" .. vesselsType)
    self:GetFlashObject():InvokeASCallback("_root", "updateFleetVessels", vesselsText)
    local forcetemp = GameUtils.numberConversion(content.force)
    local isNormalFleet = not (content.identity >= 100) or not (content.identity <= 300)
    self:GetFlashObject():InvokeASCallback("_root", "UpdateFleetInfo", isNormalFleet, self.avatarBright, iconFrame, vesselsType, name, level, GameUtils.numberConversion(content.max_durability), damage, weapon, commonAttack, GameUtils.numberConversion(content.ph_armor), GameUtils.numberConversion(content.sp_attack), GameUtils.numberConversion(content.sp_armor), atkText, forcetemp, desText)
    GameFleetInfoBackground:FleetAppear()
  end
end
function GameFleetInfoUI:UpdateFleetInfoWithContentAndServer(content)
  DebugOut("UpdateFleetInfoWithContentAndServer")
  local displayerInfo = GameGlobalData:GetFleetDisplayerInfo(content.identity)
  if self:GetFlashObject() ~= nil then
    local iconFrame = FleetDataAccessHelper:GetFleetAvatarFrameByAvatar(displayerInfo.avatar, displayerInfo.fleet_id, displayerInfo.level)
    local name = FleetDataAccessHelper:GetFleetLevelDisplayName(displayerInfo.name, displayerInfo.fleet_id, displayerInfo.color, displayerInfo.level)
    local level = GameLoader:GetGameText("LC_MENU_Level") .. GameGlobalData:GetData("levelinfo").level
    local damage = tostring(content.damage)
    local weapon = GameDataAccessHelper:GetSkillNameText(content.active_spell)
    local commonAttack = GameUtils.numberConversion(content.ph_attack)
    local atkText = GameLoader:GetGameText("LC_MENU_PHATK_COLON")
    if GameDataAccessHelper:IsEnergyAttackByServerData(content.identity) then
      commonAttack = GameUtils.numberConversion(content.en_attack)
      atkText = GameLoader:GetGameText("LC_MENU_ENATK_COLON")
    end
    local desText = "des"
    if self.lastForce == 0 then
      self.lastForce = content.force
    else
      local changeforce = content.force - self.lastForce
      if changeforce > 0 then
        GameFleetInfoUI:CheckTotorialEquip()
      end
      self.lastForce = content.force
      self:GetFlashObject():InvokeASCallback("_root", "showForceChangeAnimation", changeforce)
      if QuestTutorialsChangeFleets1:IsActive() and changeforce > 0 then
        QuestTutorialsChangeFleets1:SetFinish(true)
        GameFleetInfoBag:GetFlashObject():InvokeASCallback("_root", "HideTutorialAnimTip")
      end
    end
    local vesselsType = FleetDataAccessHelper:GetFleetVesselsFrameByVessel(displayerInfo.vessels)
    local vesselsText = FleetDataAccessHelper:GetFleetVesselsTypeStringByVessel(displayerInfo.vessels)
    self:GetFlashObject():InvokeASCallback("_root", "updateFleetVessels", vesselsText)
    local forcetemp = GameUtils.numberConversion(content.force)
    local isNormalFleet = not (content.identity >= 100) or not (content.identity <= 300)
    self:GetFlashObject():InvokeASCallback("_root", "UpdateFleetInfo", isNormalFleet, self.avatarBright, iconFrame, vesselsType, name, level, GameUtils.numberConversion(content.max_durability), damage, weapon, commonAttack, GameUtils.numberConversion(content.ph_armor), GameUtils.numberConversion(content.sp_attack), GameUtils.numberConversion(content.sp_armor), atkText, forcetemp, desText)
    GameFleetInfoBackground:FleetAppear()
  end
end
function GameFleetInfoUI:UpdateFleetLevelInfo()
  if GameFleetInfoUI:GetFlashObject() ~= nil then
    local displayerInfo = GameGlobalData:GetFleetDisplayerInfo(GameUIFleetGrowUp.mCurrentFleetID)
    local fleetId = displayerInfo.fleet_id
    local fleet = GameGlobalData:GetFleetInfo(fleetId)
    local isNormalFleet = not (fleetId >= 100) or not (fleetId <= 300)
    local forcetemp = GameUtils.numberConversion(fleet.force)
    local name = FleetDataAccessHelper:GetFleetLevelDisplayName(displayerInfo.name, displayerInfo.fleet_id, displayerInfo.color, displayerInfo.level)
    GameFleetInfoUI:GetFlashObject():InvokeASCallback("_root", "UpdateFleetLevelInfo", isNormalFleet, name, forcetemp)
  end
end
function GameFleetInfoUI:UpdateSelectedFleetInfo(fleetIndex)
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  if fleets ~= nil then
    local maxFleetIndex = LuaUtils:table_size(fleets)
    fleetIndex = fleetIndex or 1
    if fleetIndex > 0 and maxFleetIndex >= fleetIndex then
      if self:GetFlashObject() ~= nil then
        if fleetIndex ~= self.currentSlectedIndex then
          self.lastForce = 0
        end
        self:GetFlashObject():InvokeASCallback("_root", "UpdateSelectedFleetId", fleetIndex, maxFleetIndex)
      end
      do
        local content = fleets[fleetIndex]
        self.currentSlectedIndex = fleetIndex
        GameFleetInfoUI:UpdateFleetInfoWithContent(content)
        local bag_req_content = {
          owner = content.id,
          bag_type = GameFleetInfoBag.BAG_OWNER_FLEET,
          pos = -1
        }
        local function netFailedCallback()
          NetMessageMgr:SendMsg(NetAPIList.bag_req.Code, bag_req_content, self.DownloadCurrentFleetEquipment, false, netFailedCallback)
        end
        NetMessageMgr:SendMsg(NetAPIList.bag_req.Code, bag_req_content, self.DownloadCurrentFleetEquipment, false, netFailedCallback)
      end
    end
  end
end
function GameFleetInfoUI:Update(dt)
  self:GetFlashObject():InvokeASCallback("_root", "OnUpdate")
  self:GetFlashObject():Update(dt)
  self:UpdateReleased(dt)
end
function GameFleetInfoUI:UpdateReleased(dt)
  if self.doubClicked then
    self.doubClicked = false
    self:DoubClicked()
  elseif self.clicked and self.clickedTimer > 0 then
    self.clickedTimer = self.clickedTimer - dt
    if self.clickedTimer <= 0 then
      local paramString = self:GetFlashObject():InvokeASCallback("_root", "GetItemRect", self.lastReleasedIndex)
      local param = LuaUtils:string_split(paramString, "\001")
      local equip = self:IsEquipmentInSlot(self.lastReleasedIndex)
      if equip and equip.item_type then
        local equipType = equip.item_type
        local evolutionId = GameDataAccessHelper:GetEquipEvolution(equipType)
        local canEvolution = false
        if evolutionId ~= -1 then
          for k, v in pairs(GameItemBag.itemInBag) do
            if v.item_type == evolutionId then
              canEvolution = true
              break
            end
          end
        end
        local operateText = GameLoader:GetGameText("LC_MENU_UNLOAD_BUTTON")
        local operateFunc = self.EquipmentOperatorCallback
        ItemBox:showItemBox("Equip", equip, equipType, tonumber(param[1]), tonumber(param[2]), tonumber(param[3]), tonumber(param[4]), "", nil, operateText, operateFunc)
      elseif TutorialQuestManager.QuestTutorialCityHall:IsFinished() then
        DebugOut("Empty Slot Pressed")
        local fleets = GameGlobalData:GetData("fleetinfo").fleets
        local fleetIdentity = fleets[self.currentSlectedIndex].identity
        local paramString = self:GetFlashObject():InvokeASCallback("_root", "GetItemRect", self.lastReleasedIndex)
        local param = LuaUtils:string_split(paramString, "\001")
        local slot = self.lastReleasedIndex
        local operateText = GameLoader:GetGameText("LC_MENU_BUY_CHAR")
        local operateFunc = self.EquipmentBuyOperatorCallback
        local equipType = GameDataAccessHelper:GetEquipSlotShopType(slot, fleetIdentity)
        DebugOut("operateText: " .. operateText)
        ItemBox:showItemBox("Equip", nil, equipType, tonumber(param[1]), tonumber(param[2]), tonumber(param[3]), tonumber(param[4]), "", nil, operateText, operateFunc)
      end
      self.clicked = false
      DebugOut("self.clicked")
    end
  end
end
function GameFleetInfoUI:UpdateEquipArrows()
  self:GetFlashObject():InvokeASCallback("_root", "UpdateEquipArrows")
end
function GameFleetInfoUI.EquipmentOperatorCallback()
  local equip = GameFleetInfoUI:IsEquipmentInSlot(GameFleetInfoUI.lastReleasedIndex)
  if equip then
    GameFleetInfoUI:UnloadEquip(equip.item_id)
  end
end
function GameFleetInfoUI.EquipmentBuyOperatorCallback()
  DebugOut("GameFleetInfoUI.EquipmentBuyOperatorCallback() " .. GameFleetInfoUI.lastReleasedIndex)
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  local fleetId = fleets[GameFleetInfoUI.currentSlectedIndex].id
  local fleetIdentity = fleets[GameFleetInfoUI.currentSlectedIndex].identity
  local slot = GameFleetInfoUI.lastReleasedIndex
  local equipType = GameDataAccessHelper:GetEquipSlotShopType(slot, fleetIdentity)
  local shop_buy_and_wear_req_param = {
    formation_id = GameGlobalData:GetData("matrix").id,
    fleet_id = fleetId,
    equip_type = equipType
  }
  NetMessageMgr:SendMsg(NetAPIList.shop_buy_and_wear_req.Code, shop_buy_and_wear_req_param, GameFleetInfoUI.BuyAndEquipCallback, true, nil)
end
function GameFleetInfoUI.BuyAndEquipCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.shop_buy_and_wear_req.Code then
    if content.code ~= 0 then
      local GameUICollect = LuaObjectManager:GetLuaObject("GameUICollect")
      if not GameUICollect:CheckTutorialCollect(content.code) and content.code == 709 then
        local text = AlertDataList:GetTextFromErrorCode(130)
        local GameUIKrypton = LuaObjectManager:GetLuaObject("GameUIKrypton")
        GameUIKrypton.NeedMoreMoney(text)
      else
        GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      end
    end
    return true
  end
  return false
end
function GameFleetInfoUI:DoubClicked()
  self:UnloadEquip()
end
function GameFleetInfoUI:UnloadEquip(itemId)
  DebugOut("GameFleetInfoUI:UnloadEquip(itemId): ", itemId)
  local inlist, slot = self:IsEquipmentInList(itemId)
  slot = slot or self.lastReleasedIndex
  self.unloadSlot = slot
  local equip = self:IsEquipmentInSlot(slot)
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  if equip then
    local swap_bag_grid_req_content = {
      orig_grid_owner = fleets[GameFleetInfoUI.currentSlectedIndex].id,
      oirg_grid_type = GameFleetInfoBag.BAG_OWNER_FLEET,
      oirg_grid_pos = slot,
      target_grid_owner = GameGlobalData:GetData("userinfo").player_id,
      target_grid_type = GameFleetInfoBag.BAG_OWNER_USER,
      target_grid_pos = -1
    }
    local function netFailedCallback()
      GameStateFleetInfo:onEndDragItem()
      GameFleetInfoUI.refreshFleets()
    end
    NetMessageMgr:SendMsg(NetAPIList.swap_bag_grid_req.Code, swap_bag_grid_req_content, GameFleetInfoUI.UnloadEquipCallback, true, netFailedCallback)
  end
end
function GameFleetInfoUI.UnloadEquipCallback(msgType, content)
  if msgType == NetAPIList.swap_bag_grid_ack.Code then
    if content.result == 0 then
      if GameFleetInfoUI.dragItemInstance then
        GameStateFleetInfo:onEndDragItem()
      end
      return true
    else
      GameTip:Show(AlertDataList:GetTextFromErrorCode(content.result), 3000)
      if GameFleetInfoBag.dragItemInstance then
        GameStateFleetInfo:onEndDragItem()
      end
      return true
    end
  end
  return false
end
function GameFleetInfoUI:OnFSCommand(cmd, arg)
  if cmd == "Update_Btn_Clicked" then
    DebugOut("Update_Btn_Clicked")
  end
  if cmd == "beginPress" then
    self.pressIndex = tonumber(arg)
  elseif cmd == "endPress" then
    self.pressIndex = -1
  end
  if cmd == "FleetInfoUIItemReleased" or cmd == "FleetInfoUIEmptySlotReleased" then
    if self.doubClicked then
      return
    end
    local currentReleaasedIndex = tonumber(arg)
    if currentReleaasedIndex == self.lastReleasedIndex and self.clicked then
      self.doubClicked = true
      self.clicked = false
    else
      self.clicked = true
      self.clickedTimer = GameUtils.DOUBLE_CLICK_TIMER
    end
    self.lastReleasedIndex = currentReleaasedIndex
  end
  if cmd == "dragItem" then
    local param = LuaUtils:string_split(arg, "\001")
    local initX, initY, posX, posY = unpack(param)
    DebugOut("self.pressIndex", self.pressIndex)
    if self.pressIndex ~= -1 then
      local id = self:GetIdFormSlot(self.pressIndex)
      assert(id ~= -1)
      local equip = self:IsEquipmentInSlot(self.pressIndex)
      if equip then
        local equipType = equip.item_type
        self.dragItem = true
        GameStateManager:GetCurrentGameState():BeginDragItem(id, equipType, initX, initY, posX, posY)
      end
    end
  end
  if cmd == "dragOnEquip" and self.dragItemInstance then
    local slot = GameDataAccessHelper:GetEquipSlot(self.dragItemInstance.DraggedItemTYPE)
    local canShow = true
    self:GetFlashObject():InvokeASCallback("_root", "dragOnEquip", slot, canShow)
  end
  if cmd == "showFleetPopView" then
    local fleets = GameGlobalData:GetData("fleetinfo").fleets
    local fleet = fleets[self.currentSlectedIndex]
    local isNormalFleet = not (100 <= fleet.identity) or not (fleet.identity <= 300)
    ItemBox:ShowCommanderDetail2(fleet.identity, fleet)
    self:SetAvatarBright(true)
  elseif cmd == "fuck_equip_all_by_one_key" then
    GameFleetInfoUI:AllEquipAction()
  elseif cmd == "reformClicked" then
    local level_info = GameGlobalData:GetData("levelinfo")
    if level_info.level < 35 then
      GameTip:Show(GameLoader:GetGameText("LC_ALERT_REFORM_LOCKED_REQUIRE_LEVER_ALERT"))
      return
    end
    local fleets = GameGlobalData:GetData("fleetinfo").fleets
    local fleet = fleets[self.currentSlectedIndex]
    GameUIFleetGrowUp:Init(fleet.identity)
    GameStateFleetInfo:GoToSubState(SUB_STATE.STATE_GROW_UP)
  end
end
function GameFleetInfoUI:SetAvatarBright(avatarBright)
  self.avatarBright = avatarBright
  self.refreshFleets()
end
function GameFleetInfoUI:GetIdFormSlot(slot)
  local id = -1
  for k, v in pairs(self.curFleetEquipments) do
    if v.pos == slot then
      return v.item_id
    end
  end
  return id
end
function GameFleetInfoUI:ShowEquipMent()
  if not self.equipmentShown then
    self.equipmentShown = true
    GameFleetInfoUI:GetFlashObject():InvokeASCallback("_root", "ShowFleetEquipment")
  end
end
function GameFleetInfoUI:HideEquipMent()
  if self.equipmentShown then
    self.equipmentShown = false
    GameFleetInfoUI:GetFlashObject():InvokeASCallback("_root", "HideFleetEquipment")
  end
end
function GameFleetInfoUI.DownloadCurrentFleetEquipment(msgType, content)
  if msgType == NetAPIList.bag_ack.Code then
    if not GameFleetInfoUI:GetFlashObject() then
      GameFleetInfoUI:LoadFlashObject()
    end
    GameFleetInfoUI:ShowEquipMent()
    GameFleetInfoUI.curFleetEquipments = content.grids
    GameItemBag.equipedItem = GameItemBag:processEquipedItem(content.equipedItems)
    GameFleetInfoUI.curFleetEquipments = GameFleetInfoUI:processFleetEquipments()
    GameFleetInfoUI:RefreshCurFleetEquipment()
    return true
  else
    GameFleetInfoUI:RefreshCurFleetEquipment()
    return false
  end
  return false
end
function GameFleetInfoUI:processFleetEquipments()
  local dest = {}
  for k, v in pairs(self.curFleetEquipments) do
    dest[v.pos] = v
  end
  return dest
end
function GameFleetInfoUI:RefreshCurFleetEquipment()
  local frame, level = "", ""
  local isAllEquip = 0
  for i = 1, 5 do
    local equip = self:IsEquipmentInSlot(i)
    local evolutionId = -1
    local equipments = GameGlobalData:GetData("equipments")
    if equip then
      isAllEquip = isAllEquip + 1
      do
        local currentEquipId = equip.item_id
        local currentEquipInfo = LuaUtils:table_values(LuaUtils:table_filter(equipments, function(k, v)
          return v.equip_id == currentEquipId
        end))[1]
        frame = frame .. "item_" .. equip.item_type .. "\001"
        level = level .. GameLoader:GetGameText("LC_MENU_Level") .. currentEquipInfo.equip_level .. "\001"
      end
    else
      frame = frame .. "empty_e" .. i .. "\001"
      level = level .. "no" .. "\001"
    end
  end
  if isAllEquip == 5 and GameFleetInfoUI.forceEquip then
    GameFleetInfoUI.forceEquip = false
    GameItemBag.EquipTutorialAgain = false
    GameFleetInfoTabBar.finishEquipTutorialSecond = true
    GameFleetInfoTabBar:GetFlashObject():InvokeASCallback("_root", "ShowTutorialAnimTip")
    self:GetFlashObject():InvokeASCallback("_root", "SetEquipAllAnimVisible", false)
  end
  DebugOut("SetFleetEquipment", frame, level)
  self:GetFlashObject():InvokeASCallback("_root", "SetFleetEquipment", frame, level, TutorialQuestManager.QuestTutorialCityHall:IsFinished())
  local equipment_action_list_req_param = {
    ids = {}
  }
  for i = 1, 5 do
    local equip = self:IsEquipmentInSlot(i)
    if equip then
      table.insert(equipment_action_list_req_param.ids, tonumber(equip.item_id))
    else
      table.insert(equipment_action_list_req_param.ids, -1)
    end
  end
  NetMessageMgr:SendMsg(NetAPIList.equipment_action_list_req.Code, equipment_action_list_req_param, self.EquipActionListCallback, true, nil)
  GameFleetInfoBag:GetOnlyEquip()
  GameFleetInfoBag:ShowEquipmentItem()
end
function GameFleetInfoUI.EquipActionListCallback(msgType, content)
  if msgType == NetAPIList.equipment_action_list_ack.Code then
    GameFleetInfoUI.equipStatus = content.equipments
    local canEvolution = ""
    for i = 1, 5 do
      local equip = GameFleetInfoUI:IsEquipmentInSlot(i)
      if equip then
        if GameFleetInfoUI.equipStatus[i] and GameFleetInfoUI.equipStatus[i] ~= -1 and GameFleetInfoUI.equipStatus[i].can_evolute == 1 then
          canEvolution = canEvolution .. "true" .. "\001"
        else
          canEvolution = canEvolution .. "false" .. "\001"
        end
      else
        canEvolution = canEvolution .. "false" .. "\001"
      end
    end
    GameFleetInfoUI:GetFlashObject():InvokeASCallback("_root", "SetEquipArrows", canEvolution)
    return true
  elseif msgType == NetAPIList.common_ack.Code then
    if content.api == NetAPIList.equipment_action_list_req.Code and content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  return false
end
function GameFleetInfoUI:CheckTotorialEquip()
  if immanentversion == 2 then
    if QuestTutorialEquip:IsActive() and not QuestTutorialEquip:IsFinished() then
      AddFlurryEvent("FirstEquipSuccess", {}, 1)
      QuestTutorialEquip:SetFinish(true)
      AddFlurryEvent("TutorialEquip", {}, 2)
      if GameFleetInfoTabBar:GetFlashObject() then
        GameFleetInfoTabBar.closeAnimTip = "equip"
        GameFleetInfoTabBar:GetFlashObject():InvokeASCallback("_root", "ShowTutorialAnimTip")
      end
      GameFleetInfoBag:GetFlashObject():InvokeASCallback("_root", "HideTutorialAnimTip")
      GameFleetInfoBag:GetFlashObject():InvokeASCallback("_root", "HideTutorialAnimTip2")
      if not QuestTutorialSkill:IsFinished() and not QuestTutorialSkill:IsActive() then
        local function callback()
          QuestTutorialSkill:SetActive(true)
          GameStateBattleMap:SetStoryWhenFocusGain({1100014})
          if GameFleetInfoTabBar:GetFlashObject() then
            GameFleetInfoTabBar.closeAnimTip = "equip"
            GameFleetInfoTabBar:GetFlashObject():InvokeASCallback("_root", "ShowTutorialAnimTip")
          end
        end
        GameUICommonDialog:PlayStory({11000131}, callback)
      elseif GameFleetInfoTabBar:GetFlashObject() then
        GameFleetInfoTabBar.closeAnimTip = "equip"
      end
    elseif QuestTutorialEquipAllByOneKey:IsActive() and not QuestTutorialEquipAllByOneKey:IsFinished() then
      AddFlurryEvent("TutorialItem_Equip", {}, 2)
      QuestTutorialEquipAllByOneKey:SetFinish(true)
    end
  elseif immanentversion == 1 and QuestTutorialEquip:IsActive() then
    AddFlurryEvent("FirstEquipSuccess", {}, 1)
    QuestTutorialEquip:SetFinish(true)
    if immanentversion170 == 4 or immanentversion170 == 5 then
      TutorialQuestManager.QuestTutorialPveMapPoint:SetActive(true, false)
    end
    GameFleetInfoBag:GetFlashObject():InvokeASCallback("_root", "HideTutorialAnimTip")
    GameFleetInfoBag:GetFlashObject():InvokeASCallback("_root", "HideTutorialAnimTip2")
    if GameFleetInfoTabBar:GetFlashObject() then
      GameFleetInfoTabBar.closeAnimTip = "equip"
      GameFleetInfoTabBar:GetFlashObject():InvokeASCallback("_root", "ShowTutorialAnimTip")
    end
    GameStateBattleMap:SetStoryWhenFocusGain({1108})
  end
end
function GameFleetInfoUI:CheckTotorialChangeFleets()
  if not QuestTutorialsChangeFleets:IsFinished() and QuestTutorialsChangeFleets1:IsActive() and self:IsEquipmentInSlot(2) then
    GameFleetInfoBag:SetQueestTutorialsChangeFleet1True()
  end
end
function GameFleetInfoUI:IsEquipmentInSlot(slot)
  return self.curFleetEquipments[slot]
end
function GameFleetInfoUI:onBeginDragItem(dragitem)
  self.dragItemInstance = dragitem
  local itemId = self.dragItemInstance.DraggedItemID
  local inlist, index = self:IsEquipmentInList(itemId)
  local slot = -1
  if inlist then
    slot = self.curFleetEquipments[index].pos
  end
  self:GetFlashObject():InvokeASCallback("_root", "onDragItem", slot)
end
function GameFleetInfoUI:onEndDragItem()
  self.dragItem = false
  self.dragItemInstance = nil
  self:GetFlashObject():InvokeASCallback("_root", "onEndDragItem")
end
function GameFleetInfoUI:IsEquipmentInList(itemId)
  if self.curFleetEquipments == nil then
    return false
  end
  for k, v in pairs(self.curFleetEquipments) do
    if v.item_id == itemId then
      return true, k
    end
  end
  return false
end
function GameFleetInfoUI:AllEquipAction()
  if #GameFleetInfoUI.all_need_equip > 1 then
    return
  end
  local currentFleetEquipMentTemp = {}
  local currentGameItemBagTemp = {}
  local equipments = GameGlobalData:GetData("equipments")
  for k, v in pairs(equipments) do
    for k1, v1 in pairs(self.curFleetEquipments) do
      if v1.item_id == v.equip_id then
        table.insert(currentFleetEquipMentTemp, v)
      end
    end
    for k2, v2 in pairs(GameFleetInfoBag.onlyEquip) do
      if v2.item_id == v.equip_id then
        v.pos = v2.pos
        table.insert(currentGameItemBagTemp, v)
      end
    end
  end
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  local fleet = fleets[self.currentSlectedIndex]
  local fleetVessels = GameDataAccessHelper:GetCommanderVessels(fleet.identity)
  local isEnFleet = false
  if tonumber(fleetVessels) == 4 then
    isEnFleet = true
  end
  GameFleetInfoUI.all_need_equip = {}
  local tmp_table = {}
  for i = 1, 5 do
    if i == 1 then
      tmp_table = {}
      for k, v in pairs(currentGameItemBagTemp) do
        if v.equip_slot == i then
          if isEnFleet and tonumber(string.sub(v.equip_type, -1)) == 6 then
            table.insert(tmp_table, v)
          elseif not isEnFleet and tonumber(string.sub(v.equip_type, -1)) ~= 6 then
            table.insert(tmp_table, v)
          end
        end
      end
    else
      tmp_table = {}
      for k, v in pairs(currentGameItemBagTemp) do
        if v.equip_slot == i then
          table.insert(tmp_table, v)
        end
      end
    end
    DebugOut("-------------------------------------" .. i .. "   --------")
    DebugTable(tmp_table)
    if #tmp_table > 0 then
      table.sort(tmp_table, function(v1, v2)
        if v1.equip_type == v2.equip_type then
          return v1.equip_level > v2.equip_level
        else
          return v1.equip_type > v2.equip_type
        end
      end)
      local canReplaceEquip = true
      for k, v in pairs(currentFleetEquipMentTemp) do
        if v.equip_slot == i then
          if v.equip_type == tmp_table[1].equip_type and v.equip_level >= tmp_table[1].equip_level then
            canReplaceEquip = false
          elseif v.equip_type > tmp_table[1].equip_type then
            canReplaceEquip = false
          end
        end
      end
      if canReplaceEquip then
        table.insert(GameFleetInfoUI.all_need_equip, tmp_table[1])
      end
    end
  end
  if #GameFleetInfoUI.all_need_equip > 0 then
    GameFleetInfoUI:ReplaceAllEquipmentByOneKey()
  else
  end
end
function GameFleetInfoUI:ReplaceAllEquipmentByOneKey()
  if #GameFleetInfoUI.all_need_equip < 1 then
    GameItemBag:RequestBag()
    return
  end
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  local swap_bag_grid_req_content = {
    orig_grid_owner = GameGlobalData:GetData("userinfo").player_id,
    oirg_grid_type = GameFleetInfoBag.BAG_OWNER_USER,
    oirg_grid_pos = GameFleetInfoUI.all_need_equip[1].pos,
    target_grid_owner = fleets[GameFleetInfoUI.currentSlectedIndex].id,
    target_grid_type = GameFleetInfoBag.BAG_OWNER_FLEET,
    target_grid_pos = GameFleetInfoUI.all_need_equip[1].equip_slot
  }
  local function netFailedCallback()
    GameFleetInfoUI.all_need_equip = {}
    GameItemBag:RequestBag()
  end
  table.remove(GameFleetInfoUI.all_need_equip, 1)
  NetMessageMgr:SendMsg(NetAPIList.swap_bag_grid_req.Code, swap_bag_grid_req_content, GameFleetInfoUI.EquipCallback, true, netFailedCallback)
end
function GameFleetInfoUI.EquipCallback(msgType, content)
  if msgType == NetAPIList.swap_bag_grid_ack.Code then
    DebugOut("\230\137\147\229\141\176--GameFleetInfoUI.EquipCallback----")
    if content.result == 0 then
      GameFleetInfoUI:ReplaceAllEquipmentByOneKey()
      return true
    else
      GameFleetInfoUI.all_need_equip = {}
      GameTip:Show(AlertDataList:GetTextFromErrorCode(content.result), 3000)
      return true
    end
  end
  return false
end
