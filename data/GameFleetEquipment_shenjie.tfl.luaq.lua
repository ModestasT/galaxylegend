require("FleetAvatarEnhanceData.tfl")
local GameStateEquipEnhance = GameStateManager.GameStateEquipEnhance
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameUIWDStuff = LuaObjectManager:GetLuaObject("GameUIWDStuff")
local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
local QuestTutorialFirstReform = TutorialQuestManager.QuestTutorialFirstReform
local GameFleetEquipment = LuaObjectManager:GetLuaObject("GameFleetEquipment")
GameFleetEquipment.shenjie = {}
local shenjie = GameFleetEquipment.shenjie
shenjie.allFleetGrowUpInfo = {}
shenjie.GrowUpState = {
  unlocked = 0,
  locked = 1,
  can = 2
}
local E_UI_STATE = {
  UISTATE_NONE = 0,
  UISTATE_CARD = 1,
  UISTATE_MOVING_TO_DETAIL = 2,
  UISTATE_DETAIL = 3,
  UISTATE_MOVING_TO_CARD = 4
}
local CommanderRanks = {
  [1] = "rank_E",
  [2] = "rank_D",
  [3] = "rank_C",
  [4] = "rank_B",
  [5] = "rank_A",
  [6] = "rank_S",
  [7] = "rank_SS"
}
shenjie.mFleetIdentify = -1
shenjie.mCurrentAvatarLevel = -1
shenjie.mCurrentFleetEnhanceData = nil
shenjie.mCurrentUIState = E_UI_STATE.UISTATE_NONE
function shenjie:Clear()
  shenjie.mCurrentFleetEnhanceData = nil
  shenjie.mFleetIdentify = -1
  shenjie.mCurrentUIState = E_UI_STATE.UISTATE_NONE
end
function shenjie:OnFSCommand(cmd, arg)
  if cmd == "ShowItemDetail" then
    local tinfo = LuaUtils:deserializeTable(arg)
    ItemBox:ShowGameItem(tinfo)
    return true
  elseif cmd == "onClickGrowup" then
    if self:CanEvolve(self.mCurrentFleetEnhanceData.mCurLevel) then
      shenjie:RequestLevelUp(shenjie.mFleetIdentify, true)
    else
      shenjie:RequestLevelUp(shenjie.mFleetIdentify, false)
    end
    return true
  elseif cmd == "onClickGrowupForce" then
    shenjie:RequestLevelUp(shenjie.mFleetIdentify, false)
    return true
  else
    if cmd == "onClickReset" then
      if self.mCurrentFleetEnhanceData:IsMinLevel() then
        return
      else
        shenjie:RequestRecoverItems()
      end
      return true
    else
    end
  end
  return false
end
function shenjie:InitOnce()
  GameGlobalData:RemoveDataChangeCallback("fleetinfo", shenjie.OnGlobalFleetsChange)
  GameGlobalData:RegisterDataChangeCallback("fleetinfo", shenjie.OnGlobalFleetsChange)
  shenjie:GetAllFleetGrowUpDate()
end
function shenjie:Init(fleetinfo, fleetidx)
  shenjie.mFleetIdentify = fleetinfo.identity
  shenjie.m_currentSelectFleetIndex = fleetidx
  shenjie.mCurrentFleetEnhanceData = FleetAvatarEnhanceData.new(fleetinfo.identity)
  shenjie.mCurrentUIState = E_UI_STATE.UISTATE_CARD
  shenjie:RequestAvatarEnhanceInfo(shenjie.mFleetIdentify, true, shenjie.RequestAvatarEnhanceInfoCallback)
end
function shenjie:GetFlashObject()
  return (...), GameFleetEquipment
end
function shenjie:OnEraseFromGameState()
  GameGlobalData:RemoveDataChangeCallback("fleetinfo", shenjie.OnGlobalFleetsChange)
end
function shenjie:Hide()
end
function shenjie.OnGlobalFleetsChange()
  if GameFleetEquipment.currentSelect ~= "shenjie" then
    return
  end
  shenjie:RequestAvatarEnhanceInfo(shenjie.mFleetIdentify, true, shenjie.RequestAvatarEnhanceInfoCallback)
  shenjie:GetAllFleetGrowUpDate()
end
function shenjie:RequestRecoverItems()
  shenjie:RequestAvatarEnhanceInfo(shenjie.mFleetIdentify, false, shenjie.RequestRecoverItemsCallback)
end
function shenjie.RequestRecoverItemsCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.hero_level_attr_diff_req.Code then
    return true
  elseif msgType == NetAPIList.hero_level_attr_diff_ack.Code then
    shenjie.mCurrentFleetEnhanceData:SetRecoverList(content.conditions)
    shenjie.mCurrentFleetEnhanceData:SetRecoverCard(content)
    local tparam = {}
    tparam.curcard, tparam.nextcard = shenjie:GetResetCardParam()
    tparam.listparam = shenjie:GetResetListParam()
    GameStateEquipEnhance:GetPop():shenjie_showReset(tparam)
    return true
  end
  return false
end
function shenjie:GetResetCardParam()
  local fleetCard = shenjie.mCurrentFleetEnhanceData:GetRecoverCard()
  local curCard = fleetCard.mCurFleetCard
  local levelStr = GameDataAccessHelper:GetFleetLevelStrWithColor(curCard.level, curCard.color)
  local vesselFrame = FleetDataAccessHelper:GetFleetVesselsFrameByVessel(curCard.vessels)
  local vesselStr = FleetDataAccessHelper:GetFleetVesselsTypeStringByVessel(curCard.vessels)
  local colorFrame = FleetDataAccessHelper:GetFleetColorFrameByColor(curCard.color)
  local avatarName = FleetDataAccessHelper:GetFleetColorDisplayName(curCard.name, curCard.fleet_id, curCard.color)
  local sexl = GameDataAccessHelper:GetCommanderSex(curCard.fleet_id)
  local avatarFrame = GameDataAccessHelper:GetFleetAvatar(curCard.fleet_id, curCard.level, sexl)
  if curCard.fleet_id == 1 then
    local tempid = GameFleetEquipment:GetLeaderFleet()
    avatarFrame = GameDataAccessHelper:GetFleetAvatar(tempid, curCard.level)
  end
  if sexl == 1 then
    sexl = "man"
  elseif sexl == 0 then
    sexl = "woman"
  else
    sexl = "unknown"
  end
  local tcurcard = {}
  tcurcard.levelStr = levelStr
  tcurcard.vesselsType = vesselStr
  tcurcard.vesselsFrame = vesselFrame
  tcurcard.colorFrame = colorFrame
  tcurcard.avatarFrame = avatarFrame
  tcurcard.avatarName = avatarName
  tcurcard.sex = sexl
  curCard = fleetCard.mNextFleetCard
  local levelStr = GameDataAccessHelper:GetFleetLevelStrWithColor(curCard.level, curCard.color)
  vesselFrame = FleetDataAccessHelper:GetFleetVesselsFrameByVessel(curCard.vessels)
  vesselStr = FleetDataAccessHelper:GetFleetVesselsTypeStringByVessel(curCard.vessels)
  colorFrame = FleetDataAccessHelper:GetFleetColorFrameByColor(curCard.color)
  avatarName = FleetDataAccessHelper:GetFleetColorDisplayName(curCard.name, curCard.fleet_id, curCard.color)
  local sexr = GameDataAccessHelper:GetCommanderSex(curCard.fleet_id)
  avatarFrame = GameDataAccessHelper:GetFleetAvatar(curCard.fleet_id, curCard.level, sexr)
  if curCard.fleet_id == 1 then
    local tempid = GameFleetEquipment:GetLeaderFleet()
    avatarFrame = GameDataAccessHelper:GetFleetAvatar(tempid, curCard.level)
  end
  if sexr == 1 then
    sexr = "man"
  elseif sexr == 0 then
    sexr = "woman"
  else
    sexr = "unknown"
  end
  local tnextcard = {}
  tnextcard.levelStr = levelStr
  tnextcard.vesselsType = vesselStr
  tnextcard.vesselsFrame = vesselFrame
  tnextcard.colorFrame = colorFrame
  tnextcard.avatarFrame = avatarFrame
  tnextcard.avatarName = avatarName
  tnextcard.sex = sexr
  return tcurcard, tnextcard
end
function shenjie:GetResetListParam()
  local recoverItems = self.mCurrentFleetEnhanceData:GetRecoverList()
  local recoverNum = #recoverItems
  local tparam = {}
  for itemIndex = 1, recoverNum do
    local recoverItems = self.mCurrentFleetEnhanceData:GetRecoverList()
    local item = recoverItems[itemIndex]
    local iinfo = GameHelper:GetItemInfo(item)
    local itemName = iinfo.name
    local itemNumber = "x" .. GameUtils.numberConversion(iinfo.cnt)
    local t = {}
    t.itemNum = itemNumber
    t.itemName = itemName
    t.itemIcon = iinfo.icon_frame
    t.itemPic = iinfo.icon_pic
    t.strid = LuaUtils:serializeTable(item)
    table.insert(tparam, t)
  end
  return tparam
end
function shenjie:ShowRecoverFinished()
  local flash = self:GetFlashObject()
  if not flash then
    return
  end
  flash:InvokeASCallback("_root", "showRecoverFinished")
  local resourceTxt = GameLoader:GetGameText("LC_MENU_REFORM_RECOVER_SUCCESS_RECIEVE_ALERT")
  flash:InvokeASCallback("_root", "setRecoverResourceText", resourceTxt)
end
function shenjie:shenjie_onClickResetConfirm()
  local function confirmCallback()
    shenjie:RequestLevelDown(shenjie.mFleetIdentify)
  end
  local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
  GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
  GameUIMessageDialog:SetYesButton(confirmCallback, nil)
  local infoTitle = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
  local infoContent = GameLoader:GetGameText("LC_MENU_REFORM_RECOVER_CONFIRM_ALERT")
  GameUIMessageDialog:Display(infoTitle, infoContent)
end
function shenjie:RequestLevelDown(fleetID)
  local content = {fleet_id = fleetID}
  NetMessageMgr:SendMsg(NetAPIList.fleet_weaken_req.Code, content, self.RequestLevelDownCallback, true, nil)
end
function shenjie.RequestLevelDownCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.fleet_weaken_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    else
      local fleetList = {}
      local showParam = {}
      showParam.fleet_id = shenjie.mFleetIdentify
      showParam.level = -1
      fleetList[1] = showParam
      GameStateEquipEnhance:GetPop():shenjie_resetFinished()
    end
    return true
  end
  return false
end
function shenjie:RequestAvatarEnhanceInfo(fleetID, isUp, callback)
  local content = {hero_id = fleetID, is_levelup = isUp}
  NetMessageMgr:SendMsg(NetAPIList.hero_level_attr_diff_req.Code, content, callback, true, nil)
end
function shenjie.IsTopLevel()
  local fleetCard = shenjie.mCurrentFleetEnhanceData:GetFleetCardData()
  local curCard = fleetCard.mCurFleetCard
  return curCard.level >= shenjie.mMaxLevel
end
function shenjie.RequestAvatarEnhanceInfoCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.hero_level_attr_diff_req.Code then
    return true
  elseif msgType == NetAPIList.hero_level_attr_diff_ack.Code then
    shenjie.mMaxLevel = content.allow_max
    if shenjie.mCurrentFleetEnhanceData then
      shenjie.mCurrentFleetEnhanceData:GenerateAvatarCardByServerData(content)
      shenjie.mCurrentFleetEnhanceData:GenerateAvatarAttriDataByServerData(content)
      shenjie.mCurrentFleetEnhanceData:SetGrowUpRequireList(content.conditions)
      if shenjie:GetFlashObject() then
        if shenjie.IsTopLevel() then
          shenjie:SetTopLevelFlash()
        else
          shenjie:GetFlashObject():InvokeASCallback("_root", "InitShenjie")
          shenjie:SetUpMenu()
          shenjie:SetUpFleetCard()
          shenjie:SetRequrieItems()
          shenjie:SetRequriePercent()
          shenjie:SetPropDetailList()
        end
      end
    end
    return true
  end
  return false
end
function shenjie:FleetInMatrix(fleetid)
  local matrix = GameGlobalData:GetData("matrix").cells
  DebugOut("matrix::")
  DebugTable(matrix)
  for k, v in pairs(matrix or {}) do
    if v.fleet_identity == fleetid then
      return true
    end
  end
  return false
end
function shenjie:GetAllFleetGrowUpDate()
  NetMessageMgr:SendMsg(NetAPIList.can_enhance_req.Code, nil, shenjie.AllFleetGrowUpCallback, true, nil)
end
function shenjie.AllFleetGrowUpCallback(msgType, content)
  if msgType == NetAPIList.can_enhance_ack.Code then
    DebugOut("AllFleetGrowUpCallback = ")
    DebugTable(content)
    shenjie.allFleetGrowUpInfo = content.fleet_info
    if shenjie:GetFlashObject() then
      local fleetInfo, fleetidx = ZhenXinUI():GetCurFleetContent()
      shenjie:UpdateGrowUpState(fleetInfo.identity)
    end
    return true
  end
  return false
end
function shenjie:GetFleetGrowUpState(identity)
  local currentState = ""
  local playerLevelInfo = GameGlobalData:GetData("levelinfo")
  if immanentversion170 == nil then
    if playerLevelInfo.level < 35 then
      currentState = shenjie.GrowUpState.unlocked
      return currentState
    end
  elseif (immanentversion170 == 4 or immanentversion170 == 5) and not GameUtils:IsModuleUnlock("reform") then
    currentState = shenjie.GrowUpState.unlocked
    return currentState
  end
  for _, v in pairs(self.allFleetGrowUpInfo) do
    if v.identity == identity then
      if v.can_enhance then
        currentState = shenjie.GrowUpState.can
        break
      end
      currentState = shenjie.GrowUpState.locked
      break
    end
  end
  DebugOut("GetFleetGrowUpState = " .. currentState)
  return currentState
end
function shenjie:UpdateGrowUpState(fleetid)
  local curGrowUpState = self:GetFleetGrowUpState(fleetid)
  if self:GetFlashObject() then
    if self:FleetInMatrix(fleetid) then
      self:GetFlashObject():InvokeASCallback("_root", "setFunctions_news", "shenjie", curGrowUpState == shenjie.GrowUpState.can)
    else
      self:GetFlashObject():InvokeASCallback("_root", "setFunctions_news", "shenjie", false)
    end
  end
end
function shenjie:SetTopLevelFlash()
  local flash = self:GetFlashObject()
  if not flash then
    return
  end
  local fleetCard = shenjie.mCurrentFleetEnhanceData:GetFleetCardData()
  local curCard = fleetCard.mCurFleetCard
  local levelStr = GameDataAccessHelper:GetFleetLevelStrWithColor(curCard.level, curCard.color)
  local vesselFrame = FleetDataAccessHelper:GetFleetVesselsFrameByVessel(curCard.vessels)
  local vesselStr = FleetDataAccessHelper:GetFleetVesselsTypeStringByVessel(curCard.vessels)
  local colorFrame = FleetDataAccessHelper:GetFleetColorFrameByColor(curCard.color)
  local spellFrame = FleetDataAccessHelper:GetFleetSpellFrameBySpell(curCard.spell_id)
  local avatarName = FleetDataAccessHelper:GetFleetColorDisplayName(curCard.name, curCard.fleet_id, curCard.color)
  local avatarShip = GameDataAccessHelper:GetShip(curCard.fleet_id, nil, curCard.level)
  if curCard.fleet_id == 1 then
    avatarShip = GameDataAccessHelper:GetShip(GameFleetEquipment:GetLeaderFleet(), nil, curCard.level)
  end
  local sexl = GameDataAccessHelper:GetCommanderSex(curCard.fleet_id)
  local avatarFrame = GameDataAccessHelper:GetFleetAvatar(curCard.fleet_id, curCard.level, sexl)
  if curCard.fleet_id == 1 then
    local tempid = GameFleetEquipment:GetLeaderFleet()
    avatarFrame = GameDataAccessHelper:GetFleetAvatar(tempid, curCard.level)
  end
  if sexl == 1 then
    sexl = "man"
  elseif sexl == 0 then
    sexl = "woman"
  else
    sexl = "unknown"
  end
  local tparam = {}
  tparam.levelStr = levelStr
  tparam.vesselsType = vesselStr
  tparam.vesselsFrame = vesselFrame
  tparam.colorFrame = colorFrame
  tparam.avatarFrame = avatarFrame
  tparam.avatarName = avatarName
  tparam.shipFrame = avatarShip
  tparam.sex = sexl
  tparam.desctext = GameLoader:GetGameText("LC_MENU_REFORMING_LV_MAX_DESC")
  tparam.listparam = {}
  local attriData = self.mCurrentFleetEnhanceData:GetFleetAttriDiffData()
  local attriNum = #attriData.mAttriNames
  for itemIndex = 1, attriNum do
    local itemName = attriData.mAttriNames[itemIndex]
    local itemCurValue = attriData.mCurLevelAttriValue[itemIndex]
    local itemNextValue = attriData.mNextLevelAttriValue[itemIndex]
    if self.mCurrentFleetEnhanceData:IsPercentAttri(attriData.mAttriIDs[itemIndex]) then
      itemCurValue = string.format("%.02f", itemCurValue) .. "%"
      itemNextValue = string.format("%.02f", itemNextValue) .. "%"
    else
      itemCurValue = GameUtils.numberConversion(tonumber(itemCurValue))
      itemNextValue = GameUtils.numberConversion(tonumber(itemNextValue))
    end
    local t = {}
    t.itemName = itemName
    t.itemCurValue = itemCurValue
    t.itemNextValue = itemNextValue
    table.insert(tparam.listparam, t)
  end
  flash:InvokeASCallback("_root", "setFullLevelUpButtonText", GameLoader:GetGameText("LC_MENU_REFORM_BUTTON"))
  if self:CanEvolve(self.mCurrentFleetEnhanceData.mCurLevel) then
    flash:InvokeASCallback("_root", "setFullLevelUpButtonText", GameLoader:GetGameText("LC_MENU_REFORMING_EVOLUTION"))
    tparam.desctext = GameLoader:GetGameText("LC_MENU_REFORMING_LV_MAX_HERO")
  end
  flash:InvokeASCallback("_root", "InitTopLevel", tparam)
end
function shenjie:SetUpFleetCard()
  DebugOut("set up fleet")
  local flash = self:GetFlashObject()
  if not flash then
    return
  end
  local fleetCard = shenjie.mCurrentFleetEnhanceData:GetFleetCardData()
  local curCard = fleetCard.mCurFleetCard
  local levelStr = GameDataAccessHelper:GetFleetLevelStrWithColor(curCard.level, curCard.color)
  local vesselFrame = FleetDataAccessHelper:GetFleetVesselsFrameByVessel(curCard.vessels)
  local vesselStr = FleetDataAccessHelper:GetFleetVesselsTypeStringByVessel(curCard.vessels)
  local colorFrame = FleetDataAccessHelper:GetFleetColorFrameByColor(curCard.color)
  local spellFrame = FleetDataAccessHelper:GetFleetSpellFrameBySpell(curCard.spell_id)
  local avatarName = FleetDataAccessHelper:GetFleetColorDisplayName(curCard.name, curCard.fleet_id, curCard.color)
  local avatarShip = GameDataAccessHelper:GetShip(curCard.fleet_id, nil, curCard.level)
  if curCard.fleet_id == 1 then
    avatarShip = GameDataAccessHelper:GetShip(GameFleetEquipment:GetLeaderFleet(), nil, curCard.level)
  end
  local sexl = GameDataAccessHelper:GetCommanderSex(curCard.fleet_id)
  local avatarFrame = GameDataAccessHelper:GetFleetAvatar(curCard.fleet_id, curCard.level, sexl)
  if curCard.fleet_id == 1 then
    local tempid = GameFleetEquipment:GetLeaderFleet()
    avatarFrame = GameDataAccessHelper:GetFleetAvatar(tempid, curCard.level)
  end
  if sexl == 1 then
    sexl = "man"
  elseif sexl == 0 then
    sexl = "woman"
  else
    sexl = "unknown"
  end
  local tparamOrigion = {}
  tparamOrigion.levelStr = levelStr
  tparamOrigion.vesselsType = vesselStr
  tparamOrigion.vesselsFrame = vesselFrame
  tparamOrigion.colorFrame = colorFrame
  tparamOrigion.avatarFrame = avatarFrame
  tparamOrigion.avatarName = avatarName
  tparamOrigion.shipFrame = avatarShip
  tparamOrigion.sex = sexl
  flash:InvokeASCallback("_root", "SetUpOriginalFleetCard", tparamOrigion)
  local originalCard = curCard
  curCard = fleetCard.mNextFleetCard
  local levelStr = GameDataAccessHelper:GetFleetLevelStrWithColor(curCard.level, curCard.color)
  vesselFrame = FleetDataAccessHelper:GetFleetVesselsFrameByVessel(curCard.vessels)
  vesselStr = FleetDataAccessHelper:GetFleetVesselsTypeStringByVessel(curCard.vessels)
  colorFrame = FleetDataAccessHelper:GetFleetColorFrameByColor(curCard.color)
  spellFrame = FleetDataAccessHelper:GetFleetSpellFrameBySpell(curCard.spell_id)
  avatarName = FleetDataAccessHelper:GetFleetColorDisplayName(curCard.name, curCard.fleet_id, curCard.color)
  avatarShip = GameDataAccessHelper:GetShip(curCard.fleet_id, nil, curCard.level)
  if curCard.fleet_id == 1 then
    avatarShip = GameDataAccessHelper:GetShip(GameFleetEquipment:GetLeaderFleet(), nil, curCard.level)
  end
  local damage_inc = curCard.damage_rate - originalCard.damage_rate
  local defence_inc = curCard.defence_rate - originalCard.defence_rate
  local assist_inc = curCard.assist_rate - originalCard.assist_rate
  local sex = GameDataAccessHelper:GetCommanderSex(curCard.fleet_id)
  avatarFrame = GameDataAccessHelper:GetFleetAvatar(curCard.fleet_id, curCard.level, sex)
  if curCard.fleet_id == 1 then
    local tempid = GameFleetEquipment:GetLeaderFleet()
    avatarFrame = GameDataAccessHelper:GetFleetAvatar(tempid, curCard.level)
  end
  if sex == 1 then
    sex = "man"
  elseif sex == 0 then
    sex = "woman"
  else
    sex = "unknown"
  end
  local tparamGrowup = {}
  tparamGrowup.levelStr = levelStr
  tparamGrowup.vesselsType = vesselStr
  tparamGrowup.vesselsFrame = vesselFrame
  tparamGrowup.colorFrame = colorFrame
  tparamGrowup.avatarFrame = avatarFrame
  tparamGrowup.avatarName = avatarName
  tparamGrowup.shipFrame = avatarShip
  tparamGrowup.sex = sex
  flash:InvokeASCallback("_root", "SetUpGrowUpFleetCard", tparamGrowup)
end
function shenjie:SetRequrieItems()
  local flash = self:GetFlashObject()
  if not flash then
    return
  end
  local requrieItems = shenjie.mCurrentFleetEnhanceData:GetGrowUpRequireList()
  local tparams = {
    listparam = {}
  }
  for i, v in ipairs(requrieItems) do
    local itemName = GameHelper:GetAwardTypeText(v.item_type, v.number)
    local itemRequrieNumber = GameHelper:GetAwardCount(v.item_type, v.number, v.no)
    local itemRealNum = 0
    if GameHelper:IsResource(v.item_type) then
      local resource = GameGlobalData:GetData("resource")
      itemRealNum = resource[v.item_type]
    else
      local items = GameGlobalData:GetData("item_count")
      for iItem, vItem in ipairs(items.items) do
        if vItem.item_id == v.number then
          itemRealNum = vItem.item_no
        end
      end
    end
    if tonumber(itemRealNum) < tonumber(itemRequrieNumber) then
      itemRealNum = "<font color='#FF0000'>" .. GameUtils.numberConversion(itemRealNum) .. "</font>"
    else
      itemRealNum = "<font color='#28D159'>" .. GameUtils.numberConversion(itemRealNum) .. "</font>"
    end
    local tinfo = GameHelper:GetItemInfo(v)
    local t = {}
    t.itemName = itemName
    t.realNum = itemRealNum
    t.reqNum = GameUtils.numberConversion(itemRequrieNumber)
    t.itemframe = tinfo.icon_frame
    t.itempic = tinfo.icon_pic
    t.strinfo = LuaUtils:serializeTable(v)
    table.insert(tparams.listparam, t)
  end
  flash:InvokeASCallback("_root", "SetRequrieItems", tparams)
end
function shenjie:ShenJieMeterialEnough()
  local requrieItems = shenjie.mCurrentFleetEnhanceData:GetGrowUpRequireList()
  for i, v in ipairs(requrieItems) do
    local itemRequrieNumber = GameHelper:GetAwardCount(v.item_type, v.number, v.no)
    local itemRealNum = 0
    if GameHelper:IsResource(v.item_type) then
      local resource = GameGlobalData:GetData("resource")
      itemRealNum = resource[v.item_type]
    else
      local items = GameGlobalData:GetData("item_count")
      for iItem, vItem in ipairs(items.items) do
        if vItem.item_id == v.number then
          itemRealNum = vItem.item_no
        end
      end
    end
    if tonumber(itemRealNum) < tonumber(itemRequrieNumber) then
      return false
    end
  end
  return true
end
function shenjie:SetRequriePercent()
  local flash = self:GetFlashObject()
  if not flash then
    return
  end
  local probability, probabilityStep = shenjie.mCurrentFleetEnhanceData:GetGrowupProbability()
  if probability >= 100 then
    probability = 100
  end
  local percentStr = probability .. "%"
  if probability <= 0 then
    probability = 0
  elseif probability >= 100 then
    probability = 100
  end
  probability = math.ceil(probability)
  percentStepStr = GameLoader:GetGameText("LC_MENU_REFORM_REQUIRE_ENERGY_DESC")
  percentStepStr = string.format(percentStepStr, math.ceil(probabilityStep) .. " %")
  probability = probability + 1
  if probabilityStep == 0 then
    percentStepStr = ""
  end
  flash:InvokeASCallback("_root", "SetRequrieMenuProbability", probability, percentStr, percentStepStr)
end
function shenjie:SetPropDetailList()
  local flash = self:GetFlashObject()
  if not flash then
    return
  end
  local attriData = self.mCurrentFleetEnhanceData:GetFleetAttriDiffData()
  local attriNum = #attriData.mAttriNames
  local tparam = {
    listparam = {}
  }
  for itemIndex = 1, attriNum do
    local itemName = attriData.mAttriNames[itemIndex]
    local itemCurValue = attriData.mCurLevelAttriValue[itemIndex]
    local itemNextValue = attriData.mNextLevelAttriValue[itemIndex]
    if self.mCurrentFleetEnhanceData:IsPercentAttri(attriData.mAttriIDs[itemIndex]) then
      itemCurValue = string.format("%.02f", itemCurValue) .. "%"
      itemNextValue = string.format("%.02f", itemNextValue) .. "%"
    else
      itemCurValue = GameUtils.numberConversion(tonumber(itemCurValue))
      itemNextValue = GameUtils.numberConversion(tonumber(itemNextValue))
    end
    local t = {}
    t.itemName = itemName
    t.itemCurValue = itemCurValue
    t.itemNextValue = itemNextValue
    table.insert(tparam.listparam, t)
  end
  flash:InvokeASCallback("_root", "InitPropDetail", tparam)
end
function shenjie:RequestLevelUp(fleetID, isCheck)
  local content = {fleet_id = fleetID}
  if isCheck then
    NetMessageMgr:SendMsg(NetAPIList.fleet_can_enhance_req.Code, content, self.RequestLevelUpCallback, true, nil)
    GameUtils:RecordForTongdui(NetAPIList.fleet_can_enhance_req.Code, content, self.RequestLevelUpCallback, true, nil)
  else
    NetMessageMgr:SendMsg(NetAPIList.fleet_enhance_req.Code, content, self.RequestLevelUpCallback, true, nil)
    GameUtils:RecordForTongdui(NetAPIList.fleet_enhance_req.Code, content, self.RequestLevelUpCallback, true, nil)
  end
end
function shenjie:GetParam_successMenuCard()
  local fleetCard = shenjie.mCurrentFleetEnhanceData:GetFleetCardData()
  local curCard = fleetCard.mCurFleetCard
  local levelStr = GameDataAccessHelper:GetFleetLevelStrWithColor(curCard.level, curCard.color)
  local vesselFrame = FleetDataAccessHelper:GetFleetVesselsFrameByVessel(curCard.vessels)
  local vesselStr = FleetDataAccessHelper:GetFleetVesselsTypeStringByVessel(curCard.vessels)
  local colorFrame = FleetDataAccessHelper:GetFleetColorFrameByColor(curCard.color)
  local avatarName = FleetDataAccessHelper:GetFleetColorDisplayName(curCard.name, curCard.fleet_id, curCard.color)
  local avatarShip = GameDataAccessHelper:GetShip(curCard.fleet_id, nil, curCard.level)
  if curCard.fleet_id == 1 then
    avatarShip = GameDataAccessHelper:GetShip(GameFleetEquipment:GetLeaderFleet(), nil, curCard.level)
  end
  local sexl = GameDataAccessHelper:GetCommanderSex(curCard.fleet_id)
  local avatarFrame = GameDataAccessHelper:GetFleetAvatar(curCard.fleet_id, curCard.level, sexl)
  if curCard.fleet_id == 1 then
    local tempid = GameFleetEquipment:GetLeaderFleet()
    avatarFrame = GameDataAccessHelper:GetFleetAvatar(tempid, curCard.level)
  end
  if sexl == 1 then
    sexl = "man"
  elseif sexl == 0 then
    sexl = "woman"
  else
    sexl = "unknown"
  end
  local tcurcard = {}
  tcurcard.levelStr = levelStr
  tcurcard.vesselsType = vesselStr
  tcurcard.vesselsFrame = vesselFrame
  tcurcard.colorFrame = colorFrame
  tcurcard.avatarFrame = avatarFrame
  tcurcard.avatarName = avatarName
  tcurcard.shipFrame = avatarShip
  tcurcard.sex = sexl
  curCard = fleetCard.mNextFleetCard
  local levelStr = GameDataAccessHelper:GetFleetLevelStrWithColor(curCard.level, curCard.color)
  vesselFrame = FleetDataAccessHelper:GetFleetVesselsFrameByVessel(curCard.vessels)
  vesselStr = FleetDataAccessHelper:GetFleetVesselsTypeStringByVessel(curCard.vessels)
  colorFrame = FleetDataAccessHelper:GetFleetColorFrameByColor(curCard.color)
  avatarName = FleetDataAccessHelper:GetFleetColorDisplayName(curCard.name, curCard.fleet_id, curCard.color)
  avatarShip = GameDataAccessHelper:GetShip(curCard.fleet_id, nil, curCard.level)
  if curCard.fleet_id == 1 then
    avatarShip = GameDataAccessHelper:GetShip(GameFleetEquipment:GetLeaderFleet(), nil, curCard.level)
  end
  local sexr = GameDataAccessHelper:GetCommanderSex(curCard.fleet_id)
  avatarFrame = GameDataAccessHelper:GetFleetAvatar(curCard.fleet_id, curCard.level, sexr)
  if curCard.fleet_id == 1 then
    local tempid = GameFleetEquipment:GetLeaderFleet()
    avatarFrame = GameDataAccessHelper:GetFleetAvatar(tempid, curCard.level)
  end
  if sexr == 1 then
    sexr = "man"
  elseif sexr == 0 then
    sexr = "woman"
  else
    sexr = "unknown"
  end
  local tnextcard = {}
  tnextcard.levelStr = levelStr
  tnextcard.vesselsType = vesselStr
  tnextcard.vesselsFrame = vesselFrame
  tnextcard.colorFrame = colorFrame
  tnextcard.avatarFrame = avatarFrame
  tnextcard.avatarName = avatarName
  tnextcard.shipFrame = avatarShip
  tnextcard.sex = sexl
  return tcurcard, tnextcard
end
function shenjie:GetSuccessPropListParam()
  local tparam = {}
  local attriData = self.mCurrentFleetEnhanceData:GetFleetAttriDiffData()
  local attriNum = #attriData.mAttriNames
  for itemIndex = 1, attriNum do
    local attriData = self.mCurrentFleetEnhanceData:GetFleetAttriDiffData()
    local itemName = attriData.mAttriNames[itemIndex]
    local itemCurValue = attriData.mCurLevelAttriValue[itemIndex]
    local itemAddValue = attriData.mNextLevelAttriValue[itemIndex] - attriData.mCurLevelAttriValue[itemIndex]
    if self.mCurrentFleetEnhanceData:IsPercentAttri(attriData.mAttriIDs[itemIndex]) then
      itemCurValue = string.format("%.02f", itemCurValue) .. "%"
      itemAddValue = string.format("%.02f", itemAddValue) .. "%"
    else
      itemCurValue = GameUtils.numberConversion(tonumber(itemCurValue))
      itemAddValue = GameUtils.numberConversion(tonumber(itemAddValue))
    end
    local t = {}
    t.itemName = itemName
    t.itemCurValue = itemCurValue
    t.itemAddValue = itemAddValue
    table.insert(tparam, t)
  end
  return tparam
end
function shenjie:ShowBreakthroughPanel()
  local tparam = {}
  tparam.curcard, tparam.nextcard = shenjie:GetParam_successMenuCard()
  tparam.propparam = shenjie:GetSuccessPropListParam()
  local popflash = GameStateEquipEnhance:GetPop()
  popflash:shenjie_showEvolutionPanel(tparam)
end
function shenjie:ShowGrowupSuccess()
  local flash = self:GetFlashObject()
  if not flash then
    return
  end
  if (immanentversion170 == 4 or immanentversion170 == 5) and QuestTutorialFirstReform:IsActive() and shenjie.mFleetIdentify == 1 then
    QuestTutorialFirstReform:SetFinish(true, false)
    flash:InvokeASCallback("_root", "ShowReformTutorial", false)
  end
  local tparam = {}
  tparam.curcard, tparam.nextcard = shenjie:GetParam_successMenuCard()
  tparam.propparam = shenjie:GetSuccessPropListParam()
  local popflash = GameStateEquipEnhance:GetPop()
  popflash:shenjie_showGrowup(tparam)
end
function shenjie.RequestLevelUpCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and (content.api == NetAPIList.fleet_enhance_req.Code or NetAPIList.fleet_can_enhance_req.Code == content.api) then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      if content.code == 914 then
        shenjie:SetRequrieItems()
        local probability, probabilityStep = shenjie.mCurrentFleetEnhanceData:GetGrowupProbability()
        local newProbability = probability + probabilityStep
        if newProbability > 100 then
          newProbability = 100
        end
        shenjie.mCurrentFleetEnhanceData:SetGrowupProbability(newProbability)
        shenjie:SetRequriePercent()
      end
    elseif NetAPIList.fleet_can_enhance_req.Code == content.api then
      shenjie:ShowBreakthroughPanel()
    else
      shenjie:ShowGrowupSuccess()
    end
    return true
  end
  return false
end
function shenjie:CanEvolve(level)
  print("shenjie:CanEvolve " .. tostring(level) .. " " .. tostring(shenjie:IsMaxLevel()))
  if level == 15 or level == 31 then
    return true
  end
  return false
end
function shenjie:IsMaxLevel(level)
  if level == self.mMaxLevel then
    return true
  end
  return false
end
function shenjie:SetUpMenu()
  local flash = self:GetFlashObject()
  if not flash then
    return
  end
  flash:InvokeASCallback("_root", "setLevelUpButtonEnable", true)
  flash:InvokeASCallback("_root", "setLevelUpButtonText", GameLoader:GetGameText("LC_MENU_REFORM_BUTTON"))
  flash:InvokeASCallback("_root", "setLevelDownButtonEnable", true)
  if self:CanEvolve(self.mCurrentFleetEnhanceData.mCurLevel) then
    flash:InvokeASCallback("_root", "setLevelUpButtonText", GameLoader:GetGameText("LC_MENU_REFORMING_EVOLUTION"))
  end
  if self:IsMaxLevel(self.mCurrentFleetEnhanceData.mCurLevel) then
    flash:InvokeASCallback("_root", "setLevelUpButtonEnable", false)
  end
  if self.mCurrentFleetEnhanceData:IsMinLevel() then
    flash:InvokeASCallback("_root", "setLevelDownButtonEnable", false)
  end
  if immanentversion170 == 4 or immanentversion170 == 5 then
    if QuestTutorialFirstReform:IsActive() and shenjie.mFleetIdentify == 1 then
      flash:InvokeASCallback("_root", "ShowReformTutorial", true)
    else
      flash:InvokeASCallback("_root", "ShowReformTutorial", false)
    end
  end
end
