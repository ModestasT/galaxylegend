local GameUIStarSystemPort = LuaObjectManager:GetLuaObject("GameUIStarSystemPort")
local GameUIWDStuff = LuaObjectManager:GetLuaObject("GameUIWDStuff")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameUIStarSystemFuben = LuaObjectManager:GetLuaObject("GameUIStarSystemFuben")
local GameUIStarSystemHandbook = LuaObjectManager:GetLuaObject("GameUIStarSystemHandbook")
local GameUIStarSystemPort_SUB = LuaObjectManager:GetLuaObject("GameUIStarSystemPort_SUB")
local GameStateStarSystem = GameStateManager.GameStateStarSystem
GameUIStarSystemPort.allMedalInfos = {}
GameUIStarSystemPort.curSelect = ""
GameUIStarSystemPort.isPlayTutorial = false
local g_menu_config = {
  {
    ntype = 1,
    strid = "onBtn_chouka",
    strname = "LC_MENU_NAME_MEDAL_GLORY_ROAD",
    strinfo = "LC_MENU_DESC_MEDAL_GLORY_ROAD",
    showTuto = false
  },
  {
    ntype = 1,
    strid = "onBtn_shangdian",
    strname = "LC_MENU_NAME_MEDAL_STORE",
    strinfo = "LC_MENU_DESC_MEDAL_STORE",
    showTuto = false
  },
  {
    ntype = 1,
    strid = "onBtn_tujian",
    strname = "LC_MENU_NAME_MEDAL_GLORY_WALL",
    strinfo = "LC_MENU_DESC_MEDAL_GLORY_WALL",
    showTuto = false
  },
  {
    ntype = 1,
    strid = "onBtn_huishou",
    strname = "LC_MENU_NAME_MEDAL_RECYCLE",
    strinfo = "LC_MENU_DESC_MEDAL_RECYCLE",
    showTuto = false
  },
  {
    ntype = 1,
    strid = "onBtn_fuben",
    strname = "LC_MENU_NAME_MEDAL_BOSS",
    strinfo = "LC_MENU_DESC_MEDAL_BOSS",
    showTuto = false
  }
}
function GameUIStarSystemPort:onSelect(thecmd)
  GameUIStarSystemPort.curSelect = thecmd
  GameUIStarSystemPort:onMedalConfirm()
end
function GameUIStarSystemPort:onMedalConfirm()
  local thecmd = GameUIStarSystemPort.curSelect
  if thecmd == "onBtn_tujian" then
    GameStateManager:GetCurrentGameState():EraseObject(self)
    GameUIStarSystemHandbook:Show()
  elseif thecmd == "onBtn_fuben" then
    GameUIStarSystemFuben:Show()
  else
    self:GetFlashObject():InvokeASCallback("_root", "HidePort")
    GameStateStarSystem:AddObject(GameUIStarSystemPort_SUB)
    GameStateStarSystem:ForceCompleteCammandList()
    function callback(...)
      GameStateStarSystem:EraseObject(GameUIStarSystemPort_SUB)
      GameStateStarSystem:ForceCompleteCammandList()
    end
    function callback_chouka(...)
      callback()
      GameUIStarSystemPort:Show("onBtn_chouka")
    end
    function callback_shangdian(...)
      callback()
      GameUIStarSystemPort:Show("onBtn_shangdian")
    end
    function callback_huishou(...)
      callback()
      GameUIStarSystemPort:Show("onBtn_huishou")
    end
    if thecmd == "onBtn_chouka" then
      GameUIStarSystemPort.gacha:Show(callback_chouka)
    elseif thecmd == "onBtn_shangdian" then
      GameUIStarSystemPort.uiStore:Show(callback_shangdian)
    elseif thecmd == "onBtn_huishou" then
      GameUIStarSystemPort.uiRecycle:Show("resolve", nil, callback_huishou)
    else
      assert(false, thecmd)
    end
  end
end
function GameUIStarSystemPort:OnFSCommand(cmd, arg)
  if GameUtils:OnFSCommand(cmd, arg, self) then
    return
  end
  if cmd == "onBtn_tujian" then
    GameUIStarSystemPort:onSelect(cmd)
  elseif cmd == "onBtn_fuben" then
    GameUIStarSystemPort:onSelect(cmd)
  elseif cmd == "onBtn_chouka" then
    GameUIStarSystemPort:GetFlashObject():InvokeASCallback("_root", "HideAllTuto")
    GameUIStarSystemPort:onSelect(cmd)
  elseif cmd == "onBtn_huishou" then
    GameUIStarSystemPort:onSelect(cmd)
  elseif cmd == "onBtn_shangdian" then
    GameUIStarSystemPort:onSelect(cmd)
  elseif cmd == "onBtn_medalConfirm" then
    if TutorialQuestManager.QuestTutorialEnterStarSystem:IsActive() and GameGlobalData:GetModuleStatus("medal") then
      self:GetFlashObject():InvokeASCallback("_root", "showMenuTip", false)
      GameUIStarSystemPort.isPlayGachaTutorial = true
    end
    GameUIStarSystemPort:onMedalConfirm()
  elseif cmd == "onBtn_medalHelp" then
    GameUIWDStuff:Show(GameUIWDStuff.menuType.menu_type_help)
    GameUIWDStuff:SetHelpText(GameLoader:GetGameText("LC_HELP_MEDAL_SYSTEM"))
  elseif cmd == "onBtn_close" then
    self:GetFlashObject():InvokeASCallback("_root", "showMenuCloseTip", false)
    GameUIStarSystemPort:MoveOut()
  elseif cmd == "onRecycleHelp" then
    GameUIWDStuff:Show(GameUIWDStuff.menuType.menu_type_help)
    GameUIWDStuff:SetHelpText(GameLoader:GetGameText("LC_HELP_MEDAL_RECYCLE"))
  elseif cmd == "onShopHelp" then
    GameUIWDStuff:Show(GameUIWDStuff.menuType.menu_type_help)
    GameUIWDStuff:SetHelpText(GameLoader:GetGameText("LC_HELP_MEDAL_STORE"))
  elseif cmd == "onResolveitem" then
    GameUIStarSystemPort.uiRecycle:onResolveitem(cmd, arg)
  elseif cmd == "onResolveSelect" then
    GameUIStarSystemPort.uiRecycle:onResolveSelect(cmd, arg)
  elseif cmd == "onResetitem" then
    GameUIStarSystemPort.uiRecycle:onResetitem(cmd, arg)
  elseif cmd == "onIconitem" then
    GameUIStarSystemPort.uiRecycle:onIconitem(cmd, arg)
  elseif cmd == "onResetItemIcons" then
    GameUIStarSystemPort.uiRecycle:onResetItemIcons(cmd, arg)
  elseif cmd == "onResetResultIcons" then
    GameUIStarSystemPort.uiRecycle:onResetItemIcons(cmd, arg)
  elseif cmd == "onResetCancel" then
    GameUIStarSystemPort.uiRecycle:onResetCancel(cmd, arg)
  elseif cmd == "onResetConfirm" then
    GameUIStarSystemPort.uiRecycle:onResetConfirm(cmd, arg)
  elseif cmd == "onRecycleTab1" then
    GameUIStarSystemPort.uiRecycle:ShowReset(cmd, arg)
  elseif cmd == "onRecycleTab0" then
    GameUIStarSystemPort.uiRecycle:ShowResolve()
  elseif cmd == "onRecycleChoice" then
    GameUIStarSystemPort.uiRecycle:onRecycleChoice(cmd, arg)
  elseif cmd == "onResolveBtnChoice" then
    GameUIStarSystemPort.uiRecycle:onResolveBtnChoice(cmd, arg)
  elseif cmd == "onRecycleResolveConfirm" then
    GameUIStarSystemPort.uiRecycle:onRecycleResolveConfirm(cmd, arg)
  elseif cmd == "onRecycleHide" then
    GameUIStarSystemPort.uiRecycle:Hide()
  elseif cmd == "onShopBuy" then
    GameUIStarSystemPort.uiStore:onShopBuy(cmd, arg)
  elseif cmd == "onShopIcon" then
    GameUIStarSystemPort.uiStore:onShopIcon(cmd, arg)
  elseif cmd == "needUpdateShopItem" then
    GameUIStarSystemPort.uiStore:needUpdateShopItem(cmd, arg)
  elseif cmd == "onBtnClose_store" then
    GameUIStarSystemPort.uiStore:Hide()
    if GameUIStarSystemHandbook.gotoPrev then
      GameUIStarSystemHandbook.gotoPrev = false
      GameUIStarSystemHandbook:ComeBack()
    end
  elseif cmd == "onShopRefresh" then
    GameUIStarSystemPort.uiStore.RequestRefreshData()
  elseif cmd == "onShopGet" then
    GameUIStarSystemPort.uiStore:onShopGet()
  else
    GameUIStarSystemPort.gacha:OnFSCommand(cmd, arg)
    GameUIStarSystemPort.medalEquip:OnFSCommand(cmd, arg)
  end
end
function GameUIStarSystemPort:OnAddToGameState()
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  self:GetFlashObject():InvokeASCallback("_root", "setLocalText", "TEXT_EQUIP", GameLoader:GetGameText("LC_MENU_MEDAL_EQUIP"))
  self:GetFlashObject():InvokeASCallback("_root", "setLocalText", "TEXT_LEVEL_MENU_TITLE", GameLoader:GetGameText("LC_MENU_NAME_MEDAL_LEVEL_UP"))
  self:GetFlashObject():InvokeASCallback("_root", "setLocalText", "TEXT_RANK_MENU_TITLE", GameLoader:GetGameText("LC_MENU_NAME_MEDAL_RANK_UP"))
  self:GetFlashObject():InvokeASCallback("_root", "setLocalText", "TEXT_LEVEL_FULL", GameLoader:GetGameText("LC_MENU_MEDAL_LEVEL_LEVEL_MAX"))
  self:GetFlashObject():InvokeASCallback("_root", "setLocalText", "TEXT_RANK_FULL", GameLoader:GetGameText("LC_MENU_MEDAL_LEVEL_RANK_MAX"))
  self:GetFlashObject():InvokeASCallback("_root", "setLocalText", "TEXT_LEVEL_UP_BTN", GameLoader:GetGameText("LC_MENU_MEDAL_LEVEL_UP"))
  self:GetFlashObject():InvokeASCallback("_root", "setLocalText", "TEXT_RANK_UP_BTN", GameLoader:GetGameText("LC_MENU_MEDAL_RANK_UP"))
  self:GetFlashObject():InvokeASCallback("_root", "setLocalText", "TEXT_RECRUIT", GameLoader:GetGameText("LC_MENU_NAME_MEDAL_GLORY_ROAD"))
  self:GetFlashObject():InvokeASCallback("_root", "setLocalText", "TEXT_RECRUIT_OVER", GameLoader:GetGameText("LC_MENU_MEDAL_RECRUIT_SUCCESS"))
  self:GetFlashObject():InvokeASCallback("_root", "setLocalText", "MEDAL_EXCHANGE", GameLoader:GetGameText("LC_MENU_MEDAL_EXCHANGE"))
  GameGlobalData:RegisterDataChangeCallback("resource", self.RefreshResource)
  if GameGlobalData:GetModuleStatus("medal_store") then
    g_menu_config[2].ntype = 1
  else
    g_menu_config[2].ntype = 0
  end
  if GameGlobalData:GetModuleStatus("medal_recruit") then
    g_menu_config[1].ntype = 1
  else
    g_menu_config[1].ntype = 0
  end
  if GameGlobalData:GetModuleStatus("medal_recycle") then
    g_menu_config[4].ntype = 1
  else
    g_menu_config[4].ntype = 0
  end
  for _, v in pairs(g_menu_config) do
    if v.ntype == 0 then
      v.strname = GameUtils:TryGetText("LC_MENU_NAME_MEDAL_CONTINUED", "qidai")
    else
      v.strname = GameUtils:TryGetText(v.strname)
    end
    v.strinfo = GameUtils:TryGetText(v.strinfo)
    v.showTuto = false
  end
  if (ext.crc32.crc32("data2/LAZY_LOAD_newEnhance_bg.png") == "" or ext.crc32.crc32("data2/LAZY_LOAD_newEnhance_bg.png") == nil) and not AutoUpdateInBackground:IsHeroFileUpdatedToServer("data2/LAZY_LOAD_newEnhance_bg.png") then
    self:GetFlashObject():ReplaceTexture("LAZY_LOAD_newEnhance_bg.png", "territorial_map_bg.png")
  end
end
function GameUIStarSystemPort.RefreshResource()
  DebugOut("GameUIStarSystemPort:RefreshResource")
  GameUIStarSystemPort.gacha.SetCreditAndMoney()
end
function GameUIStarSystemPort:OnEraseFromGameState(game_state)
  self:UnloadFlashObject()
end
function GameUIStarSystemPort:Update(dt)
  local flash_obj = self:GetFlashObject()
  if flash_obj == nil then
    return
  end
  flash_obj:Update(dt)
  GameUIStarSystemPort.gacha:OnGachaUpdate(dt)
  GameUIStarSystemPort.medalEquip:OnMedalEquipUpdate(dt)
  flash_obj:InvokeASCallback("_root", "OnUpdate", dt)
end
function GameUIStarSystemPort:Show(btn_front)
  local flash_obj = self:GetFlashObject()
  local tparam = {}
  if btn_front then
    local idx = -1
    for k, v in ipairs(g_menu_config) do
      if v.strid == btn_front then
        idx = k
        break
      end
    end
    assert(idx ~= -1, btn_front)
    local len = #g_menu_config
    for i = 1, len do
      local newidx = idx - 1 + i
      if newidx > 5 then
        newidx = newidx - 5
      end
      tparam[i] = g_menu_config[newidx]
    end
  else
    tparam = g_menu_config
  end
  flash_obj:InvokeASCallback("_root", "showMenuCloseTip", false)
  DebugOut("what's wrong")
  for _, v in ipairs(tparam) do
    if v.strid == "onBtn_chouka" then
      v.showTuto = TutorialQuestManager.QuestTutorialEnterStarSystem:IsActive() and GameGlobalData:GetModuleStatus("medal")
    end
    if v.strid == "onBtn_tujian" then
      v.showTuto = TutorialQuestManager.QuestTutorialStarSystemTujian:IsActive()
    end
  end
  DebugOut("after")
  DebugTable(tparam)
  flash_obj:InvokeASCallback("_root", "ShowPort", tparam)
  DebugOut("self:SetRedPoint")
  DebugOut(self.ifred)
  self:SetRedPoint("onBtn_tujian", self.ifred)
  if TutorialQuestManager.QuestTutorialStarSystemTujian:IsActive() then
    local GameUICommonDialog = LuaObjectManager:GetLuaObject("GameUICommonDialog")
    GameUICommonDialog:PlayStory({9821})
  end
end
function GameUIStarSystemPort:JumpTo(strtype, close_callback)
  GameStateStarSystem:EraseObject(self)
  GameStateStarSystem:AddObject(GameUIStarSystemPort_SUB)
  GameStateStarSystem:ForceCompleteCammandList()
  function callback(...)
    GameStateStarSystem:EraseObject(GameUIStarSystemPort_SUB)
    print("JumpTo_callback")
    GameStateStarSystem:ForceCompleteCammandList()
  end
  if strtype == "shop" then
    local function close_callback_shop(...)
      callback()
      close_callback()
    end
    GameUIStarSystemPort.uiStore:Show(close_callback_shop)
  elseif strtype == "chouka" then
    local function close_callback_chouka(...)
      callback()
      close_callback()
    end
    GameUIStarSystemPort.gacha:Show(close_callback_chouka)
  elseif strtype == "boss" then
    local function close_callback_boss(...)
      callback()
      close_callback()
    end
    GameUIStarSystemFuben:Show(nil, close_callback_boss)
  else
    assert(false, strtype)
  end
end
function GameUIStarSystemPort:MoveOut()
  GameStateManager:SetCurrentGameState(GameStateManager.GameStateMainPlanet)
end
function GameUIStarSystemPort:OnMedalListNTF(content)
  if GameUIStarSystemPort.medal_list_req_ing then
    for _, v in pairs(content.medals) do
      table.insert(GameUIStarSystemPort.allMedalInfos, v)
    end
  end
end
function GameUIStarSystemPort:RefreshRedPoint()
  DebugOut("100000012 ")
  DebugTable(GameGlobalData.redPointStates.states)
  if GameGlobalData.redPointStates then
    for k, v in pairs(GameGlobalData.redPointStates.states) do
      if v.key == 100000012 then
        DebugOut("onBtn_tujian")
        DebugOut(v.value == 1 and true or false)
        self.ifred = v.value == 1 and true or false
        self:SetRedPoint("onBtn_tujian", self.ifred)
      end
    end
  end
end
function GameUIStarSystemPort:SetRedPoint(strid, visible)
  local flash_obj = self:GetFlashObject()
  if flash_obj == nil then
    return
  end
  flash_obj:InvokeASCallback("_root", "SetRedPoint", strid, visible)
end
function GameUIStarSystemPort:GetAllMedalInfo(callback)
  function netcallback(msgtype, content)
    if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.medal_list_req.Code then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      return true
    elseif msgtype == NetAPIList.medal_list_ack.Code then
      GameUIStarSystemPort.medal_list_req_ing = false
      if callback then
        callback()
      end
      return true
    end
    return false
  end
  GameUIStarSystemPort.allMedalInfos = {}
  GameUIStarSystemPort.medal_list_req_ing = true
  NetMessageMgr:SendMsg(NetAPIList.medal_list_req.Code, nil, netcallback, true, nil)
end
if AutoUpdate.isAndroidDevice then
  function GameUIStarSystemPort.OnAndroidBack()
    GameUIStarSystemPort:MoveOut()
  end
end
