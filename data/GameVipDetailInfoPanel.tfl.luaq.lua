local GameVipDetailInfoPanel = LuaObjectManager:GetLuaObject("GameVipDetailInfoPanel")
local GameUIBarLeft = LuaObjectManager:GetLuaObject("GameUIBarLeft")
local GameUIActivity = LuaObjectManager:GetLuaObject("GameUIActivity")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local GameUIKrypton = LuaObjectManager:GetLuaObject("GameUIKrypton")
local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
local GameStateGlobalState = GameStateManager.GameStateGlobalState
local GameObjectLevelUp = LuaObjectManager:GetLuaObject("GameObjectLevelUp")
local GameUIActivityNew = LuaObjectManager:GetLuaObject("GameUIActivityNew")
VIP_PANEL_TYPE = {
  VIP_PANEL_TYPE_DETAIL = 1,
  VIP_PANEL_TYPE_LEVEL_UP = 2,
  VIP_PANLE_TYPE_TMP_VIP = 3
}
VIP_EVENT = {VIP_SHOW_DETAIL = 1, VIP_SHOW_TEMP = 2}
function GameVipDetailInfoPanel:CheckShowVipEvent()
  if GameVipDetailInfoPanel.mVipEventList and #GameVipDetailInfoPanel.mVipEventList > 0 then
    DebugOut("GameVipDetailInfoPanel.mVipEventList = ")
    DebugTable(GameVipDetailInfoPanel.mVipEventList)
    local event = GameVipDetailInfoPanel.mVipEventList[1]
    table.remove(GameVipDetailInfoPanel.mVipEventList, 1)
    DebugOut("GameVipDetailInfoPanel.mVipEventList after remove = ")
    DebugTable(GameVipDetailInfoPanel.mVipEventList)
    if event then
      if event.type == VIP_EVENT.VIP_SHOW_DETAIL then
        GameVipDetailInfoPanel:InitPanel(event.info)
      elseif event.type == VIP_EVENT.VIP_SHOW_TEMP then
        GameVipDetailInfoPanel.mTmpInfo = event.info
        GameVipDetailInfoPanel:InitTmpVipInfo()
      end
    end
  end
end
function GameVipDetailInfoPanel:CheckShowTempVip()
  if GameVipDetailInfoPanel.mTempVipEvent then
    GameVipDetailInfoPanel:InitTmpVipInfo()
  end
end
function GameVipDetailInfoPanel:OnInitGame()
  DebugOut("On init game GameVipDetailInfoPanel")
  GameGlobalData:RegisterDataChangeCallback("vipinfo", self.onGlobalVipChange)
  NetMessageMgr:RegisterMsgHandler(NetAPIList.vip_level_ntf.Code, GameVipDetailInfoPanel.vipExpListNtf)
  NetMessageMgr:RegisterMsgHandler(NetAPIList.temp_vip_ntf.Code, GameVipDetailInfoPanel.tmpVipInfo)
  GameVipDetailInfoPanel.mVipEventList = {}
end
function GameVipDetailInfoPanel.onGlobalVipChange()
  DebugOut("hello world vip info change!!!")
  local function callback()
    local vipInfo = GameGlobalData:GetData("vipinfo")
    DebugOut("vipInfo = ")
    DebugTable(vipInfo)
    if not vipInfo.pay_act then
      if not GameVipDetailInfoPanel.mInitVipLevel then
        GameVipDetailInfoPanel.mInitVipLevel = vipInfo.level
        GameVipDetailInfoPanel.mInitVipExp = vipInfo.current_exp
      end
    else
      vipInfo.pay_act = false
      DebugOut("second time")
      if GameStateManager:GetCurrentGameState() ~= GameStateManager.GameStateMainPlanet or GameStateManager:GetCurrentGameState():IsObjectInState(GameVipDetailInfoPanel) or GameStateManager:GetCurrentGameState():IsObjectInState(GameUIActivityNew) and not GameStateManager:GetCurrentGameState():IsObjectInState(GameUIActivity) then
        local event = {}
        event.info = vipInfo
        event.type = VIP_EVENT.VIP_SHOW_DETAIL
        if #GameVipDetailInfoPanel.mVipEventList > 0 then
          local preEvent = GameVipDetailInfoPanel.mVipEventList[1]
          preEvent.info = vipInfo
        else
          DebugOut("insert time")
          table.insert(GameVipDetailInfoPanel.mVipEventList, event)
        end
      else
        GameVipDetailInfoPanel:InitPanel(vipInfo)
      end
    end
  end
  if GameStateManager:GetCurrentGameState():IsObjectInState(ItemBox) and ItemBox.currentBox == "rewardInfo" then
    ItemBox:SetDelayShowAfterReward(callback, nil)
  else
    callback()
  end
end
function GameVipDetailInfoPanel:GetVipRewards()
end
function GameVipDetailInfoPanel.vipExpListNtf(content)
  GameVipDetailInfoPanel.mVipExpList = content.vip_level_info
  table.sort(GameVipDetailInfoPanel.mVipExpList, function(v1, v2)
    return v1.level < v2.level
  end)
  DebugOut("GameVipDetailInfoPanel.mVipExpList = ")
  DebugTable(GameVipDetailInfoPanel.mVipExpList)
end
function GameVipDetailInfoPanel.tmpVipInfo(content)
  GameVipDetailInfoPanel.mTmpInfo = content
  GameVipDetailInfoPanel.mTmpInfoFetchTime = os.time()
  if content.left_time <= 0 then
    GameVipDetailInfoPanel.mTmpInfo = nil
    GameVipDetailInfoPanel.mTmpInfoFetchTime = nil
  end
  DebugOut("set ui bar left to temp vip level")
  GameUIBarLeft:SetVipInfo()
  if content.show_banner then
    if GameStateGlobalState:IsObjectInState(GameObjectLevelUp) or GameObjectLevelUp.willDisplay then
      local event = {}
      event.info = content
      event.type = VIP_EVENT.VIP_SHOW_TEMP
      GameVipDetailInfoPanel.mTempVipEvent = event
    else
      GameVipDetailInfoPanel:InitTmpVipInfo()
    end
  end
end
function GameVipDetailInfoPanel:InitTmpVipInfo()
  DebugOut("show init tmp vip info")
  self:InitVipDesc()
  GameVipDetailInfoPanel.mTempVipEvent = nil
  if not GameUIBarLeft.vipList then
    NetMessageMgr:SendMsg(NetAPIList.vip_loot_req.Code, nil, GameVipDetailInfoPanel.RequestVipTempCallback, false, nil)
  else
    GameVipDetailInfoPanel:Show(VIP_PANEL_TYPE.VIP_PANLE_TYPE_TMP_VIP)
    GameVipDetailInfoPanel:SetUpTmpVipInfo()
  end
end
function GameVipDetailInfoPanel:SetUpTmpVipInfo()
  local level = self.mTmpInfo.level
  local vipDesc = {}
  local vipTime = 0
  local vipTimeStr = ""
  if GameVipDetailInfoPanel.mTmpInfoFetchTime and 0 < GameVipDetailInfoPanel.mTmpInfoFetchTime then
    vipTime = GameVipDetailInfoPanel.mTmpInfo.left_time - (os.time() - GameVipDetailInfoPanel.mTmpInfoFetchTime)
  end
  if vipTime > 0 then
    vipTimeStr = math.ceil(vipTime / 60 / 60) .. ""
  end
  vipDesc.desc = string.format(GameLoader:GetGameText("LC_MENU_TEMPORARY_VIP_INFO"), level, vipTimeStr)
  vipDesc.detail = self.mGameVipDesc[level]
  vipDesc.title = string.format(GameLoader:GetGameText("LC_MENU_VIP_TITLE"), level)
  self:GetFlashObject():InvokeASCallback("_root", "setTmpVipInfo", level, vipDesc)
end
function GameVipDetailInfoPanel.RequestVipTempCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.vip_loot_req.Code then
    return true
  elseif msgType == NetAPIList.vip_loot_ack.Code then
    GameUIActivity.vipList = content.levels
    GameUIActivity.vipKrypton = content.kryptons
    table.sort(GameUIActivity.vipList, function(v1, v2)
      return v1.level < v2.level
    end)
    GameUIBarLeft.vipList = GameUIActivity.vipList
    GameUIBarLeft:checkActivityStatus()
    GameVipDetailInfoPanel:Show(VIP_PANEL_TYPE.VIP_PANLE_TYPE_TMP_VIP)
    GameVipDetailInfoPanel:SetUpTmpVipInfo()
    return true
  end
  return false
end
function GameVipDetailInfoPanel:CheckInitGameData()
  if not self.vipExp then
    self.vipExp = GameDataAccessHelper:GetVipExp()
    self.vipExp = LuaUtils:table_values(self.vipExp)
    table.sort(self.vipExp, function(v1, v2)
      return v1.level < v2.level
    end)
  end
end
function GameVipDetailInfoPanel:InitVipDesc()
  if self.mGameVipDesc then
    return
  end
  self:CheckInitGameData()
  self.maxId = 0
  for k, v in pairs(self.vipExp) do
    if v.level > self.maxId then
      self.maxId = v.level
    end
  end
  local contentsTable = {}
  for index = 0, self.maxId do
    local tempStr = ""
    for i = 1, 10 do
      tempStr = tempStr .. GameDataAccessHelper:GetVIPText(index, i) .. "\n"
    end
    DebugOut("tempStr = ", tempStr)
    contentsTable[index + 1] = tempStr
  end
  self.mGameVipDesc = contentsTable
end
function GameVipDetailInfoPanel:InitPanel(vipInfo)
  self.mCurVipLevel = 0
  self.mCurVipExp = 0
  self.mVipExpTarget = 0
  self.mVipLevelTarget = 0
  self.mTargetVipLevelList = {}
  self.mTargetVipExpList = {}
  self.mVipAnimList = {}
  self.mVIPAwardsList = {}
  self.mVIPDescList = {}
  self:InitVipDesc()
  self.mCurrentAwards = {}
  self.currentVipInfo = vipInfo
  if self:GetFlashObject() then
    if GameStateManager:GetCurrentGameState():IsObjectInState(GameUIActivity) and GameUIActivity.currentLab == "newSign" then
      self:GetFlashObject():InvokeASCallback("_root", "setVIPLeveTitleVisible", false)
    else
      self:GetFlashObject():InvokeASCallback("_root", "setVIPLeveTitleVisible", true)
    end
  end
  if not GameUIBarLeft.vipList then
    NetMessageMgr:SendMsg(NetAPIList.vip_loot_req.Code, nil, GameVipDetailInfoPanel.RequestVipCallback, false, nil)
  else
    GameVipDetailInfoPanel:PrepareVipExpAnim(vipInfo)
    GameVipDetailInfoPanel:PrepareVipAwards()
    GameVipDetailInfoPanel:PrepareVipDescs()
    GameVipDetailInfoPanel:Show(VIP_PANEL_TYPE.VIP_PANEL_TYPE_DETAIL)
  end
end
function GameVipDetailInfoPanel.RequestVipCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.vip_loot_req.Code then
    return true
  elseif msgType == NetAPIList.vip_loot_ack.Code then
    GameUIActivity.vipList = content.levels
    GameUIActivity.vipKrypton = content.kryptons
    table.sort(GameUIActivity.vipList, function(v1, v2)
      return v1.level < v2.level
    end)
    GameUIBarLeft.vipList = GameUIActivity.vipList
    GameUIBarLeft:checkActivityStatus()
    GameVipDetailInfoPanel:PrepareVipExpAnim(GameVipDetailInfoPanel.currentVipInfo)
    GameVipDetailInfoPanel:PrepareVipAwards()
    GameVipDetailInfoPanel:PrepareVipDescs()
    GameVipDetailInfoPanel:Show(VIP_PANEL_TYPE.VIP_PANEL_TYPE_DETAIL)
    return true
  end
  return false
end
function GameVipDetailInfoPanel:Show(panelType)
  if not GameStateManager:GetCurrentGameState():IsObjectInState(self) then
    GameStateManager:GetCurrentGameState():AddObject(self)
    GameStateManager:GetCurrentGameState():ForceCompleteCammandList()
  end
  self.mCurrentPanelType = panelType
  if self.mCurrentPanelType == VIP_PANEL_TYPE.VIP_PANEL_TYPE_DETAIL then
    self:PlayVipExpAnim()
  end
  DebugOut("show panelType = ", panelType)
  self:GetFlashObject():InvokeASCallback("_root", "showPanel", self.mCurrentPanelType)
end
function GameVipDetailInfoPanel:OnAddToGameState()
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  if GameStateManager:GetCurrentGameState():IsObjectInState(GameUIActivity) and GameUIActivity.currentLab == "newSign" then
    self:GetFlashObject():InvokeASCallback("_root", "setVIPLeveTitleVisible", false)
  else
    self:GetFlashObject():InvokeASCallback("_root", "setVIPLeveTitleVisible", true)
  end
end
function GameVipDetailInfoPanel:OnEraseFromGameState()
  self:UnloadFlashObject()
  if GameVipDetailInfoPanel.mExitCallback then
    GameVipDetailInfoPanel.mExitCallback()
    GameVipDetailInfoPanel.mExitCallback = nil
  end
end
function GameVipDetailInfoPanel:PlayVipExpAnim()
  if #self.mVipAnimList > 0 then
    DebugOut("player anim vip")
    local vipInfo = self.mVipAnimList[1]
    local awardsInfo = self.mVIPAwardsList[1]
    local descInfo = self.mVIPDescList[1]
    self.mCurrentAwards = self.mVIPAwardsList[1]
    self:GetFlashObject():InvokeASCallback("_root", "setUpVIPInfoDetail", vipInfo, awardsInfo, descInfo)
    self:GetFlashObject():InvokeASCallback("_root", "playVIPInCreaseAnim", vipInfo.startPercent)
    table.remove(self.mVipAnimList, 1)
    table.remove(self.mVIPAwardsList, 1)
    table.remove(self.mVIPDescList, 1)
  end
end
function GameVipDetailInfoPanel:SetUpVipDetail()
  if #self.mVipAnimList > 0 then
    DebugOut("player anim vip")
    local vipInfo = self.mVipAnimList[1]
    local awardsInfo = self.mVIPAwardsList[1]
    local descInfo = self.mVIPDescList[1]
    DebugTable(awardsInfo)
    self:GetFlashObject():InvokeASCallback("_root", "setUpVIPInfoDetail", vipInfo, awardsInfo, descInfo)
  end
end
function GameVipDetailInfoPanel:OnFSCommand(cmd, arg)
  if cmd == "VipAnimLevelUp" then
    GameVipDetailInfoPanel:SetUpVipDetail()
    GameVipDetailInfoPanel:PlayVipLevelUpAnim(tonumber(arg))
  elseif cmd == "VipAnimLevelUpFinish" then
    GameVipDetailInfoPanel:PlayVipExpAnim()
  elseif cmd == "VipDetailInfoMoveOutFinish" then
    GameStateManager:GetCurrentGameState():EraseObject(self)
  elseif cmd == "VipNoticeMoveOutFinish" then
    GameStateManager:GetCurrentGameState():EraseObject(self)
  elseif cmd == "awardsPressed" then
    local award = self.mCurrentAwards[tonumber(arg)]
    if award then
      DebugOut("award")
      local realItem = award.realItem
      GameVipDetailInfoPanel:showItemDetil(realItem)
    end
  end
end
function GameVipDetailInfoPanel:showItemDetil(item_)
  if GameHelper:IsResource(item_.item_type) then
    return
  end
  local item = item_
  if item.item_type == "krypton" then
    if item.level == 0 then
      item.level = 1
    end
    local detail = GameUIKrypton:TryQueryKryptonDetail(item.number, item.level, function(msgType, content)
      local ret = GameUIKrypton.QueryKryptonDetailCallBack(msgType, content)
      if ret then
        local tmp = GameGlobalData:GetKryptonDetail(item.number, item.level)
        if tmp == nil then
          return false
        end
        ItemBox:SetKryptonBox(tmp, item.number)
        local operationTable = {}
        operationTable.btnUnloadVisible = false
        operationTable.btnUpgradeVisible = false
        operationTable.btnUseVisible = false
        operationTable.btnDecomposeVisible = false
        ItemBox:ShowKryptonBox(320, 240, operationTable)
      end
      return ret
    end)
    if detail == nil then
    else
      ItemBox:SetKryptonBox(detail, item.number)
      local operationTable = {}
      operationTable.btnUnloadVisible = false
      operationTable.btnUpgradeVisible = false
      operationTable.btnUseVisible = false
      operationTable.btnDecomposeVisible = false
      ItemBox:ShowKryptonBox(320, 240, operationTable)
    end
  end
  if item.item_type == "item" then
    item.cnt = 1
    ItemBox:showItemBox("Item", item, tonumber(item.number), 320, 480, 200, 200, "", nil, "", nil)
  end
  if item.item_type == "fleet" then
    ItemBox:ShowCommanderDetail2(tonumber(item.number))
  end
end
function GameVipDetailInfoPanel:PlayVipLevelUpAnim(level)
  DebugOut("show level up")
  self:SetUpLevelUpPanel(level)
  self:Show(VIP_PANEL_TYPE.VIP_PANEL_TYPE_LEVEL_UP)
end
function GameVipDetailInfoPanel:PrepareVipExpAnim(vipInfo)
  GameVipDetailInfoPanel.mVipExpTarget = vipInfo.next_level_exp
  GameVipDetailInfoPanel.mVipLevelTarget = vipInfo.level + 1
  local initTotalExp = 0
  local curTotalExp = 0
  DebugOut("GameVipDetailInfoPanel.mInitVipLevel = ", GameVipDetailInfoPanel.mInitVipLevel)
  DebugOut("current level = ", vipInfo.level)
  DebugOut("GameVipDetailInfoPanel.mInitVipExp = ", GameVipDetailInfoPanel.mInitVipExp)
  DebugOut("GameVipDetailInfoPanel.mVipExpList = ")
  DebugTable(GameVipDetailInfoPanel.mVipExpList)
  for i, v in ipairs(GameVipDetailInfoPanel.mVipExpList) do
    if v.level == GameVipDetailInfoPanel.mInitVipLevel then
      initTotalExp = v.need_exp
      DebugOut("initTotalExp = ", initTotalExp)
    end
    if v.level == vipInfo.level then
      curTotalExp = v.need_exp
    end
  end
  initTotalExp = initTotalExp + GameVipDetailInfoPanel.mInitVipExp
  curTotalExp = curTotalExp + vipInfo.current_exp
  GameVipDetailInfoPanel.mVipExpGet = curTotalExp - initTotalExp
  DebugOut("GameVipDetailInfoPanel.mVipExpGet = ", GameVipDetailInfoPanel.mVipExpGet)
  GameVipDetailInfoPanel.mTargetVipExpList = {}
  GameVipDetailInfoPanel.mVipAnimList = {}
  GameVipDetailInfoPanel.mVIPAwardsList = {}
  DebugOut("GameVipDetailInfoPanel.mVipLevelTarget = ", GameVipDetailInfoPanel.mVipLevelTarget)
  for i, v in ipairs(GameVipDetailInfoPanel.mVipExpList) do
    if v.level > GameVipDetailInfoPanel.mInitVipLevel and v.level <= GameVipDetailInfoPanel.mVipLevelTarget then
      local exp = 0
      for i2, v2 in ipairs(GameVipDetailInfoPanel.mVipExpList) do
        if v2.level == v.level - 1 then
          exp = v2.need_exp
        end
      end
      table.insert(GameVipDetailInfoPanel.mTargetVipExpList, v.need_exp - exp)
    end
  end
  DebugOut("GameVipDetailInfoPanel.mTargetVipExpList = ")
  DebugTable(GameVipDetailInfoPanel.mTargetVipExpList)
  local startLevel = GameVipDetailInfoPanel.mInitVipLevel
  local startExp = GameVipDetailInfoPanel.mInitVipExp
  local vipGet = self.mVipExpGet
  GameVipDetailInfoPanel.mInitVipLevel = vipInfo.level
  GameVipDetailInfoPanel.mInitVipExp = vipInfo.current_exp
  for i, v in ipairs(GameVipDetailInfoPanel.mTargetVipExpList) do
    local vipExpInfo = {}
    vipExpInfo.vipStartLevel = startLevel
    vipExpInfo.startPercent = 1 + math.ceil(startExp / v * 100)
    vipExpInfo.vipExpGet = self.mVipExpGet
    if vipGet >= v - startExp then
      vipExpInfo.targetPercent = 101
      vipExpInfo.targetLevel = startLevel + 1
      vipExpInfo.vipExpTextInfo = v .. "/" .. v
      vipExpInfo.showPopAnim = true
      table.insert(self.mVipAnimList, vipExpInfo)
    else
      vipExpInfo.targetPercent = 1 + math.ceil(vipInfo.current_exp / v * 100)
      vipExpInfo.targetLevel = startLevel + 1
      vipExpInfo.vipExpTextInfo = vipInfo.current_exp .. "/" .. v
      vipExpInfo.showPopAnim = false
      table.insert(self.mVipAnimList, vipExpInfo)
      break
    end
    vipGet = vipGet - (v - startExp)
    startLevel = startLevel + 1
    startExp = 0
  end
  DebugOut("self.mVipAnimList = ")
  DebugTable(self.mVipAnimList)
  if GameVipDetailInfoPanel.mVipLevelTarget > self.maxId then
    local vipExpInfo = {}
    vipExpInfo.vipExpGet = self.mVipExpGet
    vipExpInfo.vipStartLevel = self.maxId
    vipExpInfo.targetLevel = self.maxId
    vipExpInfo.startPercent = 1
    vipExpInfo.targetPercent = 1
    vipExpInfo.vipExpTextInfo = ""
    table.insert(self.mVipAnimList, vipExpInfo)
  end
end
function GameVipDetailInfoPanel:PrepareVipAwards()
  DebugOut("GameUIBarLeft.vipList = ")
  DebugTable(GameUIBarLeft.vipList)
  self.mVIPAwardsList = {}
  for i, v in ipairs(self.mVipAnimList) do
    for iLoot, vLoot in ipairs(GameUIBarLeft.vipList) do
      if vLoot.level == v.vipStartLevel + 1 or v.vipStartLevel == self.maxId and vLoot.level == vipStartLevel then
        local vipLoot = {}
        DebugOut("vLoot.level = ", vLoot.level)
        DebugTable(vLoot.loots)
        for iItem, vItem in ipairs(vLoot.loots) do
          local vipLootItem = {}
          vipLootItem.realItem = vItem
          vipLootItem.iconFrame = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(vItem)
          vipLootItem.count = GameHelper:GetAwardCount(vItem.item_type, vItem.number, vItem.no)
          table.insert(vipLoot, vipLootItem)
        end
        table.insert(self.mVIPAwardsList, vipLoot)
      end
    end
  end
end
function GameVipDetailInfoPanel:PrepareVipDescs()
  self.mVIPDescList = {}
  for i, v in ipairs(self.mVipAnimList) do
    local tempDesc = {}
    local tempVIPDesc = self.mGameVipDesc[v.vipStartLevel + 1]
    DebugOut("tempVIPDesc = ", tempVIPDesc)
    DebugOut("self.maxId = ", self.maxId)
    DebugOut("v.vipStartLevel = ", v.vipStartLevel)
    if v.vipStartLevel + 1 > self.maxId then
      DebugOut("Max level")
      tempVIPDesc = self.mGameVipDesc[self.maxId]
      tempDesc.vipContenctDesc = tempVIPDesc
      tempDesc.vipTitleDesc = GameLoader:GetGameText("LC_MENU_REBUILD_HIGHEST_LEVEL") .. " " .. self.maxId
      temp_title = string.format(GameLoader:GetGameText("LC_MENU_VIP_TITLE"), self.maxId)
      tempDesc.vipTitle = temp_title
    else
      DebugOut("not Max level")
      tempDesc.vipContenctDesc = tempVIPDesc
      tempDesc.vipTitleDesc = GameLoader:GetGameText("LC_MENU_NEXT_VIP_LEVEL") .. " " .. v.vipStartLevel + 1
      temp_title = string.format(GameLoader:GetGameText("LC_MENU_VIP_TITLE"), v.vipStartLevel + 1)
      tempDesc.vipTitle = temp_title
    end
    table.insert(self.mVIPDescList, tempDesc)
  end
end
function GameVipDetailInfoPanel:SetUpLevelUpPanel(level)
  local vipLoot = {}
  local vipDesc = {}
  for iLoot, vLoot in ipairs(GameUIBarLeft.vipList) do
    if vLoot.level == level then
      for iItem, vItem in ipairs(vLoot.loots) do
        local vipLootItem = {}
        vipLootItem.iconFrame = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(vItem)
        vipLootItem.count = GameHelper:GetAwardCount(vItem.item_type, vItem.number, vItem.no)
        table.insert(vipLoot, vipLootItem)
      end
      vipDesc.desc = string.format(GameLoader:GetGameText("LC_MENU_VIP_CONGRATULATION"), vLoot.level)
      vipDesc.detail = self.mGameVipDesc[vLoot.level]
      vipDesc.title = string.format(GameLoader:GetGameText("LC_MENU_VIP_TITLE"), vLoot.level)
      DebugOut("vipDesc.title = ", vipDesc.title)
    end
  end
  self:GetFlashObject():InvokeASCallback("_root", "setVIPLevelUp", level, vipLoot, vipDesc)
end
function GameVipDetailInfoPanel:GetVipLevel()
  local vipInfo = GameGlobalData:GetData("vipinfo")
  if GameVipDetailInfoPanel.mTmpInfo then
    local tempLevel = GameVipDetailInfoPanel.mTmpInfo.level
    if tempLevel then
      local tmpVipTime = GameVipDetailInfoPanel.mTmpInfo.left_time - (os.time() - GameVipDetailInfoPanel.mTmpInfoFetchTime)
      if not vipInfo or tmpVipTime > 0 and tempLevel > vipInfo.level then
        return tempLevel
      end
    end
  end
  if not vipInfo then
    return 0
  end
  return vipInfo.level
end
