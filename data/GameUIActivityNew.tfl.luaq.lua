local GameUIActivityNew = LuaObjectManager:GetLuaObject("GameUIActivityNew")
local GameUIBarLeft = LuaObjectManager:GetLuaObject("GameUIBarLeft")
local GameUIRecruitMain = LuaObjectManager:GetLuaObject("GameUIRecruitMain")
local GameSetting = LuaObjectManager:GetLuaObject("GameSetting")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameVip = LuaObjectManager:GetLuaObject("GameVip")
local GameUIGachaBox = require("data1/GameUIGachaBox.tfl")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local GameUICommonEvent = LuaObjectManager:GetLuaObject("GameUICommonEvent")
local GameObjectMainPlanet = LuaObjectManager:GetLuaObject("GameObjectMainPlanet")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
local GameUIKrypton = LuaObjectManager:GetLuaObject("GameUIKrypton")
local GameUIWDPlanetSelect = LuaObjectManager:GetLuaObject("GameUIWDPlanetSelect")
local GameObjectClimbTower = LuaObjectManager:GetLuaObject("GameObjectClimbTower")
local GameUIFriend = LuaObjectManager:GetLuaObject("GameUIFriend")
local GameUIWDStuff = LuaObjectManager:GetLuaObject("GameUIWDStuff")
local GameUIcomboGacha = require("data1/GameUIcomboGacha.tfl")
local QuestTutorialComboGacha = TutorialQuestManager.QuestTutorialComboGacha
local GameUICommonDialog = LuaObjectManager:GetLuaObject("GameUICommonDialog")
local InterstellarAdventure = LuaObjectManager:GetLuaObject("InterstellarAdventure")
local GameStateStore = GameStateManager.GameStateStore
local GameStateArena = GameStateManager.GameStateArena
local GameGoodsList = LuaObjectManager:GetLuaObject("GameGoodsList")
local GameStateRecruit = GameStateManager.GameStateRecruit
local GameStateKrypton = GameStateManager.GameStateKrypton
local GameUIArena = LuaObjectManager:GetLuaObject("GameUIArena")
local GameCommonGoodsList = LuaObjectManager:GetLuaObject("GameCommonGoodsList")
local GameUIBattleResult = LuaObjectManager:GetLuaObject("GameUIBattleResult")
local GameVipDetailInfoPanel = LuaObjectManager:GetLuaObject("GameVipDetailInfoPanel")
local GameUIDice = LuaObjectManager:GetLuaObject("GameUIDice")
local GameStateDice = GameStateManager.GameStateDice
local GameUIActivityEvent = LuaObjectManager:GetLuaObject("GameObjectActivityEvent")
local GameUIActivity = LuaObjectManager:GetLuaObject("GameUIActivity")
local activityList, activityListTimeText
local isSortActivity = false
local currentLab = ""
local m_festivalInfo
local eventLevelDef = 0
local outCallback
local currentFocusItem = 1
local currentSelectedActivity = 0
local currentCategory = 0
local currentSubCategory = 0
local superCategory = 0
local superSubCategory = 0
GameUIActivityNew.rankType = {
  None = 0,
  player = 1,
  playerAndAlliance = 2,
  alliance = 3
}
GameUIActivityNew.category = {
  TYPE_DNA = 100,
  TYPE_SUPER = 101,
  TYPE_DUNGEON = 102,
  TYPE_BOX = 103,
  TYPE_WORM_HOLE = 104,
  TYPE_INTERSTELLAR_ADVENTURE = 105,
  TYPE_PAYWALL = 106,
  TYPE_KRYPTONLAB = 107,
  TYPE_WD = 108,
  TYPE_PVP = 109,
  TYPE_STORE_BLACK = 110,
  TYPE_STARTPOT = 111,
  TYPE_STORE = 112,
  TYPE_DICE = 113,
  TYPE_SPEEDUP = 114,
  TYPE_ACTIVITYEVENT = 115,
  TYPE_SCRATCH = 116,
  TYPE_NEWSIGN = 117,
  TYPE_MEDALSHOP = 118,
  TYPE_MEDALRECUIT = 119,
  TYPE_LARGEMAP = 120,
  TYPE_EXPEDITION = 121,
  TYPE_UPDATEVERSION = 1000
}
local currentRankItemCode = 0
local currentShowingTab = 0
local playerLeaderboard, starAllianceLeaderboard, myLeaderboard
GameUIActivityNew.hasTryOpenTable = {}
GameUIActivityNew.isModuleOpenTable = {}
GameUIActivityNew.FestivalType = {
  None = 0,
  Watting = 1,
  Request_Available_Activity = 2,
  Finished_Request = 3,
  Error = 4
}
GameUIActivityNew.SpeedType = {
  technique = 1,
  gold = 2,
  pretige = 3
}
local currentRankKey
GameUIActivityNew.activityType = {
  Normal = 0,
  WD = 1,
  WDandNormal = 2,
  Super = 3,
  SuperAndWD = 4,
  NormalAndWD = 5
}
GameUIActivityNew.DetailType = {
  UNKNOWN = 0,
  FESTIVAL = 1,
  WD_EVENT = 2,
  WDC = 3
}
GameUIActivityNew.NetMegType = {FESTIVAL = 1, WD_EVENT = 3}
local currentFestivalType = GameUIActivityNew.FestivalType.None
GameUIActivityNew.promotionsList = nil
GameUIActivityNew.activityContentIndex = 1
GameUIActivityNew.GetActivityRewardType2Index = nil
GameUIActivityNew.currentActivityContentTitle = nil
GameUIActivityNew.showContent = false
GameUIActivityNew.mGotoMonthCardDirectly = false
GameUIActivityNew.comboGahcaTutorialFlag = false
GameUIActivityNew.combogachaItemid = nil
GameUIActivityNew.currentSpeed = nil
function GameUIActivityNew:OnInitGame()
end
function GameUIActivityNew:OnAddToGameState(...)
  DebugOut("GameUIActivityNew:OnAddToGameState")
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  if QuestTutorialComboGacha:IsActive() and TutorialQuestManager.ComboGachaHeroContent == nil then
    local param = {state = 2}
    NetMessageMgr:SendMsg(NetAPIList.combo_guide_req.Code, param, GameUIActivityNew.GetCombogachaActivyIdCallback, true, nil)
  else
    GameUIActivityNew:Init()
  end
  GameUIActivityNew:GetFlashObject():unlockInput()
end
function GameUIActivityNew:Init()
  local inComing = GameLoader:GetGameText("LC_MENU_INCOMING_CAHR")
  local inprogress = GameLoader:GetGameText("LC_MENU_IN_PROGRESS_CHAR")
  local finished = GameLoader:GetGameText("LC_MENU_FINISHED_CHAR")
  local speedButtonText = GameLoader:GetGameText("LC_MENU_AUTO_COMPLETE_BUTTON")
  self:GetFlashObject():InvokeASCallback("_root", "lua2fs_setActivityItemText", inComing, inprogress, finished)
  self:GetFlashObject():InvokeASCallback("_root", "initSpeedButtonText", speedButtonText)
  GameUIActivityNew:InitActivity()
  GameTimer:Add(self._OnTimerTick, 1000)
  GameGlobalData:RegisterDataChangeCallback("fleetinfo", self.RefreshCanCall)
  GameGlobalData:RegisterDataChangeCallback("item_count", self.RefreshResource)
  GameUIActivityNew:CheckShowTutorial()
  TutorialQuestManager.GetComboGachaCallback = GameUIActivityNew.GetComboGachaCallback
  DebugTable(activityList)
  if activityList then
    GameUIActivityNew:refreshActivityList()
    if GameUIActivityNew.mGotoMonthCardDirectly then
      GameUIActivityNew:ShowEmpireBank()
      GameUIActivityNew.mGotoMonthCardDirectly = false
    end
  else
    self:RequestActivity(true)
  end
end
function GameUIActivityNew.GetCombogachaActivyIdCallback(msgType, content)
  if msgType == NetAPIList.combo_guide_ack.Code then
    DebugOut("GetCombogachaActivyIdCallback")
    DebugTable(content)
    TutorialQuestManager.ComboGachaHeroContent = content
    GameUIActivityNew:Init()
    return true
  end
  return false
end
function GameUIActivityNew:CheckShowTutorial()
  if QuestTutorialComboGacha:IsActive() then
    local function callback()
      AddFlurryEvent("Gacha_Dialog_2", {}, 1)
      GameUIActivityNew.comboGahcaTutorialFlag = true
      DebugOut("GameUIActivityNew.comboGahcaTutorialFlag = ", GameUIActivityNew.comboGahcaTutorialFlag)
      DebugOut("GameUIActivityNew.combogachaItemid = ", GameUIActivityNew.combogachaItemid)
      if GameUIActivityNew.combogachaItemid then
        GameUIActivityNew:setActivityItem(GameUIActivityNew.combogachaItemid)
      end
      GameUIActivityNew.isHide1100059Dialog = false
    end
    GameUIActivityNew.isHide1100059Dialog = true
    GameUICommonDialog:PlayStory({1100058}, callback)
  else
    GameUIActivityNew.comboGahcaTutorialFlag = false
  end
end
function GameUIActivityNew:OnEraseFromGameState()
  DebugOut("GameUIActivityNew:OnEraseFromGameState")
  self:UnloadFlashObject()
  GameUIActivityNew.showContent = false
  TutorialQuestManager.GetComboGachaCallback = nil
  GameUIActivityNew.combogachaItemid = nil
end
function GameUIActivityNew:InitActivity()
  self:GetFlashObject():InvokeASCallback("_root", "lua2fs_showActivityList")
end
function GameUIActivityNew:refreshActivityList()
  DebugActivity("GameUIActivityNew refreshActivityList")
  if not activityList or #activityList < 1 then
    return
  end
  if not self:GetFlashObject() then
    return
  end
  GameUIActivityNew:SendAllNewActivityIsRead()
  DebugOut("hehe activityList = ")
  DebugTable(activityList)
  self:GetFlashObject():InvokeASCallback("_root", "lua2fs_addActivity", #activityList, currentFocusItem)
  DebugOut("refreshActivityList showNewsDetail 1")
  if not GameUIActivityNew.showContent and not GameUIActivityNew.mGotoMonthCardDirectly then
    GameUIActivityNew:OnFSCommand("showNewsDetail", 1)
  end
end
function GameUIActivityNew:RequestActivity(isForceRequest, callback)
  DebugOut("GameUIActivityNew:RequestActivity", isForceRequest, callback)
  if isForceRequest or not activityList then
    local requestType = "servers/" .. GameUtils:GetActiveServerInfo().id .. "/server_activity"
    local loginInfo = GameUtils:GetLoginInfo()
    local requestParam = {
      lang = GameSettingData.Save_Lang
    }
    DebugOut("requestType", requestType)
    if callback ~= nil then
      outCallback = callback
    end
    GameUtils:HTTP_SendRequest(requestType, requestParam, GameUIActivityNew.RequestActivityCallback, false, GameUtils.httpRequestFailedCallback, "GET", "notencrypt")
  end
end
function GameUIActivityNew.RequestActivityCallback(responseContent)
  DebugActivity("GameUIActivityNew RequestActivityCallback")
  DebugActivityTable(responseContent)
  activityList = responseContent
  GameUIActivityNew:ChangeActivityListIsShowByStatus()
  GameUIBarLeft.activityList = activityList
  GameUIActivityNew:CheckVersionUpdateActivity()
  GameUIBarLeft:sendNoticeToServer()
  GameUIActivityNew:refreshActivityList()
  DebugTable(activityList)
  if outCallback ~= nil then
    outCallback()
    outCallback = nil
  end
  if GameUIActivityNew.mGotoMonthCardDirectly then
    GameUIActivityNew:ShowEmpireBank()
    GameUIActivityNew.mGotoMonthCardDirectly = false
  end
  return true
end
function GameUIActivityNew:CheckVersionUpdateActivity()
  if activityList and #activityList > 0 then
    for k, v in pairs(activityList) do
      if v.sub_category == GameUIActivityNew.category.TYPE_UPDATEVERSION then
        local str = v.link_btn_content
        local versionList = LuaUtils:string_split(str, ",")
        local localAppid = ext.GetBundleIdentifier()
        for k1, v1 in pairs(versionList or {}) do
          if string.find(v1, localAppid) then
            local id, serverVersion = unpack(LuaUtils:string_split(v1, ":"))
            if tonumber(serverVersion) <= AutoUpdate.localAppVersion then
              table.remove(activityList, k)
            end
            break
          end
        end
        break
      end
    end
  end
end
function GameUIActivityNew:ShowActivityRewardType2List()
  DebugOut("ShowActivityRewardType2List")
  if not GameUIActivityNew:GetFlashObject() then
    return
  end
  if not GameUIActivityNew.activityContentRewareType2.sections then
    return
  end
  for k, v in pairs(GameUIActivityNew.activityContentRewareType2.sections) do
    self:GetFlashObject():InvokeASCallback("_root", "addActicityContentRewardType2", k)
  end
end
function GameUIActivityNew:SendAllNewActivityIsRead()
  if not GameUIBarLeft.newsList then
    DebugActivity("GameUIActivityNew:SendAllNewActivityIsRead[error]newsList")
    return
  end
  if not GameUIBarLeft.activityList then
    DebugActivity("GameUIActivityNew:SendAllNewActivityIsRead[error]activityList")
    return
  end
  if not GameUIBarLeft.isNewActivity then
    DebugActivity("GameUIActivityNew:SendAllNewActivityIsRead[error]isNewActivity")
    return
  end
  local activitiesList = {}
  for k, v in pairs(activityList) do
    if v.code then
      table.insert(activitiesList, v.code)
    end
  end
  DebugOut(" ---GameUIActivityNew:sendNoticeToServer()-\227\128\139 ", os.time())
  DebugTable(GameUIBarLeft.newsList)
  DebugOut("------")
  DebugTable(activitiesList)
  local requestParam = {
    notices = GameUIBarLeft.newsList.notices,
    activities = activitiesList
  }
  NetMessageMgr:SendMsg(NetAPIList.new_activities_compare_req.Code, requestParam, GameUIActivityNew.NetCallbackClearGateway, false, nil)
end
function GameUIActivityNew:ChangeActivityListIsShowByStatus()
  DebugOut("GameUIActivityNew:ChangeActivityListIsShowByStatus")
  if not GameUIActivityNew.promotionsList then
    DebugOut("promotionsList not yet ok")
    return
  end
  if not activityList then
    DebugOut("activityList not yet ok")
    return
  end
  if activityList.error then
    DebugOut("activityList error")
    activityList = nil
    return
  end
  if GameUIActivityNew.promotionsList then
    DebugOutPutTable(GameUIActivityNew.promotionsList, " GameUIActivityNew.promotionsList")
    for k, v in pairs(GameUIActivityNew.promotionsList) do
      for k1, v1 in pairs(v.news) do
        for i = #activityList, 1, -1 do
          v2 = activityList[i]
          if v2 and tonumber(v2.code) == tonumber(v1.news_id) then
            if v1.can_show then
              v2.left_seconds = v1.left_seconds
              v2.tab = v.tab
              v2.id = v.id
              break
            end
            DebugOut("remove code: ", v2.code, " title:", v2.title)
            table.remove(activityList, i)
            DebugOut("left count:", #activityList)
            break
          end
        end
      end
    end
  end
  if #activityList < 1 then
    activityList = nil
  end
  activityListTimeText = {}
  if activityList then
    for k, v in pairs(activityList or {}) do
      activityListTimeText[k] = {}
    end
  end
  if activityList and #activityList > 0 then
    GameUIActivityNew:SortActivityList()
  end
  if activityList and #activityList > 0 then
    GameObjectClimbTower.activityListTimeText = LuaUtils:table_copy(activityListTimeText)
    GameObjectClimbTower.activityList = LuaUtils:table_copy(activityList)
    GameUIActivityNew:RemoveSpeedActivity()
  end
end
function GameUIActivityNew:RemoveSpeedActivity()
  local function callBack(msgType, content)
    if msgType == NetAPIList.speed_resource_ack.Code then
      GameUIActivityNew.speedResource = content.items
      for i = #activityList, 1, -1 do
        for k, v in ipairs(GameUIActivityNew.speedResource) do
          if activityList[i].sub_category == v.dungeon_id and activityList[i].category == GameUIActivityNew.category.TYPE_DUNGEON then
            DebugOut("remove wd code ", activityList[i].code, activityList[i].title)
            table.remove(activityList, i)
            DebugOut("left count:", #activityList)
            break
          end
        end
      end
      activityListTimeText = {}
      if activityList then
        for k, v in pairs(activityList) do
          activityListTimeText[k] = {}
        end
      end
      if activityList and #activityList > 0 then
        GameUIActivityNew:SortActivityList()
      end
      return true
    end
    return false
  end
  NetMessageMgr:SendMsg(NetAPIList.speed_resource_req.Code, nil, callBack, false, nil)
end
function GameUIActivityNew:SortActivityList()
  DebugOut("GameUIActivityNew:SortActivityList")
  if not activityList[1].weight then
    DebugOut("activityList weight not ok")
    return
  end
  for i = #activityList, 1, -1 do
    if activityList[i].is_wd == GameUIActivityNew.activityType.WD then
      DebugOut("remove wd code ", activityList[i].code, activityList[i].title)
      table.remove(activityList, i)
      DebugOut("left count:", #activityList)
    end
  end
  for k, v in pairs(activityList) do
    v.weight = 0
    if v.left_seconds then
      v.left_seconds = tonumber(v.left_seconds)
      if 0 > v.left_seconds then
        v.weight = v.weight + 20
      elseif v.left_seconds == 0 then
        v.weight = v.weight + 5
        if v.imgtype == 1 then
          v.weight = v.weight - 90
        else
          v.weight = v.weight - 100
        end
      elseif 0 < v.left_seconds then
        v.weight = v.weight + 35
      end
    end
    if v.top_weight and 0 < v.top_weight then
      v.weight = v.weight + v.top_weight * 1000
    end
  end
  GameUIActivityNew.isSortActivity = true
  for k, v in pairs(activityList) do
    if not GameUIActivityNew:GetActivityIsRead(v.code) and v.weight then
      v.weight = v.weight + 10
    end
  end
  table.sort(activityList, function(v1, v2)
    if v1.weight == v2.weight then
      return v1.started_at > v2.started_at
    else
      return v1.weight > v2.weight
    end
  end)
  DebugActivity("after sort activityList")
  DebugActivityTable(activityList)
  for k, v in pairs(activityList) do
    if v.left_seconds then
      v.left_seconds = tonumber(v.left_seconds)
      if 0 > v.left_seconds then
        activityListTimeText[k].title = GameLoader:GetGameText("LC_MENU_EVENT_UNSTART_CHAR")
        activityListTimeText[k].status = -1
      elseif v.left_seconds == 0 then
        activityListTimeText[k].title = GameLoader:GetGameText("LC_MENU_EVENT_OVER_CHAR")
        activityListTimeText[k].status = 0
      elseif 0 < v.left_seconds then
        activityListTimeText[k].title = GameLoader:GetGameText("LC_MENU_EVENT_TIME_LEFT_CHAR")
        activityListTimeText[k].status = 1
      end
      v.left_stamp = math.abs(v.left_seconds) + os.time()
      DebugOut("left_stamp", v.left_stamp)
      v.persistent_time = math.abs(v.end_at - v.started_at)
      assert(0 < v.persistent_time)
      DebugOut("persistent_time", v.persistent_time)
    end
  end
end
function GameUIActivityNew.getPromotionsActivity(content)
  DebugActivity("getPromotionsActivity")
  DebugActivityTable(content)
  GameUIActivityNew.promotionsList = content.promotions
  if not GameUIActivityNew.promotionsList then
    return
  end
  for k, v in pairs(GameUIActivityNew.promotionsList) do
    if v.id == "krypton1" or v.id == "krypton2" then
      v.enabled = false
    elseif v.id == "fleet_cutoff" or v.id == "fleet_purchase_limit" then
    elseif v.id == "fleet" and v.enabled then
      GameUIActivityNew.m_isHavePromCommander = true
    end
  end
  GameUIActivityNew:ChangeActivityListIsShowByStatus()
  if GameGlobalData.forRequestActivityInfo then
    DebugOut("forRequestActivityInfo")
    GameUIActivityNew:RequestActivity(true)
    GameGlobalData.forRequestActivityInfo = false
  else
    GameUIActivityNew:refreshActivityList()
  end
  GameUIBarLeft.activityList = activityList
  GameUIBarLeft:checkShowpromotions()
end
function GameUIActivityNew:setActivityItem(itemId)
  DebugActivity("setActivityItem", itemId)
  DebugTable(activityList)
  if not self:GetFlashObject() then
    return
  end
  if not GameUIActivityNew then
    return
  end
  if not itemId then
    return
  end
  local item = activityList[itemId]
  local _index = itemId
  local rewardText = GameLoader:GetGameText("LC_MENU_ALLIANCE_DONATE_REWARDS_CHAR")
  if item then
    DebugActivity("item:")
    DebugActivityTable(item)
    do
      local time = tonumber(item.left_seconds)
      local isCanReward = GameUIActivityNew:IsCanReward(item.code)
      local isReaded = GameUIActivityNew:GetActivityIsRead(item.code)
      DebugActivity("isReaded = ", isReaded)
      local isBig = false
      if item.imgtype == 1 then
        isBig = true
      end
      if item.is_wd == GameUIActivityNew.activityType.Super or item.is_wd == GameUIActivityNew.activityType.SuperAndWD then
        DebugOut("currentFestivalType", currentFestivalType)
        if currentFestivalType == GameUIActivityNew.FestivalType.None then
          superCategory = item.category
          superSubCategory = item.sub_category
          GameUIActivityNew.QueryFestival()
        end
      elseif item.is_wd == GameUIActivityNew.activityType.WDandNormal then
      end
      local isNew = not isReaded
      local isTop = false
      if item.top_weight and item.top_weight > 0 then
        isTop = true
      end
      local combogachaFlag = false
      if TutorialQuestManager.ComboGachaHeroContent ~= nil and item.code == tostring(TutorialQuestManager.ComboGachaHeroContent.activity_id) then
        GameUIActivityNew.combogachaItemid = itemId
        if GameUIActivityNew.comboGahcaTutorialFlag then
          combogachaFlag = true
        end
      end
      DebugOut("combogachaFlag:", combogachaFlag, GameUIActivityNew.comboGahcaTutorialFlag)
      self:GetFlashObject():InvokeASCallback("_root", "lua2fs_SetActivityListItem", itemId, GameLoader:GetGameText("LC_MENU_NEWS_DOWNLOAD_CHAR"), item.title, time, isNew, isTop, isCanReward, rewardText, isBig, combogachaFlag)
      local url = item.image_path
      if not string.find(url, "http:") and not string.find(url, "https:") then
        url = AutoUpdate.gameGateway .. url
      end
      local pngName = string.match(url, ".+/([^/]*%w+)$")
      if not pngName then
        return
      end
      if string.find(pngName, "NEWcombogacha") ~= nil then
        pngName = "NEWcombogacha.png"
        DebugOut("!!!hello world")
      end
      local path = pngName
      path = path and "data2/" .. path
      DebugOut("path", path)
      if GameUIActivityNew:IsDownloadPNGExsit(path) and ext.IsFileExist and ext.IsFileExist(path) then
        DebugActivity("Use local download file: " .. pngName)
        if GameUIActivityNew:GetFlashObject() then
          local textureName
          if isBig then
            textureName = "NA_NEWS_B_" .. _index .. ".png"
          else
            textureName = "NA_NEWS_" .. _index .. ".png"
          end
          DebugActivity("Replace png", textureName)
          ext.UpdateAutoUpdateFile(path, 0, 0, 0)
          GameUIActivityNew:GetFlashObject():ReplaceTexture(textureName, pngName)
        end
      else
        DebugActivity("Try download file: " .. pngName)
        ext.http.requestDownload({
          url,
          method = "GET",
          localpath = path,
          callback = function(statusCode, content, errstr)
            if errstr ~= nil or statusCode ~= 200 then
              DebugActivity("get file failed")
              return
            end
            local filename = string.match(url, ".+/([^/]*%w+)$")
            DebugActivity("received file", content, filename)
            ext.UpdateAutoUpdateFile(content, 0, 0, 0)
            if GameUIActivityNew:GetFlashObject() then
              local textureName
              if isBig then
                textureName = "NA_NEWS_B_" .. _index .. ".png"
              else
                textureName = "NA_NEWS_" .. _index .. ".png"
              end
              DebugActivity("Replace png", textureName)
              GameUIActivityNew:GetFlashObject():ReplaceTexture(textureName, filename)
            end
            GameSetting:SaveDownloadFileName(path)
          end
        })
      end
    end
  end
end
function GameUIActivityNew:setActivityItemTime()
  if not activityList then
    return
  end
  if not activityListTimeText then
    return
  end
  for k, v in pairs(activityList) do
    if not activityListTimeText[k] then
      DebugOut("[error] activityListTimeText short than activityList")
      return ""
    end
    local titleText, status = GameUIActivityNew:GetActivityItemTimeText(k)
    if self:GetFlashObject() then
      self:GetFlashObject():InvokeASCallback("_root", "lua2fs_updateActivityListItemTime", k, titleText, status)
    end
  end
end
function GameUIActivityNew:GetActivityItemTimeText(k)
  local titleText = ""
  local v = activityList[k]
  if v and v.left_stamp then
    v.left_stamp = tonumber(v.left_stamp)
    local _time = v.left_stamp - os.time()
    if _time < 0 then
      _time = 0
    end
    if activityListTimeText[k] then
      if activityListTimeText[k].status == 0 then
        titleText = activityListTimeText[k].title
      elseif self:GetFlashObject() then
        local timeText = GameUtils:formatTimeStringToNBit(_time)
        titleText = string.format(activityListTimeText[k].title, timeText)
      end
    end
  end
  local retStatus = -1
  if activityListTimeText[k] then
    retStatus = activityListTimeText[k].status
  end
  return titleText, retStatus
end
function GameUIActivityNew:SetActivityContentRewardType2(itemId)
  DebugOut("SetActivityContentRewardType2")
  if not self:GetFlashObject() then
    return
  end
  local item = GameUIActivityNew.activityContentRewareType2.sections[itemId]
  local item_type = ""
  local item_number = ""
  local needNUmText = ""
  local cnt_number = ""
  if GameUIActivityNew.currentLabType2Name == "officer" then
  end
  if GameUIActivityNew.currentLabType2Name == "fleet" then
    needNUmText = string.format(GameLoader:GetGameText("LC_MENU_EVENT_RECRUIT_COUNT_CHAR"), itemId + 1)
  end
  if GameUIActivityNew.currentLabType2Name == "total_credit" then
    if GameUIActivityNew.currentLabType2Code == 1 then
      local tmp_number = GameUtils.numberConversion(item.condition)
      needNUmText = GameLoader:GetGameText("LC_MENU_VIP_REWARDS_PURCHASE_BUTTON") .. "\n" .. tmp_number
    elseif GameUIActivityNew.currentLabType2Code == 2 then
      local tmp_number = GameUtils.numberConversion(item.condition)
      needNUmText = GameLoader:GetGameText("LC_MENU_VIP_REWARDS_PURCHASE_BUTTON") .. "\n" .. tmp_number
    end
  end
  DebugOut("SetActivityContentRewardType2 item.awards:")
  DebugTable(item.awards)
  for i = 1, 5 do
    if item.awards[i] then
      local type_tmp = item.awards[i].item_type
      local number_tmp = tonumber(item.awards[i].number)
      local no = item.awards[i].no
      no = no or 1
      type_tmp, number_tmp = self:getSignItemDetailInfo(type_tmp, number_tmp, no)
      item_type = item_type .. type_tmp .. "\001"
      item_number = item_number .. number_tmp .. "\001"
      cnt_number = cnt_number .. no .. "\001"
    else
      item_type = item_type .. "empty" .. "\001"
      item_number = item_number .. 0 .. "\001"
      cnt_number = cnt_number .. "1" .. "\001"
    end
  end
  local rewardText = GameLoader:GetGameText("LC_MENU_CAPTION_RECEIVE")
  local aleadyText = GameLoader:GetGameText("LC_MENU_CLAIMED_BUTTON")
  self:GetFlashObject():InvokeASCallback("_root", "SetActivityContentRewardType2", itemId, rewardText, aleadyText, item.status, item_type, item_number, needNUmText, cnt_number)
end
function GameUIActivityNew:getSignItemDetailInfo(itemType, number, no)
  DebugActivity("getSignItemDetailInfo", itemType, number, no)
  local itemType = itemType
  local rewards = tonumber(number)
  no = no or 1
  if itemType == "fleet" then
    itemType = GameDataAccessHelper:GetFleetAvatar(rewards)
    rewards = GameLoader:GetGameText("LC_NPC_" .. GameDataAccessHelper:GetCommanderName(rewards))
  elseif itemType == "item" or itemType == "krypton" then
    local isitem = itemType == "item"
    if DynamicResDownloader:IsDynamicStuff(rewards, DynamicResDownloader.resType.PIC) then
      local fullFileName = DynamicResDownloader:GetFullName(rewards .. ".png", DynamicResDownloader.resType.PIC)
      local localPath = "data2/" .. fullFileName
      if DynamicResDownloader:IfResExsit(localPath) then
        ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
        itemType = "item_" .. rewards
      else
        itemType = "temp"
        self:AddDownloadPath(rewards, 0, 0)
      end
    else
      itemType = "item_" .. rewards
    end
    if isitem then
      rewards = GameLoader:GetGameText("LC_MENU_ITEM_CHAR")
    else
      rewards = GameLoader:GetGameText("LC_MENU_KRYPTON_TAB")
    end
  elseif itemType == "pve_supply" then
    rewards = GameLoader:GetGameText("LC_MENU_SUPPLY_CHAR")
  else
    rewards = GameUtils.numberConversion(rewards)
  end
  return itemType, rewards
end
function GameUIActivityNew:getSignItemDetailInfoWithCallback(rewardItem, callback)
  local itemType = rewardItem.item_type
  local number = rewardItem.number
  local no = rewardItem.no
  DebugActivity("getSignItemDetailInfo", itemType, number, no)
  local rewards = tonumber(number)
  no = no or 1
  if itemType == "fleet" then
    itemType = GameDataAccessHelper:GetFleetAvatar(rewards)
    rewards = GameLoader:GetGameText("LC_NPC_" .. GameDataAccessHelper:GetCommanderName(rewards))
  elseif itemType == "item" or itemType == "krypton" then
    local isitem = itemType == "item"
    if DynamicResDownloader:IsDynamicStuff(rewards, DynamicResDownloader.resType.PIC) then
      local fullFileName = DynamicResDownloader:GetFullName(rewards .. ".png", DynamicResDownloader.resType.PIC)
      local localPath = "data2/" .. fullFileName
      if DynamicResDownloader:IfResExsit(localPath) then
        ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
        itemType = "item_" .. rewards
      else
        itemType = "temp"
        local itemID = rewards
        local itemKey = 0
        local index = 0
        local resName = itemID .. ".png"
        local extInfo = {}
        extInfo.rewardItem = rewardItem
        extInfo.itemKey = itemKey
        extInfo.index = index
        extInfo.item_id = itemID
        extInfo.resPath = "data2/LAZY_LOAD_dynamic_" .. resName
        DynamicResDownloader:AddDynamicRes(resName, DynamicResDownloader.resType.PIC, extInfo, callback)
      end
    else
      itemType = "item_" .. rewards
    end
    if isitem then
      rewards = GameLoader:GetGameText("LC_MENU_ITEM_CHAR")
    else
      rewards = GameLoader:GetGameText("LC_MENU_KRYPTON_TAB")
    end
  elseif itemType == "pve_supply" then
    rewards = GameLoader:GetGameText("LC_MENU_SUPPLY_CHAR")
  else
    rewards = GameUtils.numberConversion(rewards)
  end
  return itemType, rewards
end
function GameUIActivityNew:AddDownloadPath(itemID, itemKey, index)
  DebugOut("Add download path", itemID, itemKey, index)
  local resName = itemID .. ".png"
  local extInfo = {}
  extInfo.itemKey = itemKey
  extInfo.index = index
  extInfo.item_id = itemID
  extInfo.resPath = "data2/LAZY_LOAD_dynamic_" .. resName
  DynamicResDownloader:AddDynamicRes(resName, DynamicResDownloader.resType.PIC, extInfo, nil)
end
function GameUIActivityNew.PromotionsDetailCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.promotion_detail_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.promotion_detail_ack.Code then
    DebugOut("GameUIActivityNew.PromotionsDetailCallback ", msgType)
    DebugTable(content)
    if GameUIActivityNew.m_currentTap == 2 then
      GameUIActivityNew:updateActivityType2Content(content)
    elseif GameUIActivityNew.m_currentTap == 4 then
      local item = activityList[GameUIActivityNew.activityContentIndex]
      local title = item.title
      local function foo()
        GameUIActivityNew:GetSltText(content)
      end
      if pcall(foo) then
      end
      local content = GameUIActivityNew.m_currentContent
      DebugOut("\230\137\147\229\141\176 ----\227\128\139promotion_detail_ack", title, content, item.rank_type)
      GameUIActivityNew:GetFlashObject():InvokeASCallback("_root", "showNewsContent", title, content, tonumber(GameUIActivityNew.activityContentIndex), false, true, false, item.rank_type, item.award_type)
    end
    return true
  end
  return false
end
function GameUIActivityNew:updateActivityType2Content(content)
  GameUIActivityNew.activityContentRewareType2 = content
  DebugActivity("GameUIActivityNew:updateActivityType2Content")
  DebugActivityTable(content)
  DebugActivityTable(GameUIActivityNew.activityContentRewareType2.sections)
  local descText, titleText = "", ""
  if GameUIActivityNew.currentLabType2Name == "officer" then
    descText = GameLoader:GetGameText("LC_MENU_EVENT_DIPLOMAT_DES_" .. GameUIActivityNew.currentLabType2Code .. "_CHAR")
    titleText = string.format(GameLoader:GetGameText("LC_MENU_EVENT_DIPLOMAT_ALERT_CHAR"), tonumber(content.value))
  end
  if GameUIActivityNew.currentLabType2Name == "fleet" then
    descText = GameLoader:GetGameText("LC_MENU_EVENT_RECRUIT_DES_CHAR")
    titleText = string.format(GameLoader:GetGameText("LC_MENU_EVENT_RECRUIT_ALERT_CHAR"), tonumber(content.value))
  end
  if GameUIActivityNew.currentLabType2Name == "total_credit" then
    descText = GameLoader:GetGameText("LC_MENU_EVENT_PURCHES_DES_CHAR")
    titleText = string.format(GameLoader:GetGameText("LC_MENU_EVENT_PURCHES_ALERT_CHAR"), tonumber(content.value))
  end
  GameUIActivityNew:GetFlashObject():InvokeASCallback("_root", "shownewsContentType2", descText, titleText, GameUIActivityNew.currentActivityContentTitle)
  GameUIActivityNew:ShowActivityRewardType2List()
end
function GameUIActivityNew:GetSltText(content)
  local slt = require("slt2.tfl")
  local awards = content.sections
  local data = {}
  DebugOut("awards")
  DebugTable(awards)
  for i, v in ipairs(awards) do
    DebugOut(v.condition)
    data[i] = {}
    data[i].condition = v.condition
    data[i].status = v.status
    data[i].game_items = {}
    for i_1, v_1 in ipairs(v.awards) do
      data[i].game_items[i_1] = {}
      data[i].game_items[i_1].name, data[i].game_items[i_1].count = GameUIActivityNew:GetSltItemDetailInfo(v_1.item_type, v_1.number, v_1.no)
    end
  end
  DebugOut(GameUIActivityNew.m_currentContent)
  DebugTable(data)
  local temp = slt.loadstring(GameUIActivityNew.m_currentContent, "#{", "}#")
  local t = {data = data}
  local result = {}
  function t.outputfn(s)
    table.insert(result, s)
  end
  t.user = {name = ""}
  t.ipairs = ipairs
  t.string = string
  slt.render(temp, t)
  local new_content = table.concat(result)
  new_content = string.gsub(new_content, "&nbsp;", "")
  new_content = string.gsub(new_content, "<br />", " ")
  DebugOut(" ------->", new_content)
  GameUIActivityNew.m_currentContent = new_content
end
function GameUIActivityNew:GetSltItemDetailInfo(itemType, number, no)
  DebugOut("---start-", itemType, number, no)
  local name = itemType
  local rewards = tonumber(number)
  no = no or 1
  if name == "fleet" then
    name = GameLoader:GetGameText("LC_NPC_" .. GameDataAccessHelper:GetCommanderName(rewards))
    rewards = no
  elseif name == "item" or name == "krypton" or name == "equip" then
    name = GameLoader:GetGameText("LC_ITEM_ITEM_NAME_" .. number)
    rewards = no
  elseif name == "technique" then
    rewards = GameUtils.numberConversion(rewards)
    name = GameLoader:GetGameText("LC_MENU_LOOT_TECHPOINT")
  elseif name == "expedition" then
    rewards = GameUtils.numberConversion(rewards)
    name = GameLoader:GetGameText("LC_MENU_LOOT_EXPEDITION")
  elseif name == "credit" then
    rewards = GameUtils.numberConversion(rewards)
    name = GameLoader:GetGameText("LC_MENU_LOOT_CREDIT")
  elseif name == "money" then
    rewards = GameUtils.numberConversion(rewards)
    name = GameLoader:GetGameText("LC_MENU_LOOT_CUBIT")
  elseif name == "vip_exp" then
    rewards = GameUtils.numberConversion(rewards)
    name = GameLoader:GetGameText("LC_MENU_VIP_EXP_CHAR")
  elseif name == "ac_supply" then
    rewards = GameUtils.numberConversion(rewards)
    name = GameLoader:GetGameText("LC_MENU_AC_SUPPLY_CHAR")
  elseif name == "kenergy" then
    rewards = GameUtils.numberConversion(rewards)
    name = GameLoader:GetGameText("LC_MENU_krypton_energy")
  elseif name == "mine_digg_licence_one" then
    rewards = GameUtils.numberConversion(rewards)
    name = GameLoader:GetGameText("LC_MENU_MINE_CHANCE_EVENT")
  elseif name == "brick" then
    rewards = GameUtils.numberConversion(rewards)
    name = GameLoader:GetGameText("LC_MENU_BRICK_CHAR")
  elseif name == "mine_fight_licence_one" then
    rewards = GameUtils.numberConversion(rewards)
    name = GameLoader:GetGameText("LC_MENU_PLUNDER_CHANCE_EVENT")
  elseif name == "crystal" then
    rewards = GameUtils.numberConversion(rewards)
    name = GameLoader:GetGameText("LC_MENU_LOOT_CRYSTAL")
  elseif name == "exp" then
    rewards = GameUtils.numberConversion(rewards)
    name = GameLoader:GetGameText("LC_MENU_EXP_CHAR")
  elseif name == "prestige" then
    rewards = GameUtils.numberConversion(rewards)
    name = GameLoader:GetGameText("LC_MENU_PRESTIGE_CHAR")
  elseif name == "quark" then
    rewards = GameUtils.numberConversion(rewards)
    name = GameLoader:GetGameText("LC_MENU_QUARK_CHAR")
  elseif name == "pve_supply" or na == "battle_supply" then
    rewards = GameUtils.numberConversion(rewards)
    name = GameLoader:GetGameText("LC_MENU_SUPPLY_CHAR")
  else
    rewards = GameUtils.numberConversion(rewards)
  end
  DebugOut(" name = ", name, "rewards = ", rewards)
  return name, rewards
end
function GameUIActivityNew:UpdateActivityContentTime()
  if not GameUIActivityNew.activityContentIndex then
    return
  end
  if activityList then
    local time
    if not activityList[GameUIActivityNew.activityContentIndex] then
      time = ""
      DebugActivity("[error] activityContentIndex")
    elseif not activityListTimeText[GameUIActivityNew.activityContentIndex] then
      time = ""
      DebugActivity("[error] activityContentIndex beyond activityListTimeText limit")
    else
      time = GameUIActivityNew:GetActivityItemTimeText(GameUIActivityNew.activityContentIndex)
    end
    self:GetFlashObject():InvokeASCallback("_root", "updateNewContentTime", time)
    local item = activityList[GameUIActivityNew.activityContentIndex]
    if item then
      if item.left_seconds > 0 and item.left_stamp < os.time() then
        self:GetFlashObject():InvokeASCallback("_root", "lua2fs_setShowEnterBtn", false)
        self:GetFlashObject():InvokeASCallback("_root", "lua2fs_showSpeedUpButton", false)
        item.left_seconds = 0
        activityListTimeText[GameUIActivityNew.activityContentIndex].title = GameLoader:GetGameText("LC_MENU_EVENT_OVER_CHAR")
        activityListTimeText[GameUIActivityNew.activityContentIndex].status = 0
      elseif item.left_seconds < 0 and item.left_stamp < os.time() then
        if tonumber(item.category) == GameUIActivityNew.category.TYPE_DNA or tonumber(item.category) == GameUIActivityNew.category.TYPE_SUPER or tonumber(item.category) == GameUIActivityNew.category.TYPE_DUNGEON or tonumber(item.category) == GameUIActivityNew.category.TYPE_BOX or tonumber(item.category) == GameUIActivityNew.category.TYPE_WORM_HOLE or tonumber(item.category) == GameUIActivityNew.category.TYPE_INTERSTELLAR_ADVENTURE or tonumber(item.category) == GameUIActivityNew.category.TYPE_PAYWALL or tonumber(item.category) == GameUIActivityNew.category.TYPE_KRYPTONLAB or tonumber(item.category) == GameUIActivityNew.category.TYPE_WD or tonumber(item.category) == GameUIActivityNew.category.TYPE_PVP or tonumber(item.category) == GameUIActivityNew.category.TYPE_STORE_BLACK or tonumber(item.category) == GameUIActivityNew.category.TYPE_STARTPOT or tonumber(item.category) == GameUIActivityNew.category.TYPE_STORE or tonumber(item.category) == GameUIActivityNew.category.TYPE_DICE or tonumber(item.category) == GameUIActivityNew.category.TYPE_SPEEDUP or tonumber(item.category) == GameUIActivityNew.category.TYPE_MEDALRECUIT or tonumber(item.category) == GameUIActivityNew.category.TYPE_MEDALSHOP or tonumber(item.category) == GameUIActivityNew.category.TYPE_LARGEMAP or tonumber(item.category) == GameUIActivityNew.category.TYPE_EXPEDITION then
          if tonumber(item.category) == GameUIActivityNew.category.TYPE_SPEEDUP then
            self:GetFlashObject():InvokeASCallback("_root", "lua2fs_showSpeedUpButton", true)
          else
            self:GetFlashObject():InvokeASCallback("_root", "lua2fs_setShowEnterBtn", true)
          end
          DebugOut("\232\189\172\230\141\162\228\184\186\230\180\187\229\138\168\229\188\128\229\167\139\231\154\132\231\138\182\230\128\129")
          item.left_stamp = item.persistent_time + item.left_stamp
          item.left_seconds = item.persistent_time
          activityListTimeText[GameUIActivityNew.activityContentIndex].title = GameLoader:GetGameText("LC_MENU_EVENT_TIME_LEFT_CHAR")
          activityListTimeText[GameUIActivityNew.activityContentIndex].status = 1
        else
          DebugOut("\230\153\174\233\128\154\230\180\187\229\138\168\228\184\141\229\136\135\230\141\162\231\138\182\230\128\129", item.code)
        end
      end
    end
  end
end
local lastSetTime = 0
function GameUIActivityNew:Update(dt)
  if os.time() > lastSetTime then
    GameUIActivityNew:setActivityItemTime()
    lastSetTime = os.time()
  end
  GameUIGachaBox:Update(dt)
  if self:GetFlashObject() then
    self:GetFlashObject():Update(dt)
  end
  if GameUIcomboGacha.isOpenComboGacha then
    GameUIcomboGacha:Update(dt)
  end
end
function GameUIActivityNew:OnFSCommand(cmd, arg)
  DebugActivity("GameUIActivityNew:OnFSCommand", cmd, arg)
  if GameUIcomboGacha.isOpenComboGacha then
    GameUIcomboGacha:OnFSCommand(cmd, arg)
  end
  GameUIGachaBox:OnFSCommand(cmd, arg)
  if cmd == "CloseContent" then
    currentRankKey = ""
    GameUIActivityNew.showContent = false
    GameUIActivityNew.currentWebSite = nil
    self:GetFlashObject():InvokeASCallback("_root", "contentReturn", false)
    GameUIActivityNew.ResetTimerManger()
    GameUIActivityNew:InitActivity()
    GameUIActivityNew:refreshActivityList()
  elseif cmd == "gotoSeeMore" then
    if GameUIActivityNew.currentWebSite then
      ext.http.openURL(GameUIActivityNew.currentWebSite)
    end
  elseif cmd == "showNewsDetail" then
    GameUIActivityNew.currentWebSite = nil
    local itemId = tonumber(arg)
    if itemId ~= 1 then
      GameUIActivityNew.showContent = true
    end
    currentFocusItem = itemId
    DebugOut("ShowNewDetail:", itemId)
    if GameUIActivityNew:tryGotoFestivalActivity(itemId) then
      return true
    end
    DebugOut("ShowNewDetail222:", itemId)
    self:showNewsContent(itemId)
  elseif cmd == "showActivityDetail" then
    local itemId = tonumber(arg)
    self:showNewsContent(itemId)
  elseif cmd == "updateNewList" then
    local itemId = tonumber(arg)
    self:SetNewsListItem(itemId)
  elseif cmd == "updateActivtity" then
    local itemId = tonumber(arg)
    self:setActivityItem(itemId)
  elseif cmd == "showactivity" then
    self:ShowNews("activity")
  elseif cmd == "updateActivitContenRewardType2" then
    local itemId = tonumber(arg)
    self:SetActivityContentRewardType2(itemId)
  elseif cmd == "receiveActivityContentRewardType2" then
    local itemId = tonumber(arg)
    GameUIActivityNew:GetActivityContentRewardType2(itemId)
  elseif cmd == "buycutoffCard" then
    local itemId = arg
    GameUIActivityNew:GetActivityContentRewardType3(itemId)
  elseif cmd == "recharge_released" then
    if string.len(arg) == 0 then
      GameTip:Show(GameLoader:GetGameText("LC_MENU_EXCHANGE_CHAR"), 2000)
    else
      DebugActivity("input exchange code = ", arg)
      local param = {code = arg}
      NetMessageMgr:SendMsg(NetAPIList.verify_redeem_code_req.Code, param, GameUIActivityNew.exchangeCallback, true, nil)
    end
  elseif cmd == "gotoContent" then
    local itemId = tonumber(arg)
    if GameUIActivityNew:tryGotoFestivalActivity(itemId) then
      return true
    end
    GameUIActivityNew:gotoContent(currentCategory, currentSubCategory)
  elseif cmd == "move_out" or cmd == "close_menu" then
    currentFocusItem = 1
    GameUIBarLeft:setHaveNewActivity(GameUIBarLeft.isNewActivity)
    GameStateManager:GetCurrentGameState():EraseObject(GameUIActivityNew)
    DebugOut("remove ui activity new")
    local GameStateMainPlanet = GameStateManager.GameStateMainPlanet
    GameStateMainPlanet.Cover = nil
    if not GameStateMainPlanet.FirstShowSign and immanentversion == 2 then
      GameStateMainPlanet.FirstShowSign = true
      GameStateMainPlanet:CheckShowTutorial()
    end
  elseif cmd == "showRank" then
    self:getRankInfo(currentRankKey)
  elseif cmd == "showReward" then
    self:getRewardInfo()
  elseif cmd == "needUpdateRankItem" then
    self:UpdateLeaderboardItem(tonumber(arg))
  elseif cmd == "closeRank" then
  elseif cmd == "rankTabClicked" then
    local tab = tonumber(arg)
    self:CheckToLeaderBoard(tab, false)
  elseif cmd == "RewardItemReleased" then
    local rewardSecIndex_str, rewardItemIndex_str = arg:match("(%d+)\001(%d+)")
    local rewardSecIndex = tonumber(rewardSecIndex_str)
    local rewardItemIndex = tonumber(rewardItemIndex_str)
    local _rewardItems = (self.currentRewardList[rewardSecIndex] or {}).items or {}
    local _rewardItem = _rewardItems[rewardItemIndex]
    if _rewardItem then
      self:showItemDetil(_rewardItem)
    end
  elseif cmd == "bank_gift" then
    local bid_s, pid = arg:match("(%d+)\001([^\n]+)")
    DebugOut(string.format("bank_gift: %s %s", bid_s, pid))
    GameUIActivityNew:ShowBankGift(tonumber(bid_s), pid)
  elseif cmd == "bank_buy" then
    local bid_s, pid = arg:match("(%d+)\001([^\n]+)")
    DebugOut(string.format("bank_buy: %s %s", bid_s, pid))
    self:Bank_Buy(tonumber(bid_s), pid)
  elseif cmd == "bank_use" then
    GameUIActivityNew:BankUse(tonumber(arg))
  elseif cmd == "bank_get" then
    GameUIActivityNew:BankGet(tonumber(arg))
  elseif cmd == "bank_real_gift" then
    local player_id, player_name = arg:match("([^\001]+)\001([^\001]+)")
    if player_id == "undefined" then
      player_id = nil
    end
    if player_name == "undefined" then
      player_name = nil
    end
    self:BankRealGift(player_id, player_name)
  elseif cmd == "bank_go_fried" then
    GameUIFriend.OnEraseCallBack = GameUIActivityNew.RefreshBankGiftSurface
    GameUIFriend:ShowAddFriendUIAndFetchFriends()
  elseif cmd == "bank_help" then
    GameUIWDStuff:Show(GameUIWDStuff.menuType.menu_type_help)
    GameUIWDStuff:SetHelpText(GameLoader:GetGameText("LC_MENU_BANK_HELP_TEXT"))
  elseif cmd == "needUpdateInfoItem" then
    self:UpdateInfoItem(arg)
  elseif cmd == "chris_store" then
    GameStateManager:GetCurrentGameState():AddObject(GameCommonGoodsList)
  elseif cmd == "needUpdateRewardItem" then
    self:UpdateRewardItem(arg)
  elseif cmd == "showRewardList" then
    self:RefreshReward()
  elseif cmd == "btntechpoin" then
    self.currentSpeed = self.SpeedType.technique
    local itemId = 1
    local item = self:GetSpeedItem(itemId)
    GameUIActivityNew:gotoContent(currentCategory, item.dungeon_id)
  elseif cmd == "btngold" then
    self.currentSpeed = self.SpeedType.gold
    local itemId = 2
    local item = self:GetSpeedItem(itemId)
    GameUIActivityNew:gotoContent(currentCategory, item.dungeon_id)
  elseif cmd == "btnpretige" then
    self.currentSpeed = self.SpeedType.pretige
    local itemId = 3
    local item = self:GetSpeedItem(itemId)
    GameUIActivityNew:gotoContent(currentCategory, item.dungeon_id)
  elseif cmd == "techpoinFinish" then
    do
      local itemId = 1
      local item = self:GetSpeedItem(itemId)
      local textInfo = string.gsub(GameLoader:GetGameText("LC_MENU_AUTO_COMPLETE_INFO"), "<number>", GameHelper:GetItemText(item.cost_item.item_type, item.cost_item.number, item.cost_item.no))
      DebugOut("textInfo = " .. textInfo)
      local function callBack()
        GameUIActivityNew:AutoFinishSpeed(itemId)
      end
      GameUtils:CreditCostConfirm(textInfo, callBack, false, nil)
    end
  elseif cmd == "goldFinish" then
    do
      local itemId = 2
      local item = self:GetSpeedItem(itemId)
      local textInfo = string.gsub(GameLoader:GetGameText("LC_MENU_AUTO_COMPLETE_INFO"), "<number>", GameHelper:GetItemText(item.cost_item.item_type, item.cost_item.number, item.cost_item.no))
      DebugOut("textInfo = " .. textInfo)
      local function callBack()
        GameUIActivityNew:AutoFinishSpeed(itemId)
      end
      GameUtils:CreditCostConfirm(textInfo, callBack, false, nil)
    end
  elseif cmd == "pretigeFinish" then
    do
      local itemId = 3
      local item = self:GetSpeedItem(itemId)
      local textInfo = string.gsub(GameLoader:GetGameText("LC_MENU_AUTO_COMPLETE_INFO"), "<number>", GameHelper:GetItemText(item.cost_item.item_type, item.cost_item.number, item.cost_item.no))
      DebugOut("textInfo = " .. textInfo)
      local function callBack()
        GameUIActivityNew:AutoFinishSpeed(itemId)
      end
      GameUtils:CreditCostConfirm(textInfo, callBack, false, nil)
    end
  elseif cmd == "item_released" then
    local id = GameUIActivityNew.fleetInfo["id" .. arg]
    local item = {}
    item.number = id
    item.cnt = 1
    item.no = 1
    item.item_type = "item"
    ItemBox:showItemBox("Item", item, item.number, 320, 480, 200, 200, "", nil, "", nil)
  elseif cmd == "fleet_released" then
    local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
    ItemBox:ShowCommanderDetail2(GameUIActivityNew.fleetInfo.fleetId)
  elseif cmd == "CallRelease" then
    self:RequestCall()
  end
  GameTextEdit:fscommand(self:GetFlashObject(), cmd, arg)
end
function GameUIActivityNew:gotoContent(currentCategory, currentSubCategory)
  local modules_status = GameGlobalData:GetData("modules_status")
  DebugOut("currentCategory", currentCategory)
  DebugOut("currentSubCategory", currentSubCategory)
  if tonumber(currentCategory) == GameUIActivityNew.category.TYPE_DNA then
    GameUIcomboGacha:Init(GameUIActivityNew:GetFlashObject())
    GameUIcomboGacha:checkNetGateWayInfo()
    GameUIcomboGacha:RequestComboGachaInfo()
    if GameUIActivityNew.activityContentIndex == GameUIActivityNew.combogachaItemid and QuestTutorialComboGacha:IsActive() then
      local function callback()
        AddFlurryEvent("Gacha_Dialog_4", {}, 1)
        self:GetFlashObject():InvokeASCallback("_root", "ShowComboGachaBuybuttonTip")
      end
      AddFlurryEvent("Gacha_ClickMofeiEntrance", {}, 1)
      GameUICommonDialog:PlayStory({1100060}, callback)
    else
      DebugOut("HideComboGachaBuybuttonTip")
      self:GetFlashObject():InvokeASCallback("_root", "HideComboGachaBuybuttonTip")
    end
  elseif tonumber(currentCategory) == GameUIActivityNew.category.TYPE_SUPER then
  elseif tonumber(currentCategory) == GameUIActivityNew.category.TYPE_DUNGEON then
    GameStateManager.GameStateInstance.category = currentCategory
    GameStateManager.GameStateInstance.currentInstanceId = currentSubCategory
    GameObjectClimbTower:preRequestInstanceData(currentSubCategory)
  elseif tonumber(currentCategory) == GameUIActivityNew.category.TYPE_BOX then
    self:showDetailWithEnter()
  elseif tonumber(currentCategory) == GameUIActivityNew.category.TYPE_WORM_HOLE then
    GameStateManager.GameStateInstance.category = currentCategory
    GameStateManager.GameStateInstance.currentInstanceId = currentSubCategory
    GameObjectClimbTower:preRequestInstanceData(currentSubCategory)
  elseif tonumber(currentCategory) == GameUIActivityNew.category.TYPE_EXPEDITION then
    GameStateManager.GameStateInstance.category = currentCategory
    GameStateManager.GameStateInstance.currentInstanceId = currentSubCategory
    GameObjectClimbTower:preRequestInstanceData(currentSubCategory)
  elseif tonumber(currentCategory) == GameUIActivityNew.category.TYPE_INTERSTELLAR_ADVENTURE then
    InterstellarAdventure:Init()
    InterstellarAdventure:EnterInterstellarAdventure(currentSubCategory)
  elseif tonumber(currentCategory) == GameUIActivityNew.category.TYPE_PAYWALL then
    GameVip:showVip(false, function()
    end)
  elseif tonumber(currentCategory) == GameUIActivityNew.category.TYPE_KRYPTONLAB then
    if GameUtils:IsModuleUnlock("krypton") then
      GameStateKrypton:EnterKrypton(GameStateKrypton.LAB)
    else
      GameUIActivityNew:ShowTipsEnterGachaFailed()
    end
  elseif tonumber(currentCategory) == GameUIActivityNew.category.TYPE_WD then
    DebugOut("wd-=-========>" .. tostring(GameUtils:IsModuleUnlock("wd")))
    if GameUtils:IsModuleUnlock("wd") then
      GameObjectMainPlanet:BuildingClicked("world_domination")
    else
      GameUIActivityNew:ShowTipsEnterGachaFailed()
    end
  elseif tonumber(currentCategory) == GameUIActivityNew.category.TYPE_PVP then
    if GameUtils:IsModuleUnlock("arena") then
      GameUIArena:SetArenaType(GameUIArena.ARENA_TYPE.normalArena)
      GameStateManager:SetCurrentGameState(GameStateManager.GameStateArena)
    else
      GameUIActivityNew:ShowTipsEnterGachaFailed()
    end
  elseif tonumber(currentCategory) == GameUIActivityNew.category.TYPE_STORE_BLACK then
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateStore)
    GameGoodsList:EntryStore(2)
  elseif tonumber(currentCategory) == GameUIActivityNew.category.TYPE_STARTPOT then
    if GameUtils:IsModuleUnlock("hire") then
      GameStateManager:SetCurrentGameState(GameStateRecruit)
    else
      GameUIActivityNew:ShowTipsEnterGachaFailed()
    end
  elseif tonumber(currentCategory) == GameUIActivityNew.category.TYPE_STORE then
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateStore)
    GameGoodsList:EntryStore(1)
  elseif tonumber(currentCategory) == GameUIActivityNew.category.TYPE_DICE then
    GameUIDice:SetActiveId(currentSubCategory)
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateDice)
  elseif tonumber(currentCategory) == GameUIActivityNew.category.TYPE_SPEEDUP then
    GameStateManager.GameStateInstance.category = currentCategory
    GameStateManager.GameStateInstance.currentInstanceId = currentSubCategory
    GameObjectClimbTower:preRequestInstanceData(currentSubCategory)
  elseif tonumber(currentCategory) == GameUIActivityNew.category.TYPE_ACTIVITYEVENT then
    GameUIActivityEvent.curEventId = tonumber(currentSubCategory)
    GameStateManager:GetCurrentGameState():AddObject(GameUIActivityEvent)
  elseif tonumber(currentCategory) == GameUIActivityNew.category.TYPE_UPDATEVERSION then
    local localAppid = ext.GetBundleIdentifier()
    if GameUtils.AppDownLoadURL[localAppid] then
      ext.http.openURL(GameUtils.AppDownLoadURL[localAppid])
    elseif GameUtils.WorldPack[localAppid] then
      ext.http.openURL("http://tap4fun.com/en/game/7")
    else
      ext.http.openURL("http://yh.t4f.cn/")
    end
  elseif tonumber(currentCategory) == GameUIActivityNew.category.TYPE_SCRATCH then
    GameStateManager.GameStateScratchGacha.act_id = currentSubCategory
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateScratchGacha)
  elseif tonumber(currentCategory) == GameUIActivityNew.category.TYPE_NEWSIGN then
    local GameStateMainPlanet = GameStateManager.GameStateMainPlanet
    GameStateMainPlanet.Cover = nil
    GameUIActivity:ShowNews("newSign")
  elseif tonumber(currentCategory) == GameUIActivityNew.category.TYPE_MEDALRECUIT then
    if not GameGlobalData:GetModuleStatus("medal") then
      GameTip:Show(GameLoader:GetGameText("LC_MENU_MEDAL_LEVEL_ALERT"))
      return
    end
    local toactivity = function()
      GameStateManager:SetCurrentGameState(GameStateManager.GameStateStarSystem.m_previousState)
    end
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateStarSystem)
    local GameUIStarSystemPort = LuaObjectManager:GetLuaObject("GameUIStarSystemPort")
    GameUIStarSystemPort:JumpTo("chouka", toactivity)
  elseif tonumber(currentCategory) == GameUIActivityNew.category.TYPE_MEDALSHOP then
    if not GameGlobalData:GetModuleStatus("medal") then
      GameTip:Show(GameLoader:GetGameText("LC_MENU_MEDAL_LEVEL_ALERT"))
      return
    end
    local toactivity = function()
      GameStateManager:SetCurrentGameState(GameStateManager.GameStateStarSystem.m_previousState)
    end
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateStarSystem)
    local GameUIStarSystemPort = LuaObjectManager:GetLuaObject("GameUIStarSystemPort")
    GameUIStarSystemPort:JumpTo("shop", toactivity)
  elseif tonumber(currentCategory) == GameUIActivityNew.category.TYPE_LARGEMAP then
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateLargeMap)
  else
    DebugOut("error category")
  end
end
function GameUIActivityNew:ShowTipsEnterGachaFailed(...)
  GameTip:Show(GameLoader:GetGameText("LC_MENU_UNLOCK_LEVEL_INFO"))
end
function GameUIActivityNew.exchangeCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code then
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.verify_redeem_code_ack.Code then
    DebugActivity("exchangeCallback content = ")
    DebugActivityTable(content)
    for key, v in pairs(content.awards) do
      local nameText = GameHelper:GetAwardText(v.item_type, v.number, v.no)
      local tip_text = GameLoader:GetGameText("LC_MENU_EXCHANGE_ALERT")
      tip_text = string.format(tip_text, nameText)
      GameTip:Show(tip_text)
    end
    return true
  end
  return false
end
function GameUIActivityNew:tryGotoFestivalActivity(itemId)
  DebugOut("tryGotoFestivalActivity", itemId)
  local item = activityList[itemId]
  if item == nil then
    DebugOut("error itemId")
    return false
  end
  DebugOut("is_wd ", item.is_wd)
  if item.is_wd == GameUIActivityNew.activityType.Super or item.is_wd == GameUIActivityNew.activityType.SuperAndWD then
    DebugOut("Super / SuperAndWD")
    GameSettingData.EnterThanksEventAlready = 1
    GameUtils:SaveSettingData()
    local levelInfo = GameGlobalData:GetData("levelinfo")
    DebugOut("event level req", levelInfo.level, eventLevelDef)
    if eventLevelDef > levelInfo.level then
      GameUtils:ShowTips(string.format(GameLoader:GetGameText("LC_MENU_ENVENT_JOIN_LOWLEVEL_ALERT"), eventLevelDef))
    else
      DebugOut("currentFestivalType", currentFestivalType)
      if currentFestivalType == GameUIActivityNew.FestivalType.Finished_Request then
        GameUIActivityNew:ShowEvent(GameUIActivityNew.DetailType.FESTIVAL)
      elseif currentFestivalType == GameUIActivityNew.FestivalType.Watting or currentFestivalType == GameUIActivityNew.FestivalType.Request_Available_Activity then
        DebugOut("\230\173\163\229\156\168\232\175\183\230\177\130\230\149\176\230\141\174,\232\175\183\231\168\141\229\144\142\229\134\141\232\175\149\232\175\149")
      elseif currentFestivalType == GameUIActivityNew.FestivalType.Error then
        DebugOut("festival_req error(\230\180\187\229\138\168\229\136\151\232\161\168\231\154\132\228\191\161\230\129\175 \229\146\140 \229\164\167\229\158\139\230\180\187\229\138\168\231\189\145\229\133\179\228\191\161\230\129\175\228\184\141\228\184\128\232\135\180)")
      end
    end
    return true
  elseif item.is_wd == GameUIActivityNew.activityType.WDandNormal then
    DebugOut("WDandNormal")
    return false
  end
  return false
end
function GameUIActivityNew:RequestCall()
  DebugOut("request call ", GameUIActivityNew.fleetInfo.buyId)
  local reqType = GameUIActivityNew.NetMegType.FESTIVAL
  if GameUIActivityNew.curDetailType == GameUIActivityNew.DetailType.WD_EVENT then
    reqType = GameUIActivityNew.NetMegType.WD_EVENT
  end
  local shop_purchase_req_param = {
    store_type = reqType,
    id = GameUIActivityNew.fleetInfo.buyId,
    buy_times = 1
  }
  NetMessageMgr:SendMsg(NetAPIList.activity_stores_buy_req.Code, shop_purchase_req_param, GameUIActivityNew.BuyItemCallback, true, nil)
end
function GameUIActivityNew.BuyItemCallback(msgType, content)
  DebugOut("call ack")
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.activity_stores_buy_req.Code then
    if content.api == NetAPIList.activity_stores_buy_req.Code then
      if content.code ~= 0 then
        GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      else
        GameUtils:ShowTips(GameLoader:GetGameText("LC_MENU_EVENT_XMARS_GIFT_SUC_CHAR"))
      end
    end
    return true
  end
  return false
end
function GameUIActivityNew:ResetState()
  GameUIActivityNew.fleetInfo = {}
  GameUIActivityNew.fleetInfo.isOn = false
  GameUIActivityNew.fleetInfo.fleetId = -1
  GameUIActivityNew.fleetInfo.fleetPrice = {}
  GameUIActivityNew.fleetInfo.fleetPrice.p1 = -1
  GameUIActivityNew.fleetInfo.fleetPrice.p2 = -1
  GameUIActivityNew.fleetInfo.fleetPrice.p3 = -1
  GameUIActivityNew.fleetInfo.id1 = -1
  GameUIActivityNew.fleetInfo.id2 = -1
  GameUIActivityNew.fleetInfo.id3 = -1
  GameUIActivityNew.fleetInfo.frame1 = "empty"
  GameUIActivityNew.fleetInfo.frame2 = "empty"
  GameUIActivityNew.fleetInfo.frame3 = "empty"
  GameUIActivityNew.fleetInfo.title = ""
  GameUIActivityNew.fleetInfo.desc = ""
  GameUIActivityNew.fleetInfo.buffUrl = ""
  GameUIActivityNew.fleetInfo.fleetUrl = ""
  GameUIActivityNew.fleetInfo.buffExist = false
  GameUIActivityNew.fleetInfo.fleetExist = false
end
function GameUIActivityNew:ShowEvent(type)
  DebugOut("GameUIActivityNew:ShowEvent")
  GameUIActivityNew:ResetState()
  self.curDetailType = type
  for i, v in ipairs(self.festivalDetailDataset) do
    if self.curDetailType == v.detailType then
      self.curDetail = v
      self.isLeaderboardOn = v.details.isLeaderboardOn
      break
    end
  end
  if self.curDetail == nil then
    DebugOut("GameUIActivityNew:ShowEvent: self.curDetail == nil")
    return
  end
  DebugOut("QJ GameUIActivityNew:ShowEvent")
  DebugTable(self.curDetail)
  self.receiveFestivalInfoTime = self.curDetail.receiveInfoTime
  self.leftTime = self.curDetail.timeInfo.end_time - self.curDetail.timeInfo.current_time
  self.endTime = self.curDetail.timeInfo.end_time
  self:SetExchangeInfo(self.curDetail.details.exchangeInfo)
  self:SetRulesInfo(self.curDetail.details.rulesInfo)
  self:SetEventInfo(self.curDetail.details)
  self:SetRewardInfo(self.curDetail.details.rewardInfo)
  self:SetPointsInfo(self.curDetail.details.pointsInfo)
  self:SetFleetInfo(self.curDetail.details.callInfo)
  DebugOut("1")
  GameUIActivityNew:GetFlashObject():InvokeASCallback("_root", "initS_Event", GameUIActivityNew.title, self.curDetail.details.callInfo.isCallOn)
  DebugOut("2")
  local itemCount = #GameUIActivityNew.rulesInfo.descs
  for i = 1, itemCount do
    GameUIActivityNew:GetFlashObject():InvokeASCallback("_root", "addInfoListItem", i)
  end
end
GameUIActivityNew.rulesInfo = {}
GameUIActivityNew.rulesInfo.descs = {}
GameUIActivityNew.rewardInfo = {}
GameUIActivityNew.rewardInfo.rewards = {}
GameUIActivityNew.pointsInfo = {}
GameUIActivityNew.fleetInfo = {}
GameUIActivityNew.fleetInfo.fleetPrice = {}
function GameUIActivityNew:SetExchangeInfo(exchangeInfo)
  DebugOut("setExchangeInfo")
  DebugTable(exchangeInfo)
  GameUIActivityNew.isExchangeOn = exchangeInfo.isExchangeOn
  if GameUIActivityNew.isExchangeOn then
    GameCommonGoodsList:SetID(exchangeInfo)
  end
end
function GameUIActivityNew:SetRulesInfo(rulesInfo)
  DebugOut("setRulesInfo")
  DebugTable(rulesInfo)
  for i = 1, #rulesInfo.rulesDesc do
    GameUIActivityNew.rulesInfo.descs[i] = rulesInfo.rulesDesc[i]
    DebugOut("GameUIActivityNew:SetRulesInfo ", GameUIActivityNew.rulesInfo.descs[i])
  end
end
function GameUIActivityNew:SetEventInfo(eventInfo)
  DebugOut("setEventInfo")
  DebugTable(eventInfo)
  GameUIActivityNew.title = eventInfo.title
  GameUIActivityNew.subTitle = eventInfo.subTitle
  GameUIActivityNew.desc = eventInfo.desc
end
function GameUIActivityNew:SetRewardInfo(rewardInfo)
  DebugOut("setRewardInfo")
  DebugTable(rewardInfo)
  GameUIActivityNew.rewardInfo.rewardPicUrl = rewardInfo.rewardPicUrl
  GameUIActivityNew.rewardInfo.rewardDesc = rewardInfo.rewardDesc
  for i = 1, #rewardInfo.rewards do
    DebugTable(rewardInfo.rewards[i])
    GameUIActivityNew.rewardInfo.rewards[i] = rewardInfo.rewards[i]
  end
end
function GameUIActivityNew:SetPointsInfo(pointsInfo)
  DebugOut("setPointsInfo")
  DebugTable(pointsInfo)
  GameUIActivityNew.pointsInfo.isText = pointsInfo.isPointsText
  if GameUIActivityNew.pointsInfo.isText then
    GameUIActivityNew.pointsInfo.id = pointsInfo.pointId
  end
end
function GameUIActivityNew:SetFleetInfo(fleetInfo)
  DebugOut("setFleetInfo " .. debug.traceback())
  DebugTable(fleetInfo)
  GameUIActivityNew.fleetInfo.isOn = fleetInfo.isCallOn
  if GameUIActivityNew.fleetInfo.isOn then
    for i = 1, #fleetInfo.items do
      GameUIActivityNew.fleetInfo["id" .. i] = fleetInfo.items[i]
    end
    GameUIActivityNew.fleetInfo.title = fleetInfo.callTitle
    GameUIActivityNew.fleetInfo.desc = fleetInfo.callDesc
    GameUIActivityNew.fleetInfo.buffUrl = fleetInfo.buffPicUrl
    GameUIActivityNew.fleetInfo.fleetUrl = fleetInfo.fleetPicUrl
  end
  GameUIActivityNew:TrySendGoodsListReq()
  GameUIActivityNew:SetFleetContent()
end
function GameUIActivityNew:SetFleetContent()
  self:InitFleetInfo()
  self:RefreshCanCall()
  self:SetFleetInfoIcon()
  self:SetFleetInfoBanner()
  self:SetFleetInfoText()
end
function GameUIActivityNew:SetFleetInfoIcon()
  if GameUIActivityNew.fleetInfo.isOn then
    self:GetFlashObject():InvokeASCallback("_root", "SetFleetIcon", self.fleetInfo.frame1, self.fleetInfo.frame2, self.fleetInfo.frame3)
  end
end
function GameUIActivityNew:SetFleetInfoBanner()
end
function GameUIActivityNew:SetFleetInfoText()
  if GameUIActivityNew.fleetInfo.isOn then
    GameUIActivityNew:GetFlashObject():InvokeASCallback("_root", "SetFleetInfoText", GameUIActivityNew.fleetInfo.title, GameUIActivityNew.fleetInfo.desc)
  end
end
function GameUIActivityNew:TrySendGoodsListReq()
  if GameUIActivityNew.fleetInfo.isOn then
    local reqType = GameUIActivityNew.NetMegType.FESTIVAL
    if GameUIActivityNew.curDetailType == GameUIActivityNew.DetailType.WD_EVENT then
      reqType = GameUIActivityNew.NetMegType.WD_EVENT
    end
    DebugOut("---------------zm test-------------request goodsList")
    local stores_req_content = {
      store_type = reqType,
      locale = GameSettingData.Save_Lang
    }
    NetMessageMgr:SendMsg(NetAPIList.activity_stores_req.Code, stores_req_content, GameUIActivityNew.GoodsListCallback, true, nil)
  end
end
function GameUIActivityNew.GoodsListCallback(msgType, content)
  DebugOut("receive fleet list res")
  if msgType == NetAPIList.activity_stores_ack.Code then
    local initList = true
    DebugOut("zmtest:")
    DebugTable(content)
    for i = #content.items, 1, -1 do
      if content.items[i].item_type ~= "fleet" then
        DebugOut("remove item", i)
        table.remove(content.items, i)
      end
    end
    DebugOut("after remove")
    DebugTable(content)
    GameUIActivityNew.goodsList = content
    if initList and #content.items > 0 then
      local price = {}
      local p1, p2, p3 = 0, 0, 0
      for k1, v1 in ipairs(content.items[1].items) do
        DebugOut("=====item id", v1.item_id)
        DebugOut("=====item count", v1.item_no)
        if v1.item_id == GameUIActivityNew.fleetInfo.id1 then
          p1 = v1.item_no
        elseif v1.item_id == GameUIActivityNew.fleetInfo.id2 then
          p2 = v1.item_no
        elseif v1.item_id == GameUIActivityNew.fleetInfo.id3 then
          p3 = v1.item_no
        end
      end
      GameUIActivityNew.fleetInfo.fleetPrice.p1 = p1
      GameUIActivityNew.fleetInfo.fleetPrice.p2 = p2
      GameUIActivityNew.fleetInfo.fleetPrice.p3 = p3
      GameUIActivityNew.fleetInfo.buyId = content.items[1].id
      GameUIActivityNew.fleetInfo.fleetId = content.items[1].item_id
      GameUIActivityNew:RefreshResource()
      GameUIActivityNew:RefreshCanCall()
    end
    return true
  end
  return false
end
function GameUIActivityNew:RefreshResource()
  if not GameUIActivityNew.fleetInfo.isOn then
    return
  end
  local resource = GameGlobalData:GetData("item_count")
  local flash_obj = GameUIActivityNew:GetFlashObject()
  if flash_obj and GameUIActivityNew.fleetInfo.fleetPrice.p1 ~= -1 then
    local item_tree_count = 0
    local item_suger_count = 0
    local item_jew_count = 0
    for i, v in ipairs(resource.items) do
      if v.item_id == GameUIActivityNew.fleetInfo.id1 then
        item_tree_count = item_tree_count + v.item_no
      elseif v.item_id == GameUIActivityNew.fleetInfo.id2 then
        item_suger_count = item_suger_count + v.item_no
      elseif v.item_id == GameUIActivityNew.fleetInfo.id3 then
        item_jew_count = item_jew_count + v.item_no
      end
    end
    local price1 = ""
    local price2 = ""
    local price3 = ""
    price1 = "<font color='#FFCC00'>" .. item_tree_count .. "</font>/" .. GameUIActivityNew.fleetInfo.fleetPrice.p1
    price2 = "<font color='#FFCC00'>" .. item_suger_count .. "</font>/" .. GameUIActivityNew.fleetInfo.fleetPrice.p2
    price3 = "<font color='#FFCC00'>" .. item_jew_count .. "</font>/" .. GameUIActivityNew.fleetInfo.fleetPrice.p3
    DebugOut("set fleet price", price1, price2, price3)
    flash_obj:InvokeASCallback("_root", "RefreshResource", price1, price2, price3)
  end
end
function GameUIActivityNew:RefreshCanCall()
  DebugOut("GameUIActivityNew:RefreshCanCall")
  if not GameUIActivityNew.fleetInfo.isOn then
    return
  end
  local flash_obj = GameUIActivityNew:GetFlashObject()
  if flash_obj then
    local fleets = GameGlobalData:GetData("fleetinfo").fleets
    local isExist = false
    DebugTable(fleets)
    for k, v in ipairs(fleets) do
      if v.identity == GameUIActivityNew.fleetInfo.fleetId then
        isExist = true
        break
      end
    end
    if not isExist then
      DebugOut("call bnt visible")
      flash_obj:InvokeASCallback("_root", "setCallBntVisible", true)
    else
      DebugOut("call bnt un visible")
      flash_obj:InvokeASCallback("_root", "setCallBntVisible", false)
    end
  end
end
function GameUIActivityNew:InitFleetInfo()
  if GameUIActivityNew.fleetInfo.isOn then
    if not GameUIActivityNew.fleetInfo.buffExist then
      local pngName = string.match(GameUIActivityNew.fleetInfo.buffUrl, "([^/]*)$")
      local localPath = "data2/" .. pngName
      local replaceName = "buff.png"
      if DynamicResDownloader:IfResExsit(localPath) then
        if GameUIActivityNew:GetFlashObject() then
          ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
          GameUIActivityNew:GetFlashObject():ReplaceTexture(replaceName, pngName)
        end
      else
        local extInfo = {}
        extInfo.pngName = pngName
        extInfo.localPath = localPath
        extInfo.replaceName = replaceName
        DynamicResDownloader:AddDynamicRes(pngName, DynamicResDownloader.resType.FESTIVAL_BANNER, extInfo, GameUIActivityNew.bannerDownloaderCallback)
      end
    end
    if not GameUIActivityNew.fleetInfo.fleetExist then
      local pngName = string.match(GameUIActivityNew.fleetInfo.fleetUrl, "([^/]*)$")
      local localPath = "data2/" .. pngName
      local replaceName = "christmas_card.png"
      if DynamicResDownloader:IfResExsit(localPath) then
        if GameUIActivityNew:GetFlashObject() then
          ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
          GameUIActivityNew:GetFlashObject():ReplaceTexture(replaceName, pngName)
        end
        GameUIActivityNew:GetFlashObject():InvokeASCallback("_root", "unShowCardText")
      else
        local extInfo = {}
        extInfo.pngName = pngName
        extInfo.localPath = localPath
        extInfo.replaceName = replaceName
        function extInfo.callback()
          GameUIActivityNew:GetFlashObject():InvokeASCallback("_root", "unShowCardText")
        end
        DynamicResDownloader:AddDynamicRes(pngName, DynamicResDownloader.resType.FESTIVAL_BANNER, extInfo, GameUIActivityNew.bannerDownloaderCallback)
      end
    end
    for i = 1, 3 do
      local id = "id" .. i
      local frame = "frame" .. i
      DebugOut("asdjakjsdjk:", self.fleetInfo[id], self.fleetInfo[frame])
      if self.fleetInfo[id] ~= -1 and self.fleetInfo[frame] == "empty" then
        if DynamicResDownloader:IsDynamicStuff(self.fleetInfo[id], DynamicResDownloader.resType.PIC) then
          local fullFileName = DynamicResDownloader:GetFullName(self.fleetInfo[id] .. ".png", DynamicResDownloader.resType.PIC)
          local localPath = "data2/" .. fullFileName
          if DynamicResDownloader:IfResExsit(localPath) then
            ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
            self.fleetInfo[frame] = "item_" .. self.fleetInfo[id]
          else
            self.fleetInfo[frame] = "temp"
            self:AddDownloadPath(self.fleetInfo[id], id, frame, donamicDownloadFinishCallback1)
          end
        else
          self.fleetInfo[frame] = "item_" .. self.fleetInfo[id]
        end
      end
    end
  end
end
function GameUIActivityNew:UpdateInfoItem(id)
  local itemKey = tonumber(id)
  if itemKey > #GameUIActivityNew.rulesInfo.descs then
    return
  end
  self:GetFlashObject():InvokeASCallback("_root", "setInfoItem", itemKey, GameUIActivityNew.rulesInfo.descs[itemKey])
end
function GameUIActivityNew:showItemDetil(item_)
  local item = item_
  local item_type = item.item_type
  if GameHelper:IsResource(item_type) then
    return
  end
  if item_type == "krypton" then
    if item.level == 0 then
      item.level = 1
    end
    local detail = GameUIKrypton:TryQueryKryptonDetail(item.number, item.level, function(msgType, content)
      local ret = GameUIKrypton.QueryKryptonDetailCallBack(msgType, content)
      if ret then
        local tmp = GameGlobalData:GetKryptonDetail(item.number, item.level)
        if tmp == nil then
          return false
        end
        ItemBox:SetKryptonBox(tmp, item.number)
        local operationTable = {}
        operationTable.btnUnloadVisible = false
        operationTable.btnUpgradeVisible = false
        operationTable.btnUseVisible = false
        operationTable.btnDecomposeVisible = false
        ItemBox:ShowKryptonBox(320, 240, operationTable)
      end
      return ret
    end)
    if detail == nil then
    else
      ItemBox:SetKryptonBox(detail, item.number)
      local operationTable = {}
      operationTable.btnUnloadVisible = false
      operationTable.btnUpgradeVisible = false
      operationTable.btnUseVisible = false
      operationTable.btnDecomposeVisible = false
      ItemBox:ShowKryptonBox(320, 240, operationTable)
    end
  end
  if item_type == "item" then
    item.cnt = 1
    ItemBox:showItemBox("Item", item, tonumber(item.number), 320, 480, 200, 200, "", nil, "", nil)
  end
  if item_type == "fleet" then
    ItemBox:ShowCommanderDetail2(tonumber(item.number))
  end
end
function GameUIActivityNew:RefreshReward()
  local havepic = self:InitRewardInfo()
  local itemCount = #GameUIActivityNew.rewardInfo.rewards + 2
  local rewardsCount = 0
  if GameUIActivityNew.rewardsList == nil then
    rewardsCount = 0
  else
    rewardsCount = #GameUIActivityNew.rewardsList
  end
  if itemCount > rewardsCount + 2 then
    itemCount = rewardsCount + 2
  end
  GameUIActivityNew:GetFlashObject():InvokeASCallback("_root", "initRewardBox", havepic)
  DebugOut("GameUIActivityNew:RefreshReward")
  DebugTable(GameUIActivityNew.rewardsList)
  for i = 1, itemCount do
    GameUIActivityNew:GetFlashObject():InvokeASCallback("_root", "addRewardListItem", i)
  end
end
function GameUIActivityNew:InitRewardInfo()
  if not GameUIActivityNew.rewardInfo.rewardExist then
    local pngName = string.match(GameUIActivityNew.rewardInfo.rewardPicUrl, "([^/]*)$")
    local localPath = "data2/" .. pngName
    local replaceName = "reward_banner.png"
    if DynamicResDownloader:IfResExsit(localPath) then
      if GameUIActivityNew:GetFlashObject() then
        ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
        GameUIActivityNew:GetFlashObject():ReplaceTexture(replaceName, pngName)
      end
      return true
    else
      local extInfo = {}
      extInfo.pngName = pngName
      extInfo.localPath = localPath
      extInfo.replaceName = replaceName
      DynamicResDownloader:AddDynamicRes(pngName, DynamicResDownloader.resType.FESTIVAL_BANNER, extInfo, GameUIActivityNew.bannerDownloaderCallback)
      return false
    end
  end
  return false
end
function GameUIActivityNew.bannerDownloaderCallback(extInfo)
  if DynamicResDownloader:IfResExsit(extInfo.localPath) and GameUIActivityNew:GetFlashObject() then
    GameUIActivityNew:GetFlashObject():ReplaceTexture(extInfo.replaceName, extInfo.pngName)
    GameUIActivityNew:GetFlashObject():InvokeASCallback("_root", "setLoadTextVisible", false)
    if extInfo.callback then
      extInfo.callback()
    end
  end
end
function GameUIActivityNew:UpdateRewardItem(id)
  local itemKey = tonumber(id)
  if itemKey > #GameUIActivityNew.rewardInfo.rewards + 2 then
    return
  end
  if GameUIActivityNew.rewardsList == nil then
    if itemKey > 2 then
      return
    end
  elseif itemKey > #GameUIActivityNew.rewardsList + 2 then
    return
  end
  if itemKey == 1 then
    self:GetFlashObject():InvokeASCallback("_root", "setRewardItem1")
  elseif itemKey == 2 then
    self:GetFlashObject():InvokeASCallback("_root", "setRewardItem2", GameUIActivityNew.rewardInfo.rewardDesc)
  else
    local reward = GameUIActivityNew.rewardInfo.rewards[itemKey - 2]
    local items = GameUIActivityNew.rewardsList[itemKey - 2]
    local names = ""
    local frames = ""
    local nums = ""
    local levels = ""
    for i = 1, #items do
      if items[i].item_type == "item" or items[i].item_type == "krypton" then
        if DynamicResDownloader:IsDynamicStuff(items[i].number, DynamicResDownloader.resType.PIC) then
          local fullFileName = DynamicResDownloader:GetFullName(items[i].number .. ".png", DynamicResDownloader.resType.PIC)
          local localPath = "data2/" .. fullFileName
          if DynamicResDownloader:IfResExsit(localPath) then
            ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
            frames = frames .. "item_" .. items[i].number .. "\001"
          else
            frames = frames .. "temp" .. "\001"
            self:AddDownloadPath(items[i].number, itemKey, -1, donamicDownloadFinishCallback3)
          end
        else
          frames = frames .. "item_" .. items[i].number .. "\001"
        end
        names = names .. GameLoader:GetGameText("LC_ITEM_ITEM_NAME_" .. items[i].number) .. "\001"
        nums = nums .. "X" .. items[i].no .. "\001"
        if items[i].item_type == "item" then
          levels = levels .. "-1\001"
        else
          levels = levels .. GameLoader:GetGameText("LC_MENU_Level") .. items[i].level .. "\001"
        end
      else
        local f = ""
        local n = ""
        local u = 0
        n, u, f = GameUIBattleResult:GetItemTextAndNum3(items[i].item_type, items[i].no, items[i].number)
        frames = frames .. f .. "\001"
        names = names .. n .. "\001"
        nums = nums .. "X" .. u .. "\001"
        levels = levels .. "-1\001"
      end
    end
    for i = #items + 1, 5 do
      frames = frames .. "empty\001"
      names = names .. "..\001"
      nums = nums .. "0\001"
      levels = levels .. "-1\001"
    end
    DebugOut("setRewardItem", frames, names, nums, levels)
    self:GetFlashObject():InvokeASCallback("_root", "setRewardItem3", itemKey, reward.desc, frames, nums, names, levels)
  end
end
function GameUIActivityNew:GetActivityContentRewardType2(itemId)
  DebugActivity("----\230\137\147\229\141\176 ------>  GetActivityContentRewardType2 ", itemId)
  local itemId = tonumber(itemId)
  GameUIActivityNew.GetActivityRewardType2Index = itemId
  local status = GameUIActivityNew.activityContentRewareType2.sections[itemId].status
  if tonumber(status) ~= 1 then
    return
  end
  GameUIBarLeft:GetActivityReward(GameUIActivityNew.currentActivityContentId, GameUIActivityNew.activityContentRewareType2.sections[itemId].condition)
end
function GameUIActivityNew.CheckActivityType2IsReward()
  if not GameUIActivityNew.GetActivityRewardType2Index then
    return
  end
  if not GameStateManager:GetCurrentGameState():IsObjectInState(GameUIActivityNew) then
    return
  end
  GameUIActivityNew.activityContentRewareType2.sections[GameUIActivityNew.GetActivityRewardType2Index].status = 2
  GameUIActivityNew:updateActivityType2Content(GameUIActivityNew.activityContentRewareType2)
  GameUIActivityNew.GetActivityRewardType2Index = nil
  local item = activityList[GameUIActivityNew.activityContentIndex]
  if item then
    local canAward = false
    for k, v in pairs(GameUIActivityNew.activityContentRewareType2.sections) do
      if v.statusj == 1 then
        canAward = true
      end
    end
    if not canAward and GameUIBarLeft.specialActicityList.news_ids and #GameUIBarLeft.specialActicityList.news_ids > 0 then
      for k, v in pairs(GameUIBarLeft.specialActicityList.news_ids) do
        if tonumber(v) == tonumber(item.code) then
          GameUIBarLeft.specialActicityList.news_ids[k] = -1
        end
      end
    end
  end
end
function GameUIActivityNew:GetActivityContentRewardType3(itemId)
  if itemId == 0 then
    return
  end
  local card_id = GameUIActivityNew.CutOffList[tonumber(itemId)].id
  local content = {id = card_id}
  NetMessageMgr:SendMsg(NetAPIList.buy_cut_off_req.Code, content, GameUIActivityNew.BuyCutoffCardCallback, true)
end
function GameUIActivityNew.BuyCutoffCardCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.buy_cut_off_req.Code then
    local GameVip = LuaObjectManager:GetLuaObject("GameVip")
    GameVip:CheckIsNeedShowVip(content.code)
    return true
  end
  return false
end
function GameUIActivityNew:showDetailWithEnter()
  DebugActivity("show activity detail with enter")
  if not GameStateManager:GetCurrentGameState():IsObjectInState(GameUIActivityNew) then
    GameStateManager:GetCurrentGameState():AddObject(GameUIActivityNew)
    if not GameUIActivityNew:GetFlashObject() then
      GameUIActivityNew:LoadFlashObject()
    end
  end
  GameUIGachaBox:Init(GameUIActivityNew:GetFlashObject(), currentCategory, currentSubCategory)
  GameUIGachaBox:MoveIn()
end
function GameUIActivityNew:showNewsContent(itemId)
  local item
  DebugActivity("GameUIActivityNew showNewsContent", itemId)
  if not itemId then
    return
  end
  item = activityList[itemId]
  DebugOut("item:")
  DebugTable(item)
  GameUIActivityNew.activityContentIndex = itemId
  GameUIActivityNew:setNoticeIsRead(item.code)
  if item then
    GameUIActivityNew.currentActivityContentId = item.id
    GameUIActivityNew.currentActivityContentTitle = item.title
    currentRankKey = item.rank_key
    self.m_currentTap = tonumber(item.tab)
    DebugOut("item.tab", item.tab)
    self.currentAward_desc = item.award_desc
    if self:GetFlashObject() then
      self:GetFlashObject():InvokeASCallback("_root", "HideComboGachaEnterTip")
    end
    if QuestTutorialComboGacha:IsActive() and GameUIActivityNew.combogachaItemid then
      self:GetFlashObject():InvokeASCallback("_root", "lua2fs_setComboGachaTutorial", GameUIActivityNew.combogachaItemid, true)
    end
    if item.tab and tonumber(item.tab) == 2 then
      GameUIActivityNew.currentLabType2Code = tonumber(string.sub(item.code, -1)) + 1
      GameUIActivityNew:GetPromotionDetail(item.id)
    elseif item.tab and tonumber(item.tab) == 3 then
      assert(0, "unused")
    elseif item.tab and tonumber(item.tab) == 4 then
      self.m_currentContent = item.content
      GameUIActivityNew.currentLabType2Code = tonumber(string.sub(item.code, -1)) + 1
      GameUIActivityNew:GetPromotionDetail(item.id)
    elseif item.tab and tonumber(item.tab) == 5 then
      GameUIActivityNew:ShowExchangeCodeInfo(item, itemId)
    elseif item.tab and tonumber(item.tab) == 7 then
      GameUIActivityNew:ShowEmpireBank()
    else
      local title = item.title
      local content = item.content
      local isShowMore = false
      GameUIActivityNew.currentWebSite = item.link
      GameUIActivityNew.activityContentIndex = itemId
      currentSelectedActivity = item.code
      currentCategory = item.category
      currentSubCategory = item.sub_category
      DebugOut("currentCategory", currentCategory, "currentSubCategory", currentSubCategory)
      local btnText
      local enter_btn_visible = false
      if 0 < item.left_seconds and item.left_stamp < os.time() then
        enter_btn_visible = false
        item.left_seconds = 0
        DebugOut("A")
      elseif 0 > item.left_seconds and item.left_stamp < os.time() then
        enter_btn_visible = true
        DebugOut("B")
      elseif 0 < item.left_seconds and item.left_stamp > os.time() then
        enter_btn_visible = true
        DebugOut("C")
      elseif 0 > item.left_seconds and item.left_stamp > os.time() then
        enter_btn_visible = false
        DebugOut("D")
      end
      if tonumber(currentCategory) == self.category.TYPE_BOX then
        if self.getHasTry(currentCategory, currentSubCategory) then
          if self.getHasOpen(currentCategory, currentSubCategory) == false then
            DebugOut("hide enter btn")
            enter_btn_visible = false
          end
          DebugOut("enter_btn_visible " .. tostring(enter_btn_visible))
          btnText = GameLoader:GetGameText("LC_MENU_ACTIVITY_GACHA_BUTTON")
          self:GetFlashObject():InvokeASCallback("_root", "lua2fs_showEnterContent", title, content, tonumber(itemId), false, true, isShowMore, enter_btn_visible, btnText, item.rank_type, item.award_type, false)
          self.setHasTry(currentCategory, currentSubCategory, false)
        else
          self.setHasOpen(currentCategory, currentSubCategory, false)
          self.setHasTry(currentCategory, currentSubCategory, true)
          GameUIGachaBox.subCategory = currentSubCategory
          GameUIGachaBox:preRequestGachaList(self.gotoDetailPageAfterTryOpen)
        end
      elseif tonumber(currentCategory) == self.category.TYPE_DUNGEON then
        btnText = GameLoader:GetGameText("LC_MENU_DUNGEON_TITLE")
        DebugOut(btnText)
        DebugOut("enter_btn_visible " .. tostring(enter_btn_visible))
        self:GetFlashObject():InvokeASCallback("_root", "lua2fs_showEnterContent", title, content, tonumber(itemId), false, true, isShowMore, enter_btn_visible, btnText, item.rank_type, item.award_type, false)
      elseif tonumber(currentCategory) == self.category.TYPE_WORM_HOLE then
        btnText = GameLoader:GetGameText("LC_MENU_DUNGEON_WORM_TITLE")
        DebugOut(btnText)
        DebugOut("enter_btn_visible " .. tostring(enter_btn_visible))
        self:GetFlashObject():InvokeASCallback("_root", "lua2fs_showEnterContent", title, content, tonumber(itemId), false, true, isShowMore, enter_btn_visible, btnText, item.rank_type, item.award_type, false)
      elseif tonumber(currentCategory) == self.category.TYPE_EXPEDITION then
        btnText = GameLoader:GetGameText("LC_MENU_DUNGEON_EXPEDITION_TITLE")
        DebugOut(btnText)
        DebugOut("enter_btn_visible " .. tostring(enter_btn_visible))
        self:GetFlashObject():InvokeASCallback("_root", "lua2fs_showEnterContent", title, content, tonumber(itemId), false, true, isShowMore, enter_btn_visible, btnText, item.rank_type, item.award_type, false)
      elseif tonumber(currentCategory) == self.category.TYPE_ACTIVITYEVENT then
        btnText = GameLoader:GetGameText("LC_MENU_ACTIVITY_EVENT_JUMP_BUTTON")
        DebugOut(btnText)
        DebugOut("enter_btn_visible " .. tostring(enter_btn_visible))
        self:GetFlashObject():InvokeASCallback("_root", "lua2fs_showEnterContent", title, content, tonumber(itemId), false, true, isShowMore, enter_btn_visible, btnText, item.rank_type, item.award_type, false)
      elseif tonumber(currentCategory) == self.category.TYPE_DNA then
        if QuestTutorialComboGacha:IsActive() and GameUIActivityNew.combogachaItemid then
          self:GetFlashObject():InvokeASCallback("_root", "lua2fs_setComboGachaTutorial", GameUIActivityNew.combogachaItemid, false)
        end
        if self.getHasTry(currentCategory, currentSubCategory) then
          if self.getHasOpen(currentCategory, currentSubCategory) == false then
            DebugOut("hide enter btn")
            enter_btn_visible = false
          end
          DebugOut("item id = ", tonumber(itemId))
          DebugOut("enter_btn_visible " .. tostring(enter_btn_visible))
          btnText = GameLoader:GetGameText("LC_MENU_COMBO_GACHA_BUTTON")
          self:GetFlashObject():InvokeASCallback("_root", "lua2fs_showEnterContent", title, content, tonumber(itemId), false, true, isShowMore, enter_btn_visible, btnText, item.rank_type, item.award_type, false)
          if tonumber(itemId) == GameUIActivityNew.combogachaItemid and QuestTutorialComboGacha:IsActive() and not GameUIActivityNew.isHide1100059Dialog then
            local function callback()
              AddFlurryEvent("Gacha_Dialog_3", {}, 1)
              self:GetFlashObject():InvokeASCallback("_root", "ShowComboGachaEnterTip")
            end
            AddFlurryEvent("Gacha_ClickBanner", {}, 1)
            GameUICommonDialog:PlayStory({1100059}, callback)
          else
            self:GetFlashObject():InvokeASCallback("_root", "HideComboGachaEnterTip")
          end
          self.setHasTry(currentCategory, currentSubCategory, false)
        else
          self.setHasOpen(currentCategory, currentSubCategory, false)
          GameUIcomboGacha.comboGachaId = currentSubCategory
          GameUIcomboGacha.resourceId = currentSubCategory
          GameUIcomboGacha:RequestComboGachaInfo(self.gotoDetailPageAfterTryOpen)
          self.setHasTry(currentCategory, currentSubCategory, true)
        end
      elseif tonumber(currentCategory) == self.category.TYPE_INTERSTELLAR_ADVENTURE then
        btnText = GameLoader:GetGameText("LC_MENU_INTERSTELLAR_HUNT_TITLE")
        DebugOut(btnText)
        DebugOut("enter_btn_visible " .. tostring(enter_btn_visible))
        self:GetFlashObject():InvokeASCallback("_root", "lua2fs_showEnterContent", title, content, tonumber(itemId), false, true, isShowMore, enter_btn_visible, btnText, item.rank_type, item.award_type, false)
      elseif tonumber(currentCategory) == GameUIActivityNew.category.TYPE_PAYWALL then
        btnText = GameLoader:GetGameText("LC_MENU_SLAC_GET_CREDITS")
        DebugOut(btnText)
        DebugOut("enter_btn_visible " .. tostring(enter_btn_visible))
        self:GetFlashObject():InvokeASCallback("_root", "lua2fs_showEnterContent", title, content, tonumber(itemId), false, true, isShowMore, enter_btn_visible, btnText, item.rank_type, item.award_type, false)
      elseif tonumber(currentCategory) == GameUIActivityNew.category.TYPE_KRYPTONLAB then
        btnText = GameLoader:GetGameText("LC_MENU_BUILDING_NAME_KRYPTON_CENTER")
        DebugOut(btnText)
        DebugOut("enter_btn_visible " .. tostring(enter_btn_visible))
        self:GetFlashObject():InvokeASCallback("_root", "lua2fs_showEnterContent", title, content, tonumber(itemId), false, true, isShowMore, enter_btn_visible, btnText, item.rank_type, item.award_type, false)
      elseif tonumber(currentCategory) == GameUIActivityNew.category.TYPE_WD then
        btnText = GameLoader:GetGameText("LC_MENU_WD_GALAXY_TITLE")
        DebugOut(btnText)
        DebugOut("enter_btn_visible " .. tostring(enter_btn_visible))
        self:GetFlashObject():InvokeASCallback("_root", "lua2fs_showEnterContent", title, content, tonumber(itemId), false, true, isShowMore, enter_btn_visible, btnText, item.rank_type, item.award_type, false)
      elseif tonumber(currentCategory) == GameUIActivityNew.category.TYPE_PVP then
        btnText = GameLoader:GetGameText("LC_MENU_ARENA_BUTTON")
        DebugOut(btnText)
        DebugOut("enter_btn_visible " .. tostring(enter_btn_visible))
        self:GetFlashObject():InvokeASCallback("_root", "lua2fs_showEnterContent", title, content, tonumber(itemId), false, true, isShowMore, enter_btn_visible, btnText, item.rank_type, item.award_type, false)
      elseif tonumber(currentCategory) == GameUIActivityNew.category.TYPE_STORE_BLACK then
        btnText = GameLoader:GetGameText("LC_MENU_MYSTERY_SHOP_BUTTON")
        DebugOut(btnText)
        DebugOut("enter_btn_visible " .. tostring(enter_btn_visible))
        self:GetFlashObject():InvokeASCallback("_root", "lua2fs_showEnterContent", title, content, tonumber(itemId), false, true, isShowMore, enter_btn_visible, btnText, item.rank_type, item.award_type, false)
      elseif tonumber(currentCategory) == GameUIActivityNew.category.TYPE_SCRATCH then
        btnText = GameLoader:GetGameText("LC_MENU_SPACE_STATION_NAME")
        DebugOut(btnText)
        DebugOut("enter_btn_visible " .. tostring(enter_btn_visible))
        self:GetFlashObject():InvokeASCallback("_root", "lua2fs_showEnterContent", title, content, tonumber(itemId), false, true, isShowMore, enter_btn_visible, btnText, item.rank_type, item.award_type, false)
      elseif tonumber(currentCategory) == GameUIActivityNew.category.TYPE_NEWSIGN then
        btnText = GameLoader:GetGameText("LC_MENU_LOGIN_DAY_TITLE")
        DebugOut(btnText)
        DebugOut("enter_btn_visible " .. tostring(enter_btn_visible))
        self:GetFlashObject():InvokeASCallback("_root", "lua2fs_showEnterContent", title, content, tonumber(itemId), false, true, isShowMore, enter_btn_visible, btnText, item.rank_type, item.award_type, false)
      elseif tonumber(currentCategory) == GameUIActivityNew.category.TYPE_STARTPOT then
        btnText = GameLoader:GetGameText("LC_MENU_BUILDING_NAME_STAR_PORTAL")
        DebugOut(btnText)
        DebugOut("enter_btn_visible " .. tostring(enter_btn_visible))
        self:GetFlashObject():InvokeASCallback("_root", "lua2fs_showEnterContent", title, content, tonumber(itemId), false, true, isShowMore, enter_btn_visible, btnText, item.rank_type, item.award_type, false)
      elseif tonumber(currentCategory) == GameUIActivityNew.category.TYPE_STORE then
        btnText = GameLoader:GetGameText("LC_MENU_SHOP_BUTTON")
        DebugOut(btnText)
        DebugOut("enter_btn_visible " .. tostring(enter_btn_visible))
        self:GetFlashObject():InvokeASCallback("_root", "lua2fs_showEnterContent", title, content, tonumber(itemId), false, true, isShowMore, enter_btn_visible, btnText, item.rank_type, item.award_type, false)
      elseif tonumber(currentCategory) == GameUIActivityNew.category.TYPE_DICE then
        btnText = GameLoader:GetGameText("LC_MENU_GALAXY_CASINO")
        DebugOut(btnText)
        DebugOut("enter_btn_visible " .. tostring(enter_btn_visible))
        self:GetFlashObject():InvokeASCallback("_root", "lua2fs_showEnterContent", title, content, tonumber(itemId), false, true, isShowMore, enter_btn_visible, btnText, item.rank_type, item.award_type, false)
      elseif tonumber(currentCategory) == GameUIActivityNew.category.TYPE_UPDATEVERSION then
        btnText = GameLoader:GetGameText("LC_MENU_GET_NEWEST_VERSION")
        self:GetFlashObject():InvokeASCallback("_root", "lua2fs_showEnterContent", title, content, tonumber(itemId), false, true, isShowMore, enter_btn_visible, btnText, item.rank_type, item.award_type, false)
      elseif tonumber(currentCategory) == GameUIActivityNew.category.TYPE_SPEEDUP then
        local btnstatus = activityListTimeText[GameUIActivityNew.activityContentIndex].status
        local show = btnstatus > 0
        btnText = ""
        DebugOut(btnText)
        enter_btn_visible = false
        DebugOut("enter_btn_visible " .. tostring(enter_btn_visible))
        self:GetFlashObject():InvokeASCallback("_root", "lua2fs_showEnterContent", title, content, tonumber(itemId), false, true, isShowMore, enter_btn_visible, btnText, item.rank_type, item.award_type, show)
        self:GetFlashObject():InvokeASCallback("_root", "lua2fs_showSpeedUpButton", btnstatus > 0)
        GameUIActivityNew:GetSpeedResource()
      elseif tonumber(currentCategory) == GameUIActivityNew.category.TYPE_MEDALRECUIT then
        btnText = GameLoader:GetGameText("LC_MENU_NAME_MEDAL_GLORY_ROAD")
        DebugOut(btnText)
        DebugOut("enter_btn_visible " .. tostring(enter_btn_visible))
        self:GetFlashObject():InvokeASCallback("_root", "lua2fs_showEnterContent", title, content, tonumber(itemId), false, true, isShowMore, enter_btn_visible, btnText, item.rank_type, item.award_type, false)
      elseif tonumber(currentCategory) == GameUIActivityNew.category.TYPE_MEDALSHOP then
        btnText = GameLoader:GetGameText("LC_MENU_NAME_MEDAL_STORE")
        DebugOut(btnText)
        DebugOut("enter_btn_visible " .. tostring(enter_btn_visible))
        self:GetFlashObject():InvokeASCallback("_root", "lua2fs_showEnterContent", title, content, tonumber(itemId), false, true, isShowMore, enter_btn_visible, btnText, item.rank_type, item.award_type, false)
      elseif tonumber(currentCategory) == GameUIActivityNew.category.TYPE_LARGEMAP then
        btnText = GameLoader:GetGameText("LC_MENU_NAME_MEDAL_STORE")
        DebugOut(btnText)
        DebugOut("enter_btn_visible " .. tostring(enter_btn_visible))
        self:GetFlashObject():InvokeASCallback("_root", "lua2fs_showEnterContent", title, content, tonumber(itemId), false, true, isShowMore, enter_btn_visible, btnText, item.rank_type, item.award_type, false)
      else
        DebugOut("normal activity")
        DebugOut("item.rank_type", item.rank_type)
        isShowMore = item.link and 3 < string.len(item.link)
        self:GetFlashObject():InvokeASCallback("_root", "showNewsContent", title, content, tonumber(itemId), false, true, isShowMore, item.rank_type, item.award_type)
      end
    end
  end
end
function GameUIActivityNew:GetSpeedResource()
  local function callBack(msgType, content)
    if msgType == NetAPIList.speed_resource_ack.Code then
      GameUIActivityNew.speedResource = content.items
      GameUIActivityNew:UpdateSpeedLayer()
      return true
    end
    return false
  end
  NetMessageMgr:SendMsg(NetAPIList.speed_resource_req.Code, nil, callBack, false, nil)
end
function GameUIActivityNew.updateSpeedResource(content)
  GameUIActivityNew.speedResource = content.items
  GameUIActivityNew:UpdateSpeedLayer()
end
function GameUIActivityNew:UpdateSpeedLayer()
  for k, v in pairs(GameUIActivityNew.speedResource) do
    local dataTable = {}
    if v.status == 0 then
      dataTable.buttonText = GameLoader:GetGameText("LC_MENU_AUTO_COMPLETE_BUTTON")
    elseif v.status == 1 then
      dataTable.buttonText = GameLoader:GetGameText("LC_MENU_NEW_DAILY_FINISH_CHAR")
    end
    dataTable.status = v.status
    dataTable.type = v.type
    DebugOut("dataTable = ")
    DebugTable(dataTable)
    if GameUIActivityNew:GetFlashObject() then
      GameUIActivityNew:GetFlashObject():InvokeASCallback("_root", "updateSpeedLayer", dataTable)
    end
  end
end
function GameUIActivityNew:GetSpeedItem(typeid)
  for k, v in pairs(self.speedResource) do
    if v.type == typeid then
      return v
    end
  end
end
function GameUIActivityNew:UpdateSpeedData(typeid)
  for k, v in pairs(self.speedResource) do
    if v.type == typeid then
      v.status = 1
    end
  end
end
function GameUIActivityNew:AutoFinishSpeed(typeid)
  self.currentSpeedType = typeid
  local param = {type = typeid}
  local function callback(msgType, content)
    if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.finish_speed_task_req.Code then
      if content.code ~= 0 then
        GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      else
        GameUIActivityNew:UpdateSpeedData(self.currentSpeedType)
        GameUIActivityNew:UpdateSpeedLayer()
      end
      return true
    elseif msgType == NetAPIList.dungeon_enter_ack.Code then
      DebugOut("preRequestInstanceDataCallBack dungeon_enter_ack")
      DebugTable(content)
      local tipString = ""
      if content.code == 1000008 then
        tipString = GameLoader:GetGameText("LC_ALERT_dungeon_ladder_level")
        tipString = string.format(tipString, content.lev)
        GameTip:Show(tipString)
        return true
      elseif content.code == 1000023 then
        tipString = GameLoader:GetGameText("LC_ALERT_dungeon_wormhole_level")
        tipString = string.format(tipString, content.lev)
        GameTip:Show(tipString)
        return true
      end
      return true
    end
    return false
  end
  NetMessageMgr:SendMsg(NetAPIList.finish_speed_task_req.Code, param, callback, true, nil)
end
function GameUIActivityNew.gotoDetailPageAfterTryOpen()
  DebugOut("GameUIActivityNew.gotoDetailPageAfterTryOpen", GameUIActivityNew.activityContentIndex)
  GameUIActivityNew:showNewsContent(GameUIActivityNew.activityContentIndex)
end
function GameUIActivityNew:ShowNews(label)
  assert(label == "activity")
  DebugActivity("GameUIActivityNew:ShowNews currentLab: ", currentLab, " label: ", label)
  GameUIGachaBox:hideGacha()
  local inComing = GameLoader:GetGameText("LC_MENU_INCOMING_CAHR")
  local inprogress = GameLoader:GetGameText("LC_MENU_IN_PROGRESS_CHAR")
  local finished = GameLoader:GetGameText("LC_MENU_FINISHED_CHAR")
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  self:GetFlashObject():InvokeASCallback("_root", "lua2fs_setActivityItemText", inComing, inprogress, finished)
  GameUIActivityNew:InitActivity()
  GameUIActivityNew:AddToGameState()
  if activityList then
  else
    self:RequestActivity(true)
  end
end
function GameUIActivityNew:AddToGameState()
  DebugActivity("GameUIActivityNew AddToGameState")
  if not GameStateManager:GetCurrentGameState():IsObjectInState(GameUIActivityNew) then
    GameStateManager:GetCurrentGameState():AddObject(GameUIActivityNew)
    if not GameUIActivityNew:GetFlashObject() then
      GameUIActivityNew:LoadFlashObject()
    end
    self:GetFlashObject():InvokeASCallback("_root", "moveIn")
  end
end
function GameUIActivityNew:setNoticeIsRead(itemId)
  local readID = GameUtils:GetShortActivityID(itemId)
  local param = {type = 2, id = readID}
  NetMessageMgr:SendMsg(NetAPIList.notice_set_read_req.Code, param, GameUIActivityNew.SetNoticeIsReadCallback, false)
end
function GameUIActivityNew.SetNoticeIsReadCallback(msgType, content)
  DebugOut("\230\137\147\229\141\176SetNoticeIsReadCallback", msgType)
  DebugTable(content)
  if msgType == NetAPIList.gateway_info_ntf.Code then
    GameGlobalData.RefreshNewsAndAct(content)
    if GameUIActivityNew:GetFlashObject() then
      GameUIActivityNew:GetFlashObject():unlockInput()
    end
    return true
  elseif msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.notice_set_read_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  return false
end
function GameUIActivityNew:GetPromotionDetail(itemId)
  local content = {
    id = itemId,
    index = GameUIActivityNew.currentLabType2Code
  }
  GameUIActivityNew.currentLabType2Name = itemId
  NetMessageMgr:SendMsg(NetAPIList.promotion_detail_req.Code, content, GameUIActivityNew.PromotionsDetailCallback, true)
end
function GameUIActivityNew:ShowExchangeCodeInfo(item, itemId)
  DebugActivity("GameUIActivityNew:ShowExchangeCodeInfo()")
  GameUIActivityNew:GetFlashObject():InvokeASCallback("_root", "showExchangeContent", item.title, tonumber(itemId))
end
function GameUIActivityNew.RecentChestNtfHandler(content)
  GameUIGachaBox:RecentChestNtfHandler(content)
end
function GameUIActivityNew:GetActivityIsRead(code)
  local isReaded = false
  local viewedNoticeId = GameGlobalData.GlobalData.notice_and_act
  if viewedNoticeId then
    for k, v in pairs(viewedNoticeId.activities) do
      if tonumber(v) == GameUtils:GetShortActivityID(code) then
        isReaded = true
      end
    end
  else
    isReaded = true
  end
  return isReaded
end
function GameUIActivityNew:IsCanReward(code)
  if not GameUIBarLeft.specialActicityList then
    return
  end
  local isCanReward = false
  if GameUIBarLeft.specialActicityList.news_ids and #GameUIBarLeft.specialActicityList.news_ids > 0 then
    for k, v in pairs(GameUIBarLeft.specialActicityList.news_ids) do
      if tonumber(v) == tonumber(code) then
        isCanReward = true
      end
    end
  end
  return isCanReward
end
function GameUIActivityNew:IsDownloadPNGExsit(filename)
  return ...
end
if AutoUpdate.isAndroidDevice then
  function GameUIActivityNew.OnAndroidBack()
    if GameUIWDStuff.currentMenu == GameUIWDStuff.menuType.menu_type_help then
      local flash_obj = GameUIWDStuff:GetFlashObject()
      if flash_obj then
        flash_obj:InvokeASCallback("_root", "hideHelp")
      end
    elseif GameUIActivityNew.showContent then
      GameUIActivityNew:OnFSCommand("CloseContent")
    else
      GameUIActivityNew:OnFSCommand("close_menu")
    end
  end
end
function GameUIActivityNew.QueryFestival()
  DebugOut("GameUIActivityNew.QueryFestival")
  NetMessageMgr:SendMsg(NetAPIList.festival_req.Code, nil, GameUIActivityNew.QueryFestivalOffOnCallback, false, nil)
  currentFestivalType = GameUIActivityNew.FestivalType.Watting
end
GameUIActivityNew.festivalDetailDataset = {}
function GameUIActivityNew.QueryFestivalOffOnCallback(msgType, content)
  DebugOut("GameUIActivityNew.QueryFestivalOffOnCallback")
  if msgType == NetAPIList.festival_ack.Code then
    do
      local avaliableFestivalInfo
      m_festivalInfo = content.festivals
      DebugOutPutTable(m_festivalInfo, "m_festivalInfo")
      for i, v in ipairs(m_festivalInfo) do
        if v.current_time >= v.start_time and v.current_time <= v.final_time and v.activity_type == 1 then
          DebugOut("got valid activity info from server,server time")
          DebugTable(v)
          avaliableFestivalInfo = v
          break
        else
          DebugOut("v.activity_type == 1", v.activity_type == 1)
          if v.activity_type == 1 then
            DebugOut("v.current_time >= v.start_time", v.current_time >= v.start_time)
            DebugOut("v.current_time <= v.final_time", v.current_time <= v.final_time)
          end
        end
      end
      if avaliableFestivalInfo ~= nil then
        currentFestivalType = GameUIActivityNew.FestivalType.Request_Available_Activity
        DebugOut("QJ  eventLevelDef = avaliableFestivalInfo.level_req", avaliableFestivalInfo.level_req)
        eventLevelDef = avaliableFestivalInfo.level_req
        local festivalUrl = AutoUpdate.gameGateway .. GameUIActivityNew.getGatewayRequestString(superCategory, superSubCategory)
        if GameSettingData.Save_Lang == "nl" or GameSettingData.Save_Lang == "tr" or GameSettingData.Save_Lang == "in" or GameSettingData.Save_Lang == "id" then
          festivalUrl = festivalUrl .. "?lang=" .. "en"
        else
          festivalUrl = festivalUrl .. "?lang=" .. GameSettingData.Save_Lang
        end
        DebugOut("festivalUrl", festivalUrl)
        ext.http.request({
          festivalUrl,
          mode = "notencrypt",
          callback = function(statusCode, webContent, errstr)
            if errstr ~= nil or statusCode ~= 200 then
              return
            end
            currentFestivalType = GameUIActivityNew.FestivalType.Finished_Request
            DebugOut("super activity")
            DebugTable(webContent)
            local festivalDetailDatasetItem = {}
            festivalDetailDatasetItem.detailType = GameUIActivityNew.DetailType.FESTIVAL
            festivalDetailDatasetItem.timeInfo = LuaUtils:table_rcopy(avaliableFestivalInfo)
            festivalDetailDatasetItem.receiveInfoTime = os.time()
            festivalDetailDatasetItem.details = LuaUtils:table_rcopy(webContent)
            table.insert(GameUIActivityNew.festivalDetailDataset, festivalDetailDatasetItem)
          end
        })
      else
        DebugOut("[error]no available festival [festival_req]")
        currentFestivalType = GameUIActivityNew.FestivalType.Error
        GameUIActivityNew:GetFlashObject():unlockInput()
      end
      return true
    end
  end
  return false
end
function GameUIActivityNew:getRewardInfo()
  local RewardOriInfo = activityList[currentFocusItem].award_json
  local parms = {
    items = {}
  }
  for _, v in pairs(RewardOriInfo) do
    table.insert(parms.items, v.item)
  end
  NetMessageMgr:SendMsg(NetAPIList.game_items_trans_req.Code, parms, GameUIActivityNew.getRewardInfoCallBack, true)
end
GameUIActivityNew.currentRewardList = nil
function GameUIActivityNew.getRewardInfoCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.game_items_trans_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.game_items_trans_ack.Code then
    DebugOut("GameUIActivityNew.getRewardInfoCallBack ", msgType)
    DebugTable(content)
    local flash_obj = GameUIActivityNew:GetFlashObject()
    if flash_obj then
      local rewards_list = {}
      rewards_list.title = GameLoader:GetGameText("LC_MENU_REWARDS_CHAR")
      rewards_list.reward_sections = {}
      for k, v in pairs(content.all_items) do
        local reward_sec = {}
        reward_sec.desc = activityList[currentFocusItem].award_json[k].desc
        reward_sec.items = {}
        for _, value in pairs(v.items) do
          local reward_item = {}
          reward_item.item_name, reward_item.count = GameHelper:getAwardNameTextAndCount(value.item_type, value.number, value.no)
          reward_item.icon_frame = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(value, nil, nil)
          table.insert(reward_sec.items, reward_item)
        end
        table.insert(rewards_list.reward_sections, reward_sec)
      end
      GameUIActivityNew.currentRewardList = content.all_items
      flash_obj:InvokeASCallback("_root", "ShowItemReward", rewards_list)
    end
    return true
  end
  return false
end
function GameUIActivityNew:getRankInfo(rank_key)
  DebugOut("GameUIActivityNew:getRankInfo", rank_key)
  if not rank_key then
    DebugOut("GameUIActivityNew:getRankInfo error rank_key")
    return
  end
  local content = {id = rank_key}
  NetMessageMgr:SendMsg(NetAPIList.activity_rank_req.Code, content, GameUIActivityNew.getRankInfoCallback, true)
end
function GameUIActivityNew.getRankInfoCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.activity_rank_req.Code then
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.activity_rank_ack.Code then
    DebugOut("GameUIActivityNew.getRankInfoCallback")
    GameUIActivityNew:resetRankInfo()
    local firstGotoTab = 1
    if #content.players > 0 then
      firstGotoTab = 1
      playerLeaderboard = content.players
      local SortCallback = function(a, b)
        return a.rank < b.rank
      end
      table.sort(playerLeaderboard, SortCallback)
    end
    DebugTable(playerLeaderboard)
    if #content.players == 0 and 0 < #content.alliances then
      if firstGotoTab == 0 then
        firstGotoTab = 2
      end
      starAllianceLeaderboard = content.alliances
      local SortCallback = function(a, b)
        return a.rank < b.rank
      end
      table.sort(starAllianceLeaderboard, SortCallback)
    end
    myLeaderboard = content.self_info
    GameUIActivityNew:CheckToLeaderBoard(firstGotoTab, true)
    GameUIActivityNew:RefreshMyRank()
    return true
  end
  return false
end
function GameUIActivityNew:resetRankInfo()
  currentRankItemCode = 0
  currentShowingTab = 0
  playerLeaderboard = nil
  starAllianceLeaderboard = nil
end
function GameUIActivityNew:CheckToLeaderBoard(tab, isNeedAnimation)
  DebugOut("GameUIActivityNew:CheckToLeaderBoard")
  if tab == 0 then
    DebugOut("CheckToLeaderBoard: \230\178\161\230\156\137\230\149\176\230\141\174\228\191\161\230\129\175")
    return
  end
  if currentShowingTab == tab then
    DebugOut("CheckToLeaderBoard current is", tab)
    return
  else
    currentShowingTab = tab
  end
  local curLeaderboard
  if currentShowingTab == 1 then
    DebugOut("rank player")
    curLeaderboard = playerLeaderboard
  else
    DebugOut("rank alliance")
    curLeaderboard = starAllianceLeaderboard
  end
  if curLeaderboard ~= nil then
    local itemCount = #curLeaderboard
    DebugOut("rank item count ", itemCount)
    self:GetFlashObject():InvokeASCallback("_root", "lua2fs_initRankListBoxWithCount", itemCount, currentShowingTab)
  else
    self:GetFlashObject():InvokeASCallback("_root", "lua2fs_initRankListBoxWithCount", 0, currentShowingTab)
  end
end
function GameUIActivityNew:RefreshMyRank()
  if myLeaderboard ~= nil and self:GetFlashObject() ~= nil then
    local rank = myLeaderboard.rank
    if rank > 9999 then
      rank = 9999
    end
    self:GetFlashObject():InvokeASCallback("_root", "lua2fs_displayMyRank", GameUtils:GetUserDisplayName(myLeaderboard.name), "" .. myLeaderboard.box_cnt, rank)
  else
    local player_info = GameGlobalData:GetUserInfo()
    local myName = GameUtils:GetUserDisplayName(player_info.name)
    self:GetFlashObject():InvokeASCallback("_root", "lua2fs_displayMyRank", myName, "-", 0)
  end
end
function GameUIActivityNew.ActivityRankNtfHandler(content)
  DebugOut("ActivityRankNtfHandler")
  DebugTable(content)
end
function GameUIActivityNew:UpdateLeaderboardItem(id)
  local itemKey = tonumber(id)
  DebugOut("GameUIActivityNew UpdateLeaderboardItem", itemKey)
  local curLeaderboard
  if currentShowingTab == 1 then
    curLeaderboard = playerLeaderboard
  else
    curLeaderboard = starAllianceLeaderboard
  end
  if curLeaderboard == nil then
    DebugOut("GameUIActivityNew error curLeaderboard")
    return
  end
  if itemKey > #curLeaderboard then
    DebugOut("GameUIActivityNew error itemKey", itemKey)
    return
  end
  local rank = 0
  local name, turkey = "", ""
  local v1, v2
  local itemInfo = curLeaderboard[itemKey]
  local allianceName = ""
  if currentShowingTab == 1 then
    name = GameUtils:GetUserDisplayName(itemInfo.name)
    turkey = "" .. itemInfo.box_cnt
    rank = tonumber(itemInfo.rank)
    allianceName = itemInfo.alliance_name
  else
    name = itemInfo.alliance.name
    turkey = "" .. itemInfo.box_cnt
    rank = tonumber(itemInfo.rank)
  end
  if allianceName == "" then
    allianceName = "-"
  end
  DebugOut("UpdateLeaderboardItem", rank, name, turkey, allianceName, currentShowingTab)
  self:GetFlashObject():InvokeASCallback("_root", "lua2fs_setRankItem", itemKey, rank, name, turkey, allianceName, currentShowingTab)
end
function GameUIActivityNew:getActivityNameById(activityId)
  if activityList == nil then
    DebugOut("GameUIActivityNew:getActivityList error:activityList == nil")
    return ""
  else
    for k, item in pairs(activityList) do
      if activityId == item.id then
        return item.title
      end
    end
  end
end
function GameUIActivityNew:getInstanceData()
  if not GameObjectClimbTower.activityList then
    return nil
  end
  if #GameObjectClimbTower.activityList < 1 then
    return nil
  end
  local instanceTable = {}
  DebugOut("-----sfsd=")
  DebugTable(GameObjectClimbTower.activityList)
  for k, item in pairs(GameObjectClimbTower.activityList) do
    if tonumber(item.category) == GameUIActivityNew.category.TYPE_DUNGEON or tonumber(item.category) == GameUIActivityNew.category.TYPE_WORM_HOLE or tonumber(item.category) == GameUIActivityNew.category.TYPE_EXPEDITION then
      local newItem = LuaUtils:table_rcopy(item)
      newItem.timeDescription = GameObjectClimbTower.activityListTimeText[k].title
      newItem.status = GameObjectClimbTower.activityListTimeText[k].status
      table.insert(instanceTable, newItem)
      if tonumber(item.category) == GameUIActivityNew.category.TYPE_DUNGEON and item.sub_category % 10 ~= 2 then
        DebugOut("[error]TYPE_DUNGEON sub_category", item.sub_category)
      end
      if tonumber(item.category) == GameUIActivityNew.category.TYPE_WORM_HOLE and item.sub_category % 10 ~= 0 then
        DebugOut("[error]TYPE_WORM_HOLE sub_category", item.sub_category)
      end
      if tonumber(item.category) == GameUIActivityNew.category.TYPE_EXPEDITION and item.sub_category % 10 ~= 3 then
        DebugOut("[error]TYPE_EXPEDITION sub_category", item.sub_category)
      end
    end
  end
  DebugOut("GameObjectClimbTower.activityList = ")
  DebugTable(GameObjectClimbTower.activityList)
  DebugOut("instanceTable = ")
  DebugTable(instanceTable)
  return instanceTable
end
function GameUIActivityNew:GetEmpireBankInfo()
  local content = {
    pay_code = tonumber(IPlatformExt.getConfigValue("ProductListReqNum")) or GameUtils:getProductListReqNum()
  }
  NetMessageMgr:SendMsg(NetAPIList.month_card_list_req.Code, content, GameUIActivityNew.GetEmpireBankInfoCallback, true)
end
function GameUIActivityNew.GetEmpireBankInfoCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.month_card_list_req.Code then
    local errtext = AlertDataList:GetTextFromErrorCode(content.code)
    GameTip:Show(errtext, 3000)
    return true
  elseif msgType == NetAPIList.month_card_list_ack.Code then
    GameUIActivityNew.mGotoMonthCardDirectly = true
    GameUIActivityNew:RefreshEmpireBank(content)
    GameUIActivityNew:GetFlashObject():unlockInput()
    return true
  end
  return false
end
function GameUIActivityNew:ShowEmpireBank()
  DebugOut("GameUIActivityNew:ShowEmpireBank")
  self:GetFlashObject():InvokeASCallback("_root", "showNewEmpireBank", GameLoader:GetGameText("LC_MENU_BANK_MONTH_CARD_TITLE"), GameLoader:GetGameText("LC_MENU_BANK_HELP_TEXT"))
  GameUIActivityNew:GetEmpireBankInfo()
end
function GameUIActivityNew:RefreshEmpireBank(content)
  local bank_info_List = {}
  GameUIActivityNew.bank_UpdateManger:Clear()
  do
    local bank_item = {}
    bank_item.id = 0
    bank_item.product_id = "free"
    bank_item.activity_totle = 0
    bank_item.activity_num = 0
    bank_item.activity_reward = nil
    bank_item.activity_leftTime = nil
    bank_item.activity_join = false
    bank_item.title = "free"
    bank_item.price_str = nil
    bank_item.btn_title_use = GameLoader:GetGameText("LC_MENU_USE_CHAR")
    bank_item.btn_title_buy = GameLoader:GetGameText("LC_MENU_BUY_CHAR")
    bank_item.btn_title_get = GameLoader:GetGameText("LC_MENU_SA_RECEIVE_BUTTON")
    bank_item.btn_title_gift = GameLoader:GetGameText("LC_MENU_BANK_GIFT_BUTTON")
    bank_item.title_desc = GameLoader:GetGameText("LC_MENU_BANK_TODAY_TITLE")
    bank_item.remian_text = nil
    if content.free_get then
      bank_item.card_type = 2
    else
      bank_item.card_type = 3
    end
    bank_item.credit_text = content.free_num
    table.insert(bank_info_List, bank_item)
  end
  for _, v in pairs(content.info) do
    local bank_item = {}
    bank_item.id = v.id
    bank_item.product_id = v.product_id
    bank_item.activity_totle = v.activity_totle
    bank_item.activity_num = v.activity_num
    bank_item.activity_reward = nil
    bank_item.activity_leftTime = GameUtils:formatTimeString(v.activity_time)
    bank_item.activity_join = v.activity_join
    bank_item.title = v.name
    bank_item.price_str = GameVip.GetPriceByProductID(v.product_id)
    if not bank_item.price_str then
      bank_item.price_str = ""
      GameUIActivityNew.bank_UpdateManger:setUpdatePriceStr(#bank_info_List + 1, v.product_id)
    end
    bank_item.btn_title_use = GameLoader:GetGameText("LC_MENU_USE_CHAR")
    bank_item.btn_title_buy = GameLoader:GetGameText("LC_MENU_BUY_CHAR")
    bank_item.btn_title_get = GameLoader:GetGameText("LC_MENU_SA_RECEIVE_BUTTON")
    bank_item.btn_title_gift = GameLoader:GetGameText("LC_MENU_BANK_GIFT_BUTTON")
    if v.has_join then
      if v.can_get then
        bank_item.card_type = 2
      else
        bank_item.card_type = 3
      end
      bank_item.credit_text = tostring(v.day_num)
      remain_text_font = "<font color=\"#00A3D8\">%s</font> <font color=\"#99FFFF\">%d/%d</font>"
      bank_item.remian_text = string.format(remain_text_font, GameLoader:GetGameText("LC_MENU_BANK_REMAIN_TITLE"), v.less_days, v.totle_days)
      bank_item.title_desc = GameLoader:GetGameText("LC_MENU_BANK_TODAY_TITLE")
    else
      if v.can_use then
        bank_item.card_type = 1
      elseif v.can_buy then
        bank_item.card_type = 0
      else
        bank_item.card_type = 4
      end
      bank_item.credit_text = tostring(v.totle_num)
      bank_item.remain_text = nil
      bank_item.title_desc = string.format(GameLoader:GetGameText("LC_MENU_BANK_TOTAL_TITLE"), v.totle_days)
    end
    if bank_item.activity_totle ~= 0 then
      bank_item.activity_end_time_title = GameLoader:GetGameText("LC_MENU_EVENT_GIVENDAY_END_CHAR")
      if 0 < v.activity_time then
        GameUIActivityNew.bank_UpdateManger:setUpdateLeftTime(#bank_info_List + 1, v.activity_time)
      end
      bank_item.activity_reward = {}
      for _, game_item in pairs(v.activity_reward) do
        local _item = {}
        _item.type = game_item.item_type
        _item.frame = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(game_item, nil, nil)
        DebugOut(_item.frame)
        _item.cnt = GameUtils.numberConversion(GameHelper:GetAwardCount(game_item.item_type, game_item.number, game_item.no))
        table.insert(bank_item.activity_reward, _item)
      end
      if bank_item.activity_join then
        if bank_item.activity_num >= bank_item.activity_totle then
          bank_item.activity_info_forPlayer = GameLoader:GetGameText("LC_MENU_BANK_INFO_3")
          bank_item.activity_reward_mask = false
        else
          bank_item.activity_reward_mask = true
          bank_item.activity_info_forPlayer = GameLoader:GetGameText("LC_MENU_BANK_INFO_2")
        end
      elseif 0 < v.activity_time then
        bank_item.activity_reward_mask = true
        bank_item.activity_info_forPlayer = GameLoader:GetGameText("LC_MENU_BANK_INFO_1")
      else
        bank_item.activity_reward_mask = true
        bank_item.activity_info_forPlayer = GameLoader:GetGameText("LC_MENU_BANK_INFO_4")
      end
    end
    table.insert(bank_info_List, bank_item)
  end
  self:GetFlashObject():InvokeASCallback("_root", "refreshNewEmpireBank", bank_info_List)
end
function GameUIActivityNew:BankGet(bank_id)
  local content = {id = "month_card", condition = bank_id}
  local function callback_func(msgType, content)
    if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.promotion_award_req.Code then
      return true
    elseif msgType == NetAPIList.promotion_award_ack.Code then
      GameTip:Show(GameLoader:GetGameText("LC_MENU_TITLE_SUCCESS_INFO"), 2000)
      GameUIActivityNew:GetEmpireBankInfo()
      return true
    end
    return false
  end
  NetMessageMgr:SendMsg(NetAPIList.promotion_award_req.Code, content, callback_func, true, nil)
end
function GameUIActivityNew:BankUse(bank_id)
  local function callback_func(msgType, content)
    if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.month_card_use_req.Code then
      if content.code == 0 then
        GameTip:Show(GameLoader:GetGameText("LC_MENU_TITLE_SUCCESS_INFO"), 2000)
      else
        local errtext = AlertDataList:GetTextFromErrorCode(content.code)
        GameTip:Show(errtext, 3000)
      end
      GameUIActivityNew:GetEmpireBankInfo()
      return true
    end
    return false
  end
  local bank_use_params = {id = bank_id}
  NetMessageMgr:SendMsg(NetAPIList.month_card_use_req.Code, bank_use_params, callback_func, true, nil)
end
GameUIActivityNew.BankGiftFriendList = nil
GameUIActivityNew.BankGiftAllianceMemberList = nil
GameUIActivityNew.Gift_bid = 0
GameUIActivityNew.Gift_pid = nil
function GameUIActivityNew:ShowBankGift(bid, pid)
  GameUIActivityNew.BankGiftFriendList = nil
  GameUIActivityNew.BankGiftAllianceMemberList = nil
  GameUIActivityNew.Gift_bid = bid
  GameUIActivityNew.Gift_pid = pid
  price_str = GameVip.GetPriceByProductID(GameUIActivityNew.Gift_pid)
  self:GetFlashObject():InvokeASCallback("_root", "ShowBankGiftSurface", price_str)
  GameUIActivityNew.RefreshBankGiftSurface()
end
function GameUIActivityNew.RefreshBankGiftSurface()
  local function func_friendListCallback(msgType, content)
    if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.friends_req.Code then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      return true
    elseif msgType == NetAPIList.friends_ack.Code then
      GameUIActivityNew.BankGiftFriendList = content.friends
      GameUIActivityNew:TryRefreshGiftSurface()
      return true
    end
    return false
  end
  local function func_AllianceMemberListCallback(msgType, content)
    if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.alliance_members_req.Code then
      if content.code ~= 0 then
        GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      end
      return true
    elseif msgType == NetAPIList.alliance_members_ack.Code then
      GameUIActivityNew.BankGiftAllianceMemberList = {}
      local user_info = GameGlobalData:GetUserInfo()
      for _, v in pairs(content.members) do
        if v.id ~= user_info.player_id then
          table.insert(GameUIActivityNew.BankGiftAllianceMemberList, v)
        end
      end
      GameUIActivityNew:TryRefreshGiftSurface()
      return true
    end
    return false
  end
  local data_alliance = GameGlobalData:GetData("alliance")
  if data_alliance.id == "" then
    GameUIActivityNew.BankGiftAllianceMemberList = {}
  else
    local content = {
      alliance_id = data_alliance.id,
      index = 0,
      size = 50
    }
    NetMessageMgr:SendMsg(NetAPIList.alliance_members_req.Code, content, func_AllianceMemberListCallback, true, nil)
  end
  NetMessageMgr:SendMsg(NetAPIList.friends_req.Code, null, func_friendListCallback, true, nil)
end
function GameUIActivityNew:BankRealGift(PlayerID, PlayerName)
  if PlayerID == nil then
    GameTip:Show(GameLoader:GetGameText("LC_MENU_BANK_GIFT_INFO_2"), 2000)
  else
    local function sure_callback()
      local function purchase_callback()
        GameUIActivityNew:GetEmpireBankInfo()
      end
      GameVip.PurchaseForACard(GameUIActivityNew.Gift_bid, GameUIActivityNew.Gift_pid, tonumber(PlayerID), purchase_callback)
    end
    local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
    GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
    GameUIMessageDialog:SetYesButton(sure_callback, nil)
    local infoTitle = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
    local info = GameLoader:GetGameText("LC_MENU_BANK_GIFT_INFO_1")
    info = string.gsub(info, "<money_num>", GameVip.GetPriceByProductID(GameUIActivityNew.Gift_pid))
    info = string.gsub(info, "<playerName_char>", PlayerName)
    GameUIMessageDialog:Display(infoTitle, info)
  end
end
function GameUIActivityNew:TryRefreshGiftSurface()
  if not GameUIActivityNew.BankGiftFriendList or not GameUIActivityNew.BankGiftAllianceMemberList then
    return
  end
  self:GetFlashObject():InvokeASCallback("_root", "Refresh_bank_gift_surface", GameUIActivityNew.BankGiftFriendList, GameUIActivityNew.BankGiftAllianceMemberList)
end
function GameUIActivityNew:Bank_Buy(bid, pid)
  local function purchase_callback()
    GameUIActivityNew:GetEmpireBankInfo()
  end
  GameVip.PurchaseForACard(bid, pid, 0, purchase_callback)
end
GameUIActivityNew.bank_UpdateManger = {
  isEnabled = false,
  bankUpdateItem = {},
  onUpdate = function(self)
    for k, v in pairs(self.bankUpdateItem) do
      local left_time
      if v.left_time ~= nil then
        if v.left_time <= 0 then
          GameUIActivityNew:GetEmpireBankInfo()
          return
        end
        v.left_time = v.left_time - 1
        left_time = GameUtils:formatTimeString(v.left_time)
      end
      local price_str
      if v.updateProductID then
        price_str = GameVip.GetPriceByProductID(v.updateProductID)
        if price_str then
          v.updateProductID = nil
        end
      end
      GameUIActivityNew:GetFlashObject():InvokeASCallback("_root", "refreshBankItemInfo", k, left_time, price_str)
    end
  end,
  Clear = function(self)
    self.bankUpdateItem = {}
    self.isEnabled = false
  end,
  setUpdateLeftTime = function(self, item_index, left_time)
    if not self.bankUpdateItem[item_index] then
      self.bankUpdateItem[item_index] = {}
    end
    self.isEnabled = true
    self.bankUpdateItem[item_index].left_time = left_time
  end,
  setUpdatePriceStr = function(self, item_index, product_id)
    if not self.bankUpdateItem[item_index] then
      self.bankUpdateItem[item_index] = {}
    end
    self.isEnabled = true
    self.bankUpdateItem[item_index].updateProductID = product_id
  end
}
function GameUIActivityNew.getGatewayRequestString(category, subCategory)
  return "activities/info?category=" .. category .. "&sub_category=" .. subCategory
end
function GameUIActivityNew.setHasTry(category, subCategory, isTry)
  DebugOut("setHasTry", category, subCategory, isTry)
  GameUIActivityNew.hasTryOpenTable[category .. subCategory] = isTry
end
function GameUIActivityNew.getHasTry(category, subCategory)
  return GameUIActivityNew.hasTryOpenTable[category .. subCategory]
end
function GameUIActivityNew.setHasOpen(category, subCategory, isOpen)
  DebugOut("setHasOpen", category, subCategory, isOpen)
  GameUIActivityNew.isModuleOpenTable[category .. subCategory] = isOpen
end
function GameUIActivityNew.getHasOpen(category, subCategory)
  return GameUIActivityNew.isModuleOpenTable[category .. subCategory]
end
function GameUIActivityNew:modifySubCategory(category, subCategory)
  DebugOut("modifySubCategory", category, subCategory)
  for i, item in pairs(activityList) do
    if item.category == category then
      DebugOut("modified")
      item.sub_category = subCategory
    end
  end
  return nil
end
function GameUIActivityNew._OnTimerTick()
  if GameStateManager:GetCurrentGameState():IsObjectInState(GameUIActivityNew) then
    if GameUIActivityNew.bank_UpdateManger.isEnabled then
      GameUIActivityNew.bank_UpdateManger:onUpdate()
    end
    return 1000
  end
  GameUIActivityNew.ResetTimerManger()
  return nil
end
function GameUIActivityNew.ResetTimerManger()
  GameUIActivityNew.bank_UpdateManger:Clear()
end
function GameUIActivityNew.GetComboGachaCallback()
  local function callback()
    AddFlurryEvent("Gacha_Dialog_5", {}, 1)
    currentFocusItem = 1
    GameUIcomboGacha:clearData()
    GameUIBarLeft:setHaveNewActivity(GameUIBarLeft.isNewActivity)
    GameStateManager:GetCurrentGameState():EraseObject(GameUIActivityNew)
    local GameStateMainPlanet = GameStateManager.GameStateMainPlanet
    GameUIActivityNew:RequestActivity(true)
  end
  GameUICommonDialog:PlayStory({1100061}, callback)
end
