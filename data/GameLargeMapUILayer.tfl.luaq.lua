local GameUILargeMap = LuaObjectManager:GetLuaObject("GameUILargeMap")
local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local GameUIChat = LuaObjectManager:GetLuaObject("GameUIChat")
function GameUILargeMap:updateExcuAndStatus()
  if self:GetFlashObject() then
    local data = {}
    local player_main_fleet = GameGlobalData:GetFleetInfo(1)
    local player_avatar = GameUtils:GetPlayerAvatar(nil, player_main_fleet.level)
    local leaderlist = GameGlobalData:GetData("leaderlist")
    if leaderlist and GameGlobalData.GlobalData and GameGlobalData.GlobalData.curMatrixIndex and leaderlist.leader_ids and leaderlist.leader_ids[GameGlobalData.GlobalData.curMatrixIndex] then
      player_avatar = GameDataAccessHelper:GetFleetAvatar(leaderlist.leader_ids[GameGlobalData.GlobalData.curMatrixIndex], player_main_fleet.level)
      DebugOut("fix")
    elseif GameGlobalData:GetUserInfo().icon ~= 0 and GameGlobalData:GetUserInfo().icon ~= 1 then
      player_avatar = GameDataAccessHelper:GetFleetAvatar(GameGlobalData:GetUserInfo().icon, player_main_fleet.level)
    end
    data.avatar = player_avatar
    data.executxt = self.MyPlayerData.cur_energy .. "/" .. self.MyPlayerData.max_energy
    data.statustxt = self.MyPlayerData.damaged_percent
    data.FleetsData = {}
    for k, v in pairs(self.MyPlayerData.fleets) do
      local item = {}
      item.identity = v.identity
      item.avatarFrame = FleetDataAccessHelper:GetFleetAvatarFrameByAvatar(v.avatar, 0, 0)
      item.avatarLevel = ""
      item.avatarColor = FleetDataAccessHelper:GetFleetColorFrameByColor(v.color)
      item.avatarShipFrame = GameDataAccessHelper:GetCommanderShipByDownloadState(v.ship)
      item.damagePercentStr = tostring(v.percent / 10) .. "%"
      item.damagePercent = v.percent / 10
      data.FleetsData[#data.FleetsData + 1] = item
    end
    self:GetFlashObject():InvokeASCallback("_root", "updateHeadInfo", data)
  end
end
function GameUILargeMap:onBuyEnergy()
  local function callback()
    local param = {}
    param.type = 1
    NetMessageMgr:SendMsg(NetAPIList.large_map_buy_energy_req.Code, param, GameUILargeMap.LargeMapBuyEnergyCallBack, true, nil)
  end
  do break end
  do
    local text_title = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
    GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
    GameUIMessageDialog:SetYesButton(callback)
    local msgStr = GameLoader:GetGameText("LC_MENU_BUY_SALES_ITEM")
    local moneyCount = GameHelper:GetAwardCount(GameUILargeMap.MyPlayerData.buy_energy.item_type, GameUILargeMap.MyPlayerData.buy_energy.number, GameUILargeMap.MyPlayerData.buy_energy.no)
    local moneyName = GameHelper:GetAwardNameText(GameUILargeMap.MyPlayerData.buy_energy.item_type, GameUILargeMap.MyPlayerData.buy_energy.number)
    local buyCount = GameUtils.numberConversion(math.floor(GameUILargeMap.MyPlayerData.buy_energy.max)) .. GameLoader:GetGameText("LC_MENU_MAP_FUEL")
    msgStr = string.gsub(msgStr, "<number1>", tostring(moneyCount))
    msgStr = string.gsub(msgStr, "<number2>", tostring(moneyName))
    msgStr = string.gsub(msgStr, "<number3>", tostring(buyCount))
    GameUIMessageDialog:Display(text_title, msgStr)
  end
  do break end
  callback()
end
function GameUILargeMap.LargeMapBuyEnergyCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.large_map_buy_energy_req.Code then
    if content.code ~= 0 then
      local GameVip = LuaObjectManager:GetLuaObject("GameVip")
      GameVip:CheckIsNeedShowVip(content.code)
    end
    return true
  end
  return false
end
function GameUILargeMap:UpdateChatBarText(htmlText)
  DebugOut("GameUILargeMap:UpdateChatBarText:", htmlText)
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "updateChatBarText", htmlText)
  end
end
function GameUILargeMap:MoveInChatIcon()
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "MoveInChatIcon")
  end
end
function GameUILargeMap:ExitAllianceChatChannel()
  GameUIChat.largemapCountryNum = 0
  if GameStateManager:GetCurrentGameState() == GameStateManager.GameStateLargeMap and GameUIChat:GetFlashObject() and GameUIChat.activeChannel == "largemap_country" then
    GameUIChat:SelectChannel("largemap_world")
    GameUIChat:GetFlashObject():InvokeASCallback("_root", "setChannelEnable", "largemap_country", false)
  end
end
