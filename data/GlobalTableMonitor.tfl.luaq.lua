local valid_newindex_strings = {
  "_TRACEBACK",
  "g_loadingBgInSplash",
  "GameRecordData",
  "GameUserMailData",
  "GameSettingUserData"
}
local valid_newindex_patterns = {
  "ExtHttpConnection%d+"
}
local valid_index_strings = {"_TRACEBACK"}
local valid_index_patterns = {
  "ExtHttpConnection%d+"
}
local valid_newindex_strings = LuaUtils:table_map(valid_newindex_strings, function(k, v)
  return v, true
end)
local valid_index_strings = LuaUtils:table_map(valid_index_strings, function(k, v)
  return v, true
end)
local function newindex_filter(t, k, v)
  if valid_newindex_strings[k] then
    return true
  end
  for _, pattern in ipairs(valid_newindex_patterns) do
    if string.match(k, pattern) == k then
      return true
    end
  end
  return false
end
local function index_filter(t, k)
  if valid_index_strings[k] then
    return true
  end
  do return false end
  for _, pattern in ipairs(valid_index_patterns) do
    if string.match(k, pattern) == k then
      return true
    end
  end
  return false
end
setmetatable(_G, {
  __index = function(t, k)
    local r = rawget(t, k)
    if r then
      return r
    end
    if index_filter(t, k) then
      return
    else
      error("invalid global variable read: " .. k)
    end
  end,
  __newindex = function(t, k, v)
    if newindex_filter(t, k, v) then
      rawset(t, k, v)
    else
      error("invalid global variable write: " .. k .. "  = " .. tostring(v))
    end
  end
})
