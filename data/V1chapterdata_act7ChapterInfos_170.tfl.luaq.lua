local ChapterInfos_170 = GameData.chapterdata_act7.ChapterInfos_170
ChapterInfos_170[1] = {
  ChaperID = 1,
  ICON = "icon1",
  MapIndex = 1,
  BossPos = 5,
  EntroStory = {7101, 7102},
  AFTER_FINISH = {
    7105,
    7106,
    7107
  }
}
ChapterInfos_170[2] = {
  ChaperID = 2,
  ICON = "icon2",
  MapIndex = 2,
  BossPos = 10,
  EntroStory = {7201, 7202},
  AFTER_FINISH = {7206, 7207}
}
ChapterInfos_170[3] = {
  ChaperID = 3,
  ICON = "icon30",
  MapIndex = 3,
  BossPos = 16,
  EntroStory = {7301, 7302},
  AFTER_FINISH = {7306, 7307}
}
ChapterInfos_170[4] = {
  ChaperID = 4,
  ICON = "icon400",
  MapIndex = 1,
  BossPos = 18,
  EntroStory = {
    7401,
    7402,
    7403
  },
  AFTER_FINISH = {
    7408,
    7409,
    7410,
    7411,
    7412,
    7413,
    7414
  }
}
ChapterInfos_170[5] = {
  ChaperID = 5,
  ICON = "icon5",
  MapIndex = 1,
  BossPos = 18,
  EntroStory = {7501},
  AFTER_FINISH = {
    7507,
    7508,
    7509,
    7510
  }
}
ChapterInfos_170[6] = {
  ChaperID = 6,
  ICON = "icon400",
  MapIndex = 1,
  BossPos = 18,
  EntroStory = {7601, 7602},
  AFTER_FINISH = {
    7606,
    7607,
    7608,
    7609
  }
}
ChapterInfos_170[7] = {
  ChaperID = 7,
  ICON = "icon6",
  MapIndex = 1,
  BossPos = 18,
  EntroStory = {
    7701,
    7702,
    7703,
    7704,
    7705
  },
  AFTER_FINISH = {7710}
}
ChapterInfos_170[8] = {
  ChaperID = 8,
  ICON = "icon400",
  MapIndex = 1,
  BossPos = 18,
  EntroStory = {7801},
  AFTER_FINISH = {
    7810,
    7811,
    7812,
    7813,
    7814,
    7815,
    7816,
    7817,
    7818
  }
}
ChapterInfos_170[9] = {
  ChaperID = 9,
  ICON = "icon5",
  MapIndex = 1,
  BossPos = 18,
  EntroStory = {},
  AFTER_FINISH = {}
}
ChapterInfos_170[10] = {
  ChaperID = 10,
  ICON = "icon5",
  MapIndex = 1,
  BossPos = 18,
  EntroStory = {},
  AFTER_FINISH = {}
}
