local GameStateAlliance = GameStateManager.GameStateAlliance
local GameObjectAllianceDonation = LuaObjectManager:GetLuaObject("GameObjectAllianceDonation")
local GameUIUserAlliance = LuaObjectManager:GetLuaObject("GameUIUserAlliance")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
local GameVipDetailInfoPanel = LuaObjectManager:GetLuaObject("GameVipDetailInfoPanel")
function GameObjectAllianceDonation:OnInitGame()
  self:ClearLocalDonateInfo()
end
function GameObjectAllianceDonation:OnAddToGameState()
  self:LoadFlashObject()
  local userinfo = GameGlobalData:GetUserInfo()
  local content = {
    donate_type = 0,
    donate_package = {
      {
        exp = 11,
        prestige = 101,
        cost_type = "money",
        cost_value = 1001,
        vip_level = 0
      },
      {
        exp = 22,
        prestige = 202,
        cost_type = "credit",
        cost_value = 2002,
        vip_level = 1
      },
      {
        exp = 33,
        prestige = 303,
        cost_type = "credit",
        cost_value = 3003,
        vip_level = 3
      }
    },
    alliance_name = "\229\177\177\229\175\168\230\152\159\231\155\159",
    alliance_donate_level = 1,
    alliance_donate_exp = 149,
    alliance_donate_lv_exp = 150,
    donate_record = {
      {
        type = 8,
        time = 500000,
        params = {
          userinfo.name,
          "100"
        }
      },
      {
        type = 8,
        time = 350000,
        params = {
          userinfo.name,
          "200"
        }
      },
      {
        type = 8,
        time = 200000,
        params = {
          userinfo.name,
          "300"
        }
      },
      {
        type = 8,
        time = 168354,
        params = {
          userinfo.name,
          "400"
        }
      },
      {
        type = 8,
        time = 1,
        params = {
          userinfo.name,
          "500"
        }
      }
    }
  }
  local function netFailedCallback()
    NetMessageMgr:SendMsg(NetAPIList.donate_info_req.Code, nil, self.DonateInfoCallback, true, netFailedCallback)
  end
  NetMessageMgr:SendMsg(NetAPIList.donate_info_req.Code, nil, self.DonateInfoCallback, true, netFailedCallback)
  local data = {}
  data.LC_MENU_ALLIANCE_DONATE_DEF_CHAR = GameLoader:GetGameText("LC_MENU_ALLIANCE_DONATE_DEF_CHAR")
  data.LC_MENU_ALLIANCE_DONATE_RUSH_EXP_CHAR = GameLoader:GetGameText("LC_MENU_ALLIANCE_DONATE_RUSH_EXP_CHAR")
  data.LC_MENU_ALLIANCE_DONATE_PRESTIGE_CHAR = GameLoader:GetGameText("LC_MENU_ALLIANCE_DONATE_PRESTIGE_CHAR")
  GameObjectAllianceDonation:GetFlashObject():InvokeASCallback("_root", "setDonateText", data)
  self:RefreshResourceCount()
  GameGlobalData:RegisterDataChangeCallback("item_count", self.RefreshResourceCount)
end
function GameObjectAllianceDonation:OnEraseFromGameState()
  self:UnloadFlashObject()
end
function GameObjectAllianceDonation:ClearLocalDonateInfo()
  self.donate_info = {}
  self.donate_info.donate_type = 0
  self.donate_info.alliance_name = "Temp Alliance"
  self.donate_info.alliance_donate_level = 0
  self.donate_info.alliance_donate_exp = 0
  self.donate_info.alliance_donate_lv_exp = 100000000
  self.donate_info.donate_package = {}
  self.donate_info.donate_record = {}
end
function GameObjectAllianceDonation.DonateInfoCallback(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.donate_info_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  if msgtype == NetAPIList.donate_info_ack.Code then
    GameObjectAllianceDonation.donate_info.donate_type = content.donate_type
    GameObjectAllianceDonation.donate_info.alliance_name = content.alliance_name
    GameObjectAllianceDonation.donate_info.alliance_donate_level = content.alliance_donate_level
    GameObjectAllianceDonation.donate_info.alliance_donate_exp = content.alliance_donate_exp
    GameObjectAllianceDonation.donate_info.alliance_donate_lv_exp = content.alliance_donate_lv_exp
    GameObjectAllianceDonation.donate_info.donate_package = content.donate_package
    GameObjectAllianceDonation:UpdateDonateInfo()
    GameObjectAllianceDonation.donate_info.donate_record = content.donate_record
    GameObjectAllianceDonation:UpdateRecordsList()
    GameObjectAllianceDonation:GetFlashObject():InvokeASCallback("_root", "moveInAll")
    return true
  end
  return false
end
function GameObjectAllianceDonation:UpdateDonateInfo()
  local curLevel = GameVipDetailInfoPanel:GetVipLevel()
  local donate_package = self.donate_info.donate_package
  local donate_package_data = ""
  for i, v in ipairs(donate_package) do
    local btnStatus = 0
    local btnText = "donate!"
    if curLevel >= v.vip_level and self.donate_info.donate_type == 0 then
      btnStatus = 1
      btnText = GameLoader:GetGameText("LC_MENU_ALLIANCE_DONATE_BUTTON")
    elseif self.donate_info.donate_type == i then
      btnStatus = 2
      btnText = GameLoader:GetGameText("LC_MENU_ALLIANCE_DONATED_BUTTON")
    else
      btnStatus = 0
      btnText = curLevel >= v.vip_level and GameLoader:GetGameText("LC_MENU_ALLIANCE_DONATE_BUTTON") or "VIP " .. v.vip_level
    end
    if i == 2 and 0 < GameGlobalData:GetItemCount(2506) then
      donate_package_data = donate_package_data .. v.exp .. "\002" .. v.prestige .. "\002" .. v.brick .. "\002" .. 2506 .. "\002" .. 1 .. "\002" .. btnStatus .. "\002" .. btnText .. "\001"
    elseif i == 3 and 0 < GameGlobalData:GetItemCount(2507) then
      donate_package_data = donate_package_data .. v.exp .. "\002" .. v.prestige .. "\002" .. v.brick .. "\002" .. 2507 .. "\002" .. 1 .. "\002" .. btnStatus .. "\002" .. btnText .. "\001"
    else
      donate_package_data = donate_package_data .. v.exp .. "\002" .. v.prestige .. "\002" .. v.brick .. "\002" .. v.cost_type .. "\002" .. v.cost_value .. "\002" .. btnStatus .. "\002" .. btnText .. "\001"
    end
  end
  GameObjectAllianceDonation:GetFlashObject():InvokeASCallback("_root", "setDonateInfo", self.donate_info.donate_type, donate_package_data, self.donate_info.alliance_donate_level, self.donate_info.alliance_donate_exp, self.donate_info.alliance_donate_lv_exp)
end
function GameObjectAllianceDonation:UpdateRecordsList()
  local donate_record = self.donate_info.donate_record
  table.sort(donate_record, function(a, b)
    return a.time < b.time
  end)
  DebugOutPutTable(donate_record, "donate_record")
  local all_records_data = ""
  for i, record_data in ipairs(donate_record) do
    local userDisplayName = GameUtils:GetUserDisplayName(record_data.params[1])
    local msg_content = GameUIUserAlliance:GetLogStringWithTypeAndParams(record_data.type, record_data.params)
    local time_ago = GameUtils:formatTimeStringAsAgoStyle(record_data.time)
    all_records_data = all_records_data .. userDisplayName .. "\002" .. msg_content .. "\002" .. time_ago .. "\001"
  end
  GameObjectAllianceDonation:GetFlashObject():InvokeASCallback("_root", "setDonateRecords", all_records_data)
end
function GameObjectAllianceDonation:OnFSCommand(cmd, arg)
  if cmd == "onClose" then
    GameUIUserAlliance:SetVisible(nil, true)
  elseif cmd == "onErase" then
    GameStateAlliance:EraseObject(GameObjectAllianceDonation)
  elseif cmd == "onMoveInOver" then
    GameUIUserAlliance:SetVisible(nil, false)
  elseif cmd == "pressDonate" then
    local index = tonumber(arg)
    local donate_package = self.donate_info.donate_package
    if donate_package[index].cost_type == "credit" then
      local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
      GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
      GameUIMessageDialog:SetYesButton(GameObjectAllianceDonation.DonateRequest, {GameObjectAllianceDonation, index})
      local titleText = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
      local contentText = string.format(GameLoader:GetGameText("LC_MENU_DONATE_CREDITS_CONFIRM_CHAR"), donate_package[index].cost_value)
      if index == 2 then
        if GameGlobalData:GetItemCount(2506) > 0 then
          contentText = string.format(GameLoader:GetGameText("LC_MENU_ITEM_USE_ALERT"), GameLoader:GetGameText("LC_ITEM_ITEM_NAME_2506"))
        end
      elseif index == 3 and 0 < GameGlobalData:GetItemCount(2507) then
        contentText = string.format(GameLoader:GetGameText("LC_MENU_ITEM_USE_ALERT"), GameLoader:GetGameText("LC_ITEM_ITEM_NAME_2507"))
      end
      GameUIMessageDialog:Display(titleText, contentText)
      DebugOut("GameUIMessageDialog:Display")
      DebugOut(titleText)
      DebugOut(contentText)
    else
      self:DonateRequest(index)
    end
  end
end
function GameObjectAllianceDonation:DonateRequest(index)
  local req_param = {donate_type = index}
  local function netFailedCallback()
    NetMessageMgr:SendMsg(NetAPIList.donate_req.Code, req_param, self.DonateCallback, true, netFailedCallback)
  end
  NetMessageMgr:SendMsg(NetAPIList.donate_req.Code, req_param, self.DonateCallback, true, netFailedCallback)
end
function GameObjectAllianceDonation.DonateCallback(msgtype, content)
  DebugOut("DonateCallback  msgtype=" .. msgtype)
  DebugTable(content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.donate_req.Code then
    if content.code ~= 0 then
      local GameVip = LuaObjectManager:GetLuaObject("GameVip")
      GameVip:CheckIsNeedShowVip(content.code)
    end
    return true
  end
  if msgtype == NetAPIList.donate_ack.Code then
    GameObjectAllianceDonation.donate_info.donate_type = content.donate_type
    GameObjectAllianceDonation.donate_info.alliance_name = content.alliance_name
    GameObjectAllianceDonation.donate_info.alliance_donate_level = content.alliance_donate_level
    GameObjectAllianceDonation.donate_info.alliance_donate_exp = content.alliance_donate_exp
    GameObjectAllianceDonation.donate_info.alliance_donate_lv_exp = content.alliance_donate_lv_exp
    GameObjectAllianceDonation.donate_info.donate_package = content.donate_package
    GameObjectAllianceDonation:UpdateDonateInfo()
    GameObjectAllianceDonation.donate_info.donate_record = content.donate_record
    GameObjectAllianceDonation:UpdateRecordsList()
    return true
  end
  return false
end
if AutoUpdate.isAndroidDevice then
  function GameObjectAllianceDonation.OnAndroidBack()
    GameObjectAllianceDonation:GetFlashObject():InvokeASCallback("_root", "moveOutAll")
  end
end
function GameObjectAllianceDonation:RefreshResourceCount()
  local resource = GameGlobalData:GetData("item_count")
  local flash_obj = GameObjectAllianceDonation:GetFlashObject()
  if flash_obj and resource then
    local item_2506_count = 0
    local item_2507_count = 0
    for i, v in ipairs(resource.items) do
      if v.item_id == 2506 then
        item_2506_count = v.item_no
      elseif v.item_id == 2507 then
        item_2507_count = v.item_no
      end
      if item_2506_count > 0 and item_2507_count > 0 then
        break
      end
    end
    DebugOut("RefreshResourceCount", item_2506_count, item_2507_count)
    flash_obj:InvokeASCallback("_root", "setItemCount", item_2506_count, item_2507_count)
    GameObjectAllianceDonation:UpdateDonateInfo()
  end
end
