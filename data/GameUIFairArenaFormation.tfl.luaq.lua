local GameUIFairArenaRank = LuaObjectManager:GetLuaObject("GameUIFairArenaRank")
local GameUIFairArenaMain = LuaObjectManager:GetLuaObject("GameUIFairArenaMain")
local GameStateFairArena = GameStateManager.GameStateFairArena
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameUIFairArenaFormation = LuaObjectManager:GetLuaObject("GameUIFairArenaFormation")
local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
GameUIFairArenaFormation.Formations = {
  [1] = {},
  [2] = {},
  [3] = {}
}
GameUIFairArenaFormation.all_fleets = {}
local formationType = {
  first = 1,
  second = 2,
  third = 3
}
local fleetPoolType = {
  hero = "hero_list",
  adjutant = "adjutant_list"
}
local fleetCountLimit = 5
GameUIFairArenaFormation.curFormation = formationType.first
GameUIFairArenaFormation.curFleetPoolType = fleetPoolType.hero
GameUIFairArenaFormation.selectable_hero = {}
GameUIFairArenaFormation.selectable_adjutant = {}
GameUIFairArenaFormation.pressFleetSlot = -1
GameUIFairArenaFormation.pressFormationSlot = -1
GameUIFairArenaFormation.dragFleetItem = false
GameUIFairArenaFormation.mFleetInfos = {}
GameUIFairArenaFormation.dragFormationDetail = {}
GameUIFairArenaFormation.dragFormationBgColor = nil
GameUIFairArenaFormation.dragFormationAdjutantBgColor = nil
GameUIFairArenaFormation.dragFormationAdjutantHead = nil
GameUIFairArenaFormation.dragAdjutantInfo = {}
GameUIFairArenaFormation.isShowAdjutantDetail = false
GameUIFairArenaFormation.selectable_spell = {}
GameUIFairArenaFormation.curFlagshipSkill = {
  1,
  1,
  1
}
GameUIFairArenaFormation.curFlagshipDetail = {}
GameUIFairArenaFormation.isFormationModify = false
function GameUIFairArenaFormation:OnEraseFromGameState()
  self:UnloadFlashObject()
end
function GameUIFairArenaFormation:OnAddToGameState(game_state)
  DebugOut("GameUIFairArenaFormation:OnAddToGameState")
  self:LoadFlashObject()
  self:RequestAllData()
  self.isFormationModify = false
  GameTimer:Add(self._OnTimerTick, 1000)
end
function GameUIFairArenaFormation._OnTimerTick()
  local flashObj = GameUIFairArenaFormation:GetFlashObject()
  if flashObj and GameStateFairArena.baseData then
    local left = GameStateFairArena.baseData.left_time - (os.time() - GameStateFairArena.baseData.baseTime)
    if left < 0 then
      left = 0
    end
    local tiemStr = ""
    if GameStateFairArena.baseData.left_time < 0 then
      tiemStr = ""
    elseif GameStateFairArena.baseData.left_time >= 0 and left > 0 then
      tiemStr = GameLoader:GetGameText("LC_MENU_NOTICE_TEXT_FORMATION_OVER") .. GameUtils:formatTimeStringAsPartion2(left)
    end
    GameUIFairArenaFormation:GetFlashObject():InvokeASCallback("_root", "SetInfoText", tiemStr)
  end
  if GameStateManager:GetCurrentGameState():IsObjectInState(GameUIFairArenaFormation) then
    return 1000
  end
  return nil
end
function sortFleet(a, b)
  if a.vessels == 6 and b.vessels ~= 6 then
    return true
  elseif a.vessels ~= 6 and b.vessels == 6 then
    return false
  elseif a.fleet_id < b.fleet_id then
    return true
  else
    return false
  end
end
function GameUIFairArenaFormation.downLoadCallBack()
  local refreshIcon = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(GameUIFairArenaFormation.CostTable, nil, nil)
  local refreshCount = GameHelper:GetAwardCount(GameUIFairArenaFormation.CostTable.item_type, GameUIFairArenaFormation.CostTable.number, GameUIFairArenaFormation.CostTable.no)
  local costDetail = {}
  costDetail.refreshIcon = refreshIcon
  costDetail.price = refreshCount
  costDetail.stat = 1
  if GameUIFairArenaFormation:GetFlashObject() then
    GameUIFairArenaFormation:GetFlashObject():InvokeASCallback("_root", "SetRefreshCost", costDetail)
  end
end
function GameUIFairArenaFormation:OnInitMenu()
  self:ReFreshSelectableHeroList()
  self:ReFreshSelectableAdjutantList()
  local FleetPoolItemCount = math.ceil(#GameUIFairArenaFormation.selectable_hero / 3)
  local refreshIcon = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(self.CostTable, nil, GameUIFairArenaFormation.downLoadCallBack)
  local refreshCount = GameHelper:GetAwardCount(self.CostTable.item_type, self.CostTable.number, self.CostTable.no)
  local costDetail = {}
  costDetail.refreshIcon = refreshIcon
  costDetail.price = refreshCount
  costDetail.stat = self.CostTable.stat
  local formationInfo = self:GetFormationData(formationType.first, true)
  local flashObj = self:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "Show", FleetPoolItemCount, formationInfo, costDetail, GameUIFairArenaFormation.curFormation, GameUIFairArenaFormation.curFleetPoolType, self.selectable_spell, self.curFlagshipSkill[self.curFormation])
  end
end
function GameUIFairArenaFormation:RequestAllData()
  NetMessageMgr:SendMsg(NetAPIList.fair_arena_formation_all_req.Code, nil, GameUIFairArenaFormation.AllDataReqCallBack, true, nil)
end
function GameUIFairArenaFormation.AllDataReqCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.fair_arena_formation_all_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      GameStateFairArena:SwitchState(GameStateFairArena.MainState)
    end
    return true
  elseif msgType == NetAPIList.fair_arena_formation_all_ack.Code then
    DebugOut("AllDataReqCallBack")
    DebugTable(content)
    GameUIFairArenaFormation.Formations[1] = content.formation_1
    GameUIFairArenaFormation.Formations[2] = content.formation_2
    GameUIFairArenaFormation.Formations[3] = content.formation_3
    GameUIFairArenaFormation.selectable_spell = content.active_spell
    GameUIFairArenaFormation:GetCurFlagshipSkill()
    GameUIFairArenaFormation.all_fleets = content.selectable_fleets
    GameUIFairArenaFormation.CostTable = content.refresh_price.items[1]
    GameUIFairArenaFormation.CostTable.stat = content.can_refresh
    GameUIFairArenaFormation.curFormation = formationType.first
    GameUIFairArenaFormation.curFleetPoolType = fleetPoolType.hero
    table.sort(GameUIFairArenaFormation.all_fleets.hero_list, sortFleet)
    table.sort(GameUIFairArenaFormation.all_fleets.adjutant_list, sortFleet)
    GameUIFairArenaFormation:OnInitMenu()
    return true
  end
  return false
end
function GameUIFairArenaFormation:GetCurFlagshipSkill()
  for i = 1, 3 do
    if self.Formations[i] and #self.Formations[i] > 0 then
      local hasFlagSkill = false
      for k, v in ipairs(self.Formations[i]) do
        if v.fleet_info.fleet.vessels == 6 then
          hasFlagSkill = true
          self.curFlagshipSkill[i] = v.fleet_info.fleet.spell_id
        end
      end
      if not hasFlagSkill then
        self.curFlagshipSkill[i] = self.selectable_spell[1]
      end
    else
      self.Formations[i] = {}
      self.curFlagshipSkill[i] = self.selectable_spell[1]
    end
  end
end
function GameUIFairArenaFormation:GetHeroFormAllPool(fleetId)
  for k, v in ipairs(GameUIFairArenaFormation.all_fleets.hero_list) do
    if v.fleet_id == fleetId then
      return v
    end
  end
  return nil
end
function GameUIFairArenaFormation:GetAdjutantFormAllPool(fleetId)
  DebugOutPutTable(GameUIFairArenaFormation.all_fleets.adjutant_list, "GameUIFairArenaFormation:GetAdjutantFormAllPool")
  for k, v in ipairs(GameUIFairArenaFormation.all_fleets.adjutant_list) do
    if v.fleet_id == fleetId then
      return v
    end
  end
  return nil
end
function GameUIFairArenaFormation:GetFormationData(formationId, needAddFlashShip)
  GameUIFairArenaFormation.curFormation = formationId
  local formationInfo = {}
  DebugOutPutTable(self.Formations, "GameUIFairArenaFormation:GetFormationData")
  for i = 1, 3 do
    if #self.Formations[i] == 0 and needAddFlashShip then
      local firstFlagShipDetail = self.all_fleets.hero_list[1]
      if firstFlagShipDetail then
        local Detail = {
          fleet_info = {
            fleet = {
              fleet_id = firstFlagShipDetail.fleet_id,
              fleet_level = firstFlagShipDetail.fleet_level,
              vessels = firstFlagShipDetail.vessels,
              sex = firstFlagShipDetail.sex,
              adjust_info = firstFlagShipDetail.adjust_info,
              spell_id = self.curFlagshipSkill[self.curFormation]
            },
            adjutant = {
              fleet_id = 0,
              fleet_level = 0,
              vessels = 0,
              sex = 0,
              adjust_info = {},
              spell_id = 0
            }
          },
          pos = 1
        }
        table.insert(self.Formations[i], Detail)
      end
    end
  end
  self:ReFreshSelectableHeroList()
  self:RefreshFleetPool()
  for _, v in ipairs(self.Formations[GameUIFairArenaFormation.curFormation]) do
    local commanderAbility = GameDataAccessHelper:GetCommanderAbility(v.fleet_info.fleet.fleet_id, v.fleet_info.fleet.fleet_level)
    if commanderAbility ~= nil then
      local detail = {}
      detail.pos = v.pos
      if v.fleet_info.fleet.fleet_id == 1 then
        detail.avatar = GameGlobalData:GetUserInfo().sex == 1 and "male" or "female"
      else
        detail.avatar = GameDataAccessHelper:GetFleetAvatar(v.fleet_info.fleet.fleet_id, v.fleet_info.fleet.fleet_level, v.fleet_info.fleet.sex)
      end
      detail.rankIcon = GameUtils:GetFleetRankFrameInFairArena(commanderAbility.Rank, GameUIFairArenaFormation.RankImageDownloadCallback, v.pos, formationId, self.curFleetPoolType, false)
      detail.vessels_type = "TYPE_" .. tostring(commanderAbility.vessels)
      detail.bgColor = commanderAbility.COLOR
      detail.adjutant = nil
      if v.fleet_info.adjutant.fleet_id ~= 0 then
        detail.adjutant = {}
        local adjutantAbility = GameDataAccessHelper:GetCommanderAbility(v.fleet_info.adjutant.fleet_id, v.fleet_info.adjutant.fleet_level)
        if adjutantAbility ~= nil then
          detail.adjutant.avatar = GameDataAccessHelper:GetFleetAvatarByAvateImage(adjutantAbility.AVATAR)
          detail.adjutant.bgColor = adjutantAbility.COLOR
        end
      end
      table.insert(formationInfo, detail)
    end
  end
  DebugOut("GetFormationData", formationId)
  DebugTable(formationInfo)
  return formationInfo
end
function GameUIFairArenaFormation:SwitchFormation(formationId, needAddFlashShip)
  local formationInfo = self:GetFormationData(formationId, needAddFlashShip)
  local flashObj = self:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "SetFormationData", formationId, formationInfo)
  end
end
function GameUIFairArenaFormation:SwitchFleetPool(poolType)
  GameUIFairArenaFormation.curFleetPoolType = poolType
  local FleetPoolItemCount = math.ceil(#GameUIFairArenaFormation.selectable_hero / 3)
  if GameUIFairArenaFormation.curFleetPoolType == fleetPoolType.adjutant then
    FleetPoolItemCount = math.ceil(#GameUIFairArenaFormation.selectable_adjutant / 3)
  end
  local flashObj = self:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "SetFleetPoolList", poolType, FleetPoolItemCount)
  end
end
function GameUIFairArenaFormation:RequestRefreshFleetPool()
  local function callback()
    NetMessageMgr:SendMsg(NetAPIList.refresh_fleet_pool_req.Code, param, GameUIFairArenaFormation.RefreshFleetPoolCallback, true, nil)
  end
  if self.CostTable.item_type == "credit" then
    local text_title = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
    local info = string.gsub(GameLoader:GetGameText("LC_MENU_PLANETCRAFT_COST_TIP_REFRESH"), "<credits_num>", tostring(self.CostTable.number))
    GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
    GameUIMessageDialog:SetYesButton(callback)
    GameUIMessageDialog:Display(text_title, info)
  else
    callback()
  end
end
function GameUIFairArenaFormation.RefreshFleetPoolCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.refresh_fleet_pool_req.Code then
    DebugOut("GameUIFairArenaFormation.RefreshFleetPoolCallback")
    DebugTable(content)
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.refresh_fleet_pool_ack.Code then
    DebugOut("GameUIFairArenaFormation.RefreshFleetPoolCallback ")
    DebugTable(content)
    if content.result == 0 then
      GameTip:Show(GameLoader:GetGameText("LC_MENU_REFRESH_SUCCESS"), 3000)
      GameUIFairArenaFormation.Formations = {
        [1] = {},
        [2] = {},
        [3] = {}
      }
      GameUIFairArenaFormation.CostTable = content.next_refresh_price.items[1]
      GameUIFairArenaFormation.CostTable.stat = content.can_refresh
      GameUIFairArenaFormation.all_fleets = content.fleet_pool
      table.sort(GameUIFairArenaFormation.all_fleets.hero_list, sortFleet)
      table.sort(GameUIFairArenaFormation.all_fleets.adjutant_list, sortFleet)
      GameUIFairArenaFormation.curFormation = formationType.first
      GameUIFairArenaFormation.curFleetPoolType = fleetPoolType.hero
      GameUIFairArenaFormation:OnInitMenu()
    end
    return true
  end
  return false
end
function GameUIFairArenaFormation:CommitFormation()
  local function callback()
    local param = {
      formation_1 = self.Formations[1] or {},
      formation_2 = self.Formations[2] or {},
      formation_3 = self.Formations[3] or {}
    }
    DebugOutPutTable(param, "GameUIFairArenaFormation:CommitFormation")
    NetMessageMgr:SendMsg(NetAPIList.update_formations_req.Code, param, GameUIFairArenaFormation.UpdateFormationsCallback, true, nil)
  end
  local text_title = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
  local info = GameLoader:GetGameText("LC_MENU_NOTICE_TEXT_1")
  GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
  GameUIMessageDialog:SetYesButton(callback)
  GameUIMessageDialog:Display(text_title, info)
end
function GameUIFairArenaFormation.UpdateFormationsCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.update_formations_req.Code then
    DebugOut("GameUIFairArenaFormation.UpdateFormationsCallback")
    DebugTable(content)
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.update_formations_ack.Code then
    DebugOut("GameUIFairArenaFormation.UpdateFormationsCallback ")
    DebugTable(content)
    if content.result == 0 then
      GameUIFairArenaFormation.isFormationModify = false
      GameTip:Show(GameLoader:GetGameText("LC_MENU_NOTICE_TEXT_2"), 3000)
      GameUIFairArenaFormation:GetFlashObject():InvokeASCallback("_root", "MoveOut")
    end
    return true
  end
  return false
end
function GameUIFairArenaFormation:OnFSCommand(cmd, arg)
  if cmd == "close" then
    GameStateFairArena:SwitchState(GameStateFairArena.MainState)
    return
  elseif cmd == "QuitMenu" then
    local function callback()
      GameUIFairArenaFormation:GetFlashObject():InvokeASCallback("_root", "MoveOut")
    end
    if self.isFormationModify then
      local text_title = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
      local info = GameLoader:GetGameText("LC_ALERT_NOTICE_EMPIRE_COLOSSEUM_QUIT")
      GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
      GameUIMessageDialog:SetYesButton(callback)
      GameUIMessageDialog:Display(text_title, info)
    else
      callback()
    end
  elseif cmd == "UpdateFleetPoolList" then
    self:UpdateFleetPoolList(tonumber(arg))
  elseif cmd == "beginFleetItmePress" then
    self.pressFleetSlot = tonumber(arg)
  elseif cmd == "drag_fleet_item_start" then
    local param = LuaUtils:string_split(arg, "\001")
    local newArg = "" .. param[3] .. "\001" .. param[4] .. "\001" .. param[5] .. "\001" .. param[6]
    self:dragItem("dragFleetItem", newArg)
  elseif cmd == "drag_formation_item_start" then
    local param = LuaUtils:string_split(arg, "\001")
    local newArg = "" .. param[2] .. "\001" .. param[3] .. "\001" .. param[4] .. "\001" .. param[5]
    self.pressFormationSlot = tonumber(param[1])
    self:dragItem("dragFormtionItem", newArg)
  elseif cmd == "SwitchFormation" then
    self:SwitchFormation(tonumber(arg), true)
  elseif cmd == "SwitchFleetPool" then
    self:SwitchFleetPool(arg)
  elseif cmd == "FleetItemClicked" then
    local param = LuaUtils:string_split(arg, "\001")
    local fleetIdx = (tonumber(param[1]) - 1) * 3 + tonumber(param[2])
    local fleetInfo = GameUIFairArenaFormation.selectable_hero[fleetIdx]
    local isHero = true
    if GameUIFairArenaFormation.curFleetPoolType == fleetPoolType.adjutant then
      fleetInfo = GameUIFairArenaFormation.selectable_adjutant[fleetIdx]
      isHero = false
    end
    self:ShowFleetDetail(fleetInfo, isHero)
  elseif cmd == "formationItemClicked" then
    local formationDetail = self:GetCurFormationDetail(tonumber(arg))
    if formationDetail then
      local fleetInfo = formationDetail.fleet_info.fleet
      local isHero = true
      if GameUIFairArenaFormation.curFleetPoolType == fleetPoolType.adjutant and formationDetail.fleet_info.adjutant.fleet_id ~= 0 then
        fleetInfo = formationDetail.fleet_info.adjutant
        isHero = false
      end
      if isHero and fleetInfo.fleet_id == 1 then
        self:ShowSkillSelector(tonumber(arg), self.curFlagshipSkill[self.curFormation])
      else
        self:ShowFleetDetail(fleetInfo, isHero)
      end
    end
  elseif cmd == "selectedSkill" then
    self.curFlagshipSkill[self.curFormation] = tonumber(arg)
    local skillinfo = GameDataAccessHelper:GetSkillInfo(skill_id)
    self:SetSkillDesBoxInfo(tonumber(arg))
    for k, v in ipairs(self.Formations[self.curFormation]) do
      if v.fleet_info.fleet.vessels == 6 then
        v.fleet_info.fleet.spell_id = tonumber(arg)
      end
    end
  elseif cmd == "CommitFormation" then
    self:CommitFormation()
  elseif cmd == "RefreshFleetPool" then
    self:RequestRefreshFleetPool()
  end
end
function GameUIFairArenaFormation:SetSkillDesBoxInfo(skill_id)
  local datatable = {}
  datatable[#datatable + 1] = GameDataAccessHelper:GetSkillNameText(skill_id)
  datatable[#datatable + 1] = GameDataAccessHelper:GetSkillDesc(skill_id)
  local skillrangetype = GameDataAccessHelper:GetSkillRangeType(skill_id)
  datatable[#datatable + 1] = "TYPE_" .. skillrangetype
  datatable[#datatable + 1] = GameLoader:GetGameText("LC_MENU_SKILL_RANGE_TYPE_" .. skillrangetype)
  local datastring = table.concat(datatable, "\001")
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "setSkillDestBox", datastring)
  end
end
function GameUIFairArenaFormation:ShowSkillSelector(index, skill)
  DebugOut("ShowSkillSelector ", index, skill)
  self:SetSkillDesBoxInfo(skill)
  local flashObj = self:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "ShowSkillSelector", index, skill)
  end
end
function GameUIFairArenaFormation:UpdateFleetPoolList(itemId)
  local threeItem = {}
  for k = 1, 3 do
    local idx = (itemId - 1) * 3 + k
    local fleetInfo = GameUIFairArenaFormation.selectable_hero[idx]
    if GameUIFairArenaFormation.curFleetPoolType == fleetPoolType.adjutant then
      fleetInfo = GameUIFairArenaFormation.selectable_adjutant[idx]
    end
    local itemDetail = {}
    DebugOut("GameUIFairArenaFormation:UpdateFleetPoolList", itemId, k, GameGlobalData:GetUserInfo().sex)
    if fleetInfo then
      local commanderAbility = GameDataAccessHelper:GetCommanderAbility(fleetInfo.fleet_id, fleetInfo.fleet_level)
      if commanderAbility then
        if fleetInfo.fleet_id == 1 then
          fleetInfo.sex = GameGlobalData:GetUserInfo().sex
          itemDetail.head_frame = GameGlobalData:GetUserInfo().sex == 1 and "male" or "female"
        else
          itemDetail.head_frame = GameDataAccessHelper:GetFleetAvatar(fleetInfo.fleet_id, fleetInfo.fleet_level, fleetInfo.sex)
        end
        itemDetail.rank_frame = GameUtils:GetFleetRankFrameInFairArena(commanderAbility.Rank, GameUIFairArenaFormation.RankImageDownloadCallback, idx, self.curFormation, self.curFleetPoolType, true)
        itemDetail.vessels_frame = "TYPE_" .. tostring(commanderAbility.vessels)
        itemDetail.bgColor = commanderAbility.COLOR
        if GameUIFairArenaFormation.curFleetPoolType == fleetPoolType.hero then
          itemDetail.bgColor = 2
        end
        itemDetail.isHide = false
      end
    else
      itemDetail.isHide = true
    end
    DebugTable(itemDetail)
    threeItem[k] = itemDetail
  end
  local flashObj = self:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "UpdateFleetListItem", itemId, threeItem[1], threeItem[2], threeItem[3])
    DebugOut("Lua UpdateFleetListItem", itemId)
    DebugTable(threeItem)
  end
end
function GameUIFairArenaFormation.RankImageDownloadCallback(extInfo)
  DebugOut("GameUIFairArenaFormation.RankImageDownloadCallback")
  DebugTable(extInfo)
  local flashObj = GameUIFairArenaFormation:GetFlashObject()
  if flashObj then
    if extInfo.isPool then
      flashObj:InvokeASCallback("_root", "SetPoolFleetRankImage", extInfo.id, extInfo.rank_id, extInfo.poolType)
    else
      flashObj:InvokeASCallback("_root", "SetFormationFleetRankFrame", extInfo.id, extInfo.rank_id, extInfo.formationType)
    end
  end
end
function GameUIFairArenaFormation:dragItem(cmd, arg)
  local param = LuaUtils:string_split(arg, "\001")
  local initPosX, initPosY, posX, posY = unpack(param)
  DebugOut("GameUIFairArenaFormation:dragItem", initPosX, initPosY, posX, posY)
  self.dragX = tonumber(posX)
  self.dragY = tonumber(posY)
  self.initPosX = tonumber(initPosX)
  self.initPosY = tonumber(initPosY)
  local pressIndex = -1
  if cmd == "dragFleetItem" then
    pressIndex = self.pressFleetSlot
  end
  DebugOut("pressIndex: ", pressIndex)
  if pressIndex ~= -1 then
    local fleetInfo
    if cmd == "dragFleetItem" then
      fleetInfo = GameUIFairArenaFormation.selectable_hero[pressIndex]
      if GameUIFairArenaFormation.curFleetPoolType == fleetPoolType.adjutant then
        fleetInfo = GameUIFairArenaFormation.selectable_adjutant[pressIndex]
      end
    end
    DebugOutPutTable(fleetInfo, "fleetInfo: ")
    if fleetInfo then
      if cmd == "dragFleetItem" then
        self.dragFleetItem = true
      end
      self.dragAdjutantInfo = fleetInfo.adjust_info
      GameStateManager:GetCurrentGameState():BeginDragItem(GameUIFairArenaFormation.curFleetPoolType == "hero_list", self.dragFleetItem, fleetInfo.fleet_id, fleetInfo.fleet_level, fleetInfo.sex, fleetInfo.vessels, self.initPosX, self.initPosY, self.dragX, self.dragY)
    else
      GameUIFairArenaFormation:onEndDragItem()
    end
  end
  if cmd == "dragFormtionItem" then
    local fleetInfo = {}
    local formation = self.Formations[self.curFormation]
    for k, v in ipairs(formation) do
      if v.pos == self.pressFormationSlot then
        if self.curFleetPoolType == fleetPoolType.hero then
          self.dragFormationDetail = v
          table.remove(formation, k)
          break
        end
        self.dragFormationDetail = {}
        self.dragFormationDetail.fleet_info = {}
        self.dragFormationDetail.fleet_info.adjutant = {}
        self.dragFormationDetail.fleet_info.fleet = v.fleet_info.fleet
        self.dragFormationDetail.pos = self.pressFormationSlot
        self.dragFormationDetail.fleet_info.adjutant = {}
        self:copyAdjutantTable(self.dragFormationDetail.fleet_info.adjutant, v.fleet_info.adjutant)
        v.fleet_info.adjutant.fleet_id = 0
        if v.fleet_info.fleet.vessels == 6 then
          self:ChangedFlagshipAdjutant(v.fleet_info.adjutant)
        end
        break
      end
    end
    fleetInfo = self.dragFormationDetail.fleet_info.fleet
    if self.curFleetPoolType == fleetPoolType.adjutant then
      fleetInfo = self.dragFormationDetail.fleet_info.adjutant
      if fleetInfo.fleet_id ~= 0 then
        self.dragFormationAdjutantBgColor = GameDataAccessHelper:GetCommanderColor(fleetInfo.fleet_id, fleetInfo.fleet_level)
        self.dragFormationAdjutantHead = GameDataAccessHelper:GetFleetAvatar(fleetInfo.fleet_id, fleetInfo.fleet_level)
      end
    else
      self.dragFormationBgColor = GameDataAccessHelper:GetCommanderColor(fleetInfo.fleet_id, fleetInfo.fleet_level)
    end
    if fleetInfo and self.curFleetPoolType == fleetPoolType.hero or self.curFleetPoolType == fleetPoolType.adjutant and fleetInfo.fleet_id ~= 0 then
      self.dragFleetItem = false
      self.dragAdjutantInfo = fleetInfo.adjust_info
      GameStateManager:GetCurrentGameState():BeginDragItem(GameUIFairArenaFormation.curFleetPoolType == "hero_list", self.dragFleetItem, fleetInfo.fleet_id, fleetInfo.fleet_level, fleetInfo.sex, fleetInfo.vessels, self.initPosX, self.initPosY, self.dragX, self.dragY)
    else
      GameUIFairArenaFormation:onEndDragItem()
    end
  end
end
function GameUIFairArenaFormation:onBeginDragItem(dragitem)
  self.dragItemInstance = dragitem
  DebugOut("onBeginDragItem: ", self.dragFleetItem, self.pressFleetSlot, self.dragFormationBgColor, self.dragFormationAdjutantBgColor, self.dragFormationAdjutantHead)
  DebugTable(self.dragAdjutantInfo)
  local bgColor
  if not self.dragFleetItem then
    self:RefreshFormation()
  end
  self:GetFlashObject():InvokeASCallback("_root", "onBeginDragItem", self.pressFleetSlot, self.dragFleetItem, self.dragFormationBgColor, self.dragFormationAdjutantHead, self.dragFormationAdjutantBgColor, self.dragAdjutantInfo)
end
function GameUIFairArenaFormation:onEndDragItem()
  self.dragFleetItem = false
  self.dragItemInstance = nil
  self.dragFormationBgColor = nil
  self.dragFormationAdjutantBgColor = nil
  self.dragFormationAdjutantHead = nil
  self.dragAdjutantInfo = nil
  self.pressFormationSlot = -1
  self:GetFlashObject():InvokeASCallback("_root", "onEndDragItem")
end
function GameUIFairArenaFormation:RefreshFleetPool()
  self:SwitchFleetPool(GameUIFairArenaFormation.curFleetPoolType)
end
function GameUIFairArenaFormation:RefreshFormation()
  self:SwitchFormation(self.curFormation, false)
end
function GameUIFairArenaFormation:UnSelectFleet()
  self:ReFreshSelectableHeroList()
  self:ReFreshSelectableAdjutantList()
  self:RefreshFleetPool()
end
function GameUIFairArenaFormation:NotChangeFormation()
  self:RemovePosFormamtion(self.curFormation, self.dragFormationDetail.pos)
  table.insert(self.Formations[self.curFormation], self.dragFormationDetail)
  self.dragFormationDetail = {}
  self:RefreshFormation()
end
function GameUIFairArenaFormation:ChangedHero(fleet_id, fleet_level, formationIdx)
  DebugOut("GameUIFairArenaFormation:ChangedHero", fleet_id, fleet_level, formationIdx)
  self:SwitchFormamtion(self.curFormation, formationIdx)
  self.dragFormationDetail.pos = formationIdx
  self:CheckHasRepeatHero(self.dragFormationDetail)
  table.insert(self.Formations[self.curFormation], self.dragFormationDetail)
  self.dragFormationDetail = {}
  self:ReFreshSelectableHeroList()
  self:ReFreshSelectableAdjutantList()
  self:RefreshFleetPool()
  self:RefreshFormation()
  self.isFormationModify = true
end
function GameUIFairArenaFormation:RemovePosFormamtion(formationIndex, pos)
  DebugOut("GameUIFairArenaFormation:RemovePosFormamtion", formationIndex, pos)
  for k, v in ipairs(GameUIFairArenaFormation.Formations[formationIndex]) do
    if v.pos == pos then
      table.remove(GameUIFairArenaFormation.Formations[formationIndex], k)
      break
    end
  end
end
function GameUIFairArenaFormation:SwitchFormamtion(formationIndex, pos)
  DebugOut("GameUIFairArenaFormation:RemovePosFormamtion", formationIndex, pos, self.pressFormationSlot)
  for k, v in ipairs(GameUIFairArenaFormation.Formations[formationIndex]) do
    if v.pos == pos then
      v.pos = self.pressFormationSlot
      break
    end
  end
end
function GameUIFairArenaFormation:GetCurFormationDetail(pos)
  DebugOut("GameUIFairArenaFormation:GetCurFormationDetail", pos)
  for k, v in ipairs(GameUIFairArenaFormation.Formations[self.curFormation]) do
    if v.pos == pos then
      return v
    end
  end
end
function GameUIFairArenaFormation:CheckHasRepeatHero(formationDetail)
  for k, v in ipairs(GameUIFairArenaFormation.Formations[self.curFormation]) do
    if v.fleet_info.fleet.fleet_id == formationDetail.fleet_info.fleet.fleet_id then
      table.remove(GameUIFairArenaFormation.Formations[self.curFormation], k)
    end
  end
end
function GameUIFairArenaFormation:SelectedHero(fleet_id, fleet_level, sex, vesselsType, formationIdx)
  DebugOut("GameUIFairArenaFormation:SelectedHero ", fleet_id, fleet_level, vesselsType, formationIdx)
  local detail = {}
  local commanderAbility = GameDataAccessHelper:GetCommanderAbility(fleet_id, fleet_level)
  if commanderAbility ~= nil then
    detail.pos = formationIdx
    detail.avatar = GameDataAccessHelper:GetFleetAvatar(fleet_id, fleet_level, sex)
    detail.rankIcon = GameUtils:GetFleetRankFrame(commanderAbility.Rank, nil)
    detail.vessels_type = "TYPE_" .. tostring(vesselsType)
    detail.bgColor = commanderAbility.COLOR
    detail.adjutant = nil
  end
  local formationDetail = {
    fleet_info = {
      fleet = {
        fleet_id = fleet_id,
        fleet_level = fleet_level,
        vessels = vesselsType,
        sex = sex,
        adjust_info = {},
        spell_id = self.curFlagshipSkill[self.curFormation]
      },
      adjutant = {
        fleet_id = 0,
        fleet_level = 0,
        vessels = 1,
        sex = 1,
        adjust_info = {},
        spell_id = 0
      }
    },
    pos = formationIdx
  }
  local isSuccesse = self:CheckCurSlotHasFleet(formationIdx, formationDetail)
  self:ReFreshSelectableHeroList()
  self:ReFreshSelectableAdjutantList()
  self:RefreshFleetPool()
  self:RefreshFormation()
  if isSuccesse < 0 then
    if isSuccesse == -2 then
      GameTip:Show(GameLoader:GetGameText("LC_MENU_LOADING_HINT_9"), 3000)
    end
    if isSuccesse == -1 then
      GameTip:Show(GameLoader:GetGameText("LC_MENU_NOTICE_EMPIRE_COLOSSEUM_MAX"), 3000)
    end
    return
  end
  self.isFormationModify = true
  local flashObj = self:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "addFleetToFormation", formationIdx, detail.avatar, detail.rankIcon, detail.vessels_type, detail.adjutant, detail.bgColor)
  end
end
function GameUIFairArenaFormation:SwitchHeroAdjutant(adjutantInfo, formationIdx)
  DebugOut("GameUIFairArenaFormation:SwitchHeroAdjutant", formationIdx)
  local formationDetail = self:GetCurFormationDetail(formationIdx)
  local formationHeroVesselsType = formationDetail.fleet_info.fleet.vessels
  for _, v in ipairs(adjutantInfo.adjust_info) do
    if v == formationHeroVesselsType then
      formationDetail.fleet_info.adjutant = adjutantInfo
      break
    end
  end
end
function GameUIFairArenaFormation:copyAdjutantTable(dest, source)
  dest.fleet_id = source.fleet_id
  dest.fleet_level = source.fleet_level
  dest.sex = source.sex
  dest.adjust_info = source.adjust_info
  dest.vessels = source.vessels
  dest.spell_id = source.spell_id
end
function GameUIFairArenaFormation:SelectedAdjutant(fleet_id, fleet_level, sex, formationIdx)
  DebugOut("GameUIFairArenaFormation:SetAdjutantInFormation ", fleet_id, fleet_level, formationIdx, self.pressFormationSlot)
  local formationDetail
  for _, v in ipairs(self.Formations[self.curFormation]) do
    if v.pos == formationIdx then
      formationDetail = v
      break
    end
  end
  DebugTable(formationDetail)
  if formationDetail == nil then
    return
  else
    local adjutantInfo = self:GetAdjutantFormAllPool(fleet_id)
    if adjutantInfo then
      if self.pressFormationSlot > 0 then
        self:SwitchHeroAdjutant(formationDetail.fleet_info.adjutant, self.pressFormationSlot)
      end
      formationDetail.fleet_info.adjutant = {}
      self:copyAdjutantTable(formationDetail.fleet_info.adjutant, adjutantInfo)
      if formationDetail.fleet_info.fleet.vessels == 6 then
        self:ChangedFlagshipAdjutant(adjutantInfo)
      end
    else
      assert(" adjutantInfo == nil")
    end
  end
  DebugTable(self.Formations[self.curFormation])
  local adjutantAbility = GameDataAccessHelper:GetCommanderAbility(fleet_id, fleet_level)
  local adjutant = {}
  if adjutantAbility ~= nil then
    adjutant.avatar = GameDataAccessHelper:GetFleetAvatar(fleet_id, fleet_level, sex)
    adjutant.bgColor = adjutantAbility.COLOR
  end
  self:ReFreshSelectableHeroList()
  self:ReFreshSelectableAdjutantList()
  self:RefreshFleetPool()
  self:RefreshFormation()
  self.isFormationModify = true
  local flashObj = self:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "addAdjutantToFleet", formationIdx, adjutant)
  end
end
function GameUIFairArenaFormation:CheckCurSlotHasFleet(formationIdx, formationDetail)
  if formationDetail.fleet_info.fleet.vessels == 6 then
    self:ChangedFlagship(formationDetail)
  end
  local formationFleets = {}
  formationFleets = GameUIFairArenaFormation.Formations[GameUIFairArenaFormation.curFormation]
  local formationLength = #formationFleets
  for k, v in ipairs(formationFleets) do
    if v.pos == formationIdx then
      if v.fleet_info.fleet.vessels == 6 then
        return -2
      end
      formationFleets[k] = formationDetail
      return 1
    end
  end
  if formationLength == fleetCountLimit and formationDetail.fleet_info.fleet.vessels ~= 6 then
    return -1
  end
  table.insert(GameUIFairArenaFormation.Formations[GameUIFairArenaFormation.curFormation], formationDetail)
  DebugOutPutTable(self.Formations, "rest 111")
  return 1
end
function GameUIFairArenaFormation:ChangedFlagship(formationDetail)
  self.curFlagshipDetail.fleetInfo = formationDetail.fleet_info
  local flagShipId = self.curFlagshipDetail.fleetInfo.fleet.fleet_id
  DebugOut("GameUIFairArenaFormation:ChangedFlagship", flagShipId)
  DebugTable(formationDetail)
  for i = 1, 3 do
    if self.Formations[i] and #self.Formations[i] > 0 then
      for k, v in ipairs(self.Formations[i]) do
        local hasFlagShip = false
        if v.fleet_info.fleet.vessels == 6 then
          hasFlagShip = true
          if v.fleet_info.fleet.fleet_id ~= flagShipId then
            if i == self.curFormation then
              table.remove(self.Formations[i], k)
            else
              v.fleet_info = self.curFlagshipDetail.fleetInfo
              v.fleet_info.fleet.spell_id = self.curFlagshipSkill[i]
            end
          end
        end
      end
    else
      self.Formations[i] = {}
      table.insert(self.Formations[i], formationDetail)
    end
  end
  DebugOutPutTable(self.Formations, "cur formations j")
end
function GameUIFairArenaFormation:ChangedFlagshipAdjutant(adjutantInfo)
  DebugOut("ChangedFlagshipAdjutant")
  DebugTable(adjutantInfo)
  for i = 1, 3 do
    for k, v in ipairs(self.Formations[i]) do
      if v.fleet_info.fleet.vessels == 6 then
        self:copyAdjutantTable(v.fleet_info.adjutant, adjutantInfo)
      end
    end
  end
end
function GameUIFairArenaFormation:ReFreshSelectableHeroList()
  DebugOut(" GameUIFairArenaFormation:ReFreshSelectableHeroList")
  GameUIFairArenaFormation.selectable_hero = {}
  for _, v in ipairs(GameUIFairArenaFormation.all_fleets.hero_list) do
    if not self:isFleetInFormation(v.fleet_id) then
      table.insert(GameUIFairArenaFormation.selectable_hero, v)
    end
  end
end
function GameUIFairArenaFormation:isFleetInFormation(fleetId)
  for i = 1, 3 do
    if GameUIFairArenaFormation.Formations[i] then
      for _, v in ipairs(GameUIFairArenaFormation.Formations[i]) do
        if v.fleet_info.fleet.fleet_id == fleetId then
          return true
        end
      end
    end
  end
  return nil
end
function GameUIFairArenaFormation:ReFreshSelectableAdjutantList()
  GameUIFairArenaFormation.selectable_adjutant = {}
  for _, v in ipairs(GameUIFairArenaFormation.all_fleets.adjutant_list) do
    if not self:isAdjutantInFormation(v.fleet_id) then
      table.insert(GameUIFairArenaFormation.selectable_adjutant, v)
    end
  end
  DebugOut("ReFreshSelectableAdjutantList")
  DebugTable(GameUIFairArenaFormation.selectable_adjutant)
end
function GameUIFairArenaFormation:isAdjutantInFormation(fleetId)
  for i = 1, 3 do
    if GameUIFairArenaFormation.Formations[i] then
      for _, v in ipairs(GameUIFairArenaFormation.Formations[i]) do
        if v.fleet_info.adjutant.fleet_id == fleetId then
          return true
        end
      end
    end
  end
  return nil
end
function GameUIFairArenaFormation:ShowFleetDetail(fleetInfo, isHero)
  DebugOut("GameUIFairArenaFormation:ShowFleetDetail ", isHero)
  DebugTable(fleetInfo)
  self.isShowAdjutantDetail = not isHero
  if GameUIFairArenaFormation.mFleetInfos[fleetInfo.fleet_id] then
    ItemBox:ShowCommanderDetail2(fleetInfo.fleet_id, GameUIFairArenaFormation.mFleetInfos[fleetInfo.fleet_id], nil, self.isShowAdjutantDetail)
  else
    GameUIFairArenaFormation:RequestFleetInfo(fleetInfo.fleet_id, fleetInfo.fleet_level)
  end
end
function GameUIFairArenaFormation:RequestFleetInfo(fleet_id, fleet_level)
  local req = {
    fleet_id = tonumber(fleet_id),
    level = tonumber(fleet_level),
    type = 1,
    req_type = 0
  }
  NetMessageMgr:SendMsg(NetAPIList.fair_arena_fleet_info_req.Code, req, GameUIFairArenaFormation.RequestFleetInfoCallback, true, nil)
end
function GameUIFairArenaFormation.RequestFleetInfoCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.fair_arena_fleet_info_req.Code then
    DebugOut("GameUIFairArenaFormation.RequestFleetInfoCallback error")
    DebugTable(content)
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.fair_arena_fleet_info_ack.Code then
    DebugOut("GameUIFairArenaFormation.RequestFleetInfoCallback ok")
    DebugTable(content)
    GameUIFairArenaFormation.mFleetInfos[content.fleet_id] = content.fleet_info.fleets[1]
    GameUIFairArenaFormation.mFleetInfos[content.fleet_id].showLevel = content.fleet_level
    ItemBox:ShowCommanderDetail2(content.fleet_id, GameUIFairArenaFormation.mFleetInfos[content.fleet_id], nil, GameUIFairArenaFormation.isShowAdjutantDetail)
    return true
  end
  return false
end
function GameUIFairArenaFormation:Update(dt)
  local flashObj = GameUIFairArenaFormation:GetFlashObject()
  if flashObj then
    flashObj:Update(dt)
    flashObj:InvokeASCallback("_root", "OnUpdateFrame", dt)
  end
end
if AutoUpdate.isAndroidDevice then
  function GameUIFairArenaFormation.OnAndroidBack()
    GameUIFairArenaFormation:OnFSCommand("close", "")
  end
end
