local ChapterInfos = GameData.chapterdata_act5.ChapterInfos
ChapterInfos[1] = {
  ChaperID = 1,
  ICON = "icon1",
  MapIndex = 1,
  BossPos = 5,
  EntroStory = {5101, 5102},
  AFTER_FINISH = {
    5106,
    5107,
    5108
  }
}
ChapterInfos[2] = {
  ChaperID = 2,
  ICON = "icon2",
  MapIndex = 2,
  BossPos = 10,
  EntroStory = {5201},
  AFTER_FINISH = {5208, 5209}
}
ChapterInfos[3] = {
  ChaperID = 3,
  ICON = "icon30",
  MapIndex = 3,
  BossPos = 16,
  EntroStory = {5301},
  AFTER_FINISH = {}
}
ChapterInfos[4] = {
  ChaperID = 4,
  ICON = "icon400",
  MapIndex = 1,
  BossPos = 18,
  EntroStory = {5401},
  AFTER_FINISH = {
    5403,
    5404,
    5405,
    5406,
    5407,
    5408,
    5409
  }
}
ChapterInfos[5] = {
  ChaperID = 5,
  ICON = "icon5",
  MapIndex = 1,
  BossPos = 18,
  EntroStory = {5501},
  AFTER_FINISH = {
    5506,
    5507,
    5508
  }
}
ChapterInfos[6] = {
  ChaperID = 6,
  ICON = "icon400",
  MapIndex = 1,
  BossPos = 18,
  EntroStory = {5601},
  AFTER_FINISH = {5607, 5608}
}
ChapterInfos[7] = {
  ChaperID = 7,
  ICON = "icon6",
  MapIndex = 1,
  BossPos = 18,
  EntroStory = {5701, 5702},
  AFTER_FINISH = {5710}
}
ChapterInfos[8] = {
  ChaperID = 8,
  ICON = "icon400",
  MapIndex = 1,
  BossPos = 18,
  EntroStory = {5801},
  AFTER_FINISH = {
    5804,
    5805,
    5806,
    5807,
    5808
  }
}
ChapterInfos[9] = {
  ChaperID = 9,
  ICON = "icon5",
  MapIndex = 1,
  BossPos = 18,
  EntroStory = {},
  AFTER_FINISH = {}
}
ChapterInfos[10] = {
  ChaperID = 10,
  ICON = "icon5",
  MapIndex = 1,
  BossPos = 18,
  EntroStory = {},
  AFTER_FINISH = {}
}
