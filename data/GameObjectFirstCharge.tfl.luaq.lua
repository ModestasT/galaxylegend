local GameObjectFirstCharge = LuaObjectManager:GetLuaObject("GameObjectFirstCharge")
local GameUIBarLeft = LuaObjectManager:GetLuaObject("GameUIBarLeft")
local GameVip = LuaObjectManager:GetLuaObject("GameVip")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local FirstChargeState = TutorialQuestManager.FirstChargeState
local GameUIKrypton = LuaObjectManager:GetLuaObject("GameUIKrypton")
function GameObjectFirstCharge:OnAddToGameState(game_state)
  if not self:GetFlashObject() then
    self:LoadFlashObject()
    if immanentversion == 2 then
      local firstChargeText = GameLoader:GetGameText("LC_MENU_NEW_FIRSTCHARGE_DESC")
      self:GetFlashObject():InvokeASCallback("_root", "setFirstChargeText", firstChargeText)
    else
      local firstChargeText = GameLoader:GetGameText("LC_MENU_FIRSTCHARGE_DESC")
      self:GetFlashObject():InvokeASCallback("_root", "setFirstChargeText", firstChargeText)
    end
  end
  GameObjectFirstCharge:MoveIn()
end
function GameObjectFirstCharge:OnEraseFromGameState(game_state)
  GameObjectFirstCharge:UnloadFlashObject()
end
function GameObjectFirstCharge:MoveIn()
  local charge_state = GameObjectFirstCharge.first_purchase.status
  GameObjectFirstCharge:UpdateData(false)
  GameObjectFirstCharge:GetFlashObject():InvokeASCallback("_root", "MoveIn", charge_state)
end
function GameObjectFirstCharge:MoveOut()
  GameObjectFirstCharge:GetFlashObject():InvokeASCallback("_root", "MoveOut")
end
function GameObjectFirstCharge:UpdateData(is_FirstCharge)
  local first_purchase = self.first_purchase
  if not first_purchase then
    return
  end
  local items_param_table = {}
  local beforeCharge_award = {}
  local beforeCharge_krypton
  for i, v in ipairs(first_purchase.items) do
    if v.item_type ~= "fleet" then
      local itemName, itemCount = GameHelper:getAwardNameTextAndCount(v.item_type, v.number, v.no)
      local icon = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(v, nil, nil)
      table.insert(items_param_table, itemName .. "\001" .. " x " .. itemCount .. "\001" .. icon .. "\002")
      if "krypton" == v.item_type then
        beforeCharge_krypton = {}
        beforeCharge_krypton.fr = icon
        beforeCharge_krypton.itemName = itemName
        beforeCharge_krypton.itemCount = " x " .. itemCount
        GameUIKrypton:TryQueryKryptonDetail(v.number, v.level, GameObjectFirstCharge.GetKryptonDetail)
      else
        table.insert(beforeCharge_award, itemName .. "\001" .. " x " .. itemCount .. "\001" .. icon .. "\002")
      end
    else
      local req_content = {
        fleet_id = v.number,
        level = 0,
        type = 0,
        req_type = 0
      }
      NetMessageMgr:SendMsg(NetAPIList.fleet_info_req.Code, req_content, GameObjectFirstCharge.GetFleetInfo, true, nil)
    end
  end
  local creditName = GameLoader:GetGameText("LC_MENU_LOOT_CREDIT")
  local creditDouble = GameLoader:GetGameText("LC_MENU_DOUBLE_BUTTON")
  table.insert(items_param_table, 1, creditName .. "\001" .. creditDouble .. "\001" .. "item_2607" .. "\002")
  if LuaUtils:table_size(first_purchase.items) < 5 then
    for i = LuaUtils:table_size(first_purchase.items), 5 do
      table.insert(items_param_table, GameLoader:GetGameText("LC_MENU_EMPTY") .. "\001" .. 0 .. "\001" .. "empty" .. "\002")
      table.insert(beforeCharge_award, GameLoader:GetGameText("LC_MENU_EMPTY") .. "\001" .. 0 .. "\001" .. "empty" .. "\002")
    end
  end
  local items_param = table.concat(items_param_table)
  if is_FirstCharge then
    if not GameStateManager:GetCurrentGameState():IsObjectInState(self) then
      GameStateManager:GetCurrentGameState():AddObject(self)
    else
      GameObjectFirstCharge:UpdateReceivedData(GameGlobalData:GetData("vipinfo").level, items_param)
      GameObjectFirstCharge:GetFlashObject():InvokeASCallback("_root", "ShowCongratulations")
    end
  end
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    local beforeCharge_items = table.concat(beforeCharge_award)
    GameObjectFirstCharge:GetFlashObject():InvokeASCallback("_root", "UpdateData", beforeCharge_items, beforeCharge_krypton)
    if first_purchase.status == 1 then
      GameObjectFirstCharge:UpdateReceivedData(GameGlobalData:GetData("vipinfo").level, items_param)
    end
  end
end
function GameObjectFirstCharge.GetFleetInfo(msgType, content)
  DebugOut("GameObjectFirstCharge.GetFleetInfo")
  DebugOut(msgType)
  DebugTable(content)
  if msgType == NetAPIList.fleet_info_ack.Code then
    if GameObjectFirstCharge:GetFlashObject() then
      local forceText = "<font color='#D996F4'>" .. GameLoader:GetGameText("LC_MENU_FORCE_CHAR") .. "</font>" .. " " .. "<font color='#FEF4FF'>" .. tostring(content.fleet_info.fleets[1].force) .. "</font>"
      GameObjectFirstCharge:GetFlashObject():InvokeASCallback("_root", "setFleetForce", forceText)
    end
    return true
  end
  return false
end
function GameObjectFirstCharge.GetKryptonDetail(msgType, content)
  DebugStore("GameObjectFirstCharge.GetKryptonDetail")
  if msgType == NetAPIList.krypton_info_ack.Code then
    DebugTable(content)
    local typeID = content.krypton.addon[1].type
    local typeText = GameLoader:GetGameText("LC_MENU_Equip_param_" .. typeID)
    local info = typeText .. " + " .. content.krypton.addon[1].value
    if GameObjectFirstCharge:GetFlashObject() then
      GameObjectFirstCharge:GetFlashObject():InvokeASCallback("_root", "setKryptonInfo", info)
    end
    return true
  end
  return false
end
function GameObjectFirstCharge:UpdateReceivedData(vip_level, items_param)
  GameObjectFirstCharge:GetFlashObject():InvokeASCallback("_root", "UpdateReceivedData", items_param)
  local vip_title = "VIP " .. vip_level .. " :"
  local vip_content_1 = GameLoader:GetGameText("LC_MENU_VIP_" .. vip_level - 1 .. "_1")
  local vip_content_2 = GameLoader:GetGameText("LC_MENU_VIP_" .. vip_level - 1 .. "_2")
  DebugOut("vip_content_1 = ", vip_content_1)
  DebugOut("vip_content_2 = ", vip_content_1)
  GameObjectFirstCharge:GetFlashObject():InvokeASCallback("_root", "setCongratulationText", vip_title, vip_content_1, vip_content_2)
  return
end
function GameObjectFirstCharge:ShowCommanderInfo(identity)
  local basic_info = GameDataAccessHelper:GetCommanderBasicInfo(identity)
  local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
  local data_table = {}
  data_table.name = GameLoader:GetGameText("LC_NPC_" .. GameDataAccessHelper:GetCommanderName(identity))
  data_table.vessels_type = GameDataAccessHelper:GetCommanderVesselsType(identity)
  data_table.vessels_name = GameLoader:GetGameText("LC_FLEET_" .. data_table.vessels_type)
  data_table.avatar = GameDataAccessHelper:GetFleetAvatar(identity)
  data_table.commander_type = identity < 100 and "commander_normal" or "commander_advanced"
  data_table.skill_name = GameDataAccessHelper:GetSkillNameText(basic_info.SPELL_ID)
  data_table.skill_rangetype = GameDataAccessHelper:GetSkillRangeType(basic_info.SPELL_ID)
  data_table.skill_rangetype_name = GameLoader:GetGameText("LC_MENU_SKILL_RANGE_TYPE_" .. data_table.skill_rangetype)
  data_table.skill_desc = GameDataAccessHelper:GetSkillDesc(basic_info.SPELL_ID)
  data_table.identity = identity
  local pos_string = "-1,-1"
  DebugOut("pos_string:", pos_string)
  local pos_table = LuaUtils:string_split(pos_string, ",")
  data_table.pos_x = tonumber(pos_table[1])
  data_table.pos_y = tonumber(pos_table[2])
  DebugOutPutTable(commander_data, "commander_data")
  DebugOutPutTable(data_table, "data_table")
  ItemBox:ShowCommanderInfo(data_table)
end
function GameObjectFirstCharge:OnFSCommand(cmd, arg)
  if cmd == "ShowCardDetail" then
    local commander_id = -1
    for i, v in ipairs(self.first_purchase.items) do
      if v.item_type == "fleet" then
        commander_id = v.number
      end
    end
    local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
    ItemBox:ShowCommanderDetail2(commander_id)
  elseif cmd == "clickCharge" then
    GameObjectFirstCharge.goClickCharge = true
  elseif cmd == "MoveOutEnd" then
    GameStateManager:GetCurrentGameState():EraseObject(self)
    if GameObjectFirstCharge.goClickCharge then
      GameObjectFirstCharge.goClickCharge = false
      local function closeCallback()
        GameStateManager:GetCurrentGameState():AddObject(GameObjectFirstCharge)
      end
      GameVip.backToFirstCharge = closeCallback
      GameVip:showVip(false)
    end
  elseif "vip_detail" == cmd then
    GameStateManager:GetCurrentGameState():EraseObject(self)
    local function closeCallback()
      GameStateManager:GetCurrentGameState():AddObject(GameObjectFirstCharge)
    end
    GameVip.backToFirstCharge = closeCallback
    GameVip:showVip(true)
  elseif "ShowPaywall" == cmd then
    GameStateManager:GetCurrentGameState():EraseObject(self)
    local function closeCallback()
      GameStateManager:GetCurrentGameState():AddObject(GameObjectFirstCharge)
    end
    GameVip.backToFirstCharge = closeCallback
    GameVip:showVip(false)
  end
end
function GameObjectFirstCharge.CheckShowFirstCharge()
  if immanentversion == 2 then
    local levelInfo = GameGlobalData:GetData("levelinfo")
    local userLevel = levelInfo.level
    local staus = GameObjectFirstCharge.first_purchase.status
    GameUIBarLeft:UpdateFirstChargeStatus(GameObjectFirstCharge.IsShowFirstCharge(userLevel, staus))
  end
end
function GameObjectFirstCharge.IsShowFirstCharge(userLevel, status)
  if immanentversion == 1 then
    return userLevel and userLevel >= 5 and status == 0
  elseif immanentversion == 2 then
    return userLevel and userLevel >= 5 and status == 0
  end
end
function GameObjectFirstCharge.UpdateFirstCharge(content)
  DebugOut("GameObjectFirstCharge.UpdateFirstCharge")
  DebugTable(content)
  local is_FirstCharge = false
  if GameObjectFirstCharge.first_purchase and GameObjectFirstCharge.first_purchase.status == 0 and content.status == 1 then
    is_FirstCharge = true
  end
  if content.status == 1 and not FirstChargeState:IsFinished() then
    DebugOut("record first charge")
    FirstChargeState:SetFinish(true)
    local firstChargeInfo = {}
    firstChargeInfo.Type = "FirstChargeSucess"
    firstChargeInfo.Description = "First_Charge"
    firstChargeInfo.Price = "USD " .. (content.cost or 0)
    GameUtils:SafeRecordGameInfo(firstChargeInfo)
  end
  GameUIBarLeft:UpdateFirstChargeStatus(GameObjectFirstCharge.IsShowFirstCharge(GameUIBarLeft.currentUserLevel, content.status))
  GameObjectFirstCharge.first_purchase = content
  GameVip:SetFirstChageData(content.status)
  local GameVipDetailInfoPanel = LuaObjectManager:GetLuaObject("GameVipDetailInfoPanel")
  if is_FirstCharge then
    local function exitCallback()
      DebugOut("GameVipDetailInfoPanel.mExitCallback")
      GameObjectFirstCharge:UpdateData(is_FirstCharge)
    end
    GameVipDetailInfoPanel.mExitCallback = exitCallback
  else
    GameVipDetailInfoPanel.mExitCallback = nil
    GameObjectFirstCharge:UpdateData(is_FirstCharge)
  end
end
if AutoUpdate.isAndroidDevice then
  function GameObjectFirstCharge.OnAndroidBack()
    GameObjectFirstCharge:MoveOut()
  end
end
