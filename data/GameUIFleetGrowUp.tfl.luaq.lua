require("FleetAvatarEnhanceData.tfl")
local GameStateEquipEnhance = GameStateManager.GameStateEquipEnhance
local GameUIFleetGrowUp = LuaObjectManager:GetLuaObject("GameUIFleetGrowUp")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameUIWDStuff = LuaObjectManager:GetLuaObject("GameUIWDStuff")
local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
local QuestTutorialFirstReform = TutorialQuestManager.QuestTutorialFirstReform
local E_UI_STATE = {
  UISTATE_NONE = 0,
  UISTATE_CARD = 1,
  UISTATE_MOVING_TO_DETAIL = 2,
  UISTATE_DETAIL = 3,
  UISTATE_MOVING_TO_CARD = 4
}
local CommanderRanks = {
  [1] = "rank_E",
  [2] = "rank_D",
  [3] = "rank_C",
  [4] = "rank_B",
  [5] = "rank_A",
  [6] = "rank_S",
  [7] = "rank_SS"
}
GameUIFleetGrowUp.mCurrentFleetID = -1
GameUIFleetGrowUp.mCurrentAvatarLevel = -1
GameUIFleetGrowUp.mCurrentFleetEnhanceData = nil
GameUIFleetGrowUp.mCurrentUIState = E_UI_STATE.UISTATE_NONE
function GameUIFleetGrowUp:Init(fleetID)
  GameUIFleetGrowUp.mCurrentFleetID = fleetID
  GameUIFleetGrowUp.mCurrentFleetEnhanceData = FleetAvatarEnhanceData.new(fleetID)
end
function GameUIFleetGrowUp:Clear()
  GameUIFleetGrowUp.mCurrentFleetEnhanceData = nil
  GameUIFleetGrowUp.mCurrentFleetID = -1
  GameUIFleetGrowUp.mCurrentUIState = E_UI_STATE.UISTATE_NONE
end
function GameUIFleetGrowUp:OnAddToGameState()
end
function GameUIFleetGrowUp:OnEraseFromGameState()
  self:Clear()
  self:UnloadFlashObject()
end
function GameUIFleetGrowUp:CheckDownloadImage()
  if (ext.crc32.crc32("data2/LAZY_LOAD_newEnhance_bg.png") == "" or ext.crc32.crc32("data2/LAZY_LOAD_newEnhance_bg.png") == nil) and not AutoUpdateInBackground:IsHeroFileUpdatedToServer("data2/LAZY_LOAD_newEnhance_bg.png") then
    self:GetFlashObject():ReplaceTexture("LAZY_LOAD_newEnhance_bg.png", "territorial_map_bg.png")
  end
end
function GameUIFleetGrowUp:Show()
  GameUIFleetGrowUp.mCurrentUIState = E_UI_STATE.UISTATE_CARD
  GameUIFleetGrowUp:RequestAvatarEnhanceInfo(GameUIFleetGrowUp.mCurrentFleetID, true, GameUIFleetGrowUp.RequestAvatarEnhanceInfoCallback)
end
function GameUIFleetGrowUp:Hide()
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "moveOut")
  end
end
function GameUIFleetGrowUp:RequestAvatarEnhanceInfo(fleetID, isUp, callback)
  local content = {hero_id = fleetID, is_levelup = isUp}
  NetMessageMgr:SendMsg(NetAPIList.hero_level_attr_diff_req.Code, content, callback, true, nil)
end
function GameUIFleetGrowUp.RequestAvatarEnhanceInfoCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.hero_level_attr_diff_req.Code then
    return true
  elseif msgType == NetAPIList.hero_level_attr_diff_ack.Code then
    if not GameUIFleetGrowUp:GetFlashObject() then
      GameUIFleetGrowUp:LoadFlashObject()
      GameUIFleetGrowUp:CheckDownloadImage()
    end
    local lang = "en"
    if GameSettingData and GameSettingData.Save_Lang then
      lang = GameSettingData.Save_Lang
      if string.find(lang, "ru") == 1 then
        lang = "ru"
      end
    end
    GameUIFleetGrowUp:GetFlashObject():InvokeASCallback("_root", "moveIn", lang)
    GameUIFleetGrowUp.mCurrentFleetEnhanceData:GenerateAvatarCardByServerData(content)
    GameUIFleetGrowUp.mCurrentFleetEnhanceData:GenerateAvatarAttriDataByServerData(content)
    GameUIFleetGrowUp.mCurrentFleetEnhanceData:SetGrowUpRequireList(content.conditions)
    GameUIFleetGrowUp:SetUpMenu()
    GameUIFleetGrowUp:SetUpFleetCard()
    GameUIFleetGrowUp:RefreshDetailDiffList()
    GameUIFleetGrowUp:SetGrowUpLevelRequrie()
    GameUIFleetGrowUp.mMaxLevel = content.allow_max
    return true
  end
  return false
end
function GameUIFleetGrowUp:RequestLevelUp(fleetID)
  local content = {fleet_id = fleetID}
  NetMessageMgr:SendMsg(NetAPIList.fleet_enhance_req.Code, content, self.RequestLevelUpCallback, true, nil)
  GameUtils:RecordForTongdui(NetAPIList.fleet_enhance_req.Code, content, self.RequestLevelUpCallback, true, nil)
end
function GameUIFleetGrowUp.RequestLevelUpCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.fleet_enhance_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      if content.code == 914 then
        GameUIFleetGrowUp:SetRequrieItems()
        local probability, probabilityStep = GameUIFleetGrowUp.mCurrentFleetEnhanceData:GetGrowupProbability()
        local newProbability = probability + probabilityStep
        if newProbability > 100 then
          newProbability = 100
        end
        GameUIFleetGrowUp.mCurrentFleetEnhanceData:SetGrowupProbability(newProbability)
        GameUIFleetGrowUp:SetRequriePercent()
      end
    else
      local fleetList = {}
      local showParam = {}
      showParam.fleet_id = GameUIFleetGrowUp.mCurrentFleetID
      showParam.level = -1
      fleetList[1] = showParam
      GameUIFleetGrowUp:ShowLevelUpSuccessMenu()
      GameUIFleetGrowUp:SetUpSuccessMenuCard()
      GameUIFleetGrowUp:RefreshSuccessDetailList()
    end
    return true
  end
  return false
end
function GameUIFleetGrowUp:RequestLevelDown(fleetID)
  local content = {fleet_id = fleetID}
  NetMessageMgr:SendMsg(NetAPIList.fleet_weaken_req.Code, content, self.RequestLevelDownCallback, true, nil)
end
function GameUIFleetGrowUp.RequestLevelDownCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.fleet_weaken_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    else
      local fleetList = {}
      local showParam = {}
      showParam.fleet_id = GameUIFleetGrowUp.mCurrentFleetID
      showParam.level = -1
      fleetList[1] = showParam
      GameUIFleetGrowUp:ShowRecoverFinished()
    end
    return true
  end
  return false
end
function GameUIFleetGrowUp:ShowFleetAttri()
  local flash = self:GetFlashObject()
  if not flash then
    return
  end
  flash:InvokeASCallback("_root", "showDetail")
end
function GameUIFleetGrowUp:HideFleetAttri()
  local flash = self:GetFlashObject()
  if not flash then
    return
  end
  flash:InvokeASCallback("_root", "hideDetail")
end
function GameUIFleetGrowUp:SetGrowUpLevelRequrie()
  local flash = self:GetFlashObject()
  if not flash then
    return
  end
  local levelStr = GameLoader:GetGameText("LC_MENU_REFORMING_LV_REQUIR_LABEL")
  if self:IsMaxLevel(self.mCurrentFleetEnhanceData.mCurLevel) then
    levelStr = GameLoader:GetGameText("LC_MENU_REFORMING_LV_MAX_DESC")
    flash:InvokeASCallback("_root", "setLevelRequrie", levelStr, false)
    return
  end
  local levelReurie = GameUIFleetGrowUp.mCurrentFleetEnhanceData:GetGrowupLevelRequire()
  local level_info = GameGlobalData:GetData("levelinfo")
  local isLevelReach = false
  if levelReurie <= level_info.level then
    isLevelReach = true
  end
  levelStr = levelStr .. ": " .. GameUIFleetGrowUp.mCurrentFleetEnhanceData:GetGrowupLevelRequire()
  flash:InvokeASCallback("_root", "setLevelRequrie", levelStr, isLevelReach)
end
function GameUIFleetGrowUp:SetUpFleetCard()
  DebugOut("set up fleet")
  local flash = self:GetFlashObject()
  if not flash then
    return
  end
  local fleetCard = GameUIFleetGrowUp.mCurrentFleetEnhanceData:GetFleetCardData()
  local curCard = fleetCard.mCurFleetCard
  DebugOut("SetUpFleetCard :")
  DebugTable(curCard)
  local colorNum = FleetDataAccessHelper:GetFleetColorNumberByColor(curCard.color)
  local levelStr = "<font color='" .. colorNum .. "'>" .. "[" .. "</font>" .. "+" .. curCard.level .. "<font color='" .. colorNum .. "'>" .. "]" .. "</font>"
  local vesselFrame = FleetDataAccessHelper:GetFleetVesselsFrameByVessel(curCard.vessels)
  local vesselStr = FleetDataAccessHelper:GetFleetVesselsTypeStringByVessel(curCard.vessels)
  local colorFrame = FleetDataAccessHelper:GetFleetColorFrameByColor(curCard.color)
  local spellFrame = FleetDataAccessHelper:GetFleetSpellFrameBySpell(curCard.spell_id)
  local avatarName = FleetDataAccessHelper:GetFleetColorDisplayName(curCard.name, curCard.fleet_id, curCard.color)
  local avatarFrame = FleetDataAccessHelper:GetFleetAvatarFrameByAvatar(curCard.avatar, curCard.fleet_id, curCard.level)
  local avatarShip = GameDataAccessHelper:GetShip(curCard.fleet_id, nil, curCard.level)
  local leaderlist = GameGlobalData:GetData("leaderlist")
  local curMatrixIndex = GameGlobalData.GlobalData.curMatrixIndex
  if curCard.fleet_id == 1 and leaderlist and curMatrixIndex then
    avatarShip = GameDataAccessHelper:GetShip(leaderlist.leader_ids[curMatrixIndex], nil, curCard.level)
  end
  local sexl = GameDataAccessHelper:GetCommanderSex(curCard.fleet_id)
  if sexl == 1 then
    sexl = "man"
  elseif sexl == 0 then
    sexl = "woman"
  else
    sexl = "unknown"
  end
  flash:InvokeASCallback("_root", "setUpOriginalFleetCard", levelStr, spellFrame, vesselStr, vesselFrame, colorFrame, avatarFrame, avatarName, avatarShip, GameUtils:GetFleetRankFrame(curCard.rank, GameUIFleetGrowUp.DownloadRankImageCallback), curCard.damage_assess, curCard.damage_rate, curCard.defence_assess, curCard.defence_rate, curCard.assist_assess, curCard.assist_rate, sexl)
  local originalCard = curCard
  curCard = fleetCard.mNextFleetCard
  local colorNum = FleetDataAccessHelper:GetFleetColorNumberByColor(curCard.color)
  local levelStr = "<font color='" .. colorNum .. "'>" .. "[" .. "</font>" .. "+" .. curCard.level .. "<font color='" .. colorNum .. "'>" .. "]" .. "</font>"
  vesselFrame = FleetDataAccessHelper:GetFleetVesselsFrameByVessel(curCard.vessels)
  vesselStr = FleetDataAccessHelper:GetFleetVesselsTypeStringByVessel(curCard.vessels)
  colorFrame = FleetDataAccessHelper:GetFleetColorFrameByColor(curCard.color)
  spellFrame = FleetDataAccessHelper:GetFleetSpellFrameBySpell(curCard.spell_id)
  avatarName = FleetDataAccessHelper:GetFleetColorDisplayName(curCard.name, curCard.fleet_id, curCard.color)
  avatarFrame = FleetDataAccessHelper:GetFleetAvatarFrameByAvatar(curCard.avatar, curCard.fleet_id, curCard.level)
  avatarShip = GameDataAccessHelper:GetShip(curCard.fleet_id, nil, curCard.level)
  if curCard.fleet_id == 1 and leaderlist and curMatrixIndex then
    avatarShip = GameDataAccessHelper:GetShip(leaderlist.leader_ids[curMatrixIndex], nil, curCard.level)
  end
  local damage_inc = curCard.damage_rate - originalCard.damage_rate
  local defence_inc = curCard.defence_rate - originalCard.defence_rate
  local assist_inc = curCard.assist_rate - originalCard.assist_rate
  local sex = GameDataAccessHelper:GetCommanderSex(curCard.fleet_id)
  if sex == 1 then
    sex = "man"
  elseif sex == 0 then
    sex = "woman"
  else
    sex = "unknown"
  end
  flash:InvokeASCallback("_root", "setUpGrowUpFleetCard", levelStr, spellFrame, vesselStr, vesselFrame, colorFrame, avatarFrame, avatarName, avatarShip, GameUtils:GetFleetRankFrame(curCard.rank, GameUIFleetGrowUp.DownloadGrowUpRankImageCallback), damage_inc, originalCard.damage_rate, defence_inc, originalCard.defence_rate, assist_inc, originalCard.assist_rate, sex)
end
function GameUIFleetGrowUp.DownloadOriginalRankImageCallback(extInfo)
  if GameUIFleetGrowUp:GetFlashObject() then
    GameUIFleetGrowUp:GetFlashObject():InvokeASCallback("_root", "updateOriginalRankImage", extInfo.rank_id)
  end
end
function GameUIFleetGrowUp.DownloadGrowUpRankImageCallback(extInfo)
  if GameUIFleetGrowUp:GetFlashObject() then
    GameUIFleetGrowUp:GetFlashObject():InvokeASCallback("_root", "updateGrowUpRankImage", extInfo.rank_id)
  end
end
function GameUIFleetGrowUp:SetUpMenu()
  local flash = self:GetFlashObject()
  if not flash then
    return
  end
  flash:InvokeASCallback("_root", "setLevelUpButtonEnable", true)
  flash:InvokeASCallback("_root", "setLevelDownButtonEnable", true)
  if self:IsMaxLevel(self.mCurrentFleetEnhanceData.mCurLevel) then
    flash:InvokeASCallback("_root", "setLevelUpButtonEnable", false)
  end
  if self.mCurrentFleetEnhanceData:IsMinLevel() then
    flash:InvokeASCallback("_root", "setLevelDownButtonEnable", false)
  end
  if immanentversion170 == 4 or immanentversion170 == 5 then
    if QuestTutorialFirstReform:IsActive() and GameUIFleetGrowUp.mCurrentFleetID == 1 then
      flash:InvokeASCallback("_root", "ShowReformTutorial", true)
    else
      flash:InvokeASCallback("_root", "ShowReformTutorial", false)
    end
  end
end
function GameUIFleetGrowUp:RefreshDetailDiffList()
  local flash = self:GetFlashObject()
  if not flash then
    return
  end
  flash:InvokeASCallback("_root", "clearDetailDiffRowList")
  flash:InvokeASCallback("_root", "initDetailDiffList")
  local attriData = self.mCurrentFleetEnhanceData:GetFleetAttriDiffData()
  local attriNum = #attriData.mAttriNames
  for i = 1, attriNum do
    flash:InvokeASCallback("_root", "addDetailDiffItem", i)
  end
  flash:InvokeASCallback("_root", "setDetailDiffArrowVisible")
end
function GameUIFleetGrowUp:UpdateDetailInfoItem(id)
  local flash = self:GetFlashObject()
  if not flash then
    return
  end
  local itemIndex = tonumber(id)
  local attriData = self.mCurrentFleetEnhanceData:GetFleetAttriDiffData()
  local itemName = attriData.mAttriNames[itemIndex]
  local itemCurValue = attriData.mCurLevelAttriValue[itemIndex]
  local itemNextValue = attriData.mNextLevelAttriValue[itemIndex]
  if self.mCurrentFleetEnhanceData:IsPercentAttri(attriData.mAttriIDs[itemIndex]) then
    itemCurValue = string.format("%.02f", itemCurValue) .. "%"
    itemNextValue = string.format("%.02f", itemNextValue) .. "%"
  else
    itemCurValue = GameUtils.numberConversion(tonumber(itemCurValue))
    itemNextValue = GameUtils.numberConversion(tonumber(itemNextValue))
  end
  flash:InvokeASCallback("_root", "setDetailDiffItem", itemIndex, itemName, itemCurValue, itemNextValue)
end
function GameUIFleetGrowUp:ShowGrowUpRequrieMenu()
  local flash = self:GetFlashObject()
  if not flash then
    return
  end
  flash:InvokeASCallback("_root", "showRequireMenu")
end
function GameUIFleetGrowUp:HideGrowUpRequrieMenu()
  local flash = self:GetFlashObject()
  if not flash then
    return
  end
  flash:InvokeASCallback("_root", "hideRequireMenu")
end
function GameUIFleetGrowUp:SetRequrieMenuAvatar()
  local flash = self:GetFlashObject()
  if not flash then
    return
  end
  local fleetCard = GameUIFleetGrowUp.mCurrentFleetEnhanceData:GetFleetCardData()
  local curCard = fleetCard.mCurFleetCard
  local colorNum = FleetDataAccessHelper:GetFleetColorNumberByColor(curCard.color)
  local levelStr = "<font color='" .. colorNum .. "'>" .. "[" .. "</font>" .. "+" .. curCard.level .. "<font color='" .. colorNum .. "'>" .. "]" .. "</font>"
  local vesselFrame = FleetDataAccessHelper:GetFleetVesselsFrameByVessel(curCard.vessels)
  local vesselStr = FleetDataAccessHelper:GetFleetVesselsTypeStringByVessel(curCard.vessels)
  local colorFrame = FleetDataAccessHelper:GetFleetColorFrameByColor(curCard.color)
  local avatarName = FleetDataAccessHelper:GetFleetColorDisplayName(curCard.name, curCard.fleet_id, curCard.color)
  local avatarFrame = FleetDataAccessHelper:GetFleetAvatarFrameByAvatar(curCard.avatar, curCard.fleet_id, curCard.level)
  local sexl = GameDataAccessHelper:GetCommanderSex(curCard.fleet_id)
  if sexl == 1 then
    sexl = "man"
  elseif sexl == 0 then
    sexl = "woman"
  else
    sexl = "unknown"
  end
  flash:InvokeASCallback("_root", "setRequireMenuCurAvatar", levelStr, vesselStr, vesselFrame, colorFrame, avatarFrame, avatarName, sexl)
  curCard = fleetCard.mNextFleetCard
  local colorNum = FleetDataAccessHelper:GetFleetColorNumberByColor(curCard.color)
  local levelStr = "<font color='" .. colorNum .. "'>" .. "[" .. "</font>" .. "+" .. curCard.level .. "<font color='" .. colorNum .. "'>" .. "]" .. "</font>"
  vesselFrame = FleetDataAccessHelper:GetFleetVesselsFrameByVessel(curCard.vessels)
  vesselStr = FleetDataAccessHelper:GetFleetVesselsTypeStringByVessel(curCard.vessels)
  colorFrame = FleetDataAccessHelper:GetFleetColorFrameByColor(curCard.color)
  avatarName = FleetDataAccessHelper:GetFleetColorDisplayName(curCard.name, curCard.fleet_id, curCard.color)
  avatarFrame = FleetDataAccessHelper:GetFleetAvatarFrameByAvatar(curCard.avatar, curCard.fleet_id, curCard.level)
  local sexr = GameDataAccessHelper:GetCommanderSex(curCard.fleet_id)
  if sexr == 1 then
    sexr = "man"
  elseif sexr == 0 then
    sexr = "woman"
  else
    sexr = "unknown"
  end
  flash:InvokeASCallback("_root", "setRequireMenuNextAvatar", levelStr, vesselStr, vesselFrame, colorFrame, avatarFrame, avatarName, sexr)
end
function GameUIFleetGrowUp:SetRequrieItems()
  local flash = self:GetFlashObject()
  if not flash then
    return
  end
  local requrieItems = GameUIFleetGrowUp.mCurrentFleetEnhanceData:GetGrowUpRequireList()
  local itemNameList = ""
  local itemRequireNumList = ""
  local itemRealNumList = ""
  local itemIconList = ""
  for i, v in ipairs(requrieItems) do
    local itemName = GameHelper:GetAwardTypeText(v.item_type, v.number)
    local itemRequrieNumber = GameHelper:GetAwardCount(v.item_type, v.number, v.no)
    local itemRealNum = 0
    if GameHelper:IsResource(v.item_type) then
      local resource = GameGlobalData:GetData("resource")
      itemRealNum = resource[v.item_type]
    else
      local items = GameGlobalData:GetData("item_count")
      for iItem, vItem in ipairs(items.items) do
        if vItem.item_id == v.number then
          itemRealNum = vItem.item_no
        end
      end
    end
    DebugOut("itemRequrieNumber = ", itemRequrieNumber)
    DebugOut("item real number = ", itemRealNum)
    if tonumber(itemRealNum) < tonumber(itemRequrieNumber) then
      itemRealNum = "<font color='#FF0000'>" .. GameUtils.numberConversion(itemRealNum) .. "</font>"
    else
      itemRealNum = "<font color='#28D159'>" .. GameUtils.numberConversion(itemRealNum) .. "</font>"
    end
    local itemIcon
    if DynamicResDownloader:IsDynamicStuffByGameItem(v) then
      local fullFileName = DynamicResDownloader:GetFullName(v.number .. ".png", DynamicResDownloader.resType.PIC)
      local localPath = "data2/" .. fullFileName
      DebugWD("localPath = ", localPath)
      if DynamicResDownloader:IfResExsit(localPath) then
        ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
        itemIcon = "item_" .. v.number
      else
        itemIcon = "temp"
        self:AddDownloadPath(v.number, i, true)
      end
    else
      itemIcon = GameHelper:GetAwardTypeIconFrameName(v.item_type, v.number, v.no)
    end
    itemNameList = itemNameList .. itemName .. "\001"
    itemRequireNumList = itemRequireNumList .. GameUtils.numberConversion(itemRequrieNumber) .. "\001"
    itemRealNumList = itemRealNumList .. itemRealNum .. "\001"
    itemIconList = itemIconList .. itemIcon .. "\001"
  end
  flash:InvokeASCallback("_root", "setRequrieItems", itemNameList, itemRealNumList, itemRequireNumList, itemIconList)
end
function GameUIFleetGrowUp:SetRequrieLevelPanel()
  local flash = self:GetFlashObject()
  if not flash then
    return
  end
  local isLevelReach = false
  local levelReurie = GameUIFleetGrowUp.mCurrentFleetEnhanceData:GetGrowupLevelRequire()
  local level_info = GameGlobalData:GetData("levelinfo")
  if levelReurie <= level_info.level then
    isLevelReach = true
  end
  local levelStr = GameLoader:GetGameText("LC_MENU_REFORMING_LV_REQUIR_LABEL")
  levelStr = levelStr .. ": " .. levelReurie
  flash:InvokeASCallback("_root", "setRequrieMenuLevel", levelStr, isLevelReach)
end
function GameUIFleetGrowUp:SetRequriePercent()
  local flash = self:GetFlashObject()
  if not flash then
    return
  end
  local probability, probabilityStep = GameUIFleetGrowUp.mCurrentFleetEnhanceData:GetGrowupProbability()
  if probability >= 100 then
    probability = 100
  end
  local percentStr = probability .. "%"
  if probability <= 0 then
    probability = 0
  elseif probability >= 100 then
    probability = 100
  end
  probability = math.ceil(probability)
  percentStepStr = GameLoader:GetGameText("LC_MENU_REFORM_REQUIRE_ENERGY_DESC")
  percentStepStr = string.format(percentStepStr, math.ceil(probabilityStep) .. " %")
  probability = probability + 1
  if probabilityStep == 0 then
    percentStepStr = ""
  end
  flash:InvokeASCallback("_root", "setRequrieMenuProbability", probability, percentStr, percentStepStr)
end
function GameUIFleetGrowUp:ShowRecoverMenu()
  local flash = self:GetFlashObject()
  if not flash then
    return
  end
  local resourceTxt = GameLoader:GetGameText("LC_MENU_REFORM_RECOVER_RESOURCE_LABEL")
  flash:InvokeASCallback("_root", "showRecoverPanel")
  flash:InvokeASCallback("_root", "setRecoverResourceText", resourceTxt)
end
function GameUIFleetGrowUp:HideRecoverMenu()
  local flash = self:GetFlashObject()
  if not flash then
    return
  end
  flash:InvokeASCallback("_root", "hideRecoverPanel")
end
function GameUIFleetGrowUp:RecoverToDisplayerMenu()
  local flash = self:GetFlashObject()
  if not flash then
    return
  end
  flash:InvokeASCallback("_root", "recoverToDisplay")
end
function GameUIFleetGrowUp:RequestRecoverItems()
  GameUIFleetGrowUp:RequestAvatarEnhanceInfo(GameUIFleetGrowUp.mCurrentFleetID, false, GameUIFleetGrowUp.RequestRecoverItemsCallback)
end
function GameUIFleetGrowUp.RequestRecoverItemsCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.hero_level_attr_diff_req.Code then
    return true
  elseif msgType == NetAPIList.hero_level_attr_diff_ack.Code then
    GameUIFleetGrowUp.mCurrentFleetEnhanceData:SetRecoverList(content.conditions)
    GameUIFleetGrowUp.mCurrentFleetEnhanceData:SetRecoverCard(content)
    GameUIFleetGrowUp:ShowRecoverMenu()
    GameUIFleetGrowUp:SeuUpRecoverCard()
    GameUIFleetGrowUp:RefreshRecoverList()
    return true
  end
  return false
end
function GameUIFleetGrowUp:SeuUpRecoverCard()
  local flash = self:GetFlashObject()
  if not flash then
    return
  end
  local fleetCard = GameUIFleetGrowUp.mCurrentFleetEnhanceData:GetRecoverCard()
  local curCard = fleetCard.mCurFleetCard
  local colorNum = FleetDataAccessHelper:GetFleetColorNumberByColor(curCard.color)
  local levelStr = "<font color='" .. colorNum .. "'>" .. "[" .. "</font>" .. "+" .. curCard.level .. "<font color='" .. colorNum .. "'>" .. "]" .. "</font>"
  local vesselFrame = FleetDataAccessHelper:GetFleetVesselsFrameByVessel(curCard.vessels)
  local vesselStr = FleetDataAccessHelper:GetFleetVesselsTypeStringByVessel(curCard.vessels)
  local colorFrame = FleetDataAccessHelper:GetFleetColorFrameByColor(curCard.color)
  local avatarName = FleetDataAccessHelper:GetFleetColorDisplayName(curCard.name, curCard.fleet_id, curCard.color)
  local avatarFrame = FleetDataAccessHelper:GetFleetAvatarFrameByAvatar(curCard.avatar, curCard.fleet_id, curCard.level)
  local sexl = GameDataAccessHelper:GetCommanderSex(curCard.fleet_id)
  if sexl == 1 then
    sexl = "man"
  elseif sexl == 0 then
    sexl = "woman"
  else
    sexl = "unknown"
  end
  flash:InvokeASCallback("_root", "setRecoverCurCard", levelStr, vesselStr, vesselFrame, colorFrame, avatarFrame, avatarName, sexl)
  curCard = fleetCard.mNextFleetCard
  local colorNum = FleetDataAccessHelper:GetFleetColorNumberByColor(curCard.color)
  local levelStr = "<font color='" .. colorNum .. "'>" .. "[" .. "</font>" .. "+" .. curCard.level .. "<font color='" .. colorNum .. "'>" .. "]" .. "</font>"
  vesselFrame = FleetDataAccessHelper:GetFleetVesselsFrameByVessel(curCard.vessels)
  vesselStr = FleetDataAccessHelper:GetFleetVesselsTypeStringByVessel(curCard.vessels)
  colorFrame = FleetDataAccessHelper:GetFleetColorFrameByColor(curCard.color)
  avatarName = FleetDataAccessHelper:GetFleetColorDisplayName(curCard.name, curCard.fleet_id, curCard.color)
  avatarFrame = FleetDataAccessHelper:GetFleetAvatarFrameByAvatar(curCard.avatar, curCard.fleet_id, curCard.level)
  local sexr = GameDataAccessHelper:GetCommanderSex(curCard.fleet_id)
  if sexr == 1 then
    sexr = "man"
  elseif sexr == 0 then
    sexr = "woman"
  else
    sexr = "unknown"
  end
  flash:InvokeASCallback("_root", "setRecoverMenuNextCard", levelStr, vesselStr, vesselFrame, colorFrame, avatarFrame, avatarName, sexr)
end
function GameUIFleetGrowUp:ShowRecoverConfirmDialog(confirmCallback)
  local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
  GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
  GameUIMessageDialog:SetYesButton(confirmCallback, nil)
  local infoTitle = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
  local infoContent = GameLoader:GetGameText("LC_MENU_REFORM_RECOVER_CONFIRM_ALERT")
  GameUIMessageDialog:Display(infoTitle, infoContent)
end
function GameUIFleetGrowUp.ConfirmRecover()
  GameUIFleetGrowUp:RequestLevelDown(GameUIFleetGrowUp.mCurrentFleetID)
end
function GameUIFleetGrowUp:RefreshRecoverList()
  local flash = self:GetFlashObject()
  if not flash then
    return
  end
  flash:InvokeASCallback("_root", "clearRecoverList")
  flash:InvokeASCallback("_root", "initRecoverList")
  local recoverItems = self.mCurrentFleetEnhanceData:GetRecoverList()
  local recoverNum = #recoverItems
  for i = 1, recoverNum do
    flash:InvokeASCallback("_root", "addRecoverItem", i)
  end
  flash:InvokeASCallback("_root", "setRecoverArrowVisible")
end
function GameUIFleetGrowUp:UpdateRecoverItem(id)
  local flash = self:GetFlashObject()
  if not flash then
    return
  end
  local itemIndex = tonumber(id)
  local recoverItems = self.mCurrentFleetEnhanceData:GetRecoverList()
  local item = recoverItems[itemIndex]
  local itemName = GameHelper:GetAwardTypeText(item.item_type, item.number)
  local itemNumber = "x" .. GameUtils.numberConversion(GameHelper:GetAwardCount(item.item_type, item.number, item.no))
  local itemIcon
  if DynamicResDownloader:IsDynamicStuffByGameItem(item) then
    local fullFileName = DynamicResDownloader:GetFullName(item.number .. ".png", DynamicResDownloader.resType.PIC)
    local localPath = "data2/" .. fullFileName
    if DynamicResDownloader:IfResExsit(localPath) then
      ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
      itemIcon = "item_" .. item.number
    else
      itemIcon = "temp"
      self:AddDownloadPath(item.number, itemIndex, true)
    end
  else
    itemIcon = GameHelper:GetAwardTypeIconFrameName(item.item_type, item.number, item.no)
  end
  flash:InvokeASCallback("_root", "setRecoverItem", itemIndex, itemName, itemNumber, itemIcon)
end
function GameUIFleetGrowUp:ShowRecoverFinished()
  local flash = self:GetFlashObject()
  if not flash then
    return
  end
  flash:InvokeASCallback("_root", "showRecoverFinished")
  local resourceTxt = GameLoader:GetGameText("LC_MENU_REFORM_RECOVER_SUCCESS_RECIEVE_ALERT")
  flash:InvokeASCallback("_root", "setRecoverResourceText", resourceTxt)
end
GameUIFleetGrowUp.backToMainPlant = false
function GameUIFleetGrowUp:ShowLevelUpSuccessMenu()
  local flash = self:GetFlashObject()
  if not flash then
    return
  end
  flash:InvokeASCallback("_root", "showSuccessPanel")
  if (immanentversion170 == 4 or immanentversion170 == 5) and QuestTutorialFirstReform:IsActive() and GameUIFleetGrowUp.mCurrentFleetID == 1 then
    QuestTutorialFirstReform:SetFinish(true, false)
    GameUIFleetGrowUp.backToMainPlant = true
    flash:InvokeASCallback("_root", "ShowReformTutorial", false)
  end
end
function GameUIFleetGrowUp:HideLevelUpSuccessMenu()
  local flash = self:GetFlashObject()
  if not flash then
    return
  end
  flash:InvokeASCallback("_root", "hideSuccessPanel")
end
function GameUIFleetGrowUp:SetUpSuccessMenuCard()
  local flash = self:GetFlashObject()
  if not flash then
    return
  end
  local fleetCard = GameUIFleetGrowUp.mCurrentFleetEnhanceData:GetFleetCardData()
  local curCard = fleetCard.mCurFleetCard
  local colorNum = FleetDataAccessHelper:GetFleetColorNumberByColor(curCard.color)
  local levelStr = "<font color='" .. colorNum .. "'>" .. "[" .. "</font>" .. "+" .. curCard.level .. "<font color='" .. colorNum .. "'>" .. "]" .. "</font>"
  local vesselFrame = FleetDataAccessHelper:GetFleetVesselsFrameByVessel(curCard.vessels)
  local vesselStr = FleetDataAccessHelper:GetFleetVesselsTypeStringByVessel(curCard.vessels)
  local colorFrame = FleetDataAccessHelper:GetFleetColorFrameByColor(curCard.color)
  local avatarName = FleetDataAccessHelper:GetFleetColorDisplayName(curCard.name, curCard.fleet_id, curCard.color)
  local avatarFrame = FleetDataAccessHelper:GetFleetAvatarFrameByAvatar(curCard.avatar, curCard.fleet_id, curCard.level)
  local avatarShip = GameDataAccessHelper:GetShip(curCard.fleet_id, nil, curCard.level)
  local leaderlist = GameGlobalData:GetData("leaderlist")
  local curMatrixIndex = GameGlobalData.GlobalData.curMatrixIndex
  if curCard.fleet_id == 1 and leaderlist and curMatrixIndex then
    avatarShip = GameDataAccessHelper:GetShip(leaderlist.leader_ids[curMatrixIndex], nil, curCard.level)
  end
  local sexl = GameDataAccessHelper:GetCommanderSex(curCard.fleet_id)
  if sexl == 1 then
    sexl = "man"
  elseif sexl == 0 then
    sexl = "woman"
  else
    sexl = "unknown"
  end
  flash:InvokeASCallback("_root", "setUpSuccessCurCard", levelStr, vesselStr, vesselFrame, colorFrame, avatarFrame, avatarName, avatarShip, sexl)
  curCard = fleetCard.mNextFleetCard
  local colorNum = FleetDataAccessHelper:GetFleetColorNumberByColor(curCard.color)
  local levelStr = "<font color='" .. colorNum .. "'>" .. "[" .. "</font>" .. "+" .. curCard.level .. "<font color='" .. colorNum .. "'>" .. "]" .. "</font>"
  vesselFrame = FleetDataAccessHelper:GetFleetVesselsFrameByVessel(curCard.vessels)
  vesselStr = FleetDataAccessHelper:GetFleetVesselsTypeStringByVessel(curCard.vessels)
  colorFrame = FleetDataAccessHelper:GetFleetColorFrameByColor(curCard.color)
  avatarName = FleetDataAccessHelper:GetFleetColorDisplayName(curCard.name, curCard.fleet_id, curCard.color)
  avatarFrame = FleetDataAccessHelper:GetFleetAvatarFrameByAvatar(curCard.avatar, curCard.fleet_id, curCard.level)
  avatarShip = GameDataAccessHelper:GetShip(curCard.fleet_id, nil, curCard.level)
  if curCard.fleet_id == 1 and leaderlist and curMatrixIndex then
    avatarShip = GameDataAccessHelper:GetShip(leaderlist.leader_ids[curMatrixIndex], nil, curCard.level)
  end
  local sexr = GameDataAccessHelper:GetCommanderSex(curCard.fleet_id)
  if sexr == 1 then
    sexr = "man"
  elseif sexr == 0 then
    sexr = "woman"
  else
    sexr = "unknown"
  end
  flash:InvokeASCallback("_root", "setUpSuccessNextCard", levelStr, vesselStr, vesselFrame, colorFrame, avatarFrame, avatarName, avatarShip, sexr)
end
function GameUIFleetGrowUp:RefreshSuccessDetailList()
  local flash = self:GetFlashObject()
  if not flash then
    return
  end
  flash:InvokeASCallback("_root", "clearSuccessDetailList")
  flash:InvokeASCallback("_root", "initSuccessDetailList")
  local attriData = self.mCurrentFleetEnhanceData:GetFleetAttriDiffData()
  local attriNum = #attriData.mAttriNames
  for i = 1, attriNum do
    flash:InvokeASCallback("_root", "addSuccessDetailItem", i)
  end
end
function GameUIFleetGrowUp:UpdateSuccessDetailItem(id)
  local flash = self:GetFlashObject()
  if not flash then
    return
  end
  local itemIndex = tonumber(id)
  local attriData = self.mCurrentFleetEnhanceData:GetFleetAttriDiffData()
  local itemName = attriData.mAttriNames[itemIndex]
  local itemCurValue = attriData.mCurLevelAttriValue[itemIndex]
  local itemAddValue = attriData.mNextLevelAttriValue[itemIndex] - attriData.mCurLevelAttriValue[itemIndex]
  if self.mCurrentFleetEnhanceData:IsPercentAttri(attriData.mAttriIDs[itemIndex]) then
    itemCurValue = string.format("%.02f", itemCurValue) .. "%"
    itemAddValue = string.format("%.02f", itemAddValue) .. "%"
  else
    itemCurValue = GameUtils.numberConversion(tonumber(itemCurValue))
    itemAddValue = GameUtils.numberConversion(tonumber(itemAddValue))
  end
  flash:InvokeASCallback("_root", "setSuccessDetailItem", itemIndex, itemName, itemCurValue, itemAddValue)
end
function GameUIFleetGrowUp:Update(dt)
  local flash = self:GetFlashObject()
  if not flash then
    return
  end
  flash:InvokeASCallback("_root", "OnUpdate")
  flash:Update(dt)
end
function GameUIFleetGrowUp:OnFSCommand(cmd, arg)
  if cmd == "needUpdateDetailItem" then
    self:UpdateDetailInfoItem(arg)
  elseif cmd == "needUpdateSuccessDetailItem" then
    self:UpdateSuccessDetailItem(arg)
  elseif cmd == "needUpdateRecoverItem" then
    self:UpdateRecoverItem(arg)
  elseif cmd == "showDetail" then
    if GameUIFleetGrowUp.mCurrentUIState == E_UI_STATE.UISTATE_CARD then
      local flash = self:GetFlashObject()
      if not flash then
        return
      end
      GameUIFleetGrowUp.mCurrentUIState = E_UI_STATE.UISTATE_MOVING_TO_DETAIL
      flash:InvokeASCallback("_root", "showDetail")
    end
  elseif cmd == "hideDetail" then
    if GameUIFleetGrowUp.mCurrentUIState == E_UI_STATE.UISTATE_DETAIL then
      local flash = self:GetFlashObject()
      if not flash then
        return
      end
      GameUIFleetGrowUp.mCurrentUIState = E_UI_STATE.UISTATE_MOVING_TO_CARD
      flash:InvokeASCallback("_root", "hideDetail")
    end
  elseif cmd == "moveToDetailFinished" then
    GameUIFleetGrowUp.mCurrentUIState = E_UI_STATE.UISTATE_DETAIL
  elseif cmd == "moveToCardFinished" then
    GameUIFleetGrowUp.mCurrentUIState = E_UI_STATE.UISTATE_CARD
  elseif cmd == "Avatarclicked" then
    if GameUIFleetGrowUp.mCurrentUIState == E_UI_STATE.UISTATE_CARD then
      self:OnFSCommand("showDetail", arg)
    elseif GameUIFleetGrowUp.mCurrentUIState == E_UI_STATE.UISTATE_DETAIL then
      self:OnFSCommand("hideDetail", arg)
    end
  elseif cmd == "detailPanelClicked" then
    if GameUIFleetGrowUp.mCurrentUIState == E_UI_STATE.UISTATE_CARD then
      self:OnFSCommand("showDetail", arg)
    end
  elseif cmd == "closeClicked" then
    GameStateEquipEnhance:GoToSubState(SUB_STATE.STATE_NORMAL)
  elseif cmd == "openGrowUp" then
    if self:IsMaxLevel(self.mCurrentFleetEnhanceData.mCurLevel) then
      local errorContent = GameLoader:GetGameText("LC_MENU_REFORMING_LV_MAX_DESC")
      GameTip:Show(errorContent)
    else
      GameUIFleetGrowUp:ShowGrowUpRequrieMenu()
      GameUIFleetGrowUp:SetRequrieMenuAvatar()
      GameUIFleetGrowUp:SetRequrieLevelPanel()
      GameUIFleetGrowUp:SetRequriePercent()
      GameUIFleetGrowUp:SetRequrieItems()
    end
  elseif cmd == "openRecover" then
    if self.mCurrentFleetEnhanceData:IsMinLevel() then
      return
    else
      GameUIFleetGrowUp:RequestRecoverItems()
    end
  elseif cmd == "growUpClicked" then
    GameUIFleetGrowUp:RequestLevelUp(GameUIFleetGrowUp.mCurrentFleetID)
  elseif cmd == "closeGrowUpClicked" then
    GameUIFleetGrowUp:HideGrowUpRequrieMenu()
  elseif cmd == "wantRecover" then
    GameUIFleetGrowUp:ShowRecoverConfirmDialog(GameUIFleetGrowUp.ConfirmRecover)
  elseif cmd == "recoverToDisplay" then
    GameUIFleetGrowUp:RecoverToDisplayerMenu()
    GameUIFleetGrowUp:Show()
  elseif cmd == "closeRecoverClicked" then
    GameUIFleetGrowUp:HideRecoverMenu()
  elseif cmd == "growUpToDisplay" then
    if immanentversion170 == nil then
      GameUIFleetGrowUp:HideLevelUpSuccessMenu()
      GameUIFleetGrowUp:Show()
    elseif immanentversion170 == 4 or immanentversion170 == 5 then
      if GameUIFleetGrowUp.backToMainPlant then
        GameUIFleetGrowUp:HideLevelUpSuccessMenu()
        GameStateManager:SetCurrentGameState(GameStateManager.GameStateMainPlanet)
      else
        GameUIFleetGrowUp:HideLevelUpSuccessMenu()
        GameUIFleetGrowUp:Show()
      end
    end
  elseif cmd == "helpClicked" then
    GameUIWDStuff:Show(GameUIWDStuff.menuType.menu_type_help)
    GameUIWDStuff:SetHelpText(GameLoader:GetGameText("LC_MENU_REFORM_HELP_TEXT"))
  elseif cmd == "ShowItemDetail" then
    local index = tonumber(arg)
    local requrieItems = GameUIFleetGrowUp.mCurrentFleetEnhanceData:GetGrowUpRequireList()
    local item = requrieItems[index]
    if item then
      GameUIFleetGrowUp:showItemDetil(item, item.item_type)
    end
  elseif cmd == "ShowReverDetail" then
    local index = tonumber(arg)
    local recoverItems = self.mCurrentFleetEnhanceData:GetRecoverList()
    local item = recoverItems[index]
    if item then
      GameUIFleetGrowUp:showItemDetil(item, item.item_type)
    end
  end
end
function GameUIFleetGrowUp:showItemDetil(item_, item_type)
  if GameHelper:IsResource(item_type) then
    return
  end
  local item = item_
  if item_type == "krypton" then
    if item.level == 0 then
      item.level = 1
    end
    local detail = GameUIKrypton:TryQueryKryptonDetail(item.number, item.level, function(msgType, content)
      local ret = GameUIKrypton.QueryKryptonDetailCallBack(msgType, content)
      if ret then
        local tmp = GameGlobalData:GetKryptonDetail(item.number, item.level)
        if tmp == nil then
          return false
        end
        ItemBox:SetKryptonBox(tmp, item.number)
        local operationTable = {}
        operationTable.btnUnloadVisible = false
        operationTable.btnUpgradeVisible = false
        operationTable.btnUseVisible = false
        operationTable.btnDecomposeVisible = false
        ItemBox:ShowKryptonBox(320, 240, operationTable)
      end
      return ret
    end)
    if detail == nil then
    else
      ItemBox:SetKryptonBox(detail, item.number)
      local operationTable = {}
      operationTable.btnUnloadVisible = false
      operationTable.btnUpgradeVisible = false
      operationTable.btnUseVisible = false
      operationTable.btnDecomposeVisible = false
      ItemBox:ShowKryptonBox(320, 240, operationTable)
    end
  end
  if item_type == "item" then
    item.cnt = 1
    ItemBox:showItemBox("Item", item, tonumber(item.number), 320, 480, 200, 200, "", nil, "", nil)
  end
  if item_type == "fleet" then
    ItemBox:ShowCommanderDetail2(tonumber(item.number))
  end
end
function GameUIFleetGrowUp:AddDownloadPath(itemID, itemIndex, isLevelUp)
  local resName = itemID .. ".png"
  local extInfo = {}
  extInfo.isLevelUp = isLevelUp
  extInfo.item_id = itemID
  extInfo.localPath = "data2/LAZY_LOAD_dynamic_" .. resName
  extInfo.item_index = itemIndex
  DynamicResDownloader:AddDynamicRes(resName, DynamicResDownloader.resType.PIC, extInfo, GameUIFleetGrowUp.donamicDownloadFinishCallback)
end
function GameUIFleetGrowUp.donamicDownloadFinishCallback(extInfo)
  if extInfo.isLevelUp then
    GameUIFleetGrowUp:SetRequrieItems()
  else
    GameUIFleetGrowUp:UpdateRecoverItem(extInfo.item_index)
  end
end
function GameUIFleetGrowUp:IsMaxLevel(level)
  if level == self.mMaxLevel then
    return true
  end
  return false
end
