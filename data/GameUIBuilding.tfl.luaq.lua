local GameUIBuilding = LuaObjectManager:GetLuaObject("GameUIBuilding")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameStateMainPlanet = GameStateManager.GameStateMainPlanet
local NetMessageMgr = NetMessageMgr
local NetAPIList = NetAPIList
local AlertDataList = AlertDataList
local GameGlobalData = GameGlobalData
local GameUICommonDialog = LuaObjectManager:GetLuaObject("GameUICommonDialog")
local QuestTutorialEngineer = TutorialQuestManager.QuestTutorialEngineer
local QuestTutorialEnhance = TutorialQuestManager.QuestTutorialEnhance
local QuestTutorialCityHall = TutorialQuestManager.QuestTutorialCityHall
local GameUIBarRight = LuaObjectManager:GetLuaObject("GameUIBarRight")
local QuestTutorialGetQuestReward = TutorialQuestManager.QuestTutorialGetQuestReward
local QuestTutorialTax = TutorialQuestManager.QuestTutorialTax
local QuestTutorialBuildStar = TutorialQuestManager.QuestTutorialBuildStar
local QuestTutorialStarCharge = TutorialQuestManager.QuestTutorialStarCharge
local QuestTutorialRecruit = TutorialQuestManager.QuestTutorialRecruit
local QuestTutorialBuildAcademy = TutorialQuestManager.QuestTutorialBuildAcademy
local QuestTutorialBuildTechLab = TutorialQuestManager.QuestTutorialBuildTechLab
local QuestTutorialBuildFactory = TutorialQuestManager.QuestTutorialBuildFactory
local QuestTutorialBuildKrypton = TutorialQuestManager.QuestTutorialBuildKrypton
local QuestTutorialBuildAffairs = TutorialQuestManager.QuestTutorialBuildAffairs
local QuestTutorialUseTechLab = TutorialQuestManager.QuestTutorialUseTechLab
local QuestTutorialUseFactory = TutorialQuestManager.QuestTutorialUseFactory
local QuestTutorialUseKrypton = TutorialQuestManager.QuestTutorialUseKrypton
local QuestTutorialUseAffairs = TutorialQuestManager.QuestTutorialUseAffairs
local QuestTutorialsFirstKillBoss = TutorialQuestManager.QuestTutorialsFirstKillBoss
local QuestTutorialComboGacha = TutorialQuestManager.QuestTutorialComboGacha
local GameUICollect = LuaObjectManager:GetLuaObject("GameUICollect")
local GameUIActivityNew = LuaObjectManager:GetLuaObject("GameUIActivityNew")
local QuestTutorialFactoryRemodel = TutorialQuestManager.QuestTutorialFactoryRemodel
function GameUIBuilding:InitUpgradeContent(building_info)
  local name = "LC_MENU_BUILDING_NAMELG_" .. string.upper(building_info.name)
  local building_name = GameLoader:GetGameText(name)
  local desc = "LC_MENU_UPGRADE_DESC_" .. string.upper(building_info.name)
  local building_desc = GameLoader:GetGameText(desc)
  local flash_obj = self:GetFlashObject()
  local lang = "en"
  if GameSettingData and GameSettingData.Save_Lang then
    lang = GameSettingData.Save_Lang
    if string.find(lang, "ru") == 1 then
      lang = "ru"
    end
  end
  flash_obj:InvokeASCallback("_root", "DisplayUpgradeDialog", building_name, building_desc, lang)
  flash_obj:ReplaceTexture("NA_Building.png", building_info.name .. ".png")
end
function GameUIBuilding:OnAddToGameState()
  if QuestTutorialEngineer:IsActive() then
    if immanentversion == 1 then
      GameUICommonDialog:PlayStory({100014})
    end
  elseif QuestTutorialBuildAcademy:IsActive() then
  end
end
function GameUIBuilding:OnEraseFromGameState()
  GameStateMainPlanet:CheckShowTutorial()
  self:UnloadFlashObject()
  GameStateMainPlanet:CheckShowQuestInMainPlanet()
end
function GameUIBuilding:UpdateUpgradeContent(building_info)
  DebugTable(building_info)
  local nextLevel = math.min(building_info.level + 1, building_info.max_level)
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "UpdateUpgradeContent", building_info.level, nextLevel)
  local canUpgrade = building_info.status == 1 and building_info.level < building_info.max_level
  DebugOut("canUpgrade: ", canUpgrade)
  flash_obj:SetBtnEnable("_root.dialog_upgrade.btnUpgrade", canUpgrade)
  self:RefreshReq(building_info.max_level == building_info.level)
end
function GameUIBuilding:RefreshReq(isMaxLevel)
  local buildingInfo = GameGlobalData:GetBuildingInfo(self.upgradeBuildingName)
  local desLevel = math.max(buildingInfo.level + 1, buildingInfo.require_user_level)
  local costText, timeText, reqText = buildingInfo.cost, GameUtils:formatTimeString(buildingInfo.upgrade_time), GameLoader:GetGameText("LC_MENU_UPGRADE_PLAYER_LVL") .. desLevel
  local levelinfo = GameGlobalData:GetData("levelinfo")
  local level = levelinfo.level
  local canReq = desLevel <= level
  if costText >= 10000000 then
    costText = GameUtils.numberConversion(costText)
  end
  self:GetFlashObject():InvokeASCallback("_root", "RefreshReq", costText, timeText, reqText, canReq, isMaxLevel)
end
function GameUIBuilding:DisplayUpgradeDialog(building_name)
  local buildingInfo = GameGlobalData:GetBuildingInfo(building_name)
  self:LoadFlashObject()
  self.upgradeBuildingName = building_name
  local flash_obj = self:GetFlashObject()
  self:InitUpgradeContent(buildingInfo)
  self:UpdateUpgradeContent(buildingInfo)
  GameStateMainPlanet:AddObject(self)
  if QuestTutorialEngineer:IsActive() and building_name == "engineering_bay" or QuestTutorialCityHall:IsActive() and building_name == "planetary_fortress" or QuestTutorialBuildStar:IsActive() and building_name == "star_portal" or QuestTutorialBuildTechLab:IsActive() and building_name == "tech_lab" or QuestTutorialBuildFactory:IsActive() and building_name == "factory" or QuestTutorialBuildKrypton:IsActive() and building_name == "krypton_center" or QuestTutorialBuildAffairs:IsActive() and building_name == "affairs_hall" or QuestTutorialBuildAcademy:IsActive() and building_name == "commander_academy" then
    self:GetFlashObject():InvokeASCallback("_root", "ShowTutorialUpgrade")
  else
    self:GetFlashObject():InvokeASCallback("_root", "HideTutorialUpgrade")
  end
  if QuestTutorialBuildAcademy:IsActive() and building_name == "commander_academy" then
    GameUICommonDialog:PlayStory({100022})
  end
  self:GetFlashObject():InvokeASCallback("_root", "HideTutorialClose")
end
function GameUIBuilding:OnInitGame()
  GameGlobalData:RegisterDataChangeCallback("buildings", GameUIBuilding.OnGlobalBuildingDataChange)
  GameGlobalData:RegisterDataChangeCallback("resource", GameUIBuilding.OnGlobalResourceChange)
end
function GameUIBuilding.OnGlobalResourceChange()
  if not GameStateManager:GetCurrentGameState():IsObjectInState(GameUIBuilding) then
    return
  end
  if GameUIBuilding:GetFlashObject() == nil or GameUIBuilding.upgradeBuildingName == nil then
    return
  end
  GameUIBuilding:GetFlashObject():InvokeASCallback("_root", "needUpdateVisibleItem")
end
function GameUIBuilding.OnGlobalBuildingDataChange()
  if not GameStateManager:GetCurrentGameState():IsObjectInState(GameUIBuilding) then
    return
  end
  if GameUIBuilding:GetFlashObject() == nil or GameUIBuilding.upgradeBuildingName == nil then
    return
  end
  local buildingInfo = GameGlobalData:GetBuildingInfo(GameUIBuilding.upgradeBuildingName)
  GameUIBuilding:UpdateUpgradeContent(buildingInfo)
end
function GameUIBuilding.NetCallbackClearCDPrice(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.price_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  if msgtype == NetAPIList.price_ack.Code then
    local info_content = GameLoader:GetGameText("LC_MENU_CLEAR_BUILDING_CD")
    info_content = string.format(info_content, content.price)
    GameUtils:CreditCostConfirm(info_content, function()
      GameUIBuilding:ClearBuildingCDRequest()
    end, true)
    return true
  end
  return false
end
function GameUIBuilding.upgradeCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and (content.api == NetAPIList.building_upgrade_req.Code or content.api == NetAPIList.cdtimes_clear_req.Code) then
    if content.api == NetAPIList.building_upgrade_req.Code then
      DebugOut("upgradeCallback ", content.code)
      if content.code ~= 0 then
        local error_key = AlertDataList.K[content.code].text
        if error_key == "building_cdtime_not" then
          function netCallProcess()
            NetMessageMgr:SendMsg(NetAPIList.price_req.Code, {
              price_type = "clear_building_upgrade_cd",
              type = 0
            }, GameUIBuilding.NetCallbackClearCDPrice, true, netCallProcess)
          end
          netCallProcess()
        elseif content.code == 130 and not GameUICollect:CheckTutorialCollect(content.code) then
          local text = AlertDataList:GetTextFromErrorCode(content.code)
          local GameUIKrypton = LuaObjectManager:GetLuaObject("GameUIKrypton")
          GameUIKrypton.NeedMoreMoney(text)
        else
          local GameVip = LuaObjectManager:GetLuaObject("GameVip")
          GameVip:CheckIsNeedShowVip(content.code)
        end
        return true
      else
        if QuestTutorialEngineer:IsActive() and GameUIBuilding.upgradeBuildingName == "engineering_bay" then
          local engineerBuildInfo = GameGlobalData:GetBuildingInfo("engineering_bay")
          if engineerBuildInfo.level >= 1 then
            GameUIBuilding:GetFlashObject():InvokeASCallback("_root", "HideTutorialUpgrade")
            QuestTutorialEngineer:SetFinish(true)
            AddFlurryEvent("EngineeringBayUpgradeSuccess", {}, 1)
            AddFlurryEvent("TutoriaEngine_FinishUp", {}, 2)
            GameUIBuilding.tutorialClose = 2
            QuestTutorialEnhance:SetActive(true)
            GameUIBuilding:GetFlashObject():InvokeASCallback("_root", "ShowTutorialClose")
          end
        elseif QuestTutorialCityHall:IsActive() and GameUIBuilding.upgradeBuildingName == "planetary_fortress" then
          GameUIBuilding:GetFlashObject():InvokeASCallback("_root", "HideTutorialUpgrade")
          QuestTutorialCityHall:SetFinish(true)
          if immanentversion == 1 then
            GameUICommonDialog:PlayStory({100052, 100053})
          elseif immanentversion == 2 then
            GameUICommonDialog:PlayStory({51108})
          end
          AddFlurryEvent("TutorialCityHall_FinishUp", {}, 2)
          GameUIBuilding.tutorialClose = 1
          GameUIBuilding:GetFlashObject():InvokeASCallback("_root", "ShowTutorialClose")
          if not QuestTutorialGetQuestReward:IsFinished() then
            QuestTutorialGetQuestReward:SetActive(true)
          end
        elseif QuestTutorialBuildStar:IsActive() and GameUIBuilding.upgradeBuildingName == "star_portal" then
          GameUIBuilding:GetFlashObject():InvokeASCallback("_root", "HideTutorialUpgrade")
          QuestTutorialBuildStar:SetFinish(true)
          QuestTutorialRecruit:SetActive(true)
          GameUIBuilding:GetFlashObject():InvokeASCallback("_root", "ShowTutorialClose")
        elseif QuestTutorialBuildAcademy:IsActive() and GameUIBuilding.upgradeBuildingName == "commander_academy" then
          GameUIBuilding:GetFlashObject():InvokeASCallback("_root", "HideTutorialUpgrade")
          QuestTutorialBuildAcademy:SetFinish(true)
          AddFlurryEvent("BuildAcademySuccess", {}, 1)
          AddFlurryEvent("FinishBuildAcademy", {}, 2)
          GameUIBuilding.tutorialClose = 3
          local function callback()
            GameUIBuilding:GetFlashObject():InvokeASCallback("_root", "ShowTutorialClose")
            GameUIBarRight:SetForceShowTutorialBattleMapOnce()
          end
          if not QuestTutorialsFirstKillBoss:IsActive() and not QuestTutorialsFirstKillBoss:IsFinished() then
            QuestTutorialsFirstKillBoss:SetActive(true)
          end
        elseif QuestTutorialBuildTechLab:IsActive() and GameUIBuilding.upgradeBuildingName == "tech_lab" then
          GameUIBuilding:GetFlashObject():InvokeASCallback("_root", "HideTutorialUpgrade")
          QuestTutorialBuildTechLab:SetFinish(true)
          GameUIBuilding:GetFlashObject():InvokeASCallback("_root", "ShowTutorialClose")
          if not QuestTutorialUseTechLab:IsFinished() then
            QuestTutorialUseTechLab:SetActive(true)
          end
        elseif QuestTutorialBuildFactory:IsActive() and GameUIBuilding.upgradeBuildingName == "factory" then
          GameUIBuilding:GetFlashObject():InvokeASCallback("_root", "HideTutorialUpgrade")
          QuestTutorialBuildFactory:SetFinish(true)
          GameUIBuilding:GetFlashObject():InvokeASCallback("_root", "ShowTutorialClose")
          if (immanentversion170 == 4 or immanentversion170 == 5) and not QuestTutorialFactoryRemodel:IsFinished() and not QuestTutorialFactoryRemodel:IsActive() then
            local GameUICommonDialog = LuaObjectManager:GetLuaObject("GameUICommonDialog")
            GameUICommonDialog:PlayStory({100048}, function()
              QuestTutorialFactoryRemodel:SetActive(true, false)
            end)
          end
        elseif QuestTutorialBuildKrypton:IsActive() and GameUIBuilding.upgradeBuildingName == "krypton_center" then
          GameUIBuilding:GetFlashObject():InvokeASCallback("_root", "HideTutorialUpgrade")
          QuestTutorialBuildKrypton:SetFinish(true)
          GameUIBuilding:GetFlashObject():InvokeASCallback("_root", "ShowTutorialClose")
          QuestTutorialUseKrypton:SetActive(true)
        elseif QuestTutorialBuildAffairs:IsActive() and GameUIBuilding.upgradeBuildingName == "affairs_hall" then
          GameUIBuilding:GetFlashObject():InvokeASCallback("_root", "HideTutorialUpgrade")
          QuestTutorialBuildAffairs:SetFinish(true)
          GameUIBuilding:GetFlashObject():InvokeASCallback("_root", "ShowTutorialClose")
          QuestTutorialUseAffairs:SetActive(true)
        end
        GameUIBuilding:AnimationUpgrade()
        return true
      end
    elseif content.api == NetAPIList.cdtimes_clear_req.Code then
      if content.code ~= 0 then
        local GameVip = LuaObjectManager:GetLuaObject("GameVip")
        GameVip:CheckIsNeedShowVip(content.code)
        return true
      end
      local info_content = GameLoader:GetGameText("LC_MENU_CLEAR_BUILDING_CD_SUC")
      local GameTip = LuaObjectManager:GetLuaObject("GameTip")
      GameTip:Show(info_content)
      return true
    end
    return true
  end
  return false
end
function GameUIBuilding:ClearBuildingCDRequest()
  local content = {cdtype = 1}
  NetMessageMgr:SendMsg(NetAPIList.cdtimes_clear_req.Code, content, GameUIBuilding.upgradeCallback, true, nil)
end
function GameUIBuilding:AnimationUpgrade()
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "lua2fs_animationUpgrade")
  end
end
function GameUIBuilding:OnFSCommand(cmd, arg)
  if cmd == "CloseDialog" then
    if GameUIBuilding.tutorialClose and GameUIBuilding.tutorialClose == 1 then
      AddFlurryEvent("CloseCityHallUpgradeUI", {}, 1)
      AddFlurryEvent("TutorialCityHall_Close", {}, 2)
      GameUIBuilding.tutorialClose = nil
    elseif GameUIBuilding.tutorialClose and GameUIBuilding.tutorialClose == 2 then
      AddFlurryEvent("CloseEngineeringBayUpgradeUI", {}, 1)
      AddFlurryEvent("TutoriaEngine_Close", {}, 2)
      GameUIBuilding.tutorialClose = nil
    elseif GameUIBuilding.tutorialClose and GameUIBuilding.tutorialClose == 3 then
      AddFlurryEvent("CloseBuildAcademyUI", {}, 1)
      GameUIBuilding.tutorialClose = nil
    end
    GameStateMainPlanet:EraseObject(self)
    self.upgradeBuildingName = nil
    return
  elseif cmd == "UpgradeBuilding" then
    if self.upgradeBuildingName == "star_portal" then
      AddFlurryEvent("TutorialStart_ClickUp", {}, 2)
    end
    local content = {
      name = self.upgradeBuildingName
    }
    DebugOutPutTable(content, "updagrade")
    if QuestTutorialCityHall:IsActive() and content.name == "planetary_fortress" then
      AddFlurryEvent("ClickCityHallBuildingUpgrade", {}, 1)
    end
    NetMessageMgr:SendMsg(NetAPIList.building_upgrade_req.Code, content, self.upgradeCallback, false, nil)
    GameUtils:RecordForTongdui(NetAPIList.building_upgrade_req.Code, content, self.upgradeCallback, false, nil)
    return
  end
  assert(false)
end
if AutoUpdate.isAndroidDevice then
  function GameUIBuilding.OnAndroidBack()
    GameUIBuilding:GetFlashObject():InvokeASCallback("_root", "dialogUpgradeMoveOut")
  end
end
