local percent_award = GameData.alliance.percent_award
table.insert(percent_award, {
  level = 1,
  prestige_chairman = 150,
  prestige_manager = 125,
  prestige_member = 100,
  money_chairman = 150,
  money_manager = 125,
  money_member = 100
})
table.insert(percent_award, {
  level = 2,
  prestige_chairman = 300,
  prestige_manager = 250,
  prestige_member = 200,
  money_chairman = 300,
  money_manager = 250,
  money_member = 200
})
table.insert(percent_award, {
  level = 3,
  prestige_chairman = 300,
  prestige_manager = 250,
  prestige_member = 200,
  money_chairman = 300,
  money_manager = 250,
  money_member = 200
})
table.insert(percent_award, {
  level = 4,
  prestige_chairman = 300,
  prestige_manager = 250,
  prestige_member = 200,
  money_chairman = 300,
  money_manager = 250,
  money_member = 200
})
table.insert(percent_award, {
  level = 5,
  prestige_chairman = 300,
  prestige_manager = 250,
  prestige_member = 200,
  money_chairman = 300,
  money_manager = 250,
  money_member = 200
})
table.insert(percent_award, {
  level = 6,
  prestige_chairman = 300,
  prestige_manager = 250,
  prestige_member = 200,
  money_chairman = 300,
  money_manager = 250,
  money_member = 200
})
table.insert(percent_award, {
  level = 7,
  prestige_chairman = 300,
  prestige_manager = 250,
  prestige_member = 200,
  money_chairman = 300,
  money_manager = 250,
  money_member = 200
})
table.insert(percent_award, {
  level = 8,
  prestige_chairman = 300,
  prestige_manager = 250,
  prestige_member = 200,
  money_chairman = 300,
  money_manager = 250,
  money_member = 200
})
table.insert(percent_award, {
  level = 9,
  prestige_chairman = 300,
  prestige_manager = 250,
  prestige_member = 200,
  money_chairman = 300,
  money_manager = 250,
  money_member = 200
})
table.insert(percent_award, {
  level = 10,
  prestige_chairman = 300,
  prestige_manager = 250,
  prestige_member = 200,
  money_chairman = 300,
  money_manager = 250,
  money_member = 200
})
table.insert(percent_award, {
  level = 11,
  prestige_chairman = 300,
  prestige_manager = 250,
  prestige_member = 200,
  money_chairman = 300,
  money_manager = 250,
  money_member = 200
})
table.insert(percent_award, {
  level = 12,
  prestige_chairman = 300,
  prestige_manager = 250,
  prestige_member = 200,
  money_chairman = 300,
  money_manager = 250,
  money_member = 200
})
table.insert(percent_award, {
  level = 13,
  prestige_chairman = 300,
  prestige_manager = 250,
  prestige_member = 200,
  money_chairman = 300,
  money_manager = 250,
  money_member = 200
})
table.insert(percent_award, {
  level = 14,
  prestige_chairman = 300,
  prestige_manager = 250,
  prestige_member = 200,
  money_chairman = 300,
  money_manager = 250,
  money_member = 200
})
table.insert(percent_award, {
  level = 15,
  prestige_chairman = 300,
  prestige_manager = 250,
  prestige_member = 200,
  money_chairman = 300,
  money_manager = 250,
  money_member = 200
})
