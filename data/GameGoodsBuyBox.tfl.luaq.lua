local GameGoodsList = LuaObjectManager:GetLuaObject("GameGoodsList")
local GameGoodsBuyBox = LuaObjectManager:GetLuaObject("GameGoodsBuyBox")
local GameWaiting = LuaObjectManager:GetLuaObject("GameWaiting")
local GameSetting = LuaObjectManager:GetLuaObject("GameSetting")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local GameStateStore = GameStateManager.GameStateStore
local GameUIKrypton = LuaObjectManager:GetLuaObject("GameUIKrypton")
local GameVipDetailInfoPanel = LuaObjectManager:GetLuaObject("GameVipDetailInfoPanel")
local GameUISevenBenefit = LuaObjectManager:GetLuaObject("GameUISevenBenefit")
GameGoodsBuyBox.iconDisplayed = false
GameGoodsBuyBox.setEmptyAlready = false
GameGoodsBuyBox.isNeedBuySuccessTip = false
GameGoodsBuyBox.isRquestPriceForConfirming = false
GameGoodsBuyBox.isRequestPriceForUpdatePriceUI = false
GameGoodsBuyBox.quickBuyCountRecord = 0
GameGoodsBuyBox.buyCount = 0
GameGoodsBuyBox.usedItem = nil
GameGoodsBuyBox.isUseDiscount = false
GameGoodsBuyBox.currency = "empty"
GameGoodsBuyBox._curitem = {}
GameGoodsBuyBox.mybuycount = 0
function GameGoodsBuyBox:OnInitGame()
  DebugStore("GameGoodsBuyBox:OnInitGame()")
end
function GameGoodsBuyBox:Init()
  self:DisplayDetail()
  self:UpdateDisplayIcon()
end
function GameGoodsBuyBox:OnAddToGameState()
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  GameGoodsBuyBox.iconDisplayed = false
  GameGoodsBuyBox.setEmptyAlready = false
  GameGoodsBuyBox.usedItem = nil
  GameGoodsBuyBox.isUseDiscount = false
  GameGoodsBuyBox.currency = "empty"
  self:Init()
  if self:IsCurStoreItemQuickBuyable() then
    DebugOut("quick_buy")
    self:GetFlashObject():InvokeASCallback("_root", "luacall_setUIVisible", 0)
    self.RequestQuickbuyableItemsDefaultCount()
  else
    self:MoveIn()
  end
  GameTimer:Add(self._OnTimerTick, 1000)
  GameGlobalData:RegisterDataChangeCallback("item_count", self.checkIfNeedTip)
end
function GameGoodsBuyBox:OnEraseFromGameState()
  GameGoodsBuyBox.buyCount = 0
  self:UnloadFlashObject()
  GameGlobalData:RemoveDataChangeCallback("item_count", self.checkIfNeedTip)
end
function GameGoodsBuyBox:RefreshResource()
  local resource = GameGlobalData:GetData("resource")
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "RefreshResource", GameUtils.numberConversion(resource.money), GameUtils.numberConversion(resource.credit))
end
function GameGoodsBuyBox:MoveIn()
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "animationMoveIn")
end
function GameGoodsBuyBox:MoveOut()
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "animationMoveOut")
end
function GameGoodsBuyBox:Update(dt)
  local flash_obj = self:GetFlashObject()
  flash_obj:Update(dt)
  flash_obj:InvokeASCallback("_root", "onUpdateFrame", dt)
end
function GameGoodsBuyBox:UpdateDisplayIcon()
  local v = GameStateStore.CurItemDetail
  if v == nil then
    return
  end
  DebugStore("GameGoodsBuyBox:UpdateDisplayIcon")
  DebugStoreTable(v)
  local curItem = v.item
  local icon = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(curItem, nil, GameGoodsBuyBox.QueryItemIconCallback)
  if icon ~= nil and icon ~= "" then
    DebugStore("icon", icon)
    self:GetFlashObject():InvokeASCallback("_root", "setIcon", icon)
    GameGoodsBuyBox.iconDisplayed = true
  else
    DebugStore("icon info not ok", icon)
  end
end
function GameGoodsBuyBox:updateDetailItemLeft()
  DebugStore("updateDetailItemLeft")
  local v = GameStateStore.CurItemDetail
  local allLeft = "-1"
  if v.all_buy_left ~= -1 then
    allLeft = GameLoader:GetGameText("LC_MENU_SHOP_BUY_TIME_CHAR") .. v.all_buy_left
  end
  local dayLeft = "-1"
  if v.day_buy_left ~= -1 then
    dayLeft = GameLoader:GetGameText("LC_MENU_SHOP_DAY_BUY_TIME_CHAR") .. v.day_buy_left
  end
  DebugStore("allLeft", allLeft)
  DebugStore("dayLeft", dayLeft)
  self:GetFlashObject():InvokeASCallback("_root", "updateDetailItemLeft", allLeft, dayLeft)
end
function GameGoodsBuyBox:DisplayDetail()
  DebugOut("GameGoodsBuyBox:DisplayDetail")
  local v = GameStateStore.CurItemDetail
  DebugStore("GameGoodsBuyBox:DisplayDetail")
  DebugStoreTable(v)
  local curItem = v.item
  local realPrice = math.ceil(v.price * v.discount / 100)
  local timeString = ""
  if v.time_left ~= -1 then
    local _lefttime = v.time_left - (os.time() - GameGoodsList.receiveGoodsListTime)
    if _lefttime < 0 then
      _lefttime = 0
    end
    timeString = GameUtils:formatTimeString(_lefttime)
  end
  local allLeft = "-1"
  if v.all_buy_left ~= -1 then
    allLeft = GameLoader:GetGameText("LC_MENU_SHOP_BUY_TIME_CHAR") .. v.all_buy_left
  end
  local dayLeft = "-1"
  if v.day_buy_left ~= -1 then
    dayLeft = GameLoader:GetGameText("LC_MENU_SHOP_DAY_BUY_TIME_CHAR") .. v.day_buy_left
  end
  local curLevel = GameVipDetailInfoPanel:GetVipLevel()
  local levelInfo = GameGlobalData:GetData("levelinfo")
  local locked = true
  if curLevel >= v.vip_limit and levelInfo.level >= v.level_limit then
    locked = false
  end
  local refresh_buy_limit = -1
  if GameStateManager:GetCurrentGameState() == GameStateManager.GameStateStore then
    refresh_buy_limit = GameGoodsList:GetRefreshLeftBuyTimes()
  end
  DebugOut("BuyReleased:", refresh_buy_limit)
  DebugTable(GameGoodsList.MysteryData)
  if GameGoodsList.MysteryData and GameGoodsList.MysteryData.refresh_type == 1 and refresh_buy_limit == 0 then
    locked = true
  end
  local refreshLeft = "-1"
  if refresh_buy_limit ~= -1 then
    refreshLeft = GameLoader:GetGameText("LC_MENU_SHOP_BUY_TIME_CHAR") .. refresh_buy_limit
  end
  local th_token_type = GameGoodsList:serverIDToIconFrame(v.price_type)
  GameGoodsBuyBox.currency = th_token_type
  local itemName = GameHelper:GetAwardNameText(curItem.item_type, curItem.number)
  local itemDetail = ""
  DebugStore("curItem.item_type", curItem.item_type)
  if curItem.item_type == "krypton" then
    itemDetail = GameUIKrypton:TryQueryKryptonDetail(curItem.number, curItem.level, GameGoodsBuyBox.QueryKryptonDetailCallback)
  elseif curItem.item_type == "item" then
    itemDetail = GameLoader:GetGameText("LC_ITEM_ITEM_DESC_" .. curItem.number)
  elseif GameHelper:GetAwardTypeText(curItem.item_type, curItem.number) then
    local nameText = GameHelper:GetAwardText(curItem.item_type, curItem.number, curItem.no)
    itemDetail = string.format(GameLoader:GetGameText("LC_MENU_BUY_TO_GET"), nameText)
  elseif curItem.item_type == "fleet" then
    DebugStore("not support fleet")
    return
  end
  if itemName ~= nil and itemName ~= "" then
    DebugStore("itemName", itemName)
    DebugStore("itemDetail", itemDetail)
    local detailStr = ""
    if curItem.item_type == "krypton" and itemDetail ~= nil then
      DebugStoreTable(itemDetail)
      local typeID = itemDetail.addon[1].type
      detailStr = GameLoader:GetGameText("LC_MENU_Equip_param_" .. typeID)
      local paramNum = itemDetail.addon[1].value
      if typeID >= 9 and typeID <= 14 or typeID == 17 then
        paramNum = paramNum / 10 .. "%"
      end
      paramNum = "+" .. paramNum
      itemDetail = detailStr .. paramNum
    end
    if itemDetail == nil then
      itemDetail = ""
    end
    local isQuickBuyable = self:IsCurStoreItemQuickBuyable()
    DebugOut("tp:", GameGoodsList.nowStoreType, v.day_buy_left, v.all_buy_left)
    DebugTable(v)
    if GameGoodsList.MysteryData and GameGoodsList.MysteryData.refresh_type == 1 then
      DebugOut("tps:setItem_new")
      local showDiscountUse = false
      local discountPaper = GameGlobalData:GetCuponChoices()
      GameGoodsBuyBox.usedItem = self:GetDefaultItem(discountPaper)
      DebugTable(discountPaper)
      if GameGoodsList.nowStoreType == 2 and 0 < v.refresh_buy_limit and discountPaper ~= nil and #discountPaper > 0 then
        showDiscountUse = true
      end
      self:GetFlashObject():InvokeASCallback("_root", "setItem_new", itemName, itemDetail, v.price, 100 - v.discount, realPrice, v.price_type, allLeft, dayLeft, timeString, locked, th_token_type, isQuickBuyable, refreshLeft, showDiscountUse)
      GameGoodsBuyBox:UpdatePrimeIcon(GameGoodsBuyBox.usedItem)
    else
      DebugOut("tps:setItem")
      local showDiscountUse = false
      local discountPaper = GameGlobalData:GetCuponChoices()
      GameGoodsBuyBox.usedItem = self:GetDefaultItem(discountPaper)
      DebugTable(discountPaper)
      if GameGoodsList.nowStoreType == 2 and 0 < v.refresh_buy_limit and discountPaper ~= nil and #discountPaper > 0 then
        showDiscountUse = true
      end
      self:GetFlashObject():InvokeASCallback("_root", "setItem", itemName, itemDetail, v.price, 100 - v.discount, realPrice, v.price_type, allLeft, dayLeft, timeString, locked, th_token_type, isQuickBuyable, showDiscountUse)
      GameGoodsBuyBox:UpdatePrimeIcon(GameGoodsBuyBox.usedItem)
    end
    if v.buyCount and 0 < v.buyCount then
      self:GetFlashObject():InvokeASCallback("_root", "changeQuickBuyNumber", v.buyCount)
    end
  else
    DebugStore("name & detail info not ok", itemName, itemDetail)
  end
end
function GameGoodsBuyBox:GetDefaultItem(items)
  if items == nil then
    return nil
  end
  local ret = items[1]
  for k, v in ipairs(items) do
    if v.discount < ret.discount then
      ret = v
    end
  end
  return ret
end
function GameGoodsBuyBox:IsCurStoreItemQuickBuyable()
  return GameStateStore.CurItemDetail.quick_buy > 0
end
function GameGoodsBuyBox.isStoreItemCastCubit(storeItem)
  if 1 == storeItem.price_type then
    return true
  end
  if "money" == GameGoodsList.goodsList.currency then
    return true
  end
  return false
end
function GameGoodsBuyBox.isStoreItemCastCredit(storeItem)
  if 2 == storeItem.price_type then
    return true
  end
  if "credit" == GameGoodsList.goodsList.currency then
    return true
  end
  return false
end
function GameGoodsBuyBox.isStoreItemCastCrystal(storeItem)
  if GameGoodsBuyBox.isStoreItemCastCubit(storeItem) or GameGoodsBuyBox.isStoreItemCastCredit(storeItem) then
    return false
  end
  if "crystal" == GameGoodsList.goodsList.currency then
    return true
  end
  return false
end
function GameGoodsBuyBox.QueryKryptonDetailCallback(msgType, content)
  DebugStore("GameGoodsBuyBox.QueryKryptonDetailCallback ok")
  if msgType == NetAPIList.krypton_info_ack.Code then
    GameWaiting:HideLoadingScreen()
    DebugStore("content.krypton.id", content.krypton.id, "content.krypton.level", content.krypton.level)
    DebugStoreTable(content.krypton)
    GameGlobalData:SetKryptonDetail(tonumber(content.krypton.id), tonumber(content.krypton.level), content.krypton)
    GameGoodsBuyBox:DisplayDetail()
    return true
  end
  return false
end
function GameGoodsBuyBox.QueryItemIconCallback(msgType, content)
  DebugStore("QueryItemIconCallback")
  GameGoodsBuyBox:UpdateDisplayIcon()
end
function GameGoodsBuyBox:OnFSCommand(cmd, arg)
  DebugStore("store command: ", cmd)
  GameTextEdit:fscommand(self:GetFlashObject(), cmd, arg)
  if cmd == "BuyReleased" then
    if GameUISevenBenefit.currentTabId == GameUISevenBenefit.ShowPanleTypeId.HALF_PRICE then
      GameGoodsBuyBox:HalfPriceBuyItem()
    elseif self:IsCurStoreItemQuickBuyable() then
      local buyCount = tonumber(arg)
      self.quickBuyCountRecord = buyCount
      if 0 == buyCount then
        do break end
        -- unhandled boolean indicator
        assert(true)
      end
      if GameSettingData.EnablePayRemind then
        self.isRquestPriceForConfirming = true
        self.RequestQuickBuyCast(buyCount, true)
      else
        self.RequestBuyItem(buyCount, 0)
      end
    else
      self.RequestBuyItem(1, 0)
    end
  elseif cmd == "close_menu" then
    self:MoveOut()
  elseif cmd == "boxHiden" then
    if GameUISevenBenefit.currentTabId == GameUISevenBenefit.ShowPanleTypeId.HALF_PRICE then
      GameStateStore:HideBuyConfirmWinInSevenDayBenefit()
    else
      GameStateStore:HideBuyConfirmWin()
    end
  elseif cmd == "needUpdateShopItem" then
    self:UpdateGoodsItem(arg)
  elseif cmd == "shopItemReleased" then
    local itemIndex = tonumber(arg)
    self:QueryBuyItem(itemIndex)
  elseif "onQuickBuyNumberChanged" == cmd then
    local buyCount = tonumber(arg)
    GameGoodsBuyBox.onQuickBuyNumberChanged(buyCount)
  elseif cmd == "showChoices" then
    local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
    local function callback(item)
      GameGoodsBuyBox.usedItem = item
      GameGoodsBuyBox:UpdatePrimeIcon(item)
    end
    local pt = LuaUtils:string_split(arg, "!")
    local items = GameGlobalData:GetCuponChoices()
    ItemBox:ShowUseChoiceBox(pt[1], pt[2], items, callback)
  elseif cmd == "isUseDiscount" then
    if tonumber(arg) == 1 then
      self.isUseDiscount = true
      local beforeprice = GameStateStore.CurItemDetail.price
      local price = math.ceil(beforeprice * (GameGoodsBuyBox.usedItem.discount / 1000) * (GameStateStore.CurItemDetail.discount / 100))
      self:GetFlashObject():InvokeASCallback("_root", "updatePrcieForUseDiscount", beforeprice, price, GameGoodsBuyBox.currency)
    else
      self.isUseDiscount = false
      local beforeprice = GameStateStore.CurItemDetail.price
      local price = math.ceil(GameStateStore.CurItemDetail.price * (GameStateStore.CurItemDetail.discount / 100))
      self:GetFlashObject():InvokeASCallback("_root", "updatePrcieForUseDiscount", beforeprice, price, GameGoodsBuyBox.currency)
    end
  end
end
function GameGoodsBuyBox:UpdatePrimeIcon(item)
  DebugOut("GameGoodsBuyBox:UpdatePrimeIcon")
  DebugTable(item)
  if item == nil then
    return
  end
  if self:GetFlashObject() then
    local icon = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(item, nil, nil)
    self:GetFlashObject():InvokeASCallback("_root", "updatePrimeIcon", icon)
    if self.isUseDiscount then
      local beforeprice = GameStateStore.CurItemDetail.price
      local price = math.ceil(beforeprice * (GameGoodsBuyBox.usedItem.discount / 1000) * (GameStateStore.CurItemDetail.discount / 100))
      self:GetFlashObject():InvokeASCallback("_root", "updatePrcieForUseDiscount", beforeprice, price, GameGoodsBuyBox.currency)
    end
  end
end
function GameGoodsBuyBox:HalfPriceBuyItem()
  local param = {
    id = GameStateStore.CurItemDetail.id
  }
  DebugOut("HalfPriceBuyItem:")
  DebugTable(param)
  NetMessageMgr:SendMsg(NetAPIList.buy_cutoff_item_req.Code, param, GameGoodsBuyBox.HalfPriceBuyItemCallback, true, nil)
end
function GameGoodsBuyBox.HalfPriceBuyItemCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.buy_cutoff_item_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    GameGoodsBuyBox:MoveOut()
    GameUISevenBenefit:GetHalfPriceBenefitDataFromServer()
    return true
  end
  return false
end
function GameGoodsBuyBox.RequestBuyItem(count, mstate)
  DebugOut("GameGoodsBuyBox.RequestBuyItem : " .. count)
  DebugOut(count)
  GameGoodsBuyBox.buyCount = count
  GameGoodsBuyBox._curitem = GameStateStore.CurItemDetail.item
  GameGoodsBuyBox.mybuycount = count
  local shop_purchase_req_param
  if GameGoodsBuyBox.usedItem ~= nil and GameGoodsBuyBox.isUseDiscount then
    shop_purchase_req_param = {
      store_id = GameGoodsList.nowStoreType,
      id = GameStateStore.CurItemDetail.id,
      buy_times = count,
      state = mstate,
      coupon = GameGoodsBuyBox.usedItem.number
    }
  else
    shop_purchase_req_param = {
      store_id = GameGoodsList.nowStoreType,
      id = GameStateStore.CurItemDetail.id,
      buy_times = count,
      state = mstate,
      coupon = 0
    }
  end
  DebugTable(shop_purchase_req_param)
  NetMessageMgr:SendMsg(NetAPIList.stores_buy_req.Code, shop_purchase_req_param, GameGoodsBuyBox.BuyItemCallback, true, nil)
  GameUtils:RecordForTongdui(NetAPIList.stores_buy_req.Code, shop_purchase_req_param, GameGoodsBuyBox.BuyItemCallback, true, nil)
  GameGoodsBuyBox.isNeedBuySuccessTip = true
end
function GameGoodsBuyBox.RequestQuickbuyableItemsDefaultCount()
  local reqParam = {
    store_id = GameGoodsList.nowStoreType,
    id = GameStateStore.CurItemDetail.id
  }
  DebugOut("GameGoodsBuyBox.RequestQuickbuyableItemsDefaultCount")
  DebugTable(reqParam)
  NetMessageMgr:SendMsg(NetAPIList.store_quick_buy_count_req.Code, reqParam, GameGoodsBuyBox.NetRequestCallback, true, nil)
end
function GameGoodsBuyBox.RequestQuickBuyCast(count, isBlock)
  DebugOut("GameGoodsBuyBox.RequestQuickBuyCast")
  local reqParam = {
    store_id = GameGoodsList.nowStoreType,
    buy_times = count,
    id = GameStateStore.CurItemDetail.id
  }
  NetMessageMgr:SendMsg(NetAPIList.store_quick_buy_price_req.Code, reqParam, GameGoodsBuyBox.NetRequestCallback, isBlock, nil)
end
function GameGoodsBuyBox.NetRequestCallback(msgType, content)
  DebugOut("GameGoodsBuyBox.NetRequestCallback")
  DebugOut(msgType)
  DebugTable(content)
  DebugOut(NetAPIList.common_ack.Code)
  if msgtype == NetAPIList.common_ack.Code and (NetAPIList.store_quick_buy_count_req.code == content.api or NetAPIList.store_quick_buy_price_req.code == content.api) then
    DebugOut("exception")
    if content.code ~= 0 then
      local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.store_quick_buy_count_ack.Code then
    GameGoodsBuyBox.OnQuickBuyDefaultCountCallback(content)
    return true
  elseif msgType == NetAPIList.store_quick_buy_price_ack.Code then
    GameGoodsBuyBox.OnQuickBuyPriceCallback(content)
    return true
  end
  return false
end
function GameGoodsBuyBox.OnQuickBuyDefaultCountCallback(content)
  DebugOut("GameGoodsBuyBox.OnQuickBuyDefaultCountCallback")
  GameGoodsBuyBox.InitQuickBuyUI(content.count, content.dis_price, content.nor_price, content.max_count)
end
function GameGoodsBuyBox.OnQuickBuyPriceCallback(content)
  DebugOut("GameGoodsBuyBox.OnQuickBuyPriceCallback")
  if GameGoodsBuyBox.isRquestPriceForConfirming then
    GameGoodsBuyBox.OpenQuickBuyConfirmingUI(GameGoodsBuyBox.quickBuyCountRecord, content.dis_price)
    GameGoodsBuyBox.isRquestPriceForConfirming = false
  end
  if GameGoodsBuyBox.isRequestPriceForUpdatePriceUI then
    GameGoodsBuyBox.UpdateQuickBuyUIPrice(content.dis_price, content.nor_price)
    GameGoodsBuyBox.isRequestPriceForUpdatePriceUI = false
  end
end
function GameGoodsBuyBox.InitQuickBuyUI(count, price, priceBeforeDiscount, maxCount)
  DebugOut(" GameGoodsBuyBox.InitQuickBuyUI ")
  DebugOut("" .. count .. " " .. price .. " " .. priceBeforeDiscount .. " " .. maxCount)
  assert(count <= maxCount)
  GameGoodsBuyBox:GetFlashObject():InvokeASCallback("_root", "luacall_setUIVisible", 1)
  GameGoodsBuyBox:MoveIn()
  GameGoodsBuyBox:GetFlashObject():InvokeASCallback("_root", "luacall_initTheQuickBuyNumber", count, maxCount)
  GameGoodsBuyBox:GetFlashObject():InvokeASCallback("_root", "luacall_updatePrice", price, priceBeforeDiscount)
  if 0 == count then
    DebugOut("if( 0 == count ) then")
    GameGoodsBuyBox.onQuickBuyNumberChanged(1)
  end
end
function GameGoodsBuyBox.UpdateQuickBuyUIPrice(price, priceBeforeDiscount)
  GameGoodsBuyBox:GetFlashObject():InvokeASCallback("_root", "luacall_updatePrice", price, priceBeforeDiscount)
end
function GameGoodsBuyBox.OpenQuickBuyConfirmingUI(count, price)
  local function buyCallback()
    GameGoodsBuyBox.RequestBuyItem(count, 0)
  end
  local castTypeName = GameGoodsBuyBox.getCurStoreItemCastTypeName()
  local gameItemName = GameGoodsBuyBox.getCurStoreItemItemName()
  local textMsg = GameLoader:GetGameText("LC_MENU_BUY_MORE_ITEMS_PROMPT")
  textMsg = string.gsub(textMsg, "<number1>", tostring(price))
  textMsg = string.gsub(textMsg, "<number2>", tostring(castTypeName))
  textMsg = string.gsub(textMsg, "<number3>", tostring(count))
  textMsg = string.gsub(textMsg, "<number4>", tostring(gameItemName))
  local textMsgAfterFormat = textMsg
  GameUtils:CreditCostConfirm(textMsgAfterFormat, buyCallback, true, nil)
end
function GameGoodsBuyBox.getCurStoreItemCastTypeName()
  local storeItem = GameStateStore.CurItemDetail
  local name = GameGoodsList:serverIDToIconFrame(storeItem.price_type) or "crystal"
  if string.find(name, "item_") then
    name = GameHelper:GetAwardTypeText("item", storeItem.price_type)
  else
    name = GameHelper:GetAwardNameText(name)
  end
  name = name .. " "
  return name
end
function GameGoodsBuyBox.getCurStoreItemItemName()
  local gameItem = GameStateStore.CurItemDetail.item
  local itemName = GameHelper:GetAwardNameText(gameItem.item_type, gameItem.number)
  return itemName or "errorName"
end
function GameGoodsBuyBox.onQuickBuyNumberChanged(num)
  DebugOut("GameGoodsBuyBox.onQuickBuyNumberChanged " .. num)
  GameGoodsBuyBox.isRequestPriceForUpdatePriceUI = true
  GameGoodsBuyBox.RequestQuickBuyCast(num, false)
end
function GameGoodsBuyBox.BuyItemCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.stores_buy_req.Code then
    DebugStore("buy result")
    DebugStore(msgType)
    DebugStore(NetAPIList.common_ack.Code)
    DebugStoreTable(content)
    GameWaiting:HideLoadingScreen()
    if content.api == NetAPIList.stores_buy_req.Code then
      if content.code ~= 0 then
        local GameVip = LuaObjectManager:GetLuaObject("GameVip")
        GameVip:CheckIsNeedShowVip(content.code)
      else
        GameGoodsBuyBox.usedItem = nil
        if GameGoodsBuyBox:IsCurStoreItemQuickBuyable() then
          GameGoodsList:ReduceBuyTime(GameGoodsBuyBox.quickBuyCountRecord)
        else
          GameGoodsList:ReduceBuyTime(1)
        end
        GameGoodsBuyBox:MoveOut()
        GameGoodsBuyBox:updateDetailItemLeft()
        DebugOut("ShowRewardBox")
        DebugOut("in buybox")
        local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
        local param = {}
        local _param = {}
        _param.item_type = GameGoodsBuyBox._curitem.item_type
        if GameHelper:IsResource(_param.item_type) then
          _param.number = GameGoodsBuyBox._curitem.number * GameGoodsBuyBox.mybuycount
          _param.no = GameGoodsBuyBox._curitem.no
        else
          _param.no = GameGoodsBuyBox._curitem.no * GameGoodsBuyBox.mybuycount
          _param.number = GameGoodsBuyBox._curitem.number
        end
        _param.level = GameGoodsBuyBox._curitem.level
        table.insert(param, _param)
        ItemBox:ShowRewardBox(param)
        GameGoodsBuyBox._curitem = {}
        GameGoodsBuyBox.mybuycount = 0
        GameGoodsBuyBox.isNeedBuySuccessTip = false
        GameGoodsList.RequestRefreshData()
      end
    end
    return true
  elseif NetAPIList.stores_buy_ack and msgType == NetAPIList.stores_buy_ack.Code then
    DebugOut("NetAPIList.stores_buy_ack.Code ")
    DebugTable(content.award)
    GameGoodsBuyBox.usedItem = nil
    DebugOut("ShowRewardBox")
    DebugOut("in buybox")
    local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
    ItemBox:ShowRewardBox(content.award)
    if GameGoodsBuyBox:IsCurStoreItemQuickBuyable() then
      GameGoodsList:ReduceBuyTime(GameGoodsBuyBox.quickBuyCountRecord)
    else
      GameGoodsList:ReduceBuyTime(1)
    end
    GameGoodsBuyBox:updateDetailItemLeft()
    GameGoodsList.RequestRefreshData()
    return true
  elseif msgType == NetAPIList.credit_exchange_ack.Code and content.api == NetAPIList.stores_buy_req.Code then
    DebugOut("credit_exchange_ack:--stores_buy_req: = ")
    DebugTable(content)
    local itemName = GameHelper:GetAwardNameText(content.item.item_type, content.item.number)
    local str = string.gsub(GameLoader:GetGameText("LC_MENU_LACK_OF_RESOURCES"), "<name1>", itemName)
    local infoText = string.gsub(str, "<name2>", content.credit)
    GameUtils:CreditCostConfirm(infoText, GameGoodsBuyBox.CostCreditDoneThisAction, true, nil)
    return true
  end
  return false
end
function GameGoodsBuyBox.CostCreditDoneThisAction(...)
  GameGoodsBuyBox.RequestBuyItem(GameGoodsBuyBox.buyCount, 1)
end
function GameGoodsBuyBox:UpdateItemCDTime()
  local timeString = ""
  if GameStateStore.CurItemDetail ~= nil and GameStateStore.CurItemDetail.time_left ~= -1 then
    local _lefttime = GameStateStore.CurItemDetail.time_left - (os.time() - GameGoodsList.receiveGoodsListTime)
    if _lefttime < 0 then
      _lefttime = 0
    end
    timeString = GameUtils:formatTimeString(_lefttime)
    self:GetFlashObject():InvokeASCallback("_root", "setItemCDTime", timeString)
  end
end
function GameGoodsBuyBox._OnTimerTick()
  if GameStateStore:IsObjectInState(GameGoodsBuyBox) and GameGoodsList.goodsList ~= nil and GameStateStore.CurItemDetail ~= nil and GameStateStore.CurItemDetail.time_left ~= -1 then
    GameGoodsBuyBox:UpdateItemCDTime()
    return 1000
  else
    return nil
  end
end
function GameGoodsBuyBox:checkIfNeedTip()
  GameGoodsBuyBox.isNeedBuySuccessTip = false
end
