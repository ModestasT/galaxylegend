local GameStateTrade = GameStateManager.GameStateTrade
local GameUICommonDialog = LuaObjectManager:GetLuaObject("GameUICommonDialog")
local GameItemBag = LuaObjectManager:GetLuaObject("GameItemBag")
local GameFleetInfoUI = LuaObjectManager:GetLuaObject("GameFleetInfoUI")
local GameFleetInfoBag = LuaObjectManager:GetLuaObject("GameFleetInfoBag")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
local GameFleetInfoTabBar = LuaObjectManager:GetLuaObject("GameFleetInfoTabBar")
local GameStateBattleMap = GameStateManager.GameStateBattleMap
local GameTradeTabBar = LuaObjectManager:GetLuaObject("GameTradeTabBar")
local GameUICollect = LuaObjectManager:GetLuaObject("GameUICollect")
local GameObjectBuyBack = LuaObjectManager:GetLuaObject("GameObjectBuyBack")
local QuestTutorialGetGift = TutorialQuestManager.QuestTutorialGetGift
local QuestTutorialEquip = TutorialQuestManager.QuestTutorialEquip
local QuestTutorialEquipAllByOneKey = TutorialQuestManager.QuestTutorialEquipAllByOneKey
local GameSetting = LuaObjectManager:GetLuaObject("GameSetting")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local GameUIEvent = LuaObjectManager:GetLuaObject("GameUIEvent")
local QuestTutorialComboGachaGetHero = TutorialQuestManager.QuestTutorialComboGachaGetHero
local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
local GameObjectAwardCenter = LuaObjectManager:GetLuaObject("GameObjectAwardCenter")
GameItemBag.ifnotNeedToShowRewardBox = false
require("DummyItemBag.tfl")
require("DummyGameItem.tfl")
BAG_TITLE_TYPE = {
  TITLE_USE = 1,
  TITLE_EQUIP = 2,
  TITLE_BATTLE = 3,
  TITLE_OTHERS = 4,
  TITLE_COMPONENT = 5
}
GameItemBag.MAX_LINE = 4
GameItemBag.LINE_COUNT = 4
GameItemBag.itemInBag = nil
GameItemBag.equipInBag = nil
GameItemBag.useableInBag = nil
GameItemBag.othersInBag = nil
GameItemBag.equipedItem = {}
GameItemBag.dummyBagList = nil
GameItemBag.curDummyBagIndex = -1
GameItemBag.m_itemHeadKey = nil
GameItemBag.m_itemHeadY = nil
GameItemBag.showTipInfoFlag = false
GameItemBag.choosableItems = {}
GameItemBag.choosedItemIndex = 1
GameItemBag.waitDownloadInfo = {}
GameItemBag.curPressItem = {}
GameItemBag.bagSizeInfo = {}
function GameItemBag:Init()
  DebugOut("GameItemBag:Init")
  self:ClearDummyBag()
  self.isRequestBagItem = false
  self:RequestBag(true)
  GameItemBag.curDummyBagIndex = BAG_TITLE_TYPE.TITLE_USE
  self.isOnSell = nil
end
function GameItemBag:InitBagSize()
  GameItemBag.bagSizeInfo[BAG_TITLE_TYPE.TITLE_USE] = GameItemBag.bagSizeInfo[BAG_TITLE_TYPE.TITLE_USE] or {}
  GameItemBag.bagSizeInfo[BAG_TITLE_TYPE.TITLE_EQUIP] = GameItemBag.bagSizeInfo[BAG_TITLE_TYPE.TITLE_EQUIP] or {}
  GameItemBag.bagSizeInfo[BAG_TITLE_TYPE.TITLE_BATTLE] = GameItemBag.bagSizeInfo[BAG_TITLE_TYPE.TITLE_BATTLE] or {}
  GameItemBag.bagSizeInfo[BAG_TITLE_TYPE.TITLE_OTHERS] = GameItemBag.bagSizeInfo[BAG_TITLE_TYPE.TITLE_OTHERS] or {}
  GameItemBag.bagSizeInfo[BAG_TITLE_TYPE.TITLE_COMPONENT] = GameItemBag.bagSizeInfo[BAG_TITLE_TYPE.TITLE_COMPONENT] or {}
end
function GameItemBag:InitItemList(gotoTop)
  if gotoTop then
    self.m_itemHeadKey = nil
    self.m_itemHeadY = nil
  else
    self:GetItemHead()
  end
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "clearListItem")
    self:GetFlashObject():InvokeASCallback("_root", "initListItem")
  end
  self:AddEquipmentItem()
end
function GameItemBag:GetItemHead()
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "getListItemHeadPos")
  end
end
function GameItemBag:SetListItemHeadPos()
  if self:GetFlashObject() and self.m_itemHeadKey and self.m_itemHeadKey then
    self:GetFlashObject():InvokeASCallback("_root", "SetListItemHeadPos", self.m_itemHeadKey, self.m_itemHeadY)
  end
end
function GameItemBag:RequestBag(shouldInit)
  self.isRequestBagItem = true
  self.initList = shouldInit
  local userinfo = GameGlobalData:GetData("userinfo")
  DebugOut("GameItemBag:RequestBag")
  DebugTable(userinfo)
  if LuaUtils:table_empty(userinfo) then
    return
  end
  local bag_req_content = {
    owner = userinfo.player_id,
    bag_type = GameFleetInfoBag.BAG_OWNER_USER,
    pos = -1
  }
  local function netFailedCallback()
    self.isRequestBagItem = false
    NetMessageMgr:SendMsg(NetAPIList.bag_req.Code, bag_req_content, self.RequestBagCallback, false, netFailedCallback)
  end
  NetMessageMgr:SendMsg(NetAPIList.bag_req.Code, bag_req_content, self.RequestBagCallback, false, netFailedCallback)
end
function GameItemBag.RefreshBagSize(content)
  GameItemBag:InitBagSize()
  for k, v in pairs(content.bags) do
    GameItemBag.bagSizeInfo[v.title_type].item_num = v.item_num
    GameItemBag.bagSizeInfo[v.title_type].show_max = v.show_max
  end
  GameItemBag:UpdateItemSize()
end
function GameItemBag.UpdateItemInBag(content)
  DebugOut("GameItemBag.UpdateItemInBag")
  DebugTable(content)
  local addItemList = {}
  if GameItemBag.itemInBag == nil then
    GameItemBag.itemInBag = {}
  end
  if content.bag_type == GameFleetInfoBag.BAG_OWNER_USER then
    local maxPos = GameItemBag:GetItemMaxPos(GameItemBag.itemInBag)
    for k, v in pairs(content.grids) do
      local item
      if GameItemBag.itemInBag then
        item = GameItemBag.itemInBag[v.pos]
      end
      addItemList[v.item_type] = addItemList[v.item_type] or 0
      if item then
        if item.item_type ~= v.item_type then
          addItemList[v.item_type] = addItemList[v.item_type] + 1
        elseif item.item_type == v.item_type and item.cnt ~= v.cnt then
          if item.cnt < v.cnt then
            addItemList[v.item_type] = addItemList[v.item_type] + (v.cnt - item.cnt)
          elseif item.cnt > v.cnt then
          end
        end
      else
        addItemList[v.item_type] = addItemList[v.item_type] + v.cnt
      end
      if v.item_type == 0 then
        GameItemBag.itemInBag[v.pos] = nil
      else
        GameItemBag.itemInBag[v.pos] = v
      end
    end
    GameItemBag:updateDummyBags(content.grids)
    addItemList[0] = nil
    DebugTable(addItemList)
    if GameStateManager:GetCurrentGameState():IsObjectInState(GameItemBag) then
      if maxPos < GameItemBag:GetItemMaxPos(GameItemBag.itemInBag) then
        GameItemBag:InsertItemList(maxPos)
      else
      end
    else
      if GameStateManager:GetCurrentGameState():IsObjectInState(GameFleetInfoBag) and maxPos < GameItemBag:GetItemMaxPos(GameItemBag.itemInBag) then
        GameFleetInfoBag:InsertItemList(maxPos)
      else
      end
    end
  elseif content.bag_type == GameFleetInfoBag.BAG_OWNER_FLEET then
  end
  local QuestTutorialEquip = TutorialQuestManager.QuestTutorialEquip
  if QuestTutorialEquip:IsActive() and GameItemBag.itemInBag[1] ~= nil then
  elseif QuestTutorialEquip:IsActive() and immanentversion == 1 then
    GameStateBattleMap:SetStoryWhenFocusGain({1108})
  end
  TutorialQuestManager:UpdateQuestTutorialFirstGetWanaArmor(GameItemBag.itemInBag)
  TutorialQuestManager:UpdateQuestTutorialComboGachaGetHero(GameItemBag.itemInBag)
end
function GameItemBag:RefreshDummyBagList()
  GameItemBag.dummyBagList[BAG_TITLE_TYPE.TITLE_USE]:RefreshBag()
  GameItemBag.dummyBagList[BAG_TITLE_TYPE.TITLE_EQUIP]:RefreshBag()
  GameItemBag.dummyBagList[BAG_TITLE_TYPE.TITLE_BATTLE]:RefreshBag()
  GameItemBag.dummyBagList[BAG_TITLE_TYPE.TITLE_OTHERS]:RefreshBag()
  GameItemBag.dummyBagList[BAG_TITLE_TYPE.TITLE_COMPONENT]:RefreshBag()
end
function GameItemBag:Clear()
  GameUIEvent.finishSecondBag = nil
end
function GameItemBag:OnAddToGameState()
  if QuestTutorialComboGachaGetHero:IsActive() then
    TutorialQuestManager:GetComboGachaSendHero(2)
  end
  self.m_itemHeadKey = nil
  self.m_itemHeadY = nil
  self.UseItemData = nil
  self:Init()
  if GameObjectBuyBack:GetFlashObject() then
    GameObjectBuyBack:GetFlashObject():InvokeASCallback("_root", "hideBuyBackEnd")
  end
  self.isOnSell = nil
  NetMessageMgr:RegisterMsgHandler(NetAPIList.sell_item_ntf.Code, GameItemBag.SellItemNtf)
end
function GameItemBag.SellItemNtf(content)
  DebugOut("SellItemNtf:")
  DebugTable(content)
  if content and content.amount then
    GameTip:Show(string.format(GameLoader:GetGameText("LC_MENU_SALE_SUCCESS"), tonumber(content.amount)))
  end
end
function GameItemBag:OnEraseFromGameState()
  self:Clear()
  self.isRequestBagItem = false
end
function GameItemBag:Update(dt)
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "OnUpdate")
    self:GetFlashObject():Update(dt)
  end
  self:UpdateReleased(dt)
end
function GameItemBag:UpdateReleased(dt)
  if self.doubClicked then
    self.doubClicked = false
    self:DoubClicked()
  elseif self.clicked then
    if self.isRequestBagItem then
      self.clicked = false
    elseif self.clickedTimer > 0 then
      DebugOut("GameItemBag:UpdateReleased ")
      self.clickedTimer = self.clickedTimer - dt
      if self.clickedTimer <= 0 then
        local paramString = self:GetFlashObject():InvokeASCallback("_root", "GetItemRect", self.lastReleasedIndex)
        local param = LuaUtils:string_split(paramString, "\001")
        local dummyItem = self.dummyBagList[self.curDummyBagIndex]:GetDummyItem(self.lastReleasedIndex)
        if not dummyItem then
          self.clicked = false
          return
        end
        DebugOut("print table dummyItem ---")
        DebugTable(dummyItem)
        local itemType = GameDataAccessHelper:GetItemRealType(dummyItem)
        self.sellIndex = self.lastReleasedIndex
        if GameDataAccessHelper:isChoosableItem(itemType) then
          DebugOut("ChoosableItem ", itemType)
          local operateLeftFunc = GameItemBag.beforeUseItem
          self.choosedItemIndex = 1
          ItemBox:showItemBox("ChoosableItem", dummyItem, itemType, tonumber(param[1]), tonumber(param[2]), tonumber(param[3]), tonumber(param[4]), nil, operateLeftFunc, nil, nil)
        elseif DynamicResDownloader:IsDynamicStuff(dummyItem.item_type, DynamicResDownloader.resType.PIC) then
          local operateLeftText, operateLeftFunc = GameLoader:GetGameText("LC_MENU_USE_CHAR"), GameItemBag.beforeUseItem
          local operateRightText, operateRightFunc = GameLoader:GetGameText("LC_MENU_SELL_CHAR"), GameItemBag.callbackSellItem
          if GameData.Item.Keys[itemType] ~= nil then
            if not GameDataAccessHelper:IsItemCanUse(itemType) then
              operateLeftText = ""
              operateLeftFunc = nil
            end
            if GameDataAccessHelper:GetItemPrice(itemType) == 0 then
              operateRightText = ""
              operateRightFunc = nil
            end
          end
          DebugOut("operateLeftText=", operateLeftText, "operateRightText", operateRightText)
          ItemBox:showItemBox("Item", dummyItem, itemType, tonumber(param[1]), tonumber(param[2]), tonumber(param[3]), tonumber(param[4]), operateLeftText, operateLeftFunc, operateRightText, operateRightFunc)
        elseif GameDataAccessHelper:IsEquipment(dummyItem.item_id) then
          local operateText = GameLoader:GetGameText("LC_MENU_SELL_CHAR")
          local operateFunc = GameItemBag.callbackSellItem
          local operateRightText, operateRightFunc = GameLoader:GetGameText("LC_MENU_EQUIPMENT_BUTTON"), GameItemBag.gotoFleetInfoUI
          local equipments = GameGlobalData:GetData("equipments")
          local EquipItem
          for k, v in pairs(equipments) do
            if v.equip_id == dummyItem.item_id then
              EquipItem = v
              EquipItem.pos = dummyItem.pos
              break
            end
          end
          ItemBox:showItemBox("Equip", EquipItem, itemType, tonumber(param[1]), tonumber(param[2]), tonumber(param[3]), tonumber(param[4]), operateText, operateFunc, operateRightText, operateRightFunc)
        else
          if QuestTutorialGetGift:IsActive() then
            AddFlurryEvent("TutorialItem_UseBox", {}, 2)
          end
          DebugOut("isUseItem ", itemType)
          local operateLeftText, operateLeftFunc = "", nil
          local operateRightText, operateRightFunc = GameLoader:GetGameText("LC_MENU_SELL_CHAR"), GameItemBag.callbackSellItem
          if GameDataAccessHelper:IsItemCanUse(itemType) then
            operateLeftText = GameLoader:GetGameText("LC_MENU_USE_CHAR")
            operateLeftFunc = GameItemBag.beforeUseItem
          end
          if GameDataAccessHelper:GetItemPrice(itemType) == 0 then
            operateRightText = ""
            operateRightFunc = nil
          end
          DebugOut("operateLeftText=", operateLeftText, "operateRightText", operateRightText)
          ItemBox:showItemBox("Item", dummyItem, itemType, tonumber(param[1]), tonumber(param[2]), tonumber(param[3]), tonumber(param[4]), operateLeftText, operateLeftFunc, operateRightText, operateRightFunc)
        end
        self.clicked = false
      end
    end
  end
end
function GameItemBag.beforeUseItem(count, idx)
  local dummyItem = GameItemBag.dummyBagList[GameItemBag.curDummyBagIndex]:GetDummyItem(GameItemBag.lastReleasedIndex)
  local itemType = GameDataAccessHelper:GetItemRealType(dummyItem)
  DebugOut("beforeUseItem " .. tostring(count) .. " " .. tostring(idx))
  if GameDataAccessHelper:IsExpProps(itemType) then
    NetMessageMgr:SendMsg(NetAPIList.check_exp_add_item_req.Code, {
      item = dummyItem.item_type
    }, function(msgType, content)
      if msgType == NetAPIList.check_exp_add_item_ack.Code then
        if content.result ~= 0 then
          GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
          GameUIMessageDialog:SetYesButton(GameItemBag.UseItem, count, idx)
          local txt = GameLoader:GetGameText("LC_ALERT_item_Exp_Effect_Replace")
          local txt2 = tostring(content.add_percent) .. "%%"
          txt = string.gsub(txt, "<exp>", txt2)
          txt = string.gsub(txt, "<time>", GameUtils:formatTimeString(content.lefttime))
          local item_name = GameLoader:GetGameText("LC_ITEM_ITEM_NAME_" .. itemType)
          item_name = string.gsub(item_name, "%%", "%%%%")
          txt = string.gsub(txt, "<item_name>", item_name)
          GameUIMessageDialog:Display(GameLoader:GetGameText("LC_MENU_TITLE_VERIFY"), txt)
        else
          GameItemBag.UseItem(count, idx)
        end
        return true
      else
        return false
      end
    end, false, nil)
    return
  end
  GameItemBag.UseItem(count, idx)
end
function GameItemBag.UseItem(count, idx)
  local dummyItem = GameItemBag.dummyBagList[GameItemBag.curDummyBagIndex]:GetDummyItem(GameItemBag.lastReleasedIndex)
  if idx ~= nil then
    GameItemBag.choosedItemIndex = idx
  end
  GameItemBag.curPressItem = dummyItem
  DebugOut("GameItemBag.index", GameItemBag.choosedItemIndex)
  DebugOut("GameItemBag.choosableItems:")
  DebugTable(GameItemBag.choosableItems)
  local itemType = GameDataAccessHelper:GetItemRealType(dummyItem)
  if GameDataAccessHelper:isChoosableItem(itemType) then
    local netparam = {}
    netparam.id = dummyItem.item_type
    netparam.count = count or 1
    local temp = GameItemBag.choosableItems[GameItemBag.choosedItemIndex]
    netparam.item = {
      item_type = temp.item_type,
      number = temp.number,
      no = temp.no,
      level = temp.level,
      krypton_value = temp.krypton_value,
      can_use_more = temp.can_use_more
    }
    DebugOut("UseChoosableItem:")
    DebugTable(netparam)
    local function callback(msgType, content)
      DebugOut("msgType:" .. msgType)
      if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.choosable_item_use_req.Code then
        if content.code ~= 0 then
          DebugOut("failed")
          local textID = AlertDataList:GetTextFromErrorCode(content.code)
          GameItemBag.UseItemData = {}
          GameTip:Show(textID, 3000)
          return true
        else
          DebugOut("success")
          local bagSize = GameItemBag.bagSizeInfo[GameItemBag.curDummyBagIndex]
          if bagSize and bagSize.item_num > bagSize.show_max then
            GameItemBag:RequestBag(true)
            GameItemBag:InitItemList(true)
          else
            GameItemBag:RequestBag(false)
            GameItemBag:InitItemList(false)
          end
          GameItemBag:UpdateFlashListItem(GameItemBag.lastReleasedIndex)
          return true
        end
      end
      return false
    end
    NetMessageMgr:SendMsg(NetAPIList.choosable_item_use_req.Code, netparam, callback, false, nil)
    GameItemBag.RecordUseItemData(ItemBox.currentItemType, count)
    return
  end
  if QuestTutorialComboGachaGetHero:IsActive() and TutorialQuestManager.ComboGachaHeroContent and GameItemBag.curPressItem.item_type == TutorialQuestManager.ComboGachaHeroContent.id then
    local function callback()
      AddFlurryEvent("Gacha_Dialog_6", {}, 1)
      QuestTutorialComboGachaGetHero:SetFinish(true)
    end
    AddFlurryEvent("Gacha_UseDNA", {}, 1)
    GameUICommonDialog:PlayStory({1100062}, callback)
  end
  local param = {
    pos = dummyItem.pos,
    is_max = false,
    params = {}
  }
  if ItemBox.bUseMax and ItemBox.useMaxCheck == 1 then
    param.is_max = true
  end
  param.count = count or 1
  DebugOut("UseItem  ", ItemBox.bUseMax, ItemBox.useMaxCheck, count)
  DebugTable(param)
  if GameDataAccessHelper:IsItemUsedRename(ItemBox.currentItemType) then
    ItemBox.RenameText = LuaUtils:string_trim(ItemBox.RenameText)
    if ItemBox.RenameText == "" then
      GameTip:Show(GameLoader:GetGameText("LC_MENU_USE_ITEM_" .. ItemBox.currentItemType .. "_EMPTY_INPUT_ALERT"), 2000)
      return
    end
    if string.len(ItemBox.RenameText) > 14 then
      GameTip:Show(GameLoader:GetGameText("LC_MENU_SERVE_NAME_TOO_LONG_ERROR"), 2000)
      return
    end
    table.insert(param.params, ItemBox.RenameText)
  end
  if dummyItem then
    if GameUIEvent.finishSecondBag then
      GameUIEvent.finishSecondBag = nil
      AddFlurryEvent("OpenEquipBox02", {}, 2)
    end
    NetMessageMgr:SendMsg(NetAPIList.use_item_req.Code, param, GameItemBag.useItemCallback, false)
    GameItemBag.RecordUseItemData(ItemBox.currentItemType, count)
  end
end
function GameItemBag.RecordUseItemData(itemID, itemNum)
  DebugOut("RecordUseItemData", itemID, itemNum)
  if not itemID then
    DebugOut("GameItemBag.RecordUseItemData: nil value")
    return
  end
  local data = {}
  data.itemID = itemID
  data.itemNum = itemNum or 1
  if not GameItemBag.UseItemData then
    GameItemBag.UseItemData = {}
  end
  table.insert(GameItemBag.UseItemData, data)
end
function GameItemBag.RemoveUseItemData()
  if not GameItemBag.UseItemData then
    return
  end
  DebugOut("try RemoveUseItemData")
  local key
  local data = {}
  for k, v in pairs(GameItemBag.UseItemData) do
    key = k
    break
  end
  if key then
    data = GameItemBag.UseItemData[key]
    table.remove(GameItemBag.UseItemData, key)
    DebugOut("RemoveUseItemData", data.itemID, data.itemNum)
  end
  return data
end
function GameItemBag.useChoosableItemCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.choosable_item_use_req.Code then
    if content.code ~= 0 then
      local textID = AlertDataList:GetTextFromErrorCode(content.code)
      GameItemBag.UseItemData = {}
      GameTip:Show(textID, 3000)
    else
      DebugOut("hello")
      local bagSize = GameItemBag.bagSizeInfo[GameItemBag.curDummyBagIndex]
      if bagSize and bagSize.item_num > bagSize.show_max then
        GameItemBag:RequestBag(true)
        GameItemBag:InitItemList(true)
      else
        GameItemBag:RequestBag(false)
      end
    end
    return true
  end
  return false
end
function GameItemBag.useItemCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.use_item_req.Code then
    DebugOut("GameItemBag.useItemCallback", msgType)
    DebugTable(content)
    if content.code == 47012 then
      local textID = AlertDataList:GetTextFromErrorCode(content.code)
      GameTip:Show(textID, 3000)
    elseif content.code ~= 0 then
      local textID = AlertDataList:GetTextFromErrorCode(content.code)
      if content.code == 802 then
        GameItemBag:RequestBag(false)
      end
      GameItemBag.UseItemData = {}
      GameTip:Show(textID, 3000)
    end
    local bagSize = GameItemBag.bagSizeInfo[GameItemBag.curDummyBagIndex]
    if bagSize and bagSize.item_num > bagSize.show_max then
      GameItemBag:RequestBag(true)
      GameItemBag:InitItemList(true)
    else
      GameItemBag:RequestBag(false)
    end
    if immanentversion == 2 and QuestTutorialEquipAllByOneKey:IsActive() then
      GameStateManager.GameStateBattleMap:SetStoryWhenFocusGain({1100031})
      GameTradeTabBar:showCloseAnim()
    end
    return true
  end
  return false
end
function GameItemBag:DoubClicked()
end
function GameItemBag:OnFSCommand(cmd, arg)
  print("OnFSCommand", cmd, arg)
  if cmd == "popBuyBack" then
    GameObjectBuyBack:show()
  end
  if cmd == "needUpdateListItem" then
    local id = tonumber(arg)
    GameItemBag:UpdateFlashListItem(id)
  end
  if cmd == "beginPress" then
    self.pressIndex = tonumber(arg)
  elseif cmd == "endPress" then
    self.pressIndex = -1
  end
  if cmd == "bagItemReleased" then
    if self.doubClicked then
      return
    end
    local currentReleaasedIndex = tonumber(arg)
    if currentReleaasedIndex == self.lastReleasedIndex and self.clicked then
      self.doubClicked = true
      self.clicked = false
    else
      self.clicked = true
      self.clickedTimer = GameUtils.DOUBLE_CLICK_TIMER
    end
    self.lastReleasedIndex = currentReleaasedIndex
    DebugOut("lastReleasedIndex = " .. self.lastReleasedIndex)
    self.showTipInfoFlag = true
    local index = math.floor(self.lastReleasedIndex / 4) * 4 + 1
    GameItemBag:UpdateFlashListItem(index)
    self.showTipInfoFlag = false
  end
  if cmd == "dragItem" then
    local param = LuaUtils:string_split(arg, "\001")
    local initX, initY, posX, posY = unpack(param)
    self.dragX = tonumber(posX)
    self.dragY = tonumber(posY)
    if self.pressIndex ~= -1 then
      local id = self.pressIndex
      local dummyItem = self.dummyBagList[self.curDummyBagIndex]:GetDummyItem(self.pressIndex)
      local _type = GameDataAccessHelper:GetItemRealType(dummyItem)
      self.dragItem = true
      GameStateManager:GetCurrentGameState():BeginDragItem(id, _type, initX, initY, posX, posY)
    end
  end
  if cmd == "listItemHeadPosRet" then
    DebugOut("listItemHeadPosRet: " .. arg)
    if arg and string.len(arg) > 0 then
      self.m_itemHeadKey, self.m_itemHeadY = unpack(LuaUtils:string_split(arg, "\001"))
      self.m_itemHeadKey = tonumber(self.m_itemHeadKey)
      self.m_itemHeadY = tonumber(self.m_itemHeadY)
    end
  end
end
function GameItemBag:MapEquipedItem(item)
  return tostring(item.fleet_id) .. tostring(item.pos)
end
function GameItemBag:processEquipedItem(items)
  local equipedItems = {}
  for _, v in pairs(items or {}) do
    equipedItems[GameItemBag:MapEquipedItem(v)] = v
  end
  return equipedItems
end
function GameItemBag:updateEquipedItem(items)
  for _, v in pairs(items or {}) do
    local key = GameItemBag:MapEquipedItem(v)
    if v.item_type == 0 then
      GameItemBag.equipedItem[key] = nil
    else
      GameItemBag.equipedItem[key] = v
    end
  end
end
function GameItemBag.RequestBagCallback(msgType, content)
  if msgType == NetAPIList.bag_ack.Code then
    GameItemBag.isRequestBagItem = false
    DebugOut("GameItemBag.RequestBagCallback")
    DebugTable(content)
    DebugStore("GameItemBag.RequestBagCallback(msgType, content)")
    DebugStoreTable(content)
    GameItemBag.itemInBag = GameItemBag:processItemInBag(content.grids)
    GameItemBag:processDummyBags(content.grids, GameItemBag.initList)
    if GameItemBag.initList then
      GameItemBag:RefreshDummyBagList()
      GameItemBag.initList = false
      GameItemBag:InitItemList(false)
    else
      GameItemBag:RefreshBag()
    end
    TutorialQuestManager:UpdateQuestTutorialFirstGetWanaArmor(GameItemBag.itemInBag)
    TutorialQuestManager:UpdateQuestTutorialComboGachaGetHero(GameItemBag.itemInBag)
    return true
  end
  return false
end
function GameItemBag:processItemInBag(bag)
  local destBag = {}
  for k, v in pairs(bag or {}) do
    destBag[v.pos] = v
  end
  return destBag
end
function GameItemBag:GetItemMaxPos(bag)
  local max = 0
  for k, v in pairs(bag or {}) do
    max = math.max(max, v.pos)
  end
  return max
end
function GameItemBag:processDummyBags(bag, clearBag)
  GameItemBag.battleInBag = GameItemBag.battleInBag or DummyItemBag.new(BAG_TITLE_TYPE.TITLE_BATTLE)
  GameItemBag.equipInBag = GameItemBag.equipInBag or DummyItemBag.new(BAG_TITLE_TYPE.TITLE_EQUIP)
  GameItemBag.useableInBag = GameItemBag.useableInBag or DummyItemBag.new(BAG_TITLE_TYPE.TITLE_USE)
  GameItemBag.othersInBag = GameItemBag.othersInBag or DummyItemBag.new(BAG_TITLE_TYPE.TITLE_OTHERS)
  GameItemBag.componentInBag = GameItemBag.componentInBag or DummyItemBag.new(BAG_TITLE_TYPE.TITLE_COMPONENT)
  if not GameItemBag.dummyBagList then
    GameItemBag.dummyBagList = {}
    GameItemBag.dummyBagList[BAG_TITLE_TYPE.TITLE_USE] = GameItemBag.useableInBag
    GameItemBag.dummyBagList[BAG_TITLE_TYPE.TITLE_EQUIP] = GameItemBag.equipInBag
    GameItemBag.dummyBagList[BAG_TITLE_TYPE.TITLE_BATTLE] = GameItemBag.battleInBag
    GameItemBag.dummyBagList[BAG_TITLE_TYPE.TITLE_OTHERS] = GameItemBag.othersInBag
    GameItemBag.dummyBagList[BAG_TITLE_TYPE.TITLE_COMPONENT] = GameItemBag.componentInBag
  end
  GameItemBag.useableInBag:ProcessItemsInbag(bag, clearBag)
  GameItemBag.equipInBag:ProcessItemsInbag(bag, clearBag)
  GameItemBag.battleInBag:ProcessItemsInbag(bag, clearBag)
  GameItemBag.othersInBag:ProcessItemsInbag(bag, clearBag)
  GameItemBag.componentInBag:ProcessItemsInbag(bag, clearBag)
  self:UpdateItemSize()
end
function GameItemBag:updateDummyBags(changedItems)
  GameItemBag.battleInBag = GameItemBag.battleInBag or DummyItemBag.new(BAG_TITLE_TYPE.TITLE_BATTLE)
  GameItemBag.equipInBag = GameItemBag.equipInBag or DummyItemBag.new(BAG_TITLE_TYPE.TITLE_EQUIP)
  GameItemBag.useableInBag = GameItemBag.useableInBag or DummyItemBag.new(BAG_TITLE_TYPE.TITLE_USE)
  GameItemBag.othersInBag = GameItemBag.othersInBag or DummyItemBag.new(BAG_TITLE_TYPE.TITLE_OTHERS)
  GameItemBag.componentInBag = GameItemBag.componentInBag or DummyItemBag.new(BAG_TITLE_TYPE.TITLE_COMPONENT)
  if not GameItemBag.dummyBagList then
    GameItemBag.dummyBagList = {}
    GameItemBag.dummyBagList[BAG_TITLE_TYPE.TITLE_USE] = GameItemBag.useableInBag
    GameItemBag.dummyBagList[BAG_TITLE_TYPE.TITLE_EQUIP] = GameItemBag.equipInBag
    GameItemBag.dummyBagList[BAG_TITLE_TYPE.TITLE_BATTLE] = GameItemBag.battleInBag
    GameItemBag.dummyBagList[BAG_TITLE_TYPE.TITLE_OTHERS] = GameItemBag.othersInBag
    GameItemBag.dummyBagList[BAG_TITLE_TYPE.TITLE_COMPONENT] = GameItemBag.componentInBag
  end
  GameItemBag.useableInBag:updateDummyBags(changedItems)
  GameItemBag.equipInBag:updateDummyBags(changedItems)
  GameItemBag.battleInBag:updateDummyBags(changedItems)
  GameItemBag.othersInBag:updateDummyBags(changedItems)
  GameItemBag.componentInBag:updateDummyBags(changedItems)
  self:UpdateItemSize()
end
function GameItemBag:UpdateItemSize()
  local bagSize = GameItemBag.bagSizeInfo[self.curDummyBagIndex]
  if bagSize and self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "setItemCount", bagSize.item_num, bagSize.show_max, string.gsub(GameLoader:GetGameText("LC_MENU_SAVE_FORMATION_INFO_8"), "<number1>", tostring(bagSize.show_max)))
  end
end
function GameItemBag:ClearDummyBag()
  if self.dummyBagList then
    for k, v in pairs(self.dummyBagList) do
      v:ClearBag()
    end
  end
end
function GameItemBag:ClearItemsNotInRealBag(realBag)
  if self.dummyBagList then
    for k, v in pairs(self.dummyBagList) do
      v:ClearBag(realBag)
    end
  end
end
function GameItemBag:AddEquipmentItem()
  if not self:GetFlashObject() then
    return
  end
  local minItemCount = (self.MAX_LINE - 1) * self.LINE_COUNT + 1
  local maxItemCount = GameItemBag.bagSizeInfo[self.curDummyBagIndex].show_max
  local itemSize = math.max(self.dummyBagList[self.curDummyBagIndex]:GetLastPosInDummyBag(), minItemCount)
  itemSize = math.min(itemSize, maxItemCount)
  for id = 1, itemSize, 4 do
    self:GetFlashObject():InvokeASCallback("_root", "addListItem", id)
  end
  if self.m_itemHeadKey and self.m_itemHeadKey then
    self:SetListItemHeadPos()
  end
end
function GameItemBag:InsertItemList(oldMax)
  if not self:GetFlashObject() then
    return
  end
  local minItemCount = (self.MAX_LINE - 1) * self.LINE_COUNT + 1
  if math.floor(self.dummyBagList[self.curDummyBagIndex]:GetLastPosInDummyBag() / self.LINE_COUNT) <= math.floor(minItemCount / self.LINE_COUNT) then
  else
    for id = math.ceil(oldMax / self.LINE_COUNT) * self.LINE_COUNT + 1, self.dummyBagList[self.curDummyBagIndex]:GetLastPosInDummyBag(), 4 do
      self:GetFlashObject():InvokeASCallback("_root", "insertListItem", id)
    end
  end
end
function GameItemBag:onBeginDragItem(dragitem)
  if not self:GetFlashObject() then
    return
  end
  self.dragItemInstance = dragitem
  local itemId = self.dragItemInstance.DraggedItemID
  self:GetFlashObject():InvokeASCallback("_root", "onDragItem", itemId)
end
function GameItemBag:onEndDragItem()
  self.dragItem = false
  self.dragItemInstance = nil
  self:GetFlashObject():InvokeASCallback("_root", "onEndDragItem")
end
function GameItemBag.callbackSellItem(count)
  DebugOut("Sell Item callbackSellItem:" .. GameItemBag.sellIndex)
  GameItemBag:SellItem(GameItemBag.sellIndex, count)
end
function GameItemBag.gotoFleetInfoUI()
  if not GameTradeTabBar.m_previousState then
    GameTradeTabBar.m_previousState = GameStateTrade.previousState
  end
  ItemBox:OnFSCommand("boxHiden")
  local GameStateEquipEnhance = GameStateManager.GameStateEquipEnhance
  GameStateManager:SetCurrentGameState(GameStateEquipEnhance)
end
function GameItemBag:SellItem(itemSellId, count)
  if self.isOnSell then
    return
  end
  local dummyItem = GameItemBag.dummyBagList[GameItemBag.curDummyBagIndex]:GetDummyItem(itemSellId)
  local item = GameItemBag.itemInBag[dummyItem.pos]
  if item and #item.equip_matrixs > 0 then
    local text_title = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
    GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
    GameUIMessageDialog:SetYesButton(GameItemBag.SellItemCall, itemSellId, count)
    local formations = ""
    local inTlcAtk = false
    local inTlcDef = false
    for i, v in ipairs(item.equip_matrixs) do
      if v >= 1 and v <= 6 then
        formations = formations .. FleetMatrix.GetRomaNumber(v) .. "/"
      elseif v >= 101 and v <= 103 then
        inTlcAtk = true
      elseif v >= 104 and v <= 160 then
        inTlcDef = true
      end
    end
    if inTlcAtk then
      formations = formations .. GameLoader:GetGameText("LC_ALERT_TEAMLEAGUE_ATTACK_FLEETS_CHAR") .. "/"
    end
    if inTlcDef then
      formations = formations .. GameLoader:GetGameText("LC_ALERT_TEAMLEAGUE_DEFENSIVE_FLEETS_CHAR") .. "/"
    end
    formations = string.sub(formations, 1, -2)
    GameUIMessageDialog:Display(text_title, GameLoader:GetGameText("LC_MENU_SAVE_FORMATION_INFO_5") .. formations)
    return
  end
  GameItemBag.SellItemCall(itemSellId, count)
end
function GameItemBag.SellItemCall(itemId, count)
  DebugOut("itemId: ", itemId)
  GameItemBag.sellIndex = itemId
  GameItemBag.isOnSell = true
  local dummyItem = GameItemBag.dummyBagList[GameItemBag.curDummyBagIndex]:GetDummyItem(GameItemBag.sellIndex)
  local sellCount = count or 1
  if sellCount == 0 then
    sellCount = dummyItem.cnt
  end
  DebugOut("SellItem:", sellCount)
  local shop_sell_req_param = {
    grid_owner = GameGlobalData:GetData("userinfo").player_id,
    grid_type = GameFleetInfoBag.BAG_OWNER_USER,
    grid_pos = dummyItem.pos,
    cnt = sellCount
  }
  DebugOut("shop_sell_req_param: ", dummyItem.pos, dummyItem.cnt, count, shop_sell_req_param.cnt)
  local function netFailedCallback()
    GameItemBag.isOnSell = false
    GameStateTrade:onEndDragItem()
    GameItemBag:RequestBag(false)
  end
  NetMessageMgr:SendMsg(NetAPIList.shop_sell_req.Code, shop_sell_req_param, GameItemBag.sellItemCallback, false, netFailedCallback)
end
function GameItemBag:RefreshBag()
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "needUpdateVisibleItem")
  end
end
function GameItemBag.sellItemCallback(msgType, content)
  if msgType == NetAPIList.shop_sell_ack.Code then
    GameItemBag.isOnSell = false
    local bagSize = GameItemBag.bagSizeInfo[GameItemBag.curDummyBagIndex]
    if bagSize and bagSize.item_num > bagSize.show_max then
      GameItemBag:RequestBag(true)
      GameItemBag:InitItemList(true)
    else
      GameItemBag:RequestBag(false)
    end
    if content.code ~= 0 then
      GameTip:Show(AlertDataList:GetTextFromErrorCode(content.code), 3000)
    end
    if GameItemBag.dragItemInstance then
      GameStateTrade:onEndDragItem()
      GameItemBag:GetFlashObject():InvokeASCallback("_root", "showloop")
    end
    return true
  end
  return false
end
function GameItemBag:AddDownloadPath(itemID, itemKey, index)
  DebugStore("Add download path", itemID, itemKey, index)
  local resName = itemID .. ".png"
  local extInfo = {}
  extInfo.itemKey = itemKey
  extInfo.index = index
  extInfo.item_id = itemID
  extInfo.resPath = "data2/LAZY_LOAD_dynamic_" .. resName
  DynamicResDownloader:AddDynamicRes(resName, DynamicResDownloader.resType.PIC, extInfo, GameItemBag.donamicDownloadFinishCallback)
end
function GameItemBag.donamicDownloadFinishCallback(extInfo)
  if DynamicResDownloader:IfResExsit(extInfo.resPath) and GameItemBag:GetFlashObject() then
    local frameName = "item_" .. extInfo.item_id
    DebugStore("goto frame", frameName, extInfo.itemKey, extInfo.index)
    GameItemBag:GetFlashObject():InvokeASCallback("_root", "SetItemFrame", extInfo.itemKey, extInfo.index, frameName)
  end
  return true
end
function GameItemBag.UseItemNotifyHandler(content)
  DebugOut("UseItemNotifyHandler")
  DebugTable(content)
  if content.use_max == 1 then
    local rewards = {}
    local data = GameItemBag.RemoveUseItemData() or {}
    local num = tonumber(data.itemNum or 1)
    for k, v in ipairs(content.award_list) do
      local reward = {}
      reward.item_type = v.item_type
      if GameHelper:IsResource(v.item_type) then
        reward.number = v.number * num
        reward.no = v.no
      else
        reward.no = v.no * num
        reward.number = v.number
      end
      reward.level = v.level
      table.insert(rewards, reward)
    end
    ItemBox:ShowRewardBox(rewards)
    return
  else
    GameItemBag.RemoveUseItemData()
  end
  DebugOut("award_list")
  local output = content.award_list
  for key, v in pairs(output) do
    if GameStateManager:GetCurrentGameState():IsObjectInState(GameObjectAwardCenter) then
      return
    end
    if v.item_type == "fleet" then
      local GameUIPrestigeRankUp = LuaObjectManager:GetLuaObject("GameUIPrestigeRankUp")
      if v.shared_rewards and #v.shared_rewards > 0 then
        if v.item_type == "fleet" then
          GameUIPrestigeRankUp:ShowGetCommander(v, true)
          return
        else
          GameUIPrestigeRankUp:ShowGetItem(v, true)
          return
        end
      else
        GameUIPrestigeRankUp:ShowGetCommander(v, false)
      end
      table.remove(output, key)
    end
  end
  DebugOut("ShowRewardBox")
  DebugOut("in itembag")
  print("ifnotNeedToShowRewardBox~~", GameItemBag.ifnotNeedToShowRewardBox)
  if not GameItemBag.ifnotNeedToShowRewardBox then
    ItemBox:ShowRewardBox(output)
  else
    GameItemBag.ifnotNeedToShowRewardBox = false
  end
  local GameGoodsBuyBox = LuaObjectManager:GetLuaObject("GameGoodsBuyBox")
  GameGoodsBuyBox.isNeedBuySuccessTip = false
end
function GameItemBag:SetCurrentChoosableItems(choosableItems)
  self.choosableItems = choosableItems
end
function GameItemBag:SetCurrentChoosableItemsIndex(index)
  DebugOut("set index" .. index)
  self.choosedItemIndex = index
end
function GameItemBag:OnChangeToDummyBag(bagTab)
  GameItemBag.curDummyBagIndex = bagTab
  if GameItemBag.dummyBagList and GameItemBag.dummyBagList[GameItemBag.curDummyBagIndex] then
    GameItemBag:RefreshDummyBagList()
    GameItemBag:InitItemList(true)
  end
  GameItemBag:UpdateItemSize()
end
function GameItemBag:UpdateFlashListItem(itemIndex)
  local ListItemObjs = {}
  local equipments = GameGlobalData:GetData("equipments")
  for i = 0, 3 do
    do
      local ItemObj = {
        Frame = "empty",
        number = 0,
        Visible = false,
        StrengthenLv = 0
      }
      local subId = itemIndex + i
      local dummyItem
      if self.dummyBagList[self.curDummyBagIndex] then
        dummyItem = self.dummyBagList[self.curDummyBagIndex]:GetDummyItem(subId)
      end
      if dummyItem and not dummyItem.mNotInRealBag then
        local real_type = GameDataAccessHelper:GetItemRealType(dummyItem)
        if DynamicResDownloader:IsDynamicStuff(real_type, DynamicResDownloader.resType.PIC) then
          local fullFileName = DynamicResDownloader:GetFullName(real_type .. ".png", DynamicResDownloader.resType.PIC)
          local localPath = "data2/" .. fullFileName
          if DynamicResDownloader:IfResExsit(localPath) then
            ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
            ItemObj.Frame = tostring("item_" .. real_type)
          else
            ItemObj.Frame = "temp"
            self:AddDownloadPath(real_type, itemIndex, i)
          end
        else
          ItemObj.Frame = tostring("item_" .. real_type)
        end
        local equip_type = tonumber(dummyItem.item_type)
        if equip_type > 100000 and equip_type < 110000 then
          local currentEquipInfo = LuaUtils:table_values(LuaUtils:table_filter(equipments, function(k, v)
            return v.equip_id == dummyItem.item_id
          end))[1]
          ItemObj.number = GameLoader:GetGameText("LC_MENU_Level") .. currentEquipInfo.equip_level
          ItemObj.StrengthenLv = currentEquipInfo.enchant_level
        elseif 1 >= dummyItem.cnt then
          ItemObj.number = 0
        else
          ItemObj.number = dummyItem.cnt
        end
        if id == 1 and subId == 1 and QuestTutorialGetGift:IsActive() and immanentversion == 2 then
          ItemObj.Visible = true
        else
          ItemObj.Visible = false
        end
        if QuestTutorialComboGachaGetHero:IsActive() and not GameItemBag.showTipInfoFlag and TutorialQuestManager.ComboGachaHeroContent and TutorialQuestManager.ComboGachaHeroContent.id == equip_type then
          ItemObj.Visible = true
        else
          ItemObj.Visible = false
        end
      else
        ItemObj.Frame = "empty"
        ItemObj.number = 0
        ItemObj.Visible = false
      end
      table.insert(ListItemObjs, ItemObj)
    end
  end
  DebugOut("needUpdateListItem: ", itemIndex)
  DebugTable(ListItemObjs)
  self:GetFlashObject():InvokeASCallback("_root", "setListItem", itemIndex, ListItemObjs)
end
