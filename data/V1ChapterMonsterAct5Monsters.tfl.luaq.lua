local Monsters = GameData.ChapterMonsterAct5.Monsters
Monsters[501001] = {
  ID = 501001,
  durability = 15610,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[501002] = {
  ID = 501002,
  durability = 12547,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[501003] = {
  ID = 501003,
  durability = 2341,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[501004] = {
  ID = 501004,
  durability = 6423,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[501005] = {
  ID = 501005,
  durability = 6424,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[501006] = {
  ID = 501006,
  durability = 15610,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[501007] = {
  ID = 501007,
  durability = 12548,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[501008] = {
  ID = 501008,
  durability = 2341,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[501009] = {
  ID = 501009,
  durability = 6424,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[501010] = {
  ID = 501010,
  durability = 6424,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[501011] = {
  ID = 501011,
  durability = 15609,
  Avatar = "head23",
  Ship = "ship37"
}
Monsters[501012] = {
  ID = 501012,
  durability = 12547,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[501013] = {
  ID = 501013,
  durability = 2341,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[501014] = {
  ID = 501014,
  durability = 6424,
  Avatar = "head23",
  Ship = "ship37"
}
Monsters[501015] = {
  ID = 501015,
  durability = 6424,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[501016] = {
  ID = 501016,
  durability = 23268,
  Avatar = "head26",
  Ship = "ship39"
}
Monsters[501017] = {
  ID = 501017,
  durability = 12549,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[501018] = {
  ID = 501018,
  durability = 2341,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[501019] = {
  ID = 501019,
  durability = 6424,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[501020] = {
  ID = 501020,
  durability = 6424,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[501021] = {
  ID = 501021,
  durability = 15612,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[501022] = {
  ID = 501022,
  durability = 12549,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[501023] = {
  ID = 501023,
  durability = 2341,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[501024] = {
  ID = 501024,
  durability = 6423,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[501025] = {
  ID = 501025,
  durability = 6424,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[501026] = {
  ID = 501026,
  durability = 15610,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[501027] = {
  ID = 501027,
  durability = 12548,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[501028] = {
  ID = 501028,
  durability = 2341,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[501029] = {
  ID = 501029,
  durability = 6423,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[501030] = {
  ID = 501030,
  durability = 6423,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[501031] = {
  ID = 501031,
  durability = 17148,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[501032] = {
  ID = 501032,
  durability = 13778,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[501033] = {
  ID = 501033,
  durability = 2546,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[501034] = {
  ID = 501034,
  durability = 7038,
  Avatar = "head23",
  Ship = "ship37"
}
Monsters[501035] = {
  ID = 501035,
  durability = 7038,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[501036] = {
  ID = 501036,
  durability = 25572,
  Avatar = "head26",
  Ship = "ship39"
}
Monsters[501037] = {
  ID = 501037,
  durability = 13776,
  Avatar = "head23",
  Ship = "ship37"
}
Monsters[501038] = {
  ID = 501038,
  durability = 2546,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[501039] = {
  ID = 501039,
  durability = 7039,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[501040] = {
  ID = 501040,
  durability = 7039,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[502001] = {
  ID = 502001,
  durability = 17145,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[502002] = {
  ID = 502002,
  durability = 13776,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[502003] = {
  ID = 502003,
  durability = 2546,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[502004] = {
  ID = 502004,
  durability = 7039,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[502005] = {
  ID = 502005,
  durability = 7038,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[502006] = {
  ID = 502006,
  durability = 17145,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[502007] = {
  ID = 502007,
  durability = 13777,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[502008] = {
  ID = 502008,
  durability = 2546,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[502009] = {
  ID = 502009,
  durability = 7038,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[502010] = {
  ID = 502010,
  durability = 7038,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[502011] = {
  ID = 502011,
  durability = 17148,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[502012] = {
  ID = 502012,
  durability = 13777,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[502013] = {
  ID = 502013,
  durability = 2546,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[502014] = {
  ID = 502014,
  durability = 7038,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[502015] = {
  ID = 502015,
  durability = 7039,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[502016] = {
  ID = 502016,
  durability = 25569,
  Avatar = "head26",
  Ship = "ship39"
}
Monsters[502017] = {
  ID = 502017,
  durability = 13778,
  Avatar = "head23",
  Ship = "ship37"
}
Monsters[502018] = {
  ID = 502018,
  durability = 2546,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[502019] = {
  ID = 502019,
  durability = 7038,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[502020] = {
  ID = 502020,
  durability = 7039,
  Avatar = "head23",
  Ship = "ship37"
}
Monsters[502021] = {
  ID = 502021,
  durability = 17146,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[502022] = {
  ID = 502022,
  durability = 13777,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[502023] = {
  ID = 502023,
  durability = 2546,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[502024] = {
  ID = 502024,
  durability = 7038,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[502025] = {
  ID = 502025,
  durability = 7038,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[502026] = {
  ID = 502026,
  durability = 17148,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[502027] = {
  ID = 502027,
  durability = 13777,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[502028] = {
  ID = 502028,
  durability = 2546,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[502029] = {
  ID = 502029,
  durability = 7038,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[502030] = {
  ID = 502030,
  durability = 7038,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[502031] = {
  ID = 502031,
  durability = 18682,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[502032] = {
  ID = 502032,
  durability = 15006,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[502033] = {
  ID = 502033,
  durability = 2750,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[502034] = {
  ID = 502034,
  durability = 7652,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[502035] = {
  ID = 502035,
  durability = 7652,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[502036] = {
  ID = 502036,
  durability = 27876,
  Avatar = "head28",
  Ship = "ship39"
}
Monsters[502037] = {
  ID = 502037,
  durability = 15006,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[502038] = {
  ID = 502038,
  durability = 2751,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[502039] = {
  ID = 502039,
  durability = 7653,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[502040] = {
  ID = 502040,
  durability = 7653,
  Avatar = "head23",
  Ship = "ship37"
}
Monsters[503001] = {
  ID = 503001,
  durability = 11942,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[503002] = {
  ID = 503002,
  durability = 9122,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[503003] = {
  ID = 503003,
  durability = 2751,
  Avatar = "head23",
  Ship = "ship37"
}
Monsters[503004] = {
  ID = 503004,
  durability = 3976,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[503005] = {
  ID = 503005,
  durability = 5202,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[503006] = {
  ID = 503006,
  durability = 11942,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[503007] = {
  ID = 503007,
  durability = 9123,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[503008] = {
  ID = 503008,
  durability = 2751,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[503009] = {
  ID = 503009,
  durability = 3976,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[503010] = {
  ID = 503010,
  durability = 5202,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[503011] = {
  ID = 503011,
  durability = 11943,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[503012] = {
  ID = 503012,
  durability = 9124,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[503013] = {
  ID = 503013,
  durability = 2751,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[503014] = {
  ID = 503014,
  durability = 3976,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[503015] = {
  ID = 503015,
  durability = 5201,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[503016] = {
  ID = 503016,
  durability = 17763,
  Avatar = "head26",
  Ship = "ship39"
}
Monsters[503017] = {
  ID = 503017,
  durability = 9122,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[503018] = {
  ID = 503018,
  durability = 2751,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[503019] = {
  ID = 503019,
  durability = 3976,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[503020] = {
  ID = 503020,
  durability = 5201,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[503021] = {
  ID = 503021,
  durability = 11943,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[503022] = {
  ID = 503022,
  durability = 9124,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[503023] = {
  ID = 503023,
  durability = 2751,
  Avatar = "head23",
  Ship = "ship37"
}
Monsters[503024] = {
  ID = 503024,
  durability = 3976,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[503025] = {
  ID = 503025,
  durability = 5201,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[503026] = {
  ID = 503026,
  durability = 11942,
  Avatar = "head23",
  Ship = "ship37"
}
Monsters[503027] = {
  ID = 503027,
  durability = 9123,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[503028] = {
  ID = 503028,
  durability = 2751,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[503029] = {
  ID = 503029,
  durability = 3976,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[503030] = {
  ID = 503030,
  durability = 5202,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[503031] = {
  ID = 503031,
  durability = 12915,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[503032] = {
  ID = 503032,
  durability = 9860,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[503033] = {
  ID = 503033,
  durability = 2955,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[503034] = {
  ID = 503034,
  durability = 4283,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[503035] = {
  ID = 503035,
  durability = 5612,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[503036] = {
  ID = 503036,
  durability = 19222,
  Avatar = "head27",
  Ship = "ship39"
}
Monsters[503037] = {
  ID = 503037,
  durability = 9860,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[503038] = {
  ID = 503038,
  durability = 2955,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[503039] = {
  ID = 503039,
  durability = 4283,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[503040] = {
  ID = 503040,
  durability = 5611,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[504001] = {
  ID = 504001,
  durability = 12252,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[504002] = {
  ID = 504002,
  durability = 9329,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[504003] = {
  ID = 504003,
  durability = 2955,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[504004] = {
  ID = 504004,
  durability = 4283,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[504005] = {
  ID = 504005,
  durability = 5611,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[504006] = {
  ID = 504006,
  durability = 12250,
  Avatar = "head23",
  Ship = "ship37"
}
Monsters[504007] = {
  ID = 504007,
  durability = 9329,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[504008] = {
  ID = 504008,
  durability = 2956,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[504009] = {
  ID = 504009,
  durability = 4283,
  Avatar = "head23",
  Ship = "ship37"
}
Monsters[504010] = {
  ID = 504010,
  durability = 5611,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[504011] = {
  ID = 504011,
  durability = 12252,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[504012] = {
  ID = 504012,
  durability = 9329,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[504013] = {
  ID = 504013,
  durability = 2955,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[504014] = {
  ID = 504014,
  durability = 4283,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[504015] = {
  ID = 504015,
  durability = 5612,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[504016] = {
  ID = 504016,
  durability = 18226,
  Avatar = "head26",
  Ship = "ship39"
}
Monsters[504017] = {
  ID = 504017,
  durability = 9329,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[504018] = {
  ID = 504018,
  durability = 2955,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[504019] = {
  ID = 504019,
  durability = 4283,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[504020] = {
  ID = 504020,
  durability = 5612,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[504021] = {
  ID = 504021,
  durability = 12250,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[504022] = {
  ID = 504022,
  durability = 9329,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[504023] = {
  ID = 504023,
  durability = 2955,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[504024] = {
  ID = 504024,
  durability = 4283,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[504025] = {
  ID = 504025,
  durability = 5611,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[504026] = {
  ID = 504026,
  durability = 12252,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[504027] = {
  ID = 504027,
  durability = 9330,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[504028] = {
  ID = 504028,
  durability = 2956,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[504029] = {
  ID = 504029,
  durability = 4283,
  Avatar = "head23",
  Ship = "ship37"
}
Monsters[504030] = {
  ID = 504030,
  durability = 5611,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[504031] = {
  ID = 504031,
  durability = 13172,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[504032] = {
  ID = 504032,
  durability = 10025,
  Avatar = "head23",
  Ship = "ship37"
}
Monsters[504033] = {
  ID = 504033,
  durability = 3160,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[504034] = {
  ID = 504034,
  durability = 4590,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[504035] = {
  ID = 504035,
  durability = 6020,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[504036] = {
  ID = 504036,
  durability = 19609,
  Avatar = "head21",
  Ship = "ship39"
}
Monsters[504037] = {
  ID = 504037,
  durability = 10026,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[504038] = {
  ID = 504038,
  durability = 3160,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[504039] = {
  ID = 504039,
  durability = 4590,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[504040] = {
  ID = 504040,
  durability = 6021,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[505001] = {
  ID = 505001,
  durability = 13172,
  Avatar = "head35",
  Ship = "ship39"
}
Monsters[505002] = {
  ID = 505002,
  durability = 10025,
  Avatar = "head35",
  Ship = "ship39"
}
Monsters[505003] = {
  ID = 505003,
  durability = 3160,
  Avatar = "head35",
  Ship = "ship39"
}
Monsters[505004] = {
  ID = 505004,
  durability = 4590,
  Avatar = "head35",
  Ship = "ship39"
}
Monsters[505005] = {
  ID = 505005,
  durability = 6021,
  Avatar = "head35",
  Ship = "ship39"
}
Monsters[505006] = {
  ID = 505006,
  durability = 13173,
  Avatar = "head35",
  Ship = "ship39"
}
Monsters[505007] = {
  ID = 505007,
  durability = 10026,
  Avatar = "head35",
  Ship = "ship39"
}
Monsters[505008] = {
  ID = 505008,
  durability = 3160,
  Avatar = "head35",
  Ship = "ship39"
}
Monsters[505009] = {
  ID = 505009,
  durability = 4590,
  Avatar = "head35",
  Ship = "ship39"
}
Monsters[505010] = {
  ID = 505010,
  durability = 6021,
  Avatar = "head35",
  Ship = "ship39"
}
Monsters[505011] = {
  ID = 505011,
  durability = 13173,
  Avatar = "head35",
  Ship = "ship39"
}
Monsters[505012] = {
  ID = 505012,
  durability = 10026,
  Avatar = "head35",
  Ship = "ship37"
}
Monsters[505013] = {
  ID = 505013,
  durability = 3160,
  Avatar = "head35",
  Ship = "ship39"
}
Monsters[505014] = {
  ID = 505014,
  durability = 4591,
  Avatar = "head35",
  Ship = "ship39"
}
Monsters[505015] = {
  ID = 505015,
  durability = 6021,
  Avatar = "head35",
  Ship = "ship37"
}
Monsters[505016] = {
  ID = 505016,
  durability = 19610,
  Avatar = "head35",
  Ship = "ship39"
}
Monsters[505017] = {
  ID = 505017,
  durability = 10026,
  Avatar = "head35",
  Ship = "ship39"
}
Monsters[505018] = {
  ID = 505018,
  durability = 3160,
  Avatar = "head35",
  Ship = "ship39"
}
Monsters[505019] = {
  ID = 505019,
  durability = 4590,
  Avatar = "head35",
  Ship = "ship39"
}
Monsters[505020] = {
  ID = 505020,
  durability = 6020,
  Avatar = "head35",
  Ship = "ship39"
}
Monsters[505021] = {
  ID = 505021,
  durability = 13172,
  Avatar = "head35",
  Ship = "ship39"
}
Monsters[505022] = {
  ID = 505022,
  durability = 10026,
  Avatar = "head35",
  Ship = "ship39"
}
Monsters[505023] = {
  ID = 505023,
  durability = 3160,
  Avatar = "head35",
  Ship = "ship39"
}
Monsters[505024] = {
  ID = 505024,
  durability = 4590,
  Avatar = "head35",
  Ship = "ship39"
}
Monsters[505025] = {
  ID = 505025,
  durability = 6020,
  Avatar = "head35",
  Ship = "ship39"
}
Monsters[505026] = {
  ID = 505026,
  durability = 13172,
  Avatar = "head35",
  Ship = "ship39"
}
Monsters[505027] = {
  ID = 505027,
  durability = 10026,
  Avatar = "head35",
  Ship = "ship39"
}
Monsters[505028] = {
  ID = 505028,
  durability = 3160,
  Avatar = "head35",
  Ship = "ship39"
}
Monsters[505029] = {
  ID = 505029,
  durability = 4590,
  Avatar = "head35",
  Ship = "ship39"
}
Monsters[505030] = {
  ID = 505030,
  durability = 6021,
  Avatar = "head35",
  Ship = "ship39"
}
Monsters[505031] = {
  ID = 505031,
  durability = 14094,
  Avatar = "head35",
  Ship = "ship39"
}
Monsters[505032] = {
  ID = 505032,
  durability = 10722,
  Avatar = "head35",
  Ship = "ship39"
}
Monsters[505033] = {
  ID = 505033,
  durability = 3365,
  Avatar = "head35",
  Ship = "ship39"
}
Monsters[505034] = {
  ID = 505034,
  durability = 4898,
  Avatar = "head35",
  Ship = "ship39"
}
Monsters[505035] = {
  ID = 505035,
  durability = 6431,
  Avatar = "head35",
  Ship = "ship37"
}
Monsters[505036] = {
  ID = 505036,
  durability = 20991,
  Avatar = "head31",
  Ship = "ship39"
}
Monsters[505037] = {
  ID = 505037,
  durability = 10722,
  Avatar = "head35",
  Ship = "ship39"
}
Monsters[505038] = {
  ID = 505038,
  durability = 3365,
  Avatar = "head35",
  Ship = "ship37"
}
Monsters[505039] = {
  ID = 505039,
  durability = 4897,
  Avatar = "head35",
  Ship = "ship39"
}
Monsters[505040] = {
  ID = 505040,
  durability = 6431,
  Avatar = "head35",
  Ship = "ship39"
}
Monsters[506001] = {
  ID = 506001,
  durability = 14095,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[506002] = {
  ID = 506002,
  durability = 10723,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[506003] = {
  ID = 506003,
  durability = 3365,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[506004] = {
  ID = 506004,
  durability = 4898,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[506005] = {
  ID = 506005,
  durability = 6430,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[506006] = {
  ID = 506006,
  durability = 14095,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[506007] = {
  ID = 506007,
  durability = 10722,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[506008] = {
  ID = 506008,
  durability = 3365,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[506009] = {
  ID = 506009,
  durability = 4898,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[506010] = {
  ID = 506010,
  durability = 6430,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[506011] = {
  ID = 506011,
  durability = 14094,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[506012] = {
  ID = 506012,
  durability = 10723,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[506013] = {
  ID = 506013,
  durability = 3365,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[506014] = {
  ID = 506014,
  durability = 4898,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[506015] = {
  ID = 506015,
  durability = 6431,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[506016] = {
  ID = 506016,
  durability = 20991,
  Avatar = "head26",
  Ship = "ship39"
}
Monsters[506017] = {
  ID = 506017,
  durability = 10722,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[506018] = {
  ID = 506018,
  durability = 3365,
  Avatar = "head23",
  Ship = "ship37"
}
Monsters[506019] = {
  ID = 506019,
  durability = 4898,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[506020] = {
  ID = 506020,
  durability = 6430,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[506021] = {
  ID = 506021,
  durability = 14095,
  Avatar = "head23",
  Ship = "ship37"
}
Monsters[506022] = {
  ID = 506022,
  durability = 10722,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[506023] = {
  ID = 506023,
  durability = 3365,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[506024] = {
  ID = 506024,
  durability = 4897,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[506025] = {
  ID = 506025,
  durability = 6431,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[506026] = {
  ID = 506026,
  durability = 14094,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[506027] = {
  ID = 506027,
  durability = 10722,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[506028] = {
  ID = 506028,
  durability = 3365,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[506029] = {
  ID = 506029,
  durability = 4898,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[506030] = {
  ID = 506030,
  durability = 6430,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[506031] = {
  ID = 506031,
  durability = 15015,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[506032] = {
  ID = 506032,
  durability = 11418,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[506033] = {
  ID = 506033,
  durability = 3570,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[506034] = {
  ID = 506034,
  durability = 5205,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[506035] = {
  ID = 506035,
  durability = 6840,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[506036] = {
  ID = 506036,
  durability = 22373,
  Avatar = "head26",
  Ship = "ship39"
}
Monsters[506037] = {
  ID = 506037,
  durability = 11419,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[506038] = {
  ID = 506038,
  durability = 3570,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[506039] = {
  ID = 506039,
  durability = 5205,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[506040] = {
  ID = 506040,
  durability = 6840,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[507001] = {
  ID = 507001,
  durability = 15015,
  Avatar = "head23",
  Ship = "ship37"
}
Monsters[507002] = {
  ID = 507002,
  durability = 11418,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[507003] = {
  ID = 507003,
  durability = 3570,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[507004] = {
  ID = 507004,
  durability = 5205,
  Avatar = "head23",
  Ship = "ship37"
}
Monsters[507005] = {
  ID = 507005,
  durability = 6840,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[507006] = {
  ID = 507006,
  durability = 15015,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[507007] = {
  ID = 507007,
  durability = 11418,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[507008] = {
  ID = 507008,
  durability = 3570,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[507009] = {
  ID = 507009,
  durability = 5205,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[507010] = {
  ID = 507010,
  durability = 6840,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[507011] = {
  ID = 507011,
  durability = 15015,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[507012] = {
  ID = 507012,
  durability = 11418,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[507013] = {
  ID = 507013,
  durability = 3570,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[507014] = {
  ID = 507014,
  durability = 5205,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[507015] = {
  ID = 507015,
  durability = 6840,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[507016] = {
  ID = 507016,
  durability = 22373,
  Avatar = "head26",
  Ship = "ship39"
}
Monsters[507017] = {
  ID = 507017,
  durability = 11418,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[507018] = {
  ID = 507018,
  durability = 3570,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[507019] = {
  ID = 507019,
  durability = 5205,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[507020] = {
  ID = 507020,
  durability = 6840,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[507021] = {
  ID = 507021,
  durability = 15015,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[507022] = {
  ID = 507022,
  durability = 11418,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[507023] = {
  ID = 507023,
  durability = 3570,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[507024] = {
  ID = 507024,
  durability = 5205,
  Avatar = "head23",
  Ship = "ship37"
}
Monsters[507025] = {
  ID = 507025,
  durability = 6840,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[507026] = {
  ID = 507026,
  durability = 15015,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[507027] = {
  ID = 507027,
  durability = 11419,
  Avatar = "head23",
  Ship = "ship37"
}
Monsters[507028] = {
  ID = 507028,
  durability = 3570,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[507029] = {
  ID = 507029,
  durability = 5205,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[507030] = {
  ID = 507030,
  durability = 6840,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[507031] = {
  ID = 507031,
  durability = 15936,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[507032] = {
  ID = 507032,
  durability = 12115,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[507033] = {
  ID = 507033,
  durability = 3775,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[507034] = {
  ID = 507034,
  durability = 5512,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[507035] = {
  ID = 507035,
  durability = 7250,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[507036] = {
  ID = 507036,
  durability = 23756,
  Avatar = "head5",
  Ship = "ship39"
}
Monsters[507037] = {
  ID = 507037,
  durability = 12114,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[507038] = {
  ID = 507038,
  durability = 3774,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[507039] = {
  ID = 507039,
  durability = 5512,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[507040] = {
  ID = 507040,
  durability = 7249,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[508001] = {
  ID = 508001,
  durability = 15936,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[508002] = {
  ID = 508002,
  durability = 12114,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[508003] = {
  ID = 508003,
  durability = 3775,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[508004] = {
  ID = 508004,
  durability = 5512,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[508005] = {
  ID = 508005,
  durability = 7250,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[508006] = {
  ID = 508006,
  durability = 15936,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[508007] = {
  ID = 508007,
  durability = 12114,
  Avatar = "head23",
  Ship = "ship37"
}
Monsters[508008] = {
  ID = 508008,
  durability = 3774,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[508009] = {
  ID = 508009,
  durability = 5512,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[508010] = {
  ID = 508010,
  durability = 7250,
  Avatar = "head23",
  Ship = "ship37"
}
Monsters[508011] = {
  ID = 508011,
  durability = 15936,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[508012] = {
  ID = 508012,
  durability = 12114,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[508013] = {
  ID = 508013,
  durability = 3775,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[508014] = {
  ID = 508014,
  durability = 5512,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[508015] = {
  ID = 508015,
  durability = 7250,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[508016] = {
  ID = 508016,
  durability = 23756,
  Avatar = "head26",
  Ship = "ship39"
}
Monsters[508017] = {
  ID = 508017,
  durability = 12115,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[508018] = {
  ID = 508018,
  durability = 3774,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[508019] = {
  ID = 508019,
  durability = 5512,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[508020] = {
  ID = 508020,
  durability = 7249,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[508021] = {
  ID = 508021,
  durability = 15937,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[508022] = {
  ID = 508022,
  durability = 12115,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[508023] = {
  ID = 508023,
  durability = 3775,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[508024] = {
  ID = 508024,
  durability = 5512,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[508025] = {
  ID = 508025,
  durability = 7250,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[508026] = {
  ID = 508026,
  durability = 15936,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[508027] = {
  ID = 508027,
  durability = 12114,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[508028] = {
  ID = 508028,
  durability = 3775,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[508029] = {
  ID = 508029,
  durability = 5512,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[508030] = {
  ID = 508030,
  durability = 7250,
  Avatar = "head23",
  Ship = "ship37"
}
Monsters[508031] = {
  ID = 508031,
  durability = 16859,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[508032] = {
  ID = 508032,
  durability = 12812,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[508033] = {
  ID = 508033,
  durability = 3979,
  Avatar = "head23",
  Ship = "ship37"
}
Monsters[508034] = {
  ID = 508034,
  durability = 5820,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[508035] = {
  ID = 508035,
  durability = 7660,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[508036] = {
  ID = 508036,
  durability = 25140,
  Avatar = "head47",
  Ship = "ship39"
}
Monsters[508037] = {
  ID = 508037,
  durability = 12811,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[508038] = {
  ID = 508038,
  durability = 3979,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[508039] = {
  ID = 508039,
  durability = 5819,
  Avatar = "head23",
  Ship = "ship39"
}
Monsters[508040] = {
  ID = 508040,
  durability = 7660,
  Avatar = "head23",
  Ship = "ship39"
}
