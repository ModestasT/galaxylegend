local GameQuestMenu = LuaObjectManager:GetLuaObject("GameQuestMenu")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameVip = LuaObjectManager:GetLuaObject("GameVip")
local QuestTutorialCityHall = TutorialQuestManager.QuestTutorialCityHall
local GameStateMainPlanet = GameStateManager.GameStateMainPlanet
local GameStateGlobalState = GameStateManager.GameStateGlobalState
local QuestTutorialBattleMap = TutorialQuestManager.QuestTutorialBattleMap
local QuestTutorialGetQuestReward = TutorialQuestManager.QuestTutorialGetQuestReward
local QuestTutorialFirstBattle = TutorialQuestManager.QuestTutorialFirstBattle
local QuestTutorialTax = TutorialQuestManager.QuestTutorialTax
local GameUIBarLeft = LuaObjectManager:GetLuaObject("GameUIBarLeft")
local GameUIBarMiddle = LuaObjectManager:GetLuaObject("GameUIBarMiddle")
local GameObjectLevelUp = LuaObjectManager:GetLuaObject("GameObjectLevelUp")
local GameUIBuilding = LuaObjectManager:GetLuaObject("GameUIBuilding")
local GameUIKrypton = LuaObjectManager:GetLuaObject("GameUIKrypton")
local GameUICollect = LuaObjectManager:GetLuaObject("GameUICollect")
local GameUIAffairHall = LuaObjectManager:GetLuaObject("GameUIAffairHall")
local GameUITechnology = LuaObjectManager:GetLuaObject("GameUITechnology")
local GameFleetEnhance = LuaObjectManager:GetLuaObject("GameFleetEnhance")
local GameUIFactory = LuaObjectManager:GetLuaObject("GameUIFactory")
local GameStateKrypton = GameStateManager.GameStateKrypton
local GameStateBattleMap = GameStateManager.GameStateBattleMap
local GameStateEquipEnhance = GameStateManager.GameStateEquipEnhance
local GameStateConfrontation = GameStateManager.GameStateConfrontation
local GameGlobalData = GameGlobalData
local GameStateItem = GameStateManager.GameStateItem
local GameObjectBattleMap = LuaObjectManager:GetLuaObject("GameObjectBattleMap")
local GameObjectMainPlanet = LuaObjectManager:GetLuaObject("GameObjectMainPlanet")
local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
local GameUIActivityNew = LuaObjectManager:GetLuaObject("GameUIActivityNew")
local QuestTutorialComboGacha = TutorialQuestManager.QuestTutorialComboGacha
local QuestTutorialMainTask = TutorialQuestManager.QuestTutorialMainTask
local QuestTutorialBuildStar = TutorialQuestManager.QuestTutorialBuildStar
local GameUIArena = LuaObjectManager:GetLuaObject("GameUIArena")
local GameObjectWelcome = LuaObjectManager:GetLuaObject("GameObjectWelcome")
GameQuestMenu.QUEST_STS_UNACTIVE = 0
GameQuestMenu.QUEST_STS_ACTIVE = 1
GameQuestMenu.QUEST_STS_UNLOOT = 2
GameQuestMenu.QUEST_STS_NOTHINGNESS = 3
GameQuestMenu.MAIN_QUEST = 1
GameQuestMenu.ACTIVITY_QUEST = 2
GameQuestMenu.tabTag = 1
GameQuestMenu.gotoTabTag = 1
GameQuestMenu.activityId = "main"
GameQuestMenu.questType = GameQuestMenu.MAIN_QUEST
GameQuestMenu.LeftQuestType = 1
GameQuestMenu.isFirstShow = true
GameQuestMenu.isWaitForRward = false
GameQuestMenu.normalActivityQuest = {}
local needInitQuestlist = false
local hasInitQuestlist = false
GameQuestMenu.curQuestList = nil
GameQuestMenu.curKryptenList = nil
GameQuestMenu.curTopId = nil
GameQuestMenu.curLeftContent = nil
GameQuestMenu.curType = nil
local isJustMainQuest = true
function GameQuestMenu:OnInitGame()
  GameGlobalData:RegisterDataChangeCallback("user_quest", self.OnUserQuestStateChange)
end
function GameQuestMenu:OnAddToGameState(game_state)
end
function GameQuestMenu:OnEraseFromGameState()
  GameQuestMenu.isFirstShow = true
  self:GetFlashObject():InvokeASCallback("_root", "lua2fs_ClearRewardList")
  self:UnloadFlashObject()
  if GameStateManager:GetCurrentGameState():IsObjectInState(GameObjectBattleMap) then
    GameObjectBattleMap:RefreshEnemyStatus()
  end
  GameQuestMenu:resetData()
end
function GameQuestMenu.OnUserQuestStateChange()
  DebugOut("OnUserQuestStateChange")
  GameQuestMenu:checkIsShowActivityQuest()
  if not GameQuestMenu:GetFlashObject() then
    DebugOut("not GameQuestMenu:GetFlashObject")
    return
  end
end
function GameQuestMenu.CheckIsInArenaState()
  if GameStateManager:GetCurrentGameState() == GameStateManager.GameStateArena then
    return true
  end
  return false
end
function GameQuestMenu:OnFSCommand(cmd, arg)
  DebugOut("GameQuestMenu:OnFSCommand: " .. cmd)
  if cmd == "execute" then
    GameQuestMenu:HideMenu()
    if QuestTutorialFirstBattle:IsActive() then
      AddFlurryEvent("ClickConfirmFirstBattleQuest", {}, 1)
    end
    local quest = GameGlobalData:GetData("user_quest")
    if immanentversion == 2 then
      local quest = GameGlobalData:GetData("user_quest")
      if quest.quest_id > 40 then
        GameQuestMenu:ExecuteCurrentQuest()
      end
    elseif immanentversion == 1 then
      local level_info = GameGlobalData:GetData("levelinfo")
      if level_info.level > 25 then
        GameQuestMenu:ExecuteCurrentQuest()
      end
    end
  elseif cmd == "showQuestTab" then
    DebugOut("showQuestTab", arg)
    if tonumber(arg) == GameQuestMenu.MAIN_QUEST then
      GameQuestMenu.gotoTabTag = tonumber(arg)
      GameQuestMenu:gotoTab(tonumber(arg), "main")
    elseif tonumber(arg) == GameQuestMenu.ACTIVITY_QUEST then
      DebugOut("isJustMainQuest : " .. tostring(isJustMainQuest))
      if isJustMainQuest then
        local msg = GameLoader:GetGameText("LC_MENU_CLOSE_QUEST_INFO")
        if nil == msg or "" == msg then
          msg = "activities unavailable"
        end
        DebugOut("show msg")
        GameTip:Show(msg)
      else
        local item = self.normalActivityQuest[1]
        if item then
          local activityId = item.activity_id
          GameQuestMenu.gotoTabTag = tonumber(arg)
          GameQuestMenu:gotoTab(tonumber(arg), activityId)
        end
      end
    else
      error("logic error")
    end
  elseif cmd == "btnClosePress" then
    if immanentversion == 2 then
      local quest = GameGlobalData:GetData("user_quest")
      DebugTable(quest)
      if quest.status == self.QUEST_STS_UNLOOT then
        GameQuestMenu:RequestReward()
      else
        GameQuestMenu:HideMenu()
      end
    elseif immanentversion == 1 then
      GameQuestMenu:HideMenu()
      if QuestTutorialFirstBattle:IsActive() then
        AddFlurryEvent("ClickConfirmFirstBattleQuest", {}, 1)
      end
    end
    self:resetData()
  elseif cmd == "boxHiden" then
    local quest = GameGlobalData:GetData("user_quest")
    local shouldShowComboGachaTutorial = GameGlobalData:GetFeatureSwitchState("combo_guide")
    if shouldShowComboGachaTutorial and not QuestTutorialComboGacha:IsFinished() and not QuestTutorialComboGacha:IsActive() and quest.quest_id == 11 then
      local param = {state = 1}
      NetMessageMgr:SendMsg(NetAPIList.combo_guide_req.Code, param, GameQuestMenu.ComboGachaCallback, true, nil)
    else
      self.isShowQuestMenu = false
      GameStateGlobalState:EraseObject(GameQuestMenu)
      if not GameStateMainPlanet:IsObjectInState(GameUIBarMiddle) then
        GameStateMainPlanet:CheckShowTutorial()
      end
      GameUIBarLeft:CheckNeedShowQuestTutorial()
    end
    if QuestTutorialMainTask:IsActive() and quest.quest_id == 8 then
      QuestTutorialMainTask:SetFinish(true)
    end
    if not QuestTutorialBuildStar:IsActive() and not QuestTutorialBuildStar:IsFinished() and quest.quest_id == 12 and immanentversion170 == nil then
      QuestTutorialBuildStar:SetActive(true)
      if not GameStateMainPlanet:IsObjectInState(GameUIBarMiddle) then
        GameStateMainPlanet:CheckShowTutorial()
      end
    end
    if not QuestTutorialBuildStar:IsActive() and not QuestTutorialBuildStar:IsFinished() and quest.quest_id == 13 and (immanentversion170 == 4 or immanentversion170 == 5) then
      QuestTutorialBuildStar:SetActive(true)
      if not GameStateMainPlanet:IsObjectInState(GameUIBarMiddle) then
        GameStateMainPlanet:CheckShowTutorial()
      end
    end
  elseif cmd == "GetReward" then
    GameQuestMenu:RequestReward()
  elseif cmd == "updateWDListItem" then
    GameQuestMenu:UpdateWDListItem(tonumber(arg))
  elseif cmd == "showWDListItemAnim" then
    GameQuestMenu:setQuestListHideORShow(tonumber(arg))
  elseif cmd == "receivedWDQuestlistItem" then
    if not self.curQuestList[tonumber(arg)].aniTag or self.curQuestList[tonumber(arg)].aniTag == "show" then
      DebugOut("receivedWDQuestlistItem:in")
      GameQuestMenu:ReciveWDReward(tonumber(arg))
    end
  elseif cmd == "clickedWDQuestListItem" then
    GameQuestMenu:clickedWDQuestListItem(arg)
  elseif cmd == "WDbtnfinish" then
    GameQuestMenu:ReciveWDReward(GameQuestMenu.curTopId)
  elseif cmd == "showleftDetail" then
    GameQuestMenu:clickedWDQuestListItem(GameQuestMenu.curTopId)
  elseif cmd == "needUpdateItem" then
    local index = tonumber(arg)
    local titleText = ""
    local activityId = "none"
    if index == GameQuestMenu.MAIN_QUEST then
      if immanentversion170 == 4 or immanentversion170 == 5 then
        titleText = GameLoader:GetGameText("LC_QUEST170_MAIN_QUEST_TITLE")
      else
        titleText = GameLoader:GetGameText("LC_QUEST_MAIN_QUEST_TITLE")
      end
      activityId = "main"
    else
      local activityIndex = index - 1
      titleText = "quest " .. activityIndex
      local item = self.normalActivityQuest[activityIndex]
      activityId = item.activity_id
      DebugOut("get activity quest:", activityId, item.key_desc)
      titleText = GameLoader:GetGameText(item.key_desc)
    end
    DebugOut("index", index, "titleText", titleText, "activityId", activityId)
    self:GetFlashObject():InvokeASCallback("_root", "lua2fs_UpdateItem", index, titleText, activityId)
  elseif cmd == "questItemReleasedActivityId" then
    GameQuestMenu.activityId = arg
    GameQuestMenu:gotoTab(GameQuestMenu.gotoTabTag, arg)
  elseif cmd == "questItemReleasedTag" then
    GameQuestMenu.gotoTabTag = arg
  end
end
function GameQuestMenu.ComboGachaCallback(msgType, content)
  if msgType == NetAPIList.combo_guide_ack.Code then
    DebugOut("GameQuestMenu-ComboGachaCallback.content:", content)
    QuestTutorialComboGacha:SetActive(true)
    GameUIActivityNew:RequestActivity(true)
    TutorialQuestManager.ComboGachaHeroContent = content
    GameQuestMenu.isShowQuestMenu = false
    GameStateGlobalState:EraseObject(GameQuestMenu)
    if not GameStateMainPlanet:IsObjectInState(GameUIBarMiddle) then
      GameStateMainPlanet:CheckShowTutorial()
    end
    GameUIBarLeft:CheckNeedShowQuestTutorial()
    return true
  end
  return false
end
function GameQuestMenu:ShowQuestMenu()
  DebugOut("GameQuestMenu::ShowQuestMenu", isJustMainQuest)
  if GameQuestMenu.CheckIsInArenaState() then
    DebugOut("GameQuestMenu:CheckIsInArenaState")
    return
  end
  if isJustMainQuest and tonumber(GameQuestMenu.gotoTabTag) == 2 then
    local msg = GameLoader:GetGameText("LC_MENU_CLOSE_QUEST_INFO")
    if nil == msg or "" == msg then
      msg = "activities unavailable"
    end
    DebugOut("show msg")
    GameTip:Show(msg)
    return
  end
  GameQuestMenu:LoadFlashObject()
  local lang = "en"
  if GameSettingData and GameSettingData.Save_Lang then
    local lan = GameSettingData.Save_Lang
    if string.find(lan, "ru") == 1 then
      lang = "ru"
    end
  end
  GameQuestMenu:GetFlashObject():InvokeASCallback("_root", "setJustMainQuest", isJustMainQuest, lang)
  local alliance = GameGlobalData:GetData("alliance")
  GameQuestMenu:checkIsShowActivityQuest()
  if tonumber(GameQuestMenu.gotoTabTag) == 1 then
    DebugOut("GameQuestMenu.MAIN_QUEST")
    GameQuestMenu.tabTag = 1
    local quest = GameGlobalData:GetData("user_quest")
    if quest.status == self.QUEST_STS_ACTIVE then
      NetMessageMgr:SendMsg(NetAPIList.quest_req.Code, nil, GameQuestMenu.DoShowQuestMenu, false)
    end
  else
    DebugOut("goto tag ACTIVITY_QUEST", GameQuestMenu.gotoTabTag)
    local content = {
      type = GameQuestMenu.questType
    }
    NetMessageMgr:SendMsg(NetAPIList.activity_task_list_req.Code, content, GameQuestMenu.RegWdQuestCallback, true, nil)
  end
end
function GameQuestMenu:checkIsShowActivityQuest()
  local quest = GameGlobalData:GetData("user_quest")
  if quest.status == GameQuestMenu.QUEST_STS_UNLOOT then
    DebugOut("goto tab on change = ", GameQuestMenu.gotoTabTag)
    GameQuestMenu.gotoTabTag = 1
    GameQuestMenu.activityId = "main"
    GameQuestMenu.questType = GameQuestMenu.MAIN_QUEST
    return true
  end
  for k, questItem in pairs(GameQuestMenu.normalActivityQuest) do
    for m, task in pairs(questItem.task_list) do
      if task.status == GameQuestMenu.QUEST_STS_UNLOOT then
        GameQuestMenu.gotoTabTag = k + 1
        GameQuestMenu.activityId = questItem.activity_id
        GameQuestMenu.questType = questItem.type
        return true
      end
    end
  end
  return false
end
function GameQuestMenu.DoShowQuestMenu(msgType, content)
  DebugOut("GameQuestMenu::DoShowQuestMenu", msgType)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.quest_req.Code then
    if content.code ~= 0 then
      GameTip:Show(AlertDataList:GetTextFromErrorCode(content.code), 3000)
    else
      if GameStateManager:GetCurrentGameState():IsObjectInState(GameObjectWelcome) then
        GameQuestMenu:UnloadFlashObject()
        return true
      end
      if not GameStateGlobalState:IsObjectInState(GameQuestMenu) then
        GameStateGlobalState:AddObject(GameQuestMenu)
      end
      if needInitQuestlist and not hasInitQuestlist then
        DebugOut("@initQuestList DoShowQuestMenu")
        if GameQuestMenu:GetFlashObject() then
          GameQuestMenu:GetFlashObject():InvokeASCallback("_root", "setJustMainQuest", isJustMainQuest)
        end
        GameQuestMenu:initQuestList()
      end
      GameQuestMenu:selectMainTab()
      GameQuestMenu:RefreshQuestContent()
    end
    return true
  end
  return false
end
function GameQuestMenu:selectMainTab()
  DebugOut("GameQuestMenu::selectMainTab")
  if not self.isShowQuestMenu then
    self.isShowQuestMenu = true
  end
  local quest = GameGlobalData:GetData("user_quest")
  if quest.status == GameQuestMenu.QUEST_STS_UNLOOT then
    return
  end
  local main = GameLoader:GetGameText("LC_QUEST_MAIN_QUEST_TITLE")
  local second = GameLoader:GetGameText("LC_QUEST_WD_QUEST_TITLE_NEW")
  if immanentversion170 == 4 or immanentversion170 == 5 then
    main = GameLoader:GetGameText("LC_QUEST170_MAIN_QUEST_TITLE")
    second = GameLoader:GetGameText("LC_QUEST170_WD_QUEST_TITLE_NEW")
  end
  if isJustMainQuest then
  else
    local item = self.normalActivityQuest[1]
    if item ~= nil then
      local activityId = item.activity_id
      DebugOut("get activity quest:", activityId, item.key_desc)
      second = GameLoader:GetGameText(item.key_desc)
    end
  end
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "selectMainTab", main, second)
  end
end
function GameQuestMenu:HideMenu()
  self:GetFlashObject():InvokeASCallback("_root", "HideMenu")
end
function GameQuestMenu:ShowActivityQuestTab()
  if not GameQuestMenu.curQuestList then
    return
  end
  DebugOut("GameQuestMenu::ShowActivityQuestTab", self.tabTag)
  if not self:GetFlashObject() then
    self:LoadFlashObject()
    GameQuestMenu:GetFlashObject():InvokeASCallback("_root", "setJustMainQuest", isJustMainQuest)
  end
  local rewardText = GameLoader:GetGameText("LC_MENU_CAPTION_RECEIVE")
  local aleadyText = GameLoader:GetGameText("LC_MENU_CLAIMED_BUTTON")
  local main = GameLoader:GetGameText("LC_QUEST_MAIN_QUEST_TITLE")
  if immanentversion170 == 4 or immanentversion170 == 5 then
    main = GameLoader:GetGameText("LC_QUEST170_MAIN_QUEST_TITLE")
  end
  local item = self.normalActivityQuest[1]
  local titleText = ""
  if item then
    titleText = GameLoader:GetGameText(item.key_desc)
  end
  DebugOut("main", main, "sec titleText", titleText)
  self:GetFlashObject():InvokeASCallback("_root", "ShowActivityQuestTab", rewardText, aleadyText, self.tabTag, main, titleText)
  GameQuestMenu:ShowWdQuestList()
end
function GameQuestMenu:RefreshQuestContent()
  DebugOut("GameQuestMenu::RefreshQuestContent")
  local quest = GameGlobalData:GetData("user_quest")
  if quest.status == self.QUEST_STS_UNLOOT then
    return
  end
  local title1, title2, desText = GameLoader:GetGameText("LC_QUEST_MAIN_QUEST_TITLE"), "", GameDataAccessHelper:GetMainQuestDesText(quest.quest_id)
  if immanentversion170 == 4 or immanentversion170 == 5 then
    title1 = GameLoader:GetGameText("LC_QUEST170_MAIN_QUEST_TITLE")
  end
  local countNumber = quest.cond_count
  if quest.cond_count > quest.cond_total then
    countNumber = quest.cond_total
  end
  local condTypeText, condNumText = GameDataAccessHelper:GetMainQuestTitileText(quest.quest_id), tostring(countNumber) .. "/" .. tostring(quest.cond_total)
  local rewardType, rewardNum = "", ""
  for k, v in pairs(quest.loot) do
    rewardType = rewardType .. v.item_type .. "\001"
    rewardNum = rewardNum .. v.number .. "\001"
  end
  local canFinish, canExecute = false, false
  if quest.status == self.QUEST_STS_ACTIVE then
    canExecute = true
  elseif quest.status == self.QUEST_STS_UNLOOT then
    canFinish = true
  end
  local reqText = GameLoader:GetGameText("LC_MENU_REQUIRE_LEVEL_CHAR") .. ":" .. quest.player_level
  DebugOut("fuck_rewardType" .. rewardType)
  self:GetFlashObject():InvokeASCallback("_root", "RefreshContent", title1, title2, desText, condTypeText, condNumText, rewardType, rewardNum, canFinish, canExecute, reqText)
  if QuestTutorialGetQuestReward:IsActive() then
    self:GetFlashObject():InvokeASCallback("_root", "ShowFuncTip")
  else
    self:GetFlashObject():InvokeASCallback("_root", "HideFuncTip")
  end
end
function GameQuestMenu:ExecuteCurrentQuest()
  DebugOut("GameQuestMenu::ExecuteCurrentQuest")
  local quest = GameGlobalData:GetData("user_quest")
  local prompt = quest.prompt
  if tonumber(quest.cond_count) >= tonumber(quest.cond_total) then
    DebugOut("finish this quest.")
    GameUIBarLeft:OnFSCommand("showQuestMenu")
  elseif prompt ~= "undefined" then
    local fun = loadstring(" return " .. prompt)
    local promptTable = fun()
    local command = promptTable[1]
    local param = promptTable[2]
    if command == "building_up" then
      GameUIBuilding:DisplayUpgradeDialog(param[1])
    elseif command == "collect" or command == "collection" then
      if GameGlobalData:BuildingCanEnter(GameUICollect.buildingName) then
        GameUICollect:ShowCollect()
      else
        GameUIBuilding:DisplayUpgradeDialog(GameUICollect.buildingName)
      end
    elseif command == "affairs_hall" then
      GameStateManager.GameStateAffairInfo:RequestInfo()
    elseif command == "recruit" then
      GameStateManager:SetCurrentGameState(GameStateManager.GameStateRecruit)
      GameStateManager.GameStateRecruit:SelectTab(param[1])
    elseif command == "krypton_center" then
      if param[1] == "lab" then
        GameStateKrypton:EnterKrypton(GameStateKrypton.LAB)
      elseif param[1] == "adv_lab" then
        GameStateKrypton:EnterKrypton(GameStateKrypton.ADV_LAB)
      else
        GameStateKrypton:EnterKrypton(GameStateKrypton.KRYPTON)
      end
    elseif command == "tech_lab" then
      GameUITechnology:EnterTechnology()
    elseif command == "equipment" then
      if param[1] == "evolute" then
        GameStateEquipEnhance:EnterEquipEnhance(GameFleetEnhance.TAB_EVOLUTION)
      else
        GameStateEquipEnhance.isNeedShowEnhanceUI = true
        GameStateEquipEnhance:EnterEquipEnhance(GameFleetEnhance.TAB_ENHANCE)
      end
    elseif command == "repair" then
      GameStateManager:GetCurrentGameState():AddObject(GameUIFactory)
    elseif command == "vip_page" then
      if not GameGlobalData:FTPVersion() then
        GameVip:showVip(true)
      end
    elseif command == "arena" then
      GameUIArena:SetArenaType(GameUIArena.ARENA_TYPE.normalArena)
      GameStateManager:SetCurrentGameState(GameStateManager.GameStateArena)
    elseif command == "pve_latest" then
      local progress = GameGlobalData:GetData("progress")
      GameStateBattleMap:EnterSection(progress.act, progress.chapter, nil)
    elseif command == "pve" then
      GameStateBattleMap:EnterSection(param[1], param[2], nil)
    elseif command == "academy" then
      GameStateConfrontation:EnterConfrontation()
    elseif command == "equip_enhance" then
      local GameStateEquipEnhance = GameStateManager.GameStateEquipEnhance
      local GameFleetEnhance = LuaObjectManager:GetLuaObject("GameFleetEnhance")
      local QuestTutorialEnhance = TutorialQuestManager.QuestTutorialEnhance
      if QuestTutorialEnhance:IsActive() then
        GameStateEquipEnhance.isNeedShowEnhanceUI = true
        GameStateEquipEnhance:EnterEquipEnhance()
      else
        GameStateEquipEnhance:EnterEquipEnhance(GameFleetEnhance.TAB_ENHANCE)
      end
    end
  end
end
function GameQuestMenu:gotoTab(tag, activityId)
  DebugOut("GameQuestMenu::gotoTab", tag, activityId)
  DebugOut("current", GameQuestMenu.tabTag, GameQuestMenu.activityId)
  tag = tonumber(tag)
  if tag == GameQuestMenu.tabTag and GameQuestMenu.activityId == activityId then
    DebugOut("same tab")
    return
  end
  tag = GameQuestMenu.MAIN_QUEST
  if tonumber(tag) == GameQuestMenu.MAIN_QUEST then
    DebugOut("GameQuestMenu:gotoTab goto tag MAIN_QUEST")
    GameQuestMenu:selectMainTab()
    GameQuestMenu:RefreshQuestContent()
  else
    DebugOut("GameQuestMenu:gotoTab goto tag ACTIVITY_QUEST")
    if GameQuestMenu.curQuestList and not GameQuestMenu.isFirstShow then
      GameQuestMenu:ShowActivityQuestTab()
    else
      local item = self.normalActivityQuest[tag - 1]
      local content = {
        type = item.type
      }
      NetMessageMgr:SendMsg(NetAPIList.activity_task_list_req.Code, content, GameQuestMenu.RegWdQuestCallback, true, nil)
    end
    GameQuestMenu.isFirstShow = false
  end
end
function GameQuestMenu.RegWdQuestCallback(msgType, content)
  DebugOut("GameQuestMenu::RegWdQuestCallback", msgType)
  if msgType == NetAPIList.activity_task_list_ack.Code then
    if not GameStateGlobalState:IsObjectInState(GameQuestMenu) then
      GameStateGlobalState:AddObject(GameQuestMenu)
    end
    DebugOut("request wd call back", GameQuestMenu.gotoTabTag, GameQuestMenu.activityId)
    GameQuestMenu:ParseActivityQuestData(content)
    GameQuestMenu:ChecktoCurrentWdData(GameQuestMenu.gotoTabTag, GameQuestMenu.activityId)
    if needInitQuestlist and not hasInitQuestlist then
      DebugOut("@initQuestList RegWdQuestCallback")
      if GameQuestMenu:GetFlashObject() then
        GameQuestMenu:GetFlashObject():InvokeASCallback("_root", "setJustMainQuest", isJustMainQuest)
      end
      GameQuestMenu:initQuestList()
    end
    GameQuestMenu:ShowActivityQuestTab()
    return true
  end
  return false
end
function GameQuestMenu:ParseActivityQuestData(content)
  DebugOut("GameQuestMenu:ParseActivityQuestData", #content.activity_task_list)
  GameQuestMenu.normalActivityQuest = content.activity_task_list
  if GameQuestMenu.normalActivityQuest == nil then
    GameQuestMenu.normalActivityQuest = {}
  end
end
function GameQuestMenu:ChecktoCurrentWdData(tag, activityId)
  DebugOut("ChecktoCurrentWdData", tag, activityId)
  tag = tonumber(tag)
  if tag == GameQuestMenu.MAIN_QUEST then
    DebugOut("main tag,nothing to do")
    GameQuestMenu.tabTag = tag
    return
  else
    local questItem = GameQuestMenu.normalActivityQuest[tag - 1]
    if questItem == nil then
      DebugOut("get nil currentItem", activityId)
      return false
    end
    GameQuestMenu.curQuestList = LuaUtils:table_rcopy(questItem.task_list)
    GameQuestMenu.curKryptenList = questItem.kryptons
    GameQuestMenu.curTopId = questItem.special_id
    GameQuestMenu.curLeftContent = {}
    GameQuestMenu.curType = questItem.type
    if GameQuestMenu.curQuestList ~= nil then
      for k, v in pairs(GameQuestMenu.curQuestList) do
        if v.id == GameQuestMenu.curTopId then
          GameQuestMenu.curLeftContent = v
          table.remove(GameQuestMenu.curQuestList, k)
        end
      end
    end
    GameQuestMenu:RemoveMoreTypeData()
  end
  DebugOut("ChecktoCurrentWdData:", tag)
  GameQuestMenu.tabTag = tag
  GameQuestMenu.activityId = activityId
  DebugOut("set result:", GameQuestMenu.tabTag, GameQuestMenu.activityId)
  return true
end
function GameQuestMenu.ActivityQuestListNtf(content)
  DebugOut("GameQuestMenu.ActivityQuestListNtf")
  DebugTable(content)
  GameQuestMenu:ParseActivityQuestData(content)
  if GameQuestMenu.isWaitForRward then
    GameQuestMenu:ChecktoCurrentWdData(GameQuestMenu.gotoTabTag, GameQuestMenu.activityId)
    GameQuestMenu:ShowActivityQuestTab()
    GameUIBarLeft.UpdateQuestStatus()
    GameQuestMenu.isWaitForRward = false
  else
    if GameQuestMenu:checkIsShowActivityQuest() then
    else
      GameQuestMenu.gotoTabTag = GameQuestMenu.MAIN_QUEST
      GameQuestMenu.activityId = "main"
    end
    DebugOut("goto tab on change = ", GameQuestMenu.gotoTabTag)
    GameQuestMenu:ChecktoCurrentWdData(GameQuestMenu.gotoTabTag, GameQuestMenu.activityId)
    if #GameQuestMenu.normalActivityQuest == 0 then
      DebugOut("no normalActivityQuest")
      isJustMainQuest = true
      return
    else
      isJustMainQuest = false
    end
    GameUIBarLeft.UpdateQuestStatus()
    if #GameQuestMenu.normalActivityQuest > 1 then
      DebugOut("GameQuestMenu.normalActivityQuest", #GameQuestMenu.normalActivityQuest)
      needInitQuestlist = true
      DebugOut("@initQuestList ActivityQuestListNtf")
      if GameQuestMenu:GetFlashObject() then
        GameQuestMenu:GetFlashObject():InvokeASCallback("_root", "setJustMainQuest", isJustMainQuest)
      end
      GameQuestMenu:initQuestList()
    else
      needInitQuestlist = false
      if GameStateGlobalState:IsObjectInState(GameQuestMenu) then
        DebugOut("GameStateGlobalState:IsObjectInState true")
        GameQuestMenu:ShowActivityQuestTab()
      end
    end
  end
end
function GameQuestMenu:RemoveMoreTypeData()
  DebugOut("GameQuestMenu::RemoveMoreTypeData")
  local allWDtype = {}
  for k, v in pairs(GameQuestMenu.curQuestList) do
    if #allWDtype > 0 then
      local isExits = false
      for k1, v1 in pairs(allWDtype) do
        if v.type == v1 then
          isExits = true
        end
      end
      if not isExits then
        table.insert(allWDtype, v.type)
      end
    else
      table.insert(allWDtype, v.type)
    end
  end
  for k1, v1 in pairs(allWDtype) do
    local minValue = 99999999
    local indexTemp = 0
    for k, v in pairs(GameQuestMenu.curQuestList) do
      if minValue > tonumber(v.id) and v.status < 3 and v.type == v1 then
        indexTemp = k
        minValue = v.id
      end
    end
    for i = #GameQuestMenu.curQuestList, 1, -1 do
      if GameQuestMenu.curQuestList[i].type == v1 and i ~= indexTemp then
        table.remove(GameQuestMenu.curQuestList, i)
      end
    end
  end
end
function GameQuestMenu:Update(dt)
  if self:GetFlashObject() then
    self:GetFlashObject():Update(dt)
  end
end
function GameQuestMenu:ShowWdQuestList()
  DebugOut("GameQuestMenu::ShowWdQuestList")
  local quest = GameGlobalData:GetData("user_quest")
  if quest.status == self.QUEST_STS_UNLOOT then
    return
  end
  if GameQuestMenu.curLeftContent and GameQuestMenu.curLeftContent.id then
    local desText = string.format(GameDataAccessHelper:GetWDQuestDescText(GameQuestMenu.curLeftContent.type), GameQuestMenu.curLeftContent.limit_value)
    local btnStatus = GameQuestMenu.curLeftContent.status
    local no_tag = GameQuestMenu.curLeftContent.reward_list[1].no
    local num_tag = GameQuestMenu.curLeftContent.reward_list[1].number
    local item_type_tag = GameQuestMenu.curLeftContent.reward_list[1].item_type
    local item_name = GameQuestMenu:GetSltItemDetailName(item_type_tag, num_tag, no_tag)
    local item_type = GameUIActivityNew:getSignItemDetailInfo(item_type_tag, num_tag, no_tag)
    if item_type_tag == "technique" or item_type_tag == "credit" or item_type_tag == "money" or item_type_tag == "ac_supply" then
      no_tag = 0
      num_tag = GameUtils.numberConversion(num_tag)
    else
      num_tag = 0
    end
    self:GetFlashObject():InvokeASCallback("_root", "upWDleftContent", desText, false, btnStatus, item_type, no_tag, item_name, num_tag)
  else
    DebugOut("no GameQuestMenu.curLeftContent data")
    self:GetFlashObject():InvokeASCallback("_root", "upWDleftContent", "", false, 100, "", "", "", "")
  end
  local forceShow = false
  if GameQuestMenu.curQuestList then
    for k, v in pairs(GameQuestMenu.curQuestList) do
      if v.status == 2 and not forceShow then
        forceShow = true
        self:GetFlashObject():InvokeASCallback("_root", "addWdQuestlist", k, true)
      else
        self:GetFlashObject():InvokeASCallback("_root", "addWdQuestlist", k, false)
      end
    end
  end
end
function GameQuestMenu:ReciveWDReward(arg)
  DebugOut("GameQuestMenu::ReciveWDReward", arg)
  local item
  if arg == GameQuestMenu.curTopId then
    if GameQuestMenu.curLeftContent and GameQuestMenu.curLeftContent.status == 2 then
      item = GameQuestMenu.curLeftContent
    end
  else
    DebugOut("2")
    DebugTable(self.curQuestList[arg])
    if self.curQuestList[arg] and self.curQuestList[arg].status == 2 then
      item = self.curQuestList[arg]
    end
  end
  if item then
    DebugTable(item)
    local content = {
      type = GameQuestMenu.curType,
      task_id = item.id,
      activity_id = tonumber(GameQuestMenu.activityId)
    }
    DebugOut("ReciveWDReward", GameQuestMenu.activityId)
    DebugTable(content)
    NetMessageMgr:SendMsg(NetAPIList.activity_task_reward_req.Code, content, GameQuestMenu.ReciveWDRewardCallback, true, nil)
    GameQuestMenu.isWaitForRward = true
  end
end
function GameQuestMenu.ReciveWDRewardCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.activity_task_reward_req.Code then
    DebugOut("RequestRewardCallback msgType: ", msgType, " content: ", content.api, content.code)
    if content.code ~= 0 then
      GameTip:Show(AlertDataList:GetTextFromErrorCode(content.code), 3000)
    else
    end
    return true
  end
  return false
end
function GameQuestMenu:UpdateWDListItem(index, updateAfterAnim)
  DebugOut("GameQuestMenu::UpdateWDListItem", index)
  local item = self.curQuestList[index]
  if not updateAfterAnim then
    self.curQuestList[index].aniTag = nil
  end
  if item then
    local titleText = GameDataAccessHelper:GetWDQuestTitleText(item.type)
    local progressText = item.value .. "/" .. item.limit_value
    DebugOut("top quest info:", GameDataAccessHelper:GetWDQuestDescText(item.type))
    local desText = string.format(tostring(GameDataAccessHelper:GetWDQuestDescText(item.type)), tostring(item.limit_value))
    local numStr = ""
    local picStr = ""
    local btnStatus = item.status
    for i = 1, 3 do
      if item.reward_list[i] then
        picStr = picStr .. GameUIActivityNew:getSignItemDetailInfoWithCallback(item.reward_list[i], GameQuestMenu.RequestIconDataCallback) .. "\001"
        numStr = numStr .. GameQuestMenu:GetSltItemDetailInfo(item.reward_list[i].item_type, item.reward_list[i].number, item.reward_list[i].no) .. "\001"
      else
        picStr = picStr .. "not" .. "\001"
        numStr = numStr .. "not" .. "\001"
      end
    end
    self:GetFlashObject():InvokeASCallback("_root", "upWDListItem", index, titleText, progressText, desText, numStr, picStr, btnStatus)
  else
    DebugOut("get no item info")
  end
end
function GameQuestMenu:setQuestListHideORShow(arg)
  DebugOut("GameQuestMenu::setQuestListHideORShow", arg)
  if not self.curQuestList[arg].aniTag or self.curQuestList[arg].aniTag == "show" then
    self.curQuestList[arg].aniTag = "hide"
  else
    self.curQuestList[arg].aniTag = "show"
  end
  self:GetFlashObject():InvokeASCallback("_root", "setHideorShow", arg, self.curQuestList[arg].aniTag)
  GameQuestMenu:UpdateWDListItem(arg, true)
end
function GameQuestMenu:clickedWDQuestListItem(arg)
  DebugOut("GameQuestMenu::clickedWDQuestListItem", arg)
  local item_indexs = LuaUtils:string_split(arg, "\001")
  local item
  DebugOut("arg ===== ", arg, GameQuestMenu.curTopId, GameQuestMenu.curTopId == arg)
  if GameQuestMenu.curTopId == arg then
    item = GameQuestMenu.curLeftContent.reward_list[1]
  elseif not self.curQuestList[tonumber(item_indexs[1])].aniTag or self.curQuestList[tonumber(item_indexs[1])].aniTag == "show" then
    item = GameQuestMenu.curQuestList[tonumber(item_indexs[1])].reward_list[tonumber(item_indexs[2])]
  end
  if item and item.item_type == "krypton" then
    for i, v in pairs(GameQuestMenu.curKryptenList) do
      if tonumber(v.id) == item.number then
        GameUIActivityNew:showItemDetil(v, "krypton")
        break
      end
    end
  elseif item then
    GameUIActivityNew:showItemDetil(item, item.item_type)
  end
end
function GameQuestMenu:GetSltItemDetailInfo(itemType, number, no)
  DebugOut("---start-", itemType, number, no)
  local name = itemType
  local rewards = tonumber(number)
  no = no or 1
  if name == "fleet" then
    name = GameLoader:GetGameText("LC_NPC_" .. GameDataAccessHelper:GetCommanderName(rewards))
    rewards = no
  elseif name == "item" or name == "krypton" or name == "equip" then
    name = GameLoader:GetGameText("LC_ITEM_ITEM_NAME_" .. number)
    rewards = no
  elseif name == "technique" then
    rewards = GameUtils.numberConversion(rewards)
    name = GameLoader:GetGameText("LC_MENU_LOOT_TECHPOINT")
  elseif name == "expedition" then
    rewards = GameUtils.numberConversion(rewards)
    name = GameLoader:GetGameText("LC_MENU_LOOT_EXPEDITION")
  elseif name == "credit" then
    rewards = GameUtils.numberConversion(rewards)
    name = GameLoader:GetGameText("LC_MENU_LOOT_CREDIT")
  elseif name == "money" then
    rewards = GameUtils.numberConversion(rewards)
    name = GameLoader:GetGameText("LC_MENU_LOOT_CUBIT")
  elseif name == "vip_exp" then
    rewards = GameUtils.numberConversion(rewards)
    name = GameLoader:GetGameText("LC_MENU_VIP_EXP_CHAR")
  elseif name == "ac_supply" then
    rewards = GameUtils.numberConversion(rewards)
    name = GameLoader:GetGameText("LC_MENU_AC_SUPPLY_CHAR")
  else
    rewards = GameUtils.numberConversion(rewards)
  end
  DebugOut(" name = ", name, "rewards = ", rewards)
  return rewards
end
function GameQuestMenu:GetSltItemDetailName(itemType, number, no)
  DebugOut("---start-", itemType, number, no)
  local name = itemType
  local rewards = tonumber(number)
  no = no or 1
  if name == "fleet" then
    name = GameLoader:GetGameText("LC_NPC_" .. GameDataAccessHelper:GetCommanderName(rewards))
    rewards = no
  elseif name == "item" or name == "krypton" or name == "equip" then
    name = GameLoader:GetGameText("LC_ITEM_ITEM_NAME_" .. number)
    rewards = no
  elseif name == "technique" then
    rewards = GameUtils.numberConversion(rewards)
    name = GameLoader:GetGameText("LC_MENU_LOOT_TECHPOINT")
  elseif name == "expedition" then
    rewards = GameUtils.numberConversion(rewards)
    name = GameLoader:GetGameText("LC_MENU_LOOT_EXPEDITION")
  elseif name == "credit" then
    rewards = GameUtils.numberConversion(rewards)
    name = GameLoader:GetGameText("LC_MENU_LOOT_CREDIT")
  elseif name == "money" then
    rewards = GameUtils.numberConversion(rewards)
    name = GameLoader:GetGameText("LC_MENU_LOOT_CUBIT")
  elseif name == "vip_exp" then
    rewards = GameUtils.numberConversion(rewards)
    name = GameLoader:GetGameText("LC_MENU_VIP_EXP_CHAR")
  elseif name == "ac_supply" then
    rewards = GameUtils.numberConversion(rewards)
    name = GameLoader:GetGameText("LC_MENU_AC_SUPPLY_CHAR")
  else
    rewards = GameUtils.numberConversion(rewards)
  end
  DebugOut(" name = ", name, "rewards = ", rewards)
  return name
end
if AutoUpdate.isAndroidDevice then
  function GameQuestMenu.OnAndroidBack()
    GameQuestMenu:HideMenu()
  end
end
function GameQuestMenu:initQuestList()
  DebugOut("GameQuestMenu:initQuestList")
  if GameQuestMenu.normalActivityQuest == nil or #GameQuestMenu.normalActivityQuest == 0 then
    DebugOut("GameQuestMenu:initQuestList error:normalActivityQuest")
    return
  end
  local count = #GameQuestMenu.normalActivityQuest + 1
  if not self:GetFlashObject() then
    DebugOut("haven't load flash")
    return
  else
    DebugOut("initQuestListBox", count, self.tabTag, GameQuestMenu.gotoTabTag)
    self:GetFlashObject():InvokeASCallback("_root", "lua2fs_initQuestListBox", count, GameQuestMenu.gotoTabTag)
    hasInitQuestlist = true
  end
end
function GameQuestMenu.RequestIconDataCallback(extInfo)
  DebugOut("RequestIconDataCallback")
  local rewardItem = extInfo.rewardItem
  local icon = GameUIActivityNew:getSignItemDetailInfo(rewardItem.item_type, rewardItem.number, rewardItem.no)
  if icon ~= "temp" then
    GameQuestMenu:ShowActivityQuestTab()
  else
    DebugOut("can not download icon")
  end
end
function GameQuestMenu:resetData()
  DebugOut("resetData")
  GameQuestMenu.tabTag = 1
  GameQuestMenu.gotoTabTag = 1
  GameQuestMenu.activityId = "main"
  GameQuestMenu.questType = GameQuestMenu.MAIN_QUEST
  hasInitQuestlist = false
  GameQuestMenu.isWaitForRward = false
end
