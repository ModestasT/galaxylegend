TutorialQuestManager = {
  TutorialBattleMap = 1,
  TutorialFormation = 2,
  TutorialEquip = 4,
  TutorialEngineer = 6,
  TutorialEnhance = 7,
  TutorialCityHall = 8,
  TutorialTax = 9,
  TutorialFirstBattle = 13,
  TutorialStar = 15,
  TutorialStarCharge = 16,
  TutorialStarRecruit = 17,
  TutorialBattleMapAfterRecruotIndex = 20,
  TutorialBuildAcademy = 21,
  TutorialQuestCenter = 22,
  TutorialGetQuestReward = 23,
  TutorialKillBoss = 25,
  TutorialBuildTechLab = 26,
  TutorialBuildFactory = 27,
  TutorialBuildKryptonCenter = 28,
  TutorialBuildAffairsCenter = 29,
  TutorialUseTechLab = 30,
  TutorialUseFactory = 31,
  TutorialUseKrypton = 32,
  TutorialUseAffairs = 33,
  TutorialsFirstKillBoss = 34,
  TutorialsChangeFleets = 35,
  TutorialsChangeFleets1 = 36,
  TutorialsPrestige = 37,
  TutorialCollect = 38,
  TutorialMine = 39,
  TutorialUseSkill = 40,
  TutorialEquipKrypton = 41,
  TutorialEnhanceKrypton = 42,
  TutorialRepairFleet = 43,
  TutorialSlot = 44,
  TutorialArena = 45,
  TutorialGetGift = 46,
  TutorialNewBossArena = 47,
  TutorialEnhance_second = 48,
  TutorialEnhance_third = 49,
  TutorialRechargePower = 50,
  TutorialBattleMap_second = 51,
  TutorialBattleFailed = 52,
  TutorialBattleRush = 53,
  TutorialArenaReward = 54,
  TutorialGameSpeed = 55,
  TutorialNewFunctonAC = 56,
  TutorialNewFunctonWorldboss = 57,
  TutorialColonial = 58,
  TutorialEnhanceEquipFast = 59,
  TutorialEquipAllByOneKey = 60,
  TutorialMakeUserLevel10 = 61,
  TutorialWD = 62,
  TutorialSecondHeroLoadEQU = 63,
  SaveFirstCharge = 64,
  TutorialFirstGetWanaArmor = 65,
  TutorialFirstGetSilvaEquip = 66,
  TutorialSign = 67,
  TutorialRefine = 68,
  TutorialWVE = 69,
  TutorialEquipmentEvolution = 70,
  TutorialRush = 71,
  TutorialInfiniteCosmos = 72,
  TutorialComboGacha = 73,
  TutorialComboGachaGetHero = 74,
  TutorialComboGachaUseHero = 75,
  TutorialAdjutantStoryPlay = 76,
  TutorialAdjutantBind = 77,
  TutorialInfiniteJumpStep = 78,
  TutorialTacticsCenter = 79,
  TutorialTacticsRefine = 80,
  TutorialPveMapPoint = 81,
  TutorialMainTask = 82,
  TutorialTheFirstEvent = 83,
  TutorialFirstChapter = 84,
  TutorialFirstReform = 85,
  TutorialFactoryRemodel = 86,
  TutorialBuildLab = 87,
  TutorialBuildArena = 88,
  TutorialHelper = 89,
  TutorialPrimeWve = 90,
  TutorialStarwar = 91,
  TutorialEnterStarSystem = 92,
  TutorialEquipStarSystemItem = 93,
  TutorialEngineerBarMove = 94,
  TutorialStarSystemTujian = 95,
  TutorialStarSystemSkipAnim = 96,
  TutorialTlcMainUiEntry = 97,
  TutorialTlcArenaEntry = 98,
  TutorialTlcSearch = 99,
  TutorialTlcChallenge = 100,
  TutorialTlcSwitchTeam = 101,
  TutorialSkipAll = 0,
  Quests = {},
  saveVer = "1.0"
}
TutorialQuestManager.m_commanderLyonsID = 2
function TutorialQuestManager:TutorialQuestsCheck()
  if not GameGlobalData.GlobalData.userinfo then
    return
  end
  local progress = GameGlobalData:GetData("progress")
  DebugOut("TutorialQuestManager:TutorialQuestsCheck")
  DebugTable(progress)
  if self.QuestTutorialBattleMap:IsActive() and (progress.chapter > 1 or progress.finish_count > 0) then
    self.QuestTutorialBattleMap:SetFinish(true)
    if not self.QuestTutorialMainTask:IsFinished() and not self.QuestTutorialMainTask:IsActive() then
      self.QuestTutorialMainTask:SetActive(true)
    end
  end
  if not self.QuestTutorialArena:IsFinished() and (1 < progress.act or progress.chapter > 2) and not self.QuestTutorialArena:IsActive() and not self.QuestTutorialArena:IsFinished() and immanentversion == 1 then
    self.QuestTutorialArena:SetActive(true)
  end
  if self.QuestTutorialEngineer:IsActive() then
    local engineerBuildInfo = GameGlobalData:GetBuildingInfo("engineering_bay")
    TutorialQuestManager.QuestTutorialEngineerBarMove:SetFinish(true)
    if 1 <= engineerBuildInfo.level then
      self.QuestTutorialEngineer:SetFinish(true)
      if not self.QuestTutorialEnhance:IsFinished() then
        self.QuestTutorialEnhance:SetActive(true)
      end
    end
  end
  if self.QuestTutorialEnhance:IsActive() then
    local equipments = GameGlobalData:GetData("equipments")
    for k, v in pairs(equipments) do
      if v.equip_level >= 3 then
        self.QuestTutorialEnhance:SetFinish(true)
        break
      end
    end
  end
  if self.QuestTutorialCityHall:IsActive() then
    local cityHall = GameGlobalData:GetBuildingInfo("planetary_fortress")
    if 0 < cityHall.level then
      self.QuestTutorialCityHall:SetFinish(true)
      if not self.QuestTutorialGetQuestReward:IsFinished() then
        self.QuestTutorialGetQuestReward:SetActive(true)
      end
    end
  end
  if self.QuestTutorialFirstBattle:IsActive() then
    local progress = GameGlobalData:GetData("progress")
    if progress.chapter > 1 or progress.finish_count > 0 then
      self.QuestTutorialFirstBattle:SetFinish(true)
    end
  end
  if self.QuestTutorialBuildStar:IsActive() then
    local star = GameGlobalData:GetBuildingInfo("star_portal")
    if 0 < star.level then
      self.QuestTutorialBuildStar:SetFinish(true)
      if not self.QuestTutorialStarCharge:IsFinished() then
        self.QuestTutorialRecruit:SetActive(true)
      end
    end
  end
  if immanentversion == 1 and self.QuestTutorialRecruit:IsActive() then
    local fleets = GameGlobalData:GetData("fleetinfo").fleets
    if #fleets > 6 then
      self.QuestTutorialRecruit:SetFinish(true)
    end
    if #fleets > 2 and not self.QuestTutorialBattleMapAfterRecruit:IsFinished() then
      self.QuestTutorialBattleMapAfterRecruit:SetActive(true)
    end
  end
  if self.QuestTutorialBuildAcademy:IsActive() then
    local academy = GameGlobalData:GetBuildingInfo("commander_academy")
    if 0 < academy.level then
      self.QuestTutorialBuildAcademy:SetFinish(true)
      if not self.QuestTutorialsFirstKillBoss:IsActive() and not self.QuestTutorialsFirstKillBoss:IsFinished() then
        self.QuestTutorialsFirstKillBoss:SetActive(true)
      end
    end
  end
  if self.QuestTutorialBuildTechLab:IsActive() then
    local techlab = GameGlobalData:GetBuildingInfo("tech_lab")
    if 0 < techlab.level then
      self.QuestTutorialBuildTechLab:SetFinish(true)
      if not self.QuestTutorialUseTechLab:IsFinished() then
        self.QuestTutorialUseTechLab:SetActive(true)
      end
    end
  end
  if self.QuestTutorialBuildFactory:IsActive() then
    local factory = GameGlobalData:GetBuildingInfo("factory")
    if 0 < factory.level then
      self.QuestTutorialBuildFactory:SetFinish(true)
      if (immanentversion170 == 4 or immanentversion170 == 5) and not self.QuestTutorialFactoryRemodel:IsFinished() and not self.QuestTutorialFactoryRemodel:IsActive() then
        local GameUICommonDialog = LuaObjectManager:GetLuaObject("GameUICommonDialog")
        GameUICommonDialog:PlayStory({1100094}, function()
          self.QuestTutorialFactoryRemodel:SetActive(true, false)
        end)
      end
    end
  end
  if self.QuestTutorialBuildKrypton:IsActive() then
    local krypton = GameGlobalData:GetBuildingInfo("krypton_center")
    if 0 < krypton.level then
      self.QuestTutorialBuildKrypton:SetFinish(true)
      if not self.QuestTutorialUseKrypton:IsFinished() then
        self.QuestTutorialUseKrypton:SetActive(true)
      end
    end
  end
  if self.QuestTutorialEquipKrypton:IsActive() then
    local fleetKrypton = GameGlobalData:GetData("fleetkryptons")
    if fleetKrypton then
      for _, v in ipairs(fleetKrypton) do
        if 0 < #v.kryptons then
          self.QuestTutorialEquipKrypton:SetFinish(true)
          if not self.QuestTutorialEnhanceKrypton:IsFinished() then
            self.QuestTutorialEnhanceKrypton:SetActive(true)
          end
        end
      end
    end
  end
  if self.QuestTutorialBuildAffairs:IsActive() then
    local affairs_hall = GameGlobalData:GetBuildingInfo("affairs_hall")
    if affairs_hall and 0 < affairs_hall.level then
      self.QuestTutorialBuildAffairs:SetFinish(true)
    end
  end
  local affairs_hall = GameGlobalData:GetBuildingInfo("affairs_hall")
  if affairs_hall and 0 < affairs_hall.level and not self.QuestTutorialUseAffairs:IsFinished() then
    self.QuestTutorialUseAffairs:SetActive(true)
  end
  local command_post = GameGlobalData:GetBuildingInfo("Command_post")
  if command_post and 0 < command_post.level then
    DebugOut("enenenen in T", TutorialQuestManager.QuestTutorialEnterStarSystem:IsFinished())
    if not TutorialQuestManager.QuestTutorialEnterStarSystem:IsFinished() then
      self.QuestTutorialEnterStarSystem:SetActive(true)
    end
  end
  if self.QuestTutorialUseAffairs:IsFinished() and not self.QuestTutorialInfiniteCosmos:IsActive() and not self.QuestTutorialInfiniteCosmos:IsFinished() then
    self.QuestTutorialInfiniteCosmos:SetActive(true)
    local GameUIBarLeft = LuaObjectManager:GetLuaObject("GameUIBarLeft")
    GameUIBarLeft:ShowIsNewFunctionInLab()
  end
end
function TutorialQuestManager:NewQuest(questIndex)
  local newQuest = {}
  local metaTable = {}
  function metaTable.__index(t, k)
    local ret = QuestBase[k]
    newQuest[k] = ret
    return ret
  end
  setmetatable(newQuest, metaTable)
  self.Quests[questIndex] = newQuest
  newQuest:SetIndex(questIndex)
  return newQuest
end
TutorialQuestManager.cleanAllTutorial = false
function TutorialQuestManager.UpdateTutorial(content)
  DebugOut("TutorialQuest ntf :")
  DebugTable(content)
  if TutorialQuestManager.cleanAllTutorial then
    for k, v in pairs(TutorialQuestManager.Quests) do
      if not TutorialQuestManager:IsForceTutorial(k) then
        DebugOut("set finish k = ", k)
        v:SetFinish(true, true)
        v:SetActive(false, true)
      end
    end
    TutorialQuestManager.TutorialSkipAll = 1
    TutorialQuestManager:Save()
  else
    for k, v in pairs(TutorialQuestManager.Quests) do
      v:SetFinish(false, true)
      v:SetActive(false, true)
    end
    local saveString = content.progress
    if saveString ~= nil and saveString ~= "" then
      loadstring(saveString)()
      if TutorialQuests ~= nil then
        TutorialQuests.RECORD_VERSION = nil
        TutorialQuestManager.TutorialSkipAll = TutorialQuests.skipAllTutorial or 0
        TutorialQuests.skipAllTutorial = nil
        local haveWanaAndFirstSilvaEquipTutorial = false
        local haveRushTutorial = false
        local haveTutorialInfiniteCosmos = false
        local haveTutorialMainTask = false
        for k, v in pairs(TutorialQuests) do
          DebugOut("K:", k)
          if TutorialQuestManager.Quests[k] then
            DebugTable(TutorialQuestManager.Quests[k])
            TutorialQuestManager.Quests[k]:SetFinish(v.F == 1, true)
            TutorialQuestManager.Quests[k]:SetActive(v.A == 1, true)
            if TutorialQuestManager.TutorialFirstGetWanaArmor == k or TutorialQuestManager.TutorialFirstGetSilvaEquip == k then
              haveWanaAndFirstSilvaEquipTutorial = true
            end
            if TutorialQuestManager.TutorialRush == k then
              haveRushTutorial = true
            end
            if TutorialQuestManager.TutorialInfiniteCosmos == k then
              haveTutorialInfiniteCosmos = true
            end
            if TutorialQuestManager.TutorialPveMapPoint == k or TutorialQuestManager.TutorialMainTask == k or TutorialQuestManager.TutorialTheFirstEvent == k then
              haveTutorialMainTask = true
            end
          end
        end
        if not haveWanaAndFirstSilvaEquipTutorial then
          TutorialQuestManager.QuestTutorialFirstGetWanaArmor:SetFinish(true, true)
          TutorialQuestManager.QuestTutorialFirstGetWanaArmor:SetActive(false, true)
          TutorialQuestManager.QuestTutorialFirstGetSilvaEquip:SetFinish(true, true)
          TutorialQuestManager.QuestTutorialFirstGetSilvaEquip:SetActive(false, true)
          TutorialQuestManager.QuestTutorialSign:SetFinish(true, true)
          TutorialQuestManager.QuestTutorialSign:SetActive(false, true)
          TutorialQuestManager:Save()
        end
        if not haveRushTutorial then
          TutorialQuestManager.QuestTutorialRush:SetFinish(true, true)
          TutorialQuestManager.QuestTutorialRush:SetActive(false, true)
          TutorialQuestManager:Save()
        end
        if not haveTutorialInfiniteCosmos then
          TutorialQuestManager.QuestTutorialInfiniteCosmos:SetFinish(true, true)
          TutorialQuestManager.QuestTutorialInfiniteCosmos:SetActive(false, true)
          TutorialQuestManager:Save()
        end
        if not haveTutorialMainTask then
          TutorialQuestManager.QuestTutorialPveMapPoint:SetFinish(true, true)
          TutorialQuestManager.QuestTutorialPveMapPoint:SetActive(false, true)
          TutorialQuestManager.QuestTutorialMainTask:SetFinish(true, true)
          TutorialQuestManager.QuestTutorialMainTask:SetActive(false, true)
          TutorialQuestManager.QuestTutorialTheFirstEvent:SetFinish(true, true)
          TutorialQuestManager.QuestTutorialTheFirstEvent:SetActive(false, true)
          TutorialQuestManager:Save()
        end
      end
    end
  end
  TutorialQuestManager.DebugTutorial()
  GameStateManager.GameStateMainPlanet:CheckShowTutorial()
end
function TutorialQuestManager:Init()
  if not self.m_isInit then
    self.m_isInit = true
    GameGlobalData:RegisterDataChangeCallback("buildings", self.OnBuildingInfoChange)
    GameGlobalData:RegisterDataChangeCallback("matrix", self.OnFleetMatrixChange)
  end
end
function TutorialQuestManager.OnFleetMatrixChange()
  if TutorialQuestManager.QuestTutorialBattleMapAfterRecruit:IsActive() then
    local fleets_matrix = GameGlobalData:GetData("matrix").cells
    for i, v in ipairs(fleets_matrix) do
      if v.identity == 2 then
        if immanentversion == 1 then
          TutorialQuestManager.QuestTutorialBattleMapAfterRecruit:SetFinish(true)
        end
        return
      end
    end
  end
end
function TutorialQuestManager.OnFleetInfoChange()
  if TutorialQuestManager.QuestTutorialBattleMapAfterRecruit:IsActive() then
    local fleets = GameGlobalData:GetData("fleetinfo").fleets
    for i, v in ipairs(fleets) do
      if v.identity == 2 then
        return
      end
    end
    TutorialQuestManager.QuestTutorialBattleMapAfterRecruit:SetFinish(true)
  end
end
function TutorialQuestManager.OnBuildingInfoChange()
  DebugOut("TutorialQuestManager.OnBuildingInfoChange")
  local buildings = GameGlobalData:GetData("buildings")
  if not buildings then
    return
  end
  local TutorialQuestManager = TutorialQuestManager
  if not TutorialQuestManager.QuestTutorialEngineer:IsFinished() and not TutorialQuestManager.QuestTutorialEngineer:IsActive() then
    local engineerBuildInfo = GameGlobalData:GetBuildingInfo("engineering_bay")
    if engineerBuildInfo and engineerBuildInfo.status == 1 then
      TutorialQuestManager.QuestTutorialEngineerBarMove:SetFinish(true)
      if immanentversion == 1 then
        TutorialQuestManager.QuestTutorialEngineer:SetActive(true)
      end
    end
  end
  if not TutorialQuestManager.QuestTutorialBuildAcademy:IsFinished() and not TutorialQuestManager.QuestTutorialBuildAcademy:IsActive() then
    local academy = GameGlobalData:GetBuildingInfo("commander_academy")
    if academy and academy.status == 1 and immanentversion == 1 then
      TutorialQuestManager.QuestTutorialBuildAcademy:SetActive(true)
    end
  end
  DebugOut("eneneneneen", TutorialQuestManager.QuestTutorialEnterStarSystem:IsFinished(), TutorialQuestManager.QuestTutorialEnterStarSystem:IsActive())
  if not TutorialQuestManager.QuestTutorialEnterStarSystem:IsFinished() and not TutorialQuestManager.QuestTutorialEnterStarSystem:IsActive() then
    local command_post = GameGlobalData:GetBuildingInfo("Command_post")
    DebugTable(command_post)
    if command_post and command_post.level > 0 then
      TutorialQuestManager.QuestTutorialEnterStarSystem:SetActive(true)
    end
  end
end
function TutorialQuestManager:IsForceTutorial(tutorialIndex)
  if tutorialIndex == TutorialQuestManager.TutorialBattleMapAfterRecruotIndex then
    return true
  elseif tutorialIndex == TutorialQuestManager.TutorialUseAffairs then
    return true
  elseif tutorialIndex == TutorialQuestManager.TutorialInfiniteCosmos then
    return true
  elseif tutorialIndex == TutorialQuestManager.TutorialComboGacha then
    return true
  elseif tutorialIndex == TutorialQuestManager.TutorialComboGachaGetHero then
    return true
  elseif tutorialIndex == TutorialQuestManager.TutorialBuildKryptonCenter then
    return true
  elseif tutorialIndex == TutorialQuestManager.TutorialUseKrypton then
    return true
  elseif tutorialIndex == TutorialQuestManager.TutorialEquipKrypton then
    return true
  elseif tutorialIndex == TutorialQuestManager.TutorialWVE then
    return true
  elseif tutorialIndex == TutorialQuestManager.TutorialRefine then
    return true
  elseif tutorialIndex == TutorialQuestManager.TutorialTax then
    return true
  elseif tutorialIndex == TutorialQuestManager.SaveFirstCharge then
    return true
  elseif tutorialIndex == TutorialQuestManager.TutorialAdjutantStoryPlay then
    return true
  elseif tutorialIndex == TutorialQuestManager.TutorialAdjutantBind then
    return true
  elseif tutorialIndex == TutorialQuestManager.TutorialTacticsCenter then
    return true
  elseif tutorialIndex == TutorialQuestManager.TutorialTacticsRefine then
    return true
  elseif tutorialIndex == TutorialQuestManager.TutorialPrimeWve then
    return true
  elseif tutorialIndex == TutorialQuestManager.TutorialStarwar then
    return true
  elseif tutorialIndex == TutorialQuestManager.TutorialEnterStarSystem then
    return true
  elseif tutorialIndex == TutorialQuestManager.TutorialEquipStarSystemItem then
    return true
  elseif tutorialIndex == TutorialQuestManager.TutorialEngineerBarMove then
    return true
  elseif tutorialIndex == TutorialQuestManager.TutorialStarSystemTujian then
    return true
  elseif tutorialIndex == TutorialQuestManager.TutorialStarSystemSkipAnim then
    return true
  elseif tutorialIndex == TutorialQuestManager.TutorialTlcSwitchTeam then
    return true
  elseif tutorialIndex == TutorialQuestManager.TutorialTlcMainUiEntry then
    return true
  elseif tutorialIndex == TutorialQuestManager.TutorialTlcArenaEntry then
    return true
  elseif tutorialIndex == TutorialQuestManager.TutorialTlcSearch then
    return true
  elseif tutorialIndex == TutorialQuestManager.TutorialTlcChallenge then
    return true
  end
  return false
end
function TutorialQuestManager:Save()
  local saveFile = {}
  saveFile.RECORD_VERSION = self.saveVer
  saveFile.skipAllTutorial = self.TutorialSkipAll
  for k, v in pairs(self.Quests) do
    saveFile[v.m_index] = {
      A = v.m_isActive and 1 or 0,
      F = v.m_isFinish and 1 or 0
    }
  end
  local saveString = GameUtils:formatSaveString(saveFile, "TutorialQuests")
  DebugOut("TutorialQuestManager:Save", saveString)
  local content = {progress = saveString}
  NetMessageMgr:SendMsg(NetAPIList.update_guide_progress_req.Code, content, nil, false, nil)
end
QuestBase = {
  m_index = -1,
  m_isActive = false,
  m_isFinish = false
}
function QuestBase:SetIndex(index)
  self.m_index = index
end
function QuestBase:SetActive(isActive, noSave)
  DebugOut("1")
  DebugOut("TutorialQuestManager.TutorialSkipAll:" .. TutorialQuestManager.TutorialSkipAll)
  if TutorialQuestManager.TutorialSkipAll == 1 and isActive and not TutorialQuestManager:IsForceTutorial(self.m_index) then
    return
  end
  DebugOut("2")
  if self.m_isActive == isActive then
    return
  end
  DebugOut("QuestBase:SetActive", self.m_index, isActive, noSave)
  self.m_isActive = isActive
  if self.m_isActive then
    self.m_isFinish = false
  end
  if not noSave then
    TutorialQuestManager:Save()
  end
end
function QuestBase:SetFinish(isFinish, noSave)
  if self.m_isFinish == isFinish then
    return
  end
  self.m_isFinish = isFinish
  DebugOut("QuestBase:SetFinish", self.m_index, isFinish, noSave)
  if self.m_isFinish then
    self.m_isActive = false
  end
  if not noSave then
    TutorialQuestManager:Save()
  end
end
local t_forceNotActive = {
  [TutorialQuestManager.TutorialEquip] = 1
}
function QuestBase:IsActive()
  if t_forceNotActive[self.m_index] then
    return false
  end
  if self.m_isActive then
    DebugOut("active tutorial:", self.m_index)
  end
  return self.m_isActive
end
function QuestBase:IsFinished()
  return self.m_isFinish
end
function TutorialQuestManager:GetTurialFakeBattleID(aId, bId)
  local GameStateBattleMap = GameStateManager.GameStateBattleMap
  local GameObjectBattleReplay = LuaObjectManager:GetLuaObject("GameObjectBattleReplay")
  local GameUIEnemyInfo = LuaObjectManager:GetLuaObject("GameUIEnemyInfo")
  local areaID = aId or GameObjectBattleReplay._activeArea
  local battleID = bId or GameObjectBattleReplay._activeBattle
  DebugOutBattlePlay("TutorialQuestManager:GetTurialFakeBattleID: ", areaID, "    ", battleID)
  if areaID == 60 then
    if battleID == 1001 then
      return 1
    elseif battleID == 1002 then
      return 2
    elseif battleID == 1007 then
      return 3
    elseif battleID == 1013 then
      return 4
    elseif battleID == 9999 then
      return 0
    end
  end
  return nil
end
function TutorialQuestManager:GetTurialFakeBattleData(fakeBattleID)
  DebugOut("utorialQuestManager:GetTurialFakeBattleData(fakeBattleID)", fakeBattleID)
  if fakeBattleID == 1 then
    return GameData.fakeBattleResult1
  elseif fakeBattleID == 2 then
    return GameData.fakeBattleResult2
  elseif fakeBattleID == 3 then
    return GameData.fakeBattleResult3
  elseif fakeBattleID == 4 then
    return GameData.fakeBattleResult4
  elseif fakeBattleID == 0 then
    return GameData.fakeBattleResult0
  end
  return nil
end
function TutorialQuestManager:GetTurialFakeBattleAnim(fakeBattleID)
  if fakeBattleID == 1 then
    return GameData.fakeBattleAnim1
  elseif fakeBattleID == 2 then
    return GameData.fakeBattleAnim2
  elseif fakeBattleID == 3 then
    return GameData.fakeBattleAnim3
  elseif fakeBattleID == 4 then
    return GameData.fakeBattleAnim4
  elseif fakeBattleID == 0 then
    return GameData.fakeBattleAnim0
  end
  return nil
end
function TutorialQuestManager:GetTurialFakeBattleCommand(fakeBattleID)
  if fakeBattleID == 1 then
    return GameData.fakeBattleCommand1
  elseif fakeBattleID == 2 then
    return GameData.fakeBattleCommand2
  elseif fakeBattleID == 3 then
    return GameData.fakeBattleCommand3
  elseif fakeBattleID == 4 then
    return GameData.fakeBattleCommand4
  elseif fakeBattleID == 0 then
    return GameData.fakeBattleCommand0
  end
  return nil
end
function TutorialQuestManager:SysFakeBattleInfo(realBattleData, fakeBattleData)
  DebugOut("SysFakeBattleInfo--->")
  DebugTable(realBattleData)
  fakeBattleData.player1 = realBattleData.player1
  fakeBattleData.player2_avatar = realBattleData.player2_avatar
  fakeBattleData.player2 = realBattleData.player2
  fakeBattleData.player1_avatar = realBattleData.player1_avatar
end
function TutorialQuestManager:CheckInitFakeBattle()
  local GameObjectBattleReplay = LuaObjectManager:GetLuaObject("GameObjectBattleReplay")
  local fakeBattleID = self:GetTurialFakeBattleID()
  if fakeBattleID then
    DebugOutBattlePlay("TutorialQuestManager:CheckInitFakeBattle: " .. fakeBattleID)
    GameObjectBattleReplay:InitFakeBattle(fakeBattleID)
  end
end
function TutorialQuestManager:CheckStopFakeBattle(battleStep, roundStep)
  DebugOutBattlePlay("TutorialQuestManager:CheckStopFakeBattle: ", battleStep, roundStep)
  local GameObjectBattleReplay = LuaObjectManager:GetLuaObject("GameObjectBattleReplay")
  local anim = GameObjectBattleReplay.m_fakeBattle.m_fakeBattleAnim
  local currentStopStep = GameObjectBattleReplay.m_fakeBattle.m_fakeBattleStopIndex
  local stepData = anim[currentStopStep]
  if not stepData then
    return false
  end
  DebugOutBattlePlay("TutorialQuestManager:CheckStopFakeBattle: ", stepData.round_index, stepData.round_step)
  if battleStep == stepData.round_index and roundStep == stepData.round_step then
    return true
  end
  return false
end
function TutorialQuestManager.PlayFakeBattleEffectBeforDialog()
  local GameObjectFakeBattle = LuaObjectManager:GetLuaObject("GameObjectFakeBattle")
  local GameUICommonDialog = LuaObjectManager:GetLuaObject("GameUICommonDialog")
  local GameObjectBattleReplay = LuaObjectManager:GetLuaObject("GameObjectBattleReplay")
  local anim = GameObjectBattleReplay.m_fakeBattle.m_fakeBattleAnim
  if not anim then
    return
  end
  local fakeBattleStopIndex = GameObjectBattleReplay.m_fakeBattle.m_fakeBattleStopIndex
  local stepData = anim[fakeBattleStopIndex]
  DebugOutBattlePlay("TutorialQuestManager.PlayFakeBattleEffectBeforDialog: ", fakeBattleStopIndex)
  assert(stepData.befor_dialog_anim or #stepData.dialog_id > 0)
  if stepData.befor_dialog_anim then
    if #stepData.dialog_id > 0 then
      DebugOutBattlePlay("TutorialQuestManager.PlayFakeBattleEffectBeforDialog: Play Anim -> Dialog")
      GameObjectFakeBattle:PlayAnim(stepData.befor_dialog_anim, TutorialQuestManager.PlayFakeBattleDialog)
    else
      DebugOutBattlePlay("TutorialQuestManager.PlayFakeBattleEffectBeforDialog: Play Anim -> Over")
      GameObjectFakeBattle:PlayAnim(stepData.befor_dialog_anim, GameObjectBattleReplay.ResumeFakeBattle)
    end
  else
    TutorialQuestManager.PlayFakeBattleDialog()
  end
end
function TutorialQuestManager.PlayFakeBattleDialog()
  local GameObjectFakeBattle = LuaObjectManager:GetLuaObject("GameObjectFakeBattle")
  local GameUICommonDialog = LuaObjectManager:GetLuaObject("GameUICommonDialog")
  local GameObjectBattleReplay = LuaObjectManager:GetLuaObject("GameObjectBattleReplay")
  local anim = GameObjectBattleReplay.m_fakeBattle.m_fakeBattleAnim
  if not anim then
    return
  end
  local fakeBattleStopIndex = GameObjectBattleReplay.m_fakeBattle.m_fakeBattleStopIndex
  local stepData = anim[fakeBattleStopIndex]
  DebugOutBattlePlay("TutorialQuestManager.PlayFakeBattleDialog: ", fakeBattleStopIndex)
  if stepData.befor_dialog_anim and stepData.hide_anim_in_dialog then
    GameObjectFakeBattle:HideAllFakeBattleEffect()
  end
  if #stepData.dialog_id > 0 then
    if stepData.after_dialog_anim then
      DebugOutBattlePlay("TutorialQuestManager.PlayFakeBattleDialog: ForcePlayStory -> Play Anim")
      GameUICommonDialog:ForcePlayStory(stepData.dialog_id, TutorialQuestManager.PlayFakeBattleEffectAfterDialog)
    else
      DebugOutBattlePlay("TutorialQuestManager.PlayFakeBattleDialog: ForcePlayStory -> Over")
      GameUICommonDialog:ForcePlayStory(stepData.dialog_id, GameObjectBattleReplay.ResumeFakeBattle)
    end
  else
    TutorialQuestManager.PlayFakeBattleEffectAfterDialog()
  end
end
function TutorialQuestManager.PlayFakeBattleEffectAfterDialog()
  local GameObjectFakeBattle = LuaObjectManager:GetLuaObject("GameObjectFakeBattle")
  local GameUICommonDialog = LuaObjectManager:GetLuaObject("GameUICommonDialog")
  local GameObjectBattleReplay = LuaObjectManager:GetLuaObject("GameObjectBattleReplay")
  local anim = GameObjectBattleReplay.m_fakeBattle.m_fakeBattleAnim
  if not anim then
    return
  end
  local fakeBattleStopIndex = GameObjectBattleReplay.m_fakeBattle.m_fakeBattleStopIndex
  local stepData = anim[fakeBattleStopIndex]
  DebugOutBattlePlay("TutorialQuestManager.PlayFakeBattleEffectAfterDialog: ", fakeBattleStopIndex)
  if stepData.after_dialog_anim then
    DebugOutBattlePlay("TutorialQuestManager.PlayFakeBattleEffectAfterDialog: Play Anim and Over")
    GameObjectFakeBattle:PlayAnim(stepData.after_dialog_anim, GameObjectBattleReplay.ResumeFakeBattle)
  else
    GameObjectBattleReplay.ResumeFakeBattle()
  end
end
TutorialQuestManager.WanaGetCallback = nil
function TutorialQuestManager:UpdateQuestTutorialFirstGetWanaArmor(bagItemList)
  if not TutorialQuestManager.QuestTutorialFirstGetWanaArmor:IsFinished() and bagItemList then
    for k, v in pairs(bagItemList) do
      if 100003 == v.item_type then
        DebugOut("set QuestTutorialFirstGetWanaArmor to active!")
        TutorialQuestManager.QuestTutorialFirstGetWanaArmor:SetActive(true, false)
        if TutorialQuestManager.WanaGetCallback then
          TutorialQuestManager.WanaGetCallback()
        end
        return
      end
    end
  end
  DebugOut("UpdateQuestTutorialFirstGetWanaArmor !!")
  DebugTable(bagItemList)
end
function TutorialQuestManager.CheckTLCState(msgType, content)
  if msgType == NetAPIList.enter_champion_ack.Code then
    if content.code ~= 0 then
      return true
    end
    if content.tlc_champion and content.tlc_champion.status == 1 then
      TutorialQuestManager.QuestTutorialTlcMainUiEntry:SetActive(true)
    end
    return true
  end
  return false
end
function TutorialQuestManager:CheckTlcTutorial()
  local playerLevel = GameGlobalData:GetData("levelinfo").level
  local GameUIArena = LuaObjectManager:GetLuaObject("GameUIArena")
  if not TutorialQuestManager.QuestTutorialTlcMainUiEntry:IsFinished() and playerLevel >= GameUIArena.TLCUnlockLevel then
    NetMessageMgr:SendMsg(NetAPIList.enter_champion_req.Code, nil, TutorialQuestManager.CheckTLCState, true)
  end
end
TutorialQuestManager.ComboGachaGetHeroCallback = nil
TutorialQuestManager.ComboGachaHeroContent = nil
TutorialQuestManager.GetComboGachaCallback = nil
function TutorialQuestManager:UpdateQuestTutorialComboGachaGetHero(bagItemList)
  if not TutorialQuestManager.QuestTutorialComboGachaGetHero:IsFinished() and bagItemList and TutorialQuestManager.ComboGachaHeroContent then
    for k, v in pairs(bagItemList) do
      DebugOut("UpdateQuestTutorialComboGachaGetHero:")
      DebugTable(bagItemList)
      if TutorialQuestManager.ComboGachaHeroContent.id == v.item_type and v.cnt == 2 and TutorialQuestManager.QuestTutorialComboGacha:IsActive() then
        TutorialQuestManager.QuestTutorialComboGacha:SetFinish(true)
        local GameUIBarLeft = LuaObjectManager:GetLuaObject("GameUIBarLeft")
        GameUIBarLeft:ShowIsNewFunctionComboGacha()
        TutorialQuestManager.QuestTutorialComboGachaGetHero:SetActive(true, false)
        TutorialQuestManager.QuestTutorialComboGachaUseHero:SetActive(true, false)
        if TutorialQuestManager.GetComboGachaCallback then
          TutorialQuestManager.GetComboGachaCallback()
        end
        if TutorialQuestManager.ComboGachaGetHeroCallback then
          TutorialQuestManager.ComboGachaGetHeroCallback()
        end
      end
    end
  end
end
function TutorialQuestManager:GetComboGachaSendHero(id)
  local param = {state = id}
  NetMessageMgr:SendMsg(NetAPIList.combo_guide_req.Code, param, TutorialQuestManager.ComboGachaCallback, false, nil)
end
function TutorialQuestManager.ComboGachaCallback(msgType, content)
  if msgType == NetAPIList.combo_guide_ack.Code then
    DebugOut("TutorialQuestManager-ComboGachaCallback.content:")
    DebugTable(content)
    TutorialQuestManager.ComboGachaHeroContent = content
    return true
  end
  return false
end
TutorialQuestManager.QuestTutorialBattleMap = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialBattleMap)
TutorialQuestManager.QuestTutorialEquip = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialEquip)
TutorialQuestManager.QuestTutorialEngineer = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialEngineer)
TutorialQuestManager.QuestTutorialEnhance = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialEnhance)
TutorialQuestManager.QuestTutorialEnhance_second = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialEnhance_second)
TutorialQuestManager.QuestTutorialEnhance_third = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialEnhance_third)
TutorialQuestManager.QuestTutorialCityHall = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialCityHall)
TutorialQuestManager.QuestTutorialTax = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialTax)
TutorialQuestManager.QuestTutorialFirstBattle = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialFirstBattle)
TutorialQuestManager.QuestTutorialBuildStar = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialStar)
TutorialQuestManager.QuestTutorialColonial = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialColonial)
TutorialQuestManager.QuestTutorialWD = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialWD)
TutorialQuestManager.QuestTutorialSecondHeroLoadEQU = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialSecondHeroLoadEQU)
TutorialQuestManager.QuestTutorialStarCharge = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialStarCharge)
TutorialQuestManager.QuestTutorialRecruit = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialStarRecruit)
TutorialQuestManager.QuestTutorialBattleMapAfterRecruit = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialBattleMapAfterRecruotIndex)
TutorialQuestManager.QuestTutorialBuildAcademy = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialBuildAcademy)
TutorialQuestManager.QuestTutorialsFirstKillBoss = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialsFirstKillBoss)
TutorialQuestManager.QuestTutorialEquipmentEvolution = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialEquipmentEvolution)
TutorialQuestManager.QuestTutorialQuestCenter = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialQuestCenter)
TutorialQuestManager.QuestTutorialGetQuestReward = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialGetQuestReward)
TutorialQuestManager.QuestTutorialBuildTechLab = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialBuildTechLab)
TutorialQuestManager.QuestTutorialBuildFactory = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialBuildFactory)
TutorialQuestManager.QuestTutorialBuildKrypton = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialBuildKryptonCenter)
TutorialQuestManager.QuestTutorialBuildAffairs = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialBuildAffairsCenter)
TutorialQuestManager.QuestTutorialUseTechLab = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialUseTechLab)
TutorialQuestManager.QuestTutorialUseFactory = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialUseFactory)
TutorialQuestManager.QuestTutorialUseKrypton = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialUseKrypton)
TutorialQuestManager.QuestTutorialUseAffairs = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialUseAffairs)
TutorialQuestManager.QuestTutorialsChangeFleets = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialsChangeFleets)
TutorialQuestManager.QuestTutorialsChangeFleets1 = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialsChangeFleets1)
TutorialQuestManager.QuestTutorialPrestige = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialsPrestige)
TutorialQuestManager.QuestTutorialCollect = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialCollect)
TutorialQuestManager.QuestTutorialMine = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialMine)
TutorialQuestManager.QuestTutorialSkill = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialUseSkill)
TutorialQuestManager.QuestTutorialEquipKrypton = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialEquipKrypton)
TutorialQuestManager.QuestTutorialEnhanceKrypton = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialEnhanceKrypton)
TutorialQuestManager.QuestTutorialRepairFleet = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialRepairFleet)
TutorialQuestManager.QuestTutorialSlot = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialSlot)
TutorialQuestManager.QuestTutorialArena = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialArena)
TutorialQuestManager.QuestTutorialGetGift = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialGetGift)
TutorialQuestManager.QuestTutorialNewBossArena = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialNewBossArena)
TutorialQuestManager.QuestTutorialRechargePower = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialRechargePower)
TutorialQuestManager.QuestTutorialBattleMap_second = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialBattleMap_second)
TutorialQuestManager.QuestTutorialBattleFailed = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialBattleFailed)
TutorialQuestManager.QuestTutorialBattleRush = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialBattleRush)
TutorialQuestManager.QuestTutorialArenaReward = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialArenaReward)
TutorialQuestManager.QuestTutorialGameSpeed = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialGameSpeed)
TutorialQuestManager.QuestTutorialNewFunctonAC = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialNewFunctonAC)
TutorialQuestManager.QuestTutorialNewFunctonWorldboss = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialNewFunctonWorldboss)
TutorialQuestManager.QuestTutorialEnhanceEquipFast = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialEnhanceEquipFast)
TutorialQuestManager.QuestTutorialEquipAllByOneKey = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialEquipAllByOneKey)
TutorialQuestManager.QuestTutorialMakeUserLevel10 = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialMakeUserLevel10)
TutorialQuestManager.FirstChargeState = TutorialQuestManager:NewQuest(TutorialQuestManager.SaveFirstCharge)
TutorialQuestManager.QuestTutorialFirstGetWanaArmor = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialFirstGetWanaArmor)
TutorialQuestManager.QuestTutorialFirstGetSilvaEquip = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialFirstGetSilvaEquip)
TutorialQuestManager.QuestTutorialSign = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialSign)
TutorialQuestManager.QuestTutorialRefine = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialRefine)
TutorialQuestManager.QuestTutorialWVE = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialWVE)
TutorialQuestManager.QuestTutorialRush = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialRush)
TutorialQuestManager.QuestTutorialInfiniteCosmos = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialInfiniteCosmos)
TutorialQuestManager.QuestTutorialComboGacha = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialComboGacha)
TutorialQuestManager.QuestTutorialComboGachaGetHero = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialComboGachaGetHero)
TutorialQuestManager.QuestTutorialComboGachaUseHero = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialComboGachaUseHero)
TutorialQuestManager.QuestTutorialAdjutantStoryPlay = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialAdjutantStoryPlay)
TutorialQuestManager.QuestTutorialAdjutantBind = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialAdjutantBind)
TutorialQuestManager.QuestTutorialInfiniteJumpStep = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialInfiniteJumpStep)
TutorialQuestManager.QuestTutorialTacticsCenter = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialTacticsCenter)
TutorialQuestManager.QuestTutorialTacticsRefine = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialTacticsRefine)
TutorialQuestManager.QuestTutorialPveMapPoint = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialPveMapPoint)
TutorialQuestManager.QuestTutorialMainTask = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialMainTask)
TutorialQuestManager.QuestTutorialTheFirstEvent = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialTheFirstEvent)
TutorialQuestManager.QuestTutorialFirstChapter = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialFirstChapter)
TutorialQuestManager.QuestTutorialFirstReform = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialFirstReform)
TutorialQuestManager.QuestTutorialFactoryRemodel = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialFactoryRemodel)
TutorialQuestManager.QuestTutorialBuildLab = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialBuildLab)
TutorialQuestManager.QuestTutorialBuildArena = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialBuildArena)
TutorialQuestManager.QuestTutorialHelper = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialHelper)
TutorialQuestManager.QuestTutorialPrimeWve = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialPrimeWve)
TutorialQuestManager.QuestTutorialStarwar = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialStarwar)
TutorialQuestManager.QuestTutorialEnterStarSystem = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialEnterStarSystem)
TutorialQuestManager.QuestTutorialEquipStarSystemItem = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialEquipStarSystemItem)
TutorialQuestManager.QuestTutorialEngineerBarMove = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialEngineerBarMove)
TutorialQuestManager.QuestTutorialStarSystemTujian = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialStarSystemTujian)
TutorialQuestManager.QuestTutorialStarSystemSkipAnim = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialStarSystemSkipAnim)
TutorialQuestManager.QuestTutorialTlcMainUiEntry = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialTlcMainUiEntry)
TutorialQuestManager.QuestTutorialTlcArenaEntry = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialTlcArenaEntry)
TutorialQuestManager.QuestTutorialTlcSearch = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialTlcSearch)
TutorialQuestManager.QuestTutorialTlcChallenge = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialTlcChallenge)
TutorialQuestManager.QuestTutorialTlcSwitchTeam = TutorialQuestManager:NewQuest(TutorialQuestManager.TutorialTlcSwitchTeam)
function TutorialQuestManager.DebugTutorial()
  if DebugConfig.isDebugTutorialQuest then
    local TutorialQuestDebug = {
      [1] = {A = 0, F = 1},
      [4] = {A = 0, F = 1},
      [6] = {A = 0, F = 1},
      [7] = {A = 0, F = 1},
      [8] = {A = 0, F = 1},
      [9] = {A = 0, F = 1},
      [13] = {A = 0, F = 0},
      [15] = {A = 0, F = 0},
      [16] = {A = 0, F = 0},
      [17] = {A = 0, F = 0},
      [20] = {A = 0, F = 0},
      [21] = {A = 0, F = 0},
      [22] = {A = 0, F = 0},
      [23] = {A = 0, F = 1},
      [26] = {A = 0, F = 0},
      [27] = {A = 0, F = 0},
      [28] = {A = 0, F = 0},
      [29] = {A = 0, F = 0},
      [30] = {A = 0, F = 0},
      [31] = {A = 0, F = 0},
      [32] = {A = 0, F = 0},
      [33] = {A = 0, F = 0},
      [34] = {A = 0, F = 0},
      [35] = {A = 0, F = 0},
      [36] = {A = 0, F = 1},
      [37] = {A = 0, F = 1},
      [38] = {A = 0, F = 1},
      [39] = {A = 0, F = 0},
      [40] = {A = 0, F = 1},
      [41] = {A = 0, F = 0},
      [42] = {A = 0, F = 0},
      [43] = {A = 0, F = 0},
      [44] = {A = 0, F = 0},
      [45] = {A = 0, F = 0},
      [46] = {A = 0, F = 1},
      [47] = {A = 0, F = 0},
      [48] = {A = 1, F = 0},
      [49] = {A = 0, F = 0},
      [50] = {A = 0, F = 0},
      [51] = {A = 0, F = 0},
      [52] = {A = 0, F = 0},
      [53] = {A = 0, F = 0},
      [54] = {A = 0, F = 0},
      [55] = {A = 0, F = 1},
      [56] = {A = 0, F = 0},
      [57] = {A = 0, F = 0},
      [58] = {A = 0, F = 0},
      [59] = {A = 0, F = 0}
    }
    for k, v in pairs(TutorialQuestDebug) do
      TutorialQuestManager.Quests[k]:SetFinish(v.F == 1, true)
      TutorialQuestManager.Quests[k]:SetActive(v.A == 1, true)
    end
  end
end
