local GameObjectBattleReplay = LuaObjectManager:GetLuaObject("GameObjectBattleReplay")
local GameStateBattlePlay = GameStateManager.GameStateBattlePlay
local GameGlobalData = GameGlobalData
local GameStateBattleMap = GameStateManager.GameStateBattleMap
local GameObjectTutorialCutscene = LuaObjectManager:GetLuaObject("GameObjectTutorialCutscene")
local GameObjectShakeScreen = LuaObjectManager:GetLuaObject("GameObjectShakeScreen")
local GameObjectBattleBG = LuaObjectManager:GetLuaObject("GameObjectBattleBG")
local GameObjectFakeBattle = LuaObjectManager:GetLuaObject("GameObjectFakeBattle")
local GameUIEnemyInfo = LuaObjectManager:GetLuaObject("GameUIEnemyInfo")
local GameUICommonDialog = LuaObjectManager:GetLuaObject("GameUICommonDialog")
local QuestTutorialBattleFailed = TutorialQuestManager.QuestTutorialBattleFailed
local QuestTutorialGameSpeed = TutorialQuestManager.QuestTutorialGameSpeed
local GameVipDetailInfoPanel = LuaObjectManager:GetLuaObject("GameVipDetailInfoPanel")
local GameObjectClimbTower = LuaObjectManager:GetLuaObject("GameObjectClimbTower")
local GameUIBattleResult = LuaObjectManager:GetLuaObject("GameUIBattleResult")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local k_GlobalBuffName
local MAX_SKIP_BUTTON_COUNT = 0
local IS_BEBUG_BUFF = true
local spell_pause = 0
GameObjectBattleReplay.MaxShowBuffCount = 4
GameObjectBattleReplay.STEP_EFFECT_PROTECT_TIME = 5000
GameObjectBattleReplay.StepEffectProtectTimer = 0
local ArtifactSPData = {}
local RevengeSPData = {}
local ShootoffSPData = {}
local RevengeAttacker = {}
local RevengeDefender = {}
local ShootoffAttacker = {}
local ShootoffDefender = {}
local MultipleAtkData = {}
local RoundStep = {
  NONE = 1,
  SUM_FLEET = 2,
  DOT = 3,
  BUFF = 4,
  ADJUTANT = 5,
  ARTIFACT = 6,
  ATTACK = 7,
  SP_ATTACK = 8,
  RELIVE = 9
}
local AnimType = {
  ATTACK = "_attack",
  HURT = "_hurt",
  DEAD = "",
  SHIELD = "_shield",
  BLOCK = "_block",
  MISS = "_miss"
}
local RewardType = {
  NONE = 0,
  CREDIT = 1,
  MONEY = 2
}
local AttackStatuTypes = {
  BLOCK = 1,
  ABSORB = 2,
  CRIT = 3,
  SQUELCH = 4,
  IMMUNE = 6
}
local BuffUpdateStep = {
  AfterDot = 1,
  AfterState = 2,
  BeforBattle = 3,
  AfterBattle = 4,
  AfterAbsorbed = 5,
  AfterSPAttack = 6,
  AfterAttackOver = 8,
  AfterRound = 9,
  NotShowArtifactEffect = 1000
}
local BattleDamageType = {
  DOT = "dot",
  NORMAL_ATK = "normal",
  BLOCK = "counter",
  ADJUTANT = "adjutant",
  SPELL_ATK = "spell",
  AFTER_DEAD = "after_dead"
}
local AtkType = {
  Normal = 0,
  AbilityDrained = 1,
  EnergyChangeAfterSPATTACK = 2,
  EnergyChangeBeforSPATTACK = 3,
  RevengeAttack = 5,
  EnergyChangeBeforAttack = 6,
  ShootoffAttack = 7,
  MultipleAttack = 8
}
local AtkFlag = {
  Refraction = 0,
  Check = function(n, tar)
    local ns = math.pow(2, tar)
    local nb = ns * 2
    return ns <= n % nb
  end
}
GameObjectBattleReplay.needReliveFleet = {}
local _buffEffectRes = {
  buff_shootoff = {
    left = "buff_shootoff",
    right = "buff_shootoff"
  },
  buff_accumulator = {
    left = "buff_accumulator_left",
    right = "buff_accumulator_right"
  },
  buff_DamageReturn = {
    left = "buff_DamageReturn",
    right = "buff_DamageReturn"
  },
  buff_AntiInvisible = {
    left = "buff_AntiInvisible",
    right = "buff_AntiInvisible"
  },
  buff_Polymorph = {
    left = "buff_Polymorph3",
    right = "buff_Polymorph"
  },
  buff_DrainSoul = {
    left = "buff_DrainSoul",
    right = "buff_DrainSoul"
  },
  buff_scattering = {
    left = "buff_scattering_all",
    right = "buff_scattering_all"
  },
  buff_timebomb = {
    left = "buff_timebomb",
    right = "buff_timebomb"
  },
  buff_cure = {
    left = "160ADD_buff_cure",
    right = "160ADD_buff_cure"
  },
  buff_demonparasitic = {
    left = "buff_demonparasitic",
    right = "buff_demonparasitic"
  },
  buff_rollback = {
    left = "buff_rollback",
    right = "buff_rollback"
  },
  buff_exile1 = {
    left = "buff_SecondLife",
    right = "buff_SecondLife"
  },
  buff_GrowthShield = {
    left = "buff_GrowthShield",
    right = "buff_GrowthShield"
  },
  buff_GuardContract = {
    left = "buff_GuardContract",
    right = "buff_GuardContract"
  },
  buff_CallUpBaby = {
    left = "buff_CallUpBaby",
    right = "buff_CallUpBaby"
  },
  buff_SecondLife = {
    left = "buff_SecondLife",
    right = "buff_SecondLife"
  },
  buff_FinalDamageReduce = {
    left = "buff_FinalDamageReduce",
    right = "buff_FinalDamageReduce"
  },
  buff_MaqneticStorm = {
    left = "buff_MaqneticStorm",
    right = "buff_MaqneticStorm"
  },
  buff_mimesis = {
    left = "buff_mimesis",
    right = "buff_mimesis"
  },
  buff_universal = {
    left = "buff_universal",
    right = "buff_universal"
  },
  buff_scout = {left = "buff_Scout", right = "buff_Scout"},
  buff_suppress = {
    left = "buff_Suppress",
    right = "buff_Suppress"
  },
  Purify = {
    left = "buff_purity",
    right = "buff_purity"
  },
  NewDot = {left = "new_dot", right = "new_dot"},
  buff_freeoflock = {
    left = "buff_freeoflock",
    right = "buff_freeoflock"
  },
  buff_freeoffreeze = {
    left = "buff_freeoffreeze",
    right = "buff_freeoffreeze"
  },
  buff_motility = {
    left = "buff_motility_left",
    right = "buff_motility_left"
  },
  buff_hitrate = {
    left = "buff_hitrate_left",
    right = "buff_hitrate_right"
  },
  buff_ntercept = {
    left = "buff_ntercept_left",
    right = "buff_ntercept_right"
  },
  buff_crit_lv = {
    left = "buff_crit_lv",
    right = "buff_crit_lv"
  },
  locked3 = {
    left = "locked_locked3",
    right = "locked_locked3"
  },
  disturb = {
    left = "buff_effect_disturb",
    right = "buff_effect_disturb"
  },
  sp = {
    left = "buff_effect_spMC",
    right = "buff_effect_spMC"
  },
  dot2 = {
    left = "buff_new_dot_animation",
    right = "buff_new_dot_animation"
  },
  shield = {
    left = "buff_shield_animation",
    right = "buff_shield_animation"
  },
  invisible = {
    left = "locked_invisible",
    right = "locked_invisible"
  },
  locked2 = {
    left = "locked_locked",
    right = "locked_locked"
  },
  ice = {left = "ice_locked", right = "ice_locked"},
  exile = {left = "buff_exile", right = "buff_exile"},
  confusion = {
    left = "buff_confusion_anim",
    right = "buff_confusion_anim"
  },
  locked = {left = "buff_lock", right = "buff_lock"},
  def_dec = {
    left = "buff_deduct",
    right = "buff_deduct"
  },
  def_add = {left = "buff_add", right = "buff_add"},
  invincible = {
    left = "buff_shield_animation",
    right = "buff_shield_animation"
  },
  dot = {
    left = "buff_DOT_LEFT",
    right = "buff_DOT_RIGHT"
  },
  eff_sp = {left = "eff_sp", right = "eff_sp"}
}
local AdjutantHurtAnimData = {}
local SIDE_LEFT = "p1"
local SIDE_RIGHT = "p2"
local ROUND_INTERVAL_TIMER = 300
GameObjectBattleReplay.BASE_BATTLE_SPEED = 1.2
GameObjectBattleReplay.UP_BATTLE_SPEED = 2
GameObjectBattleReplay.SUPER_BATTLE_SPEED = 3
function GameObjectBattleReplay:ClearLocalData()
  DebugOutBattlePlay("ClearLocalData ")
  self.m_battleResult = nil
  self._activeArea = nil
  self._activeBattle = nil
  self.m_isPlayer1ShowInLeft = false
  self.m_totalRoundCount = 0
  self.m_currentRoundIndex = 0
  self.m_currentRoundStep = RoundStep.NONE
  self.m_currentFightIndex = 0
  self.m_currentFightFlag = false
  self.m_currentFightRecord = {}
  self.m_fleetInfo = {}
  self.m_fleetInfo.Left = {}
  self.m_leftPlayerBuff = {}
  self.m_rightPlayerBuff = {}
  self.m_fleetInfo.Left.totalHP = 0
  self.m_fleetInfo.Left.currentHP = 0
  self.m_fleetInfo.Right = {}
  self.m_fleetInfo.Right.totalHP = 0
  self.m_fleetInfo.Right.currentHP = 0
  self.m_fakeBattle = {}
  self:InitBuffData()
  ArtifactSPData = {}
  RevengeSPData = {}
  ShootoffSPData = {}
  RevengeAttacker = {}
  ShootoffAttacker = {}
  RevengeDefender = {}
  ShootoffDefender = {}
  MultipleAtkData = {}
end
function GameObjectBattleReplay:InitBuffData()
  k_GlobalBuffName = {
    coating_normal_att = {
      "20008.png",
      DynamicResDownloader.resType.PIC
    },
    coating_normal_def = {
      "20009.png",
      DynamicResDownloader.resType.PIC
    },
    coating_skill_att = {
      "20010.png",
      DynamicResDownloader.resType.PIC
    },
    coating_skill_def = {
      "20011.png",
      DynamicResDownloader.resType.PIC
    },
    coating_hp = {
      "20012.png",
      DynamicResDownloader.resType.PIC
    },
    wd_001 = {
      "wd_icon_buff_1.png",
      DynamicResDownloader.resType.FESTIVAL_BANNER
    },
    wd_002 = {
      "wd_icon_buff_2.png",
      DynamicResDownloader.resType.FESTIVAL_BANNER
    },
    wd_003 = {
      "wd_icon_buff_3.png",
      DynamicResDownloader.resType.FESTIVAL_BANNER
    },
    wd_004 = {
      "wd_icon_buff_4.png",
      DynamicResDownloader.resType.FESTIVAL_BANNER
    },
    wd_005 = {
      "wd_icon_buff_5.png",
      DynamicResDownloader.resType.FESTIVAL_BANNER
    },
    wd_006 = {
      "wd_icon_buff_6.png",
      DynamicResDownloader.resType.FESTIVAL_BANNER
    },
    wd_007 = {
      "wd_icon_buff_7.png",
      DynamicResDownloader.resType.FESTIVAL_BANNER
    },
    wd_008 = {
      "wd_icon_buff_8.png",
      DynamicResDownloader.resType.FESTIVAL_BANNER
    },
    wd_009 = {
      "wd_icon_buff_9.png",
      DynamicResDownloader.resType.FESTIVAL_BANNER
    },
    overclocking_world_boss = {},
    overclocking_mine = {},
    tc_time = {},
    tc_att = {},
    tc_hp = {},
    tc_supply = {}
  }
  GameObjectBattleReplay.FullScreenBuff = {}
end
function GameObjectBattleReplay:InitFakeBattle(battleId)
  self.m_fakeBattle.m_fakeBattleId = battleId
  self.m_fakeBattle.m_fakeBattleStopIndex = 1
  self.m_fakeBattle.m_stopBattle = false
  self.m_fakeBattle.m_fakeBattleAnim = TutorialQuestManager:GetTurialFakeBattleAnim(battleId)
  self.m_fakeBattle.m_fakeBattleCommand = TutorialQuestManager:GetTurialFakeBattleCommand(battleId)
  DebugOutBattlePlay("GameObjectBattleReplay:InitFakeBattle: " .. battleId)
  DebugOutBattlePlayTable(self.m_fakeBattle.m_fakeBattleCommand)
end
function GameObjectBattleReplay.ResumeFakeBattle()
  DebugOutBattlePlay("GameObjectBattleReplay:ResumeFakeBattle: ")
  GameObjectFakeBattle:HideAllFakeBattleEffect()
  GameObjectBattleReplay.m_fakeBattle.m_stopBattle = false
  GameObjectBattleReplay.m_fakeBattle.m_fakeBattleStopIndex = GameObjectBattleReplay.m_fakeBattle.m_fakeBattleStopIndex + 1
  GameObjectBattleReplay:PlayRoundStepEffect(GameObjectBattleReplay.m_fakeBattle.m_stopStep, true)
end
function GameObjectBattleReplay:StopFakeBattle(stopStep)
  DebugOutBattlePlay("GameObjectBattleReplay:StopFakeBattle: " .. stopStep)
  self.m_fakeBattle.m_stopStep = stopStep
  self.m_fakeBattle.m_stopBattle = true
  TutorialQuestManager.PlayFakeBattleEffectBeforDialog()
end
function GameObjectBattleReplay:OnReplayOver()
  self.m_currentRoundFocusGainTimer = nil
  ext.SetAppSpeedScale(1)
  DebugOutBattlePlay("-------- replay over ---------")
  DeviceAutoConfig.EndStatisticsInfo("BattlePlayAverageFPS")
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  if ENABLE_BATTLE_MULTIPLE_UPDATE then
    self:GetFlashObject():EnableMultipleUpdate(true)
  end
  self:GetFlashObject():InvokeASCallback("_root", "MoveOut")
end
function GameObjectBattleReplay:OnReplayStart()
  self:GetFlashObject():InvokeASCallback("_root", "MoveIn")
  DeviceAutoConfig.EndStatisticsInfo("BattlePlayAllLoadTime")
  DeviceAutoConfig.EndStatisticsInfo("BattlePlayPreLoadTime")
  DeviceAutoConfig.StartStatisticsInfo("BattlePlayAverageFPS")
  GameUtils:PlayMusic("GE2_Battle.mp3")
end
function GameObjectBattleReplay:PlayDotEffect()
  local roundData = self.m_battleResult.rounds[self.m_currentRoundIndex]
  local dotData = roundData.dot
  if #dotData > 0 then
    DebugOutBattlePlay("there is dot effect:" .. self.m_currentRoundIndex)
    local isPlayer1Action = roundData.player1_action
    local isLeftShip = isPlayer1Action and self.m_isPlayer1ShowInLeft or not isPlayer1Action and not self.m_isPlayer1ShowInLeft
    local buffPos = {}
    local buffName = {}
    for i, v in ipairs(dotData) do
      local side = SIDE_RIGHT
      buffName[v.pos] = buffName[v.pos] or {}
      if isLeftShip then
        side = SIDE_LEFT
      end
      self:UpdateBuff(side, v.pos, v.buff_effect, v.round_cnt, v.effection, v.param, true)
      local buffEffect = GameDataAccessHelper:GetBuffEffectName(v.buff_effect)
      DebugOut("PlayDotEffect:", buffEffect, ",", isLeftShip)
      if buffEffect then
        local fleetDatas = self.m_fleetInfo.Left
        if not isLeftShip then
          fleetDatas = self.m_fleetInfo.Right
        end
        local fleetData = fleetDatas[v.pos]
        if 0 < fleetData.currentHP then
          fleetData.TagHurtRoundStep = self.m_currentRoundStep
          fleetData.TagHurtRoundIndex = self.m_currentRoundIndex
          fleetData.TagHurtAttackRoundIndex = self.m_currentRoundStepInfo.AttackRoundIndex
          if not buffPos[v.pos] then
            buffPos[v.pos] = v.pos
            table.insert(buffName[v.pos], buffEffect)
            GameObjectBattleReplay:AddTotalReactionCount()
          end
          DebugOutBattlePlay("TotalReactionCount++ in GameObjectBattleReplay:PlayDotEffect: " .. self.m_currentRoundStepInfo.TotalReactionCount)
          self:UpdateFleetHPInfo(isLeftShip, v.pos, -v.dur_damage, -v.shield_damage, v.shield, false, false, false, v.is_immune, v.durability, v.index)
          self:OnUpdateHPBar(v.pos, isLeftShip)
        end
      end
    end
    for kPos, vPos in pairs(buffPos) do
      DebugOutPutTable(buffName[vPos], "buffName[vPos]")
      self:GetFlashObject():InvokeASCallback("_root", "PlayShipDotHurtAnim", isLeftShip, vPos, buffName[vPos])
    end
    self:UpdateDamageData(BattleDamageType.DOT)
    local count = 0
    for k, v in pairs(buffPos) do
      count = count + 1
      break
    end
    if count <= 0 then
      self:PlayRoundStepEffect(RoundStep.BUFF)
    end
  else
    self:PlayRoundStepEffect(RoundStep.BUFF)
  end
  self:UpdateStepBuffs(BuffUpdateStep.AfterDot)
end
function GameObjectBattleReplay:UpdateBuffEffect()
  DebugOutBattlePlay("UpdateBuffEffect ")
  local roundData = self.m_battleResult.rounds[self.m_currentRoundIndex]
  local buffData = roundData.buff
  local isPlayer1Action = roundData.player1_action
  local isLeftShip = isPlayer1Action and self.m_isPlayer1ShowInLeft or not isPlayer1Action and not self.m_isPlayer1ShowInLeft
  local spAttackData = roundData.sp_attacks
  local isTransformBuff = false
  local transformBuffPos = 0
  if spAttackData ~= nil and next(spAttackData) ~= nil and spAttackData[1].transfiguration ~= 0 then
    for i, v in ipairs(buffData) do
      if v.buff_effect == 31 then
        local curSide
        if v.side == SIDE_LEFT and self.m_isPlayer1ShowInLeft or v.side == SIDE_RIGHT and not self.m_isPlayer1ShowInLeft then
          curSide = SIDE_LEFT
        else
          curSide = SIDE_RIGHT
        end
        DebugOut("check transfiguration")
        DebugOut(curSide)
        DebugOut(v.side)
        self:AddBuff(SIDE_LEFT == curSide, v.pos, v.buff_effect, v.round_cnt, true, v.param)
        isTransformBuff = true
        transformBuffPos = v.pos
      end
    end
  end
  for i, v in ipairs(buffData) do
    local curSide
    if v.buff_step == BuffUpdateStep.AfterState or v.buff_step == BuffUpdateStep.BeforBattle then
      if v.side == SIDE_LEFT and self.m_isPlayer1ShowInLeft or v.side == SIDE_RIGHT and not self.m_isPlayer1ShowInLeft then
        curSide = SIDE_LEFT
      else
        curSide = SIDE_RIGHT
      end
      self:UpdateBuff(curSide, v.pos, v.buff_effect, v.round_cnt, v.effection, v.param)
      self:UpdateFullScreenBuff(curSide, v.buff_effect, v.round_cnt)
    end
  end
  if isTransformBuff then
    DebugOutBattlePlay("SetTransformTarget:", isLeftShip, spAttackData[1].transfiguration, spAttackData[1].atk_pos, isPlayer1Action)
    self:SetTransformTarget(isLeftShip, spAttackData[1].transfiguration, spAttackData[1].atk_pos, isPlayer1Action)
    self:CheckPlayBuff(isLeftShip, transformBuffPos)
  end
  self:CheckAllShipRemoveBuff()
  self:CheckRemoveFullScreenBuff(true)
  self:CheckRemoveFullScreenBuff(false)
  DebugOutBattlePlay("UpdateBuffEffect zm")
  self:PlayRoundStepEffect(RoundStep.ADJUTANT)
end
function GameObjectBattleReplay:TransformShipBack()
  DebugOutBattlePlay("TransformShipBack")
  DebugOutBattlePlay(self.transfromShipName_Left)
  DebugOutBattlePlay(self.transfromShipName_Right)
  if self.transfromShipName_Left ~= nil then
    self:GetFlashObject():InvokeASCallback("_root", "TransformShipBack", true, self.transfromShipGridIndex_Left)
    self.transfromShipName_Left = nil
  elseif self.transfromShipName_Right ~= nil then
    self:GetFlashObject():InvokeASCallback("_root", "TransformShipBack", false, self.transfromShipGridIndex_Right)
    self.transfromShipName_Right = nil
  end
end
function GameObjectBattleReplay:TransformShip()
  DebugOutBattlePlay("TransformShip")
  DebugOutBattlePlay(self.transfromShipName_Left)
  DebugOutBattlePlay(self.transfromShipName_Right)
  if self.transfromShipName_Left ~= nil then
    self:GetFlashObject():InvokeASCallback("_root", "TransformShip", true, self.transfromShipName_Left, self.transfromShipGridIndex_Left)
  elseif self.transfromShipName_Right ~= nil then
    self:GetFlashObject():InvokeASCallback("_root", "TransformShip", false, self.transfromShipName_Right, self.transfromShipGridIndex_Right)
  end
end
function GameObjectBattleReplay:UpdateStepBuffs(step)
  local roundData = self.m_battleResult.rounds[self.m_currentRoundIndex]
  if roundData and #roundData.buff > 0 then
    local buffData = roundData.buff
    local nextAttackIndex = -1
    local nextAttackData = MultipleAtkData[GameObjectBattleReplay.curMultipleAtkRound]
    if step == BuffUpdateStep.AfterSPAttack and GameObjectBattleReplay.isMultipleAtk and nextAttackData then
      nextAttackIndex = nextAttackData[1].index
    end
    for i, v in ipairs(buffData) do
      if v.buff_step == step and nextAttackIndex == -1 or v.index ~= nil and nextAttackIndex > v.index then
        local curSide
        if self.m_isPlayer1ShowInLeft and v.side == SIDE_LEFT then
          curSide = SIDE_LEFT
        elseif self.m_isPlayer1ShowInLeft and v.side == SIDE_RIGHT then
          curSide = SIDE_RIGHT
        elseif not self.m_isPlayer1ShowInLeft and v.side == SIDE_LEFT then
          curSide = SIDE_RIGHT
        elseif not self.m_isPlayer1ShowInLeft and v.side == SIDE_RIGHT then
          curSide = SIDE_LEFT
        end
        if curSide then
          if GameDataAccessHelper:IsFullScreenBuff(v.buff_effect) then
            self:UpdateFullScreenBuff(curSide, v.buff_effect, v.round_cnt)
          else
            self:UpdateBuff(curSide, v.pos, v.buff_effect, v.round_cnt, v.effection, v.param)
          end
        end
      end
    end
  end
end
function GameObjectBattleReplay:PlayShipAnim(isLeft, gridIndex, animType, effectName, isSwapDepth)
  DebugOutBattlePlay("PlayShipAnim", isLeft, gridIndex, animType, effectName, isSwapDepth)
  if animType ~= AnimType.DEAD then
    self:GetFlashObject():InvokeASCallback("_root", "playShipEffect", isLeft, gridIndex, effectName .. animType, isSwapDepth)
    local bgEffect = "normal"
    if animType == AnimType.ATTACK then
      bgEffect = "attack"
    else
      bgEffect = "hurt"
      local fleetDatas = self.m_fleetInfo.Left
      if not isLeft then
        fleetDatas = self.m_fleetInfo.Right
      end
      local fleetData = fleetDatas[gridIndex]
      if fleetData then
        fleetData.TagHurtRoundStep = self.m_currentRoundStep
        fleetData.TagHurtRoundIndex = self.m_currentRoundIndex
        fleetData.TagHurtAttackRoundIndex = self.m_currentRoundStepInfo.AttackRoundIndex
        fleetData.isPlayingHurtAnim = true
      end
      if fleetData and string.find(animType, "miss") then
        self:GetFlashObject():InvokeASCallback("_root", "showShipAngryBar", isLeft, gridIndex, math.min(100, fleetData.currentAcc))
        if 100 <= fleetData.currentAcc and not fleetData.isDead then
          GameObjectBattleReplay:ShowShipFullEnergy(isLeft, gridIndex, fleetData)
        else
          self:GetFlashObject():InvokeASCallback("_root", "hideShipFullEnergyEffect", isLeft, gridIndex)
        end
      end
    end
    DebugOutBattlePlay(isLeft, gridIndex, bgEffect)
    self:GetFlashObject():InvokeASCallback("_root", "playShipBGEffect", isLeft, gridIndex, bgEffect)
  else
    local fleetDatas = self.m_fleetInfo.Left
    if not isLeft then
      fleetDatas = self.m_fleetInfo.Right
    end
    local fleetData = fleetDatas[gridIndex]
    local isNormalDeadAni = true
    for k, v in pairs(fleetData.buffs) do
      if v.EffectID == 44 then
        isNormalDeadAni = false
        break
      end
    end
    DebugOut("PlayShipAnim:Dead:", isNormalDeadAni)
    if isNormalDeadAni then
      self:GetFlashObject():InvokeASCallback("_root", "playShipEffect", isLeft, gridIndex, effectName .. animType, isSwapDepth)
    elseif GameDataAccessHelper:CheckSPAttackEffectCanPlay("IndraD") then
      self:GetFlashObject():InvokeASCallback("_root", "playShipEffect", isLeft, gridIndex, "dead_boom" .. animType, isSwapDepth)
    else
      self:GetFlashObject():InvokeASCallback("_root", "playShipEffect", isLeft, gridIndex, effectName .. animType, isSwapDepth)
    end
  end
end
function GameObjectBattleReplay:ShowShipFullEnergy(isLeft, gridIndex, fleetData)
  local level = 1
  print("ShowShipFullEnergy")
  DebugOutPutTable(self.m_battleResult, "ShowShipFullEnergy")
  local fleets = self.m_battleResult.player1_fleets
  if not isLeft then
    fleets = self.m_battleResult.player2_fleets
  end
  DebugOutPutTable(fleets, "fleets")
  fleetData = nil
  for _, v in pairs(fleets) do
    if v.pos == gridIndex then
      fleetData = v
      break
    end
  end
  if fleetData and fleetData.level and fleetData.level > 15 then
    level = 2
  end
  self:GetFlashObject():InvokeASCallback("_root", "showShipFullEnergy", isLeft, gridIndex, level)
end
function GameObjectBattleReplay:CheckEffectNameCanPlay(effectName)
  if GameDataAccessHelper:CheckSPAttackEffectCanPlay(effectName) then
    return effectName
  else
    return "normal_new2"
  end
end
function GameObjectBattleReplay:PlayNextAttackEffect()
  DebugOutBattlePlay("PlayNextAttackEffect ", self.m_currentRoundStepInfo.AttackRoundIndex, self.m_currentRoundIndex)
  local roundData = self.m_battleResult.rounds[self.m_currentRoundIndex]
  local roundStepData = roundData.attacks
  local isPlayer1Action = roundData.player1_action
  self.m_currentRoundStepInfo.AttackRoundIndex = self.m_currentRoundStepInfo.AttackRoundIndex + 1
  local attackData = roundStepData[self.m_currentRoundStepInfo.AttackRoundIndex]
  if attackData ~= nil then
    local effectName = "normal"
    if attackData.atk_effect ~= "" and attackData.atk_effect ~= nil then
      effectName = attackData.atk_effect
    end
    local defSide
    DebugOut("attackData:")
    DebugTable(attackData)
    if attackData.def_side == "" then
      if isPlayer1Action and self.m_isPlayer1ShowInLeft or not isPlayer1Action and not self.m_isPlayer1ShowInLeft then
        defSide = SIDE_RIGHT
      else
        defSide = SIDE_LEFT
      end
      DebugOut(isPlayer1Action, self.m_isPlayer1ShowInLeft)
      DebugOut("1:", defSide)
    elseif attackData.def_side == SIDE_LEFT and self.m_isPlayer1ShowInLeft or attackData.def_side == SIDE_RIGHT and not self.m_isPlayer1ShowInLeft then
      defSide = SIDE_LEFT
      DebugOut("2:", defSide)
    else
      defSide = SIDE_RIGHT
      DebugOut("3", defSide)
    end
    if self.m_currentRoundStepInfo.AttackRoundIndex % 2 == 0 then
      local atkSide
      local data = roundStepData[self.m_currentRoundStepInfo.AttackRoundIndex - 1]
      if data.def_side == "" then
        isPlayer1Action = not isPlayer1Action
        if defSide == SIDE_LEFT then
          defSide = SIDE_RIGHT
        else
          defSide = SIDE_LEFT
        end
      else
        if data.def_side == SIDE_LEFT and self.m_isPlayer1ShowInLeft or data.def_side == SIDE_RIGHT and not self.m_isPlayer1ShowInLeft then
          atkSide = SIDE_LEFT
        else
          atkSide = SIDE_RIGHT
        end
        DebugOut("PlayNextAttackEffect:block")
        DebugOut(atkSide)
        if defSide ~= atkSide then
          isPlayer1Action = not isPlayer1Action
          DebugOut("change isPlayer1Action:" .. tostring(isPlayer1Action))
        end
      end
    end
    self.m_currentRoundStepInfo.TotalReactionCount = 2
    self.m_currentRoundStepInfo.RectionCount = 0
    DebugOutBattlePlay("TotalReactionCount = 2 in GameObjectBattleReplay:PlayNextAttackEffect: " .. self.m_currentRoundStepInfo.TotalReactionCount)
    local isLeftShip = isPlayer1Action and self.m_isPlayer1ShowInLeft or not isPlayer1Action and not self.m_isPlayer1ShowInLeft
    if self:IsStayTargetBuff(isLeftShip, attackData.atk_pos, 202) then
      effectName = self:CheckEffectNameCanPlay("MaqneticStorm2")
    end
    if effectName ~= "normal" and effectName ~= "normal_new2" then
      effectName = self:CheckEffectNameCanPlay(effectName)
    end
    self:PlayShipAnim(isLeftShip, attackData.atk_pos, AnimType.ATTACK, effectName, true)
    self:UpdateFleetAccInfo(isLeftShip, attackData.atk_pos, attackData.atk_acc_change, attackData.atk_acc)
    if self.m_currentRoundStepInfo.AttackRoundIndex ~= 1 then
      self:GetFlashObject():InvokeASCallback("_root", "playShipStatusAnim", isLeftShip, attackData.atk_pos, AttackStatuTypes.SQUELCH)
    end
    local isRepeatData = self:UpdateFleetHPInfo(SIDE_LEFT == defSide, attackData.def_pos, -attackData.dur_damage, -attackData.shield_damage, attackData.shield, 0 < attackData.shield, attackData.crit, attackData.intercept, attackData.is_immune, attackData.durability, attackData.index)
    self:UpdateFleetAccInfo(SIDE_LEFT == defSide, attackData.def_pos, attackData.acc_change, attackData.acc)
    if not attackData.hit or attackData.is_immune then
      self:PlayShipAnim(SIDE_LEFT == defSide, attackData.def_pos, AnimType.MISS .. "_center", effectName, true)
    elseif attackData.intercept then
      self:PlayShipAnim(SIDE_LEFT == defSide, attackData.def_pos, AnimType.BLOCK .. "_center", effectName, true)
    elseif 0 < attackData.shield then
      self:PlayShipAnim(SIDE_LEFT == defSide, attackData.def_pos, AnimType.SHIELD .. "_center", effectName, true)
    else
      self:PlayShipAnim(SIDE_LEFT == defSide, attackData.def_pos, AnimType.HURT .. "_center", effectName, true)
    end
    if self.m_currentRoundStepInfo.AttackRoundIndex == 1 then
      self:UpdateStepBuffs(BuffUpdateStep.AfterBattle)
    elseif self.m_currentRoundStepInfo.AttackRoundIndex == 2 then
      self:UpdateStepBuffs(BuffUpdateStep.AfterAbsorbed)
    end
  else
    self:PlayRoundStepEffect(RoundStep.SP_ATTACK)
  end
end
function GameObjectBattleReplay:GetSPHurtEffectDir(hurtPos, refPos, isLeft)
  if refPos == nil then
    return "_center"
  elseif hurtPos == refPos - 3 or hurtPos == refPos - 6 then
    return "_back"
  elseif hurtPos == refPos + 3 or hurtPos == refPos + 6 then
    return "_front"
  elseif hurtPos == refPos - 1 or hurtPos == refPos - 2 then
    if isLeft then
      return "_left"
    else
      return "_right"
    end
  elseif hurtPos == refPos + 1 or hurtPos == refPos + 2 then
    if isLeft then
      return "_right"
    else
      return "_left"
    end
  elseif hurtPos == refPos then
    return "_center"
  else
    DebugOutBattlePlay("fuck invalid pos")
    assert(0)
    return ""
  end
end
function GameObjectBattleReplay:GetSpellCenterPos(spellId, firstHurtPos)
  local rangeType = GameDataAccessHelper:GetSpellRealRangeType(spellId)
  if rangeType == 1 or rangeType == 5 then
    return nil
  elseif rangeType == 2 then
    return firstHurtPos
  elseif rangeType == 3 then
    return firstHurtPos
  elseif rangeType == 4 then
    return firstHurtPos
  elseif rangeType == 7 then
    return firstHurtPos
  end
end
function GameObjectBattleReplay:IsAdjutantAttack(isPlayer1, adjutant_id)
  local adjutantFleets = self.m_battleResult.player1_adjutant
  if not isPlayer1 then
    adjutantFleets = self.m_battleResult.player2_adjutant
  end
  if not adjutantFleets then
    DebugOutBattlePlay("adjutantFleets is nil")
    return false
  end
  DebugOut("IsAdjutantAttack")
  DebugOutBattlePlayTable(adjutantFleets)
  for k, v in pairs(adjutantFleets) do
    DebugOutBattlePlay(v.adjutant .. "," .. adjutant_id)
    if v.adjutant and v.adjutant == adjutant_id then
      DebugOutBattlePlay("adjutant's key:" .. k)
      return k
    end
  end
  return false
end
function GameObjectBattleReplay:CheckRemoveFullScreenBuff(isLeft)
  if not GameObjectBattleReplay.FullScreenBuff then
    DebugOutBattlePlay("error: FullScreenBuff is nil")
    return
  end
  local FullScreenBuffLocal = GameObjectBattleReplay.FullScreenBuff
  for i = #FullScreenBuffLocal, 1, -1 do
    local v = FullScreenBuffLocal[i]
    DebugOutBattlePlay("CheckRemoveFullScreenBuf", v.roundCnt)
    if isLeft == v.isLeft and v.roundCnt <= 0 then
      DebugOutBattlePlay("CheckRemoveFullScreenBuf remove")
      local effectName = GameDataAccessHelper:GetBuffEffectName(v.effect_id)
      self:GetFlashObject():InvokeASCallback("_root", "HideFullScreenBuffEffect", v.isLeft, effectName)
      table.remove(FullScreenBuffLocal, i)
    end
  end
  DebugOutBattlePlayTable(FullScreenBuffLocal)
end
function GameObjectBattleReplay:CheckPlayFullScreenBuff(isLeft)
  if not GameObjectBattleReplay.FullScreenBuff then
    DebugOutBattlePlay("error:FullScreenBuff is nil")
    return
  end
  local FullScreenBuffLocal = GameObjectBattleReplay.FullScreenBuff
  for i, v in ipairs(FullScreenBuffLocal) do
    DebugOutBattlePlay("zm CheckPlayFullScreenBuff", v.isTagPlay, v.effect_id, isLeft)
    if v.isTagPlay and isLeft == v.isLeft then
      local buffEffect = GameDataAccessHelper:GetBuffEffectName(v.effect_id)
      if buffEffect and buffEffect ~= "" then
        DebugOutBattlePlay("zm playFullScreenEffect ", v.isLeft, buffEffect)
        self:GetFlashObject():InvokeASCallback("_root", "PlayFullScreenBuffEffect", v.isLeft, buffEffect)
      end
      v.isTagPlay = false
    end
  end
end
function GameObjectBattleReplay:AddFullScreenBuff(isLeft, effectID, roundCnt)
  if effectID == 0 then
    return
  end
  if not GameObjectBattleReplay.FullScreenBuff then
    GameObjectBattleReplay.FullScreenBuff = {}
  end
  local FullScreenBuffLocal = GameObjectBattleReplay.FullScreenBuff
  for i, v in ipairs(FullScreenBuffLocal) do
    if effectID == v.effect_id and isLeft == v.isLeft then
      v.roundCnt = math.max(v.roundCnt, roundCnt)
      return
    end
  end
  local FullScreenBuffData = {}
  FullScreenBuffData.isLeft = isLeft
  FullScreenBuffData.effect_id = effectID
  FullScreenBuffData.roundCnt = roundCnt
  FullScreenBuffData.isTagPlay = true
  table.insert(FullScreenBuffLocal, FullScreenBuffData)
end
function GameObjectBattleReplay:UpdateFullScreenBuff(side, effectID, roundCnt)
  if not GameDataAccessHelper:IsFullScreenBuff(effectID) then
    return
  end
  if not GameObjectBattleReplay.FullScreenBuff then
    DebugOutBattlePlay("error:FullScreenBuff is nil")
    return
  end
  local isLeft = side == SIDE_LEFT
  DebugOutBattlePlay("UpdateFullScreenBuff", side, effectID, roundCnt)
  for i, v in ipairs(GameObjectBattleReplay.FullScreenBuff) do
    if v.effect_id == effectID and v.isLeft == isLeft then
      DebugOut("set full screen buf round count", roundCnt)
      v.roundCnt = roundCnt
      break
    end
  end
end
function GameObjectBattleReplay:IsStayTargetBuff(isLeft, gridIndex, effectID)
  DebugOut("GameObjectBattleReplay:IsStayTargetBuff:")
  DebugOut(isLeft)
  DebugOut(gridIndex)
  if effectID == 0 then
    return false
  end
  local fleetDatas = self.m_fleetInfo.Left
  if not isLeft then
    fleetDatas = self.m_fleetInfo.Right
  end
  DebugTable(fleetDatas)
  local fleetData = fleetDatas[gridIndex]
  if fleetData == nil or 0 >= fleetData.currentHP then
    DebugOutBattlePlay("ship is dead already, ignore new buff ", effectID)
    return false
  end
  for i, v in ipairs(fleetData.buffs) do
    if v.EffectID == effectID then
      DebugOut("IsStayTargetBuff:" .. tostring(effectID))
      return true
    end
  end
  return false
end
function GameObjectBattleReplay:AddBuff(isLeft, gridIndex, effectID, roundCnt, isPlayBuffEffect, param, isSelfBuff, isSelfCalled)
  if effectID == 0 then
    return
  end
  if roundCnt == 0 then
    return
  end
  DebugOutBattlePlay("Add Buff ", isLeft, gridIndex, effectID, isPlayBuffEffect, roundCnt, param, isSelfBuff, isSelfCalled)
  local fleetDatas = self.m_fleetInfo.Left
  if not isLeft then
    fleetDatas = self.m_fleetInfo.Right
  end
  local fleetData = fleetDatas[gridIndex]
  if 0 >= fleetData.currentHP then
    DebugOutBattlePlay("ship is dead already, ignore new buff ", effectID)
    return
  end
  fleetData.BuffLayerState = fleetData.BuffLayerState or {
    [1] = false,
    [2] = false,
    [3] = false,
    [4] = false
  }
  local BuffData
  for i, v in ipairs(fleetData.buffs) do
    if v.EffectID == effectID then
      BuffData = v
      BuffData.RoundCount = math.max(roundCnt, BuffData.RoundCount)
      BuffData.TagPlayBuff = isPlayBuffEffect
      BuffData.Param = param
      BuffData.isSelfBuff = false
      if isSelfCalled and isSelfBuff then
        BuffData.isSelfBuff = true
      end
      if self.m_currentRoundStepInf and self.m_currentRoundStepInfo.sp_atk_type == 1 and v.EffectID == 48 then
        BuffData.isRemoveSpecialBuffEffect = false
      end
      break
    end
  end
  if BuffData == nil then
    if LIMITE_BUFF_COUNT and 4 <= #fleetData.buffs then
      DebugOutBattlePlay("Warning : too many buffs in the same ship")
      return
    end
    BuffData = {}
    BuffData.EffectID = effectID
    BuffData.Layer = 1
    BuffData.RoundCount = roundCnt
    BuffData.TagPlayBuff = isPlayBuffEffect
    BuffData.TagPlayDesc = false
    BuffData.Param = param
    BuffData.isSelfBuff = isSelfBuff
    if self.m_currentRoundStepInf and self.m_currentRoundStepInfo.sp_atk_type == 1 and effectID == 48 then
      BuffData.isRemoveSpecialBuffEffect = false
    end
    local buffDesc = GameDataAccessHelper:GetBuffEffectDesc(effectID)
    if not buffDesc or buffDesc == "" or buffDesc == "Purify" then
      table.insert(fleetData.buffs, BuffData)
    else
      table.insert(fleetData.buffs, 1, BuffData)
    end
    local buffIndex = 1
    for k, v in pairs(fleetData.buffs) do
      v.Layer = buffIndex
      fleetData.BuffLayerState[buffIndex] = true
      if buffIndex <= self.MaxShowBuffCount and v.EffectID ~= 31 then
        v.TagPlayDesc = true
      end
      buffIndex = buffIndex + 1
    end
    DebugOut("AddBuff:test:")
    DebugTable(fleetData.buffs)
  end
  if not isSelfCalled and effectID ~= 48 and self.m_currentRoundStepInf and self.m_currentRoundStepInfo.sp_atk_type == 1 then
    fleetData.specialBuffEffect = self.m_currentRoundStepInfo.buffEffectName
  end
  if BuffData.RoundCount == 0 then
    DebugOutBattlePlay("remove buff")
  end
  if self.m_currentRoundIndex == nil or self.m_battleResult == nil or self.m_battleResult.rounds == nil or self.m_battleResult.rounds[self.m_currentRoundIndex] == nil then
    return
  end
  local isPlayer1Action = self.m_battleResult.rounds[self.m_currentRoundIndex].player1_action
  local isLeftShip = isPlayer1Action and self.m_isPlayer1ShowInLeft or not isPlayer1Action and not self.m_isPlayer1ShowInLeft
  if isLeft == isLeftShip and effectID == 48 and not isSelfCalled and self.m_currentRoundStepInf and self.m_currentRoundStepInfo.BuffTable then
    for i = 1, #self.m_currentRoundStepInfo.BuffTable do
      local buffDetail = self.m_currentRoundStepInfo.BuffTable[i]
      self:AddBuff(isLeft, gridIndex, buffDetail.EffectID, buffDetail.roundCnt, true, buffDetail.param, buffDetail.isSelf, true)
    end
  end
  DebugOutBattlePlayTable(BuffData)
end
function GameObjectBattleReplay:FindAllBuffEffectType()
  local roundData = self.m_battleResult.rounds[self.m_currentRoundIndex]
  local isPlayer1Action = roundData.player1_action
  local spAttackData = {}
  for k, v in pairs(roundData.sp_attacks) do
    if v.atk_side and v.atk_side ~= "" then
      isPlayer1Action = v.atk_side == "p1"
    else
      isPlayer1Action = roundData.player1_action
    end
    if (v.adjutant == 0 or not self:IsAdjutantAttack(player1Action, v.adjutant)) and (v.artifact == nil or v.artifact == 0) and v.atk_type == AtkType.AbilityDrained then
      self.m_currentRoundStepInfo.sp_atk_type = 1
      if v.buff_effect == 48 then
        self.m_currentRoundStepInfo.buffEffectName = GameDataAccessHelper:GetBuffEffectName(v.buff_effect)
      end
      if self.m_currentRoundStepInfo.BuffAttrs == nil then
        self.m_currentRoundStepInfo.BuffAttrs = {}
      end
      local hasAttr = false
      for i = 1, #self.m_currentRoundStepInfo.BuffAttrs do
        if self.m_currentRoundStepInfo.BuffAttrs[i] == v.absorb_attr_type then
          hasAttr = true
          break
        end
      end
      if not hasAttr then
        table.insert(self.m_currentRoundStepInfo.BuffAttrs, v.absorb_attr_type)
      end
      if self.m_currentRoundStepInfo.BuffTable == nil then
        self.m_currentRoundStepInfo.BuffTable = {}
      end
      local hasBuff = false
      for i = 1, #self.m_currentRoundStepInfo.BuffTable do
        if self.m_currentRoundStepInfo.BuffTable[i].EffectID == v.buff_effect then
          self.m_currentRoundStepInfo.BuffTable[i].roundCnt = v.round_cnt
          self.m_currentRoundStepInfo.BuffTable[i].param = v.param
          self.m_currentRoundStepInfo.BuffTable[i].isSelf = true
          hasBuff = true
          break
        end
      end
      if not hasBuff then
        local buffDetail = {}
        buffDetail.EffectID = v.buff_effect
        buffDetail.roundCnt = v.round_cnt
        buffDetail.param = v.param
        buffDetail.isSelf = true
        table.insert(self.m_currentRoundStepInfo.BuffTable, buffDetail)
      end
    end
  end
  DebugOut("GameObjectBattleReplay:FindAllBuffEffectType", self.m_currentRoundStepInfo.sp_atk_type, self.m_currentRoundStepInfo.buffEffectName)
  DebugTable(self.m_currentRoundStepInfo.BuffAttrs)
  DebugTable(self.m_currentRoundStepInfo.BuffTable)
end
function GameObjectBattleReplay:AddBuffNameEffect(isLeft, gridIndex, effectID, isAttacked, attr_type, isSelfCalled)
  if effectID == 0 then
    return
  end
  DebugOutBattlePlay("AddBuffNameEffect ", isLeft, gridIndex, effectID, attr_type, isAttacked)
  local fleetDatas = self.m_fleetInfo.Left
  if not isLeft then
    fleetDatas = self.m_fleetInfo.Right
  end
  local fleetData = fleetDatas[gridIndex]
  if 0 >= fleetData.currentHP then
    DebugOutBattlePlay("ship is dead already, ignore BuffNameEffect ", effectID)
    return
  end
  if self.m_currentRoundStepInfo.NameAnimFleetInfo == nil then
    self.m_currentRoundStepInfo.NameAnimFleetInfo = {}
  end
  local fleetFlag = gridIndex .. "_" .. (isLeft and "1" or "2")
  local hasAdded = false
  for k, v in ipairs(self.m_currentRoundStepInfo.NameAnimFleetInfo) do
    if v and v.fleetFlag == fleetFlag and attr_type ~= 0 then
      hasAdded = true
    end
  end
  if hasAdded == false and attr_type ~= 0 then
    local info = {}
    info.fleetFlag = fleetFlag
    info.isCheckedOver = false
    table.insert(self.m_currentRoundStepInfo.NameAnimFleetInfo, info)
  end
  DebugOut("NameAnimFleetInfo :")
  DebugTable(self.m_currentRoundStepInfo.NameAnimFleetInfo)
  local BuffNameEffectData
  if fleetData.BuffNameEffect ~= nil then
    for i, v in ipairs(fleetData.BuffNameEffect) do
      if v.absorb_attr_type == attr_type then
        BuffNameEffectData = v
        BuffNameEffectData.isPlayed = false
        BuffNameEffectData.isAttacked = isAttacked
        self.m_currentRoundStepInfo.BuffNameEffectTotalCount = self.m_currentRoundStepInfo.BuffNameEffectTotalCount + 1
        DebugOut(" self.m_currentRoundStepInfo.BuffNameEffectTotalCount :", self.m_currentRoundStepInfo.BuffNameEffectTotalCount)
        break
      end
    end
  end
  if BuffNameEffectData == nil and attr_type ~= 0 then
    BuffNameEffectData = {}
    BuffNameEffectData.EffectID = effectID
    BuffNameEffectData.isPlayed = false
    BuffNameEffectData.isAttacked = isAttacked
    BuffNameEffectData.absorb_attr_type = attr_type
    if fleetData.BuffNameEffect == nil then
      fleetData.BuffNameEffect = {}
    end
    table.insert(fleetData.BuffNameEffect, BuffNameEffectData)
    self.m_currentRoundStepInfo.BuffNameEffectTotalCount = self.m_currentRoundStepInfo.BuffNameEffectTotalCount + 1
    DebugOut(" self.m_currentRoundStepInfo.BuffNameEffectTotalCount :", self.m_currentRoundStepInfo.BuffNameEffectTotalCount)
    DebugOut("BuffNameEffectData:test:")
    DebugTable(fleetData.BuffNameEffect)
  end
  if not isAttacked and effectID == 48 and not isSelfCalled then
    for i = 1, #self.m_currentRoundStepInfo.BuffAttrs do
      self:AddBuffNameEffect(isLeft, gridIndex, effectID, isAttacked, self.m_currentRoundStepInfo.BuffAttrs[i], true)
    end
  end
end
function GameObjectBattleReplay:UpdateBuff(side, gridIndex, effectID, roundCnt, effection, param, isDot)
  if effectID == 0 then
    return
  end
  assert(side == SIDE_LEFT or side == SIDE_RIGHT)
  local isLeft = side == SIDE_LEFT
  if not self:CheckInsertReLiveFleet(isLeft, gridIndex, effectID) then
    DebugOutBattlePlay("UpdateBuff", self.m_currentRoundIndex, isLeft, gridIndex, effectID, roundCnt)
    local fleetDatas = self.m_fleetInfo.Left
    if not isLeft then
      fleetDatas = self.m_fleetInfo.Right
    end
    local fleetData = fleetDatas[gridIndex]
    assert(fleetData ~= nil)
    if not fleetData then
      return
    end
    local BuffData
    for i, v in ipairs(fleetData.buffs) do
      if v.EffectID == effectID then
        BuffData = v
        BuffData.RoundCount = roundCnt
        BuffData.Param = param
        break
      end
    end
    if BuffData == nil then
      DebugOutBattlePlay("fuck fuck buff errror")
      return
    end
    BuffData.isDot = isDot
    if BuffData.RoundCount <= 1 then
      BuffData.TagRemove = true
      if BuffData.EffectID == 48 and BuffData.isRemoveSpecialBuffEffect ~= nil and BuffData.isRemoveSpecialBuffEffect == false then
        BuffData.isRemoveSpecialBuffEffect = true
      end
    end
    if effection and effection == 1 then
      local effectTitleName = GameLoader:GetGameText("LC_FLEET_BUFF_BECOME_EFFECTIVE_" .. effectID)
      self:GetFlashObject():InvokeASCallback("_root", "playShipSPEffectTitle", isLeft, gridIndex, effectTitleName, false)
      DebugOut("effectTitleName:" .. effectTitleName)
    else
      DebugOut("effectTitleName:")
      DebugOut(effection)
    end
    DebugOutBattlePlayTable(BuffData)
  end
end
function GameObjectBattleReplay:CheckFleetsRemoveBuff(isLeft, dotRemove)
  for i = 1, 9 do
    self:CheckRemoveBuff(isLeft, i, dotRemove)
  end
end
function GameObjectBattleReplay:CheckDeadForDamgeData(isLeft, gridIndex)
  local fleetDatas = self.m_fleetInfo.Left
  if not isLeft then
    fleetDatas = self.m_fleetInfo.Right
  end
  local fleetData = fleetDatas[gridIndex]
  if fleetData and fleetData.currentHP <= 0 then
    self:OnShipDead(isLeft, gridIndex)
  end
  if fleetData then
    fleetData.isNeedHurtAnim = false
    fleetData.isHurtAnimOver = false
    fleetData.specialBuffEffect = ""
    fleetData.curDamageIndex = 0
  end
end
function GameObjectBattleReplay:CheckAllShipRemoveBuff(dotRemove)
  self:CheckFleetsRemoveBuff(true, dotRemove)
  self:CheckFleetsRemoveBuff(false, dotRemove)
end
function GameObjectBattleReplay:CheckDotRemove(isLeft, gridIndex)
  local fleetDatas = self.m_fleetInfo.Left
  if not isLeft then
    fleetDatas = self.m_fleetInfo.Right
  end
  local fleetData = fleetDatas[gridIndex]
  if not fleetData then
    return
  end
  for _, v in ipairs(fleetData.buffs) do
    if v.TagRemove or v.RoundCount >= 1 then
      local buffEffect = GameDataAccessHelper:GetBuffEffectName(v.EffectID)
      if GameDataAccessHelper:IsTransformationBuff(v.EffectID) then
        self:GetFlashObject():InvokeASCallback("_root", "HideShipBuffEffect", isLeft, gridIndex, buffEffect)
      else
        self:GetFlashObject():InvokeASCallback("_root", "HideShipBuffEffect", isLeft, gridIndex, buffEffect)
      end
    end
  end
end
function GameObjectBattleReplay:CheckRemoveBuff(isLeft, gridIndex, dotRemove)
  local fleetDatas = self.m_fleetInfo.Left
  if not isLeft then
    fleetDatas = self.m_fleetInfo.Right
  end
  if not fleetDatas[gridIndex] then
    return
  end
  if fleetDatas[gridIndex].currentHP <= 0 then
    DebugOutBattlePlay("ship is dead already, do not play remove buff anim")
    return false
  end
  local fleetData = fleetDatas[gridIndex]
  local has48Buff = false
  local tempBuffsNeedRemove = {}
  local buffIndex = 1
  local allBuffCount = #fleetData.buffs
  DebugOut("all buff")
  DebugTable(fleetData.buffs)
  for _, v in ipairs(fleetData.buffs) do
    if v.TagRemove and (not v.isDot or dotRemove) then
      DebugOut("remove buff", v.EffectID)
      local buffEffect = GameDataAccessHelper:GetBuffEffectName(v.EffectID)
      local buffDesc = GameDataAccessHelper:GetBuffEffectDesc(v.EffectID)
      if v.isRemoveSpecialBuffEffect == true then
        buffEffect = GameDataAccessHelper:GetBuffEffectName(48)
      end
      if v.EffectID == 32 then
        self:UpdateFleetPolymorphHPInfo(isLeft, gridIndex, v.Param)
      end
      if v.Layer <= self.MaxShowBuffCount then
        self:GetFlashObject():InvokeASCallback("_root", "HideShipBuffDesc", isLeft, gridIndex, buffDesc, v.Layer)
      end
      if GameDataAccessHelper:IsTransformationBuff(v.EffectID) then
        self:GetFlashObject():InvokeASCallback("_root", "HideShipBuffEffect", isLeft, gridIndex, buffEffect)
      else
        self:GetFlashObject():InvokeASCallback("_root", "HideShipBuffEffect", isLeft, gridIndex, buffEffect)
      end
      table.insert(tempBuffsNeedRemove, v.EffectID)
      fleetData.BuffLayerState[v.Layer] = false
      v.TagRemove = false
    end
    if v.EffectID == 48 then
      has48Buff = true
    end
    buffIndex = buffIndex + 1
  end
  local isRemoveBuff = false
  for it, vt in ipairs(tempBuffsNeedRemove) do
    for k, v in ipairs(fleetData.buffs) do
      if v.EffectID == vt then
        table.remove(fleetData.buffs, k)
        isRemoveBuff = true
        if v.EffectID == 31 then
          DebugOut("remove 31:")
          DebugTable(fleetData.buffs)
        end
        break
      end
    end
  end
  if isRemoveBuff then
    DebugOut("is remove buff")
    DebugTable(fleetData.buffs)
  end
  if isRemoveBuff then
    local buffTemp = {}
    local buffCnt = #fleetData.buffs
    if has48Buff then
      buffCnt = buffCnt - 1
    end
    for i = 1, #fleetData.buffs do
      table.insert(buffTemp, fleetData.buffs[i])
      if fleetData.buffs[i].Layer <= self.MaxShowBuffCount then
        local buffDesc = GameDataAccessHelper:GetBuffEffectDesc(fleetData.buffs[i].EffectID)
        self:GetFlashObject():InvokeASCallback("_root", "HideShipBuffDesc", isLeft, gridIndex, buffDesc, fleetData.buffs[i].Layer)
      end
    end
    fleetData.buffs = {}
    fleetData.BuffLayerState = nil
    for i = 1, #buffTemp do
      self:AddBuff(isLeft, gridIndex, buffTemp[i].EffectID, buffTemp[i].RoundCount, buffTemp[i].TagPlayBuff ~= nil and buffTemp[i].TagPlayBuff or false, buffTemp[i].Param, buffTemp[i].isSelfBuff)
      if buffTemp[i].isDot and buffTemp[i].TagRemove then
        for k, v in ipairs(fleetData.buffs) do
          if v.EffectID == buffTemp[i].EffectID then
            v.isDot = buffTemp[i].isDot
            v.TagRemove = buffTemp[i].TagRemove
          end
        end
      end
    end
  end
  return #tempBuffsNeedRemove ~= 0
end
function GameObjectBattleReplay:CheckFleetsPlayBuff(isLeft)
  for i = 1, 9 do
    self:CheckPlayBuff(isLeft, i)
  end
end
function GameObjectBattleReplay:CheckAllShipPlayBuff()
  self:CheckFleetsPlayBuff(true)
  self:CheckFleetsPlayBuff(false)
end
function GameObjectBattleReplay:CheckPlayBuff(isLeft, gridIndex)
  local fleetDatas = self.m_fleetInfo.Left
  if not isLeft then
    fleetDatas = self.m_fleetInfo.Right
  end
  local fleetData = fleetDatas[gridIndex]
  if not fleetData then
    return
  end
  if fleetDatas[gridIndex].currentHP <= 0 then
    return
  end
  DebugOutBattlePlayTable(fleetData.buffs)
  local buffIndex = 1
  for i, v in ipairs(fleetData.buffs) do
    if v.TagPlayDesc then
      local buffDesc = GameDataAccessHelper:GetBuffEffectDesc(v.EffectID)
      DebugOut(" CheckPlayBuffcc buffDesc : ", buffDesc)
      if v.isSelfBuff and buffDesc then
        buffDesc = string.sub(buffDesc, 0, string.len(buffDesc) - 3) .. "add"
      end
      DebugOutBattlePlay("not PlayShipBuffEffect", buffDesc, v.EffectID)
      if buffDesc and buffDesc ~= "" and v.Layer <= self.MaxShowBuffCount then
        DebugOutBattlePlay("PlayShipBuffDesc:" .. buffDesc .. "," .. v.Layer)
        self:GetFlashObject():InvokeASCallback("_root", "PlayShipBuffDesc", isLeft, gridIndex, buffDesc, v.Layer)
      end
      v.TagPlayDesc = false
    end
    local buffEffect = GameDataAccessHelper:GetBuffEffectName(v.EffectID)
    if (self.m_currentRoundStepInfo and self.m_currentRoundStepInfo.sp_atk_type == 1 or v.EffectID == 48) and v.isSelfBuff == true then
      buffEffect = fleetData.specialBuffEffect
    end
    if v.TagPlayBuff and buffEffect and buffEffect ~= "" then
      DebugOutBattlePlay("GameObjectBattleReplay:CheckPlayBuff: ", isLeft, gridIndex, buffEffect, v.EffectID)
      self:playBuffEffect(isLeft, gridIndex, buffEffect)
      v.TagPlayBuff = false
    end
    buffIndex = buffIndex + 1
  end
end
function GameObjectBattleReplay:CheckAllBuffNameEffect()
  self.isBegainBuffNameEffect = true
  self:CheckFleetsPlayBuffNameEffect(true)
  self:CheckFleetsPlayBuffNameEffect(false)
end
function GameObjectBattleReplay:CheckFleetsPlayBuffNameEffect(isLeft)
  for i = 1, 9 do
    self:CheckBuffNameEffect(isLeft, i)
  end
end
function GameObjectBattleReplay:CheckBuffNameEffect(isLeft, gridIndex)
  DebugOutBattlePlay("CheckFleetsPlayBuffNameEffect ", isLeft, gridIndex)
  local fleetDatas = self.m_fleetInfo.Left
  if not isLeft then
    fleetDatas = self.m_fleetInfo.Right
  end
  local fleetData = fleetDatas[gridIndex]
  DebugTable(fleetData)
  if not fleetData then
    return
  end
  if fleetData.BuffNameEffect == nil or #fleetData.BuffNameEffect < 1 then
    return
  end
  local needPlayBuffNameEffect = false
  local index = 1
  for i = 1, #fleetData.BuffNameEffect do
    if fleetData.BuffNameEffect[i].absorb_attr_type ~= 0 and fleetData.BuffNameEffect[i].isPlayed == false then
      needPlayBuffNameEffect = true
      index = i
      break
    end
  end
  if needPlayBuffNameEffect == false then
    self:IsAllFleetBuffNameAnimCheckedOver()
    DebugOut(" needPlayBuffNameEffect  == false")
    if 0 >= fleetData.currentHP and fleetData.isHurtAnimOver then
      self:OnShipDead(isLeft, gridIndex)
    end
    if self.m_currentRoundStepInfo.NameAnimFleetInfo then
      local fleetFlag = gridIndex .. "_" .. (isLeft and "1" or "2")
      for k, v in ipairs(self.m_currentRoundStepInfo.NameAnimFleetInfo) do
        if v.fleetFlag == fleetFlag then
          v.isCheckedOver = true
        end
      end
    end
    return
  end
  if needPlayBuffNameEffect then
    self:GetFlashObject():InvokeASCallback("_root", "playBuffNameEffect", isLeft, gridIndex, GameLoader:GetGameText("LC_MENU_Equip_param_" .. fleetData.BuffNameEffect[index].absorb_attr_type), fleetData.BuffNameEffect[index].isAttacked)
    fleetData.BuffNameEffect[index].isPlayed = true
  end
end
function GameObjectBattleReplay:PlayShipBGAnim(isLeft, gridIndex, effectName)
  local bgEffect = effectName
  local fleetDatas = self.m_fleetInfo.Left
  if not isLeft then
    fleetDatas = self.m_fleetInfo.Right
  end
  local fleetData = fleetDatas[gridIndex]
  DebugOutBattlePlay(isLeft, gridIndex, bgEffect)
  fleetData.TagHurtRoundStep = self.m_currentRoundStep
  fleetData.TagHurtRoundIndex = self.m_currentRoundIndex
  fleetData.TagHurtAttackRoundIndex = self.m_currentRoundStepInfo.AttackRoundIndex
  self:GetFlashObject():InvokeASCallback("_root", "PlayShipBGAnim", isLeft, gridIndex, bgEffect)
  DebugOutBattlePlay("PlayShipBGAnim")
end
function GameObjectBattleReplay:GetAdjutantLevel(adjutantID, adjutantFleetsData)
  for k, v in pairs(adjutantFleetsData) do
    if adjutantID == v.adjutant then
      return v.adjutant_level
    end
  end
  return 0
end
function GameObjectBattleReplay:PlayAdjutantSPEffect()
  local roundData = self.m_battleResult.rounds[self.m_currentRoundIndex]
  local spAttackData = roundData.sp_attacks
  local isPlayer1Action = roundData.player1_action
  DebugOutBattlePlay("PlayAdjutantSPEffect")
  self.AdjutantHurtAnimData = {}
  if spAttackData == nil or #spAttackData == 0 then
    self:PlayRoundStepEffect(RoundStep.ARTIFACT)
    return
  else
    local adjutantSPEffectData = {}
    for k, v in pairs(spAttackData) do
      DebugOutBattlePlay("key:" .. k)
      DebugOutBattlePlayTable(v)
      if v.atk_side and v.atk_side ~= "" then
        if v.adjutant ~= 0 then
          if self:IsAdjutantAttack(v.atk_side == "p1", v.adjutant) and (v.artifact == nil or v.artifact == 0) then
            adjutantSPEffectData[#adjutantSPEffectData + 1] = v
          end
        end
      elseif v.adjutant ~= 0 and self:IsAdjutantAttack(isPlayer1Action, v.adjutant) and (v.artifact == nil or v.artifact == 0) then
        adjutantSPEffectData[#adjutantSPEffectData + 1] = v
      end
    end
    if adjutantSPEffectData == nil or #adjutantSPEffectData == 0 then
      self:PlayRoundStepEffect(RoundStep.ARTIFACT)
      return
    end
    DebugOutBattlePlay("play spell title")
    local isLeftShip = isPlayer1Action and self.m_isPlayer1ShowInLeft or not isPlayer1Action and not self.m_isPlayer1ShowInLeft
    DebugOutBattlePlay("play adjutant animation")
    local adjutantFleetsData = self.m_battleResult.player1_adjutant
    if not isPlayer1Action then
      adjutantFleetsData = self.m_battleResult.player2_adjutant
    end
    local level = self:GetAdjutantLevel(adjutantSPEffectData[1].adjutant, adjutantFleetsData)
    local headAvatar = GameDataAccessHelper:GetFleetAvatar(adjutantSPEffectData[1].adjutant, level, nil)
    DebugOutBattlePlay("zm headAvatar " .. headAvatar)
    self:GetFlashObject():InvokeASCallback("_root", "playAdjutantAnim", isLeftShip, spAttackData[1].atk_pos, headAvatar, true)
    DebugOutBattlePlay("adjutant add buff")
    self.m_currentRoundStepInfo.TotalReactionCount = 1
    self.m_currentRoundStepInfo.RectionCount = 0
    local defenders = {}
    for i, v in ipairs(adjutantSPEffectData) do
      local isSelfCast = v.self_cast
      DebugOut("adjutant data:", v.self_cast, ",", v.is_time_back)
      DebugTable(v)
      if isSelfCast then
        if v.buff_effect == 0 then
          DebugOutBattlePlay("adjutant data: UpdateFleetAccInfo 1")
          if v.atk_type ~= AtkType.EnergyChangeBeforSPATTACK and v.atk_type ~= AtkType.EnergyChangeBeforAttack then
            DebugOutBattlePlay("adjutant data: UpdateFleetAccInfo 2 ! acc_change = " .. v.acc_change .. " acc = " .. v.acc)
            self:UpdateFleetAccInfo(isLeftShip, v.def_pos, v.acc_change, v.acc)
            DebugOut("adjutant data: showShipAngryBar = " .. v.acc)
            self:GetFlashObject():InvokeASCallback("_root", "showShipAngryBar", isLeftShip, v.def_pos, math.min(100, v.acc))
            if v.acc >= 100 and not GameObjectBattleReplay:GetFleetData(isLeftShip, v.def_pos).isDead then
              local fleetDatas = isLeftShip
              if not isLeft then
                fleetDatas = self.m_fleetInfo.Right
              end
              local fleetData = fleetDatas[v.def_pos]
              GameObjectBattleReplay:ShowShipFullEnergy(isLeftShip, v.def_pos, fleetData)
            else
              self:GetFlashObject():InvokeASCallback("_root", "hideShipFullEnergyEffect", not isLeftShip, v.def_pos)
            end
          end
        else
          local param, hotBuff
          for iBuff, vBuff in ipairs(roundData.buff) do
            if vBuff.buff_effect == v.buff_effect and vBuff.pos == v.def_pos and vBuff.index == v.index - 1 then
              param = vBuff.param
              hotBuff = vBuff
              break
            end
          end
          if param and param >= 0 and (v.buff_effect == 45 or v.buff_effect == 48 and param > 0) then
            self:PlayHotEffect(v.def_pos, isLeftShip, hotBuff)
          else
            self:AddBuff(isLeftShip, v.def_pos, v.buff_effect, v.round_cnt, true, param)
          end
          if v.is_time_back then
            DebugOut("roll back:", isLeftShip, ",", v.def_pos)
            local fleetDatas = self.m_fleetInfo.Left
            if not isLeftShip then
              fleetDatas = self.m_fleetInfo.Right
            end
            local fleetData = fleetDatas[v.def_pos]
            local isRepeatData = self:UpdateFleetHPInfo(isLeftShip, v.def_pos, v.durability - fleetData.currentHP, v.shield - fleetData.currentShield, v.shield, 0 < v.shield, v.crit, v.intercept, v.is_immune, v.durability, v.index)
            self:UpdateFleetAccInfo(isLeftShip, v.def_pos, v.acc - fleetData.currentAcc, v.acc)
            self:PlayShipBGAnim(isLeftShip, v.def_pos, "_hurt")
            if not isRepeatData then
              GameObjectBattleReplay:AddTotalReactionCount()
            end
          end
        end
      elseif v.round_cnt ~= 0 then
        if not v.hit then
          self.AdjutantHurtAnimData[#self.AdjutantHurtAnimData + 1] = {
            isLeftShip = not isLeftShip,
            gridIndex = v.def_pos,
            effectName = "_miss"
          }
        else
          DebugOutBattlePlay("PlayShipBGAnim:")
          self.AdjutantHurtAnimData[#self.AdjutantHurtAnimData + 1] = {
            isLeftShip = not isLeftShip,
            gridIndex = v.def_pos,
            effectName = "_hurt"
          }
        end
        self:AddBuff(not isLeftShip, v.def_pos, v.buff_effect, v.round_cnt, true, v.param)
        local key_pos = ""
        if isLeftShip then
          key_pos = "" .. v.def_pos .. "_2"
        else
          key_pos = "" .. v.def_pos .. "_1"
        end
        if not defenders[key_pos] then
          GameObjectBattleReplay:AddTotalReactionCount()
          defenders[key_pos] = true
        end
      end
    end
    DebugOutBattlePlay("adjutant TotalReactionCount:" .. self.m_currentRoundStepInfo.TotalReactionCount)
    self:UpdateStepBuffs(BuffUpdateStep.AfterSPAttack)
    self:UpdateDamageData(BattleDamageType.ADJUTANT)
  end
end
function GameObjectBattleReplay:CheckPlayArtifact()
  local roundData = self.m_battleResult.rounds[self.m_currentRoundIndex]
  local spAttackData = roundData.sp_attacks
  local isPlayer1Action = roundData.player1_action
  local artifactTypes = {}
  ArtifactSPData = {}
  RevengeSPData = {}
  ShootoffSPData = {}
  RevengeAttacker = {}
  ShootoffAttacker = {}
  RevengeDefender = {}
  ShootoffDefender = {}
  MultipleAtkData = {}
  GameObjectBattleReplay.isPlayingMultiEffect = false
  DebugOutBattlePlay("CheckPlayArtifact round ", self.m_currentRoundIndex)
  if spAttackData == nil or #spAttackData == 0 then
    self:PlayRoundStepEffect(RoundStep.ATTACK)
    return
  else
    for k, v in ipairs(spAttackData) do
      DebugOutBattlePlay("key:" .. k)
      DebugOutBattlePlayTable(v)
      if v.adjutant == 0 and v.artifact and 0 < v.artifact then
        ArtifactSPData[#ArtifactSPData + 1] = v
        v.hasPlayed = false
        if not artifactTypes["k" .. v.sp_id .. v.atk_pos] or artifactTypes["k" .. v.sp_id .. v.atk_pos] ~= "v" .. v.artifact .. v.atk_pos then
          artifactTypes["k" .. v.sp_id .. v.atk_pos] = "v" .. v.artifact .. v.atk_pos
          v.needShowAnim = true
          v.hasPlayed = false
        end
      end
      if v.atk_type == AtkType.RevengeAttack then
        RevengeSPData[#RevengeSPData + 1] = v
      end
      if v.atk_type == AtkType.ShootoffAttack then
        ShootoffSPData[#ShootoffSPData + 1] = v
      end
      if v.atk_type == AtkType.MultipleAttack then
        if MultipleAtkData[#MultipleAtkData + 1] == nil then
          MultipleAtkData[#MultipleAtkData + 1] = {}
        end
        table.insert(MultipleAtkData[#MultipleAtkData], v)
      end
      if #MultipleAtkData > 0 then
        GameObjectBattleReplay.isMultipleAtk = true
      end
      if GameObjectBattleReplay.isMultipleAtk and v.atk_type ~= AtkType.MultipleAttack and (v.adjutant == 0 or not self:IsAdjutantAttack(isPlayer1Action, v.adjutant)) and (v.artifact == nil or v.artifact == 0) and v.atk_type ~= AtkType.RevengeAttack and v.atk_type ~= AtkType.EnergyChangeBeforAttack then
        table.insert(MultipleAtkData[#MultipleAtkData], v)
      end
      if v.atk_type == AtkType.EnergyChangeBeforAttack then
        if v.atk_side and v.atk_side ~= "" then
          isPlayer1Action = v.atk_side == "p1"
        else
          isPlayer1Action = roundData.player1_action
        end
        local isLeft = isPlayer1Action and self.m_isPlayer1ShowInLeft or not isPlayer1Action and not self.m_isPlayer1ShowInLeft
        self:UpdateFleetAccInfo(not isLeft, v.def_pos, v.acc_change, v.acc)
        local persentAngry = math.min(100, v.acc)
        if persentAngry == 100 and not GameObjectBattleReplay:GetFleetData(isLeft, v.def_pos).isDead then
          local fleetDatas = self.m_fleetInfo.Left
          if not isLeft then
            fleetDatas = self.m_fleetInfo.Right
          end
          local fleetData = fleetDatas[v.def_pos]
          GameObjectBattleReplay:ShowShipFullEnergy(isLeft, v.def_pos, fleetData)
        else
          self:GetFlashObject():InvokeASCallback("_root", "hideShipFullEnergyEffect", isLeft, v.def_pos)
        end
        self:GetFlashObject():InvokeASCallback("_root", "showShipAngryBar", isLeft, v.def_pos, persentAngry)
      end
    end
    DebugOut("183 test sdfs ")
    DebugTable(MultipleAtkData)
    for k, v in pairs(roundData.buff) do
      if v.isUsed then
        v.isUsed = false
      end
    end
    if not self:HasAvailableArtifactData() then
      self:PlayRoundStepEffect(RoundStep.ATTACK)
      return
    end
    self:PlayArtifactSPEffect(true, BuffUpdateStep.BeforBattle)
  end
end
function GameObjectBattleReplay:HasAvailableArtifactData()
  if ArtifactSPData == nil then
    return false
  end
  for k, v in ipairs(ArtifactSPData) do
    if not v.hasPlayed then
      return true
    end
  end
  return false
end
function GameObjectBattleReplay:PlayArtifactSPEffect(isTakeTime, step)
  DebugOut("PlayArtifactSPEffect_2", isTakeTime, step, self.m_currentRoundIndex)
  DebugTable(ArtifactSPData)
  DebugTable(self.m_battleResult.rounds[self.m_currentRoundIndex])
  if not self:HasAvailableArtifactData() then
    return false
  end
  local roundData = self.m_battleResult.rounds[self.m_currentRoundIndex]
  if roundData then
    local isPlayer1Action = roundData.player1_action
    if ArtifactSPData[1].atk_side and ArtifactSPData[1].atk_side ~= "" then
      isPlayer1Action = ArtifactSPData[1].atk_side == "p1"
    end
    local isLeftShip = isPlayer1Action and self.m_isPlayer1ShowInLeft or not isPlayer1Action and not self.m_isPlayer1ShowInLeft
    local roundStepData = roundData.attacks
    local nextAttackData
    if roundStepData ~= nil then
      nextAttackData = roundStepData[self.m_currentRoundStepInfo.AttackRoundIndex + 1]
    end
    local nextAttackIndex = -1
    if nextAttackData ~= nil and self.m_currentRoundStep == RoundStep.ATTACK then
      nextAttackIndex = nextAttackData.index
    end
    local hasDataOfStep = false
    for k, v in ipairs(ArtifactSPData) do
      if v.artifact == step and not v.hasPlayed and not GameObjectBattleReplay.isPlayingMultiEffect and (step ~= BuffUpdateStep.AfterAttackOver or nextAttackIndex == -1 or nextAttackIndex > v.index) then
        if v.atk_side and v.atk_side ~= "" then
          isPlayer1Action = v.atk_side == "p1"
        else
          isPlayer1Action = roundData.player1_action
        end
        isLeftShip = isPlayer1Action and self.m_isPlayer1ShowInLeft or not isPlayer1Action and not self.m_isPlayer1ShowInLeft
        if not self:GetFleetData(isLeftShip, v.atk_pos).isPlayingHurtAnim and not self:GetFleetData(isLeftShip, v.atk_pos).isPlayArtifactAnim then
          local param = 0
          local isLeft = isLeftShip == v.self_cast
          local hotBuff
          for iBuff, vBuff in ipairs(roundData.buff) do
            if vBuff.buff_effect == v.buff_effect and vBuff.pos == v.def_pos and vBuff.index == v.index - 1 and not vBuff.isUsed then
              param = vBuff.param
              hotBuff = vBuff
              vBuff.isUsed = true
              break
            end
          end
          if step ~= BuffUpdateStep.NotShowArtifactEffect and v.needShowAnim == true then
            local effectName = GameDataAccessHelper:GetSpillEffect(v.sp_id)
            DebugOutBattlePlay("PlayArtifactSPEffect Detail", isLeftShip, v.atk_pos, AnimType.ATTACK, effectName, tonumber(effectName))
            if effectName and effectName ~= "gammaBlasterA" then
              effectName = tonumber(effectName)
            end
            self:GetFleetData(isLeftShip, v.atk_pos).isPlayArtifactAnim = true
            self:GetFleetData(isLeftShip, v.atk_pos).artifactStep = step
            self:GetFlashObject():InvokeASCallback("_root", "playArtifactSpellAnim", isLeftShip, v.atk_pos, effectName, true)
            if isTakeTime and v.sp_id ~= 0 and effectName ~= nil and effectName ~= "" then
              GameObjectBattleReplay:AddTotalReactionCount()
              DebugOut("TotalReactionCount++ in  PlayArtifactSPEffect", self.m_currentRoundStepInfo.TotalReactionCount, isLeftShip, v.atk_pos)
            end
            v.hasPlayed = true
          end
          hasDataOfStep = true
          self:UpdateFleetAccInfo(isLeft, v.def_pos, v.acc_change, v.acc)
          if v.buff_effect ~= 45 then
            GameObjectBattleReplay:UpdateFleetHp(isLeft, v.def_pos, v.durability)
            self:AddBuff(isLeft, v.def_pos, v.buff_effect, v.round_cnt, true, v.param)
          elseif param and param >= 0 and v.buff_effect == 45 then
            self:PlayHotEffect(v.def_pos, isLeft, hotBuff)
          end
          GameObjectBattleReplay.isPlayingMultiEffect = false
          for _, v_i in ipairs(ArtifactSPData) do
            if not v_i.hasPlayed and v_i.artifact == v.artifact and v_i.atk_pos == v.atk_pos and v_i.atk_side == v.atk_side and v_i.sp_id == v.sp_id then
              local hotBuff_i
              local isLeft_i = isLeftShip == v_i.self_cast
              for _, vBuff_i in ipairs(roundData.buff) do
                if vBuff_i.buff_effect == v_i.buff_effect and vBuff_i.pos == v_i.def_pos and vBuff_i.index == v_i.index - 1 then
                  hotBuff_i = vBuff_i
                  break
                end
              end
              self:UpdateFleetAccInfo(isLeft_i, v_i.def_pos, v_i.acc_change, v_i.acc)
              v_i.hasPlayed = true
              if v_i.buff_effect ~= 45 then
                self:AddBuff(isLeft_i, v_i.def_pos, v_i.buff_effect, v_i.round_cnt, true, v_i.param)
              elseif param and param >= 0 and v_i.buff_effect == 45 then
                self:PlayHotEffect(v_i.def_pos, isLeft_i, hotBuff_i)
              end
              if step ~= BuffUpdateStep.NotShowArtifactEffect and v.needShowAnim == true then
                GameObjectBattleReplay.isPlayingMultiEffect = true
              end
            end
          end
          if GameObjectBattleReplay.isPlayingMultiEffect then
            GameObjectBattleReplay.PlayingMultiEffectPos = v.atk_pos
            GameObjectBattleReplay.PlayingMultiEffectSide = isLeftShip
            break
          end
        end
      end
    end
    if step == BuffUpdateStep.BeforBattle and hasDataOfStep == false and self.m_currentRoundStepInfo.TotalReactionCount == 0 then
      self:PlayRoundStepEffect(RoundStep.ATTACK)
    elseif step == BuffUpdateStep.BeforBattle and hasDataOfStep == true or step == BuffUpdateStep.AfterAttackOver then
      self:CheckAllShipPlayBuff()
    end
    return hasDataOfStep
  end
end
function GameObjectBattleReplay:PlayShootoffSPEffect()
  DebugTable(ShootoffSPData)
  DebugTable(ShootoffAttacker)
  local roundData = self.m_battleResult.rounds[self.m_currentRoundIndex]
  if (ShootoffSPData == nil or #ShootoffSPData == 0) and (ShootoffAttacker == nil or #ShootoffAttacker == 0) then
    return
  end
  local roundData = self.m_battleResult.rounds[self.m_currentRoundIndex]
  if roundData then
    if ShootoffSPData and #ShootoffSPData > 0 then
      DebugTable(ShootoffDefender)
      for k, v in pairs(ShootoffSPData) do
        if v.atk_side and v.atk_side ~= "" then
          isPlayer1Action = v.atk_side == "p1"
        else
          isPlayer1Action = roundData.player1_action
        end
        local isLeftShip = isPlayer1Action and self.m_isPlayer1ShowInLeft or not isPlayer1Action and not self.m_isPlayer1ShowInLeft
        local atker = {
          pos = v.atk_pos,
          isLeft = isLeftShip,
          defPos = v.def_pos
        }
        ShootoffAttacker[#ShootoffAttacker + 1] = atker
      end
      ShootoffSPData = {}
    end
    DebugTable(ShootoffAttacker)
    if #roundData.attacks > self.m_currentRoundStepInfo.AttackRoundIndex then
      return
    end
    for k, v in pairs(ShootoffAttacker) do
      local fleetData = GameObjectBattleReplay:GetFleetData(not v.isLeft, v.defPos)
      DebugTable(fleetData)
      GameObjectBattleReplay:AddTotalReactionCount()
      self:playBuffEffect(not v.isLeft, v.defPos, "buff_shootoff")
      DebugOutBattlePlay("TotalReactionCount++ in PlayShootoffSPEffect", self.m_currentRoundStepInfo.TotalReactionCount)
    end
    ShootoffAttacker = {}
  end
end
function GameObjectBattleReplay:PlayRevengeSPEffect()
  DebugOut("PlayRevengeSPEffect ttQ ", self.m_currentRoundIndex, GameObjectBattleReplay.isMultipleAtk, isLeft, gridPos)
  DebugOut(debug.traceback())
  DebugTable(RevengeSPData)
  DebugTable(RevengeAttacker)
  if (RevengeSPData == nil or #RevengeSPData == 0) and (RevengeAttacker == nil or #RevengeAttacker == 0) then
    return
  end
  local roundData = self.m_battleResult.rounds[self.m_currentRoundIndex]
  if roundData then
    if RevengeSPData and #RevengeSPData > 0 then
      local isPlayer1Action = roundData.player1_action
      if RevengeSPData[1].atk_side and RevengeSPData[1].atk_side ~= "" then
        isPlayer1Action = RevengeSPData[1].atk_side == "p1"
      end
      local isLeftShip = isPlayer1Action and self.m_isPlayer1ShowInLeft or not isPlayer1Action and not self.m_isPlayer1ShowInLeft
      RevengeDefender = {
        pos = RevengeSPData[1].def_pos,
        isLeft = not isLeftShip,
        isPlayedHurAnim = false
      }
      DebugOut("Jjjjjjjjj")
      DebugTable(RevengeDefender)
      for k, v in pairs(RevengeSPData) do
        if v.atk_side and v.atk_side ~= "" then
          isPlayer1Action = v.atk_side == "p1"
        else
          isPlayer1Action = roundData.player1_action
        end
        isLeftShip = isPlayer1Action and self.m_isPlayer1ShowInLeft or not isPlayer1Action and not self.m_isPlayer1ShowInLeft
        local fleet = self:GetFleetData(isLeftShip, v.atk_pos)
        if not GameObjectBattleReplay.isMultipleAtk or GameObjectBattleReplay.isMultipleAtk and 0 < fleet.currentHP then
          self:UpdateFleetHPInfo(not isLeftShip, v.def_pos, -v.dur_damage, -v.shield_damage, v.shield, 0 < v.shield, v.crit, v.intercept, v.is_immune, v.durability, v.index)
          local atker = {
            pos = v.atk_pos,
            isLeft = isLeftShip
          }
          RevengeAttacker[#RevengeAttacker + 1] = atker
          table.remove(RevengeSPData, k)
        end
      end
    end
    DebugOut("kkkkkkkkkk")
    DebugTable(RevengeAttacker)
    if #roundData.attacks > self.m_currentRoundStepInfo.AttackRoundIndex then
      return
    end
    for k, v in pairs(RevengeAttacker) do
      local fleetData = GameObjectBattleReplay:GetFleetData(v.isLeft, v.pos)
      DebugOut("fsfsfasf ", v.isLeft, v.pos)
      DebugTable(fleetData)
      if fleetData.isHurtAnimOver then
        for k1, v1 in ipairs(fleetData.buffs) do
          if GameDataAccessHelper:IsRevengeBuff(v1.EffectID) and 1 < v1.RoundCount then
            fleetData.isRevengeAtkOver = true
            GameObjectBattleReplay:AddTotalReactionCount()
            self:GetFlashObject():InvokeASCallback("_root", "PlayBuffDamageReturnAttack", v.isLeft, v.pos)
            DebugOut("TotalReactionCount++ in PlayRevengeSPEffect", self.m_currentRoundStepInfo.TotalReactionCount)
          end
        end
        table.remove(RevengeAttacker, k)
      end
    end
  end
end
function GameObjectBattleReplay:IsShowDamageReturnedHurtAnim(isLeftShip, gridPos)
  local roundData = self.m_battleResult.rounds[self.m_currentRoundIndex]
  local roundStepData = roundData.attacks
  if roundStepData and #roundStepData > 0 then
    for k, v in ipairs(roundStepData) do
      if v ~= nil and v.counter then
        local isLeft = v.def_side == SIDE_LEFT and self.m_isPlayer1ShowInLeft or v.def_side == SIDE_RIGHT and not self.m_isPlayer1ShowInLeft
        if isLeftShip == isLeft and v.def_pos == gridPos then
          return false
        end
      end
    end
  end
  return true
end
function GameObjectBattleReplay:OnDamageReturnAttackOver(isLeft, gridPos)
  DebugOutBattlePlay("    OnReactionAnimOver in  OnDamageReturnAttackOver", isLeft, gridPos)
  DebugOutPutTable(RevengeDefender, "check_RevengeDefender_1")
  if RevengeDefender and RevengeDefender ~= {} and RevengeDefender.isPlayedHurAnim == false then
    if self:IsShowDamageReturnedHurtAnim(RevengeDefender.isLeft, RevengeDefender.pos) then
      GameObjectBattleReplay:AddTotalReactionCount()
      DebugOut("TotalReactionCount++ in RevengeDefPos", self.m_currentRoundStepInfo.TotalReactionCount)
      self:PlayShipBGAnim(RevengeDefender.isLeft, RevengeDefender.pos, "_hurt")
    end
    RevengeDefender.isPlayedHurAnim = true
  end
  local fleetData = self:GetFleetData(isLeft, gridPos)
  if fleetData then
    fleetData.isRevengeAtkOver = false
    if fleetData.currentHP <= 0 and not fleetData.isDead then
      self:OnShipDead(isLeft, gridPos)
    elseif fleetData.currentHP <= 0 then
      self:GetFlashObject():InvokeASCallback("_root", "HideShipBuffEffect", isLeft, gridPos, "buff_DamageReturn")
    end
  end
  self:OnReactionAnimOver(gridPos, isLeft)
end
function GameObjectBattleReplay:IsShowShootoffHurtAnim(isLeftShip, gridPos)
  local roundData = self.m_battleResult.rounds[self.m_currentRoundIndex]
  local roundStepData = roundData.attacks
  if roundStepData and #roundStepData > 0 then
    for k, v in ipairs(roundStepData) do
      if v ~= nil and v.counter then
        local isLeft = v.def_side == SIDE_LEFT and self.m_isPlayer1ShowInLeft or v.def_side == SIDE_RIGHT and not self.m_isPlayer1ShowInLeft
        if isLeftShip == isLeft and v.def_pos == gridPos then
          return false
        end
      end
    end
  end
  return true
end
function GameObjectBattleReplay:AddTotalReactionCount()
  self.m_currentRoundStepInfo.TotalReactionCount = self.m_currentRoundStepInfo.TotalReactionCount + 1
  GameObjectBattleReplay.StepEffectProtectTimer = GameObjectBattleReplay.STEP_EFFECT_PROTECT_TIME
end
function GameObjectBattleReplay:OnShootoffAttackOver(isLeft, gridPos)
  DebugOutBattlePlay("    OnReactionAnimOver in  OnShootoffAttackOver", isLeft, gridPos)
  local roundData = self.m_battleResult.rounds[self.m_currentRoundIndex]
  if roundData then
    for k, v in ipairs(roundData.sp_attacks) do
      if v.atk_type == AtkType.ShootoffAttack and v.def_pos == gridPos then
        self:UpdateFleetHPInfo(isLeft, v.def_pos, -v.dur_damage, -v.shield_damage, v.shield, v.shield > 0, v.crit, v.intercept, v.is_immune, v.durability, v.index)
        GameObjectBattleReplay:AddTotalReactionCount()
        DebugOut("TotalReactionCount++ in OnShootoffAttackOver", self.m_currentRoundStepInfo.TotalReactionCount)
        self:PlayShipBGAnim(isLeft, gridPos, "_hurt")
      end
    end
  end
  local haveRevenge = #RevengeSPData > 0
  if haveRevenge then
    self:PlayRevengeSPEffect()
  end
  self:OnReactionAnimOver(gridPos, isLeft)
end
function GameObjectBattleReplay:GetTransformShipLevel(isPlayer1Action, shipID)
  if isPlayer1Action then
    for i, v in ipairs(self.m_battleResult.player2_fleets) do
      if v.identity == shipID then
        return v.level
      end
    end
  else
    for i, v in ipairs(self.m_battleResult.player1_fleets) do
      if v.identity == shipID then
        return v.level
      end
    end
  end
  return 0
end
function GameObjectBattleReplay:PlaySPAttack()
  local roundData = self.m_battleResult.rounds[self.m_currentRoundIndex]
  DebugOut("PlaySPAttack__t index=" .. tostring(self.m_currentRoundIndex))
  DebugTable(roundData)
  local isPlayer1Action = roundData.player1_action
  local spAttackData = {}
  local isLeftShip = isPlayer1Action and self.m_isPlayer1ShowInLeft or not isPlayer1Action and not self.m_isPlayer1ShowInLeft
  local firstAtkPos = 0
  for k, v in ipairs(roundData.sp_attacks) do
    if (v.adjutant == 0 or not self:IsAdjutantAttack(isPlayer1Action, v.adjutant)) and (v.artifact == nil or v.artifact == 0) then
      spAttackData[#spAttackData + 1] = v
      if firstAtkPos == 0 and v.atk_type == AtkType.Normal then
        firstAtkPos = v.atk_pos
      end
    end
  end
  if #spAttackData > 0 then
    local fleet = self:GetFleet(isLeftShip, firstAtkPos)
    if fleet then
      local fleetAbility = GameDataAccessHelper:GetCommanderAbility(fleet.identity, fleet.level)
      DebugOut("GameObjectBattleReplay:PlaySPAttack gG", fleet.identity, fleet.level, GameObjectTutorialCutscene:IsActive())
      if spell_pause == 0 and GameStateBattlePlay.isPlayEffect then
        DebugTable(fleetAbility)
        if fleetAbility and fleetAbility.head ~= -1 and fleetAbility.ship ~= -1 and fleetAbility.banner ~= -1 then
          local checkEffectResource = GameHelper:CheckHalfPortraitResourceLoaded(fleetAbility.banner + 1)
          local checkHeroResource = GameHelper:CheckHalfPortraitAvataAndShip(fleetAbility)
          DebugOut(" resource loaded :", checkHeroResource, checkEffectResource)
          if checkEffectResource and checkHeroResource then
            local player_avatar = isPlayer1Action and self.m_battleResult.player1_avatar or self.m_battleResult.player2_avatar
            local sex
            if player_avatar == "male" then
              sex = 1
            elseif player_avatar == "female" then
              sex = 2
            end
            if fleetAbility and GameStateBattlePlay:PlayEffect(isLeftShip, fleetAbility.head, fleetAbility.ship, fleetAbility.banner, fleetAbility.AVATAR, fleetAbility.SHIP) then
              spell_pause = 1
              return
            end
          end
        end
      elseif fleetAbility and fleetAbility.voice and fleetAbility.voice ~= "undefined" and fleetAbility.voice ~= "" then
        local oldsound = fleetAbility.voice .. ".mp3"
        local args = oldsound
        if GameSettingData and GameSettingData.Save_Lang then
          lang = GameSettingData.Save_Lang
          if string.find(lang, "ru") == 1 then
            args = string.gsub(args, "%.", "_ru.")
            DebugOut("advcd:", args, ext.crc32.crc32("sound/" .. args))
            if ext.crc32.crc32("sound/" .. args) and ext.crc32.crc32("sound/" .. args) ~= "" then
              oldsound = args
            end
          end
        end
        GameUtils:PlaySound(oldsound)
      end
    end
  end
  self:CheckAntiInvisibleAnim()
end
function GameObjectBattleReplay:GetFleet(isPlayer1Action, ShipPos)
  DebugOut("GameObjectBattleReplay:GetFleet", isPlayer1Action, ShipPos)
  local playerFleets = isPlayer1Action and self.m_battleResult.player1_fleets or self.m_battleResult.player2_fleets
  DebugTable(playerFleets)
  for k, v in pairs(playerFleets) do
    if v.pos == ShipPos then
      return v
    end
  end
end
function GameObjectBattleReplay:OnEffectEnd()
  spell_pause = 0
  self:CheckAntiInvisibleAnim()
end
function GameObjectBattleReplay:playBuffEffect(isleft, grididx, buffname)
  local resok, res = GameDataAccessHelper:CheckBuffCanPlay(buffname)
  if not _buffEffectRes[buffname] then
    self:GetFlashObject():InvokeASCallback("_root", "PlayShipBuffEffect", isleft, grididx, buffname)
    print("playBuffEffect name by old method:" .. buffname)
    return
  end
  local resname = ""
  if isleft then
    resname = _buffEffectRes[buffname].left
  else
    resname = _buffEffectRes[buffname].right
  end
  self:GetFlashObject():InvokeASCallback("_root", "PlayShipBuffEffect", isleft, grididx, buffname, resname)
end
function GameObjectBattleReplay:SetCurrentRoundShipHurtAnimNotEnd(isLeft, gridIndex)
  local fleetDatas = self.m_fleetInfo.Left
  if not isLeft then
    fleetDatas = self.m_fleetInfo.Right
  end
  local fleetData = fleetDatas[gridIndex]
  fleetData.isHurtAnimOver = false
end
function GameObjectBattleReplay:CheckAntiInvisibleAnim()
  self.m_currentRoundStepInfo.AntiInvisibleShipCurCount = 0
  local roundData = self.m_battleResult.rounds[self.m_currentRoundIndex]
  local isPlayer1Action = roundData.player1_action
  local spAntiInvisibleData = {}
  local isLeftShip = isPlayer1Action and self.m_isPlayer1ShowInLeft or not isPlayer1Action and not self.m_isPlayer1ShowInLeft
  if roundData and 0 < #roundData.buff then
    for k, v in pairs(roundData.buff) do
      if v.buff_effect == 64 and v.round_cnt == 0 then
        spAntiInvisibleData[#spAntiInvisibleData + 1] = v
      end
    end
    self.m_currentRoundStepInfo.AntiInvisibleShipTotalCount = #spAntiInvisibleData
    if #spAntiInvisibleData > 0 then
      for i = 1, #spAntiInvisibleData do
        self:playBuffEffect(not isLeftShip, spAntiInvisibleData[i].pos, "buff_scout")
      end
    else
      self:PlaySPAttackEffect()
    end
  else
    self:PlaySPAttackEffect()
  end
end
function GameObjectBattleReplay:AntiInvisibleAnimCallback(isLeft, gridIndex)
  DebugOut("AntiInvisibleAnimCallback", isLeft, gridIndex)
  self.m_currentRoundStepInfo.AntiInvisibleShipCurCount = self.m_currentRoundStepInfo.AntiInvisibleShipCurCount + 1
  if self.m_currentRoundStepInfo.AntiInvisibleShipTotalCount <= self.m_currentRoundStepInfo.AntiInvisibleShipCurCount then
    self:PlaySPAttackEffect()
  end
end
function GameObjectBattleReplay:PlaySPAttackEffect()
  DebugOutBattlePlay("PlaySPAttackEffect ", self.m_JumpToNextRoundTimer)
  DebugOut(self.m_currentRoundIndex)
  local roundData = self.m_battleResult.rounds[self.m_currentRoundIndex]
  local isPlayer1Action = roundData.player1_action
  local spAttackData = {}
  local checkAccBeforAtkData = {}
  self.isBegainBuffNameEffect = false
  self.m_currentRoundStepInfo.BuffNameEffectTotalCount = 0
  self.m_currentRoundStepInfo.BuffAttrs = {}
  self.m_currentRoundStepInfo.BuffTable = {}
  self.m_currentRoundStepInfo.NameAnimFleetInfo = {}
  self.m_currentRoundStepInfo.sp_atk_type = 0
  self.m_currentRoundStepInfo.buffEffectName = ""
  self:FindAllBuffEffectType()
  if GameObjectBattleReplay.isMultipleAtk == true then
    ext.SetAppSpeedScale(GameObjectBattleReplay.SUPER_BATTLE_SPEED)
  else
    ext.SetAppSpeedScale(GameUtils:GetBattleSpeed())
  end
  local firstDefPos = 0
  local firstAtkPos = 0
  local firstSpAttackDataIdx = 1
  DebugOut("xx test qj", GameObjectBattleReplay.isMultipleAtk, GameObjectBattleReplay.curMultipleAtkRound)
  local isLeftShip = isPlayer1Action and self.m_isPlayer1ShowInLeft or not isPlayer1Action and not self.m_isPlayer1ShowInLeft
  if GameObjectBattleReplay.isMultipleAtk and #MultipleAtkData > 0 then
    spAttackData = MultipleAtkData[GameObjectBattleReplay.curMultipleAtkRound]
    firstDefPos = spAttackData[1].def_pos
    firstAtkPos = spAttackData[1].atk_pos
    if 0 < #roundData.damage then
      for _, damgeData in ipairs(roundData.damage) do
        if damgeData.pos == firstDefPos then
          if isPlayer1Action ~= (damgeData.side == "p1") then
            damgeData.shield = spAttackData[1].shield
          end
        end
      end
    end
    GameObjectBattleReplay.curMultipleAtkRound = GameObjectBattleReplay.curMultipleAtkRound + 1
    if GameObjectBattleReplay.curMultipleAtkRound > #MultipleAtkData then
      GameObjectBattleReplay.isMultipleAtk = false
    end
  else
    for k, v in ipairs(roundData.sp_attacks) do
      if v.atk_side and v.atk_side ~= "" then
        isPlayer1Action = v.atk_side == "p1"
      else
        isPlayer1Action = roundData.player1_action
      end
      local isLeft = isPlayer1Action and self.m_isPlayer1ShowInLeft or not isPlayer1Action and not self.m_isPlayer1ShowInLeft
      if (v.adjutant == 0 or not self:IsAdjutantAttack(isPlayer1Action, v.adjutant)) and (v.artifact == nil or v.artifact == 0) and v.atk_type ~= AtkType.RevengeAttack and v.atk_type ~= AtkType.EnergyChangeBeforAttack then
        spAttackData[#spAttackData + 1] = v
        if firstDefPos == 0 and firstAtkPos == 0 and firstSpAttackDataIdx == 1 and v.atk_type == AtkType.Normal then
          firstDefPos = v.def_pos
          firstAtkPos = v.atk_pos
          firstSpAttackDataIdx = #spAttackData
        end
        if v.def_pos == v.atk_pos and v.atk_type == AtkType.EnergyChangeBeforSPATTACK then
          self:UpdateFleetAccInfo(isLeft, v.def_pos, v.acc_change, v.acc)
        end
      end
    end
  end
  DebugOutBattlePlay("zm test adjutant dsft ", firstDefPos)
  DebugOutBattlePlayTable(spAttackData)
  if spAttackData == nil or #spAttackData == 0 then
    if not self:PlayArtifactSPEffect(true, BuffUpdateStep.AfterAttackOver) then
      self:PlayRoundStepEffect(RoundStep.RELIVE)
    end
  else
    self.m_currentRoundStepInfo.TotalReactionCount = 1
    self.m_currentRoundStepInfo.RectionCount = 0
    DebugOutBattlePlay("TotalReactionCount = 2 in GameObjectBattleReplay:PlaySPAttackEffect: " .. self.m_currentRoundStepInfo.TotalReactionCount)
    local effectName = GameDataAccessHelper:GetSpillEffect(spAttackData[1].sp_id)
    local effectTitleName = GameDataAccessHelper:GetSkillNameText(spAttackData[1].sp_id)
    local isNeedHideEnergy = spAttackData[1].atk_acc == 0
    local transfromSpellID = spAttackData[1].sp_id
    if spAttackData[1].transfiguration ~= 0 then
      if transfromSpellID ~= 0 then
        DebugOutBattlePlay("transfromSpellID:", transfromSpellID)
        effectName = GameDataAccessHelper:GetSpillEffect(transfromSpellID)
        effectTitleName = GameDataAccessHelper:GetSkillNameText(transfromSpellID)
      else
        DebugOutBattlePlay("transfromSpellID is 0")
      end
    end
    self:GetFlashObject():InvokeASCallback("_root", "playShipSPEffectTitle", isLeftShip, firstAtkPos, effectTitleName, isNeedHideEnergy)
    DebugOut("qingjei fsdf ", firstSpAttackDataIdx)
    if spAttackData[firstSpAttackDataIdx] and spAttackData[firstSpAttackDataIdx].atk_acc_change then
      self:UpdateFleetAccInfo(isLeftShip, firstAtkPos, spAttackData[firstSpAttackDataIdx].atk_acc_change, spAttackData[firstSpAttackDataIdx].atk_acc)
    elseif transfromSpellID ~= 100 then
      self:UpdateFleetAccInfo(isLeftShip, firstAtkPos, 0, 0)
    end
    DebugOutBattlePlay("zm test" .. effectName)
    DebugOutBattlePlayTable(spAttackData[1])
    DebugOutBattlePlay(isLeftShip, firstAtkPos, AnimType.ATTACK, effectName)
    self:PlayShipAnim(isLeftShip, firstAtkPos, AnimType.ATTACK, effectName, true)
    local hurtCenterPos = self:GetSpellCenterPos(transfromSpellID, firstDefPos)
    local isFullScreenEffect = string.sub(effectName, 1, 5) == "FULL_"
    if isFullScreenEffect then
      self:GetFlashObject():InvokeASCallback("_root", "playFullScreenEffect", not isLeftShip, effectName .. AnimType.HURT)
    end
    for i, v in ipairs(spAttackData) do
      local isSelfCast = v.self_cast
      if v.atk_side and v.atk_side ~= "" then
        isPlayer1Action = v.atk_side == "p1"
      else
        isPlayer1Action = roundData.player1_action
      end
      isLeftShip = isPlayer1Action and self.m_isPlayer1ShowInLeft or not isPlayer1Action and not self.m_isPlayer1ShowInLeft
      DebugOut("qingjie isLeftShip:", isLeftShip, isPlayer1Action, self.m_isPlayer1ShowInLeft)
      if v.adjutant ~= 0 and self:IsAdjutantAttack(isPlayer1Action, v.adjutant) then
      elseif isSelfCast then
        DebugOutBattlePlay("zm AddBuff", v.buff_effect, v.round_cnt)
        if GameDataAccessHelper:IsFullScreenBuff(v.buff_effect) then
          DebugOutBattlePlay("zm AddFullScreenBuff", isLeftShip, v.buff_effect, v.round_cnt)
          self:AddFullScreenBuff(isLeftShip, v.buff_effect, v.round_cnt)
          self:CheckPlayFullScreenBuff(isLeftShip)
        elseif v.buff_effect == 0 then
          DebugOutBattlePlay("spAttackData UpdateFleetAccInfo")
          if v.def_pos == v.atk_pos and v.atk_type ~= AtkType.EnergyChangeBeforSPATTACK and v.atk_type ~= AtkType.EnergyChangeBeforAttack then
            self:UpdateFleetAccInfo(isLeftShip, v.def_pos, v.acc_change, v.acc)
          elseif v.atk_type ~= AtkType.EnergyChangeBeforSPATTACK and v.atk_type ~= AtkType.EnergyChangeBeforAttack then
            self:UpdateFleetAccInfo(isLeftShip, v.def_pos, v.acc_change, v.acc)
            self:GetFlashObject():InvokeASCallback("_root", "showShipAngryBar", isLeftShip, v.def_pos, math.min(100, v.acc))
            if v.acc >= 100 and not GameObjectBattleReplay:GetFleetData(isLeftShip, v.def_pos).isDead then
              local fleetDatas = isLeftShip
              if not isLeft then
                fleetDatas = self.m_fleetInfo.Right
              end
              local fleetData = fleetDatas[v.def_pos]
              GameObjectBattleReplay:ShowShipFullEnergy(isLeftShip, v.def_pos, fleetData)
            else
              self:GetFlashObject():InvokeASCallback("_root", "hideShipFullEnergyEffect", not isLeftShip, v.def_pos)
            end
          end
        elseif not self:CheckInsertNewFleet(isLeftShip, v.def_pos, v.buff_effect) then
          local param, hotBuff
          for iBuff, vBuff in ipairs(roundData.buff) do
            if vBuff.buff_effect == v.buff_effect and vBuff.pos == v.def_pos and vBuff.index == v.index - 1 then
              param = vBuff.param
              hotBuff = vBuff
              break
            end
          end
          if param and param >= 0 and (v.buff_effect == 45 or v.buff_effect == 48 and param > 0) then
            self:PlayHotEffect(v.def_pos, isLeftShip, hotBuff)
          else
            self:AddBuff(isLeftShip, v.def_pos, v.buff_effect, v.round_cnt, true, param)
          end
          if v.atk_type == AtkType.AbilityDrained then
            self:AddBuff(isLeftShip, v.def_pos, v.buff_effect, v.round_cnt, true, param)
            self:AddBuffNameEffect(isLeftShip, v.def_pos, v.buff_effect, false, v.absorb_attr_type)
          end
        end
      else
        local effectDir = self:GetSPHurtEffectDir(v.def_pos, hurtCenterPos, isLeftShip)
        if isFullScreenEffect then
          effectDir = ""
        end
        local isRepeatData = false
        if v.atk_type == AtkType.Normal or v.atk_type == AtkType.MultipleAttack then
          isRepeatData = self:UpdateFleetHPInfo(not isLeftShip, v.def_pos, -v.dur_damage, -v.shield_damage, v.shield, 0 < v.shield, v.crit, v.intercept, v.is_immune, v.durability, v.index)
        end
        if v.atk_type == AtkType.Normal and GameDataAccessHelper:IsChainSpell(v.sp_id) then
          self:RecordShipChainSPAnimData(isLeftShip, v.def_pos, AnimType.HURT, effectDir, effectName, v.def_pos == hurtCenterPos, i)
          DebugOut("GameDataAccessHelper:IsChainSpell ")
          effectDir = "_center"
        end
        self:SetCurrentRoundShipHurtAnimNotEnd(not isLeftShip, v.def_pos)
        self:UpdateFleetAccInfo(not isLeftShip, v.def_pos, v.acc_change, v.acc)
        self:AddBuff(not isLeftShip, v.def_pos, v.buff_effect, v.round_cnt, true, v.param)
        if v.atk_type == AtkType.AbilityDrained then
          self:AddBuffNameEffect(not isLeftShip, v.def_pos, v.buff_effect, true, v.absorb_attr_type)
        end
        if v.atk_type == AtkType.Normal or v.atk_type == AtkType.MultipleAttack then
          if not v.hit or v.is_immune then
            self:RecordShipSPAnimData(isLeftShip, v.def_pos, AnimType.MISS, effectDir, effectName, v.def_pos == hurtCenterPos)
          elseif v.intercept then
            self:RecordShipSPAnimData(isLeftShip, v.def_pos, AnimType.BLOCK, effectDir, effectName, v.def_pos == hurtCenterPos)
          elseif 0 < v.shield then
            self:RecordShipSPAnimData(isLeftShip, v.def_pos, AnimType.SHIELD, effectDir, effectName, v.def_pos == hurtCenterPos)
          else
            self:GetFleetData(not isLeftShip, v.def_pos).isNeedHurtAnim = true
            self:RecordShipSPAnimData(isLeftShip, v.def_pos, AnimType.HURT, effectDir, effectName, v.def_pos == hurtCenterPos)
          end
        end
        if not isRepeatData and (v.atk_type == AtkType.Normal or v.atk_type == AtkType.MultipleAttack) then
          GameObjectBattleReplay:AddTotalReactionCount()
        end
        DebugOutBattlePlay("TotalReactionCount++ in GameObjectBattleReplay:PlaySPAttackEffect: " .. self.m_currentRoundStepInfo.TotalReactionCount, not isLeftShip, v.def_pos)
      end
    end
    self:StartShipSPAnim()
  end
end
function GameObjectBattleReplay:StartShipSPAnim()
  self:PlayShipChainSPAnim()
end
function GameObjectBattleReplay:PlayShipChainSPAnim()
  DebugOut("GameObjectBattleReplay:PlayShipChainSPAnim")
  DebugTable(self.ChainSPAnimData)
  if self.ChainSPAnimData and #self.ChainSPAnimData > 0 then
    DebugOut("GameObjectBattleReplay:PlayShipChainSPAnim222")
    for key, value in ipairs(self.ChainSPAnimData) do
      DebugOut("GameObjectBattleReplay:PlayShipChainSPAnim = ", value.animType .. value.effectDir)
      self:PlayShipAnim(not value.isLeftShip, value.def_pos, value.animType .. value.effectDir, value.effectName, value.ishurtCenterPos)
    end
    self.ChainSPAnimData = {}
  else
    self.mChainAnimNumber = 0
    self:PlayRecordedShipSPAnim()
  end
end
function GameObjectBattleReplay:PlayRecordedShipSPAnim()
  self:PlayShipSPAnim()
  self:UpdateStepBuffs(BuffUpdateStep.AfterSPAttack)
end
function GameObjectBattleReplay:CheckPlayRelive()
  DebugOut("CheckPlayRelive")
  DebugTable(GameObjectBattleReplay.needReliveFleet)
  self.m_currentRoundStepInfo.TotalReactionCount = 0
  self.m_currentRoundStepInfo.RectionCount = 0
  if #GameObjectBattleReplay.needReliveFleet < 1 then
    self.m_JumpToNextRoundTimer = ROUND_INTERVAL_TIMER
  else
    local roundData = self.m_battleResult.rounds[self.m_currentRoundIndex]
    if 1 > #roundData.relive_fleet then
      self.m_JumpToNextRoundTimer = ROUND_INTERVAL_TIMER
    else
      for k, v in ipairs(GameObjectBattleReplay.needReliveFleet) do
        for k1, v2 in ipairs(roundData.relive_fleet) do
          local isLeft = v2.ptype == SIDE_LEFT and self.m_isPlayer1ShowInLeft or v2.ptype == SIDE_RIGHT and not self.m_isPlayer1ShowInLeft
          if v.isLeft == isLeft and v.gridPos == v2.pos then
            GameObjectBattleReplay:AddTotalReactionCount()
            local player1_action = v2.ptype == SIDE_LEFT
            self:InsertNewFleet(v2, player1_action, 10067)
          end
        end
      end
    end
  end
end
function GameObjectBattleReplay:CheckInsertNewFleet(isLeftShip, def_pos, buff_effect)
  local roundData = self.m_battleResult.rounds[self.m_currentRoundIndex]
  local isPlayer1Action = roundData.player1_action
  local fleetDatas = self.m_fleetInfo.Left
  if not isLeftShip then
    fleetDatas = self.m_fleetInfo.Right
  end
  local fleetData = fleetDatas[def_pos]
  if fleetData and fleetData.currentHP > 0 then
    DebugOut("GameObjectBattleReplay:InsertNewFleets:insert pos has a ship already")
    DebugOut(def_pos, isLeftShip)
    DebugTable(fleetData)
    return false
  end
  if roundData.summon_fleet and 0 < #roundData.summon_fleet then
    for k, v in pairs(roundData.summon_fleet) do
      if v.atk_side and v.atk_side ~= "" then
        isPlayer1Action = v.atk_side == "p1"
      else
        isPlayer1Action = roundData.player1_action
      end
      if v.cur_durability and 0 < v.cur_durability then
        self:InsertNewFleet(v, isPlayer1Action, buff_effect)
      end
    end
  else
    return false
  end
  return true
end
function GameObjectBattleReplay:CheckInsertReLiveFleet(isLeftShip, def_pos, buff_effect)
  if buff_effect ~= 25 then
    return false
  end
  local roundData = self.m_battleResult.rounds[self.m_currentRoundIndex]
  local isPlayer1Action = roundData.player1_action
  local fleetDatas = self.m_fleetInfo.Left
  if not isLeftShip then
    fleetDatas = self.m_fleetInfo.Right
  end
  local fleetData = fleetDatas[def_pos]
  if fleetData and fleetData.currentHP > 0 then
    DebugOut("GameObjectBattleReplay:InsertNewFleets:insert pos has a ship already")
    DebugOut(def_pos, isLeftShip)
    DebugTable(fleetData)
    return false
  end
  local hasReliveDataForPos = false
  if roundData.relive_fleet and 0 < #roundData.relive_fleet then
    for k, v in pairs(roundData.relive_fleet) do
      local isLeft = v.ptype == SIDE_LEFT and self.m_isPlayer1ShowInLeft or v.ptype == SIDE_RIGHT and not self.m_isPlayer1ShowInLeft
      if isLeft == isLeftShip and v.pos == def_pos then
        if v.atk_side and v.atk_side ~= "" then
          isPlayer1Action = v.atk_side == "p1"
        else
          isPlayer1Action = roundData.player1_action
        end
        if v.cur_durability and 0 < v.cur_durability then
          self:InsertNewFleet(v, isPlayer1Action, buff_effect)
        end
        hasReliveDataForPos = true
      end
    end
  else
    return false
  end
  if not hasReliveDataForPos then
    return false
  end
  return true
end
function GameObjectBattleReplay:SetTransformTarget(isLeftShip, shipID, shipPos, isPlayer1Action)
  local transfromShipName
  if shipID > 10000 then
    transfromShipName = GameDataAccessHelper:GetMonsterShip(shipID, false)
    DebugOutBattlePlay("SetTransformTarget:shipName:", transfromShipName)
    if isLeftShip then
      self.transfromShipName_Left = transfromShipName
      self.transfromShipGridIndex_Left = shipPos
    else
      self.transfromShipName_Right = transfromShipName
      self.transfromShipGridIndex_Right = shipPos
    end
  else
    local transfromshipLevel = 0
    transfromshipLevel = GameObjectBattleReplay:GetTransformShipLevel(isPlayer1Action, shipID)
    DebugOutBattlePlay(transfromshipLevel)
    transfromShipName = GameDataAccessHelper:GetCommanderVesselsImage(shipID, transfromshipLevel, false)
    DebugOutBattlePlay("SetTransformTarget:shipName:", transfromShipName)
    if isLeftShip then
      self.transfromShipName_Left = transfromShipName
      self.transfromShipGridIndex_Left = shipPos
    else
      self.transfromShipName_Right = transfromShipName
      self.transfromShipGridIndex_Right = shipPos
    end
  end
end
function GameObjectBattleReplay:RecordShipSPAnimData(isLeftShip, defPos, animType, effectDir, effectName, ishurtCenterPos)
  DebugOut("RecordShipSPAnimData", isLeftShip, defPos, effectName, effectDir, animType)
  if self.SPAnimData == nil then
    self.SPAnimData = {}
  end
  if self.SPAnimData[defPos] == nil then
    self.SPAnimData[defPos] = {}
  end
  self.SPAnimData[defPos].isLeftShip = isLeftShip
  self.SPAnimData[defPos].effectName = effectName
  self.SPAnimData[defPos].ishurtCenterPos = ishurtCenterPos
  self.SPAnimData[defPos].effectDir = effectDir
  if animType == AnimType.MISS and (self.SPAnimData[defPos].animType == AnimType.MISS or self.SPAnimData[defPos].animType == nil) then
    self.SPAnimData[defPos].animType = AnimType.MISS
  else
    self.SPAnimData[defPos].animType = AnimType.HURT
  end
end
function GameObjectBattleReplay:RecordShipChainSPAnimData(isLeftShip, defPos, animType, effectDir, effectName, ishurtCenterPos, chainIndex)
  if self.ChainSPAnimData == nil then
    self.ChainSPAnimData = {}
  end
  for _, v in pairs(self.ChainSPAnimData) do
    if v.isLeftShip == isLeftShip and v.def_pos == defPos then
      return
    end
  end
  chainIndex = #self.ChainSPAnimData + 1
  local chainAnimData = {}
  self.mChainAnimNumber = self.mChainAnimNumber or 0
  self.mChainAnimNumber = self.mChainAnimNumber + 1
  chainAnimData.isLeftShip = isLeftShip
  chainAnimData.effectName = effectName
  chainAnimData.def_pos = defPos
  chainAnimData.ishurtCenterPos = ishurtCenterPos
  if self.ChainSPAnimData[chainIndex - 1] then
    chainAnimData.effectDir = self:GetSPHurtEffectDir(defPos, self.ChainSPAnimData[chainIndex - 1].def_pos, isLeftShip)
    DebugOut("GetSPHurtEffectDir = ")
    DebugOut("hurt pos = ", defPos)
    DebugOut("ref pos = ", self.ChainSPAnimData[chainIndex - 1].def_pos)
    DebugOut("isLeftShip = ", isLeftShip)
    DebugOut("chainAnimData.effectDir = ", chainAnimData.effectDir)
  else
    chainAnimData.effectDir = effectDir
  end
  chainAnimData.animType = AnimType.HURT .. "_" .. chainIndex
  table.insert(self.ChainSPAnimData, chainAnimData)
  DebugOut("RecordShipChainSPAnimData =  ")
  DebugTable(self.ChainSPAnimData[chainIndex])
end
function GameObjectBattleReplay:RecordAttackAnimOverData(pos, isLeft)
  if self.AttackOverData == nil then
    self.AttackOverData = {}
  end
  local attackAnimOver = {}
  attackAnimOver.pos = pos
  attackAnimOver.isLeft = isLeft
  table.insert(self.AttackOverData, attackAnimOver)
end
function GameObjectBattleReplay:ProcessRecordedAttackOver()
  if self.AttackOverData then
    for key, value in pairs(self.AttackOverData) do
      self:OnAttackAnimOver(value.pos, value.isLeft)
    end
    self.AttackOverData = {}
  end
end
function GameObjectBattleReplay:PlayShipSPAnim()
  if self.SPAnimData then
    for key, value in pairs(self.SPAnimData) do
      self:PlayShipAnim(not value.isLeftShip, key, value.animType .. value.effectDir, value.effectName, value.ishurtCenterPos)
    end
    self.SPAnimData = {}
  end
end
function GameObjectBattleReplay:PlayRoundStepEffect(step, isResume)
  if not isResume and self.m_fakeBattle.m_fakeBattleId and self.m_fakeBattle.m_fakeBattleId >= 0 then
    DebugOutBattlePlay("Check  StopFakeBattle", self.m_currentRoundIndex, step)
    if TutorialQuestManager:CheckStopFakeBattle(self.m_currentRoundIndex, step) then
      self:StopFakeBattle(step)
      return
    end
  end
  DebugOutBattlePlay("PlayRoundStepEffect ", step, debug.traceback())
  if self.m_currentRoundStep == step then
    DebugOutBattlePlay("Warning : replay the same step")
    return
  end
  self.m_currentRoundStep = step
  self.m_currentRoundStepInfo = {}
  self.m_currentRoundStepInfo.AttackRoundIndex = 0
  self.m_currentRoundStepInfo.RectionCount = 0
  self.m_currentRoundStepInfo.TotalReactionCount = 0
  GameObjectBattleReplay.StepEffectProtectTimer = 0
  self.m_currentRoundStepInfo.BuffNameEffectTotalCount = 0
  self.m_currentRoundStepInfo.BuffNameEffectCount = 0
  self.m_currentRoundStepInfo.AntiInvisibleShipTotalCount = 0
  self.m_currentRoundStepInfo.AntiInvisibleShipCurCount = 0
  self.m_currentRoundStepInfo.sp_atk_type = 0
  if self.m_currentRoundStep == RoundStep.SUM_FLEET then
    self:UpdateSumFleet()
  elseif self.m_currentRoundStep == RoundStep.DOT then
    self:PlayDotEffect()
  elseif self.m_currentRoundStep == RoundStep.BUFF then
    self:UpdateBuffEffect()
  elseif self.m_currentRoundStep == RoundStep.ADJUTANT then
    self:PlayAdjutantSPEffect()
  elseif self.m_currentRoundStep == RoundStep.ARTIFACT then
    self:CheckPlayArtifact()
  elseif self.m_currentRoundStep == RoundStep.ATTACK then
    self:PlayNextAttackEffect()
  elseif self.m_currentRoundStep == RoundStep.SP_ATTACK then
    self:PlaySPAttack()
  elseif self.m_currentRoundStep == RoundStep.RELIVE then
    self:CheckPlayRelive()
  end
end
function GameObjectBattleReplay:UpdateSumFleet()
  if not self.m_battleResult then
    return
  end
  local roundData = self.m_battleResult.rounds[self.m_currentRoundIndex]
  local isPlayer1Action = roundData.player1_action
  local sumFleetData = roundData.summon_fleet
  local reliveData = roundData.relive_fleet
  DebugOut("UpdateSumFleet:", self.m_currentRoundIndex)
  DebugTable(sumFleetData)
  DebugTable(reliveData)
  local isPlayDeadAni = false
  if sumFleetData and #sumFleetData > 0 then
    for k, v in pairs(sumFleetData) do
      if v.atk_side and v.atk_side ~= "" then
        isPlayer1Action = v.atk_side == "p1"
      else
        isPlayer1Action = roundData.player1_action
      end
      if 0 < v.identity then
        local fleetDatas = self.m_fleetInfo.Left
        local isLeft
        if v.ptype and v.ptype ~= "" then
          isLeft = v.ptype == SIDE_LEFT and self.m_isPlayer1ShowInLeft or v.ptype == SIDE_RIGHT and true
        else
          isLeft = isPlayer1Action and self.m_isPlayer1ShowInLeft or not isPlayer1Action and not self.m_isPlayer1ShowInLeft
        end
        DebugOut("isLeft:", isLeft)
        if not isLeft then
          fleetDatas = self.m_fleetInfo.Right
        end
        local fleetData = fleetDatas[v.pos]
        if fleetData and 0 < fleetData.currentHP and 0 >= v.cur_durability then
          fleetData.currentHP = v.cur_durability
          self:OnShipDead(isLeft, v.pos)
          DebugOut("UpdateSumFleet:OnShipDead", isLeft, v.pos)
          DebugTable(fleetData)
          isPlayDeadAni = true
          fleetDatas[v.pos] = nil
        end
      end
    end
  end
  if reliveData and #reliveData > 0 then
    for k, v in pairs(reliveData) do
      if v.atk_side and v.atk_side ~= "" then
        isPlayer1Action = v.atk_side == "p1"
      else
        isPlayer1Action = roundData.player1_action
      end
      if 0 < v.identity then
        local fleetDatas = self.m_fleetInfo.Left
        local isLeft
        if v.ptype and v.ptype ~= "" then
          isLeft = v.ptype == SIDE_LEFT and self.m_isPlayer1ShowInLeft or v.ptype == SIDE_RIGHT and true
        else
          isLeft = isPlayer1Action and self.m_isPlayer1ShowInLeft or not isPlayer1Action and not self.m_isPlayer1ShowInLeft
        end
        DebugOut("isLeft:", isLeft)
        if not isLeft then
          fleetDatas = self.m_fleetInfo.Right
        end
        local fleetData = fleetDatas[v.pos]
        if fleetData and 0 < fleetData.currentHP and 0 >= v.cur_durability then
          fleetData.currentHP = v.cur_durability
          self:OnShipDead(isLeft, v.pos)
          DebugOut("UpdateSumFleet:OnShipDead", isLeft, v.pos)
          DebugTable(fleetData)
          isPlayDeadAni = true
          fleetDatas[v.pos] = nil
        end
      end
    end
  end
  if not isPlayDeadAni then
    GameObjectBattleReplay:PlayRoundStepEffect(RoundStep.DOT)
  end
end
function GameObjectBattleReplay:UpdateShipEnterBattleField()
  if not self.m_showRightShipEnter then
    return
  end
  if self.m_showRightShipEnterOver then
    return
  end
end
function GameObjectBattleReplay:PlayShipsEnter()
  DebugOutBattlePlay("GameObjectBattleReplay:PlayShipsEnter")
  DebugOutBattlePlayTable(self.m_showRightShipEnterData)
  for k, v in ipairs(self.m_showRightShipEnterData) do
    self:GetFlashObject():InvokeASCallback("_root", "setShipEnterBattle", v[1], v[2])
  end
  self.m_showRightShipEnterData = {}
end
function GameObjectBattleReplay:CheckShipEnterDataForGroupBattle(isLeft, gridIndex)
  DebugOutBattlePlay("GameObjectBattleReplay:CheckShipEnterData")
  if self.m_showRightShipEnter then
    table.insert(self.m_showRightShipEnterData, {isLeft, gridIndex})
    self:GetFlashObject():InvokeASCallback("_root", "HideShip", isLeft, gridIndex)
  end
end
function GameObjectBattleReplay:CheckShipEnterData(isLeft, gridIndex)
  DebugOutBattlePlay("GameObjectBattleReplay:CheckShipEnterData")
  if self.m_showRightShipEnter and not isLeft then
    table.insert(self.m_showRightShipEnterData, {isLeft, gridIndex})
    self:GetFlashObject():InvokeASCallback("_root", "HideShip", isLeft, gridIndex)
  end
end
function GameObjectBattleReplay:EndShipEnterBattleField()
  DebugOutBattlePlay("GameObjectBattleReplay:EndShipEnterBattleField")
  if not self.m_showRightShipEnterOver then
    DebugOutBattlePlay("GameObjectBattleReplay:EndShipEnterBattleField m_showRightShipEnterOver == true")
    self.m_showRightShipEnterOver = true
    self:PlayBattleResult()
  end
end
function GameObjectBattleReplay:CheckRemoveAbsorbBuffEffect(isLeft)
  DebugOut("CheckRemoveAbsorbBuffEffect 11", isLeft)
  for pos = 1, 9 do
    self:GetFlashObject():InvokeASCallback("_root", "HideAllShipBuffDesc", not isLeft, pos)
  end
end
function GameObjectBattleReplay:ClearSideData(isLeft)
  self.m_totalRoundCount = 0
  self.m_currentRoundIndex = 0
  self.m_currentRoundStep = RoundStep.NONE
  self.m_currentFightIndex = 0
  self.m_currentFightFlag = false
  self.m_currentFightRecord = {}
  self.m_leftPlayerBuff = {}
  self.m_rightPlayerBuff = {}
  if isLeft then
    self.m_fleetInfo.Left = {}
    self.m_fleetInfo.Left.totalHP = 0
    self.m_fleetInfo.Left.currentHP = 0
    self:GetFlashObject():InvokeASCallback("_root", "resetLeftGrid")
  else
    self.m_fleetInfo.Right = {}
    self.m_fleetInfo.Right.totalHP = 0
    self.m_fleetInfo.Right.currentHP = 0
    self:GetFlashObject():InvokeASCallback("_root", "resetRightGrid")
  end
end
function GameObjectBattleReplay:StartNextGroupBattle()
  local isPlayer1Faild = self.m_battleResult.result ~= 1
  local isLeftFaild = isPlayer1Faild and self.m_isPlayer1ShowInLeft or not isPlayer1Faild and not self.m_isPlayer1ShowInLeft
  DebugOut("StartNextGroupBattle", self.m_battleResult.result, self.m_isPlayer1ShowInLeft, isLeftFaild)
  self.m_battleResult = GameObjectBattleReplay.GroupBattleReportArr[1].headers[GameObjectBattleReplay.curGroupBattleIndex].report
  self:ClearSideData(isLeftFaild)
  self:CheckPreLoadHalfPortraitRes()
  self.m_showRightShipEnter = true
  self.m_showRightShipEnterOver = false
  local player_avatar = self.m_battleResult.player2_avatar
  local player_identity = self.m_battleResult.player2_identity
  local playerFleets = self.m_battleResult.player2_fleets
  local player_name = self.m_battleResult.player2
  local playerRank = GameUtils:GetPlayerRank(self.m_battleResult.player2_rank or 0, self.m_battleResult.player2_ranking)
  if isPlayer1Faild then
    playerFleets = self.m_battleResult.player1_fleets
    player_avatar = self.m_battleResult.player1_avatar
    player_identity = self.m_battleResult.player1_identity
    player_name = self.m_battleResult.player1
    playerRank = GameUtils:GetPlayerRank(self.m_battleResult.player1_rank or 0, self.m_battleResult.player1_ranking)
  end
  DebugOut("StartNextGroupBattle 1 :", player_avatar, player_identity, player_name, playerRank)
  DebugTable(playerFleets)
  local tmpPlayerAvatr = ""
  local tmpPlayerShipLevel = ""
  for k, v in pairs(playerFleets) do
    if v.identity == 1 then
      local player_sex = GameUtils:GetPlayerSexByAvatra(player_avatar)
      if player_avatar ~= "male" and player_avatar ~= "female" then
        tmpPlayerAvatr = GameDataAccessHelper:GetFleetAvatar(tonumber(player_avatar), 1)
      else
        tmpPlayerAvatr = GameDataAccessHelper:GetFleetAvatar(1, v.level, player_sex)
      end
    end
    if v.identity == player_identity then
      tmpPlayerShipLevel = v.level
    end
  end
  if tmpPlayerAvatr == nil or tmpPlayerAvatr == "" then
    if tonumber(player_avatar) ~= nil then
      tmpPlayerAvatr = GameDataAccessHelper:GetFleetAvatar(tonumber(player_avatar), 0)
    else
      tmpPlayerAvatr = player_avatar
    end
  end
  if GameDataAccessHelper:IsHeadResNeedDownload(tmpPlayerAvatr) and not GameDataAccessHelper:CheckFleetHasAvataImage(tmpPlayerAvatr) then
    tmpPlayerAvatr = "head9001"
  end
  if LuaUtils:string_startswith(player_name, "LC_NPC_") then
    player_name = GameLoader:GetGameText(player_name)
  elseif LuaUtils:string_startswith(player_name, "LC_BATTLE_") then
    player_name = GameLoader:GetGameText(player_name)
  elseif self._activeArea and self._activeBattle then
    player_name = GameDataAccessHelper:GetAreaNameText(self._activeArea, self._activeBattle)
  else
    player_name = GameUtils:GetUserDisplayName(player_name)
  end
  local ShipCap = GameDataAccessHelper:GetShip(player_identity, self._activeArea, tmpPlayerShipLevel)
  self:GetFlashObject():InvokeASCallback("_root", "initSidePlayerInfo", player_name, tmpPlayerAvatr, ShipCap, playerRank, isLeftFaild)
  local fleetDatas = self.m_fleetInfo.Right
  if isLeftFaild then
    fleetDatas = self.m_fleetInfo.Left
  end
  for i, v in ipairs(playerFleets) do
    DebugOut("qj self.m_battleResult.player2_fleets")
    DebugTable(playerFleets)
    fleetDatas[v.pos] = {}
    local fleetData = fleetDatas[v.pos]
    fleetData.maxShield = math.max(1, v.max_shield)
    fleetData.currentShield = v.shield
    fleetData.damageShield = {}
    fleetData.maxHP = v.max_durability
    fleetData.currentHP = v.cur_durability
    fleetData.damageHP = {}
    fleetData.accDelta = 0
    fleetData.currentAcc = v.cur_accumulator
    fleetData.needShowHP = false
    fleetData.buffs = {}
    if GameStateManager:GetCurrentGameState().m_preState == GameStateManager.GameStateWVE then
      fleetDatas.totalHP = fleetDatas.totalHP + v.max_durability
    else
      fleetDatas.totalHP = fleetDatas.totalHP + v.cur_durability
    end
    fleetDatas.currentHP = fleetDatas.currentHP + v.cur_durability
    local persentAngry = math.min(100, fleetData.currentAcc)
    if v.identity > 10000 then
      DebugOut("identity > 10000", GameStateBattleMap.m_currentAreaID, " GameStateBattlePlay.isLadderBattle=", GameStateBattlePlay.isLadderBattle)
      local ship = ""
      local avatar = ""
      if GameStateBattlePlay.isLadderBattle then
        ship = GameDataAccessHelper:GetMonsterShipById(v.identity)
        avatar = GameDataAccessHelper:GetMonsterAvatarById(v.identity)
      else
        ship = GameDataAccessHelper:GetMonsterShip(v.identity, false)
        avatar = GameDataAccessHelper:GetMonsterAvatar(GameStateBattleMap.m_currentAreaID, v.identity)
      end
      GameObjectBattleReplay:setShipInGrid(isLeftFaild, v.pos, math.min(1, fleetData.currentHP / fleetData.maxHP), fleetData.currentShield / fleetData.maxShield, persentAngry, ship, avatar, true)
    else
      local sex
      if player_avatar == "male" then
        sex = 1
      elseif player_avatar == "female" then
        sex = 2
      end
      local avatar = ""
      local ship = ""
      if v.identity == 1 and player_avatar ~= "female" and player_avatar ~= "male" then
        avatar = GameDataAccessHelper:GetFleetAvatar(tonumber(player_avatar), v.level)
        ship = GameDataAccessHelper:GetCommanderVesselsImage(tonumber(player_avatar), v.level, false)
      else
        avatar = GameDataAccessHelper:GetFleetAvatar(v.identity, v.level, sex)
        ship = GameDataAccessHelper:GetCommanderVesselsImage(v.identity, v.level, false)
      end
      GameObjectBattleReplay:setShipInGrid(isLeftFaild, v.pos, math.min(1, fleetData.currentHP / fleetData.maxHP), fleetData.currentShield / fleetData.maxShield, persentAngry, ship, avatar, true)
    end
    self:CheckShipEnterDataForGroupBattle(isLeftFaild, v.pos)
  end
  self:InitAdjutantAuraEffect(isPlayer1Faild)
  self:PlayShipsEnter()
  self:GetFlashObject():InvokeASCallback("_root", "updatePlayerHP", isLeftFaild, GameUtils.numberConversion2(fleetDatas.currentHP, true), fleetDatas.currentHP / fleetDatas.totalHP, false, false)
  return
end
function GameObjectBattleReplay:ResetRoundDamageData()
  if self.m_battleResult and self.m_battleResult.rounds and self.m_battleResult.rounds[self.m_currentRoundIndex] and self.m_battleResult.rounds[self.m_currentRoundIndex].damage then
    for k, v in pairs(self.m_battleResult.rounds[self.m_currentRoundIndex].damage) do
      v.play = false
      v.isCalculated = false
    end
  end
end
function GameObjectBattleReplay:StartNextRound()
  DebugOutBattlePlay("GameObjectBattleReplay:StartNextRound ", self.m_currentRoundIndex)
  self.m_currentRoundFocusGainTimer = 0
  self:CheckAllShipRemoveBuff(true)
  if self.m_currentRoundStepInfo then
    for i = 1, 9 do
      self:CheckDeadForDamgeData(true, i)
      self:CheckDeadForDamgeData(false, i)
    end
  end
  if self.m_currentRoundIndex == 0 then
    GameObjectBattleReplay:SavaBattleProgress(1, 0)
  end
  self.m_currentRoundIndex = self.m_currentRoundIndex + 1
  if not self.m_battleResult then
    GameObjectBattleReplay:SavaBattleProgress(2, 0)
    self:OnReplayOver()
    return
  end
  GameObjectBattleReplay.isMultipleAtk = false
  GameObjectBattleReplay.curMultipleAtkRound = 1
  GameObjectBattleReplay.needReliveFleet = {}
  GameObjectBattleReplay:ResetRoundDamageData()
  if self.m_battleResult and self.m_battleResult.rounds[self.m_currentRoundIndex] == nil and (not self.isGroupBattle or self.curGroupBattleIndex == #GameObjectBattleReplay.GroupBattleReportArr[1].headers) then
    DebugOut("OnReplayOver OnExit No Next Round", QuestTutorialBattleFailed:IsActive())
    if QuestTutorialBattleFailed:IsActive() then
      local function callback()
        GameObjectBattleReplay:SavaBattleProgress(2, 0)
        self:OnReplayOver()
      end
      GameUICommonDialog:PlayStory({1100026}, callback)
    elseif GameStateBattlePlay.m_currentAreaID == 60 and GameStateBattlePlay.m_currentBattleID == 9999 then
      local function callback()
        GameObjectBattleReplay:SavaBattleProgress(2, 0)
        self:OnReplayOver()
      end
      GameUICommonDialog:PlayStory({1100003}, callback)
    else
      GameObjectBattleReplay:SavaBattleProgress(2, 0)
      self:OnReplayOver()
    end
    return
  elseif self.m_battleResult and self.m_battleResult.rounds[self.m_currentRoundIndex] == nil and self.isGroupBattle and self.curGroupBattleIndex ~= #GameObjectBattleReplay.GroupBattleReportArr[1].headers then
    self.curGroupBattleIndex = self.curGroupBattleIndex + 1
    DebugOut("StartNextRound QJ :", self.isGroupBattle, self.curGroupBattleIndex, #GameObjectBattleReplay.GroupBattleReportArr[1].headers)
    DebugTable(GameObjectBattleReplay.GroupBattleReportArr[1].headers)
    if GameObjectBattleReplay.curGroupBattleIndex > #GameObjectBattleReplay.GroupBattleReportArr[1].headers then
      GameObjectBattleReplay:SavaBattleProgress(2, 0)
      self:OnReplayOver()
      return
    end
    self:StartNextGroupBattle()
    return
  end
  local roundData = self.m_battleResult.rounds[self.m_currentRoundIndex]
  if self.m_currentRoundIndex == 1 then
    self.m_currentFightFlag = roundData.player1_action
    self.m_currentFightIndex = self.m_currentFightIndex + 1
    self.m_currentFightRecord[roundData.pos] = true
  elseif self.m_currentFightFlag == roundData.player1_action then
    for _, v in pairs(roundData.relive_fleet or {}) do
      if v.pos < roundData.pos then
        self.m_currentFightRecord[v.pos] = true
      end
    end
    if not self.m_currentFightRecord[roundData.pos] then
      self.m_currentFightRecord[roundData.pos] = true
    else
      self.m_currentFightRecord = {}
      self.m_currentFightRecord[roundData.pos] = true
      self.m_currentFightIndex = self.m_currentFightIndex + 1
    end
  end
  if self:GetFlashObject() then
    if DebugConfig.isDebugBattlePlay then
      self:GetFlashObject():InvokeASCallback("_root", "setRound", self.m_currentFightIndex * 100 + self.m_currentRoundIndex)
    else
      self:GetFlashObject():InvokeASCallback("_root", "setRound", self.m_currentFightIndex)
    end
    if roundData and roundData.p1_speed and roundData.p2_speed then
      local left_speed = " "
      local right_speed = " "
      if self.m_isPlayer1ShowInLeft then
        left_speed = GameLoader:GetGameText("LC_MENU_SPD_COLON") .. roundData.p1_speed
        right_speed = GameLoader:GetGameText("LC_MENU_SPD_COLON") .. roundData.p2_speed
      else
        left_speed = GameLoader:GetGameText("LC_MENU_SPD_COLON") .. roundData.p2_speed
        right_speed = GameLoader:GetGameText("LC_MENU_SPD_COLON") .. roundData.p1_speed
      end
      DebugOut("fsafd adsf asdf ", roundData.p1_speed, roundData.p2_speed, left_speed, right_speed)
      self:GetFlashObject():InvokeASCallback("_root", "setPlayerSpeed", left_speed, right_speed)
    end
  end
  DebugOut("check need show fake battle dialog", GameGlobalData:GetFakeBattleIndex(), GameObjectTutorialCutscene:IsActive())
  if GameObjectTutorialCutscene:IsActive() and GameObjectTutorialCutscene:IsNeedShowNewFakeBattle() and GameGlobalData:GetFakeBattleIndex() and GameGlobalData:GetFakeBattleIndex() == 0 and self.m_currentRoundIndex == 3 then
    GameUICommonDialog:JudeIsLoadFlash()
    local function callback()
      local FteDialogFiniedReqCall = function()
        NetMessageMgr:SendMsg(NetAPIList.fte_req.Code, {
          content = {
            [1] = {
              key = "FinishedDialog",
              value = "1100098"
            }
          }
        }, nil, false, FteDialogFiniedReqCall)
      end
      FteDialogFiniedReqCall()
      self:PlayRoundStepEffect(RoundStep.SUM_FLEET)
    end
    local FteBegainDialogReqCall = function()
      NetMessageMgr:SendMsg(NetAPIList.fte_req.Code, {
        content = {
          [1] = {
            key = "BeginDialog",
            value = "1100098"
          }
        }
      }, nil, false, FteBegainDialogReqCall)
    end
    FteBegainDialogReqCall()
    GameUICommonDialog:PlayStory({1100098}, callback)
  elseif GameObjectTutorialCutscene:IsActive() and GameObjectTutorialCutscene:IsNeedShowNewFakeBattle() and GameGlobalData:GetFakeBattleIndex() and GameGlobalData:GetFakeBattleIndex() == 2 and self.m_currentRoundIndex == 1 then
    GameUICommonDialog:JudeIsLoadFlash()
    local function callback()
      local FteDialogFiniedReqCall = function()
        NetMessageMgr:SendMsg(NetAPIList.fte_req.Code, {
          content = {
            [1] = {
              key = "FinishedDialog",
              value = "1100100"
            }
          }
        }, nil, false, FteDialogFiniedReqCall)
      end
      FteDialogFiniedReqCall()
      self:PlayRoundStepEffect(RoundStep.SUM_FLEET)
    end
    local FteBegainDialogReqCall = function()
      NetMessageMgr:SendMsg(NetAPIList.fte_req.Code, {
        content = {
          [1] = {
            key = "BeginDialog",
            value = "1100100"
          }
        }
      }, nil, false, FteBegainDialogReqCall)
    end
    FteBegainDialogReqCall()
    GameUICommonDialog:PlayStory({1100100}, callback)
  elseif GameObjectTutorialCutscene:IsActive() and GameObjectTutorialCutscene:IsNeedShowNewFakeBattle() and GameGlobalData:GetFakeBattleIndex() and GameGlobalData:GetFakeBattleIndex() == 4 and self.m_currentRoundIndex == 7 then
    GameUICommonDialog:JudeIsLoadFlash()
    local function callback()
      local FteDialogFiniedReqCall = function()
        NetMessageMgr:SendMsg(NetAPIList.fte_req.Code, {
          content = {
            [1] = {
              key = "FinishedDialog",
              value = "1100102|1100103"
            }
          }
        }, nil, false, FteDialogFiniedReqCall)
      end
      FteDialogFiniedReqCall()
      self:PlayRoundStepEffect(RoundStep.SUM_FLEET)
    end
    local FteBegainDialogReqCall = function()
      NetMessageMgr:SendMsg(NetAPIList.fte_req.Code, {
        content = {
          [1] = {
            key = "BeginDialog",
            value = "1100102|1100103"
          }
        }
      }, nil, false, FteBegainDialogReqCall)
    end
    FteBegainDialogReqCall()
    GameUICommonDialog:PlayStory({1100102, 1100103}, callback)
  elseif GameObjectTutorialCutscene:IsActive() and GameObjectTutorialCutscene:IsNeedShowNewFakeBattle() and GameGlobalData:GetFakeBattleIndex() and GameGlobalData:GetFakeBattleIndex() == 5 and self.m_currentRoundIndex == 1 then
    GameUICommonDialog:JudeIsLoadFlash()
    local function callback()
      local FteDialogFiniedReqCall = function()
        NetMessageMgr:SendMsg(NetAPIList.fte_req.Code, {
          content = {
            [1] = {
              key = "FinishedDialog",
              value = "1100104"
            }
          }
        }, nil, false, FteDialogFiniedReqCall)
      end
      FteDialogFiniedReqCall()
      self:PlayRoundStepEffect(RoundStep.SUM_FLEET)
    end
    local FteBegainDialogReqCall = function()
      NetMessageMgr:SendMsg(NetAPIList.fte_req.Code, {
        content = {
          [1] = {
            key = "BeginDialog",
            value = "1100104"
          }
        }
      }, nil, false, FteBegainDialogReqCall)
    end
    FteBegainDialogReqCall()
    GameUICommonDialog:PlayStory({1100104}, callback)
  elseif GameObjectTutorialCutscene:IsActive() and not GameObjectTutorialCutscene:IsNeedShowNewFakeBattle() then
    GameUICommonDialog:JudeIsLoadFlash()
    local function callback()
      AddFlurryEvent("ShamBattle_Dilaog_1", {state = 2}, 1)
      self:PlayRoundStepEffect(RoundStep.SUM_FLEET)
    end
    AddFlurryEvent("ShamBattle_Dilaog_1", {state = 1}, 1)
    GameUICommonDialog:PlayStory({
      1100063,
      1100064,
      1100065
    }, callback)
  else
    self:PlayRoundStepEffect(RoundStep.SUM_FLEET)
  end
  for i, v in ipairs(self.m_battleResult.player1_fleets) do
    local isLeft = self.m_isPlayer1ShowInLeft
    local fleetData = self.m_fleetInfo.Left[v.pos]
    if not isLeft then
      fleetData = self.m_fleetInfo.Right[v.pos]
    end
    if fleetData and self:GetFlashObject() then
      local persentAngry = math.min(100, fleetData.currentAcc)
      self:GetFlashObject():InvokeASCallback("_root", "showShipAngryBar", isLeft, v.pos, persentAngry)
    end
  end
  for i, v in ipairs(self.m_battleResult.player2_fleets) do
    local isLeft = not self.m_isPlayer1ShowInLeft
    local fleetData = self.m_fleetInfo.Left[v.pos]
    if not isLeft then
      fleetData = self.m_fleetInfo.Right[v.pos]
    end
    if fleetData and self:GetFlashObject() then
      local persentAngry = math.min(100, fleetData.currentAcc)
      self:GetFlashObject():InvokeASCallback("_root", "showShipAngryBar", isLeft, v.pos, persentAngry)
    end
  end
end
function GameObjectBattleReplay:UpdateFleetAccInfo(isLeft, pos, accDelta, currentAcc)
  DebugOutBattlePlay("UpdateFleetAccInfo ", isLeft, pos, accDelta, currentAcc)
  local fleetDatas = self.m_fleetInfo.Left
  if not isLeft then
    fleetDatas = self.m_fleetInfo.Right
  end
  local fleetData = fleetDatas[pos]
  if fleetData then
    fleetData.currentAcc = currentAcc
    fleetData.accDelta = accDelta
    fleetData.isFullEnergy = fleetData.currentAcc >= 100 and fleetData.currentHP > 0
  end
end
function GameObjectBattleReplay:UpdateFleetHPInfo(isLeft, pos, damageHP, damageShield, currentShield, isAbsorted, isCrit, isBlocked, isImmune, durability, index)
  local fleetDatas = self.m_fleetInfo.Left
  if not isLeft then
    fleetDatas = self.m_fleetInfo.Right
  end
  DebugOut("UpdateFleetHPInfo ", isLeft, pos, damageHP, fleetDatas.currentHP, damageShield, currentShield, self.m_currentRoundIndex)
  DebugOut(debug.traceback())
  local fleetData = fleetDatas[pos]
  DebugTable(fleetData)
  if fleetData == nil then
    return true
  end
  if fleetData.isDead then
    return true
  end
  local realDamage = damageHP
  if damageHP < 0 then
    realDamage = math.max(-fleetData.currentHP, realDamage)
  end
  fleetData.needShowHP = true
  if fleetData.damageHP == nil then
    fleetData.damageHP = {}
  end
  fleetData.damageHP[#fleetData.damageHP + 1] = damageHP
  if not fleetData.curDamageIndex or index and index >= fleetData.curDamageIndex then
    fleetData.currentHP = durability
    fleetData.curDamageIndex = index
  elseif not index then
    fleetData.currentHP = fleetData.currentHP + realDamage
  end
  if fleetData.damageShield == nil then
    fleetData.damageShield = {}
  end
  fleetData.damageShield[#fleetData.damageShield + 1] = damageShield
  fleetData.currentShield = currentShield
  fleetData.isAbsorted = isAbsorted
  if fleetData.isCrit == nil then
    fleetData.isCrit = {}
  end
  fleetData.isCrit[#fleetData.isCrit + 1] = isCrit
  fleetData.isBlocked = isBlocked
  fleetData.isImmune = isImmune
  fleetDatas.currentHP = fleetDatas.currentHP + damageHP
  DebugOut("UpdateFleetHPInfo2 ", isLeft, pos, fleetData.currentHP, fleetDatas.currentHP, fleetDatas.currentShield)
  if #fleetData.damageHP > 1 or 1 < #fleetData.damageShield then
    return true
  else
    return false
  end
end
function GameObjectBattleReplay:UpdateFleetHp(isLeft, pos, currentHp)
  DebugOut("UpdateFleetHp ", isLeft, pos, currentHp)
  DebugOut(debug.traceback())
  local fleetDatas = self.m_fleetInfo.Left
  if not isLeft then
    fleetDatas = self.m_fleetInfo.Right
  end
  local fleetData = fleetDatas[pos]
  DebugTable(fleetData)
  if currentHp then
    fleetData.needShowHP = true
    fleetData.currentHP = currentHp
    local hpPersent = math.min(100, fleetData.currentHP * 100 / fleetData.maxHP)
    if self:GetFlashObject() then
      self:GetFlashObject():InvokeASCallback("_root", "showHotShipHPBar", isLeft, pos, -1, hpPersent)
    end
  end
  DebugOut("UpdateFleetHp Over")
  DebugTable(fleetData)
end
function GameObjectBattleReplay:UpdateFleetHotHPInfo(isLeft, pos, hotHP)
  DebugOut("UpdateFleetHotHPInfo ", isLeft, pos, hotHP)
  DebugOut(debug.traceback())
  local fleetDatas = self.m_fleetInfo.Left
  if not isLeft then
    fleetDatas = self.m_fleetInfo.Right
  end
  local fleetData = fleetDatas[pos]
  DebugTable(fleetData)
  fleetData.needShowHP = true
  if hotHP then
    fleetData.currentHP = fleetData.currentHP + hotHP
    fleetDatas.currentHP = fleetDatas.currentHP + hotHP
    local hpPersent = fleetDatas.currentHP / fleetDatas.totalHP
    if self:GetFlashObject() then
      self:GetFlashObject():InvokeASCallback("_root", "updatePlayerHP", isLeft, GameUtils.numberConversion2(fleetDatas.currentHP, true), hpPersent, true, false)
    end
  end
  DebugOut("UpdateFleetHotHPInfo Over")
  DebugTable(fleetData)
end
function GameObjectBattleReplay:InitBattleReplayInfo()
  DebugOutBattlePlay("InitBattleReplayInfo AreaID, BattleID: ", self._activeArea, self._activeBattle)
  local enemyInfo
  self.m_bossShipIndex = nil
  self.m_showRightShipEnterData = {}
  if self._activeArea and self._activeBattle then
    enemyInfo = GameDataAccessHelper:GetBattleInfo(self._activeArea, self._activeBattle)
  end
  if enemyInfo then
    self.m_isBoss = enemyInfo.EnemyType ~= 0
    if self.m_isBoss then
      self.m_bossShipIndex = self.m_battleResult.player2_pos
    end
    self.m_showRightShipEnter = enemyInfo.EnemyType ~= 0
    if self.m_showRightShipEnter then
      self.m_showRightShipEnterOver = false
    else
      self.m_showRightShipEnterOver = true
    end
  else
    self.m_isBoss = false
    self.m_showRightShipEnter = false
  end
  if GameObjectTutorialCutscene:IsActive() and GameObjectTutorialCutscene:IsNeedShowNewFakeBattle() and GameGlobalData:GetFakeBattleIndex() and GameGlobalData:GetFakeBattleIndex() ~= 6 then
    self.m_showRightShipEnter = true
    self.m_showRightShipEnterOver = false
  end
  if ext.resetAllGridInC ~= nil then
    ext.resetAllGridInC(self:GetFlashObject())
  else
    self:GetFlashObject():InvokeASCallback("_root", "resetAllGrid")
  end
  DebugOut("InitFakeBattle:player1_fleets:")
  DebugTable(self.m_battleResult.player1_fleets)
  DebugTable(self.m_battleResult)
  for i, v in ipairs(self.m_battleResult.player1_fleets) do
    local fleetDatas = self.m_fleetInfo.Left
    if not self.m_isPlayer1ShowInLeft then
      fleetDatas = self.m_fleetInfo.Right
    end
    fleetDatas[v.pos] = {}
    local fleetData = fleetDatas[v.pos]
    fleetData.maxShield = math.max(1, v.max_shield)
    fleetData.currentShield = v.shield
    fleetData.damageShield = {}
    fleetData.maxHP = v.max_durability
    fleetData.currentHP = v.cur_durability
    fleetData.damageHP = {}
    fleetData.accDelta = 0
    fleetData.currentAcc = v.cur_accumulator
    fleetData.needShowHP = false
    fleetData.buffs = {}
    fleetData.isDead = false
    if GameStateManager:GetCurrentGameState().m_preState == GameStateManager.GameStateWVE then
      fleetDatas.totalHP = fleetDatas.totalHP + v.max_durability
    else
      fleetDatas.totalHP = fleetDatas.totalHP + v.cur_durability
    end
    fleetDatas.currentHP = fleetDatas.currentHP + v.cur_durability
    local persentAngry = math.min(100, fleetData.currentAcc)
    if v.identity > 10000 then
      GameObjectBattleReplay:setShipInGrid(self.m_isPlayer1ShowInLeft, v.pos, math.min(1, fleetData.currentHP / fleetData.maxHP), fleetData.currentShield / fleetData.maxShield, persentAngry, GameDataAccessHelper:GetMonsterShip(v.identity, false), GameDataAccessHelper:GetMonsterAvatar(GameStateBattleMap.m_currentAreaID, v.identity))
    else
      local sex
      if self.m_battleResult.player1_avatar == "male" then
        sex = 1
      elseif self.m_battleResult.player1_avatar == "female" then
        sex = 2
      end
      local avatar = ""
      local vessels = ""
      local leaderlist = GameGlobalData:GetData("leaderlist")
      if v.identity == 1 and self.m_battleResult.player1_avatar ~= "female" and self.m_battleResult.player1_avatar ~= "male" then
        avatar = GameDataAccessHelper:GetFleetAvatar(tonumber(self.m_battleResult.player1_avatar), v.level)
        vessels = GameDataAccessHelper:GetCommanderVesselsImage(tonumber(self.m_battleResult.player1_avatar), v.level, false)
      else
        avatar = GameDataAccessHelper:GetFleetAvatar(v.identity, v.level, sex)
        vessels = GameDataAccessHelper:GetCommanderVesselsImage(v.identity, v.level, false)
      end
      GameObjectBattleReplay:setShipInGrid(self.m_isPlayer1ShowInLeft, v.pos, math.min(1, fleetData.currentHP / fleetData.maxHP), fleetData.currentShield / fleetData.maxShield, persentAngry, vessels, avatar)
    end
    self:CheckShipEnterData(self.m_isPlayer1ShowInLeft, v.pos)
  end
  for i, v in ipairs(self.m_battleResult.player2_fleets) do
    DebugOut("self.m_battleResult.player2_fleets")
    DebugTable(self.m_battleResult.player2_fleets)
    local fleetDatas = self.m_fleetInfo.Right
    if not self.m_isPlayer1ShowInLeft then
      fleetDatas = self.m_fleetInfo.Left
    end
    fleetDatas[v.pos] = {}
    local fleetData = fleetDatas[v.pos]
    fleetData.maxShield = math.max(1, v.max_shield)
    fleetData.currentShield = v.shield
    fleetData.damageShield = {}
    fleetData.maxHP = v.max_durability
    fleetData.currentHP = v.cur_durability
    fleetData.damageHP = {}
    fleetData.accDelta = 0
    fleetData.currentAcc = v.cur_accumulator
    fleetData.needShowHP = false
    fleetData.buffs = {}
    DebugOut("InitBattleReplayInfo:", GameStateManager:GetCurrentGameState().m_preState, ",", GameStateManager.GameStateWVE)
    if GameStateManager:GetCurrentGameState().m_preState == GameStateManager.GameStateWVE then
      DebugOut("WVE totalHP calculate")
      fleetDatas.totalHP = fleetDatas.totalHP + v.max_durability
    else
      fleetDatas.totalHP = fleetDatas.totalHP + v.cur_durability
    end
    fleetDatas.currentHP = fleetDatas.currentHP + v.cur_durability
    local persentAngry = math.min(100, fleetData.currentAcc)
    if v.identity > 10000 then
      DebugOut("identity > 10000", GameStateBattleMap.m_currentAreaID)
      local ship = ""
      local avatar = ""
      if GameStateBattlePlay.isLadderBattle then
        ship = GameDataAccessHelper:GetMonsterShipById(v.identity)
        avatar = GameDataAccessHelper:GetMonsterAvatarById(v.identity)
      else
        ship = GameDataAccessHelper:GetMonsterShip(v.identity, false)
        avatar = GameDataAccessHelper:GetMonsterAvatar(GameStateBattleMap.m_currentAreaID, v.identity)
      end
      GameObjectBattleReplay:setShipInGrid(not self.m_isPlayer1ShowInLeft, v.pos, math.min(1, fleetData.currentHP / fleetData.maxHP), fleetData.currentShield / fleetData.maxShield, persentAngry, ship, avatar)
    else
      local sex
      if self.m_battleResult.player2_avatar == "male" then
        sex = 1
      elseif self.m_battleResult.player2_avatar == "female" then
        sex = 2
      end
      local avatar = ""
      local ship = ""
      if v.identity == 1 and self.m_battleResult.player2_avatar ~= "female" and self.m_battleResult.player2_avatar ~= "male" then
        avatar = GameDataAccessHelper:GetFleetAvatar(tonumber(self.m_battleResult.player2_avatar), v.level)
        ship = GameDataAccessHelper:GetCommanderVesselsImage(tonumber(self.m_battleResult.player2_avatar), v.level, false)
      else
        avatar = GameDataAccessHelper:GetFleetAvatar(v.identity, v.level, sex)
        ship = GameDataAccessHelper:GetCommanderVesselsImage(v.identity, v.level, false)
      end
      GameObjectBattleReplay:setShipInGrid(not self.m_isPlayer1ShowInLeft, v.pos, math.min(1, fleetData.currentHP / fleetData.maxHP), fleetData.currentShield / fleetData.maxShield, persentAngry, ship, avatar)
    end
    self:CheckShipEnterData(not self.m_isPlayer1ShowInLeft, v.pos)
  end
  if self.m_bossShipIndex then
  end
  self:GetFlashObject():InvokeASCallback("_root", "LoadShipSkillEffect")
  self:InitAdjutantAuraEffect(true)
  self:InitAdjutantAuraEffect(false)
  self:SetBattleBuff()
end
function GameObjectBattleReplay:setShipInGrid(isLeft, gridIndex, hpPersent, mpPersent, persentAngry, shipName, avatar, isRelive)
  local fleets = self.m_battleResult.player1_fleets
  if not isLeft then
    fleets = self.m_battleResult.player2_fleets
  end
  DebugOutPutTable(fleets, "fleets")
  fleetData = nil
  for _, v in pairs(fleets) do
    if v.pos == gridIndex then
      fleetData = v
      break
    end
  end
  level = 1
  if fleetData and fleetData.level and fleetData.level > 15 then
    level = 2
  end
  if ext.setShipInGridInC ~= nil then
    if isRelive == true then
      self:GetFlashObject():InvokeASCallback("_root", "setShipInGrid", isLeft, gridIndex, hpPersent, mpPersent, persentAngry, shipName, avatar, isRelive, level)
    else
      local flashObj = self:GetFlashObject()
      ext.setShipInGridInC(flashObj, isLeft, gridIndex, hpPersent, mpPersent, persentAngry, shipName, avatar, isRelive, level)
    end
  else
    self:GetFlashObject():InvokeASCallback("_root", "setShipInGrid", isLeft, gridIndex, hpPersent, mpPersent, persentAngry, shipName, avatar, isRelive, level)
  end
  if persentAngry >= 100 then
    GameObjectBattleReplay:ShowShipFullEnergy(isLeft, gridIndex, fleetData)
  else
    self:GetFlashObject():InvokeASCallback("_root", "hideShipFullEnergyEffect", isLeft, gridIndex)
  end
end
function GameObjectBattleReplay:CallUpBaby(isLeft, pos, hp, shield, acc, shipName, avatar, buff_effect)
  GameObjectBattleReplay:setShipInGrid(isLeft, pos, hp, shield, acc, shipName, avatar, true)
  local buffEffect = GameDataAccessHelper:GetBuffEffectName(buff_effect)
  if buffEffect and buffEffect ~= "" then
    DebugOut("HideShipBody", isLeft, pos)
    if buffEffect ~= "buff_resurgence" then
      self:GetFlashObject():InvokeASCallback("_root", "HideShipBody", isLeft, pos)
    end
    self:playBuffEffect(isLeft, pos, buffEffect)
  end
end
function GameObjectBattleReplay:InsertNewFleet(insertData, player1_action, buff_effect)
  if not insertData or insertData.identity <= 0 then
    return
  end
  local fleetDatas = self.m_fleetInfo.Left
  local isLeft = player1_action and self.m_isPlayer1ShowInLeft or not player1_action and not self.m_isPlayer1ShowInLeft
  if not isLeft then
    fleetDatas = self.m_fleetInfo.Right
  end
  local fleetData = fleetDatas[insertData.pos]
  if fleetData and 0 < fleetData.currentHP then
    DebugOut("GameObjectBattleReplay:InsertNewFleets:insert pos has a ship already")
    return
  end
  DebugOut("ready add fleet", buff_effect)
  DebugTable(fleetData)
  DebugTable(insertData)
  fleetData = fleetData or {}
  fleetData.maxShield = math.max(1, insertData.max_shield)
  fleetData.currentShield = insertData.shield
  fleetData.isDead = false
  fleetData.damageShield = {}
  fleetData.maxHP = insertData.max_durability
  fleetData.currentHP = insertData.cur_durability
  fleetData.damageHP = {}
  fleetData.accDelta = 0
  fleetData.currentAcc = insertData.cur_accumulator
  fleetData.needShowHP = false
  fleetData.buffs = {}
  fleetDatas.totalHP = fleetDatas.totalHP + insertData.cur_durability
  fleetDatas.currentHP = fleetDatas.currentHP + insertData.cur_durability
  fleetDatas[insertData.pos] = fleetData
  local persentAngry = math.min(100, fleetData.currentAcc)
  if insertData.identity > 10000 then
    local ship, avatar
    if GameStateBattlePlay.isLadderBattle then
      ship = GameDataAccessHelper:GetMonsterShipById(insertData.identity)
      avatar = GameDataAccessHelper:GetMonsterAvatarById(insertData.identity)
    else
      ship = GameDataAccessHelper:GetMonsterShip(insertData.identity, false)
      avatar = GameDataAccessHelper:GetMonsterAvatar(GameStateBattleMap.m_currentAreaID, insertData.identity)
    end
    self:CallUpBaby(isLeft, insertData.pos, math.min(1, fleetData.currentHP / fleetData.maxHP), fleetData.currentShield / fleetData.maxShield, persentAngry, ship, avatar, buff_effect)
  else
    local sex
    local playerAvatar = self.m_battleResult.player1_avatar
    if not player1_action then
      playerAvatar = self.m_battleResult.player2_avatar
    end
    if playerAvatar == "male" then
      sex = 1
    elseif playerAvatar == "female" then
      sex = 2
    end
    local vessels = ""
    local avatar = ""
    if insertData.identity == 1 and self.m_battleResult.player1_avatar ~= "female" and self.m_battleResult.player1_avatar ~= "male" then
      avatar = GameDataAccessHelper:GetFleetAvatar(tonumber(self.m_battleResult.player1_avatar), insertData.level)
      vessels = GameDataAccessHelper:GetCommanderVesselsImage(tonumber(self.m_battleResult.player1_avatar), insertData.level, false)
    else
      avatar = GameDataAccessHelper:GetFleetAvatar(insertData.identity, insertData.level, sex)
      vessels = GameDataAccessHelper:GetCommanderVesselsImage(insertData.identity, insertData.level, false)
    end
    self:CallUpBaby(isLeft, insertData.pos, math.min(1, fleetData.currentHP / fleetData.maxHP), fleetData.currentShield / fleetData.maxShield, persentAngry, vessels, avatar, buff_effect)
  end
  DebugOut("InsertNewFleets:", insertData.identity, insertData.pos)
end
function GameObjectBattleReplay:InitAdjutantAuraEffect(isPlayer1)
  DebugOut("InitAdjutantAuraEffect " .. debug.traceback())
  DebugTable(self.m_fleetInfo)
  local player_aura = self.m_battleResult.player1_buffs_adjutant
  if not isPlayer1 then
    player_aura = self.m_battleResult.player2_buffs_adjutant
  end
  local isLeft = isPlayer1 and self.m_isPlayer1ShowInLeft or not isPlayer1 and not self.m_isPlayer1ShowInLeft
  local LeftFleetDatas = self.m_fleetInfo.Left
  local RightFleetDatas = self.m_fleetInfo.Right
  if player_aura and #player_aura > 0 then
    for i, v in ipairs(player_aura) do
      local buffDesc = GameDataAccessHelper:GetBuffEffectDesc(v.buff)
      if not buffDesc then
        DebugOutBattlePlay("InitAdjutantAuraEffect: buffDesc is nil")
        return
      end
      DebugOutBattlePlay("InitAdjutantAuraEffect:" .. buffDesc)
      DebugTable(v)
      local fleetData = RightFleetDatas[v.def_pos]
      if isLeft and v.self_cast or not isLeft and not v.self_cast then
        fleetData = LeftFleetDatas[v.def_pos]
      end
      fleetData.BuffLayerState = fleetData.BuffLayerState or {
        [1] = false,
        [2] = false,
        [3] = false,
        [4] = false
      }
      local BuffData = {}
      BuffData.EffectID = v.buff
      local layer = 1
      for i, v in ipairs(fleetData.BuffLayerState) do
        if not v then
          layer = i
          break
        end
      end
      DebugOut("GameDataAccessHelper:IsFullScreenBuff(v.buff) = " .. tostring(GameDataAccessHelper:IsFullScreenBuff(v.buff)))
      if v.self_cast then
        if GameDataAccessHelper:IsFullScreenBuff(v.buff) then
          self:AddFullScreenBuff(isLeft, v.buff, 300)
          self:CheckPlayFullScreenBuff(isLeft)
        else
          self:AddBuff(isLeft, v.def_pos, v.buff, 300, true)
          self:CheckPlayBuff(isLeft, v.def_pos)
        end
      elseif GameDataAccessHelper:IsFullScreenBuff(v.buff) then
        self:AddFullScreenBuff(not isLeft, v.buff, 300)
        self:CheckPlayFullScreenBuff(not isLeft)
      else
        self:AddBuff(not isLeft, v.def_pos, v.buff, 300, true)
        self:CheckPlayBuff(not isLeft, v.def_pos)
      end
    end
  end
end
function GameObjectBattleReplay:AddDownloadPath(isLeft, buffName, pngInfo)
  local pngName = pngInfo[1]
  local pngFullName = DynamicResDownloader:GetFullName(pngInfo[1], pngInfo[2])
  local extInfo = {}
  extInfo.pos = isLeft
  extInfo.buffName = buffName
  extInfo.resOldName = pngName
  extInfo.resNewName = pngFullName
  DynamicResDownloader:AddDynamicRes(pngName, pngInfo[2], extInfo, GameObjectBattleReplay.BuffDownloadCallback)
end
function GameObjectBattleReplay:IsBuffDownLoad(pngInfo)
  local pngFullName = DynamicResDownloader:GetFullName(pngInfo[1], pngInfo[2])
  DebugOut(pngFullName)
  return ...
end
function GameObjectBattleReplay.BuffDownloadCallback(info)
  local flashObj = GameObjectBattleReplay:GetFlashObject()
  if flashObj == nil or not GameStateManager:GetCurrentGameState():IsObjectInState(GameObjectBattleReplay) then
    return
  end
  DebugOutBattlePlay("BuffDownloadCallback")
  flashObj:ReplaceTexture(info.resOldName, info.resNewName)
  GameObjectBattleReplay:AddPlayerBuff(info.pos, info.buffName)
end
function GameObjectBattleReplay:AddPlayerBuff(isLeft, buffName)
  local buffStack = self.m_leftPlayerBuff
  if not isLeft then
    buffStack = self.m_rightPlayerBuff
  end
  DebugOutBattlePlay("AddPlayerBuff: ", isLeft, buffName)
  table.insert(buffStack, buffName)
  self:GetFlashObject():InvokeASCallback("_root", "showPlayerBuff", isLeft, #buffStack, buffName)
end
function GameObjectBattleReplay:CheckShowBuff(isLeft)
  local playerBuff = self.m_battleResult.player1_buffs
  if self.m_isPlayer1ShowInLeft and isLeft or not self.m_isPlayer1ShowInLeft and not isLeft then
    playerBuff = self.m_battleResult.player1_buffs
  else
    playerBuff = self.m_battleResult.player2_buffs
  end
  if playerBuff == nil then
    return
  end
  for i, buffInfo in ipairs(playerBuff) do
    local pngInfo = k_GlobalBuffName[buffInfo.type]
    if pngInfo then
      if #pngInfo > 0 then
        if self:IsBuffDownLoad(pngInfo) then
          DebugOut("CheckShowBuff is here", pngInfo[1], pngInfo[2])
          self:AddPlayerBuff(isLeft, buffInfo.type)
        else
          DebugOut("CheckShowBuff is not here", pngInfo[1], pngInfo[2])
          self:AddDownloadPath(isLeft, buffInfo.type, pngInfo)
        end
      else
        self:AddPlayerBuff(isLeft, buffInfo.type)
      end
    end
  end
end
function GameObjectBattleReplay:SetBattleBuff()
  self:GetFlashObject():InvokeASCallback("_root", "hideAllPlayerBuff")
  self:CheckShowBuff(true)
  self:CheckShowBuff(false)
end
function GameObjectBattleReplay:PlayBattleResult()
  DebugOutBattlePlay("GameObjectBattleReplay:PlayBattleResult")
  if DebugConfig.isAutoSkipBattle then
    self.m_currentRoundFocusGainTimer = 0
  else
    DebugOutBattlePlay("PlayBattleResult")
    self:StartNextRound()
  end
end
function GameObjectBattleReplay:testData(battleResult)
  for i, v in ipairs(battleResult.rounds) do
    for ik, vk in ipairs(v.buff) do
      if vk.buff_effect == 152 then
        vk.buff_effect = 999
      end
    end
    for ik, vk in ipairs(v.sp_attacks) do
      if vk.buff_effect == 152 then
        vk.buff_effect = 999
      end
    end
  end
end
function GameObjectBattleReplay:SetBattleData(battleResult, areaID, battleID)
  DebugCommand.DebugPoint("setBattleData", battleResult, areaID, battleID)
  DebugOut("SetBattleData ")
  if not battleResult then
    return
  end
  GameObjectBattleReplay:SavaBattleProgress(0, 0)
  DebugOut("SetBattleData " .. battleResult.player2)
  DebugTable(battleResult or {})
  DebugOutPutTable(GameObjectBattleReplay.GroupBattleReportArr or {}, "GameObjectBattleReplay.GroupBattleReportArr")
  self:ClearLocalData()
  self.m_battleResult = battleResult
  DebugOutBattlePlayTable(self.m_battleResult)
  if GameObjectTutorialCutscene:IsActive() then
    self.m_isPlayer1ShowInLeft = true
  elseif not GameObjectBattleReplay.isGroupBattle then
    if self.m_battleResult.player2 == GameGlobalData:GetUserInfo().name then
      self.m_isPlayer1ShowInLeft = false
    else
      self.m_isPlayer1ShowInLeft = true
    end
  else
    self.m_isPlayer1ShowInLeft = false
    if #GameObjectBattleReplay.GroupBattleReportArr[1].headers[1].p1_names == 1 and GameGlobalData:GetUserInfo().name == GameObjectBattleReplay.GroupBattleReportArr[1].headers[1].p1_names then
      self.m_isPlayer1ShowInLeft = true
    elseif #GameObjectBattleReplay.GroupBattleReportArr[1].headers[1].p2_names == 1 and GameGlobalData:GetUserInfo().name == GameObjectBattleReplay.GroupBattleReportArr[1].headers[1].p2_names then
      self.m_isPlayer1ShowInLeft = false
    else
      for _, v in ipairs(GameObjectBattleReplay.GroupBattleReportArr[1].headers[1].p1_names) do
        if GameGlobalData:GetUserInfo().name == v then
          self.m_isPlayer1ShowInLeft = true
          break
        end
      end
    end
  end
  self._activeArea = areaID
  self._activeBattle = battleID
  self:CheckPreLoadHalfPortraitRes()
  TutorialQuestManager:CheckInitFakeBattle()
end
function GameObjectBattleReplay:CheckPreLoadHalfPortraitRes()
  if not GameStateBattlePlay.isPlayEffect then
    return
  end
  for k, v in pairs(self.m_battleResult.player1_fleets) do
    local fleetAbility = GameDataAccessHelper:GetCommanderAbility(v.identity, v.level)
    if fleetAbility and fleetAbility.head ~= -1 and fleetAbility.ship ~= -1 and fleetAbility.banner ~= -1 then
      GameHelper:PreLoadHalfPortraitRes(fleetAbility.banner + 1, fleetAbility)
    end
  end
  for k, v in pairs(self.m_battleResult.player2_fleets) do
    local fleetAbility = GameDataAccessHelper:GetCommanderAbility(v.identity, v.level)
    if fleetAbility and fleetAbility.head ~= -1 and fleetAbility.ship ~= -1 and fleetAbility.banner ~= -1 then
      GameHelper:PreLoadHalfPortraitRes(fleetAbility.banner + 1, fleetAbility)
    end
  end
end
function GameObjectBattleReplay:SetEnemyInfo(monsterName, monsterAvatar, force)
  self.monsterName_notReport = monsterName
  self.monsterAvatar_notReport = monsterAvatar
  self.force_notReport = force
end
function GameObjectBattleReplay:OnAddToGameState()
  self.m_battleResult = self.m_battleResult or GameUIBattleResult.m_fightReport
  self._activeArea = self._activeArea or GameUIBattleResult.m_replayAreaID
  self._activeBattle = self._activeBattle or GameUIBattleResult.m_replayBattleID
  self:LoadFlashObject()
  self:InitBattleReplayInfo()
  if self.m_fakeBattle.m_fakeBattleCommand then
    GameObjectFakeBattle:PlayAnim(self.m_fakeBattle.m_fakeBattleCommand, nil)
  end
  local progress = GameGlobalData:GetData("progress")
  if GameObjectTutorialCutscene:IsActive() then
    if immanentversion175 then
      self:GetFlashObject():InvokeASCallback("_root", "showSkipBtn", false, true, "", GameLoader:GetGameText("LC_MENU_LOGIN_SKIP_BUTTON"))
    else
      self:GetFlashObject():InvokeASCallback("_root", "showSkipBtn", true, true, "", GameLoader:GetGameText("LC_MENU_LOGIN_SKIP_BUTTON"))
    end
    if GameObjectTutorialCutscene:IsNeedShowNewFakeBattle() then
      self:GetFlashObject():InvokeASCallback("_root", "showSkipBtn", false, true, "", GameLoader:GetGameText("LC_MENU_LOGIN_SKIP_BUTTON"))
    end
  else
    local vipinfo = GameGlobalData:GetData("vipinfo")
    local levelinfo = GameGlobalData:GetData("levelinfo")
    local curLevel = GameVipDetailInfoPanel:GetVipLevel()
    local viplevellimit = GameDataAccessHelper:GetVIPLimit("skip_battle")
    local showskip = true
    local lockStr = GameDataAccessHelper:GetVIPLimitText("skip_battle")
    local skipStr = GameLoader:GetGameText("LC_MENU_LOGIN_SKIP_BUTTON")
    local unlockStatus = curLevel >= viplevellimit
    if GameStateBattlePlay.curBattleType == "climbtower" or GameStateBattlePlay.curBattleType == "chaosspace" then
      viplevellimit = GameDataAccessHelper:GetVIPLimit("climbtower_skip_battle")
      unlockStatus = curLevel >= viplevellimit
      lockStr = GameDataAccessHelper:GetVIPLimitText("climbtower_skip_battle") .. skipStr
    elseif GameStateBattlePlay.curBattleType == "arena" then
      viplevellimit = GameDataAccessHelper:GetVIPLimit("arena_skip_battle")
      unlockStatus = curLevel >= viplevellimit
      lockStr = GameDataAccessHelper:GetVIPLimitText("arena_skip_battle") .. skipStr
    elseif GameStateBattlePlay.curBattleType == "acbattle" then
      viplevellimit = GameDataAccessHelper:GetVIPLimit("ac_skip_battle")
      unlockStatus = curLevel >= viplevellimit
      lockStr = GameDataAccessHelper:GetVIPLimitText("ac_skip_battle") .. skipStr
    elseif (GameStateBattlePlay.curBattleType == "pve" or GameStateBattlePlay.curBattleType == "adventure") and unlockStatus == false then
      viplevellimit = GameDataAccessHelper:GetLevelLimit("commmon_skip_battle")
      unlockStatus = viplevellimit <= levelinfo.level
      lockStr = GameDataAccessHelper:GetVIPLimitText("commmon_skip_battle") .. skipStr
    end
    self:GetFlashObject():InvokeASCallback("_root", "showSkipBtn", showskip, unlockStatus, lockStr, skipStr)
  end
  if GameStateBattlePlay.m_preState == GameStateManager.GameStateHeroHandbook then
    self:GetFlashObject():InvokeASCallback("_root", "showSkipBtn", true, true, "", GameLoader:GetGameText("LC_MENU_LOGIN_SKIP_BUTTON"))
  end
  local mode = GameStateBattlePlay:GetReplayMode()
  spell_pause = 0
  local leftPlayerName = ""
  local rightPlayerName = ""
  local leftAvatar = ""
  local rightAvatar = ""
  local leftRank = ""
  local rightRank = ""
  local leftcurrentHP = 0
  local rightCuurrentHP = 0
  local tmpPlayer1Avatar, tmpPlayer2Avatr
  local tmpPlayer1ShipLevel = 0
  local tmpPlayer2ShipLevel = 0
  DebugOut("self.m_battleResult.player1_avatar:", self.m_battleResult.player1_avatar)
  for k, v in pairs(self.m_battleResult.player1_fleets) do
    if v.identity == 1 then
      local player1_sex = GameUtils:GetPlayerSexByAvatra(self.m_battleResult.player1_avatar)
      if self.m_battleResult.player1_avatar ~= "male" and self.m_battleResult.player1_avatar ~= "female" then
        tmpPlayer1Avatar = GameDataAccessHelper:GetFleetAvatar(tonumber(self.m_battleResult.player1_avatar), v.level)
      else
        tmpPlayer1Avatar = GameDataAccessHelper:GetFleetAvatar(1, v.level, player1_sex)
      end
    end
    if v.identity == self.m_battleResult.player1_identity then
      tmpPlayer1ShipLevel = v.level
    end
  end
  DebugOut("self.m_battleResult.player2_avatar:", self.m_battleResult.player2_avatar)
  for k, v in pairs(self.m_battleResult.player2_fleets) do
    if v.identity == 1 then
      local player2_sex = GameUtils:GetPlayerSexByAvatra(self.m_battleResult.player2_avatar)
      if self.m_battleResult.player2_avatar ~= "male" and self.m_battleResult.player2_avatar ~= "female" then
        tmpPlayer2Avatr = GameDataAccessHelper:GetFleetAvatar(tonumber(self.m_battleResult.player2_avatar), 1)
      else
        tmpPlayer2Avatr = GameDataAccessHelper:GetFleetAvatar(1, v.level, player2_sex)
      end
    end
    if v.identity == self.m_battleResult.player2_identity then
      tmpPlayer2ShipLevel = v.level
    end
  end
  local playerLeftShipLevel = 0
  local playerRightShipLevel = 0
  local playerLeftIdentity = 0
  local playerRightIdentity = 0
  local leftcurrentHP, rightcurrentHP, leftMaxHP, rightMaxHP
  if self.m_isPlayer1ShowInLeft then
    leftPlayerName = self.m_battleResult.player1
    rightPlayerName = self.m_battleResult.player2
    leftRank = GameUtils:GetPlayerRank(self.m_battleResult.player1_rank or 0, self.m_battleResult.player1_ranking)
    rightRank = GameUtils:GetPlayerRank(self.m_battleResult.player2_rank or 0, self.m_battleResult.player2_ranking)
    leftAvatar = tmpPlayer1Avatar
    if leftAvatar == nil or leftAvatar == "" then
      if tonumber(self.m_battleResult.player1_avatar) ~= nil then
        leftAvatar = GameDataAccessHelper:GetFleetAvatar(tonumber(self.m_battleResult.player1_avatar), 0)
      else
        leftAvatar = self.m_battleResult.player1_avatar
      end
    end
    rightAvatar = tmpPlayer2Avatr
    if rightAvatar == nil or rightAvatar == "" then
      if tonumber(self.m_battleResult.player2_avatar) ~= nil then
        rightAvatar = GameDataAccessHelper:GetFleetAvatar(tonumber(self.m_battleResult.player2_avatar), 0)
      else
        rightAvatar = self.m_battleResult.player2_avatar
      end
    end
    if GameStateManager:GetCurrentGameState().m_preState == GameStateManager.GameStateWVE then
      leftMaxHP = self.m_fleetInfo.Left.totalHP
      rightMaxHP = self.m_fleetInfo.Right.totalHP
    else
      leftMaxHP = self.m_fleetInfo.Left.currentHP
      rightMaxHP = self.m_fleetInfo.Right.currentHP
    end
    leftcurrentHP = self.m_fleetInfo.Left.currentHP
    rightcurrentHP = self.m_fleetInfo.Right.currentHP
    playerLeftShipLevel = tmpPlayer1ShipLevel
    playerRightShipLevel = tmpPlayer2ShipLevel
    playerLeftIdentity = self.m_battleResult.player1_identity
    playerRightIdentity = self.m_battleResult.player2_identity
  else
    leftPlayerName = self.m_battleResult.player2
    rightPlayerName = self.m_battleResult.player1
    leftRank = GameUtils:GetPlayerRank(self.m_battleResult.player2_rank or 0, self.m_battleResult.player2_ranking)
    rightRank = GameUtils:GetPlayerRank(self.m_battleResult.player1_rank or 0, self.m_battleResult.player1_ranking)
    leftAvatar = tmpPlayer2Avatr
    if leftAvatar == nil or leftAvatar == "" then
      if tonumber(self.m_battleResult.player2_avatar) ~= nil then
        leftAvatar = GameDataAccessHelper:GetFleetAvatar(tonumber(self.m_battleResult.player2_avatar), 0)
      else
        leftAvatar = self.m_battleResult.player2_avatar
      end
    end
    rightAvatar = tmpPlayer1Avatar
    if rightAvatar == nil or rightAvatar == "" then
      if tonumber(self.m_battleResult.player1_avatar) ~= nil then
        rightAvatar = GameDataAccessHelper:GetFleetAvatar(tonumber(self.m_battleResult.player1_avatar), 0)
      else
        rightAvatar = self.m_battleResult.player1_avatar
      end
    end
    if GameStateManager:GetCurrentGameState().m_preState == GameStateManager.GameStateWVE then
      leftMaxHP = self.m_fleetInfo.Left.totalHP
      rightMaxHP = self.m_fleetInfo.Right.totalHP
    else
      leftMaxHP = self.m_fleetInfo.Left.currentHP
      rightMaxHP = self.m_fleetInfo.Right.currentHP
    end
    leftcurrentHP = self.m_fleetInfo.Left.currentHP
    rightcurrentHP = self.m_fleetInfo.Right.currentHP
    playerLeftShipLevel = tmpPlayer2ShipLevel
    playerRightShipLevel = tmpPlayer1ShipLevel
    playerLeftIdentity = self.m_battleResult.player2_identity
    playerRightIdentity = self.m_battleResult.player1_identity
  end
  DebugOut("left player name = ", leftPlayerName)
  DebugOut("right player name = ", rightPlayerName)
  if LuaUtils:string_startswith(leftPlayerName, "LC_NPC_") then
    leftPlayerName = GameLoader:GetGameText(leftPlayerName)
  elseif LuaUtils:string_startswith(leftPlayerName, "LC_BATTLE_") then
    leftPlayerName = GameLoader:GetGameText(leftPlayerName)
  else
    leftPlayerName = GameUtils:GetUserDisplayName(leftPlayerName)
  end
  if LuaUtils:string_startswith(rightPlayerName, "LC_NPC_") then
    rightPlayerName = GameLoader:GetGameText(rightPlayerName)
  elseif LuaUtils:string_startswith(rightPlayerName, "LC_BATTLE_") then
    rightPlayerName = GameLoader:GetGameText(rightPlayerName)
  elseif self._activeArea and self._activeBattle then
    rightPlayerName = GameDataAccessHelper:GetAreaNameText(self._activeArea, self._activeBattle)
  else
    rightPlayerName = GameUtils:GetUserDisplayName(rightPlayerName)
  end
  DebugOut("left player name 222= ", leftPlayerName)
  DebugOut("right player name 222= ", rightPlayerName)
  local displayHPAsK = false
  if GameStateBattlePlay.m_currentAreaID == 53 then
  end
  if GameDataAccessHelper:IsHeadResNeedDownload(leftAvatar) and not GameDataAccessHelper:CheckFleetHasAvataImage(leftAvatar) then
    leftAvatar = "head9001"
  end
  if GameDataAccessHelper:IsHeadResNeedDownload(rightAvatar) and not GameDataAccessHelper:CheckFleetHasAvataImage(rightAvatar) then
    rightAvatar = "head9001"
  end
  if self.monsterAvatar_notReport ~= nil then
    rightAvatar = self.monsterAvatar_notReport
    self.monsterAvatar_notReport = nil
  end
  if self.monsterName_notReport ~= nil then
    rightPlayerName = self.monsterName_notReport
    self.monsterName_notReport = nil
  end
  local leftShipCap
  if playerLeftIdentity == 1 and self.m_battleResult.player1_avatar ~= "male" and self.m_battleResult.player1_avatar ~= "female" and tonumber(self.m_battleResult.player1_avatar) ~= nil then
    leftShipCap = GameDataAccessHelper:GetShip(tonumber(self.m_battleResult.player1_avatar), self._activeArea, playerLeftShipLevel)
  else
    leftShipCap = GameDataAccessHelper:GetShip(playerLeftIdentity, self._activeArea, playerLeftShipLevel)
  end
  local rightShipCap
  if GameStateBattlePlay.isLadderBattle then
    rightShipCap = GameDataAccessHelper:GetMonsterShipById(playerRightIdentity)
  elseif playerRightIdentity == 1 and self.m_battleResult.player2_avatar ~= "male" and self.m_battleResult.player2_avatar ~= "female" and tonumber(self.m_battleResult.player2_avatar) then
    rightShipCap = GameDataAccessHelper:GetShip(tonumber(self.m_battleResult.player2_avatar), self._activeArea, playerRightShipLevel)
  elseif playerRightIdentity > 10000 then
    local tmpShip1 = ""
    tmpShip1 = GameDataAccessHelper:GetMonsterShip(playerRightIdentity, false)
    rightShipCap = GameDataAccessHelper:GetShipByShipImage(tmpShip1)
  else
    rightShipCap = GameDataAccessHelper:GetShip(playerRightIdentity, self._activeArea, playerRightShipLevel)
  end
  DebugOut("left avatar:", leftAvatar, tmpPlayer2Avatr, self.m_battleResult.player2_avatar)
  DebugOut("right avatar:", rightAvatar, tmpPlayer1Avatr, self.m_battleResult.player1_avatar)
  self.m_totalRoundCount = #self.m_battleResult.rounds
  self:GetFlashObject():InvokeASCallback("_root", "initPlayerInfo", leftPlayerName, leftAvatar, leftShipCap, rightPlayerName, rightAvatar, rightShipCap, leftRank, rightRank)
  self:GetFlashObject():InvokeASCallback("_root", "updatePlayerHP", true, GameUtils.numberConversion2(leftcurrentHP, true), leftcurrentHP / leftMaxHP, false, false)
  self:GetFlashObject():InvokeASCallback("_root", "updatePlayerHP", false, GameUtils.numberConversion2(rightcurrentHP, true), rightcurrentHP / rightMaxHP, false, displayHPAsK)
  math.randomseed(ext.getSysTime())
  local fakeBattleID = TutorialQuestManager:GetTurialFakeBattleID()
  DebugOutBattlePlay("fakeBattleID ", fakeBattleID)
  if fakeBattleID then
    self:HideBattleSpeed()
  elseif GameGlobalData:GetModuleStatus("combat_speed") then
    ext.SetAppSpeedScale(GameUtils:GetBattleSpeed())
    self:DisplayBattleSpeed()
  else
    GameUtils:SetBattleSpeed(GameObjectBattleReplay.UP_BATTLE_SPEED)
    ext.SetAppSpeedScale(GameObjectBattleReplay.UP_BATTLE_SPEED)
    self:HideBattleSpeed()
  end
  self.m_currentRoundFocusGainTimer = nil
  self.m_frameCounter = 0
  BattleEffectManager:PreLoadBattlePlayImage(self.m_battleResult)
  if ENABLE_BATTLE_LOADING_STEP then
    self:GetFlashObject():InvokeASCallback("_root", "hidePlayerHead")
    self:GetFlashObject():InvokeASCallback("_root", "hidePlayersShip")
  end
  if FlashMemTracker then
    FlashMemTracker.DumpAllFlashMemstate()
  end
end
function GameObjectBattleReplay:OnEraseFromGameState()
  self:GetFlashObject():InvokeASCallback("_root", "freeData")
  self:UnloadFlashObject()
  self:ClearLocalData()
  self.GroupBattleReportArr = {}
  self.isGroupBattle = nil
  if GameObjectTutorialCutscene:IsActive() and not GameObjectTutorialCutscene:IsNeedShowNewFakeBattle() then
    GameUICommonDialog:JudeIsLoadFlash()
    local function dialogCallback()
      AddFlurryEvent("ShamBattle_Dilaog_2", {state = 2}, 1)
      GameStateManager:SetCurrentGameState(GameStateManager.GameStateEmptyState)
      GameObjectTutorialCutscene:ShowTutorial("1")
    end
    AddFlurryEvent("ShamBattle_Dilaog_2", {state = 1}, 1)
    GameUICommonDialog:PlayStory({1100066, 1100067}, dialogCallback)
  end
end
function GameObjectBattleReplay:StepEffectProtect(dt)
  if GameObjectBattleReplay.m_currentRoundStep ~= RoundStep.ATTACK and GameObjectBattleReplay.m_currentRoundStep ~= RoundStep.SP_ATTACK then
    GameObjectBattleReplay.StepEffectProtectTimer = 0
    return
  end
  DebugTable(GameObjectBattleReplay.m_currentRoundStepInfo)
  if not GameObjectBattleReplay.m_currentRoundStepInfo or not (GameObjectBattleReplay.m_currentRoundStepInfo.RectionCount < GameObjectBattleReplay.m_currentRoundStepInfo.TotalReactionCount) or not GameObjectBattleReplay.StepEffectProtectTimer or GameObjectBattleReplay.StepEffectProtectTimer == 0 then
    return
  end
  GameObjectBattleReplay.StepEffectProtectTimer = GameObjectBattleReplay.StepEffectProtectTimer - dt
  if GameObjectBattleReplay.StepEffectProtectTimer < 0 then
    GameObjectBattleReplay.StepEffectProtectTimer = 0
    DebugOut("StepEffectProtect StepEffectProtectTimer time out!")
    GameObjectBattleReplay.m_currentRoundStepInfo.RectionCount = GameObjectBattleReplay.m_currentRoundStepInfo.TotalReactionCount
    GameObjectBattleReplay:OnReactionAnimOver(-1, false, false)
  end
end
function GameObjectBattleReplay:Update(dt)
  if DebugConfig.isAutoSkipBattle and self.m_currentRoundFocusGainTimer then
    GameObjectBattleReplay:SavaBattleProgress(2, 0)
    self:OnReplayOver()
    self.m_currentRoundFocusGainTimer = nil
  end
  self.m_frameCounter = self.m_frameCounter + 1
  if self:GetFlashObject() then
    self:GetFlashObject():Update(dt)
    if ENABLE_BATTLE_LOADING_STEP then
      local isLoadFinished = BattleEffectManager:UpdateLoadingRes()
      if isLoadFinished and self:GetFlashObject() then
        if self.m_frameCounter == 2 then
          self:GetFlashObject():InvokeASCallback("_root", "showPlayerHead")
          self:GetFlashObject():InvokeASCallback("_root", "showPlayersShip")
          self:OnReplayStart()
        end
      else
        self.m_frameCounter = 1
      end
    elseif self.m_frameCounter == 2 then
      self:OnReplayStart()
    end
  end
  if self:GetFlashObject() then
  end
  if not self.m_showRightShipEnterOver and self.m_showRightShipEnter then
    self:UpdateShipEnterBattleField()
  elseif not self.m_fakeBattle.m_stopBattle then
    if self.m_JumpToNextRoundTimer and self.m_JumpToNextRoundTimer ~= -1 then
      self.m_JumpToNextRoundTimer = self.m_JumpToNextRoundTimer - dt
      if 0 >= self.m_JumpToNextRoundTimer then
        self.m_JumpToNextRoundTimer = -1
        DebugOutBattlePlay("m_JumpToNextRoundTimer <= 0")
        self:ClearFleetDamageData()
        self:StartNextRound()
      end
    end
    GameObjectBattleReplay:StepEffectProtect(dt)
    if self.m_currentRoundFocusGainTimer then
      if not GameUICommonDialog:HasPendingStory() then
        self.m_currentRoundFocusGainTimer = self.m_currentRoundFocusGainTimer + dt
      end
      if self.m_currentRoundFocusGainTimer > 25000 and not QuestTutorialBattleFailed:IsActive() and not GameObjectTutorialCutscene:IsActive() then
        DebugOutBattlePlay("OnReplayOver OnExit > 15000")
        GameObjectBattleReplay:SavaBattleProgress(2, 1)
        self:OnReplayOver()
      end
    end
  end
end
function GameObjectBattleReplay:ClearFleetDamageData()
  local fleetDatas = self.m_fleetInfo.Left
  for key, value in pairs(fleetDatas) do
    if type(value) == "table" then
      value.isCrit = {}
      value.damageHP = {}
      value.damageShield = {}
    end
  end
  fleetDatas = self.m_fleetInfo.Right
  for key, value in pairs(fleetDatas) do
    if type(value) == "table" then
      value.isCrit = {}
      value.damageHP = {}
      value.damageShield = {}
    end
  end
end
function GameObjectBattleReplay:ClearFleetDamageDataByGrid(isLeft, gridPos)
  DebugOut("ClearFleetDamageDataByGrid = ", isLeft)
  DebugOut("ClearFleetDamageDataByGrid = ", gridPos)
  local fleetDatas = self.m_fleetInfo.Left
  if not isLeft then
    fleetDatas = self.m_fleetInfo.Right
  end
  local fleetData = fleetDatas[gridPos]
  DebugTable(fleetData)
  if fleetData then
    fleetData.isCrit = {}
    fleetData.damageHP = {}
    fleetData.damageShield = {}
  end
end
function GameObjectBattleReplay:ClearFleetHotDataByGrid(isLeft, gridPos)
  DebugOut("ClearFleetDamageDataByGrid = ", isLeft)
  DebugOut("ClearFleetDamageDataByGrid = ", gridPos)
  local fleetDatas = self.m_fleetInfo.Left
  if not isLeft then
    fleetDatas = self.m_fleetInfo.Right
  end
  local fleetData = fleetDatas[gridPos]
  DebugTable(fleetData)
  if fleetData then
    fleetData.isCrit = {}
    fleetData.damageHP = {}
    fleetData.damageShield = {}
  end
end
function GameObjectBattleReplay:GetRandReward(isLeft, gridPos)
  do return RewardType.NONE, 0 end
  if GameObjectTutorialCutscene:IsActive() or GameStateBattlePlay:GetReplayMode() ~= GameStateBattlePlay.MODE_PLAY or isLeft and self.m_isPlayer1ShowInLeft or not isLeft and not self.m_isPlayer1ShowInLeft then
    return RewardType.NONE, 0
  end
  if math.random() <= 0.5 then
    return RewardType.MONEY, 5
  else
    return RewardType.NONE, 0
  end
end
function GameObjectBattleReplay:OnShipDead(isLeft, gridPos)
  local fleetDatas = self.m_fleetInfo.Left
  if not isLeft then
    fleetDatas = self.m_fleetInfo.Right
  end
  local fleetData = fleetDatas[gridPos]
  if fleetData.isDead then
    return
  end
  fleetData.isDead = true
  DebugOut("OnShipDead:", isLeft, gridPos, fleetData.isHurtAnimOver)
  self:PlayShipAnim(isLeft, gridPos, AnimType.DEAD, "dead", true)
  if ext.setShipDeadInVokeInC ~= nil then
    ext.setShipDeadInVokeInC(self:GetFlashObject(), isLeft, gridPos)
  else
    self:GetFlashObject():InvokeASCallback("_root", "hideHurtShipHPBar", isLeft, gridPos)
    self:GetFlashObject():InvokeASCallback("_root", "hideShipFullEnergy", isLeft, gridPos)
    self:GetFlashObject():InvokeASCallback("_root", "HideAllShipBuffDesc", isLeft, gridPos)
    self:GetFlashObject():InvokeASCallback("_root", "HideAllShipBuffEffect", isLeft, gridPos)
  end
  local rewardType, val = self:GetRandReward(isLeft, gridPos)
  if rewardType ~= RewardType.NONE then
    self:GetFlashObject():InvokeASCallback("_root", "showRewardBox", isLeft, gridPos, rewardType, val)
  end
  if not GameObjectBattleReplay.isMultipleAtk then
    self:CheckFleetsRemoveBuff(isLeft)
  end
  GameObjectShakeScreen:StartShaking()
  for i, v in ipairs(fleetData.buffs) do
    if v.EffectID == 10067 then
      local needRelive = {}
      needRelive.isLeft = isLeft
      needRelive.gridPos = gridPos
      table.insert(GameObjectBattleReplay.needReliveFleet, needRelive)
    end
  end
  GameObjectBattleReplay:AddTotalReactionCount()
  DebugOutBattlePlay("TotalReactionCount++ in GameObjectBattleReplay:OnShipDead: " .. self.m_currentRoundStepInfo.TotalReactionCount)
end
function GameObjectBattleReplay:OnHurtAnimOver(gridPos, isLeft)
  DebugOutBattlePlay("onHurtAnimIsOver ", gridPos, isLeft)
  self:GetFlashObject():InvokeASCallback("_root", "playShipBGEffect", isLeft, gridPos, "normal")
  self:GetFlashObject():InvokeASCallback("_root", "PlayShipBGAnim", isLeft, gridPos, "_loop")
  local fleetDatas = self.m_fleetInfo.Left
  if not isLeft then
    fleetDatas = self.m_fleetInfo.Right
  end
  local fleetData = fleetDatas[gridPos]
  DebugOutBattlePlay("fleetData.damageHP = ")
  DebugTable(fleetData.damageHP)
  local hpPersent = fleetDatas.currentHP / fleetDatas.totalHP
  local displayHPAsK = false
  self:GetFlashObject():InvokeASCallback("_root", "updatePlayerHP", isLeft, GameUtils.numberConversion2(fleetDatas.currentHP, true), hpPersent, true, displayHPAsK)
  local BeHurtRoundStep = fleetData.TagHurtRoundStep
  local BeHurtRoundIndex = fleetData.TagHurtRoundIndex
  local BeHurtAttackRoundIndex = fleetData.TagHurtAttackRoundIndex
  local roundData = self.m_battleResult.rounds[BeHurtRoundIndex]
  DebugOut("OnHurtAnimOver:BuHurtRoundStep:", BeHurtRoundStep, ",", fleetDatas.currentHP)
  if BeHurtRoundStep == RoundStep.DOT then
    self:GetFlashObject():InvokeASCallback("_root", "PlayShipBGAnim", isLeft, gridPos, "_loop")
  elseif BeHurtRoundStep == RoundStep.ADJUTANT then
    self:GetFlashObject():InvokeASCallback("_root", "PlayShipBGAnim", isLeft, gridPos, "_loop")
    DebugOutBattlePlay("OnHurtAnimOver ADJUTANT play_loop_anim")
  elseif BeHurtRoundStep == RoundStep.ATTACK then
  elseif BeHurtRoundStep == RoundStep.SP_ATTACK then
    DebugOutBattlePlay("OnHurtAnimOver hideFullHurtAnim")
    self:GetFlashObject():InvokeASCallback("_root", "hideFullHurtAnim", isLeft, gridPos)
  end
  fleetData.isHurtAnimOver = true
  fleetData.isPlayingHurtAnim = false
  if fleetData.currentHP <= 0 and not fleetData.isRevengeAtkOver then
    DebugOutBattlePlay("OnHurtAnimOver:fleetData:CurrentHP;", gridPos, isLeft)
    DebugTable(fleetData)
    local playDeadAimLater = false
    if fleetData.BuffNameEffect ~= nil then
      for i, v in ipairs(fleetData.BuffNameEffect) do
        if v.isPlayed == false then
          playDeadAimLater = true
          break
        end
      end
    end
    if not playDeadAimLater then
      self:OnShipDead(isLeft, gridPos)
    end
  end
  fleetData.TagHurtRound = nil
  fleetData.TagHurtAttackRound = nil
  if not GameObjectBattleReplay.isMultipleAtk then
    self:PlayArtifactSPEffect(true, BuffUpdateStep.AfterAttackOver)
  end
  self:CheckFleetsRemoveBuff(isLeft)
  self:CheckFleetsPlayBuff(isLeft)
  if not GameObjectBattleReplay.isMultipleAtk then
    local haveShootoff = #ShootoffSPData > 0
    local haveRevenge = #RevengeSPData > 0
    self:PlayShootoffSPEffect()
    if haveShootoff and haveRevenge then
    else
      self:PlayRevengeSPEffect()
    end
    self:CheckRemoveFullScreenBuff(isLeft)
  end
  DebugOutBattlePlay("    OnReactionAnimOver in  OnHurtAnimOver", gridPos, isLeft)
  self:OnReactionAnimOver(gridPos, isLeft)
end
function GameObjectBattleReplay:UpdateFleetPolymorphHPInfo(isLeft, gridPos, HPChange)
  if HPChange <= 0 then
    return
  end
  DebugOutBattlePlay("UpdateFleetPolymorphHPInfo ", gridPos, isLeft, HPChange)
  local fleetData = self.m_fleetInfo.Left[gridPos]
  if not isLeft then
    fleetData = self.m_fleetInfo.Right[gridPos]
  end
  if HPChange and HPChange > 0 then
    self:UpdateFleetHotHPInfo(isLeft, gridPos, HPChange)
  end
  local persentHP = math.min(100, fleetData.currentHP * 100 / fleetData.maxHP)
  self:GetFlashObject():InvokeASCallback("_root", "showHotShipHPBar", isLeft, gridPos, -1, persentHP)
end
function GameObjectBattleReplay:PlayHotEffect(gridPos, isLeft, buff)
  if not buff then
    return
  end
  DebugOutBattlePlay("PlayHotEffect ", gridPos, isLeft)
  local fleetData = self.m_fleetInfo.Left[gridPos]
  if not isLeft then
    fleetData = self.m_fleetInfo.Right[gridPos]
  end
  DebugOut("fleetData = ")
  DebugTable(fleetData)
  DebugOut("check play buff = ")
  DebugTable(buff)
  if buff.param and buff.param >= 0 then
    self:UpdateFleetHotHPInfo(isLeft, gridPos, buff.param)
  end
  local persentHP = math.min(100, fleetData.currentHP * 100 / fleetData.maxHP)
  hotHPValue = buff.param
  if hotHPValue then
    local buffEffect = GameDataAccessHelper:GetBuffEffectName(buff.buff_effect)
    DebugOut("buffEffect = ", buffEffect)
    if buffEffect and buffEffect ~= "" and buff.buff_effect ~= 48 then
      DebugOutBattlePlay("GameObjectBattleReplay:PlayHotEffect: ", isLeft, gridPos, buffEffect)
      self:playBuffEffect(isLeft, gridPos, buffEffect)
      DebugOutBattlePlay("PlayShipBuffEffect", isLeft, gridPos, buffEffect)
    end
    if 0 > hotHPValue then
      hotHPValue = 0
    end
    self:GetFlashObject():InvokeASCallback("_root", "showHotShipHPBar", isLeft, gridPos, hotHPValue, persentHP)
  end
end
function GameObjectBattleReplay:OnUpdateHPBar(gridPos, isLeft)
  DebugOutBattlePlay("OnUpdateHPBar ", gridPos, isLeft)
  local fleetData = self.m_fleetInfo.Left[gridPos]
  if not isLeft then
    fleetData = self.m_fleetInfo.Right[gridPos]
  end
  DebugOut("fleetData = ")
  DebugTable(fleetData)
  if not fleetData then
    return
  end
  if fleetData.isDead and fleetData.isDead == true then
    DebugOut("Ship is already dead", isLeft, gridPos)
    return
  end
  local persentHP = math.min(100, fleetData.currentHP * 100 / fleetData.maxHP)
  local persentMP = 0
  if fleetData.currentShield ~= 0 then
    persentMP = fleetData.currentShield * 100 / fleetData.maxShield
  end
  local persentAngry = math.min(100, fleetData.currentAcc)
  if persentAngry == 100 and fleetData.currentHP > 0 then
    GameObjectBattleReplay:ShowShipFullEnergy(isLeft, gridPos, fleetData)
  else
    self:GetFlashObject():InvokeASCallback("_root", "hideShipFullEnergyEffect", isLeft, gridPos)
  end
  DebugOut("fleetData.hotHP on hpbar = ")
  DebugTable(fleetData.hotHP)
  if fleetData.isCrit ~= nil then
    local fleetIsCrit = fleetData.isCrit[1]
    local damageHP = 0
    if #fleetData.isCrit > 1 then
      for key = 1, #fleetData.isCrit do
        fleetIsCrit = fleetIsCrit or fleetData.isCrit[key]
        DebugOut(">1", isLeft, gridPos, fleetData.damageShield[key], fleetData.damageHP[key])
        damageHP = fleetData.damageHP[key]
        if damageHP > 0 then
          damageHP = 0
        end
        self:GetFlashObject():InvokeASCallback("_root", "showHurtShipHPBar_N", isLeft, gridPos, fleetData.isCrit[key], fleetData.damageShield[key], persentMP, damageHP, persentHP, persentAngry, key)
        if key == #fleetData.isCrit then
          fleetData.isCrit = {}
          fleetData.damageHP = {}
          fleetData.damageShield = {}
        end
      end
    elseif #fleetData.isCrit == 1 then
      DebugOut("<1:", isLeft, gridPos, fleetData.damageShield[1], fleetData.damageHP[1])
      damageHP = fleetData.damageHP[1]
      if damageHP > 0 then
        damageHP = 0
      end
      self:GetFlashObject():InvokeASCallback("_root", "showHurtShipHPBar", isLeft, gridPos, fleetData.isCrit[1], fleetData.damageShield[1], persentMP, damageHP, persentHP, persentAngry)
      fleetData.damageShield = {}
      fleetData.damageHP = {}
      fleetData.isCrit = {}
    end
    if fleetData.isAbsorted then
      self:GetFlashObject():InvokeASCallback("_root", "playShipStatusAnim", isLeft, gridPos, AttackStatuTypes.ABSORB)
    end
    if fleetIsCrit then
      GameObjectShakeScreen:StartShaking()
    end
    if fleetData.isBlocked then
      self:GetFlashObject():InvokeASCallback("_root", "playShipStatusAnim", isLeft, gridPos, AttackStatuTypes.BLOCK)
    end
  end
  if self.m_currentRoundStep == RoundStep.SP_ATTACK then
    self:GetFlashObject():InvokeASCallback("_root", "showFullHurtAnim", isLeft)
  end
end
function GameObjectBattleReplay:OnBuffInOver(gridPos, isLeft, layer)
  DebugOutBattlePlay("OnBuffInOver", isLeft, gridPos, layer)
  self:CheckRemoveBuff(isLeft, gridPos)
end
function GameObjectBattleReplay:OnBuffEffectOver(gridPos, isLeft)
  DebugOutBattlePlay("OnBuffEffectOver ", isLeft, gridPos)
  if self.m_currentRoundStep == RoundStep.DOT then
    self:OnHurtAnimOver(gridPos, isLeft)
  else
    DebugOutBattlePlay("    OnReactionAnimOver in OnBuffEffectOver")
    self:OnReactionAnimOver(gridPos, isLeft)
  end
end
function GameObjectBattleReplay:OnBuffOutOver(gridPos, isLeft, layer)
  DebugOutBattlePlay("OnBuffOutOver", isLeft, gridPos, layer)
end
function GameObjectBattleReplay:OnMissAnimOver(gridPos, isLeft)
  self:GetFlashObject():InvokeASCallback("_root", "playShipBGEffect", isLeft, gridPos, "normal")
  self:CheckPlayBuff(isLeft, gridPos)
  DebugOutBattlePlay("    OnReactionAnimOver in OnMissAnimOver")
  self:OnReactionAnimOver(gridPos, isLeft)
end
function GameObjectBattleReplay:CheckPlayTransformBackAnim(gridPos, isLeft)
  local fleetData = self.m_fleetInfo.Left[gridPos]
  if not isLeft then
    fleetData = self.m_fleetInfo.Right[gridPos]
  end
  for key, value in pairs(fleetData.buffs) do
    if value.EffectID == 31 then
      DebugOutBattlePlay("CheckPlayTransformBackAnim")
      value.RoundCount = 1
      value.TagRemove = true
      self:CheckPlayBuff(isLeft, gridPos)
    end
  end
end
function GameObjectBattleReplay:CheckZhesheEffect(gridPos, isLeft)
  local roundData = self.m_battleResult.rounds[self.m_currentRoundIndex]
  if not roundData then
    return
  end
  local tarpos = {}
  if self.m_currentRoundStep ~= RoundStep.SP_ATTACK then
    local ta = roundData.attacks
    for _, v in ipairs(ta) do
      if AtkFlag.Check(v.flag, AtkFlag.Refraction) then
        table.insert(tarpos, v.def_pos)
      end
    end
  elseif GameObjectBattleReplay.isMultipleAtk and #MultipleAtkData > 0 then
    spAttackData = MultipleAtkData[GameObjectBattleReplay.curMultipleAtkRound - 1]
    if spAttackData and AtkFlag.Check(spAttackData[1].flag, AtkFlag.Refraction) then
      table.insert(tarpos, spAttackData[1].def_pos)
    end
  else
    local ta = roundData.sp_attacks
    for _, v in ipairs(ta) do
      if AtkFlag.Check(v.flag, AtkFlag.Refraction) then
        table.insert(tarpos, v.def_pos)
      end
    end
  end
  function _check(v, _isleft)
    local fleetDatas = self.m_fleetInfo.Left
    if not _isleft then
      fleetDatas = self.m_fleetInfo.Right
    end
    if fleetDatas[v] == nil or fleetDatas[v].currentHP <= 0 then
      return false, "r1"
    end
    return true
  end
  for _, v in ipairs(tarpos) do
    local ischeck, strr = _check(v, not isLeft)
    if ischeck then
      self:playBuffEffect(not isLeft, v, "buff_refraction")
    else
      print("xxpp zheshe failed", v, strr)
    end
  end
end
function GameObjectBattleReplay:IsFristMultipleAtk()
  preSpAttackData = MultipleAtkData[GameObjectBattleReplay.curMultipleAtkRound - 2]
  curSpAttackData = MultipleAtkData[GameObjectBattleReplay.curMultipleAtkRound - 1]
  return preSpAttackData == nil or curSpAttackData ~= nil and preSpAttackData[1].def_pos ~= curSpAttackData[1].def_pos
end
function GameObjectBattleReplay:OnAttackAnimOver(gridPos, isLeft)
  self:GetFlashObject():InvokeASCallback("_root", "playShipBGEffect", isLeft, gridPos, "normal")
  local fleetData = self.m_fleetInfo.Left[gridPos]
  if not isLeft then
    fleetData = self.m_fleetInfo.Right[gridPos]
  end
  if not fleetData then
    return
  end
  DebugOut("OnAttackAnimOver:", gridPos, isLeft)
  local persentAngry = math.min(100, fleetData.currentAcc)
  if persentAngry == 100 and fleetData.currentHP > 0 and not fleetData.isDead then
    GameObjectBattleReplay:ShowShipFullEnergy(isLeft, gridPos, fleetData)
  else
    self:GetFlashObject():InvokeASCallback("_root", "hideShipFullEnergyEffect", isLeft, gridPos)
  end
  self:GetFlashObject():InvokeASCallback("_root", "showShipAngryBar", isLeft, gridPos, persentAngry)
  self:CheckPlayTransformBackAnim(gridPos, isLeft)
  DebugOutBattlePlay("OnAttackAnimOver ", isLeft)
  self:CheckRemoveBuff(isLeft, gridPos)
  self:CheckFleetsRemoveBuff(not isLeft)
  self:CheckFleetsPlayBuff(isLeft)
  if not GameObjectBattleReplay.isMultipleAtk then
    self:CheckRemoveFullScreenBuff(not isLeft)
    self:CheckZhesheEffect(gridPos, isLeft)
    self:PlayArtifactSPEffect(true, BuffUpdateStep.AfterAttackOver)
    self:UpdateStepBuffs(BuffUpdateStep.AfterAttackOver)
    local haveShootoff = #ShootoffSPData > 0
    local haveRevenge = #RevengeSPData > 0
    self:PlayShootoffSPEffect()
    if haveShootoff and haveRevenge then
    else
      self:PlayRevengeSPEffect()
    end
  end
  if self.m_currentRoundStep == RoundStep.SP_ATTACK and 0 < self.m_currentRoundStepInfo.BuffNameEffectTotalCount then
    self:CheckAllBuffNameEffect()
  elseif self.m_currentRoundStepInfo.BuffNameEffectTotalCount == 0 then
    self:CheckDamageData()
  end
  DebugOutBattlePlay("    OnReactionAnimOver in OnAttackAnimOver")
  self:OnReactionAnimOver(gridPos, isLeft)
end
function GameObjectBattleReplay:UpdateAfterDotDamage()
  DebugOut("UpdateAfterDotDamage")
  local roundData = self.m_battleResult.rounds[self.m_currentRoundIndex]
  local damage_data = self:DamageDataFilter(BattleDamageType.DOT, roundData.damage)
  if not damage_data then
    return
  end
  local RepeatTest = {}
  for k, v in pairs(damage_data) do
    local isLeft = v.side == SIDE_LEFT and self.m_isPlayer1ShowInLeft or v.side == SIDE_RIGHT and not self.m_isPlayer1ShowInLeft
    DebugOut("UpdateAfterDotDamage:", isLeft, v.dur_damage)
    local str = "test" .. tostring(isLeft) .. tostring(v.pos)
    local isRepeatData = self:UpdateFleetHPInfo(isLeft, v.pos, -v.dur_damage, -v.shield_damage, v.shield, false, false, false, false, v.durability, v.index)
    if not isRepeatData and not RepeatTest[str] then
      GameObjectBattleReplay:AddTotalReactionCount()
      RepeatTest[str] = true
    end
    self:OnUpdateHPBar(v.pos, isLeft)
    self:GetFlashObject():InvokeASCallback("_root", "PlayShipDotHurtAnim", isLeft, v.pos)
  end
end
function GameObjectBattleReplay:UpdateAfterAttackDamage(damage_type)
  local roundData = self.m_battleResult.rounds[self.m_currentRoundIndex]
  local damage_data = self:DamageDataFilter(damage_type, roundData.damage)
  if not damage_data then
    return
  end
  for k, v in pairs(damage_data) do
    local isLeft = v.side == SIDE_LEFT and self.m_isPlayer1ShowInLeft or v.side == SIDE_RIGHT and not self.m_isPlayer1ShowInLeft
    self:UpdateFleetHPInfo(isLeft, v.pos, -v.dur_damage, -v.shield_damage, v.shield, false, false, false, false, v.durability, v.index)
    GameObjectBattleReplay:AddTotalReactionCount()
    DebugOut("TotalReactionCount++ in UpdateAfterAttackDamage", self.m_currentRoundStepInfo.TotalReactionCount, isLeft, pos)
    if v.shield > 0 then
      self:PlayShipAnim(isLeft, v.pos, AnimType.SHIELD .. "_center", "gravity", true)
    else
      self:PlayShipAnim(isLeft, v.pos, AnimType.HURT .. "_center", "gravity", true)
    end
  end
end
function GameObjectBattleReplay:UpdateAfterSPAttackDamage()
  local roundData = self.m_battleResult.rounds[self.m_currentRoundIndex]
  local damage_data = self:DamageDataFilter(BattleDamageType.SPELL_ATK, roundData.damage)
  local isLeft
  local NeedUpdateHPBarPos = {}
  if not damage_data then
    return
  end
  for k, v in pairs(damage_data) do
    isLeft = v.side == SIDE_LEFT and self.m_isPlayer1ShowInLeft or v.side == SIDE_RIGHT and not self.m_isPlayer1ShowInLeft
    local isRepeatData = self:UpdateFleetHPInfo(isLeft, v.pos, -v.dur_damage, -v.shield_damage, v.shield, false, false, false, false, v.durability, v.index)
    for _, vd in pairs(roundData.damage) do
      print("UpdateAfterSPAttackDamage_1")
      DebugTable(vd)
      if v.index == vd.index then
        vd.play = true
      end
    end
    if not isRepeatData and not v.isCalculated then
      local fleetDatas = self.m_fleetInfo.Left
      if not isLeft then
        fleetDatas = self.m_fleetInfo.Right
      end
      local fleetData = fleetDatas[v.pos]
      if fleetData.currentHP > 0 and not fleetData.isNeedHurtAnim and not fleetData.isPlayingHurtAnim then
        GameObjectBattleReplay:AddTotalReactionCount()
        DebugOut("TotalReactionCount++ in UpdateAfterSPAttackDamage", self.m_currentRoundStepInfo.TotalReactionCount, isLeft, v.pos)
        DebugOut("play gravity in UpdateAfterSPAttackDamage")
      end
      if v.shield > 0 then
        self:PlayShipAnim(isLeft, v.pos, AnimType.SHIELD .. "_center", "gravity", true)
      else
        self:PlayShipAnim(isLeft, v.pos, AnimType.HURT .. "_center", "gravity", true)
      end
      v.isCalculated = true
    end
    local fleetFlag = v.pos .. "_" .. (isLeft and "1" or "2")
    if not NeedUpdateHPBarPos[fleetFlag] or NeedUpdateHPBarPos[fleetFlag] == nil then
      NeedUpdateHPBarPos[fleetFlag] = {
        pos = v.pos,
        isLeft = isLeft
      }
    end
  end
  DebugOutPutTable(NeedUpdateHPBarPos, "show NeedUpdateHPBarPos11")
  for k, v in pairs(NeedUpdateHPBarPos) do
    self:OnUpdateHPBar(v.pos, v.isLeft)
    if self:GetFleetData(v.isLeft, v.pos) and self:GetFleetData(v.isLeft, v.pos).isHurtAnimOver and 0 >= self:GetFleetData(v.isLeft, v.pos).currentHP and not self:GetFleetData(v.isLeft, v.pos).isRevengeAtkOver then
      self:OnShipDead(v.isLeft, v.pos)
    end
  end
end
function GameObjectBattleReplay:GetFleetData(isLeft, pos)
  local fleetDatas = self.m_fleetInfo.Left
  if not isLeft then
    fleetDatas = self.m_fleetInfo.Right
  end
  local fleetData = fleetDatas[pos]
  return fleetData
end
function GameObjectBattleReplay:UpdateAfterAdjutantSPAttackDamage()
  local roundData = self.m_battleResult.rounds[self.m_currentRoundIndex]
  local damage_data = self:DamageDataFilter(BattleDamageType.ADJUTANT, roundData.damage)
  if not damage_data then
    return
  end
  for k, v in pairs(damage_data) do
    local isLeft = v.side == SIDE_LEFT and self.m_isPlayer1ShowInLeft or v.side == SIDE_RIGHT and not self.m_isPlayer1ShowInLeft
    local isRepeatData = self:UpdateFleetHPInfo(isLeft, v.pos, -v.dur_damage, -v.shield_damage, v.shield, false, false, false, false, v.durability, v.index)
    if not isRepeatData then
      GameObjectBattleReplay:AddTotalReactionCount()
      DebugOut("TotalReactionCount++ in UpdateAfterAdjutantSPAttackDamage", self.m_currentRoundStepInfo.TotalReactionCount, isLeft, v.pos)
    end
    if not self.AdjutantHurtAnimData then
      self.AdjutantHurtAnimData = {}
    end
    self.AdjutantHurtAnimData[#self.AdjutantHurtAnimData + 1] = {
      isLeftShip = isLeft,
      gridIndex = v.pos,
      effectName = "_hurt"
    }
  end
end
function GameObjectBattleReplay:DamageDataFilter(damage_type, damage_data, damage_from)
  if not damage_type or not damage_data then
    return nil
  end
  local roundData = self.m_battleResult.rounds[self.m_currentRoundIndex]
  local attackIndex = -1
  local dspAttackData = MultipleAtkData[GameObjectBattleReplay.curMultipleAtkRound - 1]
  if damage_type == BattleDamageType.SPELL_ATK and GameObjectBattleReplay.isMultipleAtk and dspAttackData then
    attackIndex = dspAttackData[1].index
  end
  local result = {}
  for k, v in pairs(damage_data) do
    if v.type == damage_type and (not damage_from or damage_from == v.atk_from) and (attackIndex == -1 or attackIndex > v.index) and not v.play then
      table.insert(result, v)
    end
  end
  DebugOutBattlePlay("DamageDataFilter:", damage_type)
  DebugOutBattlePlayTable(damage_data)
  DebugOutBattlePlayTable(result)
  return result
end
function GameObjectBattleReplay:DoDamageDataMergeHook(damage_type, damage_data)
  local damageTable = {}
  for i = 1, 9 do
    damageTable[i] = {}
  end
  for i, v in ipairs(damage_data) do
    if v.type == damage_type and v.atk_from ~= 0 then
      local dIndex = v.pos
      table.insert(damageTable[dIndex], v)
    end
  end
  local finalTable = {}
  for i, v in ipairs(damageTable) do
    if #v > 0 then
      local tmpDamage = {}
      tmpDamage = v[1]
      DebugOut("tmpDamage = ")
      DebugTable(tmpDamage)
      for i1 = 2, #v do
        DebugOut("v[i1] = ")
        DebugTable(v[i1])
        tmpDamage.durability = math.min(tmpDamage.durability, v[i1].durability)
        tmpDamage.shield = math.min(tmpDamage.shield, v[i1].shield)
        tmpDamage.shield_damage = v[i1].shield_damage + tmpDamage.shield_damage
        tmpDamage.dur_damage = v[i1].dur_damage + tmpDamage.dur_damage
      end
      DebugOut("tmpDamage end = ")
      DebugTable(tmpDamage)
      table.insert(finalTable, tmpDamage)
    end
  end
  DebugOut("DoDamageDataMergeHook result")
  DebugTable(damage_data)
  DebugTable(finalTable)
  return finalTable
end
function GameObjectBattleReplay:UpdateDamageData(damage_type, gridPos)
  DebugOutBattlePlay("UpdateDamageData:", damage_type)
  if damage_type == BattleDamageType.DOT then
    self:UpdateAfterDotDamage()
  elseif damage_type == BattleDamageType.NORMAL_ATK then
    self:UpdateAfterAttackDamage(damage_type)
  elseif damage_type == BattleDamageType.BLOCK then
    self:UpdateAfterAttackDamage(damage_type)
  elseif damage_type == BattleDamageType.ADJUTANT then
    self:UpdateAfterAdjutantSPAttackDamage()
  elseif damage_type == BattleDamageType.SPELL_ATK then
    self:UpdateAfterSPAttackDamage()
  elseif damage_type == BattleDamageType.AFTER_DEAD then
    self:UpdateAfterDeadDamage(gridPos)
  end
end
function GameObjectBattleReplay:UpdateAfterDeadDamage(gridPos)
  DebugOut("UpdateAfterDeadDamage", gridPos)
  local roundData = self.m_battleResult.rounds[self.m_currentRoundIndex]
  local mergedDamage = GameObjectBattleReplay:DoDamageDataMergeHook(BattleDamageType.AFTER_DEAD, roundData.damage)
  local damage_data = self:DamageDataFilter(BattleDamageType.AFTER_DEAD, mergedDamage, gridPos)
  DebugTable(damage_data)
  if not damage_data then
    return
  end
  for k, v in pairs(damage_data) do
    local isLeft = v.side == SIDE_LEFT and self.m_isPlayer1ShowInLeft or v.side == SIDE_RIGHT and not self.m_isPlayer1ShowInLeft
    self:ClearFleetDamageDataByGrid(isLeft, v.pos)
    local isRepeatData = self:UpdateFleetHPInfo(isLeft, v.pos, -v.dur_damage, -v.shield_damage, v.shield, false, false, false, false, v.durability, v.index)
    DebugOut("isRepeatData:", isRepeatData)
    if not isRepeatData then
      GameObjectBattleReplay:AddTotalReactionCount()
      DebugOut("TotalReactionCount++ in UpdateAfterDeadDamage", self.m_currentRoundStepInfo.TotalReactionCount, isLeft, v.pos)
    end
    DebugOut("play gravity in UpdateAfterDeadDamage")
    if v.shield > 0 then
      self:PlayShipAnim(isLeft, v.pos, AnimType.SHIELD .. "_center", "gravity", true)
    else
      self:PlayShipAnim(isLeft, v.pos, AnimType.HURT .. "_center", "gravity", true)
    end
  end
end
function GameObjectBattleReplay:OnDeadHurtAnimOver(gridPos, isLeft)
  if self:CheckNeedUpdateDeadDamage(gridPos, isLeft) then
    self:UpdateDamageData(BattleDamageType.AFTER_DEAD, gridPos)
  end
end
function GameObjectBattleReplay:OnDeadAnimOver(gridPos, isLeft)
  GameObjectBattleReplay:setShipInGrid(isLeft, gridPos, 0, 0, 0, "empty", "empty")
  DebugOutBattlePlay("    OnReactionAnimOver in OnDeadAnimOver")
  self:OnReactionAnimOver(gridPos, isLeft)
end
function GameObjectBattleReplay:CheckNeedUpdateDeadDamage(gridPos, isLeft)
  local fleetDatas = self.m_fleetInfo.Left
  if not isLeft then
    fleetDatas = self.m_fleetInfo.Right
  end
  local fleetData = fleetDatas[gridPos]
  if not fleetData then
    return
  end
  for i, v in ipairs(fleetData.buffs) do
    if v.EffectID == 44 then
      return true
    end
  end
  return false
end
function GameObjectBattleReplay:OnAdjutantAnimOver(gridPos, isLeft)
  DebugOutBattlePlay("OnAdjutantAnimOver")
  for i, v in ipairs(self.AdjutantHurtAnimData) do
    self:PlayShipBGAnim(v.isLeftShip, v.gridIndex, v.effectName)
  end
  self:CheckAllShipPlayBuff()
  self:CheckFleetsRemoveBuff(not isLeft)
  self:OnReactionAnimOver(gridPos, isLeft)
end
function GameObjectBattleReplay:OnBuffNameEffectAnimOver(gridPos, isLeft)
  self.m_currentRoundStepInfo.BuffNameEffectCount = self.m_currentRoundStepInfo.BuffNameEffectCount + 1
  DebugOut("GameObjectBattleReplay:OnBuffNameEffectAnimOver ", self.m_currentRoundStepInfo.BuffNameEffectTotalCount, self.m_currentRoundStepInfo.BuffNameEffectCount)
  DebugOut("qingjie Xx pos:", gridPos, isLeft)
  if self.m_currentRoundStepInfo.BuffNameEffectTotalCount <= self.m_currentRoundStepInfo.BuffNameEffectCount then
    if GameObjectBattleReplay:IsAllFleetBuffNameAnimCheckedOver() == false then
      return
    end
    DebugOut("111")
    self:CheckDamageData()
    self:OnReactionAnimOver(gridPos, isLeft, true)
  end
end
function GameObjectBattleReplay:CheckDamageData()
  local roundData = self.m_battleResult.rounds[self.m_currentRoundIndex]
  if roundData and roundData.attacks and #roundData.attacks > 0 then
    local damage_type = BattleDamageType.NORMAL_ATK
    if self.m_currentRoundStepInfo.AttackRoundIndex > 1 then
      damage_type = BattleDamageType.BLOCK
    end
    self:UpdateDamageData(damage_type)
  elseif roundData then
    local isPlayer1Action = roundData.player1_action
    local spAttackData = {}
    for k, v in pairs(roundData.sp_attacks) do
      DebugTable(v)
      if v.atk_side and v.atk_side ~= "" then
        if v.adjutant ~= 0 then
        elseif not self:IsAdjutantAttack(v.atk_side == "p1", v.adjutant) and (v.artifact == nil or v.artifact == 0) then
          spAttackData[#spAttackData + 1] = v
        end
      elseif (v.adjutant == 0 or not self:IsAdjutantAttack(isPlayer1Action, v.adjutant)) and (v.artifact == nil or v.artifact == 0) then
        spAttackData[#spAttackData + 1] = v
      end
    end
    DebugOut("444")
    DebugTable(spAttackData)
    local damage_data = self:DamageDataFilter(BattleDamageType.SPELL_ATK, roundData.damage)
    if damage_data and #damage_data > 0 then
      self:UpdateDamageData(BattleDamageType.SPELL_ATK)
    end
    if spAttackData and #spAttackData > 0 then
      local isPlayer1Action = roundData.player1_action
      local isLeftActcion = isPlayer1Action and self.m_isPlayer1ShowInLeft or not isPlayer1Action and not self.m_isPlayer1ShowInLeft
      for k, v in pairs(spAttackData) do
        local isLeft = isLeftActcion == v.self_cast
        if v.atk_side and v.atk_side ~= "" then
          local atk_side_p1 = v.atk_side == "p1"
          isLeft = v.self_cast == (atk_side_p1 and self.m_isPlayer1ShowInLeft or not atk_side_p1 and not self.m_isPlayer1ShowInLeft)
        end
        if (0 < v.dur_damage or 0 < v.shield_damage) and not self:IsPosInDamageData(v.def_pos, isLeft, damage_data) and v.atk_type == AtkType.Normal then
          self:OnUpdateHPBar(v.def_pos, isLeft)
        end
      end
    end
  end
end
function GameObjectBattleReplay:IsPosInDamageData(pos, isLeft, damage_data)
  for k, v in pairs(damage_data) do
    local isDamageDataLeft = self.m_isPlayer1ShowInLeft and v.side == "p1" or not self.m_isPlayer1ShowInLeft and v.side == "p2"
    if v.pos == pos and isLeft == isDamageDataLeft and (v.dur_damage > 0 or 0 < v.shield_damage) then
      return true
    end
  end
  return false
end
function GameObjectBattleReplay:IsAllFleetBuffNameAnimCheckedOver()
  local fleetPlayNameAnimCnt = 0
  local fleetNameAnimCheckedCnt = 0
  if self.m_currentRoundStepInfo.NameAnimFleetInfo then
    fleetPlayNameAnimCnt = #self.m_currentRoundStepInfo.NameAnimFleetInfo
    for k, v in ipairs(self.m_currentRoundStepInfo.NameAnimFleetInfo) do
      if v.isCheckedOver == true then
        fleetNameAnimCheckedCnt = fleetNameAnimCheckedCnt + 1
      end
    end
  end
  DebugOut("OnBuffNameEffectAnimOver adsfjkl", fleetNameAnimCheckedCnt, fleetPlayNameAnimCnt)
  DebugTable(self.m_currentRoundStepInfo.NameAnimFleetInfo)
  if fleetPlayNameAnimCnt > fleetNameAnimCheckedCnt then
    return false
  end
  return true
end
function GameObjectBattleReplay:OnReactionAnimOver(gridPos, isLeft, isNotAddRectionCount)
  if not isNotAddRectionCount then
    self.m_currentRoundStepInfo.RectionCount = self.m_currentRoundStepInfo.RectionCount + 1
  end
  DebugOutBattlePlay("OnReactionAnimOver ", self.m_currentRoundStepInfo.TotalReactionCount, self.m_currentRoundStepInfo.RectionCount)
  DebugOutBattlePlay("OnReactionAnimOver2 ", self.m_currentRoundStepInfo.BuffNameEffectCount, self.m_currentRoundStepInfo.BuffNameEffectTotalCount)
  if self.m_currentRoundStepInfo.RectionCount < self.m_currentRoundStepInfo.TotalReactionCount or self.m_currentRoundStepInfo.BuffNameEffectCount < self.m_currentRoundStepInfo.BuffNameEffectTotalCount then
    return
  end
  if GameObjectBattleReplay:IsAllFleetBuffNameAnimCheckedOver() == false then
    return
  end
  if self.m_currentRoundStep == RoundStep.SUM_FLEET then
    DebugOut("OnReactionAnimOver:SUM_FLEET")
    self:PlayRoundStepEffect(RoundStep.DOT)
  elseif self.m_currentRoundStep == RoundStep.DOT then
    if not self:CheckRemoveBuff(isLeft, gridPos) then
      self:PlayRoundStepEffect(RoundStep.BUFF)
    end
  elseif self.m_currentRoundStep == RoundStep.BUFF then
    assert(0)
  elseif self.m_currentRoundStep == RoundStep.ADJUTANT then
    DebugOutBattlePlay("OnReactionAnimOver:Adjutant:PlayRoundStepEffect")
    self:PlayRoundStepEffect(RoundStep.ARTIFACT)
  elseif self.m_currentRoundStep == RoundStep.ARTIFACT then
    DebugOutBattlePlay("OnReactionAnimOver:Adjutant:PlayRoundStepEffect")
    self:PlayRoundStepEffect(RoundStep.ATTACK)
  elseif self.m_currentRoundStep == RoundStep.ATTACK then
    self:PlayNextAttackEffect()
  elseif self.m_currentRoundStep == RoundStep.SP_ATTACK then
    if GameObjectBattleReplay.isMultipleAtk then
      self:PlaySPAttack()
    else
      self:PlayRoundStepEffect(RoundStep.RELIVE)
    end
  elseif self.m_currentRoundStep == RoundStep.RELIVE then
    self.m_JumpToNextRoundTimer = ROUND_INTERVAL_TIMER
    DebugOutBattlePlay("OnReactionAnimOver PlaySPAttackEffect ", self.m_JumpToNextRoundTimer)
  else
    assert(0)
  end
end
function GameObjectBattleReplay:OnDamageReduceEffect(isLeft, gridPos)
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "playShipReduceDamageEffect", isLeft, gridPos)
  end
end
function GameObjectBattleReplay:ShowShipAppear(isLeft, gridPos)
  self:GetFlashObject():InvokeASCallback("_root", "StartShipAppearEffect", isLeft, gridPos)
end
function GameObjectBattleReplay:HideShipWithAlpha(isLeft, gridPos)
  self:GetFlashObject():InvokeASCallback("_root", "HideShipWithAlpha", isLeft, gridPos)
end
function GameObjectBattleReplay:SetShipDisappearWithAlpha(isLeft, gridPos)
  self:GetFlashObject():InvokeASCallback("_root", "SetShipDisappearWithAlpha", isLeft, gridPos)
end
function GameObjectBattleReplay:HideBattleSpeed()
  self:GetFlashObject():InvokeASCallback("_root", "HideBattleSpeed")
end
function GameObjectBattleReplay:DisplayBattleSpeed()
  local showSpeedTutorial = false
  if not QuestTutorialGameSpeed:IsFinished() then
    QuestTutorialGameSpeed:SetActive(true)
    showSpeedTutorial = true
  end
  local showAnim = false
  if immanentversion == 2 and QuestTutorialGameSpeed:IsActive() and self._activeArea == 60 and (self._activeBattle == 1003 or self._activeBattle == 1005 or self._activeBattle == 1006) then
    showAnim = true
  end
  DebugOut("fuck_DisplayBattleSpeed", self._activeBattle, showAnim, GameUtils:GetBattleSpeed())
  local speedShow = 1
  if GameUtils:GetBattleSpeed() <= GameObjectBattleReplay.BASE_BATTLE_SPEED then
    speedShow = 1
  else
    speedShow = 2
  end
  self:GetFlashObject():InvokeASCallback("_root", "setBattleSpeed", speedShow, showSpeedTutorial, showAnim)
end
function GameObjectBattleReplay:OnFSCommand(cmd, args)
  if self:GetFlashObject() == nil or not GameStateManager:GetCurrentGameState():IsObjectInState(self) then
    return
  end
  DebugOutBattlePlay("GameObjectBattleReplay:OnFSCommand: " .. cmd)
  if cmd == "EraseBattleReplay" then
    DebugOutBattlePlay("EraseBattleReplay: ", self._activeArea, self._activeBattle)
    if self._activeArea == 60 and self._activeBattle == 1013 then
      GameStateManager:SetCurrentGameState(GameStateManager.GameStateEmptyState)
      TutorialQuestManager.QuestTutorialCityHall:SetActive(true)
      GameObjectTutorialCutscene:ShowBGAnimAfterBattle()
      AddFlurryEvent("FinishBattle_60001013", {1}, 2)
    elseif GameStateBattlePlay._battleOverCallback then
      GameStateBattlePlay:ExecuteOverCallback()
    else
      GameStateManager:SetCurrentGameState(GameStateBattlePlay.m_preState)
    end
  elseif cmd == "StartReplay" then
    local fakeBattleID = TutorialQuestManager:GetTurialFakeBattleID()
    if fakeBattleID then
      ext.SetAppSpeedScale(GameObjectBattleReplay.UP_BATTLE_SPEED)
    else
      ext.SetAppSpeedScale(GameUtils:GetBattleSpeed())
    end
    if self.m_showRightShipEnter then
      self:PlayShipsEnter()
    else
      self:PlayBattleResult()
    end
    if ENABLE_BATTLE_MULTIPLE_UPDATE then
      self:GetFlashObject():EnableMultipleUpdate(true)
    end
  elseif cmd == "attack_over" then
    local argParam = LuaUtils:string_split(args, "^")
    local isLeft = tonumber(argParam[2]) == 1
    local pos = tonumber(argParam[1])
    if self.mChainAnimNumber and self.mChainAnimNumber > 0 then
      GameObjectBattleReplay:RecordAttackAnimOverData(pos, isLeft)
    else
      self:OnAttackAnimOver(pos, isLeft)
    end
  elseif cmd == "artifact_attack_over" then
    local argParam = LuaUtils:string_split(args, "^")
    local isLeft = tonumber(argParam[2]) == 1
    local pos = tonumber(argParam[1])
    self:GetFleetData(isLeft, pos).isPlayArtifactAnim = false
    if GameObjectBattleReplay.PlayingMultiEffectPos == pos and GameObjectBattleReplay.PlayingMultiEffectSide == isLeft then
      GameObjectBattleReplay.isPlayingMultiEffect = false
    end
    self:PlayArtifactSPEffect(true, self:GetFleetData(isLeft, pos).artifactStep)
    self:OnReactionAnimOver(pos, isLeft)
  elseif cmd == "hurt_over" or cmd == "shield_over" or cmd == "block_over" then
    local argParam = LuaUtils:string_split(args, "^")
    local isLeft = tonumber(argParam[2]) == 1
    local pos = tonumber(argParam[1])
    self:OnHurtAnimOver(pos, isLeft)
  elseif cmd == "dead_over" then
    local argParam = LuaUtils:string_split(args, "^")
    local isLeft = tonumber(argParam[2]) == 1
    local pos = tonumber(argParam[1])
    self:OnDeadAnimOver(pos, isLeft)
  elseif cmd == "dead_over_2" then
    local argParam = LuaUtils:string_split(args, "^")
    local isLeft = tonumber(argParam[2]) == 1
    local pos = tonumber(argParam[1])
    self:OnDeadHurtAnimOver(pos, isLeft)
  elseif cmd == "adjutant_over" then
    local argParam = LuaUtils:string_split(args, "^")
    local isLeft = tonumber(argParam[2]) == 1
    local pos = tonumber(argParam[1])
    self:OnAdjutantAnimOver(pos, isLeft)
  elseif cmd == "miss_over" then
    local argParam = LuaUtils:string_split(args, "^")
    local isLeft = tonumber(argParam[2]) == 1
    local pos = tonumber(argParam[1])
    self:OnMissAnimOver(pos, isLeft)
  elseif cmd == "buff_in_over" then
    local argParam = LuaUtils:string_split(args, "^")
    local pos = tonumber(argParam[1])
    local isLeft = tonumber(argParam[2]) == 1
    local layer = tonumber(argParam[3])
    self:OnBuffInOver(pos, isLeft, layer)
  elseif cmd == "buff_out_over" then
    local argParam = LuaUtils:string_split(args, "^")
    local pos = tonumber(argParam[1])
    local isLeft = tonumber(argParam[2]) == 1
    local layer = tonumber(argParam[3])
    self:OnBuffOutOver(pos, isLeft, layer)
  elseif cmd == "buff_effect_over" then
    local argParam = LuaUtils:string_split(args, "^")
    local pos = tonumber(argParam[1])
    local isLeft = tonumber(argParam[2]) == 1
  elseif cmd == "chainAnimFinish" then
    DebugOut("self.mChainAnimNumber = ", self.mChainAnimNumber)
    self.mChainAnimNumber = self.mChainAnimNumber - 1
    if self.mChainAnimNumber <= 0 then
      self.mChainAnimNumber = 0
      DebugOut("PlayRecordedShipSPAnim chainAnimFinish when")
      self:ProcessRecordedAttackOver()
      self:PlayRecordedShipSPAnim()
      self:OnAttackAnimOver(pos, isLeft)
    end
  elseif cmd == "updateHPbar" then
    local argParam = LuaUtils:string_split(args, "^")
    local isLeft = tonumber(argParam[2]) == 1
    local pos = tonumber(argParam[1])
    local checkrevenge = RevengeDefender.isLeft == isLeft and RevengeDefender.pos == pos and RevengeDefender.isPlayedHurAnim
    if self.m_currentRoundStep ~= RoundStep.SP_ATTACK or checkrevenge or self.m_currentRoundStepInfo.BuffNameEffectTotalCount <= self.m_currentRoundStepInfo.BuffNameEffectCount then
      self:OnUpdateHPBar(pos, isLeft)
      self:OnDamageReduceEffect(isLeft, pos)
    end
  elseif cmd == "battle_speed_up" then
    if GameUtils:GetBattleSpeed() == GameObjectBattleReplay.BASE_BATTLE_SPEED then
      GameUtils:SetBattleSpeed(GameObjectBattleReplay.UP_BATTLE_SPEED)
      ext.SetAppSpeedScale(GameObjectBattleReplay.UP_BATTLE_SPEED)
    else
      GameUtils:SetBattleSpeed(GameObjectBattleReplay.BASE_BATTLE_SPEED)
      ext.SetAppSpeedScale(GameObjectBattleReplay.BASE_BATTLE_SPEED)
    end
    if QuestTutorialGameSpeed:IsActive() then
      QuestTutorialGameSpeed:SetFinish(true)
    end
    self:DisplayBattleSpeed()
  elseif cmd == "battle_skip" then
    if QuestTutorialBattleFailed:IsActive() then
      local function callback()
        GameObjectBattleReplay:SavaBattleProgress(2, 0)
        self:OnReplayOver()
      end
      GameUICommonDialog:PlayStory({1100026}, callback)
    else
      GameObjectBattleReplay:SavaBattleProgress(2, 0)
      self:OnReplayOver()
    end
  elseif cmd == "EnterBattleFieldOver" then
    self:EndShipEnterBattleField()
  elseif cmd == "TransformShip" then
    self:TransformShip()
  elseif cmd == "TransformShipBack" then
    self:TransformShipBack()
  elseif cmd == "christmasTrans" then
    self:ChristmasTrans(args, GameDataAccessHelper:GetFleetAvatar(152, 1))
  elseif cmd == "christmasTransBack" then
    self:ChristmasTrans(args, GameDataAccessHelper:GetFleetAvatar(151, 1))
  elseif cmd == "buffName_Anim_Over" then
    DebugOut(" buffName_Anim_Over ", args)
    if self.isBegainBuffNameEffect then
      local argParam = LuaUtils:string_split(args, "^")
      local isLeft = tonumber(argParam[2]) == 1
      local pos = tonumber(argParam[1])
      self:CheckBuffNameEffect(isLeft, pos)
      self:OnBuffNameEffectAnimOver(pos, isLeft)
    else
      DebugOut("buffName_Anim_Over  isBegainBuffNameEffect is false")
    end
  elseif cmd == "buffEffectOver_scout" then
    DebugOut(" buffEffectOver_scout ", args)
    local argParam = LuaUtils:string_split(args, "^")
    local isLeft = tonumber(argParam[2]) == 1
    local pos = tonumber(argParam[1])
    self:playBuffEffect(isLeft, pos, "buff_AntiInvisible")
  elseif cmd == "buffEffectOver_AntiInvisible" then
    DebugOut(" buffEffectOver_AntiInvisible ", args)
    local argParam = LuaUtils:string_split(args, "^")
    local isLeft = tonumber(argParam[2]) == 1
    local pos = tonumber(argParam[1])
    self:AntiInvisibleAnimCallback(isLeft, pos)
  elseif cmd == "buffEffectOver_DamageReturn" then
    DebugOut("buffEffectOver_DamageReturn", args)
    local argParam = LuaUtils:string_split(args, "^")
    local isLeft = tonumber(argParam[2]) == 1
    local pos = tonumber(argParam[1])
    self:OnDamageReturnAttackOver(isLeft, pos)
  elseif cmd == "buffEffectOver_Shootoff" then
    DebugOut("buffEffectOver_Shootoff", args)
    local argParam = LuaUtils:string_split(args, "^")
    local isLeft = tonumber(argParam[2]) == 1
    local pos = tonumber(argParam[1])
    self:OnShootoffAttackOver(isLeft, pos)
  elseif cmd == "buff_resurgence_over" and 0 < #GameObjectBattleReplay.needReliveFleet then
    self:OnReactionAnimOver()
  end
end
function GameObjectBattleReplay:ChristmasTrans(args, transHead)
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "christmasTrans", args, transHead)
  end
end
function GameObjectBattleReplay:ChristmasTransBack(args, transHead)
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "christmasTransBack", args, transHead)
  end
end
function GameObjectBattleReplay:SavaBattleProgress(state, isSaveReport, ext_info)
  local info = {
    device_name = ext.GetDeviceName(),
    device_real_name = ext.GetDeviceName(),
    open_udid = ext.GetIOSOpenUdid(),
    report_id = GameStateBattlePlay.report_id or "",
    battle_system = GameStateBattlePlay.curBattleType or "",
    played_round_count = self.m_currentRoundIndex or 0,
    state = state,
    save_report = isSaveReport,
    ext_info = ext_info or {}
  }
  DebugOut("SavaBattleProgress")
  DebugTable(info)
  NetMessageMgr:SendMsg(NetAPIList.user_statistics_req.Code, info, nil, false, nil)
end
if AutoUpdate.isAndroidDevice then
  function GameObjectBattleReplay.OnAndroidBack()
  end
end
