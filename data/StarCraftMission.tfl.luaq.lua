local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
StarCraftMission = luaClass(nil)
local GameUIStarCraftMap = LuaObjectManager:GetLuaObject("GameUIStarCraftMap")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
function StarCraftMission:ctor(Id, type, action, currentNum, targetNum, step, targetDot, awards, isAllReceived)
  self.mID = Id
  self.mType = type
  self.mAction = action
  self.mCurrent = currentNum
  self.mTarget = targetNum
  self.mStep = step
  self.mTargetPointList = targetDot
  self.mAwardsList = awards
  self.mIsAllReceived = isAllReceived
end
function StarCraftMission:GetMissionDescStr()
  local descLocId = "LC_MENU_TC_MISSION_" .. string.upper(self.mType) .. "_" .. string.upper(self.mAction)
  local desc = GameLoader:GetGameText(descLocId)
  if self.mAction == "atk" or self.mAction == "def" then
    local pointName = GameUIStarCraftMap:GetKeyPointName(self.mTargetPointList[1])
    desc = string.gsub(desc, "<point>", pointName)
    return desc
  else
    return ...
  end
end
function StarCraftMission:GetMissionTargetPoint()
  if self.mAction == "atk" or self.mAction == "def" then
    return self.mTargetPointList
  end
  return nil
end
function StarCraftMission:GetMissionStateStr()
  return self.mCurrent .. "/" .. self.mTarget
end
function StarCraftMission:GetCurrentStep()
  return self.mStep
end
function StarCraftMission:GetAwardIcon(awardIndex)
  if awardIndex > #self.mAwardsList then
    assert(false)
  end
  return ...
end
function StarCraftMission:GetAwardNum(awardIndex)
  if awardIndex > #self.mAwardsList then
    assert(false)
  end
  local item = self.mAwardsList[awardIndex]
  return ...
end
function StarCraftMission:CanGetAward()
  if self.mCurrent >= self.mTarget then
    return true
  end
  return false
end
function StarCraftMission:GetAwardReceiveFrame()
  if self.mIsAllReceived == 1 then
    return "blue"
  elseif self.mCurrent < self.mTarget then
    return "red"
  else
    return "orange"
  end
  return "red"
end
StarCraftMissionTable = {}
StarCraftMissionTable.mMissions = {}
StarCraftMissionTable.mCurrentReceiveMissionID = nil
function StarCraftMissionTable:GetMissionByID(missionID)
  return StarCraftMissionTable.mMissions[missionID]
end
function StarCraftMissionTable:SetMissionStateFetchListner(listner, extInfo)
  StarCraftMissionTable.mMissionStateFetchListner = listner
  StarCraftMissionTable.mExtInfo = extInfo
end
function StarCraftMissionTable:RequestMissionDetails(eventID, MissionIDs, waiting)
  DebugOut("before ", eventID, MissionIDs)
  local content = {event_id = eventID, id_list = MissionIDs}
  DebugOut("request")
  DebugTable(content)
  NetMessageMgr:SendMsg(NetAPIList.contention_mission_info_req.Code, content, StarCraftMissionTable.RequestMissionDetailsCallback, waiting, nil)
end
function StarCraftMissionTable.RequestMissionDetailsCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.contention_mission_info_req.Code then
    if content.code ~= 0 then
      local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    if StarCraftMissionTable.mMissionStateFetchListner then
      StarCraftMissionTable.mMissionStateFetchListner:FoeceCheckEventFinished(StarCraftMissionTable.mExtInfo)
    end
    return true
  elseif msgType == NetAPIList.contention_mission_info_ack.Code then
    for i, v in ipairs(content.mission_list) do
      local missionDetail = StarCraftMission.new(v.id, v.type, v.action, v.num, v.max, v.step, v.dot_list, v.awards, v.is_end_award)
      StarCraftMissionTable.mMissions[v.id] = missionDetail
    end
    if StarCraftMissionTable.mMissionStateFetchListner then
      StarCraftMissionTable.mMissionStateFetchListner:FoeceCheckEventFinished(StarCraftMissionTable.mExtInfo)
    end
    return true
  end
  return false
end
function StarCraftMissionTable:TryReceiveMissionAwards(eventID, missionID, missionStep)
  local content = {
    event_id = eventID,
    mission_id = missionID,
    step = missionStep
  }
  NetMessageMgr:SendMsg(NetAPIList.contention_mission_award_req.Code, content, StarCraftMissionTable.TryReceiveMissionAwardsCallback, true, nil)
end
function StarCraftMissionTable.TryReceiveMissionAwardsCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.contention_mission_award_req.Code then
    if content.code ~= 0 then
      local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      if StarCraftMissionTable.mMissionStateFetchListner then
        StarCraftMissionTable.mExtInfo.error = true
        StarCraftMissionTable.mMissionStateFetchListner:FoeceCheckEventFinished(StarCraftMissionTable.mExtInfo)
      end
    else
      if StarCraftMissionTable.mMissionStateFetchListner then
        StarCraftMissionTable.mMissionStateFetchListner:FoeceCheckEventFinished(StarCraftMissionTable.mExtInfo)
      end
      local msg = GameLoader:GetGameText("LC_MENU_CLAIMED_BUTTON")
      GameTip:Show(msg)
    end
    return true
  end
  return false
end
