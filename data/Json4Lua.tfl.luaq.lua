local math = require("math")
local string = require("string")
local table = require("table")
local tostring = tostring
local base = _G
module("tjson")
local decode_scanArray, decode_scanComment, decode_scanConstant, decode_scanNumber, decode_scanObject, decode_scanString, decode_scanWhitespace, encodeString, isArray, isEncodable
function encode(v)
  if v == nil then
    return "null"
  end
  local vtype = base.type(v)
  if vtype == "string" then
    return "\"" .. encodeString(v) .. "\""
  end
  if vtype == "number" or vtype == "boolean" then
    return (...), v, "\"", nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil
  end
  if vtype == "table" then
    local rval = {}
    local bArray, maxCount = isArray(v)
    if bArray then
      for i = 1, maxCount do
        table.insert(rval, encode(v[i]))
      end
    else
      for i, j in base.pairs(v) do
        if isEncodable(i) and isEncodable(j) then
          table.insert(rval, "\"" .. encodeString(i) .. "\":" .. encode(j))
        end
      end
    end
    if bArray then
      return "[" .. table.concat(rval, ",") .. "]"
    else
      return "{" .. table.concat(rval, ",") .. "}"
    end
  end
  if vtype == "function" and v == null then
    return "null"
  end
  base.assert(false, "encode attempt to encode unsupported type " .. vtype .. ":" .. base.tostring(v))
end
function decode(s)
  return null
end
function null()
  return null
end
local qrep = {
  ["\\"] = "\\\\",
  ["\""] = "\\\"",
  ["\n"] = "\\n",
  ["\t"] = "\\t"
}
function encodeString(s)
  return ...
end
function isArray(t)
  local maxIndex = 0
  for k, v in base.pairs(t) do
    if base.type(k) == "number" and math.floor(k) == k and k >= 1 then
      if not isEncodable(v) then
        return false
      end
      maxIndex = math.max(maxIndex, k)
    elseif k == "n" then
      if v ~= table.getn(t) then
        return false
      end
    elseif isEncodable(v) then
      return false
    end
  end
  return true, maxIndex
end
function isEncodable(o)
  local t = base.type(o)
  return t == "string" or t == "boolean" or t == "number" or t == "nil" or t == "table" or t == "function" and o == null
end
do
  local type = base.type
  local error = base.error
  local assert = base.assert
  local fprint = base.fprint
  local tonumber = base.tonumber
  local init_token_table = function(tt)
    local struct = {}
    local value
    function struct:link(other_tt)
      value = other_tt
      return struct
    end
    function struct:to(chars)
      for i = 1, #chars do
        tt[chars:byte(i)] = value
      end
      return struct
    end
    return function(name)
      tt.name = name
      return struct
    end
  end
  local c_esc, c_e, c_l, c_r, c_u, c_f, c_a, c_s, c_slash = ("\\elrufas/"):byte(1, 9)
  local tt_object_key, tt_object_colon, tt_object_value, tt_doublequote_string, tt_singlequote_string, tt_array_value, tt_array_seperator, tt_numeric, tt_boolean, tt_null, tt_comment_start, tt_comment_middle, tt_ignore = {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}
  local strchars = ""
  local allchars = ""
  for i = 0, 255 do
    local c = string.char(i)
    if c ~= "\n" and c ~= "\r" then
      strchars = strchars .. c
    end
    allchars = allchars .. c
  end
  init_token_table(tt_object_key)("object (' or \" or } or , expected)"):link(tt_singlequote_string):to("'"):link(tt_doublequote_string):to("\""):link(true):to("}"):link(tt_object_key):to(","):link(tt_comment_start):to("/"):link(tt_ignore):to(" \t\r\n")
  init_token_table(tt_object_colon)("object (: expected)"):link(tt_object_value):to(":"):link(tt_comment_start):to("/"):link(tt_ignore):to(" \t\r\n")
  init_token_table(tt_object_value)("object ({ or [ or ' or \" or number or boolean or null expected)"):link(tt_object_key):to("{"):link(tt_array_seperator):to("["):link(tt_singlequote_string):to("'"):link(tt_doublequote_string):to("\""):link(tt_numeric):to("0123456789.-"):link(tt_boolean):to("tf"):link(tt_null):to("n"):link(tt_comment_start):to("/"):link(tt_ignore):to(" \t\r\n")
  init_token_table(tt_doublequote_string)("double quoted string"):link(tt_ignore):to(strchars):link(c_esc):to("\\"):link(true):to("\"")
  init_token_table(tt_singlequote_string)("single quoted string"):link(tt_ignore):to(strchars):link(c_esc):to("\\"):link(true):to("'")
  init_token_table(tt_array_value)("array (, or ] expected)"):link(tt_array_seperator):to(","):link(true):to("]"):link(tt_comment_start):to("/"):link(tt_ignore):to(" \t\r\n")
  init_token_table(tt_array_seperator)("array ({ or [ or ' or \" or number or boolean or null expected)"):link(tt_object_key):to("{"):link(tt_array_seperator):to("["):link(tt_singlequote_string):to("'"):link(tt_doublequote_string):to("\""):link(tt_comment_start):to("/"):link(tt_numeric):to("0123456789.-"):link(tt_boolean):to("tf"):link(tt_null):to("n"):link(tt_ignore):to(" \t\r\n")
  init_token_table(tt_numeric)("number"):link(tt_ignore):to("0123456789.-Ee")
  init_token_table(tt_comment_start)("comment start (* expected)"):link(tt_comment_middle):to("*")
  init_token_table(tt_comment_middle)("comment end"):link(tt_ignore):to(allchars):link(true):to("*")
  function decode(js_string)
    local pos = 1
    js_string = string.gsub(js_string, "\n", "")
    js_string = string.gsub(js_string, "\t", "")
    local function next_byte()
      pos = pos + 1
      return ...
    end
    local function location()
      local n = ("\n"):byte()
      local line, lpos = 1, 0
      for i = 1, pos do
        if js_string:byte(i) == n then
          line, lpos = line + 1, 1
        else
          lpos = lpos + 1
        end
      end
      return "Line " .. line .. " character " .. lpos
    end
    local function next_token(tok)
      while pos <= #js_string do
        local b = js_string:byte(pos)
        local t = tok[b]
        if not t then
          error("Unexpected character at " .. location() .. ": " .. string.char(b) .. " (" .. b .. ") when reading " .. tok.name .. [[

Context: 
]] .. js_string:sub(math.max(1, pos - 30), pos + 30) .. "\n" .. (" "):rep(pos + math.min(-1, 30 - pos)) .. "^")
        end
        pos = pos + 1
        if t ~= tt_ignore then
          return t
        end
      end
      error("unexpected termination of JSON while looking for " .. tok.name)
    end
    local function read_string(tok)
      local start = pos
      repeat
        local t = next_token(tok)
        if t == c_esc then
          pos = pos + 1
        end
      until t == true
      return (base.loadstring("return " .. js_string:sub(start - 1, pos - 1))())
    end
    local function read_num()
      local start = pos
      while pos <= #js_string do
        local b = js_string:byte(pos)
        if not tt_numeric[b] then
          break
        end
        pos = pos + 1
      end
      return ...
    end
    local function read_bool()
      pos = pos + 3
      local a, b, c, d = js_string:byte(pos - 3, pos)
      if a == c_r and b == c_u and c == c_e then
        return true
      end
      pos = pos + 1
      if a ~= c_a or b ~= c_l or c ~= c_s or d ~= c_e then
        error("Invalid boolean: " .. js_string:sub(math.max(1, pos - 5), pos + 5))
      end
      return false
    end
    local function read_null()
      pos = pos + 3
      local u, l1, l2 = js_string:byte(pos - 3, pos - 1)
      if u == c_u and l1 == c_l and l2 == c_l then
        return nil
      end
      error("Invalid value (expected null):" .. js_string:sub(pos - 4, pos - 1) .. " (" .. js_string:byte(pos - 1) .. "=" .. js_string:sub(pos - 1, pos - 1) .. " / " .. c_l .. ")")
    end
    local read_object_value, read_object_key, read_array, read_value, read_comment
    function read_value(t, fromt)
