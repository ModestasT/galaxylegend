local search_170 = GameData.adventure.search_170
search_170[101] = {EQUIP_RECIPE = 101, battleid = "1002"}
search_170[102] = {EQUIP_RECIPE = 102, battleid = "1001"}
search_170[103] = {EQUIP_RECIPE = 103, battleid = "1002"}
search_170[104] = {EQUIP_RECIPE = 104, battleid = "1003"}
search_170[105] = {EQUIP_RECIPE = 105, battleid = "1003"}
search_170[106] = {EQUIP_RECIPE = 106, battleid = "1001"}
search_170[121] = {EQUIP_RECIPE = 121, battleid = "1005"}
search_170[122] = {EQUIP_RECIPE = 122, battleid = "1004"}
search_170[123] = {EQUIP_RECIPE = 123, battleid = "1005"}
search_170[124] = {EQUIP_RECIPE = 124, battleid = "1006"}
search_170[125] = {EQUIP_RECIPE = 125, battleid = "1006"}
search_170[126] = {EQUIP_RECIPE = 126, battleid = "1004"}
search_170[131] = {EQUIP_RECIPE = 131, battleid = "1008"}
search_170[132] = {EQUIP_RECIPE = 132, battleid = "1007"}
search_170[133] = {EQUIP_RECIPE = 133, battleid = "1008"}
search_170[134] = {EQUIP_RECIPE = 134, battleid = "2002"}
search_170[135] = {EQUIP_RECIPE = 135, battleid = "2001"}
search_170[136] = {EQUIP_RECIPE = 136, battleid = "1007"}
search_170[141] = {EQUIP_RECIPE = 141, battleid = "2005,2006"}
search_170[142] = {EQUIP_RECIPE = 142, battleid = "2003,2004"}
search_170[143] = {EQUIP_RECIPE = 143, battleid = "2007,2008"}
search_170[144] = {EQUIP_RECIPE = 144, battleid = "3003,3004"}
search_170[145] = {EQUIP_RECIPE = 145, battleid = "3001,3002"}
search_170[146] = {EQUIP_RECIPE = 146, battleid = "2003,2004"}
search_170[151] = {EQUIP_RECIPE = 151, battleid = "3007,3008"}
search_170[152] = {EQUIP_RECIPE = 152, battleid = "3005,3006"}
search_170[153] = {EQUIP_RECIPE = 153, battleid = "4001,4002"}
search_170[154] = {EQUIP_RECIPE = 154, battleid = "4005,4006"}
search_170[155] = {EQUIP_RECIPE = 155, battleid = "4003,4004"}
search_170[156] = {EQUIP_RECIPE = 156, battleid = "3005,3006"}
search_170[161] = {EQUIP_RECIPE = 161, battleid = "5001,5002"}
search_170[162] = {EQUIP_RECIPE = 162, battleid = "4007,4008"}
search_170[163] = {EQUIP_RECIPE = 163, battleid = "5003,5004"}
search_170[164] = {EQUIP_RECIPE = 164, battleid = "5007,5008"}
search_170[165] = {EQUIP_RECIPE = 165, battleid = "5005,5006"}
search_170[166] = {EQUIP_RECIPE = 166, battleid = "4007,4008"}
search_170[171] = {EQUIP_RECIPE = 171, battleid = "6003,6004"}
search_170[172] = {EQUIP_RECIPE = 172, battleid = "6001,6002"}
search_170[173] = {EQUIP_RECIPE = 173, battleid = "6005,6006"}
search_170[174] = {EQUIP_RECIPE = 174, battleid = "7001,7002"}
search_170[175] = {EQUIP_RECIPE = 175, battleid = "6007,6008"}
search_170[176] = {EQUIP_RECIPE = 176, battleid = "6001,6002"}
search_170[181] = {EQUIP_RECIPE = 181, battleid = "7005,7006"}
search_170[182] = {EQUIP_RECIPE = 182, battleid = "7003,7004"}
search_170[183] = {EQUIP_RECIPE = 183, battleid = "7007,7008"}
search_170[184] = {EQUIP_RECIPE = 184, battleid = "8003,8004"}
search_170[185] = {EQUIP_RECIPE = 185, battleid = "8001,8002"}
search_170[186] = {EQUIP_RECIPE = 186, battleid = "7003,7004"}
search_170[191] = {EQUIP_RECIPE = 191, battleid = "8007,8008"}
search_170[192] = {EQUIP_RECIPE = 192, battleid = "8005,8006"}
search_170[193] = {EQUIP_RECIPE = 193, battleid = "9001,9002"}
search_170[194] = {EQUIP_RECIPE = 194, battleid = "9005,9006"}
search_170[195] = {EQUIP_RECIPE = 195, battleid = "9003,9004"}
search_170[196] = {EQUIP_RECIPE = 196, battleid = "8005,8006"}
search_170[201] = {
  EQUIP_RECIPE = 201,
  battleid = "10003,10004"
}
search_170[202] = {
  EQUIP_RECIPE = 202,
  battleid = "9007,10001,10002"
}
search_170[203] = {
  EQUIP_RECIPE = 203,
  battleid = "10003,10004"
}
search_170[204] = {
  EQUIP_RECIPE = 204,
  battleid = "10005,10006,10007"
}
search_170[205] = {
  EQUIP_RECIPE = 205,
  battleid = "10005,10006,10007"
}
search_170[206] = {
  EQUIP_RECIPE = 206,
  battleid = "9008,10001,10002"
}
