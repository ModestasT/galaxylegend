local Dialogues = GameData.AllDialogues.Dialogues
Dialogues[1100001] = {
  DialogID = 1100001,
  NPCHead = 17,
  HeadName = "NPC_1",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_TUTORIAL_1"
}
Dialogues[1100002] = {
  DialogID = 1100002,
  NPCHead = 13,
  HeadName = "NPC_42",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_TUTORIAL_2"
}
Dialogues[1100003] = {
  DialogID = 1100003,
  NPCHead = 17,
  HeadName = "NPC_1",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_TUTORIAL_3"
}
Dialogues[1100004] = {
  DialogID = 1100004,
  NPCHead = 17,
  HeadName = "NPC_1",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_TUTORIAL_4"
}
Dialogues[1100005] = {
  DialogID = 1100005,
  NPCHead = 17,
  HeadName = "NPC_1",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_TUTORIAL_5"
}
Dialogues[1100006] = {
  DialogID = 1100006,
  NPCHead = 17,
  HeadName = "NPC_1",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_TUTORIAL_6"
}
Dialogues[1100007] = {
  DialogID = 1100007,
  NPCHead = 17,
  HeadName = "NPC_1",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_TUTORIAL_7"
}
Dialogues[1100008] = {
  DialogID = 1100008,
  NPCHead = 17,
  HeadName = "NPC_1",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_TUTORIAL_8"
}
Dialogues[1100009] = {
  DialogID = 1100009,
  NPCHead = 17,
  HeadName = "NPC_1",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_TUTORIAL_9"
}
Dialogues[1100010] = {
  DialogID = 1100010,
  NPCHead = 17,
  HeadName = "NPC_1",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_TUTORIAL_10"
}
Dialogues[1100011] = {
  DialogID = 1100011,
  NPCHead = 17,
  HeadName = "NPC_1",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_TUTORIAL_11"
}
Dialogues[1100012] = {
  DialogID = 1100012,
  NPCHead = 17,
  HeadName = "NPC_1",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_TUTORIAL_12"
}
Dialogues[1100013] = {
  DialogID = 1100013,
  NPCHead = 17,
  HeadName = "NPC_1",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_TUTORIAL_13"
}
Dialogues[1100014] = {
  DialogID = 1100014,
  NPCHead = 17,
  HeadName = "NPC_1",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_TUTORIAL_14"
}
Dialogues[1100015] = {
  DialogID = 1100015,
  NPCHead = 17,
  HeadName = "NPC_1",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_TUTORIAL_15"
}
Dialogues[1100016] = {
  DialogID = 1100016,
  NPCHead = 17,
  HeadName = "NPC_1",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_TUTORIAL_16"
}
Dialogues[1100017] = {
  DialogID = 1100017,
  NPCHead = 17,
  HeadName = "NPC_1",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_TUTORIAL_17"
}
Dialogues[1100018] = {
  DialogID = 1100018,
  NPCHead = 17,
  HeadName = "NPC_1",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_TUTORIAL_18"
}
Dialogues[1100019] = {
  DialogID = 1100019,
  NPCHead = 17,
  HeadName = "NPC_1",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_TUTORIAL_19"
}
Dialogues[1100020] = {
  DialogID = 1100020,
  NPCHead = 17,
  HeadName = "NPC_1",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_TUTORIAL_20"
}
Dialogues[1100021] = {
  DialogID = 1100021,
  NPCHead = 17,
  HeadName = "NPC_1",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_TUTORIAL_21"
}
Dialogues[1100022] = {
  DialogID = 1100022,
  NPCHead = 17,
  HeadName = "NPC_1",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_TUTORIAL_22"
}
Dialogues[1100023] = {
  DialogID = 1100023,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_TUTORIAL_23"
}
Dialogues[1100024] = {
  DialogID = 1100024,
  NPCHead = 17,
  HeadName = "NPC_1",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_TUTORIAL_24"
}
Dialogues[1100025] = {
  DialogID = 1100025,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_TUTORIAL_25"
}
Dialogues[1100026] = {
  DialogID = 1100026,
  NPCHead = 17,
  HeadName = "NPC_1",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_TUTORIAL_26"
}
Dialogues[1100027] = {
  DialogID = 1100027,
  NPCHead = 17,
  HeadName = "NPC_1",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_TUTORIAL_27"
}
Dialogues[1100028] = {
  DialogID = 1100028,
  NPCHead = 17,
  HeadName = "NPC_1",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_TUTORIAL_28"
}
Dialogues[1100029] = {
  DialogID = 1100029,
  NPCHead = 17,
  HeadName = "NPC_1",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_TUTORIAL_29"
}
Dialogues[1100030] = {
  DialogID = 1100030,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_TUTORIAL_30"
}
Dialogues[1100031] = {
  DialogID = 1100031,
  NPCHead = 17,
  HeadName = "NPC_1",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_TUTORIAL_31"
}
Dialogues[1100032] = {
  DialogID = 1100032,
  NPCHead = 17,
  HeadName = "NPC_1",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_TUTORIAL_32"
}
Dialogues[1100033] = {
  DialogID = 1100033,
  NPCHead = 17,
  HeadName = "NPC_1",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_TUTORIAL_33"
}
Dialogues[1100034] = {
  DialogID = 1100034,
  NPCHead = 17,
  HeadName = "NPC_1",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_TUTORIAL_34"
}
Dialogues[1100035] = {
  DialogID = 1100035,
  NPCHead = 17,
  HeadName = "NPC_1",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_TUTORIAL_35"
}
Dialogues[1100036] = {
  DialogID = 1100036,
  NPCHead = 17,
  HeadName = "NPC_1",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_TUTORIAL_36"
}
Dialogues[1100037] = {
  DialogID = 1100037,
  NPCHead = 17,
  HeadName = "NPC_1",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_TUTORIAL_37"
}
Dialogues[1100038] = {
  DialogID = 1100038,
  NPCHead = 39,
  HeadName = "NPC_51",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_TUTORIAL_38"
}
Dialogues[1100039] = {
  DialogID = 1100039,
  NPCHead = 39,
  HeadName = "NPC_51",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_TUTORIAL_39"
}
Dialogues[1100040] = {
  DialogID = 1100040,
  NPCHead = 17,
  HeadName = "NPC_1",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_TUTORIAL_40"
}
Dialogues[1100041] = {
  DialogID = 1100041,
  NPCHead = 17,
  HeadName = "NPC_1",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_TUTORIAL_41"
}
Dialogues[1100042] = {
  DialogID = 1100042,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER0_1_1"
}
Dialogues[1100043] = {
  DialogID = 1100043,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER0_1_2"
}
Dialogues[1100044] = {
  DialogID = 1100044,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER0_1_3"
}
Dialogues[1100045] = {
  DialogID = 1100045,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER0_1_4"
}
Dialogues[1100046] = {
  DialogID = 1100046,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER0_1_5"
}
Dialogues[1100047] = {
  DialogID = 1100047,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER0_1_6"
}
Dialogues[1100048] = {
  DialogID = 1100048,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 0,
  TextID = "LC_STORY_NEW_CHAPTER0_1_7"
}
Dialogues[1100049] = {
  DialogID = 1100049,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER0_2_1"
}
Dialogues[1100050] = {
  DialogID = 1100050,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER0_2_2"
}
Dialogues[1100051] = {
  DialogID = 1100051,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER0_2_3"
}
Dialogues[1100052] = {
  DialogID = 1100052,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER0_2_4"
}
Dialogues[1100053] = {
  DialogID = 1100053,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER0_3_1"
}
Dialogues[1100054] = {
  DialogID = 1100054,
  NPCHead = 1002,
  HeadName = "NPC_53",
  NPCPos = 0,
  TextID = "LC_STORY_NEW_CHAPTER0_4_1"
}
Dialogues[1100055] = {
  DialogID = 1100055,
  NPCHead = 1002,
  HeadName = "NPC_53",
  NPCPos = 0,
  TextID = "LC_STORY_NEW_CHAPTER0_4_2"
}
Dialogues[1100056] = {
  DialogID = 1100056,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 0,
  TextID = "LC_STORY_NEW_CHAPTER0_4_3"
}
Dialogues[1100057] = {
  DialogID = 1100057,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 0,
  TextID = "LC_STORY_NEW_CHAPTER0_5_1"
}
Dialogues[1100058] = {
  DialogID = 1100058,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER0_5_2"
}
Dialogues[1100059] = {
  DialogID = 1100059,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 0,
  TextID = "LC_STORY_NEW_CHAPTER0_5_3"
}
Dialogues[1100060] = {
  DialogID = 1100060,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 0,
  TextID = "LC_STORY_NEW_CHAPTER0_5_4"
}
Dialogues[1100061] = {
  DialogID = 1100061,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER0_5_5"
}
Dialogues[1100062] = {
  DialogID = 1100062,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER0_5_6"
}
Dialogues[1100063] = {
  DialogID = 1100063,
  NPCHead = -1,
  HeadName = "NPC_MISTERY",
  NPCPos = 0,
  TextID = "LC_STORY_NEW_CHAPTER0_6_1"
}
Dialogues[1100064] = {
  DialogID = 1100064,
  NPCHead = 27,
  HeadName = "NPC_38",
  NPCPos = 0,
  TextID = "LC_STORY_NEW_CHAPTER0_6_2"
}
Dialogues[1100065] = {
  DialogID = 1100065,
  NPCHead = 62,
  HeadName = "NPC_132",
  NPCPos = 0,
  TextID = "LC_STORY_NEW_CHAPTER0_6_3"
}
Dialogues[1100066] = {
  DialogID = 1100066,
  NPCHead = 5,
  HeadName = "NPC_4",
  NPCPos = 0,
  TextID = "LC_STORY_NEW_CHAPTER0_6_4"
}
Dialogues[1100067] = {
  DialogID = 1100067,
  NPCHead = 64,
  HeadName = "NPC_134",
  NPCPos = 0,
  TextID = "LC_STORY_NEW_CHAPTER0_6_5"
}
Dialogues[1100068] = {
  DialogID = 1100068,
  NPCHead = 102,
  HeadName = "NPC_173",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER0_6_6"
}
Dialogues[1100069] = {
  DialogID = 1100069,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_NEW_CHAPTER0_6_7"
}
Dialogues[1100070] = {
  DialogID = 1100070,
  NPCHead = 102,
  HeadName = "NPC_173",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER0_6_8"
}
Dialogues[1100071] = {
  DialogID = 1100071,
  NPCHead = 14,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_NEW_CHAPTER0_6_9"
}
Dialogues[1100072] = {
  DialogID = 1100072,
  NPCHead = 117,
  HeadName = "NPC_215",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER0_6_10"
}
Dialogues[1100073] = {
  DialogID = 1100073,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_NEW_CHAPTER0_6_11"
}
Dialogues[1100074] = {
  DialogID = 1100074,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_NEW_CHAPTER0_6_12"
}
Dialogues[1100075] = {
  DialogID = 1100075,
  NPCHead = 117,
  HeadName = "NPC_215",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER0_6_13"
}
Dialogues[1100076] = {
  DialogID = 1100076,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER0_6_14"
}
Dialogues[1100077] = {
  DialogID = 1100077,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER0_6_15"
}
Dialogues[1100078] = {
  DialogID = 1100078,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER0_6_16"
}
Dialogues[1100079] = {
  DialogID = 1100079,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER0_6_17"
}
Dialogues[1100080] = {
  DialogID = 1100080,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_NEW_CHAPTER0_6_18"
}
Dialogues[1100081] = {
  DialogID = 1100081,
  NPCHead = 102,
  HeadName = "NPC_173",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER0_6_19"
}
Dialogues[1100082] = {
  DialogID = 1100082,
  NPCHead = 106,
  HeadName = "NPC_177",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER0_7_1"
}
Dialogues[1100083] = {
  DialogID = 1100083,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER0_7_2"
}
Dialogues[1100084] = {
  DialogID = 1100084,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER0_7_3"
}
Dialogues[1100085] = {
  DialogID = 1100085,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER0_7_4"
}
Dialogues[1100086] = {
  DialogID = 1100086,
  NPCHead = 1002,
  HeadName = "NPC_53",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER0_8_1"
}
Dialogues[1100087] = {
  DialogID = 1100087,
  NPCHead = 1002,
  HeadName = "NPC_53",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER0_8_2"
}
Dialogues[1100088] = {
  DialogID = 1100088,
  NPCHead = 1002,
  HeadName = "NPC_53",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER0_8_3"
}
Dialogues[1100089] = {
  DialogID = 1100089,
  NPCHead = 1002,
  HeadName = "NPC_53",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER0_8_4"
}
Dialogues[1100090] = {
  DialogID = 1100090,
  NPCHead = 1002,
  HeadName = "NPC_53",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER0_8_5"
}
Dialogues[1100091] = {
  DialogID = 1100091,
  NPCHead = 1002,
  HeadName = "NPC_53",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER0_8_6"
}
Dialogues[1100092] = {
  DialogID = 1100092,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_EX_TUTORIAL_1"
}
Dialogues[1100093] = {
  DialogID = 1100093,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_EX_TUTORIAL_2"
}
Dialogues[1100094] = {
  DialogID = 1100094,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_EX_TUTORIAL_3"
}
Dialogues[1100095] = {
  DialogID = 1100095,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_EX_TUTORIAL_4"
}
Dialogues[1100096] = {
  DialogID = 1100096,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_NEW_FTE_BATTLE_1"
}
Dialogues[1100097] = {
  DialogID = 1100097,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_NEW_FTE_BATTLE_2"
}
Dialogues[1100098] = {
  DialogID = 1100098,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_NEW_FTE_BATTLE_3"
}
Dialogues[1100099] = {
  DialogID = 1100099,
  NPCHead = 65,
  HeadName = "NPC_136",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_FTE_BATTLE_4"
}
Dialogues[1100100] = {
  DialogID = 1100100,
  NPCHead = 175,
  HeadName = "NPC_345",
  NPCPos = 0,
  TextID = "LC_STORY_NEW_FTE_BATTLE_5"
}
Dialogues[1100101] = {
  DialogID = 1100101,
  NPCHead = 159,
  HeadName = "NPC_329",
  NPCPos = 0,
  TextID = "LC_STORY_NEW_FTE_BATTLE_6"
}
Dialogues[1100102] = {
  DialogID = 1100102,
  NPCHead = 65,
  HeadName = "NPC_136",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_FTE_BATTLE_7"
}
Dialogues[1100103] = {
  DialogID = 1100103,
  NPCHead = 159,
  HeadName = "NPC_329",
  NPCPos = 0,
  TextID = "LC_STORY_NEW_FTE_BATTLE_8"
}
Dialogues[1100104] = {
  DialogID = 1100104,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_NEW_FTE_BATTLE_9"
}
Dialogues[51100] = {
  DialogID = 51100,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_NEW_CHAPTER1_0_1"
}
Dialogues[51101] = {
  DialogID = 51101,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER1_1_1"
}
Dialogues[51102] = {
  DialogID = 51102,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_NEW_CHAPTER1_1_2"
}
Dialogues[51103] = {
  DialogID = 51103,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER1_1_3"
}
Dialogues[51104] = {
  DialogID = 51104,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_NEW_CHAPTER1_1_4"
}
Dialogues[51105] = {
  DialogID = 51105,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER1_1_5"
}
Dialogues[51106] = {
  DialogID = 51106,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_NEW_CHAPTER1_1_6"
}
Dialogues[51107] = {
  DialogID = 51107,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER1_1_7"
}
Dialogues[51108] = {
  DialogID = 51108,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER1_1_8"
}
Dialogues[51109] = {
  DialogID = 51109,
  NPCHead = 17,
  HeadName = "NPC_1",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER1_1_9"
}
Dialogues[51110] = {
  DialogID = 51110,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER1_1_10"
}
Dialogues[51111] = {
  DialogID = 51111,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_NEW_CHAPTER1_1_11"
}
Dialogues[51112] = {
  DialogID = 51112,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_NEW_CHAPTER1_1_12"
}
Dialogues[51113] = {
  DialogID = 51113,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER1_1_13"
}
Dialogues[51114] = {
  DialogID = 51114,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER1_1_14"
}
Dialogues[51115] = {
  DialogID = 51115,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER1_1_15"
}
Dialogues[51116] = {
  DialogID = 51116,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_NEW_CHAPTER1_1_16"
}
Dialogues[51117] = {
  DialogID = 51117,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_NEW_CHAPTER1_1_17"
}
Dialogues[51118] = {
  DialogID = 51118,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER1_1_18"
}
Dialogues[51119] = {
  DialogID = 51119,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER1_1_19"
}
Dialogues[51120] = {
  DialogID = 51120,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_NEW_CHAPTER1_1_20"
}
Dialogues[51121] = {
  DialogID = 51121,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER1_1_21"
}
Dialogues[51122] = {
  DialogID = 51122,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_NEW_CHAPTER1_1_22"
}
Dialogues[51123] = {
  DialogID = 51123,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER1_1_23"
}
Dialogues[51124] = {
  DialogID = 51124,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER1_1_24"
}
Dialogues[51125] = {
  DialogID = 51125,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER1_1_25"
}
Dialogues[51126] = {
  DialogID = 51126,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_NEW_CHAPTER1_1_26"
}
Dialogues[51127] = {
  DialogID = 51127,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER1_1_27"
}
Dialogues[51128] = {
  DialogID = 51128,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER1_1_28"
}
Dialogues[51129] = {
  DialogID = 51129,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER1_1_29"
}
Dialogues[51130] = {
  DialogID = 51130,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER1_1_30"
}
Dialogues[51131] = {
  DialogID = 51131,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_NEW_CHAPTER1_1_31"
}
Dialogues[51132] = {
  DialogID = 51132,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_NEW_CHAPTER1_1_32"
}
Dialogues[51133] = {
  DialogID = 51133,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER1_1_33"
}
Dialogues[51134] = {
  DialogID = 51134,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_NEW_CHAPTER1_1_34"
}
Dialogues[51135] = {
  DialogID = 51135,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER1_1_35"
}
Dialogues[51136] = {
  DialogID = 51136,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER1_1_36"
}
Dialogues[51137] = {
  DialogID = 51137,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER1_1_37"
}
Dialogues[51138] = {
  DialogID = 51138,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_NEW_CHAPTER1_1_38"
}
Dialogues[51139] = {
  DialogID = 51139,
  NPCHead = 24,
  HeadName = "NPC_44",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER1_1_39"
}
Dialogues[51140] = {
  DialogID = 51140,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_NEW_CHAPTER1_1_40"
}
Dialogues[51141] = {
  DialogID = 51141,
  NPCHead = 1,
  HeadName = "NPC_2",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER1_1_41"
}
Dialogues[51142] = {
  DialogID = 51142,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER1_1_42"
}
Dialogues[51143] = {
  DialogID = 51143,
  NPCHead = 1,
  HeadName = "NPC_2",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER1_1_43"
}
Dialogues[51144] = {
  DialogID = 51144,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_NEW_CHAPTER1_1_44"
}
Dialogues[51145] = {
  DialogID = 51145,
  NPCHead = 1,
  HeadName = "NPC_2",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER1_1_45"
}
Dialogues[51146] = {
  DialogID = 51146,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER1_1_46"
}
Dialogues[51147] = {
  DialogID = 51147,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_NEW_CHAPTER1_1_47"
}
Dialogues[51148] = {
  DialogID = 51148,
  NPCHead = 1,
  HeadName = "NPC_2",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER1_1_48"
}
Dialogues[51149] = {
  DialogID = 51149,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER1_1_49"
}
Dialogues[51150] = {
  DialogID = 51150,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_NEW_CHAPTER1_1_50"
}
Dialogues[51201] = {
  DialogID = 51201,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER1_2_1"
}
Dialogues[51202] = {
  DialogID = 51202,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_NEW_CHAPTER1_2_2"
}
Dialogues[51203] = {
  DialogID = 51203,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER1_2_3"
}
Dialogues[51204] = {
  DialogID = 51204,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_NEW_CHAPTER1_2_4"
}
Dialogues[51205] = {
  DialogID = 51205,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER1_2_5"
}
Dialogues[51206] = {
  DialogID = 51206,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER1_2_6"
}
Dialogues[51207] = {
  DialogID = 51207,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER1_2_7"
}
Dialogues[51208] = {
  DialogID = 51208,
  NPCHead = 1,
  HeadName = "NPC_2",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER1_2_8"
}
Dialogues[51209] = {
  DialogID = 51209,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_NEW_CHAPTER1_2_9"
}
Dialogues[51210] = {
  DialogID = 51210,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER1_2_10"
}
Dialogues[51211] = {
  DialogID = 51211,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER1_2_11"
}
Dialogues[51212] = {
  DialogID = 51212,
  NPCHead = 1002,
  HeadName = "NPC_53",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER1_2_12"
}
Dialogues[51213] = {
  DialogID = 51213,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER1_2_13"
}
Dialogues[51214] = {
  DialogID = 51214,
  NPCHead = 1002,
  HeadName = "NPC_53",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER1_2_14"
}
Dialogues[51215] = {
  DialogID = 51215,
  NPCHead = 1,
  HeadName = "NPC_2",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER1_2_15"
}
Dialogues[51216] = {
  DialogID = 51216,
  NPCHead = 1,
  HeadName = "NPC_2",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER1_2_16"
}
Dialogues[51217] = {
  DialogID = 51217,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER1_2_17"
}
Dialogues[51218] = {
  DialogID = 51218,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER1_2_18"
}
Dialogues[51219] = {
  DialogID = 51219,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_NEW_CHAPTER1_2_19"
}
Dialogues[51220] = {
  DialogID = 51220,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER1_2_20"
}
Dialogues[51221] = {
  DialogID = 51221,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_NEW_CHAPTER1_2_21"
}
Dialogues[51222] = {
  DialogID = 51222,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER1_2_22"
}
Dialogues[51223] = {
  DialogID = 51223,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER1_2_23"
}
Dialogues[51224] = {
  DialogID = 51224,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER1_2_24"
}
Dialogues[51225] = {
  DialogID = 51225,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER1_2_25"
}
Dialogues[51226] = {
  DialogID = 51226,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER1_2_26"
}
Dialogues[51227] = {
  DialogID = 51227,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_NEW_CHAPTER1_2_27"
}
Dialogues[51228] = {
  DialogID = 51228,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER1_2_28"
}
Dialogues[51229] = {
  DialogID = 51229,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_NEW_CHAPTER1_2_29"
}
Dialogues[51230] = {
  DialogID = 51230,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER1_2_30"
}
Dialogues[51231] = {
  DialogID = 51231,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_NEW_CHAPTER1_2_31"
}
Dialogues[51232] = {
  DialogID = 51232,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER1_2_32"
}
Dialogues[51301] = {
  DialogID = 51301,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_NEW_CHAPTER1_3_1"
}
Dialogues[51302] = {
  DialogID = 51302,
  NPCHead = 24,
  HeadName = "NPC_44",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER1_3_2"
}
Dialogues[51303] = {
  DialogID = 51303,
  NPCHead = 24,
  HeadName = "NPC_44",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER1_3_3"
}
Dialogues[51304] = {
  DialogID = 51304,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_NEW_CHAPTER1_3_4"
}
Dialogues[51305] = {
  DialogID = 51305,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER1_3_5"
}
Dialogues[51306] = {
  DialogID = 51306,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER1_3_6"
}
Dialogues[51307] = {
  DialogID = 51307,
  NPCHead = 13,
  HeadName = "NPC_29",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER1_3_7"
}
Dialogues[51308] = {
  DialogID = 51308,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_NEW_CHAPTER1_3_8"
}
Dialogues[51309] = {
  DialogID = 51309,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER1_3_9"
}
Dialogues[51310] = {
  DialogID = 51310,
  NPCHead = 24,
  HeadName = "NPC_44",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER1_3_10"
}
Dialogues[51311] = {
  DialogID = 51311,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_NEW_CHAPTER1_3_11"
}
Dialogues[51312] = {
  DialogID = 51312,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER1_3_12"
}
Dialogues[51313] = {
  DialogID = 51313,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_NEW_CHAPTER1_3_13"
}
Dialogues[51314] = {
  DialogID = 51314,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_NEW_CHAPTER1_3_14"
}
Dialogues[51315] = {
  DialogID = 51315,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER1_3_15"
}
Dialogues[51316] = {
  DialogID = 51316,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER1_3_16"
}
Dialogues[51317] = {
  DialogID = 51317,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER1_3_17"
}
Dialogues[51318] = {
  DialogID = 51318,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER1_3_18"
}
Dialogues[51319] = {
  DialogID = 51319,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_NEW_CHAPTER1_3_19"
}
Dialogues[51320] = {
  DialogID = 51320,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_NEW_CHAPTER1_3_20"
}
Dialogues[100002] = {
  DialogID = 100002,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_TUTORIAL_1"
}
Dialogues[100003] = {
  DialogID = 100003,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_TUTORIAL_2"
}
Dialogues[100004] = {
  DialogID = 100004,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_TUTORIAL_3"
}
Dialogues[100005] = {
  DialogID = 100005,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_TUTORIAL_4"
}
Dialogues[100006] = {
  DialogID = 100006,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_TUTORIAL_5"
}
Dialogues[100007] = {
  DialogID = 100007,
  NPCHead = 30,
  HeadName = "NPC_45",
  NPCPos = 1,
  TextID = "LC_STORY_TUTORIAL_6"
}
Dialogues[100008] = {
  DialogID = 100008,
  NPCHead = 30,
  HeadName = "NPC_45",
  NPCPos = 1,
  TextID = "LC_STORY_TUTORIAL_7"
}
Dialogues[100009] = {
  DialogID = 100009,
  NPCHead = 1002,
  HeadName = "NPC_53",
  NPCPos = 1,
  TextID = "LC_STORY_TUTORIAL_9"
}
Dialogues[100010] = {
  DialogID = 100010,
  NPCHead = 1002,
  HeadName = "NPC_53",
  NPCPos = 1,
  TextID = "LC_STORY_TUTORIAL_10"
}
Dialogues[100011] = {
  DialogID = 100011,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_TUTORIAL_11"
}
Dialogues[100012] = {
  DialogID = 100012,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_TUTORIAL_12"
}
Dialogues[100013] = {
  DialogID = 100013,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_TUTORIAL_13"
}
Dialogues[100014] = {
  DialogID = 100014,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_TUTORIAL_14"
}
Dialogues[100015] = {
  DialogID = 100015,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_TUTORIAL_15"
}
Dialogues[100016] = {
  DialogID = 100016,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_TUTORIAL_16"
}
Dialogues[100022] = {
  DialogID = 100022,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_TUTORIAL_17"
}
Dialogues[100018] = {
  DialogID = 100018,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_TUTORIAL_18"
}
Dialogues[100020] = {
  DialogID = 100020,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_TUTORIAL_20"
}
Dialogues[100021] = {
  DialogID = 100021,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_TUTORIAL_21"
}
Dialogues[100017] = {
  DialogID = 100017,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_TUTORIAL_22"
}
Dialogues[100001] = {
  DialogID = 100001,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_TUTORIAL_23"
}
Dialogues[100024] = {
  DialogID = 100024,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_TUTORIAL_24"
}
Dialogues[100025] = {
  DialogID = 100025,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_TUTORIAL_25"
}
Dialogues[100026] = {
  DialogID = 100026,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_TUTORIAL_26"
}
Dialogues[100027] = {
  DialogID = 100027,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_TUTORIAL_27"
}
Dialogues[100028] = {
  DialogID = 100028,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_TUTORIAL_28"
}
Dialogues[100029] = {
  DialogID = 100029,
  NPCHead = 1,
  HeadName = "NPC_2",
  NPCPos = 1,
  TextID = "LC_STORY_TUTORIAL_29"
}
Dialogues[100030] = {
  DialogID = 100030,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_TUTORIAL_30"
}
Dialogues[100031] = {
  DialogID = 100031,
  NPCHead = 1,
  HeadName = "NPC_2",
  NPCPos = 1,
  TextID = "LC_STORY_TUTORIAL_31"
}
Dialogues[100032] = {
  DialogID = 100032,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_TUTORIAL_32"
}
Dialogues[100033] = {
  DialogID = 100033,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_TUTORIAL_33"
}
Dialogues[100034] = {
  DialogID = 100034,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_TUTORIAL_34"
}
Dialogues[100035] = {
  DialogID = 100035,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_TUTORIAL_35"
}
Dialogues[100036] = {
  DialogID = 100036,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_TUTORIAL_36"
}
Dialogues[100037] = {
  DialogID = 100037,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_TUTORIAL_37"
}
Dialogues[100038] = {
  DialogID = 100038,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_TUTORIAL_38"
}
Dialogues[100039] = {
  DialogID = 100039,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_TUTORIAL_39"
}
Dialogues[100040] = {
  DialogID = 100040,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_TUTORIAL_40"
}
Dialogues[100041] = {
  DialogID = 100041,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_TUTORIAL_41"
}
Dialogues[100042] = {
  DialogID = 100042,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_TUTORIAL_42"
}
Dialogues[100043] = {
  DialogID = 100043,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_TUTORIAL_43"
}
Dialogues[100044] = {
  DialogID = 100044,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_TUTORIAL_44"
}
Dialogues[100045] = {
  DialogID = 100045,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_TUTORIAL_45"
}
Dialogues[100046] = {
  DialogID = 100046,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_TUTORIAL_46"
}
Dialogues[100047] = {
  DialogID = 100047,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_TUTORIAL_47"
}
Dialogues[100048] = {
  DialogID = 100048,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_TUTORIAL_48"
}
Dialogues[100049] = {
  DialogID = 100049,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_TUTORIAL_49"
}
Dialogues[100050] = {
  DialogID = 100050,
  NPCHead = 1002,
  HeadName = "NPC_53",
  NPCPos = 1,
  TextID = "LC_STORY_TUTORIAL_50"
}
Dialogues[100051] = {
  DialogID = 100051,
  NPCHead = 1002,
  HeadName = "NPC_53",
  NPCPos = 1,
  TextID = "LC_STORY_TUTORIAL_51"
}
Dialogues[100052] = {
  DialogID = 100052,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_TUTORIAL_52"
}
Dialogues[100053] = {
  DialogID = 100053,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_TUTORIAL_53"
}
Dialogues[100054] = {
  DialogID = 100054,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_TUTORIAL_54"
}
Dialogues[100055] = {
  DialogID = 100055,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_TUTORIAL_55"
}
Dialogues[100056] = {
  DialogID = 100056,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_TUTORIAL_56"
}
Dialogues[100057] = {
  DialogID = 100057,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_TUTORIAL_57"
}
Dialogues[100058] = {
  DialogID = 100058,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_TUTORIAL_58"
}
Dialogues[100059] = {
  DialogID = 100059,
  NPCHead = 0,
  HeadName = "NPC_1",
  NPCPos = 1,
  TextID = "LC_STORY_TUTORIAL_59"
}
Dialogues[100060] = {
  DialogID = 100060,
  NPCHead = 1002,
  HeadName = "NPC_53",
  NPCPos = 1,
  TextID = "LC_STORY_TUTORIAL_60"
}
Dialogues[100061] = {
  DialogID = 100061,
  NPCHead = 1002,
  HeadName = "NPC_53",
  NPCPos = 1,
  TextID = "LC_STORY_TUTORIAL_61"
}
Dialogues[100062] = {
  DialogID = 100062,
  NPCHead = 1002,
  HeadName = "NPC_53",
  NPCPos = 1,
  TextID = "LC_STORY_TUTORIAL_62"
}
Dialogues[100063] = {
  DialogID = 100063,
  NPCHead = 1002,
  HeadName = "NPC_53",
  NPCPos = 1,
  TextID = "LC_STORY_TUTORIAL_63"
}
Dialogues[100064] = {
  DialogID = 100064,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_TUTORIAL_64"
}
Dialogues[100065] = {
  DialogID = 100065,
  NPCHead = 1002,
  HeadName = "NPC_53",
  NPCPos = 1,
  TextID = "LC_STORY_TUTORIAL_65"
}
Dialogues[100066] = {
  DialogID = 100066,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_TUTORIAL_66"
}
Dialogues[100067] = {
  DialogID = 100067,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_TUTORIAL_67"
}
Dialogues[100068] = {
  DialogID = 100068,
  NPCHead = 1002,
  HeadName = "NPC_53",
  NPCPos = 1,
  TextID = "LC_STORY_TUTORIAL_68"
}
Dialogues[100069] = {
  DialogID = 100069,
  NPCHead = 0,
  HeadName = "NPC_1",
  NPCPos = 1,
  TextID = "LC_STORY_TUTORIAL_69"
}
Dialogues[100070] = {
  DialogID = 100070,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_TUTORIAL_70"
}
Dialogues[1] = {
  DialogID = 1,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER1_1_1"
}
Dialogues[2] = {
  DialogID = 2,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_1_2"
}
Dialogues[3] = {
  DialogID = 3,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_1_3"
}
Dialogues[1134] = {
  DialogID = 1134,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER1_1_34"
}
Dialogues[1104] = {
  DialogID = 1104,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER1_1_4"
}
Dialogues[1135] = {
  DialogID = 1135,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_1_35"
}
Dialogues[1105] = {
  DialogID = 1105,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_1_5"
}
Dialogues[1106] = {
  DialogID = 1106,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER1_1_6"
}
Dialogues[1136] = {
  DialogID = 1136,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_1_36"
}
Dialogues[1107] = {
  DialogID = 1107,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_1_7"
}
Dialogues[1108] = {
  DialogID = 1108,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER1_1_8"
}
Dialogues[1137] = {
  DialogID = 1137,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_1_37"
}
Dialogues[1109] = {
  DialogID = 1109,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER1_1_9"
}
Dialogues[1138] = {
  DialogID = 1138,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_1_38"
}
Dialogues[1139] = {
  DialogID = 1139,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER1_1_39"
}
Dialogues[1110] = {
  DialogID = 1110,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_1_10"
}
Dialogues[1111] = {
  DialogID = 1111,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_1_11"
}
Dialogues[1112] = {
  DialogID = 1112,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER1_1_12"
}
Dialogues[1113] = {
  DialogID = 1113,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_1_13"
}
Dialogues[1114] = {
  DialogID = 1114,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_1_14"
}
Dialogues[1140] = {
  DialogID = 1140,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER1_1_40"
}
Dialogues[1115] = {
  DialogID = 1115,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER1_1_15"
}
Dialogues[1116] = {
  DialogID = 1116,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_1_16"
}
Dialogues[1117] = {
  DialogID = 1117,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_1_17"
}
Dialogues[1118] = {
  DialogID = 1118,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_1_18"
}
Dialogues[1141] = {
  DialogID = 1141,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_1_41"
}
Dialogues[1142] = {
  DialogID = 1142,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER1_1_42"
}
Dialogues[1119] = {
  DialogID = 1119,
  NPCHead = 24,
  HeadName = "NPC_1001011",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_1_19"
}
Dialogues[1120] = {
  DialogID = 1120,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER1_1_20"
}
Dialogues[1121] = {
  DialogID = 1121,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_1_21"
}
Dialogues[1122] = {
  DialogID = 1122,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_1_22"
}
Dialogues[1123] = {
  DialogID = 1123,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_1_23"
}
Dialogues[1124] = {
  DialogID = 1124,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER1_1_24"
}
Dialogues[1125] = {
  DialogID = 1125,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER1_1_25"
}
Dialogues[1126] = {
  DialogID = 1126,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_1_26"
}
Dialogues[1127] = {
  DialogID = 1127,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_1_27"
}
Dialogues[1203] = {
  DialogID = 1203,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_2_3"
}
Dialogues[1205] = {
  DialogID = 1205,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER1_2_5"
}
Dialogues[1206] = {
  DialogID = 1206,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_2_6"
}
Dialogues[1207] = {
  DialogID = 1207,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER1_2_7"
}
Dialogues[1208] = {
  DialogID = 1208,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_2_8"
}
Dialogues[1235] = {
  DialogID = 1235,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_2_35"
}
Dialogues[1236] = {
  DialogID = 1236,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_2_36"
}
Dialogues[1209] = {
  DialogID = 1209,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_2_9"
}
Dialogues[1210] = {
  DialogID = 1210,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_2_10"
}
Dialogues[1211] = {
  DialogID = 1211,
  NPCHead = 1002,
  HeadName = "NPC_53",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_2_11"
}
Dialogues[1212] = {
  DialogID = 1212,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_2_12"
}
Dialogues[1213] = {
  DialogID = 1213,
  NPCHead = 13,
  HeadName = "NPC_1002010",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_2_13"
}
Dialogues[1214] = {
  DialogID = 1214,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER1_2_14"
}
Dialogues[1215] = {
  DialogID = 1215,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_2_15"
}
Dialogues[1216] = {
  DialogID = 1216,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_2_16"
}
Dialogues[1217] = {
  DialogID = 1217,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_2_17"
}
Dialogues[1218] = {
  DialogID = 1218,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_2_18"
}
Dialogues[1219] = {
  DialogID = 1219,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_2_19"
}
Dialogues[1220] = {
  DialogID = 1220,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_2_20"
}
Dialogues[1221] = {
  DialogID = 1221,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER1_2_21"
}
Dialogues[1222] = {
  DialogID = 1222,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_2_22"
}
Dialogues[1223] = {
  DialogID = 1223,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER1_2_23"
}
Dialogues[1224] = {
  DialogID = 1224,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_2_24"
}
Dialogues[1225] = {
  DialogID = 1225,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_2_25"
}
Dialogues[1226] = {
  DialogID = 1226,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_2_26"
}
Dialogues[1227] = {
  DialogID = 1227,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_2_27"
}
Dialogues[1228] = {
  DialogID = 1228,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_2_28"
}
Dialogues[1229] = {
  DialogID = 1229,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER1_2_29"
}
Dialogues[1230] = {
  DialogID = 1230,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_2_30"
}
Dialogues[1231] = {
  DialogID = 1231,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER1_2_31"
}
Dialogues[1232] = {
  DialogID = 1232,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_2_32"
}
Dialogues[1233] = {
  DialogID = 1233,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER1_2_33"
}
Dialogues[1234] = {
  DialogID = 1234,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_2_34"
}
Dialogues[1301] = {
  DialogID = 1301,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER1_3_1"
}
Dialogues[1302] = {
  DialogID = 1302,
  NPCHead = 12,
  HeadName = "NPC_1003005",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_3_2"
}
Dialogues[1303] = {
  DialogID = 1303,
  NPCHead = 12,
  HeadName = "NPC_1003005",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_3_3"
}
Dialogues[1304] = {
  DialogID = 1304,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER1_3_4"
}
Dialogues[1305] = {
  DialogID = 1305,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_3_5"
}
Dialogues[1306] = {
  DialogID = 1306,
  NPCHead = 25,
  HeadName = "NPC_1003012",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_3_6"
}
Dialogues[1307] = {
  DialogID = 1307,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER1_3_7"
}
Dialogues[1308] = {
  DialogID = 1308,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_3_8"
}
Dialogues[1309] = {
  DialogID = 1309,
  NPCHead = 25,
  HeadName = "NPC_1003012",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_3_9"
}
Dialogues[1310] = {
  DialogID = 1310,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER1_3_10"
}
Dialogues[1311] = {
  DialogID = 1311,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER1_3_11"
}
Dialogues[1312] = {
  DialogID = 1312,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER1_3_12"
}
Dialogues[1313] = {
  DialogID = 1313,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_3_13"
}
Dialogues[1314] = {
  DialogID = 1314,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER1_3_14"
}
Dialogues[1315] = {
  DialogID = 1315,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER1_3_15"
}
Dialogues[1401] = {
  DialogID = 1401,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_4_1"
}
Dialogues[1402] = {
  DialogID = 1402,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER1_4_2"
}
Dialogues[1403] = {
  DialogID = 1403,
  NPCHead = 25,
  HeadName = "NPC_1004004",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_4_3"
}
Dialogues[1404] = {
  DialogID = 1404,
  NPCHead = 1,
  HeadName = "NPC_2",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_4_4"
}
Dialogues[1405] = {
  DialogID = 1405,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_4_5"
}
Dialogues[1406] = {
  DialogID = 1406,
  NPCHead = 30,
  HeadName = "NPC_45",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_4_6"
}
Dialogues[1407] = {
  DialogID = 1407,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_4_7"
}
Dialogues[1408] = {
  DialogID = 1408,
  NPCHead = 30,
  HeadName = "NPC_45",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_4_8"
}
Dialogues[1409] = {
  DialogID = 1409,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_4_9"
}
Dialogues[1410] = {
  DialogID = 1410,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER1_4_10"
}
Dialogues[1411] = {
  DialogID = 1411,
  NPCHead = 24,
  HeadName = "NPC_1004009",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_4_11"
}
Dialogues[1412] = {
  DialogID = 1412,
  NPCHead = 1,
  HeadName = "NPC_2",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_4_12"
}
Dialogues[1413] = {
  DialogID = 1413,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER1_4_13"
}
Dialogues[1414] = {
  DialogID = 1414,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_4_14"
}
Dialogues[1505] = {
  DialogID = 1505,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_5_5"
}
Dialogues[1506] = {
  DialogID = 1506,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER1_5_6"
}
Dialogues[1507] = {
  DialogID = 1507,
  NPCHead = 25,
  HeadName = "NPC_1005004",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_5_7"
}
Dialogues[1508] = {
  DialogID = 1508,
  NPCHead = 1,
  HeadName = "NPC_2",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_5_8"
}
Dialogues[1509] = {
  DialogID = 1509,
  NPCHead = 1002,
  HeadName = "NPC_53",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_5_9"
}
Dialogues[1510] = {
  DialogID = 1510,
  NPCHead = 12,
  HeadName = "NPC_1005007",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_5_10"
}
Dialogues[1511] = {
  DialogID = 1511,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_5_11"
}
Dialogues[1512] = {
  DialogID = 1512,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_5_12"
}
Dialogues[1513] = {
  DialogID = 1513,
  NPCHead = 1,
  HeadName = "NPC_2",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_5_13"
}
Dialogues[1514] = {
  DialogID = 1514,
  NPCHead = 24,
  HeadName = "NPC_1005009",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_5_14"
}
Dialogues[1515] = {
  DialogID = 1515,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER1_5_15"
}
Dialogues[1601] = {
  DialogID = 1601,
  NPCHead = 1,
  HeadName = "NPC_2",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_6_1"
}
Dialogues[1602] = {
  DialogID = 1602,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_6_2"
}
Dialogues[1603] = {
  DialogID = 1603,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER1_6_3"
}
Dialogues[1604] = {
  DialogID = 1604,
  NPCHead = 25,
  HeadName = "NPC_1006005",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_6_4"
}
Dialogues[1605] = {
  DialogID = 1605,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER1_6_5"
}
Dialogues[1606] = {
  DialogID = 1606,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_6_6"
}
Dialogues[1607] = {
  DialogID = 1607,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_6_7"
}
Dialogues[1608] = {
  DialogID = 1608,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_6_8"
}
Dialogues[1609] = {
  DialogID = 1609,
  NPCHead = 1,
  HeadName = "NPC_2",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_6_9"
}
Dialogues[1610] = {
  DialogID = 1610,
  NPCHead = 16,
  HeadName = "NPC_11",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_6_10"
}
Dialogues[1611] = {
  DialogID = 1611,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_6_11"
}
Dialogues[1612] = {
  DialogID = 1612,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_6_12"
}
Dialogues[1613] = {
  DialogID = 1613,
  NPCHead = 16,
  HeadName = "NPC_11",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_6_13"
}
Dialogues[1614] = {
  DialogID = 1614,
  NPCHead = 16,
  HeadName = "NPC_11",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_6_14"
}
Dialogues[1615] = {
  DialogID = 1615,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_6_15"
}
Dialogues[1616] = {
  DialogID = 1616,
  NPCHead = 1,
  HeadName = "NPC_2",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_6_16"
}
Dialogues[1617] = {
  DialogID = 1617,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER1_6_17"
}
Dialogues[1618] = {
  DialogID = 1618,
  NPCHead = 16,
  HeadName = "NPC_11",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_6_18"
}
Dialogues[1619] = {
  DialogID = 1619,
  NPCHead = 16,
  HeadName = "NPC_11",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_6_19"
}
Dialogues[1620] = {
  DialogID = 1620,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_6_20"
}
Dialogues[1621] = {
  DialogID = 1621,
  NPCHead = 1,
  HeadName = "NPC_2",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_6_21"
}
Dialogues[1622] = {
  DialogID = 1622,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER1_6_22"
}
Dialogues[1623] = {
  DialogID = 1623,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_6_23"
}
Dialogues[1624] = {
  DialogID = 1624,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_6_24"
}
Dialogues[1625] = {
  DialogID = 1625,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER1_6_25"
}
Dialogues[1701] = {
  DialogID = 1701,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_7_1"
}
Dialogues[1702] = {
  DialogID = 1702,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER1_7_2"
}
Dialogues[1703] = {
  DialogID = 1703,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_7_3"
}
Dialogues[1704] = {
  DialogID = 1704,
  NPCHead = 12,
  HeadName = "NPC_1007005",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_7_4"
}
Dialogues[1705] = {
  DialogID = 1705,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_7_5"
}
Dialogues[1706] = {
  DialogID = 1706,
  NPCHead = 1,
  HeadName = "NPC_2",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_7_6"
}
Dialogues[1707] = {
  DialogID = 1707,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_7_7"
}
Dialogues[1708] = {
  DialogID = 1708,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_7_8"
}
Dialogues[1709] = {
  DialogID = 1709,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_7_9"
}
Dialogues[1710] = {
  DialogID = 1710,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_7_10"
}
Dialogues[1711] = {
  DialogID = 1711,
  NPCHead = 24,
  HeadName = "NPC_1007010",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_7_11"
}
Dialogues[1712] = {
  DialogID = 1712,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_7_12"
}
Dialogues[1713] = {
  DialogID = 1713,
  NPCHead = 1,
  HeadName = "NPC_2",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_7_13"
}
Dialogues[1714] = {
  DialogID = 1714,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_7_14"
}
Dialogues[1715] = {
  DialogID = 1715,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER1_7_15"
}
Dialogues[1716] = {
  DialogID = 1716,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_7_16"
}
Dialogues[1717] = {
  DialogID = 1717,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER1_7_17"
}
Dialogues[1801] = {
  DialogID = 1801,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER1_8_1"
}
Dialogues[1802] = {
  DialogID = 1802,
  NPCHead = 24,
  HeadName = "NPC_1008004",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_8_2"
}
Dialogues[1803] = {
  DialogID = 1803,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_8_3"
}
Dialogues[1804] = {
  DialogID = 1804,
  NPCHead = 4,
  HeadName = "NPC_5",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_8_4"
}
Dialogues[1805] = {
  DialogID = 1805,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER1_8_5"
}
Dialogues[1806] = {
  DialogID = 1806,
  NPCHead = 7,
  HeadName = "NPC_13",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_8_6"
}
Dialogues[1807] = {
  DialogID = 1807,
  NPCHead = 7,
  HeadName = "NPC_13",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_8_7"
}
Dialogues[1808] = {
  DialogID = 1808,
  NPCHead = 4,
  HeadName = "NPC_5",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_8_8"
}
Dialogues[1809] = {
  DialogID = 1809,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER1_8_9"
}
Dialogues[1810] = {
  DialogID = 1810,
  NPCHead = 4,
  HeadName = "NPC_5",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_8_10"
}
Dialogues[1811] = {
  DialogID = 1811,
  NPCHead = 4,
  HeadName = "NPC_5",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_8_11"
}
Dialogues[1812] = {
  DialogID = 1812,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER1_8_12"
}
Dialogues[1813] = {
  DialogID = 1813,
  NPCHead = 4,
  HeadName = "NPC_5",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_8_13"
}
Dialogues[1814] = {
  DialogID = 1814,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER1_8_14"
}
Dialogues[1815] = {
  DialogID = 1815,
  NPCHead = 4,
  HeadName = "NPC_5",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER1_8_15"
}
Dialogues[1816] = {
  DialogID = 1816,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER1_8_16"
}
Dialogues[2101] = {
  DialogID = 2101,
  NPCHead = 1,
  HeadName = "NPC_2",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER2_1_1"
}
Dialogues[2102] = {
  DialogID = 2102,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER2_1_2"
}
Dialogues[2103] = {
  DialogID = 2103,
  NPCHead = 25,
  HeadName = "NPC_2001005",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER2_1_3"
}
Dialogues[2104] = {
  DialogID = 2104,
  NPCHead = 24,
  HeadName = "NPC_2001010",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER2_1_4"
}
Dialogues[2105] = {
  DialogID = 2105,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER2_1_5"
}
Dialogues[2106] = {
  DialogID = 2106,
  NPCHead = 7,
  HeadName = "NPC_13",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER2_1_6"
}
Dialogues[2107] = {
  DialogID = 2107,
  NPCHead = 16,
  HeadName = "NPC_11",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER2_1_7"
}
Dialogues[2108] = {
  DialogID = 2108,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER2_1_8"
}
Dialogues[2109] = {
  DialogID = 2109,
  NPCHead = 1,
  HeadName = "NPC_2",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER2_1_9"
}
Dialogues[2110] = {
  DialogID = 2110,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER2_1_10"
}
Dialogues[2111] = {
  DialogID = 2111,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER2_1_11"
}
Dialogues[2201] = {
  DialogID = 2201,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER2_2_1"
}
Dialogues[2202] = {
  DialogID = 2202,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER2_2_2"
}
Dialogues[2203] = {
  DialogID = 2203,
  NPCHead = 22,
  HeadName = "NPC_8",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER2_2_3"
}
Dialogues[2204] = {
  DialogID = 2204,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER2_2_4"
}
Dialogues[2205] = {
  DialogID = 2205,
  NPCHead = 22,
  HeadName = "NPC_8",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER2_2_5"
}
Dialogues[2206] = {
  DialogID = 2206,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER2_2_6"
}
Dialogues[2207] = {
  DialogID = 2207,
  NPCHead = 22,
  HeadName = "NPC_8",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER2_2_7"
}
Dialogues[2208] = {
  DialogID = 2208,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER2_2_8"
}
Dialogues[2209] = {
  DialogID = 2209,
  NPCHead = 22,
  HeadName = "NPC_8",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER2_2_9"
}
Dialogues[2210] = {
  DialogID = 2210,
  NPCHead = 22,
  HeadName = "NPC_8",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER2_2_10"
}
Dialogues[2211] = {
  DialogID = 2211,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER2_2_11"
}
Dialogues[2212] = {
  DialogID = 2212,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER2_2_12"
}
Dialogues[2301] = {
  DialogID = 2301,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER2_3_1"
}
Dialogues[2302] = {
  DialogID = 2302,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER2_3_2"
}
Dialogues[2303] = {
  DialogID = 2303,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER2_3_3"
}
Dialogues[2304] = {
  DialogID = 2304,
  NPCHead = 24,
  HeadName = "NPC_2003005",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER2_3_4"
}
Dialogues[2305] = {
  DialogID = 2305,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER2_3_5"
}
Dialogues[2306] = {
  DialogID = 2306,
  NPCHead = 7,
  HeadName = "NPC_13",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER2_3_6"
}
Dialogues[2307] = {
  DialogID = 2307,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER2_3_7"
}
Dialogues[2308] = {
  DialogID = 2308,
  NPCHead = 12,
  HeadName = "NPC_2003010",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER2_3_8"
}
Dialogues[2309] = {
  DialogID = 2309,
  NPCHead = 1,
  HeadName = "NPC_2",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER2_3_9"
}
Dialogues[2310] = {
  DialogID = 2310,
  NPCHead = 1,
  HeadName = "NPC_2",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER2_3_10"
}
Dialogues[2311] = {
  DialogID = 2311,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER2_3_11"
}
Dialogues[2312] = {
  DialogID = 2312,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER2_3_12"
}
Dialogues[2313] = {
  DialogID = 2313,
  NPCHead = 4,
  HeadName = "NPC_5",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER2_3_13"
}
Dialogues[2314] = {
  DialogID = 2314,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER2_3_14"
}
Dialogues[2315] = {
  DialogID = 2315,
  NPCHead = 4,
  HeadName = "NPC_5",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER2_3_15"
}
Dialogues[2401] = {
  DialogID = 2401,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER2_4_1"
}
Dialogues[2402] = {
  DialogID = 2402,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER2_4_2"
}
Dialogues[2403] = {
  DialogID = 2403,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER2_4_3"
}
Dialogues[2404] = {
  DialogID = 2404,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER2_4_4"
}
Dialogues[2405] = {
  DialogID = 2405,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER2_4_5"
}
Dialogues[2501] = {
  DialogID = 2501,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER2_5_1"
}
Dialogues[2505] = {
  DialogID = 2505,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER2_5_5"
}
Dialogues[2506] = {
  DialogID = 2506,
  NPCHead = 2,
  HeadName = "NPC_12",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER2_5_6"
}
Dialogues[2507] = {
  DialogID = 2507,
  NPCHead = 2,
  HeadName = "NPC_12",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER2_5_7"
}
Dialogues[2502] = {
  DialogID = 2502,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER2_5_2"
}
Dialogues[2503] = {
  DialogID = 2503,
  NPCHead = 4,
  HeadName = "NPC_5",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER2_5_3"
}
Dialogues[2504] = {
  DialogID = 2504,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER2_5_4"
}
Dialogues[2601] = {
  DialogID = 2601,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER2_6_1"
}
Dialogues[2602] = {
  DialogID = 2602,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER2_6_2"
}
Dialogues[2603] = {
  DialogID = 2603,
  NPCHead = 3,
  HeadName = "NPC_10",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER2_6_3"
}
Dialogues[2604] = {
  DialogID = 2604,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER2_6_4"
}
Dialogues[2605] = {
  DialogID = 2605,
  NPCHead = 1,
  HeadName = "NPC_2",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER2_6_5"
}
Dialogues[2606] = {
  DialogID = 2606,
  NPCHead = 1,
  HeadName = "NPC_2",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER2_6_6"
}
Dialogues[2607] = {
  DialogID = 2607,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER2_6_7"
}
Dialogues[2608] = {
  DialogID = 2608,
  NPCHead = 3,
  HeadName = "NPC_10",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER2_6_8"
}
Dialogues[2701] = {
  DialogID = 2701,
  NPCHead = 3,
  HeadName = "NPC_10",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER2_7_1"
}
Dialogues[2702] = {
  DialogID = 2702,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER2_7_2"
}
Dialogues[2703] = {
  DialogID = 2703,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER2_7_3"
}
Dialogues[2704] = {
  DialogID = 2704,
  NPCHead = 1,
  HeadName = "NPC_2",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER2_7_4"
}
Dialogues[2705] = {
  DialogID = 2705,
  NPCHead = 7,
  HeadName = "NPC_13",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER2_7_5"
}
Dialogues[2706] = {
  DialogID = 2706,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER2_7_6"
}
Dialogues[2707] = {
  DialogID = 2707,
  NPCHead = 4,
  HeadName = "NPC_5",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER2_7_7"
}
Dialogues[2708] = {
  DialogID = 2708,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER2_7_8"
}
Dialogues[2709] = {
  DialogID = 2709,
  NPCHead = 4,
  HeadName = "NPC_5",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER2_7_9"
}
Dialogues[2801] = {
  DialogID = 2801,
  NPCHead = 3,
  HeadName = "NPC_10",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER2_8_1"
}
Dialogues[2802] = {
  DialogID = 2802,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER2_8_2"
}
Dialogues[2803] = {
  DialogID = 2803,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER2_8_3"
}
Dialogues[2804] = {
  DialogID = 2804,
  NPCHead = 9,
  HeadName = "NPC_7",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER2_8_4"
}
Dialogues[2805] = {
  DialogID = 2805,
  NPCHead = 9,
  HeadName = "NPC_7",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER2_8_5"
}
Dialogues[2806] = {
  DialogID = 2806,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER2_8_6"
}
Dialogues[2807] = {
  DialogID = 2807,
  NPCHead = 1,
  HeadName = "NPC_2",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER2_8_7"
}
Dialogues[2808] = {
  DialogID = 2808,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER2_8_8"
}
Dialogues[3101] = {
  DialogID = 3101,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER3_1_1"
}
Dialogues[3102] = {
  DialogID = 3102,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER3_1_2"
}
Dialogues[3103] = {
  DialogID = 3103,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER3_1_3"
}
Dialogues[3104] = {
  DialogID = 3104,
  NPCHead = 1,
  HeadName = "NPC_2",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER3_1_4"
}
Dialogues[3105] = {
  DialogID = 3105,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER3_1_5"
}
Dialogues[3106] = {
  DialogID = 3106,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER3_1_6"
}
Dialogues[3201] = {
  DialogID = 3201,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER3_2_1"
}
Dialogues[3202] = {
  DialogID = 3202,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER3_2_2"
}
Dialogues[3203] = {
  DialogID = 3203,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER3_2_3"
}
Dialogues[3204] = {
  DialogID = 3204,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER3_2_4"
}
Dialogues[3205] = {
  DialogID = 3205,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER3_2_5"
}
Dialogues[3206] = {
  DialogID = 3206,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER3_2_6"
}
Dialogues[3207] = {
  DialogID = 3207,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER3_2_7"
}
Dialogues[3208] = {
  DialogID = 3208,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER3_2_8"
}
Dialogues[3209] = {
  DialogID = 3209,
  NPCHead = 1,
  HeadName = "NPC_2",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER3_2_9"
}
Dialogues[3301] = {
  DialogID = 3301,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER3_3_1"
}
Dialogues[3302] = {
  DialogID = 3302,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER3_3_2"
}
Dialogues[3303] = {
  DialogID = 3303,
  NPCHead = 36,
  HeadName = "NPC_51",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER3_3_3"
}
Dialogues[3304] = {
  DialogID = 3304,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER3_3_4"
}
Dialogues[3305] = {
  DialogID = 3305,
  NPCHead = 36,
  HeadName = "NPC_51",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER3_3_5"
}
Dialogues[3306] = {
  DialogID = 3306,
  NPCHead = 9,
  HeadName = "NPC_7",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER3_3_6"
}
Dialogues[3307] = {
  DialogID = 3307,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER3_3_7"
}
Dialogues[3308] = {
  DialogID = 3308,
  NPCHead = 4,
  HeadName = "NPC_5",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER3_3_8"
}
Dialogues[3309] = {
  DialogID = 3309,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER3_3_9"
}
Dialogues[3310] = {
  DialogID = 3310,
  NPCHead = 4,
  HeadName = "NPC_5",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER3_3_10"
}
Dialogues[3311] = {
  DialogID = 3311,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER3_3_11"
}
Dialogues[3401] = {
  DialogID = 3401,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER3_4_1"
}
Dialogues[3402] = {
  DialogID = 3402,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER3_4_2"
}
Dialogues[3403] = {
  DialogID = 3403,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER3_4_3"
}
Dialogues[3404] = {
  DialogID = 3404,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER3_4_4"
}
Dialogues[3405] = {
  DialogID = 3405,
  NPCHead = 36,
  HeadName = "NPC_51",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER3_4_5"
}
Dialogues[3406] = {
  DialogID = 3406,
  NPCHead = 1,
  HeadName = "NPC_2",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER3_4_6"
}
Dialogues[3407] = {
  DialogID = 3407,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER3_4_7"
}
Dialogues[3408] = {
  DialogID = 3408,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER3_4_8"
}
Dialogues[3409] = {
  DialogID = 3409,
  NPCHead = 1,
  HeadName = "NPC_2",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER3_4_9"
}
Dialogues[3410] = {
  DialogID = 3410,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER3_4_10"
}
Dialogues[3501] = {
  DialogID = 3501,
  NPCHead = 36,
  HeadName = "NPC_51",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER3_5_1"
}
Dialogues[3502] = {
  DialogID = 3502,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER3_5_2"
}
Dialogues[3503] = {
  DialogID = 3503,
  NPCHead = 36,
  HeadName = "NPC_51",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER3_5_3"
}
Dialogues[3504] = {
  DialogID = 3504,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER3_5_4"
}
Dialogues[3505] = {
  DialogID = 3505,
  NPCHead = 36,
  HeadName = "NPC_51",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER3_5_5"
}
Dialogues[3506] = {
  DialogID = 3506,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER3_5_6"
}
Dialogues[3507] = {
  DialogID = 3507,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER3_5_7"
}
Dialogues[3508] = {
  DialogID = 3508,
  NPCHead = 36,
  HeadName = "NPC_51",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER3_5_8"
}
Dialogues[3509] = {
  DialogID = 3509,
  NPCHead = 36,
  HeadName = "NPC_51",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER3_5_9"
}
Dialogues[3510] = {
  DialogID = 3510,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER3_5_10"
}
Dialogues[3511] = {
  DialogID = 3511,
  NPCHead = 36,
  HeadName = "NPC_51",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER3_5_11"
}
Dialogues[3512] = {
  DialogID = 3512,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER3_5_12"
}
Dialogues[3513] = {
  DialogID = 3513,
  NPCHead = 1,
  HeadName = "NPC_2",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER3_5_13"
}
Dialogues[3514] = {
  DialogID = 3514,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER3_5_14"
}
Dialogues[3515] = {
  DialogID = 3515,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER3_5_15"
}
Dialogues[3516] = {
  DialogID = 3516,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER3_5_16"
}
Dialogues[3601] = {
  DialogID = 3601,
  NPCHead = 36,
  HeadName = "NPC_51",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER3_6_1"
}
Dialogues[3602] = {
  DialogID = 3602,
  NPCHead = 1,
  HeadName = "NPC_2",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER3_6_2"
}
Dialogues[3603] = {
  DialogID = 3603,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER3_6_3"
}
Dialogues[3604] = {
  DialogID = 3604,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER3_6_4"
}
Dialogues[3605] = {
  DialogID = 3605,
  NPCHead = 36,
  HeadName = "NPC_51",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER3_6_5"
}
Dialogues[3606] = {
  DialogID = 3606,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER3_6_6"
}
Dialogues[3607] = {
  DialogID = 3607,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER3_6_7"
}
Dialogues[3701] = {
  DialogID = 3701,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER3_7_1"
}
Dialogues[3702] = {
  DialogID = 3702,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER3_7_2"
}
Dialogues[3703] = {
  DialogID = 3703,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER3_7_3"
}
Dialogues[3704] = {
  DialogID = 3704,
  NPCHead = 36,
  HeadName = "NPC_51",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER3_7_4"
}
Dialogues[3705] = {
  DialogID = 3705,
  NPCHead = 1,
  HeadName = "NPC_2",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER3_7_5"
}
Dialogues[3706] = {
  DialogID = 3706,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER3_7_6"
}
Dialogues[3707] = {
  DialogID = 3707,
  NPCHead = 4,
  HeadName = "NPC_5",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER3_7_7"
}
Dialogues[3801] = {
  DialogID = 3801,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER3_8_1"
}
Dialogues[3802] = {
  DialogID = 3802,
  NPCHead = 1,
  HeadName = "NPC_2",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER3_8_2"
}
Dialogues[3803] = {
  DialogID = 3803,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER3_8_3"
}
Dialogues[3804] = {
  DialogID = 3804,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER3_8_4"
}
Dialogues[3805] = {
  DialogID = 3805,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER3_8_5"
}
Dialogues[3806] = {
  DialogID = 3806,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER3_8_6"
}
Dialogues[3807] = {
  DialogID = 3807,
  NPCHead = 1,
  HeadName = "NPC_2",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER3_8_7"
}
Dialogues[3808] = {
  DialogID = 3808,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER3_8_8"
}
Dialogues[3809] = {
  DialogID = 3809,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER3_8_9"
}
Dialogues[3810] = {
  DialogID = 3810,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER3_8_10"
}
Dialogues[3811] = {
  DialogID = 3811,
  NPCHead = 29,
  HeadName = "NPC_35",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER3_8_11"
}
Dialogues[4101] = {
  DialogID = 4101,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER4_1_1"
}
Dialogues[4102] = {
  DialogID = 4102,
  NPCHead = 1,
  HeadName = "NPC_2",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER4_1_2"
}
Dialogues[4103] = {
  DialogID = 4103,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER4_1_3"
}
Dialogues[4104] = {
  DialogID = 4104,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER4_1_4"
}
Dialogues[4105] = {
  DialogID = 4105,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER4_1_5"
}
Dialogues[4106] = {
  DialogID = 4106,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER4_1_6"
}
Dialogues[4107] = {
  DialogID = 4107,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER4_1_7"
}
Dialogues[4201] = {
  DialogID = 4201,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER4_2_1"
}
Dialogues[4202] = {
  DialogID = 4202,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER4_2_2"
}
Dialogues[4203] = {
  DialogID = 4203,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER4_2_3"
}
Dialogues[4204] = {
  DialogID = 4204,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER4_2_4"
}
Dialogues[4205] = {
  DialogID = 4205,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER4_2_5"
}
Dialogues[4206] = {
  DialogID = 4206,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER4_2_6"
}
Dialogues[4301] = {
  DialogID = 4301,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER4_3_1"
}
Dialogues[4302] = {
  DialogID = 4302,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER4_3_2"
}
Dialogues[4303] = {
  DialogID = 4303,
  NPCHead = 1,
  HeadName = "NPC_2",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER4_3_3"
}
Dialogues[4304] = {
  DialogID = 4304,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER4_3_4"
}
Dialogues[4305] = {
  DialogID = 4305,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER4_3_5"
}
Dialogues[4306] = {
  DialogID = 4306,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER4_3_6"
}
Dialogues[4307] = {
  DialogID = 4307,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER4_3_7"
}
Dialogues[4308] = {
  DialogID = 4308,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER4_3_8"
}
Dialogues[4309] = {
  DialogID = 4309,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER4_3_9"
}
Dialogues[4310] = {
  DialogID = 4310,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER4_3_10"
}
Dialogues[4401] = {
  DialogID = 4401,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER4_4_1"
}
Dialogues[4402] = {
  DialogID = 4402,
  NPCHead = 13,
  HeadName = "NPC_29",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER4_4_2"
}
Dialogues[4403] = {
  DialogID = 4403,
  NPCHead = 35,
  HeadName = "NPC_51",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER4_4_3"
}
Dialogues[4404] = {
  DialogID = 4404,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER4_4_4"
}
Dialogues[4405] = {
  DialogID = 4405,
  NPCHead = 35,
  HeadName = "NPC_51",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER4_4_5"
}
Dialogues[4406] = {
  DialogID = 4406,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER4_4_6"
}
Dialogues[4407] = {
  DialogID = 4407,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER4_4_7"
}
Dialogues[4408] = {
  DialogID = 4408,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER4_4_8"
}
Dialogues[4501] = {
  DialogID = 4501,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER4_5_1"
}
Dialogues[4502] = {
  DialogID = 4502,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER4_5_2"
}
Dialogues[4503] = {
  DialogID = 4503,
  NPCHead = 1,
  HeadName = "NPC_2",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER4_5_3"
}
Dialogues[4504] = {
  DialogID = 4504,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER4_5_4"
}
Dialogues[4505] = {
  DialogID = 4505,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER4_5_5"
}
Dialogues[4506] = {
  DialogID = 4506,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER4_5_6"
}
Dialogues[4507] = {
  DialogID = 4507,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER4_5_7"
}
Dialogues[4508] = {
  DialogID = 4508,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER4_5_8"
}
Dialogues[4509] = {
  DialogID = 4509,
  NPCHead = 1,
  HeadName = "NPC_2",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER4_5_9"
}
Dialogues[4601] = {
  DialogID = 4601,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER4_6_1"
}
Dialogues[4602] = {
  DialogID = 4602,
  NPCHead = 39,
  HeadName = "NPC_51",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER4_6_2"
}
Dialogues[4603] = {
  DialogID = 4603,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER4_6_3"
}
Dialogues[4604] = {
  DialogID = 4604,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER4_6_4"
}
Dialogues[4605] = {
  DialogID = 4605,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER4_6_5"
}
Dialogues[4606] = {
  DialogID = 4606,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER4_6_6"
}
Dialogues[4701] = {
  DialogID = 4701,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER4_7_1"
}
Dialogues[4702] = {
  DialogID = 4702,
  NPCHead = 20,
  HeadName = "NPC_32",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER4_7_2"
}
Dialogues[4703] = {
  DialogID = 4703,
  NPCHead = 1,
  HeadName = "NPC_2",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER4_7_3"
}
Dialogues[4704] = {
  DialogID = 4704,
  NPCHead = 20,
  HeadName = "NPC_32",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER4_7_4"
}
Dialogues[4705] = {
  DialogID = 4705,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER4_7_5"
}
Dialogues[4706] = {
  DialogID = 4706,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER4_7_6"
}
Dialogues[4707] = {
  DialogID = 4707,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER4_7_7"
}
Dialogues[4708] = {
  DialogID = 4708,
  NPCHead = 20,
  HeadName = "NPC_32",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER4_7_8"
}
Dialogues[4709] = {
  DialogID = 4709,
  NPCHead = 1,
  HeadName = "NPC_2",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER4_7_9"
}
Dialogues[4710] = {
  DialogID = 4710,
  NPCHead = 20,
  HeadName = "NPC_32",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER4_7_10"
}
Dialogues[4801] = {
  DialogID = 4801,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER4_8_1"
}
Dialogues[4802] = {
  DialogID = 4802,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER4_8_2"
}
Dialogues[4803] = {
  DialogID = 4803,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER4_8_3"
}
Dialogues[4804] = {
  DialogID = 4804,
  NPCHead = 1,
  HeadName = "NPC_2",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER4_8_4"
}
Dialogues[4805] = {
  DialogID = 4805,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER4_8_5"
}
Dialogues[4806] = {
  DialogID = 4806,
  NPCHead = 4,
  HeadName = "NPC_5",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER4_8_6"
}
Dialogues[4807] = {
  DialogID = 4807,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER4_8_7"
}
Dialogues[4808] = {
  DialogID = 4808,
  NPCHead = 4,
  HeadName = "NPC_5",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER4_8_8"
}
Dialogues[4809] = {
  DialogID = 4809,
  NPCHead = 1,
  HeadName = "NPC_2",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER4_8_9"
}
Dialogues[4810] = {
  DialogID = 4810,
  NPCHead = 4,
  HeadName = "NPC_5",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER4_8_10"
}
Dialogues[4811] = {
  DialogID = 4811,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER4_8_11"
}
Dialogues[4812] = {
  DialogID = 4812,
  NPCHead = 4,
  HeadName = "NPC_5",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER4_8_12"
}
Dialogues[4813] = {
  DialogID = 4813,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER4_8_13"
}
Dialogues[4814] = {
  DialogID = 4814,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER4_8_14"
}
Dialogues[4815] = {
  DialogID = 4815,
  NPCHead = 20,
  HeadName = "NPC_32",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER4_8_15"
}
Dialogues[4816] = {
  DialogID = 4816,
  NPCHead = 31,
  HeadName = "NPC_26",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER4_8_16"
}
Dialogues[4817] = {
  DialogID = 4817,
  NPCHead = 20,
  HeadName = "NPC_32",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER4_8_17"
}
Dialogues[4818] = {
  DialogID = 4818,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER4_8_18"
}
Dialogues[4819] = {
  DialogID = 4819,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER4_8_19"
}
Dialogues[4820] = {
  DialogID = 4820,
  NPCHead = 31,
  HeadName = "NPC_26",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER4_8_20"
}
Dialogues[4821] = {
  DialogID = 4821,
  NPCHead = 20,
  HeadName = "NPC_32",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER4_8_21"
}
Dialogues[4822] = {
  DialogID = 4822,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER4_8_22"
}
Dialogues[5101] = {
  DialogID = 5101,
  NPCHead = 20,
  HeadName = "NPC_32",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER5_1_1"
}
Dialogues[5102] = {
  DialogID = 5102,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER5_1_2"
}
Dialogues[5103] = {
  DialogID = 5103,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER5_1_3"
}
Dialogues[5104] = {
  DialogID = 5104,
  NPCHead = 1,
  HeadName = "NPC_2",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER5_1_4"
}
Dialogues[5105] = {
  DialogID = 5105,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER5_1_5"
}
Dialogues[5106] = {
  DialogID = 5106,
  NPCHead = 20,
  HeadName = "NPC_32",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER5_1_6"
}
Dialogues[5107] = {
  DialogID = 5107,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER5_1_7"
}
Dialogues[5108] = {
  DialogID = 5108,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER5_1_8"
}
Dialogues[5201] = {
  DialogID = 5201,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER5_2_1"
}
Dialogues[5202] = {
  DialogID = 5202,
  NPCHead = 28,
  HeadName = "NPC_37",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER5_2_2"
}
Dialogues[5203] = {
  DialogID = 5203,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER5_2_3"
}
Dialogues[5204] = {
  DialogID = 5204,
  NPCHead = 28,
  HeadName = "NPC_37",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER5_2_4"
}
Dialogues[5205] = {
  DialogID = 5205,
  NPCHead = 1,
  HeadName = "NPC_2",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER5_2_5"
}
Dialogues[5206] = {
  DialogID = 5206,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER5_2_6"
}
Dialogues[5207] = {
  DialogID = 5207,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER5_2_7"
}
Dialogues[5208] = {
  DialogID = 5208,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER5_2_8"
}
Dialogues[5209] = {
  DialogID = 5209,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER5_2_9"
}
Dialogues[5301] = {
  DialogID = 5301,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER5_3_1"
}
Dialogues[5302] = {
  DialogID = 5302,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER5_3_2"
}
Dialogues[5303] = {
  DialogID = 5303,
  NPCHead = 27,
  HeadName = "NPC_38",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER5_3_3"
}
Dialogues[5304] = {
  DialogID = 5304,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER5_3_4"
}
Dialogues[5401] = {
  DialogID = 5401,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER5_4_1"
}
Dialogues[5402] = {
  DialogID = 5402,
  NPCHead = 21,
  HeadName = "NPC_36",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER5_4_2"
}
Dialogues[5403] = {
  DialogID = 5403,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER5_4_3"
}
Dialogues[5404] = {
  DialogID = 5404,
  NPCHead = 21,
  HeadName = "NPC_36",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER5_4_4"
}
Dialogues[5405] = {
  DialogID = 5405,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER5_4_5"
}
Dialogues[5406] = {
  DialogID = 5406,
  NPCHead = 31,
  HeadName = "NPC_26",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER5_4_6"
}
Dialogues[5407] = {
  DialogID = 5407,
  NPCHead = 20,
  HeadName = "NPC_32",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER5_4_7"
}
Dialogues[5408] = {
  DialogID = 5408,
  NPCHead = 31,
  HeadName = "NPC_26",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER5_4_8"
}
Dialogues[5409] = {
  DialogID = 5409,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER5_4_9"
}
Dialogues[5501] = {
  DialogID = 5501,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER5_5_1"
}
Dialogues[5502] = {
  DialogID = 5502,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER5_5_2"
}
Dialogues[5503] = {
  DialogID = 5503,
  NPCHead = 31,
  HeadName = "NPC_26",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER5_5_3"
}
Dialogues[5504] = {
  DialogID = 5504,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER5_5_4"
}
Dialogues[5505] = {
  DialogID = 5505,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER5_5_5"
}
Dialogues[5506] = {
  DialogID = 5506,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER5_5_6"
}
Dialogues[5507] = {
  DialogID = 5507,
  NPCHead = 31,
  HeadName = "NPC_26",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER5_5_7"
}
Dialogues[5508] = {
  DialogID = 5508,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER5_5_8"
}
Dialogues[5601] = {
  DialogID = 5601,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER5_6_1"
}
Dialogues[5602] = {
  DialogID = 5602,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER5_6_2"
}
Dialogues[5603] = {
  DialogID = 5603,
  NPCHead = 31,
  HeadName = "NPC_26",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER5_6_3"
}
Dialogues[5604] = {
  DialogID = 5604,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER5_6_4"
}
Dialogues[5605] = {
  DialogID = 5605,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER5_6_5"
}
Dialogues[5606] = {
  DialogID = 5606,
  NPCHead = 31,
  HeadName = "NPC_26",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER5_6_6"
}
Dialogues[5607] = {
  DialogID = 5607,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER5_6_7"
}
Dialogues[5608] = {
  DialogID = 5608,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER5_6_8"
}
Dialogues[5701] = {
  DialogID = 5701,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER5_7_1"
}
Dialogues[5702] = {
  DialogID = 5702,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER5_7_2"
}
Dialogues[5703] = {
  DialogID = 5703,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER5_7_3"
}
Dialogues[5704] = {
  DialogID = 5704,
  NPCHead = 1,
  HeadName = "NPC_2",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER5_7_4"
}
Dialogues[5705] = {
  DialogID = 5705,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER5_7_5"
}
Dialogues[5706] = {
  DialogID = 5706,
  NPCHead = 1,
  HeadName = "NPC_2",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER5_7_6"
}
Dialogues[5707] = {
  DialogID = 5707,
  NPCHead = 5,
  HeadName = "NPC_4",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER5_7_7"
}
Dialogues[5708] = {
  DialogID = 5708,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER5_7_8"
}
Dialogues[5709] = {
  DialogID = 5709,
  NPCHead = 5,
  HeadName = "NPC_4",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER5_7_9"
}
Dialogues[5710] = {
  DialogID = 5710,
  NPCHead = 5,
  HeadName = "NPC_4",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER5_7_10"
}
Dialogues[5801] = {
  DialogID = 5801,
  NPCHead = 5,
  HeadName = "NPC_4",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER5_8_1"
}
Dialogues[5802] = {
  DialogID = 5802,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER5_8_2"
}
Dialogues[5803] = {
  DialogID = 5803,
  NPCHead = 4,
  HeadName = "NPC_5",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER5_8_3"
}
Dialogues[5804] = {
  DialogID = 5804,
  NPCHead = 20,
  HeadName = "NPC_32",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER5_8_4"
}
Dialogues[5805] = {
  DialogID = 5805,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER5_8_5"
}
Dialogues[5806] = {
  DialogID = 5806,
  NPCHead = 20,
  HeadName = "NPC_32",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER5_8_6"
}
Dialogues[5807] = {
  DialogID = 5807,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER5_8_7"
}
Dialogues[5808] = {
  DialogID = 5808,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER5_8_8"
}
Dialogues[6101] = {
  DialogID = 6101,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER6_1_1"
}
Dialogues[6102] = {
  DialogID = 6102,
  NPCHead = 13,
  HeadName = "NPC_52",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER6_1_2"
}
Dialogues[6103] = {
  DialogID = 6103,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER6_1_3"
}
Dialogues[6104] = {
  DialogID = 6104,
  NPCHead = 13,
  HeadName = "NPC_52",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER6_1_4"
}
Dialogues[6105] = {
  DialogID = 6105,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER6_1_5"
}
Dialogues[6106] = {
  DialogID = 6106,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER6_1_6"
}
Dialogues[6107] = {
  DialogID = 6107,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER6_1_7"
}
Dialogues[6201] = {
  DialogID = 6201,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER6_2_1"
}
Dialogues[6202] = {
  DialogID = 6202,
  NPCHead = 15,
  HeadName = "NPC_34",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER6_2_2"
}
Dialogues[6203] = {
  DialogID = 6203,
  NPCHead = 1,
  HeadName = "NPC_2",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER6_2_3"
}
Dialogues[6204] = {
  DialogID = 6204,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER6_2_4"
}
Dialogues[6205] = {
  DialogID = 6205,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER6_2_5"
}
Dialogues[6206] = {
  DialogID = 6206,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER6_2_6"
}
Dialogues[6301] = {
  DialogID = 6301,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER6_3_1"
}
Dialogues[6302] = {
  DialogID = 6302,
  NPCHead = 13,
  HeadName = "NPC_52",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER6_3_2"
}
Dialogues[6303] = {
  DialogID = 6303,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER6_3_3"
}
Dialogues[6304] = {
  DialogID = 6304,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER6_3_4"
}
Dialogues[6305] = {
  DialogID = 6305,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER6_3_5"
}
Dialogues[6306] = {
  DialogID = 6306,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER6_3_6"
}
Dialogues[6401] = {
  DialogID = 6401,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER6_4_1"
}
Dialogues[6402] = {
  DialogID = 6402,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER6_4_2"
}
Dialogues[6403] = {
  DialogID = 6403,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER6_4_3"
}
Dialogues[6404] = {
  DialogID = 6404,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER6_4_4"
}
Dialogues[6405] = {
  DialogID = 6405,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER6_4_5"
}
Dialogues[6406] = {
  DialogID = 6406,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER6_4_6"
}
Dialogues[6501] = {
  DialogID = 6501,
  NPCHead = 1,
  HeadName = "NPC_2",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER6_5_1"
}
Dialogues[6502] = {
  DialogID = 6502,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER6_5_2"
}
Dialogues[6503] = {
  DialogID = 6503,
  NPCHead = 31,
  HeadName = "NPC_26",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER6_5_3"
}
Dialogues[6504] = {
  DialogID = 6504,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER6_5_4"
}
Dialogues[6601] = {
  DialogID = 6601,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER6_6_1"
}
Dialogues[6602] = {
  DialogID = 6602,
  NPCHead = 26,
  HeadName = "NPC_6005005",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER6_6_2"
}
Dialogues[6603] = {
  DialogID = 6603,
  NPCHead = 1,
  HeadName = "NPC_2",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER6_6_3"
}
Dialogues[6604] = {
  DialogID = 6604,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER6_6_4"
}
Dialogues[6605] = {
  DialogID = 6605,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER6_6_5"
}
Dialogues[6606] = {
  DialogID = 6606,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER6_6_6"
}
Dialogues[6607] = {
  DialogID = 6607,
  NPCHead = 1,
  HeadName = "NPC_2",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER6_6_7"
}
Dialogues[6608] = {
  DialogID = 6608,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER6_6_8"
}
Dialogues[6701] = {
  DialogID = 6701,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER6_7_1"
}
Dialogues[6702] = {
  DialogID = 6702,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER6_7_2"
}
Dialogues[6703] = {
  DialogID = 6703,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER6_7_3"
}
Dialogues[6704] = {
  DialogID = 6704,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER6_7_4"
}
Dialogues[6705] = {
  DialogID = 6705,
  NPCHead = 1,
  HeadName = "NPC_2",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER6_7_5"
}
Dialogues[6706] = {
  DialogID = 6706,
  NPCHead = 17,
  HeadName = "NPC_1",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER6_7_6"
}
Dialogues[6707] = {
  DialogID = 6707,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER6_7_7"
}
Dialogues[6708] = {
  DialogID = 6708,
  NPCHead = 17,
  HeadName = "NPC_1",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER6_7_8"
}
Dialogues[6709] = {
  DialogID = 6709,
  NPCHead = 17,
  HeadName = "NPC_1",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER6_7_9"
}
Dialogues[6710] = {
  DialogID = 6710,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER6_7_10"
}
Dialogues[6711] = {
  DialogID = 6711,
  NPCHead = 17,
  HeadName = "NPC_1",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER6_7_11"
}
Dialogues[6712] = {
  DialogID = 6712,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER6_7_12"
}
Dialogues[6713] = {
  DialogID = 6713,
  NPCHead = 17,
  HeadName = "NPC_1",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER6_7_13"
}
Dialogues[6714] = {
  DialogID = 6714,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER6_7_14"
}
Dialogues[6801] = {
  DialogID = 6801,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER6_8_1"
}
Dialogues[6802] = {
  DialogID = 6802,
  NPCHead = 4,
  HeadName = "NPC_5",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER6_8_2"
}
Dialogues[6803] = {
  DialogID = 6803,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER6_8_3"
}
Dialogues[6804] = {
  DialogID = 6804,
  NPCHead = 4,
  HeadName = "NPC_5",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER6_8_4"
}
Dialogues[6805] = {
  DialogID = 6805,
  NPCHead = 4,
  HeadName = "NPC_5",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER6_8_5"
}
Dialogues[6806] = {
  DialogID = 6806,
  NPCHead = 31,
  HeadName = "NPC_26",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER6_8_6"
}
Dialogues[6807] = {
  DialogID = 6807,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER6_8_7"
}
Dialogues[6808] = {
  DialogID = 6808,
  NPCHead = 4,
  HeadName = "NPC_5",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER6_8_8"
}
Dialogues[6809] = {
  DialogID = 6809,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER6_8_9"
}
Dialogues[6810] = {
  DialogID = 6810,
  NPCHead = 31,
  HeadName = "NPC_26",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER6_8_10"
}
Dialogues[6811] = {
  DialogID = 6811,
  NPCHead = 4,
  HeadName = "NPC_5",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER6_8_11"
}
Dialogues[6812] = {
  DialogID = 6812,
  NPCHead = 20,
  HeadName = "NPC_32",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER6_8_12"
}
Dialogues[6813] = {
  DialogID = 6813,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER6_8_13"
}
Dialogues[6814] = {
  DialogID = 6814,
  NPCHead = 20,
  HeadName = "NPC_32",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER6_8_14"
}
Dialogues[6815] = {
  DialogID = 6815,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER6_8_15"
}
Dialogues[6816] = {
  DialogID = 6816,
  NPCHead = 20,
  HeadName = "NPC_32",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER6_8_16"
}
Dialogues[6817] = {
  DialogID = 6817,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER6_8_17"
}
Dialogues[6818] = {
  DialogID = 6818,
  NPCHead = 1,
  HeadName = "NPC_2",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER6_8_18"
}
Dialogues[6819] = {
  DialogID = 6819,
  NPCHead = 1,
  HeadName = "NPC_2",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER6_8_19"
}
Dialogues[6820] = {
  DialogID = 6820,
  NPCHead = 31,
  HeadName = "NPC_26",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER6_8_20"
}
Dialogues[6821] = {
  DialogID = 6821,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER6_8_21"
}
Dialogues[6822] = {
  DialogID = 6822,
  NPCHead = 31,
  HeadName = "NPC_26",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER6_8_22"
}
Dialogues[7101] = {
  DialogID = 7101,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER7_1_1"
}
Dialogues[7102] = {
  DialogID = 7102,
  NPCHead = 31,
  HeadName = "NPC_26",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER7_1_2"
}
Dialogues[7103] = {
  DialogID = 7103,
  NPCHead = 41,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER7_1_3"
}
Dialogues[7104] = {
  DialogID = 7104,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER7_1_4"
}
Dialogues[7105] = {
  DialogID = 7105,
  NPCHead = 20,
  HeadName = "NPC_32",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER7_1_5"
}
Dialogues[7106] = {
  DialogID = 7106,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER7_1_6"
}
Dialogues[7107] = {
  DialogID = 7107,
  NPCHead = 20,
  HeadName = "NPC_32",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER7_1_7"
}
Dialogues[7201] = {
  DialogID = 7201,
  NPCHead = 31,
  HeadName = "NPC_26",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER7_2_1"
}
Dialogues[7202] = {
  DialogID = 7202,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER7_2_2"
}
Dialogues[7203] = {
  DialogID = 7203,
  NPCHead = 36,
  HeadName = "NPC_7203",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER7_2_3"
}
Dialogues[7204] = {
  DialogID = 7204,
  NPCHead = 36,
  HeadName = "NPC_7203",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER7_2_4"
}
Dialogues[7205] = {
  DialogID = 7205,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER7_2_5"
}
Dialogues[7206] = {
  DialogID = 7206,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER7_2_6"
}
Dialogues[7207] = {
  DialogID = 7207,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER7_2_7"
}
Dialogues[7301] = {
  DialogID = 7301,
  NPCHead = 23,
  HeadName = "NPC_52",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER7_3_1"
}
Dialogues[7302] = {
  DialogID = 7302,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER7_3_2"
}
Dialogues[7303] = {
  DialogID = 7303,
  NPCHead = 36,
  HeadName = "NPC_7203",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER7_3_3"
}
Dialogues[7304] = {
  DialogID = 7304,
  NPCHead = 36,
  HeadName = "NPC_7203",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER7_3_4"
}
Dialogues[7305] = {
  DialogID = 7305,
  NPCHead = 23,
  HeadName = "NPC_52",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER7_3_5"
}
Dialogues[7306] = {
  DialogID = 7306,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER7_3_6"
}
Dialogues[7307] = {
  DialogID = 7307,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER7_3_7"
}
Dialogues[7401] = {
  DialogID = 7401,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER7_4_1"
}
Dialogues[7402] = {
  DialogID = 7402,
  NPCHead = 31,
  HeadName = "NPC_26",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER7_4_2"
}
Dialogues[7403] = {
  DialogID = 7403,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER7_4_3"
}
Dialogues[7404] = {
  DialogID = 7404,
  NPCHead = 36,
  HeadName = "NPC_7203",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER7_4_4"
}
Dialogues[7405] = {
  DialogID = 7405,
  NPCHead = 36,
  HeadName = "NPC_7203",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER7_4_5"
}
Dialogues[7406] = {
  DialogID = 7406,
  NPCHead = 31,
  HeadName = "NPC_26",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER7_4_6"
}
Dialogues[7407] = {
  DialogID = 7407,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER7_4_7"
}
Dialogues[7408] = {
  DialogID = 7408,
  NPCHead = 32,
  HeadName = "NPC_7407",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER7_4_8"
}
Dialogues[7409] = {
  DialogID = 7409,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER7_4_9"
}
Dialogues[7410] = {
  DialogID = 7410,
  NPCHead = 31,
  HeadName = "NPC_26",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER7_4_10"
}
Dialogues[7411] = {
  DialogID = 7411,
  NPCHead = 32,
  HeadName = "NPC_7407",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER7_4_11"
}
Dialogues[7412] = {
  DialogID = 7412,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER7_4_12"
}
Dialogues[7413] = {
  DialogID = 7413,
  NPCHead = 32,
  HeadName = "NPC_7407",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER7_4_13"
}
Dialogues[7414] = {
  DialogID = 7414,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER7_4_14"
}
Dialogues[7501] = {
  DialogID = 7501,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER7_5_1"
}
Dialogues[7502] = {
  DialogID = 7502,
  NPCHead = 32,
  HeadName = "NPC_28",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER7_5_2"
}
Dialogues[7503] = {
  DialogID = 7503,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER7_5_3"
}
Dialogues[7504] = {
  DialogID = 7504,
  NPCHead = 1,
  HeadName = "NPC_2",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER7_5_4"
}
Dialogues[7505] = {
  DialogID = 7505,
  NPCHead = 32,
  HeadName = "NPC_28",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER7_5_5"
}
Dialogues[7506] = {
  DialogID = 7506,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER7_5_6"
}
Dialogues[7507] = {
  DialogID = 7507,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER7_5_7"
}
Dialogues[7508] = {
  DialogID = 7508,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER7_5_8"
}
Dialogues[7509] = {
  DialogID = 7509,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER7_5_9"
}
Dialogues[7510] = {
  DialogID = 7510,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER7_5_10"
}
Dialogues[7601] = {
  DialogID = 7601,
  NPCHead = 31,
  HeadName = "NPC_26",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER7_6_1"
}
Dialogues[7602] = {
  DialogID = 7602,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER7_6_2"
}
Dialogues[7603] = {
  DialogID = 7603,
  NPCHead = 37,
  HeadName = "NPC_7603",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER7_6_3"
}
Dialogues[7604] = {
  DialogID = 7604,
  NPCHead = 33,
  HeadName = "NPC_23",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER7_6_4"
}
Dialogues[7605] = {
  DialogID = 7605,
  NPCHead = 33,
  HeadName = "NPC_23",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER7_6_5"
}
Dialogues[7606] = {
  DialogID = 7606,
  NPCHead = 31,
  HeadName = "NPC_26",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER7_6_6"
}
Dialogues[7607] = {
  DialogID = 7607,
  NPCHead = 33,
  HeadName = "NPC_23",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER7_6_7"
}
Dialogues[7608] = {
  DialogID = 7608,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER7_6_8"
}
Dialogues[7609] = {
  DialogID = 7609,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER7_6_9"
}
Dialogues[7701] = {
  DialogID = 7701,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER7_7_1"
}
Dialogues[7702] = {
  DialogID = 7702,
  NPCHead = 17,
  HeadName = "NPC_1",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER7_7_2"
}
Dialogues[7703] = {
  DialogID = 7703,
  NPCHead = 20,
  HeadName = "NPC_32",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER7_7_3"
}
Dialogues[7704] = {
  DialogID = 7704,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER7_7_4"
}
Dialogues[7705] = {
  DialogID = 7705,
  NPCHead = 17,
  HeadName = "NPC_1",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER7_7_5"
}
Dialogues[7706] = {
  DialogID = 7706,
  NPCHead = 17,
  HeadName = "NPC_1",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER7_7_6"
}
Dialogues[7707] = {
  DialogID = 7707,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER7_7_7"
}
Dialogues[7708] = {
  DialogID = 7708,
  NPCHead = 40,
  HeadName = "NPC_17",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER7_7_8"
}
Dialogues[7709] = {
  DialogID = 7709,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER7_7_9"
}
Dialogues[7710] = {
  DialogID = 7710,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER7_7_10"
}
Dialogues[7801] = {
  DialogID = 7801,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER7_8_1"
}
Dialogues[7802] = {
  DialogID = 7802,
  NPCHead = 38,
  HeadName = "NPC_7802",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER7_8_2"
}
Dialogues[7803] = {
  DialogID = 7803,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER7_8_3"
}
Dialogues[7804] = {
  DialogID = 7804,
  NPCHead = 17,
  HeadName = "NPC_1",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER7_8_4"
}
Dialogues[7805] = {
  DialogID = 7805,
  NPCHead = 38,
  HeadName = "NPC_7802",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER7_8_5"
}
Dialogues[7806] = {
  DialogID = 7806,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER7_8_6"
}
Dialogues[7807] = {
  DialogID = 7807,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER7_8_7"
}
Dialogues[7808] = {
  DialogID = 7808,
  NPCHead = 17,
  HeadName = "NPC_1",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER7_8_8"
}
Dialogues[7809] = {
  DialogID = 7809,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER7_8_9"
}
Dialogues[7810] = {
  DialogID = 7810,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER7_8_10"
}
Dialogues[7811] = {
  DialogID = 7811,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER7_8_11"
}
Dialogues[7812] = {
  DialogID = 7812,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER7_8_12"
}
Dialogues[7813] = {
  DialogID = 7813,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER7_8_13"
}
Dialogues[7814] = {
  DialogID = 7814,
  NPCHead = 31,
  HeadName = "NPC_26",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER7_8_14"
}
Dialogues[7815] = {
  DialogID = 7815,
  NPCHead = 31,
  HeadName = "NPC_26",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER7_8_15"
}
Dialogues[7816] = {
  DialogID = 7816,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER7_8_16"
}
Dialogues[7817] = {
  DialogID = 7817,
  NPCHead = 31,
  HeadName = "NPC_26",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER7_8_17"
}
Dialogues[7818] = {
  DialogID = 7818,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER7_8_18"
}
Dialogues[8101] = {
  DialogID = 8101,
  NPCHead = 31,
  HeadName = "NPC_26",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER8_1_1"
}
Dialogues[8102] = {
  DialogID = 8102,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER8_1_2"
}
Dialogues[8103] = {
  DialogID = 8103,
  NPCHead = 31,
  HeadName = "NPC_26",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER8_1_3"
}
Dialogues[8104] = {
  DialogID = 8104,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER8_1_4"
}
Dialogues[8105] = {
  DialogID = 8105,
  NPCHead = 31,
  HeadName = "NPC_26",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER8_1_5"
}
Dialogues[8106] = {
  DialogID = 8106,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER8_1_6"
}
Dialogues[8107] = {
  DialogID = 8107,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER8_1_7"
}
Dialogues[8108] = {
  DialogID = 8108,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER8_1_8"
}
Dialogues[8201] = {
  DialogID = 8201,
  NPCHead = 31,
  HeadName = "NPC_26",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER8_2_1"
}
Dialogues[8202] = {
  DialogID = 8202,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER8_2_2"
}
Dialogues[8203] = {
  DialogID = 8203,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER8_2_3"
}
Dialogues[8204] = {
  DialogID = 8204,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER8_2_4"
}
Dialogues[8301] = {
  DialogID = 8301,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER8_3_1"
}
Dialogues[8302] = {
  DialogID = 8302,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER8_3_2"
}
Dialogues[8303] = {
  DialogID = 8303,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER8_3_3"
}
Dialogues[8304] = {
  DialogID = 8304,
  NPCHead = 31,
  HeadName = "NPC_26",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER8_3_4"
}
Dialogues[8305] = {
  DialogID = 8305,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER8_3_5"
}
Dialogues[8306] = {
  DialogID = 8306,
  NPCHead = 31,
  HeadName = "NPC_26",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER8_3_6"
}
Dialogues[8307] = {
  DialogID = 8307,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER8_3_7"
}
Dialogues[8401] = {
  DialogID = 8401,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER8_4_1"
}
Dialogues[8402] = {
  DialogID = 8402,
  NPCHead = 31,
  HeadName = "NPC_26",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER8_4_2"
}
Dialogues[8403] = {
  DialogID = 8403,
  NPCHead = 42,
  HeadName = "NPC_16",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER8_4_3"
}
Dialogues[8404] = {
  DialogID = 8404,
  NPCHead = 31,
  HeadName = "NPC_26",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER8_4_4"
}
Dialogues[8405] = {
  DialogID = 8405,
  NPCHead = 42,
  HeadName = "NPC_16",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER8_4_5"
}
Dialogues[8406] = {
  DialogID = 8406,
  NPCHead = 31,
  HeadName = "NPC_26",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER8_4_6"
}
Dialogues[8407] = {
  DialogID = 8407,
  NPCHead = 42,
  HeadName = "NPC_16",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER8_4_7"
}
Dialogues[8408] = {
  DialogID = 8408,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER8_4_8"
}
Dialogues[8409] = {
  DialogID = 8409,
  NPCHead = 31,
  HeadName = "NPC_26",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER8_4_9"
}
Dialogues[8410] = {
  DialogID = 8410,
  NPCHead = 42,
  HeadName = "NPC_16",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER8_4_10"
}
Dialogues[8411] = {
  DialogID = 8411,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER8_4_11"
}
Dialogues[8501] = {
  DialogID = 8501,
  NPCHead = 42,
  HeadName = "NPC_16",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER8_5_1"
}
Dialogues[8502] = {
  DialogID = 8502,
  NPCHead = 41,
  HeadName = "NPC_19",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER8_5_2"
}
Dialogues[8503] = {
  DialogID = 8503,
  NPCHead = 42,
  HeadName = "NPC_16",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER8_5_3"
}
Dialogues[8504] = {
  DialogID = 8504,
  NPCHead = 41,
  HeadName = "NPC_19",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER8_5_4"
}
Dialogues[8505] = {
  DialogID = 8505,
  NPCHead = 42,
  HeadName = "NPC_16",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER8_5_5"
}
Dialogues[8506] = {
  DialogID = 8506,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER8_5_6"
}
Dialogues[8507] = {
  DialogID = 8507,
  NPCHead = 1,
  HeadName = "NPC_2",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER8_5_7"
}
Dialogues[8508] = {
  DialogID = 8508,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER8_5_8"
}
Dialogues[8509] = {
  DialogID = 8509,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER8_5_9"
}
Dialogues[8601] = {
  DialogID = 8601,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER8_6_1"
}
Dialogues[8602] = {
  DialogID = 8602,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER8_6_2"
}
Dialogues[8603] = {
  DialogID = 8603,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER8_6_3"
}
Dialogues[8701] = {
  DialogID = 8701,
  NPCHead = 42,
  HeadName = "NPC_16",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER8_7_1"
}
Dialogues[8702] = {
  DialogID = 8702,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER8_7_2"
}
Dialogues[8703] = {
  DialogID = 8703,
  NPCHead = 38,
  HeadName = "NPC_7802",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER8_7_3"
}
Dialogues[8704] = {
  DialogID = 8704,
  NPCHead = 42,
  HeadName = "NPC_16",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER8_7_4"
}
Dialogues[8705] = {
  DialogID = 8705,
  NPCHead = 43,
  HeadName = "NPC_15",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER8_7_5"
}
Dialogues[8706] = {
  DialogID = 8706,
  NPCHead = 4,
  HeadName = "NPC_5",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER8_7_6"
}
Dialogues[8707] = {
  DialogID = 8707,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER8_7_7"
}
Dialogues[8708] = {
  DialogID = 8708,
  NPCHead = 42,
  HeadName = "NPC_16",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER8_7_8"
}
Dialogues[8709] = {
  DialogID = 8709,
  NPCHead = 42,
  HeadName = "NPC_16",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER8_7_9"
}
Dialogues[8710] = {
  DialogID = 8710,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER8_7_10"
}
Dialogues[8801] = {
  DialogID = 8801,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER8_8_1"
}
Dialogues[8802] = {
  DialogID = 8802,
  NPCHead = 43,
  HeadName = "NPC_15",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER8_8_2"
}
Dialogues[8803] = {
  DialogID = 8803,
  NPCHead = 31,
  HeadName = "NPC_26",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER8_8_3"
}
Dialogues[8804] = {
  DialogID = 8804,
  NPCHead = 17,
  HeadName = "NPC_14",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER8_8_4"
}
Dialogues[8805] = {
  DialogID = 8805,
  NPCHead = 43,
  HeadName = "NPC_15",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER8_8_5"
}
Dialogues[8806] = {
  DialogID = 8806,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER8_8_6"
}
Dialogues[8807] = {
  DialogID = 8807,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER8_8_7"
}
Dialogues[8808] = {
  DialogID = 8808,
  NPCHead = 31,
  HeadName = "NPC_26",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER8_8_8"
}
Dialogues[8809] = {
  DialogID = 8809,
  NPCHead = 20,
  HeadName = "NPC_32",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER8_8_9"
}
Dialogues[8810] = {
  DialogID = 8810,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER8_8_10"
}
Dialogues[8811] = {
  DialogID = 8811,
  NPCHead = 20,
  HeadName = "NPC_32",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER8_8_11"
}
Dialogues[9101] = {
  DialogID = 9101,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER9_1_1"
}
Dialogues[9102] = {
  DialogID = 9102,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER9_1_2"
}
Dialogues[9103] = {
  DialogID = 9103,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER9_1_3"
}
Dialogues[9104] = {
  DialogID = 9104,
  NPCHead = 99,
  HeadName = "NPC_A3",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER9_1_4"
}
Dialogues[9105] = {
  DialogID = 9105,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER9_1_5"
}
Dialogues[9106] = {
  DialogID = 9106,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER9_1_6"
}
Dialogues[9107] = {
  DialogID = 9107,
  NPCHead = 16,
  HeadName = "NPC_26",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER9_1_7"
}
Dialogues[9108] = {
  DialogID = 9108,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER9_1_8"
}
Dialogues[9109] = {
  DialogID = 9109,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER9_1_9"
}
Dialogues[9110] = {
  DialogID = 9110,
  NPCHead = 1,
  HeadName = "NPC_2",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER9_1_10"
}
Dialogues[9111] = {
  DialogID = 9111,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER9_1_11"
}
Dialogues[9201] = {
  DialogID = 9201,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER9_2_1"
}
Dialogues[9202] = {
  DialogID = 9202,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER9_2_2"
}
Dialogues[9203] = {
  DialogID = 9203,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER9_2_3"
}
Dialogues[9204] = {
  DialogID = 9204,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER9_2_4"
}
Dialogues[9205] = {
  DialogID = 9205,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER9_2_5"
}
Dialogues[9206] = {
  DialogID = 9206,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER9_2_6"
}
Dialogues[9207] = {
  DialogID = 9207,
  NPCHead = 16,
  HeadName = "NPC_26",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER9_2_7"
}
Dialogues[9208] = {
  DialogID = 9208,
  NPCHead = 1,
  HeadName = "NPC_2",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER9_2_8"
}
Dialogues[9301] = {
  DialogID = 9301,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER9_3_1"
}
Dialogues[9302] = {
  DialogID = 9302,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER9_3_2"
}
Dialogues[9303] = {
  DialogID = 9303,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER9_3_3"
}
Dialogues[9304] = {
  DialogID = 9304,
  NPCHead = 99,
  HeadName = "NPC_A3",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER9_3_4"
}
Dialogues[9305] = {
  DialogID = 9305,
  NPCHead = 1,
  HeadName = "NPC_2",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER9_3_5"
}
Dialogues[9306] = {
  DialogID = 9306,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER9_3_6"
}
Dialogues[9307] = {
  DialogID = 9307,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER9_3_7"
}
Dialogues[9308] = {
  DialogID = 9308,
  NPCHead = 1,
  HeadName = "NPC_2",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER9_3_8"
}
Dialogues[9309] = {
  DialogID = 9309,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER9_3_9"
}
Dialogues[9401] = {
  DialogID = 9401,
  NPCHead = 99,
  HeadName = "NPC_A3",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER9_4_1"
}
Dialogues[9402] = {
  DialogID = 9402,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER9_4_2"
}
Dialogues[9403] = {
  DialogID = 9403,
  NPCHead = 99,
  HeadName = "NPC_A3",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER9_4_3"
}
Dialogues[9404] = {
  DialogID = 9404,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER9_4_4"
}
Dialogues[9405] = {
  DialogID = 9405,
  NPCHead = 99,
  HeadName = "NPC_A3",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER9_4_5"
}
Dialogues[9406] = {
  DialogID = 9406,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER9_4_6"
}
Dialogues[9407] = {
  DialogID = 9407,
  NPCHead = 8,
  HeadName = "NPC_9",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER9_4_7"
}
Dialogues[9408] = {
  DialogID = 9408,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER9_4_8"
}
Dialogues[9409] = {
  DialogID = 9409,
  NPCHead = 97,
  HeadName = "NPC_A5",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER9_4_9"
}
Dialogues[9410] = {
  DialogID = 9410,
  NPCHead = 97,
  HeadName = "NPC_A5",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER9_4_10"
}
Dialogues[9501] = {
  DialogID = 9501,
  NPCHead = 97,
  HeadName = "NPC_A5",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER9_5_1"
}
Dialogues[9502] = {
  DialogID = 9502,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER9_5_2"
}
Dialogues[9503] = {
  DialogID = 9503,
  NPCHead = 97,
  HeadName = "NPC_A5",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER9_5_3"
}
Dialogues[9504] = {
  DialogID = 9504,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER9_5_4"
}
Dialogues[9505] = {
  DialogID = 9505,
  NPCHead = 1,
  HeadName = "NPC_2",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER9_5_5"
}
Dialogues[9506] = {
  DialogID = 9506,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER9_5_6"
}
Dialogues[9507] = {
  DialogID = 9507,
  NPCHead = 98,
  HeadName = "NPC_A2",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER9_5_7"
}
Dialogues[9508] = {
  DialogID = 9508,
  NPCHead = 98,
  HeadName = "NPC_A2",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER9_5_8"
}
Dialogues[9601] = {
  DialogID = 9601,
  NPCHead = 98,
  HeadName = "NPC_A2",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER9_6_1"
}
Dialogues[9602] = {
  DialogID = 9602,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER9_6_2"
}
Dialogues[9603] = {
  DialogID = 9603,
  NPCHead = 98,
  HeadName = "NPC_A2",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER9_6_3"
}
Dialogues[9604] = {
  DialogID = 9604,
  NPCHead = 98,
  HeadName = "NPC_A2",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER9_6_4"
}
Dialogues[9605] = {
  DialogID = 9605,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER9_6_5"
}
Dialogues[9606] = {
  DialogID = 9606,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER9_6_6"
}
Dialogues[9607] = {
  DialogID = 9607,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER9_6_7"
}
Dialogues[9608] = {
  DialogID = 9608,
  NPCHead = 13,
  HeadName = "NPC_A1",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER9_6_8"
}
Dialogues[9609] = {
  DialogID = 9609,
  NPCHead = 13,
  HeadName = "NPC_A1",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER9_6_9"
}
Dialogues[9701] = {
  DialogID = 9701,
  NPCHead = 13,
  HeadName = "NPC_A1",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER9_7_1"
}
Dialogues[9702] = {
  DialogID = 9702,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER9_7_2"
}
Dialogues[9703] = {
  DialogID = 9703,
  NPCHead = 13,
  HeadName = "NPC_A1",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER9_7_3"
}
Dialogues[9704] = {
  DialogID = 9704,
  NPCHead = 46,
  HeadName = "NPC_A4",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER9_7_4"
}
Dialogues[9705] = {
  DialogID = 9705,
  NPCHead = 14,
  HeadName = "NPC_6",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER9_7_5"
}
Dialogues[9706] = {
  DialogID = 9706,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER9_7_6"
}
Dialogues[9707] = {
  DialogID = 9707,
  NPCHead = 46,
  HeadName = "NPC_A4",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER9_7_7"
}
Dialogues[9801] = {
  DialogID = 9801,
  NPCHead = 46,
  HeadName = "NPC_A4",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER9_8_1"
}
Dialogues[9802] = {
  DialogID = 9802,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER9_8_2"
}
Dialogues[9803] = {
  DialogID = 9803,
  NPCHead = 46,
  HeadName = "NPC_A4",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER9_8_3"
}
Dialogues[9804] = {
  DialogID = 9804,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER9_8_4"
}
Dialogues[9805] = {
  DialogID = 9805,
  NPCHead = 46,
  HeadName = "NPC_A4",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER9_8_5"
}
Dialogues[9806] = {
  DialogID = 9806,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER9_8_6"
}
Dialogues[9807] = {
  DialogID = 9807,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER9_8_7"
}
Dialogues[9808] = {
  DialogID = 9808,
  NPCHead = 16,
  HeadName = "NPC_26",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER9_8_8"
}
Dialogues[9809] = {
  DialogID = 9809,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_CHAPTER9_8_9"
}
Dialogues[9810] = {
  DialogID = 9810,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER9_8_10"
}
Dialogues[9811] = {
  DialogID = 9811,
  NPCHead = -1,
  HeadName = "PLAYER",
  NPCPos = 0,
  TextID = "LC_STORY_CHAPTER9_8_11"
}
Dialogues[9812] = {
  DialogID = 9812,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 0,
  TextID = "LC_STORY_PRIMUS_1"
}
Dialogues[9813] = {
  DialogID = 9813,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 0,
  TextID = "LC_STORY_PRIMUS_2"
}
Dialogues[9814] = {
  DialogID = 9814,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 0,
  TextID = "LC_STORY_FOREST_1"
}
Dialogues[9815] = {
  DialogID = 9815,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 0,
  TextID = "LC_STORY_FOREST_2"
}
Dialogues[9816] = {
  DialogID = 9816,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_MEDAL_1"
}
Dialogues[9817] = {
  DialogID = 9817,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_MEDAL_2"
}
Dialogues[9818] = {
  DialogID = 9818,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_MEDAL_3"
}
Dialogues[9819] = {
  DialogID = 9819,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_MEDAL_3"
}
Dialogues[9820] = {
  DialogID = 9820,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_MEDAL_4"
}
Dialogues[9821] = {
  DialogID = 9821,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_MEDAL_5"
}
Dialogues[9822] = {
  DialogID = 9822,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_MEDAL_6"
}
Dialogues[9823] = {
  DialogID = 9823,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_MEDAL_7"
}
Dialogues[9824] = {
  DialogID = 9824,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_MEDAL_8"
}
Dialogues[9825] = {
  DialogID = 9825,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_MEDAL_9"
}
Dialogues[9826] = {
  DialogID = 9826,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_MEDAL_10"
}
Dialogues[9901] = {
  DialogID = 9901,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_TLC_1"
}
Dialogues[9902] = {
  DialogID = 9902,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_TLC_2"
}
Dialogues[9903] = {
  DialogID = 9903,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_TLC_3"
}
Dialogues[9904] = {
  DialogID = 9904,
  NPCHead = 0,
  HeadName = "NPC_0",
  NPCPos = 1,
  TextID = "LC_STORY_TLC_4"
}
