local ChapterInfos_170 = GameData.chapterdata_act4.ChapterInfos_170
ChapterInfos_170[1] = {
  ChaperID = 1,
  ICON = "icon1",
  MapIndex = 1,
  BossPos = 5,
  EntroStory = {4101, 4102},
  AFTER_FINISH = {4107}
}
ChapterInfos_170[2] = {
  ChaperID = 2,
  ICON = "icon2",
  MapIndex = 2,
  BossPos = 10,
  EntroStory = {4201},
  AFTER_FINISH = {
    4203,
    4204,
    4205,
    4206
  }
}
ChapterInfos_170[3] = {
  ChaperID = 3,
  ICON = "icon30",
  MapIndex = 3,
  BossPos = 16,
  EntroStory = {4301},
  AFTER_FINISH = {
    4306,
    4307,
    4308,
    4309,
    4310
  }
}
ChapterInfos_170[4] = {
  ChaperID = 4,
  ICON = "icon400",
  MapIndex = 1,
  BossPos = 18,
  EntroStory = {4401},
  AFTER_FINISH = {}
}
ChapterInfos_170[5] = {
  ChaperID = 5,
  ICON = "icon5",
  MapIndex = 1,
  BossPos = 18,
  EntroStory = {4501, 4502},
  AFTER_FINISH = {4508, 4509}
}
ChapterInfos_170[6] = {
  ChaperID = 6,
  ICON = "icon400",
  MapIndex = 1,
  BossPos = 18,
  EntroStory = {4601},
  AFTER_FINISH = {4605, 4606}
}
ChapterInfos_170[7] = {
  ChaperID = 7,
  ICON = "icon6",
  MapIndex = 1,
  BossPos = 18,
  EntroStory = {
    4701,
    4702,
    4703,
    4704
  },
  AFTER_FINISH = {
    4707,
    4708,
    4709,
    4710
  }
}
ChapterInfos_170[8] = {
  ChaperID = 8,
  ICON = "icon400",
  MapIndex = 1,
  BossPos = 18,
  EntroStory = {4801, 4802},
  AFTER_FINISH = {
    4813,
    4814,
    4815,
    4816,
    4817,
    4818,
    4819,
    4820,
    4821,
    4822
  }
}
ChapterInfos_170[9] = {
  ChaperID = 9,
  ICON = "icon5",
  MapIndex = 1,
  BossPos = 18,
  EntroStory = {},
  AFTER_FINISH = {}
}
ChapterInfos_170[10] = {
  ChaperID = 10,
  ICON = "icon5",
  MapIndex = 1,
  BossPos = 18,
  EntroStory = {},
  AFTER_FINISH = {}
}
