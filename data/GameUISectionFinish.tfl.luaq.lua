local GameUISectionFinish = LuaObjectManager:GetLuaObject("GameUISectionFinish")
function GameUISectionFinish:OnFSCommand(cmd, arg)
  if cmd == "animation_over_moveout" then
    GameStateManager:GetCurrentGameState():EraseObject(self)
    self:CheckSectionFinishAfter()
  elseif cmd == "close_dialog" then
    self:MoveOut()
  end
end
function GameUISectionFinish:OnAddToGameState(game_state)
  self:MoveIn()
end
function GameUISectionFinish:CheckSectionFinishAfter()
  local after_story_callback
  local after_section_story = GameDataAccessHelper:GetSectionAfterFinishStory(self.section_finish_info.area_id, self.section_finish_info.section_id)
  self.section_finish_info = nil
  local GameUICommonDialog = LuaObjectManager:GetLuaObject("GameUICommonDialog")
  GameUICommonDialog:PlayStory(after_section_story, after_story_callback)
end
function GameUISectionFinish:MoveIn()
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "lua2fs_animationMoveIn")
end
function GameUISectionFinish:MoveOut()
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "lua2fs_animationMoveOut")
end
function GameUISectionFinish:EnableSectionFinish(area_id, section_id)
  DebugOut("AreaID - ", area_id, " SectionID - ", section_id)
  self.section_finish_info = {}
  self.section_finish_info.area_id = area_id
  self.section_finish_info.section_id = section_id
end
function GameUISectionFinish:CheckSectionFinishBefore()
  if self.section_finish_info then
    local function before_story_callback()
      local GameStateMainPlanet = GameStateManager:GetCurrentGameState()
      GameStateMainPlanet:AddObject(GameUISectionFinish)
    end
    local before_section_story = GameDataAccessHelper:GetSectionBeforeFinishStory(self.section_finish_info.area_id, self.section_finish_info.section_id)
    local GameUICommonDialog = LuaObjectManager:GetLuaObject("GameUICommonDialog")
    GameUICommonDialog:PlayStory(before_section_story, before_story_callback)
  end
end
