local GameUIArena = LuaObjectManager:GetLuaObject("GameUIArena")
local GameStateArena = GameStateManager.GameStateArena
local GameStateBattleMap = GameStateManager.GameStateBattleMap
local GameStateBattlePlay = GameStateManager.GameStateBattlePlay
local GameStateMainPlanet = GameStateManager.GameStateMainPlanet
local GameStateWD = GameStateManager.GameStateWD
local GameStateStarCraft = GameStateManager.GameStateStarCraft
function GameStateArena:InitGameState()
end
function GameStateArena:OnFocusGain(previousState)
  if previousState == GameStateBattleMap then
    function self.onQuitState()
      GameStateBattleMap:ReEnter(nil)
    end
  elseif previousState == GameStateWD then
    function self.onQuitState()
      GameStateManager:SetCurrentGameState(GameStateWD)
    end
  elseif previousState == GameStateMainPlanet then
    function self.onQuitState()
      GameStateManager:SetCurrentGameState(GameStateManager.GameStateMainPlanet)
    end
  elseif previousState == GameStateManager.GameStateDaily then
    function self.onQuitState()
      GameStateManager:SetCurrentGameState(GameStateManager.GameStateMainPlanet)
    end
  elseif previousState == GameStateStarCraft then
    function self.onQuitState()
      GameStateManager:SetCurrentGameState(GameStateManager.GameStateStarCraft)
    end
  end
  self:AddObject(GameUIArena)
end
function GameStateArena:OnFocusLost(newState)
  self:EraseObject(GameUIArena)
end
function GameStateArena:Quit()
  if self.onQuitState then
    if GameUIArena.mPreArenaType == GameUIArena.ARENA_TYPE.enterLayer then
      GameUIArena:CleanPreArenaType()
      GameUIArena:UnloadFlashObject()
      GameUIArena:OnEnterLayer()
    else
      GameUIArena:CleanPreArenaType()
      self.onQuitState()
    end
  end
end
