local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameStateManager = GameStateManager
local GameStateGlobalState = GameStateManager.GameStateGlobalState
GameUIGlobalScreen.IsShowingMessageBox = false
GameUIGlobalScreen.MessageBoxType_OK = 1
GameUIGlobalScreen.MessageBoxType_YesNo = 2
function GameUIGlobalScreen:OnAddToGameState(TheGameStateAddTo)
  self.LastGameState = TheGameStateAddTo
  DebugOut("set last game state to ", TheGameStateAddTo)
end
function GameUIGlobalScreen:OnEraseFromGameState()
  self.LastGameState = nil
  DebugOut("set last game state to nil")
end
function GameUIGlobalScreen:ShowMessageBox(messageBoxType, title, content, callbackFuncOk, callbackFuncCancel)
  if self.IsShowingMessageBox then
    DebugOut("warning : messagebox is already showing")
    return
  end
  DebugOut("ShowMessageBox ", messageBoxType, title, content, callbackFuncOk)
  self.IsShowingMessageBox = true
  self:GetFlashObject():InvokeASCallback("_root", "showMessageBox", messageBoxType, title, content)
  self.CallbackFuncOk = callbackFuncOk
  self.CallbackFuncCancel = callbackFuncCancel
  if not self.LastGameState then
    GameStateGlobalState:AddObject(self)
  end
end
GameUIGlobalScreen.buildingCdType = 1
GameUIGlobalScreen.equipmentCdType = 3
function GameUIGlobalScreen:ShowClearCD(cd_type, callbackFuncOk)
  DebugOut("ShowClearCD ", cd_type)
  if self.IsShowingMessageBox then
    return
  end
  self.clearCDType = cd_type
  self.clearCDCallback = callbackFuncOk
  local param = {price_type = cd_type, type = 0}
  NetMessageMgr:SendMsg(NetAPIList.price_req.Code, param, GameUIGlobalScreen.showClearCDCallback, true)
end
function GameUIGlobalScreen.showClearCDCallback(msgType, content)
  if msgType == NetAPIList.price_ack.Code then
    local title = GameLoader:GetGameText("LC_MENU_RESET_CD_TITLE")
    local _content = ""
    if GameUIGlobalScreen.clearCDType == "clear_equipment_enhance_cd" then
      _content = string.format(GameLoader:GetGameText("LC_MENU_ENHANCE_CD_ALERT_CHAR"), tonumber(content.price))
      _content = string.gsub(_content, "<number>", tostring(GameDataAccessHelper:GetVIPLimit("no_enhance_cd")))
    else
      _content = string.format(GameLoader:GetGameText("LC_MENU_RESET_CD_ASK"), content.price)
    end
    GameUIGlobalScreen:ShowMessageBox(2, title, _content, GameUIGlobalScreen.clearCDCallback)
    return true
  elseif msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.price_req.Code then
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  end
  return false
end
function GameUIGlobalScreen:HideMessageBox()
  if not self.IsShowingMessageBox then
    DebugOut("warning : messagebox is not showing")
    return
  end
  DebugOut("why set showing here??")
  if "confirm" == self.CloseCommand or "ok" == self.CloseCommand then
    if self.CallbackFuncOk then
      self.CallbackFuncOk(self.CloseCommand)
    end
  elseif "cancel" == self.CloseCommand and self.CallbackFuncCancel then
    self.CallbackFuncCancel(self.CloseCommand)
  end
  self.CallbackFuncOk = nil
  self.CallbackFuncCancel = nil
  self.CloseCommand = nil
  self:GetFlashObject():InvokeASCallback("_root", "hideMessageBox")
end
function GameUIGlobalScreen:ShowAlert(titleId, errorCode, callbackFunc, checkDuplicate)
  local title = "LC_MENU_ERROR_TITLE"
  if titleId == "success" then
    title = "LC_MENU_TITLE_SUCCESS_INFO"
  end
  title = GameLoader:GetGameText(title)
  local textId = AlertDataList:GetTextIdFromCode(errorCode)
  local alertType = AlertDataList:GetTypeFromCode(errorCode)
  if textId == nil then
    DebugOut("ERROR : ShowAlert nil textId")
    return
  end
  local content = GameLoader:GetGameText(textId)
  if content == "" and textId ~= nil then
    content = textId
  end
  if "Tip" == alertType then
    local GameTip = LuaObjectManager:GetLuaObject("GameTip")
    DebugOut("Show tips for errorCode", errorCode, content)
    GameTip:Show(content, 3000, callbackFunc, nil, checkDuplicate)
    return
  end
  self:ShowMessageBox(1, title, content, callbackFunc)
end
function GameUIGlobalScreen:ShowAlert2(content)
  errorCode = content.code
  errorContent = content.msg
  local title = "LC_MENU_ERROR_TITLE"
  title = GameLoader:GetGameText(title)
  local textId = AlertDataList:GetTextIdFromCode(errorCode)
  local alertType = AlertDataList:GetTypeFromCode(errorCode)
  if textId == nil then
    DebugOut("ERROR : ShowAlert nil textId")
    return
  end
  local showContext = ""
  if errorCode == 15 then
    showContext = errorContent
  else
    showContext = GameLoader:GetGameText(textId)
    if showContext == "" and textId ~= nil then
      showContext = textId
    end
    if "Tip" == alertType then
      local GameTip = LuaObjectManager:GetLuaObject("GameTip")
      DebugOut("Show tips for errorCode", errorCode, content)
      GameTip:Show(showContext, 3000, nil, nil, nil)
      return
    end
  end
  self:ShowMessageBox(1, title, showContext, nil)
end
function GameUIGlobalScreen:OnFSCommand(cmd, arg)
  DebugOut("GameUIGlobalScreen", cmd, arg)
  if cmd == "erase_dialog" then
    self.IsShowingMessageBox = false
  elseif cmd == "clickBtn" then
    if not self.IsShowingMessageBox then
      DebugOut("warning : messagebox is not showing")
      return
    end
    if "ok_button" == arg then
      self.CloseCommand = "confirm"
    elseif "yes_button" == arg then
      self.CloseCommand = "ok"
    elseif "no_button" == arg then
      self.CloseCommand = "cancel"
    end
    self:HideMessageBox()
    return
  end
end
function GameUIGlobalScreen:Update(dt)
  local flashObject = self:GetFlashObject()
  if flashObject then
    flashObject:Update(dt)
  end
  if not self.IsShowingMessageBox and self.LastGameState then
    DebugOut("Earse from update!!!!!!!")
    self.LastGameState:EraseObject(self)
  end
end
function GameUIGlobalScreen:Render()
  local flashObject = self:GetFlashObject()
  if flashObject then
    flashObject:Render()
  end
end
if AutoUpdate.isAndroidDevice then
  function GameUIGlobalScreen.OnAndroidBack()
    if GameUIGlobalScreen.CallbackFuncCancel then
      GameUIGlobalScreen.CallbackFuncCancel("cancel")
    end
    GameUIGlobalScreen:GetFlashObject():InvokeASCallback("_root", "hideMessageBox")
  end
end
