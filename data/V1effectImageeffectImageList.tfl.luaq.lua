local effectImageList = GameData.effectImage.effectImageList
table.insert(effectImageList, {
  effectName = "buff_locked3",
  images = {
    "LAZY_LOAD_buff_locked03_00.png",
    "LAZY_LOAD_buff_locked03_01.png",
    "LAZY_LOAD_buff_locked03_02.png",
    "LAZY_LOAD_buff_locked03_03.png",
    "LAZY_LOAD_buff_locked03_04.png",
    "LAZY_LOAD_buff_locked03_05.png",
    "LAZY_LOAD_buff_locked03_06.png",
    "LAZY_LOAD_buff_locked03_07.png"
  }
})
table.insert(effectImageList, {
  effectName = "buff_dot2",
  images = {
    "LAZY_LOAD_buff_new_dot01.png",
    "LAZY_LOAD_buff_new_dot03.png",
    "LAZY_LOAD_buff_new_dot05.png",
    "LAZY_LOAD_buff_new_dot07.png",
    "LAZY_LOAD_buff_new_dot09.png",
    "LAZY_LOAD_buff_new_dot11.png",
    "LAZY_LOAD_buff_new_dot13.png",
    "LAZY_LOAD_buff_new_dot15.png",
    "LAZY_LOAD_buff_new_dot17.png",
    "LAZY_LOAD_buff_new_dot19.png",
    "LAZY_LOAD_buff_new_dot21.png",
    "LAZY_LOAD_buff_new_dot23.png",
    "LAZY_LOAD_buff_new_dot25.png"
  }
})
table.insert(effectImageList, {
  effectName = "buff_shield",
  images = {
    "LAZY_LOAD_buff_shield.png"
  }
})
table.insert(effectImageList, {
  effectName = "buff_exile",
  images = {
    "battle_effct.png"
  }
})
table.insert(effectImageList, {
  effectName = "buff_confusion",
  images = {
    "LAZY_LOAD_buff_confusion_00.png"
  }
})
table.insert(effectImageList, {
  effectName = "buff_locked",
  images = {
    "LAZY_LOAD_buff_lock_01.png",
    "LAZY_LOAD_buff_lock_02.png",
    "LAZY_LOAD_buff_lock_03.png",
    "LAZY_LOAD_buff_lock_04.png",
    "LAZY_LOAD_buff_lock_05.png"
  }
})
table.insert(effectImageList, {
  effectName = "buff_invinclible",
  images = {
    "LAZY_LOAD_buff_shield.png"
  }
})
table.insert(effectImageList, {
  effectName = "buff_dot",
  images = {
    "LAZY_LOAD_buff_green.png"
  }
})
table.insert(effectImageList, {
  effectName = "buff_disturb",
  images = {
    "LAZY_LOAD_buff_effect_disturb00.png",
    "LAZY_LOAD_buff_effect_disturb01.png",
    "LAZY_LOAD_buff_effect_disturb02.png",
    "LAZY_LOAD_buff_effect_disturb03.png",
    "LAZY_LOAD_buff_effect_disturb04.png",
    "LAZY_LOAD_buff_effect_disturb05.png",
    "LAZY_LOAD_buff_effect_disturb06.png",
    "LAZY_LOAD_buff_effect_disturb07.png",
    "LAZY_LOAD_buff_effect_disturb08.png",
    "LAZY_LOAD_buff_effect_disturb09.png",
    "LAZY_LOAD_buff_effect_disturb10.png",
    "LAZY_LOAD_buff_effect_disturb11.png"
  }
})
table.insert(effectImageList, {
  effectName = "buff_sp",
  images = {
    "LAZY_LOAD_effect_sp00.png",
    "LAZY_LOAD_effect_sp01.png",
    "LAZY_LOAD_effect_sp02.png",
    "LAZY_LOAD_effect_sp03.png",
    "LAZY_LOAD_effect_sp04.png",
    "LAZY_LOAD_effect_sp05.png",
    "LAZY_LOAD_effect_sp06.png",
    "LAZY_LOAD_effect_sp07.png",
    "LAZY_LOAD_effect_sp08.png",
    "LAZY_LOAD_effect_sp09.png",
    "LAZY_LOAD_effect_sp10.png",
    "LAZY_LOAD_effect_sp11.png",
    "LAZY_LOAD_effect_sp12.png"
  }
})
table.insert(effectImageList, {
  effectName = "buff_invisible",
  images = {
    "LAZY_LOAD_locked_locked00.png",
    "LAZY_LOAD_locked_locked02.png",
    "LAZY_LOAD_locked_locked04.png",
    "LAZY_LOAD_locked_locked06.png",
    "LAZY_LOAD_locked_locked08.png",
    "LAZY_LOAD_locked_locked10.png",
    "LAZY_LOAD_locked_locked12.png",
    "LAZY_LOAD_locked_locked14.png",
    "LAZY_LOAD_locked_locked16.png",
    "LAZY_LOAD_locked_locked18.png",
    "LAZY_LOAD_locked_locked20.png",
    "LAZY_LOAD_locked_locked22.png",
    "LAZY_LOAD_locked_locked24.png",
    "LAZY_LOAD_locked_locked26.png",
    "LAZY_LOAD_locked_locked28.png"
  }
})
table.insert(effectImageList, {
  effectName = "buff_locked2",
  images = {
    "LAZY_LOAD_locked_locked00.png",
    "LAZY_LOAD_locked_locked02.png",
    "LAZY_LOAD_locked_locked04.png",
    "LAZY_LOAD_locked_locked06.png",
    "LAZY_LOAD_locked_locked08.png",
    "LAZY_LOAD_locked_locked10.png",
    "LAZY_LOAD_locked_locked12.png",
    "LAZY_LOAD_locked_locked14.png",
    "LAZY_LOAD_locked_locked16.png",
    "LAZY_LOAD_locked_locked18.png",
    "LAZY_LOAD_locked_locked20.png",
    "LAZY_LOAD_locked_locked22.png",
    "LAZY_LOAD_locked_locked24.png",
    "LAZY_LOAD_locked_locked26.png",
    "LAZY_LOAD_locked_locked28.png"
  }
})
table.insert(effectImageList, {
  effectName = "buff_ice",
  images = {
    "LAZY_LOAD_ice_0000.png",
    "LAZY_LOAD_ice_0002.png",
    "LAZY_LOAD_ice_0004.png",
    "LAZY_LOAD_ice_0006.png",
    "LAZY_LOAD_ice_0008.png",
    "LAZY_LOAD_ice_0010.png",
    "LAZY_LOAD_ice_0012.png",
    "LAZY_LOAD_ice_0014.png",
    "LAZY_LOAD_ice_0016.png",
    "LAZY_LOAD_ice_0018.png",
    "LAZY_LOAD_ice_0020.png",
    "LAZY_LOAD_ice_0022.png",
    "LAZY_LOAD_ice_0024.png",
    "LAZY_LOAD_ice_0026.png",
    "LAZY_LOAD_ice_0028.png"
  }
})
table.insert(effectImageList, {
  effectName = "buff_def_dec",
  images = {
    "LAZY_LOAD_buff_deduct_00.png",
    "LAZY_LOAD_buff_deduct_01.png",
    "LAZY_LOAD_buff_deduct_02.png",
    "LAZY_LOAD_buff_deduct_03.png",
    "LAZY_LOAD_buff_deduct_04.png",
    "LAZY_LOAD_buff_deduct_05.png",
    "LAZY_LOAD_buff_deduct_06.png",
    "LAZY_LOAD_buff_deduct_07.png"
  }
})
table.insert(effectImageList, {
  effectName = "buff_def_add",
  images = {
    "LAZY_LOAD_buff_add_00.png",
    "LAZY_LOAD_buff_add_01.png",
    "LAZY_LOAD_buff_add_02.png",
    "LAZY_LOAD_buff_add_03.png",
    "LAZY_LOAD_buff_add_04.png",
    "LAZY_LOAD_buff_add_05.png",
    "LAZY_LOAD_buff_add_06.png",
    "LAZY_LOAD_buff_add_07.png",
    "LAZY_LOAD_buff_add_08.png"
  }
})
table.insert(effectImageList, {
  effectName = "att_gamma",
  images = {
    "battle_effct.png"
  }
})
table.insert(effectImageList, {
  effectName = "att_gravity",
  images = {
    "battle_effct.png",
    "LAZY_LOAD_gravity_0_00.png",
    "LAZY_LOAD_gravity_0_01.png",
    "LAZY_LOAD_gravity_0_02.png",
    "LAZY_LOAD_gravity_0_03.png",
    "LAZY_LOAD_gravity_0_04.png",
    "LAZY_LOAD_gravity_0_05.png",
    "LAZY_LOAD_gravity_0_06.png",
    "LAZY_LOAD_gravity_0_07.png",
    "LAZY_LOAD_gravity_0_08.png",
    "LAZY_LOAD_gravity_0_09.png",
    "LAZY_LOAD_gravity_0_10.png"
  }
})
table.insert(effectImageList, {
  effectName = "att_FULL_EMP",
  images = {
    "battle_effct.png",
    "LAZY_LOAD_EMP_00.png",
    "LAZY_LOAD_EMP_01.png",
    "LAZY_LOAD_EMP_02.png",
    "LAZY_LOAD_EMP_03.png",
    "LAZY_LOAD_EMP_04.png",
    "LAZY_LOAD_EMP_05.png",
    "LAZY_LOAD_EMP_06.png",
    "LAZY_LOAD_EMP_07.png",
    "LAZY_LOAD_EMP_08.png",
    "LAZY_LOAD_EMP_09.png",
    "LAZY_LOAD_EMP_10.png",
    "LAZY_LOAD_EMP_10.png",
    "LAZY_LOAD_EMP_11.png",
    "LAZY_LOAD_EMP_12.png",
    "LAZY_LOAD_EMP_13.png",
    "LAZY_LOAD_EMP_14.png",
    "LAZY_LOAD_EMP_15.png",
    "LAZY_LOAD_EMP_16.png",
    "LAZY_LOAD_EMP_17.png",
    "LAZY_LOAD_EMP_18.png",
    "LAZY_LOAD_EMP_19.png",
    "LAZY_LOAD_EMP_20.png",
    "LAZY_LOAD_EMP_21.png",
    "LAZY_LOAD_EMP_22.png",
    "LAZY_LOAD_EMP_23.png",
    "LAZY_LOAD_EMP_24.png"
  }
})
table.insert(effectImageList, {
  effectName = "att_indra",
  images = {
    "LAZY_LOAD_indra_attack_00.png",
    "LAZY_LOAD_indra_attack_01.png",
    "LAZY_LOAD_indra_attack_02.png",
    "LAZY_LOAD_indra_attack_03.png",
    "LAZY_LOAD_indra_attack_04.png",
    "LAZY_LOAD_indra_attack_05.png",
    "LAZY_LOAD_indra_attack_06.png",
    "LAZY_LOAD_indra_attack_07.png",
    "LAZY_LOAD_indra_attack_08.png",
    "LAZY_LOAD_indra_attack_09.png",
    "LAZY_LOAD_indra_attack_10.png",
    "LAZY_LOAD_indra_attack_11.png",
    "LAZY_LOAD_indra_attack_12.png",
    "LAZY_LOAD_indra_attack_13.png",
    "LAZY_LOAD_indra_attack_14.png",
    "LAZY_LOAD_indra_hurt_01.png",
    "LAZY_LOAD_indra_hurt_02.png",
    "LAZY_LOAD_indra_hurt_03.png",
    "LAZY_LOAD_indra_hurt_04.png",
    "LAZY_LOAD_indra_hurt_05.png",
    "LAZY_LOAD_indra_hurt_06.png",
    "LAZY_LOAD_indra_hurt_07.png",
    "LAZY_LOAD_indra_hurt_08.png",
    "LAZY_LOAD_indra_hurt_09.png",
    "LAZY_LOAD_indra_hurt_10.png",
    "LAZY_LOAD_indra_hurt_11.png",
    "LAZY_LOAD_indra_hurt_12.png",
    "LAZY_LOAD_indra_hurt_13.png",
    "LAZY_LOAD_indra_hurt_14.png",
    "LAZY_LOAD_indra_hurt_15.png",
    "LAZY_LOAD_indra_hurt_16.png",
    "LAZY_LOAD_indra_hurt_17.png"
  }
})
table.insert(effectImageList, {
  effectName = "att_indraBarrage",
  images = {
    "LAZY_LOAD_indra_attack_00.png",
    "LAZY_LOAD_indra_attack_01.png",
    "LAZY_LOAD_indra_attack_02.png",
    "LAZY_LOAD_indra_attack_03.png",
    "LAZY_LOAD_indra_attack_04.png",
    "LAZY_LOAD_indra_attack_05.png",
    "LAZY_LOAD_indra_attack_06.png",
    "LAZY_LOAD_indra_attack_07.png",
    "LAZY_LOAD_indra_attack_08.png",
    "LAZY_LOAD_indra_attack_09.png",
    "LAZY_LOAD_indra_attack_10.png",
    "LAZY_LOAD_indra_attack_11.png",
    "LAZY_LOAD_indra_attack_12.png",
    "LAZY_LOAD_indra_attack_13.png",
    "LAZY_LOAD_indra_attack_14.png",
    "LAZY_LOAD_indra_hurt_01.png",
    "LAZY_LOAD_indra_hurt_02.png",
    "LAZY_LOAD_indra_hurt_03.png",
    "LAZY_LOAD_indra_hurt_04.png",
    "LAZY_LOAD_indra_hurt_05.png",
    "LAZY_LOAD_indra_hurt_06.png",
    "LAZY_LOAD_indra_hurt_07.png",
    "LAZY_LOAD_indra_hurt_08.png",
    "LAZY_LOAD_indra_hurt_09.png",
    "LAZY_LOAD_indra_hurt_10.png",
    "LAZY_LOAD_indra_hurt_11.png",
    "LAZY_LOAD_indra_hurt_12.png",
    "LAZY_LOAD_indra_hurt_13.png",
    "LAZY_LOAD_indra_hurt_14.png",
    "LAZY_LOAD_indra_hurt_15.png",
    "LAZY_LOAD_indra_hurt_16.png",
    "LAZY_LOAD_indra_hurt_17.png"
  }
})
table.insert(effectImageList, {
  effectName = "att_gravityA",
  images = {
    "battle_effct.png",
    "LAZY_LOAD_gravity_0_00.png",
    "LAZY_LOAD_gravity_0_01.png",
    "LAZY_LOAD_gravity_0_02.png",
    "LAZY_LOAD_gravity_0_03.png",
    "LAZY_LOAD_gravity_0_04.png",
    "LAZY_LOAD_gravity_0_05.png",
    "LAZY_LOAD_gravity_0_06.png",
    "LAZY_LOAD_gravity_0_07.png",
    "LAZY_LOAD_gravity_0_08.png",
    "LAZY_LOAD_gravity_0_09.png",
    "LAZY_LOAD_gravity_0_10.png"
  }
})
table.insert(effectImageList, {
  effectName = "att_gravityB",
  images = {
    "battle_effct.png",
    "LAZY_LOAD_gravity_0_00.png",
    "LAZY_LOAD_gravity_0_01.png",
    "LAZY_LOAD_gravity_0_02.png",
    "LAZY_LOAD_gravity_0_03.png",
    "LAZY_LOAD_gravity_0_04.png",
    "LAZY_LOAD_gravity_0_05.png",
    "LAZY_LOAD_gravity_0_06.png",
    "LAZY_LOAD_gravity_0_07.png",
    "LAZY_LOAD_gravity_0_08.png",
    "LAZY_LOAD_gravity_0_09.png",
    "LAZY_LOAD_gravity_0_10.png"
  }
})
table.insert(effectImageList, {
  effectName = "att_gravityC",
  images = {
    "battle_effct.png",
    "LAZY_LOAD_gravity_0_00.png",
    "LAZY_LOAD_gravity_0_01.png",
    "LAZY_LOAD_gravity_0_02.png",
    "LAZY_LOAD_gravity_0_03.png",
    "LAZY_LOAD_gravity_0_04.png",
    "LAZY_LOAD_gravity_0_05.png",
    "LAZY_LOAD_gravity_0_06.png",
    "LAZY_LOAD_gravity_0_07.png",
    "LAZY_LOAD_gravity_0_08.png",
    "LAZY_LOAD_gravity_0_09.png",
    "LAZY_LOAD_gravity_0_10.png"
  }
})
table.insert(effectImageList, {
  effectName = "att_gammaBlasterA",
  images = {
    "battle_effct.png"
  }
})
table.insert(effectImageList, {
  effectName = "att_gammaBlasterC",
  images = {
    "battle_effct.png"
  }
})
table.insert(effectImageList, {
  effectName = "att_gammaBlasterD",
  images = {
    "battle_effct.png"
  }
})
table.insert(effectImageList, {
  effectName = "att_missile",
  images = {
    "LAZY_LOAD_missile_attack00.png",
    "LAZY_LOAD_missile_attack01.png",
    "LAZY_LOAD_missile_attack02.png",
    "LAZY_LOAD_missile_attack03.png",
    "LAZY_LOAD_missile_attack04.png",
    "LAZY_LOAD_missile_attack05.png",
    "LAZY_LOAD_missile_attack06.png",
    "LAZY_LOAD_missile_attack07.png",
    "LAZY_LOAD_missile_attack08.png",
    "LAZY_LOAD_missile_attack09.png",
    "LAZY_LOAD_missile_attack10.png",
    "LAZY_LOAD_missile_attack11.png",
    "LAZY_LOAD_missile_attack12.png",
    "LAZY_LOAD_missile_attack13.png",
    "LAZY_LOAD_missile_attack14.png",
    "LAZY_LOAD_missile_attack15.png",
    "LAZY_LOAD_missile_attack16.png",
    "LAZY_LOAD_missile_attack17.png",
    "LAZY_LOAD_missile_attack18.png",
    "LAZY_LOAD_missile_attack19.png",
    "LAZY_LOAD_missile_attack20.png",
    "LAZY_LOAD_missile_attack21.png",
    "LAZY_LOAD_missile_attack22.png",
    "LAZY_LOAD_missile_attack23.png",
    "LAZY_LOAD_missile_attack24.png",
    "LAZY_LOAD_missile_hurt01.png",
    "LAZY_LOAD_missile_hurt02.png",
    "LAZY_LOAD_missile_hurt03.png",
    "LAZY_LOAD_missile_hurt04.png",
    "LAZY_LOAD_missile_hurt05.png",
    "LAZY_LOAD_missile_hurt06.png",
    "LAZY_LOAD_missile_hurt07.png",
    "LAZY_LOAD_missile_hurt08.png"
  }
})
table.insert(effectImageList, {
  effectName = "att_indra_new",
  images = {
    "LAZY_LOAD_indra_new_attack00.png",
    "LAZY_LOAD_indra_new_attack01.png",
    "LAZY_LOAD_indra_new_attack02.png",
    "LAZY_LOAD_indra_new_attack03.png",
    "LAZY_LOAD_indra_new_attack04.png",
    "LAZY_LOAD_indra_new_attack05.png",
    "LAZY_LOAD_indra_new_attack06.png",
    "LAZY_LOAD_indra_new_attack07.png",
    "LAZY_LOAD_indra_new_attack08.png",
    "LAZY_LOAD_indra_new_attack09.png",
    "LAZY_LOAD_indra_new_attack_process00.png",
    "LAZY_LOAD_indra_new_attack_process02.png",
    "LAZY_LOAD_indra_new_attack_process04.png",
    "LAZY_LOAD_indra_new_attack_process06.png",
    "LAZY_LOAD_indra_new_attack_process08.png",
    "LAZY_LOAD_indra_new_attack_process10.png",
    "LAZY_LOAD_indra_new_attack_process12.png",
    "LAZY_LOAD_indra_new_attack_process14.png",
    "LAZY_LOAD_indra_new_attack_process16.png",
    "LAZY_LOAD_indra_new_attack_process18.png",
    "LAZY_LOAD_indra_new_attack_process20.png",
    "LAZY_LOAD_indra_new_attack_process22.png",
    "LAZY_LOAD_indra_new_attack_process24.png",
    "LAZY_LOAD_indra_new_attack_process26.png",
    "LAZY_LOAD_indra_new_attack_process28.png",
    "LAZY_LOAD_indra_new_attack_process30.png",
    "LAZY_LOAD_indra_new_hurt00.png",
    "LAZY_LOAD_indra_new_hurt02.png",
    "LAZY_LOAD_indra_new_hurt04.png",
    "LAZY_LOAD_indra_new_hurt06.png",
    "LAZY_LOAD_indra_new_hurt08.png",
    "LAZY_LOAD_indra_new_hurt10.png",
    "LAZY_LOAD_indra_new_attack_process00.png",
    "LAZY_LOAD_indra_new_attack_process02.png",
    "LAZY_LOAD_indra_new_attack_process04.png",
    "LAZY_LOAD_indra_new_attack_process06.png",
    "LAZY_LOAD_indra_new_attack_process08.png",
    "LAZY_LOAD_indra_new_attack_process10.png",
    "LAZY_LOAD_indra_new_attack_process12.png",
    "LAZY_LOAD_indra_new_attack_process14.png",
    "LAZY_LOAD_indra_new_attack_process16.png",
    "LAZY_LOAD_indra_new_attack_process18.png",
    "LAZY_LOAD_indra_new_attack_process20.png",
    "LAZY_LOAD_indra_new_attack_process22.png",
    "LAZY_LOAD_indra_new_attack_process24.png",
    "LAZY_LOAD_indra_new_attack_process26.png",
    "LAZY_LOAD_indra_new_attack_process28.png",
    "LAZY_LOAD_indra_new_attack_process30.png",
    "LAZY_LOAD_indra_new_hurt00.png",
    "LAZY_LOAD_indra_new_hurt02.png",
    "LAZY_LOAD_indra_new_hurt04.png",
    "LAZY_LOAD_indra_new_hurt06.png",
    "LAZY_LOAD_indra_new_hurt08.png",
    "LAZY_LOAD_indra_new_hurt10.png"
  }
})
table.insert(effectImageList, {
  effectName = "att_indra_cross_new",
  images = {
    "LAZY_LOAD_indra_cross_new_attack00.png",
    "LAZY_LOAD_indra_cross_new_attack01.png",
    "LAZY_LOAD_indra_cross_new_attack02.png",
    "LAZY_LOAD_indra_cross_new_attack03.png",
    "LAZY_LOAD_indra_cross_new_attack04.png",
    "LAZY_LOAD_indra_cross_new_attack05.png",
    "LAZY_LOAD_indra_cross_new_attack06.png",
    "LAZY_LOAD_indra_cross_new_attack07.png",
    "LAZY_LOAD_indra_cross_new_attack08.png",
    "LAZY_LOAD_indra_cross_new_attack09.png",
    "LAZY_LOAD_indra_cross_new_attack10.png",
    "LAZY_LOAD_indra_cross_new_attack11.png",
    "LAZY_LOAD_indra_cross_new_attack12.png",
    "LAZY_LOAD_indra_cross_new_attack13.png",
    "LAZY_LOAD_indra_cross_new_attack14.png",
    "LAZY_LOAD_indra_cross_new_attack15.png",
    "LAZY_LOAD_indra_cross_hurt_center00.png",
    "LAZY_LOAD_indra_cross_hurt_center01.png",
    "LAZY_LOAD_indra_cross_hurt_center02.png",
    "LAZY_LOAD_indra_cross_hurt_center03.png",
    "LAZY_LOAD_indra_cross_hurt_center04.png",
    "LAZY_LOAD_indra_cross_hurt_center05.png",
    "LAZY_LOAD_indra_cross_hurt_center06.png",
    "LAZY_LOAD_indra_cross_hurt_center07.png",
    "LAZY_LOAD_indra_cross_hurt_center08.png",
    "LAZY_LOAD_indra_cross_hurt_center09.png",
    "LAZY_LOAD_indra_cross_hurt_center10.png"
  }
})
table.insert(effectImageList, {
  effectName = "att_EMP_new",
  images = {
    "LAZY_LOAD_effect_sp00.png",
    "LAZY_LOAD_effect_sp01.png",
    "LAZY_LOAD_effect_sp02.png",
    "LAZY_LOAD_effect_sp03.png",
    "LAZY_LOAD_effect_sp04.png",
    "LAZY_LOAD_effect_sp05.png",
    "LAZY_LOAD_effect_sp06.png",
    "LAZY_LOAD_effect_sp07.png",
    "LAZY_LOAD_effect_sp08.png",
    "LAZY_LOAD_effect_sp09.png",
    "LAZY_LOAD_effect_sp10.png",
    "LAZY_LOAD_effect_sp11.png",
    "LAZY_LOAD_effect_sp12.png",
    "LAZY_LOAD_EMP_new_hurt_single0100.png",
    "LAZY_LOAD_EMP_new_hurt_single0102.png",
    "LAZY_LOAD_EMP_new_hurt_single0104.png",
    "LAZY_LOAD_EMP_new_hurt_single0106.png",
    "LAZY_LOAD_EMP_new_hurt_single0108.png",
    "LAZY_LOAD_EMP_new_hurt_single0110.png",
    "LAZY_LOAD_EMP_new_hurt_single0112.png",
    "LAZY_LOAD_EMP_new_hurt_single0114.png",
    "LAZY_LOAD_EMP_new_hurt_single0200.png",
    "LAZY_LOAD_EMP_new_hurt_single0201.png",
    "LAZY_LOAD_EMP_new_hurt_single0202.png",
    "LAZY_LOAD_EMP_new_hurt_single0203.png",
    "LAZY_LOAD_EMP_new_hurt_single0204.png",
    "LAZY_LOAD_EMP_new_hurt_single0205.png",
    "LAZY_LOAD_EMP_new_hurt_single0206.png",
    "LAZY_LOAD_EMP_new_hurt_single0207.png",
    "LAZY_LOAD_EMP_new_hurt_single0208.png",
    "LAZY_LOAD_EMP_new_hurt_single0209.png"
  }
})
table.insert(effectImageList, {
  effectName = "att_indra_vertical",
  images = {
    "LAZY_LOAD_effect_sp00.png",
    "LAZY_LOAD_effect_sp01.png",
    "LAZY_LOAD_effect_sp02.png",
    "LAZY_LOAD_effect_sp03.png",
    "LAZY_LOAD_effect_sp04.png",
    "LAZY_LOAD_effect_sp05.png",
    "LAZY_LOAD_effect_sp06.png",
    "LAZY_LOAD_effect_sp07.png",
    "LAZY_LOAD_effect_sp08.png",
    "LAZY_LOAD_effect_sp09.png",
    "LAZY_LOAD_effect_sp10.png",
    "LAZY_LOAD_effect_sp11.png",
    "LAZY_LOAD_effect_sp12.png",
    "LAZY_LOAD_indra_cross_hurt_center00.png",
    "LAZY_LOAD_indra_cross_hurt_center01.png",
    "LAZY_LOAD_indra_cross_hurt_center02.png",
    "LAZY_LOAD_indra_cross_hurt_center03.png",
    "LAZY_LOAD_indra_cross_hurt_center04.png",
    "LAZY_LOAD_indra_cross_hurt_center05.png",
    "LAZY_LOAD_indra_cross_hurt_center06.png",
    "LAZY_LOAD_indra_cross_hurt_center07.png",
    "LAZY_LOAD_indra_cross_hurt_center08.png",
    "LAZY_LOAD_indra_cross_hurt_center09.png",
    "LAZY_LOAD_indra_cross_hurt_center10.png",
    "LAZY_LOAD_indra_vertical_attack00.png",
    "LAZY_LOAD_indra_vertical_attack01.png",
    "LAZY_LOAD_indra_vertical_attack02.png",
    "LAZY_LOAD_indra_vertical_attack03.png",
    "LAZY_LOAD_indra_vertical_attack04.png",
    "LAZY_LOAD_indra_vertical_attack05.png",
    "LAZY_LOAD_indra_vertical_attack07.png",
    "LAZY_LOAD_indra_vertical_attack08.png",
    "LAZY_LOAD_indra_vertical_attack09.png",
    "LAZY_LOAD_indra_vertical_attack10.png",
    "LAZY_LOAD_indra_cross_hurt_center00.png",
    "LAZY_LOAD_indra_cross_hurt_center01.png",
    "LAZY_LOAD_indra_cross_hurt_center02.png",
    "LAZY_LOAD_indra_cross_hurt_center03.png",
    "LAZY_LOAD_indra_cross_hurt_center04.png",
    "LAZY_LOAD_indra_cross_hurt_center05.png",
    "LAZY_LOAD_indra_cross_hurt_center06.png",
    "LAZY_LOAD_indra_cross_hurt_center07.png",
    "LAZY_LOAD_indra_cross_hurt_center08.png",
    "LAZY_LOAD_indra_cross_hurt_center09.png",
    "LAZY_LOAD_indra_cross_hurt_center10.png",
    "LAZY_LOAD_indra_cross_hurt_center00.png",
    "LAZY_LOAD_indra_cross_hurt_center01.png",
    "LAZY_LOAD_indra_cross_hurt_center02.png",
    "LAZY_LOAD_indra_cross_hurt_center03.png",
    "LAZY_LOAD_indra_cross_hurt_center04.png",
    "LAZY_LOAD_indra_cross_hurt_center05.png",
    "LAZY_LOAD_indra_cross_hurt_center06.png",
    "LAZY_LOAD_indra_cross_hurt_center07.png",
    "LAZY_LOAD_indra_cross_hurt_center08.png",
    "LAZY_LOAD_indra_cross_hurt_center09.png",
    "LAZY_LOAD_indra_cross_hurt_center10.png",
    "LAZY_LOAD_indra_vertical_attack00.png",
    "LAZY_LOAD_indra_vertical_attack01.png",
    "LAZY_LOAD_indra_vertical_attack02.png",
    "LAZY_LOAD_indra_vertical_attack03.png",
    "LAZY_LOAD_indra_vertical_attack04.png",
    "LAZY_LOAD_indra_vertical_attack05.png",
    "LAZY_LOAD_indra_vertical_attack07.png",
    "LAZY_LOAD_indra_vertical_attack08.png",
    "LAZY_LOAD_indra_vertical_attack09.png",
    "LAZY_LOAD_indra_vertical_attack10.png"
  }
})
table.insert(effectImageList, {
  effectName = "att_laser",
  images = {
    "battle_effct.png"
  }
})
table.insert(effectImageList, {
  effectName = "att_normal2",
  images = {
    "battle_effct.png"
  }
})
table.insert(effectImageList, {
  effectName = "att_FULL_gravity",
  images = {
    "battle_effct.png",
    "LAZY_LOAD_EMP_00.png",
    "LAZY_LOAD_EMP_01.png",
    "LAZY_LOAD_EMP_02.png",
    "LAZY_LOAD_EMP_03.png",
    "LAZY_LOAD_EMP_04.png",
    "LAZY_LOAD_gravity_1_00.png",
    "LAZY_LOAD_gravity_1_01.png",
    "LAZY_LOAD_gravity_1_02.png",
    "LAZY_LOAD_gravity_1_03.png",
    "LAZY_LOAD_gravity_1_04.png",
    "LAZY_LOAD_gravity_1_05.png",
    "LAZY_LOAD_gravity_1_06.png",
    "LAZY_LOAD_gravity_1_07.png",
    "LAZY_LOAD_gravity_1_08.png",
    "LAZY_LOAD_gravity_1_09.png",
    "LAZY_LOAD_gravity_1_10.png"
  }
})
table.insert(effectImageList, {
  effectName = "att_gammaBlaster",
  images = {
    "battle_effct.png"
  }
})
table.insert(effectImageList, {
  effectName = "att_photonMissile",
  images = {
    "battle_effct.png",
    "LAZY_LOAD_missile_light.png",
    "LAZY_LOAD_missile_head.png",
    "LAZY_LOAD_missile_flame.png",
    "LAZY_LOAD_missile_fly_line.png"
  }
})
table.insert(effectImageList, {
  effectName = "att_normal",
  images = {
    "LAZY_LOAD_missile_head.png",
    "LAZY_LOAD_missile_fly_line.png",
    "LAZY_LOAD_missile_light.png",
    "LAZY_LOAD_missile_flame.png"
  }
})
table.insert(effectImageList, {
  effectName = "att_indraB",
  images = {
    "LAZY_LOAD_indra_attack_00.png",
    "LAZY_LOAD_indra_attack_01.png",
    "LAZY_LOAD_indra_attack_02.png",
    "LAZY_LOAD_indra_attack_03.png",
    "LAZY_LOAD_indra_attack_04.png",
    "LAZY_LOAD_indra_attack_05.png",
    "LAZY_LOAD_indra_attack_06.png",
    "LAZY_LOAD_indra_attack_07.png",
    "LAZY_LOAD_indra_attack_08.png",
    "LAZY_LOAD_indra_attack_09.png",
    "LAZY_LOAD_indra_attack_10.png",
    "LAZY_LOAD_indra_attack_11.png",
    "LAZY_LOAD_indra_attack_12.png",
    "LAZY_LOAD_indra_attack_13.png",
    "LAZY_LOAD_indra_attack_14.png",
    "LAZY_LOAD_indra_hurt_01.png",
    "LAZY_LOAD_indra_hurt_02.png",
    "LAZY_LOAD_indra_hurt_03.png",
    "LAZY_LOAD_indra_hurt_04.png",
    "LAZY_LOAD_indra_hurt_05.png",
    "LAZY_LOAD_indra_hurt_06.png",
    "LAZY_LOAD_indra_hurt_07.png",
    "LAZY_LOAD_indra_hurt_08.png",
    "LAZY_LOAD_indra_hurt_09.png",
    "LAZY_LOAD_indra_hurt_10.png",
    "LAZY_LOAD_indra_hurt_11.png",
    "LAZY_LOAD_indra_hurt_12.png",
    "LAZY_LOAD_indra_hurt_13.png",
    "LAZY_LOAD_indra_hurt_14.png",
    "LAZY_LOAD_indra_hurt_15.png",
    "LAZY_LOAD_indra_hurt_16.png",
    "LAZY_LOAD_indra_hurt_17.png"
  }
})
table.insert(effectImageList, {
  effectName = "att_indraBarrageB",
  images = {
    "LAZY_LOAD_indra_attack_00.png",
    "LAZY_LOAD_indra_attack_01.png",
    "LAZY_LOAD_indra_attack_02.png",
    "LAZY_LOAD_indra_attack_03.png",
    "LAZY_LOAD_indra_attack_04.png",
    "LAZY_LOAD_indra_attack_05.png",
    "LAZY_LOAD_indra_attack_06.png",
    "LAZY_LOAD_indra_attack_07.png",
    "LAZY_LOAD_indra_attack_08.png",
    "LAZY_LOAD_indra_attack_09.png",
    "LAZY_LOAD_indra_attack_10.png",
    "LAZY_LOAD_indra_attack_11.png",
    "LAZY_LOAD_indra_attack_12.png",
    "LAZY_LOAD_indra_attack_13.png",
    "LAZY_LOAD_indra_attack_14.png",
    "LAZY_LOAD_indra_hurt_01.png",
    "LAZY_LOAD_indra_hurt_02.png",
    "LAZY_LOAD_indra_hurt_03.png",
    "LAZY_LOAD_indra_hurt_04.png",
    "LAZY_LOAD_indra_hurt_05.png",
    "LAZY_LOAD_indra_hurt_06.png",
    "LAZY_LOAD_indra_hurt_07.png",
    "LAZY_LOAD_indra_hurt_08.png",
    "LAZY_LOAD_indra_hurt_09.png",
    "LAZY_LOAD_indra_hurt_10.png",
    "LAZY_LOAD_indra_hurt_11.png",
    "LAZY_LOAD_indra_hurt_12.png",
    "LAZY_LOAD_indra_hurt_13.png",
    "LAZY_LOAD_indra_hurt_14.png",
    "LAZY_LOAD_indra_hurt_15.png",
    "LAZY_LOAD_indra_hurt_16.png",
    "LAZY_LOAD_indra_hurt_17.png"
  }
})
table.insert(effectImageList, {
  effectName = "att_gammaBlasterB",
  images = {
    "battle_effct.png"
  }
})
table.insert(effectImageList, {
  effectName = "att_locked",
  images = {
    "LAZY_LOAD_locked_attack00000.png",
    "LAZY_LOAD_locked_attack00001.png",
    "LAZY_LOAD_locked_attack00002.png",
    "LAZY_LOAD_locked_attack00003.png",
    "LAZY_LOAD_locked_attack00004.png",
    "LAZY_LOAD_locked_attack00005.png",
    "LAZY_LOAD_locked_attack00006.png",
    "LAZY_LOAD_hit03_00.png",
    "LAZY_LOAD_hit03_01.png",
    "LAZY_LOAD_hit03_02.png"
  }
})
table.insert(effectImageList, {
  effectName = "att_ice",
  images = {
    "LAZY_LOAD_locked_attack00000.png",
    "LAZY_LOAD_locked_attack00001.png",
    "LAZY_LOAD_locked_attack00002.png",
    "LAZY_LOAD_locked_attack00003.png",
    "LAZY_LOAD_locked_attack00004.png",
    "LAZY_LOAD_locked_attack00005.png",
    "LAZY_LOAD_locked_attack00006.png",
    "LAZY_LOAD_hit03_00.png",
    "LAZY_LOAD_hit03_01.png",
    "LAZY_LOAD_hit03_02.png",
    "LAZY_LOAD_hit03_00.png",
    "LAZY_LOAD_hit03_01.png",
    "LAZY_LOAD_hit03_02.png",
    "LAZY_LOAD_hit03_00.png",
    "LAZY_LOAD_hit03_01.png",
    "LAZY_LOAD_hit03_02.png"
  }
})
table.insert(effectImageList, {
  effectName = "att_laser_new",
  images = {
    "LAZY_LOAD_laser_attack_new00.png",
    "LAZY_LOAD_laser_attack_new01.png",
    "LAZY_LOAD_laser_attack_new02.png",
    "LAZY_LOAD_laser_attack_new03.png",
    "LAZY_LOAD_laser_attack_new04.png",
    "LAZY_LOAD_laser_attack_new05.png",
    "LAZY_LOAD_laser_attack_new06.png",
    "LAZY_LOAD_laser_attack_new07.png",
    "LAZY_LOAD_laser_attack_new08.png",
    "LAZY_LOAD_laser_attack_new09.png",
    "LAZY_LOAD_laser_attack_new10.png",
    "LAZY_LOAD_laser_attack_new11.png",
    "LAZY_LOAD_laser_attack_new12.png",
    "LAZY_LOAD_laser_attack_new13.png",
    "LAZY_LOAD_laser_attack_new14.png",
    "LAZY_LOAD_laser_attack_new15.png",
    "LAZY_LOAD_laser_attack_new16.png",
    "LAZY_LOAD_black_hole_hurt0.png",
    "LAZY_LOAD_black_hole_hurt1.png",
    "LAZY_LOAD_black_hole_hurt2.png",
    "LAZY_LOAD_black_hole_hurt3.png",
    "LAZY_LOAD_black_hole_hurt4.png",
    "LAZY_LOAD_black_hole_hurt5.png",
    "LAZY_LOAD_black_hole_hurt6.png"
  }
})
table.insert(effectImageList, {
  effectName = "att_normal_new",
  images = {
    "LAZY_LOAD_normal_attack_new00.png",
    "LAZY_LOAD_normal_attack_new02.png",
    "LAZY_LOAD_normal_attack_new04.png",
    "LAZY_LOAD_normal_attack_new06.png",
    "LAZY_LOAD_normal_attack_new08.png",
    "LAZY_LOAD_normal_attack_new10.png",
    "LAZY_LOAD_normal_attack_new12.png",
    "LAZY_LOAD_normal_attack_new14.png",
    "LAZY_LOAD_normal_attack_new16.png",
    "LAZY_LOAD_normal_attack_new18.png",
    "LAZY_LOAD_normal_attack_new20.png",
    "LAZY_LOAD_normal_attack_new22.png",
    "LAZY_LOAD_normal_attack_new24.png",
    "LAZY_LOAD_missile_hurt01.png",
    "LAZY_LOAD_missile_hurt02.png",
    "LAZY_LOAD_missile_hurt03.png",
    "LAZY_LOAD_missile_hurt04.png",
    "LAZY_LOAD_missile_hurt05.png",
    "LAZY_LOAD_missile_hurt06.png",
    "LAZY_LOAD_missile_hurt07.png",
    "LAZY_LOAD_missile_hurt08.png"
  }
})
table.insert(effectImageList, {
  effectName = "att_black_new",
  images = {
    "LAZY_LOAD_laser_attack_new00.png",
    "LAZY_LOAD_laser_attack_new01.png",
    "LAZY_LOAD_laser_attack_new02.png",
    "LAZY_LOAD_laser_attack_new03.png",
    "LAZY_LOAD_laser_attack_new04.png",
    "LAZY_LOAD_laser_attack_new05.png",
    "LAZY_LOAD_laser_attack_new06.png",
    "LAZY_LOAD_laser_attack_new07.png",
    "LAZY_LOAD_laser_attack_new08.png",
    "LAZY_LOAD_laser_attack_new09.png",
    "LAZY_LOAD_laser_attack_new10.png",
    "LAZY_LOAD_laser_attack_new11.png",
    "LAZY_LOAD_laser_attack_new12.png",
    "LAZY_LOAD_laser_attack_new13.png",
    "LAZY_LOAD_laser_attack_new14.png",
    "LAZY_LOAD_laser_attack_new15.png",
    "LAZY_LOAD_laser_attack_new16.png",
    "LAZY_LOAD_black_hole_hurt0.png",
    "LAZY_LOAD_black_hole_hurt1.png",
    "LAZY_LOAD_black_hole_hurt2.png",
    "LAZY_LOAD_black_hole_hurt3.png",
    "LAZY_LOAD_black_hole_hurt4.png",
    "LAZY_LOAD_black_hole_hurt5.png",
    "LAZY_LOAD_black_hole_hurt6.png"
  }
})
table.insert(effectImageList, {
  effectName = "att_normal_new2",
  images = {
    "LAZY_LOAD_normal_new_attack00.png",
    "LAZY_LOAD_normal_new_attack02.png",
    "LAZY_LOAD_normal_new_attack04.png",
    "LAZY_LOAD_normal_new_attack06.png",
    "LAZY_LOAD_normal_new_attack08.png",
    "LAZY_LOAD_normal_new_attack10.png",
    "LAZY_LOAD_normal_new_attack12.png",
    "LAZY_LOAD_normal_new_attack14.png",
    "LAZY_LOAD_normal_new_attack16.png",
    "LAZY_LOAD_normal_new_attack18.png",
    "LAZY_LOAD_normal_new_attack20.png",
    "LAZY_LOAD_normal_new_attack22.png",
    "LAZY_LOAD_normal_new_attack24.png",
    "LAZY_LOAD_normal_new_hurt00.png",
    "LAZY_LOAD_normal_new_hurt01.png",
    "LAZY_LOAD_normal_new_hurt02.png",
    "LAZY_LOAD_normal_new_hurt03.png",
    "LAZY_LOAD_normal_new_hurt04.png",
    "LAZY_LOAD_normal_new_hurt05.png",
    "LAZY_LOAD_normal_new_hurt06.png",
    "LAZY_LOAD_normal_new_hurt07.png",
    "LAZY_LOAD_normal_new_hurt08.png"
  }
})
table.insert(effectImageList, {
  effectName = "att_disturb",
  images = {
    "battle_effct.png",
    "LAZY_LOAD_disturb_hurt_center00.png",
    "LAZY_LOAD_disturb_hurt_center01.png",
    "LAZY_LOAD_disturb_hurt_center02.png",
    "LAZY_LOAD_disturb_hurt_center03.png",
    "LAZY_LOAD_disturb_hurt_center04.png",
    "LAZY_LOAD_disturb_hurt_center05.png"
  }
})
table.insert(effectImageList, {
  effectName = "att_indra_vertica",
  images = {
    "LAZY_LOAD_effect_sp00.png",
    "LAZY_LOAD_effect_sp01.png",
    "LAZY_LOAD_effect_sp02.png",
    "LAZY_LOAD_effect_sp03.png",
    "LAZY_LOAD_effect_sp04.png",
    "LAZY_LOAD_effect_sp05.png",
    "LAZY_LOAD_effect_sp06.png",
    "LAZY_LOAD_effect_sp07.png",
    "LAZY_LOAD_effect_sp08.png",
    "LAZY_LOAD_effect_sp09.png",
    "LAZY_LOAD_effect_sp10.png",
    "LAZY_LOAD_effect_sp11.png",
    "LAZY_LOAD_effect_sp12.png",
    "LAZY_LOAD_indra_vertical_attack00.png",
    "LAZY_LOAD_indra_vertical_attack01.png",
    "LAZY_LOAD_indra_vertical_attack02.png",
    "LAZY_LOAD_indra_vertical_attack03.png",
    "LAZY_LOAD_indra_vertical_attack04.png",
    "LAZY_LOAD_indra_vertical_attack05.png",
    "LAZY_LOAD_indra_vertical_attack07.png",
    "LAZY_LOAD_indra_vertical_attack08.png",
    "LAZY_LOAD_indra_vertical_attack09.png",
    "LAZY_LOAD_indra_vertical_attack10.png",
    "LAZY_LOAD_indra_cross_hurt_center00.png",
    "LAZY_LOAD_indra_cross_hurt_center01.png",
    "LAZY_LOAD_indra_cross_hurt_center02.png",
    "LAZY_LOAD_indra_cross_hurt_center03.png",
    "LAZY_LOAD_indra_cross_hurt_center04.png",
    "LAZY_LOAD_indra_cross_hurt_center05.png",
    "LAZY_LOAD_indra_cross_hurt_center06.png",
    "LAZY_LOAD_indra_cross_hurt_center07.png",
    "LAZY_LOAD_indra_cross_hurt_center08.png",
    "LAZY_LOAD_indra_cross_hurt_center09.png",
    "LAZY_LOAD_indra_cross_hurt_center10.png"
  }
})
table.insert(effectImageList, {
  effectName = "att_EMP_new01_single",
  images = {
    "LAZY_LOAD_effect_sp00.png",
    "LAZY_LOAD_effect_sp01.png",
    "LAZY_LOAD_effect_sp02.png",
    "LAZY_LOAD_effect_sp03.png",
    "LAZY_LOAD_effect_sp04.png",
    "LAZY_LOAD_effect_sp05.png",
    "LAZY_LOAD_effect_sp06.png",
    "LAZY_LOAD_effect_sp07.png",
    "LAZY_LOAD_effect_sp08.png",
    "LAZY_LOAD_effect_sp09.png",
    "LAZY_LOAD_effect_sp10.png",
    "LAZY_LOAD_effect_sp11.png",
    "LAZY_LOAD_effect_sp12.png",
    "LAZY_LOAD_EMP_new_hurt_single0100.png",
    "LAZY_LOAD_EMP_new_hurt_single0102.png",
    "LAZY_LOAD_EMP_new_hurt_single0104.png",
    "LAZY_LOAD_EMP_new_hurt_single0106.png",
    "LAZY_LOAD_EMP_new_hurt_single0108.png",
    "LAZY_LOAD_EMP_new_hurt_single0110.png",
    "LAZY_LOAD_EMP_new_hurt_single0112.png",
    "LAZY_LOAD_EMP_new_hurt_single0114.png"
  }
})
