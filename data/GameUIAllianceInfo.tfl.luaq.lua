local GameStateAlliance = GameStateManager.GameStateAlliance
local GameUIAllianceInfo = LuaObjectManager:GetLuaObject("GameUIAllianceInfo")
local GameUIUnalliance = LuaObjectManager:GetLuaObject("GameUIUnalliance")
local GameUIAllianceList = LuaObjectManager:GetLuaObject("GameUIAllianceList")
local GameMail = LuaObjectManager:GetLuaObject("GameMail")
function GameUIAllianceInfo:OnFSCommand(cmd, arg)
  if cmd == "move_out" then
    GameStateAlliance:EraseObject(self)
    if GameUIUnalliance:IsActive() then
      GameUIUnalliance:SelectAlliance(-1)
    elseif GameUIAllianceList:IsActive() then
      GameUIAllianceList:SelectAlliance(-1)
    end
  elseif cmd == "apply_alliance" then
    if GameUIUnalliance:IsActive() then
      GameUIUnalliance:ApplyAlliance(self.alliance_data.id)
      self:MoveOut()
    end
  elseif cmd == "unapply_alliance" then
    if GameUIUnalliance:IsActive() then
      GameUIUnalliance:UnapplyAlliance(self.alliance_data.id)
    end
  elseif cmd == "send_email_to_chairman" then
    GameStateManager:GetCurrentGameState():AddObject(GameMail)
    GameMail:ShowBox(GameMail.MAILBOX_WRITE_TAB)
    GameMail:SetWrite(GameUIAllianceInfo.alliance_data.creator_name, "", "")
  end
end
function GameUIAllianceInfo:SetAllianceData(alliance_data)
  self.alliance_data = alliance_data
  if self:GetFlashObject() then
    self:UpdateAllianceInfo()
  end
end
function GameUIAllianceInfo:GetAllianceData()
  return self.alliance_data
end
function GameUIAllianceInfo:SetApplyStatus(apply_status)
  self.apply_status = apply_status
  if self:GetFlashObject() then
    self:UpdateApplyStatus()
  end
end
function GameUIAllianceInfo:UpdateAllianceInfo()
  if not self.alliance_data then
    return
  end
  local alliance_data = self.alliance_data
  local alliance_rank = alliance_data.rank
  local alliance_name = alliance_data.name
  local alliance_level = alliance_data.level_info.level
  local alliance_members = alliance_data.members_count
  local alliance_maxmembers = alliance_data.members_max
  local alliance_chairman = GameUtils:GetUserDisplayName(alliance_data.creator_name)
  local alliance_desc = alliance_data.memo
  self:GetFlashObject():InvokeASCallback("_root", "updateAllianceData", alliance_rank, alliance_name, alliance_level, alliance_desc, alliance_members, alliance_maxmembers, alliance_chairman)
end
function GameUIAllianceInfo:UpdateApplyStatus()
  local apply_status = self.apply_status
  apply_status = apply_status or -1
  self:GetFlashObject():InvokeASCallback("_root", "updateApplyStatus", apply_status)
  if apply_status == "apply" then
    local apply_path = self:GetFlashObject():InvokeASCallback("_root", "getObjectPath", "btn_apply")
    self:GetFlashObject():SetBtnEnable(apply_path, true)
  elseif apply_status == "disable" then
    local apply_path = self:GetFlashObject():InvokeASCallback("_root", "getObjectPath", "btn_apply")
    self:GetFlashObject():SetBtnEnable(apply_path, false)
  end
end
function GameUIAllianceInfo:MoveIn()
  self:GetFlashObject():InvokeASCallback("_root", "animationMoveIn")
end
function GameUIAllianceInfo:MoveOut()
  self:GetFlashObject():InvokeASCallback("_root", "animationMoveOut")
end
function GameUIAllianceInfo:InitFlashObject()
end
function GameUIAllianceInfo:OnAddToGameState(game_state)
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  self:UpdateAllianceInfo()
  self:UpdateApplyStatus()
  self:MoveIn()
  self.is_active = true
end
function GameUIAllianceInfo:OnEraseFromGameState(game_state)
  self.is_active = nil
  self.alliance_data = nil
  self.apply_status = nil
  self:UnloadFlashObject()
end
function GameUIAllianceInfo:IsActive()
  return self.is_active
end
if AutoUpdate.isAndroidDevice then
  function GameUIAllianceInfo.OnAndroidBack()
    GameUIAllianceInfo:GetFlashObject():InvokeASCallback("_root", "animationMoveOut")
  end
end
