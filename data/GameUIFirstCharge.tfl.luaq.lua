local GameUIFirstCharge = LuaObjectManager:GetLuaObject("GameUIFirstCharge")
local GameVip = LuaObjectManager:GetLuaObject("GameVip")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
local GameWaiting = LuaObjectManager:GetLuaObject("GameWaiting")
local GameUIWDStuff = LuaObjectManager:GetLuaObject("GameUIWDStuff")
local GameUIBarLeft = LuaObjectManager:GetLuaObject("GameUIBarLeft")
local GameUIKrypton = LuaObjectManager:GetLuaObject("GameUIKrypton")
GameUIFirstCharge.allData = {}
GameUIFirstCharge.currentIndex = 1
GameUIFirstCharge.mtime = 0
GameUIFirstCharge.isUpdate = false
function GameUIFirstCharge:OnAddToGameState()
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  GameUIFirstCharge:OnInit(false)
  GameUIFirstCharge:MoveIn()
  GameUIFirstCharge:GetDataFromServer()
end
function GameUIFirstCharge:OnEraseFromGameState()
  GameUIFirstCharge:UnloadFlashObject()
  GameUIFirstCharge.currentIndex = 1
  GameUIFirstCharge.isUpdate = false
  GameUIFirstCharge.mtime = 0
end
function GameUIFirstCharge:OnInit(flag)
  local lang = "en"
  if GameSettingData and GameSettingData.Save_Lang then
    local la = GameSettingData.Save_Lang
    if string.find(la, "ru") == 1 then
      lang = "ru"
    end
  end
  self:GetFlashObject():InvokeASCallback("_root", "init", flag, lang)
end
function GameUIFirstCharge:MoveIn()
  self:GetFlashObject():InvokeASCallback("_root", "MoveIn")
end
function GameUIFirstCharge:RewardMoveIn()
  self:GetFlashObject():InvokeASCallback("_root", "RewardMoveIn")
end
function GameUIFirstCharge:MoveOut()
  self:GetFlashObject():InvokeASCallback("_root", "MoveOut")
end
function GameUIFirstCharge:GetDataFromServer()
  if ext.GetPlatform() == "Win32" then
    GameVip.isPriceValid = true
    GameUIFirstCharge:OnEnter()
  else
    GameVip:RequestProductIdentifierList()
  end
end
function GameUIFirstCharge:Update(dt)
  if self:GetFlashObject() then
    self:GetFlashObject():Update(dt)
    self:GetFlashObject():InvokeASCallback("_root", "update", dt)
    GameUIFirstCharge.mtime = GameUIFirstCharge.mtime + dt
    if GameUIFirstCharge.mtime > 1000 then
      GameUIFirstCharge.mtime = 0
      if GameUIFirstCharge.isUpdate then
        GameUIFirstCharge:DownloadImageOverCallback()
      end
    end
  end
end
function GameUIFirstCharge:OnEnter()
  if self:GetFlashObject() and GameVip.isPriceValid then
    GameUIFirstCharge:UpdateAllData()
    GameUIFirstCharge:SortMidAllData()
    GameUIFirstCharge:OnInit(true)
    GameUIFirstCharge:UpdateMainLayerData(GameUIFirstCharge.currentIndex)
    GameUIFirstCharge:InitSkillDesPanel()
    GameUIFirstCharge:RewardMoveIn()
    GameUIFirstCharge.isUpdate = true
  end
end
function GameUIFirstCharge.NetCallbackGetData(content)
  GameUIFirstCharge.allData = content.charge_items
  GameUIBarLeft:UpdateFirstChargeStatus(GameUIFirstCharge:GetIsShow())
  GameVip.UpdateFirstCharge()
end
function GameUIFirstCharge:GetIsFirstCharge(productId)
  if GameUIFirstCharge.allData then
    for k, v in pairs(GameUIFirstCharge.allData) do
      if v.product_id == productId then
        if v.first_buy then
          return 1
        else
          return 0
        end
      end
    end
  end
  return 0
end
function GameUIFirstCharge:GetIsShow()
  local isShowFirstChargeBun = false
  if GameUIFirstCharge.allData then
    for k, v in pairs(GameUIFirstCharge.allData) do
      if v.first_buy then
        isShowFirstChargeBun = true
        break
      end
    end
  end
  local levelInfo = GameGlobalData:GetData("levelinfo")
  return isShowFirstChargeBun and levelInfo.level >= 5
end
function GameUIFirstCharge:GetPriceByProductID(productId)
  if not GameVip.ProductionsList then
    return nil
  end
  for k, v in pairs(GameVip.ProductionsList) do
    if v.id == productId then
      return v.credit
    end
  end
end
function GameUIFirstCharge:UpdateAllData()
  GameUIFirstCharge.midAllData = {}
  for k, v in pairs(self.allData) do
    local _item = v
    local price = GameVip.GetPriceByProductID(v.product_id)
    _item.priceText = price
    _item.credit = GameUIFirstCharge:GetPriceByProductID(v.product_id)
    if v.first_buy then
      _item.priority = 1
    else
      _item.priority = 0
    end
    table.insert(GameUIFirstCharge.midAllData, _item)
  end
  DebugOutPutTable(self.midAllData, "midAllData = ")
end
function GameUIFirstCharge:SortMidAllData()
  table.sort(self.midAllData, function(v1, v2)
    if v1.priority == v2.priority then
      return tonumber(v1.credit) < tonumber(v2.credit)
    else
      return v1.priority > v2.priority
    end
  end)
end
function GameUIFirstCharge:GetShowCurrentIndx(isNext)
  local _m = 0
  if isNext then
    _m = (self.currentIndex + 1) % #self.midAllData
  else
    _m = (self.currentIndex - 1) % #self.midAllData
  end
  if _m == 0 then
    self.currentIndex = #self.midAllData
  else
    self.currentIndex = _m
  end
end
function GameUIFirstCharge:UpdateMainLayerData(index)
  local item = self.midAllData[index]
  if not item then
    return
  end
  local displayerInfo = item.show_info
  local vessels_image_name = GameDataAccessHelper:GetCommanderShipByDownloadState(displayerInfo.ship)
  local vessels_type_frame = FleetDataAccessHelper:GetFleetVesselsFrameByVessel(displayerInfo.vessels)
  local colorFrame = FleetDataAccessHelper:GetFleetColorFrameByColor(displayerInfo.color)
  local spellFrame = FleetDataAccessHelper:GetFleetSpellFrameBySpell(displayerInfo.spell_id)
  local commander_name = FleetDataAccessHelper:GetFleetLevelDisplayName(displayerInfo.name, displayerInfo.fleet_id, displayerInfo.color, 0)
  local avatarFrame = FleetDataAccessHelper:GetFleetAvatarFrameByAvatar(displayerInfo.avatar, displayerInfo.fleet_id)
  local vessels_name = FleetDataAccessHelper:GetFleetVesselsTypeStringByVessel(displayerInfo.vessels)
  local basic_info = GameDataAccessHelper:GetSkillNameText(displayerInfo.fleet_id, item.show_detail.level)
  local data_table = {}
  data_table.commander_name = commander_name
  data_table.commander_color = GameDataAccessHelper:GetCommanderColorFrame(displayerInfo.fleet_id, displayerInfo.level)
  data_table.avatarFrame = avatarFrame
  data_table.vessels_image_name = vessels_image_name
  data_table.vessels_type_frame = vessels_type_frame
  data_table.vessels_name = vessels_name
  data_table.level = GameGlobalData:GetData("levelinfo").level
  data_table.force = tonumber(item.show_detail.force)
  local hasAdjutant = false
  if displayerInfo.can_be_adjutant == 1 then
    hasAdjutant = true
  end
  data_table.hasAdjutant = hasAdjutant
  data_table.fleetRank = GameUtils:GetFleetRankFrame(displayerInfo.rank, nil, nil)
  data_table.atk = displayerInfo.damage_rate
  data_table.def = displayerInfo.defence_rate
  data_table.ast = displayerInfo.assist_rate
  local basic_info = GameDataAccessHelper:GetCommanderBasicInfo(displayerInfo.fleet_id, displayerInfo.level)
  data_table.skillName = GameDataAccessHelper:GetSkillNameText(basic_info.SPELL_ID)
  data_table.skillDesc = GameDataAccessHelper:GetSkillDesc(basic_info.SPELL_ID)
  data_table.skillRangeType = "TYPE_" .. GameDataAccessHelper:GetSkillRangeType(basic_info.SPELL_ID)
  data_table.skillRangeTypeName = GameLoader:GetGameText("LC_MENU_SKILL_RANGE_TYPE_" .. GameDataAccessHelper:GetSkillRangeType(basic_info.SPELL_ID))
  data_table.skillIconFram = GameDataAccessHelper:GetSkillIcon(basic_info.SPELL_ID)
  local propertyName = ""
  for i = 1, 8 do
    local isGift = false
    for j = 1, #item.show_detail.gift do
      if item.show_detail.gift[j].gift == i then
        isGift = true
        break
      end
    end
    if isGift then
      propertyName = propertyName .. "<font color='#FC9901'>" .. GameLoader:GetGameText("LC_MENU_Equip_param_" .. i) .. "</font>" .. "\001"
    else
      propertyName = propertyName .. GameLoader:GetGameText("LC_MENU_Equip_param_" .. i) .. "\001"
    end
  end
  data_table.propertyName = propertyName
  local fleetVessels = GameDataAccessHelper:GetCommanderVessels(displayerInfo.fleet_id, item.show_detail.level)
  local phAttack = item.show_detail.ph_attack
  local enAttack = item.show_detail.en_attack
  if tonumber(fleetVessels) == 4 then
    phAttack = "-"
  else
    enAttack = "-"
  end
  local propertyValue = ""
  for i = 1, 8 do
    local isGift = false
    for j = 1, #item.show_detail.gift do
      if item.show_detail.gift[j].gift == i then
        isGift = true
        break
      end
    end
    local pre = ""
    local suf = ""
    if isGift then
      pre = "<font color='#FC9901'>"
      suf = "</font>"
    end
    if i == 1 then
      propertyValue = propertyValue .. pre .. item.show_detail.max_durability .. suf .. "\001"
    elseif i == 2 then
      propertyValue = propertyValue .. pre .. item.show_detail.shield .. suf .. "\001"
    elseif i == 3 then
      propertyValue = propertyValue .. pre .. phAttack .. suf .. "\001"
    elseif i == 4 then
      propertyValue = propertyValue .. pre .. item.show_detail.ph_armor .. suf .. "\001"
    elseif i == 5 then
      propertyValue = propertyValue .. pre .. item.show_detail.sp_attack .. suf .. "\001"
    elseif i == 6 then
      propertyValue = propertyValue .. pre .. item.show_detail.sp_armor .. suf .. "\001"
    elseif i == 7 then
      propertyValue = propertyValue .. pre .. enAttack .. suf .. "\001"
    elseif i == 8 then
      propertyValue = propertyValue .. pre .. item.show_detail.en_armor .. suf .. "\001"
    end
  end
  data_table.propertyValue = propertyValue
  data_table.awardCount = #item.awards
  local itemFrame = ""
  local itemCounts = ""
  for k, v in pairs(item.awards) do
    itemCounts = itemCounts .. "x" .. GameHelper:GetAwardCount(v.item_type, v.number, v.no) .. "\001"
    itemFrame = itemFrame .. GameHelper:GetAwardTypeIconFrameNameSupportDynamic(v, nil, nil) .. "\001"
  end
  data_table.itemFrame = itemFrame
  data_table.itemCounts = itemCounts
  data_table.index = index
  data_table.itemCount = #self.midAllData
  data_table.price = item.priceText
  data_table.hasReceiveText = GameLoader:GetGameText("LC_MENU_SA_RECEIVED_INFO")
  data_table.isBuy = item.first_buy
  data_table.own_fleet = item.own_fleet
  local ownFleetText = string.gsub(GameLoader:GetGameText("LC_MENU_HAVE_THE_HERO_ALREADY"), "<number1>", GameHelper:GetAwardText(item.replace_item.item_type, item.replace_item.number, item.replace_item.no))
  data_table.ownFleetText = ownFleetText
  data_table.credit = item.credit .. "<font size='36'>" .. "x2" .. "</font>"
  self:GetFlashObject():InvokeASCallback("_root", "UpdateMainLayerData", data_table)
end
function GameUIFirstCharge.DownloadImageOverCallback()
  GameUIFirstCharge:UpdateMainLayerData(GameUIFirstCharge.currentIndex)
end
function GameUIFirstCharge:Buy(arg)
  if IPlatformExt.getConfigValue("PlatformName") == "taptap" then
    GameVip:Select_zfb_wechat(GameUIFirstCharge.doBuy, arg)
  else
    GameUIFirstCharge.doBuy(arg)
  end
end
function GameUIFirstCharge.doBuy(arg)
  local itemId = GameUIFirstCharge.midAllData[tonumber(arg)].product_id
  local fleetID = GameUIFirstCharge.midAllData[tonumber(arg)].fleet_id
  if GameUtils:isIOSBundle() and GameVip.gamePurchaseEanble == 0 then
    GameUIGlobalScreen:ShowMessageBox(GameUIGlobalScreen.MessageBoxType_OK, GameLoader:GetGameText("LC_MENU_NOTIFICATION_CHAR"), GameLoader:GetGameText("LC_MENU_UNABLE_TO_BUY_INFO"), nil)
    return
  end
  local arg = itemId
  local Ishave = false
  for k, v in pairs(GameVip.productIdentifierList) do
    if arg == v then
      Ishave = true
      arg = k
      break
    end
  end
  if not Ishave then
    DebugOut("Not found product.")
    return
  end
  local function netCall()
    GameVip.BuyArg = arg
    GameVip.BuyID = fleetID
    GameVip.BuyProductID = itemId
    GameVip.BuyCount = 1
    if GameVip.GetPriceByProductID(itemId) then
      GameVip.storeObject = GameVip.storeObject or StoreObject:new()
      DebugOut("GameVip.p[tonumber(arg)]: ", itemId)
      GameUtils:printByAndroid("GameVip.canShowMycard= " .. tostring(GameVip.canShowMycard) .. ", Wifi = ")
      local canShowMycard, isShowmycardType_1, isShowmycardType_2, isShowmycardType_3 = GameVip:GetMycardPurchageVisible(itemId)
      local mixedData = tostring(isShowmycardType_1) .. tostring(isShowmycardType_2) .. tostring(isShowmycardType_3)
      if GameUtils:IsMycardAPP() then
        local isReallyMycardEnable = 0
        if StoreObject.IsMycardReallyEnabled ~= nil then
          isReallyMycardEnable = GameVip.storeObject:IsMycardReallyEnabled()
        end
        if isReallyMycardEnable > 0 then
          GameVip:GetFlashObject():InvokeASCallback("_root", "showMyCardWin")
        else
          GameVip:DoRealPurchase()
        end
      elseif GameUtils:IsQihooApp() then
        GameUtils:printByAndroid("IsQihooApp......pay")
        GameVip.mycardType_qihooPrice = "qh360_price"
        if GameVip.BuyArg ~= nil then
          StoreObject:getMycardUrl(GameVip.mycardType_qihooPrice, GameVip.productIdentifierList[tonumber(GameVip.BuyArg)])
        end
      elseif GameVip:IsEnableExtMultiBuyFeature() then
        GameVip:ShowMultiPop()
      else
        GameVip:DoRealPurchase()
      end
    end
  end
  GameVip:ReqWishProductBeferBuy(fleetID, itemId, 1, netCall)
end
function GameUIFirstCharge:showItemDetil(item_, item_type)
  if GameHelper:IsResource(item_type) then
    return
  end
  local item = item_
  if item_type == "krypton" then
    if item.level == 0 then
      item.level = 1
    end
    local detail = GameUIKrypton:TryQueryKryptonDetail(item.number, item.level, function(msgType, content)
      local ret = GameUIKrypton.QueryKryptonDetailCallBack(msgType, content)
      if ret then
        local tmp = GameGlobalData:GetKryptonDetail(item.number, item.level)
        if tmp == nil then
          return false
        end
        ItemBox:SetKryptonBox(tmp, item.number)
        local operationTable = {}
        operationTable.btnUnloadVisible = false
        operationTable.btnUpgradeVisible = false
        operationTable.btnUseVisible = false
        operationTable.btnDecomposeVisible = false
        ItemBox:ShowKryptonBox(320, 240, operationTable)
      end
      return ret
    end)
    if detail == nil then
    else
      ItemBox:SetKryptonBox(detail, item.number)
      local operationTable = {}
      operationTable.btnUnloadVisible = false
      operationTable.btnUpgradeVisible = false
      operationTable.btnUseVisible = false
      operationTable.btnDecomposeVisible = false
      ItemBox:ShowKryptonBox(320, 240, operationTable)
    end
  end
  if item_type == "item" then
    item.cnt = 1
    ItemBox:showItemBox("Item", item, tonumber(item.number), 320, 480, 200, 200, "", nil, "", nil)
  end
  if item_type == "fleet" then
    ItemBox:ShowCommanderDetail2(tonumber(item.number))
  end
end
function GameUIFirstCharge:InitSkillDesPanel()
  self:GetFlashObject():InvokeASCallback("_root", "InitSkillDesPanel")
end
function GameUIFirstCharge:OnFSCommand(cmd, arg)
  if cmd == "close" then
    GameStateManager:GetCurrentGameState():EraseObject(self)
  elseif cmd == "click_command_item" then
    local fleetData = self.midAllData[tonumber(arg)].show_detail
    DebugOutPutTable(fleetData, "fleetData = ")
    ItemBox:ShowCommanderDetail2(fleetData.identity, fleetData, fleetData.level)
    ItemBox:SetContentsInfoPage()
  elseif cmd == "click_item" then
    local curentItem = self.midAllData[self.currentIndex]
    local item = curentItem.awards[tonumber(arg)]
    GameUIFirstCharge:showItemDetil(item, item.item_type)
  elseif cmd == "next" then
    GameUIFirstCharge:GetShowCurrentIndx(true)
    GameUIFirstCharge:UpdateMainLayerData(self.currentIndex)
    GameUIFirstCharge:InitSkillDesPanel()
    GameUIFirstCharge:MoveNextAnimation(true)
  elseif cmd == "prev" then
    GameUIFirstCharge:GetShowCurrentIndx(false)
    GameUIFirstCharge:UpdateMainLayerData(self.currentIndex)
    GameUIFirstCharge:InitSkillDesPanel()
    GameUIFirstCharge:MoveNextAnimation(false)
  elseif cmd == "help" then
    GameUIWDStuff:Show(GameUIWDStuff.menuType.menu_type_help)
    GameUIWDStuff:SetHelpText(GameLoader:GetGameText("LC_MENU_FIRST_PURCHASE_HELP_INFO"))
  elseif cmd == "click_charge" then
    GameVip.itemName_wechat = GameLoader:GetGameText("LC_MENU_PAID_WALL_FUNCTION_FIRST_RECHARGE")
    if GameUtils:IsNeedChinaIDAuth() then
      if not GameGlobalData:isIDAuth() then
        ItemBox:showIdComfirmBox()
      elseif GameGlobalData:GetModuleStatus("identity_confirm") == false then
        GameUIFirstCharge:Buy(arg)
      else
        local function callback()
          GameUIFirstCharge:Buy(arg)
        end
        local price = self.midAllData[tonumber(arg)].priceText
        local fleetName = GameDataAccessHelper:GetCommanderName(self.midAllData[tonumber(arg)].fleet_id)
        ItemBox:showBuyConfirmBox(callback, nil, price, fleetName)
      end
    else
      GameUIFirstCharge:Buy(arg)
    end
  end
end
function GameUIFirstCharge:MoveNextAnimation(flag)
  self:GetFlashObject():InvokeASCallback("_root", "MoveNextAnimation", flag)
end
function GameUIFirstCharge:ShowCommanderInfo(identity)
  local basic_info = GameDataAccessHelper:GetCommanderBasicInfo(identity)
  local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
  local data_table = {}
  data_table.name = GameLoader:GetGameText("LC_NPC_" .. GameDataAccessHelper:GetCommanderName(identity))
  data_table.vessels_type = GameDataAccessHelper:GetCommanderVesselsType(identity)
  data_table.vessels_name = GameLoader:GetGameText("LC_FLEET_" .. data_table.vessels_type)
  data_table.avatar = GameDataAccessHelper:GetFleetAvatar(identity)
  data_table.commander_type = identity < 100 and "commander_normal" or "commander_advanced"
  data_table.skill_name = GameDataAccessHelper:GetSkillNameText(basic_info.SPELL_ID)
  data_table.skill_rangetype = GameDataAccessHelper:GetSkillRangeType(basic_info.SPELL_ID)
  data_table.skill_rangetype_name = GameLoader:GetGameText("LC_MENU_SKILL_RANGE_TYPE_" .. data_table.skill_rangetype)
  data_table.skill_desc = GameDataAccessHelper:GetSkillDesc(basic_info.SPELL_ID)
  data_table.identity = identity
  local pos_string = "-1,-1"
  DebugOut("pos_string:", pos_string)
  local pos_table = LuaUtils:string_split(pos_string, ",")
  data_table.pos_x = tonumber(pos_table[1])
  data_table.pos_y = tonumber(pos_table[2])
  DebugOutPutTable(commander_data, "commander_data")
  DebugOutPutTable(data_table, "data_table")
  ItemBox:ShowCommanderInfo(data_table)
end
if AutoUpdate.isAndroidDevice then
  function GameUIFirstCharge.OnAndroidBack()
    GameUIFirstCharge:MoveOut()
  end
end
