module("TacticsCenterDataManager", package.seeall)
local __equals = function(a, b)
  return a == b
end
TacticsCenterDataManager = {}
TacticsCenterDataManager.EequipRank = {
  green_ = 1,
  blue_ = 2,
  purple_ = 3,
  glod_ = 4
}
TacticsCenterDataManager.EattrRank = {
  white_ = 0,
  green_ = 1,
  blue_ = 2,
  purple_ = 3,
  glod_ = 4
}
TacticsCenterDataManager.EvesselType = {
  qi_jian = 6,
  tu_ji = 1,
  da_ji = 2,
  pai_huai = 5,
  hui_mie = 4,
  hu_wei = 3
}
TacticsCenterDataManager.ESlotState = {
  none_ = {},
  equiped_ = {},
  lock_ = {}
}
TacticsCenterDataManager.EAttrType = {main_ = 1, bonus_ = 2}
TacticsCenterDataManager.EAttrExist = {
  new_ = 1,
  del_ = 2,
  com_ = 3
}
TacticsCenterDataManager.ShowLayerState = {
  tacticsCenter = 1,
  tacticsCenterHelp = 2,
  tacticsRefine = 3,
  tacticsRefineHelp = 4
}
function TacticsCenterDataManager:clearData()
  TacticsCenterDataManager._dataChangeListeners = {}
  TacticsCenterDataManager._slotDatas = {}
  TacticsCenterDataManager._equipDatas = {
    [TacticsCenterDataManager.EvesselType.qi_jian] = {},
    [TacticsCenterDataManager.EvesselType.tu_ji] = {},
    [TacticsCenterDataManager.EvesselType.da_ji] = {},
    [TacticsCenterDataManager.EvesselType.pai_huai] = {},
    [TacticsCenterDataManager.EvesselType.hui_mie] = {},
    [TacticsCenterDataManager.EvesselType.hu_wei] = {}
  }
  TacticsCenterDataManager._usedSlotCount = 0
  TacticsCenterDataManager._availableSlotCount = 0
  TacticsCenterDataManager._unlockSlotConditionAndCast = {}
  TacticsCenterDataManager._showState = 0
end
function TacticsCenterDataManager:initDataUseEnterTacticalACK(content)
  DebugOut("TacticsCenterDataManager:initDataUseEnterTacticalACK")
  DebugTable(content)
  DebugOut(content.equip_num)
  self._usedSlotCount = content.equip_num
  self._availableSlotCount = content.equip_max
  self._unlockSlotConditionAndCast = content.unlock_slot
  self._slotDatas = content.slots
  for i, slot in ipairs(self._slotDatas) do
    assert(self:_isEvesselValue(slot.vessel))
    self._equipDatas[slot.vessel] = slot.equips
  end
end
function TacticsCenterDataManager:createADataChangeListener()
  local listener = {}
  function listener:onEquipOff(slot, equip)
  end
  function listener:onEquipOn(equip)
  end
  function listener:onEquipChange(oldEquip, newEquip)
  end
  function listener:onDebuggingSave(equip)
  end
  function listener:onUnlockSlotCount(oldCount, newCount)
  end
  return listener
end
TacticsCenterDataManager.EDataChange = {
  equip_off = {},
  equip_on = {},
  equip_change = {},
  debugging_save = {},
  available_slot_count_change = {}
}
function TacticsCenterDataManager:getEquipList(vessel)
  assert(self:_isEvesselValue(vessel))
  return self._equipDatas[vessel]
end
function TacticsCenterDataManager:getEquipListRemoveSlotInEquip(vessel)
  assert(self:_isEvesselValue(vessel))
  local slotEquip = self:getEquipInSlot(vessel)
  local equipListRemoveSlotEquip = {}
  if slotEquip then
    for i, v in ipairs(self._equipDatas[vessel]) do
      if v.id ~= slotEquip.id then
        table.insert(equipListRemoveSlotEquip, v)
      end
    end
  else
    return self._equipDatas[vessel]
  end
  return equipListRemoveSlotEquip
end
function TacticsCenterDataManager:_dispathDataChange(changeType, ...)
