local info = GameData.adventure_monster.info
info[5100111] = {
  ID = 5100111,
  vessels = 2,
  Avatar = "head12",
  Ship = "ship39"
}
info[5100112] = {
  ID = 5100112,
  vessels = 2,
  Avatar = "head12",
  Ship = "ship39"
}
info[5100113] = {
  ID = 5100113,
  vessels = 1,
  Avatar = "head24",
  Ship = "ship5"
}
info[5100114] = {
  ID = 5100114,
  vessels = 1,
  Avatar = "head12",
  Ship = "ship39"
}
info[5100121] = {
  ID = 5100121,
  vessels = 1,
  Avatar = "head8",
  Ship = "ship5"
}
info[5100122] = {
  ID = 5100122,
  vessels = 2,
  Avatar = "head24",
  Ship = "ship39"
}
info[5100123] = {
  ID = 5100123,
  vessels = 4,
  Avatar = "head24",
  Ship = "ship47"
}
info[5100124] = {
  ID = 5100124,
  vessels = 2,
  Avatar = "head24",
  Ship = "ship39"
}
info[5100131] = {
  ID = 5100131,
  vessels = 1,
  Avatar = "head12",
  Ship = "ship39"
}
info[5100132] = {
  ID = 5100132,
  vessels = 2,
  Avatar = "head12",
  Ship = "ship39"
}
info[5100133] = {
  ID = 5100133,
  vessels = 3,
  Avatar = "head12",
  Ship = "ship39"
}
info[5100134] = {
  ID = 5100134,
  vessels = 4,
  Avatar = "head12",
  Ship = "ship39"
}
info[5100135] = {
  ID = 5100135,
  vessels = 5,
  Avatar = "head12",
  Ship = "ship39"
}
info[5100136] = {
  ID = 5100136,
  vessels = 1,
  Avatar = "head24",
  Ship = "ship47"
}
info[5100141] = {
  ID = 5100141,
  vessels = 1,
  Avatar = "head12",
  Ship = "ship47"
}
info[5100142] = {
  ID = 5100142,
  vessels = 2,
  Avatar = "head12",
  Ship = "ship39"
}
info[5100143] = {
  ID = 5100143,
  vessels = 3,
  Avatar = "head12",
  Ship = "ship47"
}
info[5100144] = {
  ID = 5100144,
  vessels = 4,
  Avatar = "head12",
  Ship = "ship39"
}
info[5100145] = {
  ID = 5100145,
  vessels = 5,
  Avatar = "head12",
  Ship = "ship39"
}
info[5100146] = {
  ID = 5100146,
  vessels = 6,
  Avatar = "head24",
  Ship = "ship5"
}
info[5100151] = {
  ID = 5100151,
  vessels = 1,
  Avatar = "head12",
  Ship = "ship47"
}
info[5100152] = {
  ID = 5100152,
  vessels = 2,
  Avatar = "head12",
  Ship = "ship47"
}
info[5100153] = {
  ID = 5100153,
  vessels = 3,
  Avatar = "head12",
  Ship = "ship47"
}
info[5100154] = {
  ID = 5100154,
  vessels = 4,
  Avatar = "head12",
  Ship = "ship39"
}
info[5100155] = {
  ID = 5100155,
  vessels = 5,
  Avatar = "head12",
  Ship = "ship39"
}
info[5100156] = {
  ID = 5100156,
  vessels = 6,
  Avatar = "head24",
  Ship = "ship47"
}
info[5100161] = {
  ID = 5100161,
  vessels = 1,
  Avatar = "head24",
  Ship = "ship47"
}
info[5100162] = {
  ID = 5100162,
  vessels = 2,
  Avatar = "head24",
  Ship = "ship42"
}
info[5100163] = {
  ID = 5100163,
  vessels = 3,
  Avatar = "head24",
  Ship = "ship47"
}
info[5100164] = {
  ID = 5100164,
  vessels = 4,
  Avatar = "head24",
  Ship = "ship39"
}
info[5100165] = {
  ID = 5100165,
  vessels = 5,
  Avatar = "head24",
  Ship = "ship39"
}
info[5100166] = {
  ID = 5100166,
  vessels = 6,
  Avatar = "head16",
  Ship = "ship5"
}
info[5100171] = {
  ID = 5100171,
  vessels = 1,
  Avatar = "head25",
  Ship = "ship47"
}
info[5100172] = {
  ID = 5100172,
  vessels = 2,
  Avatar = "head25",
  Ship = "ship5"
}
info[5100173] = {
  ID = 5100173,
  vessels = 3,
  Avatar = "head25",
  Ship = "ship47"
}
info[5100174] = {
  ID = 5100174,
  vessels = 4,
  Avatar = "head25",
  Ship = "ship39"
}
info[5100175] = {
  ID = 5100175,
  vessels = 5,
  Avatar = "head25",
  Ship = "ship39"
}
info[5100176] = {
  ID = 5100176,
  vessels = 6,
  Avatar = "head24",
  Ship = "ship42"
}
info[5100181] = {
  ID = 5100181,
  vessels = 1,
  Avatar = "head24",
  Ship = "ship47"
}
info[5100182] = {
  ID = 5100182,
  vessels = 2,
  Avatar = "head24",
  Ship = "ship5"
}
info[5100183] = {
  ID = 5100183,
  vessels = 3,
  Avatar = "head24",
  Ship = "ship39"
}
info[5100184] = {
  ID = 5100184,
  vessels = 4,
  Avatar = "head24",
  Ship = "ship47"
}
info[5100185] = {
  ID = 5100185,
  vessels = 5,
  Avatar = "head24",
  Ship = "ship5"
}
info[5100186] = {
  ID = 5100186,
  vessels = 6,
  Avatar = "head7",
  Ship = "ship42"
}
info[5100211] = {
  ID = 5100211,
  vessels = 1,
  Avatar = "head12",
  Ship = "ship5"
}
info[5100212] = {
  ID = 5100212,
  vessels = 2,
  Avatar = "head12",
  Ship = "ship5"
}
info[5100213] = {
  ID = 5100213,
  vessels = 3,
  Avatar = "head12",
  Ship = "ship39"
}
info[5100214] = {
  ID = 5100214,
  vessels = 4,
  Avatar = "head12",
  Ship = "ship47"
}
info[5100215] = {
  ID = 5100215,
  vessels = 5,
  Avatar = "head12",
  Ship = "ship47"
}
info[5100216] = {
  ID = 5100216,
  vessels = 6,
  Avatar = "head24",
  Ship = "ship42"
}
info[5100221] = {
  ID = 5100221,
  vessels = 1,
  Avatar = "head24",
  Ship = "ship5"
}
info[5100222] = {
  ID = 5100222,
  vessels = 2,
  Avatar = "head24",
  Ship = "ship5"
}
info[5100223] = {
  ID = 5100223,
  vessels = 3,
  Avatar = "head24",
  Ship = "ship39"
}
info[5100224] = {
  ID = 5100224,
  vessels = 4,
  Avatar = "head24",
  Ship = "ship47"
}
info[5100225] = {
  ID = 5100225,
  vessels = 5,
  Avatar = "head24",
  Ship = "ship47"
}
info[5100226] = {
  ID = 5100226,
  vessels = 6,
  Avatar = "head22",
  Ship = "ship42"
}
info[5100231] = {
  ID = 5100231,
  vessels = 1,
  Avatar = "head13",
  Ship = "ship5"
}
info[5100232] = {
  ID = 5100232,
  vessels = 2,
  Avatar = "head13",
  Ship = "ship5"
}
info[5100233] = {
  ID = 5100233,
  vessels = 3,
  Avatar = "head13",
  Ship = "ship39"
}
info[5100234] = {
  ID = 5100234,
  vessels = 4,
  Avatar = "head13",
  Ship = "ship47"
}
info[5100235] = {
  ID = 5100235,
  vessels = 5,
  Avatar = "head13",
  Ship = "ship47"
}
info[5100236] = {
  ID = 5100236,
  vessels = 6,
  Avatar = "head12",
  Ship = "ship25"
}
info[5100241] = {
  ID = 5100241,
  vessels = 1,
  Avatar = "head25",
  Ship = "ship5"
}
info[5100242] = {
  ID = 5100242,
  vessels = 2,
  Avatar = "head25",
  Ship = "ship5"
}
info[5100243] = {
  ID = 5100243,
  vessels = 3,
  Avatar = "head25",
  Ship = "ship39"
}
info[5100244] = {
  ID = 5100244,
  vessels = 4,
  Avatar = "head25",
  Ship = "ship47"
}
info[5100245] = {
  ID = 5100245,
  vessels = 5,
  Avatar = "head25",
  Ship = "ship47"
}
info[5100246] = {
  ID = 5100246,
  vessels = 6,
  Avatar = "head24",
  Ship = "ship25"
}
info[5100251] = {
  ID = 5100251,
  vessels = 1,
  Avatar = "head25",
  Ship = "ship42"
}
info[5100252] = {
  ID = 5100252,
  vessels = 2,
  Avatar = "head25",
  Ship = "ship5"
}
info[5100253] = {
  ID = 5100253,
  vessels = 3,
  Avatar = "head25",
  Ship = "ship43"
}
info[5100254] = {
  ID = 5100254,
  vessels = 4,
  Avatar = "head25",
  Ship = "ship47"
}
info[5100255] = {
  ID = 5100255,
  vessels = 5,
  Avatar = "head25",
  Ship = "ship42"
}
info[5100256] = {
  ID = 5100256,
  vessels = 6,
  Avatar = "head2",
  Ship = "ship47"
}
info[5100261] = {
  ID = 5100261,
  vessels = 1,
  Avatar = "head25",
  Ship = "ship42"
}
info[5100262] = {
  ID = 5100262,
  vessels = 2,
  Avatar = "head25",
  Ship = "ship5"
}
info[5100263] = {
  ID = 5100263,
  vessels = 3,
  Avatar = "head25",
  Ship = "ship43"
}
info[5100264] = {
  ID = 5100264,
  vessels = 4,
  Avatar = "head25",
  Ship = "ship47"
}
info[5100265] = {
  ID = 5100265,
  vessels = 5,
  Avatar = "head25",
  Ship = "ship42"
}
info[5100266] = {
  ID = 5100266,
  vessels = 6,
  Avatar = "head3",
  Ship = "ship43"
}
info[5100271] = {
  ID = 5100271,
  vessels = 1,
  Avatar = "head25",
  Ship = "ship42"
}
info[5100272] = {
  ID = 5100272,
  vessels = 2,
  Avatar = "head25",
  Ship = "ship5"
}
info[5100273] = {
  ID = 5100273,
  vessels = 3,
  Avatar = "head25",
  Ship = "ship43"
}
info[5100274] = {
  ID = 5100274,
  vessels = 4,
  Avatar = "head25",
  Ship = "ship47"
}
info[5100275] = {
  ID = 5100275,
  vessels = 5,
  Avatar = "head25",
  Ship = "ship42"
}
info[5100276] = {
  ID = 5100276,
  vessels = 6,
  Avatar = "head24",
  Ship = "ship42"
}
info[5100281] = {
  ID = 5100281,
  vessels = 1,
  Avatar = "head2",
  Ship = "ship42"
}
info[5100282] = {
  ID = 5100282,
  vessels = 2,
  Avatar = "head3",
  Ship = "ship5"
}
info[5100283] = {
  ID = 5100283,
  vessels = 3,
  Avatar = "head7",
  Ship = "ship43"
}
info[5100284] = {
  ID = 5100284,
  vessels = 4,
  Avatar = "head22",
  Ship = "ship47"
}
info[5100285] = {
  ID = 5100285,
  vessels = 5,
  Avatar = "head16",
  Ship = "ship42"
}
info[5100286] = {
  ID = 5100286,
  vessels = 6,
  Avatar = "head9",
  Ship = "ship43"
}
info[5100311] = {
  ID = 5100311,
  vessels = 1,
  Avatar = "head25",
  Ship = "ship11"
}
info[5100312] = {
  ID = 5100312,
  vessels = 2,
  Avatar = "head25",
  Ship = "ship11"
}
info[5100313] = {
  ID = 5100313,
  vessels = 3,
  Avatar = "head25",
  Ship = "ship14"
}
info[5100314] = {
  ID = 5100314,
  vessels = 4,
  Avatar = "head25",
  Ship = "ship11"
}
info[5100315] = {
  ID = 5100315,
  vessels = 5,
  Avatar = "head25",
  Ship = "ship14"
}
info[5100316] = {
  ID = 5100316,
  vessels = 6,
  Avatar = "head36",
  Ship = "ship25"
}
info[5100321] = {
  ID = 5100321,
  vessels = 1,
  Avatar = "head24",
  Ship = "ship11"
}
info[5100322] = {
  ID = 5100322,
  vessels = 2,
  Avatar = "head24",
  Ship = "ship14"
}
info[5100323] = {
  ID = 5100323,
  vessels = 3,
  Avatar = "head24",
  Ship = "ship11"
}
info[5100324] = {
  ID = 5100324,
  vessels = 4,
  Avatar = "head24",
  Ship = "ship14"
}
info[5100325] = {
  ID = 5100325,
  vessels = 5,
  Avatar = "head24",
  Ship = "ship11"
}
info[5100326] = {
  ID = 5100326,
  vessels = 6,
  Avatar = "head36",
  Ship = "ship25"
}
info[5100331] = {
  ID = 5100331,
  vessels = 1,
  Avatar = "head25",
  Ship = "ship14"
}
info[5100332] = {
  ID = 5100332,
  vessels = 2,
  Avatar = "head25",
  Ship = "ship11"
}
info[5100333] = {
  ID = 5100333,
  vessels = 3,
  Avatar = "head25",
  Ship = "ship14"
}
info[5100334] = {
  ID = 5100334,
  vessels = 4,
  Avatar = "head25",
  Ship = "ship11"
}
info[5100335] = {
  ID = 5100335,
  vessels = 5,
  Avatar = "head25",
  Ship = "ship14"
}
info[5100336] = {
  ID = 5100336,
  vessels = 6,
  Avatar = "head36",
  Ship = "ship25"
}
info[5100341] = {
  ID = 5100341,
  vessels = 1,
  Avatar = "head24",
  Ship = "ship14"
}
info[5100342] = {
  ID = 5100342,
  vessels = 2,
  Avatar = "head24",
  Ship = "ship14"
}
info[5100343] = {
  ID = 5100343,
  vessels = 3,
  Avatar = "head24",
  Ship = "ship14"
}
info[5100344] = {
  ID = 5100344,
  vessels = 4,
  Avatar = "head24",
  Ship = "ship11"
}
info[5100345] = {
  ID = 5100345,
  vessels = 5,
  Avatar = "head24",
  Ship = "ship11"
}
info[5100346] = {
  ID = 5100346,
  vessels = 6,
  Avatar = "head36",
  Ship = "ship25"
}
info[5100351] = {
  ID = 5100351,
  vessels = 1,
  Avatar = "head25",
  Ship = "ship14"
}
info[5100352] = {
  ID = 5100352,
  vessels = 2,
  Avatar = "head25",
  Ship = "ship14"
}
info[5100353] = {
  ID = 5100353,
  vessels = 3,
  Avatar = "head25",
  Ship = "ship14"
}
info[5100354] = {
  ID = 5100354,
  vessels = 4,
  Avatar = "head25",
  Ship = "ship11"
}
info[5100355] = {
  ID = 5100355,
  vessels = 5,
  Avatar = "head25",
  Ship = "ship11"
}
info[5100356] = {
  ID = 5100356,
  vessels = 6,
  Avatar = "head36",
  Ship = "ship25"
}
info[5100361] = {
  ID = 5100361,
  vessels = 1,
  Avatar = "head24",
  Ship = "ship14"
}
info[5100362] = {
  ID = 5100362,
  vessels = 2,
  Avatar = "head24",
  Ship = "ship14"
}
info[5100363] = {
  ID = 5100363,
  vessels = 3,
  Avatar = "head24",
  Ship = "ship25"
}
info[5100364] = {
  ID = 5100364,
  vessels = 4,
  Avatar = "head24",
  Ship = "ship11"
}
info[5100365] = {
  ID = 5100365,
  vessels = 5,
  Avatar = "head24",
  Ship = "ship11"
}
info[5100366] = {
  ID = 5100366,
  vessels = 6,
  Avatar = "head36",
  Ship = "ship25"
}
info[5100371] = {
  ID = 5100371,
  vessels = 1,
  Avatar = "head25",
  Ship = "ship25"
}
info[5100372] = {
  ID = 5100372,
  vessels = 2,
  Avatar = "head25",
  Ship = "ship14"
}
info[5100373] = {
  ID = 5100373,
  vessels = 3,
  Avatar = "head25",
  Ship = "ship14"
}
info[5100374] = {
  ID = 5100374,
  vessels = 4,
  Avatar = "head25",
  Ship = "ship11"
}
info[5100375] = {
  ID = 5100375,
  vessels = 5,
  Avatar = "head25",
  Ship = "ship11"
}
info[5100376] = {
  ID = 5100376,
  vessels = 6,
  Avatar = "head36",
  Ship = "ship12"
}
info[5100381] = {
  ID = 5100381,
  vessels = 1,
  Avatar = "head24",
  Ship = "ship11"
}
info[5100382] = {
  ID = 5100382,
  vessels = 2,
  Avatar = "head24",
  Ship = "ship14"
}
info[5100383] = {
  ID = 5100383,
  vessels = 3,
  Avatar = "head24",
  Ship = "ship14"
}
info[5100384] = {
  ID = 5100384,
  vessels = 4,
  Avatar = "head24",
  Ship = "ship11"
}
info[5100385] = {
  ID = 5100385,
  vessels = 5,
  Avatar = "head24",
  Ship = "ship11"
}
info[5100386] = {
  ID = 5100386,
  vessels = 6,
  Avatar = "head36",
  Ship = "ship12"
}
info[5100411] = {
  ID = 5100411,
  vessels = 1,
  Avatar = "head25",
  Ship = "ship25"
}
info[5100412] = {
  ID = 5100412,
  vessels = 2,
  Avatar = "head25",
  Ship = "ship47"
}
info[5100413] = {
  ID = 5100413,
  vessels = 3,
  Avatar = "head25",
  Ship = "ship47"
}
info[5100414] = {
  ID = 5100414,
  vessels = 4,
  Avatar = "head25",
  Ship = "ship43"
}
info[5100415] = {
  ID = 5100415,
  vessels = 5,
  Avatar = "head25",
  Ship = "ship25"
}
info[5100416] = {
  ID = 5100416,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship12"
}
info[5100421] = {
  ID = 5100421,
  vessels = 1,
  Avatar = "head24",
  Ship = "ship25"
}
info[5100422] = {
  ID = 5100422,
  vessels = 2,
  Avatar = "head24",
  Ship = "ship43"
}
info[5100423] = {
  ID = 5100423,
  vessels = 3,
  Avatar = "head24",
  Ship = "ship25"
}
info[5100424] = {
  ID = 5100424,
  vessels = 4,
  Avatar = "head24",
  Ship = "ship47"
}
info[5100425] = {
  ID = 5100425,
  vessels = 5,
  Avatar = "head24",
  Ship = "ship47"
}
info[5100426] = {
  ID = 5100426,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship12"
}
info[5100431] = {
  ID = 5100431,
  vessels = 1,
  Avatar = "head25",
  Ship = "ship25"
}
info[5100432] = {
  ID = 5100432,
  vessels = 2,
  Avatar = "head25",
  Ship = "ship43"
}
info[5100433] = {
  ID = 5100433,
  vessels = 3,
  Avatar = "head25",
  Ship = "ship25"
}
info[5100434] = {
  ID = 5100434,
  vessels = 4,
  Avatar = "head25",
  Ship = "ship47"
}
info[5100435] = {
  ID = 5100435,
  vessels = 5,
  Avatar = "head25",
  Ship = "ship47"
}
info[5100436] = {
  ID = 5100436,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship12"
}
info[5100441] = {
  ID = 5100441,
  vessels = 1,
  Avatar = "head24",
  Ship = "ship12"
}
info[5100442] = {
  ID = 5100442,
  vessels = 2,
  Avatar = "head24",
  Ship = "ship25"
}
info[5100443] = {
  ID = 5100443,
  vessels = 3,
  Avatar = "head24",
  Ship = "ship12"
}
info[5100444] = {
  ID = 5100444,
  vessels = 4,
  Avatar = "head24",
  Ship = "ship43"
}
info[5100445] = {
  ID = 5100445,
  vessels = 5,
  Avatar = "head24",
  Ship = "ship43"
}
info[5100446] = {
  ID = 5100446,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship24"
}
info[5100451] = {
  ID = 5100451,
  vessels = 1,
  Avatar = "head25",
  Ship = "ship12"
}
info[5100452] = {
  ID = 5100452,
  vessels = 2,
  Avatar = "head25",
  Ship = "ship12"
}
info[5100453] = {
  ID = 5100453,
  vessels = 3,
  Avatar = "head25",
  Ship = "ship12"
}
info[5100454] = {
  ID = 5100454,
  vessels = 4,
  Avatar = "head25",
  Ship = "ship25"
}
info[5100455] = {
  ID = 5100455,
  vessels = 5,
  Avatar = "head25",
  Ship = "ship43"
}
info[5100456] = {
  ID = 5100456,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship24"
}
info[5100461] = {
  ID = 5100461,
  vessels = 1,
  Avatar = "head24",
  Ship = "ship12"
}
info[5100462] = {
  ID = 5100462,
  vessels = 2,
  Avatar = "head24",
  Ship = "ship12"
}
info[5100463] = {
  ID = 5100463,
  vessels = 3,
  Avatar = "head24",
  Ship = "ship12"
}
info[5100464] = {
  ID = 5100464,
  vessels = 4,
  Avatar = "head24",
  Ship = "ship25"
}
info[5100465] = {
  ID = 5100465,
  vessels = 5,
  Avatar = "head24",
  Ship = "ship25"
}
info[5100466] = {
  ID = 5100466,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship24"
}
info[5100471] = {
  ID = 5100471,
  vessels = 1,
  Avatar = "head25",
  Ship = "ship12"
}
info[5100472] = {
  ID = 5100472,
  vessels = 2,
  Avatar = "head25",
  Ship = "ship25"
}
info[5100473] = {
  ID = 5100473,
  vessels = 3,
  Avatar = "head25",
  Ship = "ship12"
}
info[5100474] = {
  ID = 5100474,
  vessels = 4,
  Avatar = "head25",
  Ship = "ship43"
}
info[5100475] = {
  ID = 5100475,
  vessels = 5,
  Avatar = "head25",
  Ship = "ship43"
}
info[5100476] = {
  ID = 5100476,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship24"
}
info[5100481] = {
  ID = 5100481,
  vessels = 1,
  Avatar = "head24",
  Ship = "ship12"
}
info[5100482] = {
  ID = 5100482,
  vessels = 2,
  Avatar = "head24",
  Ship = "ship12"
}
info[5100483] = {
  ID = 5100483,
  vessels = 3,
  Avatar = "head24",
  Ship = "ship12"
}
info[5100484] = {
  ID = 5100484,
  vessels = 4,
  Avatar = "head24",
  Ship = "ship12"
}
info[5100485] = {
  ID = 5100485,
  vessels = 5,
  Avatar = "head24",
  Ship = "ship12"
}
info[5100486] = {
  ID = 5100486,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship24"
}
info[5100511] = {
  ID = 5100511,
  vessels = 1,
  Avatar = "head23",
  Ship = "ship37"
}
info[5100512] = {
  ID = 5100512,
  vessels = 2,
  Avatar = "head23",
  Ship = "ship37"
}
info[5100513] = {
  ID = 5100513,
  vessels = 3,
  Avatar = "head23",
  Ship = "ship37"
}
info[5100514] = {
  ID = 5100514,
  vessels = 4,
  Avatar = "head23",
  Ship = "ship37"
}
info[5100515] = {
  ID = 5100515,
  vessels = 5,
  Avatar = "head23",
  Ship = "ship37"
}
info[5100516] = {
  ID = 5100516,
  vessels = 6,
  Avatar = "head26",
  Ship = "ship38"
}
info[5100521] = {
  ID = 5100521,
  vessels = 1,
  Avatar = "head26",
  Ship = "ship38"
}
info[5100522] = {
  ID = 5100522,
  vessels = 2,
  Avatar = "head26",
  Ship = "ship38"
}
info[5100523] = {
  ID = 5100523,
  vessels = 3,
  Avatar = "head26",
  Ship = "ship38"
}
info[5100524] = {
  ID = 5100524,
  vessels = 4,
  Avatar = "head26",
  Ship = "ship37"
}
info[5100525] = {
  ID = 5100525,
  vessels = 5,
  Avatar = "head26",
  Ship = "ship37"
}
info[5100526] = {
  ID = 5100526,
  vessels = 6,
  Avatar = "head28",
  Ship = "ship38"
}
info[5100531] = {
  ID = 5100531,
  vessels = 1,
  Avatar = "head26",
  Ship = "ship38"
}
info[5100532] = {
  ID = 5100532,
  vessels = 2,
  Avatar = "head26",
  Ship = "ship38"
}
info[5100533] = {
  ID = 5100533,
  vessels = 3,
  Avatar = "head26",
  Ship = "ship38"
}
info[5100534] = {
  ID = 5100534,
  vessels = 4,
  Avatar = "head26",
  Ship = "ship37"
}
info[5100535] = {
  ID = 5100535,
  vessels = 5,
  Avatar = "head26",
  Ship = "ship37"
}
info[5100536] = {
  ID = 5100536,
  vessels = 6,
  Avatar = "head27",
  Ship = "ship49"
}
info[5100541] = {
  ID = 5100541,
  vessels = 1,
  Avatar = "head26",
  Ship = "ship5"
}
info[5100542] = {
  ID = 5100542,
  vessels = 2,
  Avatar = "head26",
  Ship = "ship42"
}
info[5100543] = {
  ID = 5100543,
  vessels = 3,
  Avatar = "head26",
  Ship = "ship5"
}
info[5100544] = {
  ID = 5100544,
  vessels = 4,
  Avatar = "head26",
  Ship = "ship38"
}
info[5100545] = {
  ID = 5100545,
  vessels = 5,
  Avatar = "head26",
  Ship = "ship38"
}
info[5100546] = {
  ID = 5100546,
  vessels = 6,
  Avatar = "head21",
  Ship = "ship43"
}
info[5100551] = {
  ID = 5100551,
  vessels = 1,
  Avatar = "head35",
  Ship = "ship12"
}
info[5100552] = {
  ID = 5100552,
  vessels = 2,
  Avatar = "head35",
  Ship = "ship12"
}
info[5100553] = {
  ID = 5100553,
  vessels = 3,
  Avatar = "head35",
  Ship = "ship12"
}
info[5100554] = {
  ID = 5100554,
  vessels = 4,
  Avatar = "head35",
  Ship = "ship24"
}
info[5100555] = {
  ID = 5100555,
  vessels = 5,
  Avatar = "head35",
  Ship = "ship24"
}
info[5100556] = {
  ID = 5100556,
  vessels = 6,
  Avatar = "head31",
  Ship = "ship24"
}
info[5100561] = {
  ID = 5100561,
  vessels = 1,
  Avatar = "head23",
  Ship = "ship2"
}
info[5100562] = {
  ID = 5100562,
  vessels = 2,
  Avatar = "head23",
  Ship = "ship49"
}
info[5100563] = {
  ID = 5100563,
  vessels = 3,
  Avatar = "head23",
  Ship = "ship2"
}
info[5100564] = {
  ID = 5100564,
  vessels = 4,
  Avatar = "head23",
  Ship = "ship38"
}
info[5100565] = {
  ID = 5100565,
  vessels = 5,
  Avatar = "head23",
  Ship = "ship38"
}
info[5100566] = {
  ID = 5100566,
  vessels = 6,
  Avatar = "head26",
  Ship = "ship49"
}
info[5100571] = {
  ID = 5100571,
  vessels = 1,
  Avatar = "head26",
  Ship = "ship2"
}
info[5100572] = {
  ID = 5100572,
  vessels = 2,
  Avatar = "head26",
  Ship = "ship49"
}
info[5100573] = {
  ID = 5100573,
  vessels = 3,
  Avatar = "head26",
  Ship = "ship2"
}
info[5100574] = {
  ID = 5100574,
  vessels = 4,
  Avatar = "head26",
  Ship = "ship38"
}
info[5100575] = {
  ID = 5100575,
  vessels = 5,
  Avatar = "head26",
  Ship = "ship38"
}
info[5100576] = {
  ID = 5100576,
  vessels = 6,
  Avatar = "head5",
  Ship = "ship43"
}
info[5100581] = {
  ID = 5100581,
  vessels = 1,
  Avatar = "head47",
  Ship = "ship12"
}
info[5100582] = {
  ID = 5100582,
  vessels = 2,
  Avatar = "head47",
  Ship = "ship12"
}
info[5100583] = {
  ID = 5100583,
  vessels = 3,
  Avatar = "head47",
  Ship = "ship12"
}
info[5100584] = {
  ID = 5100584,
  vessels = 4,
  Avatar = "head47",
  Ship = "ship24"
}
info[5100585] = {
  ID = 5100585,
  vessels = 5,
  Avatar = "head47",
  Ship = "ship24"
}
info[5100586] = {
  ID = 5100586,
  vessels = 6,
  Avatar = "head47",
  Ship = "ship24"
}
info[5100611] = {
  ID = 5100611,
  vessels = 1,
  Avatar = "head23",
  Ship = "ship2"
}
info[5100612] = {
  ID = 5100612,
  vessels = 2,
  Avatar = "head23",
  Ship = "ship2"
}
info[5100613] = {
  ID = 5100613,
  vessels = 3,
  Avatar = "head23",
  Ship = "ship2"
}
info[5100614] = {
  ID = 5100614,
  vessels = 4,
  Avatar = "head23",
  Ship = "ship38"
}
info[5100615] = {
  ID = 5100615,
  vessels = 5,
  Avatar = "head23",
  Ship = "ship38"
}
info[5100616] = {
  ID = 5100616,
  vessels = 6,
  Avatar = "head26",
  Ship = "ship49"
}
info[5100621] = {
  ID = 5100621,
  vessels = 1,
  Avatar = "head26",
  Ship = "ship49"
}
info[5100622] = {
  ID = 5100622,
  vessels = 2,
  Avatar = "head26",
  Ship = "ship2"
}
info[5100623] = {
  ID = 5100623,
  vessels = 3,
  Avatar = "head26",
  Ship = "ship49"
}
info[5100624] = {
  ID = 5100624,
  vessels = 4,
  Avatar = "head26",
  Ship = "ship47"
}
info[5100625] = {
  ID = 5100625,
  vessels = 5,
  Avatar = "head26",
  Ship = "ship47"
}
info[5100626] = {
  ID = 5100626,
  vessels = 6,
  Avatar = "head15",
  Ship = "ship18"
}
info[5100631] = {
  ID = 5100631,
  vessels = 1,
  Avatar = "head23",
  Ship = "ship2"
}
info[5100632] = {
  ID = 5100632,
  vessels = 2,
  Avatar = "head23",
  Ship = "ship2"
}
info[5100633] = {
  ID = 5100633,
  vessels = 3,
  Avatar = "head23",
  Ship = "ship2"
}
info[5100634] = {
  ID = 5100634,
  vessels = 4,
  Avatar = "head23",
  Ship = "ship38"
}
info[5100635] = {
  ID = 5100635,
  vessels = 5,
  Avatar = "head23",
  Ship = "ship38"
}
info[5100636] = {
  ID = 5100636,
  vessels = 6,
  Avatar = "head26",
  Ship = "ship49"
}
info[5100641] = {
  ID = 5100641,
  vessels = 1,
  Avatar = "head23",
  Ship = "ship2"
}
info[5100642] = {
  ID = 5100642,
  vessels = 2,
  Avatar = "head23",
  Ship = "ship2"
}
info[5100643] = {
  ID = 5100643,
  vessels = 3,
  Avatar = "head23",
  Ship = "ship2"
}
info[5100644] = {
  ID = 5100644,
  vessels = 4,
  Avatar = "head23",
  Ship = "ship38"
}
info[5100645] = {
  ID = 5100645,
  vessels = 5,
  Avatar = "head23",
  Ship = "ship38"
}
info[5100646] = {
  ID = 5100646,
  vessels = 6,
  Avatar = "head26",
  Ship = "ship49"
}
info[5100651] = {
  ID = 5100651,
  vessels = 1,
  Avatar = "head23",
  Ship = "ship2"
}
info[5100652] = {
  ID = 5100652,
  vessels = 2,
  Avatar = "head23",
  Ship = "ship2"
}
info[5100653] = {
  ID = 5100653,
  vessels = 3,
  Avatar = "head23",
  Ship = "ship2"
}
info[5100654] = {
  ID = 5100654,
  vessels = 4,
  Avatar = "head23",
  Ship = "ship38"
}
info[5100655] = {
  ID = 5100655,
  vessels = 5,
  Avatar = "head23",
  Ship = "ship38"
}
info[5100656] = {
  ID = 5100656,
  vessels = 6,
  Avatar = "head26",
  Ship = "ship49"
}
info[5100661] = {
  ID = 5100661,
  vessels = 1,
  Avatar = "head23",
  Ship = "ship2"
}
info[5100662] = {
  ID = 5100662,
  vessels = 2,
  Avatar = "head23",
  Ship = "ship2"
}
info[5100663] = {
  ID = 5100663,
  vessels = 3,
  Avatar = "head23",
  Ship = "ship2"
}
info[5100664] = {
  ID = 5100664,
  vessels = 4,
  Avatar = "head23",
  Ship = "ship38"
}
info[5100665] = {
  ID = 5100665,
  vessels = 5,
  Avatar = "head23",
  Ship = "ship38"
}
info[5100666] = {
  ID = 5100666,
  vessels = 6,
  Avatar = "head26",
  Ship = "ship49"
}
info[5100671] = {
  ID = 5100671,
  vessels = 1,
  Avatar = "head26",
  Ship = "ship49"
}
info[5100672] = {
  ID = 5100672,
  vessels = 2,
  Avatar = "head26",
  Ship = "ship2"
}
info[5100673] = {
  ID = 5100673,
  vessels = 3,
  Avatar = "head26",
  Ship = "ship49"
}
info[5100674] = {
  ID = 5100674,
  vessels = 4,
  Avatar = "head26",
  Ship = "ship47"
}
info[5100675] = {
  ID = 5100675,
  vessels = 5,
  Avatar = "head26",
  Ship = "ship47"
}
info[5100676] = {
  ID = 5100676,
  vessels = 6,
  Avatar = "head17",
  Ship = "ship41"
}
info[5100681] = {
  ID = 5100681,
  vessels = 1,
  Avatar = "head17",
  Ship = "ship41"
}
info[5100682] = {
  ID = 5100682,
  vessels = 2,
  Avatar = "head21",
  Ship = "ship41"
}
info[5100683] = {
  ID = 5100683,
  vessels = 3,
  Avatar = "head5",
  Ship = "ship41"
}
info[5100684] = {
  ID = 5100684,
  vessels = 4,
  Avatar = "head15",
  Ship = "ship49"
}
info[5100685] = {
  ID = 5100685,
  vessels = 5,
  Avatar = "head27",
  Ship = "ship49"
}
info[5100686] = {
  ID = 5100686,
  vessels = 6,
  Avatar = "head4",
  Ship = "ship20"
}
info[5100711] = {
  ID = 5100711,
  vessels = 1,
  Avatar = "head45",
  Ship = "ship11"
}
info[5100712] = {
  ID = 5100712,
  vessels = 2,
  Avatar = "head45",
  Ship = "ship11"
}
info[5100713] = {
  ID = 5100713,
  vessels = 3,
  Avatar = "head45",
  Ship = "ship11"
}
info[5100714] = {
  ID = 5100714,
  vessels = 4,
  Avatar = "head45",
  Ship = "ship14"
}
info[5100715] = {
  ID = 5100715,
  vessels = 5,
  Avatar = "head45",
  Ship = "ship14"
}
info[5100716] = {
  ID = 5100716,
  vessels = 6,
  Avatar = "head41",
  Ship = "ship12"
}
info[5100721] = {
  ID = 5100721,
  vessels = 1,
  Avatar = "head35",
  Ship = "ship25"
}
info[5100722] = {
  ID = 5100722,
  vessels = 2,
  Avatar = "head35",
  Ship = "ship25"
}
info[5100723] = {
  ID = 5100723,
  vessels = 3,
  Avatar = "head35",
  Ship = "ship25"
}
info[5100724] = {
  ID = 5100724,
  vessels = 4,
  Avatar = "head35",
  Ship = "ship14"
}
info[5100725] = {
  ID = 5100725,
  vessels = 5,
  Avatar = "head35",
  Ship = "ship14"
}
info[5100726] = {
  ID = 5100726,
  vessels = 6,
  Avatar = "head36",
  Ship = "ship12"
}
info[5100731] = {
  ID = 5100731,
  vessels = 1,
  Avatar = "head35",
  Ship = "ship25"
}
info[5100732] = {
  ID = 5100732,
  vessels = 2,
  Avatar = "head35",
  Ship = "ship25"
}
info[5100733] = {
  ID = 5100733,
  vessels = 3,
  Avatar = "head35",
  Ship = "ship25"
}
info[5100734] = {
  ID = 5100734,
  vessels = 4,
  Avatar = "head35",
  Ship = "ship14"
}
info[5100735] = {
  ID = 5100735,
  vessels = 5,
  Avatar = "head35",
  Ship = "ship14"
}
info[5100736] = {
  ID = 5100736,
  vessels = 6,
  Avatar = "head36",
  Ship = "ship12"
}
info[5100741] = {
  ID = 5100741,
  vessels = 1,
  Avatar = "head35",
  Ship = "ship25"
}
info[5100742] = {
  ID = 5100742,
  vessels = 2,
  Avatar = "head35",
  Ship = "ship25"
}
info[5100743] = {
  ID = 5100743,
  vessels = 3,
  Avatar = "head35",
  Ship = "ship25"
}
info[5100744] = {
  ID = 5100744,
  vessels = 4,
  Avatar = "head35",
  Ship = "ship14"
}
info[5100745] = {
  ID = 5100745,
  vessels = 5,
  Avatar = "head35",
  Ship = "ship14"
}
info[5100746] = {
  ID = 5100746,
  vessels = 6,
  Avatar = "head36",
  Ship = "ship12"
}
info[5100751] = {
  ID = 5100751,
  vessels = 1,
  Avatar = "head36",
  Ship = "ship12"
}
info[5100752] = {
  ID = 5100752,
  vessels = 2,
  Avatar = "head36",
  Ship = "ship12"
}
info[5100753] = {
  ID = 5100753,
  vessels = 3,
  Avatar = "head36",
  Ship = "ship12"
}
info[5100754] = {
  ID = 5100754,
  vessels = 4,
  Avatar = "head36",
  Ship = "ship12"
}
info[5100755] = {
  ID = 5100755,
  vessels = 5,
  Avatar = "head36",
  Ship = "ship12"
}
info[5100756] = {
  ID = 5100756,
  vessels = 6,
  Avatar = "head32",
  Ship = "ship24"
}
info[5100761] = {
  ID = 5100761,
  vessels = 1,
  Avatar = "head37",
  Ship = "ship12"
}
info[5100762] = {
  ID = 5100762,
  vessels = 2,
  Avatar = "head37",
  Ship = "ship12"
}
info[5100763] = {
  ID = 5100763,
  vessels = 3,
  Avatar = "head37",
  Ship = "ship12"
}
info[5100764] = {
  ID = 5100764,
  vessels = 4,
  Avatar = "head37",
  Ship = "ship25"
}
info[5100765] = {
  ID = 5100765,
  vessels = 5,
  Avatar = "head37",
  Ship = "ship25"
}
info[5100766] = {
  ID = 5100766,
  vessels = 6,
  Avatar = "head33",
  Ship = "ship24"
}
info[5100771] = {
  ID = 5100771,
  vessels = 1,
  Avatar = "head44",
  Ship = "ship24"
}
info[5100772] = {
  ID = 5100772,
  vessels = 2,
  Avatar = "head44",
  Ship = "ship24"
}
info[5100773] = {
  ID = 5100773,
  vessels = 3,
  Avatar = "head44",
  Ship = "ship24"
}
info[5100774] = {
  ID = 5100774,
  vessels = 4,
  Avatar = "head44",
  Ship = "ship24"
}
info[5100775] = {
  ID = 5100775,
  vessels = 5,
  Avatar = "head44",
  Ship = "ship24"
}
info[5100776] = {
  ID = 5100776,
  vessels = 6,
  Avatar = "head40",
  Ship = "ship16"
}
info[5100781] = {
  ID = 5100781,
  vessels = 1,
  Avatar = "head37",
  Ship = "ship25"
}
info[5100782] = {
  ID = 5100782,
  vessels = 2,
  Avatar = "head37",
  Ship = "ship25"
}
info[5100783] = {
  ID = 5100783,
  vessels = 3,
  Avatar = "head37",
  Ship = "ship25"
}
info[5100784] = {
  ID = 5100784,
  vessels = 4,
  Avatar = "head37",
  Ship = "ship24"
}
info[5100785] = {
  ID = 5100785,
  vessels = 5,
  Avatar = "head37",
  Ship = "ship24"
}
info[5100786] = {
  ID = 5100786,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship16"
}
info[5100811] = {
  ID = 5100811,
  vessels = 1,
  Avatar = "head44",
  Ship = "ship24"
}
info[5100812] = {
  ID = 5100812,
  vessels = 2,
  Avatar = "head44",
  Ship = "ship24"
}
info[5100813] = {
  ID = 5100813,
  vessels = 3,
  Avatar = "head44",
  Ship = "ship24"
}
info[5100814] = {
  ID = 5100814,
  vessels = 4,
  Avatar = "head44",
  Ship = "ship24"
}
info[5100815] = {
  ID = 5100815,
  vessels = 5,
  Avatar = "head44",
  Ship = "ship24"
}
info[5100816] = {
  ID = 5100816,
  vessels = 6,
  Avatar = "head45",
  Ship = "ship25"
}
info[5100821] = {
  ID = 5100821,
  vessels = 1,
  Avatar = "head44",
  Ship = "ship24"
}
info[5100822] = {
  ID = 5100822,
  vessels = 2,
  Avatar = "head44",
  Ship = "ship24"
}
info[5100823] = {
  ID = 5100823,
  vessels = 3,
  Avatar = "head44",
  Ship = "ship24"
}
info[5100824] = {
  ID = 5100824,
  vessels = 4,
  Avatar = "head44",
  Ship = "ship24"
}
info[5100825] = {
  ID = 5100825,
  vessels = 5,
  Avatar = "head44",
  Ship = "ship24"
}
info[5100826] = {
  ID = 5100826,
  vessels = 6,
  Avatar = "head36",
  Ship = "ship25"
}
info[5100831] = {
  ID = 5100831,
  vessels = 1,
  Avatar = "head44",
  Ship = "ship24"
}
info[5100832] = {
  ID = 5100832,
  vessels = 2,
  Avatar = "head44",
  Ship = "ship24"
}
info[5100833] = {
  ID = 5100833,
  vessels = 3,
  Avatar = "head44",
  Ship = "ship24"
}
info[5100834] = {
  ID = 5100834,
  vessels = 4,
  Avatar = "head44",
  Ship = "ship24"
}
info[5100835] = {
  ID = 5100835,
  vessels = 5,
  Avatar = "head44",
  Ship = "ship24"
}
info[5100836] = {
  ID = 5100836,
  vessels = 6,
  Avatar = "head36",
  Ship = "ship25"
}
info[5100841] = {
  ID = 5100841,
  vessels = 1,
  Avatar = "head46",
  Ship = "ship12"
}
info[5100842] = {
  ID = 5100842,
  vessels = 2,
  Avatar = "head46",
  Ship = "ship12"
}
info[5100843] = {
  ID = 5100843,
  vessels = 3,
  Avatar = "head46",
  Ship = "ship12"
}
info[5100844] = {
  ID = 5100844,
  vessels = 4,
  Avatar = "head46",
  Ship = "ship12"
}
info[5100845] = {
  ID = 5100845,
  vessels = 5,
  Avatar = "head46",
  Ship = "ship12"
}
info[5100846] = {
  ID = 5100846,
  vessels = 6,
  Avatar = "head42",
  Ship = "ship24"
}
info[5100851] = {
  ID = 5100851,
  vessels = 1,
  Avatar = "head47",
  Ship = "ship25"
}
info[5100852] = {
  ID = 5100852,
  vessels = 2,
  Avatar = "head47",
  Ship = "ship25"
}
info[5100853] = {
  ID = 5100853,
  vessels = 3,
  Avatar = "head47",
  Ship = "ship25"
}
info[5100854] = {
  ID = 5100854,
  vessels = 4,
  Avatar = "head47",
  Ship = "ship24"
}
info[5100855] = {
  ID = 5100855,
  vessels = 5,
  Avatar = "head47",
  Ship = "ship24"
}
info[5100856] = {
  ID = 5100856,
  vessels = 6,
  Avatar = "head37",
  Ship = "ship24"
}
info[5100861] = {
  ID = 5100861,
  vessels = 1,
  Avatar = "head47",
  Ship = "ship25"
}
info[5100862] = {
  ID = 5100862,
  vessels = 2,
  Avatar = "head47",
  Ship = "ship25"
}
info[5100863] = {
  ID = 5100863,
  vessels = 3,
  Avatar = "head47",
  Ship = "ship25"
}
info[5100864] = {
  ID = 5100864,
  vessels = 4,
  Avatar = "head47",
  Ship = "ship24"
}
info[5100865] = {
  ID = 5100865,
  vessels = 5,
  Avatar = "head47",
  Ship = "ship24"
}
info[5100866] = {
  ID = 5100866,
  vessels = 6,
  Avatar = "head37",
  Ship = "ship24"
}
info[5100871] = {
  ID = 5100871,
  vessels = 1,
  Avatar = "head47",
  Ship = "ship16"
}
info[5100872] = {
  ID = 5100872,
  vessels = 2,
  Avatar = "head47",
  Ship = "ship16"
}
info[5100873] = {
  ID = 5100873,
  vessels = 3,
  Avatar = "head47",
  Ship = "ship16"
}
info[5100874] = {
  ID = 5100874,
  vessels = 4,
  Avatar = "head47",
  Ship = "ship25"
}
info[5100875] = {
  ID = 5100875,
  vessels = 5,
  Avatar = "head47",
  Ship = "ship25"
}
info[5100876] = {
  ID = 5100876,
  vessels = 6,
  Avatar = "head43",
  Ship = "ship24"
}
info[5100881] = {
  ID = 5100881,
  vessels = 1,
  Avatar = "head41",
  Ship = "ship16"
}
info[5100882] = {
  ID = 5100882,
  vessels = 2,
  Avatar = "head34",
  Ship = "ship16"
}
info[5100883] = {
  ID = 5100883,
  vessels = 3,
  Avatar = "head33",
  Ship = "ship16"
}
info[5100884] = {
  ID = 5100884,
  vessels = 4,
  Avatar = "head32",
  Ship = "ship25"
}
info[5100885] = {
  ID = 5100885,
  vessels = 5,
  Avatar = "head47",
  Ship = "ship25"
}
info[5100886] = {
  ID = 5100886,
  vessels = 6,
  Avatar = "head43",
  Ship = "ship24"
}
info[5100911] = {
  ID = 5100911,
  vessels = 1,
  Avatar = "head38",
  Ship = "ship37"
}
info[5100912] = {
  ID = 5100912,
  vessels = 2,
  Avatar = "head44",
  Ship = "ship37"
}
info[5100913] = {
  ID = 5100913,
  vessels = 3,
  Avatar = "head45",
  Ship = "ship37"
}
info[5100914] = {
  ID = 5100914,
  vessels = 4,
  Avatar = "head46",
  Ship = "ship37"
}
info[5100915] = {
  ID = 5100915,
  vessels = 5,
  Avatar = "head47",
  Ship = "ship37"
}
info[5100916] = {
  ID = 5100916,
  vessels = 6,
  Avatar = "head89",
  Ship = "ship38"
}
info[5100921] = {
  ID = 5100921,
  vessels = 1,
  Avatar = "head38",
  Ship = "ship37"
}
info[5100922] = {
  ID = 5100922,
  vessels = 2,
  Avatar = "head44",
  Ship = "ship37"
}
info[5100923] = {
  ID = 5100923,
  vessels = 3,
  Avatar = "head45",
  Ship = "ship37"
}
info[5100924] = {
  ID = 5100924,
  vessels = 4,
  Avatar = "head46",
  Ship = "ship37"
}
info[5100925] = {
  ID = 5100925,
  vessels = 5,
  Avatar = "head47",
  Ship = "ship37"
}
info[5100926] = {
  ID = 5100926,
  vessels = 6,
  Avatar = "head99",
  Ship = "ship38"
}
info[5100931] = {
  ID = 5100931,
  vessels = 1,
  Avatar = "head38",
  Ship = "ship37"
}
info[5100932] = {
  ID = 5100932,
  vessels = 2,
  Avatar = "head44",
  Ship = "ship37"
}
info[5100933] = {
  ID = 5100933,
  vessels = 3,
  Avatar = "head45",
  Ship = "ship37"
}
info[5100934] = {
  ID = 5100934,
  vessels = 4,
  Avatar = "head46",
  Ship = "ship37"
}
info[5100935] = {
  ID = 5100935,
  vessels = 5,
  Avatar = "head47",
  Ship = "ship37"
}
info[5100936] = {
  ID = 5100936,
  vessels = 6,
  Avatar = "head90",
  Ship = "ship38"
}
info[5100941] = {
  ID = 5100941,
  vessels = 1,
  Avatar = "head38",
  Ship = "ship37"
}
info[5100942] = {
  ID = 5100942,
  vessels = 2,
  Avatar = "head44",
  Ship = "ship37"
}
info[5100943] = {
  ID = 5100943,
  vessels = 3,
  Avatar = "head45",
  Ship = "ship37"
}
info[5100944] = {
  ID = 5100944,
  vessels = 4,
  Avatar = "head46",
  Ship = "ship37"
}
info[5100945] = {
  ID = 5100945,
  vessels = 5,
  Avatar = "head47",
  Ship = "ship37"
}
info[5100946] = {
  ID = 5100946,
  vessels = 6,
  Avatar = "head99",
  Ship = "ship38"
}
info[5100951] = {
  ID = 5100951,
  vessels = 1,
  Avatar = "head38",
  Ship = "ship37"
}
info[5100952] = {
  ID = 5100952,
  vessels = 2,
  Avatar = "head44",
  Ship = "ship37"
}
info[5100953] = {
  ID = 5100953,
  vessels = 3,
  Avatar = "head45",
  Ship = "ship37"
}
info[5100954] = {
  ID = 5100954,
  vessels = 4,
  Avatar = "head46",
  Ship = "ship37"
}
info[5100955] = {
  ID = 5100955,
  vessels = 5,
  Avatar = "head47",
  Ship = "ship37"
}
info[5100956] = {
  ID = 5100956,
  vessels = 6,
  Avatar = "head97",
  Ship = "ship38"
}
info[5100961] = {
  ID = 5100961,
  vessels = 1,
  Avatar = "head38",
  Ship = "ship37"
}
info[5100962] = {
  ID = 5100962,
  vessels = 2,
  Avatar = "head44",
  Ship = "ship37"
}
info[5100963] = {
  ID = 5100963,
  vessels = 3,
  Avatar = "head45",
  Ship = "ship37"
}
info[5100964] = {
  ID = 5100964,
  vessels = 4,
  Avatar = "head46",
  Ship = "ship37"
}
info[5100965] = {
  ID = 5100965,
  vessels = 5,
  Avatar = "head47",
  Ship = "ship37"
}
info[5100966] = {
  ID = 5100966,
  vessels = 6,
  Avatar = "head98",
  Ship = "ship38"
}
info[5100971] = {
  ID = 5100971,
  vessels = 1,
  Avatar = "head38",
  Ship = "ship37"
}
info[5100972] = {
  ID = 5100972,
  vessels = 2,
  Avatar = "head44",
  Ship = "ship37"
}
info[5100973] = {
  ID = 5100973,
  vessels = 3,
  Avatar = "head45",
  Ship = "ship37"
}
info[5100974] = {
  ID = 5100974,
  vessels = 4,
  Avatar = "head46",
  Ship = "ship37"
}
info[5100975] = {
  ID = 5100975,
  vessels = 5,
  Avatar = "head47",
  Ship = "ship37"
}
info[5100976] = {
  ID = 5100976,
  vessels = 6,
  Avatar = "head95",
  Ship = "ship38"
}
info[5100981] = {
  ID = 5100981,
  vessels = 1,
  Avatar = "head38",
  Ship = "ship37"
}
info[5100982] = {
  ID = 5100982,
  vessels = 2,
  Avatar = "head44",
  Ship = "ship37"
}
info[5100983] = {
  ID = 5100983,
  vessels = 3,
  Avatar = "head45",
  Ship = "ship37"
}
info[5100984] = {
  ID = 5100984,
  vessels = 4,
  Avatar = "head46",
  Ship = "ship37"
}
info[5100985] = {
  ID = 5100985,
  vessels = 5,
  Avatar = "head47",
  Ship = "ship37"
}
info[5100986] = {
  ID = 5100986,
  vessels = 6,
  Avatar = "head84",
  Ship = "ship38"
}
info[5101011] = {
  ID = 5101011,
  vessels = 1,
  Avatar = "head171",
  Ship = "ship178"
}
info[5101012] = {
  ID = 5101012,
  vessels = 2,
  Avatar = "head171",
  Ship = "ship178"
}
info[5101013] = {
  ID = 5101013,
  vessels = 3,
  Avatar = "head171",
  Ship = "ship178"
}
info[5101014] = {
  ID = 5101014,
  vessels = 4,
  Avatar = "head26",
  Ship = "ship5"
}
info[5101015] = {
  ID = 5101015,
  vessels = 5,
  Avatar = "head171",
  Ship = "ship178"
}
info[5101016] = {
  ID = 5101016,
  vessels = 6,
  Avatar = "head23",
  Ship = "ship70"
}
info[5101021] = {
  ID = 5101021,
  vessels = 1,
  Avatar = "head32",
  Ship = "ship14"
}
info[5101022] = {
  ID = 5101022,
  vessels = 2,
  Avatar = "head32",
  Ship = "ship14"
}
info[5101023] = {
  ID = 5101023,
  vessels = 3,
  Avatar = "head32",
  Ship = "ship14"
}
info[5101024] = {
  ID = 5101024,
  vessels = 4,
  Avatar = "head32",
  Ship = "ship14"
}
info[5101025] = {
  ID = 5101025,
  vessels = 5,
  Avatar = "head41",
  Ship = "ship25"
}
info[5101026] = {
  ID = 5101026,
  vessels = 6,
  Avatar = "head34",
  Ship = "ship64"
}
info[5101031] = {
  ID = 5101031,
  vessels = 1,
  Avatar = "head13",
  Ship = "ship39"
}
info[5101032] = {
  ID = 5101032,
  vessels = 2,
  Avatar = "head13",
  Ship = "ship39"
}
info[5101033] = {
  ID = 5101033,
  vessels = 3,
  Avatar = "head13",
  Ship = "ship39"
}
info[5101034] = {
  ID = 5101034,
  vessels = 4,
  Avatar = "head13",
  Ship = "ship39"
}
info[5101035] = {
  ID = 5101035,
  vessels = 5,
  Avatar = "head14",
  Ship = "ship47"
}
info[5101036] = {
  ID = 5101036,
  vessels = 6,
  Avatar = "head24",
  Ship = "ship5"
}
info[5101041] = {
  ID = 5101041,
  vessels = 1,
  Avatar = "head41",
  Ship = "ship11"
}
info[5101042] = {
  ID = 5101042,
  vessels = 2,
  Avatar = "head41",
  Ship = "ship11"
}
info[5101043] = {
  ID = 5101043,
  vessels = 3,
  Avatar = "head41",
  Ship = "ship11"
}
info[5101044] = {
  ID = 5101044,
  vessels = 4,
  Avatar = "head41",
  Ship = "ship11"
}
info[5101045] = {
  ID = 5101045,
  vessels = 5,
  Avatar = "head40",
  Ship = "ship14"
}
info[5101046] = {
  ID = 5101046,
  vessels = 6,
  Avatar = "head71",
  Ship = "ship12"
}
info[5101051] = {
  ID = 5101051,
  vessels = 1,
  Avatar = "head46",
  Ship = "ship80"
}
info[5101052] = {
  ID = 5101052,
  vessels = 2,
  Avatar = "head46",
  Ship = "ship80"
}
info[5101053] = {
  ID = 5101053,
  vessels = 3,
  Avatar = "head46",
  Ship = "ship39"
}
info[5101054] = {
  ID = 5101054,
  vessels = 4,
  Avatar = "head46",
  Ship = "ship39"
}
info[5101055] = {
  ID = 5101055,
  vessels = 5,
  Avatar = "head47",
  Ship = "ship25"
}
info[5101056] = {
  ID = 5101056,
  vessels = 6,
  Avatar = "head36",
  Ship = "ship57"
}
info[5101061] = {
  ID = 5101061,
  vessels = 1,
  Avatar = "head36",
  Ship = "ship39"
}
info[5101062] = {
  ID = 5101062,
  vessels = 2,
  Avatar = "head36",
  Ship = "ship39"
}
info[5101063] = {
  ID = 5101063,
  vessels = 3,
  Avatar = "head36",
  Ship = "ship39"
}
info[5101064] = {
  ID = 5101064,
  vessels = 4,
  Avatar = "head36",
  Ship = "ship39"
}
info[5101065] = {
  ID = 5101065,
  vessels = 5,
  Avatar = "head44",
  Ship = "ship12"
}
info[5101066] = {
  ID = 5101066,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship64"
}
info[5101071] = {
  ID = 5101071,
  vessels = 1,
  Avatar = "head26",
  Ship = "ship14"
}
info[5101072] = {
  ID = 5101072,
  vessels = 2,
  Avatar = "head26",
  Ship = "ship14"
}
info[5101073] = {
  ID = 5101073,
  vessels = 3,
  Avatar = "head26",
  Ship = "ship14"
}
info[5101074] = {
  ID = 5101074,
  vessels = 4,
  Avatar = "head26",
  Ship = "ship14"
}
info[5101075] = {
  ID = 5101075,
  vessels = 5,
  Avatar = "head32",
  Ship = "ship25"
}
info[5101076] = {
  ID = 5101076,
  vessels = 6,
  Avatar = "head34",
  Ship = "ship70"
}
info[5101081] = {
  ID = 5101081,
  vessels = 1,
  Avatar = "head71",
  Ship = "ship39"
}
info[5101082] = {
  ID = 5101082,
  vessels = 2,
  Avatar = "head71",
  Ship = "ship37"
}
info[5101083] = {
  ID = 5101083,
  vessels = 3,
  Avatar = "head71",
  Ship = "ship5"
}
info[5101084] = {
  ID = 5101084,
  vessels = 4,
  Avatar = "head71",
  Ship = "ship39"
}
info[5101085] = {
  ID = 5101085,
  vessels = 5,
  Avatar = "head161",
  Ship = "ship176"
}
info[5101086] = {
  ID = 5101086,
  vessels = 6,
  Avatar = "head168",
  Ship = "ship178"
}
