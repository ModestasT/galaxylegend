local GameUIFairArenaRank = LuaObjectManager:GetLuaObject("GameUIFairArenaRank")
local GameUIFairArenaMain = LuaObjectManager:GetLuaObject("GameUIFairArenaMain")
local GameStateFairArena = GameStateManager.GameStateFairArena
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
local GameUIWDStuff = LuaObjectManager:GetLuaObject("GameUIWDStuff")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local GameObjectBattleReplay = LuaObjectManager:GetLuaObject("GameObjectBattleReplay")
local GameStateBattlePlay = GameStateManager.GameStateBattlePlay
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
GameUIFairArenaRank.baseData = nil
GameUIFairArenaRank.rankData = nil
GameUIFairArenaRank.rewardData = nil
GameUIFairArenaRank.reportData = nil
GameUIFairArenaRank.HelpType = 1
GameUIFairArenaRank.RewardType = 2
GameUIFairArenaRank.RankType = 3
GameUIFairArenaRank.ReportType = 4
GameUIFairArenaRank.curShowType = 1
GameUIFairArenaRank.curPageSeasonID = 0
GameUIFairArenaRank.curReportPlayerID = ""
GameUIFairArenaRank.curReportPlayerName = ""
GameUIFairArenaRank.curReportLogicID = ""
function GameUIFairArenaRank:OnInitGame()
end
function GameUIFairArenaRank:Close()
  if GameUIFairArenaRank:GetFlashObject() then
    GameUIFairArenaRank:GetFlashObject():InvokeASCallback("_root", "MoveOut")
  end
end
function GameUIFairArenaRank:Show(showType, playerInfo)
  GameUIFairArenaRank.curShowType = showType
  if playerInfo then
    local playerID, playerName, logicID = unpack(LuaUtils:string_split(playerInfo, "|"))
    GameUIFairArenaRank:SetReportPlayerID(playerID, playerName, logicID)
  end
  GameStateFairArena:AddObject(GameUIFairArenaRank)
end
function GameUIFairArenaRank:OnAddToGameState()
  DebugOut("GameUIFairArenaRank:OnAddToGameState")
  self:LoadFlashObject()
  if GameUIFairArenaRank.curShowType == GameUIFairArenaRank.HelpType then
    GameUIFairArenaRank:InitHelpBaseUI()
  elseif GameUIFairArenaRank.curShowType == GameUIFairArenaRank.RewardType then
    GameUIFairArenaRank:InitRewardBaseUI()
  elseif GameUIFairArenaRank.curShowType == GameUIFairArenaRank.RankType then
    GameUIFairArenaRank:InitRankBaseUI()
  elseif GameUIFairArenaRank.curShowType == GameUIFairArenaRank.ReportType then
    GameUIFairArenaRank:InitReportBaseUI()
  end
end
function GameUIFairArenaRank:InitHelpBaseUI()
  local data = {}
  data.helpStr = GameLoader:GetGameText("LC_HELP_EMPIRE_COLOSSEUM_INSTRUCTION")
  self:GetFlashObject():InvokeASCallback("_root", "initHelpBaseUI", data)
end
function GameUIFairArenaRank:InitRewardBaseUI()
  local data = {}
  self:GetFlashObject():InvokeASCallback("_root", "initRewardBaseUI", data)
end
function GameUIFairArenaRank:InitRankBaseUI()
  local data = {}
  self:GetFlashObject():InvokeASCallback("_root", "initRankBaseUI", data)
end
function GameUIFairArenaRank:InitReportBaseUI()
  local data = {}
  self:GetFlashObject():InvokeASCallback("_root", "initReportBaseUI", data)
end
function GameUIFairArenaRank:OnEraseFromGameState()
  self:UnloadFlashObject()
  GameUIFairArenaRank.baseData = nil
  GameUIFairArenaRank.rankData = nil
  GameUIFairArenaRank.rewardData = nil
  GameUIFairArenaRank.reportData = nil
  GameUIFairArenaRank.curShowType = 1
  GameUIFairArenaRank.curPageSeasonID = 0
  GameUIFairArenaRank.curReportPlayerID = ""
  GameUIFairArenaRank.curReportPlayerName = ""
  GameUIFairArenaRank.curReportLogicID = ""
  collectgarbage("collect")
end
function GameUIFairArenaRank:SetReportPlayerID(playerID, PlayerName, logicID)
  if playerID == nil then
    local userinfo = GameGlobalData:GetData("userinfo")
    GameUIFairArenaRank.curReportPlayerID = userinfo.player_id
    GameUIFairArenaRank.curReportPlayerName = userinfo.name
    local LoginInfo = GameUtils:GetLoginInfo()
    GameUIFairArenaRank.curReportLogicID = LoginInfo.ServerInfo.logic_id
  else
    GameUIFairArenaRank.curReportPlayerID = playerID
    GameUIFairArenaRank.curReportPlayerName = PlayerName
    GameUIFairArenaRank.curReportLogicID = logicID
  end
end
function GameUIFairArenaRank:setBasicData()
  if GameUIFairArenaRank.curShowType == GameUIFairArenaRank.HelpType then
  elseif GameUIFairArenaRank.curShowType == GameUIFairArenaRank.RewardType then
    GameUIFairArenaRank:SetRewardBasic()
  elseif GameUIFairArenaRank.curShowType == GameUIFairArenaRank.RankType then
    GameUIFairArenaRank:SetRankBasic()
  elseif GameUIFairArenaRank.curShowType == GameUIFairArenaRank.ReportType then
    if GameUIFairArenaRank.curReportPlayerID == "" then
      local userinfo = GameGlobalData:GetData("userinfo")
      GameUIFairArenaRank.curReportPlayerID = userinfo.player_id
      GameUIFairArenaRank.curReportPlayerName = userinfo.name
      local LoginInfo = GameUtils:GetLoginInfo()
      GameUIFairArenaRank.curReportLogicID = LoginInfo.ServerInfo.logic_id
    end
    GameUIFairArenaRank:SetReportBasic(GameUIFairArenaRank.curReportPlayerID, GameUIFairArenaRank.curReportLogicID)
  end
end
function GameUIFairArenaRank:SwitchState(state)
  GameUIFairArenaRank.curShowType = state
  if GameUIFairArenaRank.curShowType ~= GameUIFairArenaRank.ReportType then
    GameUIFairArenaRank.curReportPlayerID = ""
  end
  local flashObj = GameUIFairArenaRank:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "swicthState", state)
  end
end
function GameUIFairArenaRank:OnFSCommand(cmd, arg)
  if cmd == "close" then
    local userinfo = GameGlobalData:GetData("userinfo")
    DebugOut("Close :", GameUIFairArenaRank.curReportPlayerID)
    if GameUIFairArenaRank.curReportPlayerID == userinfo.player_id or GameUIFairArenaRank.curReportPlayerID == "" then
      GameStateFairArena:SwitchState(GameStateFairArena.MainState)
      GameUIFairArenaRank.curReportPlayerID = ""
    else
      GameUIFairArenaRank:SwitchState(GameUIFairArenaRank.RankType)
    end
    return
  end
  if cmd == "setBasicData" then
    GameUIFairArenaRank:setBasicData()
  elseif cmd == "closeBtn" then
    GameUIFairArenaRank:Close()
  elseif cmd == "update_rank_item" then
    GameUIFairArenaRank:UpdateRankItem(tonumber(arg))
  elseif cmd == "update_reward_item" then
    GameUIFairArenaRank:UpdateRewardItem(tonumber(arg))
  elseif cmd == "update_reward_sub_item" then
    local mainid, subindex = unpack(LuaUtils:string_split(arg, "|"))
    GameUIFairArenaRank:UpdateRewardSubItem(tonumber(mainid), tonumber(subindex))
  elseif cmd == "update_report_item" then
    GameUIFairArenaRank:UpdateReportItem(tonumber(arg))
  elseif cmd == "setPageData" then
    GameUIFairArenaRank:getPageData(tonumber(arg))
  elseif cmd == "moveend" then
    GameUIFairArenaRank:SetPageData()
  elseif cmd == "showDetail" then
    local id, subid = unpack(LuaUtils:string_split(arg, "|"))
    local item = GameUIFairArenaRank:GetRewardItem(tonumber(id))
    local it = item.list[tonumber(subid)]
    DebugTable(it)
    if it and it.model.item_type == "item" then
      it.model.cnt = 1
      ItemBox:showItemBox("Item", it.model, tonumber(it.model.number), 320, 480, 200, 200, "", nil, "", nil)
    elseif it and it.model.item_type == "fleet" then
      local fleetID = it.model.number
      ItemBox:ShowCommanderDetail2(fleetID, nil, nil)
    end
  elseif cmd == "GetReward" then
    do
      local id, itemIndex = unpack(LuaUtils:string_split(arg, "|"))
      local param = {}
      param.id = tonumber(id)
      local function callback(msgType, content)
        if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.fair_arena_reward_get_req.Code then
          if content.code ~= 0 then
            GameUIGlobalScreen:ShowAlert("error", content.code, nil)
          else
            GameTip:Show(GameLoader:GetGameText("LC_MENU_TITLE_SUCCESS_INFO"))
            local item = GameUIFairArenaRank:GetRewardItem(tonumber(id))
            if item then
              item.state = 2
              if item.state == 0 then
                item.stateStr = GameLoader:GetGameText("LC_MENU_TC_MISSION_AWARD_INCOMPLETE")
              elseif item.state == 1 then
                item.stateStr = GameLoader:GetGameText("LC_MENU_CAPTION_RECEIVE")
              elseif item.state == 2 then
                item.stateStr = GameLoader:GetGameText("LC_MENU_CLAIMED_BUTTON")
              end
            end
            GameUIFairArenaRank:UpdateRewardItem(tonumber(itemIndex))
          end
          return true
        end
        return false
      end
      NetMessageMgr:SendMsg(NetAPIList.fair_arena_reward_get_req.Code, param, callback, true, nil)
    end
  elseif cmd == "getAllReward" then
    local param = {}
    param.id = -1
    local function callback(msgType, content)
      if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.fair_arena_reward_get_req.Code then
        if content.code ~= 0 then
          GameUIGlobalScreen:ShowAlert("error", content.code, nil)
        else
          GameTip:Show(GameLoader:GetGameText("LC_MENU_TITLE_SUCCESS_INFO"))
          for k, v in pairs(GameUIFairArenaRank.rewardData.rewardList or {}) do
            if v.state == 1 then
              v.state = 2
              v.stateStr = GameLoader:GetGameText("LC_MENU_CLAIMED_BUTTON")
            end
          end
          local flashObj = GameUIFairArenaRank:GetFlashObject()
          if flashObj then
            flashObj:InvokeASCallback("_root", "updateAllRewardList")
          end
        end
        return true
      end
      return false
    end
    NetMessageMgr:SendMsg(NetAPIList.fair_arena_reward_get_req.Code, param, callback, true, nil)
  elseif cmd == "ReplayReport" then
    local reportID = tonumber(arg)
    GameUIFairArenaRank:onReport(reportID)
  elseif cmd == "rankHelp" then
    GameUIWDStuff:Show(GameUIWDStuff.menuType.menu_type_help)
    GameUIWDStuff:SetHelpText(GameLoader:GetGameText("LC_HELP_EMPIRE_COLOSSEUM_RANKING"))
  elseif cmd == "CheckReport" then
    local index = tonumber(arg)
    local rankItem = GameUIFairArenaRank.rankData.rankList[index]
    if rankItem then
      GameUIFairArenaRank:SetReportPlayerID(rankItem.player_id, rankItem.player_name, rankItem.logic_id)
      GameUIFairArenaRank:SwitchState(GameUIFairArenaRank.ReportType)
    end
  end
end
function GameUIFairArenaRank:SetRankBasic()
  NetMessageMgr:SendMsg(NetAPIList.fair_arena_rank_req.Code, nil, GameUIFairArenaRank.RankReqCallBack, true, nil)
end
function GameUIFairArenaRank.RankReqCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.fair_arena_rank_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.fair_arena_rank_ack.Code then
    GameUIFairArenaRank:GenerateRankData(content)
    GameUIFairArenaRank:InitRankData()
    GameUIFairArenaRank:SetRankType(1)
    return true
  end
  return false
end
function GameUIFairArenaRank:GenerateRankData(content)
  DebugTable(content)
  local data = {}
  data.rankList = {}
  data.seasonList = {}
  for k, v in ipairs(content.rank_list or {}) do
    local item = {}
    item = v
    item.isShowCheck = v.show_check
    item.displayName = v.player_name
    item.displayRate = string.format("%.02f", v.win_rate / 1000) .. "%"
    data.rankList[#data.rankList + 1] = item
  end
  for k, v in ipairs(content.season_rank_list or {}) do
    local item = {}
    item.seasonId = v.season_id
    item.seasonStr = string.format(GameLoader:GetGameText("LC_MENU_TC_SEASON_LABEL"), v.season_id)
    item.list = {}
    for i, j in ipairs(v.season_items or {}) do
      local it = {}
      it = j
      it.avatar = FleetDataAccessHelper:GetFleetAvatarFrameByAvatar(j.avatar, nil, j.level)
      it.displayName = j.player_name
      it.displayRate = string.format("%.02f", j.win_rate / 1000) .. "%"
      it.displayScore = GameLoader:GetGameText("LC_MENU_RANKINGS_INFO_1") .. j.score
      item.list[#item.list + 1] = it
    end
    table.sort(item.list, function(a, b)
      return a.rank_id <= b.rank_id
    end)
    data.seasonList[#data.seasonList + 1] = item
  end
  table.sort(data.seasonList, function(a, b)
    return a.seasonId >= b.seasonId
  end)
  local itemLast = data.rankList[#data.rankList]
  data.myRank = itemLast.rank_id
  data.myScore = itemLast.score
  data.myWinRate = itemLast.displayRate
  data.myWinCount = itemLast.win_count
  data.myName = itemLast.displayName
  table.remove(data.rankList, #data.rankList)
  data.rankItemCount = #data.rankList
  data.seasonItemCount = #data.seasonList
  data.maxSeasonID = data.seasonList[1] and data.seasonList[1].seasonId or 0
  data.noOpenStr = GameLoader:GetGameText("LC_MENU_RANKINGS_NAME_2")
  data.openStr = GameLoader:GetGameText("LC_MENU_RANKINGS_NAME_1")
  GameUIFairArenaRank.rankData = data
end
function GameUIFairArenaRank:SetRankType(tab)
  local flashObj = GameUIFairArenaRank:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "setRankType", tab)
  end
end
function GameUIFairArenaRank:InitRankData()
  if GameUIFairArenaRank.rankData then
    local flashObj = GameUIFairArenaRank:GetFlashObject()
    if flashObj then
      flashObj:InvokeASCallback("_root", "setRankData", GameUIFairArenaRank.rankData)
    end
  end
end
function GameUIFairArenaRank:UpdateRankItem(itemIndex)
  local item = GameUIFairArenaRank.rankData.rankList[itemIndex]
  if item then
    local flashObj = GameUIFairArenaRank:GetFlashObject()
    if flashObj then
      flashObj:InvokeASCallback("_root", "setRankItemData", item, itemIndex)
    end
  end
end
function GameUIFairArenaRank:getPageData(sessionId)
  GameUIFairArenaRank.curPageSeasonID = tonumber(sessionId)
  if tonumber(sessionId) < GameUIFairArenaRank.rankData.maxSeasonID and tonumber(sessionId) > 1 then
    local item1 = GameUIFairArenaRank:getSessionData(tonumber(sessionId) - 1)
    if item1 == nil then
      local param = {}
      param.season_id = tonumber(sessionId)
      NetMessageMgr:SendMsg(NetAPIList.fair_arena_season_req.Code, param, GameUIFairArenaRank.RankReqCallBack, true, nil)
    end
  end
end
function GameUIFairArenaRank.SessionReqCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.fair_arena_season_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.fair_arena_season_ack.Code then
    GameUIFairArenaRank:GenerateSeasonData(content)
    return true
  end
  return false
end
function GameUIFairArenaRank:GenerateSeasonData(content)
  for k, v in ipairs(content.season_rank_list or {}) do
    local item = {}
    item.seasonId = v.season_id
    item.seasonStr = string.gsub(GameLoader:GetGameText("LC_MENU_CLAIMED_BUTTON"), "<number1>", tostring(v.season_id))
    item.list = {}
    for i, j in ipairs(v.season_items or {}) do
      local it = {}
      it = j
      it.displayName = GameUtils:GetUserDisplayName(j.player_name)
      it.displayRate = string.format("%.02f", j.win_rate / 1000) .. "%"
      it.displayScore = GameLoader:GetGameText("LC_MENU_RANKINGS_INFO_1") .. j.score
      item.list[item.list + 1] = it
    end
    GameUIFairArenaRank.rankData.seasonList[GameUIFairArenaRank.rankData.seasonList + 1] = item
  end
  table.sort(GameUIFairArenaRank.rankData.seasonList, function(a, b)
    return a.seasonId >= b.seasonId
  end)
  GameUIFairArenaRank.rankData.seasonItemCount = #data.seasonList
end
function GameUIFairArenaRank:SetPageData()
  local item1 = GameUIFairArenaRank:getSessionData(GameUIFairArenaRank.curPageSeasonID)
  local item2 = GameUIFairArenaRank:getSessionData(GameUIFairArenaRank.curPageSeasonID - 1)
  local item3 = GameUIFairArenaRank:getSessionData(GameUIFairArenaRank.curPageSeasonID + 1)
  local flashObj = GameUIFairArenaRank:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "setSeasonPageData", item1, item2, item3)
  end
end
function GameUIFairArenaRank:getSessionData(sessionID)
  for k, v in pairs(GameUIFairArenaRank.rankData.seasonList or {}) do
    if v.seasonId == sessionID then
      return v
    end
  end
  return nil
end
function GameUIFairArenaRank:SetRewardBasic()
  NetMessageMgr:SendMsg(NetAPIList.fair_arena_reward_req.Code, nil, GameUIFairArenaRank.RewardReqCallBack, true, nil)
end
function GameUIFairArenaRank.RewardReqCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.fair_arena_reward_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      GameStateFairArena:SwitchState(GameStateFairArena.MainState)
    end
    return true
  elseif msgType == NetAPIList.fair_arena_reward_ack.Code then
    GameUIFairArenaRank:GenerateRewardData(content)
    GameUIFairArenaRank:InitRewardData()
    return true
  end
  return false
end
function GameUIFairArenaRank:GenerateRewardData(content)
  local data = {}
  data.rewardList = {}
  for k, v in ipairs(content.reward_list or {}) do
    local item = {}
    item.state = v.state
    item.level = v.state
    item.id = v.id
    item.stateStr = ""
    if v.state == 0 then
      item.stateStr = GameLoader:GetGameText("LC_MENU_TC_MISSION_AWARD_INCOMPLETE")
    elseif v.state == 1 then
      item.stateStr = GameLoader:GetGameText("LC_MENU_CAPTION_RECEIVE")
    elseif v.state == 2 then
      item.stateStr = GameLoader:GetGameText("LC_MENU_CLAIMED_BUTTON")
    end
    item.levelStr = string.gsub(GameLoader:GetGameText("LC_MENU_DAILY_REWARDS_CONDITION"), "<number1>", tostring(v.level))
    item.list = {}
    for i, j in ipairs(v.reward_items or {}) do
      local it = {}
      it.count = GameHelper:GetAwardCount(j.item_type, j.number, j.no)
      it.displayCount = GameUtils.numberConversion(math.floor(it.count))
      it.frame = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(j)
      it.model = j
      item.list[#item.list + 1] = it
    end
    data.rewardList[#data.rewardList + 1] = item
  end
  GameUIFairArenaRank.rewardData = data
end
function GameUIFairArenaRank:InitRewardData()
  if GameUIFairArenaRank.rewardData then
    local flashObj = GameUIFairArenaRank:GetFlashObject()
    if flashObj then
      flashObj:InvokeASCallback("_root", "setRewardData", #GameUIFairArenaRank.rewardData.rewardList)
    end
  end
end
function GameUIFairArenaRank:UpdateRewardItem(itemIndex)
  local item = GameUIFairArenaRank.rewardData.rewardList[itemIndex]
  if item then
    local flashObj = GameUIFairArenaRank:GetFlashObject()
    if flashObj then
      flashObj:InvokeASCallback("_root", "setRewardItemData", item, itemIndex)
    end
  end
end
function GameUIFairArenaRank:GetRewardItem(id)
  for k, v in pairs(GameUIFairArenaRank.rewardData.rewardList or {}) do
    if v.id == id then
      return v
    end
  end
  return nil
end
function GameUIFairArenaRank:UpdateRewardSubItem(mainID, itemIndex)
  local item = GameUIFairArenaRank.rewardData.rewardList[mainID]
  if item then
    local subIten = item.list[itemIndex]
    if subIten then
      local flashObj = GameUIFairArenaRank:GetFlashObject()
      if flashObj then
        flashObj:InvokeASCallback("_root", "setRewardSubItemData", subIten, mainID, itemIndex)
      end
    end
  end
end
function GameUIFairArenaRank:SetReportBasic(playerID, logicID)
  local param = {}
  param.player_id = playerID
  param.logic_id = tostring(logicID)
  NetMessageMgr:SendMsg(NetAPIList.fair_arena_report_req.Code, param, GameUIFairArenaRank.ReportReqCallBack, true, nil)
end
function GameUIFairArenaRank.ReportReqCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.fair_arena_report_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.fair_arena_report_ack.Code then
    GameUIFairArenaRank:GenerateReportData(content)
    GameUIFairArenaRank:InitReportData()
    return true
  end
  return false
end
function GameUIFairArenaRank:GenerateReportData(content)
  DebugOut("GameUIFairArenaRank:GenerateReportData")
  DebugTable(content)
  local data = {}
  data.reportList = {}
  data.winRate = content.win_rate
  data.winCount = content.win_count
  data.failCount = content.fail_count
  data.titleStr = GameLoader:GetGameText("LC_MENU_EMPIRE_COLOSSEUM_FUNCTION_BUTTON_NAME_1")
  local userinfo = GameGlobalData:GetData("userinfo")
  if GameUIFairArenaRank.curReportPlayerID == userinfo.player_id then
    data.titleStr = GameLoader:GetGameText("LC_MENU_EMPIRE_COLOSSEUM_FUNCTION_BUTTON_NAME_1")
    data.isMy = true
  else
    data.titleStr = string.gsub(GameLoader:GetGameText("LC_MENU_BATTLE_REPORT_NAME"), "<player_name>", GameUIFairArenaRank.curReportPlayerName)
    data.isMy = false
  end
  data.infoStr = GameLoader:GetGameText("LC_MENU_DAILY_REPORT_INFO_NAME_1") .. string.format("%.02f%%", data.winRate / 1000) .. "  " .. GameLoader:GetGameText("LC_MENU_DAILY_REPORT_INFO_NAME_2") .. data.winCount .. "  " .. GameLoader:GetGameText("LC_MENU_DAILY_REPORT_INFO_NAME_3") .. data.failCount
  local lang = "en"
  if GameSettingData and GameSettingData.Save_Lang then
    lang = GameSettingData.Save_Lang
    if string.find(lang, "ru") == 1 then
      lang = "ru"
    end
  end
  for k, v in ipairs(content.report_list or {}) do
    local item = {}
    item.time = v.time
    item.timeStr = GameUtils:formatTimeStringAsAgoStyle(v.time)
    item.id = v.id
    item.state = v.state
    item.opponentState = v.opponent_state
    item.name = v.own.name
    item.opponentName = v.opponent.name
    item.lang = lang
    item.ownlist = {}
    item.opponentlist = {}
    for i, j in ipairs(v.own.cached_fleets or {}) do
      local it = {}
      local fleet = GameDataAccessHelper:GetCommanderAbility(j.fleet_identity, j.level)
      it.avatar = GameDataAccessHelper:GetFleetAvatar(j.fleet_identity, j.level)
      it.color = FleetDataAccessHelper:GetFleetColorFrameByColor(fleet.COLOR)
      item.ownlist[#item.ownlist + 1] = it
    end
    for i, j in ipairs(v.opponent.cached_fleets or {}) do
      local it = {}
      local fleet = GameDataAccessHelper:GetCommanderAbility(j.fleet_identity, j.level)
      it.avatar = GameDataAccessHelper:GetFleetAvatar(j.fleet_identity, j.level)
      it.color = FleetDataAccessHelper:GetFleetColorFrameByColor(fleet.COLOR)
      item.opponentlist[#item.opponentlist + 1] = it
    end
    data.reportList[#data.reportList + 1] = item
  end
  GameUIFairArenaRank.reportData = data
end
function GameUIFairArenaRank:InitReportData()
  if GameUIFairArenaRank.reportData then
    local flashObj = GameUIFairArenaRank:GetFlashObject()
    if flashObj then
      flashObj:InvokeASCallback("_root", "setReportData", #GameUIFairArenaRank.reportData.reportList, GameUIFairArenaRank.reportData)
    end
  end
end
function GameUIFairArenaRank:UpdateReportItem(itemIndex)
  local item = GameUIFairArenaRank.reportData.reportList[itemIndex]
  if item then
    local flashObj = GameUIFairArenaRank:GetFlashObject()
    if flashObj then
      flashObj:InvokeASCallback("_root", "setReportItemData", item, itemIndex)
    end
  end
end
function GameUIFairArenaRank:GetReportItem(id)
  for k, v in pairs(GameUIFairArenaRank.reportData.reportList or {}) do
    if v.id == id then
      return v
    end
  end
  return nil
end
function GameUIFairArenaRank:onReport(report_id)
  local function _callback(msgtype, content)
    DebugTable(content)
    if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.fair_arena_report_battle_req.Code then
      if content.code ~= 0 then
        GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      else
        local battleplay = GameStateManager.GameStateBattlePlay
        if GameObjectBattleReplay.GroupBattleReportArr[1] and 0 < #GameObjectBattleReplay.GroupBattleReportArr[1].headers then
          GameStateBattlePlay.curBattleType = "fairarena"
          battleplay:InitBattle(battleplay.MODE_HISTORY, GameObjectBattleReplay.GroupBattleReportArr[1].headers[1].report)
          do
            local playerID = GameUIFairArenaRank.curReportPlayerID
            local playerName = GameUIFairArenaRank.curReportPlayerName
            local logicID = GameUIFairArenaRank.curReportLogicID
            GameStateBattlePlay:RegisterOverCallback(function()
              GameStateManager:SetCurrentGameState(GameStateFairArena)
              GameStateFairArena:SwitchState(GameStateFairArena.RankState, 4, playerID .. "|" .. playerName .. "|" .. logicID)
            end, nil)
            GameStateManager:SetCurrentGameState(battleplay)
          end
        end
      end
      return true
    end
    return false
  end
  local param = {}
  param.id = report_id
  GameStateBattlePlay.report_id = report_id
  NetMessageMgr:SendMsg(NetAPIList.fair_arena_report_battle_req.Code, param, _callback, true, nil)
end
function GameUIFairArenaRank.battleReportCallBack(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.fair_arena_report_battle_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgtype == NetAPIList.fair_arena_report_battle_ack.Code then
    local battle_report_data = content.report
    local battleResult = content.battle_result
    if battle_report_data then
      local lastGameState = GameStateManager:GetCurrentGameState()
      GameStateBattlePlay.curBattleType = "fairarena"
      GameStateBattlePlay:InitBattle(GameStateBattlePlay.MODE_PLAY, battle_report_data)
      GameStateBattlePlay:RegisterOverCallback(function()
        GameStateManager:SetCurrentGameState(GameStateFairArena)
        GameStateFairArena:SwitchState(GameStateFairArena.RankState, 4, GameUIFairArenaRank.curReportPlayerID .. "|" .. GameUIFairArenaRank.curReportPlayerName)
      end, nil)
      GameStateManager:SetCurrentGameState(GameStateBattlePlay)
    end
    return true
  end
  return false
end
function GameUIFairArenaRank:Update(dt)
  local flashObj = GameUIFairArenaRank:GetFlashObject()
  if flashObj then
    flashObj:Update(dt)
    flashObj:InvokeASCallback("_root", "OnUpdate")
  end
end
if AutoUpdate.isAndroidDevice then
  function GameUIFairArenaRank.OnAndroidBack()
    GameUIFairArenaRank:OnFSCommand("closeBtn", "")
  end
end
