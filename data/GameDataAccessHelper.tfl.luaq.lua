local GameData = GameData
local Fleets = GameData.FleetBaseInfo.Fleets
local Item = GameData.Item.Keys
local Technologies = GameData.Technologies.Technologies
local Equip = GameData.Equip.Keys
local Spells = GameData.Spell.Keys
local ArtifactSpell = GameData.Spell.Spell
local AdjutantSpells = GameData.adjutant_skill.adjutant_skill
local Adventure = GameData.adventure.adventure
local PrestigeTable = GameData.Prestige.Data
local AchievementClassifyTable = GameData.achievement.Classify
local AchievementTable = GameData.achievement.Data
local Section = GameData.section.section
local vip = GameData.vip_exp.vip
local VIPLimitTable = GameData.vip_exp.limit
local slotLevel = GameData.krypton_slot.slotLevel
local rightMenu = GameData.RightMenu.rightMenu
local rightMenu_170 = GameData.RightMenu.rightMenu_170
local evolution = GameData.equip_enhance.evolution
local BuffEffect = GameData.BuffEffect.Keys
local ArcaneEnhanceTable = GameData.Dexter_ac.enhance
local ArcaneBattleTable = GameData.Dexter_ac.battlesInfo
local ArcaneMonsterTable = GameData.Dexter_ac.monsters
local MedalBossTable = GameData.medal_boss.Monster
local KryptonBase = GameData.Krypton.KRYPTON
local WorldBossInfo = GameData.world_boss.boss_info
local WVEBossInfo = GameData.wve_boss.boss_info
local Monster = GameData.Ladder.Monster
local wormHoleMonster = GameData.wormhole.Monster
local StrengthenData_170 = GameData.enchant.enchant170
local StrengthenData = GameData.enchant.enchant
local StrengthenExtData = GameData.enchant.addition
local AutoUpdateInBackground = AutoUpdateInBackground
local Search_170 = GameData.adventure.search_170
local VIPChat = GameData.vip_exp.chat
local IntroduceHereMoster = GameData.showhero.Monsters
local DownloadSpellList = GameData.effectImage.spell
local DownloadBuffList = GameData.effectImage.buff
local DownloadShipList = GameData.effectImage.ship
local DownloadHeadList = GameData.effectImage.avtar
require("data1/DownloadImageList.tfl")
local EffectImage = {}
GameDataAccessHelper = {}
GameDataAccessHelper.ATK_TYPE_common = 0
GameDataAccessHelper.ATK_TYPE_physics = 1
GameDataAccessHelper.ATK_TYPE_energy = 2
GameDataAccessHelper.FleetAbility = {}
GameDataAccessHelper.FleetsBasic = {}
GameDataAccessHelper.BuffEffectCanPlayCache = {}
GameDataAccessHelper.SPEffectCanPlayCache = {}
GameDataAccessHelper.HeadImageCache = {}
GameDataAccessHelper.ShipImageCache = {}
local InstanceIdSectionFloor = 5400000
local InstanceIdSectionCeil = 5500000
local InstanceId2SectionFloor = 5500001
local InstanceId2SectionCeil = 5600000
local MedalBossMinID = 56000001
local MedalBossMaxID = 57000000
local IntroduceHereMosterMaxID = 200000000
local IntroduceHereMosterMinID = 100000000
local LowDeveiceSpellEffect = {
  [0] = "gravity",
  [1] = "photonMissile",
  [2] = "gammaBlasterA",
  [3] = "photonMissile",
  [4] = "indra",
  [5] = "FULL_gravity"
}
local ColorToFrame = {
  [1] = "green",
  [2] = "blue",
  [3] = "purple",
  [4] = "orange",
  [5] = "golden",
  [6] = "red"
}
local ColorToColorNum = {
  [1] = "#37DD59",
  [2] = "#98FFFD",
  [3] = "#D996F4",
  [4] = "#D9BB59",
  [5] = "#FFCC00",
  [6] = "#FF9900"
}
local CommandRankType = {
  [1] = "commander_green",
  [2] = "commander_normal",
  [3] = "commander_advanced",
  [4] = "commander_orange"
}
local headFrameToHeadImageCovertor = {
  head18 = {
    small = "head11",
    middle = "head11",
    big = "head18"
  },
  head17 = {
    small = "head19",
    middle = "head19",
    big = "head17"
  },
  head15 = {
    small = "head16",
    middle = "head16",
    big = "head15"
  }
}
local shipIdToShipHurtImageCovertor = {
  ship51 = "ship45",
  ship52 = "ship2",
  ship54 = "ship40",
  ship56 = "ship46",
  ship84 = "ship83",
  ship88 = "ship63",
  ship90 = "ship59",
  ship91 = "ship68",
  ship92 = "ship69",
  ship93 = "ship76",
  ship94 = "ship71",
  ship1001 = "ship23",
  ship124 = "ship74",
  ship125 = "ship81",
  ship126 = "ship98",
  ship127 = "ship97",
  ship128 = "ship110",
  ship129 = "ship114",
  ship149 = "ship112",
  ship151 = "ship121",
  ship152 = "ship122",
  ship155 = "ship141",
  ship156 = "ship140",
  ship180 = "ship131",
  ship184 = "ship148",
  ship185 = "ship177",
  ship186 = "ship146",
  ship196 = "ship165",
  ship200 = "ship174",
  ship205 = "ship163",
  ship206 = "ship195",
  ship207 = "ship178",
  ship209 = "ship160",
  ship213 = "ship164",
  ship216 = "ship173",
  ship228 = "ship208",
  ship238 = "ship215",
  ship242 = "ship217",
  ship245 = "ship221",
  ship248 = "ship224",
  ship252 = "ship227",
  ship253 = "ship247",
  ship255 = "ship233",
  ship259 = "ship232",
  ship265 = "ship235",
  ship266 = "ship239",
  ship53 = "false",
  ship55 = "false",
  ship65 = "false",
  ship67 = "false",
  ship72 = "false",
  ship87 = "false",
  ship89 = "false",
  ship66 = "false",
  ship38 = "false",
  ship172 = "false",
  ship169 = "false",
  ship181 = "false",
  ship177 = "false"
}
GameDataAccessHelper.m_defaultSPAttack = {
  [0] = 16,
  [1] = 1,
  [2] = 2,
  [3] = 3,
  [4] = 50,
  [5] = 614
}
function GameDataAccessHelper:Init()
end
function GameDataAccessHelper:GetEffectImages(effectName)
  return EffectImage[effectName]
end
function GameDataAccessHelper:GetBattleEffectImages(effectName)
  return BattleEffectListImage[effectName]
end
function GameDataAccessHelper:GetBattleBuffImages(buffName)
  return BuffEffectListImage[buffName]
end
function GameDataAccessHelper:IsSpellResNeedDownload(spellID)
  local spellEffect = GameDataAccessHelper:GetSpillEffect(spellID)
  if not spellEffect then
    return false
  end
  for k, v in pairs(DownloadSpellList) do
    if v and v.spellName == spellEffect.EFFECT then
      return false
    end
  end
  return true
end
function GameDataAccessHelper:IsRevengeBuff(buffID)
  if buffID == 66 or buffID == 10066 or buffID == 20066 then
    return true
  end
  return false
end
function GameDataAccessHelper:GetArtifactSkillDesc(skillID)
  DebugOut("GetArtifactSkillDesc", skillID)
  local ArtifactSkillTable = v1extra_artifactartifact_text
  if ArtifactSkillTable and ArtifactSkillTable[skillID] then
    local index = math.floor(skillID / 100)
    local skill_detail = ArtifactSkillTable[skillID]
    DebugOut("GetArtifactSkillDesc:")
    DebugTable(skill_detail)
    local descString = GameLoader:GetGameText("LC_MENU_ARTIFACT_EQUIP_SPELL_" .. index)
    for k = 1, skill_detail.param_num do
      local param = "<number" .. k .. ">"
      local num_detail = skill_detail["Para_" .. k]
      descString, _ = string.gsub(descString, param, num_detail / 10)
    end
    return descString
  else
    return ...
  end
end
function GameDataAccessHelper:IsSpellResNeedDownloadByName(spellName)
  if not spellName then
    return false
  end
  if BattleEffectListImage[spellName] then
    return true
  end
  return false
end
function GameDataAccessHelper:IsBuffResNeedDownloadByName(buffName)
  if not buffName then
    return false
  end
  if BuffEffectListImage[buffName] then
    return true
  end
  return false
end
function GameDataAccessHelper:IsBuffResNeedDownload(buffID)
  local buffEffect = BuffEffect[buffID]
  if not buffEffect then
    return false
  end
  for k, v in pairs(DownloadBuffList) do
    if v and v.buffname == buffEffect.EFFECT then
      return false
    end
  end
  return true
end
function GameDataAccessHelper:IsHeadResNeedDownload(head)
  if not head then
    return false
  end
  for k, v in pairs(DownloadHeadList) do
    if v and v.avtar == head then
      return false
    end
  end
  return true
end
function GameDataAccessHelper:IsShipResNeedDownload(ship)
  if not ship then
    return false
  end
  for k, v in pairs(DownloadShipList) do
    if v and v.ship == ship then
      return false
    end
  end
  return true
end
function GameDataAccessHelper:IsPassiveBuff(buffId)
  if BuffEffect[buffId] and BuffEffect[buffId].isPassive then
    return true
  end
  return false
end
function GameDataAccessHelper:GetBuffEffectName(effectID)
  if USE_DEFAULT_EFFECT then
    return "buff_exile"
  end
  if BuffEffect[effectID] and GameDataAccessHelper:CheckBuffCanPlay(BuffEffect[effectID].name) then
    return BuffEffect[effectID].name
  end
  return ""
end
function GameDataAccessHelper:IsFullScreenBuff(effectID)
  if effectID == 80 then
    return true
  end
  if effectID == 10080 then
    return true
  end
  return false
end
function GameDataAccessHelper:IsTransformationBuff(buffID)
  if buffID == 95 then
    return true
  end
  return false
end
function GameDataAccessHelper:GetArenaRewardInfo()
  return GameData.champion_award.rank_extra_reward
end
function GameDataAccessHelper:GetArenaRewardInfoBase()
  return GameData.champion_award.rank_basic_reward
end
function GameDataAccessHelper:GetBuffEffectDesc(effectID)
  return BuffEffect[effectID].desc
end
function GameDataAccessHelper:CheckBuffCanPlay(effectName)
  if not effectName or effectName == "" then
    return true, "r1"
  end
  DebugOut("check buff effect can play name = ", effectName)
  if GameDataAccessHelper.BuffEffectCanPlayCache[effectName] then
    return true, "r2"
  end
  if GameDataAccessHelper:IsBuffResNeedDownloadByName(effectName) then
    DebugOut("check buff effect need download = ", effectName)
    local buffImageList = GameDataAccessHelper:GetBattleBuffImages(effectName)
    if not buffImageList or #buffImageList <= 0 then
      DebugOut("check buff effect download not finish 111111 = ", effectName)
      return false, "r3"
    end
    local canPlayBuff = true
    for k, v in pairs(buffImageList) do
      if (not ext.crc32.crc32(v) or ext.crc32.crc32(v) == "") and not AutoUpdateInBackground:IsHeroFileUpdatedToServer(v) then
        DebugOut("check buff effect download not finish 2222222 = ", effectName)
        canPlayBuff = false
      end
      if AutoUpdate.isAndroidDevice and AlphaListImage[v] and (not ext.crc32.crc32(AlphaListImage[v]) or not ext.crc32.crc32(AlphaListImage[v]) == "") and not AutoUpdateInBackground:IsHeroFileUpdatedToServer(AlphaListImage[v]) then
        canPlayBuff = false
      end
    end
    if canPlayBuff then
      GameDataAccessHelper.BuffEffectCanPlayCache[effectName] = true
    end
    return canPlayBuff, "r4"
  end
  return true, "r5"
end
function GameDataAccessHelper:CheckSPAttackEffectCanPlay(effectName)
  DebugOutBattlePlay("GameDataAccessHelper:CheckSPAttackEffectCanPlay: ", effectName)
  if GameDataAccessHelper.SPEffectCanPlayCache[effectName] then
    return true
  end
  if GameDataAccessHelper:IsSpellResNeedDownloadByName(effectName) then
    local effectImageList = GameDataAccessHelper:GetBattleEffectImages(effectName)
    if not effectImageList or #effectImageList <= 0 then
      return false
    end
    local canPlaySpAttack = true
    for k, v in pairs(effectImageList) do
      if (ext.crc32.crc32(v) == nil or ext.crc32.crc32(v) == "") and not AutoUpdateInBackground:IsHeroFileUpdatedToServer(v) then
        canPlaySpAttack = false
      end
      if AutoUpdate.isAndroidDevice and AlphaListImage[v] and (ext.crc32.crc32(AlphaListImage[v]) == nil or ext.crc32.crc32(AlphaListImage[v]) == "") and not AutoUpdateInBackground:IsHeroFileUpdatedToServer(AlphaListImage[v]) then
        canPlaySpAttack = false
      end
    end
    if canPlaySpAttack then
      GameDataAccessHelper.SPEffectCanPlayCache[effectName] = true
    end
    return canPlaySpAttack
  end
  DebugOut("can play attack")
  return true
end
function GameDataAccessHelper:GetSpillEffect(spillID)
  local currentSpellTable = v1extra_herosSpells
  if immanentversion == 2 then
    currentSpellTable = v2extra_herosSpells
  end
  local currentSpell
  if currentSpellTable then
    currentSpell = currentSpellTable[spillID]
  end
  if not currentSpell then
    DebugOut("Use normal spell!!!!! = ", spillID)
    currentSpell = Spells[spillID]
  else
    DebugOut("Use extra spell!!!!! = ", spillID)
  end
  if not currentSpell then
    DebugOut("Use extral_ArtifactSpell spell!!!!! = ", spillID)
    currentSpell = v1extra_herosSpells_1[spillID]
    if currentSpell then
      return currentSpell.EFFECT
    end
  else
    DebugOut("Use Spells!!!!! = ", spillID)
  end
  if not currentSpell then
    DebugOut("Use ArtifactSpell spell!!!!! = ", spillID)
    currentSpell = ArtifactSpell[spillID]
    if currentSpell then
      return currentSpell.EFFECT
    end
  else
    DebugOut("Use extral_ArtifactSpell !!!!! = ", spillID)
  end
  if USE_DEFAULT_EFFECT then
    local range = GameDataAccessHelper:GetSpellRealRangeType(spillID)
    return LowDeveiceSpellEffect[range]
  end
  if GameDataAccessHelper:CheckSPAttackEffectCanPlay(currentSpell.EFFECT) then
    return currentSpell.EFFECT
  else
    local spAttRange = GameDataAccessHelper:GetSpellRealRangeType(spillID)
    local spDefaultID = GameDataAccessHelper.m_defaultSPAttack[spAttRange]
    local defaultSpell
    if currentSpellTable then
      defaultSpell = currentSpellTable[defaultSpell]
    end
    defaultSpell = defaultSpell or Spells[spDefaultID]
    defaultSpell = defaultSpell or Spells[50]
    return defaultSpell.EFFECT
  end
  return currentSpell.EFFECT
end
function GameDataAccessHelper:GetSkillRangeType(id_skill)
  DebugOut("fuckGetSkillRangeType" .. id_skill)
  DebugTable(Spells[id_skill])
  local currentSpellTable = v1extra_herosSpells
  if immanentversion == 2 then
    currentSpellTable = v2extra_herosSpells
  end
  local currentSpell
  if currentSpellTable then
    currentSpell = currentSpellTable[id_skill]
  end
  currentSpell = currentSpell or Spells[id_skill]
  if not currentSpell then
    DebugOut("GetSkillRangeType Use extral_ArtifactSpell spell!!!!! = ", id_skill)
    currentSpell = v1extra_herosSpells_1[id_skill]
  end
  currentSpell = currentSpell or ArtifactSpell[id_skill]
  if currentSpell == nil then
    return 1
  end
  if currentSpell.range_type == 7 then
    return 1
  end
  return currentSpell.range_type
end
function GameDataAccessHelper:GetSpellRealRangeType(id_skill)
  DebugOut("fuckGetSkillRangeType" .. id_skill)
  DebugTable(Spells[id_skill])
  local currentSpellTable = v1extra_herosSpells
  if immanentversion == 2 then
    currentSpellTable = v2extra_herosSpells
  end
  local currentSpell
  if currentSpellTable then
    currentSpell = currentSpellTable[id_skill]
  end
  currentSpell = currentSpell or Spells[id_skill]
  if not currentSpell then
    DebugOut("GetSpellRealRangeType Use extral_ArtifactSpell spell!!!!! = ", id_skill)
    currentSpell = v1extra_herosSpells_1[id_skill]
  end
  currentSpell = currentSpell or ArtifactSpell[id_skill]
  return currentSpell.range_type
end
function GameDataAccessHelper:GetSkillIcon(id_skill)
  local currentSpellTable = v1extra_herosSpells
  if immanentversion == 2 then
    currentSpellTable = v2extra_herosSpells
  end
  local currentSpell
  if currentSpellTable then
    currentSpell = currentSpellTable[id_skill]
  end
  currentSpell = currentSpell or Spells[id_skill]
  if not currentSpell then
    DebugOut("GetSkillIcon Use extral_ArtifactSpell spell!!!!! = ", id_skill)
    currentSpell = v1extra_herosSpells_1[id_skill]
  end
  currentSpell = currentSpell or ArtifactSpell[id_skill]
  return ...
end
function GameDataAccessHelper:GetSkillInfo(id_skill)
  local currentSpellTable = v1extra_herosSpells
  if immanentversion == 2 then
    currentSpellTable = v2extra_herosSpells
  end
  local currentSpell
  if currentSpellTable then
    currentSpell = currentSpellTable[id_skill]
  end
  currentSpell = currentSpell or Spells[id_skill]
  if not currentSpell then
    DebugOut("GetSkillInfo Use extral_ArtifactSpell spell!!!!! = ", id_skill)
    currentSpell = v1extra_herosSpells_1[id_skill]
  end
  currentSpell = currentSpell or ArtifactSpell[id_skill]
  return currentSpell
end
function GameDataAccessHelper:GetFleetAvatar(fleetId, level, sex)
  if fleetId == 1 then
    if sex ~= nil then
      return ...
    end
    local user_info = GameGlobalData:GetUserInfo()
    return ...
  elseif fleetId == 3 then
    return (...), GameUtils, ...
  end
  local ability_table = GameDataAccessHelper:GetCommanderAbility(fleetId, level)
  assert(ability_table, "bad ability " .. tostring(fleetId) .. "  " .. tostring(level))
  if GameDataAccessHelper:IsHeadResNeedDownload(ability_table.AVATAR) then
    if self:CheckFleetHasAvataImage(ability_table.AVATAR) then
      return ability_table.AVATAR
    else
      return "head9001"
    end
  end
  return ability_table.AVATAR
end
function GameDataAccessHelper:GetFleetAvatarByAvateImage(avatar)
  if GameDataAccessHelper:IsHeadResNeedDownload(avatar) then
    if self:CheckFleetHasAvataImage(avatar) then
      return avatar
    else
      return "head9001"
    end
  end
  return avatar
end
function GameDataAccessHelper:CheckFleetHasAvataImage(avatar)
  if avatar then
    if not self:IsHeadResNeedDownload(avatar) then
      return true
    end
    local tmpAvatar = headFrameToHeadImageCovertor[avatar]
    local smallAvatar = avatar
    local middileAvatar = avatar
    local bigAvatar = avatar
    if tmpAvatar then
      smallAvatar = tmpAvatar.small
      middileAvatar = tmpAvatar.middle
      bigAvatar = tmpAvatar.big
    end
    if GameDataAccessHelper.HeadImageCache[avatar] then
      return true
    end
    DebugOut("smallAvatar = ", smallAvatar)
    DebugOut("middileAvatar = ", middileAvatar)
    DebugOut("bigAvatar = ", bigAvatar)
    local avataImageSmall = "data2/LAZY_LOAD_DOWN_avata_49_49_" .. smallAvatar .. ".png"
    local avataImageMiddle = "data2/LAZY_LOAD_DOWN_avata_100_120_" .. middileAvatar .. ".png"
    local avataImageBig = "data2/LAZY_LOAD_DOWN_avata_256_256_" .. bigAvatar .. ".png"
    local isSmallOk = false
    if ext.crc32.crc32(avataImageSmall) ~= nil and ext.crc32.crc32(avataImageSmall) ~= "" then
      isSmallOk = true
    else
      isSmallOk = AutoUpdateInBackground:IsHeroFileUpdatedToServer(avataImageSmall)
    end
    local isMiddleOk = false
    if ext.crc32.crc32(avataImageMiddle) ~= nil and ext.crc32.crc32(avataImageMiddle) ~= "" then
      isMiddleOk = true
    else
      isMiddleOk = AutoUpdateInBackground:IsHeroFileUpdatedToServer(avataImageMiddle)
    end
    local isBigOk = false
    if ext.crc32.crc32(avataImageBig) ~= nil and ext.crc32.crc32(avataImageBig) ~= "" then
      isBigOk = true
    else
      isBigOk = AutoUpdateInBackground:IsHeroFileUpdatedToServer(avataImageBig)
    end
    if AutoUpdate.isAndroidDevice then
      local avataImageBigAlpha = "data2/LAZY_LOAD_DOWN_avata_256_256_" .. bigAvatar .. "_alpha.pak"
      local isBigAlphaOk = false
      if ext.crc32.crc32(avataImageBigAlpha) ~= nil and ext.crc32.crc32(avataImageBigAlpha) ~= "" then
        isBigAlphaOk = true
      else
        isBigAlphaOk = AutoUpdateInBackground:IsHeroFileUpdatedToServer(avataImageBigAlpha)
      end
      if not isBigAlphaOk then
        return false
      end
    end
    if isSmallOk and isMiddleOk and isBigOk then
      DebugOut("down load finished!!!")
      GameDataAccessHelper.HeadImageCache[avatar] = true
      return true
    end
  end
  DebugOut("down load not finished!!!")
  return false
end
function GameDataAccessHelper:GetHead(fleetId, areaID, level)
  DebugOut("GetHead find head in ", fleetId, areaID)
  if fleetId == 1 then
    return ...
  elseif fleetId == 3 then
    return (...), GameUtils, ...
  else
    local ability_table = GameDataAccessHelper:GetCommanderAbility(fleetId, level)
    if ability_table then
      if GameDataAccessHelper:IsHeadResNeedDownload(ability_table.AVATAR) then
        if self:CheckFleetHasAvataImage(ability_table.AVATAR) then
          return ability_table.AVATAR
        else
          return "head9001"
        end
      end
      return ability_table.AVATAR
    else
      local avatar = GameDataAccessHelper:GetMonsterHeadImage(areaID, fleetId)
      if avatar then
        return avatar
      end
    end
  end
  assert(false)
  return nil
end
function GameDataAccessHelper:GetShip(fleetId, areaID, level, skipCheckDownload)
  DebugOut("GetShip find ship in ", fleetId, areaID, skipCheckDownload)
  skipCheckDownload = skipCheckDownload or false
  local shipImage
  local ability_table = GameDataAccessHelper:GetCommanderAbility(fleetId, level)
  if ability_table then
    shipImage = ability_table.SHIP
  elseif fleetId >= IntroduceHereMosterMinID and fleetId < IntroduceHereMosterMaxID then
    shipImage = IntroduceHereMoster[fleetId].Ship
  elseif fleetId >= InstanceIdSectionFloor and fleetId < InstanceIdSectionCeil then
    shipImage = GameDataAccessHelper:GetMonsterShipById(fleetId)
  elseif fleetId >= InstanceId2SectionFloor and fleetId < InstanceId2SectionCeil then
    shipImage = GameDataAccessHelper:GetMonsterShipById(fleetId)
  else
    shipImage = GameDataAccessHelper:GetMonsterVesselsImage(areaID, fleetId, skipCheckDownload)
  end
  DebugOut("shipImage = ", shipImage)
  if not skipCheckDownload and GameDataAccessHelper:IsShipResNeedDownload(shipImage) then
    if self:CheckHasShipImage(shipImage) then
      return shipImage
    else
      return "ship9001"
    end
  end
  if shipImage then
    return shipImage
  end
  DebugOut("Error Cannot find ship in ", fleetId, areaID)
  return "ship9001"
end
function GameDataAccessHelper:GetShipExits(ship, skipCheckDownload)
  if skipCheckDownload then
    return ship
  end
  if not self:CheckHasShipImage(ship) then
    return "ship9001"
  end
  return ship
end
function GameDataAccessHelper:GetHeadExits(avatar, skipCheckDownload)
  if skipCheckDownload then
    return avatar
  end
  if not self:CheckFleetHasAvataImage(avatar) then
    return "head9001"
  end
  return avatar
end
function GameDataAccessHelper:GetShipByShipImage(shipImage)
  if GameDataAccessHelper:IsShipResNeedDownload(shipImage) then
    if self:CheckHasShipImage(shipImage) then
      return shipImage
    else
      return "ship9001"
    end
  end
  return shipImage
end
function GameDataAccessHelper:CheckHasShipImage(ship)
  local shipIamge = "data2/LAZY_LOAD_DOWN_" .. ship .. ".png"
  local battleShipImage = "data2/LAZY_LOAD_DOWN_battle_" .. ship .. ".png"
  local isHurtShipSaved = false
  local hurtShip = shipIdToShipHurtImageCovertor[ship]
  if GameDataAccessHelper.ShipImageCache[ship] then
    return true
  end
  if hurtShip then
    if hurtShip ~= "false" then
      local battleHurtShipImage = "data2/LAZY_LOAD_DOWN_hurt_" .. hurtShip .. ".png"
      if ext.crc32.crc32(battleHurtShipImage) ~= "" and ext.crc32.crc32(battleHurtShipImage) ~= nil then
        isHurtShipSaved = true
      else
        isHurtShipSaved = AutoUpdateInBackground:IsHeroFileUpdatedToServer(battleHurtShipImage)
      end
    else
      isHurtShipSaved = true
    end
  else
    local battleHurtShipImage = "data2/LAZY_LOAD_DOWN_hurt_" .. ship .. ".png"
    if ext.crc32.crc32(battleHurtShipImage) ~= "" and ext.crc32.crc32(battleHurtShipImage) ~= nil then
      isHurtShipSaved = true
    else
      isHurtShipSaved = AutoUpdateInBackground:IsHeroFileUpdatedToServer(battleHurtShipImage)
    end
  end
  local isShipSaved = false
  if ext.crc32.crc32(shipIamge) ~= "" and ext.crc32.crc32(shipIamge) ~= nil then
    isShipSaved = true
  else
    isShipSaved = AutoUpdateInBackground:IsHeroFileUpdatedToServer(shipIamge)
  end
  local isBattleShipSaved = false
  if ext.crc32.crc32(battleShipImage) ~= "" and ext.crc32.crc32(battleShipImage) ~= nil then
    isBattleShipSaved = true
  else
    isBattleShipSaved = AutoUpdateInBackground:IsHeroFileUpdatedToServer(battleShipImage)
  end
  if isShipSaved and isBattleShipSaved and isHurtShipSaved then
    DebugOut("ship image = ", shipIamge)
    if ship == "ship83" then
      return ...
    end
    GameDataAccessHelper.ShipImageCache[ship] = true
    return true
  end
  DebugOut("has no ship image!!!!")
  return false
end
function GameDataAccessHelper:GetCommanderBasicInfo(identity, level)
  if level == nil or level < 0 then
    level = 0
  end
  local fleetKey = identity .. "_" .. level
  if GameDataAccessHelper.FleetsBasic[fleetKey] then
    return GameDataAccessHelper.FleetsBasic[fleetKey]
  end
  local currentBasicInfoTable = v1extra_herosbasic
  if immanentversion == 2 then
    currentBasicInfoTable = v2extra_herosbasic
  end
  if not currentBasicInfoTable then
    currentBasicInfoTable = GameData.heros.basic
  else
    DebugOut("every thing is ok for downloaded extra basic info!!!!")
  end
  for k, v in pairs(currentBasicInfoTable) do
    if v.ID == identity and level == v.LEVEL then
      GameDataAccessHelper.FleetsBasic[fleetKey] = v
      return v
    end
  end
  for k, v in pairs(GameData.heros.basic) do
    if v.ID == identity and level == v.LEVEL then
      GameDataAccessHelper.FleetsBasic[fleetKey] = v
      return v
    end
  end
  return nil
end
function GameDataAccessHelper:GetCommanderAbility(identity, level)
  DebugOut("GetCommanderAbility_2  " .. tostring(identity) .. " " .. tostring(level))
  assert(identity ~= nil)
  if level == nil or level < 0 then
    level = 0
  end
  local fleetKey = identity .. "_" .. level
  if GameDataAccessHelper.FleetAbility[fleetKey] then
    return GameDataAccessHelper.FleetAbility[fleetKey]
  end
  local currentAbilityTable = v1extra_herosAbility
  if immanentversion == 2 then
    currentAbilityTable = v2extra_herosAbility
  end
  if not currentAbilityTable then
    currentAbilityTable = GameData.heros.Ability
  else
    DebugOut("every thing is ok for downloaded extra ability!!!!")
  end
  for k, v in pairs(currentAbilityTable) do
    if v.ID == identity and level == v.LEVEL then
      GameDataAccessHelper.FleetAbility[fleetKey] = v
      return v
    end
  end
  for k, v in pairs(GameData.heros.Ability) do
    if v.ID == identity and level == v.LEVEL then
      GameDataAccessHelper.FleetAbility[fleetKey] = v
      return v
    end
  end
  DebugOut("ability dont find the id:", identity, level)
  return nil
end
function GameDataAccessHelper:GetCommanderVessels(identity, level)
  local vesselsTable = GameDataAccessHelper:GetCommanderBasicInfo(identity, level)
  return vesselsTable.vessels
end
function GameDataAccessHelper:GetCommanderName(index, level)
  local ability_table = GameDataAccessHelper:GetCommanderAbility(index, level)
  DebugOut("get ability for name display", ability_table)
  DebugOut("index = ", index)
  DebugOut("level = ", level)
  return ability_table.NAME
end
function GameDataAccessHelper:CreateLevelDisplayName(name, level)
  local commander_name = name
  level = level or 0
  local level_info = GameGlobalData:GetData("levelinfo")
  if level_info.level < 35 or level <= 0 then
    return commander_name
  end
  commander_name = commander_name .. " [+" .. GameDataAccessHelper:GetFleetLevelStr(level) .. "]"
  return commander_name
end
function GameDataAccessHelper:GetFleetLevelStr(level)
  local levelStr = "<level>"
  if level == nil or level == 0 then
    return "0"
  end
  if level < 16 then
    levelStr = string.gsub(levelStr, "<level>", level)
  elseif level == 16 then
    levelStr = "T"
  elseif level < 32 and level > 16 then
    levelStr = "T<level>"
    levelStr = string.gsub(levelStr, "<level>", level - 16)
  elseif level == 32 then
    levelStr = "U"
  else
    levelStr = "U<level>"
    levelStr = string.gsub(levelStr, "<level>", level - 32)
  end
  return levelStr
end
function GameDataAccessHelper:GetFleetLevelStrWithColor(level, color)
  local colorNum = FleetDataAccessHelper:GetFleetColorNumberByColor(color)
  local levelStr = "<font color='<c1>'>[</font><font color='#FF9900'>+<level></font><font color='<c1>'>]</font>"
  levelStr = string.gsub(levelStr, "<c1>", colorNum)
  levelStr = string.gsub(levelStr, "<level>", GameDataAccessHelper:GetFleetLevelStr(level))
  return levelStr
end
function GameDataAccessHelper:GetFleetLevelDisplayName(fleetId, level)
  local commander_name
  level = level or 0
  local showMain = false
  local leaderlist = GameGlobalData:GetData("leaderlist")
  if leaderlist and GameGlobalData.GlobalData.curMatrixIndex and leaderlist.leader_ids[GameGlobalData.GlobalData.curMatrixIndex] and leaderlist.leader_ids[GameGlobalData.GlobalData.curMatrixIndex] == fleetId then
    showMain = true
  end
  if fleetId == 1 or showMain then
    local userinfo = GameGlobalData:GetUserInfo()
    commander_name = GameUtils:GetUserDisplayName(userinfo.name)
  else
    commander_name = GameDataAccessHelper:GetCommanderName(fleetId, level)
    DebugOut("commander_name NPC = ", commander_name)
    commander_name = GameLoader:GetGameText("LC_NPC_" .. commander_name)
  end
  DebugOut("commander_name = ", commander_name)
  local level_info = GameGlobalData:GetData("levelinfo")
  if level_info.level < 35 or level <= 0 then
    return commander_name
  end
  commander_name = commander_name .. " [+" .. GameDataAccessHelper:GetFleetLevelStr(level) .. "]"
  return commander_name
end
function GameDataAccessHelper:GetCommanderVesselsName(index, level)
  DebugOut(index)
  local ability_table = GameDataAccessHelper:GetCommanderAbility(index, level)
  return ability_table.VESSELS_NAME
end
function GameDataAccessHelper:GetCommanderColor(index, level)
  if index == 0 then
    index = 1
  end
  local ability_table = GameDataAccessHelper:GetCommanderAbility(index, level)
  return ability_table.COLOR
end
function GameDataAccessHelper:GetCommanderColorFrame(index, level)
  local color = GameDataAccessHelper:GetCommanderColor(index, level)
  if color then
    return ColorToFrame[color]
  end
  DebugOut("no color!!!!!!!!!!!!")
  return "blue"
end
function GameDataAccessHelper:GetCommanderVesselsType(index, level)
  local basic_info = GameDataAccessHelper:GetCommanderBasicInfo(index, level)
  local vessels_type = "TYPE_" .. tostring(basic_info.vessels)
  return vessels_type
end
function GameDataAccessHelper:GetCommanderVesselsImage(commander_identity, level, skipCheckDownload)
  local ability_table = GameDataAccessHelper:GetCommanderAbility(commander_identity, level)
  skipCheckDownload = skipCheckDownload or false
  if not skipCheckDownload and GameDataAccessHelper:IsShipResNeedDownload(ability_table.SHIP) then
    if self:CheckHasShipImage(ability_table.SHIP) then
      return ability_table.SHIP
    else
      return "ship9001"
    end
  end
  return ability_table.SHIP
end
function GameDataAccessHelper:GetCommanderShipByDownloadState(ship)
  if GameDataAccessHelper:IsShipResNeedDownload(ship) then
    if self:CheckHasShipImage(ship) then
      return ship
    else
      return "ship9001"
    end
  end
  return ship
end
function GameDataAccessHelper:GetCommanderDesc(identity, level)
  local ability_table = GameDataAccessHelper:GetCommanderAbility(identity, level)
  return ...
end
function GameDataAccessHelper:GetMilitaryRankName(level)
  local name_text = GameLoader:GetGameText("LC_ACHIEVEMENT_PRESTIGE_" .. tostring(level))
  return name_text
end
local IntroduceHereMosterMaxID = 200000000
local IntroduceHereMosterMinID = 100000000
function GameDataAccessHelper:GetMonsterShip(index_monster, skipCheckDownload)
  DebugOut("tp:GetMonsterShip " .. index_monster)
  local ship = "ship9001"
  if index_monster >= IntroduceHereMosterMinID and index_monster < IntroduceHereMosterMaxID then
    ship = IntroduceHereMoster[index_monster].Ship
    DebugOut("index_monster >= IntroduceHereMosterMinID and index_monster < IntroduceHereMosterMaxID ")
  elseif index_monster >= InstanceIdSectionFloor and index_monster < InstanceIdSectionCeil then
    ship = GameDataAccessHelper:GetMonsterShipById(index_monster)
    DebugOut("index_monster >= InstanceIdSectionFloor and index_monster < InstanceIdSectionCeil")
  elseif index_monster >= InstanceId2SectionFloor and index_monster < InstanceId2SectionCeil then
    DebugOut("index_monster >= InstanceId2SectionFloor and index_monster < InstanceId2SectionCeil")
    ship = GameDataAccessHelper:GetMonsterShipById(index_monster)
  elseif index_monster >= MedalBossMinID and index_monster < MedalBossMaxID and MedalBossTable[index_monster] then
    ship = MedalBossTable[index_monster].Ship
  elseif WVEBossInfo[index_monster] then
    DebugOut("WVEBossInfo[index_monster]")
    ship = WVEBossInfo[index_monster].Ship
  elseif GameStateManager.GameStateBattleMap.m_currentAreaID and GameData["ChapterMonsterAct" .. GameStateManager.GameStateBattleMap.m_currentAreaID] and not GameStateManager.GameStateConfrontation.m_IsActive and GameStateManager.GameStateBattleMap.m_currentAreaID > 0 then
    DebugOut("m_currentAreaID")
    local tableName = "ChapterMonsterAct" .. GameStateManager.GameStateBattleMap.m_currentAreaID
    if GameData[tableName].Monsters[index_monster] == nil then
      DebugOut(string.format("\230\128\170\231\137\169\232\161\168[" .. tableName .. "]\228\184\173\230\178\161\230\156\137index=%d\231\154\132\230\128\170\231\137\169\239\188\129", index_monster))
      if WorldBossInfo[index_monster] then
        ship = WorldBossInfo[index_monster].Ship
      else
        ship = "ship9001"
      end
    elseif GameData[tableName].Monsters[index_monster].Ship then
      ship = GameData[tableName].Monsters[index_monster].Ship
    end
  elseif GameData.adventure_monster.info[index_monster] then
    DebugOut("GameData.adventure_monster.info[index_monster]")
    ship = GameData.adventure_monster.info[index_monster].Ship
  elseif ArcaneMonsterTable[index_monster] then
    DebugOut("ArcaneMonsterTable[index_monster]")
    ship = ArcaneMonsterTable[index_monster].Ship
  elseif WorldBossInfo[index_monster] then
    DebugOut("WorldBossInfo[index_monster]")
    ship = WorldBossInfo[index_monster].Ship
  else
    ship = "ship9001"
  end
  if not skipCheckDownload and GameDataAccessHelper:IsShipResNeedDownload(ship) and not self:CheckHasShipImage(ship) then
    ship = "ship9001"
  end
  return ship
end
function GameDataAccessHelper:GetMonsterHeadImage(area_id, monster_id)
  local headImage
  if monster_id >= IntroduceHereMosterMinID and monster_id < IntroduceHereMosterMaxID then
    headImage = IntroduceHereMoster[monster_id].Avatar
  elseif monster_id >= InstanceIdSectionFloor and monster_id < InstanceIdSectionCeil then
    headImage = GameDataAccessHelper:GetMonsterAvatarById(monster_id)
  elseif monster_id >= InstanceId2SectionFloor and monster_id < InstanceId2SectionCeil then
    headImage = GameDataAccessHelper:GetMonsterAvatarById(monster_id)
  elseif monster_id >= MedalBossMinID and monster_id < MedalBossMaxID and MedalBossTable[index_monster] then
    headImage = MedalBossTable[monster_id].Avatar
  elseif ArcaneMonsterTable[monster_id] then
    headImage = ArcaneMonsterTable[monster_id].Avatar
  elseif WorldBossInfo[monster_id] then
    headImage = WorldBossInfo[monster_id].Avatar
  elseif GameData.adventure_monster.info[monster_id] then
    headImage = GameData.adventure_monster.info[monster_id].Avatar
  elseif area_id and GameData["ChapterMonsterAct" .. area_id] then
    headImage = GameData["ChapterMonsterAct" .. area_id].Monsters[monster_id].Avatar
  else
    headImage = nil
  end
  if headImage and GameDataAccessHelper:IsHeadResNeedDownload(headImage) then
    if self:CheckFleetHasAvataImage(headImage) then
      return headImage
    else
      return "head9001"
    end
  end
  return headImage
end
function GameDataAccessHelper:GetMonsterVesselsImage(area_id, monster_id, skipCheckDownload)
  local ship
  if monster_id >= IntroduceHereMosterMinID and monster_id < IntroduceHereMosterMaxID then
    ship = IntroduceHereMoster[monster_id].Ship
  elseif monster_id >= InstanceIdSectionFloor and monster_id < InstanceIdSectionCeil then
    ship = GameDataAccessHelper:GetMonsterShipById(monster_id)
  elseif monster_id >= InstanceId2SectionFloor and monster_id < InstanceId2SectionCeil then
    ship = GameDataAccessHelper:GetMonsterShipById(monster_id)
  elseif monster_id >= MedalBossMinID and monster_id < MedalBossMaxID and MedalBossTable[index_monster] then
    ship = MedalBossTable[monster_id].Ship
  elseif ArcaneMonsterTable[monster_id] then
    ship = ArcaneMonsterTable[monster_id].Ship
  elseif WorldBossInfo[monster_id] then
    ship = WorldBossInfo[monster_id].Ship
  elseif GameData.adventure_monster.info[monster_id] then
    ship = GameData.adventure_monster.info[monster_id].Ship
  elseif area_id and GameData["ChapterMonsterAct" .. area_id] then
    ship = GameData["ChapterMonsterAct" .. area_id].Monsters[monster_id].Ship
  else
    ship = nil
  end
  if GameDataAccessHelper:IsShipResNeedDownload(ship) and not self:CheckHasShipImage(ship) then
    ship = "ship9001"
  end
  return ship
end
function GameDataAccessHelper:GetMonsterAvatar(index_act, index_monster)
  local table_monster
  if index_monster >= IntroduceHereMosterMinID and index_monster < IntroduceHereMosterMaxID then
    table_monster = IntroduceHereMoster
  elseif index_monster >= InstanceIdSectionFloor and index_monster < InstanceIdSectionCeil then
    local avatar = GameDataAccessHelper:GetMonsterAvatarById(index_monster)
    return avatar
  elseif index_monster >= InstanceId2SectionFloor and index_monster < InstanceId2SectionCeil then
    local avatar = GameDataAccessHelper:GetMonsterAvatarById(index_monster)
    return avatar
  elseif index_monster >= MedalBossMinID and index_monster < MedalBossMaxID and MedalBossTable[index_monster] then
    table_monster = MedalBossTable
  elseif WVEBossInfo[index_monster] then
    table_monster = WVEBossInfo
  elseif index_act and GameData["ChapterMonsterAct" .. index_act] and not GameStateManager.GameStateConfrontation.m_IsActive and index_act > 0 then
    if WorldBossInfo[index_monster] then
      table_monster = WorldBossInfo
    else
      table_monster = GameData["ChapterMonsterAct" .. index_act].Monsters
    end
  elseif GameData.adventure_monster.info[index_monster] then
    table_monster = GameData.adventure_monster.info
  elseif ArcaneMonsterTable[index_monster] then
    table_monster = ArcaneMonsterTable
  elseif WorldBossInfo[index_monster] then
    table_monster = WorldBossInfo
  else
    DebugOut("GetMonsterAvatar error " .. index_monster)
    return nil
  end
  if table_monster and table_monster[index_monster] and GameDataAccessHelper:IsHeadResNeedDownload(table_monster[index_monster].Avatar) then
    if self:CheckFleetHasAvataImage(table_monster[index_monster].Avatar) then
      DebugOut("google")
      return table_monster[index_monster].Avatar
    else
      DebugOut("baidu")
      return "head9001"
    end
  end
  return table_monster[index_monster].Avatar
end
function GameDataAccessHelper:GetMonsterDurability(act_id, monster_id)
  if monster_id >= IntroduceHereMosterMinID and monster_id < IntroduceHereMosterMaxID then
    return IntroduceHereMoster[monster_id].durability
  elseif monster_id >= InstanceIdSectionFloor and monster_id < InstanceIdSectionCeil then
    return ...
  elseif monster_id >= InstanceId2SectionFloor and monster_id < InstanceId2SectionCeil then
    return ...
  elseif act_id and GameData["ChapterMonsterAct" .. act_id] then
    local table_monster = GameData["ChapterMonsterAct" .. act_id].Monsters
    return table_monster[monster_id].durability
  elseif monster_id >= MedalBossMinID and monster_id < MedalBossMaxID and MedalBossTable[index_monster] then
    return MedalBossTable[monster_id].durability
  elseif GameData.adventure_monster.info[monster_id] then
    local table_monster = GameData.adventure_monster.info
    return table_monster[monster_id].durability
  elseif ArcaneMonsterTable[monster_id] then
    return ArcaneMonsterTable[monster_id].durability
  elseif WorldBossInfo[monster_id] then
    return WorldBossInfo[monster_id].durability
  else
    return nil
  end
end
function GameDataAccessHelper:GetMonsterInfo(area_id, monster_id)
  local monsterInfo
  if monster_id >= IntroduceHereMosterMinID and monster_id < IntroduceHereMosterMaxID then
    monsterInfo = IntroduceHereMoster[monster_id]
  elseif monster_id >= InstanceIdSectionFloor and monster_id < InstanceIdSectionCeil then
    monsterInfo = GameDataAccessHelper:GetMonsterInfoById(monster_id)
  elseif monster_id >= InstanceId2SectionFloor and monster_id < InstanceId2SectionCeil then
    monsterInfo = GameDataAccessHelper:GetMonsterInfoById(monster_id)
  elseif monster_id >= MedalBossMinID and monster_id < MedalBossMaxID and MedalBossTable[index_monster] then
    monsterInfo = MedalBossTable[monster_id]
  elseif area_id and GameData["ChapterMonsterAct" .. area_id] then
    local table_monster = GameData["ChapterMonsterAct" .. area_id].Monsters
    monsterInfo = table_monster[monster_id]
  elseif GameData.adventure_monster.info[monster_id] then
    local table_monster = GameData.adventure_monster.info
    monsterInfo = table_monster[monster_id]
  elseif ArcaneMonsterTable[monster_id] then
    monsterInfo = ArcaneMonsterTable[monster_id]
  elseif WorldBossInfo[monster_id] then
    monsterInfo = WorldBossInfo[monster_id]
  else
    monsterInfo = nil
  end
  local newInfo = LuaUtils:table_rcopy(monsterInfo)
  if monsterInfo and monsterInfo.Ship and GameDataAccessHelper:IsShipResNeedDownload(monsterInfo.Ship) and not self:CheckHasShipImage(monsterInfo.Ship) then
    newInfo.Ship = "ship9001"
  end
  if monsterInfo and monsterInfo.Avatar and GameDataAccessHelper:IsHeadResNeedDownload(monsterInfo.Avatar) then
    if self:CheckFleetHasAvataImage(monsterInfo.Avatar) then
      return newInfo
    else
      newInfo.Avatar = "head9001"
      return newInfo
    end
  end
  return newInfo
end
function GameDataAccessHelper:GetAdventureCountAndBattleIDList()
  local count = 0
  local battleIDList = {}
  for i, v in pairs(Adventure) do
    count = count + 1
    battleIDList[count] = i
  end
  table.sort(battleIDList)
  return count, battleIDList
end
function GameDataAccessHelper:GetAdventureLevel(act)
  local adventure_id = act
  return Adventure[adventure_id].LV
end
function GameDataAccessHelper:GetAdventureIcon(act)
  local adventure_id = act
  if Adventure[adventure_id].HEAD and GameDataAccessHelper:IsHeadResNeedDownload(Adventure[adventure_id].HEAD) then
    if self:CheckFleetHasAvataImage(Adventure[adventure_id].HEAD) then
      return Adventure[adventure_id].HEAD
    else
      return "head9001"
    end
  end
  return Adventure[adventure_id].HEAD
end
function GameDataAccessHelper:GetAdventureForce(act)
  local adventure_id = act
  return Adventure[adventure_id].force
end
function GameDataAccessHelper:GetAdventureShip(act)
  local adventure_id = act
  if Adventure[adventure_id].Ship then
    if GameDataAccessHelper:IsShipResNeedDownload(Adventure[adventure_id].Ship) then
      if self:CheckHasShipImage(Adventure[adventure_id].Ship) then
        return Adventure[adventure_id].Ship
      else
        return "ship9001"
      end
    end
    return Adventure[adventure_id].Ship
  end
  return "ship9001"
end
function GameDataAccessHelper:GetAdventureWinDiologId(act)
  local adventure_id = act
  return Adventure[adventure_id].AFTER_STORY
end
function GameDataAccessHelper:GetAdventureFailedDialogId(act)
  local adventure_id = act
  return Adventure[adventure_id].FAILED_STORY
end
function GameDataAccessHelper:GetAdventureSceneShot(act)
  local adventure_id = act
  return Adventure[adventure_id].SceneShot
end
function GameDataAccessHelper:GetBattleInfo(act, battleID)
  DebugOut("GetBattleInfo ", act, battleID)
  local table_act, table_battles
  if GameData["chapterdata_act" .. tostring(act)] then
    table_act = GameData["chapterdata_act" .. tostring(act)]
    if immanentversion170 == 4 or immanentversion170 == 5 then
      table_battles = table_act.Battles_170
    else
      table_battles = table_act.Battles
    end
  elseif GameData.adventure.adventure[battleID] then
    table_act = GameData.adventure
    table_battles = table_act.adventure
  else
    return nil
  end
  return table_battles[battleID]
end
function GameDataAccessHelper:GetBattleRank(act, battleID)
  DebugOut("GetBattleRank ", act, battleID)
  local table_act, table_battles
  if GameData["chapterdata_act" .. tostring(act)] then
    table_act = GameData["chapterdata_act" .. tostring(act)]
    if immanentversion170 == 4 or immanentversion170 == 5 then
      table_battles = table_act.Battles_170
    else
      table_battles = table_act.Battles
    end
  else
    return nil
  end
  return table_battles[battleID].RANK
end
function GameDataAccessHelper:GetBattleEnemyType(act, battleID)
  DebugOut("GetBattleEnemyType ", act, " ", battleID)
  return self:GetBattleInfo(act, battleID).EnemyType
end
function GameDataAccessHelper:GetBattleMonsterInfo(act, battleID)
  local table_act, table_battles
  if act == 52 then
    local battleData = ArcaneBattleTable[battleID]
    local battleInfo = {}
    battleInfo._id = battleData.id
    battleInfo._avatar = battleData.Head
    if GameDataAccessHelper:IsHeadResNeedDownload(battleInfo._avatar) and not GameDataAccessHelper:CheckFleetHasAvataImage(battleInfo._avatar) then
      battleInfo._avatar = "head9001"
    end
    battleInfo._level = battleData.level
    battleInfo._image = battleData.MonsterImage
    return battleInfo
  end
  if GameData["chapterdata_act" .. tostring(act)] then
    table_act = GameData["chapterdata_act" .. tostring(act)]
    if immanentversion170 == 4 or immanentversion170 == 5 then
      table_battles = table_act.Battles_170
    else
      table_battles = table_act.Battles
    end
  elseif GameData.adventure.adventure[battleID] then
    table_act = GameData.adventure
    table_battles = table_act.adventure
  else
    assert(false)
  end
  local data_battle = table_battles[battleID]
  if data_battle then
    local head = data_battle.HEAD
    if GameDataAccessHelper:IsHeadResNeedDownload(head) and not GameDataAccessHelper:CheckFleetHasAvataImage(head) then
      head = "head9001"
    end
    return {
      _id = battleID,
      _avatar = head,
      _level = data_battle.LEVEL,
      _image = data_battle.MonsterImage
    }
  end
  return nil
end
function GameDataAccessHelper:GetBattleFailedStory(act, battleID)
  if GameData["chapterdata_act" .. tostring(act)] then
    local table_act = GameData["chapterdata_act" .. tostring(act)]
    local table_battles
    if immanentversion170 == 4 or immanentversion170 == 5 then
      table_battles = table_act.Battles_170
    else
      table_battles = table_act.Battles
    end
    local data_battle = table_battles[battleID]
    if data_battle and data_battle.FAILED_STORY then
      return data_battle.FAILED_STORY
    end
  end
  return {}
end
function GameDataAccessHelper:GetBattleBeforeStory(act, battleID)
  DebugOut("GetBattleBeforeStory ", act, battleID)
  local table_act = GameData["chapterdata_act" .. tostring(act)]
  local table_battles
  if immanentversion170 == 4 or immanentversion170 == 5 then
    table_battles = table_act.Battles_170
  else
    table_battles = table_act.Battles
  end
  local data_battle = table_battles[battleID]
  if data_battle and data_battle.BEFORE_STORY then
    return data_battle.BEFORE_STORY
  end
  return {}
end
function GameDataAccessHelper:GetBattleAfterStory(act, battleID)
  local table_act = GameData["chapterdata_act" .. tostring(act)]
  local table_battles = table_act.Battles
  if immanentversion170 == 4 or immanentversion170 == 5 then
    table_battles = table_act.Battles_170
  else
    table_battles = table_act.Battles
  end
  local data_battle = table_battles[battleID]
  if data_battle and data_battle.AFTER_STORY then
    return data_battle.AFTER_STORY
  end
  return {}
end
function GameDataAccessHelper:GetSectionBeforeFinishStory(areaID, sectionID)
  local table_act = GameData["chapterdata_act" .. tostring(areaID)]
  local table_section = table_act.ChapterInfos
  if immanentversion170 == 4 or immanentversion170 == 5 then
    table_section = table_act.ChapterInfos_170
  else
    table_section = table_act.ChapterInfos
  end
  local data_section = table_section[sectionID]
  if data_section and data_section.BEFORE_FINISH then
    return data_section.BEFORE_FINISH
  end
  return {}
end
function GameDataAccessHelper:GetSectionAfterFinishStory(areaID, sectionID)
  local table_act = GameData["chapterdata_act" .. tostring(areaID)]
  local table_section = table_act.ChapterInfos
  if immanentversion170 == 4 or immanentversion170 == 5 then
    table_section = table_act.ChapterInfos_170
  else
    table_section = table_act.ChapterInfos
  end
  local data_section = table_section[sectionID]
  if data_section and data_section.AFTER_FINISH then
    return data_section.AFTER_FINISH
  end
  return {}
end
function GameDataAccessHelper:GetEventType(area_id, battle_id)
  local table_act = GameData["chapterdata_act" .. tostring(area_id)]
  local table_battles = table_act.Battles
  if immanentversion170 == 4 or immanentversion170 == 5 then
    table_battles = table_act.Battles_170
  else
    table_battles = table_act.Battles
  end
  return table_battles[battle_id].EVENT_TYPE
end
function GameDataAccessHelper:GetTechName(index)
  return ...
end
function GameDataAccessHelper:GetTechIcon(index)
  return Technologies[index].IconName
end
function GameDataAccessHelper:GetTechDesc(index)
  return Technologies[index].TechDesc
end
function GameDataAccessHelper:GetEquipEvolution(itemtype)
  if not evolution[itemtype] then
    return -1
  end
  return evolution[itemtype].EQUIP_RECIPE
end
function GameDataAccessHelper:GetEquipSlot(itemtype)
  return Equip[itemtype].EQUIP_SLOT
end
function GameDataAccessHelper:GetEquipPrice(itemtype)
  if not Equip[itemtype] then
    return -1
  end
  return Equip[itemtype].PRICE
end
function GameDataAccessHelper:GetItemPrice(itemtype)
  if not Item[itemtype] then
    return -1
  end
  return Item[itemtype].PRICE
end
function GameDataAccessHelper:GetItemReqLevel(itemtype)
  if not Item[itemtype] then
    return -1
  end
  return Item[itemtype].REQ_LEVEL
end
function GameDataAccessHelper:GetItemMaxLevel(itemtype)
  if not Item[itemtype] then
    return -1
  end
  DebugOutPutTable(Item[itemtype], "GameDataAccessHelper:GetItemMaxLevel")
  return Item[itemtype].MAX_LEVEL
end
function GameDataAccessHelper:GetItemDescText(itemtype)
  local retText = GameLoader:GetGameText("LC_ITEM_ITEM_DESC_" .. itemtype)
  if retText == "" then
    if Item[itemtype] then
      retText = Item[itemtype].ITEM_DESC
    else
      retText = "not define!"
    end
  end
  return retText
end
function GameDataAccessHelper:GetItemNameText(itemtype)
  local retText = GameLoader:GetGameText("LC_ITEM_ITEM_NAME_" .. itemtype)
  if retText == "" then
    if Item[itemtype] then
      retText = Item[itemtype].ITEM_NAME
    else
      retText = "not define!"
    end
  end
  return retText
end
function GameDataAccessHelper:IsItemUsedRename(itemtype)
  if itemtype == 99 then
    return true
  end
  if not Item[itemtype] then
    return false
  end
  return Item[itemtype].SP_TYPE == 11
end
function GameDataAccessHelper:IsItemCanUse(itemtype)
  if not Item[itemtype] then
    return false
  end
  return Item[itemtype].SP_TYPE > 0 and Item[itemtype].SP_TYPE <= 100
end
function GameDataAccessHelper:IsExpProps(itemtype)
  DebugOut("GameDataAccessHelper:IsExpProps " .. tostring(itemtype))
  if GameData.Item.Keys[itemtype] then
    DebugOut("IsExpProps has key")
    DebugTable(GameData.Item.Keys[itemtype])
  end
  if GameData.Item.Keys[itemtype] and GameData.Item.Keys[itemtype].SP_TYPE == 50 then
    return true
  end
  return false
end
function GameDataAccessHelper:IsItemIsDNA(itemtype)
  if not Item[itemtype] then
    return false
  end
  return Item[itemtype].SP_TYPE == 4
end
function GameDataAccessHelper:IsItemUsedStrengthen(itemtype)
  if not Item[itemtype] then
    return false
  end
  return Item[itemtype].SP_TYPE == 101
end
function GameDataAccessHelper:GetStrengthenMaterialInfo(StrengthenItemType, EquipStrengthenLv, PlayerLevel)
  local retStrengthenData = {}
  local retStrengthenAddition = 0
  local strenData
  if immanentversion170 == 4 or immanentversion170 == 5 then
    strenData = StrengthenData_170
  else
    strenData = StrengthenData
  end
  if strenData[EquipStrengthenLv + 1] and strenData[EquipStrengthenLv + 1].level == EquipStrengthenLv and strenData[EquipStrengthenLv + 1].item_id == StrengthenItemType then
    retStrengthenData = strenData[EquipStrengthenLv + 1]
  else
    for k, v in pairs(strenData) do
      if v.level == EquipStrengthenLv and v.item_id == StrengthenItemType then
        retStrengthenData = v
        break
      end
    end
  end
  local tempAdditions = {}
  local tempMinLevel = 0
  for k, v in pairs(StrengthenExtData) do
    if PlayerLevel > v.player_level then
      if tempMinLevel < v.player_level then
        tempMinLevel = v.player_level
        tempAdditions = {}
      end
      table.insert(tempAdditions, v)
    end
  end
  tempMinLevel = 0
  for k, v in pairs(tempAdditions) do
    if StrengthenItemType > v.enchant_level and tempMinLevel <= v.enchant_level then
      tempMinLevel = v.enchant_level
      retStrengthenAddition = v.addition
    end
  end
  return retStrengthenData, retStrengthenAddition
end
function GameDataAccessHelper:GetEquipReqLevel(itemtype)
  return Equip[itemtype].EQUIP_LEVEL_REQ
end
function GameDataAccessHelper:GetEquipReqFleet(itemtype)
  return Equip[itemtype].EQUIP_FLEET_REQ
end
function GameDataAccessHelper:GetEquipParam(itemtype)
  return Equip[itemtype].EQUIP_PARAM1
end
function GameDataAccessHelper:GetEquipValue(itemtype)
  return Equip[itemtype].EQUIP_VAL1
end
function GameDataAccessHelper:GetEquipParam2(itemtype)
  return Equip[itemtype].EQUIP_PARAM2
end
function GameDataAccessHelper:GetEquipValue2(itemtype)
  return Equip[itemtype].EQUIP_VAL2
end
function GameDataAccessHelper:GetEquipParams(itemtype)
  DebugOut("GameDataAccessHelper:GetEquipParams:", itemtype)
  local EquipParams = {}
  if Equip[itemtype].EQUIP_PARAM1 ~= 0 then
    table.insert(EquipParams, {
      T = Equip[itemtype].EQUIP_PARAM1,
      V = Equip[itemtype].EQUIP_VAL1
    })
  end
  if Equip[itemtype].EQUIP_PARAM2 ~= 0 then
    table.insert(EquipParams, {
      T = Equip[itemtype].EQUIP_PARAM2,
      V = Equip[itemtype].EQUIP_VAL2
    })
  end
  return EquipParams
end
function GameDataAccessHelper:isChoosableItem(itemType)
  local choosableitemidmax = 1
  local choosableitemidmin = 1
  if GameData.Item.Keys[itemType] and GameData.Item.Keys[itemType].SP_TYPE == 102 then
    return true
  else
    return false
  end
end
function GameDataAccessHelper:IsEquipment(itemId)
  return #itemId > 0 and itemId ~= "0"
end
function GameDataAccessHelper:GetItemRealType(item)
  if item.item_type > 100000 and item.item_type < 110000 then
    local equipments = GameGlobalData:GetData("equipments")
    local currentEquipInfo = LuaUtils:table_values(LuaUtils:table_filter(equipments, function(k, v)
      return v.equip_id == item.item_id
    end))[1] or {}
    return currentEquipInfo.equip_type or item.item_type
  else
    return item.item_type
  end
end
function GameDataAccessHelper:GetItemTitleType(itemtype)
  if itemtype > 100000 and itemtype < 110000 then
    return Equip[itemtype].TITLE_TYPE
  else
    if not Item[itemtype] then
      return -1
    end
    return Item[itemtype].TITLE_TYPE
  end
end
function GameDataAccessHelper:GetItemSubType(itemtype)
  if itemtype > 100000 and itemtype < 110000 then
    return Equip[itemtype].SUB_TYPE
  else
    if not Item[itemtype] then
      return -1
    end
    return Item[itemtype].SUB_TYPE
  end
end
function GameDataAccessHelper:GetEquipShopTypeTable()
  local t = LuaUtils:table_keys(LuaUtils:table_filter(Equip, function(k, v)
    return v.EQUIP_LEVEL_REQ == 1
  end))
  table.sort(t, function(v1, v2)
    return v1 < v2
  end)
  return t
end
function GameDataAccessHelper:GetEquipSlotShopType(slot, fleetIdentity, fleetLevel)
  local shopItemTypes = GameDataAccessHelper:GetEquipShopTypeTable()
  local shopItemType = -1
  for k, v in pairs(shopItemTypes) do
    if GameDataAccessHelper:GetEquipSlot(v) == slot then
      if GameDataAccessHelper:GetEquipReqFleet(v) == GameDataAccessHelper.ATK_TYPE_energy and GameDataAccessHelper:IsEnergyAttack(fleetIdentity, fleetLevel) then
        shopItemType = v
      elseif GameDataAccessHelper:GetEquipReqFleet(v) == GameDataAccessHelper.ATK_TYPE_physics and not GameDataAccessHelper:IsEnergyAttack(fleetIdentity, fleetLevel) then
        shopItemType = v
      elseif GameDataAccessHelper:GetEquipReqFleet(v) == GameDataAccessHelper.ATK_TYPE_common then
        shopItemType = v
      end
    end
  end
  return shopItemType
end
function GameDataAccessHelper:GetAchievementClassifyInfo(classify)
  local data_classify = AchievementClassifyTable[classify]
  local text_name = GameLoader:GetGameText("LC_MENU_Achievement_Classify_" .. data_classify.Name)
  return {
    _name = text_name,
    _icon = data_classify.IconID
  }
end
function GameDataAccessHelper:GetSectionInfo(id_area, id_section)
  local table_act = GameData["chapterdata_act" .. tostring(id_area)]
  local table_section
  if immanentversion170 == 4 or immanentversion170 == 5 then
    table_section = table_act.ChapterInfos_170
  else
    table_section = table_act.ChapterInfos
  end
  local table_section = table_section[id_section]
  return {
    _icon = table_section.ICON,
    _mapindex = table_section.MapIndex,
    _bosspos = table_section.BossPos,
    _entrostory = table_section.EntroStory
  }
end
function GameDataAccessHelper:IsEnergyAttack(id, level)
  local commander_basic = GameDataAccessHelper:GetCommanderBasicInfo(id, level)
  return commander_basic.ATK_TYPE == self.ATK_TYPE_energy
end
function GameDataAccessHelper:IsEnergyAttackByServerData(id)
  local fleetDisplayInfo = GameGlobalData:GetFleetDisplayerInfo(id)
  return fleetDisplayInfo.atk_type == self.ATK_TYPE_energy
end
function GameDataAccessHelper:GetSectionType(areaId)
  return Section[areaId].sectionType
end
function GameDataAccessHelper:GetSectionDesc(areaID, sectionID)
  return ...
end
function GameDataAccessHelper:GetMainQuestDesText(quest_id)
  if immanentversion == 1 then
    if immanentversion170 == 4 or immanentversion170 == 5 then
      return ...
    else
      return ...
    end
  elseif immanentversion == 2 then
    return ...
  end
end
function GameDataAccessHelper:GetMainQuestTitileText(quest_id)
  if immanentversion == 1 then
    if immanentversion170 == 4 or immanentversion170 == 5 then
      return ...
    else
      return ...
    end
  elseif immanentversion == 2 then
    return ...
  end
end
function GameDataAccessHelper:GetWDQuestTitleText(quest_id)
  if immanentversion170 == 4 or immanentversion170 == 5 then
    return ...
  else
    return ...
  end
end
function GameDataAccessHelper:GetWDQuestDescText(quest_id)
  if immanentversion170 == 4 or immanentversion170 == 5 then
    return ...
  else
    return ...
  end
end
function GameDataAccessHelper:GetSkillNameText(skill_id)
  if immanentversion == 1 then
    return ...
  elseif immanentversion == 2 then
    return ...
  end
end
function GameDataAccessHelper:GetSkillDesText(skill_id)
  if immanentversion == 1 then
    return ...
  elseif immanentversion == 2 then
    return ...
  end
end
function GameDataAccessHelper:GetAdjutantSkillNameText(skill_id)
  if immanentversion == 1 then
    return ...
  elseif immanentversion == 2 then
    return ...
  end
end
function GameDataAccessHelper:GetAdjutantSkillDesText(skill_id)
  if immanentversion == 1 then
    return ...
  elseif immanentversion == 2 then
    return ...
  end
end
function GameDataAccessHelper:GetAreaNameText(actID, battleID)
  assert(type(actID) == "number")
  assert(type(battleID) == "number")
  if actID == 52 then
    return ...
  elseif actID >= 1 and actID <= 30 or actID == 60 then
    if immanentversion == 1 then
      local textName = string.format("LC_BATTLE_AREA%d_%d_NAME", actID, battleID)
      return ...
    elseif immanentversion == 2 then
      local textName = string.format("LC_NEWBATTLE_AREA%d_%d_NAME", actID, battleID)
      return ...
    end
  else
    local textName = string.format("LC_BATTLE_AREA%d_%d_NAME", actID, battleID)
    return ...
  end
end
function GameDataAccessHelper:GetAreaDesText(actID, battleID)
  assert(type(actID) == "number")
  assert(type(battleID) == "number")
  if actID >= 1 and actID <= 30 or actID == 60 then
    if immanentversion == 1 then
      local textName = string.format("LC_BATTLE_AREA%d_%d_DESC", actID, battleID)
      return ...
    elseif immanentversion == 2 then
      local textName = string.format("LC_NEWBATTLE_AREA%d_%d_DESC", actID, battleID)
      return ...
    end
  else
    local textName = string.format("LC_BATTLE_AREA%d_%d_DESC", actID, battleID)
    return ...
  end
end
function GameDataAccessHelper:GetMaxArea()
  local count = 0
  for i, v in pairs(GameData.section.section) do
    count = count + 1
  end
  return count
end
function GameDataAccessHelper:GetBossHead(areaId)
  return Section[areaId].bossHead
end
function GameDataAccessHelper:GetMaxSection(areaId)
  DebugOut("areaid:" .. areaId)
  return Section[areaId].sectionCount
end
function GameDataAccessHelper:GetVipExp()
  return vip
end
function GameDataAccessHelper:GetKryptonUnLockSlot()
  return slotLevel
end
function GameDataAccessHelper:GetRightMenu()
  if immanentversion170 == 4 or immanentversion170 == 5 then
    return rightMenu_170
  end
  return rightMenu
end
function GameDataAccessHelper:GetRightMenuPlayerReqLevel(path)
  local reqLevel = 1
  if immanentversion170 == 4 or immanentversion170 == 5 then
    if rightMenu_170[path] and rightMenu_170[path].PLAYER_REQ_LEVEL then
      reqLevel = rightMenu_170[path].PLAYER_REQ_LEVEL
    end
  elseif rightMenu[path] and rightMenu[path].PLAYER_REQ_LEVEL then
    reqLevel = rightMenu[path].PLAYER_REQ_LEVEL
  end
  return reqLevel
end
function GameDataAccessHelper:GetRightMenuBuildingName(path)
  local buildingName = ""
  if immanentversion170 == 4 or immanentversion170 == 5 then
    if rightMenu_170[path] and rightMenu_170[path].BUILD_REQ then
      buildingName = rightMenu_170[path].BUILD_REQ
    end
  elseif rightMenu[path] and rightMenu[path].BUILD_REQ then
    buildingName = rightMenu[path].BUILD_REQ
  end
  return buildingName
end
function GameDataAccessHelper:GetRightMenuBuildingReqLevel(path)
  local buildingReqLevel = 1
  if immanentversion170 == 4 or immanentversion170 == 5 then
    if rightMenu_170[path] and rightMenu_170[path].BUILD_REQ_LEVEL then
      buildingReqLevel = rightMenu_170[path].BUILD_REQ_LEVEL
    end
  elseif rightMenu[path] and rightMenu[path].BUILD_REQ_LEVEL then
    buildingReqLevel = rightMenu[path].BUILD_REQ_LEVEL
  end
  return buildingReqLevel
end
function GameDataAccessHelper:GetHelpItemTable()
  return GameData.help_info.Data
end
function GameDataAccessHelper:GetHelpItemTable_new(index)
  local ret = {}
  for k, v in ipairs(GameData.help_info.Data) do
    if v.sortID == index then
      table.insert(ret, v)
    end
  end
  return ret
end
function GameDataAccessHelper:GetHelpItemTitle(index_item)
  return ...
end
function GameDataAccessHelper:GetHelpItemContent(index_item)
  return ...
end
function GameDataAccessHelper:GetCensorWords()
  return GameData.censor_words.Censor
end
function GameDataAccessHelper:GetAllianceActivityInfo(activity_id)
  return GameData.alliance.activity[activity_id]
end
function GameDataAccessHelper:GetAllianceDefendInfo(level)
  return GameData.alliance.repair[level + 1]
end
function GameDataAccessHelper:GetLabaAwardType(type_id)
  return GameData.Dexter_laba.LABA_TYPE[type_id].name
end
function GameDataAccessHelper:GetMilitaryRankInfoWithPrestige(prestigeValue)
  local activeRankInfo
  for _, rankInfo in ipairs(PrestigeTable) do
    if prestigeValue < rankInfo.prestige and (not activeRankInfo or rankInfo.prestige <= activeRankInfo.prestige) then
      activeRankInfo = rankInfo
    end
  end
  return activeRankInfo
end
function GameDataAccessHelper:GenerateMilitaryRankAwardInfo(rank)
  local awardTable = {}
  local awardDetail = {}
  local prevRankInfo, checkRankInfo
  for indexItem, rankInfo in ipairs(PrestigeTable) do
    if rankInfo.Prestige_Level == rank then
      checkRankInfo = rankInfo
      prevRankInfo = PrestigeTable[indexItem - 1]
    end
  end
  if checkRankInfo.new_matrix_cell > 0 then
    table.insert(awardTable, "unlock_formation_" .. tostring(checkRankInfo.new_matrix_cell))
    table.insert(awardDetail, {
      "unlock_formation",
      checkRankInfo.new_matrix_cell
    })
  end
  if prevRankInfo ~= nil and checkRankInfo.fight_fleet_cnt > prevRankInfo.fight_fleet_cnt then
    table.insert(awardTable, "recruit_count_up")
    table.insert(awardDetail, {
      "recruit_count_up",
      checkRankInfo.fight_fleet_cnt
    })
  end
  if 0 < checkRankInfo.spell then
    table.insert(awardTable, "skill_" .. tostring(checkRankInfo.spell))
    table.insert(awardDetail, {
      "skill",
      checkRankInfo.spell
    })
  end
  while #awardTable > 3 do
    table.remove(awardTable, #awardTable)
    table.remove(awardDetail, #awardDetail)
  end
  while #awardTable < 3 do
    table.insert(awardTable, "empty")
    table.insert(awardDetail, {"empty", 0})
  end
  local awardDataString = table.concat(awardTable, ",")
  DebugOut("awardDataString:", awardDataString)
  return awardDataString, awardDetail
end
function GameDataAccessHelper:GenerateMilitaryRankDailyAwardInfo(rank)
  local dailyAward = {}
  for indexItem, rankInfo in ipairs(PrestigeTable) do
    if rankInfo.Prestige_Level == rank then
      local award = string.gsub(rankInfo.award, "%[", "%{")
      award = string.gsub(award, "%]", "%}")
      local fun = loadstring([[
 local money='money'
 local technique='technique'
 return ]] .. award)
      dailyAward = fun()
      break
    end
  end
  local dailyAwardDataString = ""
  for i, v in ipairs(dailyAward) do
    dailyAwardDataString = dailyAwardDataString .. table.concat(v, "=") .. "\001"
  end
  return dailyAwardDataString
end
function GameDataAccessHelper:GetAwardSkill(rank)
  local RankInfo = PrestigeTable[rank]
  if RankInfo.spell > 0 then
    return "skill_" .. tostring(RankInfo.spell)
  end
  return nil
end
function GameDataAccessHelper:GetNewmatrixcell(rank)
  local RankInfo = PrestigeTable[rank]
  if RankInfo.new_matrix_cell > 0 then
    return true
  end
  return false
end
function GameDataAccessHelper:GetFightfleetcnt(rank)
  local RankInfo = PrestigeTable[rank]
  local preRankInfo = PrestigeTable[rank - 1]
  if RankInfo and preRankInfo and RankInfo.fight_fleet_cnt > preRankInfo.fight_fleet_cnt then
    return true
  end
  return false
end
function GameDataAccessHelper:GetNextPrestigeLevelInfo(rank)
  DebugTable(PrestigeTable)
  local curValue = PrestigeTable[rank]
  for key, value in ipairs(PrestigeTable) do
    if rank < value.Prestige_Level and (value.spell > 0 or 0 < value.new_matrix_cell or value.fight_fleet_cnt > curValue.fight_fleet_cnt) then
      return value.Prestige_Level
    end
  end
  return nil
end
function GameDataAccessHelper:GetArcaneEnhanceInfoWithEnhanceLevel(enhanceLevel)
  local enhanceData = ArcaneEnhanceTable[enhanceLevel]
  if enhanceData then
    local enhanceInfo = {}
    enhanceInfo.level = enhanceData.enhance_level
    enhanceInfo.conditionLevel = enhanceData.level_req
    enhanceInfo.conditionEnergy = enhanceData.ac_energy_req
    enhanceInfo.costTechnique = enhanceData.technique_cost
    enhanceInfo.costSupply = enhanceData.ac_supply_cost
    enhanceInfo.effectType1 = enhanceData.add_type_1
    enhanceInfo.effectValue1 = enhanceData.add_param_1
    enhanceInfo.effectType2 = enhanceData.add_type_2
    enhanceInfo.effectValue2 = enhanceData.add_param_2
    return enhanceInfo
  end
  return nil
end
function GameDataAccessHelper:GetArcaneBattleInfo(battleID)
  local battleInfo = ArcaneBattleTable[battleID]
  return battleInfo
end
function GameDataAccessHelper:GetAttributeEffectText(effectType)
  return ...
end
function GameDataAccessHelper:GetVIPLimit(key)
  local vipLimitInfo = VIPLimitTable[key]
  if nil == vipLimitInfo then
    return nil
  elseif key == "no_enhance_cd" and immanentversionSpeed == 3 then
    return 0
  else
    return vipLimitInfo.vip_level
  end
end
function GameDataAccessHelper:GetVIPLimitText(key)
  local vipLimitInfo = VIPLimitTable[key]
  if nil == vipLimitInfo then
    return ""
  else
    local viplevel = vipLimitInfo.vip_level
    local playerlevel = vipLimitInfo.player_level
    local str = ""
    if viplevel > 0 then
      str = str .. string.gsub(GameLoader:GetGameText("LC_MENU_VIP_LIMIT_PRIVILIEGE"), "<number1>", tostring(viplevel))
    end
    if playerlevel > 0 then
      str = str .. string.gsub(GameLoader:GetGameText("LC_MENU_LEVEL_LIMIT_PRIVILIEGE"), "<number1>", tostring(playerlevel))
    end
    return str
  end
end
function GameDataAccessHelper:GetLevelLimit(key)
  local levelLimitInfo = VIPLimitTable[key]
  if nil == levelLimitInfo then
    return nil
  else
    return levelLimitInfo.player_level
  end
end
function GameDataAccessHelper:GetSkillIdFromHeroId(heroID)
  local basicinfo = GameDataAccessHelper:GetCommanderBasicInfo(heroID, nil)
  local skill_id = basicinfo.SPELL_ID
  return skill_id
end
function GameDataAccessHelper:GetSkillNameFromHeroID(heroID)
  local basicinfo = GameDataAccessHelper:GetCommanderBasicInfo(heroID, nil)
  local skill_id = basicinfo.SPELL_ID
  return ...
end
function GameDataAccessHelper:GetSkillDescFromHeroID(heroID)
  local basicinfo = GameDataAccessHelper:GetCommanderBasicInfo(heroID, nil)
  local skill_id = basicinfo.SPELL_ID
  return ...
end
function GameDataAccessHelper:GetSkillDesc(skill)
  local damageRate = 0
  local currentSpellTable = v1extra_herosSpells
  if immanentversion == 2 then
    currentSpellTable = v2extra_herosSpells
  end
  local currentSpell
  if currentSpellTable then
    currentSpell = currentSpellTable[skill]
  end
  currentSpell = currentSpell or Spells[skill]
  if not currentSpell then
    DebugOut("GetSkillDesc Use extral_ArtifactSpell spell!!!!! = ", spillID)
    currentSpell = v1extra_herosSpells_1[spillID]
  end
  currentSpell = currentSpell or ArtifactSpell[id_skill]
  if currentSpell then
    damageRate = currentSpell.DAMAGE
  end
  local skillDesc = GameDataAccessHelper:GetSkillDesText(skill)
  skillDesc = string.gsub(skillDesc, "<number>", damageRate)
  return skillDesc
end
function GameDataAccessHelper:GetKryptonBase(kryptonid)
  DebugOut("GetKryptonBase", kryptonid)
  DebugTable(KryptonBase)
  local krypton_info = KryptonBase[kryptonid]
  return krypton_info
end
function GameDataAccessHelper:GetVIPText(page, index)
  if immanentversion == 1 then
    if immanentversionSpeed == 3 then
      return ...
    elseif immanentversion170 == 4 or immanentversion170 == 5 then
      return ...
    else
      return ...
    end
  elseif immanentversion == 2 then
    return ...
  end
end
function GameDataAccessHelper:IsChainSpell(spellID)
  local rangeType = GameDataAccessHelper:GetSpellRealRangeType(spellID)
  DebugOut("spellID = ", spellID)
  DebugOut("rangeType = ", rangeType)
  if rangeType == 7 then
    return true
  end
  return false
end
function GameDataAccessHelper:GetFleetRankImageFrame(rank)
  local currentRankImageTable = v1extra_herosrankimage
  if immanentversion == 2 then
    currentRankImageTable = v2extra_herosrankimage
  end
  currentRankImageTable = currentRankImageTable or GameData.heros.rankimage
  if currentRankImageTable then
    DebugOut("currentRankImageTable = ")
    DebugTable(currentRankImageTable)
    for k, v in ipairs(currentRankImageTable) do
      if v.rank == rank then
        return v.image
      end
    end
  end
  GameUtils:ShowTips(GameLoader:GetGameText("LC_MENU_NOTICE_EMPIRE_COLOSSEUM_RE_LOGIN"))
  return "rank_E"
end
function GameDataAccessHelper:GetPlayerRankingFrame(ranking)
  local rankingTable = GameData.GLC_RANK.rank_img
  DebugOut("rankingTable = ")
  DebugTable(rankingTable)
  for k, v in ipairs(rankingTable) do
    if ranking == v.RANK then
      return v.img
    end
  end
  return "rank1_0"
end
GameDataAccessHelper.LadderCurrentStepInfo = {}
function GameDataAccessHelper:GetMonsterInfoById(identity)
  DebugOut("GameDataAccessHelper:GetMonsterInfoById")
  if not GameDataAccessHelper.LadderCurrentStepInfo then
    return nil
  end
  DebugTable(GameDataAccessHelper.LadderCurrentStepInfo)
  for _, monster_info in pairs(GameDataAccessHelper.LadderCurrentStepInfo.matrix) do
    if monster_info.identity == identity then
      return monster_info
    end
  end
  return nil
end
function GameDataAccessHelper:GetMonsterAvatarById(identity)
  local monsterInfo = GameDataAccessHelper:GetMonsterInfoById(identity)
  if not monsterInfo then
    return "head9001"
  end
  return ...
end
function GameDataAccessHelper:GetMonsterShipById(identity)
  local monsterInfo = GameDataAccessHelper:GetMonsterInfoById(identity)
  if not monsterInfo then
    return "ship9001"
  end
  return ...
end
function GameDataAccessHelper:GetMonsterDurabilityById(identity)
  local monsterInfo = GameDataAccessHelper:GetMonsterInfoById(identity)
  if not monsterInfo then
    return nil
  end
  return monsterInfo.durability
end
function GameDataAccessHelper:GetCommanderSex(fleetid)
  local sex = 2
  if fleetid == 1 then
    sex = GameGlobalData:GetData("userinfo").sex
  else
    local ability = self:GetCommanderBasicInfo(fleetid, 0)
    if ability then
      DebugOut("ashdhjads:", fleetid, ability.sex)
      sex = ability.sex
    else
      DebugOut("fleet ability dont found :", fleetid, 0)
    end
  end
  return sex
end
function GameDataAccessHelper:GetAdventureDrop(itemid)
  if Search_170[itemid] then
    local str = Search_170[itemid].battleid
    local battleIdList = LuaUtils:string_split(str, ",")
    return battleIdList
  end
  return nil
end
function GameDataAccessHelper:GetVipChatInfo(vipLevel)
  local info = VIPChat[vipLevel]
  return info
end
function GameDataAccessHelper:IsFleet_energyAtk(identity)
  local ab = GameDataAccessHelper:GetCommanderAbility(identity, 1)
  assert(ab, "must " .. identity)
  return ab.ATK_TYPE == 2
end
function GameDataAccessHelper:IsEquip_energyAtk(equipid)
  local nt = equipid % 10
  return nt == 6
end
GameDataAccessHelper:Init()
