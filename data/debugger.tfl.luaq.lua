local debug = require("debug")
module("remotedebugger", package.seeall)
local coro_debugger
local events = {BREAK = 1, WATCH = 2}
local breakpoints = {}
local watches = {}
local step_into = false
local step_over = false
local step_level = 0
local stack_level = 0
local controller_host = "127.0.0.1"
local controller_port = 270
local function set_breakpoint(file, line)
  if not breakpoints[file] then
    breakpoints[file] = {}
  end
  breakpoints[file][line] = true
end
local function remove_breakpoint(file, line)
  if breakpoints[file] then
    breakpoints[file][line] = nil
  end
end
local function has_breakpoint(file, line)
  return breakpoints[file] and breakpoints[file][line]
end
local function restore_vars(vars)
  if type(vars) ~= "table" then
    return
  end
  local func = debug.getinfo(3, "f").func
  local i = 1
  local written_vars = {}
  while true do
    local name = debug.getlocal(3, i)
    if not name then
      break
    end
    debug.setlocal(3, i, vars[name])
    written_vars[name] = true
    i = i + 1
  end
  i = 1
  while true do
    local name = debug.getupvalue(func, i)
    if not name then
      break
    end
    if not written_vars[name] then
      debug.setupvalue(func, i, vars[name])
      written_vars[name] = true
    end
    i = i + 1
  end
end
local function capture_vars()
  local vars = {}
  local func = debug.getinfo(3, "f").func
  local i = 1
  while true do
    local name, value = debug.getupvalue(func, i)
    if not name then
      break
    end
    vars[name] = value
    i = i + 1
  end
  i = 1
  while true do
    local name, value = debug.getlocal(3, i)
    if not name then
      break
    end
    vars[name] = value
    i = i + 1
  end
  setmetatable(vars, {
    __index = getfenv(func),
    __newindex = getfenv(func)
  })
  return vars
end
local break_dir = function(path)
  local paths = {}
  path = string.gsub(path, "\\", "/")
  for w in string.gfind(path, "[^/]+") do
    table.insert(paths, w)
  end
  return paths
end
local function merge_paths(path1, path2)
  local paths1 = break_dir(path1)
  local paths2 = break_dir(path2)
  for i, path in ipairs(paths2) do
    if path == ".." then
      table.remove(paths1, table.getn(paths1))
    elseif path ~= "." then
      table.insert(paths1, path)
    end
  end
  return ...
end
local function debug_hook(event, line)
  if event == "call" then
    stack_level = stack_level + 1
  elseif event == "return" then
    stack_level = stack_level - 1
  else
    do
      local file = debug.getinfo(2, "S").source
      if string.find(file, "@") == 1 then
        file = string.sub(file, 2)
      end
      local vars = capture_vars()
      table.foreach(watches, function(index, value)
        setfenv(value, vars)
        local status, res = pcall(value)
        if status and res then
          coroutine.resume(coro_debugger, events.WATCH, vars, file, line, index)
        end
      end)
      if step_into or step_over and stack_level <= step_level or has_breakpoint(file, line) then
        step_into = false
        step_over = false
        coroutine.resume(coro_debugger, events.BREAK, vars, file, line)
      end
    end
  end
end
local function debugger_loop(client)
  local command
  local eval_env = {}
  while true do
    local line, status = ext.ssock.recv(client)
    if line then
      command = string.sub(line, string.find(line, "^[A-Z]+"))
      if command == "SETB" then
        local _, _, _, filename, line = string.find(line, "^([A-Z]+)%s+([%w%p]+)%s+(%d+)$")
        if filename and line then
          filename = string.gsub(filename, "%%20", " ")
          set_breakpoint(filename, tonumber(line))
          ext.ssock.send(client, "200 OK")
        else
          ext.ssock.send(client, "400 Bad Request")
        end
      elseif command == "DELB" then
        local _, _, _, filename, line = string.find(line, "^([A-Z]+)%s+([%w%p]+)%s+(%d+)$")
        if filename and line then
          remove_breakpoint(filename, tonumber(line))
          ext.ssock.send(client, "200 OK")
        else
          ext.ssock.send(client, "400 Bad Request")
        end
      elseif command == "EXEC" then
        local _, _, chunk = string.find(line, "^[A-Z]+%s+(.+)$")
        if chunk then
          local func = loadstring(chunk)
          local status, res
          if func then
            setfenv(func, eval_env)
            status, res = xpcall(func, debug.traceback)
          end
          res = tostring(res)
          if status then
            ext.ssock.send(client, "200 OK " .. string.len(res))
            ext.ssock.send(client, res)
          else
            ext.ssock.send(client, "401 Error in Expression " .. string.len(res))
            ext.ssock.send(client, res)
          end
        else
          ext.ssock.send(client, "400 Bad Request")
        end
      elseif command == "SETW" then
        local _, _, exp = string.find(line, "^[A-Z]+%s+(.+)$")
        if exp then
          local func = loadstring("return(" .. exp .. ")")
          local newidx = table.getn(watches) + 1
          watches[newidx] = func
          table.setn(watches, newidx)
          ext.ssock.send(client, "200 OK " .. newidx)
        else
          ext.ssock.send(client, "400 Bad Request")
        end
      elseif command == "DELW" then
        local _, _, index = string.find(line, "^[A-Z]+%s+(%d+)$")
        index = tonumber(index)
        if index then
          watches[index] = nil
          ext.ssock.send(client, "200 OK")
        else
          ext.ssock.send(client, "400 Bad Request")
        end
      elseif command == "RUN" then
        ext.ssock.send(client, "200 OK")
        local ev, vars, file, line, idx_watch = coroutine.yield()
        eval_env = vars
        if ev == events.BREAK then
          ext.ssock.send(client, "202 Paused " .. file .. " " .. line)
        elseif ev == events.WATCH then
          ext.ssock.send(client, "203 Paused " .. file .. " " .. line .. " " .. idx_watch)
        else
          ext.ssock.send(client, "401 Error in Execution " .. string.len(file))
          ext.ssock.send(client, file)
        end
      elseif command == "STEP" then
        ext.ssock.send(client, "200 OK")
        step_into = true
        local ev, vars, file, line, idx_watch = coroutine.yield()
        eval_env = vars
        if ev == events.BREAK then
          ext.ssock.send(client, "202 Paused " .. file .. " " .. line)
        elseif ev == events.WATCH then
          ext.ssock.send(client, "203 Paused " .. file .. " " .. line .. " " .. idx_watch)
        else
          ext.ssock.send(client, "401 Error in Execution " .. string.len(file))
          ext.ssock.send(client, file)
        end
      elseif command == "OVER" then
        ext.ssock.send(client, "200 OK")
        step_over = true
        step_level = stack_level
        local ev, vars, file, line, idx_watch = coroutine.yield()
        eval_env = vars
        if ev == events.BREAK then
          ext.ssock.send(client, "202 Paused " .. file .. " " .. line)
        elseif ev == events.WATCH then
          ext.ssock.send(client, "203 Paused " .. file .. " " .. line .. " " .. idx_watch)
        else
          ext.ssock.send(client, "401 Error in Execution " .. string.len(file))
          ext.ssock.send(client, file)
        end
      else
        ext.ssock.send(client, "400 Bad Request")
      end
    end
  end
end
coro_debugger = coroutine.create(debugger_loop)
function config(tab)
  if tab.host then
    controller_host = tab.host
  end
  if tab.port then
    controller_port = tab.port
  end
end
function start(host, port)
  local controller_host = host or controller_host
  local controller_port = port or controller_port
  local client = ext.ssock.connect(controller_host, controller_port)
  if client then
    debug.sethook(debug_hook, "lcr")
    return ...
  end
end
