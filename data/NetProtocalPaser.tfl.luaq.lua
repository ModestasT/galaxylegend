require("data1/NetAPIList.tfl")
Paser = {}
function Paser:Pstring(msg, key, parentTable)
  parentTable[key] = msg:readString()
end
function Paser:Pinteger(msg, key, parentTable)
  parentTable[key] = msg:readInt()
end
function Paser:Pfloat(msg, key, parentTable)
  parentTable[key] = msg:readFloat()
end
function Paser:Pshort(msg, key, parentTable)
  parentTable[key] = msg:readShort()
end
function Paser:Pboolean(msg, key, parentTable)
  parentTable[key] = msg:readBoolean()
end
function Paser:Pdouble(msg, key, parentTable)
  parentTable[key] = msg:readDouble()
end
function Paser:Fstring(msg, value)
  msg:writeString(value)
end
function Paser:Finteger(msg, value)
  msg:writeInt(value)
end
function Paser:Fshort(msg, value)
  msg:writeShort(value)
end
function Paser:Ffloat(msg, value)
  msg:writeFloat(value)
end
function Paser:Fboolean(msg, value)
  msg:writeBoolean(value)
end
function Paser:Fdouble(msg, value)
  msg:writeDouble(value)
end
function Paser:FArray(msg, value, valueType)
  local funcName = "F" .. valueType
  local func = self[funcName]
  local arrayLen = #value
  msg:writeUShort(arrayLen)
  for i = 1, arrayLen do
    local v = value[i]
    func(self, msg, v)
  end
end
function Paser:PArray(msg, key, parentTable, valueType)
  local funcName = "P" .. valueType
  local func = self[funcName]
  parentTable[key] = {}
  local arrayLen = msg:readUShort()
  for i = 1, arrayLen do
    local tempT = {}
    func(self, msg, "tempK", tempT)
    table.insert(parentTable[key], tempT.tempK)
  end
end
function Paser:parseMessage(msg, msgType)
  local result = {}
  msgType = "MSGTYPE" .. msgType
  local dataStructName = NetAPIList:getDataStructFromMsgType(msgType)
  if dataStructName and dataStructName ~= "null" then
    local funcName = "P" .. dataStructName
    DebugOut("---- parse func name--- " .. funcName)
    local func = Paser[funcName]
    func(self, msg, nil, result)
  end
  return result
end
function Paser:formatMessage(msg, msgType, content)
  msg:beginStruct()
  msg:writeUShort(msgType)
  msgType = "MSGTYPE" .. msgType
  local dataStructName = NetAPIList:getDataStructFromMsgType(msgType)
  if dataStructName and dataStructName ~= "null" then
    local funcName = "F" .. dataStructName
    DebugOut("---- formatMessage func name--- " .. funcName)
    local func = Paser[funcName]
    func(self, msg, content)
  end
  msg:endStruct()
end
function Paser:Ppair(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "key", parentTable)
  self:Pinteger(m, "value", parentTable)
end
function Paser:Pudid_step(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "market", parentTable)
  self:Pstring(m, "terminal", parentTable)
  self:Pstring(m, "udid", parentTable)
  self:Pinteger(m, "step", parentTable)
end
function Paser:Pudid_track(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "market", parentTable)
  self:Pstring(m, "terminal", parentTable)
  self:Pstring(m, "udid", parentTable)
  self:Pstring(m, "track", parentTable)
end
function Paser:Pprotocal_version_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "version", parentTable)
end
function Paser:Pkeylist(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "keys", parentTable, "integer")
end
function Paser:Pcommon_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "api", parentTable)
  self:Pinteger(m, "code", parentTable)
  self:Pstring(m, "msg", parentTable)
end
function Paser:Puser_passport_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "passport", parentTable)
  self:Pstring(m, "password", parentTable)
  self:Pstring(m, "udid", parentTable)
  self:Pstring(m, "mac_addr", parentTable)
  self:Pstring(m, "idfa", parentTable)
  self:Pstring(m, "open_udid", parentTable)
  self:Pstring(m, "locale", parentTable)
  self:Pinteger(m, "game_id", parentTable)
  self:Pstring(m, "region_name", parentTable)
  self:Pinteger(m, "acc_type", parentTable)
end
function Paser:Puser_client_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "market", parentTable)
  self:Pstring(m, "terminal", parentTable)
  self:Pstring(m, "app_id", parentTable)
  self:Pstring(m, "os_version", parentTable)
  self:Pstring(m, "device_name", parentTable)
  self:Pstring(m, "client_version_svn", parentTable)
  self:Pstring(m, "client_version_main", parentTable)
  self:Pstring(m, "google_aid", parentTable)
  self:Pstring(m, "device_id", parentTable)
  self:Pinteger(m, "device_id_type", parentTable)
end
function Paser:Puser_register_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "name", parentTable)
  self:Puser_client_info(m, "client_info", parentTable)
  self:Puser_passport_info(m, "passport_info", parentTable)
  self:Pinteger(m, "sex", parentTable)
  self:Pstring(m, "invitecode", parentTable)
end
function Paser:Puser_login_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "name", parentTable)
  self:Puser_client_info(m, "client_info", parentTable)
  self:Puser_passport_info(m, "passport_info", parentTable)
end
function Paser:Ppassport_bind_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Puser_passport_info(m, "passport_info", parentTable)
  self:Pboolean(m, "is_new", parentTable)
  self:Pstring(m, "name", parentTable)
end
function Paser:Ppassport_bind_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pboolean(m, "can_iap", parentTable)
  self:Pboolean(m, "is_unlock", parentTable)
end
function Paser:Pbuilding(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "name", parentTable)
  self:Pinteger(m, "level", parentTable)
  self:Pinteger(m, "upgrade_time", parentTable)
  self:Pinteger(m, "cost", parentTable)
  self:Pinteger(m, "status", parentTable)
  self:Pinteger(m, "max_level", parentTable)
  self:Pinteger(m, "require_user_level", parentTable)
end
function Paser:Pskill_desc_item(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "desc_key", parentTable)
  self:Pboolean(m, "is_finish", parentTable)
  self:Pshort(m, "num", parentTable)
end
function Paser:Pfleet(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "id", parentTable)
  self:Pinteger(m, "identity", parentTable)
  self:Pshort(m, "level", parentTable)
  self:Pinteger(m, "vessels", parentTable)
  self:Pinteger(m, "max_durability", parentTable)
  self:Pinteger(m, "shield", parentTable)
  self:Pinteger(m, "durability", parentTable)
  self:Pinteger(m, "ph_attack", parentTable)
  self:Pinteger(m, "en_attack", parentTable)
  self:Pinteger(m, "en_armor", parentTable)
  self:Pinteger(m, "ph_armor", parentTable)
  self:Pinteger(m, "sp_attack", parentTable)
  self:Pinteger(m, "sp_armor", parentTable)
  self:Pshort(m, "max_accumulator", parentTable)
  self:Pshort(m, "accumulator", parentTable)
  self:Pinteger(m, "crit_damage", parentTable)
  self:Pinteger(m, "hit_rate", parentTable)
  self:Pinteger(m, "status", parentTable)
  self:Pinteger(m, "repair_money", parentTable)
  self:Pinteger(m, "krypton", parentTable)
  self:PArray(m, "spells", parentTable, "integer")
  self:Pinteger(m, "active_spell", parentTable)
  self:Pinteger(m, "damage", parentTable)
  self:Pdouble(m, "force", parentTable)
  self:Pinteger(m, "crit_lv", parentTable)
  self:Pinteger(m, "anti_crit", parentTable)
  self:Pinteger(m, "motility", parentTable)
  self:Pinteger(m, "penetration", parentTable)
  self:Pinteger(m, "intercept", parentTable)
  self:Pinteger(m, "fce", parentTable)
  self:PArray(m, "gift", parentTable, "gift_info")
  self:PArray(m, "adjutant_vessels", parentTable, "integer")
  self:Pinteger(m, "cur_adjutant", parentTable)
  self:Pinteger(m, "adjutant_level", parentTable)
  self:PArray(m, "can_adjutant", parentTable, "integer")
  self:PArray(m, "adjutant_spell", parentTable, "adjutant_spell")
  self:Pinteger(m, "damage_deepens", parentTable)
  self:Pinteger(m, "damage_reduction", parentTable)
  self:Pinteger(m, "skill_damage_deepens", parentTable)
  self:Pinteger(m, "skill_damage_reduction", parentTable)
  self:Pinteger(m, "char_damage", parentTable)
  self:Pinteger(m, "accumulater_atk", parentTable)
  self:Pinteger(m, "accumulater_def", parentTable)
  self:Pinteger(m, "accumulater_crit", parentTable)
  self:Pinteger(m, "accumulater_intercept", parentTable)
  self:Pinteger(m, "accumulater_counter", parentTable)
  self:Pinteger(m, "anti_crit_damage", parentTable)
  self:Pinteger(m, "buff_hit_rate", parentTable)
  self:Pinteger(m, "anti_buff_hit_rate", parentTable)
  self:PArray(m, "skill_upgrade_desc", parentTable, "skill_desc_item")
end
function Paser:Padjutant_spell(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "spell_id", parentTable)
  self:Pinteger(m, "level", parentTable)
  self:Pinteger(m, "state", parentTable)
end
function Paser:Pgift_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "gift", parentTable)
end
function Paser:Plevel_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "level", parentTable)
  self:Pinteger(m, "experience", parentTable)
  self:Pinteger(m, "uplevel_experience", parentTable)
end
function Paser:Puser_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "player_id", parentTable)
  self:Pstring(m, "name", parentTable)
  self:Pstring(m, "udid", parentTable)
  self:Pstring(m, "passport", parentTable)
  self:Pboolean(m, "can_iap", parentTable)
  self:Pboolean(m, "is_unlock", parentTable)
  self:Pinteger(m, "sex", parentTable)
  self:Pinteger(m, "icon", parentTable)
  self:Pstring(m, "server", parentTable)
end
function Paser:Puser_resource(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pdouble(m, "money", parentTable)
  self:Pinteger(m, "technique", parentTable)
  self:Pinteger(m, "credit", parentTable)
  self:Pinteger(m, "prestige", parentTable)
  self:Pinteger(m, "quark", parentTable)
  self:Pinteger(m, "lepton", parentTable)
  self:Pinteger(m, "kenergy", parentTable)
  self:Pinteger(m, "activity", parentTable)
  self:Pinteger(m, "brick", parentTable)
  self:Pinteger(m, "wd_supply", parentTable)
  self:Pinteger(m, "total_amount", parentTable)
  self:Pinteger(m, "ladder_search", parentTable)
  self:Pinteger(m, "wormhole_search", parentTable)
  self:Pinteger(m, "crusade_repair", parentTable)
  self:Pinteger(m, "crystal", parentTable)
  self:Pinteger(m, "silver", parentTable)
  self:Pinteger(m, "medal_resource", parentTable)
  self:Pinteger(m, "wdc_supply", parentTable)
  self:Pinteger(m, "art_point", parentTable)
  self:Pinteger(m, "expedition", parentTable)
  self:Pinteger(m, "tlc_supply", parentTable)
  self:Pinteger(m, "tlc_money", parentTable)
end
function Paser:Pbattle_status(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "status", parentTable)
  self:Pinteger(m, "battle_id", parentTable)
  self:Pinteger(m, "battle_type", parentTable)
  self:PArray(m, "fleet_list", parentTable, "integer")
  self:Pinteger(m, "event_type", parentTable)
  self:Pinteger(m, "user_level", parentTable)
end
function Paser:Puser_progress(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "act", parentTable)
  self:Pinteger(m, "chapter", parentTable)
  self:Pinteger(m, "all_count", parentTable)
  self:Pinteger(m, "finish_count", parentTable)
  self:Pinteger(m, "adv_chapter", parentTable)
end
function Paser:Puser_cdtime(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "id", parentTable)
  self:Pinteger(m, "left_time", parentTable)
  self:Pinteger(m, "content", parentTable)
  self:Pinteger(m, "status", parentTable)
  self:Pinteger(m, "cd_type", parentTable)
  self:Pinteger(m, "can_do", parentTable)
end
function Paser:Puser_cdtime_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "cdtimes", parentTable, "user_cdtime")
end
function Paser:Pfleets_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "fleets", parentTable, "fleet")
end
function Paser:Pfriend_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "id", parentTable)
  self:Pstring(m, "name", parentTable)
  self:Pinteger(m, "level", parentTable)
  self:Pinteger(m, "online", parentTable)
  self:Pinteger(m, "sex", parentTable)
  self:Pinteger(m, "icon", parentTable)
  self:Pinteger(m, "main_fleet_level", parentTable)
end
function Paser:Pfriend_detail(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "id", parentTable)
  self:Pstring(m, "name", parentTable)
  self:Pinteger(m, "level", parentTable)
  self:Pinteger(m, "pvp_rank", parentTable)
  self:Pstring(m, "alliance", parentTable)
  self:Pdouble(m, "force", parentTable)
  self:Pinteger(m, "online", parentTable)
  self:Pinteger(m, "sex", parentTable)
  self:Pinteger(m, "icon", parentTable)
end
function Paser:Puser_offline_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "minute", parentTable)
  self:Pinteger(m, "max_minute", parentTable)
  self:Pinteger(m, "expr_per_minute", parentTable)
end
function Paser:Palliance_pos_and_id(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pboolean(m, "weekend_award", parentTable)
  self:Pstring(m, "alliance_name", parentTable)
  self:Pshort(m, "position", parentTable)
  self:Pstring(m, "id", parentTable)
  self:Pshort(m, "flag", parentTable)
end
function Paser:Pbattle_matrix_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "battle_id", parentTable)
end
function Paser:Pfleet_level(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "cell_id", parentTable)
  self:Pinteger(m, "level", parentTable)
end
function Paser:Pmatrix_cell(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "cell_id", parentTable)
  self:Pinteger(m, "fleet_identity", parentTable)
  self:Pinteger(m, "fleet_spell", parentTable)
end
function Paser:Pmonster_matrix_cell(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "cell_id", parentTable)
  self:Pinteger(m, "identity", parentTable)
  self:Pinteger(m, "spell", parentTable)
  self:Pdouble(m, "durability", parentTable)
  self:Pinteger(m, "shield", parentTable)
  self:Pinteger(m, "level", parentTable)
  self:Pdouble(m, "force", parentTable)
  self:Pdouble(m, "vessels", parentTable)
  self:Pstring(m, "avatar", parentTable)
  self:Pstring(m, "ship", parentTable)
  self:Pinteger(m, "sex", parentTable)
end
function Paser:Pmatrix(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pstring(m, "name", parentTable)
  self:PArray(m, "cells", parentTable, "matrix_cell")
  self:Pinteger(m, "count", parentTable)
end
function Paser:Pmulmatrix_one(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "index", parentTable)
  self:Pinteger(m, "type", parentTable)
  self:Pstring(m, "name", parentTable)
  self:PArray(m, "cells", parentTable, "matrix_cell")
end
function Paser:Pmulmatrix_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "blank_num", parentTable)
  self:Pshort(m, "max_blank", parentTable)
  self:PArray(m, "matrixs", parentTable, "mulmatrix_one")
  self:PArray(m, "matrixs_purpose", parentTable, "pair")
  self:Pshort(m, "cur_index", parentTable)
end
function Paser:Pmulmatrix_blank(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "blank", parentTable)
end
function Paser:Pmulmatrix_condition(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "type", parentTable)
  self:Pinteger(m, "value", parentTable)
end
function Paser:Pmulmatrix_price(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "credit_cost", parentTable)
  self:Pinteger(m, "money_cost", parentTable)
  self:PArray(m, "conditions", parentTable, "mulmatrix_condition")
end
function Paser:Pmonster_matrix(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "cells", parentTable, "monster_matrix_cell")
end
function Paser:Paddition_fleet(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "identity", parentTable)
  self:Pshort(m, "pos", parentTable)
end
function Paser:Pbattle_matrix_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pmonster_matrix(m, "monsters", parentTable)
  self:Pinteger(m, "status", parentTable)
  self:Pdouble(m, "force", parentTable)
  self:PArray(m, "addition", parentTable, "addition_fleet")
end
function Paser:Puser_buildings(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "buildings", parentTable, "building")
end
function Paser:Ptask_statistic_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "finish_count", parentTable)
end
function Paser:Pmail_unread_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "count", parentTable)
end
function Paser:Pvip_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "level", parentTable)
  self:Pinteger(m, "current_exp", parentTable)
  self:Pinteger(m, "next_level_exp", parentTable)
  self:Pboolean(m, "pay_act", parentTable)
end
function Paser:Pmodule_status(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "name", parentTable)
  self:Pboolean(m, "status", parentTable)
end
function Paser:Pmodules_status(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "modules", parentTable, "module_status")
end
function Paser:Pquest(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "quest_id", parentTable)
  self:Pinteger(m, "status", parentTable)
  self:Pstring(m, "cond_type", parentTable)
  self:Pinteger(m, "cond_int", parentTable)
  self:Pinteger(m, "cond_total", parentTable)
  self:Pinteger(m, "cond_count", parentTable)
  self:PArray(m, "loot", parentTable, "game_item")
  self:Pinteger(m, "player_level", parentTable)
  self:Pinteger(m, "head", parentTable)
  self:Pstring(m, "prompt", parentTable)
end
function Paser:Puser_snapshot(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Puser_info(m, "userinfo", parentTable)
  self:Puser_resource(m, "resource", parentTable)
  self:Puser_buildings(m, "buildings", parentTable)
  self:Puser_progress(m, "progress", parentTable)
  self:Pfleets_info(m, "fleetinfo", parentTable)
  self:PArray(m, "equipments", parentTable, "equipment")
  self:Puser_offline_info(m, "offline_info", parentTable)
  self:Palliance_pos_and_id(m, "alliance", parentTable)
  self:PArray(m, "fleetkryptons", parentTable, "fleet_krypton_info")
  self:Puser_cdtime_info(m, "cdtimes", parentTable)
  self:Plevel_info(m, "levelinfo", parentTable)
  self:Pvip_info(m, "vipinfo", parentTable)
  self:Pmatrix(m, "matrix", parentTable)
  self:Ptask_statistic_info(m, "task_statistic", parentTable)
  self:Pmail_unread_info(m, "unmailinfo", parentTable)
  self:Pmodules_status(m, "modules_status", parentTable)
  self:Pquest(m, "user_quest", parentTable)
  self:Pinteger(m, "server_code", parentTable)
end
function Paser:Pbuilding_upgrade_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "name", parentTable)
end
function Paser:Pbattle_request(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "battle_id", parentTable)
  self:Pinteger(m, "action", parentTable)
end
function Paser:Pchat_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "content", parentTable)
  self:Pstring(m, "receiver", parentTable)
  self:Pstring(m, "channel", parentTable)
  self:Pshort(m, "show_effect", parentTable)
end
function Paser:Pchat_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "sender", parentTable)
  self:Pshort(m, "sender_role", parentTable)
  self:Pstring(m, "sender_name", parentTable)
  self:Pinteger(m, "sender_level", parentTable)
  self:Pstring(m, "content", parentTable)
  self:Pstring(m, "origin_content", parentTable)
  self:Pstring(m, "receiver", parentTable)
  self:Pstring(m, "channel", parentTable)
  self:Pinteger(m, "timestamp", parentTable)
  self:Pinteger(m, "champion_rank", parentTable)
  self:Pinteger(m, "tc_king", parentTable)
  self:Pshort(m, "tag", parentTable)
  self:Pshort(m, "wdc_ranking", parentTable)
  self:Pshort(m, "wdc_rank", parentTable)
  self:Pshort(m, "sex", parentTable)
  self:Pstring(m, "wdc_img", parentTable)
  self:Pshort(m, "viplevel", parentTable)
  self:Pshort(m, "show_effect", parentTable)
end
function Paser:Pmap_status_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "chapter_id", parentTable)
end
function Paser:Puser_map_status(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "all_count", parentTable)
  self:Pinteger(m, "finish_count", parentTable)
  self:PArray(m, "status", parentTable, "battle_status")
  self:Pinteger(m, "refresh_time", parentTable)
end
function Paser:Pbattle_status_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "battle_id", parentTable)
end
function Paser:Pbattle_user_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "username", parentTable)
  self:Pinteger(m, "level", parentTable)
  self:Pstring(m, "report_id", parentTable)
  self:Pinteger(m, "left_time", parentTable)
end
function Paser:Pgame_item(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "item_type", parentTable)
  self:Pinteger(m, "number", parentTable)
  self:Pinteger(m, "no", parentTable)
  self:Pinteger(m, "level", parentTable)
end
function Paser:Pgame_item_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "item_type", parentTable)
  self:Pinteger(m, "number", parentTable)
  self:Pinteger(m, "no", parentTable)
  self:Pinteger(m, "level", parentTable)
  self:PArray(m, "shared_rewards", parentTable, "game_item")
end
function Paser:Pgame_items(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "items", parentTable, "game_item")
end
function Paser:Ploot_item(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pgame_item(m, "base", parentTable)
  self:Pinteger(m, "level", parentTable)
  self:Pboolean(m, "status", parentTable)
end
function Paser:Pbattle_status_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "rewards", parentTable, "loot_item")
  self:Pinteger(m, "accomplish_level", parentTable)
  self:Pinteger(m, "reloot", parentTable)
  self:PArray(m, "first", parentTable, "battle_user_info")
  self:PArray(m, "lowest", parentTable, "battle_user_info")
  self:PArray(m, "news", parentTable, "battle_user_info")
  self:Pinteger(m, "user_level", parentTable)
  self:Pinteger(m, "consume_num", parentTable)
  self:Pinteger(m, "left_count", parentTable)
  self:Pinteger(m, "price", parentTable)
end
function Paser:Pbattle_fight_report_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "battle_report_id", parentTable)
end
function Paser:Pbattle_fight_report_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "code", parentTable)
  self:PArray(m, "histories", parentTable, "fight_history")
end
function Paser:Pbattle_result(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "accomplish_level", parentTable)
  self:PArray(m, "rewards", parentTable, "loot_item")
end
function Paser:Pbag_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "owner", parentTable)
  self:Pinteger(m, "bag_type", parentTable)
  self:Pinteger(m, "pos", parentTable)
end
function Paser:Pbag_grid(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "grid_type", parentTable)
  self:Pinteger(m, "pos", parentTable)
  self:Pinteger(m, "cnt", parentTable)
  self:Pstring(m, "item_id", parentTable)
  self:Pinteger(m, "item_type", parentTable)
  self:Pinteger(m, "title_type", parentTable)
  self:Pinteger(m, "sub_type", parentTable)
  self:PArray(m, "equip_matrixs", parentTable, "integer")
end
function Paser:Pbag_new_grid(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "grid_type", parentTable)
  self:Pinteger(m, "pos", parentTable)
  self:Pinteger(m, "fleet_id", parentTable)
  self:Pinteger(m, "cnt", parentTable)
  self:Pstring(m, "item_id", parentTable)
  self:Pinteger(m, "item_type", parentTable)
  self:Pinteger(m, "title_type", parentTable)
  self:Pinteger(m, "sub_type", parentTable)
  self:PArray(m, "equip_matrixs", parentTable, "integer")
end
function Paser:Pbag_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "owner", parentTable)
  self:Pinteger(m, "bag_type", parentTable)
  self:PArray(m, "grids", parentTable, "bag_grid")
  self:PArray(m, "equipedItems", parentTable, "bag_new_grid")
end
function Paser:Pswap_bag_grid_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "matrix_id", parentTable)
  self:Pstring(m, "orig_grid_owner", parentTable)
  self:Pinteger(m, "oirg_grid_type", parentTable)
  self:Pinteger(m, "oirg_grid_pos", parentTable)
  self:Pstring(m, "target_grid_owner", parentTable)
  self:Pinteger(m, "target_grid_type", parentTable)
  self:Pinteger(m, "target_grid_pos", parentTable)
end
function Paser:Pswap_bag_grid_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "result", parentTable)
end
function Paser:Puse_item_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "pos", parentTable)
  self:Pboolean(m, "is_max", parentTable)
  self:PArray(m, "params", parentTable, "string")
  self:Pinteger(m, "count", parentTable)
end
function Paser:Pequipment(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "equip_id", parentTable)
  self:Pinteger(m, "equip_slot", parentTable)
  self:Pinteger(m, "equip_type", parentTable)
  self:Pinteger(m, "equip_level", parentTable)
  self:Pinteger(m, "equip_level_req", parentTable)
  self:PArray(m, "equip_params", parentTable, "equipment_param")
  self:Pinteger(m, "enchant_level", parentTable)
  self:Pinteger(m, "enchant_effect", parentTable)
  self:Pinteger(m, "enchant_effect_value", parentTable)
  self:Pinteger(m, "enchant_item_id", parentTable)
end
function Paser:Pequipment_param(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "key", parentTable)
  self:Pinteger(m, "value", parentTable)
end
function Paser:Pequip_info_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
end
function Paser:Pequip_info_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pequipment(m, "equip", parentTable)
end
function Paser:Ppack_bag(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "owner", parentTable)
  self:Pinteger(m, "bag_type", parentTable)
  self:PArray(m, "grids", parentTable, "bag_grid")
end
function Paser:Pgm_cmd(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "cmd", parentTable)
end
function Paser:Ptechnique_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "id", parentTable)
  self:Pshort(m, "tech_type", parentTable)
  self:Pshort(m, "enabled", parentTable)
  self:Pshort(m, "level", parentTable)
  self:Pshort(m, "level_limit", parentTable)
  self:Pshort(m, "level_max", parentTable)
  self:Pshort(m, "attr_id", parentTable)
  self:Pinteger(m, "attr_value", parentTable)
  self:Pinteger(m, "total", parentTable)
  self:Pinteger(m, "cost", parentTable)
  self:Pinteger(m, "every_consume", parentTable)
  self:Pinteger(m, "max_for_now", parentTable)
  self:Pinteger(m, "player_limit_level", parentTable)
end
function Paser:Ptechniques_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "techniques", parentTable, "technique_info")
end
function Paser:Ptechniques_upgrade_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "id", parentTable)
  self:Pshort(m, "want_level", parentTable)
end
function Paser:Ptechniques_upgrade_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Ptechnique_info(m, "technique", parentTable)
end
function Paser:Ptechniques_upgrade_fail_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "code", parentTable)
end
function Paser:Prevenue_info_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "total_times", parentTable)
  self:Pinteger(m, "left_times", parentTable)
  self:Pinteger(m, "left_time", parentTable)
  self:Pinteger(m, "money", parentTable)
  self:Pinteger(m, "exchange_cost", parentTable)
end
function Paser:Prevenue_do_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "times", parentTable)
end
function Paser:Prevenue_do_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "more", parentTable)
  self:Prevenue_info_ack(m, "revenue", parentTable)
end
function Paser:Pfight_report_fleet(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "identity", parentTable)
  self:Pshort(m, "pos", parentTable)
  self:Pdouble(m, "cur_durability", parentTable)
  self:Pdouble(m, "max_durability", parentTable)
  self:Pshort(m, "cur_accumulator", parentTable)
  self:Pinteger(m, "shield", parentTable)
  self:Pinteger(m, "max_shield", parentTable)
  self:Pinteger(m, "level", parentTable)
  self:Pinteger(m, "spell_id", parentTable)
  self:Pstring(m, "ptype", parentTable)
end
function Paser:Pfight_report_adjutant(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "major", parentTable)
  self:Pinteger(m, "pos", parentTable)
  self:Pinteger(m, "adjutant", parentTable)
  self:Pinteger(m, "adjutant_level", parentTable)
end
function Paser:Pattack_data(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "atk_pos", parentTable)
  self:Pshort(m, "def_pos", parentTable)
  self:Pshort(m, "attack_type", parentTable)
  self:Pboolean(m, "hit", parentTable)
  self:Pboolean(m, "crit", parentTable)
  self:Pboolean(m, "intercept", parentTable)
  self:Pinteger(m, "shield_damage", parentTable)
  self:Pinteger(m, "dur_damage", parentTable)
  self:Pdouble(m, "durability", parentTable)
  self:Pinteger(m, "shield", parentTable)
  self:Pshort(m, "acc", parentTable)
  self:Pshort(m, "acc_change", parentTable)
  self:Pshort(m, "atk_acc", parentTable)
  self:Pshort(m, "atk_acc_change", parentTable)
  self:Pstring(m, "atk_effect", parentTable)
  self:Pboolean(m, "is_immune", parentTable)
  self:Pstring(m, "def_side", parentTable)
  self:Pboolean(m, "counter", parentTable)
  self:Pinteger(m, "flag", parentTable)
  self:Pinteger(m, "index", parentTable)
end
function Paser:Pbuff_data(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "buff_effect", parentTable)
  self:Pshort(m, "round_cnt", parentTable)
  self:Pshort(m, "pos", parentTable)
  self:Pinteger(m, "param", parentTable)
  self:Pshort(m, "buff_step", parentTable)
  self:Pstring(m, "side", parentTable)
  self:Pshort(m, "effection", parentTable)
  self:Pinteger(m, "index", parentTable)
end
function Paser:Pdot_data(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "pos", parentTable)
  self:Pshort(m, "buff_effect", parentTable)
  self:Pdouble(m, "durability", parentTable)
  self:Pinteger(m, "shield", parentTable)
  self:Pinteger(m, "shield_damage", parentTable)
  self:Pinteger(m, "dur_damage", parentTable)
  self:Pshort(m, "round_cnt", parentTable)
  self:Pboolean(m, "is_immune", parentTable)
  self:Pinteger(m, "index", parentTable)
end
function Paser:Pdamage_data(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "side", parentTable)
  self:Pstring(m, "type", parentTable)
  self:Pshort(m, "pos", parentTable)
  self:Pshort(m, "atk_from", parentTable)
  self:Pdouble(m, "durability", parentTable)
  self:Pinteger(m, "shield", parentTable)
  self:Pinteger(m, "shield_damage", parentTable)
  self:Pinteger(m, "dur_damage", parentTable)
  self:Pinteger(m, "index", parentTable)
end
function Paser:Psp_attack_data(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "char_id", parentTable)
  self:Pinteger(m, "adjutant", parentTable)
  self:Pshort(m, "atk_pos", parentTable)
  self:Pshort(m, "def_pos", parentTable)
  self:Pinteger(m, "sp_id", parentTable)
  self:Pboolean(m, "hit", parentTable)
  self:Pboolean(m, "crit", parentTable)
  self:Pboolean(m, "intercept", parentTable)
  self:Pinteger(m, "shield_damage", parentTable)
  self:Pinteger(m, "dur_damage", parentTable)
  self:Pshort(m, "buff_effect", parentTable)
  self:Pshort(m, "round_cnt", parentTable)
  self:Pdouble(m, "durability", parentTable)
  self:Pinteger(m, "shield", parentTable)
  self:Pboolean(m, "self_cast", parentTable)
  self:Pshort(m, "acc", parentTable)
  self:Pshort(m, "acc_change", parentTable)
  self:Pshort(m, "atk_acc", parentTable)
  self:Pshort(m, "atk_acc_change", parentTable)
  self:Pboolean(m, "is_immune", parentTable)
  self:Pinteger(m, "transfiguration", parentTable)
  self:Pboolean(m, "is_time_back", parentTable)
  self:Pshort(m, "atk_type", parentTable)
  self:Pshort(m, "absorb_attr_type", parentTable)
  self:Pstring(m, "atk_side", parentTable)
  self:Pshort(m, "artifact", parentTable)
  self:Pinteger(m, "flag", parentTable)
  self:Pinteger(m, "index", parentTable)
end
function Paser:Pfight_round(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "round_cnt", parentTable)
  self:Pboolean(m, "player1_action", parentTable)
  self:Pshort(m, "pos", parentTable)
  self:Pinteger(m, "p1_speed", parentTable)
  self:Pinteger(m, "p2_speed", parentTable)
  self:PArray(m, "buff", parentTable, "buff_data")
  self:PArray(m, "dot", parentTable, "dot_data")
  self:PArray(m, "attacks", parentTable, "attack_data")
  self:PArray(m, "sp_attacks", parentTable, "sp_attack_data")
  self:PArray(m, "summon_fleet", parentTable, "fight_report_fleet")
  self:PArray(m, "damage", parentTable, "damage_data")
  self:PArray(m, "relive_fleet", parentTable, "fight_report_fleet")
end
function Paser:Pfight_adjutant_data(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "char_id", parentTable)
  self:Pinteger(m, "atk_pos", parentTable)
  self:Pinteger(m, "def_pos", parentTable)
  self:Pinteger(m, "adjutant", parentTable)
  self:Pinteger(m, "spell_id", parentTable)
  self:Pinteger(m, "buff", parentTable)
  self:Pboolean(m, "self_cast", parentTable)
end
function Paser:Pfight_report(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "player1", parentTable)
  self:Pstring(m, "player1_avatar", parentTable)
  self:Pinteger(m, "player1_identity", parentTable)
  self:Pinteger(m, "player1_pos", parentTable)
  self:PArray(m, "player1_buffs", parentTable, "player_buff")
  self:Pstring(m, "player2", parentTable)
  self:Pstring(m, "player2_avatar", parentTable)
  self:Pinteger(m, "player2_identity", parentTable)
  self:Pinteger(m, "player2_pos", parentTable)
  self:PArray(m, "player2_buffs", parentTable, "player_buff")
  self:PArray(m, "player1_fleets", parentTable, "fight_report_fleet")
  self:PArray(m, "player2_fleets", parentTable, "fight_report_fleet")
  self:PArray(m, "rounds", parentTable, "fight_round")
  self:Pshort(m, "result", parentTable)
  self:PArray(m, "player1_adjutant", parentTable, "fight_report_adjutant")
  self:PArray(m, "player2_adjutant", parentTable, "fight_report_adjutant")
  self:PArray(m, "player1_buffs_adjutant", parentTable, "fight_adjutant_data")
  self:PArray(m, "player2_buffs_adjutant", parentTable, "fight_adjutant_data")
  self:Pinteger(m, "player1_ranking", parentTable)
  self:Pinteger(m, "player1_rank", parentTable)
  self:Pinteger(m, "player2_ranking", parentTable)
  self:Pinteger(m, "player2_rank", parentTable)
end
function Paser:Pplayer_buff(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "type", parentTable)
  self:Pstring(m, "value", parentTable)
end
function Paser:Padd_friend_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "id", parentTable)
  self:Pstring(m, "name", parentTable)
end
function Paser:Pdel_friend_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "id", parentTable)
end
function Paser:Pofficer_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "id", parentTable)
  self:Pinteger(m, "price", parentTable)
  self:Pshort(m, "level_limit", parentTable)
  self:Pshort(m, "free_time", parentTable)
  self:Pstring(m, "loot_type", parentTable)
  self:Pinteger(m, "loot_value", parentTable)
  self:Pinteger(m, "ext_num", parentTable)
  self:Pinteger(m, "strike", parentTable)
end
function Paser:Pofficers_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "officers", parentTable, "officer_info")
end
function Paser:Pofficer_buy_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "id", parentTable)
end
function Paser:Pofficer_buy_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pofficer_info(m, "officer", parentTable)
end
function Paser:Pfleet_show_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "fleet_id", parentTable)
  self:Pshort(m, "level", parentTable)
  self:Pshort(m, "color", parentTable)
  self:Pstring(m, "avatar", parentTable)
  self:Pstring(m, "name", parentTable)
  self:Pstring(m, "vessels_name", parentTable)
  self:Pshort(m, "vessels", parentTable)
  self:Pstring(m, "ship", parentTable)
  self:Pshort(m, "rank", parentTable)
  self:Pshort(m, "damage_assess", parentTable)
  self:Pshort(m, "damage_rate", parentTable)
  self:Pshort(m, "defence_assess", parentTable)
  self:Pshort(m, "defence_rate", parentTable)
  self:Pshort(m, "assist_assess", parentTable)
  self:Pshort(m, "assist_rate", parentTable)
  self:Pshort(m, "atk_type", parentTable)
  self:Pshort(m, "spell_id", parentTable)
  self:Pshort(m, "can_be_adjutant", parentTable)
  self:Pdouble(m, "force", parentTable)
end
function Paser:Precruit_fleet_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "identity", parentTable)
  self:Pshort(m, "state", parentTable)
  self:Pinteger(m, "quark_req", parentTable)
  self:Pinteger(m, "lepton_req", parentTable)
  self:Pinteger(m, "prestige_req", parentTable)
  self:Pinteger(m, "visible_battle_req", parentTable)
  self:Pinteger(m, "atk_scores", parentTable)
  self:Pinteger(m, "def_scores", parentTable)
  self:Pinteger(m, "ass_scores", parentTable)
  self:Pinteger(m, "sort_no", parentTable)
  self:Pfleet_show_info(m, "show_info", parentTable)
end
function Paser:Precruit_list_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "recruit_fleets", parentTable, "recruit_fleet_info")
end
function Paser:Precruit_fleet_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "identity", parentTable)
end
function Paser:Precruit_fleet_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "code", parentTable)
  self:Pinteger(m, "identity", parentTable)
  self:Pshort(m, "state", parentTable)
end
function Paser:Pdismiss_fleet_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
end
function Paser:Pdismiss_fleet_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "code", parentTable)
end
function Paser:Pfleet_one_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
end
function Paser:Pfriends_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "friends", parentTable, "friend_info")
  self:PArray(m, "ban_friends", parentTable, "friend_info")
  self:Pinteger(m, "message_size", parentTable)
end
function Paser:Pequipment_enhance(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "equip_id", parentTable)
  self:Pinteger(m, "equip_type", parentTable)
  self:Pinteger(m, "equip_level", parentTable)
  self:Pinteger(m, "equip_next_level", parentTable)
  self:Pshort(m, "equip_level_fulled", parentTable)
  self:PArray(m, "equip_params", parentTable, "equipment_param")
  self:PArray(m, "equip_next_params", parentTable, "equipment_param")
  self:Pinteger(m, "cost", parentTable)
  self:Pinteger(m, "cdtime", parentTable)
  self:Pinteger(m, "enchant_level", parentTable)
  self:Pinteger(m, "enchant_effect", parentTable)
  self:Pinteger(m, "enchant_effect_value", parentTable)
  self:Pinteger(m, "enchant_item_id", parentTable)
end
function Paser:Pequipment_evolution(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "equip_type", parentTable)
  self:Pinteger(m, "equip_level", parentTable)
  self:PArray(m, "equip_params", parentTable, "equipment_param")
  self:Pinteger(m, "cost", parentTable)
  self:Pinteger(m, "enchant_level", parentTable)
  self:Pinteger(m, "enchant_effect", parentTable)
  self:Pinteger(m, "enchant_effect_value", parentTable)
  self:Pinteger(m, "enchant_item_id", parentTable)
end
function Paser:Pequipment_recipe(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "equip_type", parentTable)
  self:Pinteger(m, "player_level_limit", parentTable)
  self:Pshort(m, "existed", parentTable)
  self:Pshort(m, "recipe_total", parentTable)
end
function Paser:Pequipment_detail_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pequipment_enhance(m, "enhance", parentTable)
  self:Pequipment_evolution(m, "evolution", parentTable)
  self:Pequipment_recipe(m, "recipe", parentTable)
  self:Pshort(m, "enable_evolution", parentTable)
end
function Paser:Pequipment_detail_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pequipment_enhance(m, "enhance", parentTable)
  self:Pequipment_evolution(m, "evolution", parentTable)
  self:Pequipment_recipe(m, "recipe", parentTable)
  self:Pshort(m, "enable_evolution", parentTable)
  self:Pinteger(m, "enchant_level", parentTable)
  self:Pdouble(m, "one_enhance", parentTable)
  self:Pinteger(m, "enhance_level", parentTable)
end
function Paser:Pequipment_detail_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "id", parentTable)
end
function Paser:Pfriend_detail_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "id", parentTable)
end
function Paser:Pfriend_search_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "name", parentTable)
end
function Paser:Pfriend_search_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "friends", parentTable, "friend_info")
end
function Paser:Pfriend_ban_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "id", parentTable)
  self:Pstring(m, "name", parentTable)
end
function Paser:Pwarpgate_energy_bolt(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "bolt_type", parentTable)
  self:Pshort(m, "level", parentTable)
  self:Pshort(m, "val", parentTable)
  self:Pboolean(m, "is_full", parentTable)
end
function Paser:Pwarpgate_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "lepton", parentTable)
  self:Pinteger(m, "quark", parentTable)
  self:Pboolean(m, "free_lepton_charge", parentTable)
  self:Pboolean(m, "free_quark_charge", parentTable)
  self:Pinteger(m, "charge_lepton_cost", parentTable)
  self:Pinteger(m, "charge_quark_cost", parentTable)
  self:Pinteger(m, "normal_upgrade_cost", parentTable)
  self:Pinteger(m, "max_upgrade_cost", parentTable)
  self:PArray(m, "bolts", parentTable, "warpgate_energy_bolt")
  self:Pinteger(m, "convert_cost_credits", parentTable)
end
function Paser:Pwarpgate_charge_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "charge_type", parentTable)
end
function Paser:Pwarpgate_charge_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "code", parentTable)
  self:Pwarpgate_info(m, "info", parentTable)
end
function Paser:Pwarpgate_upgrade_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "upgrade_type", parentTable)
end
function Paser:Pwarpgate_upgrade_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "code", parentTable)
  self:Pwarpgate_info(m, "info", parentTable)
end
function Paser:Pwarpgate_collect_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "code", parentTable)
  self:Pwarpgate_info(m, "info", parentTable)
end
function Paser:Pmail_page_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "page_size", parentTable)
  self:Pinteger(m, "page_no", parentTable)
end
function Paser:Pmail_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "id", parentTable)
  self:Pstring(m, "title", parentTable)
  self:Pinteger(m, "status", parentTable)
  self:Pinteger(m, "time", parentTable)
  self:Pshort(m, "sender_role", parentTable)
  self:PArray(m, "goods", parentTable, "game_item")
  self:Pinteger(m, "get_goods", parentTable)
  self:Pboolean(m, "can_send_story", parentTable)
end
function Paser:Pmail_page_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "max", parentTable)
  self:Pinteger(m, "page_size", parentTable)
  self:Pinteger(m, "page_no", parentTable)
  self:PArray(m, "mails", parentTable, "mail_info")
  self:Pinteger(m, "unread", parentTable)
end
function Paser:Pmail_goods_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "id", parentTable)
end
function Paser:Pmail_goods_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pmail_info(m, "new_info", parentTable)
end
function Paser:Pmail_content_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "id", parentTable)
end
function Paser:Pmail_content_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "id", parentTable)
  self:Pstring(m, "title", parentTable)
  self:Pstring(m, "content", parentTable)
  self:Pinteger(m, "time", parentTable)
  self:Pstring(m, "sender_name", parentTable)
  self:Pstring(m, "sender_id", parentTable)
  self:Pshort(m, "sender_role", parentTable)
end
function Paser:Pmail_send_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "recevier_names", parentTable, "string")
  self:Pstring(m, "title", parentTable)
  self:Pstring(m, "content", parentTable)
end
function Paser:Pmail_delete_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "ids", parentTable, "string")
end
function Paser:Pmail_set_read_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "ids", parentTable, "string")
end
function Paser:Pequipment_enhance_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "equip_id", parentTable)
  self:Pstring(m, "fleet_id", parentTable)
  self:Pinteger(m, "once_more_enhance", parentTable)
  self:Pinteger(m, "en_level", parentTable)
end
function Paser:Pequipment_evolution_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "equip_id", parentTable)
  self:Pstring(m, "fleet_id", parentTable)
end
function Paser:Pequipments_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "equipments", parentTable, "equipment")
end
function Paser:Pquark_exchange_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "use_type", parentTable)
  self:Pshort(m, "cnt", parentTable)
end
function Paser:Pquark_exchange_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "code", parentTable)
end
function Paser:Pshop_purchase_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "item_type", parentTable)
  self:Pshort(m, "cnt", parentTable)
end
function Paser:Pshop_purchase_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "code", parentTable)
end
function Paser:Pshop_sell_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "grid_owner", parentTable)
  self:Pinteger(m, "grid_type", parentTable)
  self:Pinteger(m, "grid_pos", parentTable)
  self:Pshort(m, "cnt", parentTable)
end
function Paser:Pshop_sell_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "code", parentTable)
end
function Paser:Ppurchase_back_item(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "item_type", parentTable)
  self:Pstring(m, "item_id", parentTable)
  self:Pshort(m, "idx", parentTable)
  self:Pshort(m, "cnt", parentTable)
  self:Pinteger(m, "enchant_level", parentTable)
end
function Paser:Pshop_purchase_back_list_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "items", parentTable, "purchase_back_item")
end
function Paser:Pshop_purchase_back_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "idx", parentTable)
end
function Paser:Pshop_purchase_back_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "code", parentTable)
end
function Paser:Puser_offline_info_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Puser_offline_info(m, "offline_info", parentTable)
end
function Paser:Puser_collect_offline_expr_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "result", parentTable)
end
function Paser:Pfleet_repair_all_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "onserver", parentTable)
end
function Paser:Pfleet_repair_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "identity", parentTable)
end
function Paser:Pfriend_ban_del_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "id", parentTable)
end
function Paser:Pmessage_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "id", parentTable)
  self:Pstring(m, "user_id", parentTable)
  self:Pstring(m, "user_name", parentTable)
end
function Paser:Pmessages_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "messages", parentTable, "message_info")
end
function Paser:Pmessage_del_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "id", parentTable)
end
function Paser:Palliances_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "keyword", parentTable)
  self:Pinteger(m, "index", parentTable)
  self:Pinteger(m, "size", parentTable)
end
function Paser:Palliances_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "total", parentTable)
  self:Pinteger(m, "index", parentTable)
  self:PArray(m, "alliances", parentTable, "alliance_info")
  self:PArray(m, "applied_alliances", parentTable, "string")
end
function Paser:Palliance_level_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "level", parentTable)
  self:Pinteger(m, "experience", parentTable)
  self:Pinteger(m, "uplevel_experience", parentTable)
end
function Paser:Palliance_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "id", parentTable)
  self:Pstring(m, "name", parentTable)
  self:Pstring(m, "memo", parentTable)
  self:Pshort(m, "flag", parentTable)
  self:Palliance_level_info(m, "level_info", parentTable)
  self:Pinteger(m, "rank", parentTable)
  self:Pstring(m, "creator_id", parentTable)
  self:Pstring(m, "creator_name", parentTable)
  self:Pshort(m, "members_count", parentTable)
  self:Pshort(m, "members_max", parentTable)
  self:Pinteger(m, "apply_limit", parentTable)
end
function Paser:Palliance_info_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "id", parentTable)
end
function Paser:Palliance_info_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Palliance_info(m, "alliance", parentTable)
end
function Paser:Palliance_members_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "alliance_id", parentTable)
  self:Pinteger(m, "index", parentTable)
  self:Pinteger(m, "size", parentTable)
end
function Paser:Palliance_members_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "total", parentTable)
  self:Pinteger(m, "index", parentTable)
  self:PArray(m, "members", parentTable, "alliance_member_info")
end
function Paser:Palliance_member_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "id", parentTable)
  self:Pstring(m, "name", parentTable)
  self:Pshort(m, "level", parentTable)
  self:Pshort(m, "position", parentTable)
  self:Pshort(m, "military_rank", parentTable)
  self:Pinteger(m, "rank", parentTable)
  self:Pinteger(m, "contribution", parentTable)
  self:Pinteger(m, "last_login", parentTable)
  self:Pdouble(m, "force", parentTable)
  self:Pshort(m, "sex", parentTable)
  self:Pinteger(m, "main_fleet_id", parentTable)
  self:PArray(m, "fleet_ids", parentTable, "alliance_fleet_info")
end
function Paser:Palliance_activities_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "alliance_id", parentTable)
end
function Paser:Palliance_activities_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "activities", parentTable, "alliance_activity_info")
end
function Paser:Palliance_activity_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "id", parentTable)
  self:Pshort(m, "level_limit", parentTable)
  self:Pshort(m, "status", parentTable)
  self:Pshort(m, "left_times", parentTable)
end
function Paser:Palliance_activity_times_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "id", parentTable)
  self:Pshort(m, "left_times", parentTable)
end
function Paser:Palliance_activity_times_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "times", parentTable, "alliance_activity_times_info")
end
function Paser:Palliance_logs_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "id", parentTable)
end
function Paser:Palliance_logs_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "logs", parentTable, "alliance_log_info")
end
function Paser:Palliance_log_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "time", parentTable)
  self:Pshort(m, "type", parentTable)
  self:PArray(m, "params", parentTable, "string")
end
function Paser:Palliance_aduits_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "alliance_id", parentTable)
  self:Pinteger(m, "index", parentTable)
  self:Pinteger(m, "size", parentTable)
end
function Paser:Palliance_aduits_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "total", parentTable)
  self:Pinteger(m, "index", parentTable)
  self:PArray(m, "members", parentTable, "alliance_aduit_info")
end
function Paser:Palliance_aduit_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "id", parentTable)
  self:Pstring(m, "name", parentTable)
  self:Pshort(m, "level", parentTable)
  self:Pinteger(m, "rank", parentTable)
  self:Pinteger(m, "applied_at", parentTable)
end
function Paser:Palliance_create_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "name", parentTable)
end
function Paser:Palliance_create_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Palliance_info(m, "alliance", parentTable)
end
function Paser:Palliance_create_fail_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "code", parentTable)
end
function Paser:Palliance_delete_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "id", parentTable)
end
function Paser:Palliance_delete_fail_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "code", parentTable)
end
function Paser:Palliance_apply_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "id", parentTable)
end
function Paser:Palliance_apply_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "apply_type", parentTable)
  self:PArray(m, "applied_alliances", parentTable, "string")
end
function Paser:Palliance_apply_fail_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "code", parentTable)
  self:Pinteger(m, "limit_level", parentTable)
end
function Paser:Palliance_unapply_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "id", parentTable)
end
function Paser:Palliance_unapply_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "applied_alliances", parentTable, "string")
end
function Paser:Palliance_unapply_fail_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "code", parentTable)
end
function Paser:Palliance_ratify_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "ids", parentTable, "string")
end
function Paser:Palliance_ratify_result(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "user_name", parentTable)
  self:Pshort(m, "code", parentTable)
end
function Paser:Palliance_ratify_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "total", parentTable)
  self:PArray(m, "result", parentTable, "alliance_ratify_result")
end
function Paser:Palliance_refuse_result(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "user_name", parentTable)
  self:Pshort(m, "code", parentTable)
end
function Paser:Palliance_refuse_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "total", parentTable)
  self:PArray(m, "result", parentTable, "alliance_refuse_result")
end
function Paser:Palliance_refuse_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "ids", parentTable, "string")
end
function Paser:Palliance_demote_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "user_id", parentTable)
end
function Paser:Palliance_demote_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "user_id", parentTable)
end
function Paser:Palliance_demote_fail_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "code", parentTable)
end
function Paser:Palliance_promote_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "user_id", parentTable)
end
function Paser:Palliance_promote_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "user_id", parentTable)
end
function Paser:Palliance_promote_fail_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "code", parentTable)
end
function Paser:Palliance_kick_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "user_id", parentTable)
end
function Paser:Palliance_kick_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "total", parentTable)
  self:Pstring(m, "user_name", parentTable)
end
function Paser:Palliance_kick_fail_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "code", parentTable)
end
function Paser:Palliance_transfer_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "user_id", parentTable)
end
function Paser:Palliance_transfer_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "user_id", parentTable)
end
function Paser:Palliance_transfer_fail_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "code", parentTable)
end
function Paser:Palliance_quit_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "user_id", parentTable)
end
function Paser:Palliance_quit_fail_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "code", parentTable)
end
function Paser:Pfleet_damage_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pinteger(m, "damage", parentTable)
end
function Paser:Pbattle_result_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pbattle_result(m, "result", parentTable)
  self:Pfight_report(m, "report", parentTable)
  self:PArray(m, "fleets", parentTable, "fleet_damage_info")
end
function Paser:Prush_result_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "rewards", parentTable, "game_item")
  self:PArray(m, "fleets", parentTable, "fleet_damage_info")
  self:Pinteger(m, "strike", parentTable)
end
function Paser:Padd_friend_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pfriend_info(m, "friend", parentTable)
end
function Paser:Padd_on(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "type", parentTable)
  self:Pinteger(m, "value", parentTable)
  self:Pinteger(m, "next_value", parentTable)
end
function Paser:Pformation_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "formation_id", parentTable)
  self:Pinteger(m, "fleet_id", parentTable)
  self:Pshort(m, "slot", parentTable)
end
function Paser:Pkrypton_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "id", parentTable)
  self:Pshort(m, "level", parentTable)
  self:Pshort(m, "rank", parentTable)
  self:Pshort(m, "status", parentTable)
  self:Pinteger(m, "item_type", parentTable)
  self:Pinteger(m, "decompose_kenergy", parentTable)
  self:Pinteger(m, "kenergy_upgrade", parentTable)
  self:Pshort(m, "slot", parentTable)
  self:PArray(m, "addon", parentTable, "add_on")
  self:Pboolean(m, "max_level", parentTable)
  self:Pinteger(m, "refine_exp", parentTable)
  self:Pinteger(m, "inject_exp", parentTable)
  self:Pinteger(m, "refine_need_exp", parentTable)
  self:Pinteger(m, "refine_need_kenergy", parentTable)
  self:PArray(m, "formation", parentTable, "formation_info")
end
function Paser:Pkrypton_store_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "formation_id", parentTable)
end
function Paser:Pkrypton_store_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "grid_capacity", parentTable)
  self:PArray(m, "kryptons", parentTable, "krypton_info")
  self:Pinteger(m, "klevel", parentTable)
  self:Pinteger(m, "gacha_need_money", parentTable)
  self:Pinteger(m, "gacha_need_credit", parentTable)
  self:Pinteger(m, "use_credit_count", parentTable)
  self:Pboolean(m, "is_high", parentTable)
end
function Paser:Pkrypton_enhance_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "id", parentTable)
  self:Pinteger(m, "fleet_identity", parentTable)
  self:Pinteger(m, "formation_id", parentTable)
end
function Paser:Pkrypton_enhance_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pkrypton_info(m, "krypton", parentTable)
end
function Paser:Pkrypton_equip_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "formation_id", parentTable)
  self:Pinteger(m, "fleet_identity", parentTable)
  self:Pstring(m, "krypton_id", parentTable)
  self:Pinteger(m, "slot", parentTable)
end
function Paser:Pkrypton_equip_off_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "formation_id", parentTable)
  self:Pinteger(m, "fleet_identity", parentTable)
  self:Pstring(m, "krypton_id", parentTable)
  self:Pinteger(m, "slot", parentTable)
end
function Paser:Pkrypton_equip_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pfleet(m, "fleet_info", parentTable)
  self:PArray(m, "kryptons", parentTable, "krypton_info")
end
function Paser:Pkrypton_gacha_one_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "money_or_krypton", parentTable)
  self:Pinteger(m, "money", parentTable)
  self:Pkrypton_info(m, "krypton", parentTable)
  self:Pinteger(m, "klevel", parentTable)
  self:Pboolean(m, "is_high", parentTable)
  self:Pinteger(m, "gacha_need_money", parentTable)
  self:Pinteger(m, "gacha_need_credit", parentTable)
  self:Pinteger(m, "increase_kenergy", parentTable)
  self:Pinteger(m, "use_credit_count", parentTable)
end
function Paser:Pkrypton_gacha_some_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "auto_decompose", parentTable)
  self:Pboolean(m, "use_credit", parentTable)
end
function Paser:Pkrypton_gacha_one_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "auto_decompose", parentTable)
  self:Pboolean(m, "use_credit", parentTable)
end
function Paser:Pkrypton_gacha_some_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "kryptons", parentTable, "krypton_info")
  self:Pinteger(m, "klevel", parentTable)
  self:Pboolean(m, "is_high", parentTable)
  self:Pinteger(m, "gacha_need_money", parentTable)
  self:Pinteger(m, "gacha_need_credit", parentTable)
  self:Pinteger(m, "increase_kenergy", parentTable)
  self:Pinteger(m, "increase_money", parentTable)
  self:Pinteger(m, "use_credit_count", parentTable)
end
function Paser:Pkrypton_decompose_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "krypton_type", parentTable)
  self:Pstring(m, "id", parentTable)
end
function Paser:Pkrypton_decompose_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "increase_kenergy", parentTable)
end
function Paser:Pkrypton_move_to_store_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "id", parentTable)
end
function Paser:Palliance_memo_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "content", parentTable)
end
function Paser:Palliance_memo_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Palliance_info(m, "alliance", parentTable)
end
function Paser:Palliance_memo_fail_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "code", parentTable)
end
function Paser:Palliance_flag_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "flag", parentTable)
end
function Paser:Puser_champion_data(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "user_id", parentTable)
  self:Pinteger(m, "rank", parentTable)
  self:Pstring(m, "name", parentTable)
  self:Pinteger(m, "icon", parentTable)
  self:Pinteger(m, "level", parentTable)
  self:Pdouble(m, "force", parentTable)
  self:PArray(m, "cached_fleets", parentTable, "cached_fleet")
end
function Paser:Pchampion_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "rank", parentTable)
  self:Pshort(m, "max_challenge_cnt", parentTable)
  self:Pshort(m, "challenge_cnt", parentTable)
  self:Pinteger(m, "next_fight_time", parentTable)
  self:Pboolean(m, "award", parentTable)
  self:Pinteger(m, "next_award_time", parentTable)
  self:Pboolean(m, "straight_award", parentTable)
  self:Pinteger(m, "straight_times", parentTable)
  self:Pinteger(m, "exchange_cost", parentTable)
end
function Paser:Pchampion_list_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pchampion_info(m, "brief_info", parentTable)
  self:PArray(m, "users", parentTable, "user_champion_data")
  self:Puser_champion_data(m, "top_user", parentTable)
end
function Paser:Pchampion_challenge_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "rank", parentTable)
end
function Paser:Pchampion_challenge_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "code", parentTable)
  self:Pshort(m, "result", parentTable)
  self:Pchampion_info(m, "brief_info", parentTable)
  self:PArray(m, "reports", parentTable, "fight_report")
  self:PArray(m, "users", parentTable, "user_champion_data")
  self:PArray(m, "awards", parentTable, "game_item")
end
function Paser:Pchampion_top_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "users", parentTable, "user_champion_data")
end
function Paser:Pchampion_reset_cd_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "code", parentTable)
  self:Pchampion_info(m, "brief_info", parentTable)
end
function Paser:Pchampion_get_award_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "code", parentTable)
  self:Pinteger(m, "rank", parentTable)
  self:PArray(m, "awards", parentTable, "game_item")
  self:Pchampion_info(m, "brief_info", parentTable)
end
function Paser:Pkrypton_store_swap_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "id_from", parentTable)
  self:Pstring(m, "id_to", parentTable)
  self:Pinteger(m, "slot_to", parentTable)
end
function Paser:Pkrypton_fleet_equip_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "formation_id", parentTable)
  self:Pinteger(m, "fleet_identity", parentTable)
end
function Paser:Pfleet_krypton_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "fleet_id", parentTable)
  self:Pinteger(m, "fleet_identity", parentTable)
  self:PArray(m, "kryptons", parentTable, "krypton_info")
end
function Paser:Pfight_history(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "report_id", parentTable)
  self:Pinteger(m, "fight_time", parentTable)
  self:Pfight_report(m, "report", parentTable)
end
function Paser:Pfight_history_with_rank(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "report_id", parentTable)
  self:Pinteger(m, "fight_time", parentTable)
  self:Pinteger(m, "player1_rank", parentTable)
  self:Pinteger(m, "player2_rank", parentTable)
  self:Pinteger(m, "result", parentTable)
  self:Pstring(m, "player1_name", parentTable)
  self:Pstring(m, "player2_name", parentTable)
end
function Paser:Puser_fight_history_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "histories", parentTable, "fight_history_with_rank")
end
function Paser:Puser_brief_info_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "user_id", parentTable)
end
function Paser:Pcached_fleet(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "cell_id", parentTable)
  self:Pinteger(m, "fleet_identity", parentTable)
  self:Pdouble(m, "force", parentTable)
  self:Pinteger(m, "krypton", parentTable)
  self:PArray(m, "kryptons", parentTable, "krypton_info")
  self:Pinteger(m, "adjutant_id", parentTable)
  self:PArray(m, "equipments", parentTable, "equipment")
  self:Pinteger(m, "level", parentTable)
end
function Paser:Puser_brief_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "name", parentTable)
  self:Pinteger(m, "level", parentTable)
  self:Pdouble(m, "force", parentTable)
  self:Pshort(m, "icon", parentTable)
  self:Pinteger(m, "krypton", parentTable)
  self:Pstring(m, "alliance", parentTable)
  self:Pstring(m, "alliance_id", parentTable)
  self:Pshort(m, "position", parentTable)
  self:Pinteger(m, "rank", parentTable)
  self:PArray(m, "cached_fleets", parentTable, "cached_fleet")
end
function Paser:Pkrypton_fleet_move_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "formation_id", parentTable)
  self:Pinteger(m, "fleet_identity", parentTable)
  self:Pstring(m, "krypton_id", parentTable)
  self:Pinteger(m, "dest_slot", parentTable)
end
function Paser:Puser_challenged_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "player1", parentTable)
  self:Pstring(m, "player2", parentTable)
  self:Pinteger(m, "result", parentTable)
  self:Pinteger(m, "rank", parentTable)
end
function Paser:Pshop_buy_and_wear_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "formation_id", parentTable)
  self:Pstring(m, "fleet_id", parentTable)
  self:Pinteger(m, "equip_type", parentTable)
end
function Paser:Palliance_info_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Palliance_pos_and_id(m, "alliance", parentTable)
end
function Paser:Ptask_reward(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "reward_type", parentTable)
  self:Pinteger(m, "number", parentTable)
end
function Paser:Psub_task_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pstring(m, "action", parentTable)
  self:Pinteger(m, "number", parentTable)
  self:Pinteger(m, "finish_count", parentTable)
  self:Pboolean(m, "is_get_reward", parentTable)
  self:Pboolean(m, "is_finished", parentTable)
  self:Pinteger(m, "player_level", parentTable)
  self:PArray(m, "rewards", parentTable, "game_item")
  self:Pboolean(m, "is_enabled", parentTable)
end
function Paser:Ptask_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pstring(m, "task_name", parentTable)
  self:Pboolean(m, "is_get_reward", parentTable)
  self:Pboolean(m, "is_finished", parentTable)
  self:PArray(m, "rewards", parentTable, "game_item")
  self:PArray(m, "task_content", parentTable, "sub_task_info")
  self:Pshort(m, "reward_number", parentTable)
end
function Paser:Ptask_list_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "task_list", parentTable, "task_info")
end
function Paser:Pget_task_reward_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "task_id", parentTable)
end
function Paser:Pprestige_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "prestige_level", parentTable)
  self:Pinteger(m, "prestige_require", parentTable)
  self:Pboolean(m, "obtained", parentTable)
  self:Pinteger(m, "award_money", parentTable)
  self:Pinteger(m, "award_fleet", parentTable)
  self:Pinteger(m, "award_tech", parentTable)
  self:Pshort(m, "recruit_cnt", parentTable)
  self:Pshort(m, "fight_fleet_cnt", parentTable)
  self:Pshort(m, "new_matrix_cell", parentTable)
  self:Pinteger(m, "crit_lv", parentTable)
  self:Pinteger(m, "motility", parentTable)
  self:Pinteger(m, "hit_rate", parentTable)
  self:Pinteger(m, "anti_crit", parentTable)
  self:PArray(m, "shared_rewards", parentTable, "game_item")
end
function Paser:Pprestige_info_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "prestige", parentTable)
  self:PArray(m, "prestige_infos", parentTable, "prestige_info")
end
function Paser:Pprestige_complete_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pprestige_info(m, "complete_prestige", parentTable)
end
function Paser:Pprestige_obtain_award_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "prestige_level", parentTable)
end
function Paser:Pprestige_obtain_award_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "code", parentTable)
end
function Paser:Pachievement_type_list(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "classify_list", parentTable, "string")
end
function Paser:Pachievement_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pstring(m, "classify", parentTable)
  self:Pinteger(m, "number", parentTable)
  self:Pinteger(m, "finish_count", parentTable)
  self:Pinteger(m, "finish_time", parentTable)
  self:Pshort(m, "is_finished", parentTable)
  self:Pshort(m, "level", parentTable)
  self:Pstring(m, "group", parentTable)
  self:Pinteger(m, "credit", parentTable)
  self:Pboolean(m, "can_show", parentTable)
  self:PArray(m, "rewards", parentTable, "game_item")
  self:Pinteger(m, "weight", parentTable)
end
function Paser:Pachievement_list_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "classify", parentTable)
end
function Paser:Pachievement_list_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "achievement_list", parentTable, "achievement_info")
end
function Paser:Pachievement_classes_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "classify_list", parentTable, "string")
end
function Paser:Pachievement_finish_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pinteger(m, "level", parentTable)
  self:PArray(m, "awards", parentTable, "game_item")
  self:PArray(m, "shared_rewards", parentTable, "game_item")
end
function Paser:Pachievement_overview_item(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "level", parentTable)
  self:Pinteger(m, "number", parentTable)
  self:Pinteger(m, "finish_count", parentTable)
end
function Paser:Pachievement_overview_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "total_number", parentTable)
  self:Pinteger(m, "total_finish_count", parentTable)
  self:PArray(m, "items", parentTable, "achievement_overview_item")
end
function Paser:Pfleet_kryptons_array(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "fleetkryptons", parentTable, "fleet_krypton_info")
end
function Paser:Psupply_notify(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "type", parentTable)
  self:Pinteger(m, "current", parentTable)
  self:Pinteger(m, "max", parentTable)
end
function Paser:Psupply_type(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "type", parentTable)
end
function Paser:Pequipments_update_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "delete_list", parentTable, "string")
  self:PArray(m, "update_list", parentTable, "equipment")
end
function Paser:Pattribute_change_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "key", parentTable)
  self:Pinteger(m, "value", parentTable)
end
function Paser:Pattributes_change_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "attributes", parentTable, "attribute_change_info")
end
function Paser:Pact_status_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "act_id", parentTable)
end
function Paser:Pchapter_status(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pinteger(m, "level", parentTable)
end
function Paser:Pact_status_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "status", parentTable, "chapter_status")
end
function Paser:Pall_act_status_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "act_no", parentTable)
end
function Paser:Pall_act_status_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "acts", parentTable, "act_status_ack")
end
function Paser:Psupply_info_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "type", parentTable)
  self:Pinteger(m, "exchange_cost", parentTable)
  self:Pinteger(m, "count", parentTable)
  self:Pinteger(m, "left", parentTable)
end
function Paser:Pcdtimes_clear_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "cdtype", parentTable)
end
function Paser:Psystem_configuration_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "config_attrs", parentTable, "string")
end
function Paser:Pconfiguration_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "config_attr", parentTable)
  self:Pstring(m, "config_value", parentTable)
end
function Paser:Psystem_configuration_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "configs", parentTable, "configuration_info")
end
function Paser:Ppay_verify_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "platform", parentTable)
  self:Pstring(m, "pay_type", parentTable)
  self:Pstring(m, "token", parentTable)
  self:Pstring(m, "signature", parentTable)
end
function Paser:Ppay_verify2_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "id", parentTable)
  self:Pstring(m, "payment_type", parentTable)
  self:PArray(m, "params", parentTable, "string")
end
function Paser:Ppay_verify2_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "code", parentTable)
  self:Pinteger(m, "id", parentTable)
  self:Pstring(m, "payment_url", parentTable)
end
function Paser:Ppay_list_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "platform", parentTable)
end
function Paser:Ppay_list2_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "client", parentTable)
end
function Paser:Ppayment_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "id", parentTable)
  self:Pinteger(m, "price", parentTable)
  self:Pstring(m, "currency_type", parentTable)
end
function Paser:Pproduction_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "id", parentTable)
  self:Pinteger(m, "price", parentTable)
  self:Pinteger(m, "credit", parentTable)
  self:PArray(m, "payments", parentTable, "payment_info")
  self:Pinteger(m, "is_push", parentTable)
end
function Paser:Pword_font(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "title_font", parentTable)
  self:Pstring(m, "end_font", parentTable)
  self:Pstring(m, "time_font", parentTable)
  self:Pstring(m, "item_font", parentTable)
  self:Pstring(m, "discount_font", parentTable)
end
function Paser:Pcredit_activity_item(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "activity_name", parentTable)
  self:Pstring(m, "production_id", parentTable)
  self:Pinteger(m, "credit", parentTable)
  self:Pinteger(m, "credit_privilege", parentTable)
  self:PArray(m, "gift_items", parentTable, "game_item")
  self:Pinteger(m, "count", parentTable)
  self:Pinteger(m, "end_time", parentTable)
  self:Pinteger(m, "times", parentTable)
  self:Pstring(m, "icon", parentTable)
  self:Pinteger(m, "top", parentTable)
  self:Pstring(m, "origin_price", parentTable)
  self:Pinteger(m, "discount_rate", parentTable)
  self:Pstring(m, "background1", parentTable)
  self:Pstring(m, "background2", parentTable)
  self:Pword_font(m, "word", parentTable)
end
function Paser:Ppay_list_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pboolean(m, "switch", parentTable)
  self:PArray(m, "pay_types", parentTable, "string")
end
function Paser:Ppay_list2_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "mycard_type", parentTable)
  self:PArray(m, "activity_items", parentTable, "credit_activity_item")
  self:PArray(m, "productions", parentTable, "production_info")
end
function Paser:Psign_item(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "days", parentTable)
  self:Pinteger(m, "status", parentTable)
  self:PArray(m, "rewards", parentTable, "game_item")
  self:Pinteger(m, "vip", parentTable)
  self:Pinteger(m, "vip_crit", parentTable)
end
function Paser:Psign_activite(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "accumulate", parentTable)
  self:Pinteger(m, "total", parentTable)
  self:Pinteger(m, "left_time", parentTable)
  self:PArray(m, "rewards", parentTable, "game_item")
  self:PArray(m, "item", parentTable, "sign_item")
  self:Pinteger(m, "status", parentTable)
  self:PArray(m, "recommands", parentTable, "integer")
  self:Pboolean(m, "share_friends", parentTable)
  self:Pboolean(m, "is_first_day", parentTable)
  self:Pinteger(m, "type", parentTable)
end
function Paser:Psign_activity_reward_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "day", parentTable)
end
function Paser:Ptipoff_content(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "sender", parentTable)
  self:Pstring(m, "content", parentTable)
  self:Pshort(m, "inappropriate", parentTable)
end
function Paser:Pcreate_tipoff_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "wrongdoer", parentTable)
  self:Pstring(m, "reason", parentTable)
  self:PArray(m, "evidences", parentTable, "tipoff_content")
end
function Paser:Pserver_kick(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "action", parentTable)
end
function Paser:Punlock_item(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "type", parentTable)
  self:Pstring(m, "content", parentTable)
end
function Paser:Plevel_up_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "unlockes", parentTable, "unlock_item")
end
function Paser:Pprice_request(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "price_type", parentTable)
  self:Pinteger(m, "type", parentTable)
end
function Paser:Pprice_response(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "price_type", parentTable)
  self:Pinteger(m, "price", parentTable)
end
function Paser:Pspecial_battle_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pfight_report(m, "report", parentTable)
end
function Paser:Pbattle_money_collect_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "money", parentTable)
end
function Paser:Pquest_loot_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "quest_id", parentTable)
end
function Paser:Pevent_loot_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "loot", parentTable, "game_item")
  self:Pinteger(m, "event_type", parentTable)
end
function Paser:Pbattle_event_status_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "loot", parentTable, "game_item")
  self:Pinteger(m, "event_type", parentTable)
end
function Paser:Padv_chapter(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "chapter_id", parentTable)
end
function Paser:Padv_battle(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "battle_id", parentTable)
end
function Paser:Ploud_cast(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "name", parentTable)
  self:Pinteger(m, "action", parentTable)
  self:Pinteger(m, "value", parentTable)
  self:Pstring(m, "valuestr", parentTable)
  self:Pboolean(m, "show_in_chat", parentTable)
  self:Pboolean(m, "show_in_panel", parentTable)
  self:Pstring(m, "local_msg", parentTable)
end
function Paser:Pmine_refresh_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "refresh_type", parentTable)
end
function Paser:Pmine_digg_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "idx", parentTable)
end
function Paser:Pmine_atk_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "user_id", parentTable)
end
function Paser:Pminer_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "user_id", parentTable)
  self:Pstring(m, "name", parentTable)
  self:Pstring(m, "alliance", parentTable)
  self:Pshort(m, "level", parentTable)
  self:Pinteger(m, "force_interval", parentTable)
  self:Pinteger(m, "atk_cd", parentTable)
  self:Pinteger(m, "icon", parentTable)
  self:Pshort(m, "mine_level", parentTable)
  self:Pshort(m, "robbed_cnt", parentTable)
  self:Pshort(m, "robbed_max", parentTable)
  self:Pinteger(m, "start_time", parentTable)
  self:Pinteger(m, "end_time", parentTable)
  self:PArray(m, "award", parentTable, "game_item")
  self:PArray(m, "atk_award", parentTable, "game_item")
  self:Pinteger(m, "main_fleet_level", parentTable)
end
function Paser:Pmine_list_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "miners", parentTable, "miner_info")
  self:PArray(m, "revenge_miners", parentTable, "miner_info")
end
function Paser:Pmine_pool_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "level", parentTable)
  self:Pinteger(m, "time", parentTable)
  self:PArray(m, "awards", parentTable, "game_item")
end
function Paser:Pmine_refresh_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "mine_area", parentTable)
  self:Pmine_pool_info(m, "pool1", parentTable)
  self:Pmine_pool_info(m, "pool2", parentTable)
  self:Pmine_pool_info(m, "pool3", parentTable)
  self:Pmine_pool_info(m, "pool4", parentTable)
end
function Paser:Pmine_info_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "digg_cnt", parentTable)
  self:Pinteger(m, "atk_max_resource", parentTable)
  self:Pinteger(m, "atk_now_resource", parentTable)
  self:Pshort(m, "boost_cnt", parentTable)
  self:Pshort(m, "boost_max_cnt", parentTable)
  self:Pinteger(m, "boost_rest_time", parentTable)
  self:Pinteger(m, "atk_cost", parentTable)
  self:Pmine_refresh_ntf(m, "mine_pool", parentTable)
  self:PArray(m, "self_info", parentTable, "miner_info")
end
function Paser:Pmine_complete_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "basic_award", parentTable, "game_item")
  self:Pinteger(m, "extra_techniques", parentTable)
  self:Pinteger(m, "extra_credit", parentTable)
  self:Pinteger(m, "extra_item", parentTable)
end
function Paser:Pmine_atk_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "atk_id", parentTable)
  self:Pstring(m, "def_id", parentTable)
  self:Pshort(m, "result", parentTable)
  self:Pminer_info(m, "def_info", parentTable)
  self:Pshort(m, "mine_level", parentTable)
  self:PArray(m, "atk_award", parentTable, "game_item")
  self:PArray(m, "reports", parentTable, "fight_report")
end
function Paser:Ppve_supply_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "x_supply", parentTable)
end
function Paser:Pguide_progress(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "progress", parentTable)
end
function Paser:Psupply_loot_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pboolean(m, "enable", parentTable)
end
function Paser:Psupply_loot_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "money", parentTable)
  self:Pinteger(m, "technique", parentTable)
  self:Pinteger(m, "laba_supply", parentTable)
  self:Pinteger(m, "battle_supply", parentTable)
end
function Paser:Plaba_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "pos1", parentTable)
  self:Pshort(m, "pos2", parentTable)
  self:Pshort(m, "pos3", parentTable)
  self:Pshort(m, "rate", parentTable)
  self:PArray(m, "awards", parentTable, "game_item")
end
function Paser:Pplayer_center_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "timepoint", parentTable)
  self:Pinteger(m, "ntf_type", parentTable)
  self:PArray(m, "ntf_content", parentTable, "string")
end
function Paser:Pdexter_lib_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "dexter_name", parentTable)
  self:Pshort(m, "used_cnt", parentTable)
  self:Pinteger(m, "count_down", parentTable)
end
function Paser:Pdexter_libs_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "libs", parentTable, "dexter_lib_info")
end
function Paser:Pchat_error_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "code", parentTable)
  self:Pstring(m, "content", parentTable)
  self:Pstring(m, "receiver", parentTable)
  self:Pstring(m, "channel", parentTable)
end
function Paser:Pplayer_force(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "user_id", parentTable)
  self:Pinteger(m, "level", parentTable)
  self:Pstring(m, "name", parentTable)
  self:Pdouble(m, "force", parentTable)
end
function Paser:Ptop_force_list_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "players", parentTable, "player_force")
end
function Paser:Pclient_ver(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "market", parentTable)
  self:Pinteger(m, "version", parentTable)
end
function Paser:Prefresh_time_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pboolean(m, "flag", parentTable)
  self:Pinteger(m, "next_time", parentTable)
  self:Pinteger(m, "wd_next_time", parentTable)
end
function Paser:Pac_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "ac_level", parentTable)
  self:Pshort(m, "ac_step", parentTable)
  self:Pinteger(m, "ac_supply", parentTable)
  self:Pinteger(m, "ac_energy", parentTable)
  self:Pinteger(m, "ac_energy_level", parentTable)
  self:Pshort(m, "jam", parentTable)
  self:Pinteger(m, "refresh_time", parentTable)
  self:Pinteger(m, "jam_cost", parentTable)
  self:Pshort(m, "can_sweep", parentTable)
end
function Paser:Pac_energy_charge_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "increase_energy", parentTable)
  self:Pshort(m, "strike", parentTable)
end
function Paser:Pgateway_info_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "notices", parentTable, "integer")
  self:PArray(m, "activities", parentTable, "integer")
end
function Paser:Pnotice_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "type", parentTable)
  self:Pinteger(m, "id", parentTable)
end
function Paser:Pblock_devil_info_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
end
function Paser:Pgateway_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "notices", parentTable, "integer")
  self:PArray(m, "activities", parentTable, "integer")
end
function Paser:Pblock_devil_player(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "name", parentTable)
  self:Pinteger(m, "state", parentTable)
end
function Paser:Pblock_devil_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "type", parentTable)
end
function Paser:Pblock_devil_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "players", parentTable, "block_devil_player")
  self:PArray(m, "award", parentTable, "game_item")
  self:Pinteger(m, "prestige", parentTable)
end
function Paser:Pblock_devil_complete_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "code", parentTable)
  self:PArray(m, "award", parentTable, "game_item")
end
function Paser:Pdonate_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "donate_type", parentTable)
end
function Paser:Pdonate_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "donate_type", parentTable)
  self:PArray(m, "donate_package", parentTable, "donate_package_info")
  self:Pinteger(m, "alliance_donate_level", parentTable)
  self:Pinteger(m, "alliance_donate_exp", parentTable)
  self:Pinteger(m, "alliance_donate_lv_exp", parentTable)
  self:PArray(m, "donate_record", parentTable, "alliance_log_info")
  self:Pinteger(m, "alliance_donate_exp_add", parentTable)
end
function Paser:Pdonate_info_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "donate_type", parentTable)
  self:PArray(m, "donate_package", parentTable, "donate_package_info")
  self:Pinteger(m, "alliance_donate_level", parentTable)
  self:Pinteger(m, "alliance_donate_exp", parentTable)
  self:Pinteger(m, "alliance_donate_lv_exp", parentTable)
  self:PArray(m, "donate_record", parentTable, "alliance_log_info")
end
function Paser:Pdonate_package_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "exp", parentTable)
  self:Pinteger(m, "prestige", parentTable)
  self:Pstring(m, "cost_type", parentTable)
  self:Pinteger(m, "cost_value", parentTable)
  self:Pinteger(m, "vip_level", parentTable)
  self:Pinteger(m, "brick", parentTable)
end
function Paser:Palliance_weekend_award_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "money", parentTable)
  self:Pinteger(m, "prestige", parentTable)
end
function Paser:Palliance_defence_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "leader_id", parentTable)
  self:Pshort(m, "leader_hp", parentTable)
  self:Pinteger(m, "leader_hp_limit", parentTable)
  self:Pstring(m, "leader_name", parentTable)
  self:Pinteger(m, "hp", parentTable)
  self:Pinteger(m, "hp_limit", parentTable)
  self:Pshort(m, "level", parentTable)
  self:Pinteger(m, "brick", parentTable)
  self:Pinteger(m, "point", parentTable)
end
function Paser:Palliance_defence_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Palliance_defence_info(m, "info", parentTable)
end
function Paser:Palliance_domination_defence_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "left_seconds", parentTable)
  self:Pinteger(m, "leader_id", parentTable)
  self:Pstring(m, "leader_name", parentTable)
  self:Pinteger(m, "leader_hp", parentTable)
  self:Pinteger(m, "leader_hp_limit", parentTable)
  self:Pinteger(m, "hp", parentTable)
  self:Pinteger(m, "hp_limit", parentTable)
  self:Pshort(m, "level", parentTable)
end
function Paser:Palliance_domination_defence_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Palliance_domination_defence_info(m, "info", parentTable)
end
function Paser:Palliance_set_defender_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "user_id", parentTable)
end
function Paser:Palliance_defence_donate_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "brick", parentTable)
end
function Paser:Pdomination_battle_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "user_id", parentTable)
  self:Pboolean(m, "is_defender", parentTable)
  self:Pshort(m, "win_count", parentTable)
  self:Pinteger(m, "point", parentTable)
  self:Pshort(m, "lose_count", parentTable)
  self:Pinteger(m, "attk_count", parentTable)
end
function Paser:Palliance_domination_battle_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "users", parentTable, "domination_battle_info")
end
function Paser:Palliance_domination_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pstring(m, "name", parentTable)
  self:Pstring(m, "server_name", parentTable)
  self:Pinteger(m, "level", parentTable)
  self:Pshort(m, "flag", parentTable)
  self:PArray(m, "members", parentTable, "alliance_member_info")
end
function Paser:Palliance_domination_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "status", parentTable)
  self:Pinteger(m, "cost_seconds", parentTable)
  self:PArray(m, "infoes", parentTable, "alliance_domination_info")
end
function Paser:Pdomination_challenge_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pboolean(m, "once", parentTable)
  self:Pboolean(m, "leap", parentTable)
end
function Paser:Pdomination_join_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pboolean(m, "is_local_matched", parentTable)
end
function Paser:Pdomination_challenge_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "result", parentTable)
  self:Pinteger(m, "id", parentTable)
  self:PArray(m, "reports", parentTable, "fight_report")
  self:PArray(m, "rewards", parentTable, "game_item")
  self:PArray(m, "def_rewards", parentTable, "game_item")
end
function Paser:Plogin_first_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pboolean(m, "enable", parentTable)
end
function Paser:Plogin_time_current_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "login_time_current", parentTable)
  self:Pinteger(m, "login_time_current_utc", parentTable)
  self:Pinteger(m, "google_trans_tag", parentTable)
  self:Pinteger(m, "mail_trans_tag", parentTable)
end
function Paser:Pchampion_reward_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "money", parentTable)
  self:Pinteger(m, "prestige", parentTable)
  self:Pinteger(m, "credit", parentTable)
end
function Paser:Pchampion_rank_reward_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pchampion_reward_info(m, "last_loot", parentTable)
  self:Pinteger(m, "last_rank", parentTable)
  self:PArray(m, "champion_reward", parentTable, "champion_reward_info")
end
function Paser:Pworld_boss_info_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pinteger(m, "level", parentTable)
  self:Pdouble(m, "hp", parentTable)
  self:Pdouble(m, "total_hp", parentTable)
  self:Pinteger(m, "motility", parentTable)
  self:Pinteger(m, "intercept", parentTable)
  self:Pinteger(m, "cd", parentTable)
  self:Pinteger(m, "clean_cd_cost", parentTable)
  self:Pinteger(m, "end_time", parentTable)
  self:PArray(m, "toplist", parentTable, "world_boss_toplist_player")
  self:Pstring(m, "kill_1st", parentTable)
  self:Pstring(m, "kill_2nd", parentTable)
  self:Pstring(m, "kill_3rd", parentTable)
  self:Pstring(m, "final_kill", parentTable)
end
function Paser:Pworld_boss_toplist_player(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "top", parentTable)
  self:Pinteger(m, "player_id", parentTable)
  self:Pstring(m, "player_name", parentTable)
  self:Pdouble(m, "destroy_hp", parentTable)
end
function Paser:Pchallenge_world_boss_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pfight_report(m, "report", parentTable)
  self:PArray(m, "award", parentTable, "game_item")
end
function Paser:Padd_world_boss_force_rate_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "rate", parentTable)
  self:Pinteger(m, "cost", parentTable)
  self:Pboolean(m, "success", parentTable)
end
function Paser:Pready_world_boss_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pboolean(m, "can_interrupt", parentTable)
end
function Paser:Pready_world_boss_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "countdown", parentTable)
  self:Pinteger(m, "force_rate", parentTable)
  self:Pinteger(m, "force_rate_cost", parentTable)
  self:Pinteger(m, "current_level", parentTable)
end
function Paser:Pfirst_purchase(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "items", parentTable, "game_item")
  self:Pinteger(m, "status", parentTable)
  self:Pdouble(m, "cost", parentTable)
end
function Paser:Plevel_loot(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "status", parentTable)
  self:Pinteger(m, "level", parentTable)
  self:PArray(m, "loots", parentTable, "game_item")
end
function Paser:Plevel_loots(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "levels", parentTable, "level_loot")
  self:Pinteger(m, "level", parentTable)
  self:PArray(m, "kryptons", parentTable, "krypton_info")
end
function Paser:Pactive_gifts_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "items", parentTable, "active_gifts_info")
end
function Paser:Pactive_gifts_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "gift_type", parentTable)
  self:Pinteger(m, "mark", parentTable)
  self:Pinteger(m, "times", parentTable)
  self:PArray(m, "gifts", parentTable, "game_item")
end
function Paser:Pactive_gifts_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "type", parentTable)
end
function Paser:Plevel_loot_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "level", parentTable)
end
function Paser:Ppromotion_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "id", parentTable)
  self:Pboolean(m, "enabled", parentTable)
  self:Pinteger(m, "left_seconds", parentTable)
  self:Pshort(m, "tab", parentTable)
  self:PArray(m, "news", parentTable, "promotion_news_info")
end
function Paser:Ppromotions_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "promotions", parentTable, "promotion_info")
end
function Paser:Ppromotion_award_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "id", parentTable)
  self:Pinteger(m, "condition", parentTable)
end
function Paser:Ppromotion_award_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "awards", parentTable, "game_item")
end
function Paser:Ppromotion_detail_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "id", parentTable)
  self:Pshort(m, "index", parentTable)
end
function Paser:Ppromotion_section_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "awards", parentTable, "game_item")
  self:Pshort(m, "status", parentTable)
  self:Pinteger(m, "condition", parentTable)
end
function Paser:Ppromotion_detail_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "value", parentTable)
  self:PArray(m, "sections", parentTable, "promotion_section_info")
end
function Paser:Pallstars_reward_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "charpter_id", parentTable)
end
function Paser:Pallstars_reward_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "rewards", parentTable, "game_item")
end
function Paser:Ppromotion_news_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "id", parentTable)
  self:Pinteger(m, "news_id", parentTable)
  self:Pboolean(m, "can_show", parentTable)
  self:Pinteger(m, "left_seconds", parentTable)
end
function Paser:Ptimelimit_hero_id_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "fleet_id", parentTable, "fleet_sale_with_rebate_info")
end
function Paser:Pfleet_sale_with_rebate_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "fleet_id", parentTable)
  self:Pinteger(m, "deposit_rate", parentTable)
  self:Pinteger(m, "lepton_req", parentTable)
  self:Pinteger(m, "quark_req", parentTable)
  self:Pinteger(m, "left_time", parentTable)
end
function Paser:Pfleet_sale_with_rebate_list_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "fleet_info_list", parentTable, "fleet_sale_with_rebate_info")
end
function Paser:Pcut_off_list_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "cutoff_list", parentTable, "cut_off_card")
end
function Paser:Pcut_off_card(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pinteger(m, "credit", parentTable)
  self:Pinteger(m, "credit_eachday", parentTable)
end
function Paser:Pbuy_cut_off_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
end
function Paser:Ppromotion_award_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "news_ids", parentTable, "integer")
end
function Paser:Pnew_activities_compare_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "notices", parentTable, "integer")
  self:PArray(m, "activities", parentTable, "integer")
end
function Paser:Pactivities_button_status_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pboolean(m, "status", parentTable)
end
function Paser:Pkrypton_fragment_core_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "code", parentTable)
end
function Paser:Pactivities_button_first_status_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "activities", parentTable, "integer")
end
function Paser:Pstores_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "store_id", parentTable)
  self:Pstring(m, "locale", parentTable)
end
function Paser:Pstore_item(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pgame_item(m, "item", parentTable)
  self:Pinteger(m, "price", parentTable)
  self:Pinteger(m, "price_type", parentTable)
  self:Pinteger(m, "discount", parentTable)
  self:Pinteger(m, "time_left", parentTable)
  self:Pinteger(m, "day_buy_left", parentTable)
  self:Pinteger(m, "all_buy_left", parentTable)
  self:Pinteger(m, "level_limit", parentTable)
  self:Pinteger(m, "vip_limit", parentTable)
  self:Pinteger(m, "state", parentTable)
  self:Pinteger(m, "number", parentTable)
  self:Pinteger(m, "max_number", parentTable)
  self:Pinteger(m, "status", parentTable)
  self:Pinteger(m, "quick_buy", parentTable)
  self:Pinteger(m, "buy_quantity", parentTable)
  self:Pinteger(m, "refresh_buy_limit", parentTable)
  self:PArray(m, "must_items", parentTable, "game_item")
  self:PArray(m, "maybe_items", parentTable, "game_item")
  self:Pinteger(m, "order", parentTable)
  self:Pinteger(m, "group", parentTable)
  self:PArray(m, "pics", parentTable, "string")
end
function Paser:Pmedal_store_item(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pgame_item(m, "item", parentTable)
  self:Pgame_item(m, "price", parentTable)
  self:Pinteger(m, "discount", parentTable)
  self:Pinteger(m, "buy_times_limit", parentTable)
  self:Pshort(m, "quality", parentTable)
  self:Pinteger(m, "time_left", parentTable)
end
function Paser:Pstore_quick_buy_count_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "count", parentTable)
  self:Pinteger(m, "max_count", parentTable)
  self:Pstring(m, "nor_price", parentTable)
  self:Pstring(m, "dis_price", parentTable)
end
function Paser:Pbuy_count_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pstore_quick_buy_count_ack(m, "count_ack", parentTable)
end
function Paser:Pstores_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "time", parentTable)
  self:PArray(m, "items", parentTable, "store_item")
  self:Pinteger(m, "currency", parentTable)
  self:Pstring(m, "normal", parentTable)
  self:Pboolean(m, "mysterystore", parentTable)
  self:PArray(m, "buy_count_infos", parentTable, "buy_count_info")
  self:Pinteger(m, "price_off_status", parentTable)
end
function Paser:Pstores_refresh_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "time", parentTable)
  self:PArray(m, "items", parentTable, "store_item")
end
function Paser:Pexpedition_stores_refresh_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "time", parentTable)
  self:PArray(m, "items", parentTable, "store_item")
end
function Paser:Pstores_buy_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "store_id", parentTable)
  self:Pinteger(m, "id", parentTable)
  self:Pinteger(m, "buy_times", parentTable)
  self:Pinteger(m, "state", parentTable)
  self:Pinteger(m, "coupon", parentTable)
end
function Paser:Pequipment_action_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "can_evolute", parentTable)
  self:Pshort(m, "can_enhance", parentTable)
  self:Pshort(m, "can_enchant", parentTable)
end
function Paser:Pequipment_action_list_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "equipments", parentTable, "equipment_action_info")
end
function Paser:Pequipment_action_list_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "ids", parentTable, "integer")
end
function Paser:Pmail_to_alliance_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "title", parentTable)
  self:Pstring(m, "content", parentTable)
  self:Pshort(m, "my_send", parentTable)
  self:Pshort(m, "receiver_type", parentTable)
end
function Paser:Pmail_to_all_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "title", parentTable)
  self:Pstring(m, "content", parentTable)
end
function Paser:Pthanksgiven_box_player(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "user_id", parentTable)
  self:Pstring(m, "name", parentTable)
  self:Pinteger(m, "box_cnt", parentTable)
  self:Pinteger(m, "rank", parentTable)
  self:Pstring(m, "alliance_name", parentTable)
end
function Paser:Pthanksgiven_box_alliance(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Palliance_info(m, "alliance", parentTable)
  self:Pinteger(m, "box_cnt", parentTable)
  self:Pinteger(m, "rank", parentTable)
end
function Paser:Pthanksgiven_box_rank(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "players", parentTable, "thanksgiven_box_player")
  self:PArray(m, "alliances", parentTable, "thanksgiven_box_alliance")
  self:Pthanksgiven_box_player(m, "self_info", parentTable)
end
function Paser:Pfestival_item(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "activity_type", parentTable)
  self:Pstring(m, "name", parentTable)
  self:Pinteger(m, "start_time", parentTable)
  self:Pinteger(m, "end_time", parentTable)
  self:Pinteger(m, "current_time", parentTable)
  self:Pinteger(m, "final_time", parentTable)
  self:Pshort(m, "level_req", parentTable)
end
function Paser:Pfestival_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "festivals", parentTable, "festival_item")
end
function Paser:Pverify_redeem_code_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "code", parentTable)
end
function Paser:Pverify_redeem_code_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "awards", parentTable, "game_item")
end
function Paser:Pitem_count(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "item_id", parentTable)
  self:Pinteger(m, "item_no", parentTable)
end
function Paser:Pitems_count_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "items", parentTable, "item_count")
end
function Paser:Pthanksgiven_champion_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "user_id", parentTable)
end
function Paser:Pitem_use_award_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "award_list", parentTable, "game_item_info")
  self:Pinteger(m, "use_max", parentTable)
end
function Paser:Pvip_sign_item_use_award_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "award_list", parentTable, "game_item_info")
  self:Pinteger(m, "use_max", parentTable)
end
function Paser:Paddition_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "locale", parentTable)
end
function Paser:Pcolony_product_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "speed", parentTable)
  self:Pinteger(m, "producted_experience", parentTable)
  self:Pinteger(m, "experience", parentTable)
  self:Pinteger(m, "max_experience", parentTable)
end
function Paser:Pcolony_user_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "player_id", parentTable)
  self:Pstring(m, "name", parentTable)
  self:Pshort(m, "position", parentTable)
  self:Pstring(m, "owner_id", parentTable)
  self:Pinteger(m, "sex", parentTable)
  self:Pinteger(m, "icon", parentTable)
  self:Pinteger(m, "forward_left", parentTable)
  self:Pinteger(m, "reverse_left", parentTable)
  self:Pcolony_product_info(m, "product", parentTable)
  self:Pstring(m, "alliance_name", parentTable)
  self:Pdouble(m, "force", parentTable)
  self:Pshort(m, "level", parentTable)
  self:Pinteger(m, "main_fleet_level", parentTable)
end
function Paser:Pcolony_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "position", parentTable)
  self:PArray(m, "users", parentTable, "colony_user_info")
end
function Paser:Pcolony_info_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pcolony_info(m, "info", parentTable)
end
function Paser:Pcolony_exp(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "experience", parentTable)
  self:Pinteger(m, "producted_experience", parentTable)
  self:Pinteger(m, "max_experience", parentTable)
end
function Paser:Pcolony_exp_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pcolony_exp(m, "ntf", parentTable)
end
function Paser:Pcolony_times_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "type", parentTable)
  self:Pshort(m, "times", parentTable)
  self:Pshort(m, "max_times", parentTable)
end
function Paser:Pcolony_times_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "times", parentTable, "colony_times_info")
end
function Paser:Pcolony_log_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "time", parentTable)
  self:Pshort(m, "type", parentTable)
  self:Pshort(m, "catalog", parentTable)
  self:Pinteger(m, "master_id", parentTable)
  self:Pstring(m, "master_name", parentTable)
  self:Pinteger(m, "slave_id", parentTable)
  self:Pstring(m, "slave_name", parentTable)
  self:Pinteger(m, "third_id", parentTable)
  self:Pstring(m, "third_name", parentTable)
  self:PArray(m, "params", parentTable, "string")
end
function Paser:Pcolony_info_test_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pcolony_info(m, "info", parentTable)
  self:Pcolony_exp(m, "exp", parentTable)
  self:Pcolony_product_info(m, "product", parentTable)
  self:PArray(m, "logs", parentTable, "colony_log_info")
end
function Paser:Pcolony_logs_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "type", parentTable)
  self:PArray(m, "logs", parentTable, "colony_log_info")
end
function Paser:Pcolony_users_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "types", parentTable, "integer")
end
function Paser:Pcolony_users(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "tag", parentTable)
  self:PArray(m, "users", parentTable, "colony_user_info")
end
function Paser:Pcolony_players_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "user_ids", parentTable, "integer")
end
function Paser:Pcolony_players_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "users", parentTable, "colony_user_info")
end
function Paser:Pcolony_users_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "users_list", parentTable, "colony_users")
end
function Paser:Pcolony_challenge_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "fight_type", parentTable)
  self:Pstring(m, "def_id", parentTable)
  self:Pshort(m, "def_pos", parentTable)
  self:Pstring(m, "third_id", parentTable)
end
function Paser:Pcolony_challenge_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "result", parentTable)
  self:Pfight_report(m, "report", parentTable)
end
function Paser:Pcolony_exploit_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "slave_id", parentTable)
  self:Pboolean(m, "pay_enabled", parentTable)
end
function Paser:Pcolony_exploit_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "experience", parentTable)
  self:Pinteger(m, "paid_experience", parentTable)
end
function Paser:Pcolony_release_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "slave_id", parentTable)
end
function Paser:Pcolony_release_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "experience", parentTable)
end
function Paser:Pcolony_fawn_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "action_id", parentTable)
  self:Pstring(m, "receiver_id", parentTable)
  self:Pshort(m, "receiver_pos", parentTable)
end
function Paser:Pcolony_fawn_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "experience", parentTable)
  self:Pinteger(m, "expect_experience", parentTable)
end
function Paser:Pactivity_stores_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "store_type", parentTable)
  self:Pstring(m, "locale", parentTable)
end
function Paser:Pgame_item_id(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "item_id", parentTable)
  self:Pinteger(m, "item_no", parentTable)
  self:Pinteger(m, "has_num", parentTable)
  self:Pboolean(m, "is_jump2store", parentTable)
end
function Paser:Pactivity_store_item(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pstring(m, "item_type", parentTable)
  self:Pinteger(m, "item_id", parentTable)
  self:Pinteger(m, "item_no", parentTable)
  self:Pinteger(m, "sp_type", parentTable)
  self:Pinteger(m, "fleet_id", parentTable)
  self:Pinteger(m, "fleet_level", parentTable)
  self:PArray(m, "items", parentTable, "game_item_id")
  self:Pinteger(m, "quick_buy", parentTable)
  self:Pinteger(m, "buy_quantity", parentTable)
  self:Pinteger(m, "day_buy_limit", parentTable)
  self:Pinteger(m, "day_buy_left", parentTable)
  self:Pinteger(m, "all_buy_limit", parentTable)
  self:Pinteger(m, "all_buy_left", parentTable)
end
function Paser:Pactivity_stores_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "items", parentTable, "activity_store_item")
end
function Paser:Pactivity_stores_buy_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "store_type", parentTable)
  self:Pinteger(m, "id", parentTable)
  self:Pinteger(m, "buy_times", parentTable)
end
function Paser:Pyys(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "key", parentTable)
  self:Pinteger(m, "value", parentTable)
end
function Paser:Pyys_dict(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "config", parentTable, "yys")
end
function Paser:Pcolony_action_purchase_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "action_type", parentTable)
end
function Paser:Pprestige_get_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "prestige_level", parentTable)
end
function Paser:Palliance_resource_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "money", parentTable)
  self:Pinteger(m, "brick", parentTable)
  self:Pinteger(m, "inventory", parentTable)
  self:Pinteger(m, "point", parentTable)
end
function Paser:Palliance_notifies_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "notifies", parentTable, "string")
end
function Paser:Palliance_resource_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Palliance_resource_info(m, "resource", parentTable)
end
function Paser:Pfleet_info_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "fleet_id", parentTable)
  self:Pinteger(m, "level", parentTable)
  self:Pinteger(m, "type", parentTable)
  self:Pinteger(m, "req_type", parentTable)
end
function Paser:Pfleet_info_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "fleet_id", parentTable)
  self:Pfleets_info(m, "fleet_info", parentTable)
end
function Paser:Pfair_fleet_info_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "fleet_id", parentTable)
  self:Pshort(m, "fleet_level", parentTable)
  self:Pfleets_info(m, "fleet_info", parentTable)
end
function Paser:Pcommon_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "code", parentTable)
end
function Paser:Pkrypton_info_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "krypton_id", parentTable)
  self:Pinteger(m, "level", parentTable)
end
function Paser:Pkrypton_info_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pkrypton_info(m, "krypton", parentTable)
  self:Pinteger(m, "level", parentTable)
end
function Paser:Pwarn_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "device_name", parentTable)
  self:Pstring(m, "device_real_name", parentTable)
  self:Pstring(m, "udid", parentTable)
  self:Pinteger(m, "player_id", parentTable)
  self:Pinteger(m, "level", parentTable)
  self:Pstring(m, "info_type", parentTable)
  self:Pstring(m, "info_code", parentTable)
end
function Paser:Pitems_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "ids", parentTable, "integer")
end
function Paser:Pitem_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pinteger(m, "price", parentTable)
  self:Pinteger(m, "level_req", parentTable)
  self:Pinteger(m, "sp_type", parentTable)
  self:Pinteger(m, "use_max", parentTable)
  self:Pboolean(m, "use_more", parentTable)
  self:Pinteger(m, "max_level", parentTable)
end
function Paser:Pitems_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "items", parentTable, "item_info")
end
function Paser:Pclient_device_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "device_full_name", parentTable)
  self:Pstring(m, "device_real_name", parentTable)
  self:Pstring(m, "gpu_renderer", parentTable)
  self:Pstring(m, "gpu_vendor", parentTable)
  self:Pstring(m, "udid", parentTable)
  self:Pinteger(m, "cpu_hz", parentTable)
  self:Pinteger(m, "max_memory", parentTable)
end
function Paser:Pclient_device_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pboolean(m, "preload_battle", parentTable)
  self:Pboolean(m, "preload_battle_bg", parentTable)
  self:Pboolean(m, "use_complex_bg", parentTable)
  self:Pboolean(m, "use_complex_battle_effect", parentTable)
end
function Paser:Paward_receive_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
end
function Paser:Pclient_fps_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "name", parentTable)
  self:Pstring(m, "value", parentTable)
  self:Pstring(m, "udid", parentTable)
  self:Pinteger(m, "user_id", parentTable)
  self:Pstring(m, "created_at", parentTable)
  self:Pstring(m, "ip", parentTable)
  self:Pstring(m, "device_type", parentTable)
  self:Pinteger(m, "fps", parentTable)
end
function Paser:Ptaobao_trade_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "trade_no", parentTable)
end
function Paser:Pdomination_rank_award_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "rank", parentTable)
  self:Pinteger(m, "point", parentTable)
  self:PArray(m, "award", parentTable, "game_item")
end
function Paser:Pdomination_awards_list_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "alliance_id", parentTable)
  self:Pstring(m, "alliance_name", parentTable)
  self:PArray(m, "awards", parentTable, "domination_rank_award_info")
end
function Paser:Pdomination_ranks_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "rank_type", parentTable)
end
function Paser:Pdomination_rank_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pstring(m, "name", parentTable)
  self:Pshort(m, "flag", parentTable)
  self:Pstring(m, "alliance_name", parentTable)
  self:Pinteger(m, "rank", parentTable)
  self:Pinteger(m, "point", parentTable)
  self:Pinteger(m, "server", parentTable)
end
function Paser:Pdomination_ranks_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "total", parentTable)
  self:PArray(m, "ranks", parentTable, "domination_rank_info")
  self:PArray(m, "params", parentTable, "integer")
end
function Paser:Pwd_star_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
end
function Paser:Pwd_star(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pstring(m, "fight_name", parentTable)
  self:Pinteger(m, "left_time", parentTable)
  self:Pstring(m, "extra", parentTable)
  self:Pinteger(m, "alliance_id", parentTable)
  self:Pstring(m, "alliance_name", parentTable)
  self:Pstring(m, "alliance_leader", parentTable)
  self:Pinteger(m, "flag", parentTable)
end
function Paser:Pall_wd_star(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "infoes", parentTable, "wd_star")
end
function Paser:Pwd_award_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "flag", parentTable)
end
function Paser:Pwd_award(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pinteger(m, "begin_rank", parentTable)
  self:Pinteger(m, "end_rank", parentTable)
  self:PArray(m, "awards", parentTable, "game_item")
end
function Paser:Pwd_award_lists(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "infoes", parentTable, "wd_award")
end
function Paser:Palliance_domination_total_rank_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "player_point", parentTable)
  self:Pinteger(m, "alliance_point", parentTable)
end
function Paser:Pwd_last_champion(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "star_id", parentTable)
  self:Pinteger(m, "alliance_id", parentTable)
  self:Pstring(m, "alliance_name", parentTable)
  self:Pstring(m, "alliance_leader", parentTable)
  self:Pinteger(m, "flag", parentTable)
  self:Pstring(m, "server_name", parentTable)
end
function Paser:Pdevice_info_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "endpoint_token", parentTable)
  self:Pstring(m, "device_lang", parentTable)
  self:Pstring(m, "endpoint_type", parentTable)
  self:Pstring(m, "registration_id", parentTable)
end
function Paser:Pwd_ranking_box_player(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "user_id", parentTable)
  self:Pstring(m, "name", parentTable)
  self:Pinteger(m, "box_cnt", parentTable)
  self:Pinteger(m, "rank", parentTable)
  self:Pstring(m, "alliance_name", parentTable)
end
function Paser:Pwd_ranking_box_alliance(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Palliance_info(m, "alliance", parentTable)
  self:Pinteger(m, "box_cnt", parentTable)
  self:Pinteger(m, "rank", parentTable)
end
function Paser:Pwd_ranking_box_rank(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "players", parentTable, "wd_ranking_box_player")
  self:PArray(m, "alliances", parentTable, "wd_ranking_box_alliance")
  self:Pwd_ranking_box_player(m, "self_info", parentTable)
end
function Paser:Pwd_ranking_champion_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "user_id", parentTable)
end
function Paser:Popen_box_open_state(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pinteger(m, "type_id", parentTable)
end
function Paser:Popen_box_reward_state(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pgame_item(m, "item", parentTable)
  self:Pgame_item(m, "show_item", parentTable)
  self:Pinteger(m, "cur_num", parentTable)
  self:Pinteger(m, "total_num", parentTable)
end
function Paser:Popen_box_progress(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "step", parentTable)
  self:PArray(m, "loots", parentTable, "game_item")
  self:Pinteger(m, "status", parentTable)
end
function Paser:Popen_box_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "now_credit_spend", parentTable)
  self:Pgame_item(m, "refresh_cost", parentTable)
  self:PArray(m, "open_state_list", parentTable, "open_box_open_state")
  self:PArray(m, "reward_state_list", parentTable, "open_box_reward_state")
  self:PArray(m, "progress_bar", parentTable, "open_box_progress")
end
function Paser:Popen_box_open_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "box_id", parentTable)
  self:Pinteger(m, "id", parentTable)
end
function Paser:Popen_box_open_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "now_credit_spend", parentTable)
  self:Pgame_item(m, "refresh_cost", parentTable)
  self:Popen_box_open_state(m, "open_state", parentTable)
  self:Popen_box_reward_state(m, "reward_state", parentTable)
  self:PArray(m, "progress_bar", parentTable, "open_box_progress")
end
function Paser:Popen_box_refresh_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "now_credit_spend", parentTable)
  self:Pgame_item(m, "refresh_cost", parentTable)
  self:PArray(m, "open_state_list", parentTable, "open_box_open_state")
  self:PArray(m, "reward_state_list", parentTable, "open_box_reward_state")
  self:PArray(m, "progress_bar", parentTable, "open_box_progress")
end
function Paser:Precent_state(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "name", parentTable)
  self:Pgame_item(m, "item", parentTable)
  self:Pinteger(m, "times", parentTable)
end
function Paser:Popen_box_recent_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "state", parentTable, "recent_state")
end
function Paser:Pbattle_rate_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "cli_data", parentTable)
end
function Paser:Pbattle_rate_desc(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "equip", parentTable)
  self:Pinteger(m, "krypton", parentTable)
  self:Pinteger(m, "technique", parentTable)
  self:Pinteger(m, "recruit", parentTable)
  self:Pinteger(m, "power", parentTable)
  self:Pinteger(m, "art", parentTable)
  self:Pinteger(m, "medal", parentTable)
  self:Pinteger(m, "tactical", parentTable)
  self:Pinteger(m, "remodel", parentTable)
end
function Paser:Pbattle_rate_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "battle_type", parentTable)
  self:Pbattle_rate_desc(m, "player1_rate", parentTable)
  self:Pbattle_rate_desc(m, "player2_rate", parentTable)
  self:Pstring(m, "cli_data", parentTable)
end
function Paser:Pset_game_local_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "game_local", parentTable)
end
function Paser:Pcontention_dot_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pshort(m, "dot_type", parentTable)
  self:PArray(m, "effection", parentTable, "integer")
  self:PArray(m, "buffers", parentTable, "integer")
end
function Paser:Pcontention_line_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pinteger(m, "dot_start", parentTable)
  self:Pinteger(m, "dot_end", parentTable)
  self:Pinteger(m, "distance", parentTable)
end
function Paser:Pcontention_base_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "server_id", parentTable)
  self:Pinteger(m, "team_id", parentTable)
end
function Paser:Pcontention_buffer_item(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "key", parentTable)
  self:Pdouble(m, "value", parentTable)
  self:Pinteger(m, "type_id", parentTable)
  self:Pdouble(m, "limit_value", parentTable)
end
function Paser:Pcontention_buffer_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "id", parentTable)
  self:PArray(m, "buffs", parentTable, "contention_buffer_item")
  self:Pstring(m, "icon", parentTable)
end
function Paser:Pcontention_map_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "version", parentTable)
end
function Paser:Pcontention_season(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pstring(m, "start_time", parentTable)
  self:Pstring(m, "end_time", parentTable)
end
function Paser:Pcontention_event_time(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "begin_time", parentTable)
  self:Pinteger(m, "end_time", parentTable)
end
function Paser:Pcontention_event_data(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "key", parentTable)
  self:Pstring(m, "value1", parentTable)
  self:Pstring(m, "value2", parentTable)
  self:Pstring(m, "value3", parentTable)
  self:Pinteger(m, "icon", parentTable)
end
function Paser:Pcontention_event_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pinteger(m, "type", parentTable)
  self:PArray(m, "time_span", parentTable, "contention_event_time")
  self:PArray(m, "addition_data", parentTable, "contention_event_data")
  self:PArray(m, "params", parentTable, "integer")
  self:Pinteger(m, "is_show", parentTable)
end
function Paser:Pcontention_mission_info_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "event_id", parentTable)
  self:PArray(m, "id_list", parentTable, "integer")
end
function Paser:Pmission_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pstring(m, "type", parentTable)
  self:Pstring(m, "action", parentTable)
  self:Pinteger(m, "num", parentTable)
  self:Pinteger(m, "max", parentTable)
  self:Pinteger(m, "step", parentTable)
  self:PArray(m, "dot_list", parentTable, "integer")
  self:PArray(m, "awards", parentTable, "game_item")
  self:Pinteger(m, "is_end_award", parentTable)
end
function Paser:Pcontention_mission_info_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "mission_list", parentTable, "mission_info")
end
function Paser:Pcontention_mission_award_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "event_id", parentTable)
  self:Pinteger(m, "mission_id", parentTable)
  self:Pinteger(m, "step", parentTable)
end
function Paser:Pcontention_mybase_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "server_id", parentTable)
  self:Pstring(m, "user_id", parentTable)
end
function Paser:Pcontention_map_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "version", parentTable)
  self:Pcontention_season(m, "season", parentTable)
  self:PArray(m, "all_season", parentTable, "contention_season")
  self:Pinteger(m, "begin_time", parentTable)
  self:Pinteger(m, "end_time", parentTable)
  self:PArray(m, "dots", parentTable, "contention_dot_info")
  self:PArray(m, "lines", parentTable, "contention_line_info")
  self:PArray(m, "buffers", parentTable, "contention_buffer_info")
  self:PArray(m, "events", parentTable, "contention_event_info")
  self:PArray(m, "bases", parentTable, "contention_base_info")
  self:Pcontention_mybase_info(m, "my_data", parentTable)
end
function Paser:Pcontention_rank_list(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "user_id", parentTable)
  self:Pstring(m, "user_name", parentTable)
  self:Pinteger(m, "rank", parentTable)
  self:Pinteger(m, "point", parentTable)
  self:Pinteger(m, "team_id", parentTable)
  self:Pstring(m, "team", parentTable)
  self:Pinteger(m, "king", parentTable)
end
function Paser:Pcontention_rank_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:PArray(m, "rank_list", parentTable, "contention_rank_list")
end
function Paser:Pcontention_rank_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
end
function Paser:Pcontention_rank_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "rank", parentTable, "contention_rank_info")
end
function Paser:Pcontention_notifies_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "notifies", parentTable, "string")
end
function Paser:Pincome_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "type", parentTable)
  self:Pinteger(m, "player", parentTable)
  self:Pinteger(m, "alliance", parentTable)
  self:Pinteger(m, "server", parentTable)
end
function Paser:Pcontention_income_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "income_list", parentTable, "income_info")
end
function Paser:Pspecial_buff_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pinteger(m, "end_time", parentTable)
  self:Pinteger(m, "ext", parentTable)
end
function Paser:Pfight_buff_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "value1", parentTable)
  self:Pstring(m, "value2", parentTable)
  self:Pstring(m, "value3", parentTable)
  self:Pinteger(m, "endtime", parentTable)
  self:Pinteger(m, "icon", parentTable)
end
function Paser:Pcontention_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "dot_start", parentTable)
  self:Pinteger(m, "dot_end", parentTable)
  self:Pinteger(m, "dot_legacy", parentTable)
  self:Pinteger(m, "revived_left_second", parentTable)
  self:Pshort(m, "hp", parentTable)
  self:Pshort(m, "hp_limit", parentTable)
  self:Pshort(m, "speed", parentTable)
  self:PArray(m, "now_lines", parentTable, "integer")
  self:PArray(m, "special_buff", parentTable, "special_buff_info")
  self:Pstring(m, "killer", parentTable)
  self:Pinteger(m, "king", parentTable)
  self:PArray(m, "fight_buff", parentTable, "fight_buff_info")
  self:Pinteger(m, "last_report", parentTable)
end
function Paser:Pcontention_info_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pcontention_info(m, "info", parentTable)
end
function Paser:Pcontention_trip_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "server_id", parentTable)
  self:Pstring(m, "user_id", parentTable)
  self:Pstring(m, "user_name", parentTable)
  self:Pinteger(m, "dot_start", parentTable)
  self:Pinteger(m, "dot_end", parentTable)
  self:Pinteger(m, "trip_seconds", parentTable)
  self:Pinteger(m, "speed", parentTable)
  self:Pinteger(m, "king", parentTable)
end
function Paser:Pmissiles_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "now_step", parentTable)
  self:Pinteger(m, "max_step", parentTable)
  self:Pinteger(m, "begin_dot", parentTable)
  self:Pinteger(m, "end_dot", parentTable)
end
function Paser:Pcontention_missiles_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "missiles_list", parentTable, "missiles_info")
end
function Paser:Pcontention_trip_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "trips", parentTable, "contention_trip_info")
end
function Paser:Pcontention_station_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "dot_id", parentTable)
  self:Pinteger(m, "server_id", parentTable)
  self:Pinteger(m, "king", parentTable)
  self:Pdouble(m, "all_power", parentTable)
end
function Paser:Pcontention_station_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "stations", parentTable, "contention_station_info")
end
function Paser:Pcontention_battle_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "attk_user_id", parentTable)
  self:Pstring(m, "attk_user_name", parentTable)
  self:Pstring(m, "def_user_id", parentTable)
  self:Pstring(m, "def_user_name", parentTable)
  self:Pinteger(m, "on_station", parentTable)
  self:Pboolean(m, "is_win", parentTable)
  self:PArray(m, "dead_list", parentTable, "string")
end
function Paser:Pcontention_battle_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "battles", parentTable, "contention_battle_info")
end
function Paser:Pcontention_production_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "dot_id", parentTable)
  self:Pboolean(m, "is_relative", parentTable)
  self:PArray(m, "resources", parentTable, "game_item")
end
function Paser:Pcontention_production_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "productions", parentTable, "contention_production_info")
end
function Paser:Pcontention_starting_line_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "dot_start", parentTable)
  self:Pinteger(m, "line_id", parentTable)
end
function Paser:Pcontention_jump_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "user_id", parentTable)
  self:Pinteger(m, "dot_id", parentTable)
end
function Paser:Pcontention_starting_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "lines", parentTable, "contention_starting_line_info")
end
function Paser:Pcontention_transfer_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "lines", parentTable, "contention_starting_line_info")
end
function Paser:Pcontention_revive_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "dot_start", parentTable)
  self:Pboolean(m, "pay_for_alive", parentTable)
end
function Paser:Pcontention_stop_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "dot_id", parentTable)
  self:Pinteger(m, "page", parentTable)
end
function Paser:Pcontention_occupier_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "user_id", parentTable)
  self:Pstring(m, "user_name", parentTable)
  self:Pshort(m, "sex", parentTable)
  self:Pstring(m, "icon", parentTable)
  self:Pinteger(m, "main_fleet_level", parentTable)
end
function Paser:Pcontention_stop_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "occupiers", parentTable, "contention_occupier_info")
  self:Pinteger(m, "max_num", parentTable)
  self:Pdouble(m, "all_power", parentTable)
  self:Pstring(m, "king_id", parentTable)
end
function Paser:Pcontention_player(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "user_id", parentTable)
  self:Pinteger(m, "icon", parentTable)
  self:Pinteger(m, "level", parentTable)
end
function Paser:Ptc_award(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pinteger(m, "min", parentTable)
  self:Pinteger(m, "max", parentTable)
  self:PArray(m, "awards", parentTable, "game_item")
end
function Paser:Pcontention_award_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
end
function Paser:Pcontention_award_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "awards", parentTable, "tc_award")
end
function Paser:Pcontention_history_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "season", parentTable)
end
function Paser:Phistory_rank(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "name", parentTable)
  self:Pinteger(m, "rank", parentTable)
  self:Pinteger(m, "point", parentTable)
  self:Pinteger(m, "server_id", parentTable)
  self:Pstring(m, "server", parentTable)
end
function Paser:Pcontention_history_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "season", parentTable)
  self:PArray(m, "top3team", parentTable, "history_rank")
  self:PArray(m, "top3player", parentTable, "history_rank")
end
function Paser:Pcontention_king_info_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "end_time", parentTable)
  self:PArray(m, "award", parentTable, "game_item")
end
function Paser:Pcontention_king_change_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "now_king", parentTable)
end
function Paser:Palliance_domination_status_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pboolean(m, "can_battle", parentTable)
end
function Paser:Pdomination_buffer_of_rank_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "icon", parentTable)
end
function Paser:Pactivity_task_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pinteger(m, "type", parentTable)
  self:Pstring(m, "title", parentTable)
  self:Pstring(m, "desc", parentTable)
  self:Pinteger(m, "value", parentTable)
  self:Pinteger(m, "status", parentTable)
  self:Pinteger(m, "limit_value", parentTable)
  self:PArray(m, "reward_list", parentTable, "game_item")
end
function Paser:Pactivity_task_list_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "activity_task_list", parentTable, "activity_task_list")
end
function Paser:Pactivity_task_list(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "key_desc", parentTable)
  self:Pinteger(m, "type", parentTable)
  self:Pinteger(m, "special_id", parentTable)
  self:Pstring(m, "type_desc", parentTable)
  self:Pinteger(m, "activity_id", parentTable)
  self:PArray(m, "task_list", parentTable, "activity_task_info")
  self:PArray(m, "kryptons", parentTable, "krypton_info")
end
function Paser:Pactivity_task_reward_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "type", parentTable)
  self:Pinteger(m, "activity_id", parentTable)
  self:Pinteger(m, "task_id", parentTable)
end
function Paser:Pactivity_task_list_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "activity_task_list", parentTable, "activity_task_list")
end
function Paser:Pcontention_name_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "dot_id", parentTable)
  self:Pstring(m, "dot_name", parentTable)
end
function Paser:Pcontention_name_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "stations", parentTable, "contention_name_info")
end
function Paser:Pcontention_occupier_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "user_id", parentTable)
end
function Paser:Pcontention_occupier_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "user_name", parentTable)
  self:Pshort(m, "sex", parentTable)
  self:Pshort(m, "hp", parentTable)
  self:Pshort(m, "hp_limit", parentTable)
  self:Pinteger(m, "influence", parentTable)
  self:Pdouble(m, "force", parentTable)
  self:Pshort(m, "level", parentTable)
  self:Pstring(m, "icon", parentTable)
  self:PArray(m, "fleet_levels", parentTable, "fleet_level")
  self:PArray(m, "matrices", parentTable, "matrix_cell")
end
function Paser:Pcontention_collect_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "dots", parentTable, "integer")
end
function Paser:Pcontention_collection_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "dot_id", parentTable)
  self:PArray(m, "resources", parentTable, "game_item")
end
function Paser:Pcontention_collect_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "productions", parentTable, "contention_collection_info")
end
function Paser:Pcontention_log_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pshort(m, "log_type", parentTable)
  self:Pinteger(m, "ext", parentTable)
  self:PArray(m, "params", parentTable, "string")
  self:Pinteger(m, "time", parentTable)
end
function Paser:Pcontention_hit_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:PArray(m, "params", parentTable, "string")
end
function Paser:Pcontention_hit_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pcontention_hit_info(m, "hit", parentTable)
end
function Paser:Pcontention_logs_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "user_id", parentTable)
end
function Paser:Pcontention_logs_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "logs", parentTable, "contention_log_info")
end
function Paser:Pcontention_logbattle_detail_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
end
function Paser:Pcontention_logbattle_detail_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pfight_report(m, "report", parentTable)
  self:PArray(m, "reward_list", parentTable, "game_item")
end
function Paser:Pactivity_status_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "ids", parentTable, "short")
end
function Paser:Pactivity_status_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "id", parentTable)
  self:Pboolean(m, "enabled", parentTable)
  self:Pinteger(m, "left_seconds", parentTable)
end
function Paser:Pactivity_status_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "activities", parentTable, "activity_status_info")
end
function Paser:Pscreen_track(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "udid", parentTable)
  self:Pstring(m, "state_object", parentTable)
end
function Paser:Ptango_invite_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "tangoid", parentTable)
  self:PArray(m, "friends", parentTable, "string")
end
function Paser:Ptango_invited_friends_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "tangoid", parentTable)
  self:PArray(m, "friends", parentTable, "string")
end
function Paser:Ptango_invited_friends_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "invited_friends", parentTable, "string")
  self:PArray(m, "registered_friends", parentTable, "string")
end
function Paser:Ppay_animation_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "twink_time", parentTable)
  self:Pinteger(m, "breath_time", parentTable)
end
function Paser:Pattr_change(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pinteger(m, "value", parentTable)
  self:Pinteger(m, "next_level_id", parentTable)
  self:Pinteger(m, "next_level_value", parentTable)
end
function Paser:Phero_level_attr_diff_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "hero_id", parentTable)
  self:Pboolean(m, "is_levelup", parentTable)
end
function Paser:Phero_level_attr_diff_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "allow_max", parentTable)
  self:Pshort(m, "level_limited", parentTable)
  self:Pshort(m, "probability", parentTable)
  self:Pshort(m, "probability_step", parentTable)
  self:PArray(m, "changed_attrs", parentTable, "attr_change")
  self:PArray(m, "fleets", parentTable, "fleet_show_info")
  self:PArray(m, "conditions", parentTable, "game_item")
end
function Paser:Pexpedition_wormhole_progress_award(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "number", parentTable)
  self:PArray(m, "awards", parentTable, "game_item")
  self:Pinteger(m, "status", parentTable)
end
function Paser:Pexpedition_wormhole_task(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "finished_num", parentTable)
  self:Pinteger(m, "total_num", parentTable)
  self:PArray(m, "progress_awards", parentTable, "expedition_wormhole_progress_award")
end
function Paser:Pexpedition_wormhole_status(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pexpedition_wormhole_task(m, "all_tasks", parentTable)
end
function Paser:Pexpedition_wormhole_award_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pdouble(m, "process_num", parentTable)
end
function Paser:Pexpedition_wormhole_rank_board_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "type", parentTable)
  self:Pinteger(m, "top", parentTable)
end
function Paser:Pexpedition_wormhole_player_brief(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "rank", parentTable)
  self:Pstring(m, "user_name", parentTable)
  self:Pdouble(m, "score", parentTable)
  self:Pinteger(m, "level", parentTable)
end
function Paser:Pexpedition_wormhole_rank_board(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "type", parentTable)
  self:Pexpedition_wormhole_player_brief(m, "self", parentTable)
  self:PArray(m, "top_list", parentTable, "expedition_wormhole_player_brief")
end
function Paser:Pexpedition_wormhole_rank_board_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "rank_board", parentTable, "expedition_wormhole_rank_board")
end
function Paser:Pexpedition_wormhole_rank_self_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "self", parentTable, "expedition_wormhole_player_brief")
end
function Paser:Pdungeon_open_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "active_id", parentTable)
  self:Pinteger(m, "time", parentTable)
  self:Pstring(m, "activity_name", parentTable)
  self:Pstring(m, "pic_name", parentTable)
  self:Pinteger(m, "store_tag", parentTable)
  self:Pinteger(m, "red_point", parentTable)
end
function Paser:Pdungeon_open_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "info", parentTable, "dungeon_open_info")
end
function Paser:Pdungeon_enter_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "active_id", parentTable)
end
function Paser:Pladder_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "cur_step", parentTable)
  self:Pshort(m, "search_num", parentTable)
  self:Pshort(m, "reset_num", parentTable)
  self:Pinteger(m, "end_time", parentTable)
  self:Pinteger(m, "interval", parentTable)
  self:PArray(m, "has_rewards", parentTable, "game_item")
  self:Pinteger(m, "captain_index", parentTable)
  self:Pinteger(m, "level", parentTable)
  self:PArray(m, "may_rewards", parentTable, "game_item")
  self:Pinteger(m, "dialog", parentTable)
  self:Pstring(m, "condition", parentTable)
  self:Pdouble(m, "monster_hp", parentTable)
  self:Pinteger(m, "monster_shield", parentTable)
  self:Pstring(m, "monster_name", parentTable)
  self:Pinteger(m, "backgroud", parentTable)
  self:PArray(m, "matrix", parentTable, "monster_matrix_cell")
  self:Pdouble(m, "force", parentTable)
  self:Pboolean(m, "raids_end", parentTable)
  self:Pboolean(m, "on_max_step", parentTable)
  self:PArray(m, "ratio_award", parentTable, "game_item")
  self:Pinteger(m, "radis_step", parentTable)
  self:Pboolean(m, "can_transmit", parentTable)
  self:Pinteger(m, "cur_max_step", parentTable)
  self:Pinteger(m, "reset_radis_step", parentTable)
  self:Pboolean(m, "can_award", parentTable)
  self:PArray(m, "buffs", parentTable, "pair")
end
function Paser:Pladder_enter_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "type", parentTable)
  self:Pladder_info(m, "info", parentTable)
  self:Pboolean(m, "is_first", parentTable)
end
function Paser:Pladder_fresh_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "type", parentTable)
end
function Paser:Pladder_fresh_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "type", parentTable)
  self:Pladder_info(m, "info", parentTable)
end
function Paser:Pladder_reset_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "type", parentTable)
end
function Paser:Pladder_reset_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "type", parentTable)
  self:Pladder_info(m, "info", parentTable)
end
function Paser:Pladder_search_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "type", parentTable)
  self:Pfight_report(m, "report", parentTable)
  self:Pinteger(m, "result", parentTable)
  self:PArray(m, "rewards", parentTable, "game_item")
  self:Pladder_info(m, "info", parentTable)
  self:PArray(m, "fleets", parentTable, "fleet_damage_info")
end
function Paser:Pladder_raids_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "type", parentTable)
  self:Pinteger(m, "end_time", parentTable)
  self:Pinteger(m, "interval", parentTable)
end
function Paser:Pladder_buy_search_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "type", parentTable)
  self:Pshort(m, "num", parentTable)
end
function Paser:Pladder_buy_reset_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "type", parentTable)
  self:Pshort(m, "num", parentTable)
end
function Paser:Pladder_rank_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "rank", parentTable)
  self:Pstring(m, "user_id", parentTable)
  self:Pinteger(m, "icon", parentTable)
  self:Pstring(m, "name", parentTable)
  self:Pinteger(m, "level", parentTable)
  self:Pshort(m, "step", parentTable)
end
function Paser:Pladder_rank_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "type", parentTable)
  self:Pladder_rank_info(m, "self", parentTable)
  self:PArray(m, "rank", parentTable, "ladder_rank_info")
end
function Paser:Pladder_special_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "step", parentTable)
  self:PArray(m, "rewards", parentTable, "game_item")
  self:Pshort(m, "status", parentTable)
end
function Paser:Pladder_special_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "type", parentTable)
  self:PArray(m, "info", parentTable, "ladder_special_info")
end
function Paser:Pladder_search_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "type", parentTable)
end
function Paser:Pladder_raids_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "type", parentTable)
end
function Paser:Pladder_cancel_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "type", parentTable)
end
function Paser:Pladder_gain_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "type", parentTable)
end
function Paser:Pladder_buy_search_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "type", parentTable)
end
function Paser:Pladder_buy_reset_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "type", parentTable)
end
function Paser:Pladder_rank_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "type", parentTable)
end
function Paser:Pladder_special_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "type", parentTable)
end
function Paser:Pfleet_enhance_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "fleet_id", parentTable)
end
function Paser:Pfleet_weaken_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "fleet_id", parentTable)
end
function Paser:Pfleet_show_param(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "fleet_id", parentTable)
  self:Pshort(m, "level", parentTable)
end
function Paser:Pfleet_show_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "fleets", parentTable, "fleet_show_param")
end
function Paser:Pfleet_show_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "fleets", parentTable, "fleet_show_info")
end
function Paser:Porder_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pshort(m, "status", parentTable)
  self:Pshort(m, "waiting_seconds", parentTable)
end
function Paser:Porders_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "orders", parentTable, "order_info")
end
function Paser:Pwxpay_info_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "mch_id", parentTable)
  self:Pstring(m, "prepay_id", parentTable)
  self:Pstring(m, "package", parentTable)
  self:Pstring(m, "nonce_str", parentTable)
  self:Pstring(m, "timestamp", parentTable)
  self:Pstring(m, "sign", parentTable)
end
function Paser:Ppay_confirm_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
end
function Paser:Puser_notifies_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "notifies", parentTable, "string")
end
function Paser:Pitems_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "grids", parentTable, "bag_grid")
end
function Paser:Pip_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "ip", parentTable)
end
function Paser:Pmycard_error(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "msg", parentTable)
end
function Paser:Paward_list_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "award_infos", parentTable, "award_info")
  self:PArray(m, "kryptons", parentTable, "krypton_info")
end
function Paser:Paward_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pinteger(m, "type", parentTable)
  self:Pinteger(m, "achievement_id", parentTable)
  self:Pinteger(m, "time", parentTable)
  self:PArray(m, "awards", parentTable, "game_item")
end
function Paser:Paward_get_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
end
function Paser:Paward_count_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "count", parentTable)
end
function Paser:Padv_chapter_status(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "chapter_id", parentTable)
  self:Pinteger(m, "status", parentTable)
end
function Paser:Padv_chapter_status_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "statuses", parentTable, "adv_chapter_status")
end
function Paser:Penchant_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "equipment_id", parentTable)
  self:Pinteger(m, "item_id", parentTable)
  self:Pinteger(m, "addition_item_id", parentTable)
  self:Pinteger(m, "addition_item_num", parentTable)
  self:Pinteger(m, "addition_credits", parentTable)
  self:Pinteger(m, "uplevel_count", parentTable)
end
function Paser:Penchant_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "origin_level", parentTable)
  self:Pinteger(m, "origin_effect", parentTable)
  self:Pinteger(m, "origin_effect_value", parentTable)
  self:Pinteger(m, "new_level", parentTable)
  self:Pinteger(m, "new_effect", parentTable)
  self:Pinteger(m, "new_effect_value", parentTable)
end
function Paser:Ppay_record_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pdouble(m, "price", parentTable)
  self:PArray(m, "param", parentTable, "string")
end
function Paser:Plogin_token_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "acc_type", parentTable)
  self:Pstring(m, "user_id", parentTable)
end
function Paser:Pdungeon_enter_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "code", parentTable)
  self:Pshort(m, "lev", parentTable)
end
function Paser:Pchange_auto_decompose(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "change_num", parentTable)
end
function Paser:Pchange_auto_decompose_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "change_num", parentTable)
  self:Pinteger(m, "max_level", parentTable)
  self:Pinteger(m, "mini_level", parentTable)
end
function Paser:Pactivity_dna_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "activity_id", parentTable)
end
function Paser:Pactivity_dna_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pgame_item(m, "item_id", parentTable)
  self:Pgame_item(m, "award_id", parentTable)
  self:Pinteger(m, "dna_had", parentTable)
  self:Pinteger(m, "dna_need", parentTable)
  self:PArray(m, "dna_charge_status", parentTable, "short")
  self:Pinteger(m, "dna_charge_max", parentTable)
  self:Pboolean(m, "charge_next_condition", parentTable)
  self:Pinteger(m, "charge_award_tag", parentTable)
  self:PArray(m, "activity_award", parentTable, "game_item")
  self:Pinteger(m, "price", parentTable)
  self:Pinteger(m, "discount", parentTable)
  self:Pinteger(m, "kid_id", parentTable)
end
function Paser:Pactivity_dna_charge_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "activity_id", parentTable)
  self:Pinteger(m, "charge_time", parentTable)
end
function Paser:Pcrusade_pos(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "pos", parentTable)
  self:Pstring(m, "name", parentTable)
  self:Pstring(m, "server", parentTable)
  self:Pshort(m, "locale", parentTable)
  self:Pinteger(m, "level", parentTable)
  self:Pshort(m, "sex", parentTable)
  self:PArray(m, "matrix", parentTable, "monster_matrix_cell")
  self:PArray(m, "adjutant", parentTable, "crusade_adjutant")
  self:Pdouble(m, "force", parentTable)
  self:Pinteger(m, "log_id", parentTable)
  self:Pboolean(m, "pass", parentTable)
end
function Paser:Pcrusade_reward(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "pos", parentTable)
  self:PArray(m, "rewards", parentTable, "game_item")
end
function Paser:Pcrusade_adjutant(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "major", parentTable)
  self:Pinteger(m, "adjutant", parentTable)
  self:Pinteger(m, "adjutant_level", parentTable)
  self:PArray(m, "adjutant_spell", parentTable, "integer")
end
function Paser:Pcrusade_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "pos_info", parentTable, "crusade_pos")
  self:PArray(m, "reward_get", parentTable, "short")
  self:PArray(m, "rewards", parentTable, "crusade_reward")
  self:Pshort(m, "cross", parentTable)
  self:PArray(m, "place", parentTable, "short")
  self:PArray(m, "pos_path", parentTable, "short")
  self:Pshort(m, "max_repair", parentTable)
  self:Pmatrix(m, "self_matrix", parentTable)
  self:PArray(m, "self_fleet", parentTable, "crusade_fleet")
  self:PArray(m, "self_adjutant", parentTable, "crusade_adjutant")
  self:Pinteger(m, "real_id", parentTable)
end
function Paser:Pcrusade_enter_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pcrusade_info(m, "info", parentTable)
end
function Paser:Pcrusade_battle_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "pos", parentTable)
end
function Paser:Pcrusade_fleet(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "fleet_id", parentTable)
  self:Pinteger(m, "hp", parentTable)
  self:Pinteger(m, "shield", parentTable)
  self:Pinteger(m, "max_hp", parentTable)
  self:Pinteger(m, "max_shield", parentTable)
  self:Pdouble(m, "force", parentTable)
  self:Pinteger(m, "level", parentTable)
end
function Paser:Pcrusade_fight_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "pos", parentTable)
  self:Pmatrix(m, "matrix", parentTable)
end
function Paser:Pcrusade_fight_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pfight_report(m, "report", parentTable)
  self:PArray(m, "rewards", parentTable, "game_item")
  self:Pmatrix(m, "matrix", parentTable)
  self:Pinteger(m, "fight_log", parentTable)
end
function Paser:Pcrusade_cross_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "pos", parentTable)
end
function Paser:Pcrusade_cross_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pcrusade_info(m, "info", parentTable)
end
function Paser:Pcrusade_repair_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "fleet_id", parentTable)
end
function Paser:Pcrusade_reward_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "pos", parentTable)
end
function Paser:Pcrusade_reward_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "reward", parentTable, "game_item")
end
function Paser:Pcrusade_buy_cross_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "num", parentTable)
end
function Paser:Pcrusade_first_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "enter_level", parentTable)
  self:Pboolean(m, "first", parentTable)
end
function Paser:Plevel_range(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "step", parentTable)
  self:Pshort(m, "max", parentTable)
  self:Pshort(m, "min", parentTable)
end
function Paser:Pbudo_stage_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "cur_stage", parentTable)
  self:Pshort(m, "cur_step", parentTable)
  self:PArray(m, "budo_level_range", parentTable, "level_range")
end
function Paser:Pbudo_sign_up_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "left_time", parentTable)
  self:Pboolean(m, "does_join", parentTable)
  self:Pshort(m, "budo_level_min", parentTable)
  self:Pshort(m, "budo_level_max", parentTable)
end
function Paser:Pbudo_join_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "budo_level_min", parentTable)
  self:Pshort(m, "budo_level_max", parentTable)
end
function Paser:Pbudo_player(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "name", parentTable)
  self:Pstring(m, "avatar", parentTable)
  self:Pinteger(m, "player_id", parentTable)
  self:Pstring(m, "server_id", parentTable)
  self:Pinteger(m, "identity", parentTable)
  self:Pshort(m, "player_level", parentTable)
  self:Pinteger(m, "get_support", parentTable)
  self:Pinteger(m, "main_fleet_level", parentTable)
end
function Paser:Pone_battle_report(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pbudo_player(m, "win_player", parentTable)
  self:Pbudo_player(m, "failed_player", parentTable)
  self:Pinteger(m, "battle_id", parentTable)
end
function Paser:Pbudo_award_list(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "round", parentTable)
  self:PArray(m, "award_list", parentTable, "game_item")
end
function Paser:Pbudo_mass_election_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "player_status", parentTable)
  self:Pshort(m, "budo_level_max", parentTable)
  self:Pshort(m, "budo_level_min", parentTable)
  self:Pshort(m, "win_time", parentTable)
  self:Pshort(m, "failed_time", parentTable)
  self:Pshort(m, "cur_kid_stage", parentTable)
  self:Pshort(m, "total_stages", parentTable)
  self:Pinteger(m, "left_battle_time", parentTable)
  self:PArray(m, "award_list", parentTable, "budo_award_list")
  self:Pinteger(m, "can_submit", parentTable)
end
function Paser:Pbudo_promotion_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "budo_level", parentTable)
end
function Paser:Ppromotion_one_round_report(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "battle_round", parentTable)
  self:Pshort(m, "index", parentTable)
  self:Pinteger(m, "player_id", parentTable)
  self:Pinteger(m, "server_id", parentTable)
  self:Pstring(m, "name", parentTable)
  self:Pstring(m, "avatar", parentTable)
  self:Pshort(m, "player_level", parentTable)
  self:Pinteger(m, "get_support", parentTable)
  self:Pinteger(m, "main_fleet_level", parentTable)
end
function Paser:Pone_player_support(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "player_id", parentTable)
  self:Pstring(m, "server_id", parentTable)
  self:Pinteger(m, "num", parentTable)
end
function Paser:Pbudo_promotion_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "step", parentTable)
  self:Pshort(m, "win", parentTable)
  self:Pshort(m, "lose", parentTable)
  self:Pinteger(m, "left_battle_time", parentTable)
  self:Pshort(m, "player_status", parentTable)
  self:Pshort(m, "budo_level_max", parentTable)
  self:Pshort(m, "budo_level_min", parentTable)
  self:PArray(m, "my_support_player", parentTable, "budo_player")
  self:PArray(m, "my_award_array", parentTable, "budo_award_list")
  self:PArray(m, "vs_player", parentTable, "promotion_one_round_report")
  self:PArray(m, "all_player", parentTable, "budo_player")
  self:Pinteger(m, "can_submit", parentTable)
  self:Pinteger(m, "audition_round", parentTable)
end
function Paser:Psupport_one_player_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "step", parentTable)
  self:Pshort(m, "index", parentTable)
  self:Pinteger(m, "player_id", parentTable)
  self:Pstring(m, "server_id", parentTable)
  self:Pshort(m, "round", parentTable)
end
function Paser:Psupport_one_player_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "worth", parentTable)
end
function Paser:Pbattle_report_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "player_id", parentTable)
  self:Pstring(m, "server_id", parentTable)
  self:Pinteger(m, "stage", parentTable)
  self:Pinteger(m, "round", parentTable)
end
function Paser:Pbattle_report_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "battle_report", parentTable, "one_battle_report")
end
function Paser:Psupport_player(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "step", parentTable)
  self:Pshort(m, "index", parentTable)
  self:Pshort(m, "round", parentTable)
  self:Pshort(m, "award_status", parentTable)
  self:PArray(m, "award", parentTable, "game_item")
end
function Paser:Pget_budo_rank_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "step", parentTable)
end
function Paser:Pget_my_support_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "support_info", parentTable, "support_player")
end
function Paser:Prank_player(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "rank", parentTable)
  self:Pstring(m, "name", parentTable)
  self:Pinteger(m, "point", parentTable)
end
function Paser:Pget_budo_rank_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "rank_info", parentTable, "rank_player")
end
function Paser:Pget_budo_replay_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "battle_id", parentTable)
end
function Paser:Pget_budo_replay_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pfight_report(m, "battle_record", parentTable)
end
function Paser:Pget_promotion_reward_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "step", parentTable)
  self:Pshort(m, "round", parentTable)
end
function Paser:Pspecial_efficacy_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pboolean(m, "flag", parentTable)
end
function Paser:Pactivity_rank_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "id", parentTable)
end
function Paser:Pactivity_box_rank(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "id", parentTable)
  self:PArray(m, "players", parentTable, "thanksgiven_box_player")
  self:PArray(m, "alliances", parentTable, "thanksgiven_box_alliance")
  self:Pthanksgiven_box_player(m, "self_info", parentTable)
end
function Paser:Pgd_fleets_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pboolean(m, "is_mine", parentTable)
  self:Pdouble(m, "force", parentTable)
  self:Pinteger(m, "thumb_up_num", parentTable)
  self:Pboolean(m, "can_send_story", parentTable)
end
function Paser:Pall_gd_fleets_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "fleets", parentTable, "gd_fleets_info")
end
function Paser:Pfleet_fight_report_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
end
function Paser:Pfleet_fight_report_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pfight_report(m, "report", parentTable)
end
function Paser:Popen_box_info_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "box_id", parentTable)
end
function Paser:Popen_box_refresh_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "box_id", parentTable)
end
function Paser:Plaba_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "coin", parentTable)
  self:Pinteger(m, "power", parentTable)
end
function Paser:Plaba_times_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "times", parentTable)
  self:Pinteger(m, "area", parentTable)
end
function Paser:Plaba_award_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "type", parentTable)
end
function Paser:Palliance_fleet_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "fleet_id", parentTable)
  self:Pshort(m, "levelup", parentTable)
end
function Paser:Ppay_list_carrier_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "channel", parentTable)
  self:Pstring(m, "area", parentTable)
end
function Paser:Ppay_list_carrier_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "productions", parentTable, "production_info")
end
function Paser:Plaba_type_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "info", parentTable, "string")
  self:PArray(m, "item", parentTable, "game_item")
end
function Paser:Pchampion_matrix_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "rank", parentTable)
end
function Paser:Pchampion_matrix_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "matrix", parentTable, "monster_matrix_cell")
end
function Paser:Pmonth_card_list_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "pay_code", parentTable)
end
function Paser:Pmonth_card_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pstring(m, "name", parentTable)
  self:Pstring(m, "product_id", parentTable)
  self:Pinteger(m, "price", parentTable)
  self:Pinteger(m, "totle_days", parentTable)
  self:Pinteger(m, "totle_num", parentTable)
  self:Pinteger(m, "day_num", parentTable)
  self:Pinteger(m, "less_days", parentTable)
  self:Pboolean(m, "can_buy", parentTable)
  self:Pboolean(m, "can_get", parentTable)
  self:Pboolean(m, "can_use", parentTable)
  self:Pboolean(m, "has_join", parentTable)
  self:Pinteger(m, "activity_totle", parentTable)
  self:Pinteger(m, "activity_num", parentTable)
  self:Pinteger(m, "activity_time", parentTable)
  self:Pboolean(m, "activity_join", parentTable)
  self:PArray(m, "activity_reward", parentTable, "game_item")
end
function Paser:Pmonth_card_list_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "free_num", parentTable)
  self:Pboolean(m, "free_get", parentTable)
  self:PArray(m, "info", parentTable, "month_card_info")
end
function Paser:Pmonth_card_buy_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pinteger(m, "gift_user", parentTable)
end
function Paser:Pmonth_card_use_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
end
function Paser:Padd_adjutant_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "major", parentTable)
  self:Pinteger(m, "adjutant", parentTable)
end
function Paser:Padd_tlc_adjutant_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "matrix_type", parentTable)
  self:Pinteger(m, "major", parentTable)
  self:Pinteger(m, "adjutant", parentTable)
end
function Paser:Prelease_adjutant_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "major", parentTable)
end
function Paser:Pchat_channel_switch_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "chat_channels", parentTable, "chat_channel")
end
function Paser:Pchat_channel(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "type", parentTable)
  self:Pboolean(m, "switch", parentTable)
end
function Paser:Padjutant_max_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "num", parentTable)
  self:Pinteger(m, "level", parentTable)
end
function Paser:Pmax_level_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "max", parentTable)
  self:Pinteger(m, "max_recruit", parentTable)
end
function Paser:Pgame_items_trans_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "items", parentTable, "string")
end
function Paser:Pgame_items_trans_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "all_items", parentTable, "game_items")
end
function Paser:Pbudo_champion_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "index", parentTable)
end
function Paser:Pchampion_battle(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pboolean(m, "is_win", parentTable)
  self:Pinteger(m, "battle_id", parentTable)
end
function Paser:Pchampion_step_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "step", parentTable)
  self:Pinteger(m, "min_lev", parentTable)
  self:Pinteger(m, "max_lev", parentTable)
  self:Pbudo_player(m, "champion", parentTable)
  self:Pstring(m, "champion_server", parentTable)
  self:Pbudo_player(m, "enemy", parentTable)
  self:Pstring(m, "enemy_server", parentTable)
  self:PArray(m, "battle", parentTable, "champion_battle")
end
function Paser:Pbudo_champion_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "max_index", parentTable)
  self:Pinteger(m, "now_index", parentTable)
  self:PArray(m, "effect_list", parentTable, "integer")
  self:PArray(m, "info", parentTable, "champion_step_info")
end
function Paser:Pbudo_champion_report_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "index", parentTable)
  self:Pinteger(m, "battle_id", parentTable)
end
function Paser:Pclient_version_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "versions", parentTable, "client_version")
end
function Paser:Pclient_version(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "appid", parentTable)
  self:Pinteger(m, "version", parentTable)
end
function Paser:Pversion_code_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "player_version", parentTable)
  self:Pinteger(m, "server_version", parentTable)
end
function Paser:Pversion_code_update_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "version", parentTable)
end
function Paser:Papply_limit_update_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "apply_limit", parentTable)
end
function Paser:Premodel_levelup_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "user_id", parentTable)
  self:Pshort(m, "type", parentTable)
end
function Paser:Premodel_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "type", parentTable)
  self:Pinteger(m, "cur_level", parentTable)
  self:Pinteger(m, "max_level", parentTable)
  self:Pinteger(m, "addhp", parentTable)
  self:Pinteger(m, "nextlevel_addhp", parentTable)
  self:Pinteger(m, "nextlevel_money", parentTable)
  self:Pinteger(m, "nextlevel_time", parentTable)
  self:Pinteger(m, "credit", parentTable)
  self:Pshort(m, "state", parentTable)
  self:Pinteger(m, "cur_help", parentTable)
  self:Pinteger(m, "max_help", parentTable)
  self:Pinteger(m, "left_time", parentTable)
end
function Paser:Premodel_info_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "remodel_info_ack", parentTable, "remodel_info")
end
function Paser:Premodel_info_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "remodel_info_ntf", parentTable, "remodel_info")
end
function Paser:Premodel_help_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "user_id", parentTable)
  self:Pstring(m, "user_name", parentTable)
  self:Pshort(m, "type", parentTable)
  self:Pinteger(m, "cur_num", parentTable)
  self:Pinteger(m, "max_num", parentTable)
  self:Pinteger(m, "is_help", parentTable)
end
function Paser:Premodel_help_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "other_info", parentTable, "remodel_help_info")
end
function Paser:Premodel_help_info_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "info", parentTable, "remodel_help_info")
end
function Paser:Phelp_others_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "user_id", parentTable)
  self:Pshort(m, "type", parentTable)
end
function Paser:Premodel_help_others_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "help_list", parentTable, "help_others_info")
end
function Paser:Premodel_help_others_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "code", parentTable)
  self:PArray(m, "other_info", parentTable, "remodel_help_info")
end
function Paser:Premodel_speed_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "type", parentTable)
  self:Pinteger(m, "item_id", parentTable)
end
function Paser:Premodel_push_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "name", parentTable)
  self:Pinteger(m, "type", parentTable)
  self:Pinteger(m, "action", parentTable)
  self:Pinteger(m, "time", parentTable)
end
function Paser:Premodel_push_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "ntf", parentTable, "remodel_push_info")
end
function Paser:Pgive_friends_gift_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "type", parentTable)
  self:PArray(m, "friends", parentTable, "string")
end
function Paser:Pgive_share_awards_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "type", parentTable)
  self:Pgame_item(m, "itemkey", parentTable)
  self:Pinteger(m, "intkey", parentTable)
end
function Paser:Premodel_event_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "times", parentTable)
end
function Paser:Pgive_friends_gift_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "friends", parentTable, "string")
end
function Paser:Pupdate_passport_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "passport", parentTable)
end
function Paser:Pfinancial_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "level", parentTable)
  self:Pinteger(m, "benefit", parentTable)
  self:Pinteger(m, "next_level", parentTable)
  self:Pinteger(m, "next_benefit", parentTable)
  self:PArray(m, "upgrade_cost", parentTable, "game_item")
end
function Paser:Pfacebook_friends_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "friends", parentTable, "string")
end
function Paser:Pfacebook_friend_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "passport", parentTable)
  self:Pinteger(m, "status", parentTable)
  self:PArray(m, "awards", parentTable, "game_item")
  self:Pinteger(m, "left_time", parentTable)
end
function Paser:Pfacebook_friends_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "friends", parentTable, "facebook_friend_info")
end
function Paser:Pfacebook_friends_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "friends", parentTable, "facebook_friend_info")
end
function Paser:Pfinancial_gifts_get_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "friends", parentTable, "string")
end
function Paser:Pfinancial_amount_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "amount", parentTable)
end
function Paser:Pmulti_acc_loot(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "acc_type", parentTable)
  self:PArray(m, "items", parentTable, "game_item")
end
function Paser:Pmulti_acc_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "loot", parentTable, "multi_acc_loot")
  self:PArray(m, "already_get", parentTable, "integer")
end
function Paser:Pint_param(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "i", parentTable)
end
function Paser:Ppush_button_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "all", parentTable)
  self:Pshort(m, "activity", parentTable)
  self:Pshort(m, "champion", parentTable)
  self:Pshort(m, "colony", parentTable)
  self:Pshort(m, "mine", parentTable)
  self:Pshort(m, "building", parentTable)
  self:Pshort(m, "equipment", parentTable)
  self:Pshort(m, "revenue", parentTable)
  self:Pshort(m, "alliance", parentTable)
end
function Paser:Ppush_button_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Ppush_button_info(m, "req", parentTable)
end
function Paser:Ppush_button_info_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Ppush_button_info(m, "ack", parentTable)
end
function Paser:Ppush_button_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Ppush_button_info(m, "ntf", parentTable)
end
function Paser:Pinvited_friend_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "passport", parentTable)
  self:Pinteger(m, "level", parentTable)
  self:Pinteger(m, "energy", parentTable)
  self:Pinteger(m, "state", parentTable)
  self:Pinteger(m, "last_active_time", parentTable)
end
function Paser:Ptreasure_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "status", parentTable)
  self:Pinteger(m, "level", parentTable)
  self:Pinteger(m, "energy", parentTable)
  self:Pinteger(m, "required", parentTable)
  self:Pinteger(m, "refresh_time", parentTable)
  self:Pinteger(m, "max_friend_num", parentTable)
  self:PArray(m, "friends", parentTable, "invited_friend_info")
end
function Paser:Pinvite_friends_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "friends", parentTable, "string")
end
function Paser:Pget_energy_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "friends", parentTable, "string")
end
function Paser:Popen_treasure_box_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "result", parentTable)
  self:PArray(m, "awards", parentTable, "game_item")
end
function Paser:Pfriend_role_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "passport", parentTable)
end
function Paser:Pfriend_role_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Puser_brief_info(m, "role_info", parentTable)
end
function Paser:Psend_global_email_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "passport", parentTable)
  self:Pstring(m, "nickname", parentTable)
  self:Pstring(m, "title", parentTable)
  self:Pstring(m, "content", parentTable)
end
function Paser:Pcollect_energy_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "passport", parentTable)
end
function Paser:Pkrypton_refine_info_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "remain_times", parentTable)
  self:Pinteger(m, "max_times", parentTable)
  self:Pinteger(m, "item_id", parentTable)
  self:Pinteger(m, "item_count", parentTable)
  self:Pboolean(m, "times_can_buy", parentTable)
  self:Pinteger(m, "item_exp", parentTable)
end
function Paser:Pkrypton_inject_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "injected_id", parentTable)
  self:Pstring(m, "inject_id", parentTable)
  self:Pboolean(m, "with_item", parentTable)
end
function Paser:Pkrypton_inject_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pkrypton_info(m, "injected_krypton_info", parentTable)
  self:Pinteger(m, "remain_times", parentTable)
  self:Pinteger(m, "max_times", parentTable)
  self:Pinteger(m, "item_count", parentTable)
  self:Pinteger(m, "kenergy", parentTable)
  self:Pboolean(m, "is_critical", parentTable)
end
function Paser:Pkrypton_refine_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "refine_id", parentTable)
end
function Paser:Pkrypton_refine_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pkrypton_info(m, "refined_krypton_info", parentTable)
  self:Pinteger(m, "kenergy", parentTable)
end
function Paser:Pkrypton_inject_buy_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "remain_times", parentTable)
  self:Pinteger(m, "max_times", parentTable)
  self:Pinteger(m, "credit", parentTable)
end
function Paser:Pkrypton_inject_list_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "formation_id", parentTable)
  self:Pstring(m, "refined_id", parentTable)
end
function Paser:Pkrypton_inject_list_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "kryptons", parentTable, "krypton_info")
end
function Paser:Pkrypton_refine_list_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "formation_id", parentTable)
end
function Paser:Pkrypton_refine_list_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "kryptons", parentTable, "krypton_info")
  self:PArray(m, "up_kryptons", parentTable, "krypton_info")
end
function Paser:Puser_info_fleet(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "ranks", parentTable, "pair")
  self:Pinteger(m, "total_count", parentTable)
end
function Paser:Puser_info_force(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pdouble(m, "total_force", parentTable)
  self:Pinteger(m, "coating_ph_atk", parentTable)
  self:Pinteger(m, "coating_ph_armor", parentTable)
  self:Pinteger(m, "coating_sp_atk", parentTable)
  self:Pinteger(m, "coating_sp_armor", parentTable)
  self:Pinteger(m, "coating_durability", parentTable)
  self:Pinteger(m, "coating_ph_atk_v2", parentTable)
  self:Pinteger(m, "coating_ph_armor_v2", parentTable)
  self:Pinteger(m, "coating_sp_atk_v2", parentTable)
  self:Pinteger(m, "coating_sp_armor_v2", parentTable)
  self:Pinteger(m, "coating_durability_v2", parentTable)
  self:Pinteger(m, "total_speed", parentTable)
  self:Pinteger(m, "aver_equip_level", parentTable)
  self:Pinteger(m, "aver_interfere_level", parentTable)
  self:Pinteger(m, "highest_interfere_level", parentTable)
  self:Pinteger(m, "highest_krypton_level", parentTable)
  self:Pinteger(m, "aver_krypton_level", parentTable)
  self:Pinteger(m, "r7_krypton_count", parentTable)
  self:Pinteger(m, "r6_krypton_count", parentTable)
  self:Pinteger(m, "fleet_highest_remould_level", parentTable)
  self:Pinteger(m, "fleet_aver_remould_level", parentTable)
  self:Pinteger(m, "intensify_level", parentTable)
  self:Pinteger(m, "wdc_buff_time", parentTable)
  self:Pinteger(m, "red_medal_count", parentTable)
  self:Pinteger(m, "golden_medal_count", parentTable)
  self:Pinteger(m, "purple_medal_count", parentTable)
  self:Pinteger(m, "average_medal_level", parentTable)
  self:Pinteger(m, "average_medal_rank", parentTable)
  self:Pinteger(m, "highest_medal_level", parentTable)
  self:Pinteger(m, "highest_medal_rank", parentTable)
  self:Pinteger(m, "total_remodel_level", parentTable)
  self:Pinteger(m, "total_tech_level", parentTable)
end
function Paser:Puser_info_resource(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "credit", parentTable)
  self:Pdouble(m, "money", parentTable)
  self:Pinteger(m, "technique", parentTable)
  self:Pinteger(m, "lepton", parentTable)
  self:Pinteger(m, "quark", parentTable)
  self:Pinteger(m, "kenergy", parentTable)
  self:Pinteger(m, "ac_supply", parentTable)
  self:Pinteger(m, "laba_supply", parentTable)
  self:Pinteger(m, "art_point", parentTable)
  self:Pinteger(m, "medal_currency", parentTable)
  self:Pinteger(m, "expedition", parentTable)
end
function Paser:Puser_info_other(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "prestige", parentTable)
  self:Pinteger(m, "achieve_total", parentTable)
  self:Pinteger(m, "achieve_end", parentTable)
  self:Pinteger(m, "plat_achieve_total", parentTable)
  self:Pinteger(m, "plat_achieve_end", parentTable)
  self:Pinteger(m, "gold_achieve_total", parentTable)
  self:Pinteger(m, "gold_achieve_end", parentTable)
  self:Pinteger(m, "silver_achieve_total", parentTable)
  self:Pinteger(m, "silver_achieve_end", parentTable)
  self:Pinteger(m, "bronze_achieve_total", parentTable)
  self:Pinteger(m, "bronze_achieve_end", parentTable)
  self:Pinteger(m, "achieve_credit_get", parentTable)
end
function Paser:Puser_info_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "name", parentTable)
  self:Pinteger(m, "level", parentTable)
  self:Pinteger(m, "champion", parentTable)
  self:Pinteger(m, "military_rank", parentTable)
  self:Pstring(m, "alliance_name", parentTable)
  self:Puser_info_fleet(m, "fleet", parentTable)
  self:Puser_info_force(m, "force", parentTable)
  self:Puser_info_resource(m, "resource", parentTable)
  self:Puser_info_other(m, "other", parentTable)
end
function Paser:Pprime_dot(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pshort(m, "dot_type", parentTable)
  self:Pstring(m, "ui_type", parentTable)
  self:PArray(m, "dot_coordinates", parentTable, "short")
  self:Pstring(m, "dot_name", parentTable)
end
function Paser:Pprime_line(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:PArray(m, "dot_to_dot", parentTable, "integer")
  self:Pinteger(m, "distance", parentTable)
end
function Paser:Pprime_map_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "dots", parentTable, "prime_dot")
  self:PArray(m, "lines", parentTable, "prime_line")
end
function Paser:Pprime_dot_act(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pshort(m, "occupy", parentTable)
  self:Pshort(m, "occupy_no", parentTable)
  self:Pshort(m, "status", parentTable)
end
function Paser:Pprime_march_act(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pshort(m, "begin_dot", parentTable)
  self:Pshort(m, "end_dot", parentTable)
  self:Pshort(m, "current", parentTable)
  self:Pshort(m, "speed", parentTable)
  self:Pshort(m, "status", parentTable)
  self:Pshort(m, "player_type", parentTable)
  self:Pinteger(m, "prime_count", parentTable)
  self:Pinteger(m, "monster_count", parentTable)
end
function Paser:Pchange_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pinteger(m, "change_percent", parentTable)
end
function Paser:Pprime_my_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "status", parentTable)
  self:Pstring(m, "pos", parentTable)
  self:Pshort(m, "power_left_time", parentTable)
  self:PArray(m, "change_list", parentTable, "change_info")
  self:Pinteger(m, "left_free_jump_time", parentTable)
  self:Pinteger(m, "total_free_jump_time", parentTable)
  self:PArray(m, "result_show_list", parentTable, "integer")
end
function Paser:Pprime_active_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "dots", parentTable, "prime_dot_act")
  self:PArray(m, "lines", parentTable, "prime_march_act")
  self:Pprime_my_info(m, "my", parentTable)
  self:Pinteger(m, "prime_dot_stay_time", parentTable)
end
function Paser:Pprime_jump_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "dest_id", parentTable)
  self:PArray(m, "line_ids", parentTable, "integer")
  self:Pinteger(m, "distance", parentTable)
end
function Paser:Pprime_dot_occupy_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pshort(m, "value", parentTable)
end
function Paser:Pprime_push_show_round_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "round_id", parentTable)
end
function Paser:Pprime_dot_status_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pshort(m, "value", parentTable)
end
function Paser:Pprime_march_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "marchs", parentTable, "prime_march_act")
end
function Paser:Pprime_march_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "dots", parentTable, "integer")
end
function Paser:Pprime_rank_player(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "server_id", parentTable)
  self:Pinteger(m, "user_id", parentTable)
  self:Pstring(m, "user_name", parentTable)
  self:Pinteger(m, "rank", parentTable)
  self:Pdouble(m, "point", parentTable)
end
function Paser:Pprime_rank(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "type", parentTable)
  self:Pprime_rank_player(m, "self_rank", parentTable)
  self:PArray(m, "players", parentTable, "prime_rank_player")
end
function Paser:Pprime_rank_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "ranks", parentTable, "prime_rank")
end
function Paser:Pprime_fight_history_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "server_id", parentTable)
  self:Pinteger(m, "user_id", parentTable)
end
function Paser:Pprime_fight_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "report_id", parentTable)
  self:Pstring(m, "atker", parentTable)
  self:Pstring(m, "defender", parentTable)
  self:Pboolean(m, "is_win", parentTable)
  self:Pinteger(m, "time", parentTable)
end
function Paser:Pprime_fight_history_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "fight_history", parentTable, "prime_fight_info")
end
function Paser:Pprime_site_info_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "site_id", parentTable)
end
function Paser:Pprime_site_info_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "status", parentTable)
  self:Pinteger(m, "team_amount", parentTable)
  self:Pdouble(m, "total_force", parentTable)
  self:Pinteger(m, "reparing_amount", parentTable)
  self:Pinteger(m, "left_time", parentTable)
  self:Pinteger(m, "prime_amount", parentTable)
  self:Pinteger(m, "monster_amount", parentTable)
end
function Paser:Pprime_increase_power_info_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "change_list", parentTable, "change_info")
end
function Paser:Pmonster_group(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "position", parentTable)
  self:Pinteger(m, "amount", parentTable)
  self:PArray(m, "monsters", parentTable, "integer")
  self:Pstring(m, "desc_id", parentTable)
end
function Paser:Pprime_enemy_info_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "prime_monsters", parentTable, "monster_group")
end
function Paser:Pprime_fight_report_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "report_id", parentTable)
end
function Paser:Pprime_last_report_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "server_id", parentTable)
  self:Pinteger(m, "user_id", parentTable)
end
function Paser:Pprime_dot_occupy_no_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pshort(m, "value", parentTable)
end
function Paser:Pkrypton_sort_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "formation_id", parentTable)
  self:Pshort(m, "type", parentTable)
end
function Paser:Pchampion_promote_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "old", parentTable)
  self:Pinteger(m, "current", parentTable)
  self:PArray(m, "award", parentTable, "game_item")
end
function Paser:Pchampion_straight_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "num", parentTable)
  self:PArray(m, "award", parentTable, "game_item")
  self:Pinteger(m, "flag", parentTable)
end
function Paser:Pchampion_straight_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "ack", parentTable, "champion_straight_info")
end
function Paser:Pchampion_straight_award_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "ids", parentTable, "integer")
end
function Paser:Pprestige_info_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "prestige", parentTable)
  self:PArray(m, "prestige_infos", parentTable, "prestige_info")
end
function Paser:Ppay_sign_item(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "day", parentTable)
  self:Pinteger(m, "status", parentTable)
  self:PArray(m, "rewards", parentTable, "game_item")
  self:Pinteger(m, "credit", parentTable)
end
function Paser:Ppay_sign_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "items", parentTable, "pay_sign_item")
  self:PArray(m, "recommands", parentTable, "integer")
end
function Paser:Ppay_sign_reward_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "day", parentTable)
end
function Paser:Plevelup_unlock_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "name", parentTable)
  self:Pinteger(m, "level", parentTable)
  self:Pinteger(m, "is_opened", parentTable)
end
function Paser:Plevelup_unlock_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "ntf", parentTable, "levelup_unlock_info")
end
function Paser:Popen_server_fund_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "num", parentTable)
  self:Pshort(m, "flag", parentTable)
end
function Paser:Popen_server_fund_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "credit", parentTable)
  self:Pinteger(m, "level", parentTable)
  self:Pinteger(m, "flag", parentTable)
end
function Paser:Popen_server_fund_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pboolean(m, "does_charged", parentTable)
  self:Pinteger(m, "credit", parentTable)
  self:Pinteger(m, "acc_credit", parentTable)
  self:Pinteger(m, "charged_amount", parentTable)
  self:Pinteger(m, "vip", parentTable)
  self:PArray(m, "info", parentTable, "open_server_fund_info")
end
function Paser:Popen_server_fund_award_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "level", parentTable)
end
function Paser:Pall_welfare_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "num", parentTable)
  self:Pshort(m, "flag", parentTable)
end
function Paser:Pall_welfare_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "num", parentTable)
  self:PArray(m, "award", parentTable, "game_item")
  self:Pinteger(m, "flag", parentTable)
end
function Paser:Pall_welfare_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "amount", parentTable)
  self:Pinteger(m, "time", parentTable)
  self:PArray(m, "info", parentTable, "all_welfare_info")
end
function Paser:Pall_welfare_award_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "amount", parentTable)
end
function Paser:Pequip_push_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "fleet_id", parentTable)
  self:Pinteger(m, "equip_type", parentTable)
  self:Pinteger(m, "equip_slot", parentTable)
  self:Pboolean(m, "display", parentTable)
end
function Paser:Pvip_level_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "vip_level_info", parentTable, "vip_level_info")
end
function Paser:Pvip_level_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "level", parentTable)
  self:Pinteger(m, "need_exp", parentTable)
end
function Paser:Ptemp_vip_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "level", parentTable)
  self:Pinteger(m, "left_time", parentTable)
  self:Pboolean(m, "show_banner", parentTable)
end
function Paser:Pprime_award_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "rank_type", parentTable)
end
function Paser:Pprime_award(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pinteger(m, "begin_rank", parentTable)
  self:Pinteger(m, "end_rank", parentTable)
  self:PArray(m, "awards", parentTable, "game_item")
end
function Paser:Pprime_award_lists(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "infoes", parentTable, "prime_award")
end
function Paser:Pprime_round_result_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "round_id", parentTable)
  self:Pinteger(m, "result", parentTable)
  self:Pinteger(m, "first_round_id", parentTable)
  self:Pinteger(m, "current_round_id", parentTable)
  self:Pinteger(m, "current_round_left_time", parentTable)
  self:Pinteger(m, "next_round_id", parentTable)
  self:Pinteger(m, "next_round_begin_time", parentTable)
  self:Pinteger(m, "activity_left_time", parentTable)
  self:Pinteger(m, "step_id", parentTable)
  self:Pinteger(m, "step_left_time", parentTable)
  self:Pinteger(m, "step_count", parentTable)
end
function Paser:Ppay_push_pop_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "can_pop", parentTable)
end
function Paser:Pprime_time_wve_boss_left(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pinteger(m, "left_time", parentTable)
end
function Paser:Pprime_time_wve_boss_left_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "list", parentTable, "prime_time_wve_boss_left")
end
function Paser:Pprime_do_wve_boss_left_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "id_list", parentTable, "integer")
end
function Paser:Pprime_round_fight_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "escape_rate", parentTable)
end
function Paser:Psell_item_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "amount", parentTable)
end
function Paser:Purl_push_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "url", parentTable)
end
function Paser:Penhance_info_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "equip_id", parentTable)
end
function Paser:Pequip_attr(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "type", parentTable)
  self:Pinteger(m, "bassic", parentTable)
  self:Pinteger(m, "mature", parentTable)
end
function Paser:Penhance_info_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "max_level", parentTable)
  self:PArray(m, "values", parentTable, "equip_attr")
  self:Pinteger(m, "consume", parentTable)
end
function Paser:Pcombo_guide_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "state", parentTable)
end
function Paser:Pcombo_guide_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pinteger(m, "activity_id", parentTable)
end
function Paser:Pplayer_pay_price_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "pay_price", parentTable)
end
function Paser:Ppay_status_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "pay_status", parentTable)
end
function Paser:Pdaily_benefits_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "info", parentTable, "daily_benefits_info")
end
function Paser:Pdaily_benefits_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "info", parentTable, "daily_benefits_info")
end
function Paser:Pdaily_benefits_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "type", parentTable)
  self:Pinteger(m, "level", parentTable)
  self:PArray(m, "award", parentTable, "game_item")
  self:Pinteger(m, "current", parentTable)
  self:Pshort(m, "flag", parentTable)
end
function Paser:Pdaily_benefits_award_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "type", parentTable)
  self:Pinteger(m, "level", parentTable)
end
function Paser:Punlocked_seven_day_article(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "daily_gift", parentTable, "daily_benefits_info")
  self:PArray(m, "seven_day_goal", parentTable, "string")
  self:PArray(m, "half_buy_item", parentTable, "store_item")
end
function Paser:Pseven_day_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "open_status", parentTable)
  self:PArray(m, "awards_status", parentTable, "integer")
  self:Pinteger(m, "activity_left_time", parentTable)
  self:Pinteger(m, "award_left_time", parentTable)
end
function Paser:Pseven_day_goal_subtask(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pinteger(m, "status", parentTable)
  self:Pinteger(m, "finished", parentTable)
  self:Pinteger(m, "total", parentTable)
  self:PArray(m, "awards", parentTable, "game_item")
end
function Paser:Pseven_day_goal_task(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "type", parentTable)
  self:Pinteger(m, "locked", parentTable)
  self:Pinteger(m, "status", parentTable)
  self:PArray(m, "subtasks", parentTable, "seven_day_goal_subtask")
end
function Paser:Pgoal_info_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "task_list", parentTable, "seven_day_goal_task")
end
function Paser:Pget_goal_award_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "task_id", parentTable)
end
function Paser:Pcutoff_info_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "cutoff_item_list", parentTable, "store_item")
end
function Paser:Pbuy_cutoff_item_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
end
function Paser:Pget_goal_award_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "status", parentTable)
  self:Pseven_day_goal_task(m, "task_info", parentTable)
end
function Paser:Penter_seven_day_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Punlocked_seven_day_article(m, "today_unlock", parentTable)
end
function Paser:Pstore_quick_buy_count_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "store_id", parentTable)
  self:Pinteger(m, "id", parentTable)
end
function Paser:Pstore_quick_buy_price_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "store_id", parentTable)
  self:Pinteger(m, "id", parentTable)
  self:Pinteger(m, "buy_times", parentTable)
end
function Paser:Pstore_quick_buy_price_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "nor_price", parentTable)
  self:Pstring(m, "dis_price", parentTable)
  self:Pinteger(m, "buy_times", parentTable)
end
function Paser:Pprime_jump_price_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "price_min", parentTable)
  self:Pinteger(m, "price_max", parentTable)
  self:Pfloat(m, "price_unit", parentTable)
end
function Paser:Pselect_goal_type_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "type", parentTable)
end
function Paser:Pactivity_store_count_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "store_type", parentTable)
  self:Pinteger(m, "id", parentTable)
end
function Paser:Pcost_attr(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "item_id", parentTable)
  self:Pinteger(m, "cost", parentTable)
end
function Paser:Pactivity_store_count_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "all_cost", parentTable, "cost_attr")
  self:Pinteger(m, "max_times", parentTable)
  self:Pinteger(m, "times", parentTable)
end
function Paser:Pactivity_store_price_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "store_type", parentTable)
  self:Pinteger(m, "id", parentTable)
  self:Pinteger(m, "buy_times", parentTable)
end
function Paser:Pactivity_store_price_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "all_cost", parentTable, "cost_attr")
end
function Paser:Pgp_test_pay_group_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "join_test", parentTable)
  self:Pinteger(m, "join_group", parentTable)
end
function Paser:Pmax_step_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "type", parentTable)
end
function Paser:Pmax_step_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "max", parentTable)
  self:Pinteger(m, "award_step", parentTable)
end
function Paser:Pladder_report_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "step", parentTable)
end
function Paser:Pladder_report_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "time_player_name", parentTable)
  self:Pstring(m, "time_report", parentTable)
  self:Pstring(m, "level_player_name", parentTable)
  self:Pstring(m, "level_report", parentTable)
end
function Paser:Pladder_award_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "type", parentTable)
  self:Pinteger(m, "step", parentTable)
end
function Paser:Pladder_award_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "type", parentTable)
  self:PArray(m, "info", parentTable, "ladder_special_info")
end
function Paser:Pladder_jump_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "type", parentTable)
  self:Pinteger(m, "step", parentTable)
end
function Paser:Pladder_jump_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pladder_info(m, "info", parentTable)
end
function Paser:Pladder_report_detail_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "id", parentTable)
end
function Paser:Pladder_report_detail_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pfight_report(m, "report", parentTable)
end
function Paser:Prefresh_type_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "refresh_type", parentTable)
  self:Pinteger(m, "max_free_times", parentTable)
  self:Pinteger(m, "cur_free_times", parentTable)
  self:Pinteger(m, "credit_cost", parentTable)
  self:Pinteger(m, "item_type", parentTable)
  self:Pinteger(m, "item_num", parentTable)
  self:Pinteger(m, "item_cost", parentTable)
  self:Pinteger(m, "price_off_status", parentTable)
end
function Paser:Pexpedition_refresh_type_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "refresh_type", parentTable)
  self:Pinteger(m, "max_free_times", parentTable)
  self:Pinteger(m, "cur_free_times", parentTable)
  self:Pinteger(m, "currency_cost", parentTable)
  self:Pinteger(m, "price_off_status", parentTable)
end
function Paser:Pstores_buy_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "award", parentTable, "game_item")
  self:Pstore_item(m, "item", parentTable)
end
function Paser:Pforce_add_fleet_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "type", parentTable)
end
function Paser:Punlock_condition(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "benefit", parentTable)
  self:Pinteger(m, "level", parentTable)
  self:Pinteger(m, "vip", parentTable)
  self:PArray(m, "cost", parentTable, "game_item")
end
function Paser:Ptactical_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "equip_num", parentTable)
  self:Pinteger(m, "equip_max", parentTable)
  self:Punlock_condition(m, "unlock_slot", parentTable)
  self:PArray(m, "slots", parentTable, "tactical_slot")
end
function Paser:Ptactical_slot(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "vessel", parentTable)
  self:Pstring(m, "equip_id", parentTable)
  self:PArray(m, "equips", parentTable, "tactical_equip")
  self:Pinteger(m, "red_point", parentTable)
end
function Paser:Ptactical_equip(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "id", parentTable)
  self:Pinteger(m, "type", parentTable)
  self:Pinteger(m, "rank", parentTable)
  self:Pinteger(m, "vessel", parentTable)
  self:Pboolean(m, "is_equip", parentTable)
  self:PArray(m, "decompose", parentTable, "game_item")
  self:PArray(m, "possible_attrs", parentTable, "possible_attr")
  self:PArray(m, "equip_attrs", parentTable, "tactical_equip_attr")
end
function Paser:Ppossible_attr(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pinteger(m, "min", parentTable)
  self:Pinteger(m, "max", parentTable)
end
function Paser:Ptactical_equip_attr(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pinteger(m, "type", parentTable)
  self:Pinteger(m, "color", parentTable)
  self:PArray(m, "effects", parentTable, "tactical_equip_effect")
end
function Paser:Ptactical_equip_effect(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "effect_vessel", parentTable)
  self:Pinteger(m, "value", parentTable)
  self:Pinteger(m, "min", parentTable)
  self:Pinteger(m, "max", parentTable)
end
function Paser:Ptactical_equip_on_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "vessel", parentTable)
  self:Pstring(m, "equip_id", parentTable)
end
function Paser:Ptactical_equip_on_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "code", parentTable)
  self:Pinteger(m, "vessel", parentTable)
  self:Pstring(m, "new_equip_id", parentTable)
  self:Pstring(m, "old_equip_id", parentTable)
end
function Paser:Ptactical_equip_off_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "vessel", parentTable)
end
function Paser:Ptactical_equip_off_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "code", parentTable)
  self:Pinteger(m, "vessel", parentTable)
  self:Pstring(m, "equip_id", parentTable)
end
function Paser:Ptactical_resource_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "used_times", parentTable)
  self:Pinteger(m, "max_times", parentTable)
  self:Pinteger(m, "next_refresh", parentTable)
end
function Paser:Ptactical_refine_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "equip_id", parentTable)
  self:Pinteger(m, "times", parentTable)
  self:PArray(m, "locked_attrs", parentTable, "integer")
end
function Paser:Ptactical_refine_price_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "code", parentTable)
  self:Pinteger(m, "lock_price", parentTable)
  self:Pinteger(m, "lock_cost", parentTable)
  self:Pinteger(m, "refine_price", parentTable)
end
function Paser:Ptactical_refine_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "code", parentTable)
  self:Pinteger(m, "cost_credit", parentTable)
  self:Pinteger(m, "cost_item", parentTable)
  self:PArray(m, "results", parentTable, "refine_result")
end
function Paser:Prefine_result(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "refine_id", parentTable)
  self:PArray(m, "equip_attrs", parentTable, "tactical_equip_attr")
end
function Paser:Ptactical_refine_save_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "equip_id", parentTable)
  self:Pinteger(m, "refine_id", parentTable)
end
function Paser:Ptactical_unlock_slot_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "code", parentTable)
  self:Punlock_condition(m, "next", parentTable)
end
function Paser:Ptactical_enter_refine_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "equip_id", parentTable)
end
function Paser:Ptactical_enter_refine_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "equip_id", parentTable)
  self:Pinteger(m, "used_times", parentTable)
  self:Pinteger(m, "max_times", parentTable)
  self:Pinteger(m, "lock_num", parentTable)
  self:Pinteger(m, "lock_max", parentTable)
  self:Punlock_condition(m, "unlock_lock_attr", parentTable)
  self:PArray(m, "lock_attr_price", parentTable, "integer")
  self:PArray(m, "lock_attr_cost", parentTable, "integer")
  self:Pgame_item(m, "need_item", parentTable)
  self:Pgame_item(m, "lock_item", parentTable)
  self:PArray(m, "last_refines", parentTable, "refine_result")
end
function Paser:Ptactical_status_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "status", parentTable)
end
function Paser:Ptactical_equip_delete_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "equip_id", parentTable)
end
function Paser:Ptactical_equip_delete_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "code", parentTable)
  self:Pstring(m, "equip_id", parentTable)
end
function Paser:Plimit_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "level", parentTable)
  self:Pshort(m, "is_open", parentTable)
  self:Pstring(m, "pay_type", parentTable)
end
function Paser:Ppay_limit_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "pay_limit_infos", parentTable, "limit_ntf")
end
function Paser:Ppay_sign_board_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "boards", parentTable, "string")
end
function Paser:Pstar_item(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pgame_item(m, "award", parentTable)
  self:Pinteger(m, "award_status", parentTable)
  self:Pinteger(m, "big_or_norm", parentTable)
end
function Paser:Pstar_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "star_id", parentTable)
  self:PArray(m, "all_award", parentTable, "star_item")
  self:Pstring(m, "cost_type", parentTable)
  self:Pinteger(m, "cost_num", parentTable)
  self:Pinteger(m, "cdtimes", parentTable)
end
function Paser:Penter_plante_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "item_id", parentTable)
  self:Pinteger(m, "star_id", parentTable)
  self:Pinteger(m, "refresh_cost", parentTable)
  self:PArray(m, "star", parentTable, "star_info")
end
function Paser:Pplante_explore_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "activity_id", parentTable)
  self:Pinteger(m, "star_id", parentTable)
  self:Pinteger(m, "state", parentTable)
end
function Paser:Pplante_explore_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "award_id", parentTable)
  self:Pinteger(m, "star_id", parentTable)
  self:Pstring(m, "cost_type", parentTable)
  self:Pinteger(m, "cost_num", parentTable)
  self:Pinteger(m, "cd_time", parentTable)
  self:Pinteger(m, "error_code", parentTable)
end
function Paser:Penter_plante_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "activity_id", parentTable)
end
function Paser:Pplante_refresh_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "activity_id", parentTable)
end
function Paser:Pplante_refresh_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "item_id", parentTable)
  self:Pinteger(m, "star_id", parentTable)
  self:Pinteger(m, "refresh_cost", parentTable)
  self:PArray(m, "star", parentTable, "star_info")
end
function Paser:Pshow_price_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "status", parentTable)
end
function Paser:Pget_invite_gift_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "number", parentTable)
  self:Pinteger(m, "total", parentTable)
  self:Pinteger(m, "pre_percent", parentTable)
  self:Pinteger(m, "cur_percent", parentTable)
  self:PArray(m, "friends", parentTable, "string")
end
function Paser:Pget_invite_gift_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "type", parentTable)
  self:Pinteger(m, "strike", parentTable)
  self:PArray(m, "awards", parentTable, "game_item")
end
function Paser:Pfacebook_switch_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "bind_awards", parentTable, "game_item")
  self:Pinteger(m, "story_image_max", parentTable)
  self:Pinteger(m, "story_title_max", parentTable)
  self:Pinteger(m, "ball_loop_seconds", parentTable)
  self:Pinteger(m, "ball_loop_times", parentTable)
  self:PArray(m, "switches", parentTable, "pair")
end
function Paser:Pfacebook_story_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "switches", parentTable, "pair")
end
function Paser:Pstory_award(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "story_id", parentTable)
  self:Pinteger(m, "strike", parentTable)
  self:PArray(m, "awards", parentTable, "game_item")
end
function Paser:Pstory_status_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "story_list", parentTable, "pair")
  self:PArray(m, "story_awards", parentTable, "story_award")
end
function Paser:Pstory_award_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "story_id", parentTable)
  self:Pinteger(m, "fleet_id", parentTable)
end
function Paser:Pstory_award_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "code", parentTable)
end
function Paser:Pfleet_can_enhance(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "identity", parentTable)
  self:Pboolean(m, "can_enhance", parentTable)
end
function Paser:Pcan_enhance_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "fleet_info", parentTable, "fleet_can_enhance")
end
function Paser:Ptactical_delete_awards_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "equip_id", parentTable)
end
function Paser:Ptactical_delete_awards_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "awards", parentTable, "game_item")
end
function Paser:Pfirst_login_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "status", parentTable)
end
function Paser:Palliance_guid_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pboolean(m, "is_guid", parentTable)
  self:Pgame_item(m, "award", parentTable)
end
function Paser:Pltv_ajust(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "type", parentTable)
  self:Pstring(m, "status", parentTable)
end
function Paser:Pltv_ajust_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "ltv_ajusts", parentTable, "ltv_ajust")
end
function Paser:Pthumb_up_info_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "fleets_thumbs", parentTable, "pair")
end
function Paser:Pthumb_up_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
end
function Paser:Pthumb_up_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "code", parentTable)
end
function Paser:Pcredit_exchange_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "api", parentTable)
  self:Pgame_item(m, "item", parentTable)
  self:Pinteger(m, "credit", parentTable)
end
function Paser:Pcredit_exchange_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "api", parentTable)
  self:Pgame_item(m, "item", parentTable)
  self:Pinteger(m, "credit", parentTable)
end
function Paser:Ptoday_task_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "id", parentTable)
  self:Pstring(m, "task_name", parentTable)
  self:Pboolean(m, "is_get_reward", parentTable)
  self:Pboolean(m, "is_finished", parentTable)
  self:PArray(m, "rewards", parentTable, "game_item")
  self:PArray(m, "task_content", parentTable, "sub_task_info")
  self:Pshort(m, "reward_number", parentTable)
end
function Paser:Ptoday_act_task_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Ptoday_task_info(m, "task", parentTable)
  self:Pstring(m, "word", parentTable)
  self:Pstring(m, "background", parentTable)
  self:Pstring(m, "title_font", parentTable)
  self:Pstring(m, "banner_font", parentTable)
end
function Paser:Penter_monthly_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "each_day", parentTable, "daily_task_info")
  self:Pinteger(m, "max", parentTable)
  self:Pinteger(m, "current", parentTable)
end
function Paser:Pdaily_task_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "id", parentTable)
  self:Pboolean(m, "is_today", parentTable)
  self:Pstring(m, "background", parentTable)
  self:Pinteger(m, "max_task_count", parentTable)
  self:Pinteger(m, "current_task_count", parentTable)
  self:Pboolean(m, "can_redo", parentTable)
end
function Paser:Pview_each_task_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "id", parentTable)
end
function Paser:Pcost_arrary(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "once_cost", parentTable)
  self:Pinteger(m, "each_cost", parentTable)
end
function Paser:Pview_each_task_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Ptoday_task_info(m, "task", parentTable)
  self:Pcost_arrary(m, "cost", parentTable)
end
function Paser:Pview_award(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pinteger(m, "count", parentTable)
  self:PArray(m, "rewards", parentTable, "game_item")
  self:Pboolean(m, "is_receive", parentTable)
end
function Paser:Pview_award_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "award", parentTable, "view_award")
end
function Paser:Pget_award_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pinteger(m, "count", parentTable)
end
function Paser:Pget_award_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "code", parentTable)
end
function Paser:Predo_task_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "id", parentTable)
  self:Pinteger(m, "sub_id", parentTable)
end
function Paser:Predo_task_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "code", parentTable)
end
function Paser:Pget_today_award_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "id", parentTable)
  self:Pinteger(m, "sub_id", parentTable)
end
function Paser:Pget_today_award_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "code", parentTable)
end
function Paser:Pother_monthly_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "id", parentTable)
end
function Paser:Pother_monthly_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "each_day", parentTable, "daily_task_info")
end
function Paser:Pswitch_formation_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "formation_id", parentTable)
end
function Paser:Pswitch_formation_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "code", parentTable)
  self:Pfleets_info(m, "fleet", parentTable)
  self:PArray(m, "fleetkryptons", parentTable, "fleet_krypton_info")
end
function Paser:Pmulmatrix_switch_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pboolean(m, "switch", parentTable)
end
function Paser:Pmulmatrix_switch_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "code", parentTable)
end
function Paser:Pmulmatrix_equip_info_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
end
function Paser:Pmulmatrix_equip_info_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "code", parentTable)
end
function Paser:Pchange_matrix_type_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Ppair(m, "matrix", parentTable)
end
function Paser:Pchange_matrix_type_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "code", parentTable)
  self:PArray(m, "matrixs_purpose", parentTable, "pair")
end
function Paser:Pbag_size_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "title_type", parentTable)
  self:Pinteger(m, "item_num", parentTable)
  self:Pinteger(m, "show_max", parentTable)
end
function Paser:Puser_bag_info_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "bags", parentTable, "bag_size_info")
end
function Paser:Pclient_req_stat_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "open_udid", parentTable)
  self:Pinteger(m, "user_id", parentTable)
  self:Pstring(m, "created_at", parentTable)
  self:Pstring(m, "ip", parentTable)
  self:Pstring(m, "device_type", parentTable)
  self:Pinteger(m, "time_consuming", parentTable)
end
function Paser:Pbudo_change_formation_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
end
function Paser:Pbugly_data_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pboolean(m, "is_open", parentTable)
  self:Pboolean(m, "lua_error_open", parentTable)
  self:Pboolean(m, "frame60_open", parentTable)
  self:Pboolean(m, "half_body_open", parentTable)
end
function Paser:Pchampion_status_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "supply", parentTable)
  self:Pinteger(m, "max_supply", parentTable)
  self:Pinteger(m, "cdtime", parentTable)
  self:Pshort(m, "status", parentTable)
end
function Paser:Penter_champion_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pchampion_status_info(m, "local_champion", parentTable)
  self:Pchampion_status_info(m, "world_champion", parentTable)
  self:Pchampion_status_info(m, "tlc_champion", parentTable)
end
function Paser:Pwdc_fleet(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "identity", parentTable)
  self:Pshort(m, "level", parentTable)
end
function Paser:Pwdc_player_brief(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "server_id", parentTable)
  self:Pstring(m, "user_id", parentTable)
  self:Pstring(m, "server_name", parentTable)
  self:Pstring(m, "user_name", parentTable)
  self:Pshort(m, "sex", parentTable)
  self:Pshort(m, "icon", parentTable)
  self:Pshort(m, "level", parentTable)
  self:Pshort(m, "flevel", parentTable)
  self:Pinteger(m, "point", parentTable)
  self:Pinteger(m, "ranking", parentTable)
  self:Pinteger(m, "rank", parentTable)
  self:Pstring(m, "rank_img", parentTable)
  self:Pinteger(m, "rank_name", parentTable)
  self:Pdouble(m, "force", parentTable)
  self:PArray(m, "fleets", parentTable, "wdc_fleet")
  self:Pinteger(m, "atk_wins", parentTable)
  self:Pinteger(m, "def_wins", parentTable)
  self:Pinteger(m, "best_socre", parentTable)
  self:Pinteger(m, "best_ranking", parentTable)
  self:Pinteger(m, "best_season", parentTable)
end
function Paser:Ptlc_fleets(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "fleets", parentTable, "tlc_fleet")
end
function Paser:Ptlc_fleet(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "identity", parentTable)
  self:Pshort(m, "level", parentTable)
end
function Paser:Ptlc_player_brief(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "server_id", parentTable)
  self:Pstring(m, "user_id", parentTable)
  self:Pstring(m, "server_name", parentTable)
  self:Pstring(m, "user_name", parentTable)
  self:Pshort(m, "sex", parentTable)
  self:Pshort(m, "icon", parentTable)
  self:Pshort(m, "level", parentTable)
  self:Pshort(m, "flevel", parentTable)
  self:Pinteger(m, "point", parentTable)
  self:Pinteger(m, "ranking", parentTable)
  self:Pinteger(m, "rank", parentTable)
  self:Pstring(m, "rank_img", parentTable)
  self:Pinteger(m, "rank_name", parentTable)
  self:Pdouble(m, "force", parentTable)
  self:PArray(m, "fleets", parentTable, "tlc_fleets")
  self:Pinteger(m, "atk_wins", parentTable)
  self:Pinteger(m, "def_wins", parentTable)
  self:Pinteger(m, "best_socre", parentTable)
  self:Pinteger(m, "best_ranking", parentTable)
  self:Pinteger(m, "best_season", parentTable)
end
function Paser:Pwdc_challenge_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pwdc_player_brief(m, "defender", parentTable)
  self:Pinteger(m, "win_score", parentTable)
  self:Pinteger(m, "lose_score", parentTable)
end
function Paser:Pwdc_player_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "point", parentTable)
  self:Pinteger(m, "rank_point", parentTable)
  self:Pinteger(m, "next_rank_point", parentTable)
  self:Pinteger(m, "ranking", parentTable)
  self:Pinteger(m, "using_ranking", parentTable)
  self:Pinteger(m, "using_rank", parentTable)
  self:Pinteger(m, "rank", parentTable)
  self:Pstring(m, "rank_img", parentTable)
  self:Pinteger(m, "rank_name", parentTable)
  self:Pinteger(m, "protect_time", parentTable)
  self:Pinteger(m, "final_time", parentTable)
  self:Pinteger(m, "end_time", parentTable)
  self:Pinteger(m, "res_per_hour", parentTable)
  self:Pdouble(m, "force", parentTable)
end
function Paser:Pwdc_fight_report(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "report_id", parentTable)
  self:Pwdc_player_brief(m, "enemy", parentTable)
  self:Pboolean(m, "is_win", parentTable)
  self:Pboolean(m, "can_revenge", parentTable)
  self:Pinteger(m, "score_before", parentTable)
  self:Pinteger(m, "score_after", parentTable)
  self:Pinteger(m, "fight_time", parentTable)
end
function Paser:Pwdc_award(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pinteger(m, "rank_level", parentTable)
  self:Pstring(m, "rank_img", parentTable)
  self:Pinteger(m, "rank_name", parentTable)
  self:Pinteger(m, "begin_rank", parentTable)
  self:Pinteger(m, "end_rank", parentTable)
  self:PArray(m, "awards", parentTable, "game_item")
  self:PArray(m, "benefits", parentTable, "game_item")
  self:PArray(m, "buffers", parentTable, "pair")
end
function Paser:Ptlc_award(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pinteger(m, "rank_level", parentTable)
  self:Pstring(m, "rank_img", parentTable)
  self:Pinteger(m, "rank_name", parentTable)
  self:Pinteger(m, "begin_rank", parentTable)
  self:Pinteger(m, "end_rank", parentTable)
  self:PArray(m, "awards", parentTable, "game_item")
  self:PArray(m, "benefits", parentTable, "game_item")
  self:PArray(m, "buffers", parentTable, "pair")
end
function Paser:Pwdc_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pgame_item(m, "currency", parentTable)
  self:Pboolean(m, "can_buy", parentTable)
  self:Pinteger(m, "search_cost", parentTable)
  self:Pinteger(m, "max_supply", parentTable)
  self:Pboolean(m, "pop_report", parentTable)
  self:Pinteger(m, "king_num", parentTable)
  self:Pwdc_player_info(m, "player_info", parentTable)
  self:PArray(m, "buffers", parentTable, "pair")
end
function Paser:Pwdc_info_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pwdc_info(m, "info", parentTable)
end
function Paser:Pwdc_enter_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pwdc_info(m, "info", parentTable)
end
function Paser:Pwdc_match_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "code", parentTable)
  self:PArray(m, "challenge_list", parentTable, "wdc_challenge_info")
end
function Paser:Pwdc_fight_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "server_id", parentTable)
  self:Pstring(m, "user_id", parentTable)
  self:Pinteger(m, "revenge_id", parentTable)
end
function Paser:Pwdc_fight_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "code", parentTable)
  self:Pwdc_fight_report(m, "result", parentTable)
  self:Pdouble(m, "damage", parentTable)
  self:Pdouble(m, "suffer_damage", parentTable)
  self:Pfight_report(m, "report", parentTable)
  self:PArray(m, "awards", parentTable, "game_item")
end
function Paser:Pwdc_report_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "atk_list", parentTable, "wdc_fight_report")
  self:PArray(m, "defend_list", parentTable, "wdc_fight_report")
end
function Paser:Ptlc_report_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "atk_list", parentTable, "tlc_fight_report")
  self:PArray(m, "defend_list", parentTable, "tlc_fight_report")
end
function Paser:Pwdc_report_detail_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "id", parentTable)
end
function Paser:Pwdc_report_detail_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pfight_report(m, "report", parentTable)
end
function Paser:Pwdc_awards_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "season", parentTable)
  self:Pinteger(m, "score", parentTable)
  self:Pinteger(m, "rank", parentTable)
  self:Pinteger(m, "ranking", parentTable)
  self:Pinteger(m, "rank_level", parentTable)
  self:Pstring(m, "rank_img", parentTable)
  self:Pinteger(m, "rank_name", parentTable)
  self:Pinteger(m, "left_time", parentTable)
  self:PArray(m, "ranking_awards", parentTable, "wdc_award")
  self:PArray(m, "class_awards", parentTable, "wdc_award")
end
function Paser:Ptlc_awards_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "season", parentTable)
  self:Pinteger(m, "score", parentTable)
  self:Pinteger(m, "rank", parentTable)
  self:Pinteger(m, "ranking", parentTable)
  self:Pinteger(m, "rank_level", parentTable)
  self:Pstring(m, "rank_img", parentTable)
  self:Pinteger(m, "rank_name", parentTable)
  self:Pinteger(m, "left_time", parentTable)
  self:PArray(m, "ranking_awards", parentTable, "tlc_award")
  self:PArray(m, "class_awards", parentTable, "tlc_award")
end
function Paser:Pwdc_server_honor(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "season", parentTable)
  self:Pinteger(m, "start_date", parentTable)
  self:Pinteger(m, "end_date", parentTable)
  self:PArray(m, "top_players", parentTable, "wdc_player_brief")
end
function Paser:Ptlc_server_honor(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "season", parentTable)
  self:Pinteger(m, "start_date", parentTable)
  self:Pinteger(m, "end_date", parentTable)
  self:PArray(m, "top_players", parentTable, "tlc_player_brief")
end
function Paser:Ptlc_player_honor(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "season", parentTable)
  self:Pinteger(m, "start_date", parentTable)
  self:Pinteger(m, "end_date", parentTable)
  self:Pinteger(m, "ranking", parentTable)
  self:Pinteger(m, "rank", parentTable)
  self:Pstring(m, "rank_img", parentTable)
  self:Pinteger(m, "score", parentTable)
end
function Paser:Pwdc_player_honor(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "season", parentTable)
  self:Pinteger(m, "start_date", parentTable)
  self:Pinteger(m, "end_date", parentTable)
  self:Pinteger(m, "ranking", parentTable)
  self:Pinteger(m, "rank", parentTable)
  self:Pstring(m, "rank_img", parentTable)
  self:Pinteger(m, "score", parentTable)
end
function Paser:Pwdc_rank_board_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pwdc_player_brief(m, "self", parentTable)
  self:Pinteger(m, "world_ranking", parentTable)
  self:Pinteger(m, "rank_level", parentTable)
  self:Pstring(m, "rank_img", parentTable)
  self:Pinteger(m, "rank_name", parentTable)
  self:Pinteger(m, "rank_ceil", parentTable)
  self:PArray(m, "top_list", parentTable, "wdc_player_brief")
  self:PArray(m, "class_list", parentTable, "wdc_player_brief")
end
function Paser:Pwdc_honor_wall_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "all", parentTable, "wdc_server_honor")
  self:PArray(m, "self", parentTable, "wdc_player_honor")
end
function Paser:Ptlc_honor_wall_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "all", parentTable, "tlc_server_honor")
  self:PArray(m, "self", parentTable, "tlc_player_honor")
end
function Paser:Pwdc_awards_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "awards", parentTable, "wdc_award")
end
function Paser:Pwdc_awards_get_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
end
function Paser:Pwdc_awards_get_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "code", parentTable)
  self:Pinteger(m, "id", parentTable)
end
function Paser:Pwdc_rankup_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "rank_name", parentTable)
  self:Pstring(m, "rank_img", parentTable)
  self:Pinteger(m, "score", parentTable)
  self:Pinteger(m, "next_rank_name", parentTable)
  self:Pinteger(m, "next_rank_score", parentTable)
end
function Paser:Pwdc_fight_status_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "protect_time", parentTable)
end
function Paser:Pwdc_status_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "status", parentTable)
end
function Paser:Pwelcome_screen_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "logic_id", parentTable)
  self:Pstring(m, "language", parentTable)
end
function Paser:Pwelcome_screen_item(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pstring(m, "type", parentTable)
  self:Pstring(m, "func_name", parentTable)
  self:Pstring(m, "content", parentTable)
  self:Pinteger(m, "order", parentTable)
  self:Pinteger(m, "left_time", parentTable)
end
function Paser:Pwelcome_screen_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "screen_items", parentTable, "welcome_screen_item")
  self:PArray(m, "png_list", parentTable, "string")
end
function Paser:Pwelcome_mask_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
end
function Paser:Ptranslate_info_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "token", parentTable)
  self:Pstring(m, "url", parentTable)
end
function Paser:Pfirst_charge_item(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "product_id", parentTable)
  self:Pboolean(m, "first_buy", parentTable)
  self:Pinteger(m, "fleet_id", parentTable)
  self:Pboolean(m, "own_fleet", parentTable)
  self:Pgame_item(m, "replace_item", parentTable)
  self:PArray(m, "awards", parentTable, "game_item")
  self:Pfleet_show_info(m, "show_info", parentTable)
  self:Pfleet(m, "show_detail", parentTable)
end
function Paser:Pfirst_charge_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "charge_items", parentTable, "first_charge_item")
end
function Paser:Penter_dice_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
end
function Paser:Pdice_cell(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "cell", parentTable)
  self:Pshort(m, "type", parentTable)
  self:Pgame_item(m, "show_item", parentTable)
  self:PArray(m, "loots", parentTable, "game_item")
  self:Pinteger(m, "status", parentTable)
end
function Paser:Pdice_detail(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "current_cell", parentTable)
  self:PArray(m, "cells", parentTable, "dice_cell")
  self:Pgame_item(m, "throw_cost", parentTable)
  self:Pgame_item(m, "refresh_cost", parentTable)
end
function Paser:Pthrow_dice_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pinteger(m, "buy_type", parentTable)
end
function Paser:Pthrow_dice_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "current_cell", parentTable)
  self:Pinteger(m, "dice_value", parentTable)
  self:PArray(m, "update_cells", parentTable, "dice_cell")
  self:Pgame_item(m, "throw_cost", parentTable)
  self:Pgame_item(m, "refresh_cost", parentTable)
end
function Paser:Pdice_awards_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pinteger(m, "cell", parentTable)
end
function Paser:Preset_dice_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pinteger(m, "buy_type", parentTable)
end
function Paser:Pspeed_activity(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "type", parentTable)
  self:Pinteger(m, "status", parentTable)
  self:Pinteger(m, "dungeon_id", parentTable)
  self:Pgame_item(m, "cost_item", parentTable)
end
function Paser:Pspeed_resource_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "items", parentTable, "speed_activity")
end
function Paser:Pfinish_speed_task_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "type", parentTable)
end
function Paser:Pdice_board_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "user", parentTable)
  self:Pgame_item(m, "award", parentTable)
end
function Paser:Pdice_board_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "boards", parentTable, "dice_board_info")
end
function Paser:Pwdc_rank_board_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "type", parentTable)
  self:PArray(m, "players", parentTable, "wdc_player_brief")
end
function Paser:Ptlc_rank_board_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "type", parentTable)
  self:PArray(m, "players", parentTable, "tlc_player_brief")
end
function Paser:Pevent_sub_task(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pdouble(m, "id", parentTable)
  self:Pstring(m, "type", parentTable)
  self:Pinteger(m, "finished_num", parentTable)
  self:Pinteger(m, "total_num", parentTable)
  self:Pinteger(m, "point", parentTable)
  self:PArray(m, "awards", parentTable, "game_item")
  self:Pinteger(m, "status", parentTable)
  self:Pstring(m, "group", parentTable)
  self:Pstring(m, "title", parentTable)
  self:Pstring(m, "desc", parentTable)
end
function Paser:Pevent_progress_award(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "number", parentTable)
  self:PArray(m, "awards", parentTable, "game_item")
  self:Pinteger(m, "status", parentTable)
end
function Paser:Pevent_task(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pdouble(m, "task_id", parentTable)
  self:PArray(m, "sub_tasks", parentTable, "event_sub_task")
  self:Pinteger(m, "finished_num", parentTable)
  self:Pinteger(m, "total_num", parentTable)
  self:PArray(m, "progress_awards", parentTable, "event_progress_award")
  self:Pstring(m, "label", parentTable)
  self:Pinteger(m, "start_time", parentTable)
  self:Pinteger(m, "end_time", parentTable)
  self:Pstring(m, "task_name", parentTable)
end
function Paser:Pevent_area_color(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "area_name", parentTable)
  self:Pstring(m, "color", parentTable)
end
function Paser:Pevent_activity_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pdouble(m, "id", parentTable)
  self:Pstring(m, "label_img", parentTable)
  self:Pstring(m, "icon", parentTable)
  self:Pstring(m, "background_image", parentTable)
  self:PArray(m, "all_tasks", parentTable, "event_task")
  self:Pstring(m, "left_image", parentTable)
  self:Pstring(m, "right_image", parentTable)
  self:Pstring(m, "progressbar_bg", parentTable)
  self:Pboolean(m, "localres", parentTable)
  self:PArray(m, "color", parentTable, "event_area_color")
  self:Pstring(m, "icon_name", parentTable)
end
function Paser:Pevent_activity_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "open_status", parentTable)
  self:Pinteger(m, "gloden_show", parentTable)
  self:Pstring(m, "label", parentTable)
  self:PArray(m, "list", parentTable, "event_activity_info")
end
function Paser:Pevent_activity_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pdouble(m, "id", parentTable)
end
function Paser:Pevent_activity_award_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pdouble(m, "id", parentTable)
  self:Pdouble(m, "task_id", parentTable)
end
function Paser:Puser_change_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "name", parentTable)
  self:Pinteger(m, "sex", parentTable)
end
function Paser:Pevent_progress_award_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pdouble(m, "activity_id", parentTable)
  self:Pdouble(m, "task_id", parentTable)
  self:Pinteger(m, "step", parentTable)
end
function Paser:Pstar_craft_data_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "type", parentTable)
  self:Pinteger(m, "end_time", parentTable)
  self:Pgame_item(m, "reward", parentTable)
  self:Pboolean(m, "open", parentTable)
  self:Pinteger(m, "status", parentTable)
end
function Paser:Pstar_craft_sign_up_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pinteger(m, "logic_id", parentTable)
end
function Paser:Padventure_add_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "left_count", parentTable)
  self:Pinteger(m, "next_price", parentTable)
end
function Paser:Pprogress(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "now_num", parentTable)
  self:Pinteger(m, "total_num", parentTable)
end
function Paser:Pprogress_detail(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "aim_id", parentTable)
  self:Pprogress(m, "progress", parentTable)
end
function Paser:Pbuff_list(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "buff_id", parentTable)
  self:Pinteger(m, "buff_num", parentTable)
end
function Paser:Pmaster_level_data(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "ask_level", parentTable)
  self:PArray(m, "conditions", parentTable, "master_condition")
  self:Pinteger(m, "level", parentTable)
  self:Pinteger(m, "max_master_level", parentTable)
  self:Pprogress(m, "total_progress", parentTable)
  self:PArray(m, "list", parentTable, "buff_list")
  self:PArray(m, "total_progress_detail", parentTable, "progress_detail")
end
function Paser:Pmaster_condition(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "condition_name", parentTable)
  self:PArray(m, "ask", parentTable, "integer")
end
function Paser:Pmaster_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "master_type", parentTable)
  self:Pinteger(m, "matrix_id", parentTable)
  self:PArray(m, "scope", parentTable, "integer")
end
function Paser:Pmaster_all_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "master_all_ack", parentTable, "master_ack")
end
function Paser:Pmaster_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "type", parentTable)
  self:Pinteger(m, "min_level", parentTable)
  self:Pmaster_level_data(m, "now_level_data", parentTable)
  self:Pmaster_level_data(m, "next_level_data", parentTable)
end
function Paser:Precruit_module_data(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pgame_item(m, "buy_item", parentTable)
  self:Pstring(m, "img", parentTable)
  self:Pgame_item(m, "buy_price_one", parentTable)
  self:Pgame_item(m, "buy_price_ten", parentTable)
  self:Pinteger(m, "desc_key", parentTable)
  self:Pboolean(m, "is_activity", parentTable)
  self:Pinteger(m, "activity_time", parentTable)
  self:Pinteger(m, "next_free_time", parentTable)
  self:Pinteger(m, "free_count", parentTable)
  self:Pinteger(m, "max_free_count", parentTable)
  self:Pinteger(m, "image", parentTable)
  self:Pinteger(m, "must_count", parentTable)
end
function Paser:Precruit_list_new_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "type", parentTable)
end
function Paser:Precruit_list_new_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "recruit_list", parentTable, "recruit_module_data")
end
function Paser:Precruit_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pinteger(m, "count", parentTable)
end
function Paser:Pmedal_config_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "type", parentTable)
  self:Pinteger(m, "type_next", parentTable)
  self:Pinteger(m, "quality", parentTable)
  self:Pinteger(m, "rank", parentTable)
  self:Pinteger(m, "level", parentTable)
  self:Pinteger(m, "exp", parentTable)
  self:PArray(m, "attr", parentTable, "attr_pair")
  self:PArray(m, "attr_add", parentTable, "attr_pair")
  self:PArray(m, "attr_rank", parentTable, "attr_pair")
  self:Pinteger(m, "price", parentTable)
  self:Pboolean(m, "is_material", parentTable)
end
function Paser:Precruit_reward_data(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "type", parentTable)
  self:Pboolean(m, "is_replacement", parentTable)
  self:Pgame_item(m, "own_item", parentTable)
  self:Pgame_item(m, "origin_item", parentTable)
  self:Pgame_item(m, "item_reward", parentTable)
  self:Pmedal_config_info(m, "medal", parentTable)
  self:Pfleet_show_info(m, "fleet_reward", parentTable)
end
function Paser:Precruit_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Precruit_module_data(m, "recruit_info", parentTable)
  self:PArray(m, "rewards", parentTable, "recruit_reward_data")
end
function Paser:Precruit_look_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
end
function Paser:Precruit_look_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:PArray(m, "rewards", parentTable, "recruit_reward_data")
end
function Paser:Phero_achievement(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "type", parentTable)
  self:Pstring(m, "img", parentTable)
  self:Pboolean(m, "lock", parentTable)
  self:Pinteger(m, "count", parentTable)
  self:Pinteger(m, "max_count", parentTable)
  self:Pinteger(m, "need_level", parentTable)
  self:Pinteger(m, "need_type", parentTable)
  self:Pinteger(m, "need_count", parentTable)
end
function Paser:Pattribute_pair(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pstring(m, "key", parentTable)
  self:Pinteger(m, "value", parentTable)
  self:Pboolean(m, "is_percent", parentTable)
end
function Paser:Phero_union_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "achievement", parentTable, "hero_achievement")
  self:PArray(m, "attribute", parentTable, "attribute_pair")
end
function Paser:Pcollect_fleet_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "fleet_id", parentTable)
  self:Pboolean(m, "is_own", parentTable)
end
function Paser:Phero_collect_item(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "type", parentTable)
  self:PArray(m, "attributes", parentTable, "attribute_pair")
  self:PArray(m, "collects", parentTable, "collect_fleet_info")
  self:Pboolean(m, "is_own", parentTable)
end
function Paser:Phero_collect_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "fleet_info", parentTable, "fleet_show_info")
  self:PArray(m, "collect_info", parentTable, "hero_collect_item")
end
function Paser:Phero_collect_rank_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pboolean(m, "is_all_server", parentTable)
  self:Pinteger(m, "type", parentTable)
end
function Paser:Pcollect_player_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "name", parentTable)
  self:Pinteger(m, "level", parentTable)
  self:Pinteger(m, "order", parentTable)
  self:Pinteger(m, "collect_count", parentTable)
end
function Paser:Phero_collect_rank_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "type", parentTable)
  self:PArray(m, "players", parentTable, "collect_player_info")
  self:Pcollect_player_info(m, "base_info", parentTable)
end
function Paser:Pred_point_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "states", parentTable, "pair")
end
function Paser:Parea_id_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "item_id", parentTable)
end
function Paser:Parea_id_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "area_id", parentTable)
  self:Pinteger(m, "boss_id", parentTable)
end
function Paser:Pmaster_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "master_ntf_detail", parentTable, "master_ntf_detail")
end
function Paser:Pmaster_ntf_detail(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "master_type", parentTable)
  self:Pinteger(m, "matrix", parentTable)
  self:Pinteger(m, "fleet_id", parentTable)
  self:Pinteger(m, "level", parentTable)
  self:PArray(m, "value", parentTable, "buff_list")
end
function Paser:Pmap_block(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pinteger(m, "status", parentTable)
  self:Pboolean(m, "is_occupy", parentTable)
  self:Pinteger(m, "search_left_time", parentTable)
  self:Pinteger(m, "max_count", parentTable)
  self:Pinteger(m, "left_count", parentTable)
  self:Pinteger(m, "buy_count_credit", parentTable)
  self:Pinteger(m, "buy_count", parentTable)
end
function Paser:Pmining_world_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "flag", parentTable)
  self:Pinteger(m, "dir", parentTable)
end
function Paser:Pmining_world_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "map_blocks", parentTable, "map_block")
end
function Paser:Plog_item(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "time", parentTable)
  self:Pstring(m, "report_id", parentTable)
  self:Pstring(m, "desc", parentTable)
end
function Paser:Pteam_log_item(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "type", parentTable)
  self:Pinteger(m, "time", parentTable)
  self:Pstring(m, "report_id", parentTable)
  self:Pinteger(m, "round", parentTable)
  self:Pstring(m, "desc", parentTable)
end
function Paser:Pmining_world_log_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "logs", parentTable, "log_item")
end
function Paser:Pmining_team_log_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "logs", parentTable, "team_log_item")
end
function Paser:Ppe_block(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "star_level", parentTable)
  self:Pstring(m, "star_frame", parentTable)
  self:Pinteger(m, "time", parentTable)
  self:Pstring(m, "desc", parentTable)
  self:Pinteger(m, "player_id", parentTable)
  self:Pinteger(m, "logic_id", parentTable)
  self:Pinteger(m, "star_id", parentTable)
  self:Pinteger(m, "pos", parentTable)
  self:Pinteger(m, "block", parentTable)
end
function Paser:Pmining_world_pe_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "pe_list", parentTable, "pe_block")
  self:Pinteger(m, "search_left_time", parentTable)
end
function Paser:Pbattle_good_item(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "item_type", parentTable)
  self:Pinteger(m, "number", parentTable)
  self:Pinteger(m, "no", parentTable)
  self:Pinteger(m, "level", parentTable)
  self:Pinteger(m, "max", parentTable)
end
function Paser:Pstar_data(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pboolean(m, "is_occupied", parentTable)
  self:Pboolean(m, "is_my", parentTable)
  self:Pinteger(m, "star_level", parentTable)
  self:Pstring(m, "flag", parentTable)
  self:Pstring(m, "alliance_name", parentTable)
  self:Pinteger(m, "pos", parentTable)
  self:Pinteger(m, "occupie_left_time", parentTable)
  self:Pstring(m, "occupie_name", parentTable)
  self:Pinteger(m, "player_id", parentTable)
  self:Pinteger(m, "logic_id", parentTable)
  self:Pstring(m, "star_frame", parentTable)
  self:Pstring(m, "name_key", parentTable)
  self:Pinteger(m, "percent", parentTable)
  self:PArray(m, "battle_limit", parentTable, "battle_good_item")
end
function Paser:Pmining_wars_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
end
function Paser:Pmining_wars_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "max_count", parentTable)
  self:Pinteger(m, "left_count", parentTable)
  self:Pinteger(m, "refresh_left_time", parentTable)
  self:PArray(m, "star_list", parentTable, "star_data")
  self:Pinteger(m, "refresh_credit", parentTable)
  self:Pinteger(m, "buy_count_credit", parentTable)
  self:Pinteger(m, "buy_count", parentTable)
  self:Pinteger(m, "layer_frame", parentTable)
end
function Paser:Pmining_wars_buy_count_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "left_count", parentTable)
  self:Pinteger(m, "next_buy_credit", parentTable)
end
function Paser:Pmining_wars_star_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
end
function Paser:Pmining_wars_star_data(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "consume_item", parentTable, "game_item")
  self:PArray(m, "produce_item", parentTable, "game_item")
  self:PArray(m, "base_item", parentTable, "game_item")
  self:Pstar_data(m, "star_info", parentTable)
end
function Paser:Pmining_wars_battle_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pinteger(m, "player_id", parentTable)
  self:Pinteger(m, "logic_id", parentTable)
  self:Pboolean(m, "use_credit", parentTable)
  self:Pstring(m, "flag", parentTable)
  self:Pinteger(m, "pos", parentTable)
  self:Pboolean(m, "is_intrusion", parentTable)
  self:Pboolean(m, "is_team_fight", parentTable)
end
function Paser:Pmining_battle_result(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pboolean(m, "is_win", parentTable)
  self:Pstring(m, "star_frame", parentTable)
  self:Pstring(m, "star_name", parentTable)
  self:PArray(m, "consume_item", parentTable, "game_item")
end
function Paser:Pmining_wars_battle_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pboolean(m, "success", parentTable)
  self:Pmining_wars_star_data(m, "result", parentTable)
  self:Pfight_report(m, "report", parentTable)
  self:Pinteger(m, "error_code", parentTable)
  self:Pinteger(m, "switch_credit", parentTable)
  self:Pmining_battle_result(m, "battle_result", parentTable)
end
function Paser:Pmining_wars_collect_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
end
function Paser:Pmining_wars_collect_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pmining_wars_star_data(m, "result", parentTable)
end
function Paser:Pmining_world_battle_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pinteger(m, "player_id", parentTable)
  self:Pinteger(m, "logic_id", parentTable)
end
function Paser:Pmining_world_battle_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pfight_report(m, "report", parentTable)
end
function Paser:Pmining_battle_report_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "id", parentTable)
end
function Paser:Pmining_battle_report_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pfight_report(m, "report", parentTable)
  self:Pmining_battle_result(m, "battle_result", parentTable)
end
function Paser:Pmining_wars_refresh_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
end
function Paser:Pmining_wars_change_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pmining_wars_star_data(m, "star_result", parentTable)
  self:Pinteger(m, "status", parentTable)
end
function Paser:Pmining_reward_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "rewards", parentTable, "game_item")
  self:Pinteger(m, "status", parentTable)
end
function Paser:Pbuttons_status(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "name", parentTable)
  self:Pinteger(m, "count", parentTable)
  self:Pboolean(m, "is_show", parentTable)
  self:Pboolean(m, "show_count", parentTable)
end
function Paser:Pmain_city_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "show_index", parentTable)
  self:PArray(m, "buttons", parentTable, "buttons_status")
end
function Paser:Pmask_show_index_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "show_index", parentTable)
end
function Paser:Pmask_modules_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "module_name", parentTable)
end
function Paser:Pactivity_item(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "type_key", parentTable)
  self:Pboolean(m, "is_new", parentTable)
  self:Pinteger(m, "status", parentTable)
  self:Pinteger(m, "left_time", parentTable)
end
function Paser:Pspace_door_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "activitys", parentTable, "activity_item")
end
function Paser:Phelper_buttons_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "buttons", parentTable, "buttons_status")
end
function Paser:Part_core_decompose_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "core_id", parentTable)
  self:Pinteger(m, "fleet_id", parentTable)
  self:Pinteger(m, "page", parentTable)
end
function Paser:Part_core_decompose_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "is_success", parentTable)
  self:PArray(m, "core_list", parentTable, "art_core")
end
function Paser:Part_main_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pinteger(m, "user_id", parentTable)
end
function Paser:Part_main_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "part_page_list", parentTable, "art_part_page")
  self:Pinteger(m, "resource", parentTable)
  self:Pinteger(m, "now_reset_cost", parentTable)
  self:Pinteger(m, "perfect_reset_cost", parentTable)
  self:PArray(m, "one_key_upgrade", parentTable, "game_item")
  self:PArray(m, "upgrade", parentTable, "game_item")
end
function Paser:Part_core(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pinteger(m, "type", parentTable)
  self:Pinteger(m, "star", parentTable)
  self:Pinteger(m, "level", parentTable)
  self:Pinteger(m, "buff", parentTable)
  self:PArray(m, "add_buff", parentTable, "integer")
  self:Pinteger(m, "color_type", parentTable)
  self:Pinteger(m, "is_corr_type", parentTable)
  self:Pinteger(m, "can_equip", parentTable)
  self:Pinteger(m, "sell_num", parentTable)
end
function Paser:Part_part_page(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "is_locked", parentTable)
  self:Pinteger(m, "id", parentTable)
  self:PArray(m, "point_list", parentTable, "art_point")
  self:Pinteger(m, "core_type", parentTable)
  self:Part_core(m, "now_core", parentTable)
  self:Pinteger(m, "upgrade_red", parentTable)
  self:Pinteger(m, "equip_red", parentTable)
end
function Paser:Parti_update_cost(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "res_type", parentTable)
  self:Pinteger(m, "res_num", parentTable)
end
function Paser:Part_point(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pinteger(m, "type", parentTable)
  self:Pinteger(m, "level", parentTable)
  self:Pinteger(m, "max_level", parentTable)
  self:PArray(m, "update_cost", parentTable, "arti_update_cost")
  self:Pinteger(m, "can_upgrade", parentTable)
  self:PArray(m, "point_buff", parentTable, "point_buff")
  self:PArray(m, "point_next_buff", parentTable, "point_buff")
end
function Paser:Ppoint_buff(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "buff_id", parentTable)
  self:Pinteger(m, "buff_num", parentTable)
end
function Paser:Plevel_point_buff(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pgame_item(m, "need_res", parentTable)
  self:Pinteger(m, "id", parentTable)
  self:Pinteger(m, "level", parentTable)
  self:PArray(m, "next_buff", parentTable, "point_buff")
end
function Paser:Part_point_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Part_point(m, "point_info", parentTable)
end
function Paser:Part_point_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
end
function Paser:Part_point_upgrade_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
end
function Paser:Part_point_upgrade_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "is_success", parentTable)
  self:PArray(m, "part_page_list", parentTable, "art_part_page")
  self:PArray(m, "one_key_upgrade", parentTable, "game_item")
  self:PArray(m, "upgrade", parentTable, "game_item")
end
function Paser:Part_core_list_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "fleet_id", parentTable)
  self:Pinteger(m, "user_id", parentTable)
  self:Pinteger(m, "page", parentTable)
end
function Paser:Part_core_list_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "core_list", parentTable, "art_core")
end
function Paser:Part_core_down_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "core_id", parentTable)
  self:Pinteger(m, "fleet_id", parentTable)
end
function Paser:Part_core_down_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "is_success", parentTable)
  self:PArray(m, "part_page_list", parentTable, "art_part_page")
end
function Paser:Part_core_equip_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "core_id", parentTable)
  self:Pinteger(m, "fleet_id", parentTable)
  self:Pinteger(m, "page", parentTable)
end
function Paser:Part_core_equip_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "is_success", parentTable)
  self:PArray(m, "part_page_list", parentTable, "art_part_page")
end
function Paser:Part_one_upgrade_data_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "fleet_id", parentTable)
  self:Pinteger(m, "user_id", parentTable)
  self:Pinteger(m, "page", parentTable)
end
function Paser:Part_one_upgrade_data_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "max_level", parentTable)
  self:PArray(m, "cur_buff", parentTable, "point_buff")
  self:PArray(m, "all_point_buff", parentTable, "level_point_buff")
end
function Paser:Part_one_upgrade_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "fleet_id", parentTable)
  self:Pinteger(m, "user_id", parentTable)
  self:Pinteger(m, "page", parentTable)
  self:Pinteger(m, "level", parentTable)
  self:Pinteger(m, "id", parentTable)
end
function Paser:Part_one_upgrade_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "point_list", parentTable, "art_point")
  self:PArray(m, "part_page_list", parentTable, "art_part_page")
  self:PArray(m, "one_key_upgrade", parentTable, "game_item")
  self:PArray(m, "upgrade", parentTable, "game_item")
end
function Paser:Part_reset_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "type", parentTable)
  self:Pinteger(m, "fleet_id", parentTable)
end
function Paser:Part_reset_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "is_success", parentTable)
  self:PArray(m, "part_page_list", parentTable, "art_part_page")
  self:PArray(m, "one_key_upgrade", parentTable, "game_item")
  self:PArray(m, "upgrade", parentTable, "game_item")
end
function Paser:Part_can_upgrade_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "fleet_id", parentTable)
end
function Paser:Part_can_upgrade_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "can_upgrade", parentTable)
end
function Paser:Ppair_string(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "str_key", parentTable)
  self:Pstring(m, "str_value", parentTable)
end
function Paser:Pversion_info_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "version_code", parentTable)
  self:PArray(m, "phone_info", parentTable, "pair_string")
end
function Paser:Pevent_log_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "event_type", parentTable)
  self:Pstring(m, "event_name", parentTable)
  self:Pstring(m, "event_param", parentTable)
end
function Paser:Pchoosable_item_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
end
function Paser:Pgame_choosable_item(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "item_type", parentTable)
  self:Pinteger(m, "number", parentTable)
  self:Pinteger(m, "no", parentTable)
  self:Pinteger(m, "level", parentTable)
  self:PArray(m, "krypton_value", parentTable, "buff_list")
  self:Pinteger(m, "can_use_more", parentTable)
end
function Paser:Pchoosable_item_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "choosable_items", parentTable, "game_choosable_item")
end
function Paser:Pchoosable_item_use_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pinteger(m, "count", parentTable)
  self:Pgame_choosable_item(m, "item", parentTable)
end
function Paser:Pfte_value(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "key", parentTable)
  self:Pstring(m, "value", parentTable)
end
function Paser:Pfte_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "content", parentTable, "fte_value")
end
function Paser:Pfte_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "is_success", parentTable)
end
function Paser:Pfte_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
end
function Paser:Pachievement_reward_receive_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
end
function Paser:Pachievement_reward_receive_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:PArray(m, "achievement_list", parentTable, "achievement_info")
end
function Paser:Pgift_item(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "id", parentTable)
  self:Pstring(m, "product_id", parentTable)
  self:PArray(m, "goods", parentTable, "game_item")
  self:Pstring(m, "icon_pic", parentTable)
  self:Pstring(m, "bg_pic", parentTable)
  self:Pstring(m, "bg_circle_pic", parentTable)
  self:Pstring(m, "bg_bottom_pic", parentTable)
  self:Pstring(m, "bg_limit_pic", parentTable)
  self:Pshort(m, "tip_type", parentTable)
  self:Pdouble(m, "discount", parentTable)
  self:Pinteger(m, "left_time", parentTable)
  self:Pshort(m, "limit_type", parentTable)
  self:Pinteger(m, "limit_count", parentTable)
  self:Pinteger(m, "order", parentTable)
  self:Pboolean(m, "can_batch", parentTable)
  self:PArray(m, "viprange", parentTable, "integer")
  self:Pboolean(m, "can_show", parentTable)
end
function Paser:Ppay_gifts_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "gifts", parentTable, "gift_item")
end
function Paser:Pwish_pay_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "id", parentTable)
  self:Pstring(m, "product_id", parentTable)
  self:Pinteger(m, "count", parentTable)
end
function Paser:Pvip_item(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "vip", parentTable)
  self:PArray(m, "rewards", parentTable, "game_item")
end
function Paser:Pvip_reward_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "vip_items", parentTable, "vip_item")
end
function Paser:Pmonth_card_item(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "id", parentTable)
  self:Pinteger(m, "order", parentTable)
  self:Pstring(m, "bg_pic", parentTable)
  self:Pstring(m, "product_id", parentTable)
  self:Pshort(m, "status", parentTable)
  self:Pinteger(m, "state_day", parentTable)
  self:Pinteger(m, "left_time", parentTable)
  self:Pshort(m, "type", parentTable)
  self:Pstring(m, "title_key", parentTable)
  self:Pstring(m, "desc_key", parentTable)
  self:Pgame_item(m, "reward", parentTable)
  self:Pshort(m, "can_buy", parentTable)
end
function Paser:Pmonth_card_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "month_cards", parentTable, "month_card_item")
end
function Paser:Pmonth_card_receive_reward_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "id", parentTable)
end
function Paser:Pmonth_card_receive_reward_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "month_cards", parentTable, "month_card_item")
end
function Paser:Pleader_info_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "leader_ids", parentTable, "integer")
  self:PArray(m, "options", parentTable, "integer")
  self:PArray(m, "active_spells", parentTable, "integer")
end
function Paser:Pchange_leader_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
end
function Paser:Pchange_tlc_leader_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "index", parentTable)
  self:Pinteger(m, "id", parentTable)
end
function Paser:Pget_reward_detail_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "itemId", parentTable)
end
function Paser:Preward_detail(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "details", parentTable, "game_item")
end
function Paser:Psave_user_id_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "id", parentTable)
  self:Pstring(m, "name", parentTable)
end
function Paser:Puser_id_status_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pboolean(m, "isAuth", parentTable)
  self:Pstring(m, "id", parentTable)
  self:Pstring(m, "name", parentTable)
end
function Paser:Pstore_coupon(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "item_id", parentTable)
  self:Pshort(m, "discount", parentTable)
end
function Paser:Pitem_classify_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "equip_enhant_items", parentTable, "integer")
  self:PArray(m, "store_coupons", parentTable, "store_coupon")
end
function Paser:Pforce_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "server_id", parentTable)
  self:Pinteger(m, "user_id", parentTable)
  self:Pstring(m, "name", parentTable)
  self:Pstring(m, "avator", parentTable)
  self:Pdouble(m, "power", parentTable)
  self:PArray(m, "fleets", parentTable, "pair")
  self:Pshort(m, "position", parentTable)
end
function Paser:Pconcentrate_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pdouble(m, "id", parentTable)
  self:Pstring(m, "name", parentTable)
  self:Pdouble(m, "enter_power", parentTable)
  self:Pshort(m, "force_count", parentTable)
  self:PArray(m, "forces", parentTable, "force_info")
  self:Pshort(m, "status", parentTable)
  self:Pinteger(m, "left_time", parentTable)
end
function Paser:Pforces_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "forces_list", parentTable, "concentrate_info")
end
function Paser:Pconcentrate_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pdouble(m, "power", parentTable)
  self:Pshort(m, "count", parentTable)
end
function Paser:Pconcentrate_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pdouble(m, "team_id", parentTable)
  self:Pinteger(m, "code", parentTable)
end
function Paser:Pconcentrate_end_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pdouble(m, "id", parentTable)
end
function Paser:Pconcentrate_end_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "code", parentTable)
end
function Paser:Pforce_join_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pdouble(m, "id", parentTable)
end
function Paser:Pforce_join_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "code", parentTable)
end
function Paser:Pconcentrate_atk_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "star_id", parentTable)
end
function Paser:Pconcentrate_atk_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "code", parentTable)
end
function Paser:Pgroup_fight_header(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "p1_names", parentTable, "string")
  self:PArray(m, "p2_names", parentTable, "string")
  self:Pshort(m, "group_id", parentTable)
  self:Pfight_report(m, "report", parentTable)
  self:Pinteger(m, "round_cnt", parentTable)
end
function Paser:Pgroup_fight_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "headers", parentTable, "group_fight_header")
end
function Paser:Pchange_atk_seq_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "seq", parentTable, "pair")
end
function Paser:Pchange_atk_seq_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "code", parentTable)
end
function Paser:Pforces_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "self_server_id", parentTable)
  self:Pinteger(m, "self_user_id", parentTable)
  self:PArray(m, "forces_list", parentTable, "concentrate_info")
end
function Paser:Pmy_forces_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "self_server_id", parentTable)
  self:Pinteger(m, "self_user_id", parentTable)
  self:Pconcentrate_info(m, "team", parentTable)
end
function Paser:Pteam_fight_report_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "report_id", parentTable)
  self:Pinteger(m, "round", parentTable)
end
function Paser:Pfair_arena_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "state", parentTable)
  self:Pboolean(m, "is_submit", parentTable)
  self:Pinteger(m, "left_time", parentTable)
  self:Pinteger(m, "season_left_time", parentTable)
  self:Pinteger(m, "battle_left_time", parentTable)
end
function Paser:Pfair_arena_rank_item(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "player_id", parentTable)
  self:Pstring(m, "player_name", parentTable)
  self:Pstring(m, "logic_id", parentTable)
  self:Pinteger(m, "score", parentTable)
  self:Pinteger(m, "win_rate", parentTable)
  self:Pinteger(m, "win_count", parentTable)
  self:Pinteger(m, "rank_id", parentTable)
  self:Pstring(m, "avatar", parentTable)
  self:Pinteger(m, "level", parentTable)
  self:Pboolean(m, "show_check", parentTable)
end
function Paser:Pfair_arena_season_rank_item(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "season_id", parentTable)
  self:PArray(m, "season_items", parentTable, "fair_arena_rank_item")
end
function Paser:Pfair_arena_rank_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "rank_list", parentTable, "fair_arena_rank_item")
  self:PArray(m, "season_rank_list", parentTable, "fair_arena_season_rank_item")
end
function Paser:Pfair_arena_season_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "season_id", parentTable)
end
function Paser:Pfair_arena_season_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "season_rank_list", parentTable, "fair_arena_rank_item")
end
function Paser:Pfair_rank_reward_item(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:PArray(m, "reward_items", parentTable, "game_item")
  self:Pinteger(m, "level", parentTable)
  self:Pinteger(m, "state", parentTable)
end
function Paser:Pfair_arena_reward_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "reward_list", parentTable, "fair_rank_reward_item")
end
function Paser:Pfair_report_item(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pinteger(m, "time", parentTable)
  self:Pinteger(m, "state", parentTable)
  self:Pinteger(m, "opponent_state", parentTable)
  self:Puser_champion_data(m, "own", parentTable)
  self:Puser_champion_data(m, "opponent", parentTable)
end
function Paser:Pfair_arena_report_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "player_id", parentTable)
  self:Pstring(m, "logic_id", parentTable)
end
function Paser:Pfair_arena_report_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "win_rate", parentTable)
  self:Pinteger(m, "win_count", parentTable)
  self:Pinteger(m, "fail_count", parentTable)
  self:PArray(m, "report_list", parentTable, "fair_report_item")
end
function Paser:Pfair_arena_reward_get_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
end
function Paser:Pfair_arena_report_battle_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
end
function Paser:Pfair_arena_report_battle_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pfight_report(m, "report", parentTable)
  self:Pmining_battle_result(m, "battle_result", parentTable)
end
function Paser:Pbasic_fleet_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "fleet_id", parentTable)
  self:Pinteger(m, "fleet_level", parentTable)
  self:Pinteger(m, "sex", parentTable)
  self:Pshort(m, "vessels", parentTable)
  self:PArray(m, "adjust_info", parentTable, "integer")
  self:Pshort(m, "spell_id", parentTable)
end
function Paser:Pformation_fleet_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pbasic_fleet_info(m, "fleet", parentTable)
  self:Pbasic_fleet_info(m, "adjutant", parentTable)
end
function Paser:Pformation_detail(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pformation_fleet_info(m, "fleet_info", parentTable)
  self:Pinteger(m, "pos", parentTable)
end
function Paser:Pfair_arena_fleet_pool(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "hero_list", parentTable, "basic_fleet_info")
  self:PArray(m, "adjutant_list", parentTable, "basic_fleet_info")
end
function Paser:Pfair_arena_formation_all_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "formation_1", parentTable, "formation_detail")
  self:PArray(m, "formation_2", parentTable, "formation_detail")
  self:PArray(m, "formation_3", parentTable, "formation_detail")
  self:Pfair_arena_fleet_pool(m, "selectable_fleets", parentTable)
  self:Pgame_items(m, "refresh_price", parentTable)
  self:Pboolean(m, "can_refresh", parentTable)
  self:PArray(m, "active_spell", parentTable, "integer")
end
function Paser:Pupdate_formations_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "formation_1", parentTable, "formation_detail")
  self:PArray(m, "formation_2", parentTable, "formation_detail")
  self:PArray(m, "formation_3", parentTable, "formation_detail")
end
function Paser:Pupdate_formations_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "result", parentTable)
end
function Paser:Prefresh_fleet_pool_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "result", parentTable)
  self:Pfair_arena_fleet_pool(m, "fleet_pool", parentTable)
  self:Pgame_items(m, "next_refresh_price", parentTable)
  self:Pboolean(m, "can_refresh", parentTable)
end
function Paser:Pfair_arena_fleet_info_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "fleet_id", parentTable)
  self:Pinteger(m, "level", parentTable)
  self:Pinteger(m, "type", parentTable)
end
function Paser:Pprice_off_store(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "group", parentTable)
  self:Pboolean(m, "status", parentTable)
  self:Pinteger(m, "left_seconds", parentTable)
end
function Paser:Pprice_off_store_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "store_infos", parentTable, "price_off_store")
end
function Paser:Pfte_gift_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "gifts", parentTable, "game_item")
end
function Paser:Pmining_wars_buy_count_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "block", parentTable)
end
function Paser:Pprice_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "off_count", parentTable)
  self:Pshort(m, "origin_price", parentTable)
  self:Pshort(m, "price", parentTable)
end
function Paser:Penter_scratch_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "act_id", parentTable)
end
function Paser:Paward_condition(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "award_id", parentTable)
  self:Pgame_item(m, "award", parentTable)
  self:Pgame_items(m, "condition", parentTable)
  self:Pshort(m, "award_status", parentTable)
  self:Pshort(m, "can_buy_times", parentTable)
  self:Pboolean(m, "can_times_buy", parentTable)
end
function Paser:Pscr_progress(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "max", parentTable)
  self:Pshort(m, "now", parentTable)
  self:PArray(m, "times_award", parentTable, "award_pos")
end
function Paser:Paward_pos(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "times", parentTable)
  self:Pgame_items(m, "award", parentTable)
  self:Pshort(m, "award_status", parentTable)
end
function Paser:Pexchange_item(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pgame_items(m, "item", parentTable)
end
function Paser:Pcurrence_cost(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pgame_items(m, "cost", parentTable)
end
function Paser:Pbg_cfg(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "bg_image", parentTable)
  self:Pstring(m, "scratch_image", parentTable)
  self:Pstring(m, "ui_image", parentTable)
end
function Paser:Penter_scratch_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pprice_info(m, "price", parentTable)
  self:Pgame_item(m, "reset_price", parentTable)
  self:PArray(m, "awards", parentTable, "award_condition")
  self:Pscr_progress(m, "progress_award", parentTable)
  self:Pexchange_item(m, "items", parentTable)
  self:Pcurrence_cost(m, "costs", parentTable)
  self:Pbg_cfg(m, "background", parentTable)
  self:PArray(m, "origin_award", parentTable, "game_item")
end
function Paser:Pbuy_scratch_card_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "act_id", parentTable)
end
function Paser:Pbuy_scratch_card_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pgame_items(m, "award", parentTable)
  self:Pscr_progress(m, "progress_award", parentTable)
  self:Pexchange_item(m, "items", parentTable)
  self:Pprice_info(m, "next_price", parentTable)
end
function Paser:Preset_scratch_card_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "act_id", parentTable)
end
function Paser:Preseting_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pscr_progress(m, "progress_award", parentTable)
  self:Pexchange_item(m, "exchange", parentTable)
  self:Pgame_item(m, "reset_price", parentTable)
  self:Pprice_info(m, "price", parentTable)
  self:PArray(m, "awards", parentTable, "award_condition")
end
function Paser:Pget_process_award_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "act_id", parentTable)
  self:Pshort(m, "award_id", parentTable)
end
function Paser:Pget_process_award_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pscr_progress(m, "progress_award", parentTable)
  self:Pexchange_item(m, "items", parentTable)
  self:Pshort(m, "code", parentTable)
end
function Paser:Pexchange_award_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "act_id", parentTable)
  self:Pshort(m, "award_id", parentTable)
  self:Pshort(m, "num", parentTable)
end
function Paser:Pend_scratch_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "act_id", parentTable)
end
function Paser:Pend_scratch_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pexchange_item(m, "item", parentTable)
  self:PArray(m, "awards", parentTable, "award_condition")
end
function Paser:Pexchange_award_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "awards", parentTable, "award_condition")
  self:Pexchange_item(m, "items", parentTable)
  self:Pshort(m, "code", parentTable)
  self:Pgame_item(m, "reset_price", parentTable)
end
function Paser:Puc_pay_sign_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "pay_info", parentTable, "pair_string")
end
function Paser:Puc_pay_sign_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "sign", parentTable)
  self:Pstring(m, "account_id", parentTable)
end
function Paser:Pscratch_board_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "user", parentTable)
  self:Pgame_item(m, "award", parentTable)
end
function Paser:Pscratch_board_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "boards", parentTable, "scratch_board_info")
end
function Paser:Pconfirm_exchange_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "api", parentTable)
  self:PArray(m, "item", parentTable, "game_item")
end
function Paser:Pnormal_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "map", parentTable, "pair_string")
end
function Paser:Pchange_fleets_data_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "matrix_index", parentTable)
  self:Pinteger(m, "fleet_id", parentTable)
end
function Paser:Pchange_fleets_data_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "fleets", parentTable, "fleet_show_info")
end
function Paser:Pchange_fleets_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "matrix_index", parentTable)
  self:Pinteger(m, "old_fleet_id", parentTable)
  self:Pinteger(m, "new_fleet_id", parentTable)
end
function Paser:Plogin_award_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "award_id", parentTable)
  self:Pshort(m, "award_status", parentTable)
  self:PArray(m, "awards", parentTable, "game_item")
  self:Pshort(m, "vip_level", parentTable)
end
function Paser:Plogin_sign_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "now_login", parentTable)
  self:Pboolean(m, "is_new", parentTable)
  self:PArray(m, "awards", parentTable, "login_award_info")
end
function Paser:Pget_login_sign_award_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "award_id", parentTable)
end
function Paser:Pget_login_sign_award_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "code", parentTable)
end
function Paser:Pkrypton_redpoint_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "matrix_index", parentTable)
  self:Pinteger(m, "fleet_id", parentTable)
end
function Paser:Pkrypton_redpoint_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pboolean(m, "show_krypton_red", parentTable)
  self:Pboolean(m, "show_artfact_red", parentTable)
  self:Pboolean(m, "show_artupgrade_red", parentTable)
end
function Paser:Predownload_res_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "res_list", parentTable, "pair_string")
end
function Paser:Popen_box_progress_award_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "box_id", parentTable)
  self:Pinteger(m, "step", parentTable)
end
function Paser:Pexchange_regulation_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "level", parentTable)
  self:PArray(m, "regulation", parentTable, "pair_string")
end
function Paser:Pcredit_exchange(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pgame_item(m, "item", parentTable)
  self:Pinteger(m, "credit", parentTable)
end
function Paser:Pmul_credit_exchange_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "api", parentTable)
  self:PArray(m, "exchange", parentTable, "credit_exchange")
end
function Paser:Pmul_credit_exchange_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "api", parentTable)
  self:PArray(m, "exchange", parentTable, "credit_exchange")
end
function Paser:Penter_medal_store_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "act_id", parentTable)
  self:PArray(m, "items", parentTable, "medal_store_item")
  self:Pgame_item(m, "reset_price", parentTable)
  self:Pshort(m, "currency_times", parentTable)
  self:Pboolean(m, "is_activity", parentTable)
  self:Pshort(m, "max_times", parentTable)
  self:Pinteger(m, "time_left", parentTable)
end
function Paser:Pmedal_store_buy_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "act_id", parentTable)
  self:Pinteger(m, "item_id", parentTable)
end
function Paser:Pmedal_store_buy_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "code", parentTable)
  self:PArray(m, "buy_item", parentTable, "game_item")
  self:Pgame_item(m, "reset_price", parentTable)
end
function Paser:Pmedal_store_refresh_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "act_id", parentTable)
end
function Paser:Pmedal_store_refresh_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "items", parentTable, "medal_store_item")
  self:Pgame_item(m, "reset_price", parentTable)
  self:Pshort(m, "currency_times", parentTable)
  self:Pshort(m, "max_times", parentTable)
  self:Pinteger(m, "time_left", parentTable)
end
function Paser:Pattr_pair(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "attr_id", parentTable)
  self:Pdouble(m, "attr_value", parentTable)
end
function Paser:Pmedal_material_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "type", parentTable)
  self:Pinteger(m, "quality", parentTable)
  self:Pinteger(m, "exp", parentTable)
  self:Pinteger(m, "price", parentTable)
  self:Pinteger(m, "num", parentTable)
end
function Paser:Pmedal_consume(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "medals", parentTable, "medal_material_info")
  self:PArray(m, "materials", parentTable, "medal_material_info")
end
function Paser:Pmedal_formation_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "medal_id", parentTable)
  self:Pinteger(m, "fleet_id", parentTable)
  self:Pinteger(m, "formation_id", parentTable)
  self:Pinteger(m, "pos", parentTable)
end
function Paser:Pmedal_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pinteger(m, "type", parentTable)
  self:Pinteger(m, "type_next", parentTable)
  self:Pinteger(m, "quality", parentTable)
  self:Pinteger(m, "rank", parentTable)
  self:Pinteger(m, "level", parentTable)
  self:Pinteger(m, "exp", parentTable)
  self:Pboolean(m, "is_material", parentTable)
  self:Pinteger(m, "material_exp", parentTable)
  self:Pboolean(m, "can_level_up", parentTable)
  self:Pboolean(m, "can_rank", parentTable)
  self:PArray(m, "can_replace", parentTable, "pair")
  self:Pinteger(m, "rank_level_limit", parentTable)
  self:Pinteger(m, "decompose_price", parentTable)
  self:Pmedal_consume(m, "reset_material", parentTable)
  self:Pdouble(m, "reset_ratio", parentTable)
  self:Pinteger(m, "target_pos", parentTable)
  self:PArray(m, "formation_info", parentTable, "medal_formation_info")
end
function Paser:Pmedal_fleet_formation(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "fleet_id", parentTable)
  self:Pinteger(m, "formation_id", parentTable)
  self:PArray(m, "equips", parentTable, "pair")
end
function Paser:Pmedal_formation_pos_red_point_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "formation_id", parentTable)
  self:Pinteger(m, "fleet_id", parentTable)
  self:PArray(m, "red_points", parentTable, "pair")
end
function Paser:Pmedal_formation_fleet_btn_red_point_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "formation_id", parentTable)
  self:Pinteger(m, "fleet_id", parentTable)
  self:Pinteger(m, "red_point", parentTable)
end
function Paser:Pmedal_list_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "currency", parentTable)
end
function Paser:Pmedal_decompose_confirm_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "medals", parentTable, "integer")
end
function Paser:Pmedal_decompose_confirm_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "currency", parentTable)
end
function Paser:Pmedal_reset_confirm_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
end
function Paser:Pmedal_reset_confirm_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pmedal_info(m, "medal", parentTable)
  self:PArray(m, "materials", parentTable, "medal_material_info")
end
function Paser:Pmedal_equip_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "materials", parentTable, "medal_material_info")
  self:PArray(m, "formations", parentTable, "medal_fleet_formation")
end
function Paser:Pmedal_level_up_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:PArray(m, "medals", parentTable, "integer")
  self:PArray(m, "materials", parentTable, "pair")
end
function Paser:Pmedal_level_up_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "materials", parentTable, "medal_material_info")
  self:PArray(m, "consumes", parentTable, "integer")
end
function Paser:Pmedal_rank_up_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pinteger(m, "rank_target", parentTable)
end
function Paser:Pmedal_rank_up_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "materials", parentTable, "medal_material_info")
  self:PArray(m, "consumes", parentTable, "integer")
end
function Paser:Pmedal_list_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "medals", parentTable, "medal_info")
end
function Paser:Pmedal_fleet_formation_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "formations", parentTable, "medal_fleet_formation")
end
function Paser:Pmedal_fleet_red_point_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "formations", parentTable, "medal_formation_pos_red_point_info")
end
function Paser:Pmedal_equip_on_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "medal_id", parentTable)
  self:Pinteger(m, "fleet_id", parentTable)
  self:Pinteger(m, "formation_id", parentTable)
  self:Pinteger(m, "pos", parentTable)
end
function Paser:Pmedal_equip_on_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pboolean(m, "success", parentTable)
end
function Paser:Pmedal_equip_off_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "medal_id", parentTable)
  self:Pinteger(m, "fleet_id", parentTable)
  self:Pinteger(m, "formation_id", parentTable)
  self:Pinteger(m, "pos", parentTable)
end
function Paser:Pmedal_equip_off_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pboolean(m, "success", parentTable)
end
function Paser:Pmedal_equip_replace_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "medal_id", parentTable)
  self:Pinteger(m, "replaced_id", parentTable)
  self:Pinteger(m, "fleet_id", parentTable)
  self:Pinteger(m, "formation_id", parentTable)
  self:Pinteger(m, "pos", parentTable)
end
function Paser:Pmedal_equip_replace_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pboolean(m, "success", parentTable)
end
function Paser:Pmedal_fleet_btn_red_point_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "btns", parentTable, "medal_formation_fleet_btn_red_point_info")
end
function Paser:Pmedal_config_info_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "type", parentTable)
end
function Paser:Pmedal_config_info_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pmedal_config_info(m, "medal", parentTable)
end
function Paser:Pmedal_resource_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "currency", parentTable)
  self:PArray(m, "materials", parentTable, "medal_material_info")
end
function Paser:Pfight_report_round_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "rounds", parentTable, "fight_round")
end
function Paser:Pmedal_boss_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "boss_id", parentTable)
  self:Pshort(m, "difficuity", parentTable)
  self:Pshort(m, "free_times", parentTable)
  self:Pgame_item(m, "reset_price", parentTable)
  self:PArray(m, "award_list", parentTable, "game_item")
  self:Pshort(m, "max_degree", parentTable)
  self:Pshort(m, "residue_degree", parentTable)
  self:Pboolean(m, "can_notify", parentTable)
  self:Pdouble(m, "force", parentTable)
  self:Pstring(m, "ship", parentTable)
  self:PArray(m, "monster_matrix", parentTable, "monster_matrix_cell")
end
function Paser:Penter_medal_boss_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "act_id", parentTable)
  self:Pmedal_boss_info(m, "boss_info", parentTable)
end
function Paser:Preset_medal_boss_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "act_id", parentTable)
end
function Paser:Preset_medal_boss_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pmedal_boss_info(m, "boss_info", parentTable)
end
function Paser:Pchallenge_medal_boss_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "act_id", parentTable)
end
function Paser:Pchallenge_medal_boss_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pmedal_boss_info(m, "boss_info", parentTable)
  self:Pfight_report(m, "report", parentTable)
  self:Pboolean(m, "result", parentTable)
  self:PArray(m, "awards", parentTable, "game_item")
end
function Paser:Pglory_collection_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "type", parentTable)
  self:Pstring(m, "img", parentTable)
  self:Pboolean(m, "open", parentTable)
  self:Ppair(m, "activate", parentTable)
  self:PArray(m, "glories", parentTable, "glory_info")
  self:PArray(m, "glory_medals", parentTable, "glory_medal_info")
  self:Pinteger(m, "red_point", parentTable)
end
function Paser:Pglory_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Ppair(m, "type", parentTable)
  self:Pinteger(m, "status", parentTable)
  self:Pinteger(m, "rank", parentTable)
  self:PArray(m, "attr", parentTable, "attr_pair")
  self:PArray(m, "glory_medals", parentTable, "integer")
end
function Paser:Pglory_medal_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "type", parentTable)
  self:Pinteger(m, "num", parentTable)
  self:PArray(m, "attr", parentTable, "attr_pair")
  self:PArray(m, "previews", parentTable, "glory_attr_preview")
end
function Paser:Pglory_attr_preview(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Ppair(m, "type", parentTable)
  self:Pinteger(m, "medal_type", parentTable)
  self:Pinteger(m, "status", parentTable)
  self:Pinteger(m, "rank", parentTable)
  self:PArray(m, "attr", parentTable, "attr_pair")
end
function Paser:Pglory_attr_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Ppair(m, "type", parentTable)
  self:Pinteger(m, "rank", parentTable)
  self:Pinteger(m, "rank_max", parentTable)
  self:PArray(m, "attr", parentTable, "attr_pair")
end
function Paser:Pmedal_glory_collection_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "type", parentTable)
end
function Paser:Pmedal_glory_collection_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "total_glory", parentTable)
  self:PArray(m, "total_attr", parentTable, "attr_pair")
  self:PArray(m, "glory_infos", parentTable, "glory_attr_info")
end
function Paser:Pmedal_glory_collection_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "glory_collections", parentTable, "glory_collection_info")
end
function Paser:Pmedal_glory_collection_enter_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "type", parentTable)
end
function Paser:Pmedal_glory_collection_enter_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pglory_collection_info(m, "glory_collection", parentTable)
end
function Paser:Pmedal_glory_collection_total_attr_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "total_attr", parentTable, "attr_pair")
end
function Paser:Pmedal_glory_activate_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Ppair(m, "type", parentTable)
end
function Paser:Pmedal_glory_activate_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "total_glory", parentTable)
  self:PArray(m, "total_attr", parentTable, "attr_pair")
  self:Pglory_collection_info(m, "glory_collection", parentTable)
end
function Paser:Pmedal_glory_rank_up_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Ppair(m, "type", parentTable)
end
function Paser:Pmedal_glory_rank_up_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "total_glory", parentTable)
  self:PArray(m, "total_attr", parentTable, "attr_pair")
  self:Pglory_collection_info(m, "glory_collection", parentTable)
end
function Paser:Pmedal_glory_reset_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Ppair(m, "type", parentTable)
end
function Paser:Pmedal_glory_reset_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "total_glory", parentTable)
  self:PArray(m, "total_attr", parentTable, "attr_pair")
  self:Pglory_collection_info(m, "glory_collection", parentTable)
end
function Paser:Pglory_achievement_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "type", parentTable)
  self:Pinteger(m, "need", parentTable)
  self:PArray(m, "attr", parentTable, "attr_pair")
end
function Paser:Pmedal_glory_achievement_red_point_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "is_red", parentTable)
end
function Paser:Pmedal_glory_achievement_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "activated", parentTable)
  self:Pinteger(m, "can_activate", parentTable)
  self:PArray(m, "total_attr", parentTable, "attr_pair")
  self:PArray(m, "achievements", parentTable, "glory_achievement_info")
end
function Paser:Pmedal_glory_achievement_activate_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "activated", parentTable)
  self:Pinteger(m, "can_activate", parentTable)
  self:PArray(m, "total_attr", parentTable, "attr_pair")
end
function Paser:Pglory_rank_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "type", parentTable)
  self:Pinteger(m, "rank", parentTable)
  self:Ppair(m, "user_key", parentTable)
  self:Pstring(m, "user_name", parentTable)
  self:Pinteger(m, "glory", parentTable)
  self:Pinteger(m, "time", parentTable)
end
function Paser:Pglory_collection_rank_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "type", parentTable)
  self:Pglory_rank_info(m, "self", parentTable)
  self:PArray(m, "tops", parentTable, "glory_rank_info")
end
function Paser:Pmedal_glory_collection_rank_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "glory_collection_ranks", parentTable, "glory_collection_rank_info")
end
function Paser:Phero_union_unlock_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "fleet_id", parentTable)
  self:Pinteger(m, "unlock_count", parentTable)
  self:PArray(m, "attribute", parentTable, "attribute_pair")
end
function Paser:Plarge_map_build_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "build_type", parentTable)
  self:Pboolean(m, "build_show", parentTable)
end
function Paser:Pexpedition_rank_awards_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "ranking_awards", parentTable, "expedition_rate_award")
end
function Paser:Pexpedition_rate_award(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pinteger(m, "begin_rank", parentTable)
  self:Pinteger(m, "end_rank", parentTable)
  self:PArray(m, "awards", parentTable, "game_item")
end
function Paser:Parmy_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
end
function Paser:Plarge_map_alliance_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "id", parentTable)
  self:Pstring(m, "name", parentTable)
  self:Pfloat(m, "rate", parentTable)
  self:Pinteger(m, "block_count", parentTable)
  self:Pinteger(m, "people_count", parentTable)
  self:Pinteger(m, "total_fleet", parentTable)
  self:Pinteger(m, "online_fleet", parentTable)
  self:Pinteger(m, "max_fleet", parentTable)
  self:Pfloat(m, "expend", parentTable)
  self:Pinteger(m, "core_star", parentTable)
end
function Paser:Plarge_map_repair_simple_item(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "identity", parentTable)
  self:Pstring(m, "ship", parentTable)
  self:Pstring(m, "avatar", parentTable)
  self:Pinteger(m, "level", parentTable)
  self:Pinteger(m, "color", parentTable)
  self:Pinteger(m, "percent", parentTable)
end
function Paser:Plarge_map_player_detail(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "player_id", parentTable)
  self:Pinteger(m, "owner_code", parentTable)
  self:Pinteger(m, "cur_energy", parentTable)
  self:Pinteger(m, "max_energy", parentTable)
  self:Pinteger(m, "pos", parentTable)
  self:Plarge_map_alliance_info(m, "alliance_info", parentTable)
  self:Pshort(m, "alliance_rank", parentTable)
  self:Pshort(m, "status", parentTable)
  self:Pinteger(m, "damaged_percent", parentTable)
  self:Pstring(m, "troop_id", parentTable)
  self:Pinteger(m, "troop_time", parentTable)
  self:PArray(m, "fleets", parentTable, "large_map_repair_simple_item")
  self:Pinteger(m, "common_money", parentTable)
  self:Pinteger(m, "exploit_money", parentTable)
  self:Pbattle_good_item(m, "buy_energy", parentTable)
end
function Paser:Plarge_map_block_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "col_left", parentTable)
  self:Pinteger(m, "col_right", parentTable)
  self:Pinteger(m, "row_up", parentTable)
  self:Pinteger(m, "row_down", parentTable)
end
function Paser:Plarge_block_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pinteger(m, "type", parentTable)
  self:Pinteger(m, "build_type", parentTable)
  self:Pinteger(m, "owner", parentTable)
  self:Pshort(m, "color", parentTable)
  self:Pboolean(m, "show", parentTable)
  self:Pshort(m, "fleet_type", parentTable)
  self:Pshort(m, "status", parentTable)
  self:Pshort(m, "side_status", parentTable)
  self:Pstring(m, "alliance_id", parentTable)
  self:Pstring(m, "alliance_name", parentTable)
  self:Pinteger(m, "total_time", parentTable)
  self:Pinteger(m, "left_time", parentTable)
  self:Pinteger(m, "occupy_color", parentTable)
  self:Pinteger(m, "occupy_owner", parentTable)
  self:Pinteger(m, "rotation", parentTable)
end
function Paser:Plarge_map_block_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "block_list", parentTable, "large_block_info")
end
function Paser:Pbg_block_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pinteger(m, "frame", parentTable)
end
function Paser:Pnebula_block_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pinteger(m, "pos_x", parentTable)
  self:Pinteger(m, "pos_y", parentTable)
  self:Pinteger(m, "frame", parentTable)
end
function Paser:Plarge_map_base_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "row_count", parentTable)
  self:Pinteger(m, "col_count", parentTable)
  self:Pinteger(m, "center_id", parentTable)
  self:PArray(m, "bg_list", parentTable, "bg_block_info")
  self:PArray(m, "nebula_list", parentTable, "nebula_block_info")
  self:Plarge_map_player_detail(m, "my_info", parentTable)
  self:PArray(m, "build_list", parentTable, "short")
end
function Paser:Plarge_map_block_update_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "block_list", parentTable, "large_block_info")
end
function Paser:Plarge_map_player_item(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "player_id", parentTable)
  self:Pstring(m, "ship_frame", parentTable)
end
function Paser:Plarge_map_route_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "type", parentTable)
  self:Pinteger(m, "target_id", parentTable)
end
function Paser:Plarge_map_route_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "type", parentTable)
  self:Pshort(m, "trip_type", parentTable)
  self:Pstring(m, "player_id", parentTable)
  self:PArray(m, "move_points", parentTable, "integer")
  self:Pinteger(m, "time", parentTable)
  self:Pinteger(m, "cur_time", parentTable)
  self:Pshort(m, "route_type", parentTable)
  self:Pinteger(m, "start_show_time", parentTable)
  self:Pinteger(m, "end_show_time", parentTable)
end
function Paser:Plarge_map_route_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "route_list", parentTable, "large_map_route_info")
end
function Paser:Plarge_map_event_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "event_id", parentTable)
  self:Pinteger(m, "event_type", parentTable)
  self:Pinteger(m, "target_id", parentTable)
end
function Paser:Plarge_map_event_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "event_list", parentTable, "large_map_event_info")
end
function Paser:Plarge_map_battle_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "event_id", parentTable)
end
function Paser:Plarge_map_join_alliance_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "id", parentTable)
end
function Paser:Plarge_map_join_alliance_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "apply_status", parentTable)
  self:Plarge_map_alliance_info(m, "alliance_info", parentTable)
end
function Paser:Plarge_map_exit_alliance_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "id", parentTable)
end
function Paser:Plarge_map_alliance_list(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pgame_item(m, "create_consume", parentTable)
  self:PArray(m, "alliance_list", parentTable, "large_map_alliance_info")
  self:PArray(m, "apply_alliances", parentTable, "string")
end
function Paser:Plarge_map_create_alliance_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "name", parentTable)
end
function Paser:Plarge_map_alliance_affais_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Plarge_map_alliance_info(m, "alliance_info", parentTable)
  self:Pstring(m, "alliance_notice", parentTable)
  self:PArray(m, "alliance_res", parentTable, "game_item")
  self:Pinteger(m, "grant_time", parentTable)
  self:Pinteger(m, "dissolve_time", parentTable)
end
function Paser:Plarge_map_alliance_member_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "id", parentTable)
  self:Pstring(m, "name", parentTable)
  self:Pshort(m, "level", parentTable)
  self:Pshort(m, "rank", parentTable)
  self:Pshort(m, "military_rank", parentTable)
  self:Pinteger(m, "contribution", parentTable)
  self:Pinteger(m, "last_login", parentTable)
  self:Pdouble(m, "force", parentTable)
  self:Pshort(m, "sex", parentTable)
  self:Pinteger(m, "main_fleet_id", parentTable)
  self:PArray(m, "fleet_ids", parentTable, "alliance_fleet_info")
  self:Pboolean(m, "is_online", parentTable)
end
function Paser:Plarge_map_alliance_army_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Plarge_map_alliance_info(m, "alliance_info", parentTable)
  self:PArray(m, "member_list", parentTable, "large_map_alliance_member_info")
end
function Paser:Plarge_map_alliance_player_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "id", parentTable)
  self:Pstring(m, "name", parentTable)
  self:Pinteger(m, "level", parentTable)
  self:Pdouble(m, "force", parentTable)
end
function Paser:Plarge_map_alliance_invite_data_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Plarge_map_alliance_info(m, "alliance_info", parentTable)
  self:PArray(m, "player_list", parentTable, "large_map_alliance_player_info")
end
function Paser:Plarge_map_alliance_player_search_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "search_name", parentTable)
end
function Paser:Plarge_map_alliance_player_search_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "search_name", parentTable)
  self:PArray(m, "player_list", parentTable, "large_map_alliance_player_info")
end
function Paser:Plarge_map_alliance_invite_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "player_id", parentTable)
end
function Paser:Plarge_map_alliance_invite_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "player_id", parentTable)
  self:Pboolean(m, "is_success", parentTable)
end
function Paser:Plarge_map_alliance_recruit_option_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "alliance_id", parentTable)
end
function Paser:Plarge_map_alliance_recruit_option_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pboolean(m, "enable_ratify", parentTable)
  self:Pinteger(m, "need_level", parentTable)
  self:Pfloat(m, "need_force", parentTable)
  self:Pinteger(m, "need_exploit", parentTable)
end
function Paser:Plarge_map_alliance_recruit_option_update_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "alliance_id", parentTable)
  self:Pboolean(m, "enable_ratify", parentTable)
  self:Pinteger(m, "need_level", parentTable)
  self:Pfloat(m, "need_force", parentTable)
  self:Pinteger(m, "need_exploit", parentTable)
end
function Paser:Plarge_map_alliance_ratify_join_data_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "alliance_id", parentTable)
end
function Paser:Plarge_map_alliance_ratify_apply_item(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "id", parentTable)
  self:Pstring(m, "player_id", parentTable)
  self:Pstring(m, "name", parentTable)
  self:Pinteger(m, "level", parentTable)
  self:Pdouble(m, "force", parentTable)
  self:Pshort(m, "rank", parentTable)
  self:Pshort(m, "apply_rank", parentTable)
  self:Pinteger(m, "last_login", parentTable)
end
function Paser:Plarge_map_alliance_ratify_apply_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "apply_list", parentTable, "large_map_alliance_ratify_apply_item")
end
function Paser:Plarge_map_alliance_ratify_duty_data_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "alliance_id", parentTable)
end
function Paser:Plarge_map_alliance_ratify_handle_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "id", parentTable, "string")
  self:Pshort(m, "apply_type", parentTable)
  self:Pboolean(m, "status", parentTable)
end
function Paser:Plarge_map_alliance_ratify_handle_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "apply_type", parentTable)
  self:Plarge_map_alliance_ratify_apply_info(m, "apply_info", parentTable)
end
function Paser:Plarge_map_alliance_apply_duty_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "player_id", parentTable)
  self:Pshort(m, "apply_rank", parentTable)
end
function Paser:Plarge_map_alliance_demotion_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "player_id", parentTable)
end
function Paser:Plarge_map_alliance_exchange_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "player_id", parentTable)
end
function Paser:Plarge_map_alliance_exchange_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Plarge_map_alliance_member_info(m, "my_alliance_info", parentTable)
  self:Plarge_map_alliance_member_info(m, "other_alliance_info", parentTable)
end
function Paser:Plarge_map_alliance_expel_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "player_id", parentTable)
end
function Paser:Plarge_map_alliance_expel_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pboolean(m, "is_success", parentTable)
  self:Pstring(m, "player_id", parentTable)
end
function Paser:Plarge_map_block_detail_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
end
function Paser:Plarge_map_warning_army(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "alliance_id", parentTable)
  self:Pstring(m, "alliance_name", parentTable)
  self:Pinteger(m, "player_count", parentTable)
end
function Paser:Plarge_map_block_detail(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pinteger(m, "owner_code", parentTable)
  self:Pstring(m, "block_name_key", parentTable)
  self:PArray(m, "res_output", parentTable, "battle_good_item")
  self:Plarge_map_alliance_info(m, "alliance_info", parentTable)
  self:Pinteger(m, "consume_energy", parentTable)
  self:PArray(m, "get_res", parentTable, "game_item")
  self:Pboolean(m, "is_border", parentTable)
  self:Pinteger(m, "self_player_count", parentTable)
  self:Pinteger(m, "other_player_count", parentTable)
  self:PArray(m, "warning_items", parentTable, "large_map_warning_army")
  self:Pboolean(m, "enable_watch", parentTable)
end
function Paser:Plarge_map_create_mass_troop_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pfloat(m, "min_force", parentTable)
  self:Pinteger(m, "mass_count", parentTable)
end
function Paser:Pmass_troops_item(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "id", parentTable)
  self:Pstring(m, "leader_name", parentTable)
  self:Pinteger(m, "time", parentTable)
  self:Pinteger(m, "mass_id", parentTable)
  self:Pstring(m, "mass_name_key", parentTable)
  self:Pinteger(m, "target_id", parentTable)
  self:Pstring(m, "target_name_key", parentTable)
  self:Pstring(m, "alliance_name", parentTable)
  self:Pinteger(m, "arrived_fleet", parentTable)
  self:Pinteger(m, "mass_total_fleet", parentTable)
  self:Pshort(m, "status", parentTable)
end
function Paser:Plarge_map_mass_troops_list(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "mass_troops", parentTable, "mass_troops_item")
end
function Paser:Plarge_map_mass_troops_member(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "player_id", parentTable)
  self:Pstring(m, "avatar", parentTable)
  self:Pstring(m, "name", parentTable)
end
function Paser:Plarge_map_troops_detail(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "time", parentTable)
  self:Pstring(m, "leader_id", parentTable)
  self:Pstring(m, "troop_id", parentTable)
  self:Pshort(m, "status", parentTable)
  self:Pshort(m, "max_count", parentTable)
  self:PArray(m, "members", parentTable, "large_map_mass_troops_member")
end
function Paser:Plarge_map_join_mass_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "id", parentTable)
end
function Paser:Pmulmatrix_get_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "type", parentTable)
end
function Paser:Plarge_map_mass_near_invite_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "search_name", parentTable)
end
function Paser:Pmass_invite_item(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pstring(m, "player_id", parentTable)
  self:Pstring(m, "name", parentTable)
  self:Pinteger(m, "level", parentTable)
  self:Pdouble(m, "force", parentTable)
  self:Pshort(m, "status", parentTable)
  self:Pinteger(m, "status_time", parentTable)
end
function Paser:Plarge_map_mass_invite_data(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "onekey_time", parentTable)
  self:PArray(m, "invite_list", parentTable, "mass_invite_item")
end
function Paser:Plarge_map_mass_invite_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "search_name", parentTable)
  self:PArray(m, "player_id", parentTable, "string")
end
function Paser:Plarge_map_mass_invite_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pstring(m, "troop_id", parentTable)
  self:Pstring(m, "block_name_key", parentTable)
  self:Plarge_map_alliance_info(m, "alliance_info", parentTable)
  self:Pstring(m, "leader_name", parentTable)
  self:Pstring(m, "leader_avatar", parentTable)
  self:Pdouble(m, "leader_force", parentTable)
  self:Pinteger(m, "leader_level", parentTable)
end
function Paser:Plarge_map_mass_invite_do_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "troops_id", parentTable)
  self:Pboolean(m, "is_accept", parentTable)
  self:Pboolean(m, "pause_notice", parentTable)
end
function Paser:Plarge_map_exit_mass_troop_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "troop_id", parentTable)
end
function Paser:Plarge_map_view_mass_player_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "player_id", parentTable)
end
function Paser:Plarge_map_simple_player_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "player_id", parentTable)
  self:Pstring(m, "name", parentTable)
  self:Pstring(m, "avatar", parentTable)
  self:Pinteger(m, "level", parentTable)
  self:Pdouble(m, "force", parentTable)
  self:Pshort(m, "rank", parentTable)
  self:Pfloat(m, "exploit", parentTable)
end
function Paser:Plarge_map_remove_mass_player_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "player_id", parentTable)
end
function Paser:Plarge_map_repair_avatar_item(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "identity", parentTable)
  self:Pstring(m, "ship", parentTable)
  self:Pstring(m, "avatar", parentTable)
  self:Pinteger(m, "level", parentTable)
  self:Pinteger(m, "color", parentTable)
  self:Pstring(m, "name", parentTable)
  self:Pdouble(m, "force", parentTable)
  self:Pshort(m, "sex", parentTable)
  self:Pinteger(m, "vessels", parentTable)
  self:Pinteger(m, "rank", parentTable)
  self:Pdouble(m, "cur_hp", parentTable)
  self:Pdouble(m, "total_hp", parentTable)
  self:Pgame_item(m, "need_res", parentTable)
end
function Paser:Plarge_map_repair_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "avatar_list", parentTable, "large_map_repair_avatar_item")
end
function Paser:Plarge_map_repair_ship_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "identity", parentTable)
end
function Paser:Plarge_map_add_mass_player_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "troop_id", parentTable)
  self:Plarge_map_mass_troops_member(m, "new_member", parentTable)
end
function Paser:Plarge_map_buy_energy_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "type", parentTable)
end
function Paser:Plarge_map_local_leader_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "troop_id", parentTable)
end
function Paser:Plarge_map_local_leader_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "leader_pos", parentTable)
end
function Paser:Plarge_map_alliance_diplomacy_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "bat_to_me", parentTable, "large_map_alliance_info")
  self:PArray(m, "bat_to_other", parentTable, "large_map_alliance_info")
  self:PArray(m, "all_army", parentTable, "large_map_alliance_info")
end
function Paser:Plarge_map_alliance_diplomacy_exchange_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "alliance_id", parentTable)
  self:Pstring(m, "cur_type", parentTable)
  self:Pstring(m, "dst_type", parentTable)
end
function Paser:Plarge_map_alliance_modify_notice_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "alliance_id", parentTable)
  self:Pstring(m, "notice", parentTable)
end
function Paser:Plarge_map_alliance_adjust_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "alliance_id", parentTable)
  self:Pfloat(m, "rate", parentTable)
  self:Pfloat(m, "expend", parentTable)
end
function Paser:Plarge_map_log_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "log_type", parentTable)
end
function Paser:Plarge_map_log_item(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "id", parentTable)
  self:Pstring(m, "report_id", parentTable)
  self:Pstring(m, "log_str", parentTable)
  self:Pinteger(m, "time", parentTable)
end
function Paser:Plarge_map_log_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "log_type", parentTable)
  self:PArray(m, "log_list", parentTable, "large_map_log_item")
end
function Paser:Puser_statistics_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "device_name", parentTable)
  self:Pstring(m, "device_real_name", parentTable)
  self:Pstring(m, "open_udid", parentTable)
  self:Pstring(m, "report_id", parentTable)
  self:Pstring(m, "battle_system", parentTable)
  self:Pinteger(m, "played_round_count", parentTable)
  self:Pshort(m, "state", parentTable)
  self:Pshort(m, "save_report", parentTable)
  self:PArray(m, "ext_info", parentTable, "pair_string")
end
function Paser:Pfps_switch_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "status", parentTable)
end
function Paser:Plarge_map_log_battle_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "report_id", parentTable)
end
function Paser:Plarge_map_search_alliance_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "search_name", parentTable)
end
function Paser:Plarge_map_dissolve_alliance_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "id", parentTable)
end
function Paser:Plarge_map_rank_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "rank_type", parentTable)
end
function Paser:Plarge_map_rank_item(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "id", parentTable)
  self:Pinteger(m, "rank", parentTable)
  self:Pinteger(m, "rank_value", parentTable)
  self:Pstring(m, "player_name", parentTable)
  self:Pstring(m, "alliance_name", parentTable)
  self:PArray(m, "rewards", parentTable, "game_item")
end
function Paser:Plarge_map_rank_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pshort(m, "rank_type", parentTable)
  self:Pinteger(m, "reward_time", parentTable)
  self:Plarge_map_rank_item(m, "my_rank_info", parentTable)
  self:PArray(m, "rank_list", parentTable, "large_map_rank_item")
end
function Paser:Plarge_map_alliance_player_exit_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "players", parentTable, "string")
end
function Paser:Plarge_map_alliance_promotion_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "player_id", parentTable)
end
function Paser:Pline_item(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "id", parentTable)
  self:Pinteger(m, "start_id", parentTable)
  self:Pinteger(m, "end_id", parentTable)
  self:Pshort(m, "line_type", parentTable)
  self:Pshort(m, "operate_type", parentTable)
end
function Paser:Plarge_map_mass_attack_line_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "lines", parentTable, "line_item")
end
function Paser:Plarge_map_battle_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pboolean(m, "is_win", parentTable)
  self:Pstring(m, "win_name", parentTable)
end
function Paser:Plarge_map_vision_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "visible_ids", parentTable, "integer")
end
function Paser:Plarge_map_vision_change_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "add_ids", parentTable, "integer")
  self:PArray(m, "sub_ids", parentTable, "integer")
end
function Paser:Psmall_map_item(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pinteger(m, "row", parentTable)
  self:Pinteger(m, "col", parentTable)
  self:Pshort(m, "color", parentTable)
end
function Paser:Plarge_map_small_map_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "my_pos", parentTable)
  self:Pinteger(m, "refresh_min", parentTable)
  self:Pinteger(m, "total_row", parentTable)
  self:Pinteger(m, "total_col", parentTable)
  self:PArray(m, "blocks", parentTable, "small_map_item")
  self:PArray(m, "fleets", parentTable, "integer")
end
function Paser:Plarge_map_ship_arrive_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "player_id", parentTable)
  self:Pinteger(m, "arrive_id", parentTable)
  self:Pinteger(m, "rotation", parentTable)
end
function Paser:Ptlc_fight_status_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "protect_time", parentTable)
end
function Paser:Ptlc_status_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "status", parentTable)
end
function Paser:Ptlc_player_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "point", parentTable)
  self:Pinteger(m, "rank_point", parentTable)
  self:Pinteger(m, "next_rank_point", parentTable)
  self:Pinteger(m, "ranking", parentTable)
  self:Pinteger(m, "using_ranking", parentTable)
  self:Pinteger(m, "using_rank", parentTable)
  self:Pinteger(m, "rank", parentTable)
  self:Pstring(m, "rank_img", parentTable)
  self:Pinteger(m, "rank_name", parentTable)
  self:Pinteger(m, "protect_time", parentTable)
  self:Pinteger(m, "final_time", parentTable)
  self:Pinteger(m, "end_time", parentTable)
  self:Pinteger(m, "res_per_hour", parentTable)
  self:Pdouble(m, "force", parentTable)
end
function Paser:Ptlc_rank_board_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Ptlc_player_brief(m, "self", parentTable)
  self:Pinteger(m, "world_ranking", parentTable)
  self:Pinteger(m, "rank_level", parentTable)
  self:Pstring(m, "rank_img", parentTable)
  self:Pinteger(m, "rank_name", parentTable)
  self:Pinteger(m, "rank_ceil", parentTable)
  self:PArray(m, "top_list", parentTable, "tlc_player_brief")
  self:PArray(m, "class_list", parentTable, "tlc_player_brief")
end
function Paser:Ptlc_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pgame_item(m, "currency", parentTable)
  self:Pboolean(m, "can_buy", parentTable)
  self:Pinteger(m, "search_cost", parentTable)
  self:Pinteger(m, "max_supply", parentTable)
  self:Pboolean(m, "pop_report", parentTable)
  self:Pinteger(m, "king_num", parentTable)
  self:Ptlc_player_info(m, "player_info", parentTable)
  self:PArray(m, "buffers", parentTable, "pair")
end
function Paser:Ptlc_info_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Ptlc_info(m, "info", parentTable)
end
function Paser:Ptlc_awards_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "awards", parentTable, "tlc_award")
end
function Paser:Ptlc_awards_get_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
end
function Paser:Ptlc_rankup_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "rank_name", parentTable)
  self:Pstring(m, "rank_img", parentTable)
  self:Pinteger(m, "score", parentTable)
  self:Pinteger(m, "next_rank_name", parentTable)
  self:Pinteger(m, "next_rank_score", parentTable)
end
function Paser:Ptlc_report_detail_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "id", parentTable)
end
function Paser:Ptlc_report_detail_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "headers", parentTable, "group_fight_header")
end
function Paser:Ptlc_fight_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "server_id", parentTable)
  self:Pstring(m, "user_id", parentTable)
  self:Pinteger(m, "revenge_id", parentTable)
end
function Paser:Ptlc_fight_report(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "report_id", parentTable)
  self:Ptlc_player_brief(m, "enemy", parentTable)
  self:Pboolean(m, "is_win", parentTable)
  self:Pboolean(m, "can_revenge", parentTable)
  self:Pinteger(m, "score_before", parentTable)
  self:Pinteger(m, "score_after", parentTable)
  self:Pinteger(m, "fight_time", parentTable)
end
function Paser:Ptlc_fight_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "code", parentTable)
  self:Ptlc_fight_report(m, "result", parentTable)
  self:Pdouble(m, "damage", parentTable)
  self:Pdouble(m, "suffer_damage", parentTable)
  self:PArray(m, "headers", parentTable, "group_fight_header")
  self:PArray(m, "awards", parentTable, "game_item")
end
function Paser:Ptlc_challenge_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Ptlc_player_brief(m, "defender", parentTable)
  self:Pinteger(m, "win_score", parentTable)
  self:Pinteger(m, "lose_score", parentTable)
end
function Paser:Ptlc_match_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "code", parentTable)
  self:PArray(m, "challenge_list", parentTable, "tlc_challenge_info")
end
function Paser:Ptlc_enter_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Ptlc_info(m, "info", parentTable)
end
function Paser:Pchampion_cdtime_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pchampion_status_info(m, "local_champion_cd", parentTable)
  self:Pchampion_status_info(m, "world_champion_cd", parentTable)
  self:Pchampion_status_info(m, "tlc_champion_cd", parentTable)
end
function Paser:Pteamleague_matrix_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "type", parentTable)
end
function Paser:Pteamleague_matrix_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "type", parentTable)
  self:Pinteger(m, "leader_fleet", parentTable)
  self:PArray(m, "matrixs", parentTable, "matrix")
end
function Paser:Pteamleague_matrix_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "teamleague_matrix", parentTable, "teamleague_matrix_info")
end
function Paser:Pteamleague_matrix_save_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "teamleague_matrix", parentTable, "teamleague_matrix_info")
end
function Paser:Ptlc_bag_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "owner", parentTable)
  self:Pinteger(m, "matrix", parentTable)
  self:Pinteger(m, "bag_type", parentTable)
  self:Pinteger(m, "pos", parentTable)
end
function Paser:Ptlc_bag_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "owner", parentTable)
  self:Pinteger(m, "matrix", parentTable)
  self:Pinteger(m, "bag_type", parentTable)
  self:PArray(m, "grids", parentTable, "bag_grid")
  self:PArray(m, "equipedItems", parentTable, "bag_new_grid")
end
function Paser:Ptlc_krypton_store_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "formation_id", parentTable)
end
function Paser:Ptlc_krypton_store_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "grid_capacity", parentTable)
  self:PArray(m, "kryptons", parentTable, "krypton_info")
  self:Pinteger(m, "klevel", parentTable)
  self:Pinteger(m, "gacha_need_money", parentTable)
  self:Pinteger(m, "gacha_need_credit", parentTable)
  self:Pinteger(m, "use_credit_count", parentTable)
  self:Pboolean(m, "is_high", parentTable)
end
function Paser:Ptlc_fleets_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "fleets", parentTable, "fleet")
end
function Paser:Ptlc_mulmatrix_switch_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pboolean(m, "switch", parentTable)
end
function Paser:Ptlc_mulmatrix_switch_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "code", parentTable)
end
function Paser:Ptlc_matrix_change_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "teamleague_matrix", parentTable, "teamleague_matrix_info")
end
function Paser:Plevel_list_item(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pinteger(m, "target", parentTable)
  self:Pinteger(m, "status", parentTable)
  self:PArray(m, "awards", parentTable, "game_item")
end
function Paser:Pcumulate_recharge_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "recharged_count", parentTable)
  self:Pinteger(m, "left_time", parentTable)
  self:Pinteger(m, "next_level_done", parentTable)
  self:Pinteger(m, "next_level_target", parentTable)
  self:PArray(m, "level_list", parentTable, "level_list_item")
end
function Paser:Pcumulate_recharge_award_get_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
end
function Paser:Plogin_fund_reward_item(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "days", parentTable)
  self:Pinteger(m, "status", parentTable)
  self:PArray(m, "awards", parentTable, "game_item")
end
function Paser:Plogin_fund_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "bln_buy", parentTable)
  self:Pinteger(m, "acc_login_days", parentTable)
  self:PArray(m, "reward_list", parentTable, "login_fund_reward_item")
end
function Paser:Plogin_fund_reward_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "days", parentTable)
end
function Paser:Pcheck_exp_add_item_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "item", parentTable)
end
function Paser:Pcheck_exp_add_item_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "result", parentTable)
  self:Pinteger(m, "add_percent", parentTable)
  self:Pinteger(m, "lefttime", parentTable)
end
function Paser:Pexp_add_item_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "bln_effect", parentTable)
  self:Pinteger(m, "add_percent", parentTable)
  self:Pinteger(m, "lefttime", parentTable)
end
function Paser:Ppop_firstpay_on_lvup_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pstring(m, "bg_pic", parentTable)
  self:Pstring(m, "title", parentTable)
  self:Pstring(m, "desc", parentTable)
end
function Paser:Ptlc_awards_get_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "code", parentTable)
  self:Pinteger(m, "id", parentTable)
end
function Paser:Pgcs_task_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "id", parentTable)
  self:Pinteger(m, "type", parentTable)
  self:Pstring(m, "desc", parentTable)
  self:Pinteger(m, "recommend_lv", parentTable)
  self:Pinteger(m, "cur_done", parentTable)
  self:Pinteger(m, "goal", parentTable)
  self:Pinteger(m, "status", parentTable)
  self:PArray(m, "awards", parentTable, "game_item")
end
function Paser:Pgcs_today_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "change_left", parentTable)
  self:PArray(m, "tasks", parentTable, "gcs_task_info")
end
function Paser:Pgcs_week_info(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "week_id", parentTable)
  self:Pinteger(m, "unlock_after", parentTable)
  self:Pinteger(m, "cur_done", parentTable)
  self:Pinteger(m, "total", parentTable)
  self:Pinteger(m, "extra_goal", parentTable)
  self:Pgame_item(m, "extra_award", parentTable)
  self:Pinteger(m, "extra_status", parentTable)
  self:PArray(m, "free_tasks", parentTable, "gcs_task_info")
  self:PArray(m, "paid_tasks", parentTable, "gcs_task_info")
end
function Paser:Pgcs_task_data_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pboolean(m, "paid_card", parentTable)
  self:Pgcs_today_info(m, "today", parentTable)
  self:PArray(m, "weeks", parentTable, "gcs_week_info")
end
function Paser:Pgcs_lv_awards(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "lv", parentTable)
  self:PArray(m, "free_awards", parentTable, "game_item")
  self:Pboolean(m, "free_received", parentTable)
  self:PArray(m, "paid_awards", parentTable, "game_item")
  self:Pboolean(m, "paid_received", parentTable)
end
function Paser:Pgcs_awards_data_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pboolean(m, "paid_card", parentTable)
  self:Pinteger(m, "cur_season", parentTable)
  self:Pinteger(m, "season_left_time", parentTable)
  self:Pinteger(m, "cur_lv", parentTable)
  self:Pinteger(m, "cur_points", parentTable)
  self:Pinteger(m, "lv_up_need", parentTable)
  self:PArray(m, "lv_awards_list", parentTable, "gcs_lv_awards")
end
function Paser:Pgcs_today_task_change_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "task_id", parentTable)
end
function Paser:Pgcs_today_task_change_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pgcs_today_info(m, "today", parentTable)
end
function Paser:Pgcs_get_today_task_reward_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "task_id", parentTable)
end
function Paser:Pgcs_get_today_task_reward_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pgcs_today_info(m, "today", parentTable)
end
function Paser:Pgcs_get_week_task_reward_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "week_id", parentTable)
  self:Pinteger(m, "task_id", parentTable)
end
function Paser:Pgcs_get_week_task_reward_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pgcs_week_info(m, "week_info", parentTable)
end
function Paser:Pgcs_get_week_extra_reward_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "week_id", parentTable)
end
function Paser:Pgcs_get_week_extra_reward_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pgcs_week_info(m, "week_info", parentTable)
end
function Paser:Pgcs_get_lv_all_rewards_ack(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:PArray(m, "lv_awards_list", parentTable, "gcs_lv_awards")
end
function Paser:Pgcs_point_update_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pboolean(m, "paid_card", parentTable)
  self:Pinteger(m, "cur_season", parentTable)
  self:Pinteger(m, "season_left_time", parentTable)
  self:Pinteger(m, "cur_lv", parentTable)
  self:Pinteger(m, "cur_points", parentTable)
  self:Pinteger(m, "lv_up_need", parentTable)
  self:PArray(m, "lv_awards_list", parentTable, "gcs_lv_awards")
end
function Paser:Pgcs_buy_update_ntf(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pboolean(m, "paid_card", parentTable)
  self:Pgcs_today_info(m, "today", parentTable)
  self:PArray(m, "weeks", parentTable, "gcs_week_info")
end
function Paser:Ptlc_krypton_sort_req(m, key, parentTable)
  if key ~= nil then
    parentTable[key] = {}
    parentTable = parentTable[key]
  end
  self:Pinteger(m, "formation_id", parentTable)
  self:Pshort(m, "type", parentTable)
end
function Paser:Fpair(m, c)
  self:Finteger(m, c.key)
  self:Finteger(m, c.value)
end
function Paser:Fudid_step(m, c)
  self:Fstring(m, c.market)
  self:Fstring(m, c.terminal)
  self:Fstring(m, c.udid)
  self:Finteger(m, c.step)
end
function Paser:Fudid_track(m, c)
  self:Fstring(m, c.market)
  self:Fstring(m, c.terminal)
  self:Fstring(m, c.udid)
  self:Fstring(m, c.track)
end
function Paser:Fprotocal_version_req(m, c)
  self:Finteger(m, c.version)
end
function Paser:Fkeylist(m, c)
  self:FArray(m, c.keys, "integer")
end
function Paser:Fcommon_ack(m, c)
  self:Finteger(m, c.api)
  self:Finteger(m, c.code)
  self:Fstring(m, c.msg)
end
function Paser:Fuser_passport_info(m, c)
  self:Fstring(m, c.passport)
  self:Fstring(m, c.password)
  self:Fstring(m, c.udid)
  self:Fstring(m, c.mac_addr)
  self:Fstring(m, c.idfa)
  self:Fstring(m, c.open_udid)
  self:Fstring(m, c.locale)
  self:Finteger(m, c.game_id)
  self:Fstring(m, c.region_name)
  self:Finteger(m, c.acc_type)
end
function Paser:Fuser_client_info(m, c)
  self:Fstring(m, c.market)
  self:Fstring(m, c.terminal)
  self:Fstring(m, c.app_id)
  self:Fstring(m, c.os_version)
  self:Fstring(m, c.device_name)
  self:Fstring(m, c.client_version_svn)
  self:Fstring(m, c.client_version_main)
  self:Fstring(m, c.google_aid)
  self:Fstring(m, c.device_id)
  self:Finteger(m, c.device_id_type)
end
function Paser:Fuser_register_info(m, c)
  self:Fstring(m, c.name)
  self:Fuser_client_info(m, c.client_info)
  self:Fuser_passport_info(m, c.passport_info)
  self:Finteger(m, c.sex)
  self:Fstring(m, c.invitecode)
end
function Paser:Fuser_login_info(m, c)
  self:Fstring(m, c.name)
  self:Fuser_client_info(m, c.client_info)
  self:Fuser_passport_info(m, c.passport_info)
end
function Paser:Fpassport_bind_req(m, c)
  self:Fuser_passport_info(m, c.passport_info)
  self:Fboolean(m, c.is_new)
  self:Fstring(m, c.name)
end
function Paser:Fpassport_bind_ack(m, c)
  self:Fboolean(m, c.can_iap)
  self:Fboolean(m, c.is_unlock)
end
function Paser:Fbuilding(m, c)
  self:Fstring(m, c.name)
  self:Finteger(m, c.level)
  self:Finteger(m, c.upgrade_time)
  self:Finteger(m, c.cost)
  self:Finteger(m, c.status)
  self:Finteger(m, c.max_level)
  self:Finteger(m, c.require_user_level)
end
function Paser:Fskill_desc_item(m, c)
  self:Finteger(m, c.desc_key)
  self:Fboolean(m, c.is_finish)
  self:Fshort(m, c.num)
end
function Paser:Ffleet(m, c)
  self:Fstring(m, c.id)
  self:Finteger(m, c.identity)
  self:Fshort(m, c.level)
  self:Finteger(m, c.vessels)
  self:Finteger(m, c.max_durability)
  self:Finteger(m, c.shield)
  self:Finteger(m, c.durability)
  self:Finteger(m, c.ph_attack)
  self:Finteger(m, c.en_attack)
  self:Finteger(m, c.en_armor)
  self:Finteger(m, c.ph_armor)
  self:Finteger(m, c.sp_attack)
  self:Finteger(m, c.sp_armor)
  self:Fshort(m, c.max_accumulator)
  self:Fshort(m, c.accumulator)
  self:Finteger(m, c.crit_damage)
  self:Finteger(m, c.hit_rate)
  self:Finteger(m, c.status)
  self:Finteger(m, c.repair_money)
  self:Finteger(m, c.krypton)
  self:FArray(m, c.spells, "integer")
  self:Finteger(m, c.active_spell)
  self:Finteger(m, c.damage)
  self:Fdouble(m, c.force)
  self:Finteger(m, c.crit_lv)
  self:Finteger(m, c.anti_crit)
  self:Finteger(m, c.motility)
  self:Finteger(m, c.penetration)
  self:Finteger(m, c.intercept)
  self:Finteger(m, c.fce)
  self:FArray(m, c.gift, "gift_info")
  self:FArray(m, c.adjutant_vessels, "integer")
  self:Finteger(m, c.cur_adjutant)
  self:Finteger(m, c.adjutant_level)
  self:FArray(m, c.can_adjutant, "integer")
  self:FArray(m, c.adjutant_spell, "adjutant_spell")
  self:Finteger(m, c.damage_deepens)
  self:Finteger(m, c.damage_reduction)
  self:Finteger(m, c.skill_damage_deepens)
  self:Finteger(m, c.skill_damage_reduction)
  self:Finteger(m, c.char_damage)
  self:Finteger(m, c.accumulater_atk)
  self:Finteger(m, c.accumulater_def)
  self:Finteger(m, c.accumulater_crit)
  self:Finteger(m, c.accumulater_intercept)
  self:Finteger(m, c.accumulater_counter)
  self:Finteger(m, c.anti_crit_damage)
  self:Finteger(m, c.buff_hit_rate)
  self:Finteger(m, c.anti_buff_hit_rate)
  self:FArray(m, c.skill_upgrade_desc, "skill_desc_item")
end
function Paser:Fadjutant_spell(m, c)
  self:Finteger(m, c.spell_id)
  self:Finteger(m, c.level)
  self:Finteger(m, c.state)
end
function Paser:Fgift_info(m, c)
  self:Finteger(m, c.gift)
end
function Paser:Flevel_info(m, c)
  self:Finteger(m, c.level)
  self:Finteger(m, c.experience)
  self:Finteger(m, c.uplevel_experience)
end
function Paser:Fuser_info(m, c)
  self:Fstring(m, c.player_id)
  self:Fstring(m, c.name)
  self:Fstring(m, c.udid)
  self:Fstring(m, c.passport)
  self:Fboolean(m, c.can_iap)
  self:Fboolean(m, c.is_unlock)
  self:Finteger(m, c.sex)
  self:Finteger(m, c.icon)
  self:Fstring(m, c.server)
end
function Paser:Fuser_resource(m, c)
  self:Fdouble(m, c.money)
  self:Finteger(m, c.technique)
  self:Finteger(m, c.credit)
  self:Finteger(m, c.prestige)
  self:Finteger(m, c.quark)
  self:Finteger(m, c.lepton)
  self:Finteger(m, c.kenergy)
  self:Finteger(m, c.activity)
  self:Finteger(m, c.brick)
  self:Finteger(m, c.wd_supply)
  self:Finteger(m, c.total_amount)
  self:Finteger(m, c.ladder_search)
  self:Finteger(m, c.wormhole_search)
  self:Finteger(m, c.crusade_repair)
  self:Finteger(m, c.crystal)
  self:Finteger(m, c.silver)
  self:Finteger(m, c.medal_resource)
  self:Finteger(m, c.wdc_supply)
  self:Finteger(m, c.art_point)
  self:Finteger(m, c.expedition)
  self:Finteger(m, c.tlc_supply)
  self:Finteger(m, c.tlc_money)
end
function Paser:Fbattle_status(m, c)
  self:Finteger(m, c.status)
  self:Finteger(m, c.battle_id)
  self:Finteger(m, c.battle_type)
  self:FArray(m, c.fleet_list, "integer")
  self:Finteger(m, c.event_type)
  self:Finteger(m, c.user_level)
end
function Paser:Fuser_progress(m, c)
  self:Finteger(m, c.act)
  self:Finteger(m, c.chapter)
  self:Finteger(m, c.all_count)
  self:Finteger(m, c.finish_count)
  self:Finteger(m, c.adv_chapter)
end
function Paser:Fuser_cdtime(m, c)
  self:Fstring(m, c.id)
  self:Finteger(m, c.left_time)
  self:Finteger(m, c.content)
  self:Finteger(m, c.status)
  self:Finteger(m, c.cd_type)
  self:Finteger(m, c.can_do)
end
function Paser:Fuser_cdtime_info(m, c)
  self:FArray(m, c.cdtimes, "user_cdtime")
end
function Paser:Ffleets_info(m, c)
  self:FArray(m, c.fleets, "fleet")
end
function Paser:Ffriend_info(m, c)
  self:Fstring(m, c.id)
  self:Fstring(m, c.name)
  self:Finteger(m, c.level)
  self:Finteger(m, c.online)
  self:Finteger(m, c.sex)
  self:Finteger(m, c.icon)
  self:Finteger(m, c.main_fleet_level)
end
function Paser:Ffriend_detail(m, c)
  self:Fstring(m, c.id)
  self:Fstring(m, c.name)
  self:Finteger(m, c.level)
  self:Finteger(m, c.pvp_rank)
  self:Fstring(m, c.alliance)
  self:Fdouble(m, c.force)
  self:Finteger(m, c.online)
  self:Finteger(m, c.sex)
  self:Finteger(m, c.icon)
end
function Paser:Fuser_offline_info(m, c)
  self:Finteger(m, c.minute)
  self:Finteger(m, c.max_minute)
  self:Finteger(m, c.expr_per_minute)
end
function Paser:Falliance_pos_and_id(m, c)
  self:Fboolean(m, c.weekend_award)
  self:Fstring(m, c.alliance_name)
  self:Fshort(m, c.position)
  self:Fstring(m, c.id)
  self:Fshort(m, c.flag)
end
function Paser:Fbattle_matrix_req(m, c)
  self:Finteger(m, c.battle_id)
end
function Paser:Ffleet_level(m, c)
  self:Finteger(m, c.cell_id)
  self:Finteger(m, c.level)
end
function Paser:Fmatrix_cell(m, c)
  self:Finteger(m, c.cell_id)
  self:Finteger(m, c.fleet_identity)
  self:Finteger(m, c.fleet_spell)
end
function Paser:Fmonster_matrix_cell(m, c)
  self:Finteger(m, c.cell_id)
  self:Finteger(m, c.identity)
  self:Finteger(m, c.spell)
  self:Fdouble(m, c.durability)
  self:Finteger(m, c.shield)
  self:Finteger(m, c.level)
  self:Fdouble(m, c.force)
  self:Fdouble(m, c.vessels)
  self:Fstring(m, c.avatar)
  self:Fstring(m, c.ship)
  self:Finteger(m, c.sex)
end
function Paser:Fmatrix(m, c)
  self:Finteger(m, c.id)
  self:Fstring(m, c.name)
  self:FArray(m, c.cells, "matrix_cell")
  self:Finteger(m, c.count)
end
function Paser:Fmulmatrix_one(m, c)
  self:Fshort(m, c.index)
  self:Finteger(m, c.type)
  self:Fstring(m, c.name)
  self:FArray(m, c.cells, "matrix_cell")
end
function Paser:Fmulmatrix_info(m, c)
  self:Fshort(m, c.blank_num)
  self:Fshort(m, c.max_blank)
  self:FArray(m, c.matrixs, "mulmatrix_one")
  self:FArray(m, c.matrixs_purpose, "pair")
  self:Fshort(m, c.cur_index)
end
function Paser:Fmulmatrix_blank(m, c)
  self:Fshort(m, c.blank)
end
function Paser:Fmulmatrix_condition(m, c)
  self:Fstring(m, c.type)
  self:Finteger(m, c.value)
end
function Paser:Fmulmatrix_price(m, c)
  self:Finteger(m, c.credit_cost)
  self:Finteger(m, c.money_cost)
  self:FArray(m, c.conditions, "mulmatrix_condition")
end
function Paser:Fmonster_matrix(m, c)
  self:FArray(m, c.cells, "monster_matrix_cell")
end
function Paser:Faddition_fleet(m, c)
  self:Finteger(m, c.identity)
  self:Fshort(m, c.pos)
end
function Paser:Fbattle_matrix_ack(m, c)
  self:Fmonster_matrix(m, c.monsters)
  self:Finteger(m, c.status)
  self:Fdouble(m, c.force)
  self:FArray(m, c.addition, "addition_fleet")
end
function Paser:Fuser_buildings(m, c)
  self:FArray(m, c.buildings, "building")
end
function Paser:Ftask_statistic_info(m, c)
  self:Finteger(m, c.finish_count)
end
function Paser:Fmail_unread_info(m, c)
  self:Finteger(m, c.count)
end
function Paser:Fvip_info(m, c)
  self:Finteger(m, c.level)
  self:Finteger(m, c.current_exp)
  self:Finteger(m, c.next_level_exp)
  self:Fboolean(m, c.pay_act)
end
function Paser:Fmodule_status(m, c)
  self:Fstring(m, c.name)
  self:Fboolean(m, c.status)
end
function Paser:Fmodules_status(m, c)
  self:FArray(m, c.modules, "module_status")
end
function Paser:Fquest(m, c)
  self:Finteger(m, c.quest_id)
  self:Finteger(m, c.status)
  self:Fstring(m, c.cond_type)
  self:Finteger(m, c.cond_int)
  self:Finteger(m, c.cond_total)
  self:Finteger(m, c.cond_count)
  self:FArray(m, c.loot, "game_item")
  self:Finteger(m, c.player_level)
  self:Finteger(m, c.head)
  self:Fstring(m, c.prompt)
end
function Paser:Fuser_snapshot(m, c)
  self:Fuser_info(m, c.userinfo)
  self:Fuser_resource(m, c.resource)
  self:Fuser_buildings(m, c.buildings)
  self:Fuser_progress(m, c.progress)
  self:Ffleets_info(m, c.fleetinfo)
  self:FArray(m, c.equipments, "equipment")
  self:Fuser_offline_info(m, c.offline_info)
  self:Falliance_pos_and_id(m, c.alliance)
  self:FArray(m, c.fleetkryptons, "fleet_krypton_info")
  self:Fuser_cdtime_info(m, c.cdtimes)
  self:Flevel_info(m, c.levelinfo)
  self:Fvip_info(m, c.vipinfo)
  self:Fmatrix(m, c.matrix)
  self:Ftask_statistic_info(m, c.task_statistic)
  self:Fmail_unread_info(m, c.unmailinfo)
  self:Fmodules_status(m, c.modules_status)
  self:Fquest(m, c.user_quest)
  self:Finteger(m, c.server_code)
end
function Paser:Fbuilding_upgrade_info(m, c)
  self:Fstring(m, c.name)
end
function Paser:Fbattle_request(m, c)
  self:Finteger(m, c.battle_id)
  self:Finteger(m, c.action)
end
function Paser:Fchat_req(m, c)
  self:Fstring(m, c.content)
  self:Fstring(m, c.receiver)
  self:Fstring(m, c.channel)
  self:Fshort(m, c.show_effect)
end
function Paser:Fchat_ntf(m, c)
  self:Fstring(m, c.sender)
  self:Fshort(m, c.sender_role)
  self:Fstring(m, c.sender_name)
  self:Finteger(m, c.sender_level)
  self:Fstring(m, c.content)
  self:Fstring(m, c.origin_content)
  self:Fstring(m, c.receiver)
  self:Fstring(m, c.channel)
  self:Finteger(m, c.timestamp)
  self:Finteger(m, c.champion_rank)
  self:Finteger(m, c.tc_king)
  self:Fshort(m, c.tag)
  self:Fshort(m, c.wdc_ranking)
  self:Fshort(m, c.wdc_rank)
  self:Fshort(m, c.sex)
  self:Fstring(m, c.wdc_img)
  self:Fshort(m, c.viplevel)
  self:Fshort(m, c.show_effect)
end
function Paser:Fmap_status_req(m, c)
  self:Finteger(m, c.chapter_id)
end
function Paser:Fuser_map_status(m, c)
  self:Finteger(m, c.all_count)
  self:Finteger(m, c.finish_count)
  self:FArray(m, c.status, "battle_status")
  self:Finteger(m, c.refresh_time)
end
function Paser:Fbattle_status_req(m, c)
  self:Finteger(m, c.battle_id)
end
function Paser:Fbattle_user_info(m, c)
  self:Fstring(m, c.username)
  self:Finteger(m, c.level)
  self:Fstring(m, c.report_id)
  self:Finteger(m, c.left_time)
end
function Paser:Fgame_item(m, c)
  self:Fstring(m, c.item_type)
  self:Finteger(m, c.number)
  self:Finteger(m, c.no)
  self:Finteger(m, c.level)
end
function Paser:Fgame_item_info(m, c)
  self:Fstring(m, c.item_type)
  self:Finteger(m, c.number)
  self:Finteger(m, c.no)
  self:Finteger(m, c.level)
  self:FArray(m, c.shared_rewards, "game_item")
end
function Paser:Fgame_items(m, c)
  self:FArray(m, c.items, "game_item")
end
function Paser:Floot_item(m, c)
  self:Fgame_item(m, c.base)
  self:Finteger(m, c.level)
  self:Fboolean(m, c.status)
end
function Paser:Fbattle_status_ack(m, c)
  self:FArray(m, c.rewards, "loot_item")
  self:Finteger(m, c.accomplish_level)
  self:Finteger(m, c.reloot)
  self:FArray(m, c.first, "battle_user_info")
  self:FArray(m, c.lowest, "battle_user_info")
  self:FArray(m, c.news, "battle_user_info")
  self:Finteger(m, c.user_level)
  self:Finteger(m, c.consume_num)
  self:Finteger(m, c.left_count)
  self:Finteger(m, c.price)
end
function Paser:Fbattle_fight_report_req(m, c)
  self:Fstring(m, c.battle_report_id)
end
function Paser:Fbattle_fight_report_ack(m, c)
  self:Finteger(m, c.code)
  self:FArray(m, c.histories, "fight_history")
end
function Paser:Fbattle_result(m, c)
  self:Finteger(m, c.accomplish_level)
  self:FArray(m, c.rewards, "loot_item")
end
function Paser:Fbag_req(m, c)
  self:Fstring(m, c.owner)
  self:Finteger(m, c.bag_type)
  self:Finteger(m, c.pos)
end
function Paser:Fbag_grid(m, c)
  self:Finteger(m, c.grid_type)
  self:Finteger(m, c.pos)
  self:Finteger(m, c.cnt)
  self:Fstring(m, c.item_id)
  self:Finteger(m, c.item_type)
  self:Finteger(m, c.title_type)
  self:Finteger(m, c.sub_type)
  self:FArray(m, c.equip_matrixs, "integer")
end
function Paser:Fbag_new_grid(m, c)
  self:Finteger(m, c.grid_type)
  self:Finteger(m, c.pos)
  self:Finteger(m, c.fleet_id)
  self:Finteger(m, c.cnt)
  self:Fstring(m, c.item_id)
  self:Finteger(m, c.item_type)
  self:Finteger(m, c.title_type)
  self:Finteger(m, c.sub_type)
  self:FArray(m, c.equip_matrixs, "integer")
end
function Paser:Fbag_info(m, c)
  self:Fstring(m, c.owner)
  self:Finteger(m, c.bag_type)
  self:FArray(m, c.grids, "bag_grid")
  self:FArray(m, c.equipedItems, "bag_new_grid")
end
function Paser:Fswap_bag_grid_req(m, c)
  self:Finteger(m, c.matrix_id)
  self:Fstring(m, c.orig_grid_owner)
  self:Finteger(m, c.oirg_grid_type)
  self:Finteger(m, c.oirg_grid_pos)
  self:Fstring(m, c.target_grid_owner)
  self:Finteger(m, c.target_grid_type)
  self:Finteger(m, c.target_grid_pos)
end
function Paser:Fswap_bag_grid_info(m, c)
  self:Finteger(m, c.result)
end
function Paser:Fuse_item_req(m, c)
  self:Finteger(m, c.pos)
  self:Fboolean(m, c.is_max)
  self:FArray(m, c.params, "string")
  self:Finteger(m, c.count)
end
function Paser:Fequipment(m, c)
  self:Fstring(m, c.equip_id)
  self:Finteger(m, c.equip_slot)
  self:Finteger(m, c.equip_type)
  self:Finteger(m, c.equip_level)
  self:Finteger(m, c.equip_level_req)
  self:FArray(m, c.equip_params, "equipment_param")
  self:Finteger(m, c.enchant_level)
  self:Finteger(m, c.enchant_effect)
  self:Finteger(m, c.enchant_effect_value)
  self:Finteger(m, c.enchant_item_id)
end
function Paser:Fequipment_param(m, c)
  self:Finteger(m, c.key)
  self:Finteger(m, c.value)
end
function Paser:Fequip_info_req(m, c)
  self:Finteger(m, c.id)
end
function Paser:Fequip_info_ack(m, c)
  self:Fequipment(m, c.equip)
end
function Paser:Fpack_bag(m, c)
  self:Fstring(m, c.owner)
  self:Finteger(m, c.bag_type)
  self:FArray(m, c.grids, "bag_grid")
end
function Paser:Fgm_cmd(m, c)
  self:Fstring(m, c.cmd)
end
function Paser:Ftechnique_info(m, c)
  self:Fshort(m, c.id)
  self:Fshort(m, c.tech_type)
  self:Fshort(m, c.enabled)
  self:Fshort(m, c.level)
  self:Fshort(m, c.level_limit)
  self:Fshort(m, c.level_max)
  self:Fshort(m, c.attr_id)
  self:Finteger(m, c.attr_value)
  self:Finteger(m, c.total)
  self:Finteger(m, c.cost)
  self:Finteger(m, c.every_consume)
  self:Finteger(m, c.max_for_now)
  self:Finteger(m, c.player_limit_level)
end
function Paser:Ftechniques_ack(m, c)
  self:FArray(m, c.techniques, "technique_info")
end
function Paser:Ftechniques_upgrade_req(m, c)
  self:Fshort(m, c.id)
  self:Fshort(m, c.want_level)
end
function Paser:Ftechniques_upgrade_ack(m, c)
  self:Ftechnique_info(m, c.technique)
end
function Paser:Ftechniques_upgrade_fail_ack(m, c)
  self:Finteger(m, c.code)
end
function Paser:Frevenue_info_ack(m, c)
  self:Finteger(m, c.total_times)
  self:Finteger(m, c.left_times)
  self:Finteger(m, c.left_time)
  self:Finteger(m, c.money)
  self:Finteger(m, c.exchange_cost)
end
function Paser:Frevenue_do_req(m, c)
  self:Finteger(m, c.times)
end
function Paser:Frevenue_do_ack(m, c)
  self:Finteger(m, c.more)
  self:Frevenue_info_ack(m, c.revenue)
end
function Paser:Ffight_report_fleet(m, c)
  self:Finteger(m, c.identity)
  self:Fshort(m, c.pos)
  self:Fdouble(m, c.cur_durability)
  self:Fdouble(m, c.max_durability)
  self:Fshort(m, c.cur_accumulator)
  self:Finteger(m, c.shield)
  self:Finteger(m, c.max_shield)
  self:Finteger(m, c.level)
  self:Finteger(m, c.spell_id)
  self:Fstring(m, c.ptype)
end
function Paser:Ffight_report_adjutant(m, c)
  self:Finteger(m, c.major)
  self:Finteger(m, c.pos)
  self:Finteger(m, c.adjutant)
  self:Finteger(m, c.adjutant_level)
end
function Paser:Fattack_data(m, c)
  self:Fshort(m, c.atk_pos)
  self:Fshort(m, c.def_pos)
  self:Fshort(m, c.attack_type)
  self:Fboolean(m, c.hit)
  self:Fboolean(m, c.crit)
  self:Fboolean(m, c.intercept)
  self:Finteger(m, c.shield_damage)
  self:Finteger(m, c.dur_damage)
  self:Fdouble(m, c.durability)
  self:Finteger(m, c.shield)
  self:Fshort(m, c.acc)
  self:Fshort(m, c.acc_change)
  self:Fshort(m, c.atk_acc)
  self:Fshort(m, c.atk_acc_change)
  self:Fstring(m, c.atk_effect)
  self:Fboolean(m, c.is_immune)
  self:Fstring(m, c.def_side)
  self:Fboolean(m, c.counter)
  self:Finteger(m, c.flag)
  self:Finteger(m, c.index)
end
function Paser:Fbuff_data(m, c)
  self:Fshort(m, c.buff_effect)
  self:Fshort(m, c.round_cnt)
  self:Fshort(m, c.pos)
  self:Finteger(m, c.param)
  self:Fshort(m, c.buff_step)
  self:Fstring(m, c.side)
  self:Fshort(m, c.effection)
  self:Finteger(m, c.index)
end
function Paser:Fdot_data(m, c)
  self:Fshort(m, c.pos)
  self:Fshort(m, c.buff_effect)
  self:Fdouble(m, c.durability)
  self:Finteger(m, c.shield)
  self:Finteger(m, c.shield_damage)
  self:Finteger(m, c.dur_damage)
  self:Fshort(m, c.round_cnt)
  self:Fboolean(m, c.is_immune)
  self:Finteger(m, c.index)
end
function Paser:Fdamage_data(m, c)
  self:Fstring(m, c.side)
  self:Fstring(m, c.type)
  self:Fshort(m, c.pos)
  self:Fshort(m, c.atk_from)
  self:Fdouble(m, c.durability)
  self:Finteger(m, c.shield)
  self:Finteger(m, c.shield_damage)
  self:Finteger(m, c.dur_damage)
  self:Finteger(m, c.index)
end
function Paser:Fsp_attack_data(m, c)
  self:Finteger(m, c.char_id)
  self:Finteger(m, c.adjutant)
  self:Fshort(m, c.atk_pos)
  self:Fshort(m, c.def_pos)
  self:Finteger(m, c.sp_id)
  self:Fboolean(m, c.hit)
  self:Fboolean(m, c.crit)
  self:Fboolean(m, c.intercept)
  self:Finteger(m, c.shield_damage)
  self:Finteger(m, c.dur_damage)
  self:Fshort(m, c.buff_effect)
  self:Fshort(m, c.round_cnt)
  self:Fdouble(m, c.durability)
  self:Finteger(m, c.shield)
  self:Fboolean(m, c.self_cast)
  self:Fshort(m, c.acc)
  self:Fshort(m, c.acc_change)
  self:Fshort(m, c.atk_acc)
  self:Fshort(m, c.atk_acc_change)
  self:Fboolean(m, c.is_immune)
  self:Finteger(m, c.transfiguration)
  self:Fboolean(m, c.is_time_back)
  self:Fshort(m, c.atk_type)
  self:Fshort(m, c.absorb_attr_type)
  self:Fstring(m, c.atk_side)
  self:Fshort(m, c.artifact)
  self:Finteger(m, c.flag)
  self:Finteger(m, c.index)
end
function Paser:Ffight_round(m, c)
  self:Fshort(m, c.round_cnt)
  self:Fboolean(m, c.player1_action)
  self:Fshort(m, c.pos)
  self:Finteger(m, c.p1_speed)
  self:Finteger(m, c.p2_speed)
  self:FArray(m, c.buff, "buff_data")
  self:FArray(m, c.dot, "dot_data")
  self:FArray(m, c.attacks, "attack_data")
  self:FArray(m, c.sp_attacks, "sp_attack_data")
  self:FArray(m, c.summon_fleet, "fight_report_fleet")
  self:FArray(m, c.damage, "damage_data")
  self:FArray(m, c.relive_fleet, "fight_report_fleet")
end
function Paser:Ffight_adjutant_data(m, c)
  self:Finteger(m, c.char_id)
  self:Finteger(m, c.atk_pos)
  self:Finteger(m, c.def_pos)
  self:Finteger(m, c.adjutant)
  self:Finteger(m, c.spell_id)
  self:Finteger(m, c.buff)
  self:Fboolean(m, c.self_cast)
end
function Paser:Ffight_report(m, c)
  self:Fstring(m, c.player1)
  self:Fstring(m, c.player1_avatar)
  self:Finteger(m, c.player1_identity)
  self:Finteger(m, c.player1_pos)
  self:FArray(m, c.player1_buffs, "player_buff")
  self:Fstring(m, c.player2)
  self:Fstring(m, c.player2_avatar)
  self:Finteger(m, c.player2_identity)
  self:Finteger(m, c.player2_pos)
  self:FArray(m, c.player2_buffs, "player_buff")
  self:FArray(m, c.player1_fleets, "fight_report_fleet")
  self:FArray(m, c.player2_fleets, "fight_report_fleet")
  self:FArray(m, c.rounds, "fight_round")
  self:Fshort(m, c.result)
  self:FArray(m, c.player1_adjutant, "fight_report_adjutant")
  self:FArray(m, c.player2_adjutant, "fight_report_adjutant")
  self:FArray(m, c.player1_buffs_adjutant, "fight_adjutant_data")
  self:FArray(m, c.player2_buffs_adjutant, "fight_adjutant_data")
  self:Finteger(m, c.player1_ranking)
  self:Finteger(m, c.player1_rank)
  self:Finteger(m, c.player2_ranking)
  self:Finteger(m, c.player2_rank)
end
function Paser:Fplayer_buff(m, c)
  self:Fstring(m, c.type)
  self:Fstring(m, c.value)
end
function Paser:Fadd_friend_req(m, c)
  self:Fstring(m, c.id)
  self:Fstring(m, c.name)
end
function Paser:Fdel_friend_req(m, c)
  self:Fstring(m, c.id)
end
function Paser:Fofficer_info(m, c)
  self:Fshort(m, c.id)
  self:Finteger(m, c.price)
  self:Fshort(m, c.level_limit)
  self:Fshort(m, c.free_time)
  self:Fstring(m, c.loot_type)
  self:Finteger(m, c.loot_value)
  self:Finteger(m, c.ext_num)
  self:Finteger(m, c.strike)
end
function Paser:Fofficers_ack(m, c)
  self:FArray(m, c.officers, "officer_info")
end
function Paser:Fofficer_buy_req(m, c)
  self:Fshort(m, c.id)
end
function Paser:Fofficer_buy_ack(m, c)
  self:Fofficer_info(m, c.officer)
end
function Paser:Ffleet_show_info(m, c)
  self:Finteger(m, c.fleet_id)
  self:Fshort(m, c.level)
  self:Fshort(m, c.color)
  self:Fstring(m, c.avatar)
  self:Fstring(m, c.name)
  self:Fstring(m, c.vessels_name)
  self:Fshort(m, c.vessels)
  self:Fstring(m, c.ship)
  self:Fshort(m, c.rank)
  self:Fshort(m, c.damage_assess)
  self:Fshort(m, c.damage_rate)
  self:Fshort(m, c.defence_assess)
  self:Fshort(m, c.defence_rate)
  self:Fshort(m, c.assist_assess)
  self:Fshort(m, c.assist_rate)
  self:Fshort(m, c.atk_type)
  self:Fshort(m, c.spell_id)
  self:Fshort(m, c.can_be_adjutant)
  self:Fdouble(m, c.force)
end
function Paser:Frecruit_fleet_info(m, c)
  self:Finteger(m, c.identity)
  self:Fshort(m, c.state)
  self:Finteger(m, c.quark_req)
  self:Finteger(m, c.lepton_req)
  self:Finteger(m, c.prestige_req)
  self:Finteger(m, c.visible_battle_req)
  self:Finteger(m, c.atk_scores)
  self:Finteger(m, c.def_scores)
  self:Finteger(m, c.ass_scores)
  self:Finteger(m, c.sort_no)
  self:Ffleet_show_info(m, c.show_info)
end
function Paser:Frecruit_list_ack(m, c)
  self:FArray(m, c.recruit_fleets, "recruit_fleet_info")
end
function Paser:Frecruit_fleet_req(m, c)
  self:Finteger(m, c.identity)
end
function Paser:Frecruit_fleet_ack(m, c)
  self:Finteger(m, c.code)
  self:Finteger(m, c.identity)
  self:Fshort(m, c.state)
end
function Paser:Fdismiss_fleet_req(m, c)
  self:Finteger(m, c.id)
end
function Paser:Fdismiss_fleet_ack(m, c)
  self:Finteger(m, c.code)
end
function Paser:Ffleet_one_req(m, c)
  self:Finteger(m, c.id)
end
function Paser:Ffriends_ack(m, c)
  self:FArray(m, c.friends, "friend_info")
  self:FArray(m, c.ban_friends, "friend_info")
  self:Finteger(m, c.message_size)
end
function Paser:Fequipment_enhance(m, c)
  self:Fstring(m, c.equip_id)
  self:Finteger(m, c.equip_type)
  self:Finteger(m, c.equip_level)
  self:Finteger(m, c.equip_next_level)
  self:Fshort(m, c.equip_level_fulled)
  self:FArray(m, c.equip_params, "equipment_param")
  self:FArray(m, c.equip_next_params, "equipment_param")
  self:Finteger(m, c.cost)
  self:Finteger(m, c.cdtime)
  self:Finteger(m, c.enchant_level)
  self:Finteger(m, c.enchant_effect)
  self:Finteger(m, c.enchant_effect_value)
  self:Finteger(m, c.enchant_item_id)
end
function Paser:Fequipment_evolution(m, c)
  self:Finteger(m, c.equip_type)
  self:Finteger(m, c.equip_level)
  self:FArray(m, c.equip_params, "equipment_param")
  self:Finteger(m, c.cost)
  self:Finteger(m, c.enchant_level)
  self:Finteger(m, c.enchant_effect)
  self:Finteger(m, c.enchant_effect_value)
  self:Finteger(m, c.enchant_item_id)
end
function Paser:Fequipment_recipe(m, c)
  self:Finteger(m, c.equip_type)
  self:Finteger(m, c.player_level_limit)
  self:Fshort(m, c.existed)
  self:Fshort(m, c.recipe_total)
end
function Paser:Fequipment_detail_info(m, c)
  self:Fequipment_enhance(m, c.enhance)
  self:Fequipment_evolution(m, c.evolution)
  self:Fequipment_recipe(m, c.recipe)
  self:Fshort(m, c.enable_evolution)
end
function Paser:Fequipment_detail_ack(m, c)
  self:Fequipment_enhance(m, c.enhance)
  self:Fequipment_evolution(m, c.evolution)
  self:Fequipment_recipe(m, c.recipe)
  self:Fshort(m, c.enable_evolution)
  self:Finteger(m, c.enchant_level)
  self:Fdouble(m, c.one_enhance)
  self:Finteger(m, c.enhance_level)
end
function Paser:Fequipment_detail_req(m, c)
  self:Fstring(m, c.id)
end
function Paser:Ffriend_detail_req(m, c)
  self:Fstring(m, c.id)
end
function Paser:Ffriend_search_req(m, c)
  self:Fstring(m, c.name)
end
function Paser:Ffriend_search_ack(m, c)
  self:FArray(m, c.friends, "friend_info")
end
function Paser:Ffriend_ban_req(m, c)
  self:Fstring(m, c.id)
  self:Fstring(m, c.name)
end
function Paser:Fwarpgate_energy_bolt(m, c)
  self:Fshort(m, c.bolt_type)
  self:Fshort(m, c.level)
  self:Fshort(m, c.val)
  self:Fboolean(m, c.is_full)
end
function Paser:Fwarpgate_info(m, c)
  self:Finteger(m, c.lepton)
  self:Finteger(m, c.quark)
  self:Fboolean(m, c.free_lepton_charge)
  self:Fboolean(m, c.free_quark_charge)
  self:Finteger(m, c.charge_lepton_cost)
  self:Finteger(m, c.charge_quark_cost)
  self:Finteger(m, c.normal_upgrade_cost)
  self:Finteger(m, c.max_upgrade_cost)
  self:FArray(m, c.bolts, "warpgate_energy_bolt")
  self:Finteger(m, c.convert_cost_credits)
end
function Paser:Fwarpgate_charge_req(m, c)
  self:Fshort(m, c.charge_type)
end
function Paser:Fwarpgate_charge_ack(m, c)
  self:Finteger(m, c.code)
  self:Fwarpgate_info(m, c.info)
end
function Paser:Fwarpgate_upgrade_req(m, c)
  self:Fshort(m, c.upgrade_type)
end
function Paser:Fwarpgate_upgrade_ack(m, c)
  self:Finteger(m, c.code)
  self:Fwarpgate_info(m, c.info)
end
function Paser:Fwarpgate_collect_ack(m, c)
  self:Finteger(m, c.code)
  self:Fwarpgate_info(m, c.info)
end
function Paser:Fmail_page_req(m, c)
  self:Finteger(m, c.page_size)
  self:Finteger(m, c.page_no)
end
function Paser:Fmail_info(m, c)
  self:Fstring(m, c.id)
  self:Fstring(m, c.title)
  self:Finteger(m, c.status)
  self:Finteger(m, c.time)
  self:Fshort(m, c.sender_role)
  self:FArray(m, c.goods, "game_item")
  self:Finteger(m, c.get_goods)
  self:Fboolean(m, c.can_send_story)
end
function Paser:Fmail_page_ack(m, c)
  self:Finteger(m, c.max)
  self:Finteger(m, c.page_size)
  self:Finteger(m, c.page_no)
  self:FArray(m, c.mails, "mail_info")
  self:Finteger(m, c.unread)
end
function Paser:Fmail_goods_req(m, c)
  self:Fstring(m, c.id)
end
function Paser:Fmail_goods_ack(m, c)
  self:Fmail_info(m, c.new_info)
end
function Paser:Fmail_content_req(m, c)
  self:Fstring(m, c.id)
end
function Paser:Fmail_content_ack(m, c)
  self:Fstring(m, c.id)
  self:Fstring(m, c.title)
  self:Fstring(m, c.content)
  self:Finteger(m, c.time)
  self:Fstring(m, c.sender_name)
  self:Fstring(m, c.sender_id)
  self:Fshort(m, c.sender_role)
end
function Paser:Fmail_send_req(m, c)
  self:FArray(m, c.recevier_names, "string")
  self:Fstring(m, c.title)
  self:Fstring(m, c.content)
end
function Paser:Fmail_delete_req(m, c)
  self:FArray(m, c.ids, "string")
end
function Paser:Fmail_set_read_req(m, c)
  self:FArray(m, c.ids, "string")
end
function Paser:Fequipment_enhance_req(m, c)
  self:Fstring(m, c.equip_id)
  self:Fstring(m, c.fleet_id)
  self:Finteger(m, c.once_more_enhance)
  self:Finteger(m, c.en_level)
end
function Paser:Fequipment_evolution_req(m, c)
  self:Fstring(m, c.equip_id)
  self:Fstring(m, c.fleet_id)
end
function Paser:Fequipments_ack(m, c)
  self:FArray(m, c.equipments, "equipment")
end
function Paser:Fquark_exchange_req(m, c)
  self:Fstring(m, c.use_type)
  self:Fshort(m, c.cnt)
end
function Paser:Fquark_exchange_ack(m, c)
  self:Finteger(m, c.code)
end
function Paser:Fshop_purchase_req(m, c)
  self:Finteger(m, c.item_type)
  self:Fshort(m, c.cnt)
end
function Paser:Fshop_purchase_ack(m, c)
  self:Finteger(m, c.code)
end
function Paser:Fshop_sell_req(m, c)
  self:Fstring(m, c.grid_owner)
  self:Finteger(m, c.grid_type)
  self:Finteger(m, c.grid_pos)
  self:Fshort(m, c.cnt)
end
function Paser:Fshop_sell_ack(m, c)
  self:Finteger(m, c.code)
end
function Paser:Fpurchase_back_item(m, c)
  self:Finteger(m, c.item_type)
  self:Fstring(m, c.item_id)
  self:Fshort(m, c.idx)
  self:Fshort(m, c.cnt)
  self:Finteger(m, c.enchant_level)
end
function Paser:Fshop_purchase_back_list_ack(m, c)
  self:FArray(m, c.items, "purchase_back_item")
end
function Paser:Fshop_purchase_back_req(m, c)
  self:Fshort(m, c.idx)
end
function Paser:Fshop_purchase_back_ack(m, c)
  self:Finteger(m, c.code)
end
function Paser:Fuser_offline_info_ack(m, c)
  self:Fuser_offline_info(m, c.offline_info)
end
function Paser:Fuser_collect_offline_expr_ack(m, c)
  self:Fshort(m, c.result)
end
function Paser:Ffleet_repair_all_req(m, c)
  self:Finteger(m, c.onserver)
end
function Paser:Ffleet_repair_req(m, c)
  self:Finteger(m, c.identity)
end
function Paser:Ffriend_ban_del_req(m, c)
  self:Fstring(m, c.id)
end
function Paser:Fmessage_info(m, c)
  self:Fstring(m, c.id)
  self:Fstring(m, c.user_id)
  self:Fstring(m, c.user_name)
end
function Paser:Fmessages_ack(m, c)
  self:FArray(m, c.messages, "message_info")
end
function Paser:Fmessage_del_req(m, c)
  self:Fstring(m, c.id)
end
function Paser:Falliances_req(m, c)
  self:Fstring(m, c.keyword)
  self:Finteger(m, c.index)
  self:Finteger(m, c.size)
end
function Paser:Falliances_ack(m, c)
  self:Finteger(m, c.total)
  self:Finteger(m, c.index)
  self:FArray(m, c.alliances, "alliance_info")
  self:FArray(m, c.applied_alliances, "string")
end
function Paser:Falliance_level_info(m, c)
  self:Fshort(m, c.level)
  self:Finteger(m, c.experience)
  self:Finteger(m, c.uplevel_experience)
end
function Paser:Falliance_info(m, c)
  self:Fstring(m, c.id)
  self:Fstring(m, c.name)
  self:Fstring(m, c.memo)
  self:Fshort(m, c.flag)
  self:Falliance_level_info(m, c.level_info)
  self:Finteger(m, c.rank)
  self:Fstring(m, c.creator_id)
  self:Fstring(m, c.creator_name)
  self:Fshort(m, c.members_count)
  self:Fshort(m, c.members_max)
  self:Finteger(m, c.apply_limit)
end
function Paser:Falliance_info_req(m, c)
  self:Fstring(m, c.id)
end
function Paser:Falliance_info_ack(m, c)
  self:Falliance_info(m, c.alliance)
end
function Paser:Falliance_members_req(m, c)
  self:Fstring(m, c.alliance_id)
  self:Finteger(m, c.index)
  self:Finteger(m, c.size)
end
function Paser:Falliance_members_ack(m, c)
  self:Finteger(m, c.total)
  self:Finteger(m, c.index)
  self:FArray(m, c.members, "alliance_member_info")
end
function Paser:Falliance_member_info(m, c)
  self:Fstring(m, c.id)
  self:Fstring(m, c.name)
  self:Fshort(m, c.level)
  self:Fshort(m, c.position)
  self:Fshort(m, c.military_rank)
  self:Finteger(m, c.rank)
  self:Finteger(m, c.contribution)
  self:Finteger(m, c.last_login)
  self:Fdouble(m, c.force)
  self:Fshort(m, c.sex)
  self:Finteger(m, c.main_fleet_id)
  self:FArray(m, c.fleet_ids, "alliance_fleet_info")
end
function Paser:Falliance_activities_req(m, c)
  self:Finteger(m, c.alliance_id)
end
function Paser:Falliance_activities_ack(m, c)
  self:FArray(m, c.activities, "alliance_activity_info")
end
function Paser:Falliance_activity_info(m, c)
  self:Fshort(m, c.id)
  self:Fshort(m, c.level_limit)
  self:Fshort(m, c.status)
  self:Fshort(m, c.left_times)
end
function Paser:Falliance_activity_times_info(m, c)
  self:Fshort(m, c.id)
  self:Fshort(m, c.left_times)
end
function Paser:Falliance_activity_times_ntf(m, c)
  self:FArray(m, c.times, "alliance_activity_times_info")
end
function Paser:Falliance_logs_req(m, c)
  self:Fstring(m, c.id)
end
function Paser:Falliance_logs_ack(m, c)
  self:FArray(m, c.logs, "alliance_log_info")
end
function Paser:Falliance_log_info(m, c)
  self:Finteger(m, c.time)
  self:Fshort(m, c.type)
  self:FArray(m, c.params, "string")
end
function Paser:Falliance_aduits_req(m, c)
  self:Fstring(m, c.alliance_id)
  self:Finteger(m, c.index)
  self:Finteger(m, c.size)
end
function Paser:Falliance_aduits_ack(m, c)
  self:Finteger(m, c.total)
  self:Finteger(m, c.index)
  self:FArray(m, c.members, "alliance_aduit_info")
end
function Paser:Falliance_aduit_info(m, c)
  self:Fstring(m, c.id)
  self:Fstring(m, c.name)
  self:Fshort(m, c.level)
  self:Finteger(m, c.rank)
  self:Finteger(m, c.applied_at)
end
function Paser:Falliance_create_req(m, c)
  self:Fstring(m, c.name)
end
function Paser:Falliance_create_ack(m, c)
  self:Falliance_info(m, c.alliance)
end
function Paser:Falliance_create_fail_ack(m, c)
  self:Finteger(m, c.code)
end
function Paser:Falliance_delete_ack(m, c)
  self:Fstring(m, c.id)
end
function Paser:Falliance_delete_fail_ack(m, c)
  self:Finteger(m, c.code)
end
function Paser:Falliance_apply_req(m, c)
  self:Fstring(m, c.id)
end
function Paser:Falliance_apply_ack(m, c)
  self:Fshort(m, c.apply_type)
  self:FArray(m, c.applied_alliances, "string")
end
function Paser:Falliance_apply_fail_ack(m, c)
  self:Finteger(m, c.code)
  self:Finteger(m, c.limit_level)
end
function Paser:Falliance_unapply_req(m, c)
  self:Fstring(m, c.id)
end
function Paser:Falliance_unapply_ack(m, c)
  self:FArray(m, c.applied_alliances, "string")
end
function Paser:Falliance_unapply_fail_ack(m, c)
  self:Finteger(m, c.code)
end
function Paser:Falliance_ratify_req(m, c)
  self:FArray(m, c.ids, "string")
end
function Paser:Falliance_ratify_result(m, c)
  self:Fstring(m, c.user_name)
  self:Fshort(m, c.code)
end
function Paser:Falliance_ratify_ack(m, c)
  self:Finteger(m, c.total)
  self:FArray(m, c.result, "alliance_ratify_result")
end
function Paser:Falliance_refuse_result(m, c)
  self:Fstring(m, c.user_name)
  self:Fshort(m, c.code)
end
function Paser:Falliance_refuse_ack(m, c)
  self:Finteger(m, c.total)
  self:FArray(m, c.result, "alliance_refuse_result")
end
function Paser:Falliance_refuse_req(m, c)
  self:FArray(m, c.ids, "string")
end
function Paser:Falliance_demote_req(m, c)
  self:Fstring(m, c.user_id)
end
function Paser:Falliance_demote_ack(m, c)
  self:Fstring(m, c.user_id)
end
function Paser:Falliance_demote_fail_ack(m, c)
  self:Finteger(m, c.code)
end
function Paser:Falliance_promote_req(m, c)
  self:Fstring(m, c.user_id)
end
function Paser:Falliance_promote_ack(m, c)
  self:Fstring(m, c.user_id)
end
function Paser:Falliance_promote_fail_ack(m, c)
  self:Finteger(m, c.code)
end
function Paser:Falliance_kick_req(m, c)
  self:Fstring(m, c.user_id)
end
function Paser:Falliance_kick_ack(m, c)
  self:Finteger(m, c.total)
  self:Fstring(m, c.user_name)
end
function Paser:Falliance_kick_fail_ack(m, c)
  self:Finteger(m, c.code)
end
function Paser:Falliance_transfer_req(m, c)
  self:Fstring(m, c.user_id)
end
function Paser:Falliance_transfer_ack(m, c)
  self:Fstring(m, c.user_id)
end
function Paser:Falliance_transfer_fail_ack(m, c)
  self:Finteger(m, c.code)
end
function Paser:Falliance_quit_ack(m, c)
  self:Fstring(m, c.user_id)
end
function Paser:Falliance_quit_fail_ack(m, c)
  self:Finteger(m, c.code)
end
function Paser:Ffleet_damage_info(m, c)
  self:Finteger(m, c.id)
  self:Finteger(m, c.damage)
end
function Paser:Fbattle_result_ack(m, c)
  self:Fbattle_result(m, c.result)
  self:Ffight_report(m, c.report)
  self:FArray(m, c.fleets, "fleet_damage_info")
end
function Paser:Frush_result_ack(m, c)
  self:FArray(m, c.rewards, "game_item")
  self:FArray(m, c.fleets, "fleet_damage_info")
  self:Finteger(m, c.strike)
end
function Paser:Fadd_friend_ack(m, c)
  self:Ffriend_info(m, c.friend)
end
function Paser:Fadd_on(m, c)
  self:Finteger(m, c.type)
  self:Finteger(m, c.value)
  self:Finteger(m, c.next_value)
end
function Paser:Fformation_info(m, c)
  self:Fshort(m, c.formation_id)
  self:Finteger(m, c.fleet_id)
  self:Fshort(m, c.slot)
end
function Paser:Fkrypton_info(m, c)
  self:Fstring(m, c.id)
  self:Fshort(m, c.level)
  self:Fshort(m, c.rank)
  self:Fshort(m, c.status)
  self:Finteger(m, c.item_type)
  self:Finteger(m, c.decompose_kenergy)
  self:Finteger(m, c.kenergy_upgrade)
  self:Fshort(m, c.slot)
  self:FArray(m, c.addon, "add_on")
  self:Fboolean(m, c.max_level)
  self:Finteger(m, c.refine_exp)
  self:Finteger(m, c.inject_exp)
  self:Finteger(m, c.refine_need_exp)
  self:Finteger(m, c.refine_need_kenergy)
  self:FArray(m, c.formation, "formation_info")
end
function Paser:Fkrypton_store_req(m, c)
  self:Finteger(m, c.formation_id)
end
function Paser:Fkrypton_store_ack(m, c)
  self:Finteger(m, c.grid_capacity)
  self:FArray(m, c.kryptons, "krypton_info")
  self:Finteger(m, c.klevel)
  self:Finteger(m, c.gacha_need_money)
  self:Finteger(m, c.gacha_need_credit)
  self:Finteger(m, c.use_credit_count)
  self:Fboolean(m, c.is_high)
end
function Paser:Fkrypton_enhance_req(m, c)
  self:Fstring(m, c.id)
  self:Finteger(m, c.fleet_identity)
  self:Finteger(m, c.formation_id)
end
function Paser:Fkrypton_enhance_ack(m, c)
  self:Fkrypton_info(m, c.krypton)
end
function Paser:Fkrypton_equip_req(m, c)
  self:Finteger(m, c.formation_id)
  self:Finteger(m, c.fleet_identity)
  self:Fstring(m, c.krypton_id)
  self:Finteger(m, c.slot)
end
function Paser:Fkrypton_equip_off_req(m, c)
  self:Finteger(m, c.formation_id)
  self:Finteger(m, c.fleet_identity)
  self:Fstring(m, c.krypton_id)
  self:Finteger(m, c.slot)
end
function Paser:Fkrypton_equip_ack(m, c)
  self:Ffleet(m, c.fleet_info)
  self:FArray(m, c.kryptons, "krypton_info")
end
function Paser:Fkrypton_gacha_one_ack(m, c)
  self:Fshort(m, c.money_or_krypton)
  self:Finteger(m, c.money)
  self:Fkrypton_info(m, c.krypton)
  self:Finteger(m, c.klevel)
  self:Fboolean(m, c.is_high)
  self:Finteger(m, c.gacha_need_money)
  self:Finteger(m, c.gacha_need_credit)
  self:Finteger(m, c.increase_kenergy)
  self:Finteger(m, c.use_credit_count)
end
function Paser:Fkrypton_gacha_some_req(m, c)
  self:Finteger(m, c.auto_decompose)
  self:Fboolean(m, c.use_credit)
end
function Paser:Fkrypton_gacha_one_req(m, c)
  self:Finteger(m, c.auto_decompose)
  self:Fboolean(m, c.use_credit)
end
function Paser:Fkrypton_gacha_some_ack(m, c)
  self:FArray(m, c.kryptons, "krypton_info")
  self:Finteger(m, c.klevel)
  self:Fboolean(m, c.is_high)
  self:Finteger(m, c.gacha_need_money)
  self:Finteger(m, c.gacha_need_credit)
  self:Finteger(m, c.increase_kenergy)
  self:Finteger(m, c.increase_money)
  self:Finteger(m, c.use_credit_count)
end
function Paser:Fkrypton_decompose_req(m, c)
  self:Fshort(m, c.krypton_type)
  self:Fstring(m, c.id)
end
function Paser:Fkrypton_decompose_ack(m, c)
  self:Finteger(m, c.increase_kenergy)
end
function Paser:Fkrypton_move_to_store_req(m, c)
  self:Fstring(m, c.id)
end
function Paser:Falliance_memo_req(m, c)
  self:Fstring(m, c.content)
end
function Paser:Falliance_memo_ack(m, c)
  self:Falliance_info(m, c.alliance)
end
function Paser:Falliance_memo_fail_ack(m, c)
  self:Finteger(m, c.code)
end
function Paser:Falliance_flag_req(m, c)
  self:Finteger(m, c.flag)
end
function Paser:Fuser_champion_data(m, c)
  self:Fstring(m, c.user_id)
  self:Finteger(m, c.rank)
  self:Fstring(m, c.name)
  self:Finteger(m, c.icon)
  self:Finteger(m, c.level)
  self:Fdouble(m, c.force)
  self:FArray(m, c.cached_fleets, "cached_fleet")
end
function Paser:Fchampion_info(m, c)
  self:Finteger(m, c.rank)
  self:Fshort(m, c.max_challenge_cnt)
  self:Fshort(m, c.challenge_cnt)
  self:Finteger(m, c.next_fight_time)
  self:Fboolean(m, c.award)
  self:Finteger(m, c.next_award_time)
  self:Fboolean(m, c.straight_award)
  self:Finteger(m, c.straight_times)
  self:Finteger(m, c.exchange_cost)
end
function Paser:Fchampion_list_ack(m, c)
  self:Fchampion_info(m, c.brief_info)
  self:FArray(m, c.users, "user_champion_data")
  self:Fuser_champion_data(m, c.top_user)
end
function Paser:Fchampion_challenge_req(m, c)
  self:Finteger(m, c.rank)
end
function Paser:Fchampion_challenge_ack(m, c)
  self:Finteger(m, c.code)
  self:Fshort(m, c.result)
  self:Fchampion_info(m, c.brief_info)
  self:FArray(m, c.reports, "fight_report")
  self:FArray(m, c.users, "user_champion_data")
  self:FArray(m, c.awards, "game_item")
end
function Paser:Fchampion_top_ack(m, c)
  self:FArray(m, c.users, "user_champion_data")
end
function Paser:Fchampion_reset_cd_ack(m, c)
  self:Finteger(m, c.code)
  self:Fchampion_info(m, c.brief_info)
end
function Paser:Fchampion_get_award_ack(m, c)
  self:Finteger(m, c.code)
  self:Finteger(m, c.rank)
  self:FArray(m, c.awards, "game_item")
  self:Fchampion_info(m, c.brief_info)
end
function Paser:Fkrypton_store_swap_req(m, c)
  self:Fstring(m, c.id_from)
  self:Fstring(m, c.id_to)
  self:Finteger(m, c.slot_to)
end
function Paser:Fkrypton_fleet_equip_req(m, c)
  self:Finteger(m, c.formation_id)
  self:Finteger(m, c.fleet_identity)
end
function Paser:Ffleet_krypton_info(m, c)
  self:Fstring(m, c.fleet_id)
  self:Finteger(m, c.fleet_identity)
  self:FArray(m, c.kryptons, "krypton_info")
end
function Paser:Ffight_history(m, c)
  self:Fstring(m, c.report_id)
  self:Finteger(m, c.fight_time)
  self:Ffight_report(m, c.report)
end
function Paser:Ffight_history_with_rank(m, c)
  self:Fstring(m, c.report_id)
  self:Finteger(m, c.fight_time)
  self:Finteger(m, c.player1_rank)
  self:Finteger(m, c.player2_rank)
  self:Finteger(m, c.result)
  self:Fstring(m, c.player1_name)
  self:Fstring(m, c.player2_name)
end
function Paser:Fuser_fight_history_ack(m, c)
  self:FArray(m, c.histories, "fight_history_with_rank")
end
function Paser:Fuser_brief_info_req(m, c)
  self:Fstring(m, c.user_id)
end
function Paser:Fcached_fleet(m, c)
  self:Finteger(m, c.cell_id)
  self:Finteger(m, c.fleet_identity)
  self:Fdouble(m, c.force)
  self:Finteger(m, c.krypton)
  self:FArray(m, c.kryptons, "krypton_info")
  self:Finteger(m, c.adjutant_id)
  self:FArray(m, c.equipments, "equipment")
  self:Finteger(m, c.level)
end
function Paser:Fuser_brief_info(m, c)
  self:Fstring(m, c.name)
  self:Finteger(m, c.level)
  self:Fdouble(m, c.force)
  self:Fshort(m, c.icon)
  self:Finteger(m, c.krypton)
  self:Fstring(m, c.alliance)
  self:Fstring(m, c.alliance_id)
  self:Fshort(m, c.position)
  self:Finteger(m, c.rank)
  self:FArray(m, c.cached_fleets, "cached_fleet")
end
function Paser:Fkrypton_fleet_move_req(m, c)
  self:Finteger(m, c.formation_id)
  self:Finteger(m, c.fleet_identity)
  self:Fstring(m, c.krypton_id)
  self:Finteger(m, c.dest_slot)
end
function Paser:Fuser_challenged_ack(m, c)
  self:Fstring(m, c.player1)
  self:Fstring(m, c.player2)
  self:Finteger(m, c.result)
  self:Finteger(m, c.rank)
end
function Paser:Fshop_buy_and_wear_req(m, c)
  self:Finteger(m, c.formation_id)
  self:Fstring(m, c.fleet_id)
  self:Finteger(m, c.equip_type)
end
function Paser:Falliance_info_ntf(m, c)
  self:Falliance_pos_and_id(m, c.alliance)
end
function Paser:Ftask_reward(m, c)
  self:Fstring(m, c.reward_type)
  self:Finteger(m, c.number)
end
function Paser:Fsub_task_info(m, c)
  self:Finteger(m, c.id)
  self:Fstring(m, c.action)
  self:Finteger(m, c.number)
  self:Finteger(m, c.finish_count)
  self:Fboolean(m, c.is_get_reward)
  self:Fboolean(m, c.is_finished)
  self:Finteger(m, c.player_level)
  self:FArray(m, c.rewards, "game_item")
  self:Fboolean(m, c.is_enabled)
end
function Paser:Ftask_info(m, c)
  self:Finteger(m, c.id)
  self:Fstring(m, c.task_name)
  self:Fboolean(m, c.is_get_reward)
  self:Fboolean(m, c.is_finished)
  self:FArray(m, c.rewards, "game_item")
  self:FArray(m, c.task_content, "sub_task_info")
  self:Fshort(m, c.reward_number)
end
function Paser:Ftask_list_ack(m, c)
  self:FArray(m, c.task_list, "task_info")
end
function Paser:Fget_task_reward_req(m, c)
  self:Finteger(m, c.task_id)
end
function Paser:Fprestige_info(m, c)
  self:Fshort(m, c.prestige_level)
  self:Finteger(m, c.prestige_require)
  self:Fboolean(m, c.obtained)
  self:Finteger(m, c.award_money)
  self:Finteger(m, c.award_fleet)
  self:Finteger(m, c.award_tech)
  self:Fshort(m, c.recruit_cnt)
  self:Fshort(m, c.fight_fleet_cnt)
  self:Fshort(m, c.new_matrix_cell)
  self:Finteger(m, c.crit_lv)
  self:Finteger(m, c.motility)
  self:Finteger(m, c.hit_rate)
  self:Finteger(m, c.anti_crit)
  self:FArray(m, c.shared_rewards, "game_item")
end
function Paser:Fprestige_info_ack(m, c)
  self:Finteger(m, c.prestige)
  self:FArray(m, c.prestige_infos, "prestige_info")
end
function Paser:Fprestige_complete_ntf(m, c)
  self:Fprestige_info(m, c.complete_prestige)
end
function Paser:Fprestige_obtain_award_req(m, c)
  self:Fshort(m, c.prestige_level)
end
function Paser:Fprestige_obtain_award_ack(m, c)
  self:Finteger(m, c.code)
end
function Paser:Fachievement_type_list(m, c)
  self:FArray(m, c.classify_list, "string")
end
function Paser:Fachievement_info(m, c)
  self:Finteger(m, c.id)
  self:Fstring(m, c.classify)
  self:Finteger(m, c.number)
  self:Finteger(m, c.finish_count)
  self:Finteger(m, c.finish_time)
  self:Fshort(m, c.is_finished)
  self:Fshort(m, c.level)
  self:Fstring(m, c.group)
  self:Finteger(m, c.credit)
  self:Fboolean(m, c.can_show)
  self:FArray(m, c.rewards, "game_item")
  self:Finteger(m, c.weight)
end
function Paser:Fachievement_list_req(m, c)
  self:Fstring(m, c.classify)
end
function Paser:Fachievement_list_ack(m, c)
  self:FArray(m, c.achievement_list, "achievement_info")
end
function Paser:Fachievement_classes_ack(m, c)
  self:FArray(m, c.classify_list, "string")
end
function Paser:Fachievement_finish_ntf(m, c)
  self:Finteger(m, c.id)
  self:Finteger(m, c.level)
  self:FArray(m, c.awards, "game_item")
  self:FArray(m, c.shared_rewards, "game_item")
end
function Paser:Fachievement_overview_item(m, c)
  self:Finteger(m, c.level)
  self:Finteger(m, c.number)
  self:Finteger(m, c.finish_count)
end
function Paser:Fachievement_overview_ack(m, c)
  self:Finteger(m, c.total_number)
  self:Finteger(m, c.total_finish_count)
  self:FArray(m, c.items, "achievement_overview_item")
end
function Paser:Ffleet_kryptons_array(m, c)
  self:FArray(m, c.fleetkryptons, "fleet_krypton_info")
end
function Paser:Fsupply_notify(m, c)
  self:Fstring(m, c.type)
  self:Finteger(m, c.current)
  self:Finteger(m, c.max)
end
function Paser:Fsupply_type(m, c)
  self:Fstring(m, c.type)
end
function Paser:Fequipments_update_ntf(m, c)
  self:FArray(m, c.delete_list, "string")
  self:FArray(m, c.update_list, "equipment")
end
function Paser:Fattribute_change_info(m, c)
  self:Fstring(m, c.key)
  self:Finteger(m, c.value)
end
function Paser:Fattributes_change_ntf(m, c)
  self:FArray(m, c.attributes, "attribute_change_info")
end
function Paser:Fact_status_req(m, c)
  self:Finteger(m, c.act_id)
end
function Paser:Fchapter_status(m, c)
  self:Finteger(m, c.id)
  self:Finteger(m, c.level)
end
function Paser:Fact_status_ack(m, c)
  self:FArray(m, c.status, "chapter_status")
end
function Paser:Fall_act_status_req(m, c)
  self:Finteger(m, c.act_no)
end
function Paser:Fall_act_status_ack(m, c)
  self:FArray(m, c.acts, "act_status_ack")
end
function Paser:Fsupply_info_ack(m, c)
  self:Fstring(m, c.type)
  self:Finteger(m, c.exchange_cost)
  self:Finteger(m, c.count)
  self:Finteger(m, c.left)
end
function Paser:Fcdtimes_clear_req(m, c)
  self:Finteger(m, c.cdtype)
end
function Paser:Fsystem_configuration_req(m, c)
  self:FArray(m, c.config_attrs, "string")
end
function Paser:Fconfiguration_info(m, c)
  self:Fstring(m, c.config_attr)
  self:Fstring(m, c.config_value)
end
function Paser:Fsystem_configuration_ack(m, c)
  self:FArray(m, c.configs, "configuration_info")
end
function Paser:Fpay_verify_req(m, c)
  self:Fshort(m, c.platform)
  self:Fstring(m, c.pay_type)
  self:Fstring(m, c.token)
  self:Fstring(m, c.signature)
end
function Paser:Fpay_verify2_req(m, c)
  self:Fstring(m, c.id)
  self:Fstring(m, c.payment_type)
  self:FArray(m, c.params, "string")
end
function Paser:Fpay_verify2_ack(m, c)
  self:Finteger(m, c.code)
  self:Finteger(m, c.id)
  self:Fstring(m, c.payment_url)
end
function Paser:Fpay_list_req(m, c)
  self:Fshort(m, c.platform)
end
function Paser:Fpay_list2_req(m, c)
  self:Fshort(m, c.client)
end
function Paser:Fpayment_info(m, c)
  self:Fstring(m, c.id)
  self:Finteger(m, c.price)
  self:Fstring(m, c.currency_type)
end
function Paser:Fproduction_info(m, c)
  self:Fstring(m, c.id)
  self:Finteger(m, c.price)
  self:Finteger(m, c.credit)
  self:FArray(m, c.payments, "payment_info")
  self:Finteger(m, c.is_push)
end
function Paser:Fword_font(m, c)
  self:Fstring(m, c.title_font)
  self:Fstring(m, c.end_font)
  self:Fstring(m, c.time_font)
  self:Fstring(m, c.item_font)
  self:Fstring(m, c.discount_font)
end
function Paser:Fcredit_activity_item(m, c)
  self:Fstring(m, c.activity_name)
  self:Fstring(m, c.production_id)
  self:Finteger(m, c.credit)
  self:Finteger(m, c.credit_privilege)
  self:FArray(m, c.gift_items, "game_item")
  self:Finteger(m, c.count)
  self:Finteger(m, c.end_time)
  self:Finteger(m, c.times)
  self:Fstring(m, c.icon)
  self:Finteger(m, c.top)
  self:Fstring(m, c.origin_price)
  self:Finteger(m, c.discount_rate)
  self:Fstring(m, c.background1)
  self:Fstring(m, c.background2)
  self:Fword_font(m, c.word)
end
function Paser:Fpay_list_ack(m, c)
  self:Fboolean(m, c.switch)
  self:FArray(m, c.pay_types, "string")
end
function Paser:Fpay_list2_ack(m, c)
  self:Fshort(m, c.mycard_type)
  self:FArray(m, c.activity_items, "credit_activity_item")
  self:FArray(m, c.productions, "production_info")
end
function Paser:Fsign_item(m, c)
  self:Finteger(m, c.days)
  self:Finteger(m, c.status)
  self:FArray(m, c.rewards, "game_item")
  self:Finteger(m, c.vip)
  self:Finteger(m, c.vip_crit)
end
function Paser:Fsign_activite(m, c)
  self:Finteger(m, c.accumulate)
  self:Finteger(m, c.total)
  self:Finteger(m, c.left_time)
  self:FArray(m, c.rewards, "game_item")
  self:FArray(m, c.item, "sign_item")
  self:Finteger(m, c.status)
  self:FArray(m, c.recommands, "integer")
  self:Fboolean(m, c.share_friends)
  self:Fboolean(m, c.is_first_day)
  self:Finteger(m, c.type)
end
function Paser:Fsign_activity_reward_req(m, c)
  self:Finteger(m, c.day)
end
function Paser:Ftipoff_content(m, c)
  self:Fstring(m, c.sender)
  self:Fstring(m, c.content)
  self:Fshort(m, c.inappropriate)
end
function Paser:Fcreate_tipoff_req(m, c)
  self:Fstring(m, c.wrongdoer)
  self:Fstring(m, c.reason)
  self:FArray(m, c.evidences, "tipoff_content")
end
function Paser:Fserver_kick(m, c)
  self:Finteger(m, c.action)
end
function Paser:Funlock_item(m, c)
  self:Finteger(m, c.type)
  self:Fstring(m, c.content)
end
function Paser:Flevel_up_ntf(m, c)
  self:FArray(m, c.unlockes, "unlock_item")
end
function Paser:Fprice_request(m, c)
  self:Fstring(m, c.price_type)
  self:Finteger(m, c.type)
end
function Paser:Fprice_response(m, c)
  self:Fstring(m, c.price_type)
  self:Finteger(m, c.price)
end
function Paser:Fspecial_battle_ack(m, c)
  self:Ffight_report(m, c.report)
end
function Paser:Fbattle_money_collect_req(m, c)
  self:Finteger(m, c.money)
end
function Paser:Fquest_loot_req(m, c)
  self:Finteger(m, c.quest_id)
end
function Paser:Fevent_loot_ack(m, c)
  self:FArray(m, c.loot, "game_item")
  self:Finteger(m, c.event_type)
end
function Paser:Fbattle_event_status_ack(m, c)
  self:FArray(m, c.loot, "game_item")
  self:Finteger(m, c.event_type)
end
function Paser:Fadv_chapter(m, c)
  self:Finteger(m, c.chapter_id)
end
function Paser:Fadv_battle(m, c)
  self:Finteger(m, c.battle_id)
end
function Paser:Floud_cast(m, c)
  self:Fstring(m, c.name)
  self:Finteger(m, c.action)
  self:Finteger(m, c.value)
  self:Fstring(m, c.valuestr)
  self:Fboolean(m, c.show_in_chat)
  self:Fboolean(m, c.show_in_panel)
  self:Fstring(m, c.local_msg)
end
function Paser:Fmine_refresh_req(m, c)
  self:Fshort(m, c.refresh_type)
end
function Paser:Fmine_digg_req(m, c)
  self:Fshort(m, c.idx)
end
function Paser:Fmine_atk_req(m, c)
  self:Fstring(m, c.user_id)
end
function Paser:Fminer_info(m, c)
  self:Fstring(m, c.user_id)
  self:Fstring(m, c.name)
  self:Fstring(m, c.alliance)
  self:Fshort(m, c.level)
  self:Finteger(m, c.force_interval)
  self:Finteger(m, c.atk_cd)
  self:Finteger(m, c.icon)
  self:Fshort(m, c.mine_level)
  self:Fshort(m, c.robbed_cnt)
  self:Fshort(m, c.robbed_max)
  self:Finteger(m, c.start_time)
  self:Finteger(m, c.end_time)
  self:FArray(m, c.award, "game_item")
  self:FArray(m, c.atk_award, "game_item")
  self:Finteger(m, c.main_fleet_level)
end
function Paser:Fmine_list_ntf(m, c)
  self:FArray(m, c.miners, "miner_info")
  self:FArray(m, c.revenge_miners, "miner_info")
end
function Paser:Fmine_pool_info(m, c)
  self:Fshort(m, c.level)
  self:Finteger(m, c.time)
  self:FArray(m, c.awards, "game_item")
end
function Paser:Fmine_refresh_ntf(m, c)
  self:Fshort(m, c.mine_area)
  self:Fmine_pool_info(m, c.pool1)
  self:Fmine_pool_info(m, c.pool2)
  self:Fmine_pool_info(m, c.pool3)
  self:Fmine_pool_info(m, c.pool4)
end
function Paser:Fmine_info_ntf(m, c)
  self:Fshort(m, c.digg_cnt)
  self:Finteger(m, c.atk_max_resource)
  self:Finteger(m, c.atk_now_resource)
  self:Fshort(m, c.boost_cnt)
  self:Fshort(m, c.boost_max_cnt)
  self:Finteger(m, c.boost_rest_time)
  self:Finteger(m, c.atk_cost)
  self:Fmine_refresh_ntf(m, c.mine_pool)
  self:FArray(m, c.self_info, "miner_info")
end
function Paser:Fmine_complete_ntf(m, c)
  self:FArray(m, c.basic_award, "game_item")
  self:Finteger(m, c.extra_techniques)
  self:Finteger(m, c.extra_credit)
  self:Finteger(m, c.extra_item)
end
function Paser:Fmine_atk_ntf(m, c)
  self:Fstring(m, c.atk_id)
  self:Fstring(m, c.def_id)
  self:Fshort(m, c.result)
  self:Fminer_info(m, c.def_info)
  self:Fshort(m, c.mine_level)
  self:FArray(m, c.atk_award, "game_item")
  self:FArray(m, c.reports, "fight_report")
end
function Paser:Fpve_supply_ack(m, c)
  self:Finteger(m, c.x_supply)
end
function Paser:Fguide_progress(m, c)
  self:Fstring(m, c.progress)
end
function Paser:Fsupply_loot_ack(m, c)
  self:Fboolean(m, c.enable)
end
function Paser:Fsupply_loot_ntf(m, c)
  self:Finteger(m, c.money)
  self:Finteger(m, c.technique)
  self:Finteger(m, c.laba_supply)
  self:Finteger(m, c.battle_supply)
end
function Paser:Flaba_info(m, c)
  self:Fshort(m, c.pos1)
  self:Fshort(m, c.pos2)
  self:Fshort(m, c.pos3)
  self:Fshort(m, c.rate)
  self:FArray(m, c.awards, "game_item")
end
function Paser:Fplayer_center_ntf(m, c)
  self:Finteger(m, c.timepoint)
  self:Finteger(m, c.ntf_type)
  self:FArray(m, c.ntf_content, "string")
end
function Paser:Fdexter_lib_info(m, c)
  self:Fstring(m, c.dexter_name)
  self:Fshort(m, c.used_cnt)
  self:Finteger(m, c.count_down)
end
function Paser:Fdexter_libs_ntf(m, c)
  self:FArray(m, c.libs, "dexter_lib_info")
end
function Paser:Fchat_error_ntf(m, c)
  self:Finteger(m, c.code)
  self:Fstring(m, c.content)
  self:Fstring(m, c.receiver)
  self:Fstring(m, c.channel)
end
function Paser:Fplayer_force(m, c)
  self:Fstring(m, c.user_id)
  self:Finteger(m, c.level)
  self:Fstring(m, c.name)
  self:Fdouble(m, c.force)
end
function Paser:Ftop_force_list_ntf(m, c)
  self:FArray(m, c.players, "player_force")
end
function Paser:Fclient_ver(m, c)
  self:Fstring(m, c.market)
  self:Finteger(m, c.version)
end
function Paser:Frefresh_time_ntf(m, c)
  self:Fboolean(m, c.flag)
  self:Finteger(m, c.next_time)
  self:Finteger(m, c.wd_next_time)
end
function Paser:Fac_ntf(m, c)
  self:Fshort(m, c.ac_level)
  self:Fshort(m, c.ac_step)
  self:Finteger(m, c.ac_supply)
  self:Finteger(m, c.ac_energy)
  self:Finteger(m, c.ac_energy_level)
  self:Fshort(m, c.jam)
  self:Finteger(m, c.refresh_time)
  self:Finteger(m, c.jam_cost)
  self:Fshort(m, c.can_sweep)
end
function Paser:Fac_energy_charge_ack(m, c)
  self:Finteger(m, c.increase_energy)
  self:Fshort(m, c.strike)
end
function Paser:Fgateway_info_ntf(m, c)
  self:FArray(m, c.notices, "integer")
  self:FArray(m, c.activities, "integer")
end
function Paser:Fnotice_info(m, c)
  self:Finteger(m, c.type)
  self:Finteger(m, c.id)
end
function Paser:Fblock_devil_info_req(m, c)
  self:Finteger(m, c.id)
end
function Paser:Fgateway_info(m, c)
  self:FArray(m, c.notices, "integer")
  self:FArray(m, c.activities, "integer")
end
function Paser:Fblock_devil_player(m, c)
  self:Fstring(m, c.name)
  self:Finteger(m, c.state)
end
function Paser:Fblock_devil_req(m, c)
  self:Finteger(m, c.type)
end
function Paser:Fblock_devil_ntf(m, c)
  self:FArray(m, c.players, "block_devil_player")
  self:FArray(m, c.award, "game_item")
  self:Finteger(m, c.prestige)
end
function Paser:Fblock_devil_complete_ntf(m, c)
  self:Finteger(m, c.code)
  self:FArray(m, c.award, "game_item")
end
function Paser:Fdonate_req(m, c)
  self:Finteger(m, c.donate_type)
end
function Paser:Fdonate_ack(m, c)
  self:Finteger(m, c.donate_type)
  self:FArray(m, c.donate_package, "donate_package_info")
  self:Finteger(m, c.alliance_donate_level)
  self:Finteger(m, c.alliance_donate_exp)
  self:Finteger(m, c.alliance_donate_lv_exp)
  self:FArray(m, c.donate_record, "alliance_log_info")
  self:Finteger(m, c.alliance_donate_exp_add)
end
function Paser:Fdonate_info_ack(m, c)
  self:Finteger(m, c.donate_type)
  self:FArray(m, c.donate_package, "donate_package_info")
  self:Finteger(m, c.alliance_donate_level)
  self:Finteger(m, c.alliance_donate_exp)
  self:Finteger(m, c.alliance_donate_lv_exp)
  self:FArray(m, c.donate_record, "alliance_log_info")
end
function Paser:Fdonate_package_info(m, c)
  self:Finteger(m, c.exp)
  self:Finteger(m, c.prestige)
  self:Fstring(m, c.cost_type)
  self:Finteger(m, c.cost_value)
  self:Finteger(m, c.vip_level)
  self:Finteger(m, c.brick)
end
function Paser:Falliance_weekend_award_ack(m, c)
  self:Finteger(m, c.money)
  self:Finteger(m, c.prestige)
end
function Paser:Falliance_defence_info(m, c)
  self:Finteger(m, c.leader_id)
  self:Fshort(m, c.leader_hp)
  self:Finteger(m, c.leader_hp_limit)
  self:Fstring(m, c.leader_name)
  self:Finteger(m, c.hp)
  self:Finteger(m, c.hp_limit)
  self:Fshort(m, c.level)
  self:Finteger(m, c.brick)
  self:Finteger(m, c.point)
end
function Paser:Falliance_defence_ntf(m, c)
  self:Falliance_defence_info(m, c.info)
end
function Paser:Falliance_domination_defence_info(m, c)
  self:Finteger(m, c.left_seconds)
  self:Finteger(m, c.leader_id)
  self:Fstring(m, c.leader_name)
  self:Finteger(m, c.leader_hp)
  self:Finteger(m, c.leader_hp_limit)
  self:Finteger(m, c.hp)
  self:Finteger(m, c.hp_limit)
  self:Fshort(m, c.level)
end
function Paser:Falliance_domination_defence_ntf(m, c)
  self:Falliance_domination_defence_info(m, c.info)
end
function Paser:Falliance_set_defender_req(m, c)
  self:Finteger(m, c.user_id)
end
function Paser:Falliance_defence_donate_req(m, c)
  self:Finteger(m, c.brick)
end
function Paser:Fdomination_battle_info(m, c)
  self:Finteger(m, c.user_id)
  self:Fboolean(m, c.is_defender)
  self:Fshort(m, c.win_count)
  self:Finteger(m, c.point)
  self:Fshort(m, c.lose_count)
  self:Finteger(m, c.attk_count)
end
function Paser:Falliance_domination_battle_ntf(m, c)
  self:FArray(m, c.users, "domination_battle_info")
end
function Paser:Falliance_domination_info(m, c)
  self:Finteger(m, c.id)
  self:Fstring(m, c.name)
  self:Fstring(m, c.server_name)
  self:Finteger(m, c.level)
  self:Fshort(m, c.flag)
  self:FArray(m, c.members, "alliance_member_info")
end
function Paser:Falliance_domination_ntf(m, c)
  self:Fshort(m, c.status)
  self:Finteger(m, c.cost_seconds)
  self:FArray(m, c.infoes, "alliance_domination_info")
end
function Paser:Fdomination_challenge_req(m, c)
  self:Finteger(m, c.id)
  self:Fboolean(m, c.once)
  self:Fboolean(m, c.leap)
end
function Paser:Fdomination_join_req(m, c)
  self:Fboolean(m, c.is_local_matched)
end
function Paser:Fdomination_challenge_ack(m, c)
  self:Fshort(m, c.result)
  self:Finteger(m, c.id)
  self:FArray(m, c.reports, "fight_report")
  self:FArray(m, c.rewards, "game_item")
  self:FArray(m, c.def_rewards, "game_item")
end
function Paser:Flogin_first_ntf(m, c)
  self:Fboolean(m, c.enable)
end
function Paser:Flogin_time_current_ntf(m, c)
  self:Finteger(m, c.login_time_current)
  self:Finteger(m, c.login_time_current_utc)
  self:Finteger(m, c.google_trans_tag)
  self:Finteger(m, c.mail_trans_tag)
end
function Paser:Fchampion_reward_info(m, c)
  self:Finteger(m, c.money)
  self:Finteger(m, c.prestige)
  self:Finteger(m, c.credit)
end
function Paser:Fchampion_rank_reward_ack(m, c)
  self:Fchampion_reward_info(m, c.last_loot)
  self:Finteger(m, c.last_rank)
  self:FArray(m, c.champion_reward, "champion_reward_info")
end
function Paser:Fworld_boss_info_ntf(m, c)
  self:Finteger(m, c.id)
  self:Finteger(m, c.level)
  self:Fdouble(m, c.hp)
  self:Fdouble(m, c.total_hp)
  self:Finteger(m, c.motility)
  self:Finteger(m, c.intercept)
  self:Finteger(m, c.cd)
  self:Finteger(m, c.clean_cd_cost)
  self:Finteger(m, c.end_time)
  self:FArray(m, c.toplist, "world_boss_toplist_player")
  self:Fstring(m, c.kill_1st)
  self:Fstring(m, c.kill_2nd)
  self:Fstring(m, c.kill_3rd)
  self:Fstring(m, c.final_kill)
end
function Paser:Fworld_boss_toplist_player(m, c)
  self:Finteger(m, c.top)
  self:Finteger(m, c.player_id)
  self:Fstring(m, c.player_name)
  self:Fdouble(m, c.destroy_hp)
end
function Paser:Fchallenge_world_boss_ack(m, c)
  self:Ffight_report(m, c.report)
  self:FArray(m, c.award, "game_item")
end
function Paser:Fadd_world_boss_force_rate_ack(m, c)
  self:Finteger(m, c.rate)
  self:Finteger(m, c.cost)
  self:Fboolean(m, c.success)
end
function Paser:Fready_world_boss_req(m, c)
  self:Fboolean(m, c.can_interrupt)
end
function Paser:Fready_world_boss_ack(m, c)
  self:Finteger(m, c.countdown)
  self:Finteger(m, c.force_rate)
  self:Finteger(m, c.force_rate_cost)
  self:Finteger(m, c.current_level)
end
function Paser:Ffirst_purchase(m, c)
  self:FArray(m, c.items, "game_item")
  self:Finteger(m, c.status)
  self:Fdouble(m, c.cost)
end
function Paser:Flevel_loot(m, c)
  self:Finteger(m, c.status)
  self:Finteger(m, c.level)
  self:FArray(m, c.loots, "game_item")
end
function Paser:Flevel_loots(m, c)
  self:FArray(m, c.levels, "level_loot")
  self:Finteger(m, c.level)
  self:FArray(m, c.kryptons, "krypton_info")
end
function Paser:Factive_gifts_ntf(m, c)
  self:FArray(m, c.items, "active_gifts_info")
end
function Paser:Factive_gifts_info(m, c)
  self:Finteger(m, c.gift_type)
  self:Finteger(m, c.mark)
  self:Finteger(m, c.times)
  self:FArray(m, c.gifts, "game_item")
end
function Paser:Factive_gifts_req(m, c)
  self:Finteger(m, c.type)
end
function Paser:Flevel_loot_req(m, c)
  self:Finteger(m, c.level)
end
function Paser:Fpromotion_info(m, c)
  self:Fstring(m, c.id)
  self:Fboolean(m, c.enabled)
  self:Finteger(m, c.left_seconds)
  self:Fshort(m, c.tab)
  self:FArray(m, c.news, "promotion_news_info")
end
function Paser:Fpromotions_ntf(m, c)
  self:FArray(m, c.promotions, "promotion_info")
end
function Paser:Fpromotion_award_req(m, c)
  self:Fstring(m, c.id)
  self:Finteger(m, c.condition)
end
function Paser:Fpromotion_award_ack(m, c)
  self:FArray(m, c.awards, "game_item")
end
function Paser:Fpromotion_detail_req(m, c)
  self:Fstring(m, c.id)
  self:Fshort(m, c.index)
end
function Paser:Fpromotion_section_info(m, c)
  self:FArray(m, c.awards, "game_item")
  self:Fshort(m, c.status)
  self:Finteger(m, c.condition)
end
function Paser:Fpromotion_detail_ack(m, c)
  self:Finteger(m, c.value)
  self:FArray(m, c.sections, "promotion_section_info")
end
function Paser:Fallstars_reward_req(m, c)
  self:Finteger(m, c.charpter_id)
end
function Paser:Fallstars_reward_ack(m, c)
  self:FArray(m, c.rewards, "game_item")
end
function Paser:Fpromotion_news_info(m, c)
  self:Fstring(m, c.id)
  self:Finteger(m, c.news_id)
  self:Fboolean(m, c.can_show)
  self:Finteger(m, c.left_seconds)
end
function Paser:Ftimelimit_hero_id_ack(m, c)
  self:FArray(m, c.fleet_id, "fleet_sale_with_rebate_info")
end
function Paser:Ffleet_sale_with_rebate_info(m, c)
  self:Finteger(m, c.fleet_id)
  self:Finteger(m, c.deposit_rate)
  self:Finteger(m, c.lepton_req)
  self:Finteger(m, c.quark_req)
  self:Finteger(m, c.left_time)
end
function Paser:Ffleet_sale_with_rebate_list_ack(m, c)
  self:FArray(m, c.fleet_info_list, "fleet_sale_with_rebate_info")
end
function Paser:Fcut_off_list_ack(m, c)
  self:FArray(m, c.cutoff_list, "cut_off_card")
end
function Paser:Fcut_off_card(m, c)
  self:Finteger(m, c.id)
  self:Finteger(m, c.credit)
  self:Finteger(m, c.credit_eachday)
end
function Paser:Fbuy_cut_off_req(m, c)
  self:Finteger(m, c.id)
end
function Paser:Fpromotion_award_ntf(m, c)
  self:FArray(m, c.news_ids, "integer")
end
function Paser:Fnew_activities_compare_req(m, c)
  self:FArray(m, c.notices, "integer")
  self:FArray(m, c.activities, "integer")
end
function Paser:Factivities_button_status_ntf(m, c)
  self:Fboolean(m, c.status)
end
function Paser:Fkrypton_fragment_core_ntf(m, c)
  self:Finteger(m, c.code)
end
function Paser:Factivities_button_first_status_req(m, c)
  self:FArray(m, c.activities, "integer")
end
function Paser:Fstores_req(m, c)
  self:Finteger(m, c.store_id)
  self:Fstring(m, c.locale)
end
function Paser:Fstore_item(m, c)
  self:Finteger(m, c.id)
  self:Fgame_item(m, c.item)
  self:Finteger(m, c.price)
  self:Finteger(m, c.price_type)
  self:Finteger(m, c.discount)
  self:Finteger(m, c.time_left)
  self:Finteger(m, c.day_buy_left)
  self:Finteger(m, c.all_buy_left)
  self:Finteger(m, c.level_limit)
  self:Finteger(m, c.vip_limit)
  self:Finteger(m, c.state)
  self:Finteger(m, c.number)
  self:Finteger(m, c.max_number)
  self:Finteger(m, c.status)
  self:Finteger(m, c.quick_buy)
  self:Finteger(m, c.buy_quantity)
  self:Finteger(m, c.refresh_buy_limit)
  self:FArray(m, c.must_items, "game_item")
  self:FArray(m, c.maybe_items, "game_item")
  self:Finteger(m, c.order)
  self:Finteger(m, c.group)
  self:FArray(m, c.pics, "string")
end
function Paser:Fmedal_store_item(m, c)
  self:Finteger(m, c.id)
  self:Fgame_item(m, c.item)
  self:Fgame_item(m, c.price)
  self:Finteger(m, c.discount)
  self:Finteger(m, c.buy_times_limit)
  self:Fshort(m, c.quality)
  self:Finteger(m, c.time_left)
end
function Paser:Fstore_quick_buy_count_ack(m, c)
  self:Finteger(m, c.count)
  self:Finteger(m, c.max_count)
  self:Fstring(m, c.nor_price)
  self:Fstring(m, c.dis_price)
end
function Paser:Fbuy_count_info(m, c)
  self:Finteger(m, c.id)
  self:Fstore_quick_buy_count_ack(m, c.count_ack)
end
function Paser:Fstores_ack(m, c)
  self:Finteger(m, c.time)
  self:FArray(m, c.items, "store_item")
  self:Finteger(m, c.currency)
  self:Fstring(m, c.normal)
  self:Fboolean(m, c.mysterystore)
  self:FArray(m, c.buy_count_infos, "buy_count_info")
  self:Finteger(m, c.price_off_status)
end
function Paser:Fstores_refresh_ack(m, c)
  self:Finteger(m, c.time)
  self:FArray(m, c.items, "store_item")
end
function Paser:Fexpedition_stores_refresh_ack(m, c)
  self:Finteger(m, c.time)
  self:FArray(m, c.items, "store_item")
end
function Paser:Fstores_buy_req(m, c)
  self:Finteger(m, c.store_id)
  self:Finteger(m, c.id)
  self:Finteger(m, c.buy_times)
  self:Finteger(m, c.state)
  self:Finteger(m, c.coupon)
end
function Paser:Fequipment_action_info(m, c)
  self:Fshort(m, c.can_evolute)
  self:Fshort(m, c.can_enhance)
  self:Fshort(m, c.can_enchant)
end
function Paser:Fequipment_action_list_ack(m, c)
  self:FArray(m, c.equipments, "equipment_action_info")
end
function Paser:Fequipment_action_list_req(m, c)
  self:FArray(m, c.ids, "integer")
end
function Paser:Fmail_to_alliance_req(m, c)
  self:Fstring(m, c.title)
  self:Fstring(m, c.content)
  self:Fshort(m, c.my_send)
  self:Fshort(m, c.receiver_type)
end
function Paser:Fmail_to_all_req(m, c)
  self:Fstring(m, c.title)
  self:Fstring(m, c.content)
end
function Paser:Fthanksgiven_box_player(m, c)
  self:Fstring(m, c.user_id)
  self:Fstring(m, c.name)
  self:Finteger(m, c.box_cnt)
  self:Finteger(m, c.rank)
  self:Fstring(m, c.alliance_name)
end
function Paser:Fthanksgiven_box_alliance(m, c)
  self:Falliance_info(m, c.alliance)
  self:Finteger(m, c.box_cnt)
  self:Finteger(m, c.rank)
end
function Paser:Fthanksgiven_box_rank(m, c)
  self:FArray(m, c.players, "thanksgiven_box_player")
  self:FArray(m, c.alliances, "thanksgiven_box_alliance")
  self:Fthanksgiven_box_player(m, c.self_info)
end
function Paser:Ffestival_item(m, c)
  self:Finteger(m, c.activity_type)
  self:Fstring(m, c.name)
  self:Finteger(m, c.start_time)
  self:Finteger(m, c.end_time)
  self:Finteger(m, c.current_time)
  self:Finteger(m, c.final_time)
  self:Fshort(m, c.level_req)
end
function Paser:Ffestival_info(m, c)
  self:FArray(m, c.festivals, "festival_item")
end
function Paser:Fverify_redeem_code_req(m, c)
  self:Fstring(m, c.code)
end
function Paser:Fverify_redeem_code_ack(m, c)
  self:FArray(m, c.awards, "game_item")
end
function Paser:Fitem_count(m, c)
  self:Finteger(m, c.item_id)
  self:Finteger(m, c.item_no)
end
function Paser:Fitems_count_ntf(m, c)
  self:FArray(m, c.items, "item_count")
end
function Paser:Fthanksgiven_champion_ntf(m, c)
  self:Fstring(m, c.user_id)
end
function Paser:Fitem_use_award_ntf(m, c)
  self:FArray(m, c.award_list, "game_item_info")
  self:Finteger(m, c.use_max)
end
function Paser:Fvip_sign_item_use_award_ntf(m, c)
  self:FArray(m, c.award_list, "game_item_info")
  self:Finteger(m, c.use_max)
end
function Paser:Faddition_info(m, c)
  self:Fstring(m, c.locale)
end
function Paser:Fcolony_product_info(m, c)
  self:Finteger(m, c.speed)
  self:Finteger(m, c.producted_experience)
  self:Finteger(m, c.experience)
  self:Finteger(m, c.max_experience)
end
function Paser:Fcolony_user_info(m, c)
  self:Fstring(m, c.player_id)
  self:Fstring(m, c.name)
  self:Fshort(m, c.position)
  self:Fstring(m, c.owner_id)
  self:Finteger(m, c.sex)
  self:Finteger(m, c.icon)
  self:Finteger(m, c.forward_left)
  self:Finteger(m, c.reverse_left)
  self:Fcolony_product_info(m, c.product)
  self:Fstring(m, c.alliance_name)
  self:Fdouble(m, c.force)
  self:Fshort(m, c.level)
  self:Finteger(m, c.main_fleet_level)
end
function Paser:Fcolony_info(m, c)
  self:Fshort(m, c.position)
  self:FArray(m, c.users, "colony_user_info")
end
function Paser:Fcolony_info_ntf(m, c)
  self:Fcolony_info(m, c.info)
end
function Paser:Fcolony_exp(m, c)
  self:Finteger(m, c.experience)
  self:Finteger(m, c.producted_experience)
  self:Finteger(m, c.max_experience)
end
function Paser:Fcolony_exp_ntf(m, c)
  self:Fcolony_exp(m, c.ntf)
end
function Paser:Fcolony_times_info(m, c)
  self:Fshort(m, c.type)
  self:Fshort(m, c.times)
  self:Fshort(m, c.max_times)
end
function Paser:Fcolony_times_ntf(m, c)
  self:FArray(m, c.times, "colony_times_info")
end
function Paser:Fcolony_log_info(m, c)
  self:Finteger(m, c.time)
  self:Fshort(m, c.type)
  self:Fshort(m, c.catalog)
  self:Finteger(m, c.master_id)
  self:Fstring(m, c.master_name)
  self:Finteger(m, c.slave_id)
  self:Fstring(m, c.slave_name)
  self:Finteger(m, c.third_id)
  self:Fstring(m, c.third_name)
  self:FArray(m, c.params, "string")
end
function Paser:Fcolony_info_test_ack(m, c)
  self:Fcolony_info(m, c.info)
  self:Fcolony_exp(m, c.exp)
  self:Fcolony_product_info(m, c.product)
  self:FArray(m, c.logs, "colony_log_info")
end
function Paser:Fcolony_logs_ntf(m, c)
  self:Finteger(m, c.type)
  self:FArray(m, c.logs, "colony_log_info")
end
function Paser:Fcolony_users_req(m, c)
  self:FArray(m, c.types, "integer")
end
function Paser:Fcolony_users(m, c)
  self:Fstring(m, c.tag)
  self:FArray(m, c.users, "colony_user_info")
end
function Paser:Fcolony_players_req(m, c)
  self:FArray(m, c.user_ids, "integer")
end
function Paser:Fcolony_players_ack(m, c)
  self:FArray(m, c.users, "colony_user_info")
end
function Paser:Fcolony_users_ack(m, c)
  self:FArray(m, c.users_list, "colony_users")
end
function Paser:Fcolony_challenge_req(m, c)
  self:Fshort(m, c.fight_type)
  self:Fstring(m, c.def_id)
  self:Fshort(m, c.def_pos)
  self:Fstring(m, c.third_id)
end
function Paser:Fcolony_challenge_ack(m, c)
  self:Fshort(m, c.result)
  self:Ffight_report(m, c.report)
end
function Paser:Fcolony_exploit_req(m, c)
  self:Fstring(m, c.slave_id)
  self:Fboolean(m, c.pay_enabled)
end
function Paser:Fcolony_exploit_ack(m, c)
  self:Finteger(m, c.experience)
  self:Finteger(m, c.paid_experience)
end
function Paser:Fcolony_release_req(m, c)
  self:Fstring(m, c.slave_id)
end
function Paser:Fcolony_release_ack(m, c)
  self:Finteger(m, c.experience)
end
function Paser:Fcolony_fawn_req(m, c)
  self:Fshort(m, c.action_id)
  self:Fstring(m, c.receiver_id)
  self:Fshort(m, c.receiver_pos)
end
function Paser:Fcolony_fawn_ack(m, c)
  self:Finteger(m, c.experience)
  self:Finteger(m, c.expect_experience)
end
function Paser:Factivity_stores_req(m, c)
  self:Finteger(m, c.store_type)
  self:Fstring(m, c.locale)
end
function Paser:Fgame_item_id(m, c)
  self:Finteger(m, c.item_id)
  self:Finteger(m, c.item_no)
  self:Finteger(m, c.has_num)
  self:Fboolean(m, c.is_jump2store)
end
function Paser:Factivity_store_item(m, c)
  self:Finteger(m, c.id)
  self:Fstring(m, c.item_type)
  self:Finteger(m, c.item_id)
  self:Finteger(m, c.item_no)
  self:Finteger(m, c.sp_type)
  self:Finteger(m, c.fleet_id)
  self:Finteger(m, c.fleet_level)
  self:FArray(m, c.items, "game_item_id")
  self:Finteger(m, c.quick_buy)
  self:Finteger(m, c.buy_quantity)
  self:Finteger(m, c.day_buy_limit)
  self:Finteger(m, c.day_buy_left)
  self:Finteger(m, c.all_buy_limit)
  self:Finteger(m, c.all_buy_left)
end
function Paser:Factivity_stores_ack(m, c)
  self:FArray(m, c.items, "activity_store_item")
end
function Paser:Factivity_stores_buy_req(m, c)
  self:Finteger(m, c.store_type)
  self:Finteger(m, c.id)
  self:Finteger(m, c.buy_times)
end
function Paser:Fyys(m, c)
  self:Finteger(m, c.key)
  self:Finteger(m, c.value)
end
function Paser:Fyys_dict(m, c)
  self:FArray(m, c.config, "yys")
end
function Paser:Fcolony_action_purchase_req(m, c)
  self:Finteger(m, c.action_type)
end
function Paser:Fprestige_get_req(m, c)
  self:Finteger(m, c.prestige_level)
end
function Paser:Falliance_resource_info(m, c)
  self:Finteger(m, c.money)
  self:Finteger(m, c.brick)
  self:Finteger(m, c.inventory)
  self:Finteger(m, c.point)
end
function Paser:Falliance_notifies_req(m, c)
  self:FArray(m, c.notifies, "string")
end
function Paser:Falliance_resource_ntf(m, c)
  self:Falliance_resource_info(m, c.resource)
end
function Paser:Ffleet_info_req(m, c)
  self:Finteger(m, c.fleet_id)
  self:Finteger(m, c.level)
  self:Finteger(m, c.type)
  self:Finteger(m, c.req_type)
end
function Paser:Ffleet_info_ack(m, c)
  self:Finteger(m, c.fleet_id)
  self:Ffleets_info(m, c.fleet_info)
end
function Paser:Ffair_fleet_info_ack(m, c)
  self:Finteger(m, c.fleet_id)
  self:Fshort(m, c.fleet_level)
  self:Ffleets_info(m, c.fleet_info)
end
function Paser:Fcommon_ntf(m, c)
  self:Finteger(m, c.code)
end
function Paser:Fkrypton_info_req(m, c)
  self:Finteger(m, c.krypton_id)
  self:Finteger(m, c.level)
end
function Paser:Fkrypton_info_ack(m, c)
  self:Fkrypton_info(m, c.krypton)
  self:Finteger(m, c.level)
end
function Paser:Fwarn_info(m, c)
  self:Fstring(m, c.device_name)
  self:Fstring(m, c.device_real_name)
  self:Fstring(m, c.udid)
  self:Finteger(m, c.player_id)
  self:Finteger(m, c.level)
  self:Fstring(m, c.info_type)
  self:Fstring(m, c.info_code)
end
function Paser:Fitems_req(m, c)
  self:FArray(m, c.ids, "integer")
end
function Paser:Fitem_info(m, c)
  self:Finteger(m, c.id)
  self:Finteger(m, c.price)
  self:Finteger(m, c.level_req)
  self:Finteger(m, c.sp_type)
  self:Finteger(m, c.use_max)
  self:Fboolean(m, c.use_more)
  self:Finteger(m, c.max_level)
end
function Paser:Fitems_ack(m, c)
  self:FArray(m, c.items, "item_info")
end
function Paser:Fclient_device_info(m, c)
  self:Fstring(m, c.device_full_name)
  self:Fstring(m, c.device_real_name)
  self:Fstring(m, c.gpu_renderer)
  self:Fstring(m, c.gpu_vendor)
  self:Fstring(m, c.udid)
  self:Finteger(m, c.cpu_hz)
  self:Finteger(m, c.max_memory)
end
function Paser:Fclient_device_ack(m, c)
  self:Fboolean(m, c.preload_battle)
  self:Fboolean(m, c.preload_battle_bg)
  self:Fboolean(m, c.use_complex_bg)
  self:Fboolean(m, c.use_complex_battle_effect)
end
function Paser:Faward_receive_req(m, c)
  self:Finteger(m, c.id)
end
function Paser:Fclient_fps_info(m, c)
  self:Fstring(m, c.name)
  self:Fstring(m, c.value)
  self:Fstring(m, c.udid)
  self:Finteger(m, c.user_id)
  self:Fstring(m, c.created_at)
  self:Fstring(m, c.ip)
  self:Fstring(m, c.device_type)
  self:Finteger(m, c.fps)
end
function Paser:Ftaobao_trade_req(m, c)
  self:Fstring(m, c.trade_no)
end
function Paser:Fdomination_rank_award_info(m, c)
  self:Finteger(m, c.rank)
  self:Finteger(m, c.point)
  self:FArray(m, c.award, "game_item")
end
function Paser:Fdomination_awards_list_ack(m, c)
  self:Finteger(m, c.alliance_id)
  self:Fstring(m, c.alliance_name)
  self:FArray(m, c.awards, "domination_rank_award_info")
end
function Paser:Fdomination_ranks_req(m, c)
  self:Finteger(m, c.rank_type)
end
function Paser:Fdomination_rank_info(m, c)
  self:Finteger(m, c.id)
  self:Fstring(m, c.name)
  self:Fshort(m, c.flag)
  self:Fstring(m, c.alliance_name)
  self:Finteger(m, c.rank)
  self:Finteger(m, c.point)
  self:Finteger(m, c.server)
end
function Paser:Fdomination_ranks_ack(m, c)
  self:Finteger(m, c.total)
  self:FArray(m, c.ranks, "domination_rank_info")
  self:FArray(m, c.params, "integer")
end
function Paser:Fwd_star_req(m, c)
  self:Finteger(m, c.id)
end
function Paser:Fwd_star(m, c)
  self:Finteger(m, c.id)
  self:Fstring(m, c.fight_name)
  self:Finteger(m, c.left_time)
  self:Fstring(m, c.extra)
  self:Finteger(m, c.alliance_id)
  self:Fstring(m, c.alliance_name)
  self:Fstring(m, c.alliance_leader)
  self:Finteger(m, c.flag)
end
function Paser:Fall_wd_star(m, c)
  self:FArray(m, c.infoes, "wd_star")
end
function Paser:Fwd_award_req(m, c)
  self:Finteger(m, c.flag)
end
function Paser:Fwd_award(m, c)
  self:Finteger(m, c.id)
  self:Finteger(m, c.begin_rank)
  self:Finteger(m, c.end_rank)
  self:FArray(m, c.awards, "game_item")
end
function Paser:Fwd_award_lists(m, c)
  self:FArray(m, c.infoes, "wd_award")
end
function Paser:Falliance_domination_total_rank_ntf(m, c)
  self:Finteger(m, c.player_point)
  self:Finteger(m, c.alliance_point)
end
function Paser:Fwd_last_champion(m, c)
  self:Finteger(m, c.star_id)
  self:Finteger(m, c.alliance_id)
  self:Fstring(m, c.alliance_name)
  self:Fstring(m, c.alliance_leader)
  self:Finteger(m, c.flag)
  self:Fstring(m, c.server_name)
end
function Paser:Fdevice_info_req(m, c)
  self:Fstring(m, c.endpoint_token)
  self:Fstring(m, c.device_lang)
  self:Fstring(m, c.endpoint_type)
  self:Fstring(m, c.registration_id)
end
function Paser:Fwd_ranking_box_player(m, c)
  self:Fstring(m, c.user_id)
  self:Fstring(m, c.name)
  self:Finteger(m, c.box_cnt)
  self:Finteger(m, c.rank)
  self:Fstring(m, c.alliance_name)
end
function Paser:Fwd_ranking_box_alliance(m, c)
  self:Falliance_info(m, c.alliance)
  self:Finteger(m, c.box_cnt)
  self:Finteger(m, c.rank)
end
function Paser:Fwd_ranking_box_rank(m, c)
  self:FArray(m, c.players, "wd_ranking_box_player")
  self:FArray(m, c.alliances, "wd_ranking_box_alliance")
  self:Fwd_ranking_box_player(m, c.self_info)
end
function Paser:Fwd_ranking_champion_ntf(m, c)
  self:Fstring(m, c.user_id)
end
function Paser:Fopen_box_open_state(m, c)
  self:Finteger(m, c.id)
  self:Finteger(m, c.type_id)
end
function Paser:Fopen_box_reward_state(m, c)
  self:Finteger(m, c.id)
  self:Fgame_item(m, c.item)
  self:Fgame_item(m, c.show_item)
  self:Finteger(m, c.cur_num)
  self:Finteger(m, c.total_num)
end
function Paser:Fopen_box_progress(m, c)
  self:Finteger(m, c.step)
  self:FArray(m, c.loots, "game_item")
  self:Finteger(m, c.status)
end
function Paser:Fopen_box_info(m, c)
  self:Finteger(m, c.now_credit_spend)
  self:Fgame_item(m, c.refresh_cost)
  self:FArray(m, c.open_state_list, "open_box_open_state")
  self:FArray(m, c.reward_state_list, "open_box_reward_state")
  self:FArray(m, c.progress_bar, "open_box_progress")
end
function Paser:Fopen_box_open_req(m, c)
  self:Finteger(m, c.box_id)
  self:Finteger(m, c.id)
end
function Paser:Fopen_box_open_ack(m, c)
  self:Finteger(m, c.now_credit_spend)
  self:Fgame_item(m, c.refresh_cost)
  self:Fopen_box_open_state(m, c.open_state)
  self:Fopen_box_reward_state(m, c.reward_state)
  self:FArray(m, c.progress_bar, "open_box_progress")
end
function Paser:Fopen_box_refresh_ack(m, c)
  self:Finteger(m, c.now_credit_spend)
  self:Fgame_item(m, c.refresh_cost)
  self:FArray(m, c.open_state_list, "open_box_open_state")
  self:FArray(m, c.reward_state_list, "open_box_reward_state")
  self:FArray(m, c.progress_bar, "open_box_progress")
end
function Paser:Frecent_state(m, c)
  self:Fstring(m, c.name)
  self:Fgame_item(m, c.item)
  self:Finteger(m, c.times)
end
function Paser:Fopen_box_recent_ntf(m, c)
  self:FArray(m, c.state, "recent_state")
end
function Paser:Fbattle_rate_req(m, c)
  self:Fstring(m, c.cli_data)
end
function Paser:Fbattle_rate_desc(m, c)
  self:Finteger(m, c.equip)
  self:Finteger(m, c.krypton)
  self:Finteger(m, c.technique)
  self:Finteger(m, c.recruit)
  self:Finteger(m, c.power)
  self:Finteger(m, c.art)
  self:Finteger(m, c.medal)
  self:Finteger(m, c.tactical)
  self:Finteger(m, c.remodel)
end
function Paser:Fbattle_rate_ntf(m, c)
  self:Finteger(m, c.battle_type)
  self:Fbattle_rate_desc(m, c.player1_rate)
  self:Fbattle_rate_desc(m, c.player2_rate)
  self:Fstring(m, c.cli_data)
end
function Paser:Fset_game_local_req(m, c)
  self:Fstring(m, c.game_local)
end
function Paser:Fcontention_dot_info(m, c)
  self:Finteger(m, c.id)
  self:Fshort(m, c.dot_type)
  self:FArray(m, c.effection, "integer")
  self:FArray(m, c.buffers, "integer")
end
function Paser:Fcontention_line_info(m, c)
  self:Finteger(m, c.id)
  self:Finteger(m, c.dot_start)
  self:Finteger(m, c.dot_end)
  self:Finteger(m, c.distance)
end
function Paser:Fcontention_base_info(m, c)
  self:Finteger(m, c.server_id)
  self:Finteger(m, c.team_id)
end
function Paser:Fcontention_buffer_item(m, c)
  self:Fstring(m, c.key)
  self:Fdouble(m, c.value)
  self:Finteger(m, c.type_id)
  self:Fdouble(m, c.limit_value)
end
function Paser:Fcontention_buffer_info(m, c)
  self:Fshort(m, c.id)
  self:FArray(m, c.buffs, "contention_buffer_item")
  self:Fstring(m, c.icon)
end
function Paser:Fcontention_map_req(m, c)
  self:Fshort(m, c.version)
end
function Paser:Fcontention_season(m, c)
  self:Finteger(m, c.id)
  self:Fstring(m, c.start_time)
  self:Fstring(m, c.end_time)
end
function Paser:Fcontention_event_time(m, c)
  self:Finteger(m, c.begin_time)
  self:Finteger(m, c.end_time)
end
function Paser:Fcontention_event_data(m, c)
  self:Fstring(m, c.key)
  self:Fstring(m, c.value1)
  self:Fstring(m, c.value2)
  self:Fstring(m, c.value3)
  self:Finteger(m, c.icon)
end
function Paser:Fcontention_event_info(m, c)
  self:Finteger(m, c.id)
  self:Finteger(m, c.type)
  self:FArray(m, c.time_span, "contention_event_time")
  self:FArray(m, c.addition_data, "contention_event_data")
  self:FArray(m, c.params, "integer")
  self:Finteger(m, c.is_show)
end
function Paser:Fcontention_mission_info_req(m, c)
  self:Finteger(m, c.event_id)
  self:FArray(m, c.id_list, "integer")
end
function Paser:Fmission_info(m, c)
  self:Finteger(m, c.id)
  self:Fstring(m, c.type)
  self:Fstring(m, c.action)
  self:Finteger(m, c.num)
  self:Finteger(m, c.max)
  self:Finteger(m, c.step)
  self:FArray(m, c.dot_list, "integer")
  self:FArray(m, c.awards, "game_item")
  self:Finteger(m, c.is_end_award)
end
function Paser:Fcontention_mission_info_ack(m, c)
  self:FArray(m, c.mission_list, "mission_info")
end
function Paser:Fcontention_mission_award_req(m, c)
  self:Finteger(m, c.event_id)
  self:Finteger(m, c.mission_id)
  self:Finteger(m, c.step)
end
function Paser:Fcontention_mybase_info(m, c)
  self:Finteger(m, c.server_id)
  self:Fstring(m, c.user_id)
end
function Paser:Fcontention_map_ack(m, c)
  self:Fshort(m, c.version)
  self:Fcontention_season(m, c.season)
  self:FArray(m, c.all_season, "contention_season")
  self:Finteger(m, c.begin_time)
  self:Finteger(m, c.end_time)
  self:FArray(m, c.dots, "contention_dot_info")
  self:FArray(m, c.lines, "contention_line_info")
  self:FArray(m, c.buffers, "contention_buffer_info")
  self:FArray(m, c.events, "contention_event_info")
  self:FArray(m, c.bases, "contention_base_info")
  self:Fcontention_mybase_info(m, c.my_data)
end
function Paser:Fcontention_rank_list(m, c)
  self:Fstring(m, c.user_id)
  self:Fstring(m, c.user_name)
  self:Finteger(m, c.rank)
  self:Finteger(m, c.point)
  self:Finteger(m, c.team_id)
  self:Fstring(m, c.team)
  self:Finteger(m, c.king)
end
function Paser:Fcontention_rank_info(m, c)
  self:Finteger(m, c.id)
  self:FArray(m, c.rank_list, "contention_rank_list")
end
function Paser:Fcontention_rank_req(m, c)
  self:Finteger(m, c.id)
end
function Paser:Fcontention_rank_ack(m, c)
  self:FArray(m, c.rank, "contention_rank_info")
end
function Paser:Fcontention_notifies_req(m, c)
  self:FArray(m, c.notifies, "string")
end
function Paser:Fincome_info(m, c)
  self:Fstring(m, c.type)
  self:Finteger(m, c.player)
  self:Finteger(m, c.alliance)
  self:Finteger(m, c.server)
end
function Paser:Fcontention_income_ack(m, c)
  self:FArray(m, c.income_list, "income_info")
end
function Paser:Fspecial_buff_info(m, c)
  self:Finteger(m, c.id)
  self:Finteger(m, c.end_time)
  self:Finteger(m, c.ext)
end
function Paser:Ffight_buff_info(m, c)
  self:Fstring(m, c.value1)
  self:Fstring(m, c.value2)
  self:Fstring(m, c.value3)
  self:Finteger(m, c.endtime)
  self:Finteger(m, c.icon)
end
function Paser:Fcontention_info(m, c)
  self:Finteger(m, c.dot_start)
  self:Finteger(m, c.dot_end)
  self:Finteger(m, c.dot_legacy)
  self:Finteger(m, c.revived_left_second)
  self:Fshort(m, c.hp)
  self:Fshort(m, c.hp_limit)
  self:Fshort(m, c.speed)
  self:FArray(m, c.now_lines, "integer")
  self:FArray(m, c.special_buff, "special_buff_info")
  self:Fstring(m, c.killer)
  self:Finteger(m, c.king)
  self:FArray(m, c.fight_buff, "fight_buff_info")
  self:Finteger(m, c.last_report)
end
function Paser:Fcontention_info_ntf(m, c)
  self:Fcontention_info(m, c.info)
end
function Paser:Fcontention_trip_info(m, c)
  self:Fshort(m, c.server_id)
  self:Fstring(m, c.user_id)
  self:Fstring(m, c.user_name)
  self:Finteger(m, c.dot_start)
  self:Finteger(m, c.dot_end)
  self:Finteger(m, c.trip_seconds)
  self:Finteger(m, c.speed)
  self:Finteger(m, c.king)
end
function Paser:Fmissiles_info(m, c)
  self:Finteger(m, c.now_step)
  self:Finteger(m, c.max_step)
  self:Finteger(m, c.begin_dot)
  self:Finteger(m, c.end_dot)
end
function Paser:Fcontention_missiles_ntf(m, c)
  self:FArray(m, c.missiles_list, "missiles_info")
end
function Paser:Fcontention_trip_ntf(m, c)
  self:FArray(m, c.trips, "contention_trip_info")
end
function Paser:Fcontention_station_info(m, c)
  self:Finteger(m, c.dot_id)
  self:Finteger(m, c.server_id)
  self:Finteger(m, c.king)
  self:Fdouble(m, c.all_power)
end
function Paser:Fcontention_station_ntf(m, c)
  self:FArray(m, c.stations, "contention_station_info")
end
function Paser:Fcontention_battle_info(m, c)
  self:Fstring(m, c.attk_user_id)
  self:Fstring(m, c.attk_user_name)
  self:Fstring(m, c.def_user_id)
  self:Fstring(m, c.def_user_name)
  self:Finteger(m, c.on_station)
  self:Fboolean(m, c.is_win)
  self:FArray(m, c.dead_list, "string")
end
function Paser:Fcontention_battle_ntf(m, c)
  self:FArray(m, c.battles, "contention_battle_info")
end
function Paser:Fcontention_production_info(m, c)
  self:Finteger(m, c.dot_id)
  self:Fboolean(m, c.is_relative)
  self:FArray(m, c.resources, "game_item")
end
function Paser:Fcontention_production_ntf(m, c)
  self:FArray(m, c.productions, "contention_production_info")
end
function Paser:Fcontention_starting_line_info(m, c)
  self:Finteger(m, c.dot_start)
  self:Finteger(m, c.line_id)
end
function Paser:Fcontention_jump_ntf(m, c)
  self:Fstring(m, c.user_id)
  self:Finteger(m, c.dot_id)
end
function Paser:Fcontention_starting_req(m, c)
  self:FArray(m, c.lines, "contention_starting_line_info")
end
function Paser:Fcontention_transfer_req(m, c)
  self:FArray(m, c.lines, "contention_starting_line_info")
end
function Paser:Fcontention_revive_req(m, c)
  self:Finteger(m, c.dot_start)
  self:Fboolean(m, c.pay_for_alive)
end
function Paser:Fcontention_stop_req(m, c)
  self:Finteger(m, c.dot_id)
  self:Finteger(m, c.page)
end
function Paser:Fcontention_occupier_info(m, c)
  self:Fstring(m, c.user_id)
  self:Fstring(m, c.user_name)
  self:Fshort(m, c.sex)
  self:Fstring(m, c.icon)
  self:Finteger(m, c.main_fleet_level)
end
function Paser:Fcontention_stop_ack(m, c)
  self:FArray(m, c.occupiers, "contention_occupier_info")
  self:Finteger(m, c.max_num)
  self:Fdouble(m, c.all_power)
  self:Fstring(m, c.king_id)
end
function Paser:Fcontention_player(m, c)
  self:Fstring(m, c.user_id)
  self:Finteger(m, c.icon)
  self:Finteger(m, c.level)
end
function Paser:Ftc_award(m, c)
  self:Finteger(m, c.id)
  self:Finteger(m, c.min)
  self:Finteger(m, c.max)
  self:FArray(m, c.awards, "game_item")
end
function Paser:Fcontention_award_req(m, c)
  self:Finteger(m, c.id)
end
function Paser:Fcontention_award_ack(m, c)
  self:FArray(m, c.awards, "tc_award")
end
function Paser:Fcontention_history_req(m, c)
  self:Finteger(m, c.season)
end
function Paser:Fhistory_rank(m, c)
  self:Fstring(m, c.name)
  self:Finteger(m, c.rank)
  self:Finteger(m, c.point)
  self:Finteger(m, c.server_id)
  self:Fstring(m, c.server)
end
function Paser:Fcontention_history_ack(m, c)
  self:Finteger(m, c.season)
  self:FArray(m, c.top3team, "history_rank")
  self:FArray(m, c.top3player, "history_rank")
end
function Paser:Fcontention_king_info_ack(m, c)
  self:Finteger(m, c.end_time)
  self:FArray(m, c.award, "game_item")
end
function Paser:Fcontention_king_change_ntf(m, c)
  self:Finteger(m, c.now_king)
end
function Paser:Falliance_domination_status_ack(m, c)
  self:Fboolean(m, c.can_battle)
end
function Paser:Fdomination_buffer_of_rank_ntf(m, c)
  self:Fstring(m, c.icon)
end
function Paser:Factivity_task_info(m, c)
  self:Finteger(m, c.id)
  self:Finteger(m, c.type)
  self:Fstring(m, c.title)
  self:Fstring(m, c.desc)
  self:Finteger(m, c.value)
  self:Finteger(m, c.status)
  self:Finteger(m, c.limit_value)
  self:FArray(m, c.reward_list, "game_item")
end
function Paser:Factivity_task_list_ack(m, c)
  self:FArray(m, c.activity_task_list, "activity_task_list")
end
function Paser:Factivity_task_list(m, c)
  self:Fstring(m, c.key_desc)
  self:Finteger(m, c.type)
  self:Finteger(m, c.special_id)
  self:Fstring(m, c.type_desc)
  self:Finteger(m, c.activity_id)
  self:FArray(m, c.task_list, "activity_task_info")
  self:FArray(m, c.kryptons, "krypton_info")
end
function Paser:Factivity_task_reward_req(m, c)
  self:Finteger(m, c.type)
  self:Finteger(m, c.activity_id)
  self:Finteger(m, c.task_id)
end
function Paser:Factivity_task_list_ntf(m, c)
  self:FArray(m, c.activity_task_list, "activity_task_list")
end
function Paser:Fcontention_name_info(m, c)
  self:Finteger(m, c.dot_id)
  self:Fstring(m, c.dot_name)
end
function Paser:Fcontention_name_ntf(m, c)
  self:FArray(m, c.stations, "contention_name_info")
end
function Paser:Fcontention_occupier_req(m, c)
  self:Fstring(m, c.user_id)
end
function Paser:Fcontention_occupier_ack(m, c)
  self:Fstring(m, c.user_name)
  self:Fshort(m, c.sex)
  self:Fshort(m, c.hp)
  self:Fshort(m, c.hp_limit)
  self:Finteger(m, c.influence)
  self:Fdouble(m, c.force)
  self:Fshort(m, c.level)
  self:Fstring(m, c.icon)
  self:FArray(m, c.fleet_levels, "fleet_level")
  self:FArray(m, c.matrices, "matrix_cell")
end
function Paser:Fcontention_collect_req(m, c)
  self:FArray(m, c.dots, "integer")
end
function Paser:Fcontention_collection_info(m, c)
  self:Finteger(m, c.dot_id)
  self:FArray(m, c.resources, "game_item")
end
function Paser:Fcontention_collect_ack(m, c)
  self:FArray(m, c.productions, "contention_collection_info")
end
function Paser:Fcontention_log_info(m, c)
  self:Finteger(m, c.id)
  self:Fshort(m, c.log_type)
  self:Finteger(m, c.ext)
  self:FArray(m, c.params, "string")
  self:Finteger(m, c.time)
end
function Paser:Fcontention_hit_info(m, c)
  self:Finteger(m, c.id)
  self:FArray(m, c.params, "string")
end
function Paser:Fcontention_hit_ntf(m, c)
  self:Fcontention_hit_info(m, c.hit)
end
function Paser:Fcontention_logs_req(m, c)
  self:Fstring(m, c.user_id)
end
function Paser:Fcontention_logs_ack(m, c)
  self:FArray(m, c.logs, "contention_log_info")
end
function Paser:Fcontention_logbattle_detail_req(m, c)
  self:Finteger(m, c.id)
end
function Paser:Fcontention_logbattle_detail_ack(m, c)
  self:Ffight_report(m, c.report)
  self:FArray(m, c.reward_list, "game_item")
end
function Paser:Factivity_status_req(m, c)
  self:FArray(m, c.ids, "short")
end
function Paser:Factivity_status_info(m, c)
  self:Fshort(m, c.id)
  self:Fboolean(m, c.enabled)
  self:Finteger(m, c.left_seconds)
end
function Paser:Factivity_status_ack(m, c)
  self:FArray(m, c.activities, "activity_status_info")
end
function Paser:Fscreen_track(m, c)
  self:Fstring(m, c.udid)
  self:Fstring(m, c.state_object)
end
function Paser:Ftango_invite_req(m, c)
  self:Fstring(m, c.tangoid)
  self:FArray(m, c.friends, "string")
end
function Paser:Ftango_invited_friends_req(m, c)
  self:Fstring(m, c.tangoid)
  self:FArray(m, c.friends, "string")
end
function Paser:Ftango_invited_friends_ack(m, c)
  self:FArray(m, c.invited_friends, "string")
  self:FArray(m, c.registered_friends, "string")
end
function Paser:Fpay_animation_ntf(m, c)
  self:Finteger(m, c.twink_time)
  self:Finteger(m, c.breath_time)
end
function Paser:Fattr_change(m, c)
  self:Finteger(m, c.id)
  self:Finteger(m, c.value)
  self:Finteger(m, c.next_level_id)
  self:Finteger(m, c.next_level_value)
end
function Paser:Fhero_level_attr_diff_req(m, c)
  self:Finteger(m, c.hero_id)
  self:Fboolean(m, c.is_levelup)
end
function Paser:Fhero_level_attr_diff_ack(m, c)
  self:Fshort(m, c.allow_max)
  self:Fshort(m, c.level_limited)
  self:Fshort(m, c.probability)
  self:Fshort(m, c.probability_step)
  self:FArray(m, c.changed_attrs, "attr_change")
  self:FArray(m, c.fleets, "fleet_show_info")
  self:FArray(m, c.conditions, "game_item")
end
function Paser:Fexpedition_wormhole_progress_award(m, c)
  self:Finteger(m, c.number)
  self:FArray(m, c.awards, "game_item")
  self:Finteger(m, c.status)
end
function Paser:Fexpedition_wormhole_task(m, c)
  self:Finteger(m, c.finished_num)
  self:Finteger(m, c.total_num)
  self:FArray(m, c.progress_awards, "expedition_wormhole_progress_award")
end
function Paser:Fexpedition_wormhole_status(m, c)
  self:Fexpedition_wormhole_task(m, c.all_tasks)
end
function Paser:Fexpedition_wormhole_award_req(m, c)
  self:Fdouble(m, c.process_num)
end
function Paser:Fexpedition_wormhole_rank_board_req(m, c)
  self:Finteger(m, c.type)
  self:Finteger(m, c.top)
end
function Paser:Fexpedition_wormhole_player_brief(m, c)
  self:Finteger(m, c.rank)
  self:Fstring(m, c.user_name)
  self:Fdouble(m, c.score)
  self:Finteger(m, c.level)
end
function Paser:Fexpedition_wormhole_rank_board(m, c)
  self:Finteger(m, c.type)
  self:Fexpedition_wormhole_player_brief(m, c.self)
  self:FArray(m, c.top_list, "expedition_wormhole_player_brief")
end
function Paser:Fexpedition_wormhole_rank_board_ack(m, c)
  self:FArray(m, c.rank_board, "expedition_wormhole_rank_board")
end
function Paser:Fexpedition_wormhole_rank_self_ntf(m, c)
  self:FArray(m, c.self, "expedition_wormhole_player_brief")
end
function Paser:Fdungeon_open_info(m, c)
  self:Fshort(m, c.active_id)
  self:Finteger(m, c.time)
  self:Fstring(m, c.activity_name)
  self:Fstring(m, c.pic_name)
  self:Finteger(m, c.store_tag)
  self:Finteger(m, c.red_point)
end
function Paser:Fdungeon_open_ack(m, c)
  self:FArray(m, c.info, "dungeon_open_info")
end
function Paser:Fdungeon_enter_req(m, c)
  self:Fshort(m, c.active_id)
end
function Paser:Fladder_info(m, c)
  self:Fshort(m, c.cur_step)
  self:Fshort(m, c.search_num)
  self:Fshort(m, c.reset_num)
  self:Finteger(m, c.end_time)
  self:Finteger(m, c.interval)
  self:FArray(m, c.has_rewards, "game_item")
  self:Finteger(m, c.captain_index)
  self:Finteger(m, c.level)
  self:FArray(m, c.may_rewards, "game_item")
  self:Finteger(m, c.dialog)
  self:Fstring(m, c.condition)
  self:Fdouble(m, c.monster_hp)
  self:Finteger(m, c.monster_shield)
  self:Fstring(m, c.monster_name)
  self:Finteger(m, c.backgroud)
  self:FArray(m, c.matrix, "monster_matrix_cell")
  self:Fdouble(m, c.force)
  self:Fboolean(m, c.raids_end)
  self:Fboolean(m, c.on_max_step)
  self:FArray(m, c.ratio_award, "game_item")
  self:Finteger(m, c.radis_step)
  self:Fboolean(m, c.can_transmit)
  self:Finteger(m, c.cur_max_step)
  self:Finteger(m, c.reset_radis_step)
  self:Fboolean(m, c.can_award)
  self:FArray(m, c.buffs, "pair")
end
function Paser:Fladder_enter_ack(m, c)
  self:Fshort(m, c.type)
  self:Fladder_info(m, c.info)
  self:Fboolean(m, c.is_first)
end
function Paser:Fladder_fresh_req(m, c)
  self:Fshort(m, c.type)
end
function Paser:Fladder_fresh_ack(m, c)
  self:Fshort(m, c.type)
  self:Fladder_info(m, c.info)
end
function Paser:Fladder_reset_req(m, c)
  self:Fshort(m, c.type)
end
function Paser:Fladder_reset_ack(m, c)
  self:Fshort(m, c.type)
  self:Fladder_info(m, c.info)
end
function Paser:Fladder_search_ack(m, c)
  self:Fshort(m, c.type)
  self:Ffight_report(m, c.report)
  self:Finteger(m, c.result)
  self:FArray(m, c.rewards, "game_item")
  self:Fladder_info(m, c.info)
  self:FArray(m, c.fleets, "fleet_damage_info")
end
function Paser:Fladder_raids_ack(m, c)
  self:Fshort(m, c.type)
  self:Finteger(m, c.end_time)
  self:Finteger(m, c.interval)
end
function Paser:Fladder_buy_search_ack(m, c)
  self:Fshort(m, c.type)
  self:Fshort(m, c.num)
end
function Paser:Fladder_buy_reset_ack(m, c)
  self:Fshort(m, c.type)
  self:Fshort(m, c.num)
end
function Paser:Fladder_rank_info(m, c)
  self:Finteger(m, c.rank)
  self:Fstring(m, c.user_id)
  self:Finteger(m, c.icon)
  self:Fstring(m, c.name)
  self:Finteger(m, c.level)
  self:Fshort(m, c.step)
end
function Paser:Fladder_rank_ack(m, c)
  self:Fshort(m, c.type)
  self:Fladder_rank_info(m, c.self)
  self:FArray(m, c.rank, "ladder_rank_info")
end
function Paser:Fladder_special_info(m, c)
  self:Fshort(m, c.step)
  self:FArray(m, c.rewards, "game_item")
  self:Fshort(m, c.status)
end
function Paser:Fladder_special_ack(m, c)
  self:Fshort(m, c.type)
  self:FArray(m, c.info, "ladder_special_info")
end
function Paser:Fladder_search_req(m, c)
  self:Fshort(m, c.type)
end
function Paser:Fladder_raids_req(m, c)
  self:Fshort(m, c.type)
end
function Paser:Fladder_cancel_req(m, c)
  self:Fshort(m, c.type)
end
function Paser:Fladder_gain_req(m, c)
  self:Fshort(m, c.type)
end
function Paser:Fladder_buy_search_req(m, c)
  self:Fshort(m, c.type)
end
function Paser:Fladder_buy_reset_req(m, c)
  self:Fshort(m, c.type)
end
function Paser:Fladder_rank_req(m, c)
  self:Fshort(m, c.type)
end
function Paser:Fladder_special_req(m, c)
  self:Fshort(m, c.type)
end
function Paser:Ffleet_enhance_req(m, c)
  self:Finteger(m, c.fleet_id)
end
function Paser:Ffleet_weaken_req(m, c)
  self:Finteger(m, c.fleet_id)
end
function Paser:Ffleet_show_param(m, c)
  self:Finteger(m, c.fleet_id)
  self:Fshort(m, c.level)
end
function Paser:Ffleet_show_req(m, c)
  self:FArray(m, c.fleets, "fleet_show_param")
end
function Paser:Ffleet_show_ack(m, c)
  self:FArray(m, c.fleets, "fleet_show_info")
end
function Paser:Forder_info(m, c)
  self:Finteger(m, c.id)
  self:Fshort(m, c.status)
  self:Fshort(m, c.waiting_seconds)
end
function Paser:Forders_ntf(m, c)
  self:FArray(m, c.orders, "order_info")
end
function Paser:Fwxpay_info_ntf(m, c)
  self:Fstring(m, c.mch_id)
  self:Fstring(m, c.prepay_id)
  self:Fstring(m, c.package)
  self:Fstring(m, c.nonce_str)
  self:Fstring(m, c.timestamp)
  self:Fstring(m, c.sign)
end
function Paser:Fpay_confirm_req(m, c)
  self:Finteger(m, c.id)
end
function Paser:Fuser_notifies_req(m, c)
  self:FArray(m, c.notifies, "string")
end
function Paser:Fitems_ntf(m, c)
  self:FArray(m, c.grids, "bag_grid")
end
function Paser:Fip_ntf(m, c)
  self:Fstring(m, c.ip)
end
function Paser:Fmycard_error(m, c)
  self:Fstring(m, c.msg)
end
function Paser:Faward_list_ack(m, c)
  self:FArray(m, c.award_infos, "award_info")
  self:FArray(m, c.kryptons, "krypton_info")
end
function Paser:Faward_info(m, c)
  self:Finteger(m, c.id)
  self:Finteger(m, c.type)
  self:Finteger(m, c.achievement_id)
  self:Finteger(m, c.time)
  self:FArray(m, c.awards, "game_item")
end
function Paser:Faward_get_req(m, c)
  self:Finteger(m, c.id)
end
function Paser:Faward_count_ntf(m, c)
  self:Finteger(m, c.count)
end
function Paser:Fadv_chapter_status(m, c)
  self:Finteger(m, c.chapter_id)
  self:Finteger(m, c.status)
end
function Paser:Fadv_chapter_status_ntf(m, c)
  self:FArray(m, c.statuses, "adv_chapter_status")
end
function Paser:Fenchant_req(m, c)
  self:Fstring(m, c.equipment_id)
  self:Finteger(m, c.item_id)
  self:Finteger(m, c.addition_item_id)
  self:Finteger(m, c.addition_item_num)
  self:Finteger(m, c.addition_credits)
  self:Finteger(m, c.uplevel_count)
end
function Paser:Fenchant_ack(m, c)
  self:Finteger(m, c.origin_level)
  self:Finteger(m, c.origin_effect)
  self:Finteger(m, c.origin_effect_value)
  self:Finteger(m, c.new_level)
  self:Finteger(m, c.new_effect)
  self:Finteger(m, c.new_effect_value)
end
function Paser:Fpay_record_ntf(m, c)
  self:Fdouble(m, c.price)
  self:FArray(m, c.param, "string")
end
function Paser:Flogin_token_ntf(m, c)
  self:Fshort(m, c.acc_type)
  self:Fstring(m, c.user_id)
end
function Paser:Fdungeon_enter_ack(m, c)
  self:Finteger(m, c.code)
  self:Fshort(m, c.lev)
end
function Paser:Fchange_auto_decompose(m, c)
  self:Finteger(m, c.change_num)
end
function Paser:Fchange_auto_decompose_ntf(m, c)
  self:Finteger(m, c.change_num)
  self:Finteger(m, c.max_level)
  self:Finteger(m, c.mini_level)
end
function Paser:Factivity_dna_req(m, c)
  self:Finteger(m, c.activity_id)
end
function Paser:Factivity_dna_ack(m, c)
  self:Fgame_item(m, c.item_id)
  self:Fgame_item(m, c.award_id)
  self:Finteger(m, c.dna_had)
  self:Finteger(m, c.dna_need)
  self:FArray(m, c.dna_charge_status, "short")
  self:Finteger(m, c.dna_charge_max)
  self:Fboolean(m, c.charge_next_condition)
  self:Finteger(m, c.charge_award_tag)
  self:FArray(m, c.activity_award, "game_item")
  self:Finteger(m, c.price)
  self:Finteger(m, c.discount)
  self:Finteger(m, c.kid_id)
end
function Paser:Factivity_dna_charge_req(m, c)
  self:Finteger(m, c.activity_id)
  self:Finteger(m, c.charge_time)
end
function Paser:Fcrusade_pos(m, c)
  self:Fshort(m, c.pos)
  self:Fstring(m, c.name)
  self:Fstring(m, c.server)
  self:Fshort(m, c.locale)
  self:Finteger(m, c.level)
  self:Fshort(m, c.sex)
  self:FArray(m, c.matrix, "monster_matrix_cell")
  self:FArray(m, c.adjutant, "crusade_adjutant")
  self:Fdouble(m, c.force)
  self:Finteger(m, c.log_id)
  self:Fboolean(m, c.pass)
end
function Paser:Fcrusade_reward(m, c)
  self:Fshort(m, c.pos)
  self:FArray(m, c.rewards, "game_item")
end
function Paser:Fcrusade_adjutant(m, c)
  self:Finteger(m, c.major)
  self:Finteger(m, c.adjutant)
  self:Finteger(m, c.adjutant_level)
  self:FArray(m, c.adjutant_spell, "integer")
end
function Paser:Fcrusade_info(m, c)
  self:FArray(m, c.pos_info, "crusade_pos")
  self:FArray(m, c.reward_get, "short")
  self:FArray(m, c.rewards, "crusade_reward")
  self:Fshort(m, c.cross)
  self:FArray(m, c.place, "short")
  self:FArray(m, c.pos_path, "short")
  self:Fshort(m, c.max_repair)
  self:Fmatrix(m, c.self_matrix)
  self:FArray(m, c.self_fleet, "crusade_fleet")
  self:FArray(m, c.self_adjutant, "crusade_adjutant")
  self:Finteger(m, c.real_id)
end
function Paser:Fcrusade_enter_ack(m, c)
  self:Fcrusade_info(m, c.info)
end
function Paser:Fcrusade_battle_req(m, c)
  self:Fshort(m, c.pos)
end
function Paser:Fcrusade_fleet(m, c)
  self:Finteger(m, c.fleet_id)
  self:Finteger(m, c.hp)
  self:Finteger(m, c.shield)
  self:Finteger(m, c.max_hp)
  self:Finteger(m, c.max_shield)
  self:Fdouble(m, c.force)
  self:Finteger(m, c.level)
end
function Paser:Fcrusade_fight_req(m, c)
  self:Fshort(m, c.pos)
  self:Fmatrix(m, c.matrix)
end
function Paser:Fcrusade_fight_ack(m, c)
  self:Ffight_report(m, c.report)
  self:FArray(m, c.rewards, "game_item")
  self:Fmatrix(m, c.matrix)
  self:Finteger(m, c.fight_log)
end
function Paser:Fcrusade_cross_req(m, c)
  self:Fshort(m, c.pos)
end
function Paser:Fcrusade_cross_ack(m, c)
  self:Fcrusade_info(m, c.info)
end
function Paser:Fcrusade_repair_req(m, c)
  self:Finteger(m, c.fleet_id)
end
function Paser:Fcrusade_reward_req(m, c)
  self:Fshort(m, c.pos)
end
function Paser:Fcrusade_reward_ack(m, c)
  self:FArray(m, c.reward, "game_item")
end
function Paser:Fcrusade_buy_cross_ack(m, c)
  self:Fshort(m, c.num)
end
function Paser:Fcrusade_first_ack(m, c)
  self:Fshort(m, c.enter_level)
  self:Fboolean(m, c.first)
end
function Paser:Flevel_range(m, c)
  self:Fshort(m, c.step)
  self:Fshort(m, c.max)
  self:Fshort(m, c.min)
end
function Paser:Fbudo_stage_ntf(m, c)
  self:Fshort(m, c.cur_stage)
  self:Fshort(m, c.cur_step)
  self:FArray(m, c.budo_level_range, "level_range")
end
function Paser:Fbudo_sign_up_ntf(m, c)
  self:Finteger(m, c.left_time)
  self:Fboolean(m, c.does_join)
  self:Fshort(m, c.budo_level_min)
  self:Fshort(m, c.budo_level_max)
end
function Paser:Fbudo_join_ack(m, c)
  self:Fshort(m, c.budo_level_min)
  self:Fshort(m, c.budo_level_max)
end
function Paser:Fbudo_player(m, c)
  self:Fstring(m, c.name)
  self:Fstring(m, c.avatar)
  self:Finteger(m, c.player_id)
  self:Fstring(m, c.server_id)
  self:Finteger(m, c.identity)
  self:Fshort(m, c.player_level)
  self:Finteger(m, c.get_support)
  self:Finteger(m, c.main_fleet_level)
end
function Paser:Fone_battle_report(m, c)
  self:Fbudo_player(m, c.win_player)
  self:Fbudo_player(m, c.failed_player)
  self:Finteger(m, c.battle_id)
end
function Paser:Fbudo_award_list(m, c)
  self:Fshort(m, c.round)
  self:FArray(m, c.award_list, "game_item")
end
function Paser:Fbudo_mass_election_ntf(m, c)
  self:Fshort(m, c.player_status)
  self:Fshort(m, c.budo_level_max)
  self:Fshort(m, c.budo_level_min)
  self:Fshort(m, c.win_time)
  self:Fshort(m, c.failed_time)
  self:Fshort(m, c.cur_kid_stage)
  self:Fshort(m, c.total_stages)
  self:Finteger(m, c.left_battle_time)
  self:FArray(m, c.award_list, "budo_award_list")
  self:Finteger(m, c.can_submit)
end
function Paser:Fbudo_promotion_req(m, c)
  self:Fshort(m, c.budo_level)
end
function Paser:Fpromotion_one_round_report(m, c)
  self:Fshort(m, c.battle_round)
  self:Fshort(m, c.index)
  self:Finteger(m, c.player_id)
  self:Finteger(m, c.server_id)
  self:Fstring(m, c.name)
  self:Fstring(m, c.avatar)
  self:Fshort(m, c.player_level)
  self:Finteger(m, c.get_support)
  self:Finteger(m, c.main_fleet_level)
end
function Paser:Fone_player_support(m, c)
  self:Finteger(m, c.player_id)
  self:Fstring(m, c.server_id)
  self:Finteger(m, c.num)
end
function Paser:Fbudo_promotion_ntf(m, c)
  self:Finteger(m, c.step)
  self:Fshort(m, c.win)
  self:Fshort(m, c.lose)
  self:Finteger(m, c.left_battle_time)
  self:Fshort(m, c.player_status)
  self:Fshort(m, c.budo_level_max)
  self:Fshort(m, c.budo_level_min)
  self:FArray(m, c.my_support_player, "budo_player")
  self:FArray(m, c.my_award_array, "budo_award_list")
  self:FArray(m, c.vs_player, "promotion_one_round_report")
  self:FArray(m, c.all_player, "budo_player")
  self:Finteger(m, c.can_submit)
  self:Finteger(m, c.audition_round)
end
function Paser:Fsupport_one_player_req(m, c)
  self:Fshort(m, c.step)
  self:Fshort(m, c.index)
  self:Finteger(m, c.player_id)
  self:Fstring(m, c.server_id)
  self:Fshort(m, c.round)
end
function Paser:Fsupport_one_player_ack(m, c)
  self:Finteger(m, c.worth)
end
function Paser:Fbattle_report_req(m, c)
  self:Finteger(m, c.player_id)
  self:Fstring(m, c.server_id)
  self:Finteger(m, c.stage)
  self:Finteger(m, c.round)
end
function Paser:Fbattle_report_ack(m, c)
  self:FArray(m, c.battle_report, "one_battle_report")
end
function Paser:Fsupport_player(m, c)
  self:Fshort(m, c.step)
  self:Fshort(m, c.index)
  self:Fshort(m, c.round)
  self:Fshort(m, c.award_status)
  self:FArray(m, c.award, "game_item")
end
function Paser:Fget_budo_rank_req(m, c)
  self:Fshort(m, c.step)
end
function Paser:Fget_my_support_ack(m, c)
  self:FArray(m, c.support_info, "support_player")
end
function Paser:Frank_player(m, c)
  self:Finteger(m, c.rank)
  self:Fstring(m, c.name)
  self:Finteger(m, c.point)
end
function Paser:Fget_budo_rank_ack(m, c)
  self:FArray(m, c.rank_info, "rank_player")
end
function Paser:Fget_budo_replay_req(m, c)
  self:Finteger(m, c.battle_id)
end
function Paser:Fget_budo_replay_ack(m, c)
  self:Ffight_report(m, c.battle_record)
end
function Paser:Fget_promotion_reward_req(m, c)
  self:Fshort(m, c.step)
  self:Fshort(m, c.round)
end
function Paser:Fspecial_efficacy_ntf(m, c)
  self:Finteger(m, c.id)
  self:Fboolean(m, c.flag)
end
function Paser:Factivity_rank_req(m, c)
  self:Fstring(m, c.id)
end
function Paser:Factivity_box_rank(m, c)
  self:Fstring(m, c.id)
  self:FArray(m, c.players, "thanksgiven_box_player")
  self:FArray(m, c.alliances, "thanksgiven_box_alliance")
  self:Fthanksgiven_box_player(m, c.self_info)
end
function Paser:Fgd_fleets_info(m, c)
  self:Finteger(m, c.id)
  self:Fboolean(m, c.is_mine)
  self:Fdouble(m, c.force)
  self:Finteger(m, c.thumb_up_num)
  self:Fboolean(m, c.can_send_story)
end
function Paser:Fall_gd_fleets_ack(m, c)
  self:FArray(m, c.fleets, "gd_fleets_info")
end
function Paser:Ffleet_fight_report_req(m, c)
  self:Finteger(m, c.id)
end
function Paser:Ffleet_fight_report_ack(m, c)
  self:Ffight_report(m, c.report)
end
function Paser:Fopen_box_info_req(m, c)
  self:Finteger(m, c.box_id)
end
function Paser:Fopen_box_refresh_req(m, c)
  self:Finteger(m, c.box_id)
end
function Paser:Flaba_req(m, c)
  self:Finteger(m, c.coin)
  self:Finteger(m, c.power)
end
function Paser:Flaba_times_ntf(m, c)
  self:Finteger(m, c.times)
  self:Finteger(m, c.area)
end
function Paser:Flaba_award_req(m, c)
  self:Finteger(m, c.type)
end
function Paser:Falliance_fleet_info(m, c)
  self:Fshort(m, c.fleet_id)
  self:Fshort(m, c.levelup)
end
function Paser:Fpay_list_carrier_req(m, c)
  self:Fstring(m, c.channel)
  self:Fstring(m, c.area)
end
function Paser:Fpay_list_carrier_ack(m, c)
  self:FArray(m, c.productions, "production_info")
end
function Paser:Flaba_type_ntf(m, c)
  self:FArray(m, c.info, "string")
  self:FArray(m, c.item, "game_item")
end
function Paser:Fchampion_matrix_req(m, c)
  self:Finteger(m, c.rank)
end
function Paser:Fchampion_matrix_ack(m, c)
  self:FArray(m, c.matrix, "monster_matrix_cell")
end
function Paser:Fmonth_card_list_req(m, c)
  self:Fshort(m, c.pay_code)
end
function Paser:Fmonth_card_info(m, c)
  self:Finteger(m, c.id)
  self:Fstring(m, c.name)
  self:Fstring(m, c.product_id)
  self:Finteger(m, c.price)
  self:Finteger(m, c.totle_days)
  self:Finteger(m, c.totle_num)
  self:Finteger(m, c.day_num)
  self:Finteger(m, c.less_days)
  self:Fboolean(m, c.can_buy)
  self:Fboolean(m, c.can_get)
  self:Fboolean(m, c.can_use)
  self:Fboolean(m, c.has_join)
  self:Finteger(m, c.activity_totle)
  self:Finteger(m, c.activity_num)
  self:Finteger(m, c.activity_time)
  self:Fboolean(m, c.activity_join)
  self:FArray(m, c.activity_reward, "game_item")
end
function Paser:Fmonth_card_list_ack(m, c)
  self:Finteger(m, c.free_num)
  self:Fboolean(m, c.free_get)
  self:FArray(m, c.info, "month_card_info")
end
function Paser:Fmonth_card_buy_req(m, c)
  self:Finteger(m, c.id)
  self:Finteger(m, c.gift_user)
end
function Paser:Fmonth_card_use_req(m, c)
  self:Finteger(m, c.id)
end
function Paser:Fadd_adjutant_req(m, c)
  self:Finteger(m, c.major)
  self:Finteger(m, c.adjutant)
end
function Paser:Fadd_tlc_adjutant_req(m, c)
  self:Finteger(m, c.matrix_type)
  self:Finteger(m, c.major)
  self:Finteger(m, c.adjutant)
end
function Paser:Frelease_adjutant_req(m, c)
  self:Finteger(m, c.major)
end
function Paser:Fchat_channel_switch_ntf(m, c)
  self:FArray(m, c.chat_channels, "chat_channel")
end
function Paser:Fchat_channel(m, c)
  self:Fshort(m, c.type)
  self:Fboolean(m, c.switch)
end
function Paser:Fadjutant_max_ntf(m, c)
  self:Finteger(m, c.num)
  self:Finteger(m, c.level)
end
function Paser:Fmax_level_ntf(m, c)
  self:Finteger(m, c.max)
  self:Finteger(m, c.max_recruit)
end
function Paser:Fgame_items_trans_req(m, c)
  self:FArray(m, c.items, "string")
end
function Paser:Fgame_items_trans_ack(m, c)
  self:FArray(m, c.all_items, "game_items")
end
function Paser:Fbudo_champion_req(m, c)
  self:Finteger(m, c.index)
end
function Paser:Fchampion_battle(m, c)
  self:Fboolean(m, c.is_win)
  self:Finteger(m, c.battle_id)
end
function Paser:Fchampion_step_info(m, c)
  self:Finteger(m, c.step)
  self:Finteger(m, c.min_lev)
  self:Finteger(m, c.max_lev)
  self:Fbudo_player(m, c.champion)
  self:Fstring(m, c.champion_server)
  self:Fbudo_player(m, c.enemy)
  self:Fstring(m, c.enemy_server)
  self:FArray(m, c.battle, "champion_battle")
end
function Paser:Fbudo_champion_ack(m, c)
  self:Finteger(m, c.max_index)
  self:Finteger(m, c.now_index)
  self:FArray(m, c.effect_list, "integer")
  self:FArray(m, c.info, "champion_step_info")
end
function Paser:Fbudo_champion_report_req(m, c)
  self:Finteger(m, c.index)
  self:Finteger(m, c.battle_id)
end
function Paser:Fclient_version_ntf(m, c)
  self:FArray(m, c.versions, "client_version")
end
function Paser:Fclient_version(m, c)
  self:Fstring(m, c.appid)
  self:Finteger(m, c.version)
end
function Paser:Fversion_code_ntf(m, c)
  self:Finteger(m, c.player_version)
  self:Finteger(m, c.server_version)
end
function Paser:Fversion_code_update_req(m, c)
  self:Finteger(m, c.version)
end
function Paser:Fapply_limit_update_req(m, c)
  self:Finteger(m, c.apply_limit)
end
function Paser:Fremodel_levelup_req(m, c)
  self:Finteger(m, c.user_id)
  self:Fshort(m, c.type)
end
function Paser:Fremodel_info(m, c)
  self:Fshort(m, c.type)
  self:Finteger(m, c.cur_level)
  self:Finteger(m, c.max_level)
  self:Finteger(m, c.addhp)
  self:Finteger(m, c.nextlevel_addhp)
  self:Finteger(m, c.nextlevel_money)
  self:Finteger(m, c.nextlevel_time)
  self:Finteger(m, c.credit)
  self:Fshort(m, c.state)
  self:Finteger(m, c.cur_help)
  self:Finteger(m, c.max_help)
  self:Finteger(m, c.left_time)
end
function Paser:Fremodel_info_ack(m, c)
  self:FArray(m, c.remodel_info_ack, "remodel_info")
end
function Paser:Fremodel_info_ntf(m, c)
  self:FArray(m, c.remodel_info_ntf, "remodel_info")
end
function Paser:Fremodel_help_info(m, c)
  self:Finteger(m, c.user_id)
  self:Fstring(m, c.user_name)
  self:Fshort(m, c.type)
  self:Finteger(m, c.cur_num)
  self:Finteger(m, c.max_num)
  self:Finteger(m, c.is_help)
end
function Paser:Fremodel_help_ntf(m, c)
  self:FArray(m, c.other_info, "remodel_help_info")
end
function Paser:Fremodel_help_info_ack(m, c)
  self:FArray(m, c.info, "remodel_help_info")
end
function Paser:Fhelp_others_info(m, c)
  self:Finteger(m, c.user_id)
  self:Fshort(m, c.type)
end
function Paser:Fremodel_help_others_req(m, c)
  self:FArray(m, c.help_list, "help_others_info")
end
function Paser:Fremodel_help_others_ack(m, c)
  self:Finteger(m, c.code)
  self:FArray(m, c.other_info, "remodel_help_info")
end
function Paser:Fremodel_speed_req(m, c)
  self:Fshort(m, c.type)
  self:Finteger(m, c.item_id)
end
function Paser:Fremodel_push_info(m, c)
  self:Fstring(m, c.name)
  self:Finteger(m, c.type)
  self:Finteger(m, c.action)
  self:Finteger(m, c.time)
end
function Paser:Fremodel_push_ntf(m, c)
  self:FArray(m, c.ntf, "remodel_push_info")
end
function Paser:Fgive_friends_gift_req(m, c)
  self:Finteger(m, c.type)
  self:FArray(m, c.friends, "string")
end
function Paser:Fgive_share_awards_req(m, c)
  self:Finteger(m, c.type)
  self:Fgame_item(m, c.itemkey)
  self:Finteger(m, c.intkey)
end
function Paser:Fremodel_event_ntf(m, c)
  self:Finteger(m, c.times)
end
function Paser:Fgive_friends_gift_ack(m, c)
  self:FArray(m, c.friends, "string")
end
function Paser:Fupdate_passport_req(m, c)
  self:Fstring(m, c.passport)
end
function Paser:Ffinancial_info(m, c)
  self:Finteger(m, c.level)
  self:Finteger(m, c.benefit)
  self:Finteger(m, c.next_level)
  self:Finteger(m, c.next_benefit)
  self:FArray(m, c.upgrade_cost, "game_item")
end
function Paser:Ffacebook_friends_req(m, c)
  self:FArray(m, c.friends, "string")
end
function Paser:Ffacebook_friend_info(m, c)
  self:Fstring(m, c.passport)
  self:Finteger(m, c.status)
  self:FArray(m, c.awards, "game_item")
  self:Finteger(m, c.left_time)
end
function Paser:Ffacebook_friends_ack(m, c)
  self:FArray(m, c.friends, "facebook_friend_info")
end
function Paser:Ffacebook_friends_ntf(m, c)
  self:FArray(m, c.friends, "facebook_friend_info")
end
function Paser:Ffinancial_gifts_get_req(m, c)
  self:FArray(m, c.friends, "string")
end
function Paser:Ffinancial_amount_ntf(m, c)
  self:Finteger(m, c.amount)
end
function Paser:Fmulti_acc_loot(m, c)
  self:Finteger(m, c.acc_type)
  self:FArray(m, c.items, "game_item")
end
function Paser:Fmulti_acc_ack(m, c)
  self:FArray(m, c.loot, "multi_acc_loot")
  self:FArray(m, c.already_get, "integer")
end
function Paser:Fint_param(m, c)
  self:Finteger(m, c.i)
end
function Paser:Fpush_button_info(m, c)
  self:Fshort(m, c.all)
  self:Fshort(m, c.activity)
  self:Fshort(m, c.champion)
  self:Fshort(m, c.colony)
  self:Fshort(m, c.mine)
  self:Fshort(m, c.building)
  self:Fshort(m, c.equipment)
  self:Fshort(m, c.revenue)
  self:Fshort(m, c.alliance)
end
function Paser:Fpush_button_req(m, c)
  self:Fpush_button_info(m, c.req)
end
function Paser:Fpush_button_info_ack(m, c)
  self:Fpush_button_info(m, c.ack)
end
function Paser:Fpush_button_ntf(m, c)
  self:Fpush_button_info(m, c.ntf)
end
function Paser:Finvited_friend_info(m, c)
  self:Fstring(m, c.passport)
  self:Finteger(m, c.level)
  self:Finteger(m, c.energy)
  self:Finteger(m, c.state)
  self:Finteger(m, c.last_active_time)
end
function Paser:Ftreasure_info(m, c)
  self:Finteger(m, c.status)
  self:Finteger(m, c.level)
  self:Finteger(m, c.energy)
  self:Finteger(m, c.required)
  self:Finteger(m, c.refresh_time)
  self:Finteger(m, c.max_friend_num)
  self:FArray(m, c.friends, "invited_friend_info")
end
function Paser:Finvite_friends_req(m, c)
  self:FArray(m, c.friends, "string")
end
function Paser:Fget_energy_req(m, c)
  self:FArray(m, c.friends, "string")
end
function Paser:Fopen_treasure_box_ack(m, c)
  self:Finteger(m, c.result)
  self:FArray(m, c.awards, "game_item")
end
function Paser:Ffriend_role_req(m, c)
  self:Fstring(m, c.passport)
end
function Paser:Ffriend_role_ack(m, c)
  self:Fuser_brief_info(m, c.role_info)
end
function Paser:Fsend_global_email_req(m, c)
  self:Fstring(m, c.passport)
  self:Fstring(m, c.nickname)
  self:Fstring(m, c.title)
  self:Fstring(m, c.content)
end
function Paser:Fcollect_energy_req(m, c)
  self:Fstring(m, c.passport)
end
function Paser:Fkrypton_refine_info_ack(m, c)
  self:Finteger(m, c.remain_times)
  self:Finteger(m, c.max_times)
  self:Finteger(m, c.item_id)
  self:Finteger(m, c.item_count)
  self:Fboolean(m, c.times_can_buy)
  self:Finteger(m, c.item_exp)
end
function Paser:Fkrypton_inject_req(m, c)
  self:Fstring(m, c.injected_id)
  self:Fstring(m, c.inject_id)
  self:Fboolean(m, c.with_item)
end
function Paser:Fkrypton_inject_ack(m, c)
  self:Fkrypton_info(m, c.injected_krypton_info)
  self:Finteger(m, c.remain_times)
  self:Finteger(m, c.max_times)
  self:Finteger(m, c.item_count)
  self:Finteger(m, c.kenergy)
  self:Fboolean(m, c.is_critical)
end
function Paser:Fkrypton_refine_req(m, c)
  self:Fstring(m, c.refine_id)
end
function Paser:Fkrypton_refine_ack(m, c)
  self:Fkrypton_info(m, c.refined_krypton_info)
  self:Finteger(m, c.kenergy)
end
function Paser:Fkrypton_inject_buy_ack(m, c)
  self:Finteger(m, c.remain_times)
  self:Finteger(m, c.max_times)
  self:Finteger(m, c.credit)
end
function Paser:Fkrypton_inject_list_req(m, c)
  self:Finteger(m, c.formation_id)
  self:Fstring(m, c.refined_id)
end
function Paser:Fkrypton_inject_list_ack(m, c)
  self:FArray(m, c.kryptons, "krypton_info")
end
function Paser:Fkrypton_refine_list_req(m, c)
  self:Finteger(m, c.formation_id)
end
function Paser:Fkrypton_refine_list_ack(m, c)
  self:FArray(m, c.kryptons, "krypton_info")
  self:FArray(m, c.up_kryptons, "krypton_info")
end
function Paser:Fuser_info_fleet(m, c)
  self:FArray(m, c.ranks, "pair")
  self:Finteger(m, c.total_count)
end
function Paser:Fuser_info_force(m, c)
  self:Fdouble(m, c.total_force)
  self:Finteger(m, c.coating_ph_atk)
  self:Finteger(m, c.coating_ph_armor)
  self:Finteger(m, c.coating_sp_atk)
  self:Finteger(m, c.coating_sp_armor)
  self:Finteger(m, c.coating_durability)
  self:Finteger(m, c.coating_ph_atk_v2)
  self:Finteger(m, c.coating_ph_armor_v2)
  self:Finteger(m, c.coating_sp_atk_v2)
  self:Finteger(m, c.coating_sp_armor_v2)
  self:Finteger(m, c.coating_durability_v2)
  self:Finteger(m, c.total_speed)
  self:Finteger(m, c.aver_equip_level)
  self:Finteger(m, c.aver_interfere_level)
  self:Finteger(m, c.highest_interfere_level)
  self:Finteger(m, c.highest_krypton_level)
  self:Finteger(m, c.aver_krypton_level)
  self:Finteger(m, c.r7_krypton_count)
  self:Finteger(m, c.r6_krypton_count)
  self:Finteger(m, c.fleet_highest_remould_level)
  self:Finteger(m, c.fleet_aver_remould_level)
  self:Finteger(m, c.intensify_level)
  self:Finteger(m, c.wdc_buff_time)
  self:Finteger(m, c.red_medal_count)
  self:Finteger(m, c.golden_medal_count)
  self:Finteger(m, c.purple_medal_count)
  self:Finteger(m, c.average_medal_level)
  self:Finteger(m, c.average_medal_rank)
  self:Finteger(m, c.highest_medal_level)
  self:Finteger(m, c.highest_medal_rank)
  self:Finteger(m, c.total_remodel_level)
  self:Finteger(m, c.total_tech_level)
end
function Paser:Fuser_info_resource(m, c)
  self:Finteger(m, c.credit)
  self:Fdouble(m, c.money)
  self:Finteger(m, c.technique)
  self:Finteger(m, c.lepton)
  self:Finteger(m, c.quark)
  self:Finteger(m, c.kenergy)
  self:Finteger(m, c.ac_supply)
  self:Finteger(m, c.laba_supply)
  self:Finteger(m, c.art_point)
  self:Finteger(m, c.medal_currency)
  self:Finteger(m, c.expedition)
end
function Paser:Fuser_info_other(m, c)
  self:Finteger(m, c.prestige)
  self:Finteger(m, c.achieve_total)
  self:Finteger(m, c.achieve_end)
  self:Finteger(m, c.plat_achieve_total)
  self:Finteger(m, c.plat_achieve_end)
  self:Finteger(m, c.gold_achieve_total)
  self:Finteger(m, c.gold_achieve_end)
  self:Finteger(m, c.silver_achieve_total)
  self:Finteger(m, c.silver_achieve_end)
  self:Finteger(m, c.bronze_achieve_total)
  self:Finteger(m, c.bronze_achieve_end)
  self:Finteger(m, c.achieve_credit_get)
end
function Paser:Fuser_info_ack(m, c)
  self:Fstring(m, c.name)
  self:Finteger(m, c.level)
  self:Finteger(m, c.champion)
  self:Finteger(m, c.military_rank)
  self:Fstring(m, c.alliance_name)
  self:Fuser_info_fleet(m, c.fleet)
  self:Fuser_info_force(m, c.force)
  self:Fuser_info_resource(m, c.resource)
  self:Fuser_info_other(m, c.other)
end
function Paser:Fprime_dot(m, c)
  self:Finteger(m, c.id)
  self:Fshort(m, c.dot_type)
  self:Fstring(m, c.ui_type)
  self:FArray(m, c.dot_coordinates, "short")
  self:Fstring(m, c.dot_name)
end
function Paser:Fprime_line(m, c)
  self:Finteger(m, c.id)
  self:FArray(m, c.dot_to_dot, "integer")
  self:Finteger(m, c.distance)
end
function Paser:Fprime_map_ack(m, c)
  self:FArray(m, c.dots, "prime_dot")
  self:FArray(m, c.lines, "prime_line")
end
function Paser:Fprime_dot_act(m, c)
  self:Finteger(m, c.id)
  self:Fshort(m, c.occupy)
  self:Fshort(m, c.occupy_no)
  self:Fshort(m, c.status)
end
function Paser:Fprime_march_act(m, c)
  self:Finteger(m, c.id)
  self:Fshort(m, c.begin_dot)
  self:Fshort(m, c.end_dot)
  self:Fshort(m, c.current)
  self:Fshort(m, c.speed)
  self:Fshort(m, c.status)
  self:Fshort(m, c.player_type)
  self:Finteger(m, c.prime_count)
  self:Finteger(m, c.monster_count)
end
function Paser:Fchange_info(m, c)
  self:Finteger(m, c.id)
  self:Finteger(m, c.change_percent)
end
function Paser:Fprime_my_info(m, c)
  self:Fshort(m, c.status)
  self:Fstring(m, c.pos)
  self:Fshort(m, c.power_left_time)
  self:FArray(m, c.change_list, "change_info")
  self:Finteger(m, c.left_free_jump_time)
  self:Finteger(m, c.total_free_jump_time)
  self:FArray(m, c.result_show_list, "integer")
end
function Paser:Fprime_active_ntf(m, c)
  self:FArray(m, c.dots, "prime_dot_act")
  self:FArray(m, c.lines, "prime_march_act")
  self:Fprime_my_info(m, c.my)
  self:Finteger(m, c.prime_dot_stay_time)
end
function Paser:Fprime_jump_req(m, c)
  self:Finteger(m, c.dest_id)
  self:FArray(m, c.line_ids, "integer")
  self:Finteger(m, c.distance)
end
function Paser:Fprime_dot_occupy_ntf(m, c)
  self:Finteger(m, c.id)
  self:Fshort(m, c.value)
end
function Paser:Fprime_push_show_round_req(m, c)
  self:Finteger(m, c.round_id)
end
function Paser:Fprime_dot_status_ntf(m, c)
  self:Finteger(m, c.id)
  self:Fshort(m, c.value)
end
function Paser:Fprime_march_ntf(m, c)
  self:FArray(m, c.marchs, "prime_march_act")
end
function Paser:Fprime_march_req(m, c)
  self:FArray(m, c.dots, "integer")
end
function Paser:Fprime_rank_player(m, c)
  self:Finteger(m, c.server_id)
  self:Finteger(m, c.user_id)
  self:Fstring(m, c.user_name)
  self:Finteger(m, c.rank)
  self:Fdouble(m, c.point)
end
function Paser:Fprime_rank(m, c)
  self:Finteger(m, c.type)
  self:Fprime_rank_player(m, c.self_rank)
  self:FArray(m, c.players, "prime_rank_player")
end
function Paser:Fprime_rank_ntf(m, c)
  self:FArray(m, c.ranks, "prime_rank")
end
function Paser:Fprime_fight_history_req(m, c)
  self:Finteger(m, c.server_id)
  self:Finteger(m, c.user_id)
end
function Paser:Fprime_fight_info(m, c)
  self:Finteger(m, c.report_id)
  self:Fstring(m, c.atker)
  self:Fstring(m, c.defender)
  self:Fboolean(m, c.is_win)
  self:Finteger(m, c.time)
end
function Paser:Fprime_fight_history_ack(m, c)
  self:FArray(m, c.fight_history, "prime_fight_info")
end
function Paser:Fprime_site_info_req(m, c)
  self:Finteger(m, c.site_id)
end
function Paser:Fprime_site_info_ack(m, c)
  self:Finteger(m, c.status)
  self:Finteger(m, c.team_amount)
  self:Fdouble(m, c.total_force)
  self:Finteger(m, c.reparing_amount)
  self:Finteger(m, c.left_time)
  self:Finteger(m, c.prime_amount)
  self:Finteger(m, c.monster_amount)
end
function Paser:Fprime_increase_power_info_ack(m, c)
  self:FArray(m, c.change_list, "change_info")
end
function Paser:Fmonster_group(m, c)
  self:Finteger(m, c.position)
  self:Finteger(m, c.amount)
  self:FArray(m, c.monsters, "integer")
  self:Fstring(m, c.desc_id)
end
function Paser:Fprime_enemy_info_ack(m, c)
  self:FArray(m, c.prime_monsters, "monster_group")
end
function Paser:Fprime_fight_report_req(m, c)
  self:Finteger(m, c.report_id)
end
function Paser:Fprime_last_report_req(m, c)
  self:Finteger(m, c.server_id)
  self:Finteger(m, c.user_id)
end
function Paser:Fprime_dot_occupy_no_ntf(m, c)
  self:Finteger(m, c.id)
  self:Fshort(m, c.value)
end
function Paser:Fkrypton_sort_req(m, c)
  self:Finteger(m, c.formation_id)
  self:Fshort(m, c.type)
end
function Paser:Fchampion_promote_ntf(m, c)
  self:Finteger(m, c.old)
  self:Finteger(m, c.current)
  self:FArray(m, c.award, "game_item")
end
function Paser:Fchampion_straight_info(m, c)
  self:Finteger(m, c.num)
  self:FArray(m, c.award, "game_item")
  self:Finteger(m, c.flag)
end
function Paser:Fchampion_straight_ack(m, c)
  self:FArray(m, c.ack, "champion_straight_info")
end
function Paser:Fchampion_straight_award_req(m, c)
  self:FArray(m, c.ids, "integer")
end
function Paser:Fprestige_info_ntf(m, c)
  self:Finteger(m, c.prestige)
  self:FArray(m, c.prestige_infos, "prestige_info")
end
function Paser:Fpay_sign_item(m, c)
  self:Finteger(m, c.day)
  self:Finteger(m, c.status)
  self:FArray(m, c.rewards, "game_item")
  self:Finteger(m, c.credit)
end
function Paser:Fpay_sign_ack(m, c)
  self:FArray(m, c.items, "pay_sign_item")
  self:FArray(m, c.recommands, "integer")
end
function Paser:Fpay_sign_reward_req(m, c)
  self:Finteger(m, c.day)
end
function Paser:Flevelup_unlock_info(m, c)
  self:Fstring(m, c.name)
  self:Finteger(m, c.level)
  self:Finteger(m, c.is_opened)
end
function Paser:Flevelup_unlock_ntf(m, c)
  self:FArray(m, c.ntf, "levelup_unlock_info")
end
function Paser:Fopen_server_fund_req(m, c)
  self:Finteger(m, c.num)
  self:Fshort(m, c.flag)
end
function Paser:Fopen_server_fund_info(m, c)
  self:Finteger(m, c.credit)
  self:Finteger(m, c.level)
  self:Finteger(m, c.flag)
end
function Paser:Fopen_server_fund_ack(m, c)
  self:Fboolean(m, c.does_charged)
  self:Finteger(m, c.credit)
  self:Finteger(m, c.acc_credit)
  self:Finteger(m, c.charged_amount)
  self:Finteger(m, c.vip)
  self:FArray(m, c.info, "open_server_fund_info")
end
function Paser:Fopen_server_fund_award_req(m, c)
  self:Finteger(m, c.level)
end
function Paser:Fall_welfare_req(m, c)
  self:Finteger(m, c.num)
  self:Fshort(m, c.flag)
end
function Paser:Fall_welfare_info(m, c)
  self:Finteger(m, c.num)
  self:FArray(m, c.award, "game_item")
  self:Finteger(m, c.flag)
end
function Paser:Fall_welfare_ack(m, c)
  self:Finteger(m, c.amount)
  self:Finteger(m, c.time)
  self:FArray(m, c.info, "all_welfare_info")
end
function Paser:Fall_welfare_award_req(m, c)
  self:Finteger(m, c.amount)
end
function Paser:Fequip_push_ntf(m, c)
  self:Finteger(m, c.fleet_id)
  self:Finteger(m, c.equip_type)
  self:Finteger(m, c.equip_slot)
  self:Fboolean(m, c.display)
end
function Paser:Fvip_level_ntf(m, c)
  self:FArray(m, c.vip_level_info, "vip_level_info")
end
function Paser:Fvip_level_info(m, c)
  self:Finteger(m, c.level)
  self:Finteger(m, c.need_exp)
end
function Paser:Ftemp_vip_ntf(m, c)
  self:Finteger(m, c.level)
  self:Finteger(m, c.left_time)
  self:Fboolean(m, c.show_banner)
end
function Paser:Fprime_award_req(m, c)
  self:Finteger(m, c.rank_type)
end
function Paser:Fprime_award(m, c)
  self:Finteger(m, c.id)
  self:Finteger(m, c.begin_rank)
  self:Finteger(m, c.end_rank)
  self:FArray(m, c.awards, "game_item")
end
function Paser:Fprime_award_lists(m, c)
  self:FArray(m, c.infoes, "prime_award")
end
function Paser:Fprime_round_result_ntf(m, c)
  self:Finteger(m, c.round_id)
  self:Finteger(m, c.result)
  self:Finteger(m, c.first_round_id)
  self:Finteger(m, c.current_round_id)
  self:Finteger(m, c.current_round_left_time)
  self:Finteger(m, c.next_round_id)
  self:Finteger(m, c.next_round_begin_time)
  self:Finteger(m, c.activity_left_time)
  self:Finteger(m, c.step_id)
  self:Finteger(m, c.step_left_time)
  self:Finteger(m, c.step_count)
end
function Paser:Fpay_push_pop_ntf(m, c)
  self:Finteger(m, c.can_pop)
end
function Paser:Fprime_time_wve_boss_left(m, c)
  self:Finteger(m, c.id)
  self:Finteger(m, c.left_time)
end
function Paser:Fprime_time_wve_boss_left_ntf(m, c)
  self:FArray(m, c.list, "prime_time_wve_boss_left")
end
function Paser:Fprime_do_wve_boss_left_ntf(m, c)
  self:FArray(m, c.id_list, "integer")
end
function Paser:Fprime_round_fight_ntf(m, c)
  self:Finteger(m, c.escape_rate)
end
function Paser:Fsell_item_ntf(m, c)
  self:Finteger(m, c.amount)
end
function Paser:Furl_push_ntf(m, c)
  self:Fstring(m, c.url)
end
function Paser:Fenhance_info_req(m, c)
  self:Fstring(m, c.equip_id)
end
function Paser:Fequip_attr(m, c)
  self:Fshort(m, c.type)
  self:Finteger(m, c.bassic)
  self:Finteger(m, c.mature)
end
function Paser:Fenhance_info_ack(m, c)
  self:Fshort(m, c.max_level)
  self:FArray(m, c.values, "equip_attr")
  self:Finteger(m, c.consume)
end
function Paser:Fcombo_guide_req(m, c)
  self:Finteger(m, c.state)
end
function Paser:Fcombo_guide_ack(m, c)
  self:Finteger(m, c.id)
  self:Finteger(m, c.activity_id)
end
function Paser:Fplayer_pay_price_req(m, c)
  self:Fstring(m, c.pay_price)
end
function Paser:Fpay_status_ntf(m, c)
  self:Finteger(m, c.pay_status)
end
function Paser:Fdaily_benefits_ack(m, c)
  self:FArray(m, c.info, "daily_benefits_info")
end
function Paser:Fdaily_benefits_ntf(m, c)
  self:FArray(m, c.info, "daily_benefits_info")
end
function Paser:Fdaily_benefits_info(m, c)
  self:Fstring(m, c.type)
  self:Finteger(m, c.level)
  self:FArray(m, c.award, "game_item")
  self:Finteger(m, c.current)
  self:Fshort(m, c.flag)
end
function Paser:Fdaily_benefits_award_req(m, c)
  self:Fstring(m, c.type)
  self:Finteger(m, c.level)
end
function Paser:Funlocked_seven_day_article(m, c)
  self:FArray(m, c.daily_gift, "daily_benefits_info")
  self:FArray(m, c.seven_day_goal, "string")
  self:FArray(m, c.half_buy_item, "store_item")
end
function Paser:Fseven_day_ntf(m, c)
  self:Finteger(m, c.open_status)
  self:FArray(m, c.awards_status, "integer")
  self:Finteger(m, c.activity_left_time)
  self:Finteger(m, c.award_left_time)
end
function Paser:Fseven_day_goal_subtask(m, c)
  self:Finteger(m, c.id)
  self:Finteger(m, c.status)
  self:Finteger(m, c.finished)
  self:Finteger(m, c.total)
  self:FArray(m, c.awards, "game_item")
end
function Paser:Fseven_day_goal_task(m, c)
  self:Fstring(m, c.type)
  self:Finteger(m, c.locked)
  self:Finteger(m, c.status)
  self:FArray(m, c.subtasks, "seven_day_goal_subtask")
end
function Paser:Fgoal_info_ack(m, c)
  self:FArray(m, c.task_list, "seven_day_goal_task")
end
function Paser:Fget_goal_award_req(m, c)
  self:Finteger(m, c.task_id)
end
function Paser:Fcutoff_info_ack(m, c)
  self:FArray(m, c.cutoff_item_list, "store_item")
end
function Paser:Fbuy_cutoff_item_req(m, c)
  self:Finteger(m, c.id)
end
function Paser:Fget_goal_award_ack(m, c)
  self:Finteger(m, c.status)
  self:Fseven_day_goal_task(m, c.task_info)
end
function Paser:Fenter_seven_day_ack(m, c)
  self:Funlocked_seven_day_article(m, c.today_unlock)
end
function Paser:Fstore_quick_buy_count_req(m, c)
  self:Finteger(m, c.store_id)
  self:Finteger(m, c.id)
end
function Paser:Fstore_quick_buy_price_req(m, c)
  self:Finteger(m, c.store_id)
  self:Finteger(m, c.id)
  self:Finteger(m, c.buy_times)
end
function Paser:Fstore_quick_buy_price_ack(m, c)
  self:Fstring(m, c.nor_price)
  self:Fstring(m, c.dis_price)
  self:Finteger(m, c.buy_times)
end
function Paser:Fprime_jump_price_ack(m, c)
  self:Finteger(m, c.price_min)
  self:Finteger(m, c.price_max)
  self:Ffloat(m, c.price_unit)
end
function Paser:Fselect_goal_type_req(m, c)
  self:Fstring(m, c.type)
end
function Paser:Factivity_store_count_req(m, c)
  self:Finteger(m, c.store_type)
  self:Finteger(m, c.id)
end
function Paser:Fcost_attr(m, c)
  self:Finteger(m, c.item_id)
  self:Finteger(m, c.cost)
end
function Paser:Factivity_store_count_ack(m, c)
  self:FArray(m, c.all_cost, "cost_attr")
  self:Finteger(m, c.max_times)
  self:Finteger(m, c.times)
end
function Paser:Factivity_store_price_req(m, c)
  self:Finteger(m, c.store_type)
  self:Finteger(m, c.id)
  self:Finteger(m, c.buy_times)
end
function Paser:Factivity_store_price_ack(m, c)
  self:FArray(m, c.all_cost, "cost_attr")
end
function Paser:Fgp_test_pay_group_req(m, c)
  self:Finteger(m, c.join_test)
  self:Finteger(m, c.join_group)
end
function Paser:Fmax_step_req(m, c)
  self:Finteger(m, c.type)
end
function Paser:Fmax_step_ack(m, c)
  self:Finteger(m, c.max)
  self:Finteger(m, c.award_step)
end
function Paser:Fladder_report_req(m, c)
  self:Finteger(m, c.step)
end
function Paser:Fladder_report_ack(m, c)
  self:Fstring(m, c.time_player_name)
  self:Fstring(m, c.time_report)
  self:Fstring(m, c.level_player_name)
  self:Fstring(m, c.level_report)
end
function Paser:Fladder_award_req(m, c)
  self:Finteger(m, c.type)
  self:Finteger(m, c.step)
end
function Paser:Fladder_award_ntf(m, c)
  self:Finteger(m, c.type)
  self:FArray(m, c.info, "ladder_special_info")
end
function Paser:Fladder_jump_req(m, c)
  self:Finteger(m, c.type)
  self:Finteger(m, c.step)
end
function Paser:Fladder_jump_ack(m, c)
  self:Fladder_info(m, c.info)
end
function Paser:Fladder_report_detail_req(m, c)
  self:Fstring(m, c.id)
end
function Paser:Fladder_report_detail_ack(m, c)
  self:Ffight_report(m, c.report)
end
function Paser:Frefresh_type_ack(m, c)
  self:Fshort(m, c.refresh_type)
  self:Finteger(m, c.max_free_times)
  self:Finteger(m, c.cur_free_times)
  self:Finteger(m, c.credit_cost)
  self:Finteger(m, c.item_type)
  self:Finteger(m, c.item_num)
  self:Finteger(m, c.item_cost)
  self:Finteger(m, c.price_off_status)
end
function Paser:Fexpedition_refresh_type_ack(m, c)
  self:Fshort(m, c.refresh_type)
  self:Finteger(m, c.max_free_times)
  self:Finteger(m, c.cur_free_times)
  self:Finteger(m, c.currency_cost)
  self:Finteger(m, c.price_off_status)
end
function Paser:Fstores_buy_ack(m, c)
  self:FArray(m, c.award, "game_item")
  self:Fstore_item(m, c.item)
end
function Paser:Fforce_add_fleet_req(m, c)
  self:Fshort(m, c.type)
end
function Paser:Funlock_condition(m, c)
  self:Finteger(m, c.benefit)
  self:Finteger(m, c.level)
  self:Finteger(m, c.vip)
  self:FArray(m, c.cost, "game_item")
end
function Paser:Ftactical_info(m, c)
  self:Finteger(m, c.equip_num)
  self:Finteger(m, c.equip_max)
  self:Funlock_condition(m, c.unlock_slot)
  self:FArray(m, c.slots, "tactical_slot")
end
function Paser:Ftactical_slot(m, c)
  self:Finteger(m, c.vessel)
  self:Fstring(m, c.equip_id)
  self:FArray(m, c.equips, "tactical_equip")
  self:Finteger(m, c.red_point)
end
function Paser:Ftactical_equip(m, c)
  self:Fstring(m, c.id)
  self:Finteger(m, c.type)
  self:Finteger(m, c.rank)
  self:Finteger(m, c.vessel)
  self:Fboolean(m, c.is_equip)
  self:FArray(m, c.decompose, "game_item")
  self:FArray(m, c.possible_attrs, "possible_attr")
  self:FArray(m, c.equip_attrs, "tactical_equip_attr")
end
function Paser:Fpossible_attr(m, c)
  self:Finteger(m, c.id)
  self:Finteger(m, c.min)
  self:Finteger(m, c.max)
end
function Paser:Ftactical_equip_attr(m, c)
  self:Finteger(m, c.id)
  self:Finteger(m, c.type)
  self:Finteger(m, c.color)
  self:FArray(m, c.effects, "tactical_equip_effect")
end
function Paser:Ftactical_equip_effect(m, c)
  self:Finteger(m, c.effect_vessel)
  self:Finteger(m, c.value)
  self:Finteger(m, c.min)
  self:Finteger(m, c.max)
end
function Paser:Ftactical_equip_on_req(m, c)
  self:Finteger(m, c.vessel)
  self:Fstring(m, c.equip_id)
end
function Paser:Ftactical_equip_on_ack(m, c)
  self:Finteger(m, c.code)
  self:Finteger(m, c.vessel)
  self:Fstring(m, c.new_equip_id)
  self:Fstring(m, c.old_equip_id)
end
function Paser:Ftactical_equip_off_req(m, c)
  self:Finteger(m, c.vessel)
end
function Paser:Ftactical_equip_off_ack(m, c)
  self:Finteger(m, c.code)
  self:Finteger(m, c.vessel)
  self:Fstring(m, c.equip_id)
end
function Paser:Ftactical_resource_ntf(m, c)
  self:Finteger(m, c.used_times)
  self:Finteger(m, c.max_times)
  self:Finteger(m, c.next_refresh)
end
function Paser:Ftactical_refine_info(m, c)
  self:Fstring(m, c.equip_id)
  self:Finteger(m, c.times)
  self:FArray(m, c.locked_attrs, "integer")
end
function Paser:Ftactical_refine_price_ack(m, c)
  self:Finteger(m, c.code)
  self:Finteger(m, c.lock_price)
  self:Finteger(m, c.lock_cost)
  self:Finteger(m, c.refine_price)
end
function Paser:Ftactical_refine_ack(m, c)
  self:Finteger(m, c.code)
  self:Finteger(m, c.cost_credit)
  self:Finteger(m, c.cost_item)
  self:FArray(m, c.results, "refine_result")
end
function Paser:Frefine_result(m, c)
  self:Finteger(m, c.refine_id)
  self:FArray(m, c.equip_attrs, "tactical_equip_attr")
end
function Paser:Ftactical_refine_save_req(m, c)
  self:Fstring(m, c.equip_id)
  self:Finteger(m, c.refine_id)
end
function Paser:Ftactical_unlock_slot_ack(m, c)
  self:Finteger(m, c.code)
  self:Funlock_condition(m, c.next)
end
function Paser:Ftactical_enter_refine_req(m, c)
  self:Fstring(m, c.equip_id)
end
function Paser:Ftactical_enter_refine_ack(m, c)
  self:Fstring(m, c.equip_id)
  self:Finteger(m, c.used_times)
  self:Finteger(m, c.max_times)
  self:Finteger(m, c.lock_num)
  self:Finteger(m, c.lock_max)
  self:Funlock_condition(m, c.unlock_lock_attr)
  self:FArray(m, c.lock_attr_price, "integer")
  self:FArray(m, c.lock_attr_cost, "integer")
  self:Fgame_item(m, c.need_item)
  self:Fgame_item(m, c.lock_item)
  self:FArray(m, c.last_refines, "refine_result")
end
function Paser:Ftactical_status_ntf(m, c)
  self:Finteger(m, c.status)
end
function Paser:Ftactical_equip_delete_req(m, c)
  self:Fstring(m, c.equip_id)
end
function Paser:Ftactical_equip_delete_ack(m, c)
  self:Finteger(m, c.code)
  self:Fstring(m, c.equip_id)
end
function Paser:Flimit_ntf(m, c)
  self:Fshort(m, c.level)
  self:Fshort(m, c.is_open)
  self:Fstring(m, c.pay_type)
end
function Paser:Fpay_limit_ntf(m, c)
  self:FArray(m, c.pay_limit_infos, "limit_ntf")
end
function Paser:Fpay_sign_board_ntf(m, c)
  self:FArray(m, c.boards, "string")
end
function Paser:Fstar_item(m, c)
  self:Fgame_item(m, c.award)
  self:Finteger(m, c.award_status)
  self:Finteger(m, c.big_or_norm)
end
function Paser:Fstar_info(m, c)
  self:Finteger(m, c.star_id)
  self:FArray(m, c.all_award, "star_item")
  self:Fstring(m, c.cost_type)
  self:Finteger(m, c.cost_num)
  self:Finteger(m, c.cdtimes)
end
function Paser:Fenter_plante_ack(m, c)
  self:Finteger(m, c.item_id)
  self:Finteger(m, c.star_id)
  self:Finteger(m, c.refresh_cost)
  self:FArray(m, c.star, "star_info")
end
function Paser:Fplante_explore_req(m, c)
  self:Finteger(m, c.activity_id)
  self:Finteger(m, c.star_id)
  self:Finteger(m, c.state)
end
function Paser:Fplante_explore_ack(m, c)
  self:Finteger(m, c.award_id)
  self:Finteger(m, c.star_id)
  self:Fstring(m, c.cost_type)
  self:Finteger(m, c.cost_num)
  self:Finteger(m, c.cd_time)
  self:Finteger(m, c.error_code)
end
function Paser:Fenter_plante_req(m, c)
  self:Finteger(m, c.activity_id)
end
function Paser:Fplante_refresh_req(m, c)
  self:Finteger(m, c.activity_id)
end
function Paser:Fplante_refresh_ack(m, c)
  self:Finteger(m, c.item_id)
  self:Finteger(m, c.star_id)
  self:Finteger(m, c.refresh_cost)
  self:FArray(m, c.star, "star_info")
end
function Paser:Fshow_price_ntf(m, c)
  self:Finteger(m, c.status)
end
function Paser:Fget_invite_gift_req(m, c)
  self:Finteger(m, c.number)
  self:Finteger(m, c.total)
  self:Finteger(m, c.pre_percent)
  self:Finteger(m, c.cur_percent)
  self:FArray(m, c.friends, "string")
end
function Paser:Fget_invite_gift_ack(m, c)
  self:Finteger(m, c.type)
  self:Finteger(m, c.strike)
  self:FArray(m, c.awards, "game_item")
end
function Paser:Ffacebook_switch_ntf(m, c)
  self:FArray(m, c.bind_awards, "game_item")
  self:Finteger(m, c.story_image_max)
  self:Finteger(m, c.story_title_max)
  self:Finteger(m, c.ball_loop_seconds)
  self:Finteger(m, c.ball_loop_times)
  self:FArray(m, c.switches, "pair")
end
function Paser:Ffacebook_story_ntf(m, c)
  self:FArray(m, c.switches, "pair")
end
function Paser:Fstory_award(m, c)
  self:Finteger(m, c.story_id)
  self:Finteger(m, c.strike)
  self:FArray(m, c.awards, "game_item")
end
function Paser:Fstory_status_ntf(m, c)
  self:FArray(m, c.story_list, "pair")
  self:FArray(m, c.story_awards, "story_award")
end
function Paser:Fstory_award_req(m, c)
  self:Finteger(m, c.story_id)
  self:Finteger(m, c.fleet_id)
end
function Paser:Fstory_award_ack(m, c)
  self:Finteger(m, c.code)
end
function Paser:Ffleet_can_enhance(m, c)
  self:Finteger(m, c.identity)
  self:Fboolean(m, c.can_enhance)
end
function Paser:Fcan_enhance_ack(m, c)
  self:FArray(m, c.fleet_info, "fleet_can_enhance")
end
function Paser:Ftactical_delete_awards_req(m, c)
  self:Fstring(m, c.equip_id)
end
function Paser:Ftactical_delete_awards_ack(m, c)
  self:FArray(m, c.awards, "game_item")
end
function Paser:Ffirst_login_ntf(m, c)
  self:Finteger(m, c.status)
end
function Paser:Falliance_guid_ntf(m, c)
  self:Fboolean(m, c.is_guid)
  self:Fgame_item(m, c.award)
end
function Paser:Fltv_ajust(m, c)
  self:Fstring(m, c.type)
  self:Fstring(m, c.status)
end
function Paser:Fltv_ajust_ntf(m, c)
  self:FArray(m, c.ltv_ajusts, "ltv_ajust")
end
function Paser:Fthumb_up_info_ack(m, c)
  self:FArray(m, c.fleets_thumbs, "pair")
end
function Paser:Fthumb_up_req(m, c)
  self:Finteger(m, c.id)
end
function Paser:Fthumb_up_ack(m, c)
  self:Finteger(m, c.code)
end
function Paser:Fcredit_exchange_ack(m, c)
  self:Finteger(m, c.api)
  self:Fgame_item(m, c.item)
  self:Finteger(m, c.credit)
end
function Paser:Fcredit_exchange_ntf(m, c)
  self:Finteger(m, c.api)
  self:Fgame_item(m, c.item)
  self:Finteger(m, c.credit)
end
function Paser:Ftoday_task_info(m, c)
  self:Fstring(m, c.id)
  self:Fstring(m, c.task_name)
  self:Fboolean(m, c.is_get_reward)
  self:Fboolean(m, c.is_finished)
  self:FArray(m, c.rewards, "game_item")
  self:FArray(m, c.task_content, "sub_task_info")
  self:Fshort(m, c.reward_number)
end
function Paser:Ftoday_act_task_ack(m, c)
  self:Ftoday_task_info(m, c.task)
  self:Fstring(m, c.word)
  self:Fstring(m, c.background)
  self:Fstring(m, c.title_font)
  self:Fstring(m, c.banner_font)
end
function Paser:Fenter_monthly_ack(m, c)
  self:FArray(m, c.each_day, "daily_task_info")
  self:Finteger(m, c.max)
  self:Finteger(m, c.current)
end
function Paser:Fdaily_task_info(m, c)
  self:Fstring(m, c.id)
  self:Fboolean(m, c.is_today)
  self:Fstring(m, c.background)
  self:Finteger(m, c.max_task_count)
  self:Finteger(m, c.current_task_count)
  self:Fboolean(m, c.can_redo)
end
function Paser:Fview_each_task_req(m, c)
  self:Fstring(m, c.id)
end
function Paser:Fcost_arrary(m, c)
  self:Finteger(m, c.once_cost)
  self:Finteger(m, c.each_cost)
end
function Paser:Fview_each_task_ack(m, c)
  self:Ftoday_task_info(m, c.task)
  self:Fcost_arrary(m, c.cost)
end
function Paser:Fview_award(m, c)
  self:Finteger(m, c.id)
  self:Finteger(m, c.count)
  self:FArray(m, c.rewards, "game_item")
  self:Fboolean(m, c.is_receive)
end
function Paser:Fview_award_ack(m, c)
  self:FArray(m, c.award, "view_award")
end
function Paser:Fget_award_req(m, c)
  self:Finteger(m, c.id)
  self:Finteger(m, c.count)
end
function Paser:Fget_award_ack(m, c)
  self:Finteger(m, c.code)
end
function Paser:Fredo_task_req(m, c)
  self:Fstring(m, c.id)
  self:Finteger(m, c.sub_id)
end
function Paser:Fredo_task_ack(m, c)
  self:Finteger(m, c.code)
end
function Paser:Fget_today_award_req(m, c)
  self:Fstring(m, c.id)
  self:Finteger(m, c.sub_id)
end
function Paser:Fget_today_award_ack(m, c)
  self:Finteger(m, c.code)
end
function Paser:Fother_monthly_req(m, c)
  self:Fstring(m, c.id)
end
function Paser:Fother_monthly_ack(m, c)
  self:FArray(m, c.each_day, "daily_task_info")
end
function Paser:Fswitch_formation_req(m, c)
  self:Finteger(m, c.formation_id)
end
function Paser:Fswitch_formation_ack(m, c)
  self:Finteger(m, c.code)
  self:Ffleets_info(m, c.fleet)
  self:FArray(m, c.fleetkryptons, "fleet_krypton_info")
end
function Paser:Fmulmatrix_switch_req(m, c)
  self:Finteger(m, c.id)
  self:Fboolean(m, c.switch)
end
function Paser:Fmulmatrix_switch_ack(m, c)
  self:Finteger(m, c.code)
end
function Paser:Fmulmatrix_equip_info_req(m, c)
  self:Finteger(m, c.id)
end
function Paser:Fmulmatrix_equip_info_ack(m, c)
  self:Finteger(m, c.code)
end
function Paser:Fchange_matrix_type_req(m, c)
  self:Fpair(m, c.matrix)
end
function Paser:Fchange_matrix_type_ack(m, c)
  self:Finteger(m, c.code)
  self:FArray(m, c.matrixs_purpose, "pair")
end
function Paser:Fbag_size_info(m, c)
  self:Finteger(m, c.title_type)
  self:Finteger(m, c.item_num)
  self:Finteger(m, c.show_max)
end
function Paser:Fuser_bag_info_ntf(m, c)
  self:FArray(m, c.bags, "bag_size_info")
end
function Paser:Fclient_req_stat_info(m, c)
  self:Fstring(m, c.open_udid)
  self:Finteger(m, c.user_id)
  self:Fstring(m, c.created_at)
  self:Fstring(m, c.ip)
  self:Fstring(m, c.device_type)
  self:Finteger(m, c.time_consuming)
end
function Paser:Fbudo_change_formation_req(m, c)
  self:Finteger(m, c.id)
end
function Paser:Fbugly_data_ntf(m, c)
  self:Fboolean(m, c.is_open)
  self:Fboolean(m, c.lua_error_open)
  self:Fboolean(m, c.frame60_open)
  self:Fboolean(m, c.half_body_open)
end
function Paser:Fchampion_status_info(m, c)
  self:Finteger(m, c.supply)
  self:Finteger(m, c.max_supply)
  self:Finteger(m, c.cdtime)
  self:Fshort(m, c.status)
end
function Paser:Fenter_champion_ack(m, c)
  self:Fchampion_status_info(m, c.local_champion)
  self:Fchampion_status_info(m, c.world_champion)
  self:Fchampion_status_info(m, c.tlc_champion)
end
function Paser:Fwdc_fleet(m, c)
  self:Fshort(m, c.identity)
  self:Fshort(m, c.level)
end
function Paser:Fwdc_player_brief(m, c)
  self:Finteger(m, c.server_id)
  self:Fstring(m, c.user_id)
  self:Fstring(m, c.server_name)
  self:Fstring(m, c.user_name)
  self:Fshort(m, c.sex)
  self:Fshort(m, c.icon)
  self:Fshort(m, c.level)
  self:Fshort(m, c.flevel)
  self:Finteger(m, c.point)
  self:Finteger(m, c.ranking)
  self:Finteger(m, c.rank)
  self:Fstring(m, c.rank_img)
  self:Finteger(m, c.rank_name)
  self:Fdouble(m, c.force)
  self:FArray(m, c.fleets, "wdc_fleet")
  self:Finteger(m, c.atk_wins)
  self:Finteger(m, c.def_wins)
  self:Finteger(m, c.best_socre)
  self:Finteger(m, c.best_ranking)
  self:Finteger(m, c.best_season)
end
function Paser:Ftlc_fleets(m, c)
  self:FArray(m, c.fleets, "tlc_fleet")
end
function Paser:Ftlc_fleet(m, c)
  self:Fshort(m, c.identity)
  self:Fshort(m, c.level)
end
function Paser:Ftlc_player_brief(m, c)
  self:Finteger(m, c.server_id)
  self:Fstring(m, c.user_id)
  self:Fstring(m, c.server_name)
  self:Fstring(m, c.user_name)
  self:Fshort(m, c.sex)
  self:Fshort(m, c.icon)
  self:Fshort(m, c.level)
  self:Fshort(m, c.flevel)
  self:Finteger(m, c.point)
  self:Finteger(m, c.ranking)
  self:Finteger(m, c.rank)
  self:Fstring(m, c.rank_img)
  self:Finteger(m, c.rank_name)
  self:Fdouble(m, c.force)
  self:FArray(m, c.fleets, "tlc_fleets")
  self:Finteger(m, c.atk_wins)
  self:Finteger(m, c.def_wins)
  self:Finteger(m, c.best_socre)
  self:Finteger(m, c.best_ranking)
  self:Finteger(m, c.best_season)
end
function Paser:Fwdc_challenge_info(m, c)
  self:Fwdc_player_brief(m, c.defender)
  self:Finteger(m, c.win_score)
  self:Finteger(m, c.lose_score)
end
function Paser:Fwdc_player_info(m, c)
  self:Finteger(m, c.point)
  self:Finteger(m, c.rank_point)
  self:Finteger(m, c.next_rank_point)
  self:Finteger(m, c.ranking)
  self:Finteger(m, c.using_ranking)
  self:Finteger(m, c.using_rank)
  self:Finteger(m, c.rank)
  self:Fstring(m, c.rank_img)
  self:Finteger(m, c.rank_name)
  self:Finteger(m, c.protect_time)
  self:Finteger(m, c.final_time)
  self:Finteger(m, c.end_time)
  self:Finteger(m, c.res_per_hour)
  self:Fdouble(m, c.force)
end
function Paser:Fwdc_fight_report(m, c)
  self:Fstring(m, c.report_id)
  self:Fwdc_player_brief(m, c.enemy)
  self:Fboolean(m, c.is_win)
  self:Fboolean(m, c.can_revenge)
  self:Finteger(m, c.score_before)
  self:Finteger(m, c.score_after)
  self:Finteger(m, c.fight_time)
end
function Paser:Fwdc_award(m, c)
  self:Finteger(m, c.id)
  self:Finteger(m, c.rank_level)
  self:Fstring(m, c.rank_img)
  self:Finteger(m, c.rank_name)
  self:Finteger(m, c.begin_rank)
  self:Finteger(m, c.end_rank)
  self:FArray(m, c.awards, "game_item")
  self:FArray(m, c.benefits, "game_item")
  self:FArray(m, c.buffers, "pair")
end
function Paser:Ftlc_award(m, c)
  self:Finteger(m, c.id)
  self:Finteger(m, c.rank_level)
  self:Fstring(m, c.rank_img)
  self:Finteger(m, c.rank_name)
  self:Finteger(m, c.begin_rank)
  self:Finteger(m, c.end_rank)
  self:FArray(m, c.awards, "game_item")
  self:FArray(m, c.benefits, "game_item")
  self:FArray(m, c.buffers, "pair")
end
function Paser:Fwdc_info(m, c)
  self:Fgame_item(m, c.currency)
  self:Fboolean(m, c.can_buy)
  self:Finteger(m, c.search_cost)
  self:Finteger(m, c.max_supply)
  self:Fboolean(m, c.pop_report)
  self:Finteger(m, c.king_num)
  self:Fwdc_player_info(m, c.player_info)
  self:FArray(m, c.buffers, "pair")
end
function Paser:Fwdc_info_ntf(m, c)
  self:Fwdc_info(m, c.info)
end
function Paser:Fwdc_enter_ack(m, c)
  self:Fwdc_info(m, c.info)
end
function Paser:Fwdc_match_ack(m, c)
  self:Finteger(m, c.code)
  self:FArray(m, c.challenge_list, "wdc_challenge_info")
end
function Paser:Fwdc_fight_req(m, c)
  self:Finteger(m, c.server_id)
  self:Fstring(m, c.user_id)
  self:Finteger(m, c.revenge_id)
end
function Paser:Fwdc_fight_ack(m, c)
  self:Finteger(m, c.code)
  self:Fwdc_fight_report(m, c.result)
  self:Fdouble(m, c.damage)
  self:Fdouble(m, c.suffer_damage)
  self:Ffight_report(m, c.report)
  self:FArray(m, c.awards, "game_item")
end
function Paser:Fwdc_report_ack(m, c)
  self:FArray(m, c.atk_list, "wdc_fight_report")
  self:FArray(m, c.defend_list, "wdc_fight_report")
end
function Paser:Ftlc_report_ack(m, c)
  self:FArray(m, c.atk_list, "tlc_fight_report")
  self:FArray(m, c.defend_list, "tlc_fight_report")
end
function Paser:Fwdc_report_detail_req(m, c)
  self:Fstring(m, c.id)
end
function Paser:Fwdc_report_detail_ack(m, c)
  self:Ffight_report(m, c.report)
end
function Paser:Fwdc_awards_ack(m, c)
  self:Finteger(m, c.season)
  self:Finteger(m, c.score)
  self:Finteger(m, c.rank)
  self:Finteger(m, c.ranking)
  self:Finteger(m, c.rank_level)
  self:Fstring(m, c.rank_img)
  self:Finteger(m, c.rank_name)
  self:Finteger(m, c.left_time)
  self:FArray(m, c.ranking_awards, "wdc_award")
  self:FArray(m, c.class_awards, "wdc_award")
end
function Paser:Ftlc_awards_ack(m, c)
  self:Finteger(m, c.season)
  self:Finteger(m, c.score)
  self:Finteger(m, c.rank)
  self:Finteger(m, c.ranking)
  self:Finteger(m, c.rank_level)
  self:Fstring(m, c.rank_img)
  self:Finteger(m, c.rank_name)
  self:Finteger(m, c.left_time)
  self:FArray(m, c.ranking_awards, "tlc_award")
  self:FArray(m, c.class_awards, "tlc_award")
end
function Paser:Fwdc_server_honor(m, c)
  self:Finteger(m, c.season)
  self:Finteger(m, c.start_date)
  self:Finteger(m, c.end_date)
  self:FArray(m, c.top_players, "wdc_player_brief")
end
function Paser:Ftlc_server_honor(m, c)
  self:Finteger(m, c.season)
  self:Finteger(m, c.start_date)
  self:Finteger(m, c.end_date)
  self:FArray(m, c.top_players, "tlc_player_brief")
end
function Paser:Ftlc_player_honor(m, c)
  self:Finteger(m, c.season)
  self:Finteger(m, c.start_date)
  self:Finteger(m, c.end_date)
  self:Finteger(m, c.ranking)
  self:Finteger(m, c.rank)
  self:Fstring(m, c.rank_img)
  self:Finteger(m, c.score)
end
function Paser:Fwdc_player_honor(m, c)
  self:Finteger(m, c.season)
  self:Finteger(m, c.start_date)
  self:Finteger(m, c.end_date)
  self:Finteger(m, c.ranking)
  self:Finteger(m, c.rank)
  self:Fstring(m, c.rank_img)
  self:Finteger(m, c.score)
end
function Paser:Fwdc_rank_board_ack(m, c)
  self:Fwdc_player_brief(m, c.self)
  self:Finteger(m, c.world_ranking)
  self:Finteger(m, c.rank_level)
  self:Fstring(m, c.rank_img)
  self:Finteger(m, c.rank_name)
  self:Finteger(m, c.rank_ceil)
  self:FArray(m, c.top_list, "wdc_player_brief")
  self:FArray(m, c.class_list, "wdc_player_brief")
end
function Paser:Fwdc_honor_wall_ack(m, c)
  self:FArray(m, c.all, "wdc_server_honor")
  self:FArray(m, c.self, "wdc_player_honor")
end
function Paser:Ftlc_honor_wall_ack(m, c)
  self:FArray(m, c.all, "tlc_server_honor")
  self:FArray(m, c.self, "tlc_player_honor")
end
function Paser:Fwdc_awards_ntf(m, c)
  self:FArray(m, c.awards, "wdc_award")
end
function Paser:Fwdc_awards_get_req(m, c)
  self:Finteger(m, c.id)
end
function Paser:Fwdc_awards_get_ack(m, c)
  self:Finteger(m, c.code)
  self:Finteger(m, c.id)
end
function Paser:Fwdc_rankup_ntf(m, c)
  self:Finteger(m, c.rank_name)
  self:Fstring(m, c.rank_img)
  self:Finteger(m, c.score)
  self:Finteger(m, c.next_rank_name)
  self:Finteger(m, c.next_rank_score)
end
function Paser:Fwdc_fight_status_ack(m, c)
  self:Finteger(m, c.protect_time)
end
function Paser:Fwdc_status_ntf(m, c)
  self:Finteger(m, c.status)
end
function Paser:Fwelcome_screen_req(m, c)
  self:Finteger(m, c.logic_id)
  self:Fstring(m, c.language)
end
function Paser:Fwelcome_screen_item(m, c)
  self:Finteger(m, c.id)
  self:Fstring(m, c.type)
  self:Fstring(m, c.func_name)
  self:Fstring(m, c.content)
  self:Finteger(m, c.order)
  self:Finteger(m, c.left_time)
end
function Paser:Fwelcome_screen_ack(m, c)
  self:FArray(m, c.screen_items, "welcome_screen_item")
  self:FArray(m, c.png_list, "string")
end
function Paser:Fwelcome_mask_req(m, c)
  self:Finteger(m, c.id)
end
function Paser:Ftranslate_info_ack(m, c)
  self:Fstring(m, c.token)
  self:Fstring(m, c.url)
end
function Paser:Ffirst_charge_item(m, c)
  self:Fstring(m, c.product_id)
  self:Fboolean(m, c.first_buy)
  self:Finteger(m, c.fleet_id)
  self:Fboolean(m, c.own_fleet)
  self:Fgame_item(m, c.replace_item)
  self:FArray(m, c.awards, "game_item")
  self:Ffleet_show_info(m, c.show_info)
  self:Ffleet(m, c.show_detail)
end
function Paser:Ffirst_charge_ntf(m, c)
  self:FArray(m, c.charge_items, "first_charge_item")
end
function Paser:Fenter_dice_req(m, c)
  self:Finteger(m, c.id)
end
function Paser:Fdice_cell(m, c)
  self:Finteger(m, c.cell)
  self:Fshort(m, c.type)
  self:Fgame_item(m, c.show_item)
  self:FArray(m, c.loots, "game_item")
  self:Finteger(m, c.status)
end
function Paser:Fdice_detail(m, c)
  self:Finteger(m, c.current_cell)
  self:FArray(m, c.cells, "dice_cell")
  self:Fgame_item(m, c.throw_cost)
  self:Fgame_item(m, c.refresh_cost)
end
function Paser:Fthrow_dice_req(m, c)
  self:Finteger(m, c.id)
  self:Finteger(m, c.buy_type)
end
function Paser:Fthrow_dice_ack(m, c)
  self:Finteger(m, c.current_cell)
  self:Finteger(m, c.dice_value)
  self:FArray(m, c.update_cells, "dice_cell")
  self:Fgame_item(m, c.throw_cost)
  self:Fgame_item(m, c.refresh_cost)
end
function Paser:Fdice_awards_req(m, c)
  self:Finteger(m, c.id)
  self:Finteger(m, c.cell)
end
function Paser:Freset_dice_req(m, c)
  self:Finteger(m, c.id)
  self:Finteger(m, c.buy_type)
end
function Paser:Fspeed_activity(m, c)
  self:Finteger(m, c.type)
  self:Finteger(m, c.status)
  self:Finteger(m, c.dungeon_id)
  self:Fgame_item(m, c.cost_item)
end
function Paser:Fspeed_resource_ack(m, c)
  self:FArray(m, c.items, "speed_activity")
end
function Paser:Ffinish_speed_task_req(m, c)
  self:Finteger(m, c.type)
end
function Paser:Fdice_board_info(m, c)
  self:Fstring(m, c.user)
  self:Fgame_item(m, c.award)
end
function Paser:Fdice_board_ntf(m, c)
  self:FArray(m, c.boards, "dice_board_info")
end
function Paser:Fwdc_rank_board_ntf(m, c)
  self:Fshort(m, c.type)
  self:FArray(m, c.players, "wdc_player_brief")
end
function Paser:Ftlc_rank_board_ntf(m, c)
  self:Fshort(m, c.type)
  self:FArray(m, c.players, "tlc_player_brief")
end
function Paser:Fevent_sub_task(m, c)
  self:Fdouble(m, c.id)
  self:Fstring(m, c.type)
  self:Finteger(m, c.finished_num)
  self:Finteger(m, c.total_num)
  self:Finteger(m, c.point)
  self:FArray(m, c.awards, "game_item")
  self:Finteger(m, c.status)
  self:Fstring(m, c.group)
  self:Fstring(m, c.title)
  self:Fstring(m, c.desc)
end
function Paser:Fevent_progress_award(m, c)
  self:Finteger(m, c.number)
  self:FArray(m, c.awards, "game_item")
  self:Finteger(m, c.status)
end
function Paser:Fevent_task(m, c)
  self:Fdouble(m, c.task_id)
  self:FArray(m, c.sub_tasks, "event_sub_task")
  self:Finteger(m, c.finished_num)
  self:Finteger(m, c.total_num)
  self:FArray(m, c.progress_awards, "event_progress_award")
  self:Fstring(m, c.label)
  self:Finteger(m, c.start_time)
  self:Finteger(m, c.end_time)
  self:Fstring(m, c.task_name)
end
function Paser:Fevent_area_color(m, c)
  self:Fstring(m, c.area_name)
  self:Fstring(m, c.color)
end
function Paser:Fevent_activity_info(m, c)
  self:Fdouble(m, c.id)
  self:Fstring(m, c.label_img)
  self:Fstring(m, c.icon)
  self:Fstring(m, c.background_image)
  self:FArray(m, c.all_tasks, "event_task")
  self:Fstring(m, c.left_image)
  self:Fstring(m, c.right_image)
  self:Fstring(m, c.progressbar_bg)
  self:Fboolean(m, c.localres)
  self:FArray(m, c.color, "event_area_color")
  self:Fstring(m, c.icon_name)
end
function Paser:Fevent_activity_ntf(m, c)
  self:Finteger(m, c.open_status)
  self:Finteger(m, c.gloden_show)
  self:Fstring(m, c.label)
  self:FArray(m, c.list, "event_activity_info")
end
function Paser:Fevent_activity_req(m, c)
  self:Fdouble(m, c.id)
end
function Paser:Fevent_activity_award_req(m, c)
  self:Fdouble(m, c.id)
  self:Fdouble(m, c.task_id)
end
function Paser:Fuser_change_info(m, c)
  self:Fstring(m, c.name)
  self:Finteger(m, c.sex)
end
function Paser:Fevent_progress_award_req(m, c)
  self:Fdouble(m, c.activity_id)
  self:Fdouble(m, c.task_id)
  self:Finteger(m, c.step)
end
function Paser:Fstar_craft_data_ntf(m, c)
  self:Finteger(m, c.type)
  self:Finteger(m, c.end_time)
  self:Fgame_item(m, c.reward)
  self:Fboolean(m, c.open)
  self:Finteger(m, c.status)
end
function Paser:Fstar_craft_sign_up_req(m, c)
  self:Finteger(m, c.id)
  self:Finteger(m, c.logic_id)
end
function Paser:Fadventure_add_ack(m, c)
  self:Finteger(m, c.left_count)
  self:Finteger(m, c.next_price)
end
function Paser:Fprogress(m, c)
  self:Finteger(m, c.now_num)
  self:Finteger(m, c.total_num)
end
function Paser:Fprogress_detail(m, c)
  self:Finteger(m, c.aim_id)
  self:Fprogress(m, c.progress)
end
function Paser:Fbuff_list(m, c)
  self:Finteger(m, c.buff_id)
  self:Finteger(m, c.buff_num)
end
function Paser:Fmaster_level_data(m, c)
  self:Finteger(m, c.ask_level)
  self:FArray(m, c.conditions, "master_condition")
  self:Finteger(m, c.level)
  self:Finteger(m, c.max_master_level)
  self:Fprogress(m, c.total_progress)
  self:FArray(m, c.list, "buff_list")
  self:FArray(m, c.total_progress_detail, "progress_detail")
end
function Paser:Fmaster_condition(m, c)
  self:Fstring(m, c.condition_name)
  self:FArray(m, c.ask, "integer")
end
function Paser:Fmaster_req(m, c)
  self:Fstring(m, c.master_type)
  self:Finteger(m, c.matrix_id)
  self:FArray(m, c.scope, "integer")
end
function Paser:Fmaster_all_ack(m, c)
  self:FArray(m, c.master_all_ack, "master_ack")
end
function Paser:Fmaster_ack(m, c)
  self:Fstring(m, c.type)
  self:Finteger(m, c.min_level)
  self:Fmaster_level_data(m, c.now_level_data)
  self:Fmaster_level_data(m, c.next_level_data)
end
function Paser:Frecruit_module_data(m, c)
  self:Finteger(m, c.id)
  self:Fgame_item(m, c.buy_item)
  self:Fstring(m, c.img)
  self:Fgame_item(m, c.buy_price_one)
  self:Fgame_item(m, c.buy_price_ten)
  self:Finteger(m, c.desc_key)
  self:Fboolean(m, c.is_activity)
  self:Finteger(m, c.activity_time)
  self:Finteger(m, c.next_free_time)
  self:Finteger(m, c.free_count)
  self:Finteger(m, c.max_free_count)
  self:Finteger(m, c.image)
  self:Finteger(m, c.must_count)
end
function Paser:Frecruit_list_new_req(m, c)
  self:Fstring(m, c.type)
end
function Paser:Frecruit_list_new_ack(m, c)
  self:FArray(m, c.recruit_list, "recruit_module_data")
end
function Paser:Frecruit_req(m, c)
  self:Finteger(m, c.id)
  self:Finteger(m, c.count)
end
function Paser:Fmedal_config_info(m, c)
  self:Finteger(m, c.type)
  self:Finteger(m, c.type_next)
  self:Finteger(m, c.quality)
  self:Finteger(m, c.rank)
  self:Finteger(m, c.level)
  self:Finteger(m, c.exp)
  self:FArray(m, c.attr, "attr_pair")
  self:FArray(m, c.attr_add, "attr_pair")
  self:FArray(m, c.attr_rank, "attr_pair")
  self:Finteger(m, c.price)
  self:Fboolean(m, c.is_material)
end
function Paser:Frecruit_reward_data(m, c)
  self:Finteger(m, c.type)
  self:Fboolean(m, c.is_replacement)
  self:Fgame_item(m, c.own_item)
  self:Fgame_item(m, c.origin_item)
  self:Fgame_item(m, c.item_reward)
  self:Fmedal_config_info(m, c.medal)
  self:Ffleet_show_info(m, c.fleet_reward)
end
function Paser:Frecruit_ack(m, c)
  self:Frecruit_module_data(m, c.recruit_info)
  self:FArray(m, c.rewards, "recruit_reward_data")
end
function Paser:Frecruit_look_req(m, c)
  self:Finteger(m, c.id)
end
function Paser:Frecruit_look_ack(m, c)
  self:Finteger(m, c.id)
  self:FArray(m, c.rewards, "recruit_reward_data")
end
function Paser:Fhero_achievement(m, c)
  self:Finteger(m, c.type)
  self:Fstring(m, c.img)
  self:Fboolean(m, c.lock)
  self:Finteger(m, c.count)
  self:Finteger(m, c.max_count)
  self:Finteger(m, c.need_level)
  self:Finteger(m, c.need_type)
  self:Finteger(m, c.need_count)
end
function Paser:Fattribute_pair(m, c)
  self:Finteger(m, c.id)
  self:Fstring(m, c.key)
  self:Finteger(m, c.value)
  self:Fboolean(m, c.is_percent)
end
function Paser:Fhero_union_ack(m, c)
  self:FArray(m, c.achievement, "hero_achievement")
  self:FArray(m, c.attribute, "attribute_pair")
end
function Paser:Fcollect_fleet_info(m, c)
  self:Fshort(m, c.fleet_id)
  self:Fboolean(m, c.is_own)
end
function Paser:Fhero_collect_item(m, c)
  self:Fshort(m, c.type)
  self:FArray(m, c.attributes, "attribute_pair")
  self:FArray(m, c.collects, "collect_fleet_info")
  self:Fboolean(m, c.is_own)
end
function Paser:Fhero_collect_ack(m, c)
  self:FArray(m, c.fleet_info, "fleet_show_info")
  self:FArray(m, c.collect_info, "hero_collect_item")
end
function Paser:Fhero_collect_rank_req(m, c)
  self:Fboolean(m, c.is_all_server)
  self:Finteger(m, c.type)
end
function Paser:Fcollect_player_info(m, c)
  self:Fstring(m, c.name)
  self:Finteger(m, c.level)
  self:Finteger(m, c.order)
  self:Finteger(m, c.collect_count)
end
function Paser:Fhero_collect_rank_ack(m, c)
  self:Finteger(m, c.type)
  self:FArray(m, c.players, "collect_player_info")
  self:Fcollect_player_info(m, c.base_info)
end
function Paser:Fred_point_ntf(m, c)
  self:FArray(m, c.states, "pair")
end
function Paser:Farea_id_req(m, c)
  self:Finteger(m, c.item_id)
end
function Paser:Farea_id_ack(m, c)
  self:Finteger(m, c.area_id)
  self:Finteger(m, c.boss_id)
end
function Paser:Fmaster_ntf(m, c)
  self:FArray(m, c.master_ntf_detail, "master_ntf_detail")
end
function Paser:Fmaster_ntf_detail(m, c)
  self:Fstring(m, c.master_type)
  self:Finteger(m, c.matrix)
  self:Finteger(m, c.fleet_id)
  self:Finteger(m, c.level)
  self:FArray(m, c.value, "buff_list")
end
function Paser:Fmap_block(m, c)
  self:Finteger(m, c.id)
  self:Finteger(m, c.status)
  self:Fboolean(m, c.is_occupy)
  self:Finteger(m, c.search_left_time)
  self:Finteger(m, c.max_count)
  self:Finteger(m, c.left_count)
  self:Finteger(m, c.buy_count_credit)
  self:Finteger(m, c.buy_count)
end
function Paser:Fmining_world_req(m, c)
  self:Fstring(m, c.flag)
  self:Finteger(m, c.dir)
end
function Paser:Fmining_world_ack(m, c)
  self:FArray(m, c.map_blocks, "map_block")
end
function Paser:Flog_item(m, c)
  self:Finteger(m, c.time)
  self:Fstring(m, c.report_id)
  self:Fstring(m, c.desc)
end
function Paser:Fteam_log_item(m, c)
  self:Finteger(m, c.type)
  self:Finteger(m, c.time)
  self:Fstring(m, c.report_id)
  self:Finteger(m, c.round)
  self:Fstring(m, c.desc)
end
function Paser:Fmining_world_log_ack(m, c)
  self:FArray(m, c.logs, "log_item")
end
function Paser:Fmining_team_log_ack(m, c)
  self:FArray(m, c.logs, "team_log_item")
end
function Paser:Fpe_block(m, c)
  self:Finteger(m, c.star_level)
  self:Fstring(m, c.star_frame)
  self:Finteger(m, c.time)
  self:Fstring(m, c.desc)
  self:Finteger(m, c.player_id)
  self:Finteger(m, c.logic_id)
  self:Finteger(m, c.star_id)
  self:Finteger(m, c.pos)
  self:Finteger(m, c.block)
end
function Paser:Fmining_world_pe_ack(m, c)
  self:FArray(m, c.pe_list, "pe_block")
  self:Finteger(m, c.search_left_time)
end
function Paser:Fbattle_good_item(m, c)
  self:Fstring(m, c.item_type)
  self:Finteger(m, c.number)
  self:Finteger(m, c.no)
  self:Finteger(m, c.level)
  self:Finteger(m, c.max)
end
function Paser:Fstar_data(m, c)
  self:Finteger(m, c.id)
  self:Fboolean(m, c.is_occupied)
  self:Fboolean(m, c.is_my)
  self:Finteger(m, c.star_level)
  self:Fstring(m, c.flag)
  self:Fstring(m, c.alliance_name)
  self:Finteger(m, c.pos)
  self:Finteger(m, c.occupie_left_time)
  self:Fstring(m, c.occupie_name)
  self:Finteger(m, c.player_id)
  self:Finteger(m, c.logic_id)
  self:Fstring(m, c.star_frame)
  self:Fstring(m, c.name_key)
  self:Finteger(m, c.percent)
  self:FArray(m, c.battle_limit, "battle_good_item")
end
function Paser:Fmining_wars_req(m, c)
  self:Finteger(m, c.id)
end
function Paser:Fmining_wars_ack(m, c)
  self:Finteger(m, c.max_count)
  self:Finteger(m, c.left_count)
  self:Finteger(m, c.refresh_left_time)
  self:FArray(m, c.star_list, "star_data")
  self:Finteger(m, c.refresh_credit)
  self:Finteger(m, c.buy_count_credit)
  self:Finteger(m, c.buy_count)
  self:Finteger(m, c.layer_frame)
end
function Paser:Fmining_wars_buy_count_ack(m, c)
  self:Finteger(m, c.left_count)
  self:Finteger(m, c.next_buy_credit)
end
function Paser:Fmining_wars_star_req(m, c)
  self:Finteger(m, c.id)
end
function Paser:Fmining_wars_star_data(m, c)
  self:FArray(m, c.consume_item, "game_item")
  self:FArray(m, c.produce_item, "game_item")
  self:FArray(m, c.base_item, "game_item")
  self:Fstar_data(m, c.star_info)
end
function Paser:Fmining_wars_battle_req(m, c)
  self:Finteger(m, c.id)
  self:Finteger(m, c.player_id)
  self:Finteger(m, c.logic_id)
  self:Fboolean(m, c.use_credit)
  self:Fstring(m, c.flag)
  self:Finteger(m, c.pos)
  self:Fboolean(m, c.is_intrusion)
  self:Fboolean(m, c.is_team_fight)
end
function Paser:Fmining_battle_result(m, c)
  self:Fboolean(m, c.is_win)
  self:Fstring(m, c.star_frame)
  self:Fstring(m, c.star_name)
  self:FArray(m, c.consume_item, "game_item")
end
function Paser:Fmining_wars_battle_ack(m, c)
  self:Fboolean(m, c.success)
  self:Fmining_wars_star_data(m, c.result)
  self:Ffight_report(m, c.report)
  self:Finteger(m, c.error_code)
  self:Finteger(m, c.switch_credit)
  self:Fmining_battle_result(m, c.battle_result)
end
function Paser:Fmining_wars_collect_req(m, c)
  self:Finteger(m, c.id)
end
function Paser:Fmining_wars_collect_ack(m, c)
  self:Fmining_wars_star_data(m, c.result)
end
function Paser:Fmining_world_battle_req(m, c)
  self:Finteger(m, c.id)
  self:Finteger(m, c.player_id)
  self:Finteger(m, c.logic_id)
end
function Paser:Fmining_world_battle_ack(m, c)
  self:Ffight_report(m, c.report)
end
function Paser:Fmining_battle_report_req(m, c)
  self:Fstring(m, c.id)
end
function Paser:Fmining_battle_report_ack(m, c)
  self:Ffight_report(m, c.report)
  self:Fmining_battle_result(m, c.battle_result)
end
function Paser:Fmining_wars_refresh_req(m, c)
  self:Finteger(m, c.id)
end
function Paser:Fmining_wars_change_ntf(m, c)
  self:Fmining_wars_star_data(m, c.star_result)
  self:Finteger(m, c.status)
end
function Paser:Fmining_reward_ntf(m, c)
  self:FArray(m, c.rewards, "game_item")
  self:Finteger(m, c.status)
end
function Paser:Fbuttons_status(m, c)
  self:Fstring(m, c.name)
  self:Finteger(m, c.count)
  self:Fboolean(m, c.is_show)
  self:Fboolean(m, c.show_count)
end
function Paser:Fmain_city_ntf(m, c)
  self:Fstring(m, c.show_index)
  self:FArray(m, c.buttons, "buttons_status")
end
function Paser:Fmask_show_index_req(m, c)
  self:Fstring(m, c.show_index)
end
function Paser:Fmask_modules_req(m, c)
  self:Fstring(m, c.module_name)
end
function Paser:Factivity_item(m, c)
  self:Fstring(m, c.type_key)
  self:Fboolean(m, c.is_new)
  self:Finteger(m, c.status)
  self:Finteger(m, c.left_time)
end
function Paser:Fspace_door_ntf(m, c)
  self:FArray(m, c.activitys, "activity_item")
end
function Paser:Fhelper_buttons_ack(m, c)
  self:FArray(m, c.buttons, "buttons_status")
end
function Paser:Fart_core_decompose_req(m, c)
  self:Finteger(m, c.core_id)
  self:Finteger(m, c.fleet_id)
  self:Finteger(m, c.page)
end
function Paser:Fart_core_decompose_ack(m, c)
  self:Finteger(m, c.is_success)
  self:FArray(m, c.core_list, "art_core")
end
function Paser:Fart_main_req(m, c)
  self:Finteger(m, c.id)
  self:Finteger(m, c.user_id)
end
function Paser:Fart_main_ack(m, c)
  self:FArray(m, c.part_page_list, "art_part_page")
  self:Finteger(m, c.resource)
  self:Finteger(m, c.now_reset_cost)
  self:Finteger(m, c.perfect_reset_cost)
  self:FArray(m, c.one_key_upgrade, "game_item")
  self:FArray(m, c.upgrade, "game_item")
end
function Paser:Fart_core(m, c)
  self:Finteger(m, c.id)
  self:Finteger(m, c.type)
  self:Finteger(m, c.star)
  self:Finteger(m, c.level)
  self:Finteger(m, c.buff)
  self:FArray(m, c.add_buff, "integer")
  self:Finteger(m, c.color_type)
  self:Finteger(m, c.is_corr_type)
  self:Finteger(m, c.can_equip)
  self:Finteger(m, c.sell_num)
end
function Paser:Fart_part_page(m, c)
  self:Finteger(m, c.is_locked)
  self:Finteger(m, c.id)
  self:FArray(m, c.point_list, "art_point")
  self:Finteger(m, c.core_type)
  self:Fart_core(m, c.now_core)
  self:Finteger(m, c.upgrade_red)
  self:Finteger(m, c.equip_red)
end
function Paser:Farti_update_cost(m, c)
  self:Fstring(m, c.res_type)
  self:Finteger(m, c.res_num)
end
function Paser:Fart_point(m, c)
  self:Finteger(m, c.id)
  self:Finteger(m, c.type)
  self:Finteger(m, c.level)
  self:Finteger(m, c.max_level)
  self:FArray(m, c.update_cost, "arti_update_cost")
  self:Finteger(m, c.can_upgrade)
  self:FArray(m, c.point_buff, "point_buff")
  self:FArray(m, c.point_next_buff, "point_buff")
end
function Paser:Fpoint_buff(m, c)
  self:Finteger(m, c.buff_id)
  self:Finteger(m, c.buff_num)
end
function Paser:Flevel_point_buff(m, c)
  self:Fgame_item(m, c.need_res)
  self:Finteger(m, c.id)
  self:Finteger(m, c.level)
  self:FArray(m, c.next_buff, "point_buff")
end
function Paser:Fart_point_ack(m, c)
  self:Fart_point(m, c.point_info)
end
function Paser:Fart_point_req(m, c)
  self:Finteger(m, c.id)
end
function Paser:Fart_point_upgrade_req(m, c)
  self:Finteger(m, c.id)
end
function Paser:Fart_point_upgrade_ack(m, c)
  self:Finteger(m, c.is_success)
  self:FArray(m, c.part_page_list, "art_part_page")
  self:FArray(m, c.one_key_upgrade, "game_item")
  self:FArray(m, c.upgrade, "game_item")
end
function Paser:Fart_core_list_req(m, c)
  self:Finteger(m, c.fleet_id)
  self:Finteger(m, c.user_id)
  self:Finteger(m, c.page)
end
function Paser:Fart_core_list_ack(m, c)
  self:FArray(m, c.core_list, "art_core")
end
function Paser:Fart_core_down_req(m, c)
  self:Finteger(m, c.core_id)
  self:Finteger(m, c.fleet_id)
end
function Paser:Fart_core_down_ack(m, c)
  self:Finteger(m, c.is_success)
  self:FArray(m, c.part_page_list, "art_part_page")
end
function Paser:Fart_core_equip_req(m, c)
  self:Finteger(m, c.core_id)
  self:Finteger(m, c.fleet_id)
  self:Finteger(m, c.page)
end
function Paser:Fart_core_equip_ack(m, c)
  self:Finteger(m, c.is_success)
  self:FArray(m, c.part_page_list, "art_part_page")
end
function Paser:Fart_one_upgrade_data_req(m, c)
  self:Finteger(m, c.fleet_id)
  self:Finteger(m, c.user_id)
  self:Finteger(m, c.page)
end
function Paser:Fart_one_upgrade_data_ack(m, c)
  self:Finteger(m, c.max_level)
  self:FArray(m, c.cur_buff, "point_buff")
  self:FArray(m, c.all_point_buff, "level_point_buff")
end
function Paser:Fart_one_upgrade_req(m, c)
  self:Finteger(m, c.fleet_id)
  self:Finteger(m, c.user_id)
  self:Finteger(m, c.page)
  self:Finteger(m, c.level)
  self:Finteger(m, c.id)
end
function Paser:Fart_one_upgrade_ack(m, c)
  self:FArray(m, c.point_list, "art_point")
  self:FArray(m, c.part_page_list, "art_part_page")
  self:FArray(m, c.one_key_upgrade, "game_item")
  self:FArray(m, c.upgrade, "game_item")
end
function Paser:Fart_reset_req(m, c)
  self:Finteger(m, c.type)
  self:Finteger(m, c.fleet_id)
end
function Paser:Fart_reset_ack(m, c)
  self:Finteger(m, c.is_success)
  self:FArray(m, c.part_page_list, "art_part_page")
  self:FArray(m, c.one_key_upgrade, "game_item")
  self:FArray(m, c.upgrade, "game_item")
end
function Paser:Fart_can_upgrade_req(m, c)
  self:Finteger(m, c.fleet_id)
end
function Paser:Fart_can_upgrade_ack(m, c)
  self:Finteger(m, c.can_upgrade)
end
function Paser:Fpair_string(m, c)
  self:Fstring(m, c.str_key)
  self:Fstring(m, c.str_value)
end
function Paser:Fversion_info_req(m, c)
  self:Fstring(m, c.version_code)
  self:FArray(m, c.phone_info, "pair_string")
end
function Paser:Fevent_log_ntf(m, c)
  self:Finteger(m, c.event_type)
  self:Fstring(m, c.event_name)
  self:Fstring(m, c.event_param)
end
function Paser:Fchoosable_item_req(m, c)
  self:Finteger(m, c.id)
end
function Paser:Fgame_choosable_item(m, c)
  self:Fstring(m, c.item_type)
  self:Finteger(m, c.number)
  self:Finteger(m, c.no)
  self:Finteger(m, c.level)
  self:FArray(m, c.krypton_value, "buff_list")
  self:Finteger(m, c.can_use_more)
end
function Paser:Fchoosable_item_ack(m, c)
  self:FArray(m, c.choosable_items, "game_choosable_item")
end
function Paser:Fchoosable_item_use_req(m, c)
  self:Finteger(m, c.id)
  self:Finteger(m, c.count)
  self:Fgame_choosable_item(m, c.item)
end
function Paser:Ffte_value(m, c)
  self:Fstring(m, c.key)
  self:Fstring(m, c.value)
end
function Paser:Ffte_req(m, c)
  self:FArray(m, c.content, "fte_value")
end
function Paser:Ffte_ack(m, c)
  self:Finteger(m, c.is_success)
end
function Paser:Ffte_ntf(m, c)
  self:Finteger(m, c.id)
end
function Paser:Fachievement_reward_receive_req(m, c)
  self:Finteger(m, c.id)
end
function Paser:Fachievement_reward_receive_ack(m, c)
  self:Finteger(m, c.id)
  self:FArray(m, c.achievement_list, "achievement_info")
end
function Paser:Fgift_item(m, c)
  self:Fstring(m, c.id)
  self:Fstring(m, c.product_id)
  self:FArray(m, c.goods, "game_item")
  self:Fstring(m, c.icon_pic)
  self:Fstring(m, c.bg_pic)
  self:Fstring(m, c.bg_circle_pic)
  self:Fstring(m, c.bg_bottom_pic)
  self:Fstring(m, c.bg_limit_pic)
  self:Fshort(m, c.tip_type)
  self:Fdouble(m, c.discount)
  self:Finteger(m, c.left_time)
  self:Fshort(m, c.limit_type)
  self:Finteger(m, c.limit_count)
  self:Finteger(m, c.order)
  self:Fboolean(m, c.can_batch)
  self:FArray(m, c.viprange, "integer")
  self:Fboolean(m, c.can_show)
end
function Paser:Fpay_gifts_ack(m, c)
  self:FArray(m, c.gifts, "gift_item")
end
function Paser:Fwish_pay_req(m, c)
  self:Fstring(m, c.id)
  self:Fstring(m, c.product_id)
  self:Finteger(m, c.count)
end
function Paser:Fvip_item(m, c)
  self:Finteger(m, c.vip)
  self:FArray(m, c.rewards, "game_item")
end
function Paser:Fvip_reward_ack(m, c)
  self:FArray(m, c.vip_items, "vip_item")
end
function Paser:Fmonth_card_item(m, c)
  self:Fstring(m, c.id)
  self:Finteger(m, c.order)
  self:Fstring(m, c.bg_pic)
  self:Fstring(m, c.product_id)
  self:Fshort(m, c.status)
  self:Finteger(m, c.state_day)
  self:Finteger(m, c.left_time)
  self:Fshort(m, c.type)
  self:Fstring(m, c.title_key)
  self:Fstring(m, c.desc_key)
  self:Fgame_item(m, c.reward)
  self:Fshort(m, c.can_buy)
end
function Paser:Fmonth_card_ack(m, c)
  self:FArray(m, c.month_cards, "month_card_item")
end
function Paser:Fmonth_card_receive_reward_req(m, c)
  self:Fstring(m, c.id)
end
function Paser:Fmonth_card_receive_reward_ack(m, c)
  self:FArray(m, c.month_cards, "month_card_item")
end
function Paser:Fleader_info_ntf(m, c)
  self:FArray(m, c.leader_ids, "integer")
  self:FArray(m, c.options, "integer")
  self:FArray(m, c.active_spells, "integer")
end
function Paser:Fchange_leader_req(m, c)
  self:Finteger(m, c.id)
end
function Paser:Fchange_tlc_leader_req(m, c)
  self:Finteger(m, c.index)
  self:Finteger(m, c.id)
end
function Paser:Fget_reward_detail_req(m, c)
  self:Finteger(m, c.itemId)
end
function Paser:Freward_detail(m, c)
  self:FArray(m, c.details, "game_item")
end
function Paser:Fsave_user_id_req(m, c)
  self:Fstring(m, c.id)
  self:Fstring(m, c.name)
end
function Paser:Fuser_id_status_ntf(m, c)
  self:Fboolean(m, c.isAuth)
  self:Fstring(m, c.id)
  self:Fstring(m, c.name)
end
function Paser:Fstore_coupon(m, c)
  self:Finteger(m, c.item_id)
  self:Fshort(m, c.discount)
end
function Paser:Fitem_classify_ntf(m, c)
  self:FArray(m, c.equip_enhant_items, "integer")
  self:FArray(m, c.store_coupons, "store_coupon")
end
function Paser:Fforce_info(m, c)
  self:Fshort(m, c.server_id)
  self:Finteger(m, c.user_id)
  self:Fstring(m, c.name)
  self:Fstring(m, c.avator)
  self:Fdouble(m, c.power)
  self:FArray(m, c.fleets, "pair")
  self:Fshort(m, c.position)
end
function Paser:Fconcentrate_info(m, c)
  self:Fdouble(m, c.id)
  self:Fstring(m, c.name)
  self:Fdouble(m, c.enter_power)
  self:Fshort(m, c.force_count)
  self:FArray(m, c.forces, "force_info")
  self:Fshort(m, c.status)
  self:Finteger(m, c.left_time)
end
function Paser:Fforces_ack(m, c)
  self:FArray(m, c.forces_list, "concentrate_info")
end
function Paser:Fconcentrate_req(m, c)
  self:Fdouble(m, c.power)
  self:Fshort(m, c.count)
end
function Paser:Fconcentrate_ack(m, c)
  self:Fdouble(m, c.team_id)
  self:Finteger(m, c.code)
end
function Paser:Fconcentrate_end_req(m, c)
  self:Fdouble(m, c.id)
end
function Paser:Fconcentrate_end_ack(m, c)
  self:Finteger(m, c.code)
end
function Paser:Fforce_join_req(m, c)
  self:Fdouble(m, c.id)
end
function Paser:Fforce_join_ack(m, c)
  self:Finteger(m, c.code)
end
function Paser:Fconcentrate_atk_req(m, c)
  self:Finteger(m, c.star_id)
end
function Paser:Fconcentrate_atk_ack(m, c)
  self:Finteger(m, c.code)
end
function Paser:Fgroup_fight_header(m, c)
  self:FArray(m, c.p1_names, "string")
  self:FArray(m, c.p2_names, "string")
  self:Fshort(m, c.group_id)
  self:Ffight_report(m, c.report)
  self:Finteger(m, c.round_cnt)
end
function Paser:Fgroup_fight_ntf(m, c)
  self:FArray(m, c.headers, "group_fight_header")
end
function Paser:Fchange_atk_seq_req(m, c)
  self:FArray(m, c.seq, "pair")
end
function Paser:Fchange_atk_seq_ack(m, c)
  self:Finteger(m, c.code)
end
function Paser:Fforces_ntf(m, c)
  self:Finteger(m, c.self_server_id)
  self:Finteger(m, c.self_user_id)
  self:FArray(m, c.forces_list, "concentrate_info")
end
function Paser:Fmy_forces_ack(m, c)
  self:Finteger(m, c.self_server_id)
  self:Finteger(m, c.self_user_id)
  self:Fconcentrate_info(m, c.team)
end
function Paser:Fteam_fight_report_req(m, c)
  self:Fstring(m, c.report_id)
  self:Finteger(m, c.round)
end
function Paser:Ffair_arena_ack(m, c)
  self:Finteger(m, c.state)
  self:Fboolean(m, c.is_submit)
  self:Finteger(m, c.left_time)
  self:Finteger(m, c.season_left_time)
  self:Finteger(m, c.battle_left_time)
end
function Paser:Ffair_arena_rank_item(m, c)
  self:Finteger(m, c.player_id)
  self:Fstring(m, c.player_name)
  self:Fstring(m, c.logic_id)
  self:Finteger(m, c.score)
  self:Finteger(m, c.win_rate)
  self:Finteger(m, c.win_count)
  self:Finteger(m, c.rank_id)
  self:Fstring(m, c.avatar)
  self:Finteger(m, c.level)
  self:Fboolean(m, c.show_check)
end
function Paser:Ffair_arena_season_rank_item(m, c)
  self:Finteger(m, c.season_id)
  self:FArray(m, c.season_items, "fair_arena_rank_item")
end
function Paser:Ffair_arena_rank_ack(m, c)
  self:FArray(m, c.rank_list, "fair_arena_rank_item")
  self:FArray(m, c.season_rank_list, "fair_arena_season_rank_item")
end
function Paser:Ffair_arena_season_req(m, c)
  self:Finteger(m, c.season_id)
end
function Paser:Ffair_arena_season_ack(m, c)
  self:FArray(m, c.season_rank_list, "fair_arena_rank_item")
end
function Paser:Ffair_rank_reward_item(m, c)
  self:Finteger(m, c.id)
  self:FArray(m, c.reward_items, "game_item")
  self:Finteger(m, c.level)
  self:Finteger(m, c.state)
end
function Paser:Ffair_arena_reward_ack(m, c)
  self:FArray(m, c.reward_list, "fair_rank_reward_item")
end
function Paser:Ffair_report_item(m, c)
  self:Finteger(m, c.id)
  self:Finteger(m, c.time)
  self:Finteger(m, c.state)
  self:Finteger(m, c.opponent_state)
  self:Fuser_champion_data(m, c.own)
  self:Fuser_champion_data(m, c.opponent)
end
function Paser:Ffair_arena_report_req(m, c)
  self:Fstring(m, c.player_id)
  self:Fstring(m, c.logic_id)
end
function Paser:Ffair_arena_report_ack(m, c)
  self:Finteger(m, c.win_rate)
  self:Finteger(m, c.win_count)
  self:Finteger(m, c.fail_count)
  self:FArray(m, c.report_list, "fair_report_item")
end
function Paser:Ffair_arena_reward_get_req(m, c)
  self:Finteger(m, c.id)
end
function Paser:Ffair_arena_report_battle_req(m, c)
  self:Finteger(m, c.id)
end
function Paser:Ffair_arena_report_battle_ack(m, c)
  self:Ffight_report(m, c.report)
  self:Fmining_battle_result(m, c.battle_result)
end
function Paser:Fbasic_fleet_info(m, c)
  self:Finteger(m, c.fleet_id)
  self:Finteger(m, c.fleet_level)
  self:Finteger(m, c.sex)
  self:Fshort(m, c.vessels)
  self:FArray(m, c.adjust_info, "integer")
  self:Fshort(m, c.spell_id)
end
function Paser:Fformation_fleet_info(m, c)
  self:Fbasic_fleet_info(m, c.fleet)
  self:Fbasic_fleet_info(m, c.adjutant)
end
function Paser:Fformation_detail(m, c)
  self:Fformation_fleet_info(m, c.fleet_info)
  self:Finteger(m, c.pos)
end
function Paser:Ffair_arena_fleet_pool(m, c)
  self:FArray(m, c.hero_list, "basic_fleet_info")
  self:FArray(m, c.adjutant_list, "basic_fleet_info")
end
function Paser:Ffair_arena_formation_all_ack(m, c)
  self:FArray(m, c.formation_1, "formation_detail")
  self:FArray(m, c.formation_2, "formation_detail")
  self:FArray(m, c.formation_3, "formation_detail")
  self:Ffair_arena_fleet_pool(m, c.selectable_fleets)
  self:Fgame_items(m, c.refresh_price)
  self:Fboolean(m, c.can_refresh)
  self:FArray(m, c.active_spell, "integer")
end
function Paser:Fupdate_formations_req(m, c)
  self:FArray(m, c.formation_1, "formation_detail")
  self:FArray(m, c.formation_2, "formation_detail")
  self:FArray(m, c.formation_3, "formation_detail")
end
function Paser:Fupdate_formations_ack(m, c)
  self:Finteger(m, c.result)
end
function Paser:Frefresh_fleet_pool_ack(m, c)
  self:Finteger(m, c.result)
  self:Ffair_arena_fleet_pool(m, c.fleet_pool)
  self:Fgame_items(m, c.next_refresh_price)
  self:Fboolean(m, c.can_refresh)
end
function Paser:Ffair_arena_fleet_info_req(m, c)
  self:Finteger(m, c.fleet_id)
  self:Finteger(m, c.level)
  self:Finteger(m, c.type)
end
function Paser:Fprice_off_store(m, c)
  self:Finteger(m, c.group)
  self:Fboolean(m, c.status)
  self:Finteger(m, c.left_seconds)
end
function Paser:Fprice_off_store_ntf(m, c)
  self:FArray(m, c.store_infos, "price_off_store")
end
function Paser:Ffte_gift_ntf(m, c)
  self:FArray(m, c.gifts, "game_item")
end
function Paser:Fmining_wars_buy_count_req(m, c)
  self:Finteger(m, c.block)
end
function Paser:Fprice_info(m, c)
  self:Fshort(m, c.off_count)
  self:Fshort(m, c.origin_price)
  self:Fshort(m, c.price)
end
function Paser:Fenter_scratch_req(m, c)
  self:Fshort(m, c.act_id)
end
function Paser:Faward_condition(m, c)
  self:Fshort(m, c.award_id)
  self:Fgame_item(m, c.award)
  self:Fgame_items(m, c.condition)
  self:Fshort(m, c.award_status)
  self:Fshort(m, c.can_buy_times)
  self:Fboolean(m, c.can_times_buy)
end
function Paser:Fscr_progress(m, c)
  self:Fshort(m, c.max)
  self:Fshort(m, c.now)
  self:FArray(m, c.times_award, "award_pos")
end
function Paser:Faward_pos(m, c)
  self:Fshort(m, c.times)
  self:Fgame_items(m, c.award)
  self:Fshort(m, c.award_status)
end
function Paser:Fexchange_item(m, c)
  self:Fgame_items(m, c.item)
end
function Paser:Fcurrence_cost(m, c)
  self:Fgame_items(m, c.cost)
end
function Paser:Fbg_cfg(m, c)
  self:Fstring(m, c.bg_image)
  self:Fstring(m, c.scratch_image)
  self:Fstring(m, c.ui_image)
end
function Paser:Fenter_scratch_ack(m, c)
  self:Fprice_info(m, c.price)
  self:Fgame_item(m, c.reset_price)
  self:FArray(m, c.awards, "award_condition")
  self:Fscr_progress(m, c.progress_award)
  self:Fexchange_item(m, c.items)
  self:Fcurrence_cost(m, c.costs)
  self:Fbg_cfg(m, c.background)
  self:FArray(m, c.origin_award, "game_item")
end
function Paser:Fbuy_scratch_card_req(m, c)
  self:Fshort(m, c.act_id)
end
function Paser:Fbuy_scratch_card_ack(m, c)
  self:Fgame_items(m, c.award)
  self:Fscr_progress(m, c.progress_award)
  self:Fexchange_item(m, c.items)
  self:Fprice_info(m, c.next_price)
end
function Paser:Freset_scratch_card_req(m, c)
  self:Fshort(m, c.act_id)
end
function Paser:Freseting_ack(m, c)
  self:Fscr_progress(m, c.progress_award)
  self:Fexchange_item(m, c.exchange)
  self:Fgame_item(m, c.reset_price)
  self:Fprice_info(m, c.price)
  self:FArray(m, c.awards, "award_condition")
end
function Paser:Fget_process_award_req(m, c)
  self:Fshort(m, c.act_id)
  self:Fshort(m, c.award_id)
end
function Paser:Fget_process_award_ack(m, c)
  self:Fscr_progress(m, c.progress_award)
  self:Fexchange_item(m, c.items)
  self:Fshort(m, c.code)
end
function Paser:Fexchange_award_req(m, c)
  self:Fshort(m, c.act_id)
  self:Fshort(m, c.award_id)
  self:Fshort(m, c.num)
end
function Paser:Fend_scratch_req(m, c)
  self:Fshort(m, c.act_id)
end
function Paser:Fend_scratch_ack(m, c)
  self:Fexchange_item(m, c.item)
  self:FArray(m, c.awards, "award_condition")
end
function Paser:Fexchange_award_ack(m, c)
  self:FArray(m, c.awards, "award_condition")
  self:Fexchange_item(m, c.items)
  self:Fshort(m, c.code)
  self:Fgame_item(m, c.reset_price)
end
function Paser:Fuc_pay_sign_req(m, c)
  self:FArray(m, c.pay_info, "pair_string")
end
function Paser:Fuc_pay_sign_ack(m, c)
  self:Fstring(m, c.sign)
  self:Fstring(m, c.account_id)
end
function Paser:Fscratch_board_info(m, c)
  self:Fstring(m, c.user)
  self:Fgame_item(m, c.award)
end
function Paser:Fscratch_board_ntf(m, c)
  self:FArray(m, c.boards, "scratch_board_info")
end
function Paser:Fconfirm_exchange_req(m, c)
  self:Finteger(m, c.api)
  self:FArray(m, c.item, "game_item")
end
function Paser:Fnormal_ntf(m, c)
  self:FArray(m, c.map, "pair_string")
end
function Paser:Fchange_fleets_data_req(m, c)
  self:Fshort(m, c.matrix_index)
  self:Finteger(m, c.fleet_id)
end
function Paser:Fchange_fleets_data_ack(m, c)
  self:FArray(m, c.fleets, "fleet_show_info")
end
function Paser:Fchange_fleets_req(m, c)
  self:Fshort(m, c.matrix_index)
  self:Finteger(m, c.old_fleet_id)
  self:Finteger(m, c.new_fleet_id)
end
function Paser:Flogin_award_info(m, c)
  self:Fshort(m, c.award_id)
  self:Fshort(m, c.award_status)
  self:FArray(m, c.awards, "game_item")
  self:Fshort(m, c.vip_level)
end
function Paser:Flogin_sign_ack(m, c)
  self:Fshort(m, c.now_login)
  self:Fboolean(m, c.is_new)
  self:FArray(m, c.awards, "login_award_info")
end
function Paser:Fget_login_sign_award_req(m, c)
  self:Fshort(m, c.award_id)
end
function Paser:Fget_login_sign_award_ack(m, c)
  self:Fshort(m, c.code)
end
function Paser:Fkrypton_redpoint_req(m, c)
  self:Fshort(m, c.matrix_index)
  self:Finteger(m, c.fleet_id)
end
function Paser:Fkrypton_redpoint_ack(m, c)
  self:Fboolean(m, c.show_krypton_red)
  self:Fboolean(m, c.show_artfact_red)
  self:Fboolean(m, c.show_artupgrade_red)
end
function Paser:Fredownload_res_ack(m, c)
  self:FArray(m, c.res_list, "pair_string")
end
function Paser:Fopen_box_progress_award_req(m, c)
  self:Finteger(m, c.box_id)
  self:Finteger(m, c.step)
end
function Paser:Fexchange_regulation_ntf(m, c)
  self:Fshort(m, c.level)
  self:FArray(m, c.regulation, "pair_string")
end
function Paser:Fcredit_exchange(m, c)
  self:Fgame_item(m, c.item)
  self:Finteger(m, c.credit)
end
function Paser:Fmul_credit_exchange_ack(m, c)
  self:Finteger(m, c.api)
  self:FArray(m, c.exchange, "credit_exchange")
end
function Paser:Fmul_credit_exchange_ntf(m, c)
  self:Finteger(m, c.api)
  self:FArray(m, c.exchange, "credit_exchange")
end
function Paser:Fenter_medal_store_ack(m, c)
  self:Finteger(m, c.act_id)
  self:FArray(m, c.items, "medal_store_item")
  self:Fgame_item(m, c.reset_price)
  self:Fshort(m, c.currency_times)
  self:Fboolean(m, c.is_activity)
  self:Fshort(m, c.max_times)
  self:Finteger(m, c.time_left)
end
function Paser:Fmedal_store_buy_req(m, c)
  self:Finteger(m, c.act_id)
  self:Finteger(m, c.item_id)
end
function Paser:Fmedal_store_buy_ack(m, c)
  self:Fshort(m, c.code)
  self:FArray(m, c.buy_item, "game_item")
  self:Fgame_item(m, c.reset_price)
end
function Paser:Fmedal_store_refresh_req(m, c)
  self:Finteger(m, c.act_id)
end
function Paser:Fmedal_store_refresh_ack(m, c)
  self:FArray(m, c.items, "medal_store_item")
  self:Fgame_item(m, c.reset_price)
  self:Fshort(m, c.currency_times)
  self:Fshort(m, c.max_times)
  self:Finteger(m, c.time_left)
end
function Paser:Fattr_pair(m, c)
  self:Finteger(m, c.attr_id)
  self:Fdouble(m, c.attr_value)
end
function Paser:Fmedal_material_info(m, c)
  self:Finteger(m, c.type)
  self:Finteger(m, c.quality)
  self:Finteger(m, c.exp)
  self:Finteger(m, c.price)
  self:Finteger(m, c.num)
end
function Paser:Fmedal_consume(m, c)
  self:FArray(m, c.medals, "medal_material_info")
  self:FArray(m, c.materials, "medal_material_info")
end
function Paser:Fmedal_formation_info(m, c)
  self:Finteger(m, c.medal_id)
  self:Finteger(m, c.fleet_id)
  self:Finteger(m, c.formation_id)
  self:Finteger(m, c.pos)
end
function Paser:Fmedal_info(m, c)
  self:Finteger(m, c.id)
  self:Finteger(m, c.type)
  self:Finteger(m, c.type_next)
  self:Finteger(m, c.quality)
  self:Finteger(m, c.rank)
  self:Finteger(m, c.level)
  self:Finteger(m, c.exp)
  self:Fboolean(m, c.is_material)
  self:Finteger(m, c.material_exp)
  self:Fboolean(m, c.can_level_up)
  self:Fboolean(m, c.can_rank)
  self:FArray(m, c.can_replace, "pair")
  self:Finteger(m, c.rank_level_limit)
  self:Finteger(m, c.decompose_price)
  self:Fmedal_consume(m, c.reset_material)
  self:Fdouble(m, c.reset_ratio)
  self:Finteger(m, c.target_pos)
  self:FArray(m, c.formation_info, "medal_formation_info")
end
function Paser:Fmedal_fleet_formation(m, c)
  self:Finteger(m, c.fleet_id)
  self:Finteger(m, c.formation_id)
  self:FArray(m, c.equips, "pair")
end
function Paser:Fmedal_formation_pos_red_point_info(m, c)
  self:Finteger(m, c.formation_id)
  self:Finteger(m, c.fleet_id)
  self:FArray(m, c.red_points, "pair")
end
function Paser:Fmedal_formation_fleet_btn_red_point_info(m, c)
  self:Finteger(m, c.formation_id)
  self:Finteger(m, c.fleet_id)
  self:Finteger(m, c.red_point)
end
function Paser:Fmedal_list_ack(m, c)
  self:Finteger(m, c.currency)
end
function Paser:Fmedal_decompose_confirm_req(m, c)
  self:FArray(m, c.medals, "integer")
end
function Paser:Fmedal_decompose_confirm_ack(m, c)
  self:Finteger(m, c.currency)
end
function Paser:Fmedal_reset_confirm_req(m, c)
  self:Finteger(m, c.id)
end
function Paser:Fmedal_reset_confirm_ack(m, c)
  self:Fmedal_info(m, c.medal)
  self:FArray(m, c.materials, "medal_material_info")
end
function Paser:Fmedal_equip_ack(m, c)
  self:FArray(m, c.materials, "medal_material_info")
  self:FArray(m, c.formations, "medal_fleet_formation")
end
function Paser:Fmedal_level_up_req(m, c)
  self:Finteger(m, c.id)
  self:FArray(m, c.medals, "integer")
  self:FArray(m, c.materials, "pair")
end
function Paser:Fmedal_level_up_ack(m, c)
  self:FArray(m, c.materials, "medal_material_info")
  self:FArray(m, c.consumes, "integer")
end
function Paser:Fmedal_rank_up_req(m, c)
  self:Finteger(m, c.id)
  self:Finteger(m, c.rank_target)
end
function Paser:Fmedal_rank_up_ack(m, c)
  self:FArray(m, c.materials, "medal_material_info")
  self:FArray(m, c.consumes, "integer")
end
function Paser:Fmedal_list_ntf(m, c)
  self:FArray(m, c.medals, "medal_info")
end
function Paser:Fmedal_fleet_formation_ntf(m, c)
  self:FArray(m, c.formations, "medal_fleet_formation")
end
function Paser:Fmedal_fleet_red_point_ntf(m, c)
  self:FArray(m, c.formations, "medal_formation_pos_red_point_info")
end
function Paser:Fmedal_equip_on_req(m, c)
  self:Finteger(m, c.medal_id)
  self:Finteger(m, c.fleet_id)
  self:Finteger(m, c.formation_id)
  self:Finteger(m, c.pos)
end
function Paser:Fmedal_equip_on_ack(m, c)
  self:Fboolean(m, c.success)
end
function Paser:Fmedal_equip_off_req(m, c)
  self:Finteger(m, c.medal_id)
  self:Finteger(m, c.fleet_id)
  self:Finteger(m, c.formation_id)
  self:Finteger(m, c.pos)
end
function Paser:Fmedal_equip_off_ack(m, c)
  self:Fboolean(m, c.success)
end
function Paser:Fmedal_equip_replace_req(m, c)
  self:Finteger(m, c.medal_id)
  self:Finteger(m, c.replaced_id)
  self:Finteger(m, c.fleet_id)
  self:Finteger(m, c.formation_id)
  self:Finteger(m, c.pos)
end
function Paser:Fmedal_equip_replace_ack(m, c)
  self:Fboolean(m, c.success)
end
function Paser:Fmedal_fleet_btn_red_point_ntf(m, c)
  self:FArray(m, c.btns, "medal_formation_fleet_btn_red_point_info")
end
function Paser:Fmedal_config_info_req(m, c)
  self:Finteger(m, c.type)
end
function Paser:Fmedal_config_info_ack(m, c)
  self:Fmedal_config_info(m, c.medal)
end
function Paser:Fmedal_resource_ntf(m, c)
  self:Finteger(m, c.currency)
  self:FArray(m, c.materials, "medal_material_info")
end
function Paser:Ffight_report_round_ntf(m, c)
  self:FArray(m, c.rounds, "fight_round")
end
function Paser:Fmedal_boss_info(m, c)
  self:Finteger(m, c.boss_id)
  self:Fshort(m, c.difficuity)
  self:Fshort(m, c.free_times)
  self:Fgame_item(m, c.reset_price)
  self:FArray(m, c.award_list, "game_item")
  self:Fshort(m, c.max_degree)
  self:Fshort(m, c.residue_degree)
  self:Fboolean(m, c.can_notify)
  self:Fdouble(m, c.force)
  self:Fstring(m, c.ship)
  self:FArray(m, c.monster_matrix, "monster_matrix_cell")
end
function Paser:Fenter_medal_boss_ack(m, c)
  self:Finteger(m, c.act_id)
  self:Fmedal_boss_info(m, c.boss_info)
end
function Paser:Freset_medal_boss_req(m, c)
  self:Finteger(m, c.act_id)
end
function Paser:Freset_medal_boss_ack(m, c)
  self:Fmedal_boss_info(m, c.boss_info)
end
function Paser:Fchallenge_medal_boss_req(m, c)
  self:Finteger(m, c.act_id)
end
function Paser:Fchallenge_medal_boss_ack(m, c)
  self:Fmedal_boss_info(m, c.boss_info)
  self:Ffight_report(m, c.report)
  self:Fboolean(m, c.result)
  self:FArray(m, c.awards, "game_item")
end
function Paser:Fglory_collection_info(m, c)
  self:Finteger(m, c.type)
  self:Fstring(m, c.img)
  self:Fboolean(m, c.open)
  self:Fpair(m, c.activate)
  self:FArray(m, c.glories, "glory_info")
  self:FArray(m, c.glory_medals, "glory_medal_info")
  self:Finteger(m, c.red_point)
end
function Paser:Fglory_info(m, c)
  self:Fpair(m, c.type)
  self:Finteger(m, c.status)
  self:Finteger(m, c.rank)
  self:FArray(m, c.attr, "attr_pair")
  self:FArray(m, c.glory_medals, "integer")
end
function Paser:Fglory_medal_info(m, c)
  self:Finteger(m, c.type)
  self:Finteger(m, c.num)
  self:FArray(m, c.attr, "attr_pair")
  self:FArray(m, c.previews, "glory_attr_preview")
end
function Paser:Fglory_attr_preview(m, c)
  self:Fpair(m, c.type)
  self:Finteger(m, c.medal_type)
  self:Finteger(m, c.status)
  self:Finteger(m, c.rank)
  self:FArray(m, c.attr, "attr_pair")
end
function Paser:Fglory_attr_info(m, c)
  self:Fpair(m, c.type)
  self:Finteger(m, c.rank)
  self:Finteger(m, c.rank_max)
  self:FArray(m, c.attr, "attr_pair")
end
function Paser:Fmedal_glory_collection_req(m, c)
  self:Finteger(m, c.type)
end
function Paser:Fmedal_glory_collection_ack(m, c)
  self:Finteger(m, c.total_glory)
  self:FArray(m, c.total_attr, "attr_pair")
  self:FArray(m, c.glory_infos, "glory_attr_info")
end
function Paser:Fmedal_glory_collection_ntf(m, c)
  self:FArray(m, c.glory_collections, "glory_collection_info")
end
function Paser:Fmedal_glory_collection_enter_req(m, c)
  self:Finteger(m, c.type)
end
function Paser:Fmedal_glory_collection_enter_ack(m, c)
  self:Fglory_collection_info(m, c.glory_collection)
end
function Paser:Fmedal_glory_collection_total_attr_ack(m, c)
  self:FArray(m, c.total_attr, "attr_pair")
end
function Paser:Fmedal_glory_activate_req(m, c)
  self:Fpair(m, c.type)
end
function Paser:Fmedal_glory_activate_ack(m, c)
  self:Finteger(m, c.total_glory)
  self:FArray(m, c.total_attr, "attr_pair")
  self:Fglory_collection_info(m, c.glory_collection)
end
function Paser:Fmedal_glory_rank_up_req(m, c)
  self:Fpair(m, c.type)
end
function Paser:Fmedal_glory_rank_up_ack(m, c)
  self:Finteger(m, c.total_glory)
  self:FArray(m, c.total_attr, "attr_pair")
  self:Fglory_collection_info(m, c.glory_collection)
end
function Paser:Fmedal_glory_reset_req(m, c)
  self:Fpair(m, c.type)
end
function Paser:Fmedal_glory_reset_ack(m, c)
  self:Finteger(m, c.total_glory)
  self:FArray(m, c.total_attr, "attr_pair")
  self:Fglory_collection_info(m, c.glory_collection)
end
function Paser:Fglory_achievement_info(m, c)
  self:Finteger(m, c.type)
  self:Finteger(m, c.need)
  self:FArray(m, c.attr, "attr_pair")
end
function Paser:Fmedal_glory_achievement_red_point_ntf(m, c)
  self:Finteger(m, c.is_red)
end
function Paser:Fmedal_glory_achievement_ack(m, c)
  self:Finteger(m, c.activated)
  self:Finteger(m, c.can_activate)
  self:FArray(m, c.total_attr, "attr_pair")
  self:FArray(m, c.achievements, "glory_achievement_info")
end
function Paser:Fmedal_glory_achievement_activate_ack(m, c)
  self:Finteger(m, c.activated)
  self:Finteger(m, c.can_activate)
  self:FArray(m, c.total_attr, "attr_pair")
end
function Paser:Fglory_rank_info(m, c)
  self:Finteger(m, c.type)
  self:Finteger(m, c.rank)
  self:Fpair(m, c.user_key)
  self:Fstring(m, c.user_name)
  self:Finteger(m, c.glory)
  self:Finteger(m, c.time)
end
function Paser:Fglory_collection_rank_info(m, c)
  self:Finteger(m, c.type)
  self:Fglory_rank_info(m, c.self)
  self:FArray(m, c.tops, "glory_rank_info")
end
function Paser:Fmedal_glory_collection_rank_ack(m, c)
  self:FArray(m, c.glory_collection_ranks, "glory_collection_rank_info")
end
function Paser:Fhero_union_unlock_ntf(m, c)
  self:Finteger(m, c.fleet_id)
  self:Finteger(m, c.unlock_count)
  self:FArray(m, c.attribute, "attribute_pair")
end
function Paser:Flarge_map_build_info(m, c)
  self:Finteger(m, c.build_type)
  self:Fboolean(m, c.build_show)
end
function Paser:Fexpedition_rank_awards_ack(m, c)
  self:FArray(m, c.ranking_awards, "expedition_rate_award")
end
function Paser:Fexpedition_rate_award(m, c)
  self:Finteger(m, c.id)
  self:Finteger(m, c.begin_rank)
  self:Finteger(m, c.end_rank)
  self:FArray(m, c.awards, "game_item")
end
function Paser:Farmy_info(m, c)
end
function Paser:Flarge_map_alliance_info(m, c)
  self:Fstring(m, c.id)
  self:Fstring(m, c.name)
  self:Ffloat(m, c.rate)
  self:Finteger(m, c.block_count)
  self:Finteger(m, c.people_count)
  self:Finteger(m, c.total_fleet)
  self:Finteger(m, c.online_fleet)
  self:Finteger(m, c.max_fleet)
  self:Ffloat(m, c.expend)
  self:Finteger(m, c.core_star)
end
function Paser:Flarge_map_repair_simple_item(m, c)
  self:Finteger(m, c.identity)
  self:Fstring(m, c.ship)
  self:Fstring(m, c.avatar)
  self:Finteger(m, c.level)
  self:Finteger(m, c.color)
  self:Finteger(m, c.percent)
end
function Paser:Flarge_map_player_detail(m, c)
  self:Fstring(m, c.player_id)
  self:Finteger(m, c.owner_code)
  self:Finteger(m, c.cur_energy)
  self:Finteger(m, c.max_energy)
  self:Finteger(m, c.pos)
  self:Flarge_map_alliance_info(m, c.alliance_info)
  self:Fshort(m, c.alliance_rank)
  self:Fshort(m, c.status)
  self:Finteger(m, c.damaged_percent)
  self:Fstring(m, c.troop_id)
  self:Finteger(m, c.troop_time)
  self:FArray(m, c.fleets, "large_map_repair_simple_item")
  self:Finteger(m, c.common_money)
  self:Finteger(m, c.exploit_money)
  self:Fbattle_good_item(m, c.buy_energy)
end
function Paser:Flarge_map_block_req(m, c)
  self:Finteger(m, c.col_left)
  self:Finteger(m, c.col_right)
  self:Finteger(m, c.row_up)
  self:Finteger(m, c.row_down)
end
function Paser:Flarge_block_info(m, c)
  self:Finteger(m, c.id)
  self:Finteger(m, c.type)
  self:Finteger(m, c.build_type)
  self:Finteger(m, c.owner)
  self:Fshort(m, c.color)
  self:Fboolean(m, c.show)
  self:Fshort(m, c.fleet_type)
  self:Fshort(m, c.status)
  self:Fshort(m, c.side_status)
  self:Fstring(m, c.alliance_id)
  self:Fstring(m, c.alliance_name)
  self:Finteger(m, c.total_time)
  self:Finteger(m, c.left_time)
  self:Finteger(m, c.occupy_color)
  self:Finteger(m, c.occupy_owner)
  self:Finteger(m, c.rotation)
end
function Paser:Flarge_map_block_ack(m, c)
  self:FArray(m, c.block_list, "large_block_info")
end
function Paser:Fbg_block_info(m, c)
  self:Finteger(m, c.id)
  self:Finteger(m, c.frame)
end
function Paser:Fnebula_block_info(m, c)
  self:Finteger(m, c.id)
  self:Finteger(m, c.pos_x)
  self:Finteger(m, c.pos_y)
  self:Finteger(m, c.frame)
end
function Paser:Flarge_map_base_ack(m, c)
  self:Finteger(m, c.row_count)
  self:Finteger(m, c.col_count)
  self:Finteger(m, c.center_id)
  self:FArray(m, c.bg_list, "bg_block_info")
  self:FArray(m, c.nebula_list, "nebula_block_info")
  self:Flarge_map_player_detail(m, c.my_info)
  self:FArray(m, c.build_list, "short")
end
function Paser:Flarge_map_block_update_ntf(m, c)
  self:FArray(m, c.block_list, "large_block_info")
end
function Paser:Flarge_map_player_item(m, c)
  self:Fstring(m, c.player_id)
  self:Fstring(m, c.ship_frame)
end
function Paser:Flarge_map_route_req(m, c)
  self:Fshort(m, c.type)
  self:Finteger(m, c.target_id)
end
function Paser:Flarge_map_route_info(m, c)
  self:Fshort(m, c.type)
  self:Fshort(m, c.trip_type)
  self:Fstring(m, c.player_id)
  self:FArray(m, c.move_points, "integer")
  self:Finteger(m, c.time)
  self:Finteger(m, c.cur_time)
  self:Fshort(m, c.route_type)
  self:Finteger(m, c.start_show_time)
  self:Finteger(m, c.end_show_time)
end
function Paser:Flarge_map_route_ntf(m, c)
  self:FArray(m, c.route_list, "large_map_route_info")
end
function Paser:Flarge_map_event_info(m, c)
  self:Fstring(m, c.event_id)
  self:Finteger(m, c.event_type)
  self:Finteger(m, c.target_id)
end
function Paser:Flarge_map_event_ntf(m, c)
  self:FArray(m, c.event_list, "large_map_event_info")
end
function Paser:Flarge_map_battle_req(m, c)
  self:Fstring(m, c.event_id)
end
function Paser:Flarge_map_join_alliance_req(m, c)
  self:Fstring(m, c.id)
end
function Paser:Flarge_map_join_alliance_ack(m, c)
  self:Fshort(m, c.apply_status)
  self:Flarge_map_alliance_info(m, c.alliance_info)
end
function Paser:Flarge_map_exit_alliance_req(m, c)
  self:Fstring(m, c.id)
end
function Paser:Flarge_map_alliance_list(m, c)
  self:Fgame_item(m, c.create_consume)
  self:FArray(m, c.alliance_list, "large_map_alliance_info")
  self:FArray(m, c.apply_alliances, "string")
end
function Paser:Flarge_map_create_alliance_req(m, c)
  self:Fstring(m, c.name)
end
function Paser:Flarge_map_alliance_affais_ack(m, c)
  self:Flarge_map_alliance_info(m, c.alliance_info)
  self:Fstring(m, c.alliance_notice)
  self:FArray(m, c.alliance_res, "game_item")
  self:Finteger(m, c.grant_time)
  self:Finteger(m, c.dissolve_time)
end
function Paser:Flarge_map_alliance_member_info(m, c)
  self:Fstring(m, c.id)
  self:Fstring(m, c.name)
  self:Fshort(m, c.level)
  self:Fshort(m, c.rank)
  self:Fshort(m, c.military_rank)
  self:Finteger(m, c.contribution)
  self:Finteger(m, c.last_login)
  self:Fdouble(m, c.force)
  self:Fshort(m, c.sex)
  self:Finteger(m, c.main_fleet_id)
  self:FArray(m, c.fleet_ids, "alliance_fleet_info")
  self:Fboolean(m, c.is_online)
end
function Paser:Flarge_map_alliance_army_ack(m, c)
  self:Flarge_map_alliance_info(m, c.alliance_info)
  self:FArray(m, c.member_list, "large_map_alliance_member_info")
end
function Paser:Flarge_map_alliance_player_info(m, c)
  self:Fstring(m, c.id)
  self:Fstring(m, c.name)
  self:Finteger(m, c.level)
  self:Fdouble(m, c.force)
end
function Paser:Flarge_map_alliance_invite_data_ack(m, c)
  self:Flarge_map_alliance_info(m, c.alliance_info)
  self:FArray(m, c.player_list, "large_map_alliance_player_info")
end
function Paser:Flarge_map_alliance_player_search_req(m, c)
  self:Fstring(m, c.search_name)
end
function Paser:Flarge_map_alliance_player_search_ack(m, c)
  self:Fstring(m, c.search_name)
  self:FArray(m, c.player_list, "large_map_alliance_player_info")
end
function Paser:Flarge_map_alliance_invite_req(m, c)
  self:Fstring(m, c.player_id)
end
function Paser:Flarge_map_alliance_invite_ack(m, c)
  self:Fstring(m, c.player_id)
  self:Fboolean(m, c.is_success)
end
function Paser:Flarge_map_alliance_recruit_option_req(m, c)
  self:Fstring(m, c.alliance_id)
end
function Paser:Flarge_map_alliance_recruit_option_info(m, c)
  self:Fboolean(m, c.enable_ratify)
  self:Finteger(m, c.need_level)
  self:Ffloat(m, c.need_force)
  self:Finteger(m, c.need_exploit)
end
function Paser:Flarge_map_alliance_recruit_option_update_req(m, c)
  self:Fstring(m, c.alliance_id)
  self:Fboolean(m, c.enable_ratify)
  self:Finteger(m, c.need_level)
  self:Ffloat(m, c.need_force)
  self:Finteger(m, c.need_exploit)
end
function Paser:Flarge_map_alliance_ratify_join_data_req(m, c)
  self:Fstring(m, c.alliance_id)
end
function Paser:Flarge_map_alliance_ratify_apply_item(m, c)
  self:Fstring(m, c.id)
  self:Fstring(m, c.player_id)
  self:Fstring(m, c.name)
  self:Finteger(m, c.level)
  self:Fdouble(m, c.force)
  self:Fshort(m, c.rank)
  self:Fshort(m, c.apply_rank)
  self:Finteger(m, c.last_login)
end
function Paser:Flarge_map_alliance_ratify_apply_info(m, c)
  self:FArray(m, c.apply_list, "large_map_alliance_ratify_apply_item")
end
function Paser:Flarge_map_alliance_ratify_duty_data_req(m, c)
  self:Fstring(m, c.alliance_id)
end
function Paser:Flarge_map_alliance_ratify_handle_req(m, c)
  self:FArray(m, c.id, "string")
  self:Fshort(m, c.apply_type)
  self:Fboolean(m, c.status)
end
function Paser:Flarge_map_alliance_ratify_handle_ack(m, c)
  self:Fshort(m, c.apply_type)
  self:Flarge_map_alliance_ratify_apply_info(m, c.apply_info)
end
function Paser:Flarge_map_alliance_apply_duty_req(m, c)
  self:Fstring(m, c.player_id)
  self:Fshort(m, c.apply_rank)
end
function Paser:Flarge_map_alliance_demotion_req(m, c)
  self:Fstring(m, c.player_id)
end
function Paser:Flarge_map_alliance_exchange_req(m, c)
  self:Fstring(m, c.player_id)
end
function Paser:Flarge_map_alliance_exchange_ack(m, c)
  self:Flarge_map_alliance_member_info(m, c.my_alliance_info)
  self:Flarge_map_alliance_member_info(m, c.other_alliance_info)
end
function Paser:Flarge_map_alliance_expel_req(m, c)
  self:Fstring(m, c.player_id)
end
function Paser:Flarge_map_alliance_expel_ack(m, c)
  self:Fboolean(m, c.is_success)
  self:Fstring(m, c.player_id)
end
function Paser:Flarge_map_block_detail_req(m, c)
  self:Finteger(m, c.id)
end
function Paser:Flarge_map_warning_army(m, c)
  self:Fstring(m, c.alliance_id)
  self:Fstring(m, c.alliance_name)
  self:Finteger(m, c.player_count)
end
function Paser:Flarge_map_block_detail(m, c)
  self:Finteger(m, c.id)
  self:Finteger(m, c.owner_code)
  self:Fstring(m, c.block_name_key)
  self:FArray(m, c.res_output, "battle_good_item")
  self:Flarge_map_alliance_info(m, c.alliance_info)
  self:Finteger(m, c.consume_energy)
  self:FArray(m, c.get_res, "game_item")
  self:Fboolean(m, c.is_border)
  self:Finteger(m, c.self_player_count)
  self:Finteger(m, c.other_player_count)
  self:FArray(m, c.warning_items, "large_map_warning_army")
  self:Fboolean(m, c.enable_watch)
end
function Paser:Flarge_map_create_mass_troop_req(m, c)
  self:Finteger(m, c.id)
  self:Ffloat(m, c.min_force)
  self:Finteger(m, c.mass_count)
end
function Paser:Fmass_troops_item(m, c)
  self:Fstring(m, c.id)
  self:Fstring(m, c.leader_name)
  self:Finteger(m, c.time)
  self:Finteger(m, c.mass_id)
  self:Fstring(m, c.mass_name_key)
  self:Finteger(m, c.target_id)
  self:Fstring(m, c.target_name_key)
  self:Fstring(m, c.alliance_name)
  self:Finteger(m, c.arrived_fleet)
  self:Finteger(m, c.mass_total_fleet)
  self:Fshort(m, c.status)
end
function Paser:Flarge_map_mass_troops_list(m, c)
  self:FArray(m, c.mass_troops, "mass_troops_item")
end
function Paser:Flarge_map_mass_troops_member(m, c)
  self:Fstring(m, c.player_id)
  self:Fstring(m, c.avatar)
  self:Fstring(m, c.name)
end
function Paser:Flarge_map_troops_detail(m, c)
  self:Finteger(m, c.time)
  self:Fstring(m, c.leader_id)
  self:Fstring(m, c.troop_id)
  self:Fshort(m, c.status)
  self:Fshort(m, c.max_count)
  self:FArray(m, c.members, "large_map_mass_troops_member")
end
function Paser:Flarge_map_join_mass_req(m, c)
  self:Fstring(m, c.id)
end
function Paser:Fmulmatrix_get_req(m, c)
  self:Finteger(m, c.type)
end
function Paser:Flarge_map_mass_near_invite_req(m, c)
  self:Fstring(m, c.search_name)
end
function Paser:Fmass_invite_item(m, c)
  self:Finteger(m, c.id)
  self:Fstring(m, c.player_id)
  self:Fstring(m, c.name)
  self:Finteger(m, c.level)
  self:Fdouble(m, c.force)
  self:Fshort(m, c.status)
  self:Finteger(m, c.status_time)
end
function Paser:Flarge_map_mass_invite_data(m, c)
  self:Finteger(m, c.onekey_time)
  self:FArray(m, c.invite_list, "mass_invite_item")
end
function Paser:Flarge_map_mass_invite_req(m, c)
  self:Fstring(m, c.search_name)
  self:FArray(m, c.player_id, "string")
end
function Paser:Flarge_map_mass_invite_ntf(m, c)
  self:Finteger(m, c.id)
  self:Fstring(m, c.troop_id)
  self:Fstring(m, c.block_name_key)
  self:Flarge_map_alliance_info(m, c.alliance_info)
  self:Fstring(m, c.leader_name)
  self:Fstring(m, c.leader_avatar)
  self:Fdouble(m, c.leader_force)
  self:Finteger(m, c.leader_level)
end
function Paser:Flarge_map_mass_invite_do_req(m, c)
  self:Fstring(m, c.troops_id)
  self:Fboolean(m, c.is_accept)
  self:Fboolean(m, c.pause_notice)
end
function Paser:Flarge_map_exit_mass_troop_req(m, c)
  self:Fstring(m, c.troop_id)
end
function Paser:Flarge_map_view_mass_player_req(m, c)
  self:Fstring(m, c.player_id)
end
function Paser:Flarge_map_simple_player_info(m, c)
  self:Fstring(m, c.player_id)
  self:Fstring(m, c.name)
  self:Fstring(m, c.avatar)
  self:Finteger(m, c.level)
  self:Fdouble(m, c.force)
  self:Fshort(m, c.rank)
  self:Ffloat(m, c.exploit)
end
function Paser:Flarge_map_remove_mass_player_req(m, c)
  self:Fstring(m, c.player_id)
end
function Paser:Flarge_map_repair_avatar_item(m, c)
  self:Finteger(m, c.identity)
  self:Fstring(m, c.ship)
  self:Fstring(m, c.avatar)
  self:Finteger(m, c.level)
  self:Finteger(m, c.color)
  self:Fstring(m, c.name)
  self:Fdouble(m, c.force)
  self:Fshort(m, c.sex)
  self:Finteger(m, c.vessels)
  self:Finteger(m, c.rank)
  self:Fdouble(m, c.cur_hp)
  self:Fdouble(m, c.total_hp)
  self:Fgame_item(m, c.need_res)
end
function Paser:Flarge_map_repair_ack(m, c)
  self:FArray(m, c.avatar_list, "large_map_repair_avatar_item")
end
function Paser:Flarge_map_repair_ship_req(m, c)
  self:Finteger(m, c.identity)
end
function Paser:Flarge_map_add_mass_player_ntf(m, c)
  self:Fstring(m, c.troop_id)
  self:Flarge_map_mass_troops_member(m, c.new_member)
end
function Paser:Flarge_map_buy_energy_req(m, c)
  self:Finteger(m, c.type)
end
function Paser:Flarge_map_local_leader_req(m, c)
  self:Fstring(m, c.troop_id)
end
function Paser:Flarge_map_local_leader_ack(m, c)
  self:Finteger(m, c.leader_pos)
end
function Paser:Flarge_map_alliance_diplomacy_ack(m, c)
  self:FArray(m, c.bat_to_me, "large_map_alliance_info")
  self:FArray(m, c.bat_to_other, "large_map_alliance_info")
  self:FArray(m, c.all_army, "large_map_alliance_info")
end
function Paser:Flarge_map_alliance_diplomacy_exchange_req(m, c)
  self:Fstring(m, c.alliance_id)
  self:Fstring(m, c.cur_type)
  self:Fstring(m, c.dst_type)
end
function Paser:Flarge_map_alliance_modify_notice_req(m, c)
  self:Fstring(m, c.alliance_id)
  self:Fstring(m, c.notice)
end
function Paser:Flarge_map_alliance_adjust_req(m, c)
  self:Fstring(m, c.alliance_id)
  self:Ffloat(m, c.rate)
  self:Ffloat(m, c.expend)
end
function Paser:Flarge_map_log_req(m, c)
  self:Fshort(m, c.log_type)
end
function Paser:Flarge_map_log_item(m, c)
  self:Fstring(m, c.id)
  self:Fstring(m, c.report_id)
  self:Fstring(m, c.log_str)
  self:Finteger(m, c.time)
end
function Paser:Flarge_map_log_ack(m, c)
  self:Fshort(m, c.log_type)
  self:FArray(m, c.log_list, "large_map_log_item")
end
function Paser:Fuser_statistics_req(m, c)
  self:Fstring(m, c.device_name)
  self:Fstring(m, c.device_real_name)
  self:Fstring(m, c.open_udid)
  self:Fstring(m, c.report_id)
  self:Fstring(m, c.battle_system)
  self:Finteger(m, c.played_round_count)
  self:Fshort(m, c.state)
  self:Fshort(m, c.save_report)
  self:FArray(m, c.ext_info, "pair_string")
end
function Paser:Ffps_switch_ntf(m, c)
  self:Fshort(m, c.status)
end
function Paser:Flarge_map_log_battle_req(m, c)
  self:Fstring(m, c.report_id)
end
function Paser:Flarge_map_search_alliance_req(m, c)
  self:Fstring(m, c.search_name)
end
function Paser:Flarge_map_dissolve_alliance_req(m, c)
  self:Fstring(m, c.id)
end
function Paser:Flarge_map_rank_req(m, c)
  self:Fshort(m, c.rank_type)
end
function Paser:Flarge_map_rank_item(m, c)
  self:Fstring(m, c.id)
  self:Finteger(m, c.rank)
  self:Finteger(m, c.rank_value)
  self:Fstring(m, c.player_name)
  self:Fstring(m, c.alliance_name)
  self:FArray(m, c.rewards, "game_item")
end
function Paser:Flarge_map_rank_ack(m, c)
  self:Fshort(m, c.rank_type)
  self:Finteger(m, c.reward_time)
  self:Flarge_map_rank_item(m, c.my_rank_info)
  self:FArray(m, c.rank_list, "large_map_rank_item")
end
function Paser:Flarge_map_alliance_player_exit_ntf(m, c)
  self:FArray(m, c.players, "string")
end
function Paser:Flarge_map_alliance_promotion_req(m, c)
  self:Fstring(m, c.player_id)
end
function Paser:Fline_item(m, c)
  self:Fstring(m, c.id)
  self:Finteger(m, c.start_id)
  self:Finteger(m, c.end_id)
  self:Fshort(m, c.line_type)
  self:Fshort(m, c.operate_type)
end
function Paser:Flarge_map_mass_attack_line_ntf(m, c)
  self:FArray(m, c.lines, "line_item")
end
function Paser:Flarge_map_battle_ack(m, c)
  self:Fboolean(m, c.is_win)
  self:Fstring(m, c.win_name)
end
function Paser:Flarge_map_vision_ntf(m, c)
  self:FArray(m, c.visible_ids, "integer")
end
function Paser:Flarge_map_vision_change_ntf(m, c)
  self:FArray(m, c.add_ids, "integer")
  self:FArray(m, c.sub_ids, "integer")
end
function Paser:Fsmall_map_item(m, c)
  self:Finteger(m, c.id)
  self:Finteger(m, c.row)
  self:Finteger(m, c.col)
  self:Fshort(m, c.color)
end
function Paser:Flarge_map_small_map_ack(m, c)
  self:Finteger(m, c.my_pos)
  self:Finteger(m, c.refresh_min)
  self:Finteger(m, c.total_row)
  self:Finteger(m, c.total_col)
  self:FArray(m, c.blocks, "small_map_item")
  self:FArray(m, c.fleets, "integer")
end
function Paser:Flarge_map_ship_arrive_req(m, c)
  self:Fstring(m, c.player_id)
  self:Finteger(m, c.arrive_id)
  self:Finteger(m, c.rotation)
end
function Paser:Ftlc_fight_status_ack(m, c)
  self:Finteger(m, c.protect_time)
end
function Paser:Ftlc_status_ntf(m, c)
  self:Finteger(m, c.status)
end
function Paser:Ftlc_player_info(m, c)
  self:Finteger(m, c.point)
  self:Finteger(m, c.rank_point)
  self:Finteger(m, c.next_rank_point)
  self:Finteger(m, c.ranking)
  self:Finteger(m, c.using_ranking)
  self:Finteger(m, c.using_rank)
  self:Finteger(m, c.rank)
  self:Fstring(m, c.rank_img)
  self:Finteger(m, c.rank_name)
  self:Finteger(m, c.protect_time)
  self:Finteger(m, c.final_time)
  self:Finteger(m, c.end_time)
  self:Finteger(m, c.res_per_hour)
  self:Fdouble(m, c.force)
end
function Paser:Ftlc_rank_board_ack(m, c)
  self:Ftlc_player_brief(m, c.self)
  self:Finteger(m, c.world_ranking)
  self:Finteger(m, c.rank_level)
  self:Fstring(m, c.rank_img)
  self:Finteger(m, c.rank_name)
  self:Finteger(m, c.rank_ceil)
  self:FArray(m, c.top_list, "tlc_player_brief")
  self:FArray(m, c.class_list, "tlc_player_brief")
end
function Paser:Ftlc_info(m, c)
  self:Fgame_item(m, c.currency)
  self:Fboolean(m, c.can_buy)
  self:Finteger(m, c.search_cost)
  self:Finteger(m, c.max_supply)
  self:Fboolean(m, c.pop_report)
  self:Finteger(m, c.king_num)
  self:Ftlc_player_info(m, c.player_info)
  self:FArray(m, c.buffers, "pair")
end
function Paser:Ftlc_info_ntf(m, c)
  self:Ftlc_info(m, c.info)
end
function Paser:Ftlc_awards_ntf(m, c)
  self:FArray(m, c.awards, "tlc_award")
end
function Paser:Ftlc_awards_get_req(m, c)
  self:Finteger(m, c.id)
end
function Paser:Ftlc_rankup_ntf(m, c)
  self:Finteger(m, c.rank_name)
  self:Fstring(m, c.rank_img)
  self:Finteger(m, c.score)
  self:Finteger(m, c.next_rank_name)
  self:Finteger(m, c.next_rank_score)
end
function Paser:Ftlc_report_detail_req(m, c)
  self:Fstring(m, c.id)
end
function Paser:Ftlc_report_detail_ack(m, c)
  self:FArray(m, c.headers, "group_fight_header")
end
function Paser:Ftlc_fight_req(m, c)
  self:Finteger(m, c.server_id)
  self:Fstring(m, c.user_id)
  self:Finteger(m, c.revenge_id)
end
function Paser:Ftlc_fight_report(m, c)
  self:Fstring(m, c.report_id)
  self:Ftlc_player_brief(m, c.enemy)
  self:Fboolean(m, c.is_win)
  self:Fboolean(m, c.can_revenge)
  self:Finteger(m, c.score_before)
  self:Finteger(m, c.score_after)
  self:Finteger(m, c.fight_time)
end
function Paser:Ftlc_fight_ack(m, c)
  self:Finteger(m, c.code)
  self:Ftlc_fight_report(m, c.result)
  self:Fdouble(m, c.damage)
  self:Fdouble(m, c.suffer_damage)
  self:FArray(m, c.headers, "group_fight_header")
  self:FArray(m, c.awards, "game_item")
end
function Paser:Ftlc_challenge_info(m, c)
  self:Ftlc_player_brief(m, c.defender)
  self:Finteger(m, c.win_score)
  self:Finteger(m, c.lose_score)
end
function Paser:Ftlc_match_ack(m, c)
  self:Finteger(m, c.code)
  self:FArray(m, c.challenge_list, "tlc_challenge_info")
end
function Paser:Ftlc_enter_ack(m, c)
  self:Ftlc_info(m, c.info)
end
function Paser:Fchampion_cdtime_ack(m, c)
  self:Fchampion_status_info(m, c.local_champion_cd)
  self:Fchampion_status_info(m, c.world_champion_cd)
  self:Fchampion_status_info(m, c.tlc_champion_cd)
end
function Paser:Fteamleague_matrix_req(m, c)
  self:Finteger(m, c.type)
end
function Paser:Fteamleague_matrix_info(m, c)
  self:Finteger(m, c.type)
  self:Finteger(m, c.leader_fleet)
  self:FArray(m, c.matrixs, "matrix")
end
function Paser:Fteamleague_matrix_ack(m, c)
  self:FArray(m, c.teamleague_matrix, "teamleague_matrix_info")
end
function Paser:Fteamleague_matrix_save_req(m, c)
  self:FArray(m, c.teamleague_matrix, "teamleague_matrix_info")
end
function Paser:Ftlc_bag_req(m, c)
  self:Fstring(m, c.owner)
  self:Finteger(m, c.matrix)
  self:Finteger(m, c.bag_type)
  self:Finteger(m, c.pos)
end
function Paser:Ftlc_bag_info(m, c)
  self:Fstring(m, c.owner)
  self:Finteger(m, c.matrix)
  self:Finteger(m, c.bag_type)
  self:FArray(m, c.grids, "bag_grid")
  self:FArray(m, c.equipedItems, "bag_new_grid")
end
function Paser:Ftlc_krypton_store_req(m, c)
  self:Finteger(m, c.formation_id)
end
function Paser:Ftlc_krypton_store_ack(m, c)
  self:Finteger(m, c.grid_capacity)
  self:FArray(m, c.kryptons, "krypton_info")
  self:Finteger(m, c.klevel)
  self:Finteger(m, c.gacha_need_money)
  self:Finteger(m, c.gacha_need_credit)
  self:Finteger(m, c.use_credit_count)
  self:Fboolean(m, c.is_high)
end
function Paser:Ftlc_fleets_info(m, c)
  self:FArray(m, c.fleets, "fleet")
end
function Paser:Ftlc_mulmatrix_switch_req(m, c)
  self:Finteger(m, c.id)
  self:Fboolean(m, c.switch)
end
function Paser:Ftlc_mulmatrix_switch_ack(m, c)
  self:Finteger(m, c.code)
end
function Paser:Ftlc_matrix_change_ntf(m, c)
  self:FArray(m, c.teamleague_matrix, "teamleague_matrix_info")
end
function Paser:Flevel_list_item(m, c)
  self:Finteger(m, c.id)
  self:Finteger(m, c.target)
  self:Finteger(m, c.status)
  self:FArray(m, c.awards, "game_item")
end
function Paser:Fcumulate_recharge_ack(m, c)
  self:Finteger(m, c.recharged_count)
  self:Finteger(m, c.left_time)
  self:Finteger(m, c.next_level_done)
  self:Finteger(m, c.next_level_target)
  self:FArray(m, c.level_list, "level_list_item")
end
function Paser:Fcumulate_recharge_award_get_req(m, c)
  self:Finteger(m, c.id)
end
function Paser:Flogin_fund_reward_item(m, c)
  self:Finteger(m, c.days)
  self:Finteger(m, c.status)
  self:FArray(m, c.awards, "game_item")
end
function Paser:Flogin_fund_ack(m, c)
  self:Finteger(m, c.bln_buy)
  self:Finteger(m, c.acc_login_days)
  self:FArray(m, c.reward_list, "login_fund_reward_item")
end
function Paser:Flogin_fund_reward_req(m, c)
  self:Finteger(m, c.days)
end
function Paser:Fcheck_exp_add_item_req(m, c)
  self:Finteger(m, c.item)
end
function Paser:Fcheck_exp_add_item_ack(m, c)
  self:Finteger(m, c.result)
  self:Finteger(m, c.add_percent)
  self:Finteger(m, c.lefttime)
end
function Paser:Fexp_add_item_ntf(m, c)
  self:Finteger(m, c.bln_effect)
  self:Finteger(m, c.add_percent)
  self:Finteger(m, c.lefttime)
end
function Paser:Fpop_firstpay_on_lvup_ntf(m, c)
  self:Fstring(m, c.bg_pic)
  self:Fstring(m, c.title)
  self:Fstring(m, c.desc)
end
function Paser:Ftlc_awards_get_ack(m, c)
  self:Finteger(m, c.code)
  self:Finteger(m, c.id)
end
function Paser:Fgcs_task_info(m, c)
  self:Finteger(m, c.id)
  self:Finteger(m, c.type)
  self:Fstring(m, c.desc)
  self:Finteger(m, c.recommend_lv)
  self:Finteger(m, c.cur_done)
  self:Finteger(m, c.goal)
  self:Finteger(m, c.status)
  self:FArray(m, c.awards, "game_item")
end
function Paser:Fgcs_today_info(m, c)
  self:Finteger(m, c.change_left)
  self:FArray(m, c.tasks, "gcs_task_info")
end
function Paser:Fgcs_week_info(m, c)
  self:Finteger(m, c.week_id)
  self:Finteger(m, c.unlock_after)
  self:Finteger(m, c.cur_done)
  self:Finteger(m, c.total)
  self:Finteger(m, c.extra_goal)
  self:Fgame_item(m, c.extra_award)
  self:Finteger(m, c.extra_status)
  self:FArray(m, c.free_tasks, "gcs_task_info")
  self:FArray(m, c.paid_tasks, "gcs_task_info")
end
function Paser:Fgcs_task_data_ack(m, c)
  self:Fboolean(m, c.paid_card)
  self:Fgcs_today_info(m, c.today)
  self:FArray(m, c.weeks, "gcs_week_info")
end
function Paser:Fgcs_lv_awards(m, c)
  self:Finteger(m, c.lv)
  self:FArray(m, c.free_awards, "game_item")
  self:Fboolean(m, c.free_received)
  self:FArray(m, c.paid_awards, "game_item")
  self:Fboolean(m, c.paid_received)
end
function Paser:Fgcs_awards_data_ack(m, c)
  self:Fboolean(m, c.paid_card)
  self:Finteger(m, c.cur_season)
  self:Finteger(m, c.season_left_time)
  self:Finteger(m, c.cur_lv)
  self:Finteger(m, c.cur_points)
  self:Finteger(m, c.lv_up_need)
  self:FArray(m, c.lv_awards_list, "gcs_lv_awards")
end
function Paser:Fgcs_today_task_change_req(m, c)
  self:Finteger(m, c.task_id)
end
function Paser:Fgcs_today_task_change_ack(m, c)
  self:Fgcs_today_info(m, c.today)
end
function Paser:Fgcs_get_today_task_reward_req(m, c)
  self:Finteger(m, c.task_id)
end
function Paser:Fgcs_get_today_task_reward_ack(m, c)
  self:Fgcs_today_info(m, c.today)
end
function Paser:Fgcs_get_week_task_reward_req(m, c)
  self:Finteger(m, c.week_id)
  self:Finteger(m, c.task_id)
end
function Paser:Fgcs_get_week_task_reward_ack(m, c)
  self:Fgcs_week_info(m, c.week_info)
end
function Paser:Fgcs_get_week_extra_reward_req(m, c)
  self:Finteger(m, c.week_id)
end
function Paser:Fgcs_get_week_extra_reward_ack(m, c)
  self:Fgcs_week_info(m, c.week_info)
end
function Paser:Fgcs_get_lv_all_rewards_ack(m, c)
  self:FArray(m, c.lv_awards_list, "gcs_lv_awards")
end
function Paser:Fgcs_point_update_ntf(m, c)
  self:Fboolean(m, c.paid_card)
  self:Finteger(m, c.cur_season)
  self:Finteger(m, c.season_left_time)
  self:Finteger(m, c.cur_lv)
  self:Finteger(m, c.cur_points)
  self:Finteger(m, c.lv_up_need)
  self:FArray(m, c.lv_awards_list, "gcs_lv_awards")
end
function Paser:Fgcs_buy_update_ntf(m, c)
  self:Fboolean(m, c.paid_card)
  self:Fgcs_today_info(m, c.today)
  self:FArray(m, c.weeks, "gcs_week_info")
end
function Paser:Ftlc_krypton_sort_req(m, c)
  self:Finteger(m, c.formation_id)
  self:Fshort(m, c.type)
end
