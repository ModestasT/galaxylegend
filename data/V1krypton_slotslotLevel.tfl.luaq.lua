local slotLevel = GameData.krypton_slot.slotLevel
slotLevel[1] = {slot = 1, labLevel = 1}
slotLevel[2] = {slot = 2, labLevel = 5}
slotLevel[3] = {slot = 3, labLevel = 7}
slotLevel[4] = {slot = 4, labLevel = 9}
slotLevel[5] = {slot = 5, labLevel = 11}
slotLevel[6] = {slot = 6, labLevel = 13}
slotLevel[7] = {slot = 7, labLevel = 15}
slotLevel[8] = {slot = 8, labLevel = 17}
