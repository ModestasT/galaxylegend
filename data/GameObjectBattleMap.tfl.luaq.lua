local GameObjectBattleMap = LuaObjectManager:GetLuaObject("GameObjectBattleMap")
local GameObjectBattleMapBG = LuaObjectManager:GetLuaObject("GameObjectBattleMapBG")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameUIBarRight = LuaObjectManager:GetLuaObject("GameUIBarRight")
local GameUIBarLeft = LuaObjectManager:GetLuaObject("GameUIBarLeft")
local GameStateCampaignSelection = GameStateManager.GameStateCampaignSelection
local GameStateBattleMap = GameStateManager.GameStateBattleMap
local GameUIEnemyInfo = LuaObjectManager:GetLuaObject("GameUIEnemyInfo")
local GameUIEvent = LuaObjectManager:GetLuaObject("GameUIEvent")
local GameQuestMenu = LuaObjectManager:GetLuaObject("GameQuestMenu")
local GameWaiting = LuaObjectManager:GetLuaObject("GameWaiting")
local GameUICommonDialog = LuaObjectManager:GetLuaObject("GameUICommonDialog")
local GameObjectTutorialCutscene = LuaObjectManager:GetLuaObject("GameObjectTutorialCutscene")
local GameStateMainPlanet = GameStateManager.GameStateMainPlanet
local GameGlobalData = GameGlobalData
local QuestTutorialCityHall = TutorialQuestManager.QuestTutorialCityHall
local QuestTutorialRechargePower = TutorialQuestManager.QuestTutorialRechargePower
local QuestTutorialBattleRush = TutorialQuestManager.QuestTutorialBattleRush
local QuestTutorialPveMapPoint = TutorialQuestManager.QuestTutorialPveMapPoint
local GameUIBattleResult = LuaObjectManager:GetLuaObject("GameUIBattleResult")
local QuestTutorialEquip = TutorialQuestManager.QuestTutorialEquip
local QuestTutorialFirstGetWanaArmor = TutorialQuestManager.QuestTutorialFirstGetWanaArmor
local QuestTutorialPrestige = TutorialQuestManager.QuestTutorialPrestige
local QuestTutorialFirstGetSilvaEquip = TutorialQuestManager.QuestTutorialFirstGetSilvaEquip
local QuestTutorialTheFirstEvent = TutorialQuestManager.QuestTutorialTheFirstEvent
local QuestTutorialGetQuestReward = TutorialQuestManager.QuestTutorialGetQuestReward
local TutorialAdjutantManager = LuaObjectManager:GetLuaObject("TutorialAdjutantManager")
local GameUIMaskLayer = LuaObjectManager:GetLuaObject("GameUIMaskLayer")
local GameUIActivity = LuaObjectManager:GetLuaObject("GameUIActivity")
local GameUIActivityNew = LuaObjectManager:GetLuaObject("GameUIActivityNew")
local GameStateBattlePlay = GameStateManager.GameStateBattlePlay
local GameObjectLevelUp = LuaObjectManager:GetLuaObject("GameObjectLevelUp")
require("data1/BattlePlayTutorial.tfl")
function GameObjectBattleMap:Update(dt)
  self:GetFlashObject():Update(dt)
  self:GetFlashObject():InvokeASCallback("_root", "OnUpdate", dt)
end
function GameObjectBattleMap:OnInitGame()
end
function GameObjectBattleMap.RefreshQuestData()
  local quest = GameGlobalData:GetData("user_quest")
  local canFinish, canShowWDQuest = false, false
  local GameQuestMenu = LuaObjectManager:GetLuaObject("GameQuestMenu")
  if quest.status == GameQuestMenu.QUEST_STS_UNLOOT or GameQuestMenu:checkIsShowActivityQuest() then
    canFinish = true
    local GameUIPrestigeRankUp = LuaObjectManager:GetLuaObject("GameUIPrestigeRankUp")
    if GameStateManager:GetCurrentGameState():IsObjectInState(GameObjectBattleMap) and not GameStateManager:GetCurrentGameState():IsObjectInState(GameQuestMenu) and not GameStateManager.GameStateGlobalState:IsObjectInState(GameUIPrestigeRankUp) and not GameUIPrestigeRankUp.mIsGotoDetail then
      GameQuestMenu:ShowQuestMenu()
    end
  end
  local condTypeText, condNumText = GameDataAccessHelper:GetMainQuestTitileText(quest.quest_id), tostring(quest.cond_count) .. "/" .. tostring(quest.cond_total)
  GameUIBarLeft:GetFlashObject():InvokeASCallback("_root", "refreshQuest", condTypeText, condNumText, canFinish)
end
function GameObjectBattleMap:RefreshEnemyStatus()
  if not self:GetFlashObject() then
    return
  end
  if GameObjectTutorialCutscene:IsActive() and GameObjectTutorialCutscene:IsNeedShowNewFakeBattle() then
    local index = GameGlobalData:GetFakeBattleIndex() or 0
    DebugOut("RefreshEnemyStatus ", GameGlobalData:GetFakeBattleIndex())
    index = index + 1
    local dataCtr = BattlePlayTutorial.new()
    self.m_battleStatus = dataCtr:GetMapSectionInfo()
    local bgSize = GameObjectBattleMapBG:GetFlashObject():InvokeASCallback("_root", "getBGSize")
    local param_list = LuaUtils:string_split(bgSize, "^")
    self:GetFlashObject():InvokeASCallback("_root", "initBattleMap", "", "", "", "", param_list[1], param_list[2])
    DebugOut("initBattleMapForTutorial Lua")
    for i, v in ipairs(self.m_battleStatus) do
      local name = ""
      local star = 0
      if index > i then
        star = 3
      elseif index < i then
        star = -1
      end
      local monster1 = "ship39"
      local monster2 = "ship39"
      local monster3 = "ship39"
      local monster4 = "ship39"
      local monster5 = "ship39"
      local monster6 = "ship39"
      local numMonster = #v.fleet_list
      local bosstext = GameLoader:GetGameText("LC_MENU_BOSS_CHAR")
      self:GetFlashObject():InvokeASCallback("_root", "setEnemyStatus", i, i == index, GameLoader:GetGameText("LC_MENU_Level") .. 1, name, 0, star, i == 6, numMonster, monster1, monster2, monster3, monster4, monster5, monster6, i <= 4, true, bosstext)
    end
  else
    local quest = GameGlobalData:GetData("user_quest")
    local condTypeText, condNumText = GameDataAccessHelper:GetMainQuestTitileText(quest.quest_id), tostring(quest.cond_count) .. "/" .. tostring(quest.cond_total)
    local passedText = GameLoader:GetGameText("LC_MENU_ENEMY_STATUS_PASSED")
    local currentTitleText = GameLoader:GetGameText("LC_MENU_SECTION_NAME_" .. GameStateBattleMap.m_currentAreaID .. "_" .. GameStateBattleMap.m_currentSectionID)
    local bgSize = ""
    if GameObjectBattleMapBG:GetFlashObject() == nil then
      if ext.is4x3() then
        bgSize = "1500^768"
      else
        bgSize = "1500^640"
      end
    else
      bgSize = GameObjectBattleMapBG:GetFlashObject():InvokeASCallback("_root", "getBGSize")
    end
    local param_list = LuaUtils:string_split(bgSize, "^")
    local targetText = GameLoader:GetGameText("LC_MENU_TARGET_CHAR")
    local missionText = GameLoader:GetGameText("LC_MENU_MISSION_CHAR")
    self:GetFlashObject():InvokeASCallback("_root", "initBattleMap", passedText, targetText, missionText, currentTitleText, param_list[1], param_list[2])
    for i, v in ipairs(self.m_battleStatus) do
      local areaID, battle_id = GameUtils:ResolveBattleID(v.battle_id)
      DebugOut(GameStateBattleMap.m_currentAreaID, battle_id)
      local enemyInfo = GameDataAccessHelper:GetBattleInfo(GameStateBattleMap.m_currentAreaID, battle_id)
      local isBoss = enemyInfo.EnemyType ~= 0
      local isMission = quest.cond_int == v.battle_id and quest.status ~= GameQuestMenu.QUEST_STS_UNLOOT
      local battleType = math.floor(math.fmod(battle_id, 1000) / 100)
      local tutorialPve = GameObjectBattleMap:CheckeIsShowTutorialPveMapPoint(v)
      DebugOut("tutorialPve: = ", tutorialPve)
      if #v.fleet_list == 0 then
        DebugOut("111111111111111111")
        local bosstext = GameLoader:GetGameText("LC_MENU_BOSS_CHAR")
        self:GetFlashObject():InvokeASCallback("_root", "setEnemyStatus", i, isMission, GameLoader:GetGameText("LC_MENU_Level") .. enemyInfo.LEVEL, "event", battleType, v.status, isBoss, 0, 0, 0, 0, 0, 0, 0, tutorialPve, false, bosstext)
      else
        local name = GameDataAccessHelper:GetAreaNameText(areaID, battle_id)
        local monster1 = GameDataAccessHelper:GetMonsterShip(v.fleet_list[1], false)
        local monster2 = GameDataAccessHelper:GetMonsterShip(v.fleet_list[2] or v.fleet_list[1], false)
        local monster3 = GameDataAccessHelper:GetMonsterShip(v.fleet_list[3] or v.fleet_list[2] or v.fleet_list[1], false)
        local monster4 = GameDataAccessHelper:GetMonsterShip(v.fleet_list[4] or v.fleet_list[3] or v.fleet_list[2] or v.fleet_list[1], false)
        local monster5 = GameDataAccessHelper:GetMonsterShip(v.fleet_list[5] or v.fleet_list[4] or v.fleet_list[3] or v.fleet_list[2] or v.fleet_list[1], false)
        local monster6 = GameDataAccessHelper:GetMonsterShip(v.fleet_list[6] or v.fleet_list[5] or v.fleet_list[4] or v.fleet_list[3] or v.fleet_list[2] or v.fleet_list[1], false)
        local numMonster = #v.fleet_list
        if areaID == 60 and battle_id == 1013 and immanentversion == 2 then
        else
          DebugOut("222222222222")
          local bosstext = GameLoader:GetGameText("LC_MENU_BOSS_CHAR")
          self:GetFlashObject():InvokeASCallback("_root", "setEnemyStatus", i, isMission, GameLoader:GetGameText("LC_MENU_Level") .. enemyInfo.LEVEL, name, battleType, v.status, isBoss, numMonster, monster1, monster2, monster3, monster4, monster5, monster6, tutorialPve, false, bosstext)
        end
      end
      if immanentversion == 2 and isMission and QuestTutorialBattleRush:IsActive() then
        self:GetFlashObject():InvokeASCallback("_root", "SetShowEnemyTip", i, true)
      end
    end
  end
end
function GameObjectBattleMap:FindShowTutorial()
  local battleTab = {}
  for i, v in ipairs(self.m_battleStatus) do
    if v.status == 0 then
      table.insert(battleTab, v)
    end
  end
  local battle = -1
  local showTutorial
  for _, k in ipairs(battleTab) do
    local areaID, battle_id = GameUtils:ResolveBattleID(k.battle_id)
    local id = math.floor(math.fmod(battle_id, 1000) / 100)
    if battle < id then
      battle = id
      showTutorial = k.battle_id
    end
  end
  return showTutorial
end
function GameObjectBattleMap:CheckeIsShowTutorialPveMapPoint(v)
  local mapBattleId = self:FindShowTutorial()
  if v.battle_id == mapBattleId and QuestTutorialPveMapPoint:IsActive() and not QuestTutorialEquip:IsActive() and not QuestTutorialFirstGetWanaArmor:IsActive() and not QuestTutorialPrestige:IsActive() and not QuestTutorialFirstGetSilvaEquip:IsActive() and not QuestTutorialGetQuestReward:IsActive() then
    return true
  else
    return false
  end
end
function GameObjectBattleMap:IsBattleActive(areaID, battle_id)
  local battleInfo = self:GetBattleInfo(areaID, battle_id)
  return battleInfo.status == 0
end
function GameObjectBattleMap:GetBattleInfo(areaID, battle_id)
  local combinedBattleID = GameUtils:CombineBattleID(areaID, battle_id)
  for i, v in ipairs(self.m_battleStatus) do
    if v.battle_id == combinedBattleID then
      return v
    end
  end
  return nil
end
function GameObjectBattleMap:OnFSCommand(cmd, args)
  if cmd == "ShipEnterMapOver" then
    if GameObjectTutorialCutscene:IsActive() and GameObjectTutorialCutscene:IsNeedShowNewFakeBattle() and (not GameGlobalData:GetFakeBattleIndex() or GameGlobalData:GetFakeBattleIndex() == 0) then
      GameUtils:PlayMusic("sound/GE2_Map.mp3")
      GameUICommonDialog:JudeIsLoadFlash()
      local function callback()
        local FteDialogFiniedReqCall = function()
          NetMessageMgr:SendMsg(NetAPIList.fte_req.Code, {
            content = {
              [1] = {
                key = "FinishedDialog",
                value = "1100096|1100097"
              }
            }
          }, nil, false, FteDialogFiniedReqCall)
        end
        FteDialogFiniedReqCall()
        self:RefreshEnemyStatus()
      end
      local FteBegainDialogReqCall = function()
        NetMessageMgr:SendMsg(NetAPIList.fte_req.Code, {
          content = {
            [1] = {
              key = "BeginDialog",
              value = "1100096|1100097"
            }
          }
        }, nil, false, FteBegainDialogReqCall)
      end
      FteBegainDialogReqCall()
      GameUICommonDialog:PlayStory({1100096, 1100097}, callback)
    elseif GameObjectTutorialCutscene:IsActive() and (not GameGlobalData:GetFakeBattleIndex() or GameGlobalData:GetFakeBattleIndex() ~= 6) then
      GameUtils:PlayMusic("sound/GE2_Map.mp3")
      self:RefreshEnemyStatus()
    end
  elseif cmd == "ShowVictoryTipsOver" then
    GameStateBattleMap:ShowDialog()
    GameStateBattleMap.isShowVictoryTip = false
  elseif cmd == "TriggerTarget" then
    if GameObjectTutorialCutscene:IsActive() and GameObjectTutorialCutscene:IsNeedShowNewFakeBattle() then
      local function callback()
        GameObjectLevelUp:showNewFakeBattleVictory()
        GameStateManager:GetCurrentGameState():AddObject(GameObjectLevelUp)
        if not GameGlobalData.FakeBattleIndex then
          GameGlobalData.FakeBattleIndex = 1
        else
          GameGlobalData.FakeBattleIndex = tonumber(args)
        end
        local function FteProgressReqCall()
          NetMessageMgr:SendMsg(NetAPIList.fte_req.Code, {
            content = {
              [1] = {
                key = "fte_progress",
                value = tostring(GameGlobalData.FakeBattleIndex)
              }
            }
          }, nil, false, FteProgressReqCall)
        end
        local function FteFakeBattleFinishReqCall()
          NetMessageMgr:SendMsg(NetAPIList.fte_req.Code, {
            content = {
              [1] = {
                key = "FinishedFakeBattle",
                value = tostring(GameGlobalData.FakeBattleIndex)
              }
            }
          }, nil, false, FteFakeBattleFinishReqCall)
        end
        FteProgressReqCall()
        FteFakeBattleFinishReqCall()
        GameStateBattleMap.isShowVictoryTip = true
        if GameObjectTutorialCutscene:IsActive() and GameObjectTutorialCutscene:IsNeedShowNewFakeBattle() then
          DebugOut(" TriggerTarget Fake Battle :", GameGlobalData.FakeBattleIndex)
          if GameGlobalData.FakeBattleIndex == 6 then
            GameStateManager:SetCurrentGameState(GameStateManager.GameStateEmptyState)
            GameObjectTutorialCutscene:ShowTutorial("1")
          else
            GameStateManager:SetCurrentGameState(GameStateManager.GameStateBattleMap)
          end
        end
      end
      if tonumber(args) > GameGlobalData.FakeBattleIndex then
        GameStateBattlePlay:RegisterOverCallback(callback, nil)
        local dataCtr = BattlePlayTutorial.new()
        DebugOut("StartBattlePlay:")
        DebugTable(dataCtr:GetNewFakeBattleData(tonumber(args)))
        local reports = dataCtr:GetNewFakeBattleData(tonumber(args))
        GameStateBattleMap.m_currentAreaID = 60
        GameStateBattleMap.m_currentSectionID = 1
        GameStateBattlePlay.m_showRightShipEnter = true
        GameStateBattlePlay.curBattleType = "pve"
        GameStateBattlePlay:InitBattle(GameStateBattlePlay.MODE_PLAY, reports, 60, 1)
        GameStateManager:SetCurrentGameState(GameStateBattlePlay)
        local function FteBegainBattleReqCall()
          NetMessageMgr:SendMsg(NetAPIList.fte_req.Code, {
            content = {
              [1] = {
                key = "BeginFakeBattle",
                value = args
              }
            }
          }, nil, false, FteBegainBattleReqCall)
        end
        FteBegainBattleReqCall()
      end
    else
      if GameStateManager:GetCurrentGameState():IsObjectInState(GameUIActivity) or GameStateManager:GetCurrentGameState():IsObjectInState(GameUIActivityNew) or GameStateManager:GetCurrentGameState():IsObjectInState(GameQuestMenu) then
        return
      end
      if GameUtils:GetTutorialHelp() and GameUtils:GetTutorialName() == "explorePlanetary" then
        GameUtils:HideTutorialHelp()
      end
      do
        local enemyIndex = tonumber(args)
        local combinedBattleID = self.m_battleStatus[enemyIndex].battle_id
        local areaID, battleID = GameUtils:ResolveBattleID(combinedBattleID)
        DebugOut("!!! areaID=", areaID, "battleID=", battleID, "eventID=", combinedBattleID)
        if 1008 == battleID and not TutorialQuestManager.QuestTutorialFirstGetWanaArmor:IsFinished() then
          TutorialQuestManager.QuestTutorialFirstGetWanaArmor:SetActive(false, true)
          TutorialQuestManager.QuestTutorialFirstGetWanaArmor:SetFinish(true)
        end
        if 1011 == battleID and not TutorialQuestManager.QuestTutorialFirstGetSilvaEquip:IsFinished() then
          TutorialQuestManager.QuestTutorialFirstGetSilvaEquip:SetActive(false, true)
          TutorialQuestManager.QuestTutorialFirstGetSilvaEquip:SetFinish(true)
        end
        if immanentversion == 1 then
          if areaID == 1 and battleID == 1001 then
            AddFlurryEvent("ClickEnemy_1_1_1", {}, 1)
            AddFlurryEvent("EP_ClickEnemy_1_1_1", {}, 1)
          elseif areaID == 1 and battleID == 1002 then
            AddFlurryEvent("ClickEnemy_1_1_2", {}, 1)
            AddFlurryEvent("EP_ClickEnemy_1_1_2", {}, 1)
          elseif areaID == 1 and battleID == 1103 then
            AddFlurryEvent("TargetBattleMapEvent_1_1_1", {}, 1)
            AddFlurryEvent("EP_TargetBattleMapEvent_1_1_1", {}, 1)
          elseif areaID == 1 and battleID == 1004 then
            AddFlurryEvent("ClickEnemy_1_1_3", {}, 1)
          elseif areaID == 1 and battleID == 1105 then
            AddFlurryEvent("TargetBattleMapEvent_1_1_2", {}, 1)
          elseif areaID == 1 and battleID == 1006 then
            AddFlurryEvent("ClickEnemy_1_1_4_Boss", {}, 1)
          elseif areaID == 1 and battleID == 1007 then
            AddFlurryEvent("ClickEnemy_1_1_5", {}, 1)
          elseif areaID == 1 and battleID == 1008 then
            AddFlurryEvent("ClickEnemy_1_1_6", {}, 1)
          elseif areaID == 1 and battleID == 1109 then
            AddFlurryEvent("TargetBattleMapEvent_1_1_3", {}, 1)
          elseif areaID == 1 and battleID == 1010 then
            AddFlurryEvent("ClickEnemy_1_1_7", {}, 1)
          elseif areaID == 1 and battleID == 1011 then
            AddFlurryEvent("ClickEnemy_1_1_8_Boss", {}, 1)
          elseif areaID == 1 and battleID == 2001 then
            AddFlurryEvent("ClickEnemy_1_2_1", {}, 1)
          end
          if (areaID ~= 1 or battleID ~= 1001) and (areaID ~= 1 or battleID ~= 1002) then
            AddFlurryEvent("EP_ClickEnemy_" .. combinedBattleID, {}, 1)
          end
        elseif immanentversion == 2 then
          local combinedBattleID = GameUtils:CombineBattleID(areaID, battleID)
          local flurryName = k_FlurryEnemyClick[combinedBattleID]
          if flurryName then
            AddFlurryEvent(flurryName, {}, 2)
          end
        end
        local function callback()
          local battleInfo = GameDataAccessHelper:GetBattleInfo(areaID, battleID)
          local event_type = battleInfo.EVENT_TYPE
          GameStateBattlePlay.curBattleType = "pve"
          if event_type == 1 then
            if areaID == 1 and battleID == 1007 then
              TutorialAdjutantManager:RequestAddAdjutantFleet(3)
            end
            if areaID == 60 and self:IsBattleActive(areaID, battleID) and (battleID == 1001 or battleID == 1007) then
              GameUIEnemyInfo.m_currentBattleID = battleID
              GameUIEnemyInfo.m_combinedBattleID = GameUtils:CombineBattleID(areaID, GameUIEnemyInfo.m_currentBattleID)
              GameUIEnemyInfo:BattleNowClicked()
            elseif self:IsSkipEnemyInfo(combinedBattleID) then
              GameUIEnemyInfo:SetAccomplisthLevel(self.m_battleStatus[enemyIndex].status)
              local GameStateFormation = GameStateManager.GameStateFormation
              GameStateFormation:SetBattleID(areaID, battleID)
              GameStateManager:SetCurrentGameState(GameStateFormation)
              GameUIBattleResult:SetCurrentBattleState(self.m_battleStatus[enemyIndex].status)
            else
              GameUIEnemyInfo:ShowEnemyInfo(areaID, battleID, enemyIndex)
            end
          else
            GameUIEvent:ShowEvent(battleID, combinedBattleID)
          end
        end
        GameStateBattleMap:CheckBeforeEnemyStory(battleID, callback)
      end
    end
  elseif cmd == "backToAreaMenu" then
    GameStateCampaignSelection:EnterArea(GameStateBattleMap.m_currentAreaID)
  elseif cmd == "BGClicked" then
    GameUIBarRight:MoveOutRightMenu()
  elseif cmd == "offsetBG" then
    local param_list = LuaUtils:string_split(args, "^")
    local bgPosX = param_list[1]
    local bgPosY = param_list[2]
    local offsetX = param_list[3]
    local offsetY = param_list[4]
    if GameObjectBattleMapBG:GetFlashObject() then
      GameObjectBattleMapBG:GetFlashObject():InvokeASCallback("_root", "offsetBGPos", bgPosX, bgPosY, offsetX, offsetY)
    end
  elseif cmd == "bigBombAnimEnd" then
    DebugOut("\230\137\147\229\141\176 bigBombAnimEnd")
    GameStateManager:SetCurrentGameState(GameStateMainPlanet)
  elseif cmd == "helpTutorial_pos" and GameUtils:GetTutorialHelp() and GameUtils:GetTutorialName() == "explorePlanetary" then
    local param = LuaUtils:string_split(args, "\001")
    GameUtils:ShowMaskLayer(tonumber(param[1]), tonumber(param[2]), GameUIMaskLayer.ButtonPos.left, "empty", GameUIMaskLayer.MaskStyle.small)
  end
end
function GameObjectBattleMap:RestoreBattleMap()
  DebugOut("RestoreBattleMap", self.m_battleMapInfo)
  if not self.m_battleMapInfo or not self:GetFlashObject() then
    return
  end
  local param_list = LuaUtils:string_split(self.m_battleMapInfo, "^")
  local scenePosX = param_list[1]
  local scenePosY = param_list[2]
  local bgX = param_list[3]
  local bgY = param_list[4]
  local shipX = param_list[5]
  local shipY = param_list[6]
  local shipRot = param_list[7]
  local shipDesX = param_list[8]
  local shipDesY = param_list[9]
  DebugOut("restoreInfo", scenePosX, scenePosY, bgX, bgY, shipX, shipY, shipRot, shipDesX, shipDesY)
  self:GetFlashObject():InvokeASCallback("_root", "restoreInfo", scenePosX, scenePosY, bgX, bgY, shipX, shipY, shipRot, shipDesX, shipDesY)
end
function GameObjectBattleMap:HideCloseButton(isHide)
  self:GetFlashObject():InvokeASCallback("_root", "setbtnCloseVisible", not isHide)
end
function GameObjectBattleMap:OnAddToGameState()
  self:LoadFlashObject()
  local scalePosX = 1
  local scalePosY = 1
  if ext.is4x3() then
    scalePosX = 0.9634285714285714
    scalePosY = 1.2
  elseif ext.is16x9() then
    scalePosX = 0.8994285714285715
    scalePosY = 1
  elseif ext.is2x1 and ext.is2x1() then
    scalePosX = 0.8994285714285715
    scalePosY = 1
  end
  if GameObjectTutorialCutscene:IsActive() and GameObjectTutorialCutscene:IsNeedShowNewFakeBattle() then
    DebugOut("OnAddToGameState in fake battle,", GameGlobalData:GetFakeBattleIndex())
    self:GetFlashObject():InvokeASCallback("_root", "setEnemyPos", 1, 120 * scalePosX, 200 * scalePosY)
    self:GetFlashObject():InvokeASCallback("_root", "setEnemyPos", 2, 350 * scalePosX, 110 * scalePosY)
    local bgSize = GameObjectBattleMapBG:GetFlashObject():InvokeASCallback("_root", "getBGSize")
    local param_list = LuaUtils:string_split(bgSize, "^")
    self:GetFlashObject():InvokeASCallback("_root", "initBattleMap", "", "", "", "", param_list[1], param_list[2])
    if GameGlobalData:GetFakeBattleIndex() and GameGlobalData:GetFakeBattleIndex() > 0 then
      self:RefreshEnemyStatus()
    end
  else
    for i, v in ipairs(self.m_battleStatus) do
      local areaID, battle_id = GameUtils:ResolveBattleID(v.battle_id)
      local pos = GameDataAccessHelper:GetBattleInfo(GameStateBattleMap.m_currentAreaID, battle_id).POS
      local posX = pos[1] * scalePosX
      local posY = pos[2] * scalePosY
      self:GetFlashObject():InvokeASCallback("_root", "setEnemyPos", i, posX, posY)
    end
    GameObjectBattleMap:CheckHelpTutorial()
    local GameUIBarLeft = LuaObjectManager:GetLuaObject("GameUIBarLeft")
    GameUIBarLeft:CheckTutorial()
    self:RefreshEnemyStatus()
  end
  GameUtils:PlayMusic("GE2_BattleMap.mp3")
  if immanentversion == 2 and GameStateBattleMap.m_isEnterFromLoading then
    for i, v in pairs(self.m_battleStatus) do
      if tonumber(v.battle_id) == 60001013 and tonumber(v.status) == 0 then
        local function callback()
          GameUIEnemyInfo.m_currentBattleID = 1013
          GameUIEnemyInfo.m_combinedBattleID = GameUtils:CombineBattleID(60, GameUIEnemyInfo.m_currentBattleID)
          GameUIEnemyInfo:BattleNowClicked()
        end
        local storyRef = GameDataAccessHelper:GetBattleAfterStory(60, 1012)
        GameUICommonDialog:PlayStory(storyRef, callback)
      end
    end
  end
end
function GameObjectBattleMap:OnEraseFromGameState()
  self.m_BattleContent = nil
  self.m_battleMapInfo = self:GetFlashObject():InvokeASCallback("_root", "recordBattleMapInfo")
  self:UnloadFlashObject()
end
function GameObjectBattleMap._GetBattleStatusNetCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.pve_map_status_req.Code then
    assert(content.code ~= 0)
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  end
  if msgType == NetAPIList.user_map_status_ack.Code then
    DebugOut("\230\137\147\229\141\176_GetBattleStatusNetCallback")
    DebugTable(content)
    GameObjectBattleMap.m_BattleContent = content
    GameObjectBattleMap.m_battleStatus = content.status
    table.sort(GameObjectBattleMap.m_battleStatus, function(a, b)
      return a.battle_id < b.battle_id
    end)
    GameObjectBattleMap:RefreshEnemyStatus()
    if GameStateManager:GetCurrentGameState() ~= GameStateBattleMap then
      GameStateManager:SetCurrentGameState(GameStateBattleMap)
    end
    if GameObjectBattleMap.m_getBattleStatusCallback then
      GameObjectBattleMap.m_getBattleStatusCallback()
      GameObjectBattleMap.m_getBattleStatusCallback = nil
    end
    return true
  end
  return false
end
function GameObjectBattleMap:CheckIsFinishedTutorialBattle()
  DebugOut("\230\137\147\229\141\176CheckIsFinishedTutorialBattle")
  if not self.m_BattleContent then
    return
  end
  if immanentversion == 2 then
    for k, v in pairs(self.m_BattleContent.status) do
      if tonumber(v.battle_id) == 60001013 and (tonumber(v.status) == 3 or tonumber(v.status) == 2) and not QuestTutorialCityHall:IsActive() and not QuestTutorialCityHall:IsFinished() then
        QuestTutorialCityHall:SetActive(true)
        GameObjectTutorialCutscene:ShowBGAnimAfterBattle()
        GameWaiting:HideLoadingScreen()
        return true
      end
      if tonumber(v.battle_id) == 60001013 and tonumber(v.status) == 0 then
        GameUIEnemyInfo.m_currentBattleID = 1013
        GameUIEnemyInfo.m_combinedBattleID = GameUtils:CombineBattleID(60, GameUIEnemyInfo.m_currentBattleID)
        GameUIEnemyInfo:BattleNowClicked()
        return true
      end
    end
  end
end
function GameObjectBattleMap:IsSkipEnemyInfo(battleCombineID)
  local actID, battleID = GameUtils:ResolveBattleID(battleCombineID)
  if self:IsBattleActive(actID, battleID) and self:IsCanPlayBattle(actID, battleID) then
    return true
  end
  return false
end
function GameObjectBattleMap:IsCanPlayBattle(actID, battleID)
  local requireLevel = 0
  local battleInfo = self:GetBattleInfo(actID, battleID)
  requireLevel = battleInfo.user_level
  local userinfo = GameGlobalData:GetData("levelinfo")
  if requireLevel > userinfo.level then
    return false
  end
  local battle_supply = GameGlobalData:GetData("battle_supply")
  local left_times = battle_supply.current
  if left_times <= 0 then
    return false
  end
  return true
end
function GameObjectBattleMap:RequireBattleMapInfoFromServer(combinedSectionID, callback)
  self.m_getBattleStatusCallback = callback
  local map_status_req = {
    chapter_id = tonumber(combinedSectionID)
  }
  local netFailedCallback
  function netFailedCallback()
    NetMessageMgr:SendMsg(NetAPIList.pve_map_status_req.Code, map_status_req, self._GetBattleStatusNetCallback, true, netFailedCallback)
  end
  NetMessageMgr:SendMsg(NetAPIList.pve_map_status_req.Code, map_status_req, self._GetBattleStatusNetCallback, true, netFailedCallback)
end
function GameObjectBattleMap:CheckTutorial(...)
  if not QuestTutorialPveMapPoint:IsFinished() and not QuestTutorialPveMapPoint:IsActive() then
    QuestTutorialPveMapPoint:SetActive(true, false)
  end
end
function GameObjectBattleMap:CheckHelpTutorial(...)
  if self:GetFlashObject() then
    if GameUtils:GetTutorialHelp() and GameUtils:GetTutorialName() == "explorePlanetary" then
      self:GetFlashObject():InvokeASCallback("_root", "setHelpTutorial", true)
    else
      self:GetFlashObject():InvokeASCallback("_root", "setHelpTutorial", false)
    end
  end
end
if AutoUpdate.isAndroidDevice then
  function GameObjectBattleMap.OnAndroidBack()
    if GameObjectTutorialCutscene:IsActive() and GameObjectTutorialCutscene:IsNeedShowNewFakeBattle() and GameObjectTutorialCutscene:IsNeedShowNewFakeBattle() ~= 6 then
      ext.ShowExitGameDialog()
    elseif GameStateBattleMap.m_currentAreaID == 60 then
      AutoUpdateInBackground:SaveHRFileList()
      if ext.GetBundleIdentifier() == "com.tap4fun.galaxyempire2_android.platform.uc" and AutoUpdate.localAppVersion >= 10608 then
        IPlatformExt.ExitGame()
      else
        ext.ShowExitGameDialog()
      end
    else
      GameStateCampaignSelection:EnterArea(GameStateBattleMap.m_currentAreaID)
    end
  end
end
