local rightMenu = GameData.RightMenu.rightMenu
rightMenu.fleets = {
  MODULE_NAME = "fleets",
  PLAYER_REQ_LEVEL = 0,
  BUILDING_REQ = "undefined",
  BUILDING_REQ_LEVEL = 0,
  BATTLE_ID = 1001103
}
rightMenu.bag = {
  MODULE_NAME = "bag",
  PLAYER_REQ_LEVEL = 5,
  BUILDING_REQ = "undefined",
  BUILDING_REQ_LEVEL = 0,
  BATTLE_ID = 0
}
rightMenu.hire = {
  MODULE_NAME = "hire",
  PLAYER_REQ_LEVEL = 0,
  BUILDING_REQ = "undefined",
  BUILDING_REQ_LEVEL = 0,
  BATTLE_ID = 1002012
}
rightMenu.arena = {
  MODULE_NAME = "arena",
  PLAYER_REQ_LEVEL = 0,
  BUILDING_REQ = "undefined",
  BUILDING_REQ_LEVEL = 0,
  BATTLE_ID = 1002012
}
rightMenu.friends = {
  MODULE_NAME = "friends",
  PLAYER_REQ_LEVEL = 0,
  BUILDING_REQ = "undefined",
  BUILDING_REQ_LEVEL = 0,
  BATTLE_ID = 1003005
}
rightMenu.setting = {
  MODULE_NAME = "setting",
  PLAYER_REQ_LEVEL = 1,
  BUILDING_REQ = "undefined",
  BUILDING_REQ_LEVEL = 0,
  BATTLE_ID = 0
}
rightMenu.wc = {
  MODULE_NAME = "wc",
  PLAYER_REQ_LEVEL = 1,
  BUILDING_REQ = "undefined",
  BUILDING_REQ_LEVEL = 0,
  BATTLE_ID = 0
}
rightMenu.slot = {
  MODULE_NAME = "slot",
  PLAYER_REQ_LEVEL = 0,
  BUILDING_REQ = "undefined",
  BUILDING_REQ_LEVEL = 0,
  BATTLE_ID = 1002005
}
rightMenu.alliance = {
  MODULE_NAME = "alliance",
  PLAYER_REQ_LEVEL = 0,
  BUILDING_REQ = "undefined",
  BUILDING_REQ_LEVEL = 0,
  BATTLE_ID = 1003005
}
rightMenu.enhance = {
  MODULE_NAME = "enhance",
  PLAYER_REQ_LEVEL = 8,
  BUILDING_REQ = "engineering_bay",
  BUILDING_REQ_LEVEL = 1,
  BATTLE_ID = 0
}
rightMenu.chat = {
  MODULE_NAME = "chat",
  PLAYER_REQ_LEVEL = 10,
  BUILDING_REQ = "undefined",
  BUILDING_REQ_LEVEL = 0,
  BATTLE_ID = 0
}
rightMenu.combat_speed = {
  MODULE_NAME = "combat_speed",
  PLAYER_REQ_LEVEL = 10,
  BUILDING_REQ = "undefined",
  BUILDING_REQ_LEVEL = 0,
  BATTLE_ID = 0
}
rightMenu.arena = {
  MODULE_NAME = "arena",
  PLAYER_REQ_LEVEL = 13,
  BUILDING_REQ = "undefined",
  BUILDING_REQ_LEVEL = 0,
  BATTLE_ID = 1002010
}
rightMenu.rush = {
  MODULE_NAME = "rush",
  PLAYER_REQ_LEVEL = 16,
  BUILDING_REQ = "undefined",
  BUILDING_REQ_LEVEL = 0,
  BATTLE_ID = 0
}
rightMenu.equip_evolution = {
  MODULE_NAME = "equip_evolution",
  PLAYER_REQ_LEVEL = 20,
  BUILDING_REQ = "undefined",
  BUILDING_REQ_LEVEL = 0,
  BATTLE_ID = 0
}
rightMenu.elite_challenge = {
  MODULE_NAME = "elite_challenge",
  PLAYER_REQ_LEVEL = 20,
  BUILDING_REQ = "commander_academy ",
  BUILDING_REQ_LEVEL = 0,
  BATTLE_ID = 0
}
rightMenu.infinite_cosmos = {
  MODULE_NAME = "infinite_cosmos",
  PLAYER_REQ_LEVEL = 21,
  BUILDING_REQ = "undefined",
  BUILDING_REQ_LEVEL = 0,
  BATTLE_ID = 0
}
rightMenu.task = {
  MODULE_NAME = "task",
  PLAYER_REQ_LEVEL = 22,
  BUILDING_REQ = "undefined",
  BUILDING_REQ_LEVEL = 0,
  BATTLE_ID = 0
}
rightMenu.tech = {
  MODULE_NAME = "tech",
  PLAYER_REQ_LEVEL = 22,
  BUILDING_REQ = "tech_lab",
  BUILDING_REQ_LEVEL = 1,
  BATTLE_ID = 0
}
rightMenu.colonial = {
  MODULE_NAME = "colonial",
  PLAYER_REQ_LEVEL = 24,
  BUILDING_REQ = "undefined",
  BUILDING_REQ_LEVEL = 0,
  BATTLE_ID = 0
}
rightMenu.wd = {
  MODULE_NAME = "wd",
  PLAYER_REQ_LEVEL = 25,
  BUILDING_REQ = "undefined",
  BUILDING_REQ_LEVEL = 0,
  BATTLE_ID = 0
}
rightMenu.tc = {
  MODULE_NAME = "tc",
  PLAYER_REQ_LEVEL = 28,
  BUILDING_REQ = "undefined",
  BUILDING_REQ_LEVEL = 0,
  BATTLE_ID = 0
}
rightMenu.mining = {
  MODULE_NAME = "mining",
  PLAYER_REQ_LEVEL = 0,
  BUILDING_REQ = "undefined",
  BUILDING_REQ_LEVEL = 0,
  BATTLE_ID = 1005004
}
rightMenu.remodel = {
  MODULE_NAME = "remodel",
  PLAYER_REQ_LEVEL = 30,
  BUILDING_REQ = "factory",
  BUILDING_REQ_LEVEL = 1,
  BATTLE_ID = 0
}
rightMenu.krypton = {
  MODULE_NAME = "krypton",
  PLAYER_REQ_LEVEL = 30,
  BUILDING_REQ = "krypton_center",
  BUILDING_REQ_LEVEL = 1,
  BATTLE_ID = 0
}
rightMenu.worldboss = {
  MODULE_NAME = "worldboss",
  PLAYER_REQ_LEVEL = 32,
  BUILDING_REQ = "undefined",
  BUILDING_REQ_LEVEL = 0,
  BATTLE_ID = 0
}
rightMenu.centalsystem = {
  MODULE_NAME = "centalsystem",
  PLAYER_REQ_LEVEL = 38,
  BUILDING_REQ = "undefined",
  BUILDING_REQ_LEVEL = 0,
  BATTLE_ID = 0
}
rightMenu.ac = {
  MODULE_NAME = "ac",
  PLAYER_REQ_LEVEL = 40,
  BUILDING_REQ = "undefined",
  BUILDING_REQ_LEVEL = 0,
  BATTLE_ID = 0
}
rightMenu.prime_wve = {
  MODULE_NAME = "prime_wve",
  PLAYER_REQ_LEVEL = 40,
  BUILDING_REQ = "undefined",
  BUILDING_REQ_LEVEL = 0,
  BATTLE_ID = 0
}
rightMenu.crusade = {
  MODULE_NAME = "crusade",
  PLAYER_REQ_LEVEL = 45,
  BUILDING_REQ = "undefined",
  BUILDING_REQ_LEVEL = 0,
  BATTLE_ID = 0
}
rightMenu.krypton_refine = {
  MODULE_NAME = "krypton_refine",
  PLAYER_REQ_LEVEL = 65,
  BUILDING_REQ = "krypton_center",
  BUILDING_REQ_LEVEL = 14,
  BATTLE_ID = 0
}
rightMenu.offline = {
  MODULE_NAME = "offline",
  PLAYER_REQ_LEVEL = 1000,
  BUILDING_REQ = "undefined",
  BUILDING_REQ_LEVEL = 0,
  BATTLE_ID = 0
}
rightMenu.starwar = {
  MODULE_NAME = "starwar",
  PLAYER_REQ_LEVEL = 90,
  BUILDING_REQ = "undefined",
  BUILDING_REQ_LEVEL = 0,
  BATTLE_ID = 0
}
rightMenu.artifact = {
  MODULE_NAME = "artifact",
  PLAYER_REQ_LEVEL = 90,
  BUILDING_REQ = "undefined",
  BUILDING_REQ_LEVEL = 0,
  BATTLE_ID = 0
}
rightMenu.empire_colosseum = {
  MODULE_NAME = "empire_colosseum",
  PLAYER_REQ_LEVEL = 90,
  BUILDING_REQ = "undefined",
  BUILDING_REQ_LEVEL = 0,
  BATTLE_ID = 0
}
rightMenu.enchant = {
  MODULE_NAME = "enchant",
  PLAYER_REQ_LEVEL = 55,
  BUILDING_REQ = "undefined",
  BUILDING_REQ_LEVEL = 0,
  BATTLE_ID = 0
}
rightMenu.large_map = {
  MODULE_NAME = "large_map",
  PLAYER_REQ_LEVEL = 40,
  BUILDING_REQ = "undefined",
  BUILDING_REQ_LEVEL = 0,
  BATTLE_ID = 0
}
