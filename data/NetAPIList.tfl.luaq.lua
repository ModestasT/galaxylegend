NetAPIList = {
  keylist_ntf = {Code = 10000, S = "keylist"},
  udid_step_req = {Code = 10001, S = "udid_step"},
  udid_track_req = {Code = 10002, S = "udid_track"},
  protocal_version_req = {
    Code = 10003,
    S = "protocal_version_req"
  },
  common_ack = {Code = 0, S = "common_ack"},
  user_register_req = {
    Code = 1,
    S = "user_register_info"
  },
  user_register_ack = {
    Code = 2,
    S = "user_snapshot"
  },
  user_login_req = {
    Code = 3,
    S = "user_login_info"
  },
  user_login_ack = {
    Code = 4,
    S = "user_snapshot"
  },
  building_upgrade_req = {
    Code = 5,
    S = "building_upgrade_info"
  },
  user_snapshot_req = {Code = 7, S = "null"},
  user_snapshot_ack = {
    Code = 8,
    S = "user_snapshot"
  },
  chat_req = {Code = 9, S = "chat_req"},
  chat_ntf = {Code = 10, S = "chat_ntf"},
  user_logout_req = {Code = 11, S = "null"},
  fleets_req = {Code = 12, S = "null"},
  fleets_ack = {
    Code = 13,
    S = "fleets_info"
  },
  pve_battle_req = {
    Code = 14,
    S = "battle_request"
  },
  user_rush_req = {
    Code = 15,
    S = "battle_request"
  },
  rush_result_ack = {
    Code = 16,
    S = "rush_result_ack"
  },
  user_cdtime_ack = {
    Code = 19,
    S = "user_cdtime_info"
  },
  user_cdtime_req = {Code = 20, S = "null"},
  pve_map_status_req = {
    Code = 21,
    S = "map_status_req"
  },
  user_map_status_ack = {
    Code = 22,
    S = "user_map_status"
  },
  battle_status_req = {
    Code = 23,
    S = "battle_status_req"
  },
  battle_status_ack = {
    Code = 24,
    S = "battle_status_ack"
  },
  battle_matrix_req = {
    Code = 25,
    S = "battle_matrix_req"
  },
  battle_matrix_ack = {
    Code = 26,
    S = "battle_matrix_ack"
  },
  user_matrix_save_req = {Code = 27, S = "matrix"},
  battle_result_ack = {
    Code = 28,
    S = "battle_result_ack"
  },
  bag_req = {Code = 30, S = "bag_req"},
  bag_ack = {Code = 31, S = "bag_info"},
  swap_bag_grid_req = {
    Code = 32,
    S = "swap_bag_grid_req"
  },
  swap_bag_grid_ack = {
    Code = 33,
    S = "swap_bag_grid_info"
  },
  techniques_req = {Code = 34, S = "null"},
  techniques_ack = {
    Code = 35,
    S = "techniques_ack"
  },
  gm_cmd_req = {Code = 36, S = "gm_cmd"},
  pack_bag_req = {Code = 37, S = "pack_bag"},
  techniques_upgrade_req = {
    Code = 38,
    S = "techniques_upgrade_req"
  },
  techniques_upgrade_ack = {
    Code = 39,
    S = "techniques_upgrade_ack"
  },
  revenue_info_req = {Code = 40, S = "null"},
  revenue_info_ack = {
    Code = 41,
    S = "revenue_info_ack"
  },
  revenue_do_req = {
    Code = 42,
    S = "revenue_do_req"
  },
  revenue_do_ack = {
    Code = 43,
    S = "revenue_do_ack"
  },
  add_friend_req = {
    Code = 45,
    S = "add_friend_req"
  },
  del_friend_req = {
    Code = 46,
    S = "del_friend_req"
  },
  officers_req = {Code = 47, S = "null"},
  officers_ack = {
    Code = 48,
    S = "officers_ack"
  },
  officer_buy_req = {
    Code = 49,
    S = "officer_buy_req"
  },
  officer_buy_ack = {
    Code = 50,
    S = "officer_buy_ack"
  },
  recruit_list_req = {Code = 54, S = "null"},
  recruit_list_ack = {
    Code = 55,
    S = "recruit_list_ack"
  },
  recruit_fleet_req = {
    Code = 56,
    S = "recruit_fleet_req"
  },
  recruit_fleet_ack = {
    Code = 57,
    S = "recruit_fleet_ack"
  },
  dismiss_fleet_req = {
    Code = 58,
    S = "dismiss_fleet_req"
  },
  dismiss_fleet_ack = {
    Code = 59,
    S = "dismiss_fleet_ack"
  },
  friends_req = {Code = 60, S = "null"},
  friends_ack = {
    Code = 61,
    S = "friends_ack"
  },
  friend_detail_req = {
    Code = 63,
    S = "friend_detail_req"
  },
  equipment_detail_req = {
    Code = 64,
    S = "equipment_detail_req"
  },
  equipment_detail_ack = {
    Code = 65,
    S = "equipment_detail_ack"
  },
  friend_detail_ack = {
    Code = 66,
    S = "friend_detail"
  },
  friend_search_req = {
    Code = 67,
    S = "friend_search_req"
  },
  friend_search_ack = {
    Code = 68,
    S = "friend_search_ack"
  },
  friend_ban_req = {
    Code = 69,
    S = "friend_ban_req"
  },
  warpgate_status_req = {Code = 70, S = "null"},
  warpgate_status_ack = {
    Code = 71,
    S = "warpgate_info"
  },
  warpgate_charge_req = {
    Code = 72,
    S = "warpgate_charge_req"
  },
  warpgate_charge_ack = {
    Code = 73,
    S = "warpgate_charge_ack"
  },
  warpgate_collect_req = {Code = 74, S = "null"},
  warpgate_collect_ack = {
    Code = 75,
    S = "warpgate_collect_ack"
  },
  warpgate_upgrade_req = {
    Code = 76,
    S = "warpgate_upgrade_req"
  },
  warpgate_upgrade_ack = {
    Code = 77,
    S = "warpgate_upgrade_ack"
  },
  mail_page_req = {
    Code = 78,
    S = "mail_page_req"
  },
  mail_page_ack = {
    Code = 79,
    S = "mail_page_ack"
  },
  mail_content_req = {
    Code = 80,
    S = "mail_content_req"
  },
  mail_content_ack = {
    Code = 81,
    S = "mail_content_ack"
  },
  mail_send_req = {
    Code = 82,
    S = "mail_send_req"
  },
  mail_delete_req = {
    Code = 83,
    S = "mail_delete_req"
  },
  mail_set_read_req = {
    Code = 84,
    S = "mail_set_read_req"
  },
  fight_ack = {
    Code = 86,
    S = "fight_report"
  },
  equipment_enhance_req = {
    Code = 87,
    S = "equipment_enhance_req"
  },
  equipment_evolution_req = {
    Code = 90,
    S = "equipment_evolution_req"
  },
  equipments_req = {Code = 93, S = "null"},
  equipments_ack = {
    Code = 94,
    S = "equipments_ack"
  },
  quark_exchange_req = {
    Code = 95,
    S = "quark_exchange_req"
  },
  quark_exchange_ack = {
    Code = 96,
    S = "quark_exchange_ack"
  },
  shop_purchase_req = {
    Code = 97,
    S = "shop_purchase_req"
  },
  shop_purchase_ack = {
    Code = 98,
    S = "shop_purchase_ack"
  },
  shop_sell_req = {
    Code = 99,
    S = "shop_sell_req"
  },
  shop_sell_ack = {
    Code = 100,
    S = "shop_sell_ack"
  },
  shop_purchase_back_list_req = {Code = 101, S = "null"},
  shop_purchase_back_list_ack = {
    Code = 102,
    S = "shop_purchase_back_list_ack"
  },
  shop_purchase_back_req = {
    Code = 103,
    S = "shop_purchase_back_req"
  },
  shop_purchase_back_ack = {
    Code = 104,
    S = "shop_purchase_back_ack"
  },
  user_offline_info_req = {Code = 105, S = "null"},
  user_offline_info_ack = {
    Code = 106,
    S = "user_offline_info_ack"
  },
  user_collect_offline_expr_req = {Code = 107, S = "null"},
  user_collect_offline_expr_ack = {
    Code = 108,
    S = "user_collect_offline_expr_ack"
  },
  fleet_repair_all_req = {
    Code = 109,
    S = "fleet_repair_all_req"
  },
  fleet_repair_req = {
    Code = 110,
    S = "fleet_repair_req"
  },
  friend_ban_del_req = {
    Code = 112,
    S = "friend_ban_del_req"
  },
  messages_req = {Code = 113, S = "null"},
  messages_ack = {
    Code = 114,
    S = "messages_ack"
  },
  message_del_req = {
    Code = 115,
    S = "message_del_req"
  },
  alliances_req = {
    Code = 116,
    S = "alliances_req"
  },
  alliances_ack = {
    Code = 117,
    S = "alliances_ack"
  },
  alliance_members_req = {
    Code = 118,
    S = "alliance_members_req"
  },
  alliance_members_ack = {
    Code = 119,
    S = "alliance_members_ack"
  },
  alliance_logs_req = {
    Code = 120,
    S = "alliance_logs_req"
  },
  alliance_logs_ack = {
    Code = 121,
    S = "alliance_logs_ack"
  },
  alliance_aduits_req = {
    Code = 122,
    S = "alliance_aduits_req"
  },
  alliance_aduits_ack = {
    Code = 123,
    S = "alliance_aduits_ack"
  },
  alliance_create_req = {
    Code = 124,
    S = "alliance_create_req"
  },
  alliance_create_ack = {
    Code = 125,
    S = "alliance_create_ack"
  },
  alliance_create_fail_ack = {
    Code = 126,
    S = "alliance_create_fail_ack"
  },
  alliance_delete_req = {Code = 127, S = "null"},
  alliance_delete_ack = {
    Code = 128,
    S = "alliance_delete_ack"
  },
  alliance_delete_fail_ack = {
    Code = 129,
    S = "alliance_delete_fail_ack"
  },
  alliance_apply_req = {
    Code = 130,
    S = "alliance_apply_req"
  },
  alliance_apply_ack = {
    Code = 131,
    S = "alliance_apply_ack"
  },
  alliance_apply_fail_ack = {
    Code = 132,
    S = "alliance_apply_fail_ack"
  },
  alliance_unapply_req = {
    Code = 133,
    S = "alliance_unapply_req"
  },
  alliance_unapply_ack = {
    Code = 134,
    S = "alliance_unapply_ack"
  },
  alliance_info_req = {
    Code = 135,
    S = "alliance_info_req"
  },
  alliance_info_ack = {
    Code = 136,
    S = "alliance_info_ack"
  },
  add_friend_ack = {
    Code = 137,
    S = "add_friend_ack"
  },
  krypton_store_req = {
    Code = 140,
    S = "krypton_store_req"
  },
  krypton_store_ack = {
    Code = 141,
    S = "krypton_store_ack"
  },
  krypton_enhance_req = {
    Code = 142,
    S = "krypton_enhance_req"
  },
  krypton_enhance_ack = {
    Code = 143,
    S = "krypton_enhance_ack"
  },
  krypton_equip_req = {
    Code = 144,
    S = "krypton_equip_req"
  },
  krypton_gacha_one_req = {
    Code = 145,
    S = "krypton_gacha_one_req"
  },
  krypton_gacha_one_ack = {
    Code = 146,
    S = "krypton_gacha_one_ack"
  },
  krypton_gacha_some_req = {
    Code = 147,
    S = "krypton_gacha_some_req"
  },
  krypton_gacha_some_ack = {
    Code = 148,
    S = "krypton_gacha_some_ack"
  },
  krypton_equip_off_req = {
    Code = 149,
    S = "krypton_equip_off_req"
  },
  krypton_decompose_req = {
    Code = 150,
    S = "krypton_decompose_req"
  },
  krypton_decompose_ack = {
    Code = 151,
    S = "krypton_decompose_ack"
  },
  alliance_ratify_req = {
    Code = 153,
    S = "alliance_ratify_req"
  },
  alliance_ratify_ack = {
    Code = 154,
    S = "alliance_ratify_ack"
  },
  alliance_refuse_req = {
    Code = 155,
    S = "alliance_refuse_req"
  },
  alliance_refuse_ack = {
    Code = 156,
    S = "alliance_refuse_ack"
  },
  alliance_memo_req = {
    Code = 157,
    S = "alliance_memo_req"
  },
  alliance_memo_ack = {
    Code = 158,
    S = "alliance_memo_ack"
  },
  alliance_memo_fail_ack = {
    Code = 159,
    S = "alliance_memo_fail_ack"
  },
  champion_list_req = {Code = 160, S = "null"},
  champion_list_ack = {
    Code = 161,
    S = "champion_list_ack"
  },
  champion_challenge_req = {
    Code = 162,
    S = "champion_challenge_req"
  },
  champion_challenge_ack = {
    Code = 163,
    S = "champion_challenge_ack"
  },
  champion_top_req = {Code = 164, S = "null"},
  champion_top_ack = {
    Code = 165,
    S = "champion_top_ack"
  },
  champion_reset_cd_req = {Code = 166, S = "null"},
  champion_reset_cd_ack = {
    Code = 167,
    S = "champion_reset_cd_ack"
  },
  champion_get_award_req = {Code = 168, S = "null"},
  champion_get_award_ack = {
    Code = 169,
    S = "champion_get_award_ack"
  },
  krypton_store_swap_req = {
    Code = 179,
    S = "krypton_store_swap_req"
  },
  alliance_quit_req = {Code = 180, S = "null"},
  alliance_quit_ack = {
    Code = 181,
    S = "alliance_quit_ack"
  },
  alliance_quit_fail_ack = {
    Code = 182,
    S = "alliance_quit_fail_ack"
  },
  alliance_transfer_req = {
    Code = 183,
    S = "alliance_transfer_req"
  },
  alliance_transfer_ack = {
    Code = 184,
    S = "alliance_transfer_ack"
  },
  alliance_transfer_fail_ack = {
    Code = 185,
    S = "alliance_transfer_fail_ack"
  },
  alliance_demote_req = {
    Code = 186,
    S = "alliance_demote_req"
  },
  alliance_demote_ack = {
    Code = 187,
    S = "alliance_demote_ack"
  },
  alliance_demote_fail_ack = {
    Code = 188,
    S = "alliance_demote_fail_ack"
  },
  alliance_promote_req = {
    Code = 189,
    S = "alliance_promote_req"
  },
  alliance_promote_ack = {
    Code = 191,
    S = "alliance_promote_ack"
  },
  alliance_promote_fail_ack = {
    Code = 192,
    S = "alliance_promote_fail_ack"
  },
  alliance_kick_req = {
    Code = 193,
    S = "alliance_kick_req"
  },
  alliance_kick_ack = {
    Code = 194,
    S = "alliance_kick_ack"
  },
  alliance_kick_fail_ack = {
    Code = 195,
    S = "alliance_kick_fail_ack"
  },
  krypton_equip_ack = {
    Code = 196,
    S = "krypton_equip_ack"
  },
  krypton_fleet_equip_req = {
    Code = 197,
    S = "krypton_fleet_equip_req"
  },
  user_fight_history_req = {Code = 198, S = "null"},
  user_fight_history_ack = {
    Code = 199,
    S = "user_fight_history_ack"
  },
  user_brief_info_req = {
    Code = 200,
    S = "user_brief_info_req"
  },
  user_brief_info_ack = {
    Code = 201,
    S = "user_brief_info"
  },
  krypton_fleet_move_req = {
    Code = 202,
    S = "krypton_fleet_move_req"
  },
  alliance_unapply_fail_ack = {
    Code = 203,
    S = "alliance_unapply_fail_ack"
  },
  alliance_info_ntf = {
    Code = 204,
    S = "alliance_info_ntf"
  },
  user_challenged_ack = {
    Code = 205,
    S = "user_challenged_ack"
  },
  task_list_req = {Code = 206, S = "null"},
  task_list_ack = {
    Code = 207,
    S = "task_list_ack"
  },
  get_task_reward_req = {
    Code = 208,
    S = "get_task_reward_req"
  },
  battle_fight_report_req = {
    Code = 210,
    S = "battle_fight_report_req"
  },
  battle_fight_report_ack = {
    Code = 211,
    S = "battle_fight_report_ack"
  },
  shop_buy_and_wear_req = {
    Code = 212,
    S = "shop_buy_and_wear_req"
  },
  prestige_info_req = {Code = 214, S = "null"},
  prestige_info_ack = {
    Code = 215,
    S = "prestige_info_ack"
  },
  prestige_obtain_award_req = {
    Code = 216,
    S = "prestige_obtain_award_req"
  },
  prestige_obtain_award_ack = {
    Code = 217,
    S = "prestige_obtain_award_ack"
  },
  prestige_complete_ntf = {
    Code = 218,
    S = "prestige_complete_ntf"
  },
  achievement_list_req = {Code = 219, S = "null"},
  achievement_list_ack = {
    Code = 220,
    S = "achievement_list_ack"
  },
  achievement_finish_ntf = {
    Code = 223,
    S = "achievement_finish_ntf"
  },
  fleets_ntf = {
    Code = 226,
    S = "fleets_info"
  },
  resource_ntf = {
    Code = 227,
    S = "user_resource"
  },
  cd_time_ntf = {
    Code = 228,
    S = "user_cdtime_info"
  },
  fleet_kryptons_ntf = {
    Code = 229,
    S = "fleet_kryptons_array"
  },
  supply_ntf = {
    Code = 230,
    S = "supply_notify"
  },
  level_ntf = {Code = 231, S = "level_info"},
  equipments_update_ntf = {
    Code = 232,
    S = "equipments_update_ntf"
  },
  attributes_change_ntf = {
    Code = 233,
    S = "attributes_change_ntf"
  },
  progress_ntf = {
    Code = 234,
    S = "user_progress"
  },
  buildings_ntf = {
    Code = 235,
    S = "user_buildings"
  },
  offline_changed_ntf = {
    Code = 236,
    S = "user_offline_info"
  },
  fleet_matrix_ntf = {Code = 238, S = "matrix"},
  heart_beat_req = {Code = 239, S = "null"},
  task_finished_ntf = {
    Code = 240,
    S = "task_statistic_info"
  },
  mail_unread_ntf = {
    Code = 241,
    S = "mail_unread_info"
  },
  passport_bind_req = {
    Code = 242,
    S = "passport_bind_req"
  },
  passport_bind_ack = {
    Code = 243,
    S = "passport_bind_ack"
  },
  act_status_req = {
    Code = 245,
    S = "act_status_req"
  },
  act_status_ack = {
    Code = 246,
    S = "act_status_ack"
  },
  vip_info_ntf = {Code = 247, S = "vip_info"},
  revenue_exchange_req = {Code = 248, S = "null"},
  supply_info_req = {
    Code = 249,
    S = "supply_type"
  },
  supply_info_ack = {
    Code = 250,
    S = "supply_info_ack"
  },
  supply_exchange_req = {
    Code = 251,
    S = "supply_type"
  },
  connection_close_req = {Code = 252, S = "null"},
  cdtimes_clear_req = {
    Code = 253,
    S = "cdtimes_clear_req"
  },
  system_configuration_req = {
    Code = 254,
    S = "system_configuration_req"
  },
  system_configuration_ack = {
    Code = 255,
    S = "system_configuration_ack"
  },
  pay_verify_req = {
    Code = 256,
    S = "pay_verify_req"
  },
  pay_list_req = {
    Code = 257,
    S = "pay_list_req"
  },
  pay_list_ack = {
    Code = 258,
    S = "pay_list_ack"
  },
  use_item_req = {
    Code = 259,
    S = "use_item_req"
  },
  sign_activity_req = {Code = 260, S = "null"},
  sign_activity_ack = {
    Code = 261,
    S = "sign_activite"
  },
  sign_activity_reward_req = {
    Code = 262,
    S = "sign_activity_reward_req"
  },
  create_tipoff_req = {
    Code = 263,
    S = "create_tipoff_req"
  },
  server_kick_user_ntf = {
    Code = 264,
    S = "server_kick"
  },
  bag_grid_ntf = {Code = 265, S = "bag_info"},
  update_spell_ntf = {Code = 266, S = "null"},
  level_up_ntf = {
    Code = 267,
    S = "level_up_ntf"
  },
  pve_awards_get_req = {
    Code = 268,
    S = "battle_request"
  },
  pve_awards_get_ack = {
    Code = 269,
    S = "battle_result"
  },
  price_req = {
    Code = 270,
    S = "price_request"
  },
  price_ack = {
    Code = 271,
    S = "price_response"
  },
  module_status_ntf = {
    Code = 272,
    S = "modules_status"
  },
  all_act_status_req = {
    Code = 273,
    S = "all_act_status_req"
  },
  all_act_status_ack = {
    Code = 274,
    S = "all_act_status_ack"
  },
  special_battle_req = {
    Code = 275,
    S = "battle_request"
  },
  special_battle_ack = {
    Code = 276,
    S = "special_battle_ack"
  },
  battle_money_collect_req = {
    Code = 277,
    S = "battle_money_collect_req"
  },
  quest_ntf = {Code = 278, S = "quest"},
  quest_loot_req = {
    Code = 279,
    S = "quest_loot_req"
  },
  quest_req = {Code = 280, S = "null"},
  event_loot_ack = {
    Code = 281,
    S = "event_loot_ack"
  },
  battle_event_status_ack = {
    Code = 282,
    S = "battle_event_status_ack"
  },
  adventure_map_status_req = {
    Code = 283,
    S = "map_status_req"
  },
  adventure_battle_req = {
    Code = 284,
    S = "battle_request"
  },
  adventure_cd_clear_req = {
    Code = 285,
    S = "adv_chapter"
  },
  adventure_rush_req = {
    Code = 286,
    S = "battle_request"
  },
  adventure_cd_price_req = {
    Code = 288,
    S = "adv_chapter"
  },
  loud_cast_ntf = {Code = 292, S = "loud_cast"},
  mine_list_req = {Code = 293, S = "null"},
  mine_refresh_req = {
    Code = 294,
    S = "mine_refresh_req"
  },
  mine_digg_req = {
    Code = 295,
    S = "mine_digg_req"
  },
  mine_atk_req = {
    Code = 296,
    S = "mine_atk_req"
  },
  mine_speedup_req = {Code = 297, S = "null"},
  mine_boost_req = {Code = 298, S = "null"},
  mine_list_ntf = {
    Code = 299,
    S = "mine_list_ntf"
  },
  mine_refresh_ntf = {
    Code = 300,
    S = "mine_refresh_ntf"
  },
  mine_info_ntf = {
    Code = 301,
    S = "mine_info_ntf"
  },
  mine_complete_ntf = {
    Code = 304,
    S = "mine_complete_ntf"
  },
  mine_atk_ntf = {
    Code = 305,
    S = "mine_atk_ntf"
  },
  mine_info_req = {Code = 306, S = "null"},
  mine_reset_atk_cd_req = {Code = 307, S = "null"},
  event_supply_ack = {
    Code = 310,
    S = "pve_supply_ack"
  },
  supply_loot_req = {Code = 311, S = "null"},
  supply_loot_ack = {
    Code = 312,
    S = "supply_loot_ack"
  },
  supply_loot_get_req = {Code = 313, S = "null"},
  update_guide_progress_req = {
    Code = 314,
    S = "guide_progress"
  },
  guide_progress_ntf = {
    Code = 315,
    S = "guide_progress"
  },
  laba_req = {Code = 316, S = "laba_req"},
  laba_ntf = {Code = 317, S = "laba_info"},
  laba_rate_reroll_req = {Code = 318, S = "null"},
  laba_info_req = {Code = 319, S = "null"},
  laba_award_req = {
    Code = 320,
    S = "laba_award_req"
  },
  dexter_libs_ntf = {
    Code = 321,
    S = "dexter_libs_ntf"
  },
  center_ntf = {
    Code = 322,
    S = "player_center_ntf"
  },
  chat_error_ntf = {
    Code = 323,
    S = "chat_error_ntf"
  },
  top_force_list_req = {Code = 324, S = "null"},
  top_force_list_ntf = {
    Code = 325,
    S = "top_force_list_ntf"
  },
  supply_loot_ntf = {
    Code = 326,
    S = "supply_loot_ntf"
  },
  ver_check_req = {Code = 327, S = "client_ver"},
  refresh_time_ntf = {
    Code = 328,
    S = "refresh_time_ntf"
  },
  ac_info_req = {Code = 329, S = "null"},
  ac_ntf = {Code = 330, S = "ac_ntf"},
  ac_battle_req = {Code = 331, S = "null"},
  ac_energy_charge_req = {Code = 332, S = "null"},
  ac_energy_charge_ack = {
    Code = 333,
    S = "ac_energy_charge_ack"
  },
  ac_jam_req = {Code = 334, S = "null"},
  ac_battle_info_req = {Code = 335, S = "null"},
  ac_matrix_req = {Code = 336, S = "null"},
  gateway_info_ntf = {
    Code = 337,
    S = "gateway_info_ntf"
  },
  notice_set_read_req = {
    Code = 338,
    S = "notice_info"
  },
  gateway_clear_req = {
    Code = 339,
    S = "gateway_info"
  },
  alliance_activities_req = {
    Code = 340,
    S = "alliance_activities_req"
  },
  alliance_activities_ack = {
    Code = 341,
    S = "alliance_activities_ack"
  },
  alliance_weekend_award_req = {Code = 342, S = "null"},
  alliance_weekend_award_ack = {
    Code = 343,
    S = "alliance_weekend_award_ack"
  },
  block_devil_info_req = {
    Code = 344,
    S = "block_devil_info_req"
  },
  block_devil_ntf = {
    Code = 345,
    S = "block_devil_ntf"
  },
  block_devil_req = {
    Code = 346,
    S = "block_devil_req"
  },
  block_devil_complete_ntf = {
    Code = 347,
    S = "block_devil_complete_ntf"
  },
  donate_req = {Code = 348, S = "donate_req"},
  donate_ack = {Code = 349, S = "donate_ack"},
  donate_info_req = {Code = 350, S = "null"},
  donate_info_ack = {
    Code = 351,
    S = "donate_info_ack"
  },
  alliance_level_info_ntf = {
    Code = 352,
    S = "alliance_level_info"
  },
  login_first_ntf = {
    Code = 353,
    S = "login_first_ntf"
  },
  champion_top_record_req = {Code = 354, S = "null"},
  champion_top_record_ack = {
    Code = 355,
    S = "user_fight_history_ack"
  },
  login_time_current_ntf = {
    Code = 356,
    S = "login_time_current_ntf"
  },
  champion_rank_reward_req = {Code = 357, S = "null"},
  champion_rank_reward_ack = {
    Code = 358,
    S = "champion_rank_reward_ack"
  },
  first_purchase_ntf = {
    Code = 359,
    S = "first_purchase"
  },
  vip_loot_req = {Code = 360, S = "null"},
  vip_loot_ack = {
    Code = 361,
    S = "level_loots"
  },
  level_loot_req = {Code = 362, S = "null"},
  level_loot_ack = {
    Code = 363,
    S = "level_loots"
  },
  vip_loot_get_req = {
    Code = 364,
    S = "level_loot_req"
  },
  level_loot_get_req = {
    Code = 365,
    S = "level_loot_req"
  },
  active_gifts_ntf = {
    Code = 366,
    S = "active_gifts_ntf"
  },
  active_gifts_req = {
    Code = 367,
    S = "active_gifts_req"
  },
  krypton_energy_req = {Code = 368, S = "null"},
  promotions_ntf = {
    Code = 369,
    S = "promotions_ntf"
  },
  promotions_req = {Code = 370, S = "null"},
  promotion_award_req = {
    Code = 371,
    S = "promotion_award_req"
  },
  promotion_award_ack = {
    Code = 372,
    S = "promotion_award_ack"
  },
  ready_world_boss_req = {
    Code = 373,
    S = "ready_world_boss_req"
  },
  challenge_world_boss_req = {Code = 374, S = "null"},
  exit_world_boss_req = {Code = 375, S = "null"},
  world_boss_info_ntf = {
    Code = 376,
    S = "world_boss_info_ntf"
  },
  challenge_world_boss_ack = {
    Code = 377,
    S = "challenge_world_boss_ack"
  },
  ready_world_boss_ack = {
    Code = 378,
    S = "ready_world_boss_ack"
  },
  add_world_boss_force_rate_req = {Code = 379, S = "null"},
  clean_world_boss_cd_req = {Code = 380, S = "null"},
  add_world_boss_force_rate_ack = {
    Code = 381,
    S = "add_world_boss_force_rate_ack"
  },
  allstars_reward_req = {
    Code = 382,
    S = "allstars_reward_req"
  },
  allstars_reward_ack = {
    Code = 383,
    S = "allstars_reward_ack"
  },
  timelimit_hero_id_req = {Code = 384, S = "null"},
  timelimit_hero_id_ack = {
    Code = 385,
    S = "timelimit_hero_id_ack"
  },
  fleet_sale_with_rebate_list_req = {Code = 386, S = "null"},
  fleet_sale_with_rebate_list_ack = {
    Code = 387,
    S = "fleet_sale_with_rebate_list_ack"
  },
  promotion_detail_req = {
    Code = 388,
    S = "promotion_detail_req"
  },
  promotion_detail_ack = {
    Code = 389,
    S = "promotion_detail_ack"
  },
  cut_off_list_req = {Code = 390, S = "null"},
  cut_off_list_ack = {
    Code = 391,
    S = "cut_off_list_ack"
  },
  buy_cut_off_req = {
    Code = 392,
    S = "buy_cut_off_req"
  },
  promotion_award_ntf = {
    Code = 393,
    S = "promotion_award_ntf"
  },
  new_activities_compare_req = {
    Code = 394,
    S = "new_activities_compare_req"
  },
  activities_button_status_ntf = {
    Code = 395,
    S = "activities_button_status_ntf"
  },
  krypton_fragment_core_ntf = {
    Code = 396,
    S = "krypton_fragment_core_ntf"
  },
  activities_button_first_status_req = {
    Code = 397,
    S = "activities_button_first_status_req"
  },
  stores_req = {Code = 400, S = "stores_req"},
  stores_ack = {Code = 401, S = "stores_ack"},
  stores_buy_req = {
    Code = 403,
    S = "stores_buy_req"
  },
  equipment_action_list_req = {
    Code = 404,
    S = "equipment_action_list_req"
  },
  equipment_action_list_ack = {
    Code = 405,
    S = "equipment_action_list_ack"
  },
  mail_to_alliance_req = {
    Code = 406,
    S = "mail_to_alliance_req"
  },
  chat_history_async_req = {Code = 407, S = "null"},
  thanksgiven_rank_req = {Code = 408, S = "null"},
  thanksgiven_rank_ack = {
    Code = 409,
    S = "thanksgiven_box_rank"
  },
  festival_req = {Code = 410, S = "null"},
  festival_ack = {
    Code = 411,
    S = "festival_info"
  },
  verify_redeem_code_req = {
    Code = 412,
    S = "verify_redeem_code_req"
  },
  verify_redeem_code_ack = {
    Code = 413,
    S = "verify_redeem_code_ack"
  },
  items_count_ntf = {
    Code = 414,
    S = "items_count_ntf"
  },
  thanksgiven_champion_ntf = {
    Code = 415,
    S = "thanksgiven_champion_ntf"
  },
  item_use_award_ntf = {
    Code = 416,
    S = "item_use_award_ntf"
  },
  game_addtion_req = {
    Code = 417,
    S = "addition_info"
  },
  colony_info_ntf = {
    Code = 418,
    S = "colony_info_ntf"
  },
  colony_exp_ntf = {
    Code = 419,
    S = "colony_exp_ntf"
  },
  colony_times_ntf = {
    Code = 420,
    S = "colony_times_ntf"
  },
  colony_logs_ntf = {
    Code = 421,
    S = "colony_logs_ntf"
  },
  colony_users_req = {
    Code = 422,
    S = "colony_users_req"
  },
  colony_users_ack = {
    Code = 423,
    S = "colony_users_ack"
  },
  colony_challenge_req = {
    Code = 424,
    S = "colony_challenge_req"
  },
  colony_challenge_ack = {
    Code = 425,
    S = "colony_challenge_ack"
  },
  colony_info_test_req = {Code = 426, S = "null"},
  colony_info_test_ack = {
    Code = 427,
    S = "colony_info_test_ack"
  },
  colony_exploit_req = {
    Code = 428,
    S = "colony_exploit_req"
  },
  colony_exploit_ack = {
    Code = 429,
    S = "colony_exploit_ack"
  },
  colony_fawn_req = {
    Code = 430,
    S = "colony_fawn_req"
  },
  colony_fawn_ack = {
    Code = 431,
    S = "colony_fawn_ack"
  },
  activity_stores_req = {
    Code = 432,
    S = "activity_stores_req"
  },
  activity_stores_ack = {
    Code = 433,
    S = "activity_stores_ack"
  },
  activity_stores_buy_req = {
    Code = 434,
    S = "activity_stores_buy_req"
  },
  yys_ntf = {Code = 435, S = "yys_dict"},
  colony_release_req = {
    Code = 436,
    S = "colony_release_req"
  },
  colony_release_ack = {
    Code = 437,
    S = "colony_release_ack"
  },
  colony_action_purchase_req = {
    Code = 438,
    S = "colony_action_purchase_req"
  },
  colony_players_req = {
    Code = 439,
    S = "colony_players_req"
  },
  colony_players_ack = {
    Code = 440,
    S = "colony_players_ack"
  },
  prestige_get_req = {
    Code = 441,
    S = "prestige_get_req"
  },
  alliance_defence_req = {Code = 442, S = "null"},
  alliance_defence_ntf = {
    Code = 443,
    S = "alliance_defence_ntf"
  },
  alliance_set_defender_req = {
    Code = 444,
    S = "alliance_set_defender_req"
  },
  alliance_notifies_req = {
    Code = 445,
    S = "alliance_notifies_req"
  },
  alliance_resource_ntf = {
    Code = 446,
    S = "alliance_resource_ntf"
  },
  alliance_defence_repair_req = {Code = 447, S = "null"},
  alliance_defence_donate_req = {
    Code = 448,
    S = "alliance_defence_donate_req"
  },
  alliance_activity_times_ntf = {
    Code = 449,
    S = "alliance_activity_times_ntf"
  },
  fleet_info_req = {
    Code = 450,
    S = "fleet_info_req"
  },
  fleet_info_ack = {
    Code = 451,
    S = "fleet_info_ack"
  },
  common_ntf = {Code = 452, S = "common_ntf"},
  warn_req = {Code = 453, S = "warn_info"},
  krypton_info_req = {
    Code = 454,
    S = "krypton_info_req"
  },
  krypton_info_ack = {
    Code = 455,
    S = "krypton_info_ack"
  },
  items_req = {Code = 456, S = "items_req"},
  items_ack = {Code = 457, S = "items_ack"},
  client_effect_req = {
    Code = 458,
    S = "client_device_info"
  },
  client_effect_ack = {
    Code = 459,
    S = "client_device_ack"
  },
  award_receive_req = {
    Code = 460,
    S = "award_receive_req"
  },
  client_fps_req = {
    Code = 461,
    S = "client_fps_info"
  },
  taobao_trade_req = {
    Code = 462,
    S = "taobao_trade_req"
  },
  domination_join_req = {
    Code = 464,
    S = "domination_join_req"
  },
  alliance_domination_ntf = {
    Code = 465,
    S = "alliance_domination_ntf"
  },
  alliance_flag_req = {
    Code = 466,
    S = "alliance_flag_req"
  },
  domination_challenge_req = {
    Code = 467,
    S = "domination_challenge_req"
  },
  domination_challenge_ack = {
    Code = 468,
    S = "domination_challenge_ack"
  },
  domination_awards_list_req = {Code = 469, S = "null"},
  domination_awards_list_ack = {
    Code = 470,
    S = "domination_awards_list_ack"
  },
  domination_ranks_req = {
    Code = 471,
    S = "domination_ranks_req"
  },
  domination_ranks_ack = {
    Code = 472,
    S = "domination_ranks_ack"
  },
  alliance_domination_battle_ntf = {
    Code = 473,
    S = "alliance_domination_battle_ntf"
  },
  alliance_domination_defence_ntf = {
    Code = 474,
    S = "alliance_domination_defence_ntf"
  },
  alliance_defence_revive_req = {Code = 475, S = "null"},
  wd_star_req = {Code = 476, S = "null"},
  wd_star_ack = {
    Code = 477,
    S = "all_wd_star"
  },
  wd_award_req = {
    Code = 478,
    S = "wd_award_req"
  },
  wd_award_ack = {
    Code = 479,
    S = "wd_award_lists"
  },
  alliance_domination_total_rank_ntf = {
    Code = 480,
    S = "alliance_domination_total_rank_ntf"
  },
  wd_last_champion_req = {Code = 481, S = "null"},
  wd_last_champion_ack = {
    Code = 482,
    S = "wd_last_champion"
  },
  device_info_req = {
    Code = 483,
    S = "device_info_req"
  },
  wd_ranking_rank_req = {Code = 484, S = "null"},
  wd_ranking_rank_ack = {
    Code = 485,
    S = "wd_ranking_box_rank"
  },
  wd_ranking_champion_ntf = {
    Code = 486,
    S = "wd_ranking_champion_ntf"
  },
  open_box_info_req = {
    Code = 487,
    S = "open_box_info_req"
  },
  open_box_info_ack = {
    Code = 488,
    S = "open_box_info"
  },
  open_box_open_req = {
    Code = 489,
    S = "open_box_open_req"
  },
  open_box_open_ack = {
    Code = 490,
    S = "open_box_open_ack"
  },
  open_box_refresh_req = {
    Code = 491,
    S = "open_box_refresh_req"
  },
  open_box_refresh_ack = {
    Code = 492,
    S = "open_box_refresh_ack"
  },
  open_box_recent_ntf = {
    Code = 493,
    S = "open_box_recent_ntf"
  },
  battle_rate_ntf = {
    Code = 494,
    S = "battle_rate_ntf"
  },
  set_game_local_req = {
    Code = 495,
    S = "set_game_local_req"
  },
  contention_map_req = {
    Code = 496,
    S = "contention_map_req"
  },
  contention_map_ack = {
    Code = 497,
    S = "contention_map_ack"
  },
  contention_notifies_req = {
    Code = 498,
    S = "contention_notifies_req"
  },
  contention_info_ntf = {
    Code = 499,
    S = "contention_info_ntf"
  },
  contention_trip_ntf = {
    Code = 500,
    S = "contention_trip_ntf"
  },
  contention_station_ntf = {
    Code = 501,
    S = "contention_station_ntf"
  },
  contention_starting_req = {
    Code = 502,
    S = "contention_starting_req"
  },
  contention_revive_req = {
    Code = 503,
    S = "contention_revive_req"
  },
  contention_stop_req = {
    Code = 504,
    S = "contention_stop_req"
  },
  contention_stop_ack = {
    Code = 505,
    S = "contention_stop_ack"
  },
  alliance_domination_status_req = {Code = 506, S = "null"},
  alliance_domination_status_ack = {
    Code = 507,
    S = "alliance_domination_status_ack"
  },
  domination_buffer_of_rank_ntf = {
    Code = 508,
    S = "domination_buffer_of_rank_ntf"
  },
  activity_task_list_req = {Code = 509, S = "null"},
  activity_task_list_ack = {
    Code = 510,
    S = "activity_task_list_ack"
  },
  activity_task_reward_req = {
    Code = 511,
    S = "activity_task_reward_req"
  },
  activity_task_list_ntf = {
    Code = 512,
    S = "activity_task_list_ntf"
  },
  contention_battle_ntf = {
    Code = 513,
    S = "contention_battle_ntf"
  },
  contention_name_ntf = {
    Code = 514,
    S = "contention_name_ntf"
  },
  contention_occupier_req = {
    Code = 515,
    S = "contention_occupier_req"
  },
  contention_occupier_ack = {
    Code = 516,
    S = "contention_occupier_ack"
  },
  contention_transfer_req = {
    Code = 517,
    S = "contention_transfer_req"
  },
  contention_production_ntf = {
    Code = 518,
    S = "contention_production_ntf"
  },
  contention_collect_req = {
    Code = 519,
    S = "contention_collect_req"
  },
  contention_collect_ack = {
    Code = 520,
    S = "contention_collect_ack"
  },
  contention_logs_req = {
    Code = 521,
    S = "contention_logs_req"
  },
  contention_logs_ack = {
    Code = 522,
    S = "contention_logs_ack"
  },
  contention_logbattle_detail_req = {
    Code = 523,
    S = "contention_logbattle_detail_req"
  },
  contention_logbattle_detail_ack = {
    Code = 524,
    S = "contention_logbattle_detail_ack"
  },
  activity_status_req = {
    Code = 525,
    S = "activity_status_req"
  },
  activity_status_ack = {
    Code = 526,
    S = "activity_status_ack"
  },
  screen_track_req = {
    Code = 527,
    S = "screen_track"
  },
  contention_jump_ntf = {
    Code = 528,
    S = "contention_jump_ntf"
  },
  tango_invited_friends_req = {
    Code = 529,
    S = "tango_invited_friends_req"
  },
  tango_invited_friends_ack = {
    Code = 530,
    S = "tango_invited_friends_ack"
  },
  tango_invite_req = {
    Code = 531,
    S = "tango_invite_req"
  },
  dungeon_open_req = {Code = 532, S = "null"},
  dungeon_open_ack = {
    Code = 533,
    S = "dungeon_open_ack"
  },
  dungeon_enter_req = {
    Code = 534,
    S = "dungeon_enter_req"
  },
  ladder_enter_ack = {
    Code = 535,
    S = "ladder_enter_ack"
  },
  ladder_search_req = {
    Code = 536,
    S = "ladder_search_req"
  },
  ladder_search_ack = {
    Code = 537,
    S = "ladder_search_ack"
  },
  ladder_raids_req = {
    Code = 538,
    S = "ladder_raids_req"
  },
  ladder_raids_ack = {
    Code = 539,
    S = "ladder_raids_ack"
  },
  ladder_cancel_req = {
    Code = 540,
    S = "ladder_cancel_req"
  },
  ladder_fresh_req = {
    Code = 541,
    S = "ladder_fresh_req"
  },
  ladder_fresh_ack = {
    Code = 542,
    S = "ladder_fresh_ack"
  },
  ladder_gain_req = {
    Code = 543,
    S = "ladder_gain_req"
  },
  ladder_buy_search_req = {
    Code = 544,
    S = "ladder_buy_search_req"
  },
  ladder_buy_search_ack = {
    Code = 545,
    S = "ladder_buy_search_ack"
  },
  ladder_buy_reset_req = {
    Code = 546,
    S = "ladder_buy_reset_req"
  },
  ladder_buy_reset_ack = {
    Code = 547,
    S = "ladder_buy_reset_ack"
  },
  ladder_rank_req = {
    Code = 548,
    S = "ladder_rank_req"
  },
  ladder_rank_ack = {
    Code = 549,
    S = "ladder_rank_ack"
  },
  ladder_special_req = {
    Code = 550,
    S = "ladder_special_req"
  },
  ladder_special_ack = {
    Code = 551,
    S = "ladder_special_ack"
  },
  pay_animation_ntf = {
    Code = 552,
    S = "pay_animation_ntf"
  },
  hero_level_attr_diff_req = {
    Code = 553,
    S = "hero_level_attr_diff_req"
  },
  hero_level_attr_diff_ack = {
    Code = 554,
    S = "hero_level_attr_diff_ack"
  },
  fleet_enhance_req = {
    Code = 555,
    S = "fleet_enhance_req"
  },
  fleet_weaken_req = {
    Code = 556,
    S = "fleet_weaken_req"
  },
  contention_rank_req = {
    Code = 557,
    S = "contention_rank_req"
  },
  contention_rank_ack = {
    Code = 558,
    S = "contention_rank_ack"
  },
  fleet_show_req = {
    Code = 559,
    S = "fleet_show_req"
  },
  fleet_show_ack = {
    Code = 560,
    S = "fleet_show_ack"
  },
  contention_award_req = {
    Code = 561,
    S = "contention_award_req"
  },
  contention_award_ack = {
    Code = 562,
    S = "contention_award_ack"
  },
  contention_history_req = {
    Code = 563,
    S = "contention_history_req"
  },
  contention_history_ack = {
    Code = 564,
    S = "contention_history_ack"
  },
  orders_ntf = {Code = 565, S = "orders_ntf"},
  pay_list2_req = {
    Code = 566,
    S = "pay_list2_req"
  },
  pay_list2_ack = {
    Code = 567,
    S = "pay_list2_ack"
  },
  pay_verify2_req = {
    Code = 568,
    S = "pay_verify2_req"
  },
  pay_verify2_ack = {
    Code = 569,
    S = "pay_verify2_ack"
  },
  pay_confirm_req = {
    Code = 570,
    S = "pay_confirm_req"
  },
  contention_hit_ntf = {
    Code = 571,
    S = "contention_hit_ntf"
  },
  items_ntf = {Code = 572, S = "items_ntf"},
  user_notifies_req = {
    Code = 573,
    S = "user_notifies_req"
  },
  ip_ntf = {Code = 574, S = "ip_ntf"},
  mail_goods_req = {
    Code = 575,
    S = "mail_goods_req"
  },
  mail_goods_ack = {
    Code = 576,
    S = "mail_goods_ack"
  },
  mulmatrix_get_req = {
    Code = 577,
    S = "mulmatrix_get_req"
  },
  mulmatrix_get_ack = {
    Code = 578,
    S = "mulmatrix_info"
  },
  mulmatrix_save_req = {
    Code = 579,
    S = "mulmatrix_one"
  },
  mulmatrix_buy_req = {Code = 580, S = "null"},
  mulmatrix_blank_ntf = {
    Code = 581,
    S = "mulmatrix_blank"
  },
  mulmatrix_price_req = {Code = 582, S = "null"},
  mulmatrix_price_ack = {
    Code = 583,
    S = "mulmatrix_price"
  },
  equip_info_req = {
    Code = 584,
    S = "equip_info_req"
  },
  equip_info_ack = {
    Code = 585,
    S = "equip_info_ack"
  },
  mycard_error_ntf = {
    Code = 586,
    S = "mycard_error"
  },
  ladder_reset_req = {
    Code = 587,
    S = "ladder_reset_req"
  },
  ladder_reset_ack = {
    Code = 588,
    S = "ladder_fresh_ack"
  },
  contention_king_info_req = {Code = 589, S = "null"},
  contention_king_info_ack = {
    Code = 590,
    S = "contention_king_info_ack"
  },
  contention_king_change_ntf = {
    Code = 591,
    S = "contention_king_change_ntf"
  },
  award_list_req = {Code = 592, S = "null"},
  award_list_ack = {
    Code = 593,
    S = "award_list_ack"
  },
  award_get_req = {
    Code = 594,
    S = "award_get_req"
  },
  award_get_all_req = {Code = 595, S = "null"},
  award_count_ntf = {
    Code = 596,
    S = "award_count_ntf"
  },
  contention_mission_info_req = {
    Code = 597,
    S = "contention_mission_info_req"
  },
  contention_mission_info_ack = {
    Code = 598,
    S = "contention_mission_info_ack"
  },
  contention_mission_award_req = {
    Code = 599,
    S = "contention_mission_award_req"
  },
  contention_leave_req = {Code = 600, S = "null"},
  adventure_chapter_status_ntf = {
    Code = 601,
    S = "adv_chapter_status_ntf"
  },
  pay_record_ntf = {
    Code = 602,
    S = "pay_record_ntf"
  },
  login_token_ntf = {
    Code = 603,
    S = "login_token_ntf"
  },
  dungeon_enter_ack = {
    Code = 604,
    S = "dungeon_enter_ack"
  },
  change_auto_decompose_req = {
    Code = 605,
    S = "change_auto_decompose"
  },
  change_auto_decompose_ntf = {
    Code = 606,
    S = "change_auto_decompose_ntf"
  },
  enchant_req = {
    Code = 607,
    S = "enchant_req"
  },
  enchant_ack = {
    Code = 608,
    S = "enchant_ack"
  },
  activity_dna_req = {
    Code = 609,
    S = "activity_dna_req"
  },
  activity_dna_ack = {
    Code = 610,
    S = "activity_dna_ack"
  },
  activity_dna_charge_req = {
    Code = 611,
    S = "activity_dna_charge_req"
  },
  akt_dna_next_stg_req = {
    Code = 612,
    S = "activity_dna_req"
  },
  crusade_enter_req = {Code = 613, S = "null"},
  crusade_enter_ack = {
    Code = 614,
    S = "crusade_enter_ack"
  },
  crusade_fight_req = {
    Code = 615,
    S = "crusade_fight_req"
  },
  crusade_fight_ack = {
    Code = 616,
    S = "crusade_fight_ack"
  },
  crusade_cross_req = {
    Code = 617,
    S = "crusade_cross_req"
  },
  crusade_cross_ack = {
    Code = 618,
    S = "crusade_cross_ack"
  },
  crusade_repair_req = {
    Code = 619,
    S = "crusade_repair_req"
  },
  crusade_reward_req = {
    Code = 620,
    S = "crusade_reward_req"
  },
  crusade_reward_ack = {
    Code = 621,
    S = "crusade_reward_ack"
  },
  crusade_buy_cross_req = {Code = 622, S = "null"},
  crusade_buy_cross_ack = {
    Code = 623,
    S = "crusade_buy_cross_ack"
  },
  crusade_first_req = {Code = 624, S = "null"},
  crusade_first_ack = {
    Code = 625,
    S = "crusade_first_ack"
  },
  akt_dna_get_award_req = {
    Code = 626,
    S = "activity_dna_req"
  },
  dna_gacha_ntf = {
    Code = 627,
    S = "open_box_recent_ntf"
  },
  contention_income_req = {Code = 628, S = "null"},
  contention_income_ack = {
    Code = 629,
    S = "contention_income_ack"
  },
  contention_missiles_ntf = {
    Code = 630,
    S = "contention_missiles_ntf"
  },
  budo_req = {Code = 631, S = "null"},
  budo_test_req = {
    Code = 632,
    S = "activity_dna_req"
  },
  budo_stage_ntf = {
    Code = 633,
    S = "budo_stage_ntf"
  },
  budo_sign_up_ntf = {
    Code = 634,
    S = "budo_sign_up_ntf"
  },
  budo_join_req = {Code = 635, S = "null"},
  budo_join_ack = {
    Code = 636,
    S = "budo_join_ack"
  },
  budo_mass_election_ntf = {
    Code = 637,
    S = "budo_mass_election_ntf"
  },
  budo_change_formation_req = {
    Code = 638,
    S = "budo_change_formation_req"
  },
  budo_change_formation_ack = {
    Code = 639,
    S = "budo_join_ack"
  },
  budo_promotion_req = {
    Code = 640,
    S = "budo_promotion_req"
  },
  budo_promotion_ntf = {
    Code = 642,
    S = "budo_promotion_ntf"
  },
  support_one_player_req = {
    Code = 643,
    S = "support_one_player_req"
  },
  battle_report_req = {
    Code = 644,
    S = "battle_report_req"
  },
  battle_report_ack = {
    Code = 645,
    S = "battle_report_ack"
  },
  get_my_support_req = {Code = 646, S = "null"},
  get_my_support_ack = {
    Code = 647,
    S = "get_my_support_ack"
  },
  get_budo_rank_req = {
    Code = 648,
    S = "get_budo_rank_req"
  },
  get_budo_rank_ack = {
    Code = 649,
    S = "get_budo_rank_ack"
  },
  get_budo_replay_req = {
    Code = 650,
    S = "get_budo_replay_req"
  },
  get_budo_replay_ack = {
    Code = 651,
    S = "get_budo_replay_ack"
  },
  get_promotion_reward_req = {
    Code = 652,
    S = "get_promotion_reward_req"
  },
  special_efficacy_ntf = {
    Code = 654,
    S = "special_efficacy_ntf"
  },
  activity_rank_req = {
    Code = 655,
    S = "activity_rank_req"
  },
  activity_rank_ack = {
    Code = 656,
    S = "activity_box_rank"
  },
  support_one_player_ack = {
    Code = 657,
    S = "support_one_player_ack"
  },
  all_gd_fleets_req = {Code = 658, S = "null"},
  all_gd_fleets_ack = {
    Code = 659,
    S = "all_gd_fleets_ack"
  },
  fleet_fight_report_req = {
    Code = 660,
    S = "fleet_fight_report_req"
  },
  fleet_fight_report_ack = {
    Code = 661,
    S = "fleet_fight_report_ack"
  },
  stores_refresh_req = {Code = 662, S = "null"},
  stores_refresh_ack = {
    Code = 663,
    S = "stores_refresh_ack"
  },
  laba_times_ntf = {
    Code = 664,
    S = "laba_times_ntf"
  },
  change_name_ntf = {Code = 665, S = "user_info"},
  pay_list_carrier_req = {
    Code = 666,
    S = "pay_list_carrier_req"
  },
  pay_list_carrier_ack = {
    Code = 667,
    S = "pay_list_carrier_ack"
  },
  laba_type_ntf = {
    Code = 668,
    S = "laba_type_ntf"
  },
  mail_to_all_req = {
    Code = 669,
    S = "mail_to_all_req"
  },
  champion_matrix_req = {
    Code = 670,
    S = "champion_matrix_req"
  },
  champion_matrix_ack = {
    Code = 671,
    S = "champion_matrix_ack"
  },
  add_adjutant_req = {
    Code = 672,
    S = "add_adjutant_req"
  },
  release_adjutant_req = {
    Code = 673,
    S = "release_adjutant_req"
  },
  month_card_list_req = {
    Code = 674,
    S = "month_card_list_req"
  },
  month_card_list_ack = {
    Code = 675,
    S = "month_card_list_ack"
  },
  month_card_buy_req = {
    Code = 676,
    S = "month_card_buy_req"
  },
  month_card_use_req = {
    Code = 677,
    S = "month_card_use_req"
  },
  chat_channel_switch_ntf = {
    Code = 678,
    S = "chat_channel_switch_ntf"
  },
  fleet_dismiss_req = {Code = 679, S = "null"},
  fleet_dismiss_ack = {
    Code = 680,
    S = "fleets_info"
  },
  adjutant_max_ntf = {
    Code = 681,
    S = "adjutant_max_ntf"
  },
  max_level_ntf = {
    Code = 682,
    S = "max_level_ntf"
  },
  game_items_trans_req = {
    Code = 683,
    S = "game_items_trans_req"
  },
  game_items_trans_ack = {
    Code = 684,
    S = "game_items_trans_ack"
  },
  budo_champion_req = {
    Code = 685,
    S = "budo_champion_req"
  },
  budo_champion_ack = {
    Code = 686,
    S = "budo_champion_ack"
  },
  budo_champion_report_req = {
    Code = 687,
    S = "budo_champion_report_req"
  },
  budo_champion_report_ack = {
    Code = 688,
    S = "get_budo_replay_ack"
  },
  client_version_ntf = {
    Code = 689,
    S = "client_version_ntf"
  },
  apply_limit_update_req = {
    Code = 690,
    S = "apply_limit_update_req"
  },
  version_code_ntf = {
    Code = 691,
    S = "version_code_ntf"
  },
  version_code_update_req = {
    Code = 692,
    S = "version_code_update_req"
  },
  remodel_info_req = {Code = 693, S = "null"},
  remodel_info_ack = {
    Code = 694,
    S = "remodel_info_ack"
  },
  remodel_info_ntf = {
    Code = 695,
    S = "remodel_info_ntf"
  },
  remodel_levelup_req = {
    Code = 696,
    S = "remodel_levelup_req"
  },
  remodel_help_req = {Code = 697, S = "null"},
  remodel_help_others_req = {
    Code = 698,
    S = "remodel_help_others_req"
  },
  remodel_speed_req = {
    Code = 699,
    S = "remodel_speed_req"
  },
  remodel_help_info_req = {Code = 700, S = "null"},
  remodel_help_info_ack = {
    Code = 701,
    S = "remodel_help_info_ack"
  },
  remodel_help_ntf = {
    Code = 702,
    S = "remodel_help_ntf"
  },
  remodel_push_ntf = {
    Code = 703,
    S = "remodel_push_ntf"
  },
  give_friends_gift_req = {
    Code = 704,
    S = "give_friends_gift_req"
  },
  give_share_awards_req = {
    Code = 705,
    S = "give_share_awards_req"
  },
  remodel_event_ntf = {
    Code = 706,
    S = "remodel_event_ntf"
  },
  give_friends_gift_ack = {
    Code = 707,
    S = "give_friends_gift_ack"
  },
  update_passport_req = {
    Code = 708,
    S = "update_passport_req"
  },
  financial_req = {Code = 709, S = "null"},
  financial_ack = {
    Code = 710,
    S = "financial_info"
  },
  financial_upgrade_req = {Code = 711, S = "null"},
  facebook_friends_req = {
    Code = 712,
    S = "facebook_friends_req"
  },
  facebook_friends_ack = {
    Code = 713,
    S = "facebook_friends_ack"
  },
  financial_gifts_get_req = {
    Code = 714,
    S = "financial_gifts_get_req"
  },
  financial_amount_ntf = {
    Code = 715,
    S = "financial_amount_ntf"
  },
  multi_acc_req = {Code = 716, S = "null"},
  multi_acc_ack = {
    Code = 717,
    S = "multi_acc_ack"
  },
  multi_acc_loot_req = {Code = 718, S = "int_param"},
  facebook_friends_ntf = {
    Code = 719,
    S = "facebook_friends_ntf"
  },
  financial_ntf = {
    Code = 720,
    S = "financial_info"
  },
  push_button_req = {
    Code = 721,
    S = "push_button_req"
  },
  push_button_info_req = {Code = 722, S = "null"},
  push_button_info_ack = {
    Code = 723,
    S = "push_button_info_ack"
  },
  push_button_ntf = {
    Code = 724,
    S = "push_button_ntf"
  },
  treasure_info_ntf = {
    Code = 725,
    S = "treasure_info"
  },
  invite_friends_req = {
    Code = 726,
    S = "invite_friends_req"
  },
  get_energy_req = {
    Code = 727,
    S = "get_energy_req"
  },
  open_treasure_box_req = {Code = 728, S = "null"},
  open_treasure_box_ack = {
    Code = 729,
    S = "open_treasure_box_ack"
  },
  treasure_info_req = {Code = 730, S = "null"},
  friend_role_req = {
    Code = 731,
    S = "friend_role_req"
  },
  friend_role_ack = {
    Code = 732,
    S = "user_brief_info"
  },
  send_global_email_req = {
    Code = 733,
    S = "send_global_email_req"
  },
  collect_energy_req = {
    Code = 734,
    S = "collect_energy_req"
  },
  krypton_refine_req = {
    Code = 735,
    S = "krypton_refine_req"
  },
  krypton_refine_ack = {
    Code = 736,
    S = "krypton_refine_ack"
  },
  krypton_inject_req = {
    Code = 737,
    S = "krypton_inject_req"
  },
  krypton_inject_ack = {
    Code = 738,
    S = "krypton_inject_ack"
  },
  krypton_refine_info_req = {Code = 739, S = "null"},
  krypton_refine_info_ack = {
    Code = 740,
    S = "krypton_refine_info_ack"
  },
  krypton_inject_buy_req = {Code = 741, S = "null"},
  krypton_inject_buy_ack = {
    Code = 742,
    S = "krypton_inject_buy_ack"
  },
  krypton_inject_list_req = {
    Code = 743,
    S = "krypton_inject_list_req"
  },
  krypton_inject_list_ack = {
    Code = 744,
    S = "krypton_inject_list_ack"
  },
  krypton_refine_list_req = {
    Code = 745,
    S = "krypton_refine_list_req"
  },
  krypton_refine_list_ack = {
    Code = 746,
    S = "krypton_refine_list_ack"
  },
  user_info_req = {Code = 747, S = "null"},
  user_info_ack = {
    Code = 748,
    S = "user_info_ack"
  },
  prime_map_req = {Code = 800, S = "null"},
  prime_map_ack = {
    Code = 801,
    S = "prime_map_ack"
  },
  prime_active_req = {Code = 802, S = "null"},
  prime_active_ntf = {
    Code = 803,
    S = "prime_active_ntf"
  },
  prime_jump_req = {
    Code = 804,
    S = "prime_jump_req"
  },
  prime_quit_req = {Code = 805, S = "null"},
  prime_my_info_ntf = {
    Code = 806,
    S = "prime_my_info"
  },
  prime_dot_occupy_ntf = {
    Code = 807,
    S = "prime_dot_occupy_ntf"
  },
  prime_dot_status_ntf = {
    Code = 808,
    S = "prime_dot_status_ntf"
  },
  prime_march_ntf = {
    Code = 809,
    S = "prime_march_ntf"
  },
  prime_march_req = {
    Code = 810,
    S = "prime_march_req"
  },
  prime_rank_ntf = {
    Code = 811,
    S = "prime_rank_ntf"
  },
  prime_fight_history_req = {
    Code = 812,
    S = "prime_fight_history_req"
  },
  prime_fight_history_ack = {
    Code = 813,
    S = "prime_fight_history_ack"
  },
  prime_increase_power_req = {Code = 814, S = "null"},
  prime_site_info_req = {
    Code = 815,
    S = "prime_site_info_req"
  },
  prime_site_info_ack = {
    Code = 816,
    S = "prime_site_info_ack"
  },
  revive_self_req = {Code = 817, S = "null"},
  prime_increase_power_info_req = {Code = 818, S = "null"},
  prime_increase_power_info_ack = {
    Code = 819,
    S = "prime_increase_power_info_ack"
  },
  prime_enemy_info_req = {Code = 820, S = "null"},
  prime_enemy_info_ack = {
    Code = 821,
    S = "prime_enemy_info_ack"
  },
  prime_fight_report_req = {
    Code = 822,
    S = "prime_fight_report_req"
  },
  prime_last_report_req = {
    Code = 823,
    S = "prime_last_report_req"
  },
  prime_dot_occupy_no_ntf = {
    Code = 824,
    S = "prime_dot_occupy_no_ntf"
  },
  prime_award_req = {
    Code = 825,
    S = "prime_award_req"
  },
  prime_award_ack = {
    Code = 826,
    S = "prime_award_lists"
  },
  prime_round_result_ntf = {
    Code = 827,
    S = "prime_round_result_ntf"
  },
  krypton_sort_req = {
    Code = 828,
    S = "krypton_sort_req"
  },
  krypton_sort_ack = {
    Code = 829,
    S = "krypton_store_ack"
  },
  prestige_info_ntf = {
    Code = 830,
    S = "prestige_info_ntf"
  },
  levelup_unlock_ntf = {
    Code = 831,
    S = "levelup_unlock_ntf"
  },
  champion_promote_ntf = {
    Code = 832,
    S = "champion_promote_ntf"
  },
  champion_straight_req = {Code = 833, S = "null"},
  champion_straight_ack = {
    Code = 834,
    S = "champion_straight_ack"
  },
  champion_straight_award_req = {
    Code = 835,
    S = "champion_straight_award_req"
  },
  vip_sign_item_use_award_ntf = {
    Code = 836,
    S = "vip_sign_item_use_award_ntf"
  },
  pay_sign_req = {Code = 837, S = "null"},
  pay_sign_ack = {
    Code = 838,
    S = "pay_sign_ack"
  },
  pay_sign_reward_req = {
    Code = 839,
    S = "pay_sign_reward_req"
  },
  equip_push_ntf = {
    Code = 840,
    S = "equip_push_ntf"
  },
  equip_pushed_req = {Code = 841, S = "null"},
  open_server_fund_req = {
    Code = 842,
    S = "open_server_fund_req"
  },
  open_server_fund_ack = {
    Code = 843,
    S = "open_server_fund_ack"
  },
  charge_open_server_fund_req = {Code = 844, S = "null"},
  open_server_fund_award_req = {
    Code = 845,
    S = "open_server_fund_award_req"
  },
  all_welfare_req = {
    Code = 846,
    S = "all_welfare_req"
  },
  all_welfare_ack = {
    Code = 847,
    S = "all_welfare_ack"
  },
  all_welfare_award_req = {
    Code = 848,
    S = "all_welfare_award_req"
  },
  vip_level_ntf = {
    Code = 849,
    S = "vip_level_ntf"
  },
  temp_vip_ntf = {
    Code = 850,
    S = "temp_vip_ntf"
  },
  pay_push_pop_ntf = {
    Code = 851,
    S = "pay_push_pop_ntf"
  },
  pay_push_pop_set_req = {Code = 852, S = "null"},
  prime_do_wve_boss_left_ntf = {
    Code = 853,
    S = "prime_do_wve_boss_left_ntf"
  },
  prime_time_wve_boss_left_ntf = {
    Code = 854,
    S = "prime_time_wve_boss_left_ntf"
  },
  prime_round_fight_ntf = {
    Code = 856,
    S = "prime_round_fight_ntf"
  },
  sell_item_ntf = {
    Code = 857,
    S = "sell_item_ntf"
  },
  url_push_ntf = {
    Code = 858,
    S = "url_push_ntf"
  },
  enhance_info_req = {
    Code = 859,
    S = "enhance_info_req"
  },
  enhance_info_ack = {
    Code = 860,
    S = "enhance_info_ack"
  },
  combo_guide_req = {
    Code = 861,
    S = "combo_guide_req"
  },
  combo_guide_ack = {
    Code = 862,
    S = "combo_guide_ack"
  },
  player_pay_price_req = {
    Code = 863,
    S = "player_pay_price_req"
  },
  pay_status_ntf = {
    Code = 864,
    S = "pay_status_ntf"
  },
  daily_benefits_req = {Code = 865, S = "null"},
  daily_benefits_ack = {
    Code = 866,
    S = "daily_benefits_ack"
  },
  daily_benefits_award_req = {
    Code = 867,
    S = "daily_benefits_award_req"
  },
  enter_seven_day_req = {Code = 868, S = "null"},
  seven_day_ntf = {
    Code = 869,
    S = "seven_day_ntf"
  },
  goal_info_req = {Code = 870, S = "null"},
  goal_info_ack = {
    Code = 871,
    S = "goal_info_ack"
  },
  get_goal_award_req = {
    Code = 872,
    S = "get_goal_award_req"
  },
  cutoff_info_req = {Code = 873, S = "null"},
  cutoff_info_ack = {
    Code = 874,
    S = "cutoff_info_ack"
  },
  buy_cutoff_item_req = {
    Code = 875,
    S = "buy_cutoff_item_req"
  },
  get_goal_award_ack = {
    Code = 876,
    S = "get_goal_award_ack"
  },
  enter_seven_day_ack = {
    Code = 879,
    S = "enter_seven_day_ack"
  },
  prime_jump_price_req = {
    Code = 880,
    S = "prime_jump_req"
  },
  prime_jump_price_ack = {
    Code = 881,
    S = "prime_jump_price_ack"
  },
  select_goal_type_req = {
    Code = 882,
    S = "select_goal_type_req"
  },
  gp_test_pay_group_req = {
    Code = 883,
    S = "gp_test_pay_group_req"
  },
  store_quick_buy_count_req = {
    Code = 884,
    S = "store_quick_buy_count_req"
  },
  store_quick_buy_count_ack = {
    Code = 885,
    S = "store_quick_buy_count_ack"
  },
  store_quick_buy_price_req = {
    Code = 886,
    S = "store_quick_buy_price_req"
  },
  store_quick_buy_price_ack = {
    Code = 887,
    S = "store_quick_buy_price_ack"
  },
  activity_store_count_req = {
    Code = 888,
    S = "activity_store_count_req"
  },
  activity_store_count_ack = {
    Code = 889,
    S = "activity_store_count_ack"
  },
  activity_store_price_req = {
    Code = 890,
    S = "activity_store_price_req"
  },
  activity_store_price_ack = {
    Code = 891,
    S = "activity_store_price_ack"
  },
  daily_benefits_ntf = {
    Code = 892,
    S = "daily_benefits_ntf"
  },
  client_login_ok_req = {Code = 893, S = "null"},
  prime_push_show_round_req = {
    Code = 894,
    S = "prime_push_show_round_req"
  },
  refresh_type_req = {Code = 895, S = "null"},
  refresh_type_ack = {
    Code = 896,
    S = "refresh_type_ack"
  },
  force_add_fleet_req = {
    Code = 897,
    S = "force_add_fleet_req"
  },
  max_step_req = {
    Code = 898,
    S = "max_step_req"
  },
  max_step_ack = {
    Code = 899,
    S = "max_step_ack"
  },
  ladder_report_req = {
    Code = 900,
    S = "ladder_report_req"
  },
  ladder_report_ack = {
    Code = 901,
    S = "ladder_report_ack"
  },
  ladder_jump_req = {
    Code = 902,
    S = "ladder_jump_req"
  },
  ladder_jump_ack = {
    Code = 903,
    S = "ladder_jump_ack"
  },
  ladder_award_req = {
    Code = 904,
    S = "ladder_award_req"
  },
  ladder_award_ntf = {
    Code = 905,
    S = "ladder_award_ntf"
  },
  ladder_report_detail_req = {
    Code = 906,
    S = "ladder_report_detail_req"
  },
  ladder_report_detail_ack = {
    Code = 907,
    S = "ladder_report_detail_ack"
  },
  stores_buy_ack = {
    Code = 908,
    S = "stores_buy_ack"
  },
  enter_tactical_req = {Code = 920, S = "null"},
  enter_tactical_ack = {
    Code = 921,
    S = "tactical_info"
  },
  tactical_equip_on_req = {
    Code = 922,
    S = "tactical_equip_on_req"
  },
  tactical_equip_on_ack = {
    Code = 923,
    S = "tactical_equip_on_ack"
  },
  tactical_equip_off_req = {
    Code = 924,
    S = "tactical_equip_off_req"
  },
  tactical_equip_off_ack = {
    Code = 925,
    S = "tactical_equip_off_ack"
  },
  tactical_resource_ntf = {
    Code = 926,
    S = "tactical_resource_ntf"
  },
  tactical_refine_price_req = {
    Code = 927,
    S = "tactical_refine_info"
  },
  tactical_refine_price_ack = {
    Code = 928,
    S = "tactical_refine_price_ack"
  },
  tactical_refine_req = {
    Code = 929,
    S = "tactical_refine_info"
  },
  tactical_refine_ack = {
    Code = 930,
    S = "tactical_refine_ack"
  },
  tactical_refine_save_req = {
    Code = 931,
    S = "tactical_refine_save_req"
  },
  tactical_unlock_slot_req = {Code = 932, S = "null"},
  tactical_unlock_slot_ack = {
    Code = 933,
    S = "tactical_unlock_slot_ack"
  },
  tactical_enter_refine_req = {
    Code = 934,
    S = "tactical_enter_refine_req"
  },
  tactical_enter_refine_ack = {
    Code = 935,
    S = "tactical_enter_refine_ack"
  },
  tactical_status_ntf = {
    Code = 936,
    S = "tactical_status_ntf"
  },
  tactical_equip_delete_req = {
    Code = 937,
    S = "tactical_equip_delete_req"
  },
  tactical_equip_delete_ack = {
    Code = 938,
    S = "tactical_equip_delete_ack"
  },
  pay_limit_ntf = {
    Code = 939,
    S = "pay_limit_ntf"
  },
  pay_sign_board_ntf = {
    Code = 940,
    S = "pay_sign_board_ntf"
  },
  tactical_guide_req = {Code = 941, S = "null"},
  enter_plante_req = {
    Code = 942,
    S = "enter_plante_req"
  },
  enter_plante_ack = {
    Code = 943,
    S = "enter_plante_ack"
  },
  plante_explore_req = {
    Code = 944,
    S = "plante_explore_req"
  },
  plante_explore_ack = {
    Code = 945,
    S = "plante_explore_ack"
  },
  plante_refresh_req = {
    Code = 946,
    S = "plante_refresh_req"
  },
  plante_refresh_ack = {
    Code = 947,
    S = "plante_refresh_ack"
  },
  show_price_ntf = {
    Code = 948,
    S = "show_price_ntf"
  },
  get_invite_gift_req = {
    Code = 949,
    S = "get_invite_gift_req"
  },
  get_invite_gift_ack = {
    Code = 950,
    S = "get_invite_gift_ack"
  },
  facebook_switch_ntf = {
    Code = 951,
    S = "facebook_switch_ntf"
  },
  can_enhance_req = {Code = 952, S = "null"},
  can_enhance_ack = {
    Code = 953,
    S = "can_enhance_ack"
  },
  tactical_delete_awards_req = {
    Code = 954,
    S = "tactical_delete_awards_req"
  },
  tactical_delete_awards_ack = {
    Code = 955,
    S = "tactical_delete_awards_ack"
  },
  first_login_ntf = {
    Code = 956,
    S = "first_login_ntf"
  },
  alliance_guid_ntf = {
    Code = 957,
    S = "alliance_guid_ntf"
  },
  remodel_help_others_ack = {
    Code = 958,
    S = "remodel_help_others_ack"
  },
  ltv_ajust_ntf = {
    Code = 959,
    S = "ltv_ajust_ntf"
  },
  story_status_ntf = {
    Code = 970,
    S = "story_status_ntf"
  },
  story_award_req = {
    Code = 971,
    S = "story_award_req"
  },
  story_award_ack = {
    Code = 972,
    S = "story_award_ack"
  },
  thumb_up_info_req = {Code = 973, S = "null"},
  thumb_up_info_ack = {
    Code = 974,
    S = "thumb_up_info_ack"
  },
  thumb_up_req = {
    Code = 975,
    S = "thumb_up_req"
  },
  thumb_up_ack = {
    Code = 976,
    S = "thumb_up_ack"
  },
  credit_exchange_ack = {
    Code = 977,
    S = "credit_exchange_ack"
  },
  today_act_task_req = {Code = 978, S = "null"},
  today_act_task_ack = {
    Code = 979,
    S = "today_act_task_ack"
  },
  enter_monthly_req = {Code = 980, S = "null"},
  enter_monthly_ack = {
    Code = 981,
    S = "enter_monthly_ack"
  },
  view_each_task_req = {
    Code = 982,
    S = "view_each_task_req"
  },
  view_each_task_ack = {
    Code = 983,
    S = "view_each_task_ack"
  },
  view_award_req = {Code = 984, S = "null"},
  view_award_ack = {
    Code = 985,
    S = "view_award_ack"
  },
  redo_task_req = {
    Code = 986,
    S = "redo_task_req"
  },
  redo_task_ack = {
    Code = 987,
    S = "redo_task_ack"
  },
  get_award_req = {
    Code = 988,
    S = "get_award_req"
  },
  get_award_ack = {
    Code = 989,
    S = "get_award_ack"
  },
  get_today_award_req = {
    Code = 990,
    S = "get_today_award_req"
  },
  get_today_award_ack = {
    Code = 991,
    S = "get_today_award_ack"
  },
  other_monthly_req = {
    Code = 992,
    S = "other_monthly_req"
  },
  other_monthly_ack = {
    Code = 993,
    S = "other_monthly_ack"
  },
  today_task_ntf = {
    Code = 994,
    S = "task_statistic_info"
  },
  switch_formation_req = {
    Code = 995,
    S = "switch_formation_req"
  },
  switch_formation_ack = {
    Code = 996,
    S = "switch_formation_ack"
  },
  mulmatrix_switch_req = {
    Code = 997,
    S = "mulmatrix_switch_req"
  },
  mulmatrix_switch_ack = {
    Code = 998,
    S = "mulmatrix_switch_ack"
  },
  mulmatrix_equip_info_req = {
    Code = 999,
    S = "mulmatrix_equip_info_req"
  },
  mulmatrix_equip_info_ack = {
    Code = 1000,
    S = "mulmatrix_equip_info_ack"
  },
  change_matrix_type_req = {
    Code = 1001,
    S = "change_matrix_type_req"
  },
  change_matrix_type_ack = {
    Code = 1002,
    S = "change_matrix_type_ack"
  },
  user_bag_info_ntf = {
    Code = 1003,
    S = "user_bag_info_ntf"
  },
  client_req_stat_req = {
    Code = 1004,
    S = "client_req_stat_info"
  },
  bugly_data_ntf = {
    Code = 1005,
    S = "bugly_data_ntf"
  },
  enter_champion_req = {Code = 1101, S = "null"},
  enter_champion_ack = {
    Code = 1102,
    S = "enter_champion_ack"
  },
  wdc_enter_req = {Code = 1103, S = "null"},
  wdc_enter_ack = {
    Code = 1104,
    S = "wdc_enter_ack"
  },
  wdc_match_req = {Code = 1105, S = "null"},
  wdc_match_ack = {
    Code = 1106,
    S = "wdc_match_ack"
  },
  wdc_fight_req = {
    Code = 1107,
    S = "wdc_fight_req"
  },
  wdc_fight_ack = {
    Code = 1108,
    S = "wdc_fight_ack"
  },
  wdc_report_req = {Code = 1109, S = "null"},
  wdc_report_ack = {
    Code = 1110,
    S = "wdc_report_ack"
  },
  wdc_report_detail_req = {
    Code = 1111,
    S = "wdc_report_detail_req"
  },
  wdc_report_detail_ack = {
    Code = 1112,
    S = "wdc_report_detail_ack"
  },
  wdc_info_ntf = {
    Code = 1113,
    S = "wdc_info_ntf"
  },
  wdc_awards_req = {Code = 1114, S = "null"},
  wdc_awards_ack = {
    Code = 1115,
    S = "wdc_awards_ack"
  },
  wdc_rank_board_req = {Code = 1116, S = "null"},
  wdc_rank_board_ack = {
    Code = 1117,
    S = "wdc_rank_board_ack"
  },
  wdc_honor_wall_req = {Code = 1118, S = "null"},
  wdc_honor_wall_ack = {
    Code = 1119,
    S = "wdc_honor_wall_ack"
  },
  wdc_enemy_release_req = {Code = 1120, S = "null"},
  wdc_awards_ntf = {
    Code = 1121,
    S = "wdc_awards_ntf"
  },
  wdc_awards_get_req = {
    Code = 1122,
    S = "wdc_awards_get_req"
  },
  wdc_awards_get_ack = {
    Code = 1123,
    S = "wdc_awards_get_ack"
  },
  wdc_rankup_ntf = {
    Code = 1124,
    S = "wdc_rankup_ntf"
  },
  wdc_fight_status_req = {Code = 1125, S = "null"},
  wdc_fight_status_ack = {
    Code = 1126,
    S = "wdc_fight_status_ack"
  },
  wdc_status_ntf = {
    Code = 1127,
    S = "wdc_status_ntf"
  },
  translate_info_req = {Code = 1128, S = "null"},
  translate_info_ack = {
    Code = 1129,
    S = "translate_info_ack"
  },
  welcome_screen_req = {Code = 1130, S = "null"},
  welcome_screen_ack = {
    Code = 1131,
    S = "welcome_screen_ack"
  },
  welcome_mask_req = {
    Code = 1132,
    S = "welcome_mask_req"
  },
  first_charge_ntf = {
    Code = 1133,
    S = "first_charge_ntf"
  },
  enter_dice_req = {
    Code = 1134,
    S = "enter_dice_req"
  },
  enter_dice_ack = {
    Code = 1135,
    S = "dice_detail"
  },
  throw_dice_req = {
    Code = 1136,
    S = "throw_dice_req"
  },
  throw_dice_ack = {
    Code = 1137,
    S = "throw_dice_ack"
  },
  dice_awards_req = {
    Code = 1138,
    S = "dice_awards_req"
  },
  reset_dice_req = {
    Code = 1139,
    S = "reset_dice_req"
  },
  reset_dice_ack = {
    Code = 1140,
    S = "dice_detail"
  },
  speed_resource_req = {Code = 1141, S = "null"},
  speed_resource_ack = {
    Code = 1142,
    S = "speed_resource_ack"
  },
  finish_speed_task_req = {
    Code = 1143,
    S = "finish_speed_task_req"
  },
  dice_board_ntf = {
    Code = 1144,
    S = "dice_board_ntf"
  },
  wdc_rank_board_ntf = {
    Code = 1145,
    S = "wdc_rank_board_ntf"
  },
  speed_resource_ntf = {
    Code = 1146,
    S = "speed_resource_ack"
  },
  event_activity_ntf = {
    Code = 1147,
    S = "event_activity_ntf"
  },
  event_activity_req = {
    Code = 1148,
    S = "event_activity_req"
  },
  event_activity_ack = {
    Code = 1149,
    S = "event_activity_info"
  },
  event_activity_award_req = {
    Code = 1150,
    S = "event_activity_award_req"
  },
  user_change_req = {
    Code = 1151,
    S = "user_change_info"
  },
  user_change_ack = {Code = 1152, S = "user_info"},
  event_progress_award_req = {
    Code = 1153,
    S = "event_progress_award_req"
  },
  star_craft_data_ntf = {
    Code = 1154,
    S = "star_craft_data_ntf"
  },
  star_craft_sign_up_req = {
    Code = 1155,
    S = "star_craft_sign_up_req"
  },
  event_ntf_req = {Code = 1156, S = "null"},
  adventure_add_req = {Code = 1157, S = "adv_battle"},
  adventure_add_ack = {
    Code = 1158,
    S = "adventure_add_ack"
  },
  adventure_add_price_req = {Code = 1159, S = "adv_battle"},
  recruit_list_new_req = {
    Code = 1160,
    S = "recruit_list_new_req"
  },
  recruit_list_new_ack = {
    Code = 1161,
    S = "recruit_list_new_ack"
  },
  recruit_req = {
    Code = 1162,
    S = "recruit_req"
  },
  recruit_ack = {
    Code = 1163,
    S = "recruit_ack"
  },
  recruit_look_req = {
    Code = 1164,
    S = "recruit_look_req"
  },
  recruit_look_ack = {
    Code = 1165,
    S = "recruit_look_ack"
  },
  master_req = {Code = 1166, S = "master_req"},
  master_ack = {
    Code = 1167,
    S = "master_all_ack"
  },
  hero_union_req = {Code = 1168, S = "null"},
  hero_union_ack = {
    Code = 1169,
    S = "hero_union_ack"
  },
  hero_collect_req = {Code = 1170, S = "null"},
  hero_collect_ack = {
    Code = 1171,
    S = "hero_collect_ack"
  },
  hero_collect_rank_req = {
    Code = 1172,
    S = "hero_collect_rank_req"
  },
  hero_collect_rank_ack = {
    Code = 1173,
    S = "hero_collect_rank_ack"
  },
  red_point_ntf = {
    Code = 1174,
    S = "red_point_ntf"
  },
  area_id_req = {
    Code = 1175,
    S = "area_id_req"
  },
  area_id_ack = {
    Code = 1176,
    S = "area_id_ack"
  },
  master_ntf = {Code = 1177, S = "master_ntf"},
  mining_world_req = {
    Code = 1178,
    S = "mining_world_req"
  },
  mining_world_ack = {
    Code = 1179,
    S = "mining_world_ack"
  },
  mining_world_log_req = {Code = 1180, S = "null"},
  mining_world_log_ack = {
    Code = 1181,
    S = "mining_world_log_ack"
  },
  mining_world_pe_req = {Code = 1182, S = "null"},
  mining_world_pe_ack = {
    Code = 1183,
    S = "mining_world_pe_ack"
  },
  mining_wars_req = {
    Code = 1184,
    S = "mining_wars_req"
  },
  mining_wars_ack = {
    Code = 1185,
    S = "mining_wars_ack"
  },
  mining_wars_buy_count_req = {
    Code = 1186,
    S = "mining_wars_buy_count_req"
  },
  mining_wars_buy_count_ack = {
    Code = 1187,
    S = "mining_wars_buy_count_ack"
  },
  mining_wars_refresh_req = {
    Code = 1188,
    S = "mining_wars_refresh_req"
  },
  mining_wars_star_req = {
    Code = 1189,
    S = "mining_wars_star_req"
  },
  mining_wars_star_ack = {
    Code = 1190,
    S = "mining_wars_star_data"
  },
  mining_wars_battle_req = {
    Code = 1191,
    S = "mining_wars_battle_req"
  },
  mining_wars_battle_ack = {
    Code = 1192,
    S = "mining_wars_battle_ack"
  },
  mining_wars_collect_req = {
    Code = 1193,
    S = "mining_wars_collect_req"
  },
  mining_wars_collect_ack = {
    Code = 1194,
    S = "mining_wars_collect_ack"
  },
  mining_wars_change_ntf = {
    Code = 1195,
    S = "mining_wars_change_ntf"
  },
  mining_world_battle_req = {
    Code = 1196,
    S = "mining_world_battle_req"
  },
  mining_world_battle_ack = {
    Code = 1197,
    S = "mining_world_battle_ack"
  },
  mining_battle_report_req = {
    Code = 1198,
    S = "mining_battle_report_req"
  },
  mining_battle_report_ack = {
    Code = 1199,
    S = "mining_battle_report_ack"
  },
  mining_reward_ntf = {
    Code = 1200,
    S = "mining_reward_ntf"
  },
  version_info_req = {
    Code = 1201,
    S = "version_info_req"
  },
  art_main_req = {
    Code = 1202,
    S = "art_main_req"
  },
  art_main_ack = {
    Code = 1203,
    S = "art_main_ack"
  },
  art_point_req = {
    Code = 1204,
    S = "art_point_req"
  },
  art_point_ack = {
    Code = 1205,
    S = "art_point_ack"
  },
  art_point_upgrade_req = {
    Code = 1206,
    S = "art_point_upgrade_req"
  },
  art_point_upgrade_ack = {
    Code = 1207,
    S = "art_point_upgrade_ack"
  },
  art_core_list_req = {
    Code = 1208,
    S = "art_core_list_req"
  },
  art_core_list_ack = {
    Code = 1209,
    S = "art_core_list_ack"
  },
  art_core_down_req = {
    Code = 1210,
    S = "art_core_down_req"
  },
  art_core_down_ack = {
    Code = 1211,
    S = "art_core_down_ack"
  },
  art_core_equip_req = {
    Code = 1212,
    S = "art_core_equip_req"
  },
  art_core_equip_ack = {
    Code = 1213,
    S = "art_core_equip_ack"
  },
  art_reset_req = {
    Code = 1214,
    S = "art_reset_req"
  },
  art_reset_ack = {
    Code = 1215,
    S = "art_reset_ack"
  },
  art_core_decompose_req = {
    Code = 1216,
    S = "art_core_decompose_req"
  },
  art_core_decompose_ack = {
    Code = 1217,
    S = "art_core_decompose_ack"
  },
  art_can_upgrade_req = {
    Code = 1218,
    S = "art_can_upgrade_req"
  },
  art_can_upgrade_ack = {
    Code = 1219,
    S = "art_can_upgrade_ack"
  },
  main_city_ntf = {
    Code = 1251,
    S = "main_city_ntf"
  },
  mask_modules_req = {
    Code = 1252,
    S = "mask_modules_req"
  },
  mask_show_index_req = {
    Code = 1253,
    S = "mask_show_index_req"
  },
  space_door_req = {Code = 1254, S = "null"},
  space_door_ntf = {
    Code = 1255,
    S = "space_door_ntf"
  },
  helper_buttons_req = {Code = 1256, S = "null"},
  helper_buttons_ack = {
    Code = 1257,
    S = "helper_buttons_ack"
  },
  event_log_ntf = {
    Code = 1258,
    S = "event_log_ntf"
  },
  choosable_item_req = {
    Code = 1259,
    S = "choosable_item_req"
  },
  choosable_item_ack = {
    Code = 1260,
    S = "choosable_item_ack"
  },
  choosable_item_use_req = {
    Code = 1261,
    S = "choosable_item_use_req"
  },
  fte_req = {Code = 1263, S = "fte_req"},
  fte_ack = {Code = 1264, S = "fte_ack"},
  fte_ntf = {Code = 1265, S = "fte_ntf"},
  achievement_reward_receive_req = {
    Code = 1266,
    S = "achievement_reward_receive_req"
  },
  achievement_reward_receive_ack = {
    Code = 1267,
    S = "achievement_reward_receive_ack"
  },
  pay_gifts_req = {Code = 1268, S = "null"},
  pay_gifts_ack = {
    Code = 1269,
    S = "pay_gifts_ack"
  },
  wish_pay_req = {
    Code = 1270,
    S = "wish_pay_req"
  },
  vip_reward_req = {Code = 1271, S = "null"},
  vip_reward_ack = {
    Code = 1272,
    S = "vip_reward_ack"
  },
  month_card_req = {Code = 1273, S = "null"},
  month_card_ack = {
    Code = 1274,
    S = "month_card_ack"
  },
  month_card_receive_reward_req = {
    Code = 1275,
    S = "month_card_receive_reward_req"
  },
  month_card_receive_reward_ack = {
    Code = 1276,
    S = "month_card_receive_reward_ack"
  },
  change_leader_req = {
    Code = 1277,
    S = "change_leader_req"
  },
  leader_info_ntf = {
    Code = 1278,
    S = "leader_info_ntf"
  },
  get_reward_detail_req = {
    Code = 1279,
    S = "get_reward_detail_req"
  },
  get_reward_detail_ack = {
    Code = 1280,
    S = "reward_detail"
  },
  save_user_id_req = {
    Code = 1281,
    S = "save_user_id_req"
  },
  user_id_status_ntf = {
    Code = 1282,
    S = "user_id_status_ntf"
  },
  item_classify_ntf = {
    Code = 1283,
    S = "item_classify_ntf"
  },
  forces_req = {Code = 1284, S = "null"},
  forces_ack = {Code = 1285, S = "forces_ack"},
  concentrate_req = {
    Code = 1286,
    S = "concentrate_req"
  },
  concentrate_ack = {
    Code = 1287,
    S = "concentrate_ack"
  },
  concentrate_end_req = {
    Code = 1288,
    S = "concentrate_end_req"
  },
  concentrate_end_ack = {
    Code = 1289,
    S = "concentrate_end_ack"
  },
  force_join_req = {
    Code = 1290,
    S = "force_join_req"
  },
  force_join_ack = {
    Code = 1291,
    S = "force_join_ack"
  },
  concentrate_atk_req = {
    Code = 1292,
    S = "concentrate_atk_req"
  },
  concentrate_atk_ack = {
    Code = 1293,
    S = "concentrate_atk_ack"
  },
  group_fight_ntf = {
    Code = 1294,
    S = "group_fight_ntf"
  },
  change_atk_seq_req = {
    Code = 1295,
    S = "change_atk_seq_req"
  },
  change_atk_seq_ack = {
    Code = 1296,
    S = "change_atk_seq_ack"
  },
  forces_ntf = {Code = 1297, S = "forces_ntf"},
  my_forces_req = {Code = 1298, S = "null"},
  my_forces_ack = {
    Code = 1299,
    S = "my_forces_ack"
  },
  mining_team_log_req = {Code = 1300, S = "null"},
  mining_team_log_ack = {
    Code = 1301,
    S = "mining_team_log_ack"
  },
  team_fight_report_req = {
    Code = 1302,
    S = "team_fight_report_req"
  },
  fair_arena_req = {Code = 1303, S = "null"},
  fair_arena_ack = {
    Code = 1304,
    S = "fair_arena_ack"
  },
  fair_arena_rank_req = {Code = 1305, S = "null"},
  fair_arena_rank_ack = {
    Code = 1306,
    S = "fair_arena_rank_ack"
  },
  fair_arena_season_req = {
    Code = 1307,
    S = "fair_arena_season_req"
  },
  fair_arena_season_ack = {
    Code = 1308,
    S = "fair_arena_rank_ack"
  },
  fair_arena_reward_req = {Code = 1309, S = "null"},
  fair_arena_reward_ack = {
    Code = 1310,
    S = "fair_arena_reward_ack"
  },
  fair_arena_reward_get_req = {
    Code = 1311,
    S = "fair_arena_reward_get_req"
  },
  fair_arena_report_req = {
    Code = 1312,
    S = "fair_arena_report_req"
  },
  fair_arena_report_ack = {
    Code = 1313,
    S = "fair_arena_report_ack"
  },
  fair_arena_report_battle_req = {
    Code = 1314,
    S = "fair_arena_report_battle_req"
  },
  fair_arena_report_battle_ack = {
    Code = 1315,
    S = "fair_arena_report_battle_ack"
  },
  fair_arena_formation_all_req = {Code = 1316, S = "null"},
  fair_arena_formation_all_ack = {
    Code = 1317,
    S = "fair_arena_formation_all_ack"
  },
  update_formations_req = {
    Code = 1318,
    S = "update_formations_req"
  },
  update_formations_ack = {
    Code = 1319,
    S = "update_formations_ack"
  },
  refresh_fleet_pool_req = {Code = 1320, S = "null"},
  refresh_fleet_pool_ack = {
    Code = 1321,
    S = "refresh_fleet_pool_ack"
  },
  fair_arena_fleet_info_req = {
    Code = 1322,
    S = "fair_arena_fleet_info_req"
  },
  fair_arena_fleet_info_ack = {
    Code = 1323,
    S = "fair_fleet_info_ack"
  },
  price_off_store_ntf = {
    Code = 1324,
    S = "price_off_store_ntf"
  },
  fte_gift_ntf = {
    Code = 1325,
    S = "fte_gift_ntf"
  },
  enter_scratch_req = {
    Code = 1326,
    S = "enter_scratch_req"
  },
  enter_scratch_ack = {
    Code = 1327,
    S = "enter_scratch_ack"
  },
  buy_scratch_card_req = {
    Code = 1328,
    S = "buy_scratch_card_req"
  },
  buy_scratch_card_ack = {
    Code = 1329,
    S = "buy_scratch_card_ack"
  },
  reset_scratch_card_req = {
    Code = 1330,
    S = "reset_scratch_card_req"
  },
  reset_scratch_card_ack = {
    Code = 1331,
    S = "reseting_ack"
  },
  get_process_award_req = {
    Code = 1332,
    S = "get_process_award_req"
  },
  get_process_award_ack = {
    Code = 1333,
    S = "get_process_award_ack"
  },
  exchange_award_req = {
    Code = 1334,
    S = "exchange_award_req"
  },
  exchange_award_ack = {
    Code = 1335,
    S = "exchange_award_ack"
  },
  end_scratch_req = {
    Code = 1336,
    S = "end_scratch_req"
  },
  end_scratch_ack = {
    Code = 1337,
    S = "end_scratch_ack"
  },
  uc_pay_sign_req = {
    Code = 1338,
    S = "uc_pay_sign_req"
  },
  uc_pay_sign_ack = {
    Code = 1339,
    S = "uc_pay_sign_ack"
  },
  ac_sweep_req = {Code = 1340, S = "null"},
  art_one_upgrade_data_req = {
    Code = 1341,
    S = "art_one_upgrade_data_req"
  },
  art_one_upgrade_data_ack = {
    Code = 1342,
    S = "art_one_upgrade_data_ack"
  },
  art_one_upgrade_req = {
    Code = 1343,
    S = "art_one_upgrade_req"
  },
  art_one_upgrade_ack = {
    Code = 1344,
    S = "art_one_upgrade_ack"
  },
  scratch_board_ntf = {
    Code = 1345,
    S = "scratch_board_ntf"
  },
  confirm_exchange_req = {
    Code = 1346,
    S = "confirm_exchange_req"
  },
  credit_exchange_ntf = {
    Code = 1347,
    S = "credit_exchange_ntf"
  },
  normal_ntf = {Code = 1348, S = "normal_ntf"},
  change_fleets_data_req = {
    Code = 1349,
    S = "change_fleets_data_req"
  },
  change_fleets_data_ack = {
    Code = 1350,
    S = "change_fleets_data_ack"
  },
  change_fleets_req = {
    Code = 1351,
    S = "change_fleets_req"
  },
  enter_login_sign_req = {Code = 1352, S = "null"},
  enter_login_sign_ack = {
    Code = 1353,
    S = "login_sign_ack"
  },
  get_login_sign_award_req = {
    Code = 1354,
    S = "get_login_sign_award_req"
  },
  get_login_sign_award_ack = {
    Code = 1355,
    S = "get_login_sign_award_ack"
  },
  krypton_redpoint_req = {
    Code = 1356,
    S = "krypton_redpoint_req"
  },
  krypton_redpoint_ack = {
    Code = 1357,
    S = "krypton_redpoint_ack"
  },
  event_click_req = {Code = 1358, S = "null"},
  wxpay_info_ntf = {
    Code = 1359,
    S = "wxpay_info_ntf"
  },
  redownload_res_req = {Code = 1360, S = "null"},
  redownload_res_ack = {
    Code = 1361,
    S = "redownload_res_ack"
  },
  open_box_progress_award_req = {
    Code = 1362,
    S = "open_box_progress_award_req"
  },
  exchange_regulation_ntf = {
    Code = 1363,
    S = "exchange_regulation_ntf"
  },
  mul_credit_exchange_ack = {
    Code = 1364,
    S = "mul_credit_exchange_ack"
  },
  mul_credit_exchange_ntf = {
    Code = 1365,
    S = "mul_credit_exchange_ntf"
  },
  enter_medal_store_req = {Code = 1366, S = "null"},
  enter_medal_store_ack = {
    Code = 1367,
    S = "enter_medal_store_ack"
  },
  medal_store_buy_req = {
    Code = 1368,
    S = "medal_store_buy_req"
  },
  medal_store_buy_ack = {
    Code = 1369,
    S = "medal_store_buy_ack"
  },
  medal_store_refresh_req = {
    Code = 1370,
    S = "medal_store_refresh_req"
  },
  medal_store_refresh_ack = {
    Code = 1371,
    S = "medal_store_refresh_ack"
  },
  medal_list_req = {Code = 1372, S = "null"},
  medal_list_ack = {
    Code = 1373,
    S = "medal_list_ack"
  },
  medal_decompose_confirm_req = {
    Code = 1374,
    S = "medal_decompose_confirm_req"
  },
  medal_decompose_confirm_ack = {
    Code = 1375,
    S = "medal_decompose_confirm_ack"
  },
  medal_reset_confirm_req = {
    Code = 1376,
    S = "medal_reset_confirm_req"
  },
  medal_reset_confirm_ack = {
    Code = 1377,
    S = "medal_reset_confirm_ack"
  },
  medal_equip_req = {Code = 1378, S = "null"},
  medal_equip_ack = {
    Code = 1379,
    S = "medal_equip_ack"
  },
  medal_level_up_req = {
    Code = 1380,
    S = "medal_level_up_req"
  },
  medal_level_up_ack = {
    Code = 1381,
    S = "medal_level_up_ack"
  },
  medal_rank_up_req = {
    Code = 1382,
    S = "medal_rank_up_req"
  },
  medal_rank_up_ack = {
    Code = 1383,
    S = "medal_rank_up_ack"
  },
  medal_list_ntf = {
    Code = 1384,
    S = "medal_list_ntf"
  },
  medal_fleet_formation_ntf = {
    Code = 1385,
    S = "medal_fleet_formation_ntf"
  },
  medal_fleet_red_point_ntf = {
    Code = 1386,
    S = "medal_fleet_red_point_ntf"
  },
  medal_equip_on_req = {
    Code = 1387,
    S = "medal_equip_on_req"
  },
  medal_equip_on_ack = {
    Code = 1388,
    S = "medal_equip_on_ack"
  },
  medal_equip_off_req = {
    Code = 1389,
    S = "medal_equip_off_req"
  },
  medal_equip_off_ack = {
    Code = 1390,
    S = "medal_equip_off_ack"
  },
  medal_equip_replace_req = {
    Code = 1391,
    S = "medal_equip_replace_req"
  },
  medal_equip_replace_ack = {
    Code = 1392,
    S = "medal_equip_replace_ack"
  },
  medal_fleet_btn_red_point_ntf = {
    Code = 1393,
    S = "medal_fleet_btn_red_point_ntf"
  },
  medal_config_info_req = {
    Code = 1398,
    S = "medal_config_info_req"
  },
  medal_config_info_ack = {
    Code = 1399,
    S = "medal_config_info_ack"
  },
  fight_report_round_ntf = {
    Code = 1400,
    S = "fight_report_round_ntf"
  },
  medal_glory_collection_ntf = {
    Code = 1401,
    S = "medal_glory_collection_ntf"
  },
  medal_glory_collection_req = {
    Code = 1402,
    S = "medal_glory_collection_req"
  },
  medal_glory_collection_ack = {
    Code = 1403,
    S = "medal_glory_collection_ack"
  },
  medal_glory_activate_req = {
    Code = 1404,
    S = "medal_glory_activate_req"
  },
  medal_glory_activate_ack = {
    Code = 1405,
    S = "medal_glory_activate_ack"
  },
  medal_glory_rank_up_req = {
    Code = 1406,
    S = "medal_glory_rank_up_req"
  },
  medal_glory_rank_up_ack = {
    Code = 1407,
    S = "medal_glory_rank_up_ack"
  },
  medal_glory_reset_req = {
    Code = 1408,
    S = "medal_glory_reset_req"
  },
  medal_glory_reset_ack = {
    Code = 1409,
    S = "medal_glory_reset_ack"
  },
  medal_glory_achievement_req = {Code = 1410, S = "null"},
  medal_glory_achievement_ack = {
    Code = 1411,
    S = "medal_glory_achievement_ack"
  },
  medal_glory_achievement_activate_req = {Code = 1412, S = "null"},
  medal_glory_achievement_activate_ack = {
    Code = 1413,
    S = "medal_glory_achievement_activate_ack"
  },
  medal_glory_collection_rank_req = {Code = 1414, S = "null"},
  medal_glory_collection_rank_ack = {
    Code = 1415,
    S = "medal_glory_collection_rank_ack"
  },
  enter_medal_boss_req = {Code = 1416, S = "null"},
  enter_medal_boss_ack = {
    Code = 1417,
    S = "enter_medal_boss_ack"
  },
  reset_medal_boss_req = {
    Code = 1418,
    S = "reset_medal_boss_req"
  },
  reset_medal_boss_ack = {
    Code = 1419,
    S = "reset_medal_boss_ack"
  },
  challenge_medal_boss_req = {
    Code = 1420,
    S = "challenge_medal_boss_req"
  },
  challenge_medal_boss_ack = {
    Code = 1421,
    S = "challenge_medal_boss_ack"
  },
  medal_glory_achievement_red_point_ntf = {
    Code = 1422,
    S = "medal_glory_achievement_red_point_ntf"
  },
  battle_rate_req = {
    Code = 1423,
    S = "battle_rate_req"
  },
  hero_union_unlock_ntf = {
    Code = 1424,
    S = "hero_union_unlock_ntf"
  },
  medal_glory_collection_total_attr_req = {Code = 1425, S = "null"},
  medal_glory_collection_total_attr_ack = {
    Code = 1426,
    S = "medal_glory_collection_total_attr_ack"
  },
  medal_glory_collection_enter_req = {
    Code = 1427,
    S = "medal_glory_collection_enter_req"
  },
  medal_glory_collection_enter_ack = {
    Code = 1428,
    S = "medal_glory_collection_enter_ack"
  },
  user_statistics_req = {
    Code = 1429,
    S = "user_statistics_req"
  },
  user_rush_exit_req = {
    Code = 1430,
    S = "battle_request"
  },
  fps_switch_ntf = {
    Code = 1431,
    S = "fps_switch_ntf"
  },
  fleet_can_enhance_req = {
    Code = 1432,
    S = "fleet_enhance_req"
  },
  expedition_wormhole_status_req = {Code = 1433, S = "null"},
  expedition_wormhole_status_ack = {
    Code = 1434,
    S = "expedition_wormhole_status"
  },
  expedition_wormhole_award_req = {
    Code = 1435,
    S = "expedition_wormhole_award_req"
  },
  expedition_wormhole_rank_board_req = {
    Code = 1436,
    S = "expedition_wormhole_rank_board_req"
  },
  expedition_wormhole_rank_board_ack = {
    Code = 1437,
    S = "expedition_wormhole_rank_board_ack"
  },
  expedition_wormhole_rank_self_ntf = {
    Code = 1438,
    S = "expedition_wormhole_rank_self_ntf"
  },
  expedition_task_update_ntf = {
    Code = 1439,
    S = "expedition_wormhole_task"
  },
  expedition_top3_update_ntf = {
    Code = 1440,
    S = "expedition_wormhole_rank_board"
  },
  expedition_rank_awards_req = {Code = 1441, S = "null"},
  expedition_rank_awards_ack = {
    Code = 1442,
    S = "expedition_rank_awards_ack"
  },
  expedition_stores_refresh_req = {Code = 1443, S = "null"},
  expedition_stores_refresh_ack = {
    Code = 1444,
    S = "expedition_stores_refresh_ack"
  },
  expedition_refresh_type_req = {Code = 1445, S = "null"},
  expedition_refresh_type_ack = {
    Code = 1446,
    S = "expedition_refresh_type_ack"
  },
  large_map_my_info_ntf = {
    Code = 1501,
    S = "large_map_player_detail"
  },
  large_map_block_req = {
    Code = 1502,
    S = "large_map_block_req"
  },
  large_map_block_ack = {
    Code = 1503,
    S = "large_map_block_ack"
  },
  large_map_base_req = {Code = 1504, S = "null"},
  large_map_base_ack = {
    Code = 1505,
    S = "large_map_base_ack"
  },
  large_map_block_update_ntf = {
    Code = 1506,
    S = "large_map_block_update_ntf"
  },
  large_map_route_req = {
    Code = 1507,
    S = "large_map_route_req"
  },
  large_map_route_ack = {
    Code = 1508,
    S = "large_map_route_info"
  },
  large_map_route_ntf = {
    Code = 1509,
    S = "large_map_route_ntf"
  },
  large_map_event_ntf = {
    Code = 1510,
    S = "large_map_event_ntf"
  },
  large_map_battle_req = {
    Code = 1511,
    S = "large_map_battle_req"
  },
  large_map_join_alliance_req = {
    Code = 1512,
    S = "large_map_join_alliance_req"
  },
  large_map_join_alliance_ack = {
    Code = 1513,
    S = "large_map_join_alliance_ack"
  },
  large_map_exit_alliance_req = {
    Code = 1514,
    S = "large_map_exit_alliance_req"
  },
  large_map_exit_alliance_ack = {
    Code = 1515,
    S = "large_map_alliance_info"
  },
  large_map_near_alliance_req = {Code = 1516, S = "null"},
  large_map_near_alliance_ack = {
    Code = 1517,
    S = "large_map_alliance_list"
  },
  large_map_create_alliance_req = {
    Code = 1518,
    S = "large_map_create_alliance_req"
  },
  large_map_create_alliance_ack = {
    Code = 1519,
    S = "large_map_alliance_info"
  },
  large_map_alliance_affais_req = {Code = 1520, S = "null"},
  large_map_alliance_affais_ack = {
    Code = 1521,
    S = "large_map_alliance_affais_ack"
  },
  large_map_alliance_army_req = {Code = 1522, S = "null"},
  large_map_alliance_army_ack = {
    Code = 1523,
    S = "large_map_alliance_army_ack"
  },
  large_map_alliance_invite_data_req = {Code = 1524, S = "null"},
  large_map_alliance_invite_data_ack = {
    Code = 1525,
    S = "large_map_alliance_invite_data_ack"
  },
  large_map_alliance_player_search_req = {
    Code = 1526,
    S = "large_map_alliance_player_search_req"
  },
  large_map_alliance_player_search_ack = {
    Code = 1527,
    S = "large_map_alliance_player_search_ack"
  },
  large_map_alliance_invite_req = {
    Code = 1528,
    S = "large_map_alliance_invite_req"
  },
  large_map_alliance_invite_ack = {
    Code = 1529,
    S = "large_map_alliance_invite_ack"
  },
  large_map_alliance_recruit_option_req = {
    Code = 1530,
    S = "large_map_alliance_recruit_option_req"
  },
  large_map_alliance_recruit_option_ack = {
    Code = 1531,
    S = "large_map_alliance_recruit_option_info"
  },
  large_map_alliance_recruit_option_update_req = {
    Code = 1532,
    S = "large_map_alliance_recruit_option_update_req"
  },
  large_map_alliance_ratify_join_data_req = {
    Code = 1533,
    S = "large_map_alliance_ratify_join_data_req"
  },
  large_map_alliance_ratify_join_data_ack = {
    Code = 1534,
    S = "large_map_alliance_ratify_apply_info"
  },
  large_map_alliance_ratify_duty_data_req = {
    Code = 1535,
    S = "large_map_alliance_ratify_duty_data_req"
  },
  large_map_alliance_ratify_duty_data_ack = {
    Code = 1536,
    S = "large_map_alliance_ratify_apply_info"
  },
  large_map_alliance_ratify_handle_req = {
    Code = 1537,
    S = "large_map_alliance_ratify_handle_req"
  },
  large_map_alliance_ratify_handle_ack = {
    Code = 1538,
    S = "large_map_alliance_ratify_handle_ack"
  },
  large_map_alliance_apply_duty_req = {
    Code = 1539,
    S = "large_map_alliance_apply_duty_req"
  },
  large_map_alliance_demotion_req = {
    Code = 1540,
    S = "large_map_alliance_demotion_req"
  },
  large_map_alliance_demotion_ack = {
    Code = 1541,
    S = "large_map_alliance_member_info"
  },
  large_map_alliance_exchange_req = {
    Code = 1542,
    S = "large_map_alliance_exchange_req"
  },
  large_map_alliance_exchange_ack = {
    Code = 1543,
    S = "large_map_alliance_exchange_ack"
  },
  large_map_alliance_expel_req = {
    Code = 1544,
    S = "large_map_alliance_expel_req"
  },
  large_map_alliance_expel_ack = {
    Code = 1545,
    S = "large_map_alliance_expel_ack"
  },
  large_map_block_detail_req = {
    Code = 1546,
    S = "large_map_block_detail_req"
  },
  large_map_block_detail_ack = {
    Code = 1547,
    S = "large_map_block_detail"
  },
  large_map_create_mass_troop_req = {
    Code = 1548,
    S = "large_map_create_mass_troop_req"
  },
  large_map_mass_troops_req = {Code = 1549, S = "null"},
  large_map_mass_troops_ack = {
    Code = 1550,
    S = "large_map_mass_troops_list"
  },
  large_map_join_mass_req = {
    Code = 1551,
    S = "large_map_join_mass_req"
  },
  large_map_join_mass_ack = {
    Code = 1552,
    S = "large_map_troops_detail"
  },
  large_map_mass_troops_info_req = {Code = 1553, S = "null"},
  large_map_mass_troops_info_ack = {
    Code = 1554,
    S = "large_map_troops_detail"
  },
  large_map_dissolve_mass_troop_req = {Code = 1555, S = "null"},
  large_map_mass_near_invite_req = {
    Code = 1556,
    S = "large_map_mass_near_invite_req"
  },
  large_map_mass_near_invite_ack = {
    Code = 1557,
    S = "large_map_mass_invite_data"
  },
  large_map_mass_invite_req = {
    Code = 1558,
    S = "large_map_mass_invite_req"
  },
  large_map_mass_invite_ack = {
    Code = 1559,
    S = "large_map_mass_invite_data"
  },
  large_map_mass_invite_ntf = {
    Code = 1560,
    S = "large_map_mass_invite_ntf"
  },
  large_map_mass_invite_do_req = {
    Code = 1561,
    S = "large_map_mass_invite_do_req"
  },
  large_map_exit_mass_troop_req = {
    Code = 1562,
    S = "large_map_exit_mass_troop_req"
  },
  large_map_start_mass_war_req = {Code = 1563, S = "null"},
  large_map_view_mass_player_req = {
    Code = 1564,
    S = "large_map_view_mass_player_req"
  },
  large_map_view_mass_player_ack = {
    Code = 1565,
    S = "large_map_simple_player_info"
  },
  large_map_remove_mass_player_req = {
    Code = 1566,
    S = "large_map_remove_mass_player_req"
  },
  large_map_remove_mass_player_ntf = {
    Code = 1567,
    S = "large_map_mass_troops_member"
  },
  large_map_repair_req = {Code = 1569, S = "null"},
  large_map_repair_ack = {
    Code = 1570,
    S = "large_map_repair_ack"
  },
  large_map_repair_ship_req = {
    Code = 1571,
    S = "large_map_repair_ship_req"
  },
  large_map_repair_ship_ack = {
    Code = 1572,
    S = "large_map_repair_avatar_item"
  },
  large_map_add_mass_player_ntf = {
    Code = 1573,
    S = "large_map_add_mass_player_ntf"
  },
  large_map_buy_energy_req = {
    Code = 1574,
    S = "large_map_buy_energy_req"
  },
  large_map_local_leader_req = {
    Code = 1575,
    S = "large_map_local_leader_req"
  },
  large_map_local_leader_ack = {
    Code = 1576,
    S = "large_map_local_leader_ack"
  },
  large_map_alliance_diplomacy_req = {Code = 1577, S = "null"},
  large_map_alliance_diplomacy_ack = {
    Code = 1578,
    S = "large_map_alliance_diplomacy_ack"
  },
  large_map_alliance_diplomacy_exchange_req = {
    Code = 1579,
    S = "large_map_alliance_diplomacy_exchange_req"
  },
  large_map_alliance_modify_notice_req = {
    Code = 1580,
    S = "large_map_alliance_modify_notice_req"
  },
  large_map_alliance_modify_notice_ack = {
    Code = 1581,
    S = "large_map_alliance_affais_ack"
  },
  large_map_alliance_adjust_req = {
    Code = 1582,
    S = "large_map_alliance_adjust_req"
  },
  large_map_alliance_adjust_ack = {
    Code = 1583,
    S = "large_map_alliance_affais_ack"
  },
  large_map_log_req = {
    Code = 1584,
    S = "large_map_log_req"
  },
  large_map_log_ack = {
    Code = 1585,
    S = "large_map_log_ack"
  },
  large_map_log_battle_req = {
    Code = 1586,
    S = "large_map_log_battle_req"
  },
  large_map_search_alliance_req = {
    Code = 1587,
    S = "large_map_search_alliance_req"
  },
  large_map_search_alliance_ack = {
    Code = 1588,
    S = "large_map_alliance_list"
  },
  large_map_dissolve_alliance_req = {
    Code = 1589,
    S = "large_map_dissolve_alliance_req"
  },
  large_map_dissolve_alliance_ack = {
    Code = 1590,
    S = "large_map_alliance_info"
  },
  large_map_rank_req = {
    Code = 1591,
    S = "large_map_rank_req"
  },
  large_map_rank_ack = {
    Code = 1592,
    S = "large_map_rank_ack"
  },
  large_map_alliance_player_exit_ntf = {
    Code = 1593,
    S = "large_map_alliance_player_exit_ntf"
  },
  large_map_alliance_promotion_req = {
    Code = 1594,
    S = "large_map_alliance_promotion_req"
  },
  large_map_alliance_promotion_ack = {
    Code = 1595,
    S = "large_map_alliance_member_info"
  },
  large_map_mass_attack_line_ntf = {
    Code = 1596,
    S = "large_map_mass_attack_line_ntf"
  },
  large_map_battle_ack = {
    Code = 1597,
    S = "large_map_battle_ack"
  },
  large_map_vision_ntf = {
    Code = 1598,
    S = "large_map_vision_ntf"
  },
  large_map_vision_change_ntf = {
    Code = 1599,
    S = "large_map_vision_change_ntf"
  },
  large_map_small_map_req = {Code = 1600, S = "null"},
  large_map_small_map_ack = {
    Code = 1601,
    S = "large_map_small_map_ack"
  },
  large_map_ship_arrive_req = {
    Code = 1602,
    S = "large_map_ship_arrive_req"
  },
  tlc_enter_req = {Code = 1701, S = "null"},
  tlc_enter_ack = {
    Code = 1702,
    S = "tlc_enter_ack"
  },
  tlc_match_req = {Code = 1705, S = "null"},
  tlc_match_ack = {
    Code = 1706,
    S = "tlc_match_ack"
  },
  tlc_fight_req = {
    Code = 1707,
    S = "tlc_fight_req"
  },
  tlc_fight_ack = {
    Code = 1708,
    S = "tlc_fight_ack"
  },
  tlc_report_req = {Code = 1709, S = "null"},
  tlc_report_ack = {
    Code = 1710,
    S = "tlc_report_ack"
  },
  tlc_report_detail_req = {
    Code = 1711,
    S = "tlc_report_detail_req"
  },
  tlc_report_detail_ack = {
    Code = 1712,
    S = "tlc_report_detail_ack"
  },
  tlc_info_ntf = {
    Code = 1713,
    S = "tlc_info_ntf"
  },
  tlc_awards_req = {Code = 1714, S = "null"},
  tlc_awards_ack = {
    Code = 1715,
    S = "tlc_awards_ack"
  },
  tlc_rank_board_req = {Code = 1716, S = "null"},
  tlc_rank_board_ack = {
    Code = 1717,
    S = "tlc_rank_board_ack"
  },
  tlc_honor_wall_req = {Code = 1718, S = "null"},
  tlc_honor_wall_ack = {
    Code = 1719,
    S = "tlc_honor_wall_ack"
  },
  tlc_enemy_release_req = {Code = 1720, S = "null"},
  tlc_awards_ntf = {
    Code = 1721,
    S = "tlc_awards_ntf"
  },
  tlc_awards_get_req = {
    Code = 1722,
    S = "tlc_awards_get_req"
  },
  tlc_awards_get_ack = {
    Code = 1723,
    S = "tlc_awards_get_ack"
  },
  tlc_rankup_ntf = {
    Code = 1724,
    S = "tlc_rankup_ntf"
  },
  tlc_fight_status_req = {Code = 1725, S = "null"},
  tlc_fight_status_ack = {
    Code = 1726,
    S = "tlc_fight_status_ack"
  },
  tlc_status_ntf = {
    Code = 1727,
    S = "tlc_status_ntf"
  },
  champion_cdtime_req = {Code = 1729, S = "null"},
  champion_cdtime_ack = {
    Code = 1730,
    S = "champion_cdtime_ack"
  },
  tlc_rank_board_ntf = {
    Code = 1745,
    S = "tlc_rank_board_ntf"
  },
  tlc_bag_req = {
    Code = 1746,
    S = "tlc_bag_req"
  },
  tlc_bag_ack = {
    Code = 1747,
    S = "tlc_bag_info"
  },
  tlc_krypton_store_req = {
    Code = 1748,
    S = "tlc_krypton_store_req"
  },
  tlc_krypton_store_ack = {
    Code = 1749,
    S = "tlc_krypton_store_ack"
  },
  tlc_mulmatrix_switch_req = {
    Code = 1750,
    S = "tlc_mulmatrix_switch_req"
  },
  tlc_quit_req = {Code = 1752, S = "null"},
  add_tlc_adjutant_req = {
    Code = 1753,
    S = "add_tlc_adjutant_req"
  },
  simple_mulmatrix_get_req = {Code = 1754, S = "null"},
  simple_mulmatrix_get_ack = {
    Code = 1755,
    S = "mulmatrix_info"
  },
  change_tlc_leader_req = {
    Code = 1756,
    S = "change_tlc_leader_req"
  },
  tlc_krypton_sort_req = {
    Code = 1757,
    S = "tlc_krypton_sort_req"
  },
  tlc_krypton_sort_ack = {
    Code = 1758,
    S = "tlc_krypton_store_ack"
  },
  teamleague_matrix_req = {
    Code = 1801,
    S = "teamleague_matrix_req"
  },
  teamleague_matrix_ack = {
    Code = 1802,
    S = "teamleague_matrix_ack"
  },
  teamleague_matrix_save_req = {
    Code = 1803,
    S = "teamleague_matrix_save_req"
  },
  tlc_matrix_change_ntf = {
    Code = 1804,
    S = "tlc_matrix_change_ntf"
  },
  cumulate_recharge_req = {Code = 1810, S = "null"},
  cumulate_recharge_ack = {
    Code = 1811,
    S = "cumulate_recharge_ack"
  },
  cumulate_recharge_award_get_req = {
    Code = 1812,
    S = "cumulate_recharge_award_get_req"
  },
  cumulate_recharge_award_get_ack = {
    Code = 1813,
    S = "cumulate_recharge_ack"
  },
  login_fund_req = {Code = 1814, S = "null"},
  login_fund_ack = {
    Code = 1815,
    S = "login_fund_ack"
  },
  login_fund_reward_req = {
    Code = 1816,
    S = "login_fund_reward_req"
  },
  check_exp_add_item_req = {
    Code = 1817,
    S = "check_exp_add_item_req"
  },
  check_exp_add_item_ack = {
    Code = 1818,
    S = "check_exp_add_item_ack"
  },
  exp_add_item_ntf = {
    Code = 1819,
    S = "exp_add_item_ntf"
  },
  pop_firstpay_on_lvup_ntf = {
    Code = 1820,
    S = "pop_firstpay_on_lvup_ntf"
  },
  sync_server_time_req = {Code = 1821, S = "null"},
  gcs_task_data_req = {Code = 1822, S = "null"},
  gcs_task_data_ack = {
    Code = 1823,
    S = "gcs_task_data_ack"
  },
  gcs_awards_data_req = {Code = 1824, S = "null"},
  gcs_awards_data_ack = {
    Code = 1825,
    S = "gcs_awards_data_ack"
  },
  gcs_today_task_change_req = {
    Code = 1826,
    S = "gcs_today_task_change_req"
  },
  gcs_today_task_change_ack = {
    Code = 1827,
    S = "gcs_today_task_change_ack"
  },
  gcs_get_today_task_reward_req = {
    Code = 1828,
    S = "gcs_get_today_task_reward_req"
  },
  gcs_get_today_task_reward_ack = {
    Code = 1829,
    S = "gcs_get_today_task_reward_ack"
  },
  gcs_get_week_task_reward_req = {
    Code = 1830,
    S = "gcs_get_week_task_reward_req"
  },
  gcs_get_week_task_reward_ack = {
    Code = 1831,
    S = "gcs_get_week_task_reward_ack"
  },
  gcs_get_week_extra_reward_req = {
    Code = 1832,
    S = "gcs_get_week_extra_reward_req"
  },
  gcs_get_week_extra_reward_ack = {
    Code = 1833,
    S = "gcs_get_week_extra_reward_ack"
  },
  gcs_get_lv_all_rewards_req = {Code = 1834, S = "null"},
  gcs_get_lv_all_rewards_ack = {
    Code = 1835,
    S = "gcs_get_lv_all_rewards_ack"
  },
  gcs_point_update_ntf = {
    Code = 1836,
    S = "gcs_point_update_ntf"
  },
  gcs_buy_update_ntf = {
    Code = 1837,
    S = "gcs_buy_update_ntf"
  }
}
NetAPIList.CodeToMsgType = {
  MSGTYPE632 = NetAPIList.budo_test_req.S,
  MSGTYPE563 = NetAPIList.contention_history_req.S,
  MSGTYPE1430 = NetAPIList.user_rush_exit_req.S,
  MSGTYPE1348 = NetAPIList.normal_ntf.S,
  MSGTYPE579 = NetAPIList.mulmatrix_save_req.S,
  MSGTYPE383 = NetAPIList.allstars_reward_ack.S,
  MSGTYPE345 = NetAPIList.block_devil_ntf.S,
  MSGTYPE667 = NetAPIList.pay_list_carrier_ack.S,
  MSGTYPE358 = NetAPIList.champion_rank_reward_ack.S,
  MSGTYPE433 = NetAPIList.activity_stores_ack.S,
  MSGTYPE1398 = NetAPIList.medal_config_info_req.S,
  MSGTYPE899 = NetAPIList.max_step_ack.S,
  MSGTYPE724 = NetAPIList.push_button_ntf.S,
  MSGTYPE1592 = NetAPIList.large_map_rank_ack.S,
  MSGTYPE587 = NetAPIList.ladder_reset_req.S,
  MSGTYPE1111 = NetAPIList.wdc_report_detail_req.S,
  MSGTYPE1169 = NetAPIList.hero_union_ack.S,
  MSGTYPE949 = NetAPIList.get_invite_gift_req.S,
  MSGTYPE1190 = NetAPIList.mining_wars_star_ack.S,
  MSGTYPE301 = NetAPIList.mine_info_ntf.S,
  MSGTYPE1137 = NetAPIList.throw_dice_ack.S,
  MSGTYPE1527 = NetAPIList.large_map_alliance_player_search_ack.S,
  MSGTYPE540 = NetAPIList.ladder_cancel_req.S,
  MSGTYPE1435 = NetAPIList.expedition_wormhole_award_req.S,
  MSGTYPE972 = NetAPIList.story_award_ack.S,
  MSGTYPE361 = NetAPIList.vip_loot_ack.S,
  MSGTYPE147 = NetAPIList.krypton_gacha_some_req.S,
  MSGTYPE180 = NetAPIList.alliance_quit_req.S,
  MSGTYPE281 = NetAPIList.event_loot_ack.S,
  MSGTYPE1153 = NetAPIList.event_progress_award_req.S,
  MSGTYPE160 = NetAPIList.champion_list_req.S,
  MSGTYPE1526 = NetAPIList.large_map_alliance_player_search_req.S,
  MSGTYPE1531 = NetAPIList.large_map_alliance_recruit_option_ack.S,
  MSGTYPE958 = NetAPIList.remodel_help_others_ack.S,
  MSGTYPE982 = NetAPIList.view_each_task_req.S,
  MSGTYPE922 = NetAPIList.tactical_equip_on_req.S,
  MSGTYPE664 = NetAPIList.laba_times_ntf.S,
  MSGTYPE1596 = NetAPIList.large_map_mass_attack_line_ntf.S,
  MSGTYPE188 = NetAPIList.alliance_demote_fail_ack.S,
  MSGTYPE562 = NetAPIList.contention_award_ack.S,
  MSGTYPE559 = NetAPIList.fleet_show_req.S,
  MSGTYPE459 = NetAPIList.client_effect_ack.S,
  MSGTYPE0 = NetAPIList.common_ack.S,
  MSGTYPE1160 = NetAPIList.recruit_list_new_req.S,
  MSGTYPE456 = NetAPIList.items_req.S,
  MSGTYPE699 = NetAPIList.remodel_speed_req.S,
  MSGTYPE1381 = NetAPIList.medal_level_up_ack.S,
  MSGTYPE943 = NetAPIList.enter_plante_ack.S,
  MSGTYPE1121 = NetAPIList.wdc_awards_ntf.S,
  MSGTYPE464 = NetAPIList.domination_join_req.S,
  MSGTYPE300 = NetAPIList.mine_refresh_ntf.S,
  MSGTYPE572 = NetAPIList.items_ntf.S,
  MSGTYPE130 = NetAPIList.alliance_apply_req.S,
  MSGTYPE195 = NetAPIList.alliance_kick_fail_ack.S,
  MSGTYPE623 = NetAPIList.crusade_buy_cross_ack.S,
  MSGTYPE336 = NetAPIList.ac_matrix_req.S,
  MSGTYPE10000 = NetAPIList.keylist_ntf.S,
  MSGTYPE133 = NetAPIList.alliance_unapply_req.S,
  MSGTYPE1179 = NetAPIList.mining_world_ack.S,
  MSGTYPE1184 = NetAPIList.mining_wars_req.S,
  MSGTYPE1412 = NetAPIList.medal_glory_achievement_activate_req.S,
  MSGTYPE7 = NetAPIList.user_snapshot_req.S,
  MSGTYPE553 = NetAPIList.hero_level_attr_diff_req.S,
  MSGTYPE258 = NetAPIList.pay_list_ack.S,
  MSGTYPE196 = NetAPIList.krypton_equip_ack.S,
  MSGTYPE297 = NetAPIList.mine_speedup_req.S,
  MSGTYPE273 = NetAPIList.all_act_status_req.S,
  MSGTYPE1136 = NetAPIList.throw_dice_req.S,
  MSGTYPE554 = NetAPIList.hero_level_attr_diff_ack.S,
  MSGTYPE467 = NetAPIList.domination_challenge_req.S,
  MSGTYPE40 = NetAPIList.revenue_info_req.S,
  MSGTYPE1364 = NetAPIList.mul_credit_exchange_ack.S,
  MSGTYPE890 = NetAPIList.activity_store_price_req.S,
  MSGTYPE602 = NetAPIList.pay_record_ntf.S,
  MSGTYPE404 = NetAPIList.equipment_action_list_req.S,
  MSGTYPE447 = NetAPIList.alliance_defence_repair_req.S,
  MSGTYPE257 = NetAPIList.pay_list_req.S,
  MSGTYPE64 = NetAPIList.equipment_detail_req.S,
  MSGTYPE1383 = NetAPIList.medal_rank_up_ack.S,
  MSGTYPE1753 = NetAPIList.add_tlc_adjutant_req.S,
  MSGTYPE103 = NetAPIList.shop_purchase_back_req.S,
  MSGTYPE569 = NetAPIList.pay_verify2_ack.S,
  MSGTYPE419 = NetAPIList.colony_exp_ntf.S,
  MSGTYPE838 = NetAPIList.pay_sign_ack.S,
  MSGTYPE212 = NetAPIList.shop_buy_and_wear_req.S,
  MSGTYPE895 = NetAPIList.refresh_type_req.S,
  MSGTYPE1829 = NetAPIList.gcs_get_today_task_reward_ack.S,
  MSGTYPE606 = NetAPIList.change_auto_decompose_ntf.S,
  MSGTYPE87 = NetAPIList.equipment_enhance_req.S,
  MSGTYPE527 = NetAPIList.screen_track_req.S,
  MSGTYPE872 = NetAPIList.get_goal_award_req.S,
  MSGTYPE294 = NetAPIList.mine_refresh_req.S,
  MSGTYPE1144 = NetAPIList.dice_board_ntf.S,
  MSGTYPE1569 = NetAPIList.large_map_repair_req.S,
  MSGTYPE526 = NetAPIList.activity_status_ack.S,
  MSGTYPE1280 = NetAPIList.get_reward_detail_ack.S,
  MSGTYPE1822 = NetAPIList.gcs_task_data_req.S,
  MSGTYPE223 = NetAPIList.achievement_finish_ntf.S,
  MSGTYPE851 = NetAPIList.pay_push_pop_ntf.S,
  MSGTYPE1271 = NetAPIList.vip_reward_req.S,
  MSGTYPE45 = NetAPIList.add_friend_req.S,
  MSGTYPE1724 = NetAPIList.tlc_rankup_ntf.S,
  MSGTYPE716 = NetAPIList.multi_acc_req.S,
  MSGTYPE127 = NetAPIList.alliance_delete_req.S,
  MSGTYPE615 = NetAPIList.crusade_fight_req.S,
  MSGTYPE23 = NetAPIList.battle_status_req.S,
  MSGTYPE249 = NetAPIList.supply_info_req.S,
  MSGTYPE1552 = NetAPIList.large_map_join_mass_ack.S,
  MSGTYPE439 = NetAPIList.colony_players_req.S,
  MSGTYPE200 = NetAPIList.user_brief_info_req.S,
  MSGTYPE96 = NetAPIList.quark_exchange_ack.S,
  MSGTYPE1207 = NetAPIList.art_point_upgrade_ack.S,
  MSGTYPE1209 = NetAPIList.art_core_list_ack.S,
  MSGTYPE1310 = NetAPIList.fair_arena_reward_ack.S,
  MSGTYPE666 = NetAPIList.pay_list_carrier_req.S,
  MSGTYPE1251 = NetAPIList.main_city_ntf.S,
  MSGTYPE121 = NetAPIList.alliance_logs_ack.S,
  MSGTYPE396 = NetAPIList.krypton_fragment_core_ntf.S,
  MSGTYPE743 = NetAPIList.krypton_inject_list_req.S,
  MSGTYPE1585 = NetAPIList.large_map_log_ack.S,
  MSGTYPE387 = NetAPIList.fleet_sale_with_rebate_list_ack.S,
  MSGTYPE421 = NetAPIList.colony_logs_ntf.S,
  MSGTYPE850 = NetAPIList.temp_vip_ntf.S,
  MSGTYPE328 = NetAPIList.refresh_time_ntf.S,
  MSGTYPE519 = NetAPIList.contention_collect_req.S,
  MSGTYPE271 = NetAPIList.price_ack.S,
  MSGTYPE1152 = NetAPIList.user_change_ack.S,
  MSGTYPE1388 = NetAPIList.medal_equip_on_ack.S,
  MSGTYPE820 = NetAPIList.prime_enemy_info_req.S,
  MSGTYPE2 = NetAPIList.user_register_ack.S,
  MSGTYPE1266 = NetAPIList.achievement_reward_receive_req.S,
  MSGTYPE155 = NetAPIList.alliance_refuse_req.S,
  MSGTYPE542 = NetAPIList.ladder_fresh_ack.S,
  MSGTYPE39 = NetAPIList.techniques_upgrade_ack.S,
  MSGTYPE1721 = NetAPIList.tlc_awards_ntf.S,
  MSGTYPE1274 = NetAPIList.month_card_ack.S,
  MSGTYPE56 = NetAPIList.recruit_fleet_req.S,
  MSGTYPE574 = NetAPIList.ip_ntf.S,
  MSGTYPE1711 = NetAPIList.tlc_report_detail_req.S,
  MSGTYPE1107 = NetAPIList.wdc_fight_req.S,
  MSGTYPE557 = NetAPIList.contention_rank_req.S,
  MSGTYPE75 = NetAPIList.warpgate_collect_ack.S,
  MSGTYPE119 = NetAPIList.alliance_members_ack.S,
  MSGTYPE684 = NetAPIList.game_items_trans_ack.S,
  MSGTYPE659 = NetAPIList.all_gd_fleets_ack.S,
  MSGTYPE971 = NetAPIList.story_award_req.S,
  MSGTYPE108 = NetAPIList.user_collect_offline_expr_ack.S,
  MSGTYPE1386 = NetAPIList.medal_fleet_red_point_ntf.S,
  MSGTYPE1745 = NetAPIList.tlc_rank_board_ntf.S,
  MSGTYPE700 = NetAPIList.remodel_help_info_req.S,
  MSGTYPE38 = NetAPIList.techniques_upgrade_req.S,
  MSGTYPE591 = NetAPIList.contention_king_change_ntf.S,
  MSGTYPE892 = NetAPIList.daily_benefits_ntf.S,
  MSGTYPE317 = NetAPIList.laba_ntf.S,
  MSGTYPE253 = NetAPIList.cdtimes_clear_req.S,
  MSGTYPE1580 = NetAPIList.large_map_alliance_modify_notice_req.S,
  MSGTYPE1423 = NetAPIList.battle_rate_req.S,
  MSGTYPE363 = NetAPIList.level_loot_ack.S,
  MSGTYPE619 = NetAPIList.crusade_repair_req.S,
  MSGTYPE84 = NetAPIList.mail_set_read_req.S,
  MSGTYPE1213 = NetAPIList.art_core_equip_ack.S,
  MSGTYPE672 = NetAPIList.add_adjutant_req.S,
  MSGTYPE207 = NetAPIList.task_list_ack.S,
  MSGTYPE1135 = NetAPIList.enter_dice_ack.S,
  MSGTYPE324 = NetAPIList.top_force_list_req.S,
  MSGTYPE1836 = NetAPIList.gcs_point_update_ntf.S,
  MSGTYPE210 = NetAPIList.battle_fight_report_req.S,
  MSGTYPE681 = NetAPIList.adjutant_max_ntf.S,
  MSGTYPE132 = NetAPIList.alliance_apply_fail_ack.S,
  MSGTYPE603 = NetAPIList.login_token_ntf.S,
  MSGTYPE923 = NetAPIList.tactical_equip_on_ack.S,
  MSGTYPE448 = NetAPIList.alliance_defence_donate_req.S,
  MSGTYPE821 = NetAPIList.prime_enemy_info_ack.S,
  MSGTYPE937 = NetAPIList.tactical_equip_delete_req.S,
  MSGTYPE883 = NetAPIList.gp_test_pay_group_req.S,
  MSGTYPE675 = NetAPIList.month_card_list_ack.S,
  MSGTYPE145 = NetAPIList.krypton_gacha_one_req.S,
  MSGTYPE693 = NetAPIList.remodel_info_req.S,
  MSGTYPE323 = NetAPIList.chat_error_ntf.S,
  MSGTYPE1413 = NetAPIList.medal_glory_achievement_activate_ack.S,
  MSGTYPE12 = NetAPIList.fleets_req.S,
  MSGTYPE816 = NetAPIList.prime_site_info_ack.S,
  MSGTYPE592 = NetAPIList.award_list_req.S,
  MSGTYPE745 = NetAPIList.krypton_refine_list_req.S,
  MSGTYPE545 = NetAPIList.ladder_buy_search_ack.S,
  MSGTYPE1106 = NetAPIList.wdc_match_ack.S,
  MSGTYPE445 = NetAPIList.alliance_notifies_req.S,
  MSGTYPE1408 = NetAPIList.medal_glory_reset_req.S,
  MSGTYPE589 = NetAPIList.contention_king_info_req.S,
  MSGTYPE1542 = NetAPIList.large_map_alliance_exchange_req.S,
  MSGTYPE10002 = NetAPIList.udid_track_req.S,
  MSGTYPE1263 = NetAPIList.fte_req.S,
  MSGTYPE10001 = NetAPIList.udid_step_req.S,
  MSGTYPE902 = NetAPIList.ladder_jump_req.S,
  MSGTYPE314 = NetAPIList.update_guide_progress_req.S,
  MSGTYPE163 = NetAPIList.champion_challenge_ack.S,
  MSGTYPE1148 = NetAPIList.event_activity_req.S,
  MSGTYPE275 = NetAPIList.special_battle_req.S,
  MSGTYPE58 = NetAPIList.dismiss_fleet_req.S,
  MSGTYPE43 = NetAPIList.revenue_do_ack.S,
  MSGTYPE493 = NetAPIList.open_box_recent_ntf.S,
  MSGTYPE270 = NetAPIList.price_req.S,
  MSGTYPE344 = NetAPIList.block_devil_info_req.S,
  MSGTYPE106 = NetAPIList.user_offline_info_ack.S,
  MSGTYPE806 = NetAPIList.prime_my_info_ntf.S,
  MSGTYPE1414 = NetAPIList.medal_glory_collection_rank_req.S,
  MSGTYPE973 = NetAPIList.thumb_up_info_req.S,
  MSGTYPE1817 = NetAPIList.check_exp_add_item_req.S,
  MSGTYPE164 = NetAPIList.champion_top_req.S,
  MSGTYPE255 = NetAPIList.system_configuration_ack.S,
  MSGTYPE1514 = NetAPIList.large_map_exit_alliance_req.S,
  MSGTYPE805 = NetAPIList.prime_quit_req.S,
  MSGTYPE1329 = NetAPIList.buy_scratch_card_ack.S,
  MSGTYPE376 = NetAPIList.world_boss_info_ntf.S,
  MSGTYPE1757 = NetAPIList.tlc_krypton_sort_req.S,
  MSGTYPE618 = NetAPIList.crusade_cross_ack.S,
  MSGTYPE217 = NetAPIList.prestige_obtain_award_ack.S,
  MSGTYPE282 = NetAPIList.battle_event_status_ack.S,
  MSGTYPE47 = NetAPIList.officers_req.S,
  MSGTYPE546 = NetAPIList.ladder_buy_reset_req.S,
  MSGTYPE369 = NetAPIList.promotions_ntf.S,
  MSGTYPE1281 = NetAPIList.save_user_id_req.S,
  MSGTYPE1343 = NetAPIList.art_one_upgrade_req.S,
  MSGTYPE1185 = NetAPIList.mining_wars_ack.S,
  MSGTYPE539 = NetAPIList.ladder_raids_ack.S,
  MSGTYPE581 = NetAPIList.mulmatrix_blank_ntf.S,
  MSGTYPE1541 = NetAPIList.large_map_alliance_demotion_ack.S,
  MSGTYPE10 = NetAPIList.chat_ntf.S,
  MSGTYPE1178 = NetAPIList.mining_world_req.S,
  MSGTYPE1355 = NetAPIList.get_login_sign_award_ack.S,
  MSGTYPE995 = NetAPIList.switch_formation_req.S,
  MSGTYPE1174 = NetAPIList.red_point_ntf.S,
  MSGTYPE141 = NetAPIList.krypton_store_ack.S,
  MSGTYPE137 = NetAPIList.add_friend_ack.S,
  MSGTYPE158 = NetAPIList.alliance_memo_ack.S,
  MSGTYPE429 = NetAPIList.colony_exploit_ack.S,
  MSGTYPE265 = NetAPIList.bag_grid_ntf.S,
  MSGTYPE392 = NetAPIList.buy_cut_off_req.S,
  MSGTYPE588 = NetAPIList.ladder_reset_ack.S,
  MSGTYPE357 = NetAPIList.champion_rank_reward_req.S,
  MSGTYPE610 = NetAPIList.activity_dna_ack.S,
  MSGTYPE656 = NetAPIList.activity_rank_ack.S,
  MSGTYPE203 = NetAPIList.alliance_unapply_fail_ack.S,
  MSGTYPE243 = NetAPIList.passport_bind_ack.S,
  MSGTYPE1427 = NetAPIList.medal_glory_collection_enter_req.S,
  MSGTYPE858 = NetAPIList.url_push_ntf.S,
  MSGTYPE996 = NetAPIList.switch_formation_ack.S,
  MSGTYPE340 = NetAPIList.alliance_activities_req.S,
  MSGTYPE125 = NetAPIList.alliance_create_ack.S,
  MSGTYPE494 = NetAPIList.battle_rate_ntf.S,
  MSGTYPE889 = NetAPIList.activity_store_count_ack.S,
  MSGTYPE364 = NetAPIList.vip_loot_get_req.S,
  MSGTYPE412 = NetAPIList.verify_redeem_code_req.S,
  MSGTYPE1203 = NetAPIList.art_main_ack.S,
  MSGTYPE186 = NetAPIList.alliance_demote_req.S,
  MSGTYPE1128 = NetAPIList.translate_info_req.S,
  MSGTYPE1564 = NetAPIList.large_map_view_mass_player_req.S,
  MSGTYPE530 = NetAPIList.tango_invited_friends_ack.S,
  MSGTYPE427 = NetAPIList.colony_info_test_ack.S,
  MSGTYPE73 = NetAPIList.warpgate_charge_ack.S,
  MSGTYPE1393 = NetAPIList.medal_fleet_btn_red_point_ntf.S,
  MSGTYPE11 = NetAPIList.user_logout_req.S,
  MSGTYPE261 = NetAPIList.sign_activity_ack.S,
  MSGTYPE80 = NetAPIList.mail_content_req.S,
  MSGTYPE516 = NetAPIList.contention_occupier_ack.S,
  MSGTYPE993 = NetAPIList.other_monthly_ack.S,
  MSGTYPE687 = NetAPIList.budo_champion_report_req.S,
  MSGTYPE449 = NetAPIList.alliance_activity_times_ntf.S,
  MSGTYPE144 = NetAPIList.krypton_equip_req.S,
  MSGTYPE382 = NetAPIList.allstars_reward_req.S,
  MSGTYPE1003 = NetAPIList.user_bag_info_ntf.S,
  MSGTYPE927 = NetAPIList.tactical_refine_price_req.S,
  MSGTYPE520 = NetAPIList.contention_collect_ack.S,
  MSGTYPE541 = NetAPIList.ladder_fresh_req.S,
  MSGTYPE555 = NetAPIList.fleet_enhance_req.S,
  MSGTYPE1199 = NetAPIList.mining_battle_report_ack.S,
  MSGTYPE1129 = NetAPIList.translate_info_ack.S,
  MSGTYPE575 = NetAPIList.mail_goods_req.S,
  MSGTYPE1536 = NetAPIList.large_map_alliance_ratify_duty_data_ack.S,
  MSGTYPE1309 = NetAPIList.fair_arena_reward_req.S,
  MSGTYPE241 = NetAPIList.mail_unread_ntf.S,
  MSGTYPE452 = NetAPIList.common_ntf.S,
  MSGTYPE254 = NetAPIList.system_configuration_req.S,
  MSGTYPE1132 = NetAPIList.welcome_mask_req.S,
  MSGTYPE388 = NetAPIList.promotion_detail_req.S,
  MSGTYPE373 = NetAPIList.ready_world_boss_req.S,
  MSGTYPE202 = NetAPIList.krypton_fleet_move_req.S,
  MSGTYPE267 = NetAPIList.level_up_ntf.S,
  MSGTYPE524 = NetAPIList.contention_logbattle_detail_ack.S,
  MSGTYPE14 = NetAPIList.pve_battle_req.S,
  MSGTYPE663 = NetAPIList.stores_refresh_ack.S,
  MSGTYPE295 = NetAPIList.mine_digg_req.S,
  MSGTYPE1758 = NetAPIList.tlc_krypton_sort_ack.S,
  MSGTYPE604 = NetAPIList.dungeon_enter_ack.S,
  MSGTYPE189 = NetAPIList.alliance_promote_req.S,
  MSGTYPE863 = NetAPIList.player_pay_price_req.S,
  MSGTYPE102 = NetAPIList.shop_purchase_back_list_ack.S,
  MSGTYPE645 = NetAPIList.battle_report_ack.S,
  MSGTYPE1382 = NetAPIList.medal_rank_up_req.S,
  MSGTYPE343 = NetAPIList.alliance_weekend_award_ack.S,
  MSGTYPE1834 = NetAPIList.gcs_get_lv_all_rewards_req.S,
  MSGTYPE1561 = NetAPIList.large_map_mass_invite_do_req.S,
  MSGTYPE296 = NetAPIList.mine_atk_req.S,
  MSGTYPE547 = NetAPIList.ladder_buy_reset_ack.S,
  MSGTYPE128 = NetAPIList.alliance_delete_ack.S,
  MSGTYPE8 = NetAPIList.user_snapshot_ack.S,
  MSGTYPE154 = NetAPIList.alliance_ratify_ack.S,
  MSGTYPE999 = NetAPIList.mulmatrix_equip_info_req.S,
  MSGTYPE347 = NetAPIList.block_devil_complete_ntf.S,
  MSGTYPE679 = NetAPIList.fleet_dismiss_req.S,
  MSGTYPE313 = NetAPIList.supply_loot_get_req.S,
  MSGTYPE848 = NetAPIList.all_welfare_award_req.S,
  MSGTYPE525 = NetAPIList.activity_status_req.S,
  MSGTYPE1154 = NetAPIList.star_craft_data_ntf.S,
  MSGTYPE844 = NetAPIList.charge_open_server_fund_req.S,
  MSGTYPE1716 = NetAPIList.tlc_rank_board_req.S,
  MSGTYPE906 = NetAPIList.ladder_report_detail_req.S,
  MSGTYPE94 = NetAPIList.equipments_ack.S,
  MSGTYPE1176 = NetAPIList.area_id_ack.S,
  MSGTYPE1832 = NetAPIList.gcs_get_week_extra_reward_req.S,
  MSGTYPE104 = NetAPIList.shop_purchase_back_ack.S,
  MSGTYPE900 = NetAPIList.ladder_report_req.S,
  MSGTYPE531 = NetAPIList.tango_invite_req.S,
  MSGTYPE862 = NetAPIList.combo_guide_ack.S,
  MSGTYPE538 = NetAPIList.ladder_raids_req.S,
  MSGTYPE1308 = NetAPIList.fair_arena_season_ack.S,
  MSGTYPE1349 = NetAPIList.change_fleets_data_req.S,
  MSGTYPE1357 = NetAPIList.krypton_redpoint_ack.S,
  MSGTYPE571 = NetAPIList.contention_hit_ntf.S,
  MSGTYPE41 = NetAPIList.revenue_info_ack.S,
  MSGTYPE54 = NetAPIList.recruit_list_req.S,
  MSGTYPE561 = NetAPIList.contention_award_req.S,
  MSGTYPE288 = NetAPIList.adventure_cd_price_req.S,
  MSGTYPE150 = NetAPIList.krypton_decompose_req.S,
  MSGTYPE1722 = NetAPIList.tlc_awards_get_req.S,
  MSGTYPE22 = NetAPIList.user_map_status_ack.S,
  MSGTYPE409 = NetAPIList.thanksgiven_rank_ack.S,
  MSGTYPE633 = NetAPIList.budo_stage_ntf.S,
  MSGTYPE533 = NetAPIList.dungeon_open_ack.S,
  MSGTYPE354 = NetAPIList.champion_top_record_req.S,
  MSGTYPE250 = NetAPIList.supply_info_ack.S,
  MSGTYPE665 = NetAPIList.change_name_ntf.S,
  MSGTYPE183 = NetAPIList.alliance_transfer_req.S,
  MSGTYPE95 = NetAPIList.quark_exchange_req.S,
  MSGTYPE59 = NetAPIList.dismiss_fleet_ack.S,
  MSGTYPE321 = NetAPIList.dexter_libs_ntf.S,
  MSGTYPE1835 = NetAPIList.gcs_get_lv_all_rewards_ack.S,
  MSGTYPE191 = NetAPIList.alliance_promote_ack.S,
  MSGTYPE46 = NetAPIList.del_friend_req.S,
  MSGTYPE1823 = NetAPIList.gcs_task_data_ack.S,
  MSGTYPE509 = NetAPIList.activity_task_list_req.S,
  MSGTYPE278 = NetAPIList.quest_ntf.S,
  MSGTYPE286 = NetAPIList.adventure_rush_req.S,
  MSGTYPE714 = NetAPIList.financial_gifts_get_req.S,
  MSGTYPE492 = NetAPIList.open_box_refresh_ack.S,
  MSGTYPE1001 = NetAPIList.change_matrix_type_req.S,
  MSGTYPE1346 = NetAPIList.confirm_exchange_req.S,
  MSGTYPE612 = NetAPIList.akt_dna_next_stg_req.S,
  MSGTYPE1320 = NetAPIList.refresh_fleet_pool_req.S,
  MSGTYPE1318 = NetAPIList.update_formations_req.S,
  MSGTYPE26 = NetAPIList.battle_matrix_ack.S,
  MSGTYPE1294 = NetAPIList.group_fight_ntf.S,
  MSGTYPE229 = NetAPIList.fleet_kryptons_ntf.S,
  MSGTYPE260 = NetAPIList.sign_activity_req.S,
  MSGTYPE1370 = NetAPIList.medal_store_refresh_req.S,
  MSGTYPE262 = NetAPIList.sign_activity_reward_req.S,
  MSGTYPE168 = NetAPIList.champion_get_award_req.S,
  MSGTYPE1102 = NetAPIList.enter_champion_ack.S,
  MSGTYPE414 = NetAPIList.items_count_ntf.S,
  MSGTYPE305 = NetAPIList.mine_atk_ntf.S,
  MSGTYPE356 = NetAPIList.login_time_current_ntf.S,
  MSGTYPE952 = NetAPIList.can_enhance_req.S,
  MSGTYPE10003 = NetAPIList.protocal_version_req.S,
  MSGTYPE1282 = NetAPIList.user_id_status_ntf.S,
  MSGTYPE550 = NetAPIList.ladder_special_req.S,
  MSGTYPE377 = NetAPIList.challenge_world_boss_ack.S,
  MSGTYPE310 = NetAPIList.event_supply_ack.S,
  MSGTYPE1316 = NetAPIList.fair_arena_formation_all_req.S,
  MSGTYPE385 = NetAPIList.timelimit_hero_id_ack.S,
  MSGTYPE192 = NetAPIList.alliance_promote_fail_ack.S,
  MSGTYPE1590 = NetAPIList.large_map_dissolve_alliance_ack.S,
  MSGTYPE881 = NetAPIList.prime_jump_price_ack.S,
  MSGTYPE718 = NetAPIList.multi_acc_loot_req.S,
  MSGTYPE936 = NetAPIList.tactical_status_ntf.S,
  MSGTYPE1211 = NetAPIList.art_core_down_ack.S,
  MSGTYPE341 = NetAPIList.alliance_activities_ack.S,
  MSGTYPE596 = NetAPIList.award_count_ntf.S,
  MSGTYPE1513 = NetAPIList.large_map_join_alliance_ack.S,
  MSGTYPE688 = NetAPIList.budo_champion_report_ack.S,
  MSGTYPE276 = NetAPIList.special_battle_ack.S,
  MSGTYPE395 = NetAPIList.activities_button_status_ntf.S,
  MSGTYPE389 = NetAPIList.promotion_detail_ack.S,
  MSGTYPE1312 = NetAPIList.fair_arena_report_req.S,
  MSGTYPE148 = NetAPIList.krypton_gacha_some_ack.S,
  MSGTYPE491 = NetAPIList.open_box_refresh_req.S,
  MSGTYPE1400 = NetAPIList.fight_report_round_ntf.S,
  MSGTYPE836 = NetAPIList.vip_sign_item_use_award_ntf.S,
  MSGTYPE976 = NetAPIList.thumb_up_ack.S,
  MSGTYPE69 = NetAPIList.friend_ban_req.S,
  MSGTYPE1570 = NetAPIList.large_map_repair_ack.S,
  MSGTYPE1286 = NetAPIList.concentrate_req.S,
  MSGTYPE1727 = NetAPIList.tlc_status_ntf.S,
  MSGTYPE593 = NetAPIList.award_list_ack.S,
  MSGTYPE120 = NetAPIList.alliance_logs_req.S,
  MSGTYPE903 = NetAPIList.ladder_jump_ack.S,
  MSGTYPE446 = NetAPIList.alliance_resource_ntf.S,
  MSGTYPE25 = NetAPIList.battle_matrix_req.S,
  MSGTYPE1445 = NetAPIList.expedition_refresh_type_req.S,
  MSGTYPE338 = NetAPIList.notice_set_read_req.S,
  MSGTYPE185 = NetAPIList.alliance_transfer_fail_ack.S,
  MSGTYPE1399 = NetAPIList.medal_config_info_ack.S,
  MSGTYPE1330 = NetAPIList.reset_scratch_card_req.S,
  MSGTYPE1287 = NetAPIList.concentrate_ack.S,
  MSGTYPE1567 = NetAPIList.large_map_remove_mass_player_ntf.S,
  MSGTYPE1750 = NetAPIList.tlc_mulmatrix_switch_req.S,
  MSGTYPE118 = NetAPIList.alliance_members_req.S,
  MSGTYPE235 = NetAPIList.buildings_ntf.S,
  MSGTYPE882 = NetAPIList.select_goal_type_req.S,
  MSGTYPE651 = NetAPIList.get_budo_replay_ack.S,
  MSGTYPE473 = NetAPIList.alliance_domination_battle_ntf.S,
  MSGTYPE1578 = NetAPIList.large_map_alliance_diplomacy_ack.S,
  MSGTYPE5 = NetAPIList.building_upgrade_req.S,
  MSGTYPE876 = NetAPIList.get_goal_award_ack.S,
  MSGTYPE114 = NetAPIList.messages_ack.S,
  MSGTYPE813 = NetAPIList.prime_fight_history_ack.S,
  MSGTYPE1719 = NetAPIList.tlc_honor_wall_ack.S,
  MSGTYPE65 = NetAPIList.equipment_detail_ack.S,
  MSGTYPE30 = NetAPIList.bag_req.S,
  MSGTYPE841 = NetAPIList.equip_pushed_req.S,
  MSGTYPE1256 = NetAPIList.helper_buttons_req.S,
  MSGTYPE1720 = NetAPIList.tlc_enemy_release_req.S,
  MSGTYPE1215 = NetAPIList.art_reset_ack.S,
  MSGTYPE1358 = NetAPIList.event_click_req.S,
  MSGTYPE219 = NetAPIList.achievement_list_req.S,
  MSGTYPE1827 = NetAPIList.gcs_today_task_change_ack.S,
  MSGTYPE840 = NetAPIList.equip_push_ntf.S,
  MSGTYPE1255 = NetAPIList.space_door_ntf.S,
  MSGTYPE1172 = NetAPIList.hero_collect_rank_req.S,
  MSGTYPE930 = NetAPIList.tactical_refine_ack.S,
  MSGTYPE897 = NetAPIList.force_add_fleet_req.S,
  MSGTYPE460 = NetAPIList.award_receive_req.S,
  MSGTYPE1361 = NetAPIList.redownload_res_ack.S,
  MSGTYPE959 = NetAPIList.ltv_ajust_ntf.S,
  MSGTYPE466 = NetAPIList.alliance_flag_req.S,
  MSGTYPE1811 = NetAPIList.cumulate_recharge_ack.S,
  MSGTYPE708 = NetAPIList.update_passport_req.S,
  MSGTYPE834 = NetAPIList.champion_straight_ack.S,
  MSGTYPE1196 = NetAPIList.mining_world_battle_req.S,
  MSGTYPE1593 = NetAPIList.large_map_alliance_player_exit_ntf.S,
  MSGTYPE1116 = NetAPIList.wdc_rank_board_req.S,
  MSGTYPE1417 = NetAPIList.enter_medal_boss_ack.S,
  MSGTYPE110 = NetAPIList.fleet_repair_req.S,
  MSGTYPE329 = NetAPIList.ac_info_req.S,
  MSGTYPE1195 = NetAPIList.mining_wars_change_ntf.S,
  MSGTYPE1143 = NetAPIList.finish_speed_task_req.S,
  MSGTYPE515 = NetAPIList.contention_occupier_req.S,
  MSGTYPE239 = NetAPIList.heart_beat_req.S,
  MSGTYPE988 = NetAPIList.get_award_req.S,
  MSGTYPE577 = NetAPIList.mulmatrix_get_req.S,
  MSGTYPE1284 = NetAPIList.forces_req.S,
  MSGTYPE330 = NetAPIList.ac_ntf.S,
  MSGTYPE942 = NetAPIList.enter_plante_req.S,
  MSGTYPE833 = NetAPIList.champion_straight_req.S,
  MSGTYPE312 = NetAPIList.supply_loot_ack.S,
  MSGTYPE334 = NetAPIList.ac_jam_req.S,
  MSGTYPE99 = NetAPIList.shop_sell_req.S,
  MSGTYPE424 = NetAPIList.colony_challenge_req.S,
  MSGTYPE16 = NetAPIList.rush_result_ack.S,
  MSGTYPE1359 = NetAPIList.wxpay_info_ntf.S,
  MSGTYPE187 = NetAPIList.alliance_demote_ack.S,
  MSGTYPE635 = NetAPIList.budo_join_req.S,
  MSGTYPE490 = NetAPIList.open_box_open_ack.S,
  MSGTYPE122 = NetAPIList.alliance_aduits_req.S,
  MSGTYPE216 = NetAPIList.prestige_obtain_award_req.S,
  MSGTYPE989 = NetAPIList.get_award_ack.S,
  MSGTYPE1175 = NetAPIList.area_id_req.S,
  MSGTYPE1296 = NetAPIList.change_atk_seq_ack.S,
  MSGTYPE981 = NetAPIList.enter_monthly_ack.S,
  MSGTYPE4 = NetAPIList.user_login_ack.S,
  MSGTYPE991 = NetAPIList.get_today_award_ack.S,
  MSGTYPE339 = NetAPIList.gateway_clear_req.S,
  MSGTYPE256 = NetAPIList.pay_verify_req.S,
  MSGTYPE929 = NetAPIList.tactical_refine_req.S,
  MSGTYPE438 = NetAPIList.colony_action_purchase_req.S,
  MSGTYPE9 = NetAPIList.chat_req.S,
  MSGTYPE1402 = NetAPIList.medal_glory_collection_req.S,
  MSGTYPE1372 = NetAPIList.medal_list_req.S,
  MSGTYPE809 = NetAPIList.prime_march_ntf.S,
  MSGTYPE824 = NetAPIList.prime_dot_occupy_no_ntf.S,
  MSGTYPE585 = NetAPIList.equip_info_ack.S,
  MSGTYPE351 = NetAPIList.donate_info_ack.S,
  MSGTYPE331 = NetAPIList.ac_battle_req.S,
  MSGTYPE496 = NetAPIList.contention_map_req.S,
  MSGTYPE1157 = NetAPIList.adventure_add_req.S,
  MSGTYPE709 = NetAPIList.financial_req.S,
  MSGTYPE161 = NetAPIList.champion_list_ack.S,
  MSGTYPE1295 = NetAPIList.change_atk_seq_req.S,
  MSGTYPE733 = NetAPIList.send_global_email_req.S,
  MSGTYPE60 = NetAPIList.friends_req.S,
  MSGTYPE869 = NetAPIList.seven_day_ntf.S,
  MSGTYPE284 = NetAPIList.adventure_battle_req.S,
  MSGTYPE1291 = NetAPIList.force_join_ack.S,
  MSGTYPE370 = NetAPIList.promotions_req.S,
  MSGTYPE1216 = NetAPIList.art_core_decompose_req.S,
  MSGTYPE1279 = NetAPIList.get_reward_detail_req.S,
  MSGTYPE601 = NetAPIList.adventure_chapter_status_ntf.S,
  MSGTYPE1597 = NetAPIList.large_map_battle_ack.S,
  MSGTYPE86 = NetAPIList.fight_ack.S,
  MSGTYPE1258 = NetAPIList.event_log_ntf.S,
  MSGTYPE1756 = NetAPIList.change_tlc_leader_req.S,
  MSGTYPE722 = NetAPIList.push_button_info_req.S,
  MSGTYPE807 = NetAPIList.prime_dot_occupy_ntf.S,
  MSGTYPE327 = NetAPIList.ver_check_req.S,
  MSGTYPE582 = NetAPIList.mulmatrix_price_req.S,
  MSGTYPE566 = NetAPIList.pay_list2_req.S,
  MSGTYPE362 = NetAPIList.level_loot_req.S,
  MSGTYPE479 = NetAPIList.wd_award_ack.S,
  MSGTYPE565 = NetAPIList.orders_ntf.S,
  MSGTYPE535 = NetAPIList.ladder_enter_ack.S,
  MSGTYPE1411 = NetAPIList.medal_glory_achievement_ack.S,
  MSGTYPE101 = NetAPIList.shop_purchase_back_list_req.S,
  MSGTYPE1218 = NetAPIList.art_can_upgrade_req.S,
  MSGTYPE843 = NetAPIList.open_server_fund_ack.S,
  MSGTYPE482 = NetAPIList.wd_last_champion_ack.S,
  MSGTYPE857 = NetAPIList.sell_item_ntf.S,
  MSGTYPE397 = NetAPIList.activities_button_first_status_req.S,
  MSGTYPE134 = NetAPIList.alliance_unapply_ack.S,
  MSGTYPE474 = NetAPIList.alliance_domination_defence_ntf.S,
  MSGTYPE738 = NetAPIList.krypton_inject_ack.S,
  MSGTYPE710 = NetAPIList.financial_ack.S,
  MSGTYPE564 = NetAPIList.contention_history_ack.S,
  MSGTYPE1446 = NetAPIList.expedition_refresh_type_ack.S,
  MSGTYPE63 = NetAPIList.friend_detail_req.S,
  MSGTYPE381 = NetAPIList.add_world_boss_force_rate_ack.S,
  MSGTYPE205 = NetAPIList.user_challenged_ack.S,
  MSGTYPE723 = NetAPIList.push_button_info_ack.S,
  MSGTYPE517 = NetAPIList.contention_transfer_req.S,
  MSGTYPE629 = NetAPIList.contention_income_ack.S,
  MSGTYPE208 = NetAPIList.get_task_reward_req.S,
  MSGTYPE193 = NetAPIList.alliance_kick_req.S,
  MSGTYPE28 = NetAPIList.battle_result_ack.S,
  MSGTYPE484 = NetAPIList.wd_ranking_rank_req.S,
  MSGTYPE1141 = NetAPIList.speed_resource_req.S,
  MSGTYPE552 = NetAPIList.pay_animation_ntf.S,
  MSGTYPE925 = NetAPIList.tactical_equip_off_ack.S,
  MSGTYPE415 = NetAPIList.thanksgiven_champion_ntf.S,
  MSGTYPE1313 = NetAPIList.fair_arena_report_ack.S,
  MSGTYPE607 = NetAPIList.enchant_req.S,
  MSGTYPE875 = NetAPIList.buy_cutoff_item_req.S,
  MSGTYPE166 = NetAPIList.champion_reset_cd_req.S,
  MSGTYPE674 = NetAPIList.month_card_list_req.S,
  MSGTYPE1747 = NetAPIList.tlc_bag_ack.S,
  MSGTYPE136 = NetAPIList.alliance_info_ack.S,
  MSGTYPE311 = NetAPIList.supply_loot_req.S,
  MSGTYPE455 = NetAPIList.krypton_info_ack.S,
  MSGTYPE1752 = NetAPIList.tlc_quit_req.S,
  MSGTYPE416 = NetAPIList.item_use_award_ntf.S,
  MSGTYPE705 = NetAPIList.give_share_awards_req.S,
  MSGTYPE502 = NetAPIList.contention_starting_req.S,
  MSGTYPE112 = NetAPIList.friend_ban_del_req.S,
  MSGTYPE49 = NetAPIList.officer_buy_req.S,
  MSGTYPE1252 = NetAPIList.mask_modules_req.S,
  MSGTYPE1299 = NetAPIList.my_forces_ack.S,
  MSGTYPE231 = NetAPIList.level_ntf.S,
  MSGTYPE432 = NetAPIList.activity_stores_req.S,
  MSGTYPE365 = NetAPIList.level_loot_get_req.S,
  MSGTYPE1532 = NetAPIList.large_map_alliance_recruit_option_update_req.S,
  MSGTYPE483 = NetAPIList.device_info_req.S,
  MSGTYPE617 = NetAPIList.crusade_cross_req.S,
  MSGTYPE1170 = NetAPIList.hero_collect_req.S,
  MSGTYPE741 = NetAPIList.krypton_inject_buy_req.S,
  MSGTYPE325 = NetAPIList.top_force_list_ntf.S,
  MSGTYPE658 = NetAPIList.all_gd_fleets_req.S,
  MSGTYPE422 = NetAPIList.colony_users_req.S,
  MSGTYPE272 = NetAPIList.module_status_ntf.S,
  MSGTYPE159 = NetAPIList.alliance_memo_fail_ack.S,
  MSGTYPE1198 = NetAPIList.mining_battle_report_req.S,
  MSGTYPE1504 = NetAPIList.large_map_base_req.S,
  MSGTYPE1528 = NetAPIList.large_map_alliance_invite_req.S,
  MSGTYPE182 = NetAPIList.alliance_quit_fail_ack.S,
  MSGTYPE1595 = NetAPIList.large_map_alliance_promotion_ack.S,
  MSGTYPE204 = NetAPIList.alliance_info_ntf.S,
  MSGTYPE151 = NetAPIList.krypton_decompose_ack.S,
  MSGTYPE1337 = NetAPIList.end_scratch_ack.S,
  MSGTYPE153 = NetAPIList.alliance_ratify_req.S,
  MSGTYPE98 = NetAPIList.shop_purchase_ack.S,
  MSGTYPE1334 = NetAPIList.exchange_award_req.S,
  MSGTYPE568 = NetAPIList.pay_verify2_req.S,
  MSGTYPE1253 = NetAPIList.mask_show_index_req.S,
  MSGTYPE1301 = NetAPIList.mining_team_log_ack.S,
  MSGTYPE248 = NetAPIList.revenue_exchange_req.S,
  MSGTYPE511 = NetAPIList.activity_task_reward_req.S,
  MSGTYPE904 = NetAPIList.ladder_award_req.S,
  MSGTYPE1163 = NetAPIList.recruit_ack.S,
  MSGTYPE634 = NetAPIList.budo_sign_up_ntf.S,
  MSGTYPE320 = NetAPIList.laba_award_req.S,
  MSGTYPE888 = NetAPIList.activity_store_count_req.S,
  MSGTYPE1818 = NetAPIList.check_exp_add_item_ack.S,
  MSGTYPE1543 = NetAPIList.large_map_alliance_exchange_ack.S,
  MSGTYPE19 = NetAPIList.user_cdtime_ack.S,
  MSGTYPE481 = NetAPIList.wd_last_champion_req.S,
  MSGTYPE742 = NetAPIList.krypton_inject_buy_ack.S,
  MSGTYPE600 = NetAPIList.contention_leave_req.S,
  MSGTYPE1529 = NetAPIList.large_map_alliance_invite_ack.S,
  MSGTYPE228 = NetAPIList.cd_time_ntf.S,
  MSGTYPE194 = NetAPIList.alliance_kick_ack.S,
  MSGTYPE368 = NetAPIList.krypton_energy_req.S,
  MSGTYPE558 = NetAPIList.contention_rank_ack.S,
  MSGTYPE835 = NetAPIList.champion_straight_award_req.S,
  MSGTYPE79 = NetAPIList.mail_page_ack.S,
  MSGTYPE734 = NetAPIList.collect_energy_req.S,
  MSGTYPE701 = NetAPIList.remodel_help_info_ack.S,
  MSGTYPE626 = NetAPIList.akt_dna_get_award_req.S,
  MSGTYPE668 = NetAPIList.laba_type_ntf.S,
  MSGTYPE20 = NetAPIList.user_cdtime_req.S,
  MSGTYPE1165 = NetAPIList.recruit_look_ack.S,
  MSGTYPE66 = NetAPIList.friend_detail_ack.S,
  MSGTYPE76 = NetAPIList.warpgate_upgrade_req.S,
  MSGTYPE885 = NetAPIList.store_quick_buy_count_ack.S,
  MSGTYPE67 = NetAPIList.friend_search_req.S,
  MSGTYPE1831 = NetAPIList.gcs_get_week_task_reward_ack.S,
  MSGTYPE27 = NetAPIList.user_matrix_save_req.S,
  MSGTYPE1142 = NetAPIList.speed_resource_ack.S,
  MSGTYPE1723 = NetAPIList.tlc_awards_get_ack.S,
  MSGTYPE1815 = NetAPIList.login_fund_ack.S,
  MSGTYPE1002 = NetAPIList.change_matrix_type_ack.S,
  MSGTYPE523 = NetAPIList.contention_logbattle_detail_req.S,
  MSGTYPE908 = NetAPIList.stores_buy_ack.S,
  MSGTYPE1005 = NetAPIList.bugly_data_ntf.S,
  MSGTYPE1505 = NetAPIList.large_map_base_ack.S,
  MSGTYPE1126 = NetAPIList.wdc_fight_status_ack.S,
  MSGTYPE1837 = NetAPIList.gcs_buy_update_ntf.S,
  MSGTYPE1833 = NetAPIList.gcs_get_week_extra_reward_ack.S,
  MSGTYPE451 = NetAPIList.fleet_info_ack.S,
  MSGTYPE985 = NetAPIList.view_award_ack.S,
  MSGTYPE74 = NetAPIList.warpgate_collect_req.S,
  MSGTYPE471 = NetAPIList.domination_ranks_req.S,
  MSGTYPE1828 = NetAPIList.gcs_get_today_task_reward_req.S,
  MSGTYPE1115 = NetAPIList.wdc_awards_ack.S,
  MSGTYPE1501 = NetAPIList.large_map_my_info_ntf.S,
  MSGTYPE1327 = NetAPIList.enter_scratch_ack.S,
  MSGTYPE616 = NetAPIList.crusade_fight_ack.S,
  MSGTYPE140 = NetAPIList.krypton_store_req.S,
  MSGTYPE1825 = NetAPIList.gcs_awards_data_ack.S,
  MSGTYPE1824 = NetAPIList.gcs_awards_data_req.S,
  MSGTYPE1125 = NetAPIList.wdc_fight_status_req.S,
  MSGTYPE874 = NetAPIList.cutoff_info_ack.S,
  MSGTYPE293 = NetAPIList.mine_list_req.S,
  MSGTYPE1819 = NetAPIList.exp_add_item_ntf.S,
  MSGTYPE921 = NetAPIList.enter_tactical_ack.S,
  MSGTYPE1544 = NetAPIList.large_map_alliance_expel_req.S,
  MSGTYPE611 = NetAPIList.activity_dna_charge_req.S,
  MSGTYPE884 = NetAPIList.store_quick_buy_count_req.S,
  MSGTYPE1814 = NetAPIList.login_fund_req.S,
  MSGTYPE827 = NetAPIList.prime_round_result_ntf.S,
  MSGTYPE1385 = NetAPIList.medal_fleet_formation_ntf.S,
  MSGTYPE437 = NetAPIList.colony_release_ack.S,
  MSGTYPE992 = NetAPIList.other_monthly_req.S,
  MSGTYPE1336 = NetAPIList.end_scratch_req.S,
  MSGTYPE698 = NetAPIList.remodel_help_others_req.S,
  MSGTYPE410 = NetAPIList.festival_req.S,
  MSGTYPE655 = NetAPIList.activity_rank_req.S,
  MSGTYPE682 = NetAPIList.max_level_ntf.S,
  MSGTYPE726 = NetAPIList.invite_friends_req.S,
  MSGTYPE802 = NetAPIList.prime_active_req.S,
  MSGTYPE660 = NetAPIList.fleet_fight_report_req.S,
  MSGTYPE676 = NetAPIList.month_card_buy_req.S,
  MSGTYPE1801 = NetAPIList.teamleague_matrix_req.S,
  MSGTYPE1755 = NetAPIList.simple_mulmatrix_get_ack.S,
  MSGTYPE1183 = NetAPIList.mining_world_pe_ack.S,
  MSGTYPE1754 = NetAPIList.simple_mulmatrix_get_req.S,
  MSGTYPE1749 = NetAPIList.tlc_krypton_store_ack.S,
  MSGTYPE1748 = NetAPIList.tlc_krypton_store_req.S,
  MSGTYPE746 = NetAPIList.krypton_refine_list_ack.S,
  MSGTYPE146 = NetAPIList.krypton_gacha_one_ack.S,
  MSGTYPE35 = NetAPIList.techniques_ack.S,
  MSGTYPE1746 = NetAPIList.tlc_bag_req.S,
  MSGTYPE947 = NetAPIList.plante_refresh_ack.S,
  MSGTYPE650 = NetAPIList.get_budo_replay_req.S,
  MSGTYPE1729 = NetAPIList.champion_cdtime_req.S,
  MSGTYPE907 = NetAPIList.ladder_report_detail_ack.S,
  MSGTYPE1726 = NetAPIList.tlc_fight_status_ack.S,
  MSGTYPE1725 = NetAPIList.tlc_fight_status_req.S,
  MSGTYPE812 = NetAPIList.prime_fight_history_req.S,
  MSGTYPE1546 = NetAPIList.large_map_block_detail_req.S,
  MSGTYPE226 = NetAPIList.fleets_ntf.S,
  MSGTYPE1717 = NetAPIList.tlc_rank_board_ack.S,
  MSGTYPE1418 = NetAPIList.reset_medal_boss_req.S,
  MSGTYPE1387 = NetAPIList.medal_equip_on_req.S,
  MSGTYPE1201 = NetAPIList.version_info_req.S,
  MSGTYPE1712 = NetAPIList.tlc_report_detail_ack.S,
  MSGTYPE453 = NetAPIList.warn_req.S,
  MSGTYPE436 = NetAPIList.colony_release_req.S,
  MSGTYPE657 = NetAPIList.support_one_player_ack.S,
  MSGTYPE379 = NetAPIList.add_world_boss_force_rate_req.S,
  MSGTYPE1710 = NetAPIList.tlc_report_ack.S,
  MSGTYPE1000 = NetAPIList.mulmatrix_equip_info_ack.S,
  MSGTYPE605 = NetAPIList.change_auto_decompose_req.S,
  MSGTYPE378 = NetAPIList.ready_world_boss_ack.S,
  MSGTYPE1709 = NetAPIList.tlc_report_req.S,
  MSGTYPE1708 = NetAPIList.tlc_fight_ack.S,
  MSGTYPE90 = NetAPIList.equipment_evolution_req.S,
  MSGTYPE83 = NetAPIList.mail_delete_req.S,
  MSGTYPE1371 = NetAPIList.medal_store_refresh_ack.S,
  MSGTYPE1510 = NetAPIList.large_map_event_ntf.S,
  MSGTYPE1802 = NetAPIList.teamleague_matrix_ack.S,
  MSGTYPE1577 = NetAPIList.large_map_alliance_diplomacy_req.S,
  MSGTYPE13 = NetAPIList.fleets_ack.S,
  MSGTYPE36 = NetAPIList.gm_cmd_req.S,
  MSGTYPE1702 = NetAPIList.tlc_enter_ack.S,
  MSGTYPE853 = NetAPIList.prime_do_wve_boss_left_ntf.S,
  MSGTYPE478 = NetAPIList.wd_award_req.S,
  MSGTYPE1210 = NetAPIList.art_core_down_req.S,
  MSGTYPE81 = NetAPIList.mail_content_ack.S,
  MSGTYPE1602 = NetAPIList.large_map_ship_arrive_req.S,
  MSGTYPE1601 = NetAPIList.large_map_small_map_ack.S,
  MSGTYPE238 = NetAPIList.fleet_matrix_ntf.S,
  MSGTYPE529 = NetAPIList.tango_invited_friends_req.S,
  MSGTYPE928 = NetAPIList.tactical_refine_price_ack.S,
  MSGTYPE586 = NetAPIList.mycard_error_ntf.S,
  MSGTYPE1598 = NetAPIList.large_map_vision_ntf.S,
  MSGTYPE576 = NetAPIList.mail_goods_ack.S,
  MSGTYPE1323 = NetAPIList.fair_arena_fleet_info_ack.S,
  MSGTYPE55 = NetAPIList.recruit_list_ack.S,
  MSGTYPE859 = NetAPIList.enhance_info_req.S,
  MSGTYPE677 = NetAPIList.month_card_use_req.S,
  MSGTYPE211 = NetAPIList.battle_fight_report_ack.S,
  MSGTYPE1519 = NetAPIList.large_map_create_alliance_ack.S,
  MSGTYPE1594 = NetAPIList.large_map_alliance_promotion_req.S,
  MSGTYPE685 = NetAPIList.budo_champion_req.S,
  MSGTYPE259 = NetAPIList.use_item_req.S,
  MSGTYPE1591 = NetAPIList.large_map_rank_req.S,
  MSGTYPE1589 = NetAPIList.large_map_dissolve_alliance_req.S,
  MSGTYPE825 = NetAPIList.prime_award_req.S,
  MSGTYPE1319 = NetAPIList.update_formations_ack.S,
  MSGTYPE1278 = NetAPIList.leader_info_ntf.S,
  MSGTYPE1587 = NetAPIList.large_map_search_alliance_req.S,
  MSGTYPE1401 = NetAPIList.medal_glory_collection_ntf.S,
  MSGTYPE123 = NetAPIList.alliance_aduits_ack.S,
  MSGTYPE818 = NetAPIList.prime_increase_power_info_req.S,
  MSGTYPE1117 = NetAPIList.wdc_rank_board_ack.S,
  MSGTYPE1259 = NetAPIList.choosable_item_req.S,
  MSGTYPE638 = NetAPIList.budo_change_formation_req.S,
  MSGTYPE97 = NetAPIList.shop_purchase_req.S,
  MSGTYPE951 = NetAPIList.facebook_switch_ntf.S,
  MSGTYPE1581 = NetAPIList.large_map_alliance_modify_notice_ack.S,
  MSGTYPE590 = NetAPIList.contention_king_info_ack.S,
  MSGTYPE129 = NetAPIList.alliance_delete_fail_ack.S,
  MSGTYPE1566 = NetAPIList.large_map_remove_mass_player_req.S,
  MSGTYPE1579 = NetAPIList.large_map_alliance_diplomacy_exchange_req.S,
  MSGTYPE1305 = NetAPIList.fair_arena_rank_req.S,
  MSGTYPE1575 = NetAPIList.large_map_local_leader_req.S,
  MSGTYPE595 = NetAPIList.award_get_all_req.S,
  MSGTYPE1574 = NetAPIList.large_map_buy_energy_req.S,
  MSGTYPE731 = NetAPIList.friend_role_req.S,
  MSGTYPE1573 = NetAPIList.large_map_add_mass_player_ntf.S,
  MSGTYPE1572 = NetAPIList.large_map_repair_ship_ack.S,
  MSGTYPE1571 = NetAPIList.large_map_repair_ship_req.S,
  MSGTYPE318 = NetAPIList.laba_rate_reroll_req.S,
  MSGTYPE1565 = NetAPIList.large_map_view_mass_player_ack.S,
  MSGTYPE1563 = NetAPIList.large_map_start_mass_war_req.S,
  MSGTYPE1562 = NetAPIList.large_map_exit_mass_troop_req.S,
  MSGTYPE234 = NetAPIList.progress_ntf.S,
  MSGTYPE1438 = NetAPIList.expedition_wormhole_rank_self_ntf.S,
  MSGTYPE1560 = NetAPIList.large_map_mass_invite_ntf.S,
  MSGTYPE15 = NetAPIList.user_rush_req.S,
  MSGTYPE920 = NetAPIList.enter_tactical_req.S,
  MSGTYPE1341 = NetAPIList.art_one_upgrade_data_req.S,
  MSGTYPE375 = NetAPIList.exit_world_boss_req.S,
  MSGTYPE408 = NetAPIList.thanksgiven_rank_req.S,
  MSGTYPE48 = NetAPIList.officers_ack.S,
  MSGTYPE1557 = NetAPIList.large_map_mass_near_invite_ack.S,
  MSGTYPE1556 = NetAPIList.large_map_mass_near_invite_req.S,
  MSGTYPE24 = NetAPIList.battle_status_ack.S,
  MSGTYPE227 = NetAPIList.resource_ntf.S,
  MSGTYPE1548 = NetAPIList.large_map_create_mass_troop_req.S,
  MSGTYPE3 = NetAPIList.user_login_req.S,
  MSGTYPE1555 = NetAPIList.large_map_dissolve_mass_troop_req.S,
  MSGTYPE359 = NetAPIList.first_purchase_ntf.S,
  MSGTYPE1554 = NetAPIList.large_map_mass_troops_info_ack.S,
  MSGTYPE269 = NetAPIList.pve_awards_get_ack.S,
  MSGTYPE488 = NetAPIList.open_box_info_ack.S,
  MSGTYPE1553 = NetAPIList.large_map_mass_troops_info_req.S,
  MSGTYPE1551 = NetAPIList.large_map_join_mass_req.S,
  MSGTYPE1533 = NetAPIList.large_map_alliance_ratify_join_data_req.S,
  MSGTYPE285 = NetAPIList.adventure_cd_clear_req.S,
  MSGTYPE1550 = NetAPIList.large_map_mass_troops_ack.S,
  MSGTYPE1549 = NetAPIList.large_map_mass_troops_req.S,
  MSGTYPE1547 = NetAPIList.large_map_block_detail_ack.S,
  MSGTYPE390 = NetAPIList.cut_off_list_req.S,
  MSGTYPE342 = NetAPIList.alliance_weekend_award_req.S,
  MSGTYPE31 = NetAPIList.bag_ack.S,
  MSGTYPE954 = NetAPIList.tactical_delete_awards_req.S,
  MSGTYPE1540 = NetAPIList.large_map_alliance_demotion_req.S,
  MSGTYPE868 = NetAPIList.enter_seven_day_req.S,
  MSGTYPE886 = NetAPIList.store_quick_buy_price_req.S,
  MSGTYPE1419 = NetAPIList.reset_medal_boss_ack.S,
  MSGTYPE485 = NetAPIList.wd_ranking_rank_ack.S,
  MSGTYPE1538 = NetAPIList.large_map_alliance_ratify_handle_ack.S,
  MSGTYPE1155 = NetAPIList.star_craft_sign_up_req.S,
  MSGTYPE1537 = NetAPIList.large_map_alliance_ratify_handle_req.S,
  MSGTYPE1289 = NetAPIList.concentrate_end_ack.S,
  MSGTYPE1535 = NetAPIList.large_map_alliance_ratify_duty_data_req.S,
  MSGTYPE1534 = NetAPIList.large_map_alliance_ratify_join_data_ack.S,
  MSGTYPE861 = NetAPIList.combo_guide_req.S,
  MSGTYPE1530 = NetAPIList.large_map_alliance_recruit_option_req.S,
  MSGTYPE245 = NetAPIList.act_status_req.S,
  MSGTYPE279 = NetAPIList.quest_loot_req.S,
  MSGTYPE497 = NetAPIList.contention_map_ack.S,
  MSGTYPE1525 = NetAPIList.large_map_alliance_invite_data_ack.S,
  MSGTYPE1524 = NetAPIList.large_map_alliance_invite_data_req.S,
  MSGTYPE367 = NetAPIList.active_gifts_req.S,
  MSGTYPE537 = NetAPIList.ladder_search_ack.S,
  MSGTYPE1415 = NetAPIList.medal_glory_collection_rank_ack.S,
  MSGTYPE394 = NetAPIList.new_activities_compare_req.S,
  MSGTYPE1522 = NetAPIList.large_map_alliance_army_req.S,
  MSGTYPE1521 = NetAPIList.large_map_alliance_affais_ack.S,
  MSGTYPE730 = NetAPIList.treasure_info_req.S,
  MSGTYPE814 = NetAPIList.prime_increase_power_req.S,
  MSGTYPE1520 = NetAPIList.large_map_alliance_affais_req.S,
  MSGTYPE696 = NetAPIList.remodel_levelup_req.S,
  MSGTYPE1517 = NetAPIList.large_map_near_alliance_ack.S,
  MSGTYPE21 = NetAPIList.pve_map_status_req.S,
  MSGTYPE1509 = NetAPIList.large_map_route_ntf.S,
  MSGTYPE1273 = NetAPIList.month_card_req.S,
  MSGTYPE646 = NetAPIList.get_my_support_req.S,
  MSGTYPE1515 = NetAPIList.large_map_exit_alliance_ack.S,
  MSGTYPE598 = NetAPIList.contention_mission_info_ack.S,
  MSGTYPE501 = NetAPIList.contention_station_ntf.S,
  MSGTYPE1202 = NetAPIList.art_main_req.S,
  MSGTYPE1342 = NetAPIList.art_one_upgrade_data_ack.S,
  MSGTYPE1713 = NetAPIList.tlc_info_ntf.S,
  MSGTYPE1706 = NetAPIList.tlc_match_ack.S,
  MSGTYPE472 = NetAPIList.domination_ranks_ack.S,
  MSGTYPE252 = NetAPIList.connection_close_req.S,
  MSGTYPE360 = NetAPIList.vip_loot_req.S,
  MSGTYPE1516 = NetAPIList.large_map_near_alliance_req.S,
  MSGTYPE1508 = NetAPIList.large_map_route_ack.S,
  MSGTYPE1439 = NetAPIList.expedition_task_update_ntf.S,
  MSGTYPE1506 = NetAPIList.large_map_block_update_ntf.S,
  MSGTYPE1503 = NetAPIList.large_map_block_ack.S,
  MSGTYPE1502 = NetAPIList.large_map_block_req.S,
  MSGTYPE435 = NetAPIList.yys_ntf.S,
  MSGTYPE1347 = NetAPIList.credit_exchange_ntf.S,
  MSGTYPE1444 = NetAPIList.expedition_stores_refresh_ack.S,
  MSGTYPE887 = NetAPIList.store_quick_buy_price_ack.S,
  MSGTYPE926 = NetAPIList.tactical_resource_ntf.S,
  MSGTYPE1156 = NetAPIList.event_ntf_req.S,
  MSGTYPE924 = NetAPIList.tactical_equip_off_req.S,
  MSGTYPE692 = NetAPIList.version_code_update_req.S,
  MSGTYPE1443 = NetAPIList.expedition_stores_refresh_req.S,
  MSGTYPE214 = NetAPIList.prestige_info_req.S,
  MSGTYPE1441 = NetAPIList.expedition_rank_awards_req.S,
  MSGTYPE1269 = NetAPIList.pay_gifts_ack.S,
  MSGTYPE536 = NetAPIList.ladder_search_req.S,
  MSGTYPE470 = NetAPIList.domination_awards_list_ack.S,
  MSGTYPE1368 = NetAPIList.medal_store_buy_req.S,
  MSGTYPE1440 = NetAPIList.expedition_top3_update_ntf.S,
  MSGTYPE828 = NetAPIList.krypton_sort_req.S,
  MSGTYPE1373 = NetAPIList.medal_list_ack.S,
  MSGTYPE251 = NetAPIList.supply_exchange_req.S,
  MSGTYPE1507 = NetAPIList.large_map_route_req.S,
  MSGTYPE946 = NetAPIList.plante_refresh_req.S,
  MSGTYPE1436 = NetAPIList.expedition_wormhole_rank_board_req.S,
  MSGTYPE974 = NetAPIList.thumb_up_info_ack.S,
  MSGTYPE528 = NetAPIList.contention_jump_ntf.S,
  MSGTYPE1434 = NetAPIList.expedition_wormhole_status_ack.S,
  MSGTYPE1433 = NetAPIList.expedition_wormhole_status_req.S,
  MSGTYPE1707 = NetAPIList.tlc_fight_req.S,
  MSGTYPE1431 = NetAPIList.fps_switch_ntf.S,
  MSGTYPE1429 = NetAPIList.user_statistics_req.S,
  MSGTYPE430 = NetAPIList.colony_fawn_req.S,
  MSGTYPE70 = NetAPIList.warpgate_status_req.S,
  MSGTYPE1428 = NetAPIList.medal_glory_collection_enter_ack.S,
  MSGTYPE854 = NetAPIList.prime_time_wve_boss_left_ntf.S,
  MSGTYPE986 = NetAPIList.redo_task_req.S,
  MSGTYPE1426 = NetAPIList.medal_glory_collection_total_attr_ack.S,
  MSGTYPE1339 = NetAPIList.uc_pay_sign_ack.S,
  MSGTYPE82 = NetAPIList.mail_send_req.S,
  MSGTYPE1219 = NetAPIList.art_can_upgrade_ack.S,
  MSGTYPE648 = NetAPIList.get_budo_rank_req.S,
  MSGTYPE1424 = NetAPIList.hero_union_unlock_ntf.S,
  MSGTYPE1123 = NetAPIList.wdc_awards_get_ack.S,
  MSGTYPE704 = NetAPIList.give_friends_gift_req.S,
  MSGTYPE1421 = NetAPIList.challenge_medal_boss_ack.S,
  MSGTYPE801 = NetAPIList.prime_map_ack.S,
  MSGTYPE1539 = NetAPIList.large_map_alliance_apply_duty_req.S,
  MSGTYPE1206 = NetAPIList.art_point_upgrade_req.S,
  MSGTYPE77 = NetAPIList.warpgate_upgrade_ack.S,
  MSGTYPE1715 = NetAPIList.tlc_awards_ack.S,
  MSGTYPE1409 = NetAPIList.medal_glory_reset_ack.S,
  MSGTYPE521 = NetAPIList.contention_logs_req.S,
  MSGTYPE1380 = NetAPIList.medal_level_up_req.S,
  MSGTYPE931 = NetAPIList.tactical_refine_save_req.S,
  MSGTYPE522 = NetAPIList.contention_logs_ack.S,
  MSGTYPE1523 = NetAPIList.large_map_alliance_army_ack.S,
  MSGTYPE1410 = NetAPIList.medal_glory_achievement_req.S,
  MSGTYPE1416 = NetAPIList.enter_medal_boss_req.S,
  MSGTYPE1104 = NetAPIList.wdc_enter_ack.S,
  MSGTYPE181 = NetAPIList.alliance_quit_ack.S,
  MSGTYPE124 = NetAPIList.alliance_create_req.S,
  MSGTYPE1407 = NetAPIList.medal_glory_rank_up_ack.S,
  MSGTYPE126 = NetAPIList.alliance_create_fail_ack.S,
  MSGTYPE1406 = NetAPIList.medal_glory_rank_up_req.S,
  MSGTYPE1701 = NetAPIList.tlc_enter_req.S,
  MSGTYPE348 = NetAPIList.donate_req.S,
  MSGTYPE846 = NetAPIList.all_welfare_req.S,
  MSGTYPE1404 = NetAPIList.medal_glory_activate_req.S,
  MSGTYPE165 = NetAPIList.champion_top_ack.S,
  MSGTYPE1403 = NetAPIList.medal_glory_collection_ack.S,
  MSGTYPE661 = NetAPIList.fleet_fight_report_ack.S,
  MSGTYPE979 = NetAPIList.today_act_task_ack.S,
  MSGTYPE374 = NetAPIList.challenge_world_boss_req.S,
  MSGTYPE93 = NetAPIList.equipments_req.S,
  MSGTYPE1586 = NetAPIList.large_map_log_battle_req.S,
  MSGTYPE732 = NetAPIList.friend_role_ack.S,
  MSGTYPE1315 = NetAPIList.fair_arena_report_battle_ack.S,
  MSGTYPE218 = NetAPIList.prestige_complete_ntf.S,
  MSGTYPE1181 = NetAPIList.mining_world_log_ack.S,
  MSGTYPE1392 = NetAPIList.medal_equip_replace_ack.S,
  MSGTYPE1391 = NetAPIList.medal_equip_replace_req.S,
  MSGTYPE1390 = NetAPIList.medal_equip_off_ack.S,
  MSGTYPE815 = NetAPIList.prime_site_info_req.S,
  MSGTYPE983 = NetAPIList.view_each_task_ack.S,
  MSGTYPE1177 = NetAPIList.master_ntf.S,
  MSGTYPE717 = NetAPIList.multi_acc_ack.S,
  MSGTYPE1389 = NetAPIList.medal_equip_off_req.S,
  MSGTYPE1714 = NetAPIList.tlc_awards_req.S,
  MSGTYPE1813 = NetAPIList.cumulate_recharge_award_get_ack.S,
  MSGTYPE1340 = NetAPIList.ac_sweep_req.S,
  MSGTYPE1384 = NetAPIList.medal_list_ntf.S,
  MSGTYPE567 = NetAPIList.pay_list2_ack.S,
  MSGTYPE1379 = NetAPIList.medal_equip_ack.S,
  MSGTYPE938 = NetAPIList.tactical_equip_delete_ack.S,
  MSGTYPE1322 = NetAPIList.fair_arena_fleet_info_req.S,
  MSGTYPE1344 = NetAPIList.art_one_upgrade_ack.S,
  MSGTYPE37 = NetAPIList.pack_bag_req.S,
  MSGTYPE1378 = NetAPIList.medal_equip_req.S,
  MSGTYPE1377 = NetAPIList.medal_reset_confirm_ack.S,
  MSGTYPE1376 = NetAPIList.medal_reset_confirm_req.S,
  MSGTYPE940 = NetAPIList.pay_sign_board_ntf.S,
  MSGTYPE678 = NetAPIList.chat_channel_switch_ntf.S,
  MSGTYPE1600 = NetAPIList.large_map_small_map_req.S,
  MSGTYPE1432 = NetAPIList.fleet_can_enhance_req.S,
  MSGTYPE1369 = NetAPIList.medal_store_buy_ack.S,
  MSGTYPE1140 = NetAPIList.reset_dice_ack.S,
  MSGTYPE1307 = NetAPIList.fair_arena_season_req.S,
  MSGTYPE1584 = NetAPIList.large_map_log_req.S,
  MSGTYPE715 = NetAPIList.financial_amount_ntf.S,
  MSGTYPE1205 = NetAPIList.art_point_ack.S,
  MSGTYPE642 = NetAPIList.budo_promotion_ntf.S,
  MSGTYPE711 = NetAPIList.financial_upgrade_req.S,
  MSGTYPE206 = NetAPIList.task_list_req.S,
  MSGTYPE639 = NetAPIList.budo_change_formation_ack.S,
  MSGTYPE1366 = NetAPIList.enter_medal_store_req.S,
  MSGTYPE264 = NetAPIList.server_kick_user_ntf.S,
  MSGTYPE719 = NetAPIList.facebook_friends_ntf.S,
  MSGTYPE970 = NetAPIList.story_status_ntf.S,
  MSGTYPE1363 = NetAPIList.exchange_regulation_ntf.S,
  MSGTYPE162 = NetAPIList.champion_challenge_req.S,
  MSGTYPE236 = NetAPIList.offline_changed_ntf.S,
  MSGTYPE1360 = NetAPIList.redownload_res_req.S,
  MSGTYPE1345 = NetAPIList.scratch_board_ntf.S,
  MSGTYPE1290 = NetAPIList.force_join_req.S,
  MSGTYPE1118 = NetAPIList.wdc_honor_wall_req.S,
  MSGTYPE866 = NetAPIList.daily_benefits_ack.S,
  MSGTYPE149 = NetAPIList.krypton_equip_off_req.S,
  MSGTYPE1354 = NetAPIList.get_login_sign_award_req.S,
  MSGTYPE1353 = NetAPIList.enter_login_sign_ack.S,
  MSGTYPE1352 = NetAPIList.enter_login_sign_req.S,
  MSGTYPE1351 = NetAPIList.change_fleets_req.S,
  MSGTYPE1350 = NetAPIList.change_fleets_data_ack.S,
  MSGTYPE1512 = NetAPIList.large_map_join_alliance_req.S,
  MSGTYPE1558 = NetAPIList.large_map_mass_invite_req.S,
  MSGTYPE1105 = NetAPIList.wdc_match_req.S,
  MSGTYPE1338 = NetAPIList.uc_pay_sign_req.S,
  MSGTYPE1810 = NetAPIList.cumulate_recharge_req.S,
  MSGTYPE1127 = NetAPIList.wdc_status_ntf.S,
  MSGTYPE1335 = NetAPIList.exchange_award_ack.S,
  MSGTYPE1333 = NetAPIList.get_process_award_ack.S,
  MSGTYPE443 = NetAPIList.alliance_defence_ntf.S,
  MSGTYPE811 = NetAPIList.prime_rank_ntf.S,
  MSGTYPE683 = NetAPIList.game_items_trans_req.S,
  MSGTYPE1332 = NetAPIList.get_process_award_req.S,
  MSGTYPE232 = NetAPIList.equipments_update_ntf.S,
  MSGTYPE461 = NetAPIList.client_fps_req.S,
  MSGTYPE326 = NetAPIList.supply_loot_ntf.S,
  MSGTYPE1331 = NetAPIList.reset_scratch_card_ack.S,
  MSGTYPE1328 = NetAPIList.buy_scratch_card_req.S,
  MSGTYPE1826 = NetAPIList.gcs_today_task_change_req.S,
  MSGTYPE1326 = NetAPIList.enter_scratch_req.S,
  MSGTYPE247 = NetAPIList.vip_info_ntf.S,
  MSGTYPE1325 = NetAPIList.fte_gift_ntf.S,
  MSGTYPE1324 = NetAPIList.price_off_store_ntf.S,
  MSGTYPE740 = NetAPIList.krypton_refine_info_ack.S,
  MSGTYPE1321 = NetAPIList.refresh_fleet_pool_ack.S,
  MSGTYPE440 = NetAPIList.colony_players_ack.S,
  MSGTYPE1317 = NetAPIList.fair_arena_formation_all_ack.S,
  MSGTYPE1314 = NetAPIList.fair_arena_report_battle_req.S,
  MSGTYPE735 = NetAPIList.krypton_refine_req.S,
  MSGTYPE654 = NetAPIList.special_efficacy_ntf.S,
  MSGTYPE230 = NetAPIList.supply_ntf.S,
  MSGTYPE299 = NetAPIList.mine_list_ntf.S,
  MSGTYPE721 = NetAPIList.push_button_req.S,
  MSGTYPE423 = NetAPIList.colony_users_ack.S,
  MSGTYPE403 = NetAPIList.stores_buy_req.S,
  MSGTYPE468 = NetAPIList.domination_challenge_ack.S,
  MSGTYPE1306 = NetAPIList.fair_arena_rank_ack.S,
  MSGTYPE1168 = NetAPIList.hero_union_req.S,
  MSGTYPE1576 = NetAPIList.large_map_local_leader_ack.S,
  MSGTYPE1304 = NetAPIList.fair_arena_ack.S,
  MSGTYPE1303 = NetAPIList.fair_arena_req.S,
  MSGTYPE1442 = NetAPIList.expedition_rank_awards_ack.S,
  MSGTYPE695 = NetAPIList.remodel_info_ntf.S,
  MSGTYPE1300 = NetAPIList.mining_team_log_req.S,
  MSGTYPE507 = NetAPIList.alliance_domination_status_ack.S,
  MSGTYPE1114 = NetAPIList.wdc_awards_req.S,
  MSGTYPE72 = NetAPIList.warpgate_charge_req.S,
  MSGTYPE1297 = NetAPIList.forces_ntf.S,
  MSGTYPE1293 = NetAPIList.concentrate_atk_ack.S,
  MSGTYPE199 = NetAPIList.user_fight_history_ack.S,
  MSGTYPE1292 = NetAPIList.concentrate_atk_req.S,
  MSGTYPE1288 = NetAPIList.concentrate_end_req.S,
  MSGTYPE1110 = NetAPIList.wdc_report_ack.S,
  MSGTYPE240 = NetAPIList.task_finished_ntf.S,
  MSGTYPE352 = NetAPIList.alliance_level_info_ntf.S,
  MSGTYPE1283 = NetAPIList.item_classify_ntf.S,
  MSGTYPE1588 = NetAPIList.large_map_search_alliance_ack.S,
  MSGTYPE1277 = NetAPIList.change_leader_req.S,
  MSGTYPE1151 = NetAPIList.user_change_req.S,
  MSGTYPE1182 = NetAPIList.mining_world_pe_req.S,
  MSGTYPE292 = NetAPIList.loud_cast_ntf.S,
  MSGTYPE1276 = NetAPIList.month_card_receive_reward_ack.S,
  MSGTYPE1275 = NetAPIList.month_card_receive_reward_req.S,
  MSGTYPE1272 = NetAPIList.vip_reward_ack.S,
  MSGTYPE1270 = NetAPIList.wish_pay_req.S,
  MSGTYPE1101 = NetAPIList.enter_champion_req.S,
  MSGTYPE1268 = NetAPIList.pay_gifts_req.S,
  MSGTYPE349 = NetAPIList.donate_ack.S,
  MSGTYPE109 = NetAPIList.fleet_repair_all_req.S,
  MSGTYPE597 = NetAPIList.contention_mission_info_req.S,
  MSGTYPE1204 = NetAPIList.art_point_req.S,
  MSGTYPE1265 = NetAPIList.fte_ntf.S,
  MSGTYPE1264 = NetAPIList.fte_ack.S,
  MSGTYPE1261 = NetAPIList.choosable_item_use_req.S,
  MSGTYPE631 = NetAPIList.budo_req.S,
  MSGTYPE1367 = NetAPIList.enter_medal_store_ack.S,
  MSGTYPE518 = NetAPIList.contention_production_ntf.S,
  MSGTYPE1257 = NetAPIList.helper_buttons_ack.S,
  MSGTYPE1254 = NetAPIList.space_door_req.S,
  MSGTYPE1425 = NetAPIList.medal_glory_collection_total_attr_req.S,
  MSGTYPE1188 = NetAPIList.mining_wars_refresh_req.S,
  MSGTYPE1214 = NetAPIList.art_reset_req.S,
  MSGTYPE1437 = NetAPIList.expedition_wormhole_rank_board_ack.S,
  MSGTYPE32 = NetAPIList.swap_bag_grid_req.S,
  MSGTYPE1405 = NetAPIList.medal_glory_activate_ack.S,
  MSGTYPE826 = NetAPIList.prime_award_ack.S,
  MSGTYPE1208 = NetAPIList.art_core_list_req.S,
  MSGTYPE142 = NetAPIList.krypton_enhance_req.S,
  MSGTYPE78 = NetAPIList.mail_page_req.S,
  MSGTYPE1267 = NetAPIList.achievement_reward_receive_ack.S,
  MSGTYPE1511 = NetAPIList.large_map_battle_req.S,
  MSGTYPE401 = NetAPIList.stores_ack.S,
  MSGTYPE720 = NetAPIList.financial_ntf.S,
  MSGTYPE1197 = NetAPIList.mining_world_battle_ack.S,
  MSGTYPE263 = NetAPIList.create_tipoff_req.S,
  MSGTYPE1803 = NetAPIList.teamleague_matrix_save_req.S,
  MSGTYPE1192 = NetAPIList.mining_wars_battle_ack.S,
  MSGTYPE1191 = NetAPIList.mining_wars_battle_req.S,
  MSGTYPE306 = NetAPIList.mine_info_req.S,
  MSGTYPE1189 = NetAPIList.mining_wars_star_req.S,
  MSGTYPE1217 = NetAPIList.art_core_decompose_ack.S,
  MSGTYPE1187 = NetAPIList.mining_wars_buy_count_ack.S,
  MSGTYPE143 = NetAPIList.krypton_enhance_ack.S,
  MSGTYPE1186 = NetAPIList.mining_wars_buy_count_req.S,
  MSGTYPE1180 = NetAPIList.mining_world_log_req.S,
  MSGTYPE1004 = NetAPIList.client_req_stat_req.S,
  MSGTYPE1171 = NetAPIList.hero_collect_ack.S,
  MSGTYPE476 = NetAPIList.wd_star_req.S,
  MSGTYPE832 = NetAPIList.champion_promote_ntf.S,
  MSGTYPE107 = NetAPIList.user_collect_offline_expr_req.S,
  MSGTYPE1167 = NetAPIList.master_ack.S,
  MSGTYPE1158 = NetAPIList.adventure_add_ack.S,
  MSGTYPE1166 = NetAPIList.master_req.S,
  MSGTYPE624 = NetAPIList.crusade_first_req.S,
  MSGTYPE727 = NetAPIList.get_energy_req.S,
  MSGTYPE644 = NetAPIList.battle_report_req.S,
  MSGTYPE1162 = NetAPIList.recruit_req.S,
  MSGTYPE1161 = NetAPIList.recruit_list_new_ack.S,
  MSGTYPE506 = NetAPIList.alliance_domination_status_req.S,
  MSGTYPE1830 = NetAPIList.gcs_get_week_task_reward_req.S,
  MSGTYPE1150 = NetAPIList.event_activity_award_req.S,
  MSGTYPE686 = NetAPIList.budo_champion_ack.S,
  MSGTYPE1149 = NetAPIList.event_activity_ack.S,
  MSGTYPE1147 = NetAPIList.event_activity_ntf.S,
  MSGTYPE71 = NetAPIList.warpgate_status_ack.S,
  MSGTYPE1146 = NetAPIList.speed_resource_ntf.S,
  MSGTYPE1145 = NetAPIList.wdc_rank_board_ntf.S,
  MSGTYPE1139 = NetAPIList.reset_dice_req.S,
  MSGTYPE1138 = NetAPIList.dice_awards_req.S,
  MSGTYPE386 = NetAPIList.fleet_sale_with_rebate_list_req.S,
  MSGTYPE514 = NetAPIList.contention_name_ntf.S,
  MSGTYPE891 = NetAPIList.activity_store_price_ack.S,
  MSGTYPE1362 = NetAPIList.open_box_progress_award_req.S,
  MSGTYPE1133 = NetAPIList.first_charge_ntf.S,
  MSGTYPE1131 = NetAPIList.welcome_screen_ack.S,
  MSGTYPE1130 = NetAPIList.welcome_screen_req.S,
  MSGTYPE1821 = NetAPIList.sync_server_time_req.S,
  MSGTYPE669 = NetAPIList.mail_to_all_req.S,
  MSGTYPE1124 = NetAPIList.wdc_rankup_ntf.S,
  MSGTYPE277 = NetAPIList.battle_money_collect_req.S,
  MSGTYPE116 = NetAPIList.alliances_req.S,
  MSGTYPE1122 = NetAPIList.wdc_awards_get_req.S,
  MSGTYPE1120 = NetAPIList.wdc_enemy_release_req.S,
  MSGTYPE1119 = NetAPIList.wdc_honor_wall_ack.S,
  MSGTYPE454 = NetAPIList.krypton_info_req.S,
  MSGTYPE1298 = NetAPIList.my_forces_req.S,
  MSGTYPE1113 = NetAPIList.wdc_info_ntf.S,
  MSGTYPE945 = NetAPIList.plante_explore_ack.S,
  MSGTYPE1112 = NetAPIList.wdc_report_detail_ack.S,
  MSGTYPE1109 = NetAPIList.wdc_report_req.S,
  MSGTYPE233 = NetAPIList.attributes_change_ntf.S,
  MSGTYPE393 = NetAPIList.promotion_award_ntf.S,
  MSGTYPE57 = NetAPIList.recruit_fleet_ack.S,
  MSGTYPE1108 = NetAPIList.wdc_fight_ack.S,
  MSGTYPE1103 = NetAPIList.wdc_enter_req.S,
  MSGTYPE939 = NetAPIList.pay_limit_ntf.S,
  MSGTYPE830 = NetAPIList.prestige_info_ntf.S,
  MSGTYPE1173 = NetAPIList.hero_collect_rank_ack.S,
  MSGTYPE998 = NetAPIList.mulmatrix_switch_ack.S,
  MSGTYPE935 = NetAPIList.tactical_enter_refine_ack.S,
  MSGTYPE957 = NetAPIList.alliance_guid_ntf.S,
  MSGTYPE371 = NetAPIList.promotion_award_req.S,
  MSGTYPE117 = NetAPIList.alliances_ack.S,
  MSGTYPE1812 = NetAPIList.cumulate_recharge_award_get_req.S,
  MSGTYPE990 = NetAPIList.get_today_award_req.S,
  MSGTYPE987 = NetAPIList.redo_task_ack.S,
  MSGTYPE879 = NetAPIList.enter_seven_day_ack.S,
  MSGTYPE184 = NetAPIList.alliance_transfer_ack.S,
  MSGTYPE1159 = NetAPIList.adventure_add_price_req.S,
  MSGTYPE870 = NetAPIList.goal_info_req.S,
  MSGTYPE873 = NetAPIList.cutoff_info_req.S,
  MSGTYPE978 = NetAPIList.today_act_task_req.S,
  MSGTYPE609 = NetAPIList.activity_dna_req.S,
  MSGTYPE977 = NetAPIList.credit_exchange_ack.S,
  MSGTYPE975 = NetAPIList.thumb_up_req.S,
  MSGTYPE442 = NetAPIList.alliance_defence_req.S,
  MSGTYPE1365 = NetAPIList.mul_credit_exchange_ntf.S,
  MSGTYPE956 = NetAPIList.first_login_ntf.S,
  MSGTYPE955 = NetAPIList.tactical_delete_awards_ack.S,
  MSGTYPE444 = NetAPIList.alliance_set_defender_req.S,
  MSGTYPE628 = NetAPIList.contention_income_req.S,
  MSGTYPE1582 = NetAPIList.large_map_alliance_adjust_req.S,
  MSGTYPE950 = NetAPIList.get_invite_gift_ack.S,
  MSGTYPE556 = NetAPIList.fleet_weaken_req.S,
  MSGTYPE413 = NetAPIList.verify_redeem_code_ack.S,
  MSGTYPE1730 = NetAPIList.champion_cdtime_ack.S,
  MSGTYPE1212 = NetAPIList.art_core_equip_req.S,
  MSGTYPE406 = NetAPIList.mail_to_alliance_req.S,
  MSGTYPE737 = NetAPIList.krypton_inject_req.S,
  MSGTYPE941 = NetAPIList.tactical_guide_req.S,
  MSGTYPE33 = NetAPIList.swap_bag_grid_ack.S,
  MSGTYPE307 = NetAPIList.mine_reset_atk_cd_req.S,
  MSGTYPE156 = NetAPIList.alliance_refuse_ack.S,
  MSGTYPE694 = NetAPIList.remodel_info_ack.S,
  MSGTYPE1375 = NetAPIList.medal_decompose_confirm_ack.S,
  MSGTYPE549 = NetAPIList.ladder_rank_ack.S,
  MSGTYPE997 = NetAPIList.mulmatrix_switch_req.S,
  MSGTYPE671 = NetAPIList.champion_matrix_ack.S,
  MSGTYPE933 = NetAPIList.tactical_unlock_slot_ack.S,
  MSGTYPE469 = NetAPIList.domination_awards_list_req.S,
  MSGTYPE1559 = NetAPIList.large_map_mass_invite_ack.S,
  MSGTYPE905 = NetAPIList.ladder_award_ntf.S,
  MSGTYPE157 = NetAPIList.alliance_memo_req.S,
  MSGTYPE901 = NetAPIList.ladder_report_ack.S,
  MSGTYPE898 = NetAPIList.max_step_req.S,
  MSGTYPE896 = NetAPIList.refresh_type_ack.S,
  MSGTYPE647 = NetAPIList.get_my_support_ack.S,
  MSGTYPE465 = NetAPIList.alliance_domination_ntf.S,
  MSGTYPE894 = NetAPIList.prime_push_show_round_req.S,
  MSGTYPE893 = NetAPIList.client_login_ok_req.S,
  MSGTYPE548 = NetAPIList.ladder_rank_req.S,
  MSGTYPE880 = NetAPIList.prime_jump_price_req.S,
  MSGTYPE1820 = NetAPIList.pop_firstpay_on_lvup_ntf.S,
  MSGTYPE366 = NetAPIList.active_gifts_ntf.S,
  MSGTYPE980 = NetAPIList.enter_monthly_req.S,
  MSGTYPE871 = NetAPIList.goal_info_ack.S,
  MSGTYPE984 = NetAPIList.view_award_req.S,
  MSGTYPE594 = NetAPIList.award_get_req.S,
  MSGTYPE867 = NetAPIList.daily_benefits_award_req.S,
  MSGTYPE1356 = NetAPIList.krypton_redpoint_req.S,
  MSGTYPE167 = NetAPIList.champion_reset_cd_ack.S,
  MSGTYPE865 = NetAPIList.daily_benefits_req.S,
  MSGTYPE864 = NetAPIList.pay_status_ntf.S,
  MSGTYPE580 = NetAPIList.mulmatrix_buy_req.S,
  MSGTYPE845 = NetAPIList.open_server_fund_award_req.S,
  MSGTYPE860 = NetAPIList.enhance_info_ack.S,
  MSGTYPE856 = NetAPIList.prime_round_fight_ntf.S,
  MSGTYPE1302 = NetAPIList.team_fight_report_req.S,
  MSGTYPE849 = NetAPIList.vip_level_ntf.S,
  MSGTYPE847 = NetAPIList.all_welfare_ack.S,
  MSGTYPE842 = NetAPIList.open_server_fund_req.S,
  MSGTYPE622 = NetAPIList.crusade_buy_cross_req.S,
  MSGTYPE839 = NetAPIList.pay_sign_reward_req.S,
  MSGTYPE837 = NetAPIList.pay_sign_req.S,
  MSGTYPE831 = NetAPIList.levelup_unlock_ntf.S,
  MSGTYPE829 = NetAPIList.krypton_sort_ack.S,
  MSGTYPE621 = NetAPIList.crusade_reward_ack.S,
  MSGTYPE823 = NetAPIList.prime_last_report_req.S,
  MSGTYPE800 = NetAPIList.prime_map_req.S,
  MSGTYPE822 = NetAPIList.prime_fight_report_req.S,
  MSGTYPE819 = NetAPIList.prime_increase_power_info_ack.S,
  MSGTYPE817 = NetAPIList.revive_self_req.S,
  MSGTYPE1718 = NetAPIList.tlc_honor_wall_req.S,
  MSGTYPE50 = NetAPIList.officer_buy_ack.S,
  MSGTYPE543 = NetAPIList.ladder_gain_req.S,
  MSGTYPE810 = NetAPIList.prime_march_req.S,
  MSGTYPE808 = NetAPIList.prime_dot_status_ntf.S,
  MSGTYPE384 = NetAPIList.timelimit_hero_id_req.S,
  MSGTYPE804 = NetAPIList.prime_jump_req.S,
  MSGTYPE803 = NetAPIList.prime_active_ntf.S,
  MSGTYPE1420 = NetAPIList.challenge_medal_boss_req.S,
  MSGTYPE748 = NetAPIList.user_info_ack.S,
  MSGTYPE532 = NetAPIList.dungeon_open_req.S,
  MSGTYPE744 = NetAPIList.krypton_inject_list_ack.S,
  MSGTYPE739 = NetAPIList.krypton_refine_info_req.S,
  MSGTYPE346 = NetAPIList.block_devil_req.S,
  MSGTYPE944 = NetAPIList.plante_explore_req.S,
  MSGTYPE135 = NetAPIList.alliance_info_req.S,
  MSGTYPE169 = NetAPIList.champion_get_award_ack.S,
  MSGTYPE407 = NetAPIList.chat_history_async_req.S,
  MSGTYPE736 = NetAPIList.krypton_refine_ack.S,
  MSGTYPE115 = NetAPIList.message_del_req.S,
  MSGTYPE729 = NetAPIList.open_treasure_box_ack.S,
  MSGTYPE728 = NetAPIList.open_treasure_box_req.S,
  MSGTYPE1705 = NetAPIList.tlc_match_req.S,
  MSGTYPE725 = NetAPIList.treasure_info_ntf.S,
  MSGTYPE713 = NetAPIList.facebook_friends_ack.S,
  MSGTYPE1200 = NetAPIList.mining_reward_ntf.S,
  MSGTYPE712 = NetAPIList.facebook_friends_req.S,
  MSGTYPE707 = NetAPIList.give_friends_gift_ack.S,
  MSGTYPE450 = NetAPIList.fleet_info_req.S,
  MSGTYPE61 = NetAPIList.friends_ack.S,
  MSGTYPE503 = NetAPIList.contention_revive_req.S,
  MSGTYPE220 = NetAPIList.achievement_list_ack.S,
  MSGTYPE691 = NetAPIList.version_code_ntf.S,
  MSGTYPE34 = NetAPIList.techniques_req.S,
  MSGTYPE131 = NetAPIList.alliance_apply_ack.S,
  MSGTYPE105 = NetAPIList.user_offline_info_req.S,
  MSGTYPE1422 = NetAPIList.medal_glory_achievement_red_point_ntf.S,
  MSGTYPE350 = NetAPIList.donate_info_req.S,
  MSGTYPE620 = NetAPIList.crusade_reward_req.S,
  MSGTYPE702 = NetAPIList.remodel_help_ntf.S,
  MSGTYPE1804 = NetAPIList.tlc_matrix_change_ntf.S,
  MSGTYPE697 = NetAPIList.remodel_help_req.S,
  MSGTYPE1518 = NetAPIList.large_map_create_alliance_req.S,
  MSGTYPE690 = NetAPIList.apply_limit_update_req.S,
  MSGTYPE689 = NetAPIList.client_version_ntf.S,
  MSGTYPE1193 = NetAPIList.mining_wars_collect_req.S,
  MSGTYPE434 = NetAPIList.activity_stores_buy_req.S,
  MSGTYPE573 = NetAPIList.user_notifies_req.S,
  MSGTYPE332 = NetAPIList.ac_energy_charge_req.S,
  MSGTYPE355 = NetAPIList.champion_top_record_ack.S,
  MSGTYPE673 = NetAPIList.release_adjutant_req.S,
  MSGTYPE934 = NetAPIList.tactical_enter_refine_req.S,
  MSGTYPE670 = NetAPIList.champion_matrix_req.S,
  MSGTYPE662 = NetAPIList.stores_refresh_req.S,
  MSGTYPE652 = NetAPIList.get_promotion_reward_req.S,
  MSGTYPE649 = NetAPIList.get_budo_rank_ack.S,
  MSGTYPE584 = NetAPIList.equip_info_req.S,
  MSGTYPE636 = NetAPIList.budo_join_ack.S,
  MSGTYPE1164 = NetAPIList.recruit_look_req.S,
  MSGTYPE643 = NetAPIList.support_one_player_req.S,
  MSGTYPE640 = NetAPIList.budo_promotion_req.S,
  MSGTYPE42 = NetAPIList.revenue_do_req.S,
  MSGTYPE630 = NetAPIList.contention_missiles_ntf.S,
  MSGTYPE637 = NetAPIList.budo_mass_election_ntf.S,
  MSGTYPE197 = NetAPIList.krypton_fleet_equip_req.S,
  MSGTYPE1260 = NetAPIList.choosable_item_ack.S,
  MSGTYPE627 = NetAPIList.dna_gacha_ntf.S,
  MSGTYPE625 = NetAPIList.crusade_first_ack.S,
  MSGTYPE703 = NetAPIList.remodel_push_ntf.S,
  MSGTYPE614 = NetAPIList.crusade_enter_ack.S,
  MSGTYPE613 = NetAPIList.crusade_enter_req.S,
  MSGTYPE1816 = NetAPIList.login_fund_reward_req.S,
  MSGTYPE608 = NetAPIList.enchant_ack.S,
  MSGTYPE1545 = NetAPIList.large_map_alliance_expel_ack.S,
  MSGTYPE1599 = NetAPIList.large_map_vision_change_ntf.S,
  MSGTYPE405 = NetAPIList.equipment_action_list_ack.S,
  MSGTYPE578 = NetAPIList.mulmatrix_get_ack.S,
  MSGTYPE570 = NetAPIList.pay_confirm_req.S,
  MSGTYPE560 = NetAPIList.fleet_show_ack.S,
  MSGTYPE948 = NetAPIList.show_price_ntf.S,
  MSGTYPE266 = NetAPIList.update_spell_ntf.S,
  MSGTYPE551 = NetAPIList.ladder_special_ack.S,
  MSGTYPE544 = NetAPIList.ladder_buy_search_req.S,
  MSGTYPE304 = NetAPIList.mine_complete_ntf.S,
  MSGTYPE534 = NetAPIList.dungeon_enter_req.S,
  MSGTYPE747 = NetAPIList.user_info_req.S,
  MSGTYPE1374 = NetAPIList.medal_decompose_confirm_req.S,
  MSGTYPE513 = NetAPIList.contention_battle_ntf.S,
  MSGTYPE512 = NetAPIList.activity_task_list_ntf.S,
  MSGTYPE510 = NetAPIList.activity_task_list_ack.S,
  MSGTYPE508 = NetAPIList.domination_buffer_of_rank_ntf.S,
  MSGTYPE505 = NetAPIList.contention_stop_ack.S,
  MSGTYPE504 = NetAPIList.contention_stop_req.S,
  MSGTYPE706 = NetAPIList.remodel_event_ntf.S,
  MSGTYPE500 = NetAPIList.contention_trip_ntf.S,
  MSGTYPE499 = NetAPIList.contention_info_ntf.S,
  MSGTYPE498 = NetAPIList.contention_notifies_req.S,
  MSGTYPE495 = NetAPIList.set_game_local_req.S,
  MSGTYPE953 = NetAPIList.can_enhance_ack.S,
  MSGTYPE457 = NetAPIList.items_ack.S,
  MSGTYPE486 = NetAPIList.wd_ranking_champion_ntf.S,
  MSGTYPE333 = NetAPIList.ac_energy_charge_ack.S,
  MSGTYPE100 = NetAPIList.shop_sell_ack.S,
  MSGTYPE480 = NetAPIList.alliance_domination_total_rank_ntf.S,
  MSGTYPE477 = NetAPIList.wd_star_ack.S,
  MSGTYPE475 = NetAPIList.alliance_defence_revive_req.S,
  MSGTYPE201 = NetAPIList.user_brief_info_ack.S,
  MSGTYPE932 = NetAPIList.tactical_unlock_slot_req.S,
  MSGTYPE462 = NetAPIList.taobao_trade_req.S,
  MSGTYPE418 = NetAPIList.colony_info_ntf.S,
  MSGTYPE319 = NetAPIList.laba_info_req.S,
  MSGTYPE458 = NetAPIList.client_effect_req.S,
  MSGTYPE487 = NetAPIList.open_box_info_req.S,
  MSGTYPE280 = NetAPIList.quest_req.S,
  MSGTYPE298 = NetAPIList.mine_boost_req.S,
  MSGTYPE489 = NetAPIList.open_box_open_req.S,
  MSGTYPE441 = NetAPIList.prestige_get_req.S,
  MSGTYPE431 = NetAPIList.colony_fawn_ack.S,
  MSGTYPE428 = NetAPIList.colony_exploit_req.S,
  MSGTYPE426 = NetAPIList.colony_info_test_req.S,
  MSGTYPE425 = NetAPIList.colony_challenge_ack.S,
  MSGTYPE420 = NetAPIList.colony_times_ntf.S,
  MSGTYPE179 = NetAPIList.krypton_store_swap_req.S,
  MSGTYPE411 = NetAPIList.festival_ack.S,
  MSGTYPE583 = NetAPIList.mulmatrix_price_ack.S,
  MSGTYPE1311 = NetAPIList.fair_arena_reward_get_req.S,
  MSGTYPE400 = NetAPIList.stores_req.S,
  MSGTYPE391 = NetAPIList.cut_off_list_ack.S,
  MSGTYPE242 = NetAPIList.passport_bind_req.S,
  MSGTYPE380 = NetAPIList.clean_world_boss_cd_req.S,
  MSGTYPE372 = NetAPIList.promotion_award_ack.S,
  MSGTYPE994 = NetAPIList.today_task_ntf.S,
  MSGTYPE680 = NetAPIList.fleet_dismiss_ack.S,
  MSGTYPE353 = NetAPIList.login_first_ntf.S,
  MSGTYPE113 = NetAPIList.messages_req.S,
  MSGTYPE1 = NetAPIList.user_register_req.S,
  MSGTYPE599 = NetAPIList.contention_mission_award_req.S,
  MSGTYPE337 = NetAPIList.gateway_info_ntf.S,
  MSGTYPE335 = NetAPIList.ac_battle_info_req.S,
  MSGTYPE322 = NetAPIList.center_ntf.S,
  MSGTYPE316 = NetAPIList.laba_req.S,
  MSGTYPE315 = NetAPIList.guide_progress_ntf.S,
  MSGTYPE283 = NetAPIList.adventure_map_status_req.S,
  MSGTYPE274 = NetAPIList.all_act_status_ack.S,
  MSGTYPE852 = NetAPIList.pay_push_pop_set_req.S,
  MSGTYPE1194 = NetAPIList.mining_wars_collect_ack.S,
  MSGTYPE246 = NetAPIList.act_status_ack.S,
  MSGTYPE1285 = NetAPIList.forces_ack.S,
  MSGTYPE1134 = NetAPIList.enter_dice_req.S,
  MSGTYPE215 = NetAPIList.prestige_info_ack.S,
  MSGTYPE268 = NetAPIList.pve_awards_get_req.S,
  MSGTYPE198 = NetAPIList.user_fight_history_req.S,
  MSGTYPE417 = NetAPIList.game_addtion_req.S,
  MSGTYPE1583 = NetAPIList.large_map_alliance_adjust_ack.S,
  MSGTYPE68 = NetAPIList.friend_search_ack.S
}
NetAPIList.nameToMsgType = {
  MSGTYPE632 = "budo_test_req",
  MSGTYPE563 = "contention_history_req",
  MSGTYPE1430 = "user_rush_exit_req",
  MSGTYPE1348 = "normal_ntf",
  MSGTYPE579 = "mulmatrix_save_req",
  MSGTYPE383 = "allstars_reward_ack",
  MSGTYPE345 = "block_devil_ntf",
  MSGTYPE667 = "pay_list_carrier_ack",
  MSGTYPE358 = "champion_rank_reward_ack",
  MSGTYPE433 = "activity_stores_ack",
  MSGTYPE1398 = "medal_config_info_req",
  MSGTYPE899 = "max_step_ack",
  MSGTYPE724 = "push_button_ntf",
  MSGTYPE1592 = "large_map_rank_ack",
  MSGTYPE587 = "ladder_reset_req",
  MSGTYPE1111 = "wdc_report_detail_req",
  MSGTYPE1169 = "hero_union_ack",
  MSGTYPE949 = "get_invite_gift_req",
  MSGTYPE1190 = "mining_wars_star_ack",
  MSGTYPE301 = "mine_info_ntf",
  MSGTYPE1137 = "throw_dice_ack",
  MSGTYPE1527 = "large_map_alliance_player_search_ack",
  MSGTYPE540 = "ladder_cancel_req",
  MSGTYPE1435 = "expedition_wormhole_award_req",
  MSGTYPE972 = "story_award_ack",
  MSGTYPE361 = "vip_loot_ack",
  MSGTYPE147 = "krypton_gacha_some_req",
  MSGTYPE180 = "alliance_quit_req",
  MSGTYPE281 = "event_loot_ack",
  MSGTYPE1153 = "event_progress_award_req",
  MSGTYPE160 = "champion_list_req",
  MSGTYPE1526 = "large_map_alliance_player_search_req",
  MSGTYPE1531 = "large_map_alliance_recruit_option_ack",
  MSGTYPE958 = "remodel_help_others_ack",
  MSGTYPE982 = "view_each_task_req",
  MSGTYPE922 = "tactical_equip_on_req",
  MSGTYPE664 = "laba_times_ntf",
  MSGTYPE1596 = "large_map_mass_attack_line_ntf",
  MSGTYPE188 = "alliance_demote_fail_ack",
  MSGTYPE562 = "contention_award_ack",
  MSGTYPE559 = "fleet_show_req",
  MSGTYPE459 = "client_effect_ack",
  MSGTYPE0 = "common_ack",
  MSGTYPE1160 = "recruit_list_new_req",
  MSGTYPE456 = "items_req",
  MSGTYPE699 = "remodel_speed_req",
  MSGTYPE1381 = "medal_level_up_ack",
  MSGTYPE943 = "enter_plante_ack",
  MSGTYPE1121 = "wdc_awards_ntf",
  MSGTYPE464 = "domination_join_req",
  MSGTYPE300 = "mine_refresh_ntf",
  MSGTYPE572 = "items_ntf",
  MSGTYPE130 = "alliance_apply_req",
  MSGTYPE195 = "alliance_kick_fail_ack",
  MSGTYPE623 = "crusade_buy_cross_ack",
  MSGTYPE336 = "ac_matrix_req",
  MSGTYPE10000 = "keylist_ntf",
  MSGTYPE133 = "alliance_unapply_req",
  MSGTYPE1179 = "mining_world_ack",
  MSGTYPE1184 = "mining_wars_req",
  MSGTYPE1412 = "medal_glory_achievement_activate_req",
  MSGTYPE7 = "user_snapshot_req",
  MSGTYPE553 = "hero_level_attr_diff_req",
  MSGTYPE258 = "pay_list_ack",
  MSGTYPE196 = "krypton_equip_ack",
  MSGTYPE297 = "mine_speedup_req",
  MSGTYPE273 = "all_act_status_req",
  MSGTYPE1136 = "throw_dice_req",
  MSGTYPE554 = "hero_level_attr_diff_ack",
  MSGTYPE467 = "domination_challenge_req",
  MSGTYPE40 = "revenue_info_req",
  MSGTYPE1364 = "mul_credit_exchange_ack",
  MSGTYPE890 = "activity_store_price_req",
  MSGTYPE602 = "pay_record_ntf",
  MSGTYPE404 = "equipment_action_list_req",
  MSGTYPE447 = "alliance_defence_repair_req",
  MSGTYPE257 = "pay_list_req",
  MSGTYPE64 = "equipment_detail_req",
  MSGTYPE1383 = "medal_rank_up_ack",
  MSGTYPE1753 = "add_tlc_adjutant_req",
  MSGTYPE103 = "shop_purchase_back_req",
  MSGTYPE569 = "pay_verify2_ack",
  MSGTYPE419 = "colony_exp_ntf",
  MSGTYPE838 = "pay_sign_ack",
  MSGTYPE212 = "shop_buy_and_wear_req",
  MSGTYPE895 = "refresh_type_req",
  MSGTYPE1829 = "gcs_get_today_task_reward_ack",
  MSGTYPE606 = "change_auto_decompose_ntf",
  MSGTYPE87 = "equipment_enhance_req",
  MSGTYPE527 = "screen_track_req",
  MSGTYPE872 = "get_goal_award_req",
  MSGTYPE294 = "mine_refresh_req",
  MSGTYPE1144 = "dice_board_ntf",
  MSGTYPE1569 = "large_map_repair_req",
  MSGTYPE526 = "activity_status_ack",
  MSGTYPE1280 = "get_reward_detail_ack",
  MSGTYPE1822 = "gcs_task_data_req",
  MSGTYPE223 = "achievement_finish_ntf",
  MSGTYPE851 = "pay_push_pop_ntf",
  MSGTYPE1271 = "vip_reward_req",
  MSGTYPE45 = "add_friend_req",
  MSGTYPE1724 = "tlc_rankup_ntf",
  MSGTYPE716 = "multi_acc_req",
  MSGTYPE127 = "alliance_delete_req",
  MSGTYPE615 = "crusade_fight_req",
  MSGTYPE23 = "battle_status_req",
  MSGTYPE249 = "supply_info_req",
  MSGTYPE1552 = "large_map_join_mass_ack",
  MSGTYPE439 = "colony_players_req",
  MSGTYPE200 = "user_brief_info_req",
  MSGTYPE96 = "quark_exchange_ack",
  MSGTYPE1207 = "art_point_upgrade_ack",
  MSGTYPE1209 = "art_core_list_ack",
  MSGTYPE1310 = "fair_arena_reward_ack",
  MSGTYPE666 = "pay_list_carrier_req",
  MSGTYPE1251 = "main_city_ntf",
  MSGTYPE121 = "alliance_logs_ack",
  MSGTYPE396 = "krypton_fragment_core_ntf",
  MSGTYPE743 = "krypton_inject_list_req",
  MSGTYPE1585 = "large_map_log_ack",
  MSGTYPE387 = "fleet_sale_with_rebate_list_ack",
  MSGTYPE421 = "colony_logs_ntf",
  MSGTYPE850 = "temp_vip_ntf",
  MSGTYPE328 = "refresh_time_ntf",
  MSGTYPE519 = "contention_collect_req",
  MSGTYPE271 = "price_ack",
  MSGTYPE1152 = "user_change_ack",
  MSGTYPE1388 = "medal_equip_on_ack",
  MSGTYPE820 = "prime_enemy_info_req",
  MSGTYPE2 = "user_register_ack",
  MSGTYPE1266 = "achievement_reward_receive_req",
  MSGTYPE155 = "alliance_refuse_req",
  MSGTYPE542 = "ladder_fresh_ack",
  MSGTYPE39 = "techniques_upgrade_ack",
  MSGTYPE1721 = "tlc_awards_ntf",
  MSGTYPE1274 = "month_card_ack",
  MSGTYPE56 = "recruit_fleet_req",
  MSGTYPE574 = "ip_ntf",
  MSGTYPE1711 = "tlc_report_detail_req",
  MSGTYPE1107 = "wdc_fight_req",
  MSGTYPE557 = "contention_rank_req",
  MSGTYPE75 = "warpgate_collect_ack",
  MSGTYPE119 = "alliance_members_ack",
  MSGTYPE684 = "game_items_trans_ack",
  MSGTYPE659 = "all_gd_fleets_ack",
  MSGTYPE971 = "story_award_req",
  MSGTYPE108 = "user_collect_offline_expr_ack",
  MSGTYPE1386 = "medal_fleet_red_point_ntf",
  MSGTYPE1745 = "tlc_rank_board_ntf",
  MSGTYPE700 = "remodel_help_info_req",
  MSGTYPE38 = "techniques_upgrade_req",
  MSGTYPE591 = "contention_king_change_ntf",
  MSGTYPE892 = "daily_benefits_ntf",
  MSGTYPE317 = "laba_ntf",
  MSGTYPE253 = "cdtimes_clear_req",
  MSGTYPE1580 = "large_map_alliance_modify_notice_req",
  MSGTYPE1423 = "battle_rate_req",
  MSGTYPE363 = "level_loot_ack",
  MSGTYPE619 = "crusade_repair_req",
  MSGTYPE84 = "mail_set_read_req",
  MSGTYPE1213 = "art_core_equip_ack",
  MSGTYPE672 = "add_adjutant_req",
  MSGTYPE207 = "task_list_ack",
  MSGTYPE1135 = "enter_dice_ack",
  MSGTYPE324 = "top_force_list_req",
  MSGTYPE1836 = "gcs_point_update_ntf",
  MSGTYPE210 = "battle_fight_report_req",
  MSGTYPE681 = "adjutant_max_ntf",
  MSGTYPE132 = "alliance_apply_fail_ack",
  MSGTYPE603 = "login_token_ntf",
  MSGTYPE923 = "tactical_equip_on_ack",
  MSGTYPE448 = "alliance_defence_donate_req",
  MSGTYPE821 = "prime_enemy_info_ack",
  MSGTYPE937 = "tactical_equip_delete_req",
  MSGTYPE883 = "gp_test_pay_group_req",
  MSGTYPE675 = "month_card_list_ack",
  MSGTYPE145 = "krypton_gacha_one_req",
  MSGTYPE693 = "remodel_info_req",
  MSGTYPE323 = "chat_error_ntf",
  MSGTYPE1413 = "medal_glory_achievement_activate_ack",
  MSGTYPE12 = "fleets_req",
  MSGTYPE816 = "prime_site_info_ack",
  MSGTYPE592 = "award_list_req",
  MSGTYPE745 = "krypton_refine_list_req",
  MSGTYPE545 = "ladder_buy_search_ack",
  MSGTYPE1106 = "wdc_match_ack",
  MSGTYPE445 = "alliance_notifies_req",
  MSGTYPE1408 = "medal_glory_reset_req",
  MSGTYPE589 = "contention_king_info_req",
  MSGTYPE1542 = "large_map_alliance_exchange_req",
  MSGTYPE10002 = "udid_track_req",
  MSGTYPE1263 = "fte_req",
  MSGTYPE10001 = "udid_step_req",
  MSGTYPE902 = "ladder_jump_req",
  MSGTYPE314 = "update_guide_progress_req",
  MSGTYPE163 = "champion_challenge_ack",
  MSGTYPE1148 = "event_activity_req",
  MSGTYPE275 = "special_battle_req",
  MSGTYPE58 = "dismiss_fleet_req",
  MSGTYPE43 = "revenue_do_ack",
  MSGTYPE493 = "open_box_recent_ntf",
  MSGTYPE270 = "price_req",
  MSGTYPE344 = "block_devil_info_req",
  MSGTYPE106 = "user_offline_info_ack",
  MSGTYPE806 = "prime_my_info_ntf",
  MSGTYPE1414 = "medal_glory_collection_rank_req",
  MSGTYPE973 = "thumb_up_info_req",
  MSGTYPE1817 = "check_exp_add_item_req",
  MSGTYPE164 = "champion_top_req",
  MSGTYPE255 = "system_configuration_ack",
  MSGTYPE1514 = "large_map_exit_alliance_req",
  MSGTYPE805 = "prime_quit_req",
  MSGTYPE1329 = "buy_scratch_card_ack",
  MSGTYPE376 = "world_boss_info_ntf",
  MSGTYPE1757 = "tlc_krypton_sort_req",
  MSGTYPE618 = "crusade_cross_ack",
  MSGTYPE217 = "prestige_obtain_award_ack",
  MSGTYPE282 = "battle_event_status_ack",
  MSGTYPE47 = "officers_req",
  MSGTYPE546 = "ladder_buy_reset_req",
  MSGTYPE369 = "promotions_ntf",
  MSGTYPE1281 = "save_user_id_req",
  MSGTYPE1343 = "art_one_upgrade_req",
  MSGTYPE1185 = "mining_wars_ack",
  MSGTYPE539 = "ladder_raids_ack",
  MSGTYPE581 = "mulmatrix_blank_ntf",
  MSGTYPE1541 = "large_map_alliance_demotion_ack",
  MSGTYPE10 = "chat_ntf",
  MSGTYPE1178 = "mining_world_req",
  MSGTYPE1355 = "get_login_sign_award_ack",
  MSGTYPE995 = "switch_formation_req",
  MSGTYPE1174 = "red_point_ntf",
  MSGTYPE141 = "krypton_store_ack",
  MSGTYPE137 = "add_friend_ack",
  MSGTYPE158 = "alliance_memo_ack",
  MSGTYPE429 = "colony_exploit_ack",
  MSGTYPE265 = "bag_grid_ntf",
  MSGTYPE392 = "buy_cut_off_req",
  MSGTYPE588 = "ladder_reset_ack",
  MSGTYPE357 = "champion_rank_reward_req",
  MSGTYPE610 = "activity_dna_ack",
  MSGTYPE656 = "activity_rank_ack",
  MSGTYPE203 = "alliance_unapply_fail_ack",
  MSGTYPE243 = "passport_bind_ack",
  MSGTYPE1427 = "medal_glory_collection_enter_req",
  MSGTYPE858 = "url_push_ntf",
  MSGTYPE996 = "switch_formation_ack",
  MSGTYPE340 = "alliance_activities_req",
  MSGTYPE125 = "alliance_create_ack",
  MSGTYPE494 = "battle_rate_ntf",
  MSGTYPE889 = "activity_store_count_ack",
  MSGTYPE364 = "vip_loot_get_req",
  MSGTYPE412 = "verify_redeem_code_req",
  MSGTYPE1203 = "art_main_ack",
  MSGTYPE186 = "alliance_demote_req",
  MSGTYPE1128 = "translate_info_req",
  MSGTYPE1564 = "large_map_view_mass_player_req",
  MSGTYPE530 = "tango_invited_friends_ack",
  MSGTYPE427 = "colony_info_test_ack",
  MSGTYPE73 = "warpgate_charge_ack",
  MSGTYPE1393 = "medal_fleet_btn_red_point_ntf",
  MSGTYPE11 = "user_logout_req",
  MSGTYPE261 = "sign_activity_ack",
  MSGTYPE80 = "mail_content_req",
  MSGTYPE516 = "contention_occupier_ack",
  MSGTYPE993 = "other_monthly_ack",
  MSGTYPE687 = "budo_champion_report_req",
  MSGTYPE449 = "alliance_activity_times_ntf",
  MSGTYPE144 = "krypton_equip_req",
  MSGTYPE382 = "allstars_reward_req",
  MSGTYPE1003 = "user_bag_info_ntf",
  MSGTYPE927 = "tactical_refine_price_req",
  MSGTYPE520 = "contention_collect_ack",
  MSGTYPE541 = "ladder_fresh_req",
  MSGTYPE555 = "fleet_enhance_req",
  MSGTYPE1199 = "mining_battle_report_ack",
  MSGTYPE1129 = "translate_info_ack",
  MSGTYPE575 = "mail_goods_req",
  MSGTYPE1536 = "large_map_alliance_ratify_duty_data_ack",
  MSGTYPE1309 = "fair_arena_reward_req",
  MSGTYPE241 = "mail_unread_ntf",
  MSGTYPE452 = "common_ntf",
  MSGTYPE254 = "system_configuration_req",
  MSGTYPE1132 = "welcome_mask_req",
  MSGTYPE388 = "promotion_detail_req",
  MSGTYPE373 = "ready_world_boss_req",
  MSGTYPE202 = "krypton_fleet_move_req",
  MSGTYPE267 = "level_up_ntf",
  MSGTYPE524 = "contention_logbattle_detail_ack",
  MSGTYPE14 = "pve_battle_req",
  MSGTYPE663 = "stores_refresh_ack",
  MSGTYPE295 = "mine_digg_req",
  MSGTYPE1758 = "tlc_krypton_sort_ack",
  MSGTYPE604 = "dungeon_enter_ack",
  MSGTYPE189 = "alliance_promote_req",
  MSGTYPE863 = "player_pay_price_req",
  MSGTYPE102 = "shop_purchase_back_list_ack",
  MSGTYPE645 = "battle_report_ack",
  MSGTYPE1382 = "medal_rank_up_req",
  MSGTYPE343 = "alliance_weekend_award_ack",
  MSGTYPE1834 = "gcs_get_lv_all_rewards_req",
  MSGTYPE1561 = "large_map_mass_invite_do_req",
  MSGTYPE296 = "mine_atk_req",
  MSGTYPE547 = "ladder_buy_reset_ack",
  MSGTYPE128 = "alliance_delete_ack",
  MSGTYPE8 = "user_snapshot_ack",
  MSGTYPE154 = "alliance_ratify_ack",
  MSGTYPE999 = "mulmatrix_equip_info_req",
  MSGTYPE347 = "block_devil_complete_ntf",
  MSGTYPE679 = "fleet_dismiss_req",
  MSGTYPE313 = "supply_loot_get_req",
  MSGTYPE848 = "all_welfare_award_req",
  MSGTYPE525 = "activity_status_req",
  MSGTYPE1154 = "star_craft_data_ntf",
  MSGTYPE844 = "charge_open_server_fund_req",
  MSGTYPE1716 = "tlc_rank_board_req",
  MSGTYPE906 = "ladder_report_detail_req",
  MSGTYPE94 = "equipments_ack",
  MSGTYPE1176 = "area_id_ack",
  MSGTYPE1832 = "gcs_get_week_extra_reward_req",
  MSGTYPE104 = "shop_purchase_back_ack",
  MSGTYPE900 = "ladder_report_req",
  MSGTYPE531 = "tango_invite_req",
  MSGTYPE862 = "combo_guide_ack",
  MSGTYPE538 = "ladder_raids_req",
  MSGTYPE1308 = "fair_arena_season_ack",
  MSGTYPE1349 = "change_fleets_data_req",
  MSGTYPE1357 = "krypton_redpoint_ack",
  MSGTYPE571 = "contention_hit_ntf",
  MSGTYPE41 = "revenue_info_ack",
  MSGTYPE54 = "recruit_list_req",
  MSGTYPE561 = "contention_award_req",
  MSGTYPE288 = "adventure_cd_price_req",
  MSGTYPE150 = "krypton_decompose_req",
  MSGTYPE1722 = "tlc_awards_get_req",
  MSGTYPE22 = "user_map_status_ack",
  MSGTYPE409 = "thanksgiven_rank_ack",
  MSGTYPE633 = "budo_stage_ntf",
  MSGTYPE533 = "dungeon_open_ack",
  MSGTYPE354 = "champion_top_record_req",
  MSGTYPE250 = "supply_info_ack",
  MSGTYPE665 = "change_name_ntf",
  MSGTYPE183 = "alliance_transfer_req",
  MSGTYPE95 = "quark_exchange_req",
  MSGTYPE59 = "dismiss_fleet_ack",
  MSGTYPE321 = "dexter_libs_ntf",
  MSGTYPE1835 = "gcs_get_lv_all_rewards_ack",
  MSGTYPE191 = "alliance_promote_ack",
  MSGTYPE46 = "del_friend_req",
  MSGTYPE1823 = "gcs_task_data_ack",
  MSGTYPE509 = "activity_task_list_req",
  MSGTYPE278 = "quest_ntf",
  MSGTYPE286 = "adventure_rush_req",
  MSGTYPE714 = "financial_gifts_get_req",
  MSGTYPE492 = "open_box_refresh_ack",
  MSGTYPE1001 = "change_matrix_type_req",
  MSGTYPE1346 = "confirm_exchange_req",
  MSGTYPE612 = "akt_dna_next_stg_req",
  MSGTYPE1320 = "refresh_fleet_pool_req",
  MSGTYPE1318 = "update_formations_req",
  MSGTYPE26 = "battle_matrix_ack",
  MSGTYPE1294 = "group_fight_ntf",
  MSGTYPE229 = "fleet_kryptons_ntf",
  MSGTYPE260 = "sign_activity_req",
  MSGTYPE1370 = "medal_store_refresh_req",
  MSGTYPE262 = "sign_activity_reward_req",
  MSGTYPE168 = "champion_get_award_req",
  MSGTYPE1102 = "enter_champion_ack",
  MSGTYPE414 = "items_count_ntf",
  MSGTYPE305 = "mine_atk_ntf",
  MSGTYPE356 = "login_time_current_ntf",
  MSGTYPE952 = "can_enhance_req",
  MSGTYPE10003 = "protocal_version_req",
  MSGTYPE1282 = "user_id_status_ntf",
  MSGTYPE550 = "ladder_special_req",
  MSGTYPE377 = "challenge_world_boss_ack",
  MSGTYPE310 = "event_supply_ack",
  MSGTYPE1316 = "fair_arena_formation_all_req",
  MSGTYPE385 = "timelimit_hero_id_ack",
  MSGTYPE192 = "alliance_promote_fail_ack",
  MSGTYPE1590 = "large_map_dissolve_alliance_ack",
  MSGTYPE881 = "prime_jump_price_ack",
  MSGTYPE718 = "multi_acc_loot_req",
  MSGTYPE936 = "tactical_status_ntf",
  MSGTYPE1211 = "art_core_down_ack",
  MSGTYPE341 = "alliance_activities_ack",
  MSGTYPE596 = "award_count_ntf",
  MSGTYPE1513 = "large_map_join_alliance_ack",
  MSGTYPE688 = "budo_champion_report_ack",
  MSGTYPE276 = "special_battle_ack",
  MSGTYPE395 = "activities_button_status_ntf",
  MSGTYPE389 = "promotion_detail_ack",
  MSGTYPE1312 = "fair_arena_report_req",
  MSGTYPE148 = "krypton_gacha_some_ack",
  MSGTYPE491 = "open_box_refresh_req",
  MSGTYPE1400 = "fight_report_round_ntf",
  MSGTYPE836 = "vip_sign_item_use_award_ntf",
  MSGTYPE976 = "thumb_up_ack",
  MSGTYPE69 = "friend_ban_req",
  MSGTYPE1570 = "large_map_repair_ack",
  MSGTYPE1286 = "concentrate_req",
  MSGTYPE1727 = "tlc_status_ntf",
  MSGTYPE593 = "award_list_ack",
  MSGTYPE120 = "alliance_logs_req",
  MSGTYPE903 = "ladder_jump_ack",
  MSGTYPE446 = "alliance_resource_ntf",
  MSGTYPE25 = "battle_matrix_req",
  MSGTYPE1445 = "expedition_refresh_type_req",
  MSGTYPE338 = "notice_set_read_req",
  MSGTYPE185 = "alliance_transfer_fail_ack",
  MSGTYPE1399 = "medal_config_info_ack",
  MSGTYPE1330 = "reset_scratch_card_req",
  MSGTYPE1287 = "concentrate_ack",
  MSGTYPE1567 = "large_map_remove_mass_player_ntf",
  MSGTYPE1750 = "tlc_mulmatrix_switch_req",
  MSGTYPE118 = "alliance_members_req",
  MSGTYPE235 = "buildings_ntf",
  MSGTYPE882 = "select_goal_type_req",
  MSGTYPE651 = "get_budo_replay_ack",
  MSGTYPE473 = "alliance_domination_battle_ntf",
  MSGTYPE1578 = "large_map_alliance_diplomacy_ack",
  MSGTYPE5 = "building_upgrade_req",
  MSGTYPE876 = "get_goal_award_ack",
  MSGTYPE114 = "messages_ack",
  MSGTYPE813 = "prime_fight_history_ack",
  MSGTYPE1719 = "tlc_honor_wall_ack",
  MSGTYPE65 = "equipment_detail_ack",
  MSGTYPE30 = "bag_req",
  MSGTYPE841 = "equip_pushed_req",
  MSGTYPE1256 = "helper_buttons_req",
  MSGTYPE1720 = "tlc_enemy_release_req",
  MSGTYPE1215 = "art_reset_ack",
  MSGTYPE1358 = "event_click_req",
  MSGTYPE219 = "achievement_list_req",
  MSGTYPE1827 = "gcs_today_task_change_ack",
  MSGTYPE840 = "equip_push_ntf",
  MSGTYPE1255 = "space_door_ntf",
  MSGTYPE1172 = "hero_collect_rank_req",
  MSGTYPE930 = "tactical_refine_ack",
  MSGTYPE897 = "force_add_fleet_req",
  MSGTYPE460 = "award_receive_req",
  MSGTYPE1361 = "redownload_res_ack",
  MSGTYPE959 = "ltv_ajust_ntf",
  MSGTYPE466 = "alliance_flag_req",
  MSGTYPE1811 = "cumulate_recharge_ack",
  MSGTYPE708 = "update_passport_req",
  MSGTYPE834 = "champion_straight_ack",
  MSGTYPE1196 = "mining_world_battle_req",
  MSGTYPE1593 = "large_map_alliance_player_exit_ntf",
  MSGTYPE1116 = "wdc_rank_board_req",
  MSGTYPE1417 = "enter_medal_boss_ack",
  MSGTYPE110 = "fleet_repair_req",
  MSGTYPE329 = "ac_info_req",
  MSGTYPE1195 = "mining_wars_change_ntf",
  MSGTYPE1143 = "finish_speed_task_req",
  MSGTYPE515 = "contention_occupier_req",
  MSGTYPE239 = "heart_beat_req",
  MSGTYPE988 = "get_award_req",
  MSGTYPE577 = "mulmatrix_get_req",
  MSGTYPE1284 = "forces_req",
  MSGTYPE330 = "ac_ntf",
  MSGTYPE942 = "enter_plante_req",
  MSGTYPE833 = "champion_straight_req",
  MSGTYPE312 = "supply_loot_ack",
  MSGTYPE334 = "ac_jam_req",
  MSGTYPE99 = "shop_sell_req",
  MSGTYPE424 = "colony_challenge_req",
  MSGTYPE16 = "rush_result_ack",
  MSGTYPE1359 = "wxpay_info_ntf",
  MSGTYPE187 = "alliance_demote_ack",
  MSGTYPE635 = "budo_join_req",
  MSGTYPE490 = "open_box_open_ack",
  MSGTYPE122 = "alliance_aduits_req",
  MSGTYPE216 = "prestige_obtain_award_req",
  MSGTYPE989 = "get_award_ack",
  MSGTYPE1175 = "area_id_req",
  MSGTYPE1296 = "change_atk_seq_ack",
  MSGTYPE981 = "enter_monthly_ack",
  MSGTYPE4 = "user_login_ack",
  MSGTYPE991 = "get_today_award_ack",
  MSGTYPE339 = "gateway_clear_req",
  MSGTYPE256 = "pay_verify_req",
  MSGTYPE929 = "tactical_refine_req",
  MSGTYPE438 = "colony_action_purchase_req",
  MSGTYPE9 = "chat_req",
  MSGTYPE1402 = "medal_glory_collection_req",
  MSGTYPE1372 = "medal_list_req",
  MSGTYPE809 = "prime_march_ntf",
  MSGTYPE824 = "prime_dot_occupy_no_ntf",
  MSGTYPE585 = "equip_info_ack",
  MSGTYPE351 = "donate_info_ack",
  MSGTYPE331 = "ac_battle_req",
  MSGTYPE496 = "contention_map_req",
  MSGTYPE1157 = "adventure_add_req",
  MSGTYPE709 = "financial_req",
  MSGTYPE161 = "champion_list_ack",
  MSGTYPE1295 = "change_atk_seq_req",
  MSGTYPE733 = "send_global_email_req",
  MSGTYPE60 = "friends_req",
  MSGTYPE869 = "seven_day_ntf",
  MSGTYPE284 = "adventure_battle_req",
  MSGTYPE1291 = "force_join_ack",
  MSGTYPE370 = "promotions_req",
  MSGTYPE1216 = "art_core_decompose_req",
  MSGTYPE1279 = "get_reward_detail_req",
  MSGTYPE601 = "adventure_chapter_status_ntf",
  MSGTYPE1597 = "large_map_battle_ack",
  MSGTYPE86 = "fight_ack",
  MSGTYPE1258 = "event_log_ntf",
  MSGTYPE1756 = "change_tlc_leader_req",
  MSGTYPE722 = "push_button_info_req",
  MSGTYPE807 = "prime_dot_occupy_ntf",
  MSGTYPE327 = "ver_check_req",
  MSGTYPE582 = "mulmatrix_price_req",
  MSGTYPE566 = "pay_list2_req",
  MSGTYPE362 = "level_loot_req",
  MSGTYPE479 = "wd_award_ack",
  MSGTYPE565 = "orders_ntf",
  MSGTYPE535 = "ladder_enter_ack",
  MSGTYPE1411 = "medal_glory_achievement_ack",
  MSGTYPE101 = "shop_purchase_back_list_req",
  MSGTYPE1218 = "art_can_upgrade_req",
  MSGTYPE843 = "open_server_fund_ack",
  MSGTYPE482 = "wd_last_champion_ack",
  MSGTYPE857 = "sell_item_ntf",
  MSGTYPE397 = "activities_button_first_status_req",
  MSGTYPE134 = "alliance_unapply_ack",
  MSGTYPE474 = "alliance_domination_defence_ntf",
  MSGTYPE738 = "krypton_inject_ack",
  MSGTYPE710 = "financial_ack",
  MSGTYPE564 = "contention_history_ack",
  MSGTYPE1446 = "expedition_refresh_type_ack",
  MSGTYPE63 = "friend_detail_req",
  MSGTYPE381 = "add_world_boss_force_rate_ack",
  MSGTYPE205 = "user_challenged_ack",
  MSGTYPE723 = "push_button_info_ack",
  MSGTYPE517 = "contention_transfer_req",
  MSGTYPE629 = "contention_income_ack",
  MSGTYPE208 = "get_task_reward_req",
  MSGTYPE193 = "alliance_kick_req",
  MSGTYPE28 = "battle_result_ack",
  MSGTYPE484 = "wd_ranking_rank_req",
  MSGTYPE1141 = "speed_resource_req",
  MSGTYPE552 = "pay_animation_ntf",
  MSGTYPE925 = "tactical_equip_off_ack",
  MSGTYPE415 = "thanksgiven_champion_ntf",
  MSGTYPE1313 = "fair_arena_report_ack",
  MSGTYPE607 = "enchant_req",
  MSGTYPE875 = "buy_cutoff_item_req",
  MSGTYPE166 = "champion_reset_cd_req",
  MSGTYPE674 = "month_card_list_req",
  MSGTYPE1747 = "tlc_bag_ack",
  MSGTYPE136 = "alliance_info_ack",
  MSGTYPE311 = "supply_loot_req",
  MSGTYPE455 = "krypton_info_ack",
  MSGTYPE1752 = "tlc_quit_req",
  MSGTYPE416 = "item_use_award_ntf",
  MSGTYPE705 = "give_share_awards_req",
  MSGTYPE502 = "contention_starting_req",
  MSGTYPE112 = "friend_ban_del_req",
  MSGTYPE49 = "officer_buy_req",
  MSGTYPE1252 = "mask_modules_req",
  MSGTYPE1299 = "my_forces_ack",
  MSGTYPE231 = "level_ntf",
  MSGTYPE432 = "activity_stores_req",
  MSGTYPE365 = "level_loot_get_req",
  MSGTYPE1532 = "large_map_alliance_recruit_option_update_req",
  MSGTYPE483 = "device_info_req",
  MSGTYPE617 = "crusade_cross_req",
  MSGTYPE1170 = "hero_collect_req",
  MSGTYPE741 = "krypton_inject_buy_req",
  MSGTYPE325 = "top_force_list_ntf",
  MSGTYPE658 = "all_gd_fleets_req",
  MSGTYPE422 = "colony_users_req",
  MSGTYPE272 = "module_status_ntf",
  MSGTYPE159 = "alliance_memo_fail_ack",
  MSGTYPE1198 = "mining_battle_report_req",
  MSGTYPE1504 = "large_map_base_req",
  MSGTYPE1528 = "large_map_alliance_invite_req",
  MSGTYPE182 = "alliance_quit_fail_ack",
  MSGTYPE1595 = "large_map_alliance_promotion_ack",
  MSGTYPE204 = "alliance_info_ntf",
  MSGTYPE151 = "krypton_decompose_ack",
  MSGTYPE1337 = "end_scratch_ack",
  MSGTYPE153 = "alliance_ratify_req",
  MSGTYPE98 = "shop_purchase_ack",
  MSGTYPE1334 = "exchange_award_req",
  MSGTYPE568 = "pay_verify2_req",
  MSGTYPE1253 = "mask_show_index_req",
  MSGTYPE1301 = "mining_team_log_ack",
  MSGTYPE248 = "revenue_exchange_req",
  MSGTYPE511 = "activity_task_reward_req",
  MSGTYPE904 = "ladder_award_req",
  MSGTYPE1163 = "recruit_ack",
  MSGTYPE634 = "budo_sign_up_ntf",
  MSGTYPE320 = "laba_award_req",
  MSGTYPE888 = "activity_store_count_req",
  MSGTYPE1818 = "check_exp_add_item_ack",
  MSGTYPE1543 = "large_map_alliance_exchange_ack",
  MSGTYPE19 = "user_cdtime_ack",
  MSGTYPE481 = "wd_last_champion_req",
  MSGTYPE742 = "krypton_inject_buy_ack",
  MSGTYPE600 = "contention_leave_req",
  MSGTYPE1529 = "large_map_alliance_invite_ack",
  MSGTYPE228 = "cd_time_ntf",
  MSGTYPE194 = "alliance_kick_ack",
  MSGTYPE368 = "krypton_energy_req",
  MSGTYPE558 = "contention_rank_ack",
  MSGTYPE835 = "champion_straight_award_req",
  MSGTYPE79 = "mail_page_ack",
  MSGTYPE734 = "collect_energy_req",
  MSGTYPE701 = "remodel_help_info_ack",
  MSGTYPE626 = "akt_dna_get_award_req",
  MSGTYPE668 = "laba_type_ntf",
  MSGTYPE20 = "user_cdtime_req",
  MSGTYPE1165 = "recruit_look_ack",
  MSGTYPE66 = "friend_detail_ack",
  MSGTYPE76 = "warpgate_upgrade_req",
  MSGTYPE885 = "store_quick_buy_count_ack",
  MSGTYPE67 = "friend_search_req",
  MSGTYPE1831 = "gcs_get_week_task_reward_ack",
  MSGTYPE27 = "user_matrix_save_req",
  MSGTYPE1142 = "speed_resource_ack",
  MSGTYPE1723 = "tlc_awards_get_ack",
  MSGTYPE1815 = "login_fund_ack",
  MSGTYPE1002 = "change_matrix_type_ack",
  MSGTYPE523 = "contention_logbattle_detail_req",
  MSGTYPE908 = "stores_buy_ack",
  MSGTYPE1005 = "bugly_data_ntf",
  MSGTYPE1505 = "large_map_base_ack",
  MSGTYPE1126 = "wdc_fight_status_ack",
  MSGTYPE1837 = "gcs_buy_update_ntf",
  MSGTYPE1833 = "gcs_get_week_extra_reward_ack",
  MSGTYPE451 = "fleet_info_ack",
  MSGTYPE985 = "view_award_ack",
  MSGTYPE74 = "warpgate_collect_req",
  MSGTYPE471 = "domination_ranks_req",
  MSGTYPE1828 = "gcs_get_today_task_reward_req",
  MSGTYPE1115 = "wdc_awards_ack",
  MSGTYPE1501 = "large_map_my_info_ntf",
  MSGTYPE1327 = "enter_scratch_ack",
  MSGTYPE616 = "crusade_fight_ack",
  MSGTYPE140 = "krypton_store_req",
  MSGTYPE1825 = "gcs_awards_data_ack",
  MSGTYPE1824 = "gcs_awards_data_req",
  MSGTYPE1125 = "wdc_fight_status_req",
  MSGTYPE874 = "cutoff_info_ack",
  MSGTYPE293 = "mine_list_req",
  MSGTYPE1819 = "exp_add_item_ntf",
  MSGTYPE921 = "enter_tactical_ack",
  MSGTYPE1544 = "large_map_alliance_expel_req",
  MSGTYPE611 = "activity_dna_charge_req",
  MSGTYPE884 = "store_quick_buy_count_req",
  MSGTYPE1814 = "login_fund_req",
  MSGTYPE827 = "prime_round_result_ntf",
  MSGTYPE1385 = "medal_fleet_formation_ntf",
  MSGTYPE437 = "colony_release_ack",
  MSGTYPE992 = "other_monthly_req",
  MSGTYPE1336 = "end_scratch_req",
  MSGTYPE698 = "remodel_help_others_req",
  MSGTYPE410 = "festival_req",
  MSGTYPE655 = "activity_rank_req",
  MSGTYPE682 = "max_level_ntf",
  MSGTYPE726 = "invite_friends_req",
  MSGTYPE802 = "prime_active_req",
  MSGTYPE660 = "fleet_fight_report_req",
  MSGTYPE676 = "month_card_buy_req",
  MSGTYPE1801 = "teamleague_matrix_req",
  MSGTYPE1755 = "simple_mulmatrix_get_ack",
  MSGTYPE1183 = "mining_world_pe_ack",
  MSGTYPE1754 = "simple_mulmatrix_get_req",
  MSGTYPE1749 = "tlc_krypton_store_ack",
  MSGTYPE1748 = "tlc_krypton_store_req",
  MSGTYPE746 = "krypton_refine_list_ack",
  MSGTYPE146 = "krypton_gacha_one_ack",
  MSGTYPE35 = "techniques_ack",
  MSGTYPE1746 = "tlc_bag_req",
  MSGTYPE947 = "plante_refresh_ack",
  MSGTYPE650 = "get_budo_replay_req",
  MSGTYPE1729 = "champion_cdtime_req",
  MSGTYPE907 = "ladder_report_detail_ack",
  MSGTYPE1726 = "tlc_fight_status_ack",
  MSGTYPE1725 = "tlc_fight_status_req",
  MSGTYPE812 = "prime_fight_history_req",
  MSGTYPE1546 = "large_map_block_detail_req",
  MSGTYPE226 = "fleets_ntf",
  MSGTYPE1717 = "tlc_rank_board_ack",
  MSGTYPE1418 = "reset_medal_boss_req",
  MSGTYPE1387 = "medal_equip_on_req",
  MSGTYPE1201 = "version_info_req",
  MSGTYPE1712 = "tlc_report_detail_ack",
  MSGTYPE453 = "warn_req",
  MSGTYPE436 = "colony_release_req",
  MSGTYPE657 = "support_one_player_ack",
  MSGTYPE379 = "add_world_boss_force_rate_req",
  MSGTYPE1710 = "tlc_report_ack",
  MSGTYPE1000 = "mulmatrix_equip_info_ack",
  MSGTYPE605 = "change_auto_decompose_req",
  MSGTYPE378 = "ready_world_boss_ack",
  MSGTYPE1709 = "tlc_report_req",
  MSGTYPE1708 = "tlc_fight_ack",
  MSGTYPE90 = "equipment_evolution_req",
  MSGTYPE83 = "mail_delete_req",
  MSGTYPE1371 = "medal_store_refresh_ack",
  MSGTYPE1510 = "large_map_event_ntf",
  MSGTYPE1802 = "teamleague_matrix_ack",
  MSGTYPE1577 = "large_map_alliance_diplomacy_req",
  MSGTYPE13 = "fleets_ack",
  MSGTYPE36 = "gm_cmd_req",
  MSGTYPE1702 = "tlc_enter_ack",
  MSGTYPE853 = "prime_do_wve_boss_left_ntf",
  MSGTYPE478 = "wd_award_req",
  MSGTYPE1210 = "art_core_down_req",
  MSGTYPE81 = "mail_content_ack",
  MSGTYPE1602 = "large_map_ship_arrive_req",
  MSGTYPE1601 = "large_map_small_map_ack",
  MSGTYPE238 = "fleet_matrix_ntf",
  MSGTYPE529 = "tango_invited_friends_req",
  MSGTYPE928 = "tactical_refine_price_ack",
  MSGTYPE586 = "mycard_error_ntf",
  MSGTYPE1598 = "large_map_vision_ntf",
  MSGTYPE576 = "mail_goods_ack",
  MSGTYPE1323 = "fair_arena_fleet_info_ack",
  MSGTYPE55 = "recruit_list_ack",
  MSGTYPE859 = "enhance_info_req",
  MSGTYPE677 = "month_card_use_req",
  MSGTYPE211 = "battle_fight_report_ack",
  MSGTYPE1519 = "large_map_create_alliance_ack",
  MSGTYPE1594 = "large_map_alliance_promotion_req",
  MSGTYPE685 = "budo_champion_req",
  MSGTYPE259 = "use_item_req",
  MSGTYPE1591 = "large_map_rank_req",
  MSGTYPE1589 = "large_map_dissolve_alliance_req",
  MSGTYPE825 = "prime_award_req",
  MSGTYPE1319 = "update_formations_ack",
  MSGTYPE1278 = "leader_info_ntf",
  MSGTYPE1587 = "large_map_search_alliance_req",
  MSGTYPE1401 = "medal_glory_collection_ntf",
  MSGTYPE123 = "alliance_aduits_ack",
  MSGTYPE818 = "prime_increase_power_info_req",
  MSGTYPE1117 = "wdc_rank_board_ack",
  MSGTYPE1259 = "choosable_item_req",
  MSGTYPE638 = "budo_change_formation_req",
  MSGTYPE97 = "shop_purchase_req",
  MSGTYPE951 = "facebook_switch_ntf",
  MSGTYPE1581 = "large_map_alliance_modify_notice_ack",
  MSGTYPE590 = "contention_king_info_ack",
  MSGTYPE129 = "alliance_delete_fail_ack",
  MSGTYPE1566 = "large_map_remove_mass_player_req",
  MSGTYPE1579 = "large_map_alliance_diplomacy_exchange_req",
  MSGTYPE1305 = "fair_arena_rank_req",
  MSGTYPE1575 = "large_map_local_leader_req",
  MSGTYPE595 = "award_get_all_req",
  MSGTYPE1574 = "large_map_buy_energy_req",
  MSGTYPE731 = "friend_role_req",
  MSGTYPE1573 = "large_map_add_mass_player_ntf",
  MSGTYPE1572 = "large_map_repair_ship_ack",
  MSGTYPE1571 = "large_map_repair_ship_req",
  MSGTYPE318 = "laba_rate_reroll_req",
  MSGTYPE1565 = "large_map_view_mass_player_ack",
  MSGTYPE1563 = "large_map_start_mass_war_req",
  MSGTYPE1562 = "large_map_exit_mass_troop_req",
  MSGTYPE234 = "progress_ntf",
  MSGTYPE1438 = "expedition_wormhole_rank_self_ntf",
  MSGTYPE1560 = "large_map_mass_invite_ntf",
  MSGTYPE15 = "user_rush_req",
  MSGTYPE920 = "enter_tactical_req",
  MSGTYPE1341 = "art_one_upgrade_data_req",
  MSGTYPE375 = "exit_world_boss_req",
  MSGTYPE408 = "thanksgiven_rank_req",
  MSGTYPE48 = "officers_ack",
  MSGTYPE1557 = "large_map_mass_near_invite_ack",
  MSGTYPE1556 = "large_map_mass_near_invite_req",
  MSGTYPE24 = "battle_status_ack",
  MSGTYPE227 = "resource_ntf",
  MSGTYPE1548 = "large_map_create_mass_troop_req",
  MSGTYPE3 = "user_login_req",
  MSGTYPE1555 = "large_map_dissolve_mass_troop_req",
  MSGTYPE359 = "first_purchase_ntf",
  MSGTYPE1554 = "large_map_mass_troops_info_ack",
  MSGTYPE269 = "pve_awards_get_ack",
  MSGTYPE488 = "open_box_info_ack",
  MSGTYPE1553 = "large_map_mass_troops_info_req",
  MSGTYPE1551 = "large_map_join_mass_req",
  MSGTYPE1533 = "large_map_alliance_ratify_join_data_req",
  MSGTYPE285 = "adventure_cd_clear_req",
  MSGTYPE1550 = "large_map_mass_troops_ack",
  MSGTYPE1549 = "large_map_mass_troops_req",
  MSGTYPE1547 = "large_map_block_detail_ack",
  MSGTYPE390 = "cut_off_list_req",
  MSGTYPE342 = "alliance_weekend_award_req",
  MSGTYPE31 = "bag_ack",
  MSGTYPE954 = "tactical_delete_awards_req",
  MSGTYPE1540 = "large_map_alliance_demotion_req",
  MSGTYPE868 = "enter_seven_day_req",
  MSGTYPE886 = "store_quick_buy_price_req",
  MSGTYPE1419 = "reset_medal_boss_ack",
  MSGTYPE485 = "wd_ranking_rank_ack",
  MSGTYPE1538 = "large_map_alliance_ratify_handle_ack",
  MSGTYPE1155 = "star_craft_sign_up_req",
  MSGTYPE1537 = "large_map_alliance_ratify_handle_req",
  MSGTYPE1289 = "concentrate_end_ack",
  MSGTYPE1535 = "large_map_alliance_ratify_duty_data_req",
  MSGTYPE1534 = "large_map_alliance_ratify_join_data_ack",
  MSGTYPE861 = "combo_guide_req",
  MSGTYPE1530 = "large_map_alliance_recruit_option_req",
  MSGTYPE245 = "act_status_req",
  MSGTYPE279 = "quest_loot_req",
  MSGTYPE497 = "contention_map_ack",
  MSGTYPE1525 = "large_map_alliance_invite_data_ack",
  MSGTYPE1524 = "large_map_alliance_invite_data_req",
  MSGTYPE367 = "active_gifts_req",
  MSGTYPE537 = "ladder_search_ack",
  MSGTYPE1415 = "medal_glory_collection_rank_ack",
  MSGTYPE394 = "new_activities_compare_req",
  MSGTYPE1522 = "large_map_alliance_army_req",
  MSGTYPE1521 = "large_map_alliance_affais_ack",
  MSGTYPE730 = "treasure_info_req",
  MSGTYPE814 = "prime_increase_power_req",
  MSGTYPE1520 = "large_map_alliance_affais_req",
  MSGTYPE696 = "remodel_levelup_req",
  MSGTYPE1517 = "large_map_near_alliance_ack",
  MSGTYPE21 = "pve_map_status_req",
  MSGTYPE1509 = "large_map_route_ntf",
  MSGTYPE1273 = "month_card_req",
  MSGTYPE646 = "get_my_support_req",
  MSGTYPE1515 = "large_map_exit_alliance_ack",
  MSGTYPE598 = "contention_mission_info_ack",
  MSGTYPE501 = "contention_station_ntf",
  MSGTYPE1202 = "art_main_req",
  MSGTYPE1342 = "art_one_upgrade_data_ack",
  MSGTYPE1713 = "tlc_info_ntf",
  MSGTYPE1706 = "tlc_match_ack",
  MSGTYPE472 = "domination_ranks_ack",
  MSGTYPE252 = "connection_close_req",
  MSGTYPE360 = "vip_loot_req",
  MSGTYPE1516 = "large_map_near_alliance_req",
  MSGTYPE1508 = "large_map_route_ack",
  MSGTYPE1439 = "expedition_task_update_ntf",
  MSGTYPE1506 = "large_map_block_update_ntf",
  MSGTYPE1503 = "large_map_block_ack",
  MSGTYPE1502 = "large_map_block_req",
  MSGTYPE435 = "yys_ntf",
  MSGTYPE1347 = "credit_exchange_ntf",
  MSGTYPE1444 = "expedition_stores_refresh_ack",
  MSGTYPE887 = "store_quick_buy_price_ack",
  MSGTYPE926 = "tactical_resource_ntf",
  MSGTYPE1156 = "event_ntf_req",
  MSGTYPE924 = "tactical_equip_off_req",
  MSGTYPE692 = "version_code_update_req",
  MSGTYPE1443 = "expedition_stores_refresh_req",
  MSGTYPE214 = "prestige_info_req",
  MSGTYPE1441 = "expedition_rank_awards_req",
  MSGTYPE1269 = "pay_gifts_ack",
  MSGTYPE536 = "ladder_search_req",
  MSGTYPE470 = "domination_awards_list_ack",
  MSGTYPE1368 = "medal_store_buy_req",
  MSGTYPE1440 = "expedition_top3_update_ntf",
  MSGTYPE828 = "krypton_sort_req",
  MSGTYPE1373 = "medal_list_ack",
  MSGTYPE251 = "supply_exchange_req",
  MSGTYPE1507 = "large_map_route_req",
  MSGTYPE946 = "plante_refresh_req",
  MSGTYPE1436 = "expedition_wormhole_rank_board_req",
  MSGTYPE974 = "thumb_up_info_ack",
  MSGTYPE528 = "contention_jump_ntf",
  MSGTYPE1434 = "expedition_wormhole_status_ack",
  MSGTYPE1433 = "expedition_wormhole_status_req",
  MSGTYPE1707 = "tlc_fight_req",
  MSGTYPE1431 = "fps_switch_ntf",
  MSGTYPE1429 = "user_statistics_req",
  MSGTYPE430 = "colony_fawn_req",
  MSGTYPE70 = "warpgate_status_req",
  MSGTYPE1428 = "medal_glory_collection_enter_ack",
  MSGTYPE854 = "prime_time_wve_boss_left_ntf",
  MSGTYPE986 = "redo_task_req",
  MSGTYPE1426 = "medal_glory_collection_total_attr_ack",
  MSGTYPE1339 = "uc_pay_sign_ack",
  MSGTYPE82 = "mail_send_req",
  MSGTYPE1219 = "art_can_upgrade_ack",
  MSGTYPE648 = "get_budo_rank_req",
  MSGTYPE1424 = "hero_union_unlock_ntf",
  MSGTYPE1123 = "wdc_awards_get_ack",
  MSGTYPE704 = "give_friends_gift_req",
  MSGTYPE1421 = "challenge_medal_boss_ack",
  MSGTYPE801 = "prime_map_ack",
  MSGTYPE1539 = "large_map_alliance_apply_duty_req",
  MSGTYPE1206 = "art_point_upgrade_req",
  MSGTYPE77 = "warpgate_upgrade_ack",
  MSGTYPE1715 = "tlc_awards_ack",
  MSGTYPE1409 = "medal_glory_reset_ack",
  MSGTYPE521 = "contention_logs_req",
  MSGTYPE1380 = "medal_level_up_req",
  MSGTYPE931 = "tactical_refine_save_req",
  MSGTYPE522 = "contention_logs_ack",
  MSGTYPE1523 = "large_map_alliance_army_ack",
  MSGTYPE1410 = "medal_glory_achievement_req",
  MSGTYPE1416 = "enter_medal_boss_req",
  MSGTYPE1104 = "wdc_enter_ack",
  MSGTYPE181 = "alliance_quit_ack",
  MSGTYPE124 = "alliance_create_req",
  MSGTYPE1407 = "medal_glory_rank_up_ack",
  MSGTYPE126 = "alliance_create_fail_ack",
  MSGTYPE1406 = "medal_glory_rank_up_req",
  MSGTYPE1701 = "tlc_enter_req",
  MSGTYPE348 = "donate_req",
  MSGTYPE846 = "all_welfare_req",
  MSGTYPE1404 = "medal_glory_activate_req",
  MSGTYPE165 = "champion_top_ack",
  MSGTYPE1403 = "medal_glory_collection_ack",
  MSGTYPE661 = "fleet_fight_report_ack",
  MSGTYPE979 = "today_act_task_ack",
  MSGTYPE374 = "challenge_world_boss_req",
  MSGTYPE93 = "equipments_req",
  MSGTYPE1586 = "large_map_log_battle_req",
  MSGTYPE732 = "friend_role_ack",
  MSGTYPE1315 = "fair_arena_report_battle_ack",
  MSGTYPE218 = "prestige_complete_ntf",
  MSGTYPE1181 = "mining_world_log_ack",
  MSGTYPE1392 = "medal_equip_replace_ack",
  MSGTYPE1391 = "medal_equip_replace_req",
  MSGTYPE1390 = "medal_equip_off_ack",
  MSGTYPE815 = "prime_site_info_req",
  MSGTYPE983 = "view_each_task_ack",
  MSGTYPE1177 = "master_ntf",
  MSGTYPE717 = "multi_acc_ack",
  MSGTYPE1389 = "medal_equip_off_req",
  MSGTYPE1714 = "tlc_awards_req",
  MSGTYPE1813 = "cumulate_recharge_award_get_ack",
  MSGTYPE1340 = "ac_sweep_req",
  MSGTYPE1384 = "medal_list_ntf",
  MSGTYPE567 = "pay_list2_ack",
  MSGTYPE1379 = "medal_equip_ack",
  MSGTYPE938 = "tactical_equip_delete_ack",
  MSGTYPE1322 = "fair_arena_fleet_info_req",
  MSGTYPE1344 = "art_one_upgrade_ack",
  MSGTYPE37 = "pack_bag_req",
  MSGTYPE1378 = "medal_equip_req",
  MSGTYPE1377 = "medal_reset_confirm_ack",
  MSGTYPE1376 = "medal_reset_confirm_req",
  MSGTYPE940 = "pay_sign_board_ntf",
  MSGTYPE678 = "chat_channel_switch_ntf",
  MSGTYPE1600 = "large_map_small_map_req",
  MSGTYPE1432 = "fleet_can_enhance_req",
  MSGTYPE1369 = "medal_store_buy_ack",
  MSGTYPE1140 = "reset_dice_ack",
  MSGTYPE1307 = "fair_arena_season_req",
  MSGTYPE1584 = "large_map_log_req",
  MSGTYPE715 = "financial_amount_ntf",
  MSGTYPE1205 = "art_point_ack",
  MSGTYPE642 = "budo_promotion_ntf",
  MSGTYPE711 = "financial_upgrade_req",
  MSGTYPE206 = "task_list_req",
  MSGTYPE639 = "budo_change_formation_ack",
  MSGTYPE1366 = "enter_medal_store_req",
  MSGTYPE264 = "server_kick_user_ntf",
  MSGTYPE719 = "facebook_friends_ntf",
  MSGTYPE970 = "story_status_ntf",
  MSGTYPE1363 = "exchange_regulation_ntf",
  MSGTYPE162 = "champion_challenge_req",
  MSGTYPE236 = "offline_changed_ntf",
  MSGTYPE1360 = "redownload_res_req",
  MSGTYPE1345 = "scratch_board_ntf",
  MSGTYPE1290 = "force_join_req",
  MSGTYPE1118 = "wdc_honor_wall_req",
  MSGTYPE866 = "daily_benefits_ack",
  MSGTYPE149 = "krypton_equip_off_req",
  MSGTYPE1354 = "get_login_sign_award_req",
  MSGTYPE1353 = "enter_login_sign_ack",
  MSGTYPE1352 = "enter_login_sign_req",
  MSGTYPE1351 = "change_fleets_req",
  MSGTYPE1350 = "change_fleets_data_ack",
  MSGTYPE1512 = "large_map_join_alliance_req",
  MSGTYPE1558 = "large_map_mass_invite_req",
  MSGTYPE1105 = "wdc_match_req",
  MSGTYPE1338 = "uc_pay_sign_req",
  MSGTYPE1810 = "cumulate_recharge_req",
  MSGTYPE1127 = "wdc_status_ntf",
  MSGTYPE1335 = "exchange_award_ack",
  MSGTYPE1333 = "get_process_award_ack",
  MSGTYPE443 = "alliance_defence_ntf",
  MSGTYPE811 = "prime_rank_ntf",
  MSGTYPE683 = "game_items_trans_req",
  MSGTYPE1332 = "get_process_award_req",
  MSGTYPE232 = "equipments_update_ntf",
  MSGTYPE461 = "client_fps_req",
  MSGTYPE326 = "supply_loot_ntf",
  MSGTYPE1331 = "reset_scratch_card_ack",
  MSGTYPE1328 = "buy_scratch_card_req",
  MSGTYPE1826 = "gcs_today_task_change_req",
  MSGTYPE1326 = "enter_scratch_req",
  MSGTYPE247 = "vip_info_ntf",
  MSGTYPE1325 = "fte_gift_ntf",
  MSGTYPE1324 = "price_off_store_ntf",
  MSGTYPE740 = "krypton_refine_info_ack",
  MSGTYPE1321 = "refresh_fleet_pool_ack",
  MSGTYPE440 = "colony_players_ack",
  MSGTYPE1317 = "fair_arena_formation_all_ack",
  MSGTYPE1314 = "fair_arena_report_battle_req",
  MSGTYPE735 = "krypton_refine_req",
  MSGTYPE654 = "special_efficacy_ntf",
  MSGTYPE230 = "supply_ntf",
  MSGTYPE299 = "mine_list_ntf",
  MSGTYPE721 = "push_button_req",
  MSGTYPE423 = "colony_users_ack",
  MSGTYPE403 = "stores_buy_req",
  MSGTYPE468 = "domination_challenge_ack",
  MSGTYPE1306 = "fair_arena_rank_ack",
  MSGTYPE1168 = "hero_union_req",
  MSGTYPE1576 = "large_map_local_leader_ack",
  MSGTYPE1304 = "fair_arena_ack",
  MSGTYPE1303 = "fair_arena_req",
  MSGTYPE1442 = "expedition_rank_awards_ack",
  MSGTYPE695 = "remodel_info_ntf",
  MSGTYPE1300 = "mining_team_log_req",
  MSGTYPE507 = "alliance_domination_status_ack",
  MSGTYPE1114 = "wdc_awards_req",
  MSGTYPE72 = "warpgate_charge_req",
  MSGTYPE1297 = "forces_ntf",
  MSGTYPE1293 = "concentrate_atk_ack",
  MSGTYPE199 = "user_fight_history_ack",
  MSGTYPE1292 = "concentrate_atk_req",
  MSGTYPE1288 = "concentrate_end_req",
  MSGTYPE1110 = "wdc_report_ack",
  MSGTYPE240 = "task_finished_ntf",
  MSGTYPE352 = "alliance_level_info_ntf",
  MSGTYPE1283 = "item_classify_ntf",
  MSGTYPE1588 = "large_map_search_alliance_ack",
  MSGTYPE1277 = "change_leader_req",
  MSGTYPE1151 = "user_change_req",
  MSGTYPE1182 = "mining_world_pe_req",
  MSGTYPE292 = "loud_cast_ntf",
  MSGTYPE1276 = "month_card_receive_reward_ack",
  MSGTYPE1275 = "month_card_receive_reward_req",
  MSGTYPE1272 = "vip_reward_ack",
  MSGTYPE1270 = "wish_pay_req",
  MSGTYPE1101 = "enter_champion_req",
  MSGTYPE1268 = "pay_gifts_req",
  MSGTYPE349 = "donate_ack",
  MSGTYPE109 = "fleet_repair_all_req",
  MSGTYPE597 = "contention_mission_info_req",
  MSGTYPE1204 = "art_point_req",
  MSGTYPE1265 = "fte_ntf",
  MSGTYPE1264 = "fte_ack",
  MSGTYPE1261 = "choosable_item_use_req",
  MSGTYPE631 = "budo_req",
  MSGTYPE1367 = "enter_medal_store_ack",
  MSGTYPE518 = "contention_production_ntf",
  MSGTYPE1257 = "helper_buttons_ack",
  MSGTYPE1254 = "space_door_req",
  MSGTYPE1425 = "medal_glory_collection_total_attr_req",
  MSGTYPE1188 = "mining_wars_refresh_req",
  MSGTYPE1214 = "art_reset_req",
  MSGTYPE1437 = "expedition_wormhole_rank_board_ack",
  MSGTYPE32 = "swap_bag_grid_req",
  MSGTYPE1405 = "medal_glory_activate_ack",
  MSGTYPE826 = "prime_award_ack",
  MSGTYPE1208 = "art_core_list_req",
  MSGTYPE142 = "krypton_enhance_req",
  MSGTYPE78 = "mail_page_req",
  MSGTYPE1267 = "achievement_reward_receive_ack",
  MSGTYPE1511 = "large_map_battle_req",
  MSGTYPE401 = "stores_ack",
  MSGTYPE720 = "financial_ntf",
  MSGTYPE1197 = "mining_world_battle_ack",
  MSGTYPE263 = "create_tipoff_req",
  MSGTYPE1803 = "teamleague_matrix_save_req",
  MSGTYPE1192 = "mining_wars_battle_ack",
  MSGTYPE1191 = "mining_wars_battle_req",
  MSGTYPE306 = "mine_info_req",
  MSGTYPE1189 = "mining_wars_star_req",
  MSGTYPE1217 = "art_core_decompose_ack",
  MSGTYPE1187 = "mining_wars_buy_count_ack",
  MSGTYPE143 = "krypton_enhance_ack",
  MSGTYPE1186 = "mining_wars_buy_count_req",
  MSGTYPE1180 = "mining_world_log_req",
  MSGTYPE1004 = "client_req_stat_req",
  MSGTYPE1171 = "hero_collect_ack",
  MSGTYPE476 = "wd_star_req",
  MSGTYPE832 = "champion_promote_ntf",
  MSGTYPE107 = "user_collect_offline_expr_req",
  MSGTYPE1167 = "master_ack",
  MSGTYPE1158 = "adventure_add_ack",
  MSGTYPE1166 = "master_req",
  MSGTYPE624 = "crusade_first_req",
  MSGTYPE727 = "get_energy_req",
  MSGTYPE644 = "battle_report_req",
  MSGTYPE1162 = "recruit_req",
  MSGTYPE1161 = "recruit_list_new_ack",
  MSGTYPE506 = "alliance_domination_status_req",
  MSGTYPE1830 = "gcs_get_week_task_reward_req",
  MSGTYPE1150 = "event_activity_award_req",
  MSGTYPE686 = "budo_champion_ack",
  MSGTYPE1149 = "event_activity_ack",
  MSGTYPE1147 = "event_activity_ntf",
  MSGTYPE71 = "warpgate_status_ack",
  MSGTYPE1146 = "speed_resource_ntf",
  MSGTYPE1145 = "wdc_rank_board_ntf",
  MSGTYPE1139 = "reset_dice_req",
  MSGTYPE1138 = "dice_awards_req",
  MSGTYPE386 = "fleet_sale_with_rebate_list_req",
  MSGTYPE514 = "contention_name_ntf",
  MSGTYPE891 = "activity_store_price_ack",
  MSGTYPE1362 = "open_box_progress_award_req",
  MSGTYPE1133 = "first_charge_ntf",
  MSGTYPE1131 = "welcome_screen_ack",
  MSGTYPE1130 = "welcome_screen_req",
  MSGTYPE1821 = "sync_server_time_req",
  MSGTYPE669 = "mail_to_all_req",
  MSGTYPE1124 = "wdc_rankup_ntf",
  MSGTYPE277 = "battle_money_collect_req",
  MSGTYPE116 = "alliances_req",
  MSGTYPE1122 = "wdc_awards_get_req",
  MSGTYPE1120 = "wdc_enemy_release_req",
  MSGTYPE1119 = "wdc_honor_wall_ack",
  MSGTYPE454 = "krypton_info_req",
  MSGTYPE1298 = "my_forces_req",
  MSGTYPE1113 = "wdc_info_ntf",
  MSGTYPE945 = "plante_explore_ack",
  MSGTYPE1112 = "wdc_report_detail_ack",
  MSGTYPE1109 = "wdc_report_req",
  MSGTYPE233 = "attributes_change_ntf",
  MSGTYPE393 = "promotion_award_ntf",
  MSGTYPE57 = "recruit_fleet_ack",
  MSGTYPE1108 = "wdc_fight_ack",
  MSGTYPE1103 = "wdc_enter_req",
  MSGTYPE939 = "pay_limit_ntf",
  MSGTYPE830 = "prestige_info_ntf",
  MSGTYPE1173 = "hero_collect_rank_ack",
  MSGTYPE998 = "mulmatrix_switch_ack",
  MSGTYPE935 = "tactical_enter_refine_ack",
  MSGTYPE957 = "alliance_guid_ntf",
  MSGTYPE371 = "promotion_award_req",
  MSGTYPE117 = "alliances_ack",
  MSGTYPE1812 = "cumulate_recharge_award_get_req",
  MSGTYPE990 = "get_today_award_req",
  MSGTYPE987 = "redo_task_ack",
  MSGTYPE879 = "enter_seven_day_ack",
  MSGTYPE184 = "alliance_transfer_ack",
  MSGTYPE1159 = "adventure_add_price_req",
  MSGTYPE870 = "goal_info_req",
  MSGTYPE873 = "cutoff_info_req",
  MSGTYPE978 = "today_act_task_req",
  MSGTYPE609 = "activity_dna_req",
  MSGTYPE977 = "credit_exchange_ack",
  MSGTYPE975 = "thumb_up_req",
  MSGTYPE442 = "alliance_defence_req",
  MSGTYPE1365 = "mul_credit_exchange_ntf",
  MSGTYPE956 = "first_login_ntf",
  MSGTYPE955 = "tactical_delete_awards_ack",
  MSGTYPE444 = "alliance_set_defender_req",
  MSGTYPE628 = "contention_income_req",
  MSGTYPE1582 = "large_map_alliance_adjust_req",
  MSGTYPE950 = "get_invite_gift_ack",
  MSGTYPE556 = "fleet_weaken_req",
  MSGTYPE413 = "verify_redeem_code_ack",
  MSGTYPE1730 = "champion_cdtime_ack",
  MSGTYPE1212 = "art_core_equip_req",
  MSGTYPE406 = "mail_to_alliance_req",
  MSGTYPE737 = "krypton_inject_req",
  MSGTYPE941 = "tactical_guide_req",
  MSGTYPE33 = "swap_bag_grid_ack",
  MSGTYPE307 = "mine_reset_atk_cd_req",
  MSGTYPE156 = "alliance_refuse_ack",
  MSGTYPE694 = "remodel_info_ack",
  MSGTYPE1375 = "medal_decompose_confirm_ack",
  MSGTYPE549 = "ladder_rank_ack",
  MSGTYPE997 = "mulmatrix_switch_req",
  MSGTYPE671 = "champion_matrix_ack",
  MSGTYPE933 = "tactical_unlock_slot_ack",
  MSGTYPE469 = "domination_awards_list_req",
  MSGTYPE1559 = "large_map_mass_invite_ack",
  MSGTYPE905 = "ladder_award_ntf",
  MSGTYPE157 = "alliance_memo_req",
  MSGTYPE901 = "ladder_report_ack",
  MSGTYPE898 = "max_step_req",
  MSGTYPE896 = "refresh_type_ack",
  MSGTYPE647 = "get_my_support_ack",
  MSGTYPE465 = "alliance_domination_ntf",
  MSGTYPE894 = "prime_push_show_round_req",
  MSGTYPE893 = "client_login_ok_req",
  MSGTYPE548 = "ladder_rank_req",
  MSGTYPE880 = "prime_jump_price_req",
  MSGTYPE1820 = "pop_firstpay_on_lvup_ntf",
  MSGTYPE366 = "active_gifts_ntf",
  MSGTYPE980 = "enter_monthly_req",
  MSGTYPE871 = "goal_info_ack",
  MSGTYPE984 = "view_award_req",
  MSGTYPE594 = "award_get_req",
  MSGTYPE867 = "daily_benefits_award_req",
  MSGTYPE1356 = "krypton_redpoint_req",
  MSGTYPE167 = "champion_reset_cd_ack",
  MSGTYPE865 = "daily_benefits_req",
  MSGTYPE864 = "pay_status_ntf",
  MSGTYPE580 = "mulmatrix_buy_req",
  MSGTYPE845 = "open_server_fund_award_req",
  MSGTYPE860 = "enhance_info_ack",
  MSGTYPE856 = "prime_round_fight_ntf",
  MSGTYPE1302 = "team_fight_report_req",
  MSGTYPE849 = "vip_level_ntf",
  MSGTYPE847 = "all_welfare_ack",
  MSGTYPE842 = "open_server_fund_req",
  MSGTYPE622 = "crusade_buy_cross_req",
  MSGTYPE839 = "pay_sign_reward_req",
  MSGTYPE837 = "pay_sign_req",
  MSGTYPE831 = "levelup_unlock_ntf",
  MSGTYPE829 = "krypton_sort_ack",
  MSGTYPE621 = "crusade_reward_ack",
  MSGTYPE823 = "prime_last_report_req",
  MSGTYPE800 = "prime_map_req",
  MSGTYPE822 = "prime_fight_report_req",
  MSGTYPE819 = "prime_increase_power_info_ack",
  MSGTYPE817 = "revive_self_req",
  MSGTYPE1718 = "tlc_honor_wall_req",
  MSGTYPE50 = "officer_buy_ack",
  MSGTYPE543 = "ladder_gain_req",
  MSGTYPE810 = "prime_march_req",
  MSGTYPE808 = "prime_dot_status_ntf",
  MSGTYPE384 = "timelimit_hero_id_req",
  MSGTYPE804 = "prime_jump_req",
  MSGTYPE803 = "prime_active_ntf",
  MSGTYPE1420 = "challenge_medal_boss_req",
  MSGTYPE748 = "user_info_ack",
  MSGTYPE532 = "dungeon_open_req",
  MSGTYPE744 = "krypton_inject_list_ack",
  MSGTYPE739 = "krypton_refine_info_req",
  MSGTYPE346 = "block_devil_req",
  MSGTYPE944 = "plante_explore_req",
  MSGTYPE135 = "alliance_info_req",
  MSGTYPE169 = "champion_get_award_ack",
  MSGTYPE407 = "chat_history_async_req",
  MSGTYPE736 = "krypton_refine_ack",
  MSGTYPE115 = "message_del_req",
  MSGTYPE729 = "open_treasure_box_ack",
  MSGTYPE728 = "open_treasure_box_req",
  MSGTYPE1705 = "tlc_match_req",
  MSGTYPE725 = "treasure_info_ntf",
  MSGTYPE713 = "facebook_friends_ack",
  MSGTYPE1200 = "mining_reward_ntf",
  MSGTYPE712 = "facebook_friends_req",
  MSGTYPE707 = "give_friends_gift_ack",
  MSGTYPE450 = "fleet_info_req",
  MSGTYPE61 = "friends_ack",
  MSGTYPE503 = "contention_revive_req",
  MSGTYPE220 = "achievement_list_ack",
  MSGTYPE691 = "version_code_ntf",
  MSGTYPE34 = "techniques_req",
  MSGTYPE131 = "alliance_apply_ack",
  MSGTYPE105 = "user_offline_info_req",
  MSGTYPE1422 = "medal_glory_achievement_red_point_ntf",
  MSGTYPE350 = "donate_info_req",
  MSGTYPE620 = "crusade_reward_req",
  MSGTYPE702 = "remodel_help_ntf",
  MSGTYPE1804 = "tlc_matrix_change_ntf",
  MSGTYPE697 = "remodel_help_req",
  MSGTYPE1518 = "large_map_create_alliance_req",
  MSGTYPE690 = "apply_limit_update_req",
  MSGTYPE689 = "client_version_ntf",
  MSGTYPE1193 = "mining_wars_collect_req",
  MSGTYPE434 = "activity_stores_buy_req",
  MSGTYPE573 = "user_notifies_req",
  MSGTYPE332 = "ac_energy_charge_req",
  MSGTYPE355 = "champion_top_record_ack",
  MSGTYPE673 = "release_adjutant_req",
  MSGTYPE934 = "tactical_enter_refine_req",
  MSGTYPE670 = "champion_matrix_req",
  MSGTYPE662 = "stores_refresh_req",
  MSGTYPE652 = "get_promotion_reward_req",
  MSGTYPE649 = "get_budo_rank_ack",
  MSGTYPE584 = "equip_info_req",
  MSGTYPE636 = "budo_join_ack",
  MSGTYPE1164 = "recruit_look_req",
  MSGTYPE643 = "support_one_player_req",
  MSGTYPE640 = "budo_promotion_req",
  MSGTYPE42 = "revenue_do_req",
  MSGTYPE630 = "contention_missiles_ntf",
  MSGTYPE637 = "budo_mass_election_ntf",
  MSGTYPE197 = "krypton_fleet_equip_req",
  MSGTYPE1260 = "choosable_item_ack",
  MSGTYPE627 = "dna_gacha_ntf",
  MSGTYPE625 = "crusade_first_ack",
  MSGTYPE703 = "remodel_push_ntf",
  MSGTYPE614 = "crusade_enter_ack",
  MSGTYPE613 = "crusade_enter_req",
  MSGTYPE1816 = "login_fund_reward_req",
  MSGTYPE608 = "enchant_ack",
  MSGTYPE1545 = "large_map_alliance_expel_ack",
  MSGTYPE1599 = "large_map_vision_change_ntf",
  MSGTYPE405 = "equipment_action_list_ack",
  MSGTYPE578 = "mulmatrix_get_ack",
  MSGTYPE570 = "pay_confirm_req",
  MSGTYPE560 = "fleet_show_ack",
  MSGTYPE948 = "show_price_ntf",
  MSGTYPE266 = "update_spell_ntf",
  MSGTYPE551 = "ladder_special_ack",
  MSGTYPE544 = "ladder_buy_search_req",
  MSGTYPE304 = "mine_complete_ntf",
  MSGTYPE534 = "dungeon_enter_req",
  MSGTYPE747 = "user_info_req",
  MSGTYPE1374 = "medal_decompose_confirm_req",
  MSGTYPE513 = "contention_battle_ntf",
  MSGTYPE512 = "activity_task_list_ntf",
  MSGTYPE510 = "activity_task_list_ack",
  MSGTYPE508 = "domination_buffer_of_rank_ntf",
  MSGTYPE505 = "contention_stop_ack",
  MSGTYPE504 = "contention_stop_req",
  MSGTYPE706 = "remodel_event_ntf",
  MSGTYPE500 = "contention_trip_ntf",
  MSGTYPE499 = "contention_info_ntf",
  MSGTYPE498 = "contention_notifies_req",
  MSGTYPE495 = "set_game_local_req",
  MSGTYPE953 = "can_enhance_ack",
  MSGTYPE457 = "items_ack",
  MSGTYPE486 = "wd_ranking_champion_ntf",
  MSGTYPE333 = "ac_energy_charge_ack",
  MSGTYPE100 = "shop_sell_ack",
  MSGTYPE480 = "alliance_domination_total_rank_ntf",
  MSGTYPE477 = "wd_star_ack",
  MSGTYPE475 = "alliance_defence_revive_req",
  MSGTYPE201 = "user_brief_info_ack",
  MSGTYPE932 = "tactical_unlock_slot_req",
  MSGTYPE462 = "taobao_trade_req",
  MSGTYPE418 = "colony_info_ntf",
  MSGTYPE319 = "laba_info_req",
  MSGTYPE458 = "client_effect_req",
  MSGTYPE487 = "open_box_info_req",
  MSGTYPE280 = "quest_req",
  MSGTYPE298 = "mine_boost_req",
  MSGTYPE489 = "open_box_open_req",
  MSGTYPE441 = "prestige_get_req",
  MSGTYPE431 = "colony_fawn_ack",
  MSGTYPE428 = "colony_exploit_req",
  MSGTYPE426 = "colony_info_test_req",
  MSGTYPE425 = "colony_challenge_ack",
  MSGTYPE420 = "colony_times_ntf",
  MSGTYPE179 = "krypton_store_swap_req",
  MSGTYPE411 = "festival_ack",
  MSGTYPE583 = "mulmatrix_price_ack",
  MSGTYPE1311 = "fair_arena_reward_get_req",
  MSGTYPE400 = "stores_req",
  MSGTYPE391 = "cut_off_list_ack",
  MSGTYPE242 = "passport_bind_req",
  MSGTYPE380 = "clean_world_boss_cd_req",
  MSGTYPE372 = "promotion_award_ack",
  MSGTYPE994 = "today_task_ntf",
  MSGTYPE680 = "fleet_dismiss_ack",
  MSGTYPE353 = "login_first_ntf",
  MSGTYPE113 = "messages_req",
  MSGTYPE1 = "user_register_req",
  MSGTYPE599 = "contention_mission_award_req",
  MSGTYPE337 = "gateway_info_ntf",
  MSGTYPE335 = "ac_battle_info_req",
  MSGTYPE322 = "center_ntf",
  MSGTYPE316 = "laba_req",
  MSGTYPE315 = "guide_progress_ntf",
  MSGTYPE283 = "adventure_map_status_req",
  MSGTYPE274 = "all_act_status_ack",
  MSGTYPE852 = "pay_push_pop_set_req",
  MSGTYPE1194 = "mining_wars_collect_ack",
  MSGTYPE246 = "act_status_ack",
  MSGTYPE1285 = "forces_ack",
  MSGTYPE1134 = "enter_dice_req",
  MSGTYPE215 = "prestige_info_ack",
  MSGTYPE268 = "pve_awards_get_req",
  MSGTYPE198 = "user_fight_history_req",
  MSGTYPE417 = "game_addtion_req",
  MSGTYPE1583 = "large_map_alliance_adjust_ack",
  MSGTYPE68 = "friend_search_ack"
}
function NetAPIList:getDataStructFromMsgType(msgType)
  return self.CodeToMsgType[msgType]
end
