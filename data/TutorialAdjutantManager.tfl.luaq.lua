local TutorialAdjutantManager = LuaObjectManager:GetLuaObject("TutorialAdjutantManager")
local QuestTutorialAdjutantStoryPlay = TutorialQuestManager.QuestTutorialAdjutantStoryPlay
local QuestTutorialAdjutantBind = TutorialQuestManager.QuestTutorialAdjutantBind
local GameStateMainPlanet = GameStateManager.GameStateMainPlanet
local GameUICommonDialog = LuaObjectManager:GetLuaObject("GameUICommonDialog")
local GameUIBarRight = LuaObjectManager:GetLuaObject("GameUIBarRight")
local GameFleetNewEnhance = LuaObjectManager:GetLuaObject("GameFleetNewEnhance")
local GameUIAdjutant = LuaObjectManager:GetLuaObject("GameUIAdjutant")
local GameFleetEquipment = LuaObjectManager:GetLuaObject("GameFleetEquipment")
TutorialAdjutantManager.AdjutantTutorialFleetID = 177
function TutorialAdjutantManager:CheckActiveTutorialAdjutantStoryPlay()
  local levelinfo = GameGlobalData:GetData("levelinfo")
  if levelinfo and tonumber(levelinfo.level) >= 45 and not QuestTutorialAdjutantStoryPlay:IsFinished() and not QuestTutorialAdjutantStoryPlay:IsActive() then
    if self:IsPlayerHadAdjutant() then
      QuestTutorialAdjutantStoryPlay:SetFinish(true)
      QuestTutorialAdjutantBind:SetFinish(true)
      self:RequestAddAdjutantFleet(2)
      return
    end
    DebugOut("TutorialAdjutantManager: activity adjutant story tutorial")
    QuestTutorialAdjutantStoryPlay:SetActive(true)
    if GameStateManager:GetCurrentGameState() ~= GameStateMainPlanet then
      DebugOut("not in main planet")
      GameStateManager:SetCurrentGameState(GameStateMainPlanet)
    else
      DebugOut("in main planet")
      self:CheckStartTutorialAdjutantStoryPlay()
    end
  end
end
function TutorialAdjutantManager:CheckStartTutorialAdjutantStoryPlay()
  DebugOut("TutorialAdjutantManager: start adjutant story tutorial")
  if QuestTutorialAdjutantStoryPlay:IsActive() and not QuestTutorialAdjutantStoryPlay:IsFinished() then
    local function callback()
      if QuestTutorialAdjutantStoryPlay:IsActive() and not QuestTutorialAdjutantStoryPlay:IsFinished() then
        DebugOut("TutorialAdjutantManager: adjutant story tutorial finished")
        QuestTutorialAdjutantStoryPlay:SetFinish(true)
      end
      if not QuestTutorialAdjutantBind:IsFinished() and not QuestTutorialAdjutantBind:IsActive() then
        DebugOut("TutorialAdjutantManager: activity bind tutorial")
        QuestTutorialAdjutantBind:SetActive(true)
        if GameUIBarRight:GetFlashObject() then
          self:StartTutorialAdjutantBind_UIBarRight()
        end
      end
    end
    DebugOut("play story")
    GameUICommonDialog:PlayStory({1100082, 1100083}, callback)
  end
end
function TutorialAdjutantManager:IsTutorialAdjutantBindActive()
  if QuestTutorialAdjutantBind:IsActive() and not QuestTutorialAdjutantBind:IsFinished() then
    return true
  end
  return false
end
function TutorialAdjutantManager:StartTutorialAdjutantBind_UIBarRight()
  if self:IsTutorialAdjutantBindActive() then
    GameUIBarRight:MoveInRightMenu()
  end
end
function TutorialAdjutantManager:StartTutorialAdjutantBind_UIBarRightEquip()
  if self:IsTutorialAdjutantBindActive() then
    GameUIBarRight:ShowTutorialEquip()
  end
end
function TutorialAdjutantManager:StartTutorialAdjutantBind_NewEnhance(select_fleet_id)
  if self:IsTutorialAdjutantBindActive() and select_fleet_id == 1 then
    ZhenXinUI():SetAdjutantRedpoint(true)
    self.showAdjutantTutorialInNewEnhance = true
  else
    self.showAdjutantTutorialInNewEnhance = false
  end
end
function TutorialAdjutantManager:ReadyTutorialAdjutantBind_Adjutant(callback)
  self.adjutantCallback = nil
  self.dismissFleets = nil
  if self:IsTutorialAdjutantBindActive() and self.showAdjutantTutorialInNewEnhance then
    if self:HasFleetId(self.AdjutantTutorialFleetID) then
      DebugOut("HasFleetId")
      if self:IsFleetOnBattle(self.AdjutantTutorialFleetID) then
        self.adjutantCallback = callback
        NetMessageMgr:SendMsg(NetAPIList.mulmatrix_get_req.Code, {type = 1}, TutorialAdjutantManager.requestMultiMatrixCallBack, true)
        return true
      end
    else
      DebugOut("request add fleet 177")
      self.adjutantCallback = callback
      self.addNewFleet = false
      self:RequestAddAdjutantFleet(1)
      return true
    end
  end
  return false
end
function TutorialAdjutantManager:RequestAddAdjutantFleet(type)
  local param = {}
  param.type = type
  NetMessageMgr:SendMsg(NetAPIList.force_add_fleet_req.Code, param, TutorialAdjutantManager.RequestAddAdjutantFleetCallback, true, nil)
end
function TutorialAdjutantManager.RequestDismissHeroList()
  DebugOut("TutorialAdjutantManager RequestDismissHeroList ")
  TutorialAdjutantManager.dismissFleets = {}
  NetMessageMgr:SendMsg(NetAPIList.fleet_dismiss_req.Code, nil, TutorialAdjutantManager.RequestDismissHeroListCallback, true, nil)
end
function TutorialAdjutantManager.RequestDismissHeroListCallback(msgType, content)
  DebugOut("RequestDismissHeroListCallback:", msgType)
  DebugTable(content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.fleet_dismiss_req.Code then
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.fleet_dismiss_ack.Code then
    if content.fleets then
      TutorialAdjutantManager.dismissFleets = content.fleets
    end
    return true
  end
  return false
end
function TutorialAdjutantManager:CheckRefreshNewEnhance()
  DebugOut("CheckRefreshNewEnhance:", GameStateManager:GetCurrentGameState(), ",", GameStateManager.GameStateEquipEnhance)
  if self.addNewFleet and GameStateManager:GetCurrentGameState() == GameStateManager.GameStateEquipEnhance then
    self.addNewFleet = false
  end
end
function TutorialAdjutantManager.RequestAddAdjutantFleetCallback(msgType, content)
  local result = false
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.force_add_fleet_req.Code then
    result = true
  elseif msgType == NetAPIList.force_add_fleet_req.Code then
    result = true
  end
  if result and TutorialAdjutantManager.adjutantCallback then
    TutorialAdjutantManager.RequestFleetsInfo()
    TutorialAdjutantManager.RequestDismissHeroList()
  end
  DebugOut("RequestAddAdjutantFleetCallback:", msgType, ",", result)
  DebugTable(content)
  return result
end
function TutorialAdjutantManager.RequestFleetsInfo()
  NetMessageMgr:SendMsg(NetAPIList.fleets_req.Code, nil, TutorialAdjutantManager.RequestFleetsInfoCallback, true, nil)
end
function TutorialAdjutantManager.RequestFleetsInfoCallback(msgType, content)
  DebugOut("RequestFleetsInfoCallback:", msgType)
  DebugTable(content)
  local result = false
  if msgType == NetAPIList.fleets_ack.Code then
    result = true
  elseif msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.fleets_req.Code then
    result = true
  end
  if result and TutorialAdjutantManager.adjutantCallback then
    GameGlobalData.GlobalData.fleetinfo.fleets = {}
    GameGlobalData.UpdateFleets(content)
    TutorialAdjutantManager.addNewFleet = true
    TutorialAdjutantManager.adjutantCallback()
  end
  return result
end
function TutorialAdjutantManager:ShowTutorialAdjutantBind_ShowInFirst()
  self.showAwatar = false
  self.showAwatarItemID = 1
  if self:IsTutorialAdjutantBindActive() and self.showAdjutantTutorialInNewEnhance and GameUIAdjutant.mCurSelectedHeroIdx == 1 then
    for k = 1, #GameUIAdjutant.mCurAdjutantList do
      if self.AdjutantTutorialFleetID == GameUIAdjutant.mCurAdjutantList[k] then
        local curAdjutant = GameUIAdjutant.mCurAdjutantList[k]
        table.remove(GameUIAdjutant.mCurAdjutantList, k)
        table.insert(GameUIAdjutant.mCurAdjutantList, 1, curAdjutant)
        break
      end
    end
  end
end
function TutorialAdjutantManager:ShowTutorialAdjutantBind_Awatar(item_id)
  DebugOut("ShowTutorialAdjutantBind_Awatar:", item_id, ",", self:IsTutorialAdjutantBindActive(), ",", self.showAdjutantTutorialInNewEnhance, ",", GameUIAdjutant.mCurSelectedHeroIdx)
  if self:IsTutorialAdjutantBindActive() and self.showAdjutantTutorialInNewEnhance and GameUIAdjutant.mCurSelectedHeroIdx == 1 and item_id == 1 then
    GameUIAdjutant:GetFlashObject():InvokeASCallback("_root", "ShowAdjutantTutorialItem", item_id)
    self.showAwatar = true
    self.showAwatarItemID = item_id
  end
end
function TutorialAdjutantManager:ShowTutorialAdjutantBind_BindBtn(CurSelectAdjutantIdx)
  DebugOut("ShowTutorialAdjutantBind_BindBtn:", self:IsTutorialAdjutantBindActive(), ",", self.showAwatar, ",", CurSelectAdjutantIdx)
  self.showBindBtn = false
  if self:IsTutorialAdjutantBindActive() and self.showAwatar and CurSelectAdjutantIdx == 1 then
    self.showBindBtn = true
    GameUIAdjutant:GetFlashObject():InvokeASCallback("_root", "SetTutorialBindBtnVisible", true)
    GameUIAdjutant:GetFlashObject():InvokeASCallback("_root", "HideAdjutantTutorialItem", self.showAwatarItemID)
  else
    GameUIAdjutant:GetFlashObject():InvokeASCallback("_root", "SetTutorialBindBtnVisible", false)
  end
end
function TutorialAdjutantManager:CheckFinishedTutorialAdjutantBind()
  DebugOut("CheckFinishedTutorialAdjutantBind:", self:IsTutorialAdjutantBindActive(), ",", self.showAwatar, ",", self.showBindBtn)
  if self:IsTutorialAdjutantBindActive() then
    GameUIAdjutant:GetFlashObject():InvokeASCallback("_root", "SetTutorialBindBtnVisible", false)
    QuestTutorialAdjutantBind:SetFinish(true)
    DebugOut("PlayStory")
    GameUICommonDialog:PlayStory({1100084}, nil)
    DebugTable(GameGlobalData:GetData("fleetinfo").fleets)
    if not self:HasFleetId(self.AdjutantTutorialFleetID) then
      DebugOut("RequestAddAdjutantFleet 2")
      self:RequestAddAdjutantFleet(2)
    end
  end
end
function TutorialAdjutantManager:HasFleetId(fleet_id)
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  for k, v in pairs(fleets) do
    if v.identity == fleet_id then
      return true
    end
  end
  DebugOut("HasFleetId:", self.dismissFleets)
  if self.dismissFleets then
    for k, v in pairs(self.dismissFleets) do
      if v.identity == fleet_id then
        return true
      end
    end
  end
  return false
end
function TutorialAdjutantManager:IsFleetOnBattle(fleet_id)
  local matrixCells = GameGlobalData:GetData("matrix").cells
  if matrixCells and #matrixCells > 0 then
    for k = 1, #matrixCells do
      if fleet_id == matrixCells[k].fleet_identity then
        return true
      end
    end
  end
  return false
end
function TutorialAdjutantManager.requestMultiMatrixCallBack(msgType, content)
  DebugOut("requestMultiMatrixCallBack:", msgType)
  DebugTable(content)
  if msgType == NetAPIList.mulmatrix_get_ack.Code then
    TutorialAdjutantManager.currentMatrixIndex = content.cur_index
    TutorialAdjutantManager:CancleFleetOnBattle(TutorialAdjutantManager.AdjutantTutorialFleetID)
    return true
  end
  if TutorialAdjutantManager.adjutantCallback then
    TutorialAdjutantManager.adjutantCallback()
  end
  return false
end
function TutorialAdjutantManager:CancleFleetOnBattle(fleet_id)
  local battle_matrix = {}
  for _, v in ipairs(GameGlobalData:GetData("matrix").cells) do
    battle_matrix[v.cell_id] = v.fleet_identity
  end
  local formation = {}
  formation.cells = {}
  formation.count = 1
  for i = 1, 9 do
    local data = {}
    data.cell_id = i
    commander_id = battle_matrix[i]
    if commander_id == nil or commander_id == -1 or commander_id == fleet_id then
      data.fleet_identity = -1
      data.fleet_spell = -1
      table.insert(formation.cells, data)
    elseif commander_id > 0 then
      local fleet_info = GameGlobalData:GetFleetInfo(commander_id)
      data.fleet_identity = fleet_info.identity
      data.fleet_spell = fleet_info.active_spell
      table.insert(formation.cells, data)
    end
  end
  NetMessageMgr:SendMsg(NetAPIList.user_matrix_save_req.Code, formation, self.CancleFleetOnBattleCallback, true, nil)
  local currentMatrixOne = {}
  currentMatrixOne.index = self.currentMatrixIndex
  currentMatrixOne.cells = {}
  for i_cell = 1, 9 do
    local matrixCell = {}
    matrixCell.cell_id = i_cell
    matrixCell.fleet_spell = -1
    matrixCell.fleet_identity = battle_matrix[i_cell]
    if matrixCell.fleet_identity ~= nil and matrixCell.fleet_identity ~= -1 and matrixCell.fleet_identity ~= 0 and matrixCell.fleet_identity ~= fleet_id then
      table.insert(currentMatrixOne.cells, matrixCell)
    end
  end
  DebugOut("TutorialAdjutantManager.requestSaveMatrix")
  DebugTable(currentMatrixOne)
  NetMessageMgr:SendMsg(NetAPIList.mulmatrix_save_req.Code, currentMatrixOne, TutorialAdjutantManager.requestSaveMatrixCallBack, false)
end
function TutorialAdjutantManager.requestSaveMatrixCallBack(msgType, content)
  DebugOut("TutorialAdjutantManager.requestSaveMatrixCallBack:", msgType)
  DebugTable(content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.mulmatrix_save_req.Code then
    return true
  end
  return false
end
function TutorialAdjutantManager.CancleFleetOnBattleCallback(msgType, content)
  DebugOut("CancleFleetOnBattleCallback:", msgType)
  DebugTable(content)
  DebugTable(GameGlobalData:GetData("matrix").cells)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.user_matrix_save_req.Code then
    DebugOut(TutorialAdjutantManager.adjutantCallback)
    if TutorialAdjutantManager.adjutantCallback then
      TutorialAdjutantManager.adjutantCallback()
    end
    return true
  end
  return false
end
function TutorialAdjutantManager:IsFleetHasAdjutant(fleet_id)
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  for k, v in pairs(fleets) do
    if v.identity == fleet_id then
      if v.cur_adjutant > 0 then
        return true
      else
        return false
      end
    end
  end
  return false
end
function TutorialAdjutantManager:IsPlayerHadAdjutant()
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  for k, v in pairs(fleets) do
    if v.cur_adjutant > 0 then
      return true
    else
      return false
    end
  end
  return false
end
function TutorialAdjutantManager:CancleAdjutantOfFleet(fleet_id)
  local req = {major = fleet_id}
  NetMessageMgr:SendMsg(NetAPIList.release_adjutant_req.Code, req, self.CancleAdjutantOfFleetCallback, true, nil)
end
function TutorialAdjutantManager.CancleAdjutantOfFleetCallback(msgType, content)
  DebugOut("CancleAdjutantOfFleetCallback:", msgType)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.release_adjutant_req.Code then
    if TutorialAdjutantManager.adjutantCallback then
      TutorialAdjutantManager.adjutantCallback()
    end
    return true
  end
  return false
end
