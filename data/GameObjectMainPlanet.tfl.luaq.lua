local GameUIBarRight = LuaObjectManager:GetLuaObject("GameUIBarRight")
local QuestTutorialsFirstKillBoss = TutorialQuestManager.QuestTutorialsFirstKillBoss
local GameUIBarMiddle = LuaObjectManager:GetLuaObject("GameUIBarMiddle")
local GameObjectMainPlanet = LuaObjectManager:GetLuaObject("GameObjectMainPlanet")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameUICommonDialog = LuaObjectManager:GetLuaObject("GameUICommonDialog")
local GameWaiting = LuaObjectManager:GetLuaObject("GameWaiting")
local GameUIActivity = LuaObjectManager:GetLuaObject("GameUIActivity")
local GameVip = LuaObjectManager:GetLuaObject("GameVip")
local GameStateMainPlanet = GameStateManager.GameStateMainPlanet
local GameGlobalData = GameGlobalData
local NetAPIList = NetAPIList
local AlertDataList = AlertDataList
local isMiddleUIShowing = false
local GameStateConsortium = GameStateManager.GameStateConsortium
local GameUIConsortium = LuaObjectManager:GetLuaObject("GameUIConsortium")
local WVEGameManeger = LuaObjectManager:GetLuaObject("WVEGameManeger")
local FacebookPopUI = LuaObjectManager:GetLuaObject("FacebookPopUI")
local GameUIMaskLayer = LuaObjectManager:GetLuaObject("GameUIMaskLayer")
local GameUILab = LuaObjectManager:GetLuaObject("GameUILab")
GameObjectMainPlanet.lastWDUpdateTime = -1
GameObjectMainPlanet.PrimeWveStatus = false
local k_fakeBuildNames = {
  "krypton_center",
  "commander_academy",
  "affairs_hall",
  "factory",
  "tech_lab",
  "star_portal",
  "engineering_bay",
  "planetary_fortress",
  "finacial_entry",
  "Command_post"
}
function GameObjectMainPlanet:OnInitGame()
  GameGlobalData:RegisterDataChangeCallback("buildings", self.buildingsListener)
  GameGlobalData:RegisterDataChangeCallback("modules_status", self.OnMenuStatusChange)
end
function GameObjectMainPlanet:InitFlashObject()
  local status = GameGlobalData:GetModuleStatus("prime_wve")
  local data = {}
  data.prime_wve = {}
  data.prime_wve.status = status
  GameObjectMainPlanet.PrimeWveStatus = status
  DebugOut("GameObjectMainPlanet:InitFlashObject:", data.prime_wve.status)
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "initMainCityFlash", data)
  end
end
function GameObjectMainPlanet:SetUpforOptimize()
  if self:GetFlashObject() then
    DebugOut("set for device")
    self:GetFlashObject():InvokeASCallback("_root", "lua2fs_setUpforOptimize", isLowMemDevice, g_deviceName)
  end
end
function GameObjectMainPlanet:ShowFakePlanet()
  self:LoadFlashObject()
  self:SetMapOffset()
  GameStateManager:GetCurrentGameState():AddObject(self)
  GameObjectMainPlanet.lastWDUpdateTime = -1
  for i, buildName in ipairs(k_fakeBuildNames) do
    self:GetFlashObject():InvokeASCallback("_root", "lua2fs_setBuidingStatus", buildName, "Normal")
  end
end
function GameObjectMainPlanet:ShowFBBall()
  if self:GetFlashObject() then
    GameStateMainPlanet.mFBBallState = GameStateMainPlanet.FBBALL_STATE.BALL_STATE_SNAP
    self:GetFlashObject():InvokeASCallback("_root", "showFBBall")
  end
end
function GameObjectMainPlanet:HideFBBall()
  if self:GetFlashObject() then
    GameStateMainPlanet.mFBBallState = GameStateMainPlanet.FBBALL_STATE.BALL_STATE_HIDE
    self:GetFlashObject():InvokeASCallback("_root", "hideFBBall")
  end
end
function GameObjectMainPlanet:FBBallSnapToScreen()
  if self:GetFlashObject() then
    GameStateMainPlanet.mFBBallState = GameStateMainPlanet.FBBALL_STATE.BALL_STATE_SNAP
    self:GetFlashObject():InvokeASCallback("_root", "fbBallSnapToScreen")
  end
end
function GameObjectMainPlanet:HideFakePlanet()
  GameObjectMainPlanet:UnloadFlashObject()
  GameStateManager:GetCurrentGameState():EraseObject(self)
end
function GameObjectMainPlanet:SetMapOffset()
  local moveYtoTC = -40
  if ext.is16x9() then
    moveYtoTC = -105
  end
  if ext.is4x3() then
    moveYtoTC = 0
  end
  if ext.is2x1 and ext.is2x1() then
    moveYtoTC = -105
  end
  local status = GameGlobalData:GetModuleStatus("prime_wve")
  if status then
    moveYtoTC = moveYtoTC + 80
  end
  if GameStateMainPlanet.CityMapPosInfo then
    local params = LuaUtils:string_split(GameStateMainPlanet.CityMapPosInfo, "^")
    self:GetFlashObject():InvokeASCallback("_root", "setCityMapPosAndScale", params[1], params[2] + moveYtoTC, params[3])
  else
    self:GetFlashObject():InvokeASCallback("_root", "setCityMapPosAndScale", 0, 0 + moveYtoTC, 80, 80)
  end
end
GameObjectMainPlanet.isShowWDEndTime = false
function GameObjectMainPlanet:ReleasedInBG()
  GameUIBarMiddle:HideMiddleMenu()
  GameUIBarRight:MoveOutRightMenu()
  if self:GetFlashObject() then
    GameObjectMainPlanet:GetFlashObject():InvokeASCallback("_root", "hideHightlight")
  end
end
function GameObjectMainPlanet:OnFSCommand(cmd, arg)
  if cmd == "buildingClicked" then
    GameUtils:PlaySound("open_effect2.mp3")
    self:BuildingClicked(arg)
  elseif cmd == "ReleasedInBG" then
    self:ReleasedInBG()
  elseif cmd == "hitZoneUnderBuildingClicked" then
    local buildingName = arg
    self:hitZoneUnderBuildingClicked(buildingName)
  elseif cmd == "FBBallClicked" then
    FacebookPopUI.mInviteTitleText = GameLoader:GetGameText("LC_MENU_NEW_FACEBOOK_LOG_IN_BUTTON_4")
    FacebookPopUI:GetFacebookInvitableFriendList(false)
  elseif cmd == "planetary_fortress_pos" then
    if GameUtils:GetTutorialHelp() then
      local param = LuaUtils:string_split(arg, "\001")
      GameUtils:ShowMaskLayer(tonumber(param[1]), tonumber(param[2]), GameUIMaskLayer.ButtonPos.right, GameLoader:GetGameText("LC_MENU_ASSISTANT_INFO_2"), GameUIMaskLayer.MaskStyle.big)
    end
  elseif cmd == "engineering_bay_pos" then
    if GameUtils:GetTutorialHelp() then
      local param = LuaUtils:string_split(arg, "\001")
      GameUtils:ShowMaskLayer(tonumber(param[1]), tonumber(param[2]), GameUIMaskLayer.ButtonPos.right, GameLoader:GetGameText("LC_MENU_ASSISTANT_INFO_3"), GameUIMaskLayer.MaskStyle.big)
    end
  elseif cmd == "CommandPost_pos" then
    if GameUtils:GetTutorialHelp() then
      local param = LuaUtils:string_split(arg, "\001")
      GameUtils:ShowMaskLayer(tonumber(param[1]), tonumber(param[2]), GameUIMaskLayer.ButtonPos.right, GameLoader:GetGameText("LC_MENU_ASSISTANT_INFO_5"), GameUIMaskLayer.MaskStyle.big)
    end
  elseif cmd == "tech_lab_pos" then
    if GameUtils:GetTutorialHelp() then
      local param = LuaUtils:string_split(arg, "\001")
      GameUtils:ShowMaskLayer(tonumber(param[1]), tonumber(param[2]), GameUIMaskLayer.ButtonPos.right, GameLoader:GetGameText("LC_MENU_ASSISTANT_INFO_4"), GameUIMaskLayer.MaskStyle.big)
    end
  elseif cmd == "krypton_center_pos" then
    if GameUtils:GetTutorialHelp() then
      local param = LuaUtils:string_split(arg, "\001")
      GameUtils:ShowMaskLayer(tonumber(param[1]), tonumber(param[2]), GameUIMaskLayer.ButtonPos.right, GameLoader:GetGameText("LC_MENU_ASSISTANT_INFO_5"), GameUIMaskLayer.MaskStyle.big)
    end
  elseif cmd == "engineering_bay_pos_action" then
    if GameUtils:GetTutorialHelp() then
      local param = LuaUtils:string_split(arg, "\001")
      GameUtils:ShowMaskLayer(tonumber(param[1]), tonumber(param[2]), GameUIMaskLayer.ButtonPos.right, GameLoader:GetGameText("LC_MENU_ASSISTANT_INFO_9"), GameUIMaskLayer.MaskStyle.big)
    end
  elseif cmd == "upgrade_tech_pos" then
    if GameUtils:GetTutorialHelp() then
      local param = LuaUtils:string_split(arg, "\001")
      GameUtils:ShowMaskLayer(tonumber(param[1]), tonumber(param[2]), GameUIMaskLayer.ButtonPos.right, GameLoader:GetGameText("LC_MENU_ASSISTANT_INFO_11"), GameUIMaskLayer.MaskStyle.big)
    end
  elseif cmd == "collect_pos" then
    if GameUtils:GetTutorialHelp() then
      local param = LuaUtils:string_split(arg, "\001")
      GameUtils:ShowMaskLayer(tonumber(param[1]), tonumber(param[2]), GameUIMaskLayer.ButtonPos.right, GameLoader:GetGameText("LC_MENU_ASSISTANT_INFO_18"), GameUIMaskLayer.MaskStyle.big)
    end
  elseif cmd == "ComposeKrypton_pos" and GameUtils:GetTutorialHelp() then
    local param = LuaUtils:string_split(arg, "\001")
    GameUtils:ShowMaskLayer(tonumber(param[1]), tonumber(param[2]), GameUIMaskLayer.ButtonPos.right, GameLoader:GetGameText("LC_MENU_ASSISTANT_INFO_14"), GameUIMaskLayer.MaskStyle.big)
  end
end
function GameObjectMainPlanet:PauseAnim()
  self.m_isPause = true
end
function GameObjectMainPlanet:ResumeAnim()
  self.m_isPause = false
end
GameObjectMainPlanet.needSetWorldState = false
function GameObjectMainPlanet:UpdateWorldStatus()
  local modules_status = GameGlobalData:GetData("modules_status")
  DebugOut("UpdateWorldStatus")
  DebugTable(modules_status)
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "SetTerritorialEntryTip", false, "")
  end
  if modules_status ~= nil then
    local visible = "Invisible"
    local territorialVisible = "Invisible"
    for i, v in ipairs(modules_status.modules) do
      if v.name == "wd" and v.status == true then
        visible = "Normal"
      end
      if v.name == "tc" and v.status == true then
        territorialVisible = "Normal"
        if GameStateManager.GameStateStarCraft.mStarCraftLeftTime > 0 then
          territorialVisible = "Buildable"
        end
        if GameGlobalData.starCraftData and GameGlobalData.starCraftData.type == 2 and GameGlobalData.starCraftData.open == false then
          territorialVisible = "Buildable"
          if self:GetFlashObject() then
            self:GetFlashObject():InvokeASCallback("_root", "SetTerritorialEntryTip", true, GameLoader:GetGameText("LC_MENU_TC_ON_ENROLLING"))
          end
        end
      end
    end
    DebugOut("world visible", visible)
    if self:GetFlashObject() then
      DebugOut("GameObjectMainPlanet:UpdateWorldStatus() and Territorial")
      self:GetFlashObject():InvokeASCallback("_root", "lua2fs_setBuidingStatus", "world_domination", visible)
      self:GetFlashObject():InvokeASCallback("_root", "lua2fs_setBuidingStatus", "territorial_entry", territorialVisible)
      self.needSetWorldState = false
    else
      self.needSetWorldState = true
    end
  end
end
GameObjectMainPlanet.needSetColonialState = false
function GameObjectMainPlanet:UpdateColonialStatus()
  local modules_status = GameGlobalData:GetData("modules_status")
  DebugOut("UpdateColonialStatus")
  DebugTable(modules_status)
  if modules_status ~= nil then
    local visible = "Invisible"
    for i, v in ipairs(modules_status.modules) do
      if v.name == "colonial" and v.status == true then
        visible = "Normal"
        break
      end
    end
    if self:GetFlashObject() then
      DebugOut("GameObjectMainPlanet:UpdateColonialStatus()")
      DebugOut(visible)
      self:ShowBuilding("colonial", visible)
      self.needSetColonialState = false
    else
      self.needSetColonialState = true
    end
  end
end
function GameObjectMainPlanet:UpdatePrimeWveStatus()
  local modules_status = GameGlobalData:GetData("modules_status")
  if modules_status ~= nil then
    local visible = "Invisible"
    local status = GameGlobalData:GetModuleStatus("prime_wve")
    if status then
      visible = "Normal"
      if not GameObjectMainPlanet.PrimeWveStatus then
        GameObjectMainPlanet:InitFlashObject()
        if self:GetFlashObject() then
          GameStateMainPlanet:CheckNewTutorial()
        end
      end
    end
    if self:GetFlashObject() then
      DebugOut("GameObjectMainPlanet:UpdatePrimeWveStatus()")
      DebugOut(visible)
      self:ShowBuilding("prime_wve", visible)
    end
  end
  GameObjectMainPlanet:UpdatePrimeWveActivityStatus()
end
function GameObjectMainPlanet:UpdatePrimeWveActivityStatus()
  if self:GetFlashObject() then
    local item = GameUILab:GetLibsItemByName("prime_wve")
    local status = item and item.count_down == 0
    self:GetFlashObject():InvokeASCallback("_root", "updatePrimeWveActivityStatus", status)
  end
end
function GameObjectMainPlanet:UpdateStarWarStatus()
  local modules_status = GameGlobalData:GetData("modules_status")
  if modules_status ~= nil then
    local visible = "Invisible"
    local status = GameGlobalData:GetModuleStatus("starwar")
    if status then
      visible = "Normal"
      if self:GetFlashObject() then
        GameStateMainPlanet:CheckNewTutorial()
      end
    end
    if self:GetFlashObject() then
      DebugOut("GameObjectMainPlanet:UpdateStarWarStatus()")
      DebugOut(visible)
      self:ShowBuilding("starwar", visible)
    end
  end
end
function GameObjectMainPlanet:UpdateBuildingStatus()
  DebugOut(">>>>>>>>>>>>>>>>> UpdateBuildingStatus")
  local buildings = GameGlobalData:GetData("buildings")
  if not buildings then
    return
  end
  DebugTable(buildings)
  buildings = buildings.buildings
  TutorialQuestManager.OnBuildingInfoChange()
  local buildable_building_table = {}
  for _, v in ipairs(buildings) do
    if v then
      local status_building = "Normal"
      if v.level == 0 then
        if v.name == "Command_post" or v.status == 0 then
          status_building = "Invisible"
        elseif v.status == 1 or v.status == 3 then
          status_building = "Buildable"
          table.insert(buildable_building_table, v)
        end
      else
        status_building = "Normal"
      end
      if status_building ~= "Buildable" then
        GameObjectMainPlanet:ShowBuilding(v.name, status_building)
        DebugOut("lua : set buiding status -", v.name, ",", status_building)
      end
    end
  end
  if #buildable_building_table > 0 then
    if #buildable_building_table > 1 then
      table.sort(buildable_building_table, function(a, b)
        return a.require_user_level < b.require_user_level
      end)
    end
    local buildable_building = buildable_building_table[1]
    GameObjectMainPlanet:ShowBuilding(buildable_building.name, "Buildable")
    DebugOutPutTable(buildable_building, "buildable")
  end
  if GameObjectMainPlanet.FireworkData then
    GameObjectMainPlanet:SpecialEfficacyNotifyHandler(GameObjectMainPlanet.FireworkData)
  end
end
GameObjectMainPlanet.fetchNtfForWD = false
GameObjectMainPlanet.waitFetchNtfForWD = false
function GameObjectMainPlanet:ShowBuilding(name, buildingStatus)
  if self:GetFlashObject() then
    if name == "star_portal" then
      if buildingStatus == "Normal" and (GameSettingData.m_newFleetUnlock or GameUIActivity.IsHavePromCommander()) then
        buildingStatus = "News"
      end
      DebugOut("GameObjectMainPlanet:ShowBuilding star_portal ", buildingStatus, GameSettingData.m_newFleetUnlock, GameUIActivity.IsHavePromCommander())
    end
    self:GetFlashObject():InvokeASCallback("_root", "lua2fs_setBuidingStatus", name, buildingStatus)
  end
end
function GameObjectMainPlanet:BuildingClicked(name_building)
  if name_building == "colonial" then
    local QuestTutorialColonial = TutorialQuestManager.QuestTutorialColonial
    QuestTutorialColonial:SetActive(true, false)
    self:GetFlashObject():InvokeASCallback("_root", "HideTutorialColonial")
    local gs = GameStateManager.GameStateColonization
    DebugOut(gs)
    gs.lastState = GameStateManager:GetCurrentGameState()
    GameStateManager:SetCurrentGameState(gs)
  elseif name_building == "Command_post" then
    GameObjectMainPlanet:GetFlashObject():InvokeASCallback("_root", "HideTutorialCommandPost")
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateStarSystem)
    local GameUIStarSystemPort = LuaObjectManager:GetLuaObject("GameUIStarSystemPort")
    GameUIStarSystemPort:Show()
  elseif name_building == "territorial_entry" then
    if GameUtils:IsModuleUnlock("tc") and GameStateManager.GameStateStarCraft.mStarCraftLeftTime > 0 or GameUtils:IsModuleUnlock("tc") and GameGlobalData.starCraftData and GameGlobalData.starCraftData.type == 2 then
      local GameUIStarCraftEnter = LuaObjectManager:GetLuaObject("GameUIStarCraftEnter")
      GameUIStarCraftEnter:Show()
    elseif GameUtils:IsModuleUnlock("tc") then
      local content = GameLoader:GetGameText("LC_MENU_TC_IN_TRUCE_ALERT")
      local tmpLeftTime = -GameStateManager.GameStateStarCraft.mStarCraftLeftTime - (os.time() - GameStateManager.GameStateStarCraft.mStartCraftFetchTime)
      if tmpLeftTime < 0 then
        tmpLeftTime = 0
      end
      local timeStr = GameUtils:formatTimeString(tmpLeftTime)
      content = string.format(content, timeStr)
      GameUtils:ShowTips(content)
    end
  elseif name_building == "finacial_entry" then
    DebugOut("enterConsortium")
    GameStateManager:SetCurrentGameState(GameStateConsortium)
  elseif name_building == "world_domination" then
    if not GameStateMainPlanet.WDActive then
      local QuestTutorialWD = TutorialQuestManager.QuestTutorialWD
      QuestTutorialWD:SetActive(true, false)
      if self:GetFlashObject() then
        self:GetFlashObject():InvokeASCallback("_root", "HideTutorialWorld")
        self:GetFlashObject():InvokeASCallback("_root", "CloseWDEntrency")
      end
    end
    local GameUIWDPlanetSelect = LuaObjectManager:GetLuaObject("GameUIWDPlanetSelect")
    GameUIWDPlanetSelect.EnterEventDirectly = true
    local gs = GameStateManager.GameStateWD
    if not GameObjectMainPlanet.fetchNtfForWD then
      local playerList = {}
      local requestParam = {notifies = playerList}
      NetMessageMgr:SendMsg(NetAPIList.alliance_notifies_req.Code, requestParam, GameObjectMainPlanet.fetchNtfForWDCallback, true, nil)
      if not GameStateManager.GameStateWD.AllDataFetched() and GameStateManager.GameStateWD.HaveAlliance() then
        DebugWD("wait here 1")
        GameObjectMainPlanet.waitFetchNtfForWD = true
      end
    end
    if GameStateManager.GameStateWD.wdList ~= nil and GameObjectMainPlanet.fetchNtfForWD and (GameStateManager.GameStateWD.HaveAlliance() and GameStateManager.GameStateWD.AllDataFetched() or not GameStateManager.GameStateWD.HaveAlliance()) then
      if GameStateManager.GameStateWD.WDBaseInfo.State == GameStateManager.GameStateWD.WDState.BATTLE and not GameStateManager.GameStateWD.OpponentDataFetched() then
        GameObjectMainPlanet.waitFetchNtfForWD = true
        GameWaiting:ShowLoadingScreen()
      else
        gs.lastState = GameStateManager:GetCurrentGameState()
        GameStateManager:SetCurrentGameState(gs)
      end
    end
  elseif name_building == "prime_wve" then
    local WVEGameManeger = LuaObjectManager:GetLuaObject("WVEGameManeger")
    local libItem = GameUILab:GetLibsItemByName("prime_wve")
    if libItem and 0 < libItem.count_down and 0 < libItem.count_down + GameUILab.baseTime - os.time() then
      local countDownTimeText = GameUtils:formatTimeString(libItem.count_down + GameUILab.baseTime - os.time())
      m_TipText = string.format(GameLoader:GetGameText("LC_MENU_WVE_LOCKING_DESC"), countDownTimeText)
      GameUtils:ShowTips(m_TipText)
    elseif not libItem or 0 > libItem.count_down then
      GameUtils:ShowTips(GameLoader:GetGameText("LC_MENU_WVE_LOCKING_DESC_2"))
    else
      WVEGameManeger:StartWVEGame()
    end
  elseif name_building == "starwar" then
    local libItem = GameUILab:GetLibsItemByName("starwar")
    if libItem and 0 < libItem.count_down then
      GameStateManager:SetCurrentGameState(GameStateManager.GameStateMiningWorld)
      local GameUIMiningWorld = LuaObjectManager:GetLuaObject("GameUIMiningWorld")
      GameUIMiningWorld.enterDir = 1
      GameUIMiningWorld:Show()
    else
      local lockTipStr = GameLoader:GetGameText("LC_MENU_DEXTER_INFO_3")
      GameUtils:ShowTips(lockTipStr)
    end
  else
    local QuestTutorialCityHall = TutorialQuestManager.QuestTutorialCityHall
    local QuestTutorialEngineer = TutorialQuestManager.QuestTutorialEngineer
    local QuestTutorialEnhance = TutorialQuestManager.QuestTutorialEnhance
    local QuestTutorialBuildAcademy = TutorialQuestManager.QuestTutorialBuildAcademy
    local QuestTutorialsFirstKillBoss = TutorialQuestManager.QuestTutorialsFirstKillBoss
    local QuestTutorialTax = TutorialQuestManager.QuestTutorialTax
    if QuestTutorialCityHall:IsActive() and name_building == "planetary_fortress" then
      AddFlurryEvent("ClickCityHall", {}, 1)
      AddFlurryEvent("TutorialCityHall_SelectBuilding", {}, 2)
    elseif QuestTutorialTax:IsActive() and name_building == "planetary_fortress" then
      AddFlurryEvent("TutorialCityHallTax_SelectBuilding", {}, 2)
    elseif QuestTutorialEngineer:IsActive() and name_building == "engineering_bay" then
      AddFlurryEvent("FirstClickEngineeringBay", {}, 1)
      AddFlurryEvent("TutoriaEngine_SelectBuilding", {}, 2)
    elseif QuestTutorialEnhance:IsActive() and name_building == "engineering_bay" then
      AddFlurryEvent("SecondClickEngineeringBay", {}, 1)
      AddFlurryEvent("TutoriaEngineUse_SelectBuilding", {}, 2)
    elseif QuestTutorialBuildAcademy:IsActive() and name_building == "commander_academy" then
      AddFlurryEvent("FirstClickAcademy", {}, 1)
    elseif QuestTutorialsFirstKillBoss:IsActive() and name_building == "commander_academy" then
      AddFlurryEvent("SecondClickAcademy", {}, 1)
    end
    local buildingInfo = GameGlobalData:GetBuildingInfo(name_building)
    if buildingInfo.level < buildingInfo.max_level then
      GameUIBarMiddle:ShowMiddleMenu(name_building)
    else
      DebugOut(" functon  clicked: ", buildingInfo.name, buildingInfo.level)
      local funfun = GameUIBarMiddle.FunctionCallbackList[buildingInfo.name]
      if funfun ~= nil then
        funfun()
      end
    end
  end
  GameUIBarRight:ForceMoveOutRightMenu()
end
function GameObjectMainPlanet.buildingsListener()
  GameObjectMainPlanet:UpdateBuildingStatus()
end
function GameObjectMainPlanet.OnMenuStatusChange()
  GameObjectMainPlanet:UpdateColonialStatus()
  GameObjectMainPlanet:UpdateWorldStatus()
  GameObjectMainPlanet:UpdateStarWarStatus()
  GameObjectMainPlanet:UpdatePrimeWveStatus()
end
function GameObjectMainPlanet:Update(dt)
  if not self:GetFlashObject() or self.m_isPause then
    return
  end
  if self.needSetColonialState then
    GameObjectMainPlanet:UpdateColonialStatus()
  end
  if self.needSetWorldState then
    GameObjectMainPlanet:UpdateWorldStatus()
  end
  self:GetFlashObject():Update(dt)
  if self.waitFetchNtfForWD then
    local opponentFetched = true
    if GameStateManager.GameStateWD.WDBaseInfo.State == GameStateManager.GameStateWD.WDState.BATTLE then
      if GameStateManager.GameStateWD.OpponentDataFetched() then
        opponentFetched = true
      else
        opponentFetched = false
      end
    end
    if GameStateManager.GameStateWD.AllDataFetched() and opponentFetched then
      self.waitFetchNtfForWD = false
      GameWaiting:HideLoadingScreen()
      self:BuildingClicked("world_domination")
    end
  end
  GameObjectMainPlanet:UpdateWDEndTime()
  if GameUtils:IsMycardAPP() and GameVip:IsMycardEnabled() then
    GameVip.checkMycardOrder()
  end
  if GameUtils:IsQihooApp() then
    GameVip.checkMycardOrder()
  end
end
function GameObjectMainPlanet:UpdateWDEndTime()
  if not GameObjectMainPlanet:GetFlashObject() then
    return
  end
  if self.isShowWDEndTime and tonumber(self.isShowWDEndTime) - os.time() > 0 then
    local showtype = "hour"
    local day, day_1, day_2 = 0, 0, nil
    local hour_1, hour_2 = 0, 0
    local min_1, min_2 = 0, 0
    local sec_1, sec_2 = 0, 0
    local endTime = tonumber(self.isShowWDEndTime) - os.time()
    if endTime > 86400 then
      showtype = "day"
      day_1 = math.floor(endTime / 86400)
    end
    if endTime ~= GameObjectMainPlanet.lastWDUpdateTime then
      GameObjectMainPlanet.lastWDUpdateTime = endTime
      hour_1 = math.floor((endTime - day_1 * 86400) / 3600)
      min_1 = math.floor((endTime - day_1 * 86400 - hour_1 * 3600) / 60)
      sec_1 = endTime - day_1 * 86400 - hour_1 * 3600 - min_1 * 60
      day_2 = day_1 % 10
      day_1 = math.floor(day_1 / 10)
      hour_2 = hour_1 % 10
      hour_1 = math.floor(hour_1 / 10)
      min_2 = min_1 % 10
      min_1 = math.floor(min_1 / 10)
      sec_2 = sec_1 % 10
      sec_1 = math.floor(sec_1 / 10)
      GameObjectMainPlanet:GetFlashObject():InvokeASCallback("_root", "showWDLeftTime", true, showtype, day_1, day_2, hour_1, hour_2, min_1, min_2, sec_1, sec_2)
    end
  else
    local endTime = 0
    if endTime ~= GameObjectMainPlanet.lastWDUpdateTime then
      GameObjectMainPlanet.lastWDUpdateTime = endTime
      GameObjectMainPlanet:GetFlashObject():InvokeASCallback("_root", "showWDLeftTime", false, "day", 0, 0, 0, 0, 0, 0, 0, 0)
    end
  end
  GameObjectMainPlanet:CheckBattleChanelState()
end
function GameObjectMainPlanet:CheckBattleChanelState()
  if GameObjectMainPlanet.isShowWDEndTime and tonumber(GameObjectMainPlanet.isShowWDEndTime) - os.time() > 0 then
    if not GameStateManager.GameStateWD.inActive then
      GameStateManager.GameStateWD.inActive = true
    end
  elseif GameStateManager.GameStateWD.inActive then
    GameStateManager.GameStateWD.inActive = false
  end
  if GameStateManager.GameStateStarCraft.mStarCraftLeftTime and GameStateManager.GameStateStarCraft.mStartCraftFetchTime and 0 < GameStateManager.GameStateStarCraft.mStarCraftLeftTime + GameStateManager.GameStateStarCraft.mStartCraftFetchTime - os.time() then
    if not GameStateManager.GameStateStarCraft.mStarCraftActive then
      GameStateManager.GameStateStarCraft.mStarCraftActive = true
    end
  elseif GameStateManager.GameStateStarCraft.mStarCraftActive then
    GameStateManager.GameStateStarCraft.mStarCraftActive = false
  end
end
function GameObjectMainPlanet.fetchNtfForWDCallback(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.alliance_notifies_req.Code then
    GameObjectMainPlanet.fetchNtfForWD = true
    GameObjectMainPlanet:BuildingClicked("world_domination")
    return true
  end
  return false
end
GameObjectMainPlanet.FireworkData = nil
function GameObjectMainPlanet:SpecialEfficacyNotifyHandler(content)
  DebugOut("GameObjectMainPlanet.SpecialEfficacyNotifyHandler")
  DebugTable(content)
  GameObjectMainPlanet.FireworkData = content
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "lua2fs_loadFirework", content.flag)
  end
end
function GameObjectMainPlanet:UpdateConsortiumState()
  DebugOut("GameObjectMainPlanet:UpdateConsortiumState")
  assert(false, "UpdateConsortiumState")
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "updateConsortiumState", GameUIConsortium:isShowHighLight())
  end
end
function GameObjectMainPlanet:UpdateConsortiumStateInChinese()
  DebugOut("GameObjectMainPlanet:UpdateConsortiumStateInChinese")
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "updateConsortiumStateInChinese")
  end
end
function GameObjectMainPlanet:hitZoneUnderBuildingClicked(buildingName)
  DebugOut("GameObjectMainPlanet:hitZoneUnderBuildingClicked : " .. buildingName)
  local buildings = GameGlobalData:GetData("buildings")
  DebugTable(buildings)
  local playerLevel = GameGlobalData:GetData("levelinfo").level
  for i, buildingInfo in ipairs(buildings.buildings) do
    if buildingName == buildingInfo.name then
      local tipMsg
      if playerLevel >= buildingInfo.require_user_level then
        tipMsg = GameLoader:GetGameText("LC_MENU_NEED_REBUILD_FIRST_ALERT")
        local preBuilding = self:_getPreBuildingInfoByBuildingInfo(buildingInfo)
        if nil == preBuilding then
          break
        end
        local preBuildingNameText = self:_getBuildingNameTextByBuildingName(preBuilding.name)
        if nil == tipMsg or "" == tipMsg then
          tipMsg = "you should build %s do before"
        end
        tipMsg = string.format(tipMsg, preBuildingNameText)
      else
        tipMsg = GameLoader:GetGameText("LC_MENU_MAIN_INTERFACE_REBUILDE_INFO")
        if nil == tipMsg or "" == tipMsg then
          tipMsg = "%s, LV%d unlock this building"
        end
        local buildingNameText = self:_getBuildingNameTextByBuildingName(buildingName)
        tipMsg = string.format(tipMsg, buildingNameText, buildingInfo.require_user_level)
      end
      local GameTip = LuaObjectManager:GetLuaObject("GameTip")
      GameTip:Show(tipMsg)
      break
    end
  end
end
function GameObjectMainPlanet:_getBuildingNameTextByBuildingName(buildingName)
  local key = "LC_MENU_BUILDING_NAME_" .. string.upper(buildingName)
  local tipMsg = GameLoader:GetGameText(key)
  if nil == tipMsg or "" == tipMsg then
    tipMsg = "XXX"
  end
  return tipMsg
end
function GameObjectMainPlanet:_getPreBuildingInfoByBuildingInfo(buildingInfo)
  local buildings = GameGlobalData:GetData("buildings")
  table.sort(buildings.buildings, function(buildingLhs, buildingRhs)
    return buildingLhs.require_user_level < buildingRhs.require_user_level
  end)
  DebugTable(buildings.buildings)
  for i, building in ipairs(buildings.buildings) do
    if buildingInfo.name == building.name and 1 ~= i then
      return buildings.buildings[i - 1]
    end
  end
  return nil
end
function GameObjectMainPlanet:getPlanetaryFortressPos()
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "getPlanetaryFortressPos")
  end
end
function GameObjectMainPlanet:getEngineeringbayPos(...)
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "getEngineeringbayPos")
  end
end
function GameObjectMainPlanet:getCommandPostPos(...)
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "getCommandPostPos")
  end
end
function GameObjectMainPlanet:getTechlabPos(...)
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "getTechlabPos")
  end
end
function GameObjectMainPlanet:getKryptoncenterPos(...)
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "getKryptoncenterPos")
  end
end
function GameObjectMainPlanet:getEngineeringbayPos_action(...)
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "getEngineeringbayPos")
  end
end
function GameObjectMainPlanet:getUpgradeTechPos(...)
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "getUpgradeTechPos")
  end
end
function GameObjectMainPlanet:getCollectPos(...)
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "getCollectPos")
  end
end
function GameObjectMainPlanet:getComposeKrypton(...)
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "getComposeKrypton")
  end
end
