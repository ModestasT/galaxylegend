local GameStateBattleMap = GameStateManager.GameStateBattleMap
local GameStateLoading = GameStateManager.GameStateLoading
local GameStateEmptyState = GameStateManager.GameStateEmptyState
local GameStateCampaignSelection = GameStateManager.GameStateCampaignSelection
local GameObjectBattleMap = LuaObjectManager:GetLuaObject("GameObjectBattleMap")
local GameUICommonDialog = LuaObjectManager:GetLuaObject("GameUICommonDialog")
local GameSetting = LuaObjectManager:GetLuaObject("GameSetting")
local GameUIBarRight = LuaObjectManager:GetLuaObject("GameUIBarRight")
local GameUIBarLeft = LuaObjectManager:GetLuaObject("GameUIBarLeft")
local GameUIEvent = LuaObjectManager:GetLuaObject("GameUIEvent")
local GameItemBag = LuaObjectManager:GetLuaObject("GameItemBag")
local QuestTutorialBattleMap = TutorialQuestManager.QuestTutorialBattleMap
local QuestTutorialBuildTechLab = TutorialQuestManager.QuestTutorialBuildTechLab
local QuestTutorialBuildFactory = TutorialQuestManager.QuestTutorialBuildFactory
local QuestTutorialBuildKrypton = TutorialQuestManager.QuestTutorialBuildKrypton
local QuestTutorialBuildAffairs = TutorialQuestManager.QuestTutorialBuildAffairs
local QuestTutorialUseTechLab = TutorialQuestManager.QuestTutorialUseTechLab
local QuestTutorialUseFactory = TutorialQuestManager.QuestTutorialUseFactory
local QuestTutorialUseKrypton = TutorialQuestManager.QuestTutorialUseKrypton
local QuestTutorialUseAffairs = TutorialQuestManager.QuestTutorialUseAffairs
local QuestTutorialGetGift = TutorialQuestManager.QuestTutorialGetGift
local QuestTutorialBattleRush = TutorialQuestManager.QuestTutorialBattleRush
local QuestTutorialsChangeFleets1 = TutorialQuestManager.QuestTutorialsChangeFleets1
local QuestTutorialArena = TutorialQuestManager.QuestTutorialArena
local QuestTutorialBuildStar = TutorialQuestManager.QuestTutorialBuildStar
local QuestTutorialEquipAllByOneKey = TutorialQuestManager.QuestTutorialEquipAllByOneKey
local QuestTutorialEnhance_third = TutorialQuestManager.QuestTutorialEnhance_third
local QuestTutorialEngineer = TutorialQuestManager.QuestTutorialEngineer
local QuestTutorialMine = TutorialQuestManager.QuestTutorialMine
local GameUIPrestigeRankUp = LuaObjectManager:GetLuaObject("GameUIPrestigeRankUp")
local GameUIEnemyInfo = LuaObjectManager:GetLuaObject("GameUIEnemyInfo")
local GameStateMainPlanet = GameStateManager.GameStateMainPlanet
local GameObjectBattleMapBG = LuaObjectManager:GetLuaObject("GameObjectBattleMapBG")
local GameFleetInfoUI = LuaObjectManager:GetLuaObject("GameFleetInfoUI")
local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
local QuestTutorialEquip = TutorialQuestManager.QuestTutorialEquip
local GamePushNotification = LuaObjectManager:GetLuaObject("GamePushNotification")
local GameUIActivityNew = LuaObjectManager:GetLuaObject("GameUIActivityNew")
local QuestTutorialPveMapPoint = TutorialQuestManager.QuestTutorialPveMapPoint
local QuestTutorialMainTask = TutorialQuestManager.QuestTutorialMainTask
local FacebookPopUI = LuaObjectManager:GetLuaObject("FacebookPopUI")
local GameFleetInfoTabBar = LuaObjectManager:GetLuaObject("GameFleetInfoTabBar")
local QuestTutorialTheFirstEvent = TutorialQuestManager.QuestTutorialTheFirstEvent
local GameStateDaily = GameStateManager.GameStateDaily
local GameObjectTutorialCutscene = LuaObjectManager:GetLuaObject("GameObjectTutorialCutscene")
local GameStateBattlePlay = GameStateManager.GameStateBattlePlay
local GameObjectLevelUp = LuaObjectManager:GetLuaObject("GameObjectLevelUp")
function GameStateBattleMap:InitGameState()
end
function GameStateBattleMap:OnFocusGain(previousState)
  DebugOut("GameStateBattleMap:OnFocusGain")
  GameObjectBattleMap:LoadFlashObject()
  self:AddObject(GameObjectBattleMapBG)
  self:AddObject(GameObjectBattleMap)
  if previousState == GameStateLoading or previousState == GameStateEmptyState or self.m_currentAreaID == 60 then
    GameObjectBattleMap:HideCloseButton(true)
  else
    GameObjectBattleMap:HideCloseButton(false)
  end
  if not GameObjectTutorialCutscene:IsActive() or not GameObjectTutorialCutscene:IsNeedShowNewFakeBattle() then
    self:AddObject(GameUIBarLeft)
    self:AddObject(GameUIBarRight)
  end
  if GameUIEvent:IsActive() then
    self:AddObject(GameUIEvent)
  end
  if GameObjectTutorialCutscene:IsActive() and GameObjectTutorialCutscene:IsNeedShowNewFakeBattle() and GameStateBattleMap.isShowVictoryTip then
    GameObjectLevelUp:showNewFakeBattleVictory()
    GameStateManager:GetCurrentGameState():AddObject(GameObjectLevelUp)
  else
    GameStateBattleMap:ShowDialog()
  end
  if GameObjectTutorialCutscene:IsActive() and GameObjectTutorialCutscene:IsNeedShowNewFakeBattle() and previousState ~= GameStateBattlePlay or previousState == GameStateCampaignSelection or previousState == GameStateDaily or previousState == GameStateMainPlanet or previousState == GameStateLoading or previousState == GameStateEmptyState then
    if GameObjectBattleMap:GetFlashObject() then
      GameObjectBattleMap:GetFlashObject():InvokeASCallback("_root", "resetAll")
    end
    if GameObjectBattleMapBG:GetFlashObject() then
      GameObjectBattleMapBG:GetFlashObject():InvokeASCallback("_root", "resetBGPos")
    end
  else
    GameObjectBattleMap:RestoreBattleMap()
  end
end
function GameStateBattleMap:OnFocusLost(nextState)
  DebugOut("GameStateBattleMap:OnFocusLost")
  self:EraseObject(GameObjectBattleMap)
  self:EraseObject(GameUIBarRight)
  self:EraseObject(GameUIBarLeft)
  self:EraseObject(GameObjectBattleMapBG)
  self.m_isEnterFromLoading = false
end
function GameStateBattleMap:ShowDialog()
  local function callback()
    self:CheckNeedPlayEntryStory()
  end
  if GameObjectTutorialCutscene:IsNeedShowNewFakeBattle() then
    if GameObjectTutorialCutscene:IsActive() and GameGlobalData.FakeBattleIndex == 1 then
      local callback = function()
        local FteDialogFiniedReqCall = function()
          NetMessageMgr:SendMsg(NetAPIList.fte_req.Code, {
            content = {
              [1] = {
                key = "FinishedDialog",
                value = "1100099"
              }
            }
          }, nil, false, FteDialogFiniedReqCall)
        end
        FteDialogFiniedReqCall()
      end
      local FteBegainDialogReqCall = function()
        NetMessageMgr:SendMsg(NetAPIList.fte_req.Code, {
          content = {
            [1] = {
              key = "BeginDialog",
              value = "1100099"
            }
          }
        }, nil, false, FteBegainDialogReqCall)
      end
      FteBegainDialogReqCall()
      GameUICommonDialog:PlayStory({1100099}, callback)
    elseif GameObjectTutorialCutscene:IsActive() and GameGlobalData.FakeBattleIndex == 4 then
      local callback = function()
        local FteDialogFiniedReqCall = function()
          NetMessageMgr:SendMsg(NetAPIList.fte_req.Code, {
            content = {
              [1] = {
                key = "FinishedDialog",
                value = "1100101"
              }
            }
          }, nil, false, FteDialogFiniedReqCall)
        end
        FteDialogFiniedReqCall()
      end
      local FteBegainDialogReqCall = function()
        NetMessageMgr:SendMsg(NetAPIList.fte_req.Code, {
          content = {
            [1] = {
              key = "BeginDialog",
              value = "1100101"
            }
          }
        }, nil, false, FteBegainDialogReqCall)
      end
      FteBegainDialogReqCall()
      GameUICommonDialog:PlayStory({1100101}, callback)
    end
  end
  if Facebook:IsFacebookEnabled() and GameFleetInfoTabBar.needPromptFacebookStory then
    GameFleetInfoTabBar.needPromptFacebookStory = false
    DebugOut("hello world")
    if FacebookPopUI.mFeatureSwitch[FACEBOOKFEATURESWITCH.SWITCH_PRESTIGE] and FacebookPopUI.mFeatureSwitch[FACEBOOKFEATURESWITCH.SWITCH_PRESTIGE] == 1 then
      if Facebook:IsLoginedIn() then
        FacebookPopUI.mInviteTitleText = GameLoader:GetGameText("LC_MENU_NEW_FACEBOOK_LOG_IN_BUTTON_4")
        FacebookPopUI:GetFacebookInvitableFriendList()
        if not ext.GetDeviceCountry or not ext.GetDeviceCountry() then
          local countryStr = "unknow"
        end
      else
        local function facebookPromtCallback()
          FacebookPopUI:ShowFacebookMenu(FACEBOOKMENUTYPE.TYPE_MENU_LOGIN)
          FacebookPopUI:SetBindAwardType(FACEBOOKBINDAWARDTYPE.AWARD_PRESTIGE_CREDIT)
          local countryStr = ext.GetDeviceCountry and ext.GetDeviceCountry() or "unknow"
          AddFlurryEvent("POPFacebookBind_Prestige", {}, 1)
        end
        if not Facebook:IsBindFacebook() then
          DebugOut("not Facebook:IsBindFacebook()")
          GameUICommonDialog:PlayStory({100069, 100070}, facebookPromtCallback)
        else
          GameUICommonDialog:PlayStory(self.m_storyOnFocusGain, callback)
        end
      end
    else
      GameUICommonDialog:PlayStory(self.m_storyOnFocusGain, callback)
    end
  else
    GameUICommonDialog:PlayStory(self.m_storyOnFocusGain, callback)
  end
  self.m_storyWhenFocusGain = nil
end
function GameStateBattleMap:ReEnter(callback)
  self:EnterSection(self.m_currentAreaID, self.m_currentSectionID, callback)
end
function GameStateBattleMap:EnterSection(areaID, sectionID, callback, isFromLoading)
  DebugOut("EnterSection areaID=", areaID, sectionID)
  self.m_currentAreaID = areaID
  self.m_currentSectionID = sectionID
  self.m_combinedSectionID = 1000 * self.m_currentAreaID + self.m_currentSectionID
  self.m_isEnterFromLoading = isFromLoading
  GameObjectBattleMap:RequireBattleMapInfoFromServer(self.m_combinedSectionID, callback)
  GameObjectBattleMapBG:LoadBGMap(self.m_currentAreaID, self.m_currentSectionID)
  if QuestTutorialBattleMap:IsActive() and immanentversion == 2 then
    GameStateBattleMap:SetStoryWhenFocusGain({51117})
  end
  self:CheckTutorial(areaID, sectionID)
end
function GameStateBattleMap:CheckNeedPlayEntryStory()
  DebugOut("CheckNeedPlayEntryStory", self.m_currentAreaID, self.m_currentSectionID)
  DebugOut("CheckNeedPlayEntryStory", self.m_combinedSectionID, GameSettingData.m_lastSectionShowEnterDialog)
  if self.m_combinedSectionID then
    if self.m_combinedSectionID == 60001 and GameSettingData.m_lastSectionShowEnterDialog < -1 or self.m_combinedSectionID ~= 60001 and self.m_combinedSectionID > GameSettingData.m_lastSectionShowEnterDialog then
      local sectionInfo = GameDataAccessHelper:GetSectionInfo(self.m_currentAreaID, self.m_currentSectionID)
      local storyRef = sectionInfo._entrostory or {}
      local function dialogCallBack()
        GameSetting:SaveChapterEnter(self.m_combinedSectionID)
      end
      if self.m_combinedSectionID == 60001 then
        function dialogCallBack()
          GameSetting:SaveChapterEnter(-1)
        end
        do break end
        if ext.GetPlatform() ~= "Android" and GameUtils:GetLeftNumFromOSVersion(ext.GetOSVersion()) and GameUtils:GetLeftNumFromOSVersion(ext.GetOSVersion()) >= 8 then
          GameStateBattleMap:CheckPlayNotificationStory(self.m_currentAreaID, self.m_currentSectionID)
        end
      end
      DebugTable(storyRef)
      GameUICommonDialog:PlayStory(storyRef, dialogCallBack)
    end
    if self.m_combinedSectionID == 60001 and self.m_isEnterFromLoading then
      GameObjectBattleMap:CheckIsFinishedTutorialBattle()
    end
  end
end
function GameStateBattleMap:CheckPlayNotificationStory(areaID, battleID)
  DebugOut("CheckPlayNotificationStory:")
  DebugOut(areaID, battleID)
  if areaID == 1 and battleID == 2 then
    DebugOut("play notification")
    local function callback()
      if ext.RegisterNotification ~= nil then
        DebugOut("RegisterNotification > 8.0")
        ext.RegisterNotification()
      end
      GameUICommonDialog:PlayStory({1100043}, GamePushNotification.InitNotification)
    end
    GameUICommonDialog:PlayStory({1100042}, callback)
  end
end
function GameStateBattleMap:CheckTutorial(areaID, battleID)
  DebugOut("GameStateBattleMap:CheckTutorial:")
  DebugOut(areaID, battleID)
  if areaID == 1 and battleID == 1 then
    if not QuestTutorialPveMapPoint:IsFinished() and not QuestTutorialPveMapPoint:IsActive() then
      QuestTutorialPveMapPoint:SetActive(true, false)
    end
    if not QuestTutorialTheFirstEvent:IsFinished() and not QuestTutorialTheFirstEvent:IsActive() then
      QuestTutorialTheFirstEvent:SetActive(true, false)
    end
  end
end
function GameStateBattleMap:CheckBeforeEnemyStory(battleID, callback)
  DebugOut("CheckBeforeEnemyStory ", battleID, self.m_currentAreaID)
  if GameDataAccessHelper then
    DebugOut("GameDataAccessHelper not nil")
  else
    DebugOut("GameDataAccessHelper is nil")
  end
  DebugOut(GameObjectBattleMap:IsBattleActive(self.m_currentAreaID, battleID))
  local storyRef = {}
  if GameObjectBattleMap:IsBattleActive(self.m_currentAreaID, battleID) then
    storyRef = GameDataAccessHelper:GetBattleBeforeStory(self.m_currentAreaID, battleID)
  end
  local shouldShowComboGachaTutorial = GameGlobalData:GetFeatureSwitchState("combo_guide")
  if self.m_currentAreaID == 1 and battleID == 2001 and not shouldShowComboGachaTutorial then
    callback()
  else
    GameUICommonDialog:PlayStory(storyRef, callback)
  end
end
function GameStateBattleMap:OnSectionOver(areaID, sectionID)
  DebugOut("OnSectionOver ", areaID, sectionID)
  if areaID == 1 and sectionID == 1 and not QuestTutorialEngineer:IsActive() and not QuestTutorialEngineer:IsFinished() then
    local engineerBuildInfo = GameGlobalData:GetBuildingInfo("engineering_bay")
    if engineerBuildInfo.status == 1 then
      QuestTutorialEngineer:SetActive(true)
    end
  end
  if immanentversion170 == nil then
    local story = GameDataAccessHelper:GetSectionBeforeFinishStory(self.m_currentAreaID, sectionID)
    GameStateMainPlanet:SetStoryWhenFocusGain(story)
    GameStateManager:SetCurrentGameState(GameStateMainPlanet)
  elseif immanentversion170 == 4 or immanentversion170 == 5 then
    if areaID == 1 and sectionID == 1 then
      GameObjectBattleMap:OnFSCommand("backToAreaMenu", "")
    else
      local story = GameDataAccessHelper:GetSectionBeforeFinishStory(self.m_currentAreaID, sectionID)
      GameStateMainPlanet:SetStoryWhenFocusGain(story)
      GameStateManager:SetCurrentGameState(GameStateMainPlanet)
    end
  end
end
function GameStateBattleMap:SetStoryWhenFocusGain(story)
  DebugOut("------\227\128\139 SetStoryWhenFocusGain")
  DebugTable(story)
  self.m_storyWhenFocusGain = story
end
function GameStateBattleMap:OnBattleOrEventFinish(battleID)
  DebugOut("OnBattleOrEventFinish " .. self.m_currentAreaID .. "  " .. battleID)
  local storyRef = GameDataAccessHelper:GetBattleAfterStory(self.m_currentAreaID, battleID)
  local callback
  if self.m_currentAreaID == 1 and battleID == 1006 then
    if immanentversion == 2 then
      table.insert(storyRef, 51130)
    end
  elseif self.m_currentAreaID == 1 and battleID == 1102 then
    if immanentversion170 == 4 or immanentversion170 == 5 then
      TutorialQuestManager.QuestTutorialPveMapPoint:SetActive(false, false)
      TutorialQuestManager.QuestTutorialEquip:SetActive(true, false)
      GameObjectBattleMap:RefreshEnemyStatus()
      GameUIBarRight:MoveInRightMenu()
    end
  elseif self.m_currentAreaID == 1 and battleID == 1004 then
    if immanentversion170 == 4 or immanentversion170 == 5 then
      TutorialQuestManager.QuestTutorialPveMapPoint:SetActive(false, false)
      GameObjectBattleMap:RefreshEnemyStatus()
      function callback()
        GameStateManager:SetCurrentGameState(GameStateMainPlanet)
      end
    end
  elseif self.m_currentAreaID == 1 and battleID == 1007 then
    if not QuestTutorialsChangeFleets1:IsFinished() then
    end
  elseif self.m_currentAreaID == 1 and battleID == 1011 then
    if TutorialQuestManager.QuestTutorialFirstChapter:IsFinished() == false and (immanentversion170 == 4 or immanentversion170 == 5) then
      TutorialQuestManager.QuestTutorialFirstChapter:SetActive(true, false)
    end
  elseif self.m_currentAreaID == 1 and battleID == 1109 then
    DebugOut("create  TutorialFirstGetSilvaEquip callback")
    function callback()
      if not TutorialQuestManager.QuestTutorialFirstGetSilvaEquip:IsFinished() then
        TutorialQuestManager.QuestTutorialFirstGetSilvaEquip:SetActive(true, false)
      end
      GameUIBarRight:CheckQuestTutorial()
    end
  elseif self.m_currentAreaID == 1 and battleID == 1103 then
    function callback()
      GameUIBarRight:MoveInRightMenu()
    end
  elseif self.m_currentAreaID == 1 and battleID == 2001 then
    function callback()
      if immanentversion == 2 and not QuestTutorialEnhance_third:IsActive() then
        QuestTutorialEnhance_third:SetActive(true)
        GameUIBarRight:MoveInRightMenu()
      end
    end
  elseif self.m_currentAreaID == 1 and battleID == 2008 then
    function callback()
      if immanentversion == 2 and not QuestTutorialBattleRush:IsActive() and not QuestTutorialBattleRush:IsFinished() then
        QuestTutorialBattleRush:SetActive(true)
      end
    end
  elseif self.m_currentAreaID == 1 and battleID == 3005 then
    DebugOut("3005:go back main planet")
    function callback()
      GameStateManager:SetCurrentGameState(GameStateMainPlanet)
    end
  elseif self.m_currentAreaID == 1 and battleID == 3012 then
    function callback()
      GameObjectBattleMap:GetFlashObject():InvokeASCallback("_root", "playbigbombAnim")
    end
  elseif self.m_currentAreaID == 1 and battleID == 2005 then
    function callback()
      GameStateManager:SetCurrentGameState(GameStateMainPlanet)
    end
  elseif self.m_currentAreaID == 1 and battleID == 2011 then
    function callback()
      GameStateManager:SetCurrentGameState(GameStateMainPlanet)
    end
  elseif self.m_currentAreaID == 1 and battleID == 4004 then
    if immanentversion == 1 then
      function callback()
        GameStateManager:SetCurrentGameState(GameStateMainPlanet)
      end
    end
  elseif self.m_currentAreaID == 1 and battleID == 5004 then
    function callback()
      GameStateManager:SetCurrentGameState(GameStateMainPlanet)
    end
  elseif self.m_currentAreaID == 1 and battleID == 7005 then
    function callback()
      GameStateManager:SetCurrentGameState(GameStateMainPlanet)
    end
  elseif self.m_currentAreaID == 60 and battleID == 1009 and immanentversion == 2 then
    function callback()
      GameUIEnemyInfo.m_currentBattleID = 1013
      GameUIEnemyInfo.m_combinedBattleID = GameUtils:CombineBattleID(60, GameUIEnemyInfo.m_currentBattleID)
      GameUIEnemyInfo:BattleNowClicked()
    end
  elseif self.m_currentAreaID == 60 and battleID == 1005 and immanentversion == 2 then
    function callback()
      if not QuestTutorialEquipAllByOneKey:IsActive() and not QuestTutorialEquipAllByOneKey:IsFinished() then
        QuestTutorialEquipAllByOneKey:SetActive(true)
        GameItemBag.EquipTutorialAgain = true
        GameUIBarRight:MoveInRightMenu()
      end
    end
  elseif self.m_currentAreaID ~= 60 or battleID ~= 1110 or immanentversion == 2 then
  end
  local battleInfo = GameDataAccessHelper:GetBattleInfo(self.m_currentAreaID, battleID)
  if battleInfo.over == 1 then
    function callback()
      GameStateBattleMap:OnSectionOver(self.m_currentAreaID, self.m_currentSectionID)
      if (immanentversion170 == 4 or immanentversion170 == 5) and battleID == 2012 and not QuestTutorialArena:IsFinished() then
        QuestTutorialArena:SetActive(true)
        GameStateManager:SetCurrentGameState(GameStateMainPlanet)
      end
    end
  end
  local function gCallback()
    GameObjectBattleMap:RefreshEnemyStatus()
    if callback then
      callback()
    end
    GameObjectBattleMap.RefreshQuestData()
    DebugOut("here we go")
    GameUIPrestigeRankUp:CheckNeedDisplay()
  end
  local shouldShowComboGachaTutorial = GameGlobalData:GetFeatureSwitchState("combo_guide")
  if self.m_currentAreaID == 1 and battleID == 3005 and not shouldShowComboGachaTutorial then
    local filterStory = {}
    for k, v in pairs(storyRef) do
      if v ~= 1100074 and v ~= 1100075 and v ~= 1100079 then
        table.insert(filterStory, v)
      end
    end
    GameUICommonDialog:PlayStory(filterStory, gCallback)
  else
    GameUICommonDialog:PlayStory(storyRef, gCallback)
  end
end
