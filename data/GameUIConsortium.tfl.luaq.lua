local GameUIConsortium = LuaObjectManager:GetLuaObject("GameUIConsortium")
local GameStateConsortium = GameStateManager.GameStateConsortium
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameObjectMainPlanet = LuaObjectManager:GetLuaObject("GameObjectMainPlanet")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local QuestTutorialComboGacha = TutorialQuestManager.QuestTutorialComboGacha
local GameUIActivityNew = LuaObjectManager:GetLuaObject("GameUIActivityNew")
GameUIConsortium.isHaveBenefit = {}
GameUIConsortium.level = 0
GameUIConsortium.benefit = 0
GameUIConsortium.next_level = 0
GameUIConsortium.next_benefit = 0
GameUIConsortium.count = 0
GameUIConsortium.upgrade_cost = {}
GameUIConsortium.time = 0
GameUIConsortium.facebookFriendList = {}
GameUIConsortium.friendList = {}
GameUIConsortium.isUpdateCommandFlag = false
GameUIConsortium.isShowFlag = false
GameUIConsortium.sendAllbuttonStatus = false
GameUIConsortium.getAllbuttonStatus = false
function GameUIConsortium:OnAddToGameState(...)
  DebugOut("GameUIConsortium:OnAddToGameState:")
  GameUIConsortium.mInviteFriendViewPoped = false
  DebugTable(GameUIConsortium.isHaveBenefit)
  GameUIConsortium.mItemTextureName = {}
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  self:MoveIn()
end
function GameUIConsortium:OnEraseFromGameState(...)
  DebugOut("GameUIConsortium:OnEraseFromGameState:")
  self:UnloadFlashObject()
end
function GameUIConsortium:OnFSCommand(cmd, arg)
  DebugOut("GameUIConsortium:OnFSCommand:")
  DebugOut("cmd = " .. cmd)
  if cmd == "close_menu" then
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateMainPlanet)
  elseif cmd == "consortium_update" then
    GameUIConsortium:ShowConsortiumUpdate()
  elseif cmd == "item_clicked" then
    GameUIConsortium:ItemButtonClicked(arg)
  elseif cmd == "consortium_facebook" then
    GameUIConsortium:FacebookButtonCallback()
  elseif cmd == "updateItemData" then
    GameUIConsortium:UpdateItemData(arg)
  elseif cmd == "receive_all" then
    if Facebook:IsBindFacebook() then
      if Facebook:IsLoginedIn() and GameUIConsortium.getAllbuttonStatus then
        GameUIConsortium:GetAllGiftButtonCallback()
      elseif Facebook:IsLoginedIn() and GameUIConsortium.getAllbuttonStatus == false then
        if #GameUIConsortium.friendList > 0 then
          GameTip:Show(GameLoader:GetGameText("LC_MENU_FINANCIAL_NO_GIFT_INFO"))
        else
          GameTip:Show(GameLoader:GetGameText("LC_MENU_INVITE_FACEBOOK_FRIEND_INFO"))
        end
      elseif Facebook:IsLoginedIn() == false then
        GameTip:Show(GameLoader:GetGameText("LC_MENU_LOGIN_FACEBOOK_FIRST_ALERT"))
      end
    else
      GameTip:Show(GameLoader:GetGameText("LC_MENU_FACEBOOK_SIGN_ALERT_1"))
    end
  elseif cmd == "send_all" then
    if Facebook:IsBindFacebook() then
      if Facebook:IsLoginedIn() and GameUIConsortium.sendAllbuttonStatus then
        GameUIConsortium:SendAllFriendButtonCallback()
      elseif Facebook:IsLoginedIn() and GameUIConsortium.sendAllbuttonStatus == false then
        if #GameUIConsortium.friendList > 0 then
          GameTip:Show(GameLoader:GetGameText("LC_MENU_FINANCIAL_NO_FRIENDS_INFO"))
        else
          GameTip:Show(GameLoader:GetGameText("LC_MENU_INVITE_FACEBOOK_FRIEND_INFO"))
        end
      elseif Facebook:IsLoginedIn() == false then
        GameTip:Show(GameLoader:GetGameText("LC_MENU_LOGIN_FACEBOOK_FIRST_ALERT"))
      end
    else
      GameTip:Show(GameLoader:GetGameText("LC_MENU_FACEBOOK_SIGN_ALERT_1"))
    end
  elseif cmd == "consortium_update_command" then
    GameUIConsortium:UpdateCommand()
  elseif cmd == "CloseDialog" then
    GameUIConsortium.IsShowConsortiumUpdate = false
  end
end
function GameUIConsortium:UpdateCommand()
  DebugOut("GameUIConsortium:UpdateCommand")
  NetMessageMgr:SendMsg(NetAPIList.financial_upgrade_req.Code, parm, GameUIConsortium.UpdateCommandCallback, true, nil)
end
function GameUIConsortium.UpdateCommandCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.financial_upgrade_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    if content.code == 0 then
      GameUIConsortium.isUpdateCommandFlag = true
      GameUIConsortium:GetConsortiumInfo()
    end
    return true
  end
  return false
end
function GameUIConsortium:ShowConsortiumUpdate()
  DebugOut("GameUIConsortium:ShowConsortiumUpdate")
  local maxText = ""
  local isMax = false
  if GameUIConsortium.level == GameUIConsortium.next_level then
    isMax = true
    maxText = GameLoader:GetGameText("LC_MENU_MAX_CHAR")
  end
  DebugOut(isMax)
  DebugOut(maxText)
  GameUIConsortium:RefurResource(isMax)
  GameUIConsortium:GetFlashObject():InvokeASCallback("_root", "showConsortiumUpdate", GameUIConsortium.level, GameUIConsortium.next_level, GameUIConsortium.benefit, GameUIConsortium.next_benefit, isMax, maxText)
  GameUIConsortium.IsShowConsortiumUpdate = true
end
function GameUIConsortium:ShowConsortiumUpdateAnimation()
  DebugOut("GameUIConsortium:ShowConsortiumUpdateAnimation")
  GameUIConsortium:GetFlashObject():InvokeASCallback("_root", "showUpdateAnimation")
end
function GameUIConsortium:RefurResource(isMax)
  DebugOut("GameUIConsortium:RefurResource")
  local havecount = 0
  local needcount = 0
  local iconFrame = ""
  if GameUIConsortium.upgrade_cost and 0 < #GameUIConsortium.upgrade_cost then
    DebugTable(GameUIConsortium.upgrade_cost)
    DebugOut(GameUIConsortium.upgrade_cost[1].item_type)
    if GameHelper:IsResource(GameUIConsortium.upgrade_cost[1].item_type) then
      local count = GameGlobalData:GetData("resource")
      havecount = GameUtils.numberConversion(count[GameUIConsortium.upgrade_cost[1].item_type])
    else
      havecount = GameGlobalData:GetItemCount(GameUIConsortium.upgrade_cost[1].number)
    end
    needcount = GameHelper:GetAwardCount(GameUIConsortium.upgrade_cost[1].item_type, GameUIConsortium.upgrade_cost[1].number, GameUIConsortium.upgrade_cost[1].no)
    iconFrame = GameHelper:GetCommonIconFrame(GameUIConsortium.upgrade_cost[1])
    DebugOut(havecount)
    DebugOut(needcount)
    DebugOut(iconFrame)
    local text1 = 0
    local text2 = 0
    local text3 = 0
    local isShowUpdate = false
    if havecount < needcount then
      isShowUpdate = false
      text1 = "<font color='#FF0000'>" .. havecount
    else
      isShowUpdate = true
      text1 = "<font color='#0CFF00'>" .. havecount
    end
    text2 = "<font color='#0CFF00'>" .. needcount
    text3 = text1 .. "/" .. text2
    GameUIConsortium:GetFlashObject():InvokeASCallback("_root", "refresIcon", iconFrame, text3, isShowUpdate, isMax)
  else
    GameUIConsortium:GetFlashObject():InvokeASCallback("_root", "refresIcon", "", "", false, isMax)
  end
end
function GameUIConsortium:UpdateItemData(arg)
  DebugOut("GameUIConsortium:UpdateItemData")
  local param = LuaUtils:string_split(arg, "\001")
  local itemId = tonumber(param[1])
  local mcId = param[2]
  local headIconFr = string.format("facebook_avatar%02d", 1)
  local itemData = GameUIConsortium.friendList[tonumber(itemId)]
  DebugTable(itemData)
  DebugTable(GameUIConsortium.facebookFriendList)
  DebugOut(itemData.passport)
  local name = 0
  for key, value in ipairs(GameUIConsortium.facebookFriendList) do
    DebugOut(value.id)
    if tonumber(value.id) == tonumber(itemData.passport) then
      DebugOut(value.name)
      name = value.name
    end
  end
  DebugOut(name)
  DebugOut(status)
  GameUIConsortium.text = 0
  local status = 0
  if itemData.status == 1 then
    status = 1
    GameUIConsortium.text = GameLoader:GetGameText("LC_MENU_FINANCIAL_GROUP_RECIEVE")
    GameUIConsortium:GetFlashObject():InvokeASCallback("_root", "updateItemData", itemId, headIconFr, tostring(name), status, GameUtils:formatTimeString(itemData.left_time), itemData.awards[1].number, GameUIConsortium.text)
  elseif itemData.status == 2 then
    if itemData.left_time == 0 then
      status = 3
      GameUIConsortium.text = GameLoader:GetGameText("LC_MENU_FINANCIAL_GROUP_SEND")
    else
      status = 2
      GameUIConsortium.text = GameLoader:GetGameText("LC_MENU_FINANCIAL_GROUP_GIFT_TIME")
    end
    GameUIConsortium:GetFlashObject():InvokeASCallback("_root", "updateItemData", itemId, headIconFr, tostring(name), status, GameUtils:formatTimeString(itemData.left_time), 0, GameUIConsortium.text)
  end
  GameUIConsortium:UpdateFBHeadIcon(itemId, mcId, itemData.passport)
end
function GameUIConsortium:MoveIn()
  DebugOut("GameUIConsortium:MoveIn")
  local isLoginedIn = Facebook:IsLoginedIn()
  DebugOut(isLoginedIn)
  if Facebook:IsLoginedIn() then
    GameUIConsortium:GetFacebookFriendList()
  end
  local lang = "en"
  if GameSettingData and GameSettingData.Save_Lang then
    local la = GameSettingData.Save_Lang
    if string.find(la, "ru") == 1 then
      lang = "ru"
    end
  end
  GameUIConsortium:GetConsortiumInfo()
  GameUIConsortium:GetFlashObject():InvokeASCallback("_root", "initConsortium", isLoginedIn, lang)
  GameUIConsortium:SendAllAndGetAllButtonStatus()
end
function GameUIConsortium:MoveOut()
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "hideConsortiumLab")
end
function GameUIConsortium:InitNoLoginedIn()
end
function GameUIConsortium:GetConsortiumInfo()
  DebugOut("GameUIConsortium:GetConsortiumInfo")
  NetMessageMgr:SendMsg(NetAPIList.financial_req.Code, nil, GameUIConsortium.GetConsortiumInfoCallback, true, nil)
end
function GameUIConsortium.GetConsortiumInfoCallback(msgType, content)
  DebugOut(msgType)
  if msgType == NetAPIList.financial_ack.Code then
    DebugOut("GameUIConsortium.GetConsortiumInfoCallback")
    DebugTable(content)
    if content then
      GameUIConsortium.level = content.level
      GameUIConsortium.benefit = content.benefit
      GameUIConsortium.next_level = content.next_level
      GameUIConsortium.next_benefit = content.next_benefit
      GameUIConsortium.upgrade_cost = content.upgrade_cost
      local maxText = ""
      local isMax = false
      if GameUIConsortium.level == GameUIConsortium.next_level then
        isMax = true
        maxText = GameLoader:GetGameText("LC_MENU_MAX_CHAR")
      end
      GameUIConsortium:GetFlashObject():InvokeASCallback("_root", "setConsortiumInfo", GameLoader:GetGameText("LC_MENU_Level") .. GameUIConsortium.level, GameUIConsortium.count, GameUIConsortium.benefit, isMax, GameLoader:GetGameText("LC_MENU_Level") .. maxText)
      if GameUIConsortium.isUpdateCommandFlag then
        GameUIConsortium.isUpdateCommandFlag = false
        GameUIConsortium:ShowConsortiumUpdate()
        GameUIConsortium:ShowConsortiumUpdateAnimation()
      end
    end
    return true
  end
  return false
end
function GameUIConsortium:GetFacebookFriendList()
  DebugOut("GameUIConsortium:GetFacebookFriendList")
  local function callback(success, friends)
    local function delayCall()
      GameUIConsortium.GetFacebookFriendListCallback(success, friends)
    end
    if NetMessageMgr.ReConnect then
      DebugOut("GameUIConsortium Now is reconnect server.")
      NetMessageMgr:AddReconnectSuccessCall(delayCall)
    else
      GameUIConsortium.GetFacebookFriendListCallback(success, friends)
    end
  end
  Facebook:GetFriendList(callback)
end
function GameUIConsortium.GetFacebookFriendListCallback(success, friends)
  DebugOut("GameUIConsortium.GetFacebookFriendListCallback")
  if success then
    DebugTable(friends)
    GameUIConsortium.facebookFriendList = friends.friendList
    local facebookFriendList = {}
    for key, value in ipairs(GameUIConsortium.facebookFriendList) do
      table.insert(facebookFriendList, value.id)
    end
    local parm = {}
    parm = {friends = facebookFriendList}
    DebugTable(parm)
    NetMessageMgr:SendMsg(NetAPIList.facebook_friends_req.Code, parm, GameUIConsortium.GetFacebookFriendListToServerCallback, true, nil)
  end
end
function GameUIConsortium.GetFacebookFriendListToServerCallback(msgType, content)
  if msgType == NetAPIList.facebook_friends_ack.Code then
    DebugOut("GameUIConsortium.GetFacebookFriendListToServerCallback")
    DebugTable(content)
    GameUIConsortium.isShowFlag = true
    GameUIConsortium:SortFacebookFriendList(content)
    return true
  end
  return false
end
function GameUIConsortium:SortFacebookFriendList(friendList)
  DebugOut("GameUIConsortium:SortFacebookFriendList")
  GameUIConsortium.friendList = friendList.friends
  table.sort(GameUIConsortium.friendList, GameUIConsortium.SortTabel)
  DebugTable(GameUIConsortium.friendList)
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "initFacebookDataList", #GameUIConsortium.friendList)
  end
  GameUIConsortium:SendAllAndGetAllButtonStatus()
end
function GameUIConsortium:SortFacebookFriendListWhenFull(friendList)
  DebugOut("GameUIConsortium:SortFacebookFriendListWhenFull")
  GameUIConsortium.friendList = friendList.friends
  table.sort(GameUIConsortium.friendList, GameUIConsortium.SortTabel)
  DebugTable(GameUIConsortium.friendList)
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "initFacebookDataList", #GameUIConsortium.friendList)
  end
  GameUIConsortium:SendAllAndGetAllButtonStatus()
end
function GameUIConsortium:SendAllAndGetAllButtonStatus()
  GameUIConsortium.getAllbuttonStatus = false
  GameUIConsortium.sendAllbuttonStatus = false
  for key, value in ipairs(GameUIConsortium.friendList) do
    if value.status == 1 and value.left_time > 0 then
      GameUIConsortium.getAllbuttonStatus = true
    end
    if value.status == 2 and value.left_time == 0 then
      GameUIConsortium.sendAllbuttonStatus = true
    end
  end
end
function GameUIConsortium:FindFacebookFriendList()
  for key, value in ipairs(GameUIConsortium.friendList) do
    if value.status == 1 and value.left_time > 0 then
      return true
    end
  end
  return false
end
function GameUIConsortium.SortTabel(a, b)
  if a.status == b.status then
    return a.left_time < b.left_time
  else
    return a.status < b.status
  end
end
function GameUIConsortium:ItemCommand(arg)
end
function GameUIConsortium:Update(dt)
  self:GetFlashObject():InvokeASCallback("_root", "UpdateDataList", dt)
  self:GetFlashObject():Update(dt)
  GameUIConsortium:UpdateItemTime(dt)
end
function GameUIConsortium.GetAcountOfGiftFromFriendsNtf(content)
  DebugOut("GameUIConsortium.GetAcountOfGiftFromFriendsNtf")
  DebugTable(content)
  DebugOut(GameUIConsortium.benefit)
  GameUIConsortium.count = tonumber(content.amount)
  local maxText = ""
  local isMax = false
  if GameUIConsortium.level == GameUIConsortium.next_level then
    isMax = true
    maxText = GameLoader:GetGameText("LC_MENU_MAX_CHAR")
  end
  if GameUIConsortium.benefit then
    local flash_obj = GameUIConsortium:GetFlashObject()
    if flash_obj then
      flash_obj:InvokeASCallback("_root", "setConsortiumInfo", GameUIConsortium.level, GameUIConsortium.count, GameUIConsortium.benefit, isMax, maxText)
    end
  end
end
function GameUIConsortium.GetConsortiumInfoNtf(content)
  DebugOut("GameUIConsortium.GetConsortiumInfoNtf")
  if content then
    GameUIConsortium.level = content.level
    GameUIConsortium.benefit = content.benefit
    GameUIConsortium.next_level = content.next_level
    GameUIConsortium.next_benefit = content.next_benefit
    GameUIConsortium.upgrade_cost = content.upgrade_cost
    local maxText = ""
    local isMax = false
    if GameUIConsortium.level == GameUIConsortium.next_level then
      isMax = true
      maxText = GameLoader:GetGameText("LC_MENU_MAX_CHAR")
    end
    if GameUIConsortium:GetFlashObject() then
      GameUIConsortium:GetFlashObject():InvokeASCallback("_root", "setConsortiumInfo", GameUIConsortium.level, GameUIConsortium.count, GameUIConsortium.benefit, isMax, maxText)
      do break end
      GameObjectMainPlanet:UpdateConsortiumState()
    else
      GameObjectMainPlanet:UpdateConsortiumStateInChinese()
    end
  end
end
function GameUIConsortium.GetFacebookDataChangeNtf(content)
  DebugOut("GameUIConsortium.facebook_friends_ntf")
  DebugTable(content)
  if #GameUIConsortium.friendList > 0 then
    DebugTable(GameUIConsortium.friendList)
    for key, value in ipairs(content.friends) do
      for i, j in ipairs(GameUIConsortium.friendList) do
        DebugOut(value.passport)
        DebugOut(j.passport)
        if value.passport == j.passport then
          DebugOut("passport:" .. value.passport)
          j.status = value.status
          j.left_time = value.left_time
          j.awards = value.awards
          if value.status == 2 and value.left_time == 0 then
            if GameUIConsortium:GetFlashObject() then
              GameUIConsortium.text = GameLoader:GetGameText("LC_MENU_FINANCIAL_GROUP_SEND")
              GameUIConsortium:GetFlashObject():InvokeASCallback("_root", "updateItemButtonState", i, 3, 0, GameUIConsortium.text, nil)
            end
          else
            if value.status == 1 then
              DebugOut("value.status:" .. value.status)
              GameUIConsortium.text = GameLoader:GetGameText("LC_MENU_FINANCIAL_GROUP_RECIEVE")
              if GameUIConsortium:GetFlashObject() then
                GameUIConsortium:GetFlashObject():InvokeASCallback("_root", "updateItemButtonState", i, value.status, GameUtils:formatTimeString(value.left_time), GameUIConsortium.text, value.awards[1].number)
              end
            end
            if value.status == 2 then
              GameUIConsortium.text = GameLoader:GetGameText("LC_MENU_FINANCIAL_GROUP_GIFT_TIME")
              if GameUIConsortium:GetFlashObject() then
                GameUIConsortium:GetFlashObject():InvokeASCallback("_root", "updateItemButtonState", i, value.status, GameUtils:formatTimeString(value.left_time), GameUIConsortium.text, nil)
              end
            end
          end
        end
      end
    end
  else
    GameUIConsortium.isHaveBenefit = content.friends
    do break end
    GameObjectMainPlanet:UpdateConsortiumState()
    do break end
    GameObjectMainPlanet:UpdateConsortiumStateInChinese()
  end
  GameUIConsortium:SendAllAndGetAllButtonStatus()
end
GameUIConsortium.mInviteFriendViewPoped = false
local FacebookPopUI = LuaObjectManager:GetLuaObject("FacebookPopUI")
function GameUIConsortium:FacebookButtonCallback()
  DebugOut("GameUIConsortium:FacebookButtonCallback")
  DebugOut(Facebook:IsLoginedIn())
  if Facebook:IsLoginedIn() then
    if GameUIConsortium.lastInviteTime then
      local now = os.time()
      if now - GameUIConsortium.lastInviteTime <= 2 then
        return
      end
    end
    GameUIConsortium.mInviteFriendViewPoped = true
    local title = GameLoader:GetGameText("LC_MENU_APPLICATION_REQUEST_TITLE")
    local msg = GameLoader:GetGameText("LC_MENU_APPLICATION_REQUEST_INFO_2")
    if not Facebook:GetCanFetchInvitableFriendList() then
      Facebook:InviteFriends(title, msg, nil, GameUIConsortium.FacebookInviteFriendsCallback)
    else
      FacebookPopUI.mInviteTitleText = GameLoader:GetGameText("LC_MENU_NEW_FACEBOOK_LOG_IN_BUTTON_4")
      FacebookPopUI:GetFacebookInvitableFriendList(true)
    end
  else
    Facebook:Login(GameUIConsortium.FacebookLoginCallback, true)
  end
end
function GameUIConsortium.FacebookLoginCallback(success, idOrError, token, username, email)
  DebugOut("GameUIConsortium.FacebookLoginCallback")
  DebugOut(success)
  if success then
    local isLoginedIn = Facebook:IsLoginedIn()
    DebugOut(tostring(isLoginedIn))
    GameUIConsortium:GetFacebookFriendList()
    Facebook:CheckAccountBind()
    Facebook.mIsBindFacebookAccount = true
    local flash_obj = GameUIConsortium:GetFlashObject()
    if flash_obj then
      flash_obj:InvokeASCallback("_root", "initConsortium", isLoginedIn)
    end
    return true
  else
    return false
  end
end
function GameUIConsortium.FacebookInviteFriendsCallback(success)
  GameUIConsortium.mInviteFriendViewPoped = false
  DebugOut("GameUIConsortium.FacebookInviteFriendsCallback")
  DebugOut(success)
  GameUIConsortium.lastInviteTime = os.time()
  if success then
    DebugOut("InviteFriends success")
    GameTip:Show(GameLoader:GetGameText("LC_MENU_INVITATION_SENT"))
  else
    local isLoginedIn = Facebook:IsLoginedIn()
    if not isLoginedIn then
      local flash_obj = GameUIConsortium:GetFlashObject()
      if flash_obj then
        flash_obj:InvokeASCallback("_root", "initConsortium", isLoginedIn)
      end
      GameUIConsortium.friendList = {}
      flash_obj:InvokeASCallback("_root", "initFacebookDataList", #GameUIConsortium.friendList)
    end
  end
end
function GameUIConsortium:ItemButtonClicked(arg)
  DebugOut("GameUIConsortium:ItemButtonClicked")
  DebugOut(arg)
  local item = GameUIConsortium.friendList[tonumber(arg)]
  if item then
    if item.status == 1 then
      GameUIConsortium:GetGiftButtonCallback(tonumber(arg))
    end
    if item.status == 2 and item.left_time == 0 then
      GameUIConsortium:SendFriendButtonCallback(tonumber(arg))
    end
  end
end
function GameUIConsortium:GetGiftButtonCallback(arg)
  DebugOut("GameUIConsortium:GetGiftButtonCallback")
  local item = GameUIConsortium.friendList[arg]
  local parm = {}
  if item then
    table.insert(parm, item.passport)
  end
  local parmtosever = {friends = parm}
  DebugTable(parmtosever)
  NetMessageMgr:SendMsg(NetAPIList.financial_gifts_get_req.Code, parmtosever, GameUIConsortium.GetAllGiftButtonCallbackToServerCallback, true, nil)
end
function GameUIConsortium:GetAllGiftButtonCallback()
  DebugOut("GameUIConsortium:GetAllGiftButtonCallback")
  local parm = {}
  for key, value in ipairs(GameUIConsortium.friendList) do
    table.insert(parm, value.passport)
  end
  local parmtosever = {friends = parm}
  NetMessageMgr:SendMsg(NetAPIList.financial_gifts_get_req.Code, parmtosever, GameUIConsortium.GetAllGiftButtonCallbackToServerCallback, true, nil)
end
function GameUIConsortium.GetAllGiftButtonCallbackToServerCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.financial_gifts_get_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  return false
end
function GameUIConsortium:SendFriendButtonCallback(arg)
  DebugOut("GameUIConsortium:SendFriendButtonCallback")
  local item = GameUIConsortium.friendList[arg]
  local parm = {}
  if item then
    table.insert(parm, item.passport)
  end
  local parmtosever = {type = 2, friends = parm}
  DebugTable(parmtosever)
  NetMessageMgr:SendMsg(NetAPIList.give_friends_gift_req.Code, parmtosever, GameUIConsortium.SendFriendButtonCallbackToServerCallBack, true, nil)
end
function GameUIConsortium:SendAllFriendButtonCallback()
  DebugOut("GameUIConsortium:SendAllFriendButtonCallback")
  local parm = {}
  for key, value in ipairs(GameUIConsortium.friendList) do
    table.insert(parm, value.passport)
  end
  local parmtosever = {type = 2, friends = parm}
  NetMessageMgr:SendMsg(NetAPIList.give_friends_gift_req.Code, parmtosever, GameUIConsortium.SendFriendButtonCallbackToServerCallBack, true, nil)
end
function GameUIConsortium.SendFriendButtonCallbackToServerCallBack(msgType, content)
  if msgType == NetAPIList.give_friends_gift_ack.Code then
    return true
  end
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.give_friends_gift_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  return false
end
function GameUIConsortium:UpdateItemTime(dt)
  GameUIConsortium.time = GameUIConsortium.time + dt
  if GameUIConsortium.time >= 1000 then
    GameUIConsortium.time = 0
    if 0 < #GameUIConsortium.friendList then
      for key, value in ipairs(GameUIConsortium.friendList) do
        if value.status == 2 and value.left_time == 0 then
          value.left_time = 0
          if GameUIConsortium:GetFlashObject() then
            GameUIConsortium.text = GameLoader:GetGameText("LC_MENU_FINANCIAL_GROUP_SEND")
            GameUIConsortium:GetFlashObject():InvokeASCallback("_root", "updateItemTime", key, 3, 0, GameUIConsortium.text)
          end
        else
          value.left_time = value.left_time - 1
          if value.status == 1 then
            GameUIConsortium.text = GameLoader:GetGameText("LC_MENU_FINANCIAL_GROUP_RECIEVE")
          end
          if value.status == 2 then
            GameUIConsortium.text = GameLoader:GetGameText("LC_MENU_FINANCIAL_GROUP_GIFT_TIME")
          end
          if GameUIConsortium:GetFlashObject() then
            GameUIConsortium:GetFlashObject():InvokeASCallback("_root", "updateItemTime", key, value.status, GameUtils:formatTimeString(value.left_time), GameUIConsortium.text)
          end
        end
        if value.status == 1 and value.left_time == 0 then
          GameUIConsortium:GetFacebookFriendList()
        end
      end
    end
  end
end
function GameUIConsortium:isShowHighLight()
  DebugOut("GameUIConsortium:isShowHighLight")
  local isShow = false
  for key, value in ipairs(GameUIConsortium.isHaveBenefit) do
    if value.status == 1 and value.left_time > 0 then
      isShow = true
    end
  end
  if GameUIConsortium.isShowFlag then
    if 0 < GameUIConsortium.count and isShow and GameUIConsortium:FindFacebookFriendList() then
      GameUIConsortium.isShowFlag = false
      return true
    end
  elseif 0 < GameUIConsortium.count and isShow then
    return true
  end
  return false
end
GameUIConsortium.mItemTextureName = {}
function GameUIConsortium:UpdateFBHeadIcon(itemId, mcId, fbId)
  DebugOut("itemId" .. itemId)
  DebugOut("mcId:" .. mcId)
  local function callback(fbId, filename)
    local function timeCall()
      if GameUIConsortium.mInviteFriendViewPoped then
        DebugOut("UpdateFBHeadIcon GameUIConsortium.mInviteFriendViewPoped ")
        return 15
      else
        local flashObj = GameUIConsortium:GetFlashObject()
        if flashObj then
          local orgTexture = string.format("facebook_avatar%02d.png", tonumber(mcId + 1))
          DebugOut("orgTexture " .. orgTexture .. " filename " .. filename)
          flashObj:ReplaceTexture(orgTexture, filename)
          local headIconFr = string.format("facebook_avatar%02d", mcId + 1)
          flashObj:InvokeASCallback("_root", "updateItemHeadIcon", itemId, headIconFr)
        end
        return nil
      end
    end
    if GameUIConsortium.mInviteFriendViewPoped then
      GameTimer:Add(timeCall, 15)
    else
      timeCall()
    end
  end
  Facebook:GetHeadIcon(fbId, callback)
end
if AutoUpdate.isAndroidDevice then
  function GameUIConsortium.OnAndroidBack()
    if GameUIConsortium.IsShowConsortiumUpdate then
      GameUIConsortium:GetFlashObject():InvokeASCallback("_root", "CloseDialogUpgrade")
    else
      GameUIConsortium:OnFSCommand("close_menu")
    end
  end
end
