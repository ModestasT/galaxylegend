local GameStateColonization = GameStateManager.GameStateColonization
local GameStateBattlePlay = GameStateManager.GameStateBattlePlay
local GameUIBattleResult = LuaObjectManager:GetLuaObject("GameUIBattleResult")
local GameUIColonial = LuaObjectManager:GetLuaObject("GameUIColonial")
local GameUISlaveSelect = LuaObjectManager:GetLuaObject("GameUISlaveSelect")
local GameUIColonialMenu = LuaObjectManager:GetLuaObject("GameUIColonialMenu")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
GameUIColonialMenu.LayerType = {
  unknown = -1,
  help = 0,
  slave = 1,
  save = 2,
  colonist = 3
}
GameUIColonialMenu.playerList_ally = {}
GameUIColonialMenu.PlayerListType = {ally = 11}
GameUIColonialMenu.expReapShow = 0
GameUIColonialMenu.curLayerType = GameUIColonialMenu.LayerType.unknown
GameUIColonialMenu.fetchTime = 0
GameUIColonialMenu.lastTime = 0
GameUIColonialMenu.curTricky = {}
GameUIColonialMenu.curTricky.UserInfo = nil
GameUIColonialMenu.curTricky.Type = 0
GameUIColonialMenu.curPlayful = {}
GameUIColonialMenu.curPlayful.UserInfo = nil
GameUIColonialMenu.curPlayful.Type = 0
GameUIColonialMenu.curMaster = {}
GameUIColonialMenu.curMaster.UserInfo = nil
GameUIColonialMenu.curMaster.AllyUserInfo = nil
GameUIColonialMenu.curSave = {}
GameUIColonialMenu.curSave.UserInfo = nil
GameUIColonialMenu.curSave.BeSavedUserID = ""
GameUIColonialMenu.curSave.BeSavedUserName = ""
GameUIColonialMenu.colonistBtnEnaled = nil
GameUIColonialMenu.slaveBtnEnaled = nil
GameUIColonialMenu.mShareStoryChecked = {}
GameUIColonialMenu.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_COLONIAL_SLAVER_TRICK] = true
GameUIColonialMenu.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_COLONIAL_ORDER_SLAVER] = true
function GameUIColonialMenu:OnInitGame()
  DebugColonial("GameUIColonialMenu:OnInitGame()")
  GameGlobalData:RegisterDataChangeCallback("cdtimes", GameUIColonialMenu.OnTrickyCDTimeChange)
end
function GameUIColonialMenu:Init()
end
function GameUIColonialMenu:OnAddToGameState()
  DebugColonial("GameUIColonialMenu:OnAddToGameState()")
  if not self:GetFlashObject() then
    assert(false)
    self:LoadFlashObject()
  end
  self:Init()
end
function GameUIColonialMenu:OnEraseFromGameState()
  GameUIColonialMenu.curLayerType = GameUIColonialMenu.LayerType.unknown
  GameUIColonialMenu.colonistBtnEnaled = nil
  GameUIColonialMenu.slaveBtnEnaled = nil
  DebugFestival("GameUIColonialMenu:OnEraseFromGameState()")
end
function GameUIColonialMenu:MoveIn(layerType, shareStoryEnabled, shareChecked)
  if layerType == GameUIColonialMenu.LayerType.unknown then
    return
  end
  self.curLayerType = layerType
  local flash_obj = self:GetFlashObject()
  if self.curLayerType == GameUIColonialMenu.LayerType.help then
    self:RefreshHelpList()
  end
  self:SetTimesInfo()
  flash_obj:InvokeASCallback("_root", "animationMoveIn", self.curLayerType, shareStoryEnabled, shareChecked)
  self.IsShowChildWindow = true
end
function GameUIColonialMenu:MoveOut()
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "animationMoveOut")
  self.IsShowChildWindow = false
end
function GameUIColonialMenu:Update(dt)
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    if GameUIColonialMenu.curLayerType == GameUIColonialMenu.LayerType.help then
      flash_obj:InvokeASCallback("_root", "OnUpdate")
    end
    if GameUIColonialMenu.curLayerType == GameUIColonialMenu.LayerType.colonist then
      self:SetColonistCDTime()
    elseif GameUIColonialMenu.curLayerType == GameUIColonialMenu.LayerType.slave then
      self:SetSlaveCDTime()
      self:UpdateExploitExpBar(dt)
    end
    flash_obj:Update(dt)
  end
end
function GameUIColonialMenu:SetExpInfo()
  if GameUIColonialMenu.curLayerType == GameUIColonialMenu.LayerType.slave then
    self:UpdateExploitBtnState()
  end
end
function GameUIColonialMenu:SetTimesInfo()
  if GameUIColonialMenu.curLayerType == GameUIColonialMenu.LayerType.unknown then
    return false
  end
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    local timesStr = self:GetTimesString()
    flash_obj:InvokeASCallback("_root", "setTimesText", GameUIColonialMenu.curLayerType, timesStr)
    if GameUIColonialMenu.curLayerType == GameUIColonialMenu.LayerType.slave then
      local _lefttime = self.lastTime - (os.time() - self.fetchTime)
      local enabled = GameStateColonization:GetActiveTimes(GameStateColonization.TimesType.action_playful) > 0 and _lefttime <= 0
      if GameUIColonialMenu.slaveBtnEnaled ~= enabled then
        GameUIColonialMenu.slaveBtnEnaled = enabled
        flash_obj:InvokeASCallback("_root", "SetSlaveActionBtnState", enabled)
      end
    elseif GameUIColonialMenu.curLayerType == GameUIColonialMenu.LayerType.colonist then
      local _lefttime = self.lastTimeForTricky - (os.time() - self.fetchTimeForTricky)
      local enabled = 0 < GameStateColonization:GetActiveTimes(GameStateColonization.TimesType.action_tricky) and _lefttime <= 0
      if GameUIColonialMenu.colonistBtnEnaled ~= enabled then
        GameUIColonialMenu.colonistBtnEnaled = enabled
        flash_obj:InvokeASCallback("_root", "SetColonistActionBtnState", enabled)
      end
    end
  end
  return true
end
function GameUIColonialMenu:GetTimesString()
  local timesStr = ""
  if GameUIColonialMenu.curLayerType == GameUIColonialMenu.LayerType.slave then
    timesStr = GameStateColonization:GetTimesString(GameStateColonization.TimesType.action_playful)
  elseif GameUIColonialMenu.curLayerType == GameUIColonialMenu.LayerType.help then
    timesStr = GameStateColonization:GetTimesString(GameStateColonization.TimesType.help)
  elseif GameUIColonialMenu.curLayerType == GameUIColonialMenu.LayerType.colonist then
    timesStr = GameStateColonization:GetTimesString(GameStateColonization.TimesType.action_tricky)
  elseif GameUIColonialMenu.curLayerType == GameUIColonialMenu.LayerType.save then
    timesStr = GameStateColonization:GetTimesString(GameStateColonization.TimesType.rescue)
  end
  return timesStr
end
function GameUIColonialMenu:OnFSCommand(cmd, arg)
  DebugColonial("colonial command: ", cmd, arg)
  if cmd == "auto_close_menu" then
    self:MoveOut()
  elseif cmd == "move_in_finished" then
    if #GameUIColonialMenu.playerList_ally == 0 and GameUIColonialMenu.curLayerType == GameUIColonialMenu.LayerType.help then
      local playerList = {}
      table.insert(playerList, GameUIColonialMenu.PlayerListType.ally)
      local requestParam = {types = playerList}
      NetMessageMgr:SendMsg(NetAPIList.colony_users_req.Code, requestParam, GameUIColonialMenu.FetchListCallback, true, nil)
    end
  elseif cmd == "boxHiden" then
    GameUIColonialMenu.playerList_ally = {}
    GameStateColonization:EraseObject(self)
  elseif cmd == "needUpdateHelpItem" then
    self:UpdateHelpItem(arg)
  elseif cmd == "helpItemReleased" then
    self:HelpFight(arg)
  elseif cmd == "colonial_action_released" then
    local actionId = tonumber(arg)
    GameUIColonialMenu.curTricky.Type = GameUIColonialMenu.trickyType.list[actionId]
    local requestParam = {
      action_id = GameUIColonialMenu.curTricky.Type,
      receiver_id = GameUIColonialMenu.curTricky.UserInfo.player_id,
      receiver_pos = GameUIColonialMenu.curTricky.UserInfo.position
    }
    DebugColonial("colonial_action_released")
    DebugColonialTable(requestParam)
    NetMessageMgr:SendMsg(NetAPIList.colony_fawn_req.Code, requestParam, GameUIColonialMenu.TrickyCallback, true, nil)
  elseif cmd == "slave_action_released" then
    local actionId = tonumber(arg)
    GameUIColonialMenu.curPlayful.Type = GameUIColonialMenu.playfulType.list[actionId]
    local requestParam = {
      action_id = GameUIColonialMenu.curPlayful.Type,
      receiver_id = GameUIColonialMenu.curPlayful.UserInfo.player_id,
      receiver_pos = GameUIColonialMenu.curPlayful.UserInfo.position
    }
    DebugColonial("slave_action_released")
    DebugColonialTable(requestParam)
    DebugColonialTable(GameUIColonialMenu.curPlayful)
    NetMessageMgr:SendMsg(NetAPIList.colony_fawn_req.Code, requestParam, GameUIColonialMenu.PlayfulCallback, true, nil)
  elseif cmd == "del_released" then
    local text_title = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
    local text_content = GameLoader:GetGameText("LC_MENU_EVENT_COLONIAL_ABANDON_CHAR")
    GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
    GameUIMessageDialog:SetYesButton(function()
      local requestParam = {
        slave_id = GameUIColonialMenu.curPlayful.UserInfo.player_id
      }
      DebugColonial("del_released")
      DebugColonialTable(requestParam)
      DebugColonialTable(GameUIColonialMenu.curPlayful.UserInfo)
      NetMessageMgr:SendMsg(NetAPIList.colony_release_req.Code, requestParam, GameUIColonialMenu.DelSlaveCallback, true, nil)
    end)
    GameUIMessageDialog:Display(text_title, text_content)
  elseif cmd == "exp_released" then
    if self.expReapShow == 0 then
      self:SendPayColonyExploit()
    else
      self:SendFreeColonyExploit()
    end
  elseif cmd == "buyCredit" then
    do
      local function purchase_callback()
        local requestParam = {
          action_type = GameStateColonization.TimesType.rescue
        }
        NetMessageMgr:SendMsg(NetAPIList.colony_action_purchase_req.Code, requestParam, GameUIColonialMenu.BuyTimesListCallback, true, nil)
      end
      if GameSettingData.EnablePayRemind ~= nil and GameSettingData.EnablePayRemind then
        do
          local price = 300
          local function price_callback(msgType, content)
            if msgType == NetAPIList.common_ack.Code then
              if content.api == NetAPIList.price_req.Code then
                if content.code ~= 0 then
                  GameUIGlobalScreen:ShowAlert("error", content.code, nil)
                end
                return true
              end
            elseif msgType == NetAPIList.price_ack.Code then
              price = content.price
              local text_content = GameLoader:GetGameText("LC_MENU_EVENT_COLONIAL_LIBERATION_BUY_CHAR")
              text_content = string.format(text_content, price)
              GameUtils:CreditCostConfirm(text_content, purchase_callback)
              return true
            end
            return false
          end
          local price_requestParam = {
            price_type = "rescue_cost",
            type = 0
          }
          NetMessageMgr:SendMsg(NetAPIList.price_req.Code, price_requestParam, price_callback, true, nil)
        end
      else
        purchase_callback()
      end
    end
  elseif cmd == "save_released" then
    local requestParam = {
      fight_type = GameStateColonization.FightType.save,
      def_id = GameUIColonialMenu.curSave.UserInfo.player_id,
      def_pos = GameUIColonialMenu.curSave.UserInfo.position,
      third_id = GameUIColonialMenu.curSave.BeSavedUserID
    }
    DebugColonialTable(requestParam)
    if GameStateManager:GetCurrentGameState().fightRoundData then
      GameStateManager:GetCurrentGameState().fightRoundData = nil
    end
    NetMessageMgr:SendMsg(NetAPIList.colony_challenge_req.Code, requestParam, GameUIColonialMenu.SaveFightCallback, true, nil)
  elseif cmd == "shareOrderSlaveCheckboxClicked" then
    if self.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_COLONIAL_ORDER_SLAVER] then
      self.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_COLONIAL_ORDER_SLAVER] = false
    else
      self.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_COLONIAL_ORDER_SLAVER] = true
    end
    local flash_obj = self:GetFlashObject()
    if flash_obj then
      flash_obj:InvokeASCallback("_root", "setCheckBox", self.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_COLONIAL_ORDER_SLAVER], FacebookGraphStorySharer.StoryType.STORY_TYPE_COLONIAL_ORDER_SLAVER)
    end
  elseif cmd == "shareTrickCheckboxClicked" then
    if self.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_COLONIAL_SLAVER_TRICK] then
      self.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_COLONIAL_SLAVER_TRICK] = false
    else
      self.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_COLONIAL_SLAVER_TRICK] = true
    end
    local flash_obj = self:GetFlashObject()
    if flash_obj then
      flash_obj:InvokeASCallback("_root", "setCheckBox", self.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_COLONIAL_SLAVER_TRICK], FacebookGraphStorySharer.StoryType.STORY_TYPE_COLONIAL_SLAVER_TRICK)
    end
  end
end
function GameUIColonialMenu:CheckUserInCD(userInfo, fetch_time)
  if userInfo == nil then
    return "none"
  end
  local lastTime = userInfo.reverse_left
  local fetchTime = fetch_time
  local restTime = lastTime - (os.time() - fetchTime)
  if restTime < 0 then
    restTime = 0
  end
  if restTime <= 0 then
    return "none"
  else
    return ...
  end
end
function GameUIColonialMenu:SetSlaveLayerInfo(userInfo, fetch_time)
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    GameUIColonialMenu.curPlayful.UserInfo = LuaUtils:table_copy(userInfo)
    self.lastTime = GameUIColonialMenu.curPlayful.UserInfo.reverse_left
    self.fetchTime = fetch_time
    local nexttime = "0"
    local _lefttime = self.lastTime - (os.time() - self.fetchTime)
    if _lefttime < 0 then
      _lefttime = 0
    end
    nexttime = GameUtils:formatTimeString(_lefttime)
    local lefttimes = self:GetTimesString()
    local flash_obj = self:GetFlashObject()
    if flash_obj then
      local s
      local avatar, name, status, lv, force, alliance, icon = GameStateColonization:GetUserInfo(GameUIColonialMenu.curPlayful.UserInfo)
      local curExp = GameUIColonialMenu.curPlayful.UserInfo.product.experience + math.floor(GameUIColonialMenu.curPlayful.UserInfo.product.speed * (os.time() - GameUIColonialMenu.curPlayful.UserInfo.product.fetch_os_time) / 60)
      local maxExp = GameUIColonialMenu.curPlayful.UserInfo.product.max_experience
      if curExp > maxExp then
        curExp = maxExp
      end
      self.expReapShow = curExp
      local percent = 0
      if maxExp <= 0 then
        percent = 0
      else
        percent = math.floor(curExp / maxExp * 100)
      end
      local expStr = curExp .. "/" .. maxExp
      DebugColonial("SetSlaveLayerInfo = ", avatar, name, status, lv, force, alliance, nexttime, lefttimes, expStr, percent)
      flash_obj:InvokeASCallback("_root", "SetSlaveLayerInfo", avatar, name, status, lv, force, alliance, nexttime, lefttimes, expStr, percent)
      local levelInfo = GameGlobalData:GetData("levelinfo")
      if levelInfo.level == GameGlobalData.max_level then
        flash_obj:InvokeASCallback("_root", "SetSlaveTitleText", GameLoader:GetGameText("LC_MENU_EVENT_COLONIAL_MONEY_PRODUCT_CHAR"))
      end
      self:UpdateExploitBtnState()
    end
  end
end
function GameUIColonialMenu:UpdateExploitExpBar(dt)
  if GameUIColonialMenu.curPlayful.UserInfo ~= nil and GameUIColonial:IsFreshEXP() then
    local flash_obj = self:GetFlashObject()
    if flash_obj then
      local curExp = GameUIColonialMenu.curPlayful.UserInfo.product.experience + math.floor(GameUIColonialMenu.curPlayful.UserInfo.product.speed * (os.time() - GameUIColonialMenu.curPlayful.UserInfo.product.fetch_os_time) / 60)
      local maxExp = GameUIColonialMenu.curPlayful.UserInfo.product.max_experience
      if curExp > maxExp then
        curExp = maxExp
      end
      local percent = 0
      if maxExp <= 0 then
        percent = 0
      else
        percent = math.floor(curExp / maxExp * 100)
      end
      self.expReapShow = curExp
      local expStr = curExp .. "/" .. maxExp
      flash_obj:InvokeASCallback("_root", "SetSlaveExpBar", expStr, percent)
    end
  end
end
function GameUIColonialMenu:UpdateExploitBtnState()
  local active = true
  if GameUIColonialMenu.curPlayful.UserInfo ~= nil and GameStateColonization.curExpInfo ~= nil then
    if GameUIColonialMenu.curPlayful.UserInfo.product.max_experience == 0 then
      active = false
    end
    if GameStateColonization.curExpInfo.ntf.experience >= GameStateColonization.curExpInfo.ntf.max_experience then
      active = false
    end
    local flash_obj = self:GetFlashObject()
    if flash_obj then
      flash_obj:InvokeASCallback("_root", "SetExploitBtnState", active)
    end
  end
end
function GameUIColonialMenu:SetSlaveCDTime()
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    local nexttime = "0"
    local _lefttime = self.lastTime - (os.time() - self.fetchTime)
    if _lefttime < 0 then
      _lefttime = 0
    end
    local visible = false
    if _lefttime > 0 then
      visible = true
    end
    nexttime = GameUtils:formatTimeString(_lefttime)
    local title = GameLoader:GetGameText("LC_MENU_EVENT_COLONIAL_NEXT_REQUIRE_CHAR")
    flash_obj:InvokeASCallback("_root", "SetSlaveCDTime", title, nexttime, visible)
    local enabled = 0 < GameStateColonization:GetActiveTimes(GameStateColonization.TimesType.action_playful) and _lefttime <= 0
    if GameUIColonialMenu.slaveBtnEnaled ~= enabled then
      GameUIColonialMenu.slaveBtnEnaled = enabled
      flash_obj:InvokeASCallback("_root", "SetSlaveActionBtnState", enabled)
    end
  end
end
GameUIColonialMenu.playfulType = {}
GameUIColonialMenu.playfulType.min = 1
GameUIColonialMenu.playfulType.split = 6
GameUIColonialMenu.playfulType.max = 10
GameUIColonialMenu.playfulType.list = {}
function GameUIColonialMenu:SetRandomSlaveAction()
  math.randomseed(tostring(os.time()):reverse():sub(1, 6))
  GameUIColonialMenu.playfulType.list[1] = math.random(GameUIColonialMenu.playfulType.min, GameUIColonialMenu.playfulType.split)
  GameUIColonialMenu.playfulType.list[2] = math.random(GameUIColonialMenu.playfulType.split + 1, GameUIColonialMenu.playfulType.max)
  GameUIColonialMenu.playfulType.list[3] = math.random(GameUIColonialMenu.playfulType.min, GameUIColonialMenu.playfulType.max)
  while GameUIColonialMenu.playfulType.list[3] == GameUIColonialMenu.playfulType.list[2] or GameUIColonialMenu.playfulType.list[3] == GameUIColonialMenu.playfulType.list[1] do
    GameUIColonialMenu.playfulType.list[3] = math.random(GameUIColonialMenu.playfulType.min, GameUIColonialMenu.playfulType.max)
  end
  DebugColonial("SetRandomSlaveAction")
  DebugColonialTable(GameUIColonialMenu.playfulType.list)
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    local s1 = GameLoader:GetGameText("LC_MENU_EVENT_COLONIAL_REQUIRE_" .. GameUIColonialMenu.playfulType.list[1] .. "_BUTTON")
    local s2 = GameLoader:GetGameText("LC_MENU_EVENT_COLONIAL_REQUIRE_" .. GameUIColonialMenu.playfulType.list[2] .. "_BUTTON")
    local s3 = GameLoader:GetGameText("LC_MENU_EVENT_COLONIAL_REQUIRE_" .. GameUIColonialMenu.playfulType.list[3] .. "_BUTTON")
    flash_obj:InvokeASCallback("_root", "SetSlaveActionStr", s1, s2, s3)
  end
end
function GameUIColonialMenu:ShowSlaveLayer()
  if not GameStateManager:GetCurrentGameState():IsObjectInState(GameUIColonialMenu) then
    GameStateColonization:AddObject(self)
  end
  local shareStoryEnabled = false
  if FacebookGraphStorySharer:IsStoryEnabled(FacebookGraphStorySharer.StoryType.STORY_TYPE_COLONIAL_ORDER_SLAVER) then
    shareStoryEnabled = true
  end
  self:SetRandomSlaveAction()
  self:MoveIn(GameUIColonialMenu.LayerType.slave, shareStoryEnabled, self.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_COLONIAL_ORDER_SLAVER])
end
function GameUIColonialMenu.PlayfulCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code then
    if content.api == NetAPIList.colony_fawn_req.Code then
      if content.code ~= 0 then
        GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      end
      return true
    end
  elseif msgType == NetAPIList.colony_fawn_ack.Code then
    DebugColonial("PlayfulCallback")
    DebugColonialTable(content)
    local GameTip = LuaObjectManager:GetLuaObject("GameTip")
    if 0 < content.experience then
      local oriMsg = GameLoader:GetGameText("LC_MENU_EVENT_COLONIAL_REQUIRE_" .. GameUIColonialMenu.curPlayful.Type .. "_MESSAGE")
      local levelInfo = GameGlobalData:GetData("levelinfo")
      if levelInfo.level == GameGlobalData.max_level then
        oriMsg = GameLoader:GetGameText("LC_MENU_EVENT_COLONIAL_REQUIRE_" .. GameUIColonialMenu.curPlayful.Type .. "_MESSAGE_MONEY")
      end
      oriMsg = string.format(oriMsg, content.experience)
      local msg = GameStateColonization:MakeMsgs(oriMsg, "", GameUIColonialMenu.curPlayful.UserInfo.name, "", "", false)
      GameTip:Show(msg)
    else
      local oriMsg = GameLoader:GetGameText("LC_MENU_EVENT_COLONIAL_SLAVE_EXP_MAX_ALERT")
      GameTip:Show(oriMsg)
    end
    local countryStr = ext.GetDeviceCountry and ext.GetDeviceCountry() or "unknow"
    if GameUIColonialMenu.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_COLONIAL_ORDER_SLAVER] then
      local extraInfo = {}
      extraInfo.playerName = GameUIColonialMenu.curPlayful.UserInfo.name or ""
      FacebookGraphStorySharer:ShareStory(FacebookGraphStorySharer.StoryType.STORY_TYPE_COLONIAL_ORDER_SLAVER, extraInfo)
    elseif not FacebookGraphStorySharer:IsStoryEnabled(FacebookGraphStorySharer.StoryType.STORY_TYPE_COLONIAL_ORDER_SLAVER) or FacebookGraphStorySharer:IsStoryEnabled(FacebookGraphStorySharer.StoryType.STORY_TYPE_COLONIAL_ORDER_SLAVER) then
    end
    GameUIColonialMenu:SetRandomSlaveAction()
    return true
  end
  return false
end
function GameUIColonialMenu:SetSaveLayerInfo(userInfo, beSavedUserId, beSavedUserName)
  GameUIColonialMenu.curSave.UserInfo = LuaUtils:table_rcopy(userInfo)
  GameUIColonialMenu.curSave.BeSavedUserID = beSavedUserId
  GameUIColonialMenu.curSave.BeSaveedUserName = beSavedUserName
  local flash_obj = self:GetFlashObject()
  if flash_obj and GameUIColonialMenu.curSave.UserInfo ~= nil then
    local avatar, name, status, lv, force, alliance, icon = GameStateColonization:GetUserInfo(GameUIColonialMenu.curSave.UserInfo)
    local timesStr = self:GetTimesString()
    self:GetFlashObject():InvokeASCallback("_root", "SetSaveLayerInfo", avatar, name, status, lv, force, alliance, timesStr)
  end
end
function GameUIColonialMenu:ShowSaveLayer()
  if not GameStateManager:GetCurrentGameState():IsObjectInState(GameUIColonialMenu) then
    GameStateColonization:AddObject(self)
  end
  self:MoveIn(GameUIColonialMenu.LayerType.save, false, false)
end
function GameUIColonialMenu.SaveFightCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code then
    if content.api == NetAPIList.colony_challenge_req.Code then
      if content.code ~= 0 then
        GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      end
      return true
    end
  elseif msgType == NetAPIList.colony_challenge_ack.Code then
    DebugColonial("GameUIColonial.SaveFightCallback")
    DebugColonial(msgType)
    DebugColonialTable(content)
    do
      local window_type = ""
      GameUIBattleResult:SetFightReport(content.report, nil, nil)
      if content.result == 1 then
        GameStateColonization.directlyTo.locate = GameStateColonization.directlyTo.type.colonial
        local oriMsg = GameLoader:GetGameText("LC_MENU_EVENT_COLONIAL_YOU_LIB_SLAVE_SUCSS_MESSAGE")
        local msg = GameStateColonization:MakeMsgs(oriMsg, GameUIColonialMenu.curSave.UserInfo.name, GameUIColonialMenu.curSave.BeSaveedUserName, "", "", false)
        local userinfo = GameGlobalData:GetUserInfo()
        local nameText = GameUtils:GetUserDisplayName(userinfo.name)
        local level_info = GameGlobalData:GetData("levelinfo")
        local lv = GameLoader:GetGameText("LC_MENU_Level") .. level_info.level
        GameUIBattleResult:updateColonialWinInfo("0", nameText, lv, msg)
        window_type = "colonial_win"
      else
        GameStateColonization.directlyTo.locate = GameStateColonization.directlyTo.type.colonial
        window_type = "colonial_lose"
      end
      local lastGameState = GameStateManager:GetCurrentGameState()
      if #content.report.rounds == 0 and GameStateManager:GetCurrentGameState().fightRoundData then
        content.report.rounds = GameStateManager:GetCurrentGameState().fightRoundData
      end
      GameStateBattlePlay.curBattleType = "UIColonialMenu"
      GameStateBattlePlay:InitBattle(GameStateBattlePlay.MODE_HISTORY, content.report)
      GameStateBattlePlay:RegisterOverCallback(function()
        GameStateManager:SetCurrentGameState(lastGameState)
        GameUIBattleResult:LoadFlashObject()
        GameUIBattleResult:AnimationMoveIn(window_type, false)
        lastGameState:AddObject(GameUIBattleResult)
      end)
      GameStateManager:SetCurrentGameState(GameStateBattlePlay)
      return true
    end
  end
  return false
end
function GameUIColonialMenu.BuyTimesListCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code then
    DebugColonial("BuyTimesListCallback")
    DebugColonial(msgType)
    DebugColonialTable(content)
    if content.api == NetAPIList.colony_action_purchase_req.Code then
      if content.code ~= 0 then
        GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      else
        local GameTip = LuaObjectManager:GetLuaObject("GameTip")
        GameTip:Show(GameLoader:GetGameText("LC_MENU_PURCHASE_SUCCESS_CHAR"))
      end
      return true
    end
  end
  return false
end
function GameUIColonialMenu:SetHelpLayerInfo(userInfo)
  GameUIColonialMenu.curMaster.UserInfo = LuaUtils:table_rcopy(userInfo)
  local data_alliance = GameGlobalData:GetData("alliance")
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    flash_obj:InvokeASCallback("_root", "SetAllianceName", data_alliance.alliance_name)
  end
end
function GameUIColonialMenu:ShowHelpLayer()
  if not GameStateManager:GetCurrentGameState():IsObjectInState(GameUIColonialMenu) then
    GameStateColonization:AddObject(self)
  end
  self:MoveIn(GameUIColonialMenu.LayerType.help, false, false)
end
function GameUIColonialMenu:RefreshHelpList()
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    flash_obj:InvokeASCallback("_root", "clearListItem")
    if #GameUIColonialMenu.playerList_ally > 0 then
      local itemCount = #GameUIColonialMenu.playerList_ally
      flash_obj:InvokeASCallback("_root", "initListItem", itemCount)
      for i = 1, itemCount do
        flash_obj:InvokeASCallback("_root", "addListItem", i)
      end
    end
  end
end
function GameUIColonialMenu:UpdateHelpItem(id)
  local itemKey = tonumber(id)
  local userInfo = GameUIColonialMenu.playerList_ally[itemKey]
  local avatar, name, status, lv, force, alliance, icon = GameStateColonization:GetUserInfo(userInfo)
  local helpText = GameLoader:GetGameText("LC_MENU_EVENT_COLONIAL_ASK_HELP_BUTTON")
  local forceText = GameLoader:GetGameText("LC_MENU_FRIEND_BATTLE")
  self:GetFlashObject():InvokeASCallback("_root", "setItem", itemKey, name, avatar, status, lv, force, helpText, forceText)
end
function GameUIColonialMenu:HelpFight(arg)
  local itemKey = tonumber(arg)
  DebugColonial("HelpFight")
  GameUIColonialMenu.curMaster.AllyUserInfo = LuaUtils:table_rcopy(GameUIColonialMenu.playerList_ally[itemKey])
  DebugColonialTable(GameUIColonialMenu.curMaster.AllyUserInfo)
  local requestParam = {
    fight_type = GameStateColonization.FightType.help,
    def_id = GameUIColonialMenu.curMaster.UserInfo.player_id,
    def_pos = GameUIColonialMenu.curMaster.UserInfo.position,
    third_id = GameUIColonialMenu.curMaster.AllyUserInfo.player_id
  }
  if GameStateManager:GetCurrentGameState().fightRoundData then
    GameStateManager:GetCurrentGameState().fightRoundData = nil
  end
  DebugColonialTable(requestParam)
  NetMessageMgr:SendMsg(NetAPIList.colony_challenge_req.Code, requestParam, GameUIColonialMenu.HelpFightCallback, true, nil)
end
function GameUIColonialMenu.FetchListCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code then
    if content.api == NetAPIList.colony_users_req.Code then
      if content.code ~= 0 then
        GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      end
      return true
    end
  elseif msgType == NetAPIList.colony_users_ack.Code then
    DebugColonial("GameUIColonialMenu.FetchListCallback")
    DebugColonial(msgType)
    DebugColonialTable(content)
    GameUIColonialMenu.playerList_ally = LuaUtils:table_rcopy(content.users_list[1].users)
    DebugColonialTable(GameUIColonialMenu.playerList_ally)
    local flash_obj = GameUIColonialMenu:GetFlashObject()
    if flash_obj then
      GameUIColonialMenu:RefreshHelpList()
    end
    return true
  end
  return false
end
function GameUIColonialMenu.HelpFightCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code then
    if content.api == NetAPIList.colony_challenge_req.Code then
      if content.code ~= 0 then
        GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      end
      return true
    end
  elseif msgType == NetAPIList.colony_challenge_ack.Code then
    DebugColonial("GameUIColonial.HelpFightCallback")
    DebugColonial(msgType)
    DebugColonialTable(content)
    do
      local window_type = ""
      GameUIBattleResult:SetFightReport(content.report, nil, nil)
      if content.result == 1 then
        GameStateColonization.directlyTo.locate = GameStateColonization.directlyTo.type.colonial
        local oriMsg = GameLoader:GetGameText("LC_MENU_EVENT_COLONIAL_SLAVE_HELPED_SUCESS_MESSAGE")
        local msg = GameStateColonization:MakeMsgs(oriMsg, GameUIColonialMenu.curMaster.UserInfo.name, "", GameUIColonialMenu.curMaster.AllyUserInfo.name, "", false)
        local userinfo = GameGlobalData:GetUserInfo()
        local nameText = GameUtils:GetUserDisplayName(userinfo.name)
        local level_info = GameGlobalData:GetData("levelinfo")
        local lv = GameLoader:GetGameText("LC_MENU_Level") .. level_info.level
        GameUIBattleResult:updateColonialWinInfo("0", nameText, lv, msg)
        window_type = "colonial_win"
      else
        GameStateColonization.directlyTo.locate = GameStateColonization.directlyTo.type.colonial
        window_type = "colonial_lose"
      end
      local lastGameState = GameStateManager:GetCurrentGameState()
      if #content.report.rounds == 0 and GameStateManager:GetCurrentGameState().fightRoundData then
        content.report.rounds = GameStateManager:GetCurrentGameState().fightRoundData
      end
      GameStateBattlePlay.curBattleType = "UIColonialMenu"
      GameStateBattlePlay:InitBattle(GameStateBattlePlay.MODE_HISTORY, content.report)
      GameStateBattlePlay:RegisterOverCallback(function()
        GameStateManager:SetCurrentGameState(lastGameState)
        GameUIBattleResult:LoadFlashObject()
        GameUIBattleResult:AnimationMoveIn(window_type, false)
        lastGameState:AddObject(GameUIBattleResult)
      end)
      GameStateManager:SetCurrentGameState(GameStateBattlePlay)
      return true
    end
  end
  return false
end
function GameUIColonialMenu:SetColonistLayerInfo(userInfo, fetch_time, leftstimes)
  self.lastTime = userInfo.forward_left
  self.fetchTime = fetch_time
  local nexttime = "0"
  local _lefttime = self.lastTimeForTricky - (os.time() - self.fetchTimeForTricky)
  if _lefttime < 0 then
    _lefttime = 0
  end
  nexttime = GameUtils:formatTimeString(_lefttime)
  local lefttimes = "10/10"
  GameUIColonialMenu.curTricky.UserInfo = LuaUtils:table_copy(userInfo)
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    local avatar, name, status, lv, force, alliance, icon = GameStateColonization:GetUserInfo(GameUIColonialMenu.curTricky.UserInfo)
    flash_obj:InvokeASCallback("_root", "SetColonistLayerInfo", avatar, name, status, lv, force, alliance, nexttime, lefttimes)
  end
end
function GameUIColonialMenu:SetColonistCDTime()
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    local nexttime = "0"
    local _lefttime = self.lastTimeForTricky - (os.time() - self.fetchTimeForTricky)
    if _lefttime < 0 then
      _lefttime = 0
    end
    local visible = false
    if _lefttime > 0 then
      visible = true
    end
    nexttime = GameUtils:formatTimeString(_lefttime)
    local title = GameLoader:GetGameText("LC_MENU_EVENT_COLONIAL_NEXT_PROTEST_CHAR")
    flash_obj:InvokeASCallback("_root", "setColonistCDTime", title, nexttime, visible)
    local enabled = 0 < GameStateColonization:GetActiveTimes(GameStateColonization.TimesType.action_tricky) and _lefttime <= 0
    if GameUIColonialMenu.colonistBtnEnaled ~= enabled then
      flash_obj:InvokeASCallback("_root", "SetColonistActionBtnState", enabled)
      GameUIColonialMenu.colonistBtnEnaled = enabled
    end
  end
end
GameUIColonialMenu.trickyType = {}
GameUIColonialMenu.trickyType.min = 101
GameUIColonialMenu.trickyType.split = 106
GameUIColonialMenu.trickyType.max = 110
GameUIColonialMenu.trickyType.list = {}
function GameUIColonialMenu:SetRandomColonistAction()
  math.randomseed(tostring(os.time()):reverse():sub(1, 6))
  GameUIColonialMenu.trickyType.list[1] = math.random(GameUIColonialMenu.trickyType.min, GameUIColonialMenu.trickyType.split)
  GameUIColonialMenu.trickyType.list[2] = math.random(GameUIColonialMenu.trickyType.split + 1, GameUIColonialMenu.trickyType.max)
  GameUIColonialMenu.trickyType.list[3] = math.random(GameUIColonialMenu.trickyType.min, GameUIColonialMenu.trickyType.max)
  while GameUIColonialMenu.trickyType.list[3] == GameUIColonialMenu.trickyType.list[2] or GameUIColonialMenu.trickyType.list[3] == GameUIColonialMenu.trickyType.list[1] do
    GameUIColonialMenu.trickyType.list[3] = math.random(GameUIColonialMenu.trickyType.min, GameUIColonialMenu.trickyType.max)
  end
  DebugColonial("SetRandomColonistAction")
  DebugColonialTable(GameUIColonialMenu.trickyType.list)
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    local s1 = GameLoader:GetGameText("LC_MENU_EVENT_COLONIAL_PROTEST_" .. GameUIColonialMenu.trickyType.list[1] - 100 .. "_BUTTON")
    local s2 = GameLoader:GetGameText("LC_MENU_EVENT_COLONIAL_PROTEST_" .. GameUIColonialMenu.trickyType.list[2] - 100 .. "_BUTTON")
    local s3 = GameLoader:GetGameText("LC_MENU_EVENT_COLONIAL_PROTEST_" .. GameUIColonialMenu.trickyType.list[3] - 100 .. "_BUTTON")
    flash_obj:InvokeASCallback("_root", "SetActionStr", s1, s2, s3)
  end
end
function GameUIColonialMenu:ShowColonistLayer()
  if not GameStateManager:GetCurrentGameState():IsObjectInState(GameUIColonialMenu) then
    GameStateColonization:AddObject(self)
  end
  local shareStoryEnabled = false
  if FacebookGraphStorySharer:IsStoryEnabled(FacebookGraphStorySharer.StoryType.STORY_TYPE_COLONIAL_SLAVER_TRICK) then
    shareStoryEnabled = true
  end
  self:SetRandomColonistAction()
  self:MoveIn(GameUIColonialMenu.LayerType.colonist, shareStoryEnabled, self.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_COLONIAL_SLAVER_TRICK])
end
function GameUIColonialMenu:ChangeUserInfo()
  if GameUIColonialMenu.curLayerType == GameUIColonialMenu.LayerType.slave then
    for i, v in ipairs(GameUIColonial.curColonyInfo.info.users) do
      if v.player_id == GameUIColonialMenu.curPlayful.UserInfo.player_id then
        self:SetSlaveLayerInfo(v, GameUIColonial.fetchUserInfoTime)
        break
      end
    end
  end
end
function GameUIColonialMenu.TrickyCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code then
    if content.api == NetAPIList.colony_fawn_req.Code then
      if content.code ~= 0 then
        GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      end
      return true
    end
  elseif msgType == NetAPIList.colony_fawn_ack.Code then
    DebugColonial("TrickyCallback")
    DebugColonialTable(content)
    local GameTip = LuaObjectManager:GetLuaObject("GameTip")
    if 0 < content.experience then
      local oriMsg = GameLoader:GetGameText("LC_MENU_EVENT_COLONIAL_PROTEST_" .. GameUIColonialMenu.curTricky.Type - 100 .. "_MESSAGE")
      local levelInfo = GameGlobalData:GetData("levelinfo")
      if levelInfo.level == GameGlobalData.max_level then
        oriMsg = GameLoader:GetGameText("LC_MENU_EVENT_COLONIAL_PROTEST_" .. GameUIColonialMenu.curTricky.Type - 100 .. "_MESSAGE_MONEY")
      end
      oriMsg = string.format(oriMsg, content.experience)
      DebugOut("TrickyCallback oriMsg oriMsg = ", oriMsg)
      local msg = GameStateColonization:MakeMsgs(oriMsg, GameUIColonialMenu.curTricky.UserInfo.name, "", "", "", false)
      GameTip:Show(msg)
    else
      local oriMsg = GameLoader:GetGameText("LC_MENU_EVENT_COLONIAL_MASTER_EXP_MAX_ALERT")
      GameTip:Show(oriMsg)
    end
    local countryStr = ext.GetDeviceCountry and ext.GetDeviceCountry() or "unknow"
    local extraInfo = {}
    extraInfo.playerName = GameUIColonialMenu.curTricky.UserInfo.name or ""
    if GameUIColonialMenu.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_COLONIAL_SLAVER_TRICK] then
      FacebookGraphStorySharer:ShareStory(FacebookGraphStorySharer.StoryType.STORY_TYPE_COLONIAL_SLAVER_TRICK, extraInfo)
    elseif not FacebookGraphStorySharer:IsStoryEnabled(FacebookGraphStorySharer.StoryType.STORY_TYPE_COLONIAL_SLAVER_TRICK) or FacebookGraphStorySharer:IsStoryEnabled(FacebookGraphStorySharer.StoryType.STORY_TYPE_COLONIAL_SLAVER_TRICK) then
    end
    GameUIColonialMenu:SetRandomColonistAction()
    return true
  end
  return false
end
function GameUIColonialMenu.DelSlaveCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code then
    if content.api == NetAPIList.colony_release_req.Code then
      if content.code ~= 0 then
        GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      end
      return true
    end
  elseif msgType == NetAPIList.colony_release_ack.Code then
    DebugColonial("DelSlaveCallback")
    DebugColonialTable(content)
    DebugColonialTable(GameUIColonialMenu.curPlayful.UserInfo)
    local GameTip = LuaObjectManager:GetLuaObject("GameTip")
    local oriMsg = GameLoader:GetGameText("LC_MENU_EVENT_COLONIAL_ABANDON_ALERT")
    oriMsg = GameStateColonization:MakeMsgs(oriMsg, "", GameUIColonialMenu.curPlayful.UserInfo.name, "", "", false)
    GameTip:Show(oriMsg)
    GameUIColonialMenu:OnFSCommand("auto_close_menu")
    return true
  end
  return false
end
function GameUIColonialMenu:SendFreeColonyExploit()
  local requestParam = {
    slave_id = GameUIColonialMenu.curPlayful.UserInfo.player_id,
    pay_enabled = false
  }
  NetMessageMgr:SendMsg(NetAPIList.colony_exploit_req.Code, requestParam, GameUIColonialMenu.ExploitSlaveCallback, true, nil)
end
function GameUIColonialMenu:SendPayColonyExploit()
  local function callback()
    local requestParam = {
      slave_id = GameUIColonialMenu.curPlayful.UserInfo.player_id,
      pay_enabled = true
    }
    NetMessageMgr:SendMsg(NetAPIList.colony_exploit_req.Code, requestParam, GameUIColonialMenu.ExploitSlaveCallback, true, nil)
  end
  local text_content = GameLoader:GetGameText("LC_MENU_EVENT_COLONIAL_FINISH_PRO_ALERT")
  text_content = string.format(text_content, 300)
  GameUtils:CreditCostConfirm(text_content, callback)
end
function GameUIColonialMenu.ExploitSlaveCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code then
    if content.api == NetAPIList.colony_exploit_req.Code then
      DebugColonial("ExploitSlaveCallback")
      DebugColonialTable(content)
      if content.code ~= 0 then
        GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      end
      return true
    end
  elseif msgType == NetAPIList.colony_exploit_ack.Code then
    DebugColonial("ExploitSlaveCallback")
    DebugColonialTable(content)
    GameUIColonial:ResetFreshEXP()
    local GameTip = LuaObjectManager:GetLuaObject("GameTip")
    if 0 < content.experience then
      local text_content = GameLoader:GetGameText("LC_MENU_EVENT_COLONIAL_REAP_CHAR")
      local levelInfo = GameGlobalData:GetData("levelinfo")
      if levelInfo.level == GameGlobalData.max_level then
        text_content = GameLoader:GetGameText("LC_MENU_EVENT_COLONIAL_REAP_CHAR_MONEY")
      end
      text_content = string.format(text_content, content.experience)
      GameTip:Show(text_content)
    end
    return true
  end
  return false
end
function GameUIColonialMenu.OnTrickyCDTimeChange()
  local cd_time = GameGlobalData:GetTrickyCDTime()
  DebugColonial("OnTrickyCDTimeChange")
  DebugColonialTable(cd_time)
  if not cd_time then
    GameUIColonialMenu.fetchTimeForTricky = 0
    GameUIColonialMenu.lastTimeForTricky = 0
    return
  end
  GameUIColonialMenu.fetchTimeForTricky = os.time()
  GameUIColonialMenu.lastTimeForTricky = cd_time.left_time
end
