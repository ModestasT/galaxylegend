local GameUIMiningWorld = LuaObjectManager:GetLuaObject("GameUIMiningWorld")
local GameStateMiningWorld = GameStateManager.GameStateMiningWorld
function GameStateMiningWorld:InitGameState()
end
function GameStateMiningWorld:OnFocusGain(previousState)
  DebugOut("GameStateMiningWorld:OnFocusGain")
  self.m_preState = previousState
  GameUtils:PlayMusic("GE2_Ore_Map.mp3")
end
function GameStateMiningWorld:OnFocusLost(newState)
  self.m_preState = nil
  self:EraseObject(GameUIMiningWorld)
end
function GameStateMiningWorld:GetPreState()
  return self.m_preState
end
