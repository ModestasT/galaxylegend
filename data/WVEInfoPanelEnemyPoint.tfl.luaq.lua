WVEInfoPanelEnemyPoint = luaClass(nil)
local WVEGameManeger = LuaObjectManager:GetLuaObject("WVEGameManeger")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
function WVEInfoPanelEnemyPoint:ctor(wveGameManager, pointID)
  self.mGameManger = wveGameManager
  self.mPointID = pointID
  self.mCurrentPrice = nil
  self.mCurrentDistance = nil
end
function WVEInfoPanelEnemyPoint:Show(pointInfo)
  DebugOut("show!!!!!!!!!")
  local flash_obj = self.mGameManger:GetFlashObject()
  if not flash_obj then
    return
  end
  self.mCurrentPrice = nil
  self.mCurrentDistance = nil
  self.mIsShow = true
  self.mLeftTime = pointInfo.leftTime
  self.mOpenTime = os.time()
  self.mLastFrameTime = -1
  local jumpInfo = {}
  jumpInfo.freeTime = WVEGameManeger.leftFreeJumpTime
  jumpInfo.totalfreeTime = WVEGameManeger.totalFreeJumpTime
  jumpInfo.jumpPirce = self:GetJumpPrice(self.mPointID)
  jumpInfo.freeText = GameLoader:GetGameText("LC_MENU_CAPTION_FREE")
  DebugOut("preapard to show")
  DebugTable(jumpInfo)
  flash_obj:InvokeASCallback("_root", "EnemyPointMC_Show", pointInfo, jumpInfo)
  DebugOut("preapard to show end")
end
function WVEInfoPanelEnemyPoint:Hide()
  local flash_obj = self.mGameManger:GetFlashObject()
  if not flash_obj then
    return
  end
  self.mIsShow = false
  self.mLeftTime = nil
  flash_obj:InvokeASCallback("_root", "EnemyPointMC_Hide")
end
function WVEInfoPanelEnemyPoint:SetPointInfo()
  local flash_obj = self.mGameManger:GetFlashObject()
  if not flash_obj then
    return
  end
end
function WVEInfoPanelEnemyPoint:SetAsMarchTargetPoint()
end
function WVEInfoPanelEnemyPoint:OnFSCommand(cmd, arg)
  DebugOut("WVEInfoPanelEnemyPoint cmd process")
  if cmd == "EnemyPointMC_movetoPressed" then
    self.mGameManger:RequestMarch(self.mPointID)
    return true
  elseif cmd == "EnemyPointMC_jumptoPressed" then
    DebugOut("gogogo")
    self:AskForJumpTo(self.mPointID)
    return true
  elseif cmd == "EnemyPointMC_closePressed" then
    self:Hide()
    return true
  end
  return false
end
function WVEInfoPanelEnemyPoint:GetJumpPrice(PointID)
  if self.mCurrentPrice then
    return self.mCurrentPrice
  end
  local playerMyself = self.mGameManger.mWVEPlayerManager:GetPlayerMyself()
  local marchID = playerMyself:GetMarchID()
  local startPointID = playerMyself:GetPointID()
  local endPointID
  if marchID then
    DebugOut("here")
    local marchInfo = playerMyself:GetMyselfMarchInfo()
    if marchInfo then
      startPointID = marchInfo:GetStartPoint()
      endPointID = marchInfo:GetEndPoint()
    else
      marchID = nil
    end
  end
  DebugOut("startPointID = ", startPointID)
  DebugOut("PointID = ", PointID)
  local endToTargetList, startToTargetList, endToTargetLength, startToTargetLength, totalLength
  startToTargetList = self.mGameManger.mWVEMap:CalculaeMarchPath(startPointID, PointID)
  if marchID then
    endToTargetList = self.mGameManger.mWVEMap:CalculaeMarchPath(endPointID, PointID)
    table.insert(endToTargetList, 1, endPointID)
    table.insert(startToTargetList, 1, startPointID)
    DebugOut("marchID is not nil:")
    DebugTable(endToTargetList)
    DebugTable(startToTargetList)
    endToTargetLength = self.mGameManger.mWVEMap:CalculateTotalLengthOfPoints(endToTargetList)
    startToTargetLength = self.mGameManger.mWVEMap:CalculateTotalLengthOfPoints(startToTargetList)
    if endToTargetLength > startToTargetLength then
      totalLength = startToTargetLength
    else
      totalLength = endToTargetLength
    end
    DebugOut(endToTargetLength, ",", startToTargetLength, ", totalLength")
  else
    if startToTargetList and startPointID then
      table.insert(startToTargetList, 1, startPointID)
    end
    totalLength = self.mGameManger.mWVEMap:CalculateTotalLengthOfPoints(startToTargetList)
  end
  DebugOut("fjjjhhh")
  DebugOut("fjjjhhh 333333")
  DebugOut("WVEGameManeger.jump_in_credit = ", WVEGameManeger.jump_in_credit)
  local totalPrice = totalLength * WVEGameManeger.jump_in_credit
  if marchID then
    totalPrice = totalPrice + 50
  end
  DebugOut("fjjjhhh 222222222")
  totalPrice = math.min(WVEGameManeger.jump_in_credit_max, totalPrice)
  totalPrice = math.max(WVEGameManeger.jump_in_credit_min, totalPrice)
  self.mCurrentPrice = math.ceil(totalPrice)
  self.mCurrentDistance = totalLength
  return self.mCurrentPrice
end
function WVEInfoPanelEnemyPoint:GetCurDistance()
  return self.mCurrentDistance
end
function WVEInfoPanelEnemyPoint:AskForJumpTo(PointID)
  local curPrice = self:GetJumpPrice(PointID)
  local jumpInfo = {}
  jumpInfo.targetPoint = PointID
  jumpInfo.pointList = {}
  jumpInfo.totalDistance = self:GetCurDistance()
  DebugOut("jumpInfo = ")
  DebugTable(jumpInfo)
  DebugOut("WVEGameManeger.leftFreeJumpTime = ", WVEGameManeger.leftFreeJumpTime)
  if WVEGameManeger.leftFreeJumpTime and WVEGameManeger.leftFreeJumpTime > 0 then
    DebugOut("jumpInfo 111= ")
    self.RequestJumpTo(PointID, true)
  else
    DebugOut("jumpInfo 22222111= ")
    local titleText = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
    local contentText = string.format(GameLoader:GetGameText("LC_MENU_WVE_JUMP_CREDIT"), curPrice)
    local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
    GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
    GameUIMessageDialog:SetYesButton(self.RequestJumpTo, PointID)
    GameUIMessageDialog:Display(titleText, contentText)
  end
end
function WVEInfoPanelEnemyPoint.RequestJumpTo(PointID, isFree)
  local userResource = GameGlobalData:GetData("resource")
  if not isFree and userResource and userResource.credit < WVEGameManeger.jump_in_credit then
    GameTip:Show(GameLoader:GetGameText("LC_MENU_NOT_ENOUGH_CREDIT"))
  else
    WVEGameManeger:RequestJump(PointID)
  end
end
function WVEInfoPanelEnemyPoint:OnUpdate(dt)
  if self.mIsShow then
    local tmpLeftTime = 0
    if self.mLeftTime and 0 < self.mLeftTime then
      tmpLeftTime = self.mLeftTime - (os.time() - self.mOpenTime)
      if tmpLeftTime < 0 then
        tmpLeftTime = 0
      end
    else
      tmpLeftTime = 0
    end
    if self.mLastFrameTime == tmpLeftTime then
      return
    end
    self.mLastFrameTime = tmpLeftTime
    local timeStr = GameUtils:formatTimeString(tmpLeftTime)
    local flashObj = self.mGameManger:GetFlashObject()
    if flashObj then
      flashObj:InvokeASCallback("_root", "EnemyPointMC_SetPointLeftTime", timeStr)
    end
  end
end
