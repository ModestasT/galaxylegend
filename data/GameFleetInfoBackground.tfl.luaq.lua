local GameFleetInfoBackground = LuaObjectManager:GetLuaObject("GameFleetInfoBackground")
local GameFleetInfoUI = LuaObjectManager:GetLuaObject("GameFleetInfoUI")
local GameFleetEnhance = LuaObjectManager:GetLuaObject("GameFleetEnhance")
local GameUIKrypton = LuaObjectManager:GetLuaObject("GameUIKrypton")
local GameStateManager = GameStateManager
local QuestTutorialsChangeFleets = TutorialQuestManager.QuestTutorialsChangeFleets
local QuestTutorialsChangeFleets1 = TutorialQuestManager.QuestTutorialsChangeFleets1
local QuestTutorialEnhance_third = TutorialQuestManager.QuestTutorialEnhance_third
local GameFleetInfoBag = LuaObjectManager:GetLuaObject("GameFleetInfoBag")
function GameFleetInfoBackground:OnInitGame()
  GameGlobalData:RegisterDataChangeCallback("fleetinfo", GameFleetInfoBackground.OnChangeFleetInfo)
  GameFleetInfoBackground.currentIndex = -1
end
function GameFleetInfoBackground.OnChangeFleetInfo()
  DebugOut("GameFleetInfoBackground.OnChangeFleetInfo")
  if GameStateManager:GetCurrentGameState():IsObjectInState(GameFleetInfoBackground) then
    GameFleetInfoBackground:RefreshCurrentFleet()
  end
end
function GameFleetInfoBackground:CheckTutorial()
  if immanentversion == 2 then
    if QuestTutorialEnhance_third:IsActive() then
      local fleets = GameGlobalData:GetData("fleetinfo").fleets
      local isHaveLiAng = false
      for k, v in pairs(fleets) do
        if v.identity == 2 then
          isHaveLiAng = true
        end
      end
      if not isHaveLiAng then
        QuestTutorialEnhance_third:SetFinish(true)
      end
    end
    if QuestTutorialsChangeFleets1:IsActive() and GameStateManager:GetCurrentGameState() == GameStateManager.GameStateFleetInfo or QuestTutorialEnhance_third:IsActive() then
      self:GetFlashObject():InvokeASCallback("_root", "ShowTutorial", true)
    else
      self:GetFlashObject():InvokeASCallback("_root", "ShowTutorial", false)
    end
  elseif immanentversion == 1 then
    if not QuestTutorialsChangeFleets:IsActive() and not QuestTutorialsChangeFleets:IsFinished() then
      local pve_progress = GameGlobalData:GetData("progress")
      if pve_progress.act == 1 and pve_progress.chapter == 1 then
        local fleets = GameGlobalData:GetData("fleetinfo").fleets
        if #fleets > 1 then
          QuestTutorialsChangeFleets:SetActive(true)
        end
      end
    end
    if QuestTutorialsChangeFleets:IsActive() then
      self:GetFlashObject():InvokeASCallback("_root", "ShowTutorial", true)
    else
      self:GetFlashObject():InvokeASCallback("_root", "ShowTutorial", false)
    end
  end
end
function GameFleetInfoBackground:RefreshCurrentFleet()
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  if immanentversion == 1 and QuestTutorialsChangeFleets:IsActive() then
    QuestTutorialsChangeFleets:SetFinish(true)
    GameFleetInfoBag:CheckTutorial()
    self:GetFlashObject():InvokeASCallback("_root", "ShowTutorial", false)
  end
  if fleets ~= nil then
    local maxFleetId = LuaUtils:table_size(fleets)
    GameFleetInfoBackground:SetMaxFleetId(maxFleetId, self.currentIndex)
  end
  local leaderlist = GameGlobalData:GetData("leaderlist")
  local curMatrixIndex = GameGlobalData.GlobalData.curMatrixIndex
  local frame = GameDataAccessHelper:GetCommanderVesselsImage(fleets[self.currentIndex].identity, fleets[self.currentIndex].level)
  if fleets[self.currentIndex].identity == 1 and leaderlist and curMatrixIndex then
    frame = GameDataAccessHelper:GetCommanderVesselsImage(leaderlist.leader_ids[curMatrixIndex], fleets[self.currentIndex].level)
  end
  local name = ""
  local kryptonPoint = fleets[self.currentIndex].krypton
  DebugOut("GameFleetInfoBackground:RefreshCurrentFleet: ", frame, name, kryptonPoint)
  DebugTable(fleets)
  if immanentversion == 2 and fleets[self.currentIndex].identity == 2 and QuestTutorialEnhance_third:IsActive() then
    self:GetFlashObject():InvokeASCallback("_root", "ShowTutorial", false)
    QuestTutorialEnhance_third:SetFinish(true)
    GameFleetInfoBackground.finishEnhance_third = true
  end
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "setMC1", frame, name, kryptonPoint)
  end
end
function GameFleetInfoBackground:Init()
  self.initFleetIndex = self.initFleetIndex or -1
  self.currentIndex = math.max(self.initFleetIndex, 1)
  self:RefreshCurrentFleet()
  local QuestTutorialEquip = TutorialQuestManager.QuestTutorialEquip
  if QuestTutorialEquip:IsActive() then
    self:GetFlashObject():SetBtnEnable("_root.Fleet.hitzone", false)
  else
    self:GetFlashObject():SetBtnEnable("_root.Fleet.hitzone", true)
  end
end
function GameFleetInfoBackground:SetInitFleetIndex(currentIndex)
  self.initFleetIndex = currentIndex
end
function GameFleetInfoBackground:Clear()
  self.initFleetIndex = -1
  self.currentIndex = -1
end
function GameFleetInfoBackground:OnAddToGameState()
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  GameFleetInfoBackground:SetRootVisible(true)
  self:CheckTutorial()
  self:Init()
end
function GameFleetInfoBackground:OnEraseGameState()
  self:Clear()
  self:UnloadFlashObject()
  GameFleetInfoBackground.finishEnhance_third = nil
end
function GameFleetInfoBackground:OnFSCommand(cmd, arg)
  if cmd == "Change_Fleet_By_FleetId" then
    self.currentIndex = tonumber(arg)
    if GameStateManager:GetCurrentGameState():IsObjectInState(GameFleetInfoUI) then
      GameFleetInfoBackground:SetFleetChanegeTutorial()
      GameFleetInfoUI:UpdateSelectedFleetInfo(self.currentIndex)
    elseif GameStateManager:GetCurrentGameState():IsObjectInState(GameFleetEnhance) then
      GameFleetEnhance:UpdateSelectedFleetInfo(self.currentIndex)
    elseif GameStateManager:GetCurrentGameState():IsObjectInState(GameUIKrypton) then
      GameUIKrypton:UpdateSelectedFleetInfo(self.currentIndex)
    end
    self:RefreshCurrentFleet()
  end
  if cmd == "InertiaBackOver" then
    if GameStateManager:GetCurrentGameState():IsObjectInState(GameFleetInfoUI) then
      GameFleetInfoUI:ShowEquipMent()
    elseif GameStateManager:GetCurrentGameState():IsObjectInState(GameFleetEnhance) then
      GameFleetEnhance:ShowEquipMent()
    elseif GameStateManager:GetCurrentGameState():IsObjectInState(GameUIKrypton) then
      GameUIKrypton:ShowEquipMent()
    end
    self:FleetAppear()
  end
  if cmd == "setTextureOfMC2" then
    local currentMC2Index = tonumber(arg)
    if GameStateManager:GetCurrentGameState():IsObjectInState(GameFleetInfoUI) then
      GameFleetInfoUI:HideEquipMent()
    elseif GameStateManager:GetCurrentGameState():IsObjectInState(GameFleetEnhance) then
      GameFleetEnhance:HideEquipMent()
    elseif GameStateManager:GetCurrentGameState():IsObjectInState(GameUIKrypton) then
      GameUIKrypton:HideEquipMent()
    end
    local fleets = GameGlobalData:GetData("fleetinfo").fleets
    local frame = GameDataAccessHelper:GetCommanderVesselsImage(fleets[currentMC2Index].identity, fleets[currentMC2Index].level)
    self:GetFlashObject():InvokeASCallback("_root", "setMC2", frame)
  end
  if cmd == "clicked_pre_next" then
    if GameFleetInfoUI:GetFlashObject() then
      GameFleetInfoUI:GetFlashObject():InvokeASCallback("_root", "ShowFleetEquipment")
    end
    GameFleetInfoBackground:SetFleetChanegeTutorial()
  end
end
function GameFleetInfoBackground:SetFleetChanegeTutorial()
  if immanentversion == 2 and QuestTutorialsChangeFleets1:IsActive() and GameStateManager:GetCurrentGameState() == GameStateManager.GameStateFleetInfo then
    self:GetFlashObject():InvokeASCallback("_root", "ShowTutorial", false)
    AddFlurryEvent("FinishEventID_1001103", {}, 2)
    QuestTutorialsChangeFleets1:SetFinish(true)
    GameStateManager.GameStateBattleMap:SetStoryWhenFocusGain({51126})
    GameFleetInfoBag:CheckTutorial()
  end
end
function GameFleetInfoBackground:onPressedFleetRect()
  self:GetFlashObject():InvokeASCallback("_root", "onPressedFleetRect")
end
function GameFleetInfoBackground:onReleasedFleetRect()
  self:GetFlashObject():InvokeASCallback("_root", "onReleasedFleetRect")
end
function GameFleetInfoBackground:SetMousePos(posx, posy)
  self:GetFlashObject():InvokeASCallback("_root", "SetMousePos", posx, posy)
end
function GameFleetInfoBackground:Update(dt)
  self:GetFlashObject():InvokeASCallback("_root", "OnUpdate")
  self:GetFlashObject():Update(dt)
end
function GameFleetInfoBackground:FleetAppear()
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  self:GetFlashObject():InvokeASCallback("_root", "FleetAppear")
end
function GameFleetInfoBackground:FleetDisAppear()
  self:GetFlashObject():InvokeASCallback("_root", "FleetDisAppear")
end
function GameFleetInfoBackground:SetMaxFleetId(maxfleetId, curfleetId)
  curfleetId = math.max(curfleetId, 1)
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "setMaxFleetId", maxfleetId, curfleetId)
  end
end
function GameFleetInfoBackground:onBeginDragItem(itemId)
end
function GameFleetInfoBackground:onEndDragItem()
end
function GameFleetInfoBackground:SetRootVisible(isVisible)
  local flashObj = GameFleetInfoBackground:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "SetRootVisible", isVisible)
  end
end
