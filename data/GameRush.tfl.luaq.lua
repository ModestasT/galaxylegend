local GameRush = LuaObjectManager:GetLuaObject("GameRush")
local GameObjectAdventure = LuaObjectManager:GetLuaObject("GameObjectAdventure")
local GameUtils = GameUtils
local NetMessageMgr = NetMessageMgr
local NetAPIList = NetAPIList
local GameUIEnemyInfo = LuaObjectManager:GetLuaObject("GameUIEnemyInfo")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameUISection = LuaObjectManager:GetLuaObject("GameUISection")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameObjectLevelUp = LuaObjectManager:GetLuaObject("GameObjectLevelUp")
local GameStateConfrontation = GameStateManager.GameStateConfrontation
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local GameQuestMenu = LuaObjectManager:GetLuaObject("GameQuestMenu")
local QuestTutorialBattleRush = TutorialQuestManager.QuestTutorialBattleRush
local FacebookPopUI = LuaObjectManager:GetLuaObject("FacebookPopUI")
local isStopRushByAccident = false
local isCheckBoxTick = true
local isCheckBoxTickForBoss = true
GameRush.isElite = false
GameRush.maxEliteCount = 0
GameRush.eliteCount = 0
GameRush.eliteCosme = 1
function GameRush:OnInitGame()
  GameGlobalData:RegisterDataChangeCallback("battle_supply", GameRush.RefreshBattleSupply)
  GameGlobalData:RegisterDataChangeCallback("user_quest", GameRush.OnUserQuestStateChange)
  GameGlobalData:RegisterDataChangeCallback("levelupNotify", GameRush.levelupNotifyHandler)
end
function GameRush:InitText()
  GameRush.RefreshBattleSupply()
end
function GameRush.RefreshBattleSupply()
  if not GameRush:GetFlashObject() then
    return
  end
  local battle_supply = GameGlobalData:GetData("battle_supply")
  if battle_supply then
    if GameRush.isElite then
      local p1 = math.floor(battle_supply.current / GameRush.eliteCosme)
      DebugOut("ajsdhjaj:", p1, GameRush.eliteCount)
      GameRush.maxEliteCount = p1 < GameRush.eliteCount and p1 or GameRush.eliteCount
    end
    GameRush:GetFlashObject():InvokeASCallback("_root", "RefreshBattleSupply", GameUtils.numberConversion(battle_supply.current) .. "/" .. GameUtils.numberConversion(battle_supply.max))
    if GameRush.isElite then
      GameRush:GetFlashObject():InvokeASCallback("_root", "SetRoundRange", 1, GameRush.maxEliteCount)
      if GameRush.maxEliteCount <= 1 then
        GameRush:GetFlashObject():InvokeASCallback("_root", "setBtnminus10State", 0)
      end
    else
      GameRush:GetFlashObject():InvokeASCallback("_root", "SetRoundRange", 1, battle_supply.current)
    end
  end
  if GameRush.rushRunning == false then
    GameRush:SetButton10State()
  end
end
function GameRush.OnUserQuestStateChange()
  if immanentversion == 2 then
    local quest = GameGlobalData:GetData("user_quest")
    if quest.cond_int == 1002008 and quest.cond_count > 1 and quest.status == GameQuestMenu.QUEST_STS_UNLOOT then
      QuestTutorialBattleRush:SetFinish(true)
      AddFlurryEvent("RushEnemy_1002008", {}, 2)
      if GameStateManager:GetCurrentGameState():IsObjectInState(GameRush) then
        GameRush:GetFlashObject():InvokeASCallback("_root", "HideRushMenu")
      end
    end
  end
end
function GameRush:Init()
  local levelInfo = GameGlobalData:GetData("levelinfo")
  GameRush.currentBosses = {}
  GameRush.firstBossIndex = 0
  GameRush.lastBossIndex = 0
  GameRush.rushBossTickInterval = 1000
  GameRush.passedRushBossTickTime = 0
  GameRush.startRushBossTick = false
  GameRush.rushBossNextTickEnabled = false
  GameRush.rushRunning = false
  GameRush:ShowRushMenu()
  GameRush:InitText()
  GameRush:GetFlashObject():InvokeASCallback("_root", "InitBossList")
  GameRush.allRewardItems = {}
  GameRush:GetFlashObject():InvokeASCallback("_root", "InitRewardList", 1)
  GameRush.itemPos = 0
  isStopRushByAccident = false
  local percent = math.floor(100 * levelInfo.experience / levelInfo.uplevel_experience)
  if GameUtils:isPlayerMaxLevel(levelInfo.level) then
    GameRush:GetFlashObject():InvokeASCallback("_root", "setLevelAndExp", GameLoader:GetGameText("LC_MENU_Level") .. levelInfo.level, "-", "-", 100, 0)
  else
    GameRush:GetFlashObject():InvokeASCallback("_root", "setLevelAndExp", GameLoader:GetGameText("LC_MENU_Level") .. levelInfo.level, levelInfo.experience, levelInfo.uplevel_experience, percent, 0)
  end
  GameRush:SetButton10State()
  if immanentversion == 2 then
    DebugOut("QuestTutorialBattleRush:IsActive() ---> ,QuestTutorialBattleRush:IsActive()", QuestTutorialBattleRush:IsActive())
    if QuestTutorialBattleRush:IsActive() then
      AddFlurryEvent("ChooseRush", {}, 2)
      self:GetFlashObject():InvokeASCallback("_root", "setBtnAnimVisible", true)
    else
      self:GetFlashObject():InvokeASCallback("_root", "setBtnAnimVisible", false)
    end
  end
end
function GameRush:Clear()
  GameRush.roundCount = -1
  isStopRushByAccident = false
  GameRush:GetFlashObject():InvokeASCallback("_root", "ClearBossList")
  GameRush:GetFlashObject():InvokeASCallback("_root", "ClearRewardList")
  if self:IsRushBoss() then
    self:EndRushBossTick()
    GameObjectAdventure:InitAdventureData()
  else
    GameRush.rushRunning = false
  end
end
function GameRush:ShowRush(isElite, maxEliteCount, eliteCosme)
  self.isElite = isElite or false
  self.maxEliteCount = maxEliteCount or 0
  self.eliteCount = maxEliteCount or 0
  self.eliteCosme = eliteCosme or 1
  GameStateManager:GetCurrentGameState():AddObject(self)
end
function GameRush:OnAddToGameState()
  self:LoadFlashObject()
  GameRush:Init()
end
function GameRush:OnEraseFromGameState()
  DebugOut("GameRush:OnEraseFromGameState")
  GameRush:Clear()
  self:UnloadFlashObject()
  local GameUIPrestigeRankUp = LuaObjectManager:GetLuaObject("GameUIPrestigeRankUp")
  local quest = GameGlobalData:GetData("user_quest")
  if (quest.status == GameQuestMenu.QUEST_STS_UNLOOT or GameQuestMenu:checkIsShowActivityQuest()) and not GameStateManager:GetCurrentGameState():IsObjectInState(GameQuestMenu) and not GameStateManager.GameStateGlobalState:IsObjectInState(GameQuestMenu) and not GameStateManager.GameStateGlobalState:IsObjectInState(GameUIPrestigeRankUp) and not GameUIPrestigeRankUp.mIsGotoDetail then
    GameQuestMenu:ShowQuestMenu()
  end
end
function GameRush:ShowRushMenu()
  local isRushBoss = self:IsRushBoss()
  if self.isElite then
    isRushBoss = false
  end
  if isRushBoss then
    GameRush:UpdateBossList(true)
  end
  GameRush:GetFlashObject():InvokeASCallback("_root", "ShowRushMenu", isRushBoss, immanentversion, self.isElite)
end
function GameRush:HideRushMenu()
  GameRush:GetFlashObject():InvokeASCallback("_root", "HideRushMenu")
end
function GameRush:Update(dt)
  GameRush:GetFlashObject():Update(dt)
  GameRush:GetFlashObject():InvokeASCallback("_root", "OnUpdateList")
  GameRush:GetFlashObject():InvokeASCallback("_root", "OnUpdateProcessbar")
  if GameRush:IsRushBoss() then
    GameRush:GetFlashObject():InvokeASCallback("_root", "OnUpdateBossList")
  end
  GameRush:UpdateRushBossTick(dt)
end
function GameRush:StartRushBossTick()
  self.passedRushBossTickTime = 0
  self.startRushBossTick = true
  self.rushBossNextTickEnabled = false
end
function GameRush:UpdateRushBossTick(dt)
  if self.startRushBossTick == true then
    self.passedRushBossTickTime = self.passedRushBossTickTime + dt
    if self.passedRushBossTickTime >= self.rushBossTickInterval then
      if self.rushBossNextTickEnabled == true then
        if isStopRushByAccident == true then
          DebugOut("stop rush")
          DebugOut("UpdateRushBossTick isStopRushByAccident", isStopRushByAccident)
          isStopRushByAccident = false
          self.rushBossNextTickEnabled = false
          self.startRushBossTick = false
          self:GetFlashObject():InvokeASCallback("_root", "lua2fs_stopRushBoss")
          return
        end
        GameRush:StartRushBoss()
        self.passedRushBossTickTime = self.passedRushBossTickTime - self.rushBossTickInterval
        self.rushBossNextTickEnabled = false
      else
        self.passedRushBossTickTime = self.rushBossTickInterval
      end
    end
  end
end
function GameRush:EndRushBossTick()
  DebugOut("GameRush:EndRushBossTick()")
  self.passedRushBossTickTime = 0
  self.startRushBossTick = false
  self.rushBossNextTickEnabled = false
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "lua2fs_stopRushBoss")
  end
end
function GameRush:StartRush()
  DebugOut("StartRush")
  local battle_supply = GameGlobalData:GetData("battle_supply")
  if battle_supply.current <= 0 then
    self.rushRunning = false
    GameRush:EndRushBossTick()
    self:GetFlashObject():InvokeASCallback("_root", "lua2fs_stopRush")
    DebugOut("battle_supply.current", battle_supply.current)
    if Facebook:IsFacebookEnabled() then
      if not Facebook:IsBindFacebook() then
        if FacebookPopUI.mFeatureSwitch[FACEBOOKFEATURESWITCH.SWITCH_SUPPLY] and FacebookPopUI.mFeatureSwitch[FACEBOOKFEATURESWITCH.SWITCH_SUPPLY] == 1 then
          FacebookPopUI.mManullyCloseCallback = GameUIEnemyInfo.netCallSupplyInfo
          FacebookPopUI:ShowFacebookMenu(FACEBOOKMENUTYPE.TYPE_MENU_LOGIN)
          FacebookPopUI:SetBindAwardType(FACEBOOKBINDAWARDTYPE.AWARD_PVESUPPLY)
          if not ext.GetDeviceCountry or not ext.GetDeviceCountry() then
            local countryStr = "unknow"
          end
        else
          GameUIEnemyInfo.netCallSupplyInfo()
        end
      else
        GameUIEnemyInfo.netCallSupplyInfo()
      end
    else
      GameUIEnemyInfo.netCallSupplyInfo()
    end
    return false
  elseif self.roundCount and 0 < self.roundCount then
    if immanentversion == 2 and QuestTutorialBattleRush:IsActive() then
      GameUIEnemyInfo.canFinishRushTutorial = true
      self:GetFlashObject():InvokeASCallback("_root", "setBtnAnimVisible", false)
    end
    self:RequestRush()
    return true
  end
end
function GameRush:RequestRush()
  local user_rush_req_param = {
    battle_id = GameUIEnemyInfo.m_combinedBattleID,
    action = 0
  }
  NetMessageMgr:SendMsg(NetAPIList.user_rush_req.Code, user_rush_req_param, GameRush.RushCallback, false, nil)
end
function GameRush.RushCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.user_rush_req.Code then
    GameRush.roundCount = -1
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.rush_result_ack.Code then
    local battle_supply = GameGlobalData:GetData("battle_supply")
    if battle_supply and GameRush.roundCount > 0 then
      GameRush:GetFlashObject():InvokeASCallback("_root", "ShowRoundAnim", GameRush.roundCount)
      GameRush.roundCount = GameRush.roundCount - 1
      if GameRush.isElite then
        GameRush.eliteCount = GameRush.eliteCount - 1
        GameRush.maxEliteCount = GameRush.maxEliteCount - 1
      end
      local displayCountDown = GameRush.roundCount > 99 and 99 or GameRush.roundCount
      GameRush:GetFlashObject():InvokeASCallback("_root", "setRoundCountdown", displayCountDown)
      GameRush:GetFlashObject():InvokeASCallback("_root", "SetRoundCount", GameRush.roundCount)
      GameRush:UpdateReward("", content.rewards, content.strike)
      GameRush:SetButtonMinus10State()
    else
      DebugOut("stop rush")
      if not GameRush:GetFlashObject() then
        return true
      end
      if GameRush.roundCount == 0 then
        GameRush:GetFlashObject():InvokeASCallback("_root", "SetRoundCount", 1)
      end
      GameRush:GetFlashObject():InvokeASCallback("_root", "lua2fs_stopRush")
    end
    return true
  end
  return false
end
function GameRush:StartRushBoss()
  local bossCount = self:GetBossListCnt()
  if bossCount and bossCount > 0 then
    self:RequestRushBoss()
  end
end
function GameRush:RequestRushBoss()
  DebugOut("RequestRushBoss, self.firstBossIndex=", self.firstBossIndex, ", self.lastBossIndex=", self.lastBossIndex, ", battle_id=", self.currentBosses[self.firstBossIndex].battle_id)
  local adventure_rush_req_param = {
    battle_id = self.currentBosses[self.firstBossIndex].battle_id,
    action = 0
  }
  NetMessageMgr:SendMsg(NetAPIList.adventure_rush_req.Code, adventure_rush_req_param, GameRush.RushBossCallback, false, nil)
end
function GameRush.RushBossCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.adventure_rush_req.Code then
    DebugOut("RushBossCallback", msgType)
    DebugTable(content)
    GameRush:EndRushBossTick()
    GameRush.rushRunning = false
    if tonumber(content.code) == 165 then
      if Facebook:IsFacebookEnabled() then
        if not Facebook:IsBindFacebook() then
          if FacebookPopUI.mFeatureSwitch[FACEBOOKFEATURESWITCH.SWITCH_SUPPLY] and FacebookPopUI.mFeatureSwitch[FACEBOOKFEATURESWITCH.SWITCH_SUPPLY] == 1 then
            FacebookPopUI.mManullyCloseCallback = GameUIEnemyInfo.netCallSupplyInfo
            FacebookPopUI:ShowFacebookMenu(FACEBOOKMENUTYPE.TYPE_MENU_LOGIN)
            FacebookPopUI:SetBindAwardType(FACEBOOKBINDAWARDTYPE.AWARD_PVESUPPLY)
            if not ext.GetDeviceCountry or not ext.GetDeviceCountry() then
              local countryStr = "unknow"
            end
          else
            GameUIEnemyInfo.netCallSupplyInfo()
          end
        else
          GameUIEnemyInfo.netCallSupplyInfo()
        end
      else
        GameUIEnemyInfo.netCallSupplyInfo()
      end
    else
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.rush_result_ack.Code then
    if GameRush.startRushBossTick == true then
      GameRush.rushBossNextTickEnabled = true
      local name = GameRush.currentBosses[GameRush.firstBossIndex].name
      GameRush:UpdateReward(name, content.rewards, nil)
      GameRush:UpdateBossList(false)
      local bossCount = GameRush:GetBossListCnt()
      if bossCount == 0 then
        GameRush:EndRushBossTick()
      end
    end
    return true
  end
  return false
end
function GameRush:UpdateReward(name, rewardsList, strike)
  local currentRoundRewards = {}
  local levelInfo = GameGlobalData:GetData("levelinfo")
  local deltaExp = 0
  GameRush.itemPos = GameRush.itemPos + 1
  if rewardsList ~= nil and #rewardsList > 0 then
    local roundText = string.format(GameLoader:GetGameText("LC_MENU_RUSH_ROUND_CHAR"), GameRush.itemPos)
    currentRoundRewards.name = name ~= "" and name or roundText
    currentRoundRewards.icon = ""
    currentRoundRewards.text = ""
    currentRoundRewards.strike = strike
    for i, v in ipairs(rewardsList) do
      local itemName, icon = GameHelper:GetAwardTypeTextAndIcon(v.item_type, v.number)
      local text = v.number
      if v.item_type == "equip" then
        text = itemName
      elseif v.item_type == "item" then
        text = v.no
      elseif v.item_type == "exp" then
        deltaExp = deltaExp + v.number
      end
      currentRoundRewards.icon = currentRoundRewards.icon .. icon .. "\001"
      currentRoundRewards.text = currentRoundRewards.text .. text .. "\001"
      currentRoundRewards.item_type = v.item_type
      currentRoundRewards.item_id = v.number
    end
  end
  if LuaUtils:table_size(rewardsList) < 5 then
    for i = LuaUtils:table_size(rewardsList), 5 do
      currentRoundRewards.icon = currentRoundRewards.icon .. "empty" .. "\001"
      currentRoundRewards.text = currentRoundRewards.text .. GameLoader:GetGameText("LC_MENU_EMPTY") .. "\001"
      currentRoundRewards.item_type = nil
      currentRoundRewards.item_id = nil
    end
  end
  table.insert(GameRush.allRewardItems, GameRush.itemPos, currentRoundRewards)
  self:GetFlashObject():InvokeASCallback("_root", "InsertItem", GameRush.itemPos, GameRush.itemPos)
  if GameUtils:isPlayerMaxLevel(levelInfo.level) then
    self:GetFlashObject():InvokeASCallback("_root", "setLevelAndExp", GameLoader:GetGameText("LC_MENU_Level") .. levelInfo.level, "-", "-", 100, deltaExp)
  else
    local expPercent = math.floor(100 * levelInfo.experience / levelInfo.uplevel_experience)
    self:GetFlashObject():InvokeASCallback("_root", "updateLevelAndExp", GameLoader:GetGameText("LC_MENU_Level") .. levelInfo.level, levelInfo.experience, levelInfo.uplevel_experience, expPercent, deltaExp)
  end
end
function GameRush:UpdateBossList(reInitBossList)
  if reInitBossList then
    self:GetFlashObject():InvokeASCallback("_root", "ReInitBossList")
    self.firstBossIndex, self.lastBossIndex, self.currentBosses = 1, GameObjectAdventure:getCanRushBossCountAndList()
    for i = self.firstBossIndex, self.lastBossIndex do
      self:GetFlashObject():InvokeASCallback("_root", "InsertBossItem", i, i)
    end
  else
    self:GetFlashObject():InvokeASCallback("_root", "EraseBossItem", self.firstBossIndex)
    self.firstBossIndex = self.firstBossIndex + 1
  end
end
function GameRush:IsRushBoss()
  return GameStateManager:GetCurrentGameState() == GameStateConfrontation
end
function GameRush:GetBossListCnt()
  return self.lastBossIndex - self.firstBossIndex + 1
end
function GameRush:OnFSCommand(cmd, arg)
  GameTextEdit:fscommand(self:GetFlashObject(), cmd, arg)
  if cmd == "rushPress" then
    if self:IsRushBoss() and not self.isElite then
      local bossCount = self:GetBossListCnt()
      DebugOut("OnFSCommand IsRushBoss", bossCount)
      if bossCount and bossCount > 0 and not self.startRushBossTick then
        self:StartRushBoss()
        self:StartRushBossTick()
      end
    else
      DebugOut("set self.roundCount = " .. tonumber(arg))
      local battle_supply = GameGlobalData:GetData("battle_supply")
      if self.isElite and battle_supply.current < 2 then
        GameUISection:AddBattleSupply()
        return
      end
      if not self.rushRunning then
        self.roundCount = tonumber(arg)
        self:GetFlashObject():InvokeASCallback("_root", "setRoundCountdown", self.roundCount)
        if self:StartRush() then
          self.rushRunning = true
        end
      else
        DebugOut("self.rushRunning", self.rushRunning)
      end
    end
  end
  if cmd == "IsAgree" then
    isCheckBoxTick = tonumber(arg)
    DebugOut("isCheckBoxTick", isCheckBoxTick)
  end
  if cmd == "IsAgreeForBoss" then
    isCheckBoxTickForBoss = tonumber(arg)
    DebugOut("isCheckBoxTickForBoss", isCheckBoxTickForBoss)
  end
  if cmd == "needUpdateItem" then
    local itemKey = tonumber(arg)
    local roundNum = itemKey
    local rewardsName = self.allRewardItems[roundNum].name or ""
    local rewards = self.allRewardItems[roundNum].icon or ""
    local texts = self.allRewardItems[roundNum].text or ""
    local strike = self.allRewardItems[roundNum].strike
    if self.allRewardItems[roundNum].item_type and self.allRewardItems[roundNum].item_id and (self.allRewardItems[roundNum].item_type == "item" or self.allRewardItems[roundNum].item_type == "equip") and DynamicResDownloader:IsDynamicStuff(self.allRewardItems[roundNum].item_id, DynamicResDownloader.resType.PIC) then
      local fullFileName = DynamicResDownloader:GetFullName(self.allRewardItems[roundNum].item_id .. ".png", DynamicResDownloader.resType.PIC)
      local localPath = "data2/" .. fullFileName
      if DynamicResDownloader:IfResExsit(localPath) then
        ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
        rewards = self.allRewardItems[roundNum].icon
      else
        rewards = "temp"
        if self.allRewardItems[roundNum].item_type == "item" or self.allRewardItems[roundNum].item_type == "equip" then
          self:AddDownloadPath(self.allRewardItems[roundNum].item_id)
        else
          self:AddDownloadPath(self.allRewardItems[roundNum].item_type)
        end
      end
    end
    GameRush:GetFlashObject():InvokeASCallback("_root", "UpdateItem", itemKey, roundNum, rewardsName, rewards, texts, strike)
  end
  if cmd == "needUpdateBossItem" then
    local itemKey = tonumber(arg)
    local roundNum = itemKey
    local bossIcon = self.currentBosses[roundNum].icon or ""
    local bossName = self.currentBosses[roundNum].name or ""
    GameRush:GetFlashObject():InvokeASCallback("_root", "UpdateBossItem", itemKey, roundNum, bossIcon, bossName)
  end
  if cmd == "ChangeCountdownTimer" then
    if isStopRushByAccident then
      isStopRushByAccident = false
      self.rushRunning = false
      GameRush:GetFlashObject():InvokeASCallback("_root", "lua2fs_stopRush")
      local battle_supply = GameGlobalData:GetData("battle_supply")
      if battle_supply.current > 1 then
        if self.roundCount == 0 then
          GameRush:GetFlashObject():InvokeASCallback("_root", "SetRoundCount", 1)
          self:GetFlashObject():InvokeASCallback("_root", "lua2fs_stopBtn10State")
          if self.isElite and 1 < self.maxEliteCount then
            GameRush:GetFlashObject():InvokeASCallback("_root", "setBtnPlus10State", 1)
          elseif self.isElite and 1 >= self.maxEliteCount then
            GameRush:GetFlashObject():InvokeASCallback("_root", "setBtnPlus10State", 0)
          end
        else
          GameRush:GetFlashObject():InvokeASCallback("_root", "setBtnminus10State", 1)
          GameRush:GetFlashObject():InvokeASCallback("_root", "setBtnPlus10State", 1)
        end
      else
        if self.roundCount == 0 then
          GameRush:GetFlashObject():InvokeASCallback("_root", "SetRoundCount", 1)
        end
        GameRush:GetFlashObject():InvokeASCallback("_root", "setBtnminus10State", 0)
        GameRush:GetFlashObject():InvokeASCallback("_root", "setBtnPlus10State", 0)
      end
      return
    end
    if 0 < self.roundCount then
      GameRush:StartRush()
    else
      self.rushRunning = false
      GameRush:GetFlashObject():InvokeASCallback("_root", "lua2fs_stopRush")
      local battle_supply = GameGlobalData:GetData("battle_supply")
      if battle_supply.current > 1 then
        if self.roundCount == 0 then
          GameRush:GetFlashObject():InvokeASCallback("_root", "SetRoundCount", 1)
          self:GetFlashObject():InvokeASCallback("_root", "lua2fs_stopBtn10State")
          if self.isElite and 1 < self.maxEliteCount then
            GameRush:GetFlashObject():InvokeASCallback("_root", "setBtnPlus10State", 1)
          elseif self.isElite and 1 >= self.maxEliteCount then
            GameRush:GetFlashObject():InvokeASCallback("_root", "setBtnPlus10State", 0)
          end
        end
      else
        if self.roundCount == 0 then
          GameRush:GetFlashObject():InvokeASCallback("_root", "SetRoundCount", 1)
        end
        GameRush:GetFlashObject():InvokeASCallback("_root", "setBtnminus10State", 0)
        GameRush:GetFlashObject():InvokeASCallback("_root", "setBtnPlus10State", 0)
      end
    end
  end
  if cmd == "Remove_Rush" then
    if not self:IsRushBoss() then
      local user_rush_exit_req_param = {
        battle_id = GameUIEnemyInfo.m_combinedBattleID,
        action = 0
      }
      NetMessageMgr:SendMsg(NetAPIList.user_rush_exit_req.Code, user_rush_exit_req_param, nil, false, nil)
    end
    GameStateManager:GetCurrentGameState():EraseObject(self)
  end
  if cmd == "add_energy" then
    GameUISection:AddBattleSupply()
  end
  if cmd == "btn_minus10" then
    self.roundCount = tonumber(arg)
    if self.isElite then
      self.roundCount = self.roundCount - 3
    else
      self.roundCount = self.roundCount - 10
    end
    if self.roundCount < 1 then
      self.roundCount = 1
    end
    GameRush:GetFlashObject():InvokeASCallback("_root", "setRoundCountNew", GameRush.roundCount)
    if self.isElite and self.roundCount < self.maxEliteCount then
      GameRush:GetFlashObject():InvokeASCallback("_root", "setBtnPlus10State", 1)
    end
  end
  if cmd == "btn_plus10" then
    local battle_supply = GameGlobalData:GetData("battle_supply")
    self.roundCount = tonumber(arg)
    if self.isElite then
      self.roundCount = self.roundCount + 3
      if self.roundCount >= self.maxEliteCount then
        self.roundCount = self.maxEliteCount
        GameRush:GetFlashObject():InvokeASCallback("_root", "setBtnPlus10State", 0)
      end
    else
      self.roundCount = self.roundCount + 10
      if self.roundCount > battle_supply.current then
        self.roundCount = battle_supply.current
      end
    end
    GameRush:GetFlashObject():InvokeASCallback("_root", "setRoundCountNew", GameRush.roundCount)
  end
  if cmd == "touch_return_press" then
    GameTextEdit:HideKeyboard(0, 0)
  end
end
function GameRush:AddDownloadPath(itemID, itemKey, index)
  local resName = itemID .. ".png"
  DynamicResDownloader:AddDynamicRes(resName, DynamicResDownloader.resType.PIC, nil, nil)
end
if AutoUpdate.isAndroidDevice then
  function GameRush.OnAndroidBack()
    GameRush:GetFlashObject():InvokeASCallback("_root", "HideRushMenu")
  end
end
function GameRush.levelupNotifyHandler()
  DebugOut("GameRush.levelupNotifyHandler")
  local IsAgree = false
  if GameRush:IsRushBoss() then
    IsAgree = isCheckBoxTickForBoss
    if GameRush.isElite then
      IsAgree = isCheckBoxTick
    end
  else
    IsAgree = isCheckBoxTick
  end
  if IsAgree == 1 then
    isStopRushByAccident = true
  end
end
function GameRush:SetButton10State()
  DebugOut("SetButton10State:")
  if not GameRush:GetFlashObject() then
    return
  end
  local battle_supply = GameGlobalData:GetData("battle_supply")
  if self.isElite then
    GameRush:GetFlashObject():InvokeASCallback("_root", "setCurrentSupply", self.maxEliteCount)
  else
    GameRush:GetFlashObject():InvokeASCallback("_root", "setCurrentSupply", battle_supply.current)
  end
  if battle_supply.current == 0 then
    GameRush:GetFlashObject():InvokeASCallback("_root", "setBtnminus10State", 0)
    GameRush:GetFlashObject():InvokeASCallback("_root", "setBtnPlus10State", 0)
  elseif battle_supply.current == 1 then
    GameRush:GetFlashObject():InvokeASCallback("_root", "setBtnminus10State", 0)
    GameRush:GetFlashObject():InvokeASCallback("_root", "setBtnPlus10State", 0)
  elseif battle_supply.current > 1 then
    if self.roundCount == 0 then
      GameRush:GetFlashObject():InvokeASCallback("_root", "setBtnminus10State", 0)
      GameRush:GetFlashObject():InvokeASCallback("_root", "setBtnPlus10State", 1)
    else
      GameRush:GetFlashObject():InvokeASCallback("_root", "setBtnminus10State", 1)
      GameRush:GetFlashObject():InvokeASCallback("_root", "setBtnPlus10State", 1)
    end
  end
  if self.isElite and self.maxEliteCount <= 1 then
    GameRush:GetFlashObject():InvokeASCallback("_root", "setBtnminus10State", 0)
    GameRush:GetFlashObject():InvokeASCallback("_root", "setBtnPlus10State", 0)
  elseif self.isElite then
    GameRush:GetFlashObject():InvokeASCallback("_root", "setBtnPlus10State", 0)
  end
end
function GameRush:SetButtonMinus10State()
  if not GameRush:GetFlashObject() then
    return
  end
  if self.roundCount == 0 then
    GameRush:GetFlashObject():InvokeASCallback("_root", "setBtnminus10State", 0)
  end
end
