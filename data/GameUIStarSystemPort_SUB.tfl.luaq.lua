local GameUIStarSystemPort_SUB = LuaObjectManager:GetLuaObject("GameUIStarSystemPort_SUB")
local GameUIStarSystemPort = LuaObjectManager:GetLuaObject("GameUIStarSystemPort")
print("GameUIStarSystemPort_SUB")
function GameUIStarSystemPort_SUB:OnFSCommand(cmd, arg)
  GameUIStarSystemPort:OnFSCommand(cmd, arg)
end
function GameUIStarSystemPort_SUB:OnAddToGameState()
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
end
function GameUIStarSystemPort_SUB:OnEraseFromGameState()
  if self:GetFlashObject() then
    self:UnloadFlashObject()
  end
end
function GameUIStarSystemPort_SUB:Update(dt)
  local flash_obj = self:GetFlashObject()
  if flash_obj == nil then
    return
  end
  flash_obj:Update(dt)
  GameUIStarSystemPort.gacha:OnGachaUpdate(dt)
  GameUIStarSystemPort.medalEquip:OnMedalEquipUpdate(dt)
  flash_obj:InvokeASCallback("_root", "OnUpdate", dt)
end
