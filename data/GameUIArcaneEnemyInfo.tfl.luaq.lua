local GameStateArcane = GameStateManager.GameStateArcane
local GameStateFormation = GameStateManager.GameStateFormation
local GameStateBattlePlay = GameStateManager.GameStateBattlePlay
local GameUIArcaneEnemyInfo = LuaObjectManager:GetLuaObject("GameUIArcaneEnemyInfo")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameObjectFormationBackground = LuaObjectManager:GetLuaObject("GameObjectFormationBackground")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
function GameUIArcaneEnemyInfo:OnAddToGameState()
  self:LoadFlashObject()
  local function netCall()
    NetMessageMgr:SendMsg(NetAPIList.ac_battle_info_req.Code, nil, self.NetCallbackBattleInfo, true, netCall)
  end
  netCall()
end
function GameUIArcaneEnemyInfo:OnEraseFromGameState()
  self:UnloadFlashObject()
end
function GameUIArcaneEnemyInfo:OnFSCommand(cmd, arg)
  if cmd == "Close_Enemy_Info" then
    self:MoveOutAll()
  elseif cmd == "Remove_Enemy_Info" then
    GameStateArcane:EraseObject(self)
  elseif cmd == "Enemy_Info_BattleNow" then
    local function netCall()
      NetMessageMgr:SendMsg(NetAPIList.ac_matrix_req.Code, nil, self.NetCallbackEnemiesMatrix, netCall)
    end
    netCall()
  elseif cmd == "history_box_move_out" then
  end
end
function GameUIArcaneEnemyInfo:SetArcaneBattleInfo(battleInfo)
  self:GetFlashObject():InvokeASCallback("_root", "setEnemyInfo", battleInfo.enemyName, battleInfo.enemyAvatar, battleInfo.enemyLevel, battleInfo.enemyDesc, true, 0)
end
function GameUIArcaneEnemyInfo:SetAwardsInfo(awardsTable)
  for indexAward = 1, 5 do
    local text = -1
    local icon = -1
    local dataAward = awardsTable[indexAward]
    if dataAward then
      text = GameLoader:GetGameText("LC_MENU_" .. string.upper(dataAward.base.item_type) .. "_CHAR")
      icon = dataAward.base.item_type
      if dataAward.base.item_type == "item" or dataAward.base.item_type == "equip" then
        if tonumber(dataAward.base.number) > 100000 then
          text = GameLoader:GetGameText("LC_ITEM_EQUIP_TYPE_SLOT" .. GameDataAccessHelper:GetEquipSlot(dataAward.base.number))
        else
          text = GameLoader:GetGameText("LC_ITEM_ITEM_CHAR")
        end
        icon = "item_" .. dataAward.base.number
      end
    else
      text = GameLoader:GetGameText("LC_MENU_EMPTY")
      icon = -1
    end
    self:GetFlashObject():InvokeASCallback("_root", "SetReward", tostring(icon), tostring(text), indexAward)
  end
end
function GameUIArcaneEnemyInfo:MoveInAll()
  self:GetFlashObject():InvokeASCallback("_root", "InitUI", false, false, "Train")
  self:GetFlashObject():InvokeASCallback("_root", "moveInArcaneEnemyInfo")
end
function GameUIArcaneEnemyInfo:MoveOutAll()
  self:GetFlashObject():InvokeASCallback("_root", "moveOutArcaneEnemyInfo")
end
function GameUIArcaneEnemyInfo:Update(dt)
  local flashObject = self:GetFlashObject()
  if flashObject then
    flashObject:Update(dt)
    flashObject:InvokeASCallback("_root", "onUpdateFrame", dt)
  end
end
function GameUIArcaneEnemyInfo.NetCallbackBattleInfo(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.ac_battle_info_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  if msgType == NetAPIList.battle_status_ack.Code then
    local battleInfo = {}
    local arcaneInfo = GameStateArcane:GetArcaneInfo()
    local arcaneBattleInfo = GameDataAccessHelper:GetArcaneBattleInfo(arcaneInfo.ac_level * 1000 + arcaneInfo.ac_step)
    battleInfo.enemyName = GameLoader:GetGameText("LC_BATTLE_AREA52_1001_NAME")
    battleInfo.enemyAvatar = arcaneBattleInfo.Head
    battleInfo.enemyLevel = arcaneBattleInfo.level_req
    battleInfo.enemyDesc = GameLoader:GetGameText("LC_BATTLE_AREA52_1001_DESC")
    GameUIArcaneEnemyInfo:SetArcaneBattleInfo(battleInfo)
    GameUIArcaneEnemyInfo:SetAwardsInfo(content.rewards)
    GameUIArcaneEnemyInfo:MoveInAll()
    return true
  end
  return false
end
function GameUIArcaneEnemyInfo.NetCallbackEnemiesMatrix(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.ac_matrix_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  if msgType == NetAPIList.battle_matrix_ack.Code then
    local arcaneInfo = GameStateArcane:GetArcaneInfo()
    GameObjectFormationBackground.force = content.force
    GameObjectFormationBackground:SetEnemiesMatrixData(content.monsters.cells)
    local jamed = 0
    if arcaneInfo then
      jamed = arcaneInfo.ac_step or 1
      jamed = jamed - 1
    end
    GameObjectFormationBackground:SetJamedCount(arcaneInfo.ac_step - 1)
    GameStateArcane:EraseObject(GameUIArcaneEnemyInfo)
    GameStateFormation:SetBattleID(52, arcaneInfo.ac_level * 1000 + arcaneInfo.ac_step)
    local dataProvider = {}
    function dataProvider:GetEnemiesMatrix()
      return content.monsters.cells
    end
    GameObjectFormationBackground:SetDataProvider(dataProvider)
    GameStateManager:SetCurrentGameState(GameStateFormation)
    return true
  end
  return false
end
function GameUIArcaneEnemyInfo.NetCallbackBattleResult(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.ac_battle_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  if msgType == NetAPIList.battle_result_ack.Code then
    do
      local battleReport = content.report
      if battleReport then
        local function playOverCallback()
          local GameUIBattleResult = LuaObjectManager:GetLuaObject("GameUIBattleResult")
          GameUIBattleResult:LoadFlashObject()
          local windowType = ""
          local awardDataTable = {}
          if battleReport.result == 1 then
            for indexAward, dataAward in ipairs(content.result.rewards) do
              local itemName, icon = GameHelper:GetAwardTypeTextAndIcon(dataAward.base.item_type, dataAward.base.number)
              if dataAward.base.item_type ~= "exp" then
                table.insert(awardDataTable, {
                  itemType = dataAward.base.item_type,
                  itemDesc = itemName,
                  itemNumber = dataAward.base.number
                })
              end
            end
            GameUIBattleResult:UpdateAwardList("battle_win", awardDataTable)
            windowType = "battle_win"
          else
            windowType = "battle_lose"
          end
          GameUIBattleResult:SetFightReport(battleReport, nil, nil)
          GameUIBattleResult:UpdateExperiencePercent()
          GameUIBattleResult:UpdateAccomplishLevel(-1)
          local function enterCallback()
            GameUIBattleResult:AnimationMoveIn(windowType, false)
            GameStateArcane:AddObject(GameUIBattleResult)
          end
          GameStateArcane:SetEnterCallback(enterCallback)
          GameStateManager:SetCurrentGameState(GameStateArcane)
        end
        GameStateBattlePlay.curBattleType = "acbattle"
        GameStateBattlePlay:InitBattle(GameStateBattlePlay.MODE_PLAY, battleReport)
        GameStateBattlePlay:RegisterOverCallback(playOverCallback)
        GameStateManager:SetCurrentGameState(GameStateBattlePlay)
      end
      return true
    end
  end
  return false
end
function GameUIArcaneEnemyInfo:BattleNow()
  local function startBattleCallback()
    NetMessageMgr:SendMsg(NetAPIList.ac_battle_req.Code, nil, GameUIArcaneEnemyInfo.NetCallbackBattleResult, true, startBattleCallback)
  end
  startBattleCallback()
end
if AutoUpdate.isAndroidDevice then
  function GameUIArcaneEnemyInfo.OnAndroidBack()
    GameUIArcaneEnemyInfo:MoveOutAll()
  end
end
