local GameCommonBackground = LuaObjectManager:GetLuaObject("GameCommonBackground")
function GameCommonBackground:OnAddToGameState()
  self:LoadFlashObject()
end
function GameCommonBackground:OnEraseFromGameState()
  self:UnloadFlashObject()
end
