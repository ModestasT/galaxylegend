local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local GameStateDaily = GameStateManager.GameStateDaily
local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
function GameStateDaily:OnFocusGain(previousState)
  self:init()
end
function GameStateDaily:OnFocusLost(newState)
  self:EraseObject(GameHelper)
end
function GameStateDaily:init()
  self:AddObject(GameHelper)
end
function GameStateDaily:AddObject(obj)
  GameStateBase.AddObject(self, obj)
  if obj ~= GameHelper and obj ~= ItemBox then
    GameHelper:OnAddOther()
  end
end
function GameStateDaily:EraseObject(obj)
  GameStateBase.EraseObject(self, obj)
  if obj ~= GameHelper and obj ~= ItemBox then
    GameHelper:OnEraseOther()
  end
end
function GameStateDaily:GetTexture(uri, downLoadCallBack, arg)
  local fullFileName = DynamicResDownloader:GetFullName(uri, DynamicResDownloader.resType.PIC)
  local downResName = uri
  local extInfo = {FileName = fullFileName}
  extInfo.arg = arg
  local localPath = "data2/" .. fullFileName
  DebugOut("fullFileName = " .. fullFileName)
  if DynamicResDownloader:IfResExsit(localPath) then
    downLoadCallBack(extInfo)
  else
    DynamicResDownloader:AddDynamicRes(downResName, DynamicResDownloader.resType.PIC, extInfo, downLoadCallBack)
  end
end
