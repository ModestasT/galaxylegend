slt = {}
local function precompile(template, start_tag, end_tag)
  local result = {}
  local orig_stag = start_tag
  start_tag = start_tag .. "include:"
  local start1, end1 = string.find(template, start_tag, 1, true)
  local start2
  local end2 = 0
  local fin, filename, func, err
  while start1 ~= nil do
    if start1 > end2 + 1 then
      table.insert(result, string.sub(template, end2 + 1, start1 - 1))
    end
    start2, end2 = string.find(template, end_tag, end1 + 1, true)
    if start2 ~= nil then
      func, err = loadstring("return " .. string.sub(template, end1 + 1, start2 - 1))
      if func == nil then
        return nil, err
      end
      filename = func()
      fin, err = io.open(filename)
      if fin == nil then
        return nil, err
      end
      table.insert(result, precompile(fin:read("*a"), orig_stag, end_tag))
      fin:close()
      start1, end1 = string.find(template, start_tag, end2 + 1, true)
    else
      end2 = end1
      start1 = nil
    end
  end
  table.insert(result, string.sub(template, end2 + 1))
  return (...), result, string.sub(template, end2 + 1)
end
function slt.loadstring(template, start_tag, end_tag, output_func)
  local lua_code = {}
  if start_tag == nil then
    start_tag = "#{"
  end
  if end_tag == nil then
    end_tag = " }"
  end
  if output_func == nil then
    output_func = "outputfn"
  end
  local err
  template, err = precompile(template, start_tag, end_tag)
  if template == nil then
    return nil, err
  end
  local start1, end1 = string.find(template, start_tag, 1, true)
  local start2
  local end2 = 0
  local cEqual = string.byte("=", 1)
  while start1 ~= nil do
    if start1 > end2 + 1 then
      table.insert(lua_code, output_func .. "(" .. string.format("%q", string.sub(template, end2 + 1, start1 - 1)) .. ")")
    end
    start2, end2 = string.find(template, end_tag, end1 + 1, true)
    if start2 ~= nil then
      if string.byte(template, end1 + 1) ~= cEqual then
        table.insert(lua_code, string.sub(template, end1 + 1, start2 - 1))
      else
        table.insert(lua_code, output_func .. "(" .. string.sub(template, end1 + 2, start2 - 1) .. ")")
      end
      start1, end1 = string.find(template, start_tag, end2 + 1, true)
    else
      end2 = end1
      start1 = nil
    end
  end
  table.insert(lua_code, output_func .. "(" .. string.format("%q", string.sub(template, end2 + 1)) .. ")")
  return ...
end
function slt.loadfile(filename, start_tag, end_tag, output_func)
  local fin = io.open(filename)
  local all = fin:read("*a")
  fin:close()
  return ...
end
function slt.render(f, env)
  if env == nil then
    env = _G
  end
  setfenv(f, env)()
end
function slt.fuck()
  print("fuck  everybody")
end
return slt
