local GameObjectClimbTower = LuaObjectManager:GetLuaObject("GameObjectClimbTower")
GameObjectClimbTower.ExpeditionRankList = {RANK_LIST_LAYER = 1, RANK_LIST_DAMAGE = 2}
local ExpeditionRankList = GameObjectClimbTower.ExpeditionRankList
local Private = {}
function ExpeditionRankList:Show(t, refreshData)
  if t == nil then
    t = ExpeditionRankList.RANK_LIST_LAYER
  end
  Private.showType = t
  local function callback(msgType, content)
    if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.expedition_wormhole_rank_board_req.Code then
      if content.code ~= 0 then
        local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
        GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      end
      return true
    elseif msgType == NetAPIList.expedition_wormhole_rank_board_ack.Code then
      Private:StoreRankListContent(content)
      Private:Show(true)
      return true
    end
  end
  if refreshData then
    local param = {type = 0, top = 500}
    NetMessageMgr:SendMsg(NetAPIList.expedition_wormhole_rank_board_req.Code, param, callback, true, nil)
  else
    Private:Show(false)
  end
end
function ExpeditionRankList:GetFlashObject()
  return (...), GameObjectClimbTower
end
function ExpeditionRankList:Hide()
  ExpeditionRankList:GetFlashObject():InvokeASCallback("_root", "hideInstanceRankList")
end
function Private:StoreRankListContent(content)
  ExpeditionRankList.listData = content
end
function Private:Show(playAnim)
  if ExpeditionRankList.listData == nil or ExpeditionRankList.listData.rank_board == nil or ExpeditionRankList.listData.rank_board[Private.showType] == nil then
    DebugOut("ExpeditionRankList Private:Show empty list", debug.traceback())
    return
  end
  local rankListData = ExpeditionRankList.listData.rank_board[Private.showType]
  DebugOut("ExpeditionRankList Private:Show ")
  if rankListData.self == nil then
    return
  end
  local data = {}
  data.count = #rankListData.top_list
  data.self = {
    user_name = rankListData.self.user_name,
    point = rankListData.self.score,
    level = rankListData.self.level
  }
  data.rank_level = rankListData.self.rank
  data.RankingCeil = 99999
  data.LevelText = GameLoader:GetGameText("LC_MENU_EXPEDITION_RANK_LEVEL_SHEET")
  print("ExpeditionRankList:GetFlashObject():")
  print(ExpeditionRankList:GetFlashObject())
  rank_desc_text = GameLoader:GetGameText("LC_MENU_EXPEDITION_RANK_NUMBER_SHEET")
  if Private.showType == ExpeditionRankList.RANK_LIST_DAMAGE then
    rank_desc_text = GameLoader:GetGameText("LC_MENU_EXPEDITION_RANK_DAMAGE_SHEET")
  end
  ExpeditionRankList:GetFlashObject():InvokeASCallback("_root", "showInstanceRankList", data, Private.showType, rank_desc_text, playAnim)
end
function ExpeditionRankList:UpdateListItem(itemIndex)
  local DataString = -1
  local rankInfo
  local rankListData = ExpeditionRankList.listData.rank_board[Private.showType]
  local rankList = rankListData.top_list
  if itemIndex > 0 and itemIndex <= #rankList then
    rankInfo = rankList[itemIndex]
  end
  if rankInfo then
    local DataTable = {}
    table.insert(DataTable, rankInfo.rank)
    table.insert(DataTable, rankInfo.user_name)
    table.insert(DataTable, rankInfo.level)
    table.insert(DataTable, rankInfo.score)
    DataString = table.concat(DataTable, "\001")
  end
  ExpeditionRankList:GetFlashObject():InvokeASCallback("_root", "SetListItem", itemIndex, DataString)
end
