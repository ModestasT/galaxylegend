local GameStateToprank = GameStateManager.GameStateToprank
local GameObjectToprank = LuaObjectManager:GetLuaObject("GameObjectToprank")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
function GameObjectToprank:OnAddToGameState(parent)
  self:LoadFlashObject()
  self:CheckShowBtnGP()
  local function netCallToprank()
    NetMessageMgr:SendMsg(NetAPIList.top_force_list_req.Code, nil, self.NetCallbackToprank, true, netCallToprank)
  end
  netCallToprank()
end
function GameObjectToprank:CheckShowBtnGP()
  DebugOut("CheckShowBtnGP:", IPlatformExt.isLeaderBoard_support())
  if IPlatformExt.isLeaderBoard_support() then
    self:GetFlashObject():InvokeASCallback("_root", "SetBtnGPVisible", true)
  else
    self:GetFlashObject():InvokeASCallback("_root", "SetBtnGPVisible", false)
  end
end
function GameObjectToprank:OnEraseFromGameState(parent)
  self:UnloadFlashObject()
  self.ToprankInfo = nil
end
function GameObjectToprank:InitFlashObject()
  self:GetFlashObject():InvokeASCallback("_root", "onFlashObjectLoaded")
end
function GameObjectToprank:UpdatePlayerRankInfo()
  local dataTable = {}
  local userInfo = GameGlobalData:GetData("userinfo")
  local levelInfo = GameGlobalData:GetData("levelinfo")
  local playerRank = -1
  for rankIndex, playerInfo in ipairs(self.ToprankInfo.players) do
    if playerInfo.user_id == userInfo.player_id then
      playerRank = tostring(rankIndex)
      break
    end
  end
  table.insert(dataTable, playerRank)
  table.insert(dataTable, GameUtils:GetUserDisplayName(userInfo.name))
  table.insert(dataTable, tostring(levelInfo.level))
  table.insert(dataTable, GameUtils.numberConversion(GameGlobalData:ComputePlayerBattleForce()))
  local dataString = table.concat(dataTable, "\001")
  DebugOut(dataString)
  self:GetFlashObject():InvokeASCallback("_root", "setPlayerRankInfo", dataString)
end
function GameObjectToprank:UpdateToprankInfo(content)
  self.ToprankInfo = content
  self:GetFlashObject():InvokeASCallback("_root", "syncToprankList", #self.ToprankInfo.players)
  self:UpdatePlayerRankInfo()
end
function GameObjectToprank:UpdateToprankItem(itemIndex)
  local playerData = self.ToprankInfo.players[itemIndex]
  local dataTable = {}
  table.insert(dataTable, tostring(itemIndex))
  table.insert(dataTable, GameUtils:GetUserDisplayName(playerData.name))
  table.insert(dataTable, tostring(playerData.level))
  table.insert(dataTable, GameUtils.numberConversion(playerData.force))
  local dataString = table.concat(dataTable, "\001")
  self:GetFlashObject():InvokeASCallback("_root", "setToprankItem", itemIndex, dataString)
end
function GameObjectToprank:CheckNeedMoveIn()
  if self.ToprankInfo then
    self:GetFlashObject():InvokeASCallback("_root", "showMoveIn")
  end
end
function GameObjectToprank.NetCallbackToprank(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.champion_top_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  if msgType == NetAPIList.top_force_list_ntf.Code then
    GameObjectToprank:UpdateToprankInfo(content)
    GameObjectToprank:CheckNeedMoveIn()
    return true
  end
  return false
end
function GameObjectToprank:OnFSCommand(ascmd, arg)
  if ascmd == "onClose" then
    GameStateToprank:Quit()
  elseif ascmd == "onUpdateRankItem" then
    self:UpdateToprankItem(tonumber(arg))
  elseif ascmd == "btn_GP" then
    DebugOut("click btn_GP")
    if IPlatformExt.isLeaderBoard_support() then
      IPlatformExt.show_LeaderBord()
    end
  end
end
function GameObjectToprank:Update(dt)
  local flashObject = self:GetFlashObject()
  if flashObject then
    flashObject:Update(dt)
    flashObject:InvokeASCallback("_root", "onUpdateFrame", dt)
  end
end
if AutoUpdate.isAndroidDevice then
  function GameObjectToprank.OnAndroidBack()
    GameObjectToprank:GetFlashObject():InvokeASCallback("_root", "mainMoveOut")
  end
end
