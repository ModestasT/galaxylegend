local GameUIBuildingInfo = LuaObjectManager:GetLuaObject("GameUIBuildingInfo")
local GameStateMainPlanet = GameStateManager.GameStateMainPlanet
local GameGlobalData = GameGlobalData
function GameUIBuildingInfo:UpdateInfoContent(building_info)
  DebugOut("GameUIBuildingInfo")
  DebugTable(building_info)
  local building_desc = "LC_MENU_BUILDING_DESC_" .. string.upper(building_info.name)
  building_desc = GameLoader:GetGameText(building_desc)
  local flash_obj = self:GetFlashObject()
  local levelStr = building_info.level
  flash_obj:InvokeASCallback("_root", "UpdateInfoContent", levelStr, building_desc)
  flash_obj:ReplaceTexture("NA_Building.png", building_info.name .. ".png")
end
function GameUIBuildingInfo:DisplayInfoDialog(building_name)
  local buildingInfo = GameGlobalData:GetBuildingInfo(building_name)
  self:LoadFlashObject()
  local flash_obj = self:GetFlashObject()
  self.curBuildingName = building_name
  self:UpdateInfoContent(buildingInfo)
  local building_name_text = GameLoader:GetGameText("LC_MENU_BUILDING_NAMELG_" .. string.upper(building_name))
  flash_obj:InvokeASCallback("_root", "DisplayInfoDialog", building_name_text)
  GameStateMainPlanet:AddObject(self)
end
function GameUIBuildingInfo:OnEraseFromGameState()
  self:UnloadFlashObject()
end
function GameUIBuildingInfo:OnInitGame()
  GameGlobalData:RegisterDataChangeCallback("buildings", self.OnGlobalBuildingDataChange)
end
function GameUIBuildingInfo.OnGlobalBuildingDataChange()
  if not GameStateManager:GetCurrentGameState():IsObjectInState(GameUIBuildingInfo) then
    return
  end
  if GameUIBuildingInfo:GetFlashObject() == nil or GameUIBuildingInfo.curBuildingName == nil then
    return
  end
  local buildingInfo = GameGlobalData:GetBuildingInfo(GameUIBuildingInfo.curBuildingName)
  GameUIBuildingInfo:UpdateInfoContent(buildingInfo)
end
function GameUIBuildingInfo:OnFSCommand(cmd, arg)
  if cmd == "CloseDialog" then
    GameStateMainPlanet:EraseObject(self)
    GameStateMainPlanet:CheckShowTutorial()
    return
  end
end
if AutoUpdate.isAndroidDevice then
  function GameUIBuildingInfo.OnAndroidBack()
    GameUIBuildingInfo:GetFlashObject():InvokeASCallback("_root", "dialogInfoMoveOut")
  end
end
