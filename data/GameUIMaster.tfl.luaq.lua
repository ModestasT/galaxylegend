local NetMessageMgr = NetMessageMgr
local NetAPIList = NetAPIList
local GameTimer = GameTimer
local AlertDataList = AlertDataList
local GameGlobalData = GameGlobalData
local GameUIMaster = LuaObjectManager:GetLuaObject("GameUIMaster")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
local GameUIMasterAchievementTips = LuaObjectManager:GetLuaObject("MasterAchievementTips")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameFleetEquipment = LuaObjectManager:GetLuaObject("GameFleetEquipment")
local GameStatePlayerMatrix = GameStateManager.GameStatePlayerMatrix
require("TacticsCenterDataManager.tfl")
local TacticsCenterDataManager = TacticsCenterDataManager.TacticsCenterDataManager
local tcdm = TacticsCenterDataManager
local RefineDataManager = LuaObjectManager:GetLuaObject("RefineDataManager")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
GameUIMaster.MasterSystem = {
  ModuleMaster = "module",
  KryptonMaster = "krypton",
  EquipMaster = "equip",
  TechnologyMaster = "tech",
  FormationMaster = "formation"
}
GameUIMaster.MasterPage = {
  equip_enchance = "equip_enchance",
  equip_upgrade = "equip_upgrade",
  equip_interfere = "equip_interfere",
  tech_initial = "tech_initial",
  tech_advanced = "tech_advanced",
  tech_hardcore = "tech_hardcore",
  tech_dimension = "tech_dimension",
  krypton_enchance = "krypton_enchance",
  krypton_grade = "krypton_grade",
  centrol_adjust = "centrol_adjust",
  centrol_grade = "centrol_grade",
  fleets_augment = "fleets_augment"
}
GameUIMaster.CurrentSystemType = 0
GameUIMaster.CurrentPageType = 0
GameUIMaster.CurMatrixId = 0
GameUIMaster.CurrentScope = nil
GameUIMaster.CurPageInfo = nil
GameUIMaster.CurPageDetail = nil
GameUIMaster.statList = {}
function GameUIMaster:OnAddToGameState(parent_state)
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  self:GetFlashObject():InvokeASCallback("_root", "setLocalText", "ModuleMaster", GameLoader:GetGameText("LC_MENU_MASTER_MODULE"))
  self:GetFlashObject():InvokeASCallback("_root", "setLocalText", "KryptonMaster", GameLoader:GetGameText("LC_MENU_MASTER_GALACTONITE"))
  self:GetFlashObject():InvokeASCallback("_root", "setLocalText", "EquipMaster", GameLoader:GetGameText("LC_MENU_MASTER_EQUIPMENT"))
  self:GetFlashObject():InvokeASCallback("_root", "setLocalText", "TechnologyMaster", GameLoader:GetGameText("LC_MENU_MASTER_TECHNIQUE"))
  self:GetFlashObject():InvokeASCallback("_root", "setLocalText", "FormationMaster", GameLoader:GetGameText("LC_MENU_MASTER_AUGMENT"))
  self:GetFlashObject():InvokeASCallback("_root", "setLocalText", "BuffForCurFleet", GameLoader:GetGameText("LC_MENU_MASTER_EFFECT_FLEET"))
  self:GetFlashObject():InvokeASCallback("_root", "setLocalText", "BuffForCurFormation", GameLoader:GetGameText("LC_MENU_MASTER_EFFECT_FORMATION"))
  self:GetFlashObject():InvokeASCallback("_root", "setLocalText", "BuffForAllFleet", GameLoader:GetGameText("LC_MENU_MASTER_EFFECT_ALL"))
  self:GetFlashObject():InvokeASCallback("_root", "setLocalText", "info_name", GameLoader:GetGameText("LC_MENU_MASTER_SCHEDULE_NAME"))
  self:RequestCurMasterInfo()
end
function GameUIMaster:OnEraseFromGameState()
  if self:GetFlashObject() then
    self:UnloadFlashObject()
  end
end
function GameUIMaster:ShowMainMenu()
  self:GetFlashObject():InvokeASCallback("_root", "MoveIn")
end
function GameUIMaster:Update(dt)
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    flash_obj:Update(dt)
    flash_obj:InvokeASCallback("_root", "onUpdateFrame", dt)
  end
end
function GameUIMaster:OnFSCommand(cmd, arg)
  if cmd == "movedOut" then
    if GameStateManager:GetCurrentGameState():IsObjectInState(GameUIMaster) then
      GameStateManager:GetCurrentGameState():EraseObject(GameUIMaster)
    end
  elseif cmd == "UpdateAttrItem" then
    self:UpdateAttrItem(tonumber(arg))
  elseif cmd == "UpdateProgressItem" then
    self:UpdateProgressItem(tonumber(arg))
  elseif cmd == "switchPage" then
    self:SwitchPage(arg)
  elseif cmd == "showLevelNotice" then
    self:ShowLevelNotice(arg)
  end
end
function GameUIMaster:ShowLevelNotice(pageName)
  local level = 0
  for k, v in ipairs(GameUIMaster.statList) do
    if v.pageType == pageName then
      level = v.min_level
      break
    end
  end
  local infoContent = string.gsub(GameLoader:GetGameText("LC_MENU_MASTER_PLAYER_LEVEL_LIMIT"), "<Level_num>", level)
  GameTip:Show(infoContent)
end
function GameUIMaster:SwitchPage(pageName)
  GameUIMaster.CurrentPageType = GameUIMaster.MasterPage[pageName]
  GameUIMaster:InitPage()
end
function GameUIMaster:RequestCurMasterInfo()
  DebugOut("GameUIMaster:RequestCurMasterInfo", GameUIMaster.CurrentSystemType, GameUIMaster.CurrentPageType, GameUIMaster.CurMatrixId)
  if GameUIMaster.CurrentSystemType ~= 0 and GameUIMaster.CurrentPageType ~= 0 and GameUIMaster.CurrentScope ~= nil then
    local param = {
      master_type = GameUIMaster.CurrentSystemType,
      matrix_id = GameUIMaster.CurMatrixId,
      scope = GameUIMaster.CurrentScope
    }
    DebugTable(param)
    NetMessageMgr:SendMsg(NetAPIList.master_req.Code, param, GameUIMaster.MasterPageInfoCallback, true)
  end
end
function GameUIMaster.MasterPageInfoCallback(msgType, content)
  if msgType == NetAPIList.master_ack.Code then
    DebugOut("MasterPageInfoCallback")
    DebugTable(content.master_all_ack)
    GameUIMaster.CurPageInfo = content.master_all_ack
    GameUIMaster:InitPage(true)
    return true
  end
  return false
end
function GameUIMaster:GetMenuBtnStat()
  GameUIMaster.statList = {}
  local userinfo = GameGlobalData:GetData("levelinfo")
  for k, v in ipairs(GameUIMaster.CurPageInfo) do
    local detail = {
      pageType = v.type
    }
    detail.stat = userinfo.level >= v.min_level
    detail.min_level = v.min_level
    table.insert(GameUIMaster.statList, detail)
  end
  DebugOutPutTable(GameUIMaster.statList, "GetMenuBtnStat")
end
function GameUIMaster:InitPage(isShowMoveIn)
  GameUIMaster.CurPageDetail = self:GetPageDetail()
  local userinfo = GameGlobalData:GetData("levelinfo")
  if GameUIMaster.CurPageDetail then
    local nowLevel = GameUIMaster.CurPageDetail.now_level_data.level
    local nextLevel = GameUIMaster.CurPageDetail.next_level_data.level
    local askLevel = GameUIMaster.CurPageDetail.next_level_data.ask_level
    local maxLevel = GameUIMaster.CurPageDetail.next_level_data.max_master_level
    if nowLevel == nextLevel and nowLevel == 1 then
      nowLevel = 0
    end
    local popTitle = self:GetCurPopTitle(GameUIMaster.CurPageDetail.type)
    local goalText = self:GetGoalText(GameUIMaster.CurPageDetail.next_level_data.conditions)
    local curProgress = GameUIMaster.CurPageDetail.next_level_data.total_progress.now_num
    local total_progress = GameUIMaster.CurPageDetail.next_level_data.total_progress.total_num
    local equipNum = #GameUIMaster.CurPageDetail.next_level_data.total_progress_detail
    local buffNum = #GameUIMaster.CurPageDetail.next_level_data.list
    local noticeText = GameLoader:GetGameText("LC_MENU_MASTER_UPGRADE_TIP")
    if maxLevel > nowLevel and askLevel > userinfo.level then
      noticeText = string.gsub(GameLoader:GetGameText("LC_MENU_MASTER_PLAYER_LEVEL_LIMIT"), "<Level_num>", askLevel)
    end
    if curProgress == total_progress and nowLevel == maxLevel then
      noticeText = GameLoader:GetGameText("LC_MENU_MAX_CHAR") .. " " .. maxLevel
    end
    self:GetMenuBtnStat()
    self:setBtnTabText()
    self:GetFlashObject():InvokeASCallback("_root", "InitMenu", GameUIMaster.CurrentSystemType, GameUIMaster.CurrentPageType, nowLevel, nextLevel, popTitle, goalText, curProgress, total_progress, equipNum, buffNum, GameUIMaster.statList, noticeText)
    if isShowMoveIn then
      self:ShowMainMenu()
    end
  end
end
function GameUIMaster:UpdateAttrItem(index)
  local leftData = {}
  local rightData = {}
  local nowLevel = GameUIMaster.CurPageDetail.now_level_data.level
  local nextLevel = GameUIMaster.CurPageDetail.next_level_data.level
  if not GameUIMaster.CurPageDetail.now_level_data.list[index] then
    local dupData = {}
    dupData.buff_id = GameUIMaster.CurPageDetail.next_level_data.list[index].buff_id
    dupData.buff_num = 0
    GameUIMaster.CurPageDetail.now_level_data.list[index] = dupData
  end
  local leftBuffId = GameUIMaster.CurPageDetail.now_level_data.list[index].buff_id
  leftData.num = self:GetAttrNum(leftBuffId, GameUIMaster.CurPageDetail.now_level_data.list[index].buff_num)
  if leftBuffId >= 19 and leftBuffId <= 22 then
    leftBuffId = leftBuffId + 2
  end
  leftData.name = GameLoader:GetGameText("LC_MENU_Equip_param_" .. leftBuffId)
  if nowLevel == nextLevel and nowLevel == 1 then
    leftData.num = 0
  end
  rightData.name = leftData.name
  local rightBuffId = GameUIMaster.CurPageDetail.next_level_data.list[index].buff_id
  rightData.num = self:GetAttrNum(rightBuffId, GameUIMaster.CurPageDetail.next_level_data.list[index].buff_num)
  DebugOut("UpdateAttrItem ---- ", index)
  DebugOutPutTable(leftData, "left")
  DebugOutPutTable(rightData, "right")
  self:GetFlashObject():InvokeASCallback("_root", "setAttrData", index, leftData, rightData)
end
function GameUIMaster:UpdateProgressItem(index)
  local itemDetail = {}
  local data = GameUIMaster.CurPageDetail.next_level_data.total_progress_detail[index]
  if data then
    itemDetail.icon = ""
    itemDetail.name = ""
    if GameUIMaster.CurrentSystemType == GameUIMaster.MasterSystem.EquipMaster then
      local equipInfo = self:GetEquipItemInfo(data.aim_id)
      if equipInfo then
        itemDetail.icon = "item_" .. equipInfo.equip_type
        itemDetail.name = GameDataAccessHelper:GetItemNameText(equipInfo.equip_type)
      end
    elseif GameUIMaster.CurrentSystemType == GameUIMaster.MasterSystem.KryptonMaster then
      itemDetail.icon = "item_" .. data.aim_id
      itemDetail.name = GameLoader:GetGameText("LC_ITEM_ITEM_NAME_" .. data.aim_id)
    elseif GameUIMaster.CurrentSystemType == GameUIMaster.MasterSystem.TechnologyMaster then
      itemDetail.icon = "tech_" .. data.aim_id
      itemDetail.name = GameDataAccessHelper:GetTechName(data.aim_id)
    elseif GameUIMaster.CurrentSystemType == GameUIMaster.MasterSystem.FormationMaster then
      if data.aim_id ~= -1 then
        local fleetid = self:GetFleetIdentity(data.aim_id)
        local leaderlist = GameGlobalData:GetData("leaderlist")
        local curMatrixIndex = GameGlobalData.GlobalData.curMatrixIndex
        if fleetid == 1 and leaderlist and curMatrixIndex then
          itemDetail.icon = GameDataAccessHelper:GetFleetAvatar(leaderlist.leader_ids[curMatrixIndex])
          itemDetail.name = GameDataAccessHelper:GetFleetLevelDisplayName(1)
        else
          itemDetail.icon = GameDataAccessHelper:GetFleetAvatar(fleetid)
          itemDetail.name = GameDataAccessHelper:GetFleetLevelDisplayName(fleetid)
        end
      end
    elseif GameUIMaster.CurrentSystemType == GameUIMaster.MasterSystem.ModuleMaster then
      local equip = tcdm:getEquipById(tostring(data.aim_id))
      itemDetail.icon = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(RefineDataManager:EquipToItem(equip.type), nil, nil)
      itemDetail.name = GameLoader:GetGameText("LC_ITEM_ITEM_NAME_" .. equip.type)
    end
    itemDetail.curProgress = data.progress.now_num
    itemDetail.totalProgress = data.progress.total_num
    DebugOut("Lua setProgressData")
    DebugTable(itemDetail)
    self:GetFlashObject():InvokeASCallback("_root", "setProgressData", index, itemDetail)
  end
end
function GameUIMaster:GetEquipItemInfo(aim_id)
  local equipments = GameGlobalData:GetData("equipments")
  local currentEquipInfo, k = GameGlobalData:GetEquipment(tostring(aim_id))
  DebugOut("GetEquipItemInfo", aim_id, k)
  DebugTable(currentEquipInfo)
  return currentEquipInfo
end
function GameUIMaster:GetPageDetail()
  for k, v in ipairs(GameUIMaster.CurPageInfo) do
    if v.type == GameUIMaster.CurrentPageType then
      return v
    end
  end
end
function GameUIMaster:GetFleetIdentity(fleetId)
  local fleetinfo = GameGlobalData.GlobalData.fleetinfo.fleets
  for k, v in pairs(fleetinfo) do
    if v.id == tostring(fleetId) then
      return v.identity
    end
  end
end
function GameUIMaster.OnMasterAchievementNTF(content)
  local isFormationMaster = content.master_ntf_detail[1].master_type == "fleets_augment"
  local isStatePlayerMatrix = GameStateManager:GetCurrentGameState() == GameStatePlayerMatrix
  local isPreStatePlayerMatrix = GameStateManager:GetCurrentGameState().preState == GameStatePlayerMatrix
  local isSelfInState = GameStatePlayerMatrix:IsObjectInState(GameUIMaster)
  DebugTable(content)
  DebugOut("OnMasterAchievementNTF", isFormationMaster, isStatePlayerMatrix, isPreStatePlayerMatrix, isSelfInState)
  local notShow_1 = isFormationMaster and isStatePlayerMatrix and not isSelfInState
  local notShow_2 = isFormationMaster and isPreStatePlayerMatrix and not isSelfInState
  local notShow_3 = GameFleetEquipment.isTeamLeague
  if not notShow_1 and not notShow_2 and not notShow_3 then
    local achieveList = content.master_ntf_detail
    local textItemCnt = #achieveList
    local achievementCnt = #achieveList
    local mergerList = {}
    local textTitleArr = {}
    local textAttrArr = {}
    for k, v in ipairs(achieveList) do
      local textTitle = "<font color='#FFFF00' size='32'>" .. GameUIMaster:GetCurPopTitle(v.master_type) .. " " .. v.level .. "</font>"
      table.insert(textTitleArr, textTitle)
    end
    for k, v in ipairs(achieveList) do
      for i = 1, #v.value do
        if not mergerList[v.value[i].buff_id] then
          mergerList[v.value[i].buff_id] = v.value[i].buff_num
        else
          mergerList[v.value[i].buff_id] = mergerList[v.value[i].buff_id] + v.value[i].buff_num
        end
      end
    end
    DebugOutPutTable(mergerList, "OnMasterAchievementNTF2")
    for k, v in pairs(mergerList) do
      if textItemCnt < 8 then
        local num = GameUIMaster:GetAttrNum(k, tonumber(v))
        if k >= 19 and k <= 22 then
          k = k + 2
        end
        local textAttr = GameHelper:GetFoatColor("0099FF", GameLoader:GetGameText("LC_MENU_Equip_param_" .. k)) .. " " .. GameHelper:GetFoatColor("BFFFFF", num)
        table.insert(textAttrArr, textAttr)
        textItemCnt = textItemCnt + 1
      else
        break
      end
    end
    GameUIMasterAchievementTips:ShowMasterAchievementTips(achievementCnt, textTitleArr, textAttrArr)
  end
end
function GameUIMaster:GetAttrNum(attr, num)
  if attr >= 19 and attr <= 22 or attr >= 9 and attr <= 14 or attr == 17 then
    num = num / 10 .. "%"
  end
  return num
end
function GameUIMaster:GetCurPopTitle(pageType)
  local title = ""
  if pageType == GameUIMaster.MasterPage.equip_enchance then
    title = GameLoader:GetGameText("LC_MENU_MASTER_EQUIP_ENHANCE_MASTER")
  elseif pageType == GameUIMaster.MasterPage.equip_upgrade then
    title = GameLoader:GetGameText("LC_MENU_MASTER_EQUIP_UPGRADE")
  elseif pageType == GameUIMaster.MasterPage.equip_interfere then
    title = GameLoader:GetGameText("LC_MENU_MASTER_EQUIP_INTERFERENCE")
  elseif pageType == GameUIMaster.MasterPage.tech_initial then
    title = GameLoader:GetGameText("LC_MENU_MASTER_TECH_INITIAL")
  elseif pageType == GameUIMaster.MasterPage.tech_advanced then
    title = GameLoader:GetGameText("LC_MENU_MASTER_TECH_ADVANCED")
  elseif pageType == GameUIMaster.MasterPage.tech_hardcore then
    title = GameLoader:GetGameText("LC_MENU_MASTER_TECH_HARDCORE")
  elseif pageType == GameUIMaster.MasterPage.tech_dimension then
    title = GameLoader:GetGameText("LC_MENU_MASTER_TECH_HDIMENSION")
  elseif pageType == GameUIMaster.MasterPage.krypton_enchance then
    title = GameLoader:GetGameText("LC_MENU_MASTER_KRYPTON_LEVEL")
  elseif pageType == GameUIMaster.MasterPage.krypton_grade then
    title = GameLoader:GetGameText("LC_MENU_MASTER_KRYPTON_UPGRADE")
  elseif pageType == GameUIMaster.MasterPage.centrol_adjust then
    title = GameLoader:GetGameText("LC_MENU_MASTER_MODULE_ADJUST")
  elseif pageType == GameUIMaster.MasterPage.centrol_grade then
    title = GameLoader:GetGameText("LC_MENU_MASTER_MODULE_QUALITY")
  elseif pageType == GameUIMaster.MasterPage.fleets_augment then
    title = GameLoader:GetGameText("LC_MENU_MASTER_AUGMENT")
  end
  DebugOut("GetCurPopTitle", pageType, title)
  return title
end
function GameUIMaster:setBtnTabText()
  if GameUIMaster.CurrentSystemType == GameUIMaster.MasterSystem.ModuleMaster then
    self:GetFlashObject():InvokeASCallback("_root", "setLocalText", "tabTitle_1", GameLoader:GetGameText("LC_MENU_MASTER_MODULE_ADJUST"))
    self:GetFlashObject():InvokeASCallback("_root", "setLocalText", "tabTitle_2", GameLoader:GetGameText("LC_MENU_MASTER_MODULE_QUALITY"))
  elseif GameUIMaster.CurrentSystemType == GameUIMaster.MasterSystem.KryptonMaster then
    self:GetFlashObject():InvokeASCallback("_root", "setLocalText", "tabTitle_1", GameLoader:GetGameText("LC_MENU_MASTER_KRYPTON_LEVEL"))
    self:GetFlashObject():InvokeASCallback("_root", "setLocalText", "tabTitle_2", GameLoader:GetGameText("LC_MENU_MASTER_KRYPTON_UPGRADE"))
  elseif GameUIMaster.CurrentSystemType == GameUIMaster.MasterSystem.EquipMaster then
    DebugOut("ahsdhasjd:", GameLoader:GetGameText("LC_MENU_MASTER_EQUIP_UPGRADE"))
    DebugOut("ahsdhasjd 2:", GameLoader:GetGameText("LC_MENU_MASTER_EQUIP_ENHANCE"))
    DebugOut("ahsdhasjd 3:", GameLoader:GetGameText("LC_MENU_MASTER_EQUIP_INTERFERENCE"))
    self:GetFlashObject():InvokeASCallback("_root", "setLocalText", "tabTitle_1", GameLoader:GetGameText("LC_MENU_MASTER_EQUIP_UPGRADE"))
    self:GetFlashObject():InvokeASCallback("_root", "setLocalText", "tabTitle_3", GameLoader:GetGameText("LC_MENU_MASTER_EQUIP_ENHANCE"))
    self:GetFlashObject():InvokeASCallback("_root", "setLocalText", "tabTitle_2", GameLoader:GetGameText("LC_MENU_MASTER_EQUIP_INTERFERENCE"))
  end
end
function GameUIMaster:GetGoalText(conditions)
  local goalText = ""
  if #conditions[1].ask == 0 then
    goalText = GameLoader:GetGameText("LC_MENU_MASTER_" .. conditions[1].condition_name)
  elseif #conditions[1].ask == 1 then
    goalText = string.gsub(GameLoader:GetGameText("LC_MENU_MASTER_" .. conditions[1].condition_name), "<number1>", tostring(conditions[1].ask[1]))
  elseif #conditions[1].ask == 2 then
    goalText = string.gsub(GameLoader:GetGameText("LC_MENU_MASTER_" .. conditions[1].condition_name), "<number1>", tostring(conditions[1].ask[1]))
    if GameUIMaster.CurrentPageType == GameUIMaster.MasterPage.centrol_grade then
      goalText = string.gsub(goalText, "<quality>", tostring(conditions[1].ask[2]))
    else
      goalText = string.gsub(goalText, "<number2>", tostring(conditions[1].ask[2]))
    end
  end
  return goalText
end
if AutoUpdate.isAndroidDevice then
  function GameUIMaster.OnAndroidBack()
    if GameStateManager:GetCurrentGameState():IsObjectInState(GameUIMaster) then
      GameUIMaster:GetFlashObject():InvokeASCallback("_root", "MoveOut")
    end
  end
end
