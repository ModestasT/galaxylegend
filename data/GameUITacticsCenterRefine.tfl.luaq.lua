local GameStateTacticsCenter = LuaObjectManager:GetLuaObject("GameStateTacticsCenter")
local GameObjectTacticsCenter = LuaObjectManager:GetLuaObject("GameObjectTacticsCenter")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local RefineDataManager = LuaObjectManager:GetLuaObject("RefineDataManager")
require("TacticsCenterDataManager.tfl")
local TacticsCenterDataManager = TacticsCenterDataManager.TacticsCenterDataManager
local QuestTutorialTacticsRefine = TutorialQuestManager.QuestTutorialTacticsRefine
local GameUICommonDialog = LuaObjectManager:GetLuaObject("GameUICommonDialog")
local GameUIWDStuff = LuaObjectManager:GetLuaObject("GameUIWDStuff")
GameUITacticsCenterRefine = {}
GameUITacticsCenterRefine.lockState = {
  none = 0,
  unlock = 1,
  lock = 2,
  nolock = 3
}
GameUITacticsCenterRefine.lockFlag = {
  canlock = 1,
  atLeastAttr = 2,
  vipDiscontent = 3,
  moreAttrs = 4
}
GameUITacticsCenterRefine.refreshState = {
  value = 1,
  addValue = 2,
  commixture = 3
}
GameUITacticsCenterRefine.attrCount = 5
GameUITacticsCenterRefine.newEquipAttrs = {}
GameUITacticsCenterRefine.tacticsResults = {}
GameUITacticsCenterRefine.currentRefreshState = GameUITacticsCenterRefine.refreshState.commixture
GameUITacticsCenterRefine.showAllAttrsTable = {}
GameUITacticsCenterRefine.showAllAttrsColor = {}
GameUITacticsCenterRefine.currentChoseAttrid = -1
GameUITacticsCenterRefine.currentTimes = 0
GameUITacticsCenterRefine.time = 0
function _equals(a, b)
  return a == b
end
function GameUITacticsCenterRefine:AddRefineToTacticsCenter(vessel, flashObject, equip)
  self.m_flashobject = flashObject
  self.vessel = vessel
  self:SetEquipData(equip)
  self:GetRefineData()
end
function GameUITacticsCenterRefine:Show(...)
  self:Init()
  self:_setEquipDateStringToAs()
  self:_setRefineCountToAs()
  self:_RefreshTacticsResultsLayer(-1)
  self:_refreshAttrLock()
  self:TutorialCheckRefine()
  GameUITacticsCenterRefine:InitStateText()
  self:MoveIn()
end
function GameUITacticsCenterRefine:UpdateLayer(...)
  self:_setEquipDateStringToAs()
  if self.currentSelectAttr then
    self:_RefreshTacticsResultsLayer(self.currentSelectAttr)
  else
    self:_RefreshTacticsResultsLayer(-1)
  end
  self:_refreshAttrLock()
end
function GameUITacticsCenterRefine:TutorialCheckRefine(...)
  self.m_flashobject:InvokeASCallback("_root", "refine_hideTutorial")
  if QuestTutorialTacticsRefine:IsActive() then
    DebugOut("refine_showTutorialRefine:")
    DebugOut("true")
    self.m_flashobject:InvokeASCallback("_root", "refine_showTutorialRefine")
  else
    DebugOut("refine_showTutorialRefine:")
    DebugOut("false")
    self.m_flashobject:InvokeASCallback("_root", "refine_hideTutorialRefine")
  end
end
function GameUITacticsCenterRefine:TutorialCheckItem(...)
  self.m_flashobject:InvokeASCallback("_root", "refine_hideTutorial")
  if QuestTutorialTacticsRefine:IsActive() then
    local function callback(...)
      self.m_flashobject:InvokeASCallback("_root", "refine_showTutorialItem")
    end
    GameUICommonDialog:PlayStory({1100090}, callback)
  else
    self.m_flashobject:InvokeASCallback("_root", "refine_hideTutorialItem")
  end
end
function GameUITacticsCenterRefine:TutorialCheckSave(...)
  self.m_flashobject:InvokeASCallback("_root", "refine_hideTutorial")
  if QuestTutorialTacticsRefine:IsActive() then
    self.m_flashobject:InvokeASCallback("_root", "refine_showTutorialSave")
  else
    self.m_flashobject:InvokeASCallback("_root", "refine_hideTutorialSave")
  end
end
function GameUITacticsCenterRefine:TutorialCheck(...)
  if QuestTutorialTacticsRefine:IsActive() then
    local function callback(...)
      QuestTutorialTacticsRefine:SetFinish(true)
      GameUITacticsCenterRefine:TutorialCheckSave()
    end
    GameUICommonDialog:PlayStory({1100091}, callback)
  end
end
function GameUITacticsCenterRefine:GetRefineData(...)
  local param = {
    equip_id = self.m_equipData.id
  }
  NetMessageMgr:SendMsg(NetAPIList.tactical_enter_refine_req.Code, param, GameUITacticsCenterRefine._netCallback, true, nil)
end
function GameUITacticsCenterRefine:InitStateText(...)
  local name = GameLoader:GetGameText("LC_MENU_TACTICAL_CENTER_CHARACTER_CHAR")
  local max = GameLoader:GetGameText("LC_MENU_TACTICAL_CENTER_MAX_CHAR")
  local min = GameLoader:GetGameText("LC_MENU_TACTICAL_CENTER_NOW_CHAR")
  local refine1Text = GameLoader:GetGameText("LC_MENU_TACTICAL_CENTER_PANEL_1")
  local refine5Text = GameLoader:GetGameText("LC_MENU_TACTICAL_CENTER_PANEL_2")
  local saveText = GameLoader:GetGameText("LC_MENU_TACTICAL_CENTER_SAVE")
  if self.m_flashobject then
    self.m_flashobject:InvokeASCallback("_root", "refine_InitSaticText", name, max, min, refine1Text, refine5Text, saveText)
  end
end
function GameUITacticsCenterRefine:Init()
  self.currentTimes = 0
  GameUITacticsCenterRefine.currentChoseAttrid = -1
  GameUITacticsCenterRefine.currentRefreshState = GameUITacticsCenterRefine.refreshState.commixture
  GameUITacticsCenterRefine.showAllAttrsTable = {}
  GameUITacticsCenterRefine.showAllAttrsColor = {}
  GameUITacticsCenterRefine.tacticsResults = RefineDataManager:GetLastRefine()
  self.last_lockNum = 0
end
function GameUITacticsCenterRefine:SetEquipData(equipData)
  self.m_equipData = equipData
  DebugOut("GameUITacticsCenterRefine:m_equipData:")
  DebugTable(self.m_equipData)
  GameUITacticsCenterRefine.newEquipAttrs = self:_getNewEquipAttrsList()
end
function GameUITacticsCenterRefine:_getNewEquipAttrsList()
  local equipAllAttrs = self.m_equipData.possible_attrs
  local currentAttrs = self.m_equipData.equip_attrs
  local newEquipAttrsList = {}
  if #currentAttrs > 1 then
    for _, v in ipairs(equipAllAttrs) do
      local flag = false
      for _, t in ipairs(currentAttrs) do
        if v.id == t.id then
          flag = true
          break
        end
      end
      local __new = {}
      __new.id = v.id
      __new.max = v.max
      if flag == false then
        __new.lock_state = GameUITacticsCenterRefine.lockState.none
      else
        __new.lock_state = GameUITacticsCenterRefine.lockState.unlock
      end
      table.insert(newEquipAttrsList, __new)
    end
  else
    for _, v in ipairs(equipAllAttrs) do
      local flag = false
      for _, t in ipairs(currentAttrs) do
        if v.id == t.id then
          flag = true
          break
        end
      end
      local __new = {}
      if flag == false then
        __new.id = -1
        __new.max = -1
        __new.lock_state = GameUITacticsCenterRefine.lockState.none
      else
        __new.id = v.id
        __new.max = v.max
        __new.lock_state = GameUITacticsCenterRefine.lockState.nolock
      end
      table.insert(newEquipAttrsList, __new)
    end
  end
  DebugOut("newEquipAttrsList:")
  DebugTable(newEquipAttrsList)
  return newEquipAttrsList
end
function GameUITacticsCenterRefine:_getEquipAttrFromRefineResults(id)
  for _, v in ipairs(self.tacticsResults) do
    if v.refine_id == id then
      return v.equip_attrs
    end
  end
  return nil
end
function GameUITacticsCenterRefine.RefreshGlobalData()
  GameUITacticsCenterRefine:_setRefineCountToAs()
end
function GameUITacticsCenterRefine:_setRefineCountToAs(...)
  if self.m_flashobject == nil then
    return
  end
  local tacticsCountText = ""
  if RefineDataManager:GetUsedTimes() > 0 then
    tacticsCountText = GameLoader:GetGameText("LC_MENU_TACTICAL_CENTER_FREE_TIME_CHAR") .. RefineDataManager:GetUsedTimes() .. "/" .. RefineDataManager:GetMaxTimes()
  else
    tacticsCountText = GameUtils:formatTimeString(RefineDataManager:GetNextRefreshTime())
  end
  local need_item = RefineDataManager:GetNeedItem()
  DebugTable(need_item)
  local itemIcon = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(need_item, nil, nil)
  local count = GameGlobalData:GetData("resource")
  local itemCount = 0
  if GameHelper:IsResource(need_item.item_type) then
    itemCount = GameUtils.numberConversion(count[need_item.item_type])
  else
    itemCount = GameGlobalData:GetItemCount(need_item.number)
  end
  local cost_item = RefineDataManager:GetUsedCount()
  local creditIcon = GameHelper:GetAwardTypeIconFrameName("credit", "", "")
  local creditCount = GameUtils.numberConversion(count.credit)
  local lockitem = RefineDataManager.item_lock
  local lockIcon = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(lockitem)
  local lockNum = GameGlobalData:GetItemCount(lockitem.number)
  lockNum = GameUtils.numberConversion(lockNum)
  if self.m_flashobject then
    self.m_flashobject:InvokeASCallback("_root", "refine_ResourcePanel", tacticsCountText, itemIcon, itemCount, creditIcon, creditCount, cost_item, lockIcon, lockNum)
  end
end
function GameUITacticsCenterRefine:_updateRefineCountToAsTime()
  if self.m_flashobject == nil then
    return
  end
  local tacticsCountText = ""
  if RefineDataManager:GetUsedTimes() > 0 then
    tacticsCountText = GameLoader:GetGameText("LC_MENU_TACTICAL_CENTER_FREE_TIME_CHAR") .. RefineDataManager:GetUsedTimes() .. "/" .. RefineDataManager:GetMaxTimes()
  else
    tacticsCountText = GameUtils:formatTimeString(RefineDataManager:GetNextRefreshTime())
  end
  if self.m_flashobject then
    self.m_flashobject:InvokeASCallback("_root", "refine_updateTime", tacticsCountText)
  end
end
function GameUITacticsCenterRefine:_setEquipDateStringToAs(...)
  if self.m_equipData then
    local equipName = GameLoader:GetGameText("LC_ITEM_ITEM_NAME_" .. self.m_equipData.type)
    local equipIcon = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(RefineDataManager:EquipToItem(self.m_equipData.type), nil, nil)
    local equipDec = GameLoader:GetGameText("LC_ITEM_ITEM_DESC_" .. self.m_equipData.type)
    DebugOut("equipName:" .. equipName .. " equipIcon : " .. equipIcon .. " equipDec:" .. equipDec)
    local attrsNames = ""
    local attrsMax = ""
    attrsNames = self:_getNewEquipAttrsListNameText(GameUITacticsCenterRefine.newEquipAttrs)
    attrsMax = self:_getNewEquipAttrsListMaxText(GameUITacticsCenterRefine.newEquipAttrs)
    local attrsId, attrRank, attrNow = GameUITacticsCenterRefine:_getEquipAttrsListText(GameUITacticsCenterRefine.newEquipAttrs)
    if self.m_flashobject then
      self.m_flashobject:InvokeASCallback("_root", "refine_SetData", equipName, equipIcon, equipDec, attrsNames, attrsId, attrRank, attrNow, attrsMax)
    end
  end
end
function GameUITacticsCenterRefine.donamicDownloadFinishCallback()
  if GameUITacticsCenterRefine.m_flashobject == nil then
    return
  end
  GameUITacticsCenterRefine:_setEquipDateStringToAs()
end
function GameUITacticsCenterRefine._sortAttr(v1, v2)
  if v1.id == v2.id then
    return v1.id < v2.id
  else
    return v1.type < v2.type
  end
end
function GameUITacticsCenterRefine:_getNewEquipAttrsListNameText(newEquipAttrs)
  local nameText = ""
  for _, v in ipairs(newEquipAttrs) do
    if v.id == -1 then
      nameText = nameText .. "empty" .. "\001"
    else
      nameText = nameText .. GameLoader:GetGameText("LC_MENU_Equip_param_" .. v.id) .. "\001"
    end
  end
  return nameText
end
function GameUITacticsCenterRefine:_getNewEquipAttrsListMaxText(newEquipAttrs)
  local maxText = ""
  for _, v in ipairs(newEquipAttrs) do
    if v.max == -1 then
      maxText = maxText .. "empty" .. "\001"
    else
      maxText = maxText .. GameObjectTacticsCenter:_convertToPercentStr(v.max) .. "\001"
    end
  end
  return maxText
end
function GameUITacticsCenterRefine:_getEquipAttrsListText(newEquipAttrs)
  local attrsId = ""
  local attrRank = ""
  local attrNow = ""
  for _, v in ipairs(newEquipAttrs) do
    local flag = false
    for _, m in ipairs(self.m_equipData.equip_attrs) do
      if v.id == m.id then
        flag = true
      end
    end
    if flag then
      for _, t in ipairs(self.m_equipData.equip_attrs) do
        if v.id == t.id then
          attrsId = attrsId .. v.id .. "\001"
          for _, m in ipairs(t.effects) do
            if m.effect_vessel == self.m_equipData.vessel then
              attrNow = attrNow .. GameObjectTacticsCenter:_convertToPercentStr(m.value) .. "\001"
              attrRank = attrRank .. t.color .. "\001"
            end
          end
        end
      end
    else
      attrsId = attrsId .. "empty" .. "\001"
      attrNow = attrNow .. "empty" .. "\001"
      attrRank = attrRank .. "empty" .. "\001"
    end
  end
  DebugOut("attrsId:" .. attrsId .. " attrRank:" .. attrRank .. " attrNow:" .. attrNow)
  return attrsId, attrRank, attrNow
end
function GameUITacticsCenterRefine:_refreshAttrLock()
  local lockState = ""
  for _, v in ipairs(GameUITacticsCenterRefine.newEquipAttrs) do
    lockState = lockState .. v.lock_state .. "\001"
  end
  DebugOut("lockState:" .. lockState)
  if self.m_flashobject then
    self.m_flashobject:InvokeASCallback("_root", "refine_refreshLockState", lockState)
  end
end
function GameUITacticsCenterRefine:InitLayer()
  if self.m_flashobject then
    self.m_flashobject:InvokeASCallback("_root", "InitTacticsCenterRefineLayer")
  end
end
function GameUITacticsCenterRefine:MoveIn()
  if self.m_flashobject then
    self.m_flashobject:InvokeASCallback("_root", "refine_Movein")
  end
end
function GameUITacticsCenterRefine:MoveOut(...)
  if self.m_flashobject then
    self.m_flashobject:InvokeASCallback("_root", "refine_Moveout")
  end
end
function GameUITacticsCenterRefine:Update(dt)
  self:_UpdateRefreshTime(dt)
end
function GameUITacticsCenterRefine:_UpdateRefreshTime(dt)
  if self.m_flashobject then
    self.time = self.time + dt
    if self.time > 1000 then
      self.time = 0
      if 0 >= RefineDataManager:GetUsedTimes() then
        local next_time = RefineDataManager:GetNextRefreshTime()
        next_time = next_time - 1
        if next_time < 0 then
          next_time = 0
        end
        RefineDataManager:SetNextRefreshTime(next_time)
        self:_updateRefineCountToAsTime()
      end
    end
  end
end
function GameUITacticsCenterRefine:OnFSCommand(cmd, arg)
  DebugOut("cmd:" .. cmd .. " arg:" .. arg)
  if cmd == "lock_attr" then
    GameUITacticsCenterRefine:_lockAttr(tonumber(arg))
  elseif cmd == "select_attr" then
    self.currentSelectAttr = tonumber(arg)
    self:_RefreshTacticsResultsLayer(tonumber(arg))
    self:TutorialCheckSave()
    self:_setChoseAttrs(tonumber(arg))
  elseif cmd == "refresh_tacticspanel" then
    self:_refreshStateChange()
  elseif cmd == "save_attr" then
    if self.currentChoseAttrid == -1 then
    else
      self:_saveAttrs()
    end
  elseif cmd == "refine_one" then
    self.currentTimes = tonumber(arg)
    self:_refineGetPrice(self.currentTimes)
  elseif cmd == "close_refine" then
    self:MoveOut()
    RefineDataManager:RemoveAllNtfMesgHandler()
    GameObjectTacticsCenter:TutorialCheckRefinebutton()
    TacticsCenterDataManager:setShowState(TacticsCenterDataManager.ShowLayerState.tacticsCenter)
  elseif cmd == "refine_help" then
    GameUIWDStuff:Show(GameUIWDStuff.menuType.menu_type_help)
    GameUIWDStuff:SetHelpText(GameLoader:GetGameText("LC_MENU_TACTICAL_CENTER_HELP_2"))
  end
end
function GameUITacticsCenterRefine:_lockAttr(attrId)
  local currentAttr = self.newEquipAttrs[attrId + 1]
  DebugOut("currentAttr:")
  DebugTable(currentAttr)
  if currentAttr.lock_state == self.lockState.unlock then
    currentAttr.lock_state = self.lockState.lock
  elseif currentAttr.lock_state == self.lockState.lock then
    currentAttr.lock_state = self.lockState.unlock
  elseif currentAttr.lock_state == self.lockState.nolock then
  elseif currentAttr.lock_state == self.lockState.none then
  end
  local flag = self:_checkLockState()
  self:_setLockState(flag, attrId)
  self:_refreshAttrLock()
end
function GameUITacticsCenterRefine:_refreshStateChange()
  self.currentRefreshState = self.currentRefreshState + 1
  if self.currentRefreshState > 3 then
    self.currentRefreshState = self.refreshState.value
  end
  if self.currentSelectAttr then
    self:_RefreshTacticsResultsLayer(self.currentSelectAttr)
  else
    self:_RefreshTacticsResultsLayer(-1)
  end
  local tipText = ""
  if self.currentRefreshState == self.refreshState.value then
    tipText = GameLoader:GetGameText("LC_MENU_TACTICAL_CENTER_INFO_11")
  elseif self.currentRefreshState == self.refreshState.addValue then
    tipText = GameLoader:GetGameText("LC_MENU_TACTICAL_CENTER_INFO_13")
  elseif self.currentRefreshState == self.refreshState.commixture then
    tipText = GameLoader:GetGameText("LC_MENU_TACTICAL_CENTER_INFO_12")
  end
  GameTip:Show(tipText)
end
function GameUITacticsCenterRefine:_checkLockState()
  local flag = 0
  local lockNum = 0
  for _, v in ipairs(self.newEquipAttrs) do
    if v.lock_state == self.lockState.lock then
      lockNum = lockNum + 1
    end
  end
  if lockNum > #self.m_equipData.equip_attrs - 1 then
    flag = GameUITacticsCenterRefine.lockFlag.moreAttrs
    DebugOut("error")
  elseif lockNum <= RefineDataManager:GetLockNum() then
    local lockNum = #self:_getLockAttrList()
    if lockNum > self.last_lockNum then
      local item_lock = RefineDataManager.item_lock
      local needitem = RefineDataManager:GetLockAttrPrice_item(lockNum)
      local haveItem = GameGlobalData:GetItemCount(item_lock.number)
      local lockPriceText
      if needitem <= haveItem then
        lockPriceText = GameLoader:GetGameText("LC_MENU_TACTICAL_CENTER_INFO_19")
        lockPriceText = string.gsub(lockPriceText, "<attribute_num>", tostring(lockNum))
        lockPriceText = string.gsub(lockPriceText, "<item_type>", GameHelper:GetAwardTypeText(item_lock.item_type, item_lock.number))
        lockPriceText = string.gsub(lockPriceText, "<credits_num>", needitem)
      else
        local lockitem_n = 0
        local costitem_n = 0
        costitem_n = RefineDataManager:GetLockAttrPrice_item(lockNum)
        if haveItem == 0 then
          lockPriceText = GameLoader:GetGameText("LC_MENU_TACTICAL_CENTER_INFO_1")
          lockPriceText = string.gsub(lockPriceText, "<attribute_num>", tostring(lockNum))
          lockPriceText = string.gsub(lockPriceText, "<credits_num>", tostring(RefineDataManager:GetLockAttrPrice(lockNum)))
        else
          lockPriceText = GameUtils:TryGetText("LC_MENU_HEJIN_COST2", "lock <num> props, need item <item_type> <item1>, need credit <item2>")
          lockPriceText = string.gsub(lockPriceText, "<num>", tostring(lockNum))
          lockPriceText = string.gsub(lockPriceText, "<item_type>", GameHelper:GetAwardTypeText(item_lock.item_type, item_lock.number))
          lockPriceText = string.gsub(lockPriceText, "<item1>", tostring(haveItem))
          local costCredit = RefineDataManager:GetLockAttrPrice(1) * (costitem_n - haveItem)
          lockPriceText = string.gsub(lockPriceText, "<item2>", tostring(costCredit))
        end
      end
      GameTip:Show(lockPriceText)
    end
    self.last_lockNum = lockNum
    if lockNum < #self.m_equipData.equip_attrs - 1 and lockNum < RefineDataManager:GetLockMax() then
      flag = GameUITacticsCenterRefine.lockFlag.canlock
    else
      flag = GameUITacticsCenterRefine.lockFlag.atLeastAttr
    end
  elseif lockNum <= RefineDataManager:GetLockMax() then
    local codition = RefineDataManager:GetUnlockCondition()
    local coditionText = ""
    if 0 < codition.level and 0 < codition.vip then
      coditionText = GameLoader:GetGameText("LC_MENU_TACTICAL_CENTER_INFO_16")
      coditionText = string.gsub(coditionText, "<playerLevel_num>", tostring(codition.level))
      coditionText = string.gsub(coditionText, "<vipLevel_num>", tostring(codition.vip))
    elseif 0 < codition.level and codition.vip == 0 then
      coditionText = string.format(GameLoader:GetGameText("LC_MENU_TACTICAL_CENTER_INFO_14"), tostring(codition.level))
    elseif codition.level == 0 and 0 < codition.vip then
      coditionText = string.format(GameLoader:GetGameText("LC_MENU_TACTICAL_CENTER_INFO_15"), tostring(codition.vip))
    end
    GameTip:Show(coditionText)
    flag = GameUITacticsCenterRefine.lockFlag.vipDiscontent
  end
  return flag
end
function GameUITacticsCenterRefine:_setLockState(flag, attrId)
  DebugOut("_setLockState:" .. flag)
  DebugTable(self.newEquipAttrs)
  if flag == GameUITacticsCenterRefine.lockFlag.canlock then
    for _, v in ipairs(GameUITacticsCenterRefine.newEquipAttrs) do
      if v.lock_state == self.lockState.nolock then
        v.lock_state = self.lockState.unlock
      end
    end
  elseif flag == 2 then
    for _, v in ipairs(GameUITacticsCenterRefine.newEquipAttrs) do
      for _, t in ipairs(self.m_equipData.equip_attrs) do
        if v.id == t.id and v.lock_state == self.lockState.unlock then
          v.lock_state = self.lockState.nolock
        end
      end
    end
  elseif flag == 3 or flag == 4 then
    GameUITacticsCenterRefine.newEquipAttrs[attrId + 1].lock_state = self.lockState.unlock
  end
end
function GameUITacticsCenterRefine:_getShowAttrbyAttrId(id, attrs)
  local showAttrTable = {}
  for _, v in ipairs(attrs) do
    if v.id == id then
      for _, t in ipairs(v.effects) do
        if t.effect_vessel == self.m_equipData.vessel then
          showAttrTable = t
        end
      end
    end
  end
  DebugOut("_getShowAttrbyAttrId:")
  DebugTable(showAttrTable)
  return showAttrTable
end
function GameUITacticsCenterRefine:_getLockAttrList(...)
  local lockTable = {}
  for _, v in ipairs(self.newEquipAttrs) do
    if v.lock_state == self.lockState.lock then
      table.insert(lockTable, v.id)
    end
  end
  return lockTable
end
function GameUITacticsCenterRefine:_refineGetPrice(times)
  local lockTable = self:_getLockAttrList()
  local param = {
    equip_id = self.m_equipData.id,
    times = times,
    locked_attrs = lockTable
  }
  DebugTable(param)
  NetMessageMgr:SendMsg(NetAPIList.tactical_refine_price_req.Code, param, GameUITacticsCenterRefine._TacticalRefineCallback, true, nil)
end
function GameUITacticsCenterRefine:_tacticalRefine(times)
  local lockTable = self:_getLockAttrList()
  local para = {
    equip_id = self.m_equipData.id,
    times = times,
    locked_attrs = lockTable
  }
  NetMessageMgr:SendMsg(NetAPIList.tactical_refine_req.Code, para, GameUITacticsCenterRefine._TacticalRefineCallback, true, nil)
end
function GameUITacticsCenterRefine._TacticalRefineCallback(msgtype, content)
  if msgtype == NetAPIList.tactical_refine_ack.Code then
    DebugOut("_TacticalRefineCallback:")
    DebugTable(content)
    if content.code ~= 0 then
      local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    if content.code == 0 then
      GameUITacticsCenterRefine.tacticsResults = content.results
      RefineDataManager:SetUsedCount(content.cost_item)
      GameUITacticsCenterRefine:_setRefineCountToAs()
      GameUITacticsCenterRefine:_RefreshTacticsResultsLayer(-1)
      GameUITacticsCenterRefine:TutorialCheckItem()
    end
    return true
  end
  if msgtype == NetAPIList.tactical_refine_price_ack.Code then
    DebugOut("tactical_refine_price_ack:")
    DebugOut(content)
    if content.code ~= 0 then
      local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    GameUITacticsCenterRefine:_RefinePriceShowDialog(content)
    return true
  end
  return false
end
function GameUITacticsCenterRefine:_createYellowText(text)
  return "<font color='#FFC926'>" .. text .. "</font>"
end
function GameUITacticsCenterRefine:_RefinePriceShowDialog(content)
  local have_lock = content.lock_price > 0 or 0 < content.lock_cost
  if 0 < content.refine_price or have_lock then
    local lockdesc = ""
    if content.lock_price > 0 then
      lockdesc = string.format(GameLoader:GetGameText("LC_MENU_TACTICAL_CENTER_INFO_5"), self:_createYellowText(tostring(content.lock_price)))
    elseif 0 < content.lock_cost then
      local item_lock = RefineDataManager.item_lock
      local txt = string.format("%s %s", self:_createYellowText(tostring(content.lock_cost)), GameHelper:GetAwardTypeText(item_lock.item_type, item_lock.number))
      lockdesc = string.format(GameLoader:GetGameText("LC_MENU_TACTICAL_CENTER_INFO_18"), txt)
    end
    local textInfo = ""
    if 0 < content.refine_price and have_lock then
      textInfo = string.format(GameLoader:GetGameText("LC_MENU_TACTICAL_CENTER_INFO_4"), self:_createYellowText(tostring(content.refine_price))) .. "\n" .. lockdesc
    elseif 0 < content.refine_price and not have_lock then
      textInfo = string.format(GameLoader:GetGameText("LC_MENU_TACTICAL_CENTER_INFO_4"), self:_createYellowText(tostring(content.refine_price)))
    elseif content.refine_price == 0 and have_lock then
      textInfo = lockdesc
    end
    local callback = function(...)
      GameUITacticsCenterRefine:_tacticalRefine(GameUITacticsCenterRefine.currentTimes)
    end
    GameUtils:CreditCostConfirm(textInfo, callback, false, nil)
  else
    GameUITacticsCenterRefine:_tacticalRefine(GameUITacticsCenterRefine.currentTimes)
  end
end
function GameUITacticsCenterRefine:_RefreshTacticsResultsLayer(index)
  if GameUITacticsCenterRefine.m_flashobject then
    GameUITacticsCenterRefine.m_flashobject:InvokeASCallback("_root", "refine_InitTacticPanel")
  end
  if GameUITacticsCenterRefine.m_flashobject then
    GameUITacticsCenterRefine.m_flashobject:InvokeASCallback("_root", "refine_setButtonTihuanState", false)
  end
  if GameUITacticsCenterRefine.tacticsResults == nil or #GameUITacticsCenterRefine.tacticsResults < 1 then
    return
  end
  GameUITacticsCenterRefine.showAllAttrsTable = {}
  GameUITacticsCenterRefine.showAllAttrsColor = {}
  for _, r in ipairs(GameUITacticsCenterRefine.tacticsResults) do
    local showAttrTable = {}
    local showAttrColor = {}
    for _, v in ipairs(GameUITacticsCenterRefine.newEquipAttrs) do
      local isHave = false
      local oldValue = 0
      local newValue = 0
      local showAttrText = ""
      local color = ""
      for _, m in ipairs(r.equip_attrs) do
        if v.id == m.id then
          if v.lock_state ~= GameUITacticsCenterRefine.lockState.none then
            oldValue = GameUITacticsCenterRefine:_getShowAttrbyAttrId(v.id, GameUITacticsCenterRefine.m_equipData.equip_attrs).value
            newValue = GameUITacticsCenterRefine:_getShowAttrbyAttrId(m.id, r.equip_attrs).value
          else
            oldValue = ""
            newValue = GameUITacticsCenterRefine:_getShowAttrbyAttrId(m.id, r.equip_attrs).value
          end
          color = m.color
          isHave = true
        end
      end
      if isHave == false then
        showAttrText = "empty"
        color = "empty"
      elseif oldValue == "" then
        showAttrText = GameUITacticsCenterRefine:_getNewattrValue(newValue, true)
      elseif GameUITacticsCenterRefine.currentRefreshState == GameUITacticsCenterRefine.refreshState.commixture then
        showAttrText = GameUITacticsCenterRefine:_getCommixtureValue(oldValue, newValue, true)
      elseif GameUITacticsCenterRefine.currentRefreshState == GameUITacticsCenterRefine.refreshState.value then
        showAttrText = GameUITacticsCenterRefine:_getNewattrValue(newValue, true)
      elseif GameUITacticsCenterRefine.currentRefreshState == GameUITacticsCenterRefine.refreshState.addValue then
        showAttrText = GameUITacticsCenterRefine:_getAddattrValue(oldValue, newValue, true)
      end
      DebugOut("showAttrText:" .. showAttrText)
      table.insert(showAttrTable, showAttrText)
      table.insert(showAttrColor, color)
    end
    table.insert(GameUITacticsCenterRefine.showAllAttrsTable, showAttrTable)
    table.insert(GameUITacticsCenterRefine.showAllAttrsColor, showAttrColor)
  end
  DebugOut("GameUITacticsCenterRefine.showAllAttrsTable:")
  DebugTable(GameUITacticsCenterRefine.showAllAttrsTable)
  DebugTable(GameUITacticsCenterRefine.showAllAttrsColor)
  local tacticsCount = #self.showAllAttrsTable
  for i = 1, tacticsCount do
    local as_AttrsText = ""
    local as_AttrsColor = ""
    for _, v in ipairs(self.showAllAttrsTable[i]) do
      as_AttrsText = as_AttrsText .. v .. "\001"
    end
    for _, t in ipairs(self.showAllAttrsColor[i]) do
      as_AttrsColor = as_AttrsColor .. t .. "\001"
    end
    if GameUITacticsCenterRefine.m_flashobject then
      if index == i - 1 then
        GameUITacticsCenterRefine.m_flashobject:InvokeASCallback("_root", "refine_SetTacticPanelData", as_AttrsText, as_AttrsColor, i, true)
        GameUITacticsCenterRefine.m_flashobject:InvokeASCallback("_root", "refine_setButtonTihuanState", true)
      else
        GameUITacticsCenterRefine.m_flashobject:InvokeASCallback("_root", "refine_SetTacticPanelData", as_AttrsText, as_AttrsColor, i, false)
      end
    end
  end
end
function GameUITacticsCenterRefine:_setChoseAttrs(index)
  self.currentChoseAttrid = self.tacticsResults[index + 1].refine_id
end
function GameUITacticsCenterRefine:_getNewattrValue(newValue, isPercent)
  local newValuaTable = ""
  local newValuaText = "" .. newValue
  if isPercent then
    newValuaText = GameObjectTacticsCenter:_convertToPercentStr(newValue)
  end
  newValuaTable = GameObjectTacticsCenter:_createBlueText(newValuaText)
  return newValuaTable
end
function GameUITacticsCenterRefine:_getAddattrValue(oldValue, newValue, isPercent)
  local newValuaTable = {}
  local newValuaText = ""
  if not _equals(oldValue, newValue) then
    if oldValue < newValue then
      local _add = newValue - oldValue
      newValuaText = "" .. _add
      if isPercent then
        newValuaText = GameObjectTacticsCenter:_convertToPercentStr(_add)
      end
      table.insert(newValuaTable, GameObjectTacticsCenter:_createGreenText("+"))
      table.insert(newValuaTable, GameObjectTacticsCenter:_createGreenText(newValuaText))
    else
      local _cut = oldValue - newValue
      newValuaText = "" .. _cut
      if isPercent then
        newValuaText = GameObjectTacticsCenter:_convertToPercentStr(_cut)
      end
      table.insert(newValuaTable, GameObjectTacticsCenter:_createRedText("-"))
      table.insert(newValuaTable, GameObjectTacticsCenter:_createRedText(newValuaText))
    end
  else
    if isPercent then
      newValuaText = GameObjectTacticsCenter:_convertToPercentStr(0)
    end
    table.insert(newValuaTable, GameObjectTacticsCenter:_createBlueText(newValuaText))
  end
  return ...
end
function GameUITacticsCenterRefine:_getCommixtureValue(oldValue, newValue, isPercent)
  return ...
end
function GameUITacticsCenterRefine:_get(...)
end
function GameUITacticsCenterRefine:_saveAttrs()
  local param = {
    equip_id = self.m_equipData.id,
    refine_id = self.currentChoseAttrid
  }
  NetMessageMgr:SendMsg(NetAPIList.tactical_refine_save_req.Code, param, GameUITacticsCenterRefine._netCallback, true, nil)
end
function GameUITacticsCenterRefine._netCallback(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.tactical_refine_save_req.Code then
    if content.code ~= 0 then
      local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    DebugOut("UpdateLayer:")
    local new_equipattrs = GameUITacticsCenterRefine:_getEquipAttrFromRefineResults(GameUITacticsCenterRefine.currentChoseAttrid)
    DebugTable(new_equipattrs)
    if new_equipattrs then
      DebugOut("UpdateLayer:")
      DebugTable(GameUITacticsCenterRefine.m_equipData)
      GameUITacticsCenterRefine.m_equipData.equip_attrs = new_equipattrs
      GameUITacticsCenterRefine:SetEquipData(GameUITacticsCenterRefine.m_equipData)
      GameUITacticsCenterRefine:UpdateLayer()
      TacticsCenterDataManager:saveDebuggingResult(GameUITacticsCenterRefine.m_equipData.vessel, GameUITacticsCenterRefine.m_equipData.id, new_equipattrs)
      GameUITacticsCenterRefine:TutorialCheck()
      GameUITacticsCenterRefine:TutorialCheckSave()
      GameTip:Show(GameLoader:GetGameText("LC_MENU_SUGGESTION_SAVE_SUCCESS"))
    else
      DebugOut("errorxxxx")
    end
    return true
  end
  if msgtype == NetAPIList.tactical_enter_refine_ack.Code then
    if content.code ~= 0 then
      local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    DebugOut("GameUITacticsCenterRefine._netCallback:")
    DebugTable(content)
    RefineDataManager:InitData(content)
    GameUITacticsCenterRefine:Show()
    return true
  end
  return false
end
return GameUITacticsCenterRefine
