local normalStep = GameData.Ladder.normalStep
table.insert(normalStep, {
  ID = "{3,1}",
  HEAD = "head16",
  LEVEL = 20,
  Dialog = 3,
  Reward = "[{money,1.001},{prestige,100}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 24159
})
table.insert(normalStep, {
  ID = "{3,2}",
  HEAD = "head25",
  LEVEL = 20,
  Dialog = 2,
  Reward = "[{money,1.002},{prestige,100}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 29337
})
table.insert(normalStep, {
  ID = "{3,3}",
  HEAD = "head13",
  LEVEL = 20,
  Dialog = 2,
  Reward = "[{money,1.003},{prestige,100}]",
  Reward2 = "[{item,101},{kenergy,30}]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 33712
})
table.insert(normalStep, {
  ID = "{3,4}",
  HEAD = "head24",
  LEVEL = 20,
  Dialog = 1,
  Reward = "[{money,1.004},{prestige,100}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 38458
})
table.insert(normalStep, {
  ID = "{3,5}",
  HEAD = "head12",
  LEVEL = 20,
  Dialog = 12,
  Reward = "[{money,1.005},{prestige,100}]",
  Reward2 = "[{item,102},{kenergy,40}]",
  ratio_reward = "[{krypton,50101,200,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 47884
})
table.insert(normalStep, {
  ID = "{3,6}",
  HEAD = "head24",
  LEVEL = 20,
  Dialog = 1,
  Reward = "[{money,1.006},{prestige,100}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 33220
})
table.insert(normalStep, {
  ID = "{3,7}",
  HEAD = "head12",
  LEVEL = 21,
  Dialog = 1,
  Reward = "[{money,1.007},{prestige,100}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 42220
})
table.insert(normalStep, {
  ID = "{3,8}",
  HEAD = "head16",
  LEVEL = 21,
  Dialog = 3,
  Reward = "[{money,1.008},{prestige,100}]",
  Reward2 = "[{item,103},{kenergy,50}]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 54342
})
table.insert(normalStep, {
  ID = "{3,9}",
  HEAD = "head25",
  LEVEL = 21,
  Dialog = 2,
  Reward = "[{money,1.009},{prestige,100}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 63985
})
table.insert(normalStep, {
  ID = "{3,10}",
  HEAD = "head13",
  LEVEL = 21,
  Dialog = 12,
  Reward = "[{money,1.01},{prestige,101}]",
  Reward2 = "[{item,104},{kenergy,60}]",
  ratio_reward = "[{krypton,50301,200,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 2,
  Force = 92484
})
table.insert(normalStep, {
  ID = "{3,11}",
  HEAD = "head25",
  LEVEL = 21,
  Dialog = 2,
  Reward = "[{money,1.011},{prestige,101}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 55364
})
table.insert(normalStep, {
  ID = "{3,12}",
  HEAD = "head13",
  LEVEL = 21,
  Dialog = 2,
  Reward = "[{money,1.012},{prestige,101}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 64708
})
table.insert(normalStep, {
  ID = "{3,13}",
  HEAD = "head24",
  LEVEL = 22,
  Dialog = 1,
  Reward = "[{money,1.013},{prestige,101}]",
  Reward2 = "[{item,105},{kenergy,70}]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 72396
})
table.insert(normalStep, {
  ID = "{3,14}",
  HEAD = "head12",
  LEVEL = 22,
  Dialog = 1,
  Reward = "[{money,1.014},{prestige,101}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 95903
})
table.insert(normalStep, {
  ID = "{3,15}",
  HEAD = "head16",
  LEVEL = 22,
  Dialog = 12,
  Reward = "[{money,1.015},{prestige,101}]",
  Reward2 = "[{item,106},{kenergy,80}]",
  ratio_reward = "[{krypton,50701,200,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 134166
})
table.insert(normalStep, {
  ID = "{3,16}",
  HEAD = "head12",
  LEVEL = 22,
  Dialog = 1,
  Reward = "[{money,1.016},{prestige,101}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 67278
})
table.insert(normalStep, {
  ID = "{3,17}",
  HEAD = "head16",
  LEVEL = 22,
  Dialog = 3,
  Reward = "[{money,1.017},{prestige,101}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 90241
})
table.insert(normalStep, {
  ID = "{3,18}",
  HEAD = "head25",
  LEVEL = 22,
  Dialog = 2,
  Reward = "[{money,1.018},{prestige,101}]",
  Reward2 = "[{item,121},{kenergy,90}]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 111812
})
table.insert(normalStep, {
  ID = "{3,19}",
  HEAD = "head13",
  LEVEL = 23,
  Dialog = 2,
  Reward = "[{money,1.019},{prestige,101}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 160202
})
table.insert(normalStep, {
  ID = "{3,20}",
  HEAD = "head24",
  LEVEL = 23,
  Dialog = 12,
  Reward = "[{money,1.02},{prestige,102}]",
  Reward2 = "[{item,122},{kenergy,100}]",
  ratio_reward = "[{krypton,50501,200,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 2,
  Force = 163628
})
table.insert(normalStep, {
  ID = "{3,21}",
  HEAD = "head13",
  LEVEL = 23,
  Dialog = 2,
  Reward = "[{money,1.021},{prestige,102}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 106243
})
table.insert(normalStep, {
  ID = "{3,22}",
  HEAD = "head24",
  LEVEL = 23,
  Dialog = 1,
  Reward = "[{money,1.022},{prestige,102}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 129051
})
table.insert(normalStep, {
  ID = "{3,23}",
  HEAD = "head12",
  LEVEL = 23,
  Dialog = 1,
  Reward = "[{money,1.023},{prestige,102}]",
  Reward2 = "[{item,123},{kenergy,110}]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 166707
})
table.insert(normalStep, {
  ID = "{3,24}",
  HEAD = "head16",
  LEVEL = 23,
  Dialog = 3,
  Reward = "[{money,1.024},{prestige,102}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 237436
})
table.insert(normalStep, {
  ID = "{3,25}",
  HEAD = "head25",
  LEVEL = 24,
  Dialog = 12,
  Reward = "[{money,1.025},{prestige,102}]",
  Reward2 = "[{item,124},{kenergy,120}]",
  ratio_reward = "[{krypton,50101,210,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 247968
})
table.insert(normalStep, {
  ID = "{3,26}",
  HEAD = "head16",
  LEVEL = 24,
  Dialog = 3,
  Reward = "[{money,1.026},{prestige,102}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 171446
})
table.insert(normalStep, {
  ID = "{3,27}",
  HEAD = "head25",
  LEVEL = 24,
  Dialog = 2,
  Reward = "[{money,1.027},{prestige,102}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 195078
})
table.insert(normalStep, {
  ID = "{3,28}",
  HEAD = "head13",
  LEVEL = 24,
  Dialog = 2,
  Reward = "[{money,1.028},{prestige,102}]",
  Reward2 = "[{item,125},{kenergy,130}]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 250472
})
table.insert(normalStep, {
  ID = "{3,29}",
  HEAD = "head24",
  LEVEL = 24,
  Dialog = 1,
  Reward = "[{money,1.029},{prestige,102}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 265814
})
table.insert(normalStep, {
  ID = "{3,30}",
  HEAD = "head12",
  LEVEL = 24,
  Dialog = 12,
  Reward = "[{money,1.03},{prestige,103}]",
  Reward2 = "[{item,126},{kenergy,140}]",
  ratio_reward = "[{krypton,50301,210,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 2,
  Force = 319801
})
table.insert(normalStep, {
  ID = "{3,31}",
  HEAD = "head24",
  LEVEL = 24,
  Dialog = 1,
  Reward = "[{money,1.031},{prestige,103}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 202862
})
table.insert(normalStep, {
  ID = "{3,32}",
  HEAD = "head12",
  LEVEL = 25,
  Dialog = 1,
  Reward = "[{money,1.032},{prestige,103}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 256052
})
table.insert(normalStep, {
  ID = "{3,33}",
  HEAD = "head16",
  LEVEL = 25,
  Dialog = 3,
  Reward = "[{money,1.033},{prestige,103}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 322652
})
table.insert(normalStep, {
  ID = "{3,34}",
  HEAD = "head25",
  LEVEL = 25,
  Dialog = 2,
  Reward = "[{money,1.034},{prestige,103}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 344906
})
table.insert(normalStep, {
  ID = "{3,35}",
  HEAD = "head13",
  LEVEL = 25,
  Dialog = 12,
  Reward = "[{money,1.035},{prestige,103}]",
  Reward2 = "[]",
  ratio_reward = "[{krypton,50701,210,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 442440
})
table.insert(normalStep, {
  ID = "{3,36}",
  HEAD = "head25",
  LEVEL = 25,
  Dialog = 2,
  Reward = "[{money,1.036},{prestige,103}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 288068
})
table.insert(normalStep, {
  ID = "{3,37}",
  HEAD = "head13",
  LEVEL = 25,
  Dialog = 2,
  Reward = "[{money,1.037},{prestige,103}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 352026
})
table.insert(normalStep, {
  ID = "{3,38}",
  HEAD = "head24",
  LEVEL = 26,
  Dialog = 1,
  Reward = "[{money,1.038},{prestige,103}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 376643
})
table.insert(normalStep, {
  ID = "{3,39}",
  HEAD = "head12",
  LEVEL = 26,
  Dialog = 1,
  Reward = "[{money,1.039},{prestige,103}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 426282
})
table.insert(normalStep, {
  ID = "{3,40}",
  HEAD = "head16",
  LEVEL = 26,
  Dialog = 12,
  Reward = "[{money,1.04},{prestige,104}]",
  Reward2 = "[{krypton,50101},{kenergy,140}]",
  ratio_reward = "[{krypton,50501,210,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 2,
  Force = 518280
})
table.insert(normalStep, {
  ID = "{3,41}",
  HEAD = "head12",
  LEVEL = 26,
  Dialog = 1,
  Reward = "[{money,1.041},{prestige,104}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 357823
})
table.insert(normalStep, {
  ID = "{3,42}",
  HEAD = "head16",
  LEVEL = 26,
  Dialog = 3,
  Reward = "[{money,1.042},{prestige,104}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 426148
})
table.insert(normalStep, {
  ID = "{3,43}",
  HEAD = "head25",
  LEVEL = 26,
  Dialog = 2,
  Reward = "[{money,1.043},{prestige,104}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 461358
})
table.insert(normalStep, {
  ID = "{3,44}",
  HEAD = "head13",
  LEVEL = 27,
  Dialog = 2,
  Reward = "[{money,1.044},{prestige,104}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 566531
})
table.insert(normalStep, {
  ID = "{3,45}",
  HEAD = "head24",
  LEVEL = 27,
  Dialog = 12,
  Reward = "[{money,1.045},{prestige,104}]",
  Reward2 = "[]",
  ratio_reward = "[{krypton,50101,220,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 2,
  Force = 603319
})
table.insert(normalStep, {
  ID = "{3,46}",
  HEAD = "head13",
  LEVEL = 27,
  Dialog = 2,
  Reward = "[{money,1.046},{prestige,104}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 464535
})
table.insert(normalStep, {
  ID = "{3,47}",
  HEAD = "head24",
  LEVEL = 27,
  Dialog = 1,
  Reward = "[{money,1.047},{prestige,104}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 473729
})
table.insert(normalStep, {
  ID = "{3,48}",
  HEAD = "head12",
  LEVEL = 27,
  Dialog = 1,
  Reward = "[{money,1.048},{prestige,104}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 538045
})
table.insert(normalStep, {
  ID = "{3,49}",
  HEAD = "head16",
  LEVEL = 27,
  Dialog = 3,
  Reward = "[{money,1.049},{prestige,104}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 627473
})
table.insert(normalStep, {
  ID = "{3,50}",
  HEAD = "head25",
  LEVEL = 28,
  Dialog = 12,
  Reward = "[{money,1.05},{prestige,105}]",
  Reward2 = "[{krypton,50301},{kenergy,150}]",
  ratio_reward = "[{krypton,50301,220,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 739081
})
table.insert(normalStep, {
  ID = "{3,51}",
  HEAD = "head16",
  LEVEL = 28,
  Dialog = 3,
  Reward = "[{money,1.051},{prestige,105}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 541676
})
table.insert(normalStep, {
  ID = "{3,52}",
  HEAD = "head25",
  LEVEL = 28,
  Dialog = 2,
  Reward = "[{money,1.052},{prestige,105}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 580519
})
table.insert(normalStep, {
  ID = "{3,53}",
  HEAD = "head13",
  LEVEL = 28,
  Dialog = 2,
  Reward = "[{money,1.053},{prestige,105}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 651556
})
table.insert(normalStep, {
  ID = "{3,54}",
  HEAD = "head24",
  LEVEL = 28,
  Dialog = 1,
  Reward = "[{money,1.054},{prestige,105}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 729131
})
table.insert(normalStep, {
  ID = "{3,55}",
  HEAD = "head12",
  LEVEL = 28,
  Dialog = 12,
  Reward = "[{money,1.055},{prestige,105}]",
  Reward2 = "[]",
  ratio_reward = "[{krypton,50701,220,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 2,
  Force = 839205
})
table.insert(normalStep, {
  ID = "{3,56}",
  HEAD = "head24",
  LEVEL = 28,
  Dialog = 1,
  Reward = "[{money,1.056},{prestige,105}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 580189
})
table.insert(normalStep, {
  ID = "{3,57}",
  HEAD = "head12",
  LEVEL = 29,
  Dialog = 1,
  Reward = "[{money,1.057},{prestige,105}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 664056
})
table.insert(normalStep, {
  ID = "{3,58}",
  HEAD = "head16",
  LEVEL = 29,
  Dialog = 3,
  Reward = "[{money,1.058},{prestige,105}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 739090
})
table.insert(normalStep, {
  ID = "{3,59}",
  HEAD = "head25",
  LEVEL = 29,
  Dialog = 2,
  Reward = "[{money,1.059},{prestige,105}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 767771
})
table.insert(normalStep, {
  ID = "{3,60}",
  HEAD = "head13",
  LEVEL = 29,
  Dialog = 12,
  Reward = "[{money,1.06},{prestige,106}]",
  Reward2 = "[{krypton,50701},{kenergy,160}]",
  ratio_reward = "[{krypton,50501,220,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 1001627
})
table.insert(normalStep, {
  ID = "{3,61}",
  HEAD = "head25",
  LEVEL = 29,
  Dialog = 2,
  Reward = "[{money,1.061},{prestige,106}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 710755
})
table.insert(normalStep, {
  ID = "{3,62}",
  HEAD = "head13",
  LEVEL = 29,
  Dialog = 2,
  Reward = "[{money,1.062},{prestige,106}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 793905
})
table.insert(normalStep, {
  ID = "{3,63}",
  HEAD = "head24",
  LEVEL = 30,
  Dialog = 1,
  Reward = "[{money,1.063},{prestige,106}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 804376
})
table.insert(normalStep, {
  ID = "{3,64}",
  HEAD = "head12",
  LEVEL = 30,
  Dialog = 1,
  Reward = "[{money,1.064},{prestige,106}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 877199
})
table.insert(normalStep, {
  ID = "{3,65}",
  HEAD = "head16",
  LEVEL = 30,
  Dialog = 12,
  Reward = "[{money,1.065},{prestige,106}]",
  Reward2 = "[]",
  ratio_reward = "[{krypton,50101,230,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 2,
  Force = 1105910
})
table.insert(normalStep, {
  ID = "{3,66}",
  HEAD = "head12",
  LEVEL = 30,
  Dialog = 1,
  Reward = "[{money,1.066},{prestige,106}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 806470
})
table.insert(normalStep, {
  ID = "{3,67}",
  HEAD = "head16",
  LEVEL = 30,
  Dialog = 3,
  Reward = "[{money,1.067},{prestige,106}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 880926
})
table.insert(normalStep, {
  ID = "{3,68}",
  HEAD = "head25",
  LEVEL = 30,
  Dialog = 2,
  Reward = "[{money,1.068},{prestige,106}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 936475
})
table.insert(normalStep, {
  ID = "{3,69}",
  HEAD = "head13",
  LEVEL = 31,
  Dialog = 2,
  Reward = "[{money,1.069},{prestige,106}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 1115324
})
table.insert(normalStep, {
  ID = "{3,70}",
  HEAD = "head24",
  LEVEL = 31,
  Dialog = 12,
  Reward = "[{money,1.07},{prestige,107}]",
  Reward2 = "[{krypton,50501},{kenergy,170}]",
  ratio_reward = "[{krypton,50301,230,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 1222508
})
table.insert(normalStep, {
  ID = "{3,71}",
  HEAD = "head13",
  LEVEL = 31,
  Dialog = 2,
  Reward = "[{money,1.071},{prestige,107}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 946542
})
table.insert(normalStep, {
  ID = "{3,72}",
  HEAD = "head24",
  LEVEL = 31,
  Dialog = 1,
  Reward = "[{money,1.072},{prestige,107}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 975011
})
table.insert(normalStep, {
  ID = "{3,73}",
  HEAD = "head12",
  LEVEL = 31,
  Dialog = 1,
  Reward = "[{money,1.073},{prestige,107}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 1038101
})
table.insert(normalStep, {
  ID = "{3,74}",
  HEAD = "head16",
  LEVEL = 31,
  Dialog = 3,
  Reward = "[{money,1.074},{prestige,107}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 1149815
})
table.insert(normalStep, {
  ID = "{3,75}",
  HEAD = "head25",
  LEVEL = 32,
  Dialog = 12,
  Reward = "[{money,1.075},{prestige,107}]",
  Reward2 = "[]",
  ratio_reward = "[{krypton,50701,230,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 2,
  Force = 1399776
})
table.insert(normalStep, {
  ID = "{3,76}",
  HEAD = "head16",
  LEVEL = 32,
  Dialog = 3,
  Reward = "[{money,1.076},{prestige,107}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 1036910
})
table.insert(normalStep, {
  ID = "{3,77}",
  HEAD = "head25",
  LEVEL = 32,
  Dialog = 2,
  Reward = "[{money,1.077},{prestige,107}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 1096847
})
table.insert(normalStep, {
  ID = "{3,78}",
  HEAD = "head13",
  LEVEL = 32,
  Dialog = 2,
  Reward = "[{money,1.078},{prestige,107}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 1187593
})
table.insert(normalStep, {
  ID = "{3,79}",
  HEAD = "head24",
  LEVEL = 32,
  Dialog = 1,
  Reward = "[{money,1.079},{prestige,107}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 1331073
})
table.insert(normalStep, {
  ID = "{3,80}",
  HEAD = "head12",
  LEVEL = 32,
  Dialog = 12,
  Reward = "[{money,1.08},{prestige,108}]",
  Reward2 = "[{krypton,50101},{krypton,50101},{kenergy,180}]",
  ratio_reward = "[{krypton,50501,230,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 2,
  Force = 1533491
})
table.insert(normalStep, {
  ID = "{3,81}",
  HEAD = "head24",
  LEVEL = 32,
  Dialog = 1,
  Reward = "[{money,1.081},{prestige,108}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 1081711
})
table.insert(normalStep, {
  ID = "{3,82}",
  HEAD = "head12",
  LEVEL = 33,
  Dialog = 1,
  Reward = "[{money,1.082},{prestige,108}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 1207539
})
table.insert(normalStep, {
  ID = "{3,83}",
  HEAD = "head16",
  LEVEL = 33,
  Dialog = 3,
  Reward = "[{money,1.083},{prestige,108}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 1285690
})
table.insert(normalStep, {
  ID = "{3,84}",
  HEAD = "head25",
  LEVEL = 33,
  Dialog = 2,
  Reward = "[{money,1.084},{prestige,108}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 1322978
})
table.insert(normalStep, {
  ID = "{3,85}",
  HEAD = "head13",
  LEVEL = 33,
  Dialog = 12,
  Reward = "[{money,1.085},{prestige,108}]",
  Reward2 = "[]",
  ratio_reward = "[{krypton,50101,240,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 1741563
})
table.insert(normalStep, {
  ID = "{3,86}",
  HEAD = "head25",
  LEVEL = 33,
  Dialog = 2,
  Reward = "[{money,1.086},{prestige,108}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 1270994
})
table.insert(normalStep, {
  ID = "{3,87}",
  HEAD = "head13",
  LEVEL = 33,
  Dialog = 2,
  Reward = "[{money,1.087},{prestige,108}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 1371819
})
table.insert(normalStep, {
  ID = "{3,88}",
  HEAD = "head24",
  LEVEL = 34,
  Dialog = 1,
  Reward = "[{money,1.088},{prestige,108}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 1492628
})
table.insert(normalStep, {
  ID = "{3,89}",
  HEAD = "head12",
  LEVEL = 34,
  Dialog = 1,
  Reward = "[{money,1.089},{prestige,108}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 1651525
})
table.insert(normalStep, {
  ID = "{3,90}",
  HEAD = "head16",
  LEVEL = 34,
  Dialog = 12,
  Reward = "[{money,1.09},{prestige,109}]",
  Reward2 = "[{krypton,50301},{krypton,50301},{kenergy,190}]",
  ratio_reward = "[{krypton,50301,240,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 2,
  Force = 1877220
})
table.insert(normalStep, {
  ID = "{3,91}",
  HEAD = "head12",
  LEVEL = 34,
  Dialog = 1,
  Reward = "[{money,1.091},{prestige,109}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 1392132
})
table.insert(normalStep, {
  ID = "{3,92}",
  HEAD = "head16",
  LEVEL = 34,
  Dialog = 3,
  Reward = "[{money,1.092},{prestige,109}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 1470893
})
table.insert(normalStep, {
  ID = "{3,93}",
  HEAD = "head25",
  LEVEL = 34,
  Dialog = 2,
  Reward = "[{money,1.093},{prestige,109}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 1550650
})
table.insert(normalStep, {
  ID = "{3,94}",
  HEAD = "head13",
  LEVEL = 35,
  Dialog = 2,
  Reward = "[{money,1.094},{prestige,109}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 1822004
})
table.insert(normalStep, {
  ID = "{3,95}",
  HEAD = "head24",
  LEVEL = 35,
  Dialog = 12,
  Reward = "[{money,1.095},{prestige,109}]",
  Reward2 = "[]",
  ratio_reward = "[{krypton,50701,240,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 2027516
})
table.insert(normalStep, {
  ID = "{3,96}",
  HEAD = "head13",
  LEVEL = 35,
  Dialog = 2,
  Reward = "[{money,1.096},{prestige,109}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 1587277
})
table.insert(normalStep, {
  ID = "{3,97}",
  HEAD = "head24",
  LEVEL = 35,
  Dialog = 1,
  Reward = "[{money,1.097},{prestige,109}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 1618770
})
table.insert(normalStep, {
  ID = "{3,98}",
  HEAD = "head12",
  LEVEL = 35,
  Dialog = 1,
  Reward = "[{money,1.098},{prestige,109}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 1697807
})
table.insert(normalStep, {
  ID = "{3,99}",
  HEAD = "head16",
  LEVEL = 35,
  Dialog = 3,
  Reward = "[{money,1.099},{prestige,109}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 1831989
})
table.insert(normalStep, {
  ID = "{3,100}",
  HEAD = "head25",
  LEVEL = 36,
  Dialog = 12,
  Reward = "[{money,1.1},{prestige,110}]",
  Reward2 = "[{krypton,50701},{krypton,50701},{kenergy,200}]",
  ratio_reward = "[{krypton,50501,240,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 2,
  Force = 2268616
})
table.insert(normalStep, {
  ID = "{3,101}",
  HEAD = "head16",
  LEVEL = 36,
  Dialog = 3,
  Reward = "[{money,1.101},{prestige,110}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 1689114
})
table.insert(normalStep, {
  ID = "{3,102}",
  HEAD = "head25",
  LEVEL = 36,
  Dialog = 2,
  Reward = "[{money,1.102},{prestige,110}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 1775402
})
table.insert(normalStep, {
  ID = "{3,103}",
  HEAD = "head13",
  LEVEL = 36,
  Dialog = 2,
  Reward = "[{money,1.103},{prestige,110}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 1885716
})
table.insert(normalStep, {
  ID = "{3,104}",
  HEAD = "head24",
  LEVEL = 36,
  Dialog = 1,
  Reward = "[{money,1.104},{prestige,110}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 2012927
})
table.insert(normalStep, {
  ID = "{3,105}",
  HEAD = "head12",
  LEVEL = 36,
  Dialog = 12,
  Reward = "[{money,1.105},{prestige,110}]",
  Reward2 = "[]",
  ratio_reward = "[{item,{3101,1},750,1000},{krypton,50101,250,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 2455561
})
table.insert(normalStep, {
  ID = "{3,106}",
  HEAD = "head24",
  LEVEL = 36,
  Dialog = 1,
  Reward = "[{money,1.106},{prestige,110}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 1740482
})
table.insert(normalStep, {
  ID = "{3,107}",
  HEAD = "head12",
  LEVEL = 37,
  Dialog = 1,
  Reward = "[{money,1.107},{prestige,110}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 1917452
})
table.insert(normalStep, {
  ID = "{3,108}",
  HEAD = "head16",
  LEVEL = 37,
  Dialog = 3,
  Reward = "[{money,1.108},{prestige,110}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 2048638
})
table.insert(normalStep, {
  ID = "{3,109}",
  HEAD = "head25",
  LEVEL = 37,
  Dialog = 2,
  Reward = "[{money,1.109},{prestige,110}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 2098929
})
table.insert(normalStep, {
  ID = "{3,110}",
  HEAD = "head13",
  LEVEL = 37,
  Dialog = 12,
  Reward = "[{money,1.11},{prestige,111}]",
  Reward2 = "[{krypton,50501},{krypton,50501},{kenergy,210}]",
  ratio_reward = "[{item,{3101,1},850,1000},{krypton,50301,250,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 2,
  Force = 2738528
})
table.insert(normalStep, {
  ID = "{3,111}",
  HEAD = "head25",
  LEVEL = 37,
  Dialog = 2,
  Reward = "[{money,1.111},{prestige,111}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 2000607
})
table.insert(normalStep, {
  ID = "{3,112}",
  HEAD = "head13",
  LEVEL = 37,
  Dialog = 2,
  Reward = "[{money,1.112},{prestige,111}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 2090463
})
table.insert(normalStep, {
  ID = "{3,113}",
  HEAD = "head24",
  LEVEL = 38,
  Dialog = 1,
  Reward = "[{money,1.113},{prestige,111}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 2143626
})
table.insert(normalStep, {
  ID = "{3,114}",
  HEAD = "head12",
  LEVEL = 38,
  Dialog = 1,
  Reward = "[{money,1.114},{prestige,111}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 2282064
})
table.insert(normalStep, {
  ID = "{3,115}",
  HEAD = "head16",
  LEVEL = 38,
  Dialog = 12,
  Reward = "[{money,1.115},{prestige,111}]",
  Reward2 = "[]",
  ratio_reward = "[{item,{3101,1},900,1000},{krypton,50701,250,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 2930952
})
table.insert(normalStep, {
  ID = "{3,116}",
  HEAD = "head12",
  LEVEL = 38,
  Dialog = 1,
  Reward = "[{money,1.116},{prestige,111}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 2208112
})
table.insert(normalStep, {
  ID = "{3,117}",
  HEAD = "head16",
  LEVEL = 38,
  Dialog = 3,
  Reward = "[{money,1.117},{prestige,111}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 2290659
})
table.insert(normalStep, {
  ID = "{3,118}",
  HEAD = "head25",
  LEVEL = 38,
  Dialog = 2,
  Reward = "[{money,1.118},{prestige,111}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 2408809
})
table.insert(normalStep, {
  ID = "{3,119}",
  HEAD = "head13",
  LEVEL = 39,
  Dialog = 2,
  Reward = "[{money,1.119},{prestige,111}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 2799584
})
table.insert(normalStep, {
  ID = "{3,120}",
  HEAD = "head24",
  LEVEL = 39,
  Dialog = 12,
  Reward = "[{money,1.12},{prestige,112}]",
  Reward2 = "[{krypton,50102},{kenergy,220}]",
  ratio_reward = "[{item,{3101,1},1000,1000},{krypton,50501,250,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 2,
  Force = 3146366
})
table.insert(normalStep, {
  ID = "{3,121}",
  HEAD = "head13",
  LEVEL = 39,
  Dialog = 2,
  Reward = "[{money,1.121},{prestige,112}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 2437197
})
table.insert(normalStep, {
  ID = "{3,122}",
  HEAD = "head24",
  LEVEL = 39,
  Dialog = 1,
  Reward = "[{money,1.122},{prestige,112}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 2524050
})
table.insert(normalStep, {
  ID = "{3,123}",
  HEAD = "head12",
  LEVEL = 39,
  Dialog = 1,
  Reward = "[{money,1.123},{prestige,112}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 2579856
})
table.insert(normalStep, {
  ID = "{3,124}",
  HEAD = "head16",
  LEVEL = 39,
  Dialog = 3,
  Reward = "[{money,1.124},{prestige,112}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 2744697
})
table.insert(normalStep, {
  ID = "{3,125}",
  HEAD = "head25",
  LEVEL = 40,
  Dialog = 12,
  Reward = "[{money,1.125},{prestige,112}]",
  Reward2 = "[]",
  ratio_reward = "[{item,{3101,1},750,1000},{krypton,50102,150,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 3435462
})
table.insert(normalStep, {
  ID = "{3,126}",
  HEAD = "head16",
  LEVEL = 40,
  Dialog = 3,
  Reward = "[{money,1.126},{prestige,112}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 2554809
})
table.insert(normalStep, {
  ID = "{3,127}",
  HEAD = "head25",
  LEVEL = 40,
  Dialog = 2,
  Reward = "[{money,1.127},{prestige,112}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 2680888
})
table.insert(normalStep, {
  ID = "{3,128}",
  HEAD = "head13",
  LEVEL = 40,
  Dialog = 2,
  Reward = "[{money,1.128},{prestige,112}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 2819289
})
table.insert(normalStep, {
  ID = "{3,129}",
  HEAD = "head24",
  LEVEL = 40,
  Dialog = 1,
  Reward = "[{money,1.129},{prestige,112}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 3117981
})
table.insert(normalStep, {
  ID = "{3,130}",
  HEAD = "head12",
  LEVEL = 40,
  Dialog = 12,
  Reward = "[{money,1.13},{prestige,113}]",
  Reward2 = "[{krypton,50302},{kenergy,230}]",
  ratio_reward = "[{item,{3101,1},850,1000},{krypton,50302,150,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 2,
  Force = 3661873
})
table.insert(normalStep, {
  ID = "{3,131}",
  HEAD = "head24",
  LEVEL = 40,
  Dialog = 1,
  Reward = "[{money,1.131},{prestige,113}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 2619794
})
table.insert(normalStep, {
  ID = "{3,132}",
  HEAD = "head12",
  LEVEL = 41,
  Dialog = 1,
  Reward = "[{money,1.132},{prestige,113}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 2863096
})
table.insert(normalStep, {
  ID = "{3,133}",
  HEAD = "head16",
  LEVEL = 41,
  Dialog = 3,
  Reward = "[{money,1.133},{prestige,113}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 2951826
})
table.insert(normalStep, {
  ID = "{3,134}",
  HEAD = "head25",
  LEVEL = 41,
  Dialog = 2,
  Reward = "[{money,1.134},{prestige,113}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 3017942
})
table.insert(normalStep, {
  ID = "{3,135}",
  HEAD = "head13",
  LEVEL = 41,
  Dialog = 12,
  Reward = "[{money,1.135},{prestige,113}]",
  Reward2 = "[]",
  ratio_reward = "[{item,{3101,1},900,1000},{krypton,50702,150,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 3993985
})
table.insert(normalStep, {
  ID = "{3,136}",
  HEAD = "head25",
  LEVEL = 41,
  Dialog = 2,
  Reward = "[{money,1.136},{prestige,113}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 2970269
})
table.insert(normalStep, {
  ID = "{3,137}",
  HEAD = "head13",
  LEVEL = 41,
  Dialog = 2,
  Reward = "[{money,1.137},{prestige,113}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 3064904
})
table.insert(normalStep, {
  ID = "{3,138}",
  HEAD = "head24",
  LEVEL = 42,
  Dialog = 1,
  Reward = "[{money,1.138},{prestige,113}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 3156976
})
table.insert(normalStep, {
  ID = "{3,139}",
  HEAD = "head12",
  LEVEL = 42,
  Dialog = 1,
  Reward = "[{money,1.139},{prestige,113}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 3244698
})
table.insert(normalStep, {
  ID = "{3,140}",
  HEAD = "head16",
  LEVEL = 42,
  Dialog = 12,
  Reward = "[{money,1.14},{prestige,114}]",
  Reward2 = "[{krypton,50702},{kenergy,240}]",
  ratio_reward = "[{item,{3101,1},1000,1000},{krypton,50502,150,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 2,
  Force = 4219453
})
table.insert(normalStep, {
  ID = "{3,141}",
  HEAD = "head12",
  LEVEL = 42,
  Dialog = 1,
  Reward = "[{money,1.141},{prestige,114}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 3171905
})
table.insert(normalStep, {
  ID = "{3,142}",
  HEAD = "head16",
  LEVEL = 42,
  Dialog = 3,
  Reward = "[{money,1.142},{prestige,114}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 3253628
})
table.insert(normalStep, {
  ID = "{3,143}",
  HEAD = "head25",
  LEVEL = 42,
  Dialog = 2,
  Reward = "[{money,1.143},{prestige,114}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 3412843
})
table.insert(normalStep, {
  ID = "{3,144}",
  HEAD = "head13",
  LEVEL = 43,
  Dialog = 2,
  Reward = "[{money,1.144},{prestige,114}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 3944324
})
table.insert(normalStep, {
  ID = "{3,145}",
  HEAD = "head24",
  LEVEL = 43,
  Dialog = 12,
  Reward = "[{money,1.145},{prestige,114}]",
  Reward2 = "[]",
  ratio_reward = "[{item,{3101,1},750,1000},{krypton,50102,160,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 2,
  Force = 4468786
})
table.insert(normalStep, {
  ID = "{3,146}",
  HEAD = "head13",
  LEVEL = 43,
  Dialog = 2,
  Reward = "[{money,1.146},{prestige,114}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 3351432
})
table.insert(normalStep, {
  ID = "{3,147}",
  HEAD = "head24",
  LEVEL = 43,
  Dialog = 1,
  Reward = "[{money,1.147},{prestige,114}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 3426574
})
table.insert(normalStep, {
  ID = "{3,148}",
  HEAD = "head12",
  LEVEL = 43,
  Dialog = 1,
  Reward = "[{money,1.148},{prestige,114}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 3685916
})
table.insert(normalStep, {
  ID = "{3,149}",
  HEAD = "head16",
  LEVEL = 43,
  Dialog = 3,
  Reward = "[{money,1.149},{prestige,114}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 3882132
})
table.insert(normalStep, {
  ID = "{3,150}",
  HEAD = "head25",
  LEVEL = 44,
  Dialog = 12,
  Reward = "[{money,1.15},{prestige,115}]",
  Reward2 = "[{krypton,50502},{kenergy,250}]",
  ratio_reward = "[{item,{3101,1},850,1000},{krypton,50302,160,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 4886145
})
table.insert(normalStep, {
  ID = "{3,151}",
  HEAD = "head16",
  LEVEL = 44,
  Dialog = 3,
  Reward = "[{money,1.151},{prestige,115}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 3649360
})
table.insert(normalStep, {
  ID = "{3,152}",
  HEAD = "head25",
  LEVEL = 44,
  Dialog = 2,
  Reward = "[{money,1.152},{prestige,115}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 3817099
})
table.insert(normalStep, {
  ID = "{3,153}",
  HEAD = "head13",
  LEVEL = 44,
  Dialog = 2,
  Reward = "[{money,1.153},{prestige,115}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 3975733
})
table.insert(normalStep, {
  ID = "{3,154}",
  HEAD = "head24",
  LEVEL = 44,
  Dialog = 1,
  Reward = "[{money,1.154},{prestige,115}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 4254929
})
table.insert(normalStep, {
  ID = "{3,155}",
  HEAD = "head12",
  LEVEL = 44,
  Dialog = 12,
  Reward = "[{money,1.155},{prestige,115}]",
  Reward2 = "[]",
  ratio_reward = "[{item,{3101,1},900,1000},{krypton,50702,160,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 2,
  Force = 5243041
})
table.insert(normalStep, {
  ID = "{3,156}",
  HEAD = "head24",
  LEVEL = 44,
  Dialog = 1,
  Reward = "[{money,1.156},{prestige,115}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 3742792
})
table.insert(normalStep, {
  ID = "{3,157}",
  HEAD = "head12",
  LEVEL = 45,
  Dialog = 1,
  Reward = "[{money,1.157},{prestige,115}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 4043932
})
table.insert(normalStep, {
  ID = "{3,158}",
  HEAD = "head16",
  LEVEL = 45,
  Dialog = 3,
  Reward = "[{money,1.158},{prestige,115}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 4353064
})
table.insert(normalStep, {
  ID = "{3,159}",
  HEAD = "head25",
  LEVEL = 45,
  Dialog = 2,
  Reward = "[{money,1.159},{prestige,115}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 4452723
})
table.insert(normalStep, {
  ID = "{3,160}",
  HEAD = "head13",
  LEVEL = 45,
  Dialog = 12,
  Reward = "[{money,1.16},{prestige,116}]",
  Reward2 = "[{krypton,50902},{kenergy,260}]",
  ratio_reward = "[{item,{3101,1},1000,1000},{krypton,50502,160,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 5712884
})
table.insert(normalStep, {
  ID = "{3,161}",
  HEAD = "head25",
  LEVEL = 45,
  Dialog = 2,
  Reward = "[{money,1.161},{prestige,116}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 4183193
})
table.insert(normalStep, {
  ID = "{3,162}",
  HEAD = "head13",
  LEVEL = 45,
  Dialog = 2,
  Reward = "[{money,1.162},{prestige,116}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 4592407
})
table.insert(normalStep, {
  ID = "{3,163}",
  HEAD = "head24",
  LEVEL = 46,
  Dialog = 1,
  Reward = "[{money,1.163},{prestige,116}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 4685574
})
table.insert(normalStep, {
  ID = "{3,164}",
  HEAD = "head12",
  LEVEL = 46,
  Dialog = 1,
  Reward = "[{money,1.164},{prestige,116}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 4749645
})
table.insert(normalStep, {
  ID = "{3,165}",
  HEAD = "head16",
  LEVEL = 46,
  Dialog = 12,
  Reward = "[{money,1.165},{prestige,116}]",
  Reward2 = "[]",
  ratio_reward = "[{item,{3101,1},750,1000},{krypton,50102,170,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 2,
  Force = 6084469
})
table.insert(normalStep, {
  ID = "{3,166}",
  HEAD = "head12",
  LEVEL = 46,
  Dialog = 1,
  Reward = "[{money,1.166},{prestige,116}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 4654411
})
table.insert(normalStep, {
  ID = "{3,167}",
  HEAD = "head16",
  LEVEL = 46,
  Dialog = 3,
  Reward = "[{money,1.167},{prestige,116}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 4754271
})
table.insert(normalStep, {
  ID = "{3,168}",
  HEAD = "head25",
  LEVEL = 46,
  Dialog = 2,
  Reward = "[{money,1.168},{prestige,116}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 4984420
})
table.insert(normalStep, {
  ID = "{3,169}",
  HEAD = "head13",
  LEVEL = 47,
  Dialog = 2,
  Reward = "[{money,1.169},{prestige,116}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 5691006
})
table.insert(normalStep, {
  ID = "{3,170}",
  HEAD = "head24",
  LEVEL = 47,
  Dialog = 12,
  Reward = "[{money,1.17},{prestige,117}]",
  Reward2 = "[{krypton,51002},{kenergy,270}]",
  ratio_reward = "[{item,{3101,1},850,1000},{krypton,50302,170,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 6485338
})
table.insert(normalStep, {
  ID = "{3,171}",
  HEAD = "head13",
  LEVEL = 47,
  Dialog = 2,
  Reward = "[{money,1.171},{prestige,117}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 5020214
})
table.insert(normalStep, {
  ID = "{3,172}",
  HEAD = "head24",
  LEVEL = 47,
  Dialog = 1,
  Reward = "[{money,1.172},{prestige,117}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 5080973
})
table.insert(normalStep, {
  ID = "{3,173}",
  HEAD = "head12",
  LEVEL = 47,
  Dialog = 1,
  Reward = "[{money,1.173},{prestige,117}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 5255486
})
table.insert(normalStep, {
  ID = "{3,174}",
  HEAD = "head16",
  LEVEL = 47,
  Dialog = 3,
  Reward = "[{money,1.174},{prestige,117}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 5509658
})
table.insert(normalStep, {
  ID = "{3,175}",
  HEAD = "head25",
  LEVEL = 48,
  Dialog = 12,
  Reward = "[{money,1.175},{prestige,117}]",
  Reward2 = "[]",
  ratio_reward = "[{item,{3101,1},900,1000},{krypton,50702,170,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 2,
  Force = 6941103
})
table.insert(normalStep, {
  ID = "{3,176}",
  HEAD = "head16",
  LEVEL = 48,
  Dialog = 3,
  Reward = "[{money,1.176},{prestige,117}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 5185622
})
table.insert(normalStep, {
  ID = "{3,177}",
  HEAD = "head25",
  LEVEL = 48,
  Dialog = 2,
  Reward = "[{money,1.177},{prestige,117}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 5429627
})
table.insert(normalStep, {
  ID = "{3,178}",
  HEAD = "head13",
  LEVEL = 48,
  Dialog = 2,
  Reward = "[{money,1.178},{prestige,117}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 5640111
})
table.insert(normalStep, {
  ID = "{3,179}",
  HEAD = "head24",
  LEVEL = 48,
  Dialog = 1,
  Reward = "[{money,1.179},{prestige,117}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 6112946
})
table.insert(normalStep, {
  ID = "{3,180}",
  HEAD = "head12",
  LEVEL = 48,
  Dialog = 12,
  Reward = "[{money,1.18},{prestige,118}]",
  Reward2 = "[{krypton,51302},{kenergy,280}]",
  ratio_reward = "[{item,{3101,1},1000,1000},{krypton,50502,170,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 2,
  Force = 7314592
})
table.insert(normalStep, {
  ID = "{3,181}",
  HEAD = "head24",
  LEVEL = 48,
  Dialog = 1,
  Reward = "[{money,1.181},{prestige,118}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 5315667
})
table.insert(normalStep, {
  ID = "{3,182}",
  HEAD = "head12",
  LEVEL = 49,
  Dialog = 1,
  Reward = "[{money,1.182},{prestige,118}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 5717844
})
table.insert(normalStep, {
  ID = "{3,183}",
  HEAD = "head16",
  LEVEL = 49,
  Dialog = 3,
  Reward = "[{money,1.183},{prestige,118}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 5820519
})
table.insert(normalStep, {
  ID = "{3,184}",
  HEAD = "head25",
  LEVEL = 49,
  Dialog = 2,
  Reward = "[{money,1.184},{prestige,118}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 5949879
})
table.insert(normalStep, {
  ID = "{3,185}",
  HEAD = "head13",
  LEVEL = 49,
  Dialog = 12,
  Reward = "[{money,1.185},{prestige,118}]",
  Reward2 = "[]",
  ratio_reward = "[{item,{3101,1},750,1000},{krypton,50102,180,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 7829631
})
table.insert(normalStep, {
  ID = "{3,186}",
  HEAD = "head25",
  LEVEL = 49,
  Dialog = 2,
  Reward = "[{money,1.186},{prestige,118}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 5901481
})
table.insert(normalStep, {
  ID = "{3,187}",
  HEAD = "head13",
  LEVEL = 49,
  Dialog = 2,
  Reward = "[{money,1.187},{prestige,118}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 6130524
})
table.insert(normalStep, {
  ID = "{3,188}",
  HEAD = "head24",
  LEVEL = 50,
  Dialog = 1,
  Reward = "[{money,1.188},{prestige,118}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 6198942
})
table.insert(normalStep, {
  ID = "{3,189}",
  HEAD = "head12",
  LEVEL = 50,
  Dialog = 1,
  Reward = "[{money,1.189},{prestige,118}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 6307257
})
table.insert(normalStep, {
  ID = "{3,190}",
  HEAD = "head16",
  LEVEL = 50,
  Dialog = 12,
  Reward = "[{money,1.19},{prestige,119}]",
  Reward2 = "[{krypton,51702},{kenergy,290}]",
  ratio_reward = "[{item,{3101,1},850,1000},{krypton,50302,180,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 2,
  Force = 8199189
})
table.insert(normalStep, {
  ID = "{3,191}",
  HEAD = "head12",
  LEVEL = 50,
  Dialog = 1,
  Reward = "[{money,1.191},{prestige,119}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 6209867
})
table.insert(normalStep, {
  ID = "{3,192}",
  HEAD = "head16",
  LEVEL = 50,
  Dialog = 3,
  Reward = "[{money,1.192},{prestige,119}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 6306444
})
table.insert(normalStep, {
  ID = "{3,193}",
  HEAD = "head25",
  LEVEL = 50,
  Dialog = 2,
  Reward = "[{money,1.193},{prestige,119}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 6601722
})
table.insert(normalStep, {
  ID = "{3,194}",
  HEAD = "head13",
  LEVEL = 51,
  Dialog = 2,
  Reward = "[{money,1.194},{prestige,119}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 7507518
})
table.insert(normalStep, {
  ID = "{3,195}",
  HEAD = "head24",
  LEVEL = 51,
  Dialog = 12,
  Reward = "[{money,1.195},{prestige,119}]",
  Reward2 = "[]",
  ratio_reward = "[{item,{3101,1},900,1000},{krypton,50702,180,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 8607019
})
table.insert(normalStep, {
  ID = "{3,196}",
  HEAD = "head13",
  LEVEL = 51,
  Dialog = 2,
  Reward = "[{money,1.196},{prestige,119}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 6808601
})
table.insert(normalStep, {
  ID = "{3,197}",
  HEAD = "head24",
  LEVEL = 51,
  Dialog = 1,
  Reward = "[{money,1.197},{prestige,119}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 7020701
})
table.insert(normalStep, {
  ID = "{3,198}",
  HEAD = "head12",
  LEVEL = 51,
  Dialog = 1,
  Reward = "[{money,1.198},{prestige,119}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 7084465
})
table.insert(normalStep, {
  ID = "{3,199}",
  HEAD = "head16",
  LEVEL = 51,
  Dialog = 3,
  Reward = "[{money,1.199},{prestige,119}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 7379187
})
table.insert(normalStep, {
  ID = "{3,200}",
  HEAD = "head25",
  LEVEL = 52,
  Dialog = 12,
  Reward = "[{money,1.2},{prestige,120}]",
  Reward2 = "[{krypton,50102},{krypton,50102},{kenergy,300}]",
  ratio_reward = "[{item,{3101,1},1000,1000},{krypton,50502,180,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 2,
  Force = 9310920
})
table.insert(normalStep, {
  ID = "{3,201}",
  HEAD = "head16",
  LEVEL = 52,
  Dialog = 3,
  Reward = "[{money,1.201},{prestige,120}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 6998353
})
table.insert(normalStep, {
  ID = "{3,202}",
  HEAD = "head25",
  LEVEL = 52,
  Dialog = 2,
  Reward = "[{money,1.202},{prestige,120}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 7309310
})
table.insert(normalStep, {
  ID = "{3,203}",
  HEAD = "head13",
  LEVEL = 52,
  Dialog = 2,
  Reward = "[{money,1.203},{prestige,120}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 7551995
})
table.insert(normalStep, {
  ID = "{3,204}",
  HEAD = "head24",
  LEVEL = 52,
  Dialog = 1,
  Reward = "[{money,1.204},{prestige,120}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 8215239
})
table.insert(normalStep, {
  ID = "{3,205}",
  HEAD = "head12",
  LEVEL = 52,
  Dialog = 12,
  Reward = "[{money,1.205},{prestige,120}]",
  Reward2 = "[]",
  ratio_reward = "[{item,{3101,1},750,1000},{krypton,50102,190,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 9936613
})
table.insert(normalStep, {
  ID = "{3,206}",
  HEAD = "head24",
  LEVEL = 52,
  Dialog = 1,
  Reward = "[{money,1.206},{prestige,120}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 7177198
})
table.insert(normalStep, {
  ID = "{3,207}",
  HEAD = "head12",
  LEVEL = 53,
  Dialog = 1,
  Reward = "[{money,1.207},{prestige,120}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 7665591
})
table.insert(normalStep, {
  ID = "{3,208}",
  HEAD = "head16",
  LEVEL = 53,
  Dialog = 3,
  Reward = "[{money,1.208},{prestige,120}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 8256961
})
table.insert(normalStep, {
  ID = "{3,209}",
  HEAD = "head25",
  LEVEL = 53,
  Dialog = 2,
  Reward = "[{money,1.209},{prestige,120}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 8460301
})
table.insert(normalStep, {
  ID = "{3,210}",
  HEAD = "head13",
  LEVEL = 53,
  Dialog = 12,
  Reward = "[{money,1.21},{prestige,121}]",
  Reward2 = "[{krypton,50302},{krypton,50302},{kenergy,310}]",
  ratio_reward = "[{item,{3101,1},850,1000},{krypton,50302,190,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 2,
  Force = 10745318
})
table.insert(normalStep, {
  ID = "{3,211}",
  HEAD = "head25",
  LEVEL = 53,
  Dialog = 2,
  Reward = "[{money,1.211},{prestige,121}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 7903426
})
table.insert(normalStep, {
  ID = "{3,212}",
  HEAD = "head13",
  LEVEL = 53,
  Dialog = 2,
  Reward = "[{money,1.212},{prestige,121}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 8676916
})
table.insert(normalStep, {
  ID = "{3,213}",
  HEAD = "head24",
  LEVEL = 54,
  Dialog = 1,
  Reward = "[{money,1.213},{prestige,121}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 8767091
})
table.insert(normalStep, {
  ID = "{3,214}",
  HEAD = "head12",
  LEVEL = 54,
  Dialog = 1,
  Reward = "[{money,1.214},{prestige,121}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 8947854
})
table.insert(normalStep, {
  ID = "{3,215}",
  HEAD = "head16",
  LEVEL = 54,
  Dialog = 12,
  Reward = "[{money,1.215},{prestige,121}]",
  Reward2 = "[]",
  ratio_reward = "[{item,{3101,1},900,1000},{krypton,50702,190,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 11406316
})
table.insert(normalStep, {
  ID = "{3,216}",
  HEAD = "head12",
  LEVEL = 54,
  Dialog = 1,
  Reward = "[{money,1.216},{prestige,121}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 8789644
})
table.insert(normalStep, {
  ID = "{3,217}",
  HEAD = "head16",
  LEVEL = 54,
  Dialog = 3,
  Reward = "[{money,1.217},{prestige,121}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 8920600
})
table.insert(normalStep, {
  ID = "{3,218}",
  HEAD = "head25",
  LEVEL = 54,
  Dialog = 2,
  Reward = "[{money,1.218},{prestige,121}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 9353553
})
table.insert(normalStep, {
  ID = "{3,219}",
  HEAD = "head13",
  LEVEL = 55,
  Dialog = 2,
  Reward = "[{money,1.219},{prestige,121}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 10539689
})
table.insert(normalStep, {
  ID = "{3,220}",
  HEAD = "head24",
  LEVEL = 55,
  Dialog = 12,
  Reward = "[{money,1.22},{prestige,122}]",
  Reward2 = "[{krypton,50702},{krypton,50702},{kenergy,320}]",
  ratio_reward = "[{item,{3101,1},1000,1000},{krypton,50502,190,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 2,
  Force = 12133081
})
table.insert(normalStep, {
  ID = "{3,221}",
  HEAD = "head13",
  LEVEL = 55,
  Dialog = 2,
  Reward = "[{money,1.221},{prestige,122}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 9475959
})
table.insert(normalStep, {
  ID = "{3,222}",
  HEAD = "head24",
  LEVEL = 55,
  Dialog = 1,
  Reward = "[{money,1.222},{prestige,122}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 9629920
})
table.insert(normalStep, {
  ID = "{3,223}",
  HEAD = "head12",
  LEVEL = 55,
  Dialog = 1,
  Reward = "[{money,1.223},{prestige,122}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 9876190
})
table.insert(normalStep, {
  ID = "{3,224}",
  HEAD = "head16",
  LEVEL = 55,
  Dialog = 3,
  Reward = "[{money,1.224},{prestige,122}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 10273724
})
table.insert(normalStep, {
  ID = "{3,225}",
  HEAD = "head25",
  LEVEL = 56,
  Dialog = 12,
  Reward = "[{money,1.225},{prestige,122}]",
  Reward2 = "[]",
  ratio_reward = "[{item,{3101,1},750,1000},{krypton,50102,200,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 12943689
})
table.insert(normalStep, {
  ID = "{3,226}",
  HEAD = "head16",
  LEVEL = 56,
  Dialog = 3,
  Reward = "[{money,1.226},{prestige,122}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 9718816
})
table.insert(normalStep, {
  ID = "{3,227}",
  HEAD = "head25",
  LEVEL = 56,
  Dialog = 2,
  Reward = "[{money,1.227},{prestige,122}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 10174428
})
table.insert(normalStep, {
  ID = "{3,228}",
  HEAD = "head13",
  LEVEL = 56,
  Dialog = 2,
  Reward = "[{money,1.228},{prestige,122}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 10515696
})
table.insert(normalStep, {
  ID = "{3,229}",
  HEAD = "head24",
  LEVEL = 56,
  Dialog = 1,
  Reward = "[{money,1.229},{prestige,122}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 11054808
})
table.insert(normalStep, {
  ID = "{3,230}",
  HEAD = "head12",
  LEVEL = 56,
  Dialog = 12,
  Reward = "[{money,1.23},{prestige,123}]",
  Reward2 = "[{krypton,50502},{krypton,50502},{kenergy,330}]",
  ratio_reward = "[{item,{3101,1},850,1000},{krypton,50302,200,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 2,
  Force = 13578856
})
table.insert(normalStep, {
  ID = "{3,231}",
  HEAD = "head24",
  LEVEL = 56,
  Dialog = 1,
  Reward = "[{money,1.231},{prestige,123}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 9920019
})
table.insert(normalStep, {
  ID = "{3,232}",
  HEAD = "head12",
  LEVEL = 57,
  Dialog = 1,
  Reward = "[{money,1.232},{prestige,123}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 10642340
})
table.insert(normalStep, {
  ID = "{3,233}",
  HEAD = "head16",
  LEVEL = 57,
  Dialog = 3,
  Reward = "[{money,1.233},{prestige,123}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 10950742
})
table.insert(normalStep, {
  ID = "{3,234}",
  HEAD = "head25",
  LEVEL = 57,
  Dialog = 2,
  Reward = "[{money,1.234},{prestige,123}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 11215853
})
table.insert(normalStep, {
  ID = "{3,235}",
  HEAD = "head13",
  LEVEL = 57,
  Dialog = 12,
  Reward = "[{money,1.235},{prestige,123}]",
  Reward2 = "[]",
  ratio_reward = "[{item,{3101,1},900,1000},{krypton,50702,200,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 14577011
})
table.insert(normalStep, {
  ID = "{3,236}",
  HEAD = "head25",
  LEVEL = 57,
  Dialog = 2,
  Reward = "[{money,1.236},{prestige,123}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 10956458
})
table.insert(normalStep, {
  ID = "{3,237}",
  HEAD = "head13",
  LEVEL = 57,
  Dialog = 2,
  Reward = "[{money,1.237},{prestige,123}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 11496240
})
table.insert(normalStep, {
  ID = "{3,238}",
  HEAD = "head24",
  LEVEL = 58,
  Dialog = 1,
  Reward = "[{money,1.238},{prestige,123}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 11569816
})
table.insert(normalStep, {
  ID = "{3,239}",
  HEAD = "head12",
  LEVEL = 58,
  Dialog = 1,
  Reward = "[{money,1.239},{prestige,123}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 11806567
})
table.insert(normalStep, {
  ID = "{3,240}",
  HEAD = "head16",
  LEVEL = 58,
  Dialog = 12,
  Reward = "[{money,1.24},{prestige,124}]",
  Reward2 = "[{krypton,50902},{krypton,50902},{kenergy,340}]",
  ratio_reward = "[{item,{3101,1},1000,1000},{krypton,50502,200,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 2,
  Force = 15211008
})
table.insert(normalStep, {
  ID = "{3,241}",
  HEAD = "head12",
  LEVEL = 58,
  Dialog = 1,
  Reward = "[{money,1.241},{prestige,124}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 11637220
})
table.insert(normalStep, {
  ID = "{3,242}",
  HEAD = "head16",
  LEVEL = 58,
  Dialog = 3,
  Reward = "[{money,1.242},{prestige,124}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 11760539
})
table.insert(normalStep, {
  ID = "{3,243}",
  HEAD = "head25",
  LEVEL = 58,
  Dialog = 2,
  Reward = "[{money,1.243},{prestige,124}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 12314313
})
table.insert(normalStep, {
  ID = "{3,244}",
  HEAD = "head13",
  LEVEL = 59,
  Dialog = 2,
  Reward = "[{money,1.244},{prestige,124}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 13857794
})
table.insert(normalStep, {
  ID = "{3,245}",
  HEAD = "head24",
  LEVEL = 59,
  Dialog = 12,
  Reward = "[{money,1.245},{prestige,124}]",
  Reward2 = "[]",
  ratio_reward = "[{item,{3101,1},750,1000},{krypton,50103,100,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 2,
  Force = 15989881
})
table.insert(normalStep, {
  ID = "{3,246}",
  HEAD = "head13",
  LEVEL = 59,
  Dialog = 2,
  Reward = "[{money,1.246},{prestige,124}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 12810174
})
table.insert(normalStep, {
  ID = "{3,247}",
  HEAD = "head24",
  LEVEL = 59,
  Dialog = 1,
  Reward = "[{money,1.247},{prestige,124}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 13003693
})
table.insert(normalStep, {
  ID = "{3,248}",
  HEAD = "head12",
  LEVEL = 59,
  Dialog = 1,
  Reward = "[{money,1.248},{prestige,124}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 13283682
})
table.insert(normalStep, {
  ID = "{3,249}",
  HEAD = "head16",
  LEVEL = 59,
  Dialog = 3,
  Reward = "[{money,1.249},{prestige,124}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 13752992
})
table.insert(normalStep, {
  ID = "{3,250}",
  HEAD = "head25",
  LEVEL = 60,
  Dialog = 12,
  Reward = "[{money,1.25},{prestige,125}]",
  Reward2 = "[{krypton,51002},{krypton,51002},{kenergy,350}]",
  ratio_reward = "[{item,{3101,1},850,1000},{krypton,50303,100,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 17318032
})
table.insert(normalStep, {
  ID = "{3,251}",
  HEAD = "head16",
  LEVEL = 60,
  Dialog = 3,
  Reward = "[{money,1.251},{prestige,125}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 13180769
})
table.insert(normalStep, {
  ID = "{3,252}",
  HEAD = "head25",
  LEVEL = 60,
  Dialog = 2,
  Reward = "[{money,1.252},{prestige,125}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 13756565
})
table.insert(normalStep, {
  ID = "{3,253}",
  HEAD = "head13",
  LEVEL = 60,
  Dialog = 2,
  Reward = "[{money,1.253},{prestige,125}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 14152854
})
table.insert(normalStep, {
  ID = "{3,254}",
  HEAD = "head24",
  LEVEL = 60,
  Dialog = 1,
  Reward = "[{money,1.254},{prestige,125}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 14232510
})
table.insert(normalStep, {
  ID = "{3,255}",
  HEAD = "head12",
  LEVEL = 60,
  Dialog = 12,
  Reward = "[{money,1.255},{prestige,125}]",
  Reward2 = "[]",
  ratio_reward = "[{item,{3101,1},900,1000},{krypton,50703,100,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 2,
  Force = 18652247
})
table.insert(normalStep, {
  ID = "{3,256}",
  HEAD = "head24",
  LEVEL = 60,
  Dialog = 1,
  Reward = "[{money,1.256},{prestige,125}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 13678545
})
table.insert(normalStep, {
  ID = "{3,257}",
  HEAD = "head12",
  LEVEL = 61,
  Dialog = 1,
  Reward = "[{money,1.257},{prestige,125}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 14422021
})
table.insert(normalStep, {
  ID = "{3,258}",
  HEAD = "head16",
  LEVEL = 61,
  Dialog = 3,
  Reward = "[{money,1.258},{prestige,125}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 15917406
})
table.insert(normalStep, {
  ID = "{3,259}",
  HEAD = "head25",
  LEVEL = 61,
  Dialog = 2,
  Reward = "[{money,1.259},{prestige,125}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 16559882
})
table.insert(normalStep, {
  ID = "{3,260}",
  HEAD = "head13",
  LEVEL = 61,
  Dialog = 12,
  Reward = "[{money,1.26},{prestige,126}]",
  Reward2 = "[{krypton,51302},{krypton,51302},{kenergy,360}]",
  ratio_reward = "[{item,{3101,1},1000,1000},{krypton,50503,100,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 20329077
})
table.insert(normalStep, {
  ID = "{3,261}",
  HEAD = "head25",
  LEVEL = 61,
  Dialog = 2,
  Reward = "[{money,1.261},{prestige,126}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 14922814
})
table.insert(normalStep, {
  ID = "{3,262}",
  HEAD = "head13",
  LEVEL = 61,
  Dialog = 2,
  Reward = "[{money,1.262},{prestige,126}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 16763956
})
table.insert(normalStep, {
  ID = "{3,263}",
  HEAD = "head24",
  LEVEL = 62,
  Dialog = 1,
  Reward = "[{money,1.263},{prestige,126}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 16921297
})
table.insert(normalStep, {
  ID = "{3,264}",
  HEAD = "head12",
  LEVEL = 62,
  Dialog = 1,
  Reward = "[{money,1.264},{prestige,126}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 17702164
})
table.insert(normalStep, {
  ID = "{3,265}",
  HEAD = "head16",
  LEVEL = 62,
  Dialog = 12,
  Reward = "[{money,1.265},{prestige,126}]",
  Reward2 = "[]",
  ratio_reward = "[{item,{3101,1},750,1000},{krypton,50103,110,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 2,
  Force = 21917266
})
table.insert(normalStep, {
  ID = "{3,266}",
  HEAD = "head12",
  LEVEL = 62,
  Dialog = 1,
  Reward = "[{money,1.266},{prestige,126}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 17028712
})
table.insert(normalStep, {
  ID = "{3,267}",
  HEAD = "head16",
  LEVEL = 62,
  Dialog = 3,
  Reward = "[{money,1.267},{prestige,126}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 17459948
})
table.insert(normalStep, {
  ID = "{3,268}",
  HEAD = "head25",
  LEVEL = 62,
  Dialog = 2,
  Reward = "[{money,1.268},{prestige,126}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 18518513
})
table.insert(normalStep, {
  ID = "{3,269}",
  HEAD = "head13",
  LEVEL = 63,
  Dialog = 2,
  Reward = "[{money,1.269},{prestige,126}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 20759230
})
table.insert(normalStep, {
  ID = "{3,270}",
  HEAD = "head24",
  LEVEL = 63,
  Dialog = 12,
  Reward = "[{money,1.27},{prestige,127}]",
  Reward2 = "[{krypton,51702},{krypton,51702},{kenergy,370}]",
  ratio_reward = "[{item,{3101,1},850,1000},{krypton,50303,110,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 23763979
})
table.insert(normalStep, {
  ID = "{3,271}",
  HEAD = "head13",
  LEVEL = 63,
  Dialog = 2,
  Reward = "[{money,1.271},{prestige,127}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 18412264
})
table.insert(normalStep, {
  ID = "{3,272}",
  HEAD = "head24",
  LEVEL = 63,
  Dialog = 1,
  Reward = "[{money,1.272},{prestige,127}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 18549520
})
table.insert(normalStep, {
  ID = "{3,273}",
  HEAD = "head12",
  LEVEL = 63,
  Dialog = 1,
  Reward = "[{money,1.273},{prestige,127}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 19597381
})
table.insert(normalStep, {
  ID = "{3,274}",
  HEAD = "head16",
  LEVEL = 63,
  Dialog = 3,
  Reward = "[{money,1.274},{prestige,127}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 20530166
})
table.insert(normalStep, {
  ID = "{3,275}",
  HEAD = "head25",
  LEVEL = 64,
  Dialog = 12,
  Reward = "[{money,1.275},{prestige,127}]",
  Reward2 = "[]",
  ratio_reward = "[{item,{3101,1},900,1000},{krypton,50703,110,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 2,
  Force = 25385243
})
table.insert(normalStep, {
  ID = "{3,276}",
  HEAD = "head16",
  LEVEL = 64,
  Dialog = 3,
  Reward = "[{money,1.276},{prestige,127}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 19129097
})
table.insert(normalStep, {
  ID = "{3,277}",
  HEAD = "head25",
  LEVEL = 64,
  Dialog = 2,
  Reward = "[{money,1.277},{prestige,127}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 20242361
})
table.insert(normalStep, {
  ID = "{3,278}",
  HEAD = "head13",
  LEVEL = 64,
  Dialog = 2,
  Reward = "[{money,1.278},{prestige,127}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 21055752
})
table.insert(normalStep, {
  ID = "{3,279}",
  HEAD = "head24",
  LEVEL = 64,
  Dialog = 1,
  Reward = "[{money,1.279},{prestige,127}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 22203816
})
table.insert(normalStep, {
  ID = "{3,280}",
  HEAD = "head12",
  LEVEL = 64,
  Dialog = 12,
  Reward = "[{money,1.28},{prestige,128}]",
  Reward2 = "[{krypton,50103},{kenergy,380}]",
  ratio_reward = "[{item,{3101,1},1000,1000},{krypton,50503,110,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 2,
  Force = 27023355
})
table.insert(normalStep, {
  ID = "{3,281}",
  HEAD = "head24",
  LEVEL = 64,
  Dialog = 1,
  Reward = "[{money,1.281},{prestige,128}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 20237035
})
table.insert(normalStep, {
  ID = "{3,282}",
  HEAD = "head12",
  LEVEL = 65,
  Dialog = 1,
  Reward = "[{money,1.282},{prestige,128}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 21362625
})
table.insert(normalStep, {
  ID = "{3,283}",
  HEAD = "head16",
  LEVEL = 65,
  Dialog = 3,
  Reward = "[{money,1.283},{prestige,128}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 21893517
})
table.insert(normalStep, {
  ID = "{3,284}",
  HEAD = "head25",
  LEVEL = 65,
  Dialog = 2,
  Reward = "[{money,1.284},{prestige,128}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 22690640
})
table.insert(normalStep, {
  ID = "{3,285}",
  HEAD = "head13",
  LEVEL = 65,
  Dialog = 12,
  Reward = "[{money,1.285},{prestige,128}]",
  Reward2 = "[]",
  ratio_reward = "[{item,{3101,1},750,1000},{krypton,50103,120,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 28838891
})
table.insert(normalStep, {
  ID = "{3,286}",
  HEAD = "head25",
  LEVEL = 65,
  Dialog = 2,
  Reward = "[{money,1.286},{prestige,128}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 22047153
})
table.insert(normalStep, {
  ID = "{3,287}",
  HEAD = "head13",
  LEVEL = 65,
  Dialog = 2,
  Reward = "[{money,1.287},{prestige,128}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 22988960
})
table.insert(normalStep, {
  ID = "{3,288}",
  HEAD = "head24",
  LEVEL = 66,
  Dialog = 1,
  Reward = "[{money,1.288},{prestige,128}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 23221906
})
table.insert(normalStep, {
  ID = "{3,289}",
  HEAD = "head12",
  LEVEL = 66,
  Dialog = 1,
  Reward = "[{money,1.289},{prestige,128}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 24038765
})
table.insert(normalStep, {
  ID = "{3,290}",
  HEAD = "head16",
  LEVEL = 66,
  Dialog = 12,
  Reward = "[{money,1.29},{prestige,129}]",
  Reward2 = "[{krypton,50303},{kenergy,390}]",
  ratio_reward = "[{item,{3101,1},850,1000},{krypton,50303,120,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 2,
  Force = 30477870
})
table.insert(normalStep, {
  ID = "{3,291}",
  HEAD = "head12",
  LEVEL = 66,
  Dialog = 1,
  Reward = "[{money,1.291},{prestige,129}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 23295625
})
table.insert(normalStep, {
  ID = "{3,292}",
  HEAD = "head16",
  LEVEL = 66,
  Dialog = 3,
  Reward = "[{money,1.292},{prestige,129}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 23749205
})
table.insert(normalStep, {
  ID = "{3,293}",
  HEAD = "head25",
  LEVEL = 66,
  Dialog = 2,
  Reward = "[{money,1.293},{prestige,129}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 25062297
})
table.insert(normalStep, {
  ID = "{3,294}",
  HEAD = "head13",
  LEVEL = 67,
  Dialog = 2,
  Reward = "[{money,1.294},{prestige,129}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 27750509
})
table.insert(normalStep, {
  ID = "{3,295}",
  HEAD = "head24",
  LEVEL = 67,
  Dialog = 12,
  Reward = "[{money,1.295},{prestige,129}]",
  Reward2 = "[]",
  ratio_reward = "[{item,{3101,1},900,1000},{krypton,50703,120,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 32279021
})
table.insert(normalStep, {
  ID = "{3,296}",
  HEAD = "head13",
  LEVEL = 67,
  Dialog = 2,
  Reward = "[{money,1.296},{prestige,129}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 26219159
})
table.insert(normalStep, {
  ID = "{3,297}",
  HEAD = "head24",
  LEVEL = 67,
  Dialog = 1,
  Reward = "[{money,1.297},{prestige,129}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 26709505
})
table.insert(normalStep, {
  ID = "{3,298}",
  HEAD = "head12",
  LEVEL = 67,
  Dialog = 1,
  Reward = "[{money,1.298},{prestige,129}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 27518558
})
table.insert(normalStep, {
  ID = "{3,299}",
  HEAD = "head16",
  LEVEL = 67,
  Dialog = 3,
  Reward = "[{money,1.299},{prestige,129}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 28612427
})
table.insert(normalStep, {
  ID = "{3,300}",
  HEAD = "head25",
  LEVEL = 68,
  Dialog = 12,
  Reward = "[{money,1.3},{prestige,130}]",
  Reward2 = "[{krypton,50703},{kenergy,400}]",
  ratio_reward = "[{item,{3101,1},1000,1000},{krypton,50503,120,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 2,
  Force = 35397582
})
table.insert(normalStep, {
  ID = "{3,301}",
  HEAD = "head16",
  LEVEL = 68,
  Dialog = 3,
  Reward = "[{money,1.301},{prestige,130}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 27054879
})
table.insert(normalStep, {
  ID = "{3,302}",
  HEAD = "head25",
  LEVEL = 68,
  Dialog = 2,
  Reward = "[{money,1.302},{prestige,130}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 28428583
})
table.insert(normalStep, {
  ID = "{3,303}",
  HEAD = "head13",
  LEVEL = 68,
  Dialog = 2,
  Reward = "[{money,1.303},{prestige,130}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 29409708
})
table.insert(normalStep, {
  ID = "{3,304}",
  HEAD = "head24",
  LEVEL = 68,
  Dialog = 1,
  Reward = "[{money,1.304},{prestige,130}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 32135137
})
table.insert(normalStep, {
  ID = "{3,305}",
  HEAD = "head12",
  LEVEL = 68,
  Dialog = 12,
  Reward = "[{money,1.305},{prestige,130}]",
  Reward2 = "[]",
  ratio_reward = "[{item,{3101,1},750,1000},{krypton,50103,130,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 39015212
})
table.insert(normalStep, {
  ID = "{3,306}",
  HEAD = "head24",
  LEVEL = 68,
  Dialog = 1,
  Reward = "[{money,1.306},{prestige,130}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 29033366
})
table.insert(normalStep, {
  ID = "{3,307}",
  HEAD = "head12",
  LEVEL = 69,
  Dialog = 1,
  Reward = "[{money,1.307},{prestige,130}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 29842584
})
table.insert(normalStep, {
  ID = "{3,308}",
  HEAD = "head16",
  LEVEL = 69,
  Dialog = 3,
  Reward = "[{money,1.308},{prestige,130}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 34103440
})
table.insert(normalStep, {
  ID = "{3,309}",
  HEAD = "head25",
  LEVEL = 69,
  Dialog = 2,
  Reward = "[{money,1.309},{prestige,130}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 35386132
})
table.insert(normalStep, {
  ID = "{3,310}",
  HEAD = "head13",
  LEVEL = 69,
  Dialog = 12,
  Reward = "[{money,1.31},{prestige,131}]",
  Reward2 = "[{krypton,50503},{kenergy,410}]",
  ratio_reward = "[{item,{3101,1},850,1000},{krypton,50303,130,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 2,
  Force = 42338366
})
table.insert(normalStep, {
  ID = "{3,311}",
  HEAD = "head25",
  LEVEL = 69,
  Dialog = 2,
  Reward = "[{money,1.311},{prestige,131}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 30806440
})
table.insert(normalStep, {
  ID = "{3,312}",
  HEAD = "head13",
  LEVEL = 69,
  Dialog = 2,
  Reward = "[{money,1.312},{prestige,131}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 35737967
})
table.insert(normalStep, {
  ID = "{3,313}",
  HEAD = "head24",
  LEVEL = 70,
  Dialog = 1,
  Reward = "[{money,1.313},{prestige,131}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 36618513
})
table.insert(normalStep, {
  ID = "{3,314}",
  HEAD = "head12",
  LEVEL = 70,
  Dialog = 1,
  Reward = "[{money,1.314},{prestige,131}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 37432433
})
table.insert(normalStep, {
  ID = "{3,315}",
  HEAD = "head16",
  LEVEL = 70,
  Dialog = 12,
  Reward = "[{money,1.315},{prestige,131}]",
  Reward2 = "[]",
  ratio_reward = "[{item,{3101,1},900,1000},{krypton,50703,130,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 46242356
})
table.insert(normalStep, {
  ID = "{3,316}",
  HEAD = "head12",
  LEVEL = 70,
  Dialog = 1,
  Reward = "[{money,1.316},{prestige,131}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 36202246
})
table.insert(normalStep, {
  ID = "{3,317}",
  HEAD = "head16",
  LEVEL = 70,
  Dialog = 3,
  Reward = "[{money,1.317},{prestige,131}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 36913686
})
table.insert(normalStep, {
  ID = "{3,318}",
  HEAD = "head25",
  LEVEL = 70,
  Dialog = 2,
  Reward = "[{money,1.318},{prestige,131}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 38978963
})
table.insert(normalStep, {
  ID = "{3,319}",
  HEAD = "head13",
  LEVEL = 71,
  Dialog = 2,
  Reward = "[{money,1.319},{prestige,131}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 42352613
})
table.insert(normalStep, {
  ID = "{3,320}",
  HEAD = "head24",
  LEVEL = 71,
  Dialog = 12,
  Reward = "[{money,1.32},{prestige,132}]",
  Reward2 = "[{krypton,50903},{kenergy,420}]",
  ratio_reward = "[{item,{3101,1},1000,1000},{krypton,50503,130,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 2,
  Force = 50238367
})
table.insert(normalStep, {
  ID = "{3,321}",
  HEAD = "head13",
  LEVEL = 71,
  Dialog = 2,
  Reward = "[{money,1.321},{prestige,132}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 38718224
})
table.insert(normalStep, {
  ID = "{3,322}",
  HEAD = "head24",
  LEVEL = 71,
  Dialog = 1,
  Reward = "[{money,1.322},{prestige,132}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 39654185
})
table.insert(normalStep, {
  ID = "{3,323}",
  HEAD = "head12",
  LEVEL = 71,
  Dialog = 1,
  Reward = "[{money,1.323},{prestige,132}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 40840851
})
table.insert(normalStep, {
  ID = "{3,324}",
  HEAD = "head16",
  LEVEL = 71,
  Dialog = 3,
  Reward = "[{money,1.324},{prestige,132}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 42491728
})
table.insert(normalStep, {
  ID = "{3,325}",
  HEAD = "head25",
  LEVEL = 72,
  Dialog = 12,
  Reward = "[{money,1.325},{prestige,132}]",
  Reward2 = "[]",
  ratio_reward = "[{item,{3101,1},750,1000},{krypton,50103,140,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 53015386
})
table.insert(normalStep, {
  ID = "{3,326}",
  HEAD = "head16",
  LEVEL = 72,
  Dialog = 3,
  Reward = "[{money,1.326},{prestige,132}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 39911030
})
table.insert(normalStep, {
  ID = "{3,327}",
  HEAD = "head25",
  LEVEL = 72,
  Dialog = 2,
  Reward = "[{money,1.327},{prestige,132}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 42083647
})
table.insert(normalStep, {
  ID = "{3,328}",
  HEAD = "head13",
  LEVEL = 72,
  Dialog = 2,
  Reward = "[{money,1.328},{prestige,132}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 43518472
})
table.insert(normalStep, {
  ID = "{3,329}",
  HEAD = "head24",
  LEVEL = 72,
  Dialog = 1,
  Reward = "[{money,1.329},{prestige,132}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 45250167
})
table.insert(normalStep, {
  ID = "{3,330}",
  HEAD = "head12",
  LEVEL = 72,
  Dialog = 12,
  Reward = "[{money,1.33},{prestige,133}]",
  Reward2 = "[{krypton,51003},{kenergy,430}]",
  ratio_reward = "[{item,{3101,1},850,1000},{krypton,50303,140,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 2,
  Force = 56090567
})
table.insert(normalStep, {
  ID = "{3,331}",
  HEAD = "head24",
  LEVEL = 72,
  Dialog = 1,
  Reward = "[{money,1.331},{prestige,133}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 42808299
})
table.insert(normalStep, {
  ID = "{3,332}",
  HEAD = "head12",
  LEVEL = 73,
  Dialog = 1,
  Reward = "[{money,1.332},{prestige,133}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 44040570
})
table.insert(normalStep, {
  ID = "{3,333}",
  HEAD = "head16",
  LEVEL = 73,
  Dialog = 3,
  Reward = "[{money,1.333},{prestige,133}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 44812583
})
table.insert(normalStep, {
  ID = "{3,334}",
  HEAD = "head25",
  LEVEL = 73,
  Dialog = 2,
  Reward = "[{money,1.334},{prestige,133}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 46395900
})
table.insert(normalStep, {
  ID = "{3,335}",
  HEAD = "head13",
  LEVEL = 73,
  Dialog = 12,
  Reward = "[{money,1.335},{prestige,133}]",
  Reward2 = "[]",
  ratio_reward = "[{item,{3101,1},900,1000},{krypton,50703,140,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 59169976
})
table.insert(normalStep, {
  ID = "{3,336}",
  HEAD = "head25",
  LEVEL = 73,
  Dialog = 2,
  Reward = "[{money,1.336},{prestige,133}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 45579352
})
table.insert(normalStep, {
  ID = "{3,337}",
  HEAD = "head13",
  LEVEL = 73,
  Dialog = 2,
  Reward = "[{money,1.337},{prestige,133}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 47087960
})
table.insert(normalStep, {
  ID = "{3,338}",
  HEAD = "head24",
  LEVEL = 74,
  Dialog = 1,
  Reward = "[{money,1.338},{prestige,133}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 48229083
})
table.insert(normalStep, {
  ID = "{3,339}",
  HEAD = "head12",
  LEVEL = 74,
  Dialog = 1,
  Reward = "[{money,1.339},{prestige,133}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 48899723
})
table.insert(normalStep, {
  ID = "{3,340}",
  HEAD = "head16",
  LEVEL = 74,
  Dialog = 12,
  Reward = "[{money,1.34},{prestige,134}]",
  Reward2 = "[{krypton,51303},{kenergy,440}]",
  ratio_reward = "[{item,{3101,1},1000,1000},{krypton,50503,140,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 2,
  Force = 62457996
})
table.insert(normalStep, {
  ID = "{3,341}",
  HEAD = "head12",
  LEVEL = 74,
  Dialog = 1,
  Reward = "[{money,1.341},{prestige,134}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 48043350
})
table.insert(normalStep, {
  ID = "{3,342}",
  HEAD = "head16",
  LEVEL = 74,
  Dialog = 3,
  Reward = "[{money,1.342},{prestige,134}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 49075730
})
table.insert(normalStep, {
  ID = "{3,343}",
  HEAD = "head25",
  LEVEL = 74,
  Dialog = 2,
  Reward = "[{money,1.343},{prestige,134}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 51850830
})
table.insert(normalStep, {
  ID = "{3,344}",
  HEAD = "head13",
  LEVEL = 75,
  Dialog = 2,
  Reward = "[{money,1.344},{prestige,134}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 56184517
})
table.insert(normalStep, {
  ID = "{3,345}",
  HEAD = "head24",
  LEVEL = 75,
  Dialog = 12,
  Reward = "[{money,1.345},{prestige,134}]",
  Reward2 = "[]",
  ratio_reward = "[{item,{3101,1},750,1000},{krypton,50103,150,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 2,
  Force = 66773061
})
table.insert(normalStep, {
  ID = "{3,346}",
  HEAD = "head13",
  LEVEL = 75,
  Dialog = 2,
  Reward = "[{money,1.346},{prestige,134}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 53863474
})
table.insert(normalStep, {
  ID = "{3,347}",
  HEAD = "head24",
  LEVEL = 75,
  Dialog = 1,
  Reward = "[{money,1.347},{prestige,134}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 55176503
})
table.insert(normalStep, {
  ID = "{3,348}",
  HEAD = "head12",
  LEVEL = 75,
  Dialog = 1,
  Reward = "[{money,1.348},{prestige,134}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 56773719
})
table.insert(normalStep, {
  ID = "{3,349}",
  HEAD = "head16",
  LEVEL = 75,
  Dialog = 3,
  Reward = "[{money,1.349},{prestige,134}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 60977371
})
table.insert(normalStep, {
  ID = "{3,350}",
  HEAD = "head25",
  LEVEL = 76,
  Dialog = 12,
  Reward = "[{money,1.35},{prestige,135}]",
  Reward2 = "[{krypton,51703},{kenergy,450}]",
  ratio_reward = "[{item,{3101,1},850,1000},{krypton,50303,150,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 73204786
})
table.insert(normalStep, {
  ID = "{3,351}",
  HEAD = "head16",
  LEVEL = 76,
  Dialog = 3,
  Reward = "[{money,1.351},{prestige,135}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 55480341
})
table.insert(normalStep, {
  ID = "{3,352}",
  HEAD = "head25",
  LEVEL = 76,
  Dialog = 2,
  Reward = "[{money,1.352},{prestige,135}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 58739178
})
table.insert(normalStep, {
  ID = "{3,353}",
  HEAD = "head13",
  LEVEL = 76,
  Dialog = 2,
  Reward = "[{money,1.353},{prestige,135}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 62601863
})
table.insert(normalStep, {
  ID = "{3,354}",
  HEAD = "head24",
  LEVEL = 76,
  Dialog = 1,
  Reward = "[{money,1.354},{prestige,135}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 69223179
})
table.insert(normalStep, {
  ID = "{3,355}",
  HEAD = "head12",
  LEVEL = 76,
  Dialog = 12,
  Reward = "[{money,1.355},{prestige,135}]",
  Reward2 = "[]",
  ratio_reward = "[{item,{3101,1},900,1000},{krypton,50703,150,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 2,
  Force = 81004588
})
table.insert(normalStep, {
  ID = "{3,356}",
  HEAD = "head24",
  LEVEL = 76,
  Dialog = 1,
  Reward = "[{money,1.356},{prestige,135}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 60523169
})
table.insert(normalStep, {
  ID = "{3,357}",
  HEAD = "head12",
  LEVEL = 77,
  Dialog = 1,
  Reward = "[{money,1.357},{prestige,135}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 63538644
})
table.insert(normalStep, {
  ID = "{3,358}",
  HEAD = "head16",
  LEVEL = 77,
  Dialog = 3,
  Reward = "[{money,1.358},{prestige,135}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 71626349
})
table.insert(normalStep, {
  ID = "{3,359}",
  HEAD = "head25",
  LEVEL = 77,
  Dialog = 2,
  Reward = "[{money,1.359},{prestige,135}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 74771496
})
table.insert(normalStep, {
  ID = "{3,360}",
  HEAD = "head13",
  LEVEL = 77,
  Dialog = 12,
  Reward = "[{money,1.36},{prestige,136}]",
  Reward2 = "[{krypton,50103},{krypton,50103},{kenergy,460}]",
  ratio_reward = "[{item,{3101,1},1000,1000},{krypton,50503,150,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 88453907
})
table.insert(normalStep, {
  ID = "{3,361}",
  HEAD = "head25",
  LEVEL = 77,
  Dialog = 2,
  Reward = "[{money,1.361},{prestige,136}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 65785764
})
table.insert(normalStep, {
  ID = "{3,362}",
  HEAD = "head13",
  LEVEL = 77,
  Dialog = 2,
  Reward = "[{money,1.362},{prestige,136}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 74980977
})
table.insert(normalStep, {
  ID = "{3,363}",
  HEAD = "head24",
  LEVEL = 78,
  Dialog = 1,
  Reward = "[{money,1.363},{prestige,136}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 78037004
})
table.insert(normalStep, {
  ID = "{3,364}",
  HEAD = "head12",
  LEVEL = 78,
  Dialog = 1,
  Reward = "[{money,1.364},{prestige,136}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 79251770
})
table.insert(normalStep, {
  ID = "{3,365}",
  HEAD = "head16",
  LEVEL = 78,
  Dialog = 12,
  Reward = "[{money,1.365},{prestige,136}]",
  Reward2 = "[]",
  ratio_reward = "[{item,{3101,1},750,1000},{item,{1013,1},100,1000},{item,{1012,1},100,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 2,
  Force = 97426948
})
table.insert(normalStep, {
  ID = "{3,366}",
  HEAD = "head12",
  LEVEL = 78,
  Dialog = 1,
  Reward = "[{money,1.366},{prestige,136}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 75943353
})
table.insert(normalStep, {
  ID = "{3,367}",
  HEAD = "head16",
  LEVEL = 78,
  Dialog = 3,
  Reward = "[{money,1.367},{prestige,136}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 77794757
})
table.insert(normalStep, {
  ID = "{3,368}",
  HEAD = "head25",
  LEVEL = 78,
  Dialog = 2,
  Reward = "[{money,1.368},{prestige,136}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 82406899
})
table.insert(normalStep, {
  ID = "{3,369}",
  HEAD = "head13",
  LEVEL = 79,
  Dialog = 2,
  Reward = "[{money,1.369},{prestige,136}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 88314173
})
table.insert(normalStep, {
  ID = "{3,370}",
  HEAD = "head24",
  LEVEL = 79,
  Dialog = 12,
  Reward = "[{money,1.37},{prestige,137}]",
  Reward2 = "[{krypton,50303},{krypton,50303},{kenergy,470}]",
  ratio_reward = "[{item,{3101,1},850,1000},{item,{1013,1},110,1000},{item,{1012,2},110,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 105712508
})
table.insert(normalStep, {
  ID = "{3,371}",
  HEAD = "head13",
  LEVEL = 79,
  Dialog = 2,
  Reward = "[{money,1.371},{prestige,137}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 82736518
})
table.insert(normalStep, {
  ID = "{3,372}",
  HEAD = "head24",
  LEVEL = 79,
  Dialog = 1,
  Reward = "[{money,1.372},{prestige,137}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 84829947
})
table.insert(normalStep, {
  ID = "{3,373}",
  HEAD = "head12",
  LEVEL = 79,
  Dialog = 1,
  Reward = "[{money,1.373},{prestige,137}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 87610308
})
table.insert(normalStep, {
  ID = "{3,374}",
  HEAD = "head16",
  LEVEL = 79,
  Dialog = 3,
  Reward = "[{money,1.374},{prestige,137}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 91273092
})
table.insert(normalStep, {
  ID = "{3,375}",
  HEAD = "head25",
  LEVEL = 80,
  Dialog = 12,
  Reward = "[{money,1.375},{prestige,137}]",
  Reward2 = "[]",
  ratio_reward = "[{item,{3101,1},900,1000},{item,{1013,1},120,1000},{item,{1012,1},120,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 2,
  Force = 112742468
})
table.insert(normalStep, {
  ID = "{3,376}",
  HEAD = "head16",
  LEVEL = 80,
  Dialog = 3,
  Reward = "[{money,1.376},{prestige,137}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 85498759
})
table.insert(normalStep, {
  ID = "{3,377}",
  HEAD = "head25",
  LEVEL = 80,
  Dialog = 2,
  Reward = "[{money,1.377},{prestige,137}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 90355141
})
table.insert(normalStep, {
  ID = "{3,378}",
  HEAD = "head13",
  LEVEL = 80,
  Dialog = 2,
  Reward = "[{money,1.378},{prestige,137}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 93704006
})
table.insert(normalStep, {
  ID = "{3,379}",
  HEAD = "head24",
  LEVEL = 80,
  Dialog = 1,
  Reward = "[{money,1.379},{prestige,137}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 95696862
})
table.insert(normalStep, {
  ID = "{3,380}",
  HEAD = "head12",
  LEVEL = 80,
  Dialog = 12,
  Reward = "[{money,1.38},{prestige,138}]",
  Reward2 = "[{krypton,50703},{krypton,50703},{kenergy,480}]",
  ratio_reward = "[{item,{3101,1},1000,1000},{item,{1013,1},130,1000},{item,{1012,2},130,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 2,
  Force = 119763729
})
table.insert(normalStep, {
  ID = "{3,381}",
  HEAD = "head24",
  LEVEL = 80,
  Dialog = 1,
  Reward = "[{money,1.381},{prestige,138}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 91889229
})
table.insert(normalStep, {
  ID = "{3,382}",
  HEAD = "head12",
  LEVEL = 81,
  Dialog = 1,
  Reward = "[{money,1.382},{prestige,138}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 94680760
})
table.insert(normalStep, {
  ID = "{3,383}",
  HEAD = "head16",
  LEVEL = 81,
  Dialog = 3,
  Reward = "[{money,1.383},{prestige,138}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 98895805
})
table.insert(normalStep, {
  ID = "{3,384}",
  HEAD = "head25",
  LEVEL = 81,
  Dialog = 2,
  Reward = "[{money,1.384},{prestige,138}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 102796203
})
table.insert(normalStep, {
  ID = "{3,385}",
  HEAD = "head13",
  LEVEL = 81,
  Dialog = 12,
  Reward = "[{money,1.385},{prestige,138}]",
  Reward2 = "[]",
  ratio_reward = "[{item,{3101,1},750,1000},{item,{1013,1},140,1000},{item,{1012,1},140,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 128716010
})
table.insert(normalStep, {
  ID = "{3,386}",
  HEAD = "head25",
  LEVEL = 81,
  Dialog = 2,
  Reward = "[{money,1.386},{prestige,138}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 97580277
})
table.insert(normalStep, {
  ID = "{3,387}",
  HEAD = "head13",
  LEVEL = 81,
  Dialog = 2,
  Reward = "[{money,1.387},{prestige,138}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 103381768
})
table.insert(normalStep, {
  ID = "{3,388}",
  HEAD = "head24",
  LEVEL = 82,
  Dialog = 1,
  Reward = "[{money,1.388},{prestige,138}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 106851665
})
table.insert(normalStep, {
  ID = "{3,389}",
  HEAD = "head12",
  LEVEL = 82,
  Dialog = 1,
  Reward = "[{money,1.389},{prestige,138}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 108181564
})
table.insert(normalStep, {
  ID = "{3,390}",
  HEAD = "head16",
  LEVEL = 82,
  Dialog = 12,
  Reward = "[{money,1.39},{prestige,139}]",
  Reward2 = "[{krypton,50503},{krypton,50503},{kenergy,490}]",
  ratio_reward = "[{item,{3101,1},850,1000},{item,{1013,1},150,1000},{item,{1012,2},150,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 2,
  Force = 135742518
})
table.insert(normalStep, {
  ID = "{3,391}",
  HEAD = "head12",
  LEVEL = 82,
  Dialog = 1,
  Reward = "[{money,1.391},{prestige,139}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 107162849
})
table.insert(normalStep, {
  ID = "{3,392}",
  HEAD = "head16",
  LEVEL = 82,
  Dialog = 3,
  Reward = "[{money,1.392},{prestige,139}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 110271318
})
table.insert(normalStep, {
  ID = "{3,393}",
  HEAD = "head25",
  LEVEL = 82,
  Dialog = 2,
  Reward = "[{money,1.393},{prestige,139}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 116441632
})
table.insert(normalStep, {
  ID = "{3,394}",
  HEAD = "head13",
  LEVEL = 83,
  Dialog = 2,
  Reward = "[{money,1.394},{prestige,139}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 124147659
})
table.insert(normalStep, {
  ID = "{3,395}",
  HEAD = "head24",
  LEVEL = 83,
  Dialog = 12,
  Reward = "[{money,1.395},{prestige,139}]",
  Reward2 = "[]",
  ratio_reward = "[{item,{3101,1},900,1000},{item,{1013,1},160,1000},{item,{1012,1},160,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 148984400
})
table.insert(normalStep, {
  ID = "{3,396}",
  HEAD = "head13",
  LEVEL = 83,
  Dialog = 2,
  Reward = "[{money,1.396},{prestige,139}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 125387833
})
table.insert(normalStep, {
  ID = "{3,397}",
  HEAD = "head24",
  LEVEL = 83,
  Dialog = 1,
  Reward = "[{money,1.397},{prestige,139}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 129567743
})
table.insert(normalStep, {
  ID = "{3,398}",
  HEAD = "head12",
  LEVEL = 83,
  Dialog = 1,
  Reward = "[{money,1.398},{prestige,139}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 131650109
})
table.insert(normalStep, {
  ID = "{3,399}",
  HEAD = "head16",
  LEVEL = 83,
  Dialog = 3,
  Reward = "[{money,1.399},{prestige,139}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 146982283
})
table.insert(normalStep, {
  ID = "{3,400}",
  HEAD = "head25",
  LEVEL = 84,
  Dialog = 12,
  Reward = "[{money,1.4},{prestige,140}]",
  Reward2 = "[{krypton,50903},{krypton,50903},{kenergy,500}]",
  ratio_reward = "[{item,{3101,1},1000,1000},{item,{1013,1},170,1000},{item,{1012,2},170,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 2,
  Force = 169480807
})
table.insert(normalStep, {
  ID = "{3,401}",
  HEAD = "head16",
  LEVEL = 84,
  Dialog = 3,
  Reward = "[{money,1.401},{prestige,140}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 129253307
})
table.insert(normalStep, {
  ID = "{3,402}",
  HEAD = "head25",
  LEVEL = 84,
  Dialog = 2,
  Reward = "[{money,1.402},{prestige,140}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 135648480
})
table.insert(normalStep, {
  ID = "{3,403}",
  HEAD = "head13",
  LEVEL = 84,
  Dialog = 2,
  Reward = "[{money,1.403},{prestige,140}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 151293663
})
table.insert(normalStep, {
  ID = "{3,404}",
  HEAD = "head24",
  LEVEL = 84,
  Dialog = 1,
  Reward = "[{money,1.404},{prestige,140}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 180277286
})
table.insert(normalStep, {
  ID = "{3,405}",
  HEAD = "head12",
  LEVEL = 84,
  Dialog = 12,
  Reward = "[{money,1.405},{prestige,140}]",
  Reward2 = "[]",
  ratio_reward = "[{item,{3101,1},750,1000},{item,{1014,1},80,1000},{item,{1012,1},80,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 192194187
})
table.insert(normalStep, {
  ID = "{3,406}",
  HEAD = "head24",
  LEVEL = 84,
  Dialog = 1,
  Reward = "[{money,1.406},{prestige,140}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 141535784
})
table.insert(normalStep, {
  ID = "{3,407}",
  HEAD = "head12",
  LEVEL = 85,
  Dialog = 1,
  Reward = "[{money,1.407},{prestige,140}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 151595896
})
table.insert(normalStep, {
  ID = "{3,408}",
  HEAD = "head16",
  LEVEL = 85,
  Dialog = 3,
  Reward = "[{money,1.408},{prestige,140}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 184485428
})
table.insert(normalStep, {
  ID = "{3,409}",
  HEAD = "head25",
  LEVEL = 85,
  Dialog = 2,
  Reward = "[{money,1.409},{prestige,140}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 192819204
})
table.insert(normalStep, {
  ID = "{3,410}",
  HEAD = "head13",
  LEVEL = 85,
  Dialog = 12,
  Reward = "[{money,1.41},{prestige,141}]",
  Reward2 = "[{krypton,51003},{krypton,51003},{kenergy,510}]",
  ratio_reward = "[{item,{3101,1},850,1000},{item,{1014,1},100,1000},{item,{1012,2},100,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 2,
  Force = 217062813
})
table.insert(normalStep, {
  ID = "{3,411}",
  HEAD = "head25",
  LEVEL = 85,
  Dialog = 2,
  Reward = "[{money,1.411},{prestige,141}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 159831371
})
table.insert(normalStep, {
  ID = "{3,412}",
  HEAD = "head13",
  LEVEL = 85,
  Dialog = 2,
  Reward = "[{money,1.412},{prestige,141}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 192185574
})
table.insert(normalStep, {
  ID = "{3,413}",
  HEAD = "head24",
  LEVEL = 86,
  Dialog = 1,
  Reward = "[{money,1.413},{prestige,141}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 201035884
})
table.insert(normalStep, {
  ID = "{3,414}",
  HEAD = "head12",
  LEVEL = 86,
  Dialog = 1,
  Reward = "[{money,1.414},{prestige,141}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 203433194
})
table.insert(normalStep, {
  ID = "{3,415}",
  HEAD = "head16",
  LEVEL = 86,
  Dialog = 12,
  Reward = "[{money,1.415},{prestige,141}]",
  Reward2 = "[]",
  ratio_reward = "[{item,{3101,1},900,1000},{item,{1015,1},50,1000},{item,{1012,2},50,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 244242621
})
table.insert(normalStep, {
  ID = "{3,416}",
  HEAD = "head12",
  LEVEL = 86,
  Dialog = 1,
  Reward = "[{money,1.416},{prestige,141}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 192306010
})
table.insert(normalStep, {
  ID = "{3,417}",
  HEAD = "head16",
  LEVEL = 86,
  Dialog = 3,
  Reward = "[{money,1.417},{prestige,141}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 200996839
})
table.insert(normalStep, {
  ID = "{3,418}",
  HEAD = "head25",
  LEVEL = 86,
  Dialog = 2,
  Reward = "[{money,1.418},{prestige,141}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 213529525
})
table.insert(normalStep, {
  ID = "{3,419}",
  HEAD = "head13",
  LEVEL = 87,
  Dialog = 2,
  Reward = "[{money,1.419},{prestige,141}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 224702426
})
table.insert(normalStep, {
  ID = "{3,420}",
  HEAD = "head24",
  LEVEL = 87,
  Dialog = 12,
  Reward = "[{money,1.42},{prestige,142}]",
  Reward2 = "[{krypton,51303},{krypton,51303},{kenergy,520}]",
  ratio_reward = "[{item,{3101,1},1000,1000},{item,{1013,1},180,1000},{item,{1012,3},180,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 2,
  Force = 270754755
})
table.insert(normalStep, {
  ID = "{3,421}",
  HEAD = "head13",
  LEVEL = 87,
  Dialog = 2,
  Reward = "[{money,1.421},{prestige,142}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 209711355
})
table.insert(normalStep, {
  ID = "{3,422}",
  HEAD = "head24",
  LEVEL = 87,
  Dialog = 1,
  Reward = "[{money,1.422},{prestige,142}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 219418462
})
table.insert(normalStep, {
  ID = "{3,423}",
  HEAD = "head12",
  LEVEL = 87,
  Dialog = 1,
  Reward = "[{money,1.423},{prestige,142}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 221103948
})
table.insert(normalStep, {
  ID = "{3,424}",
  HEAD = "head16",
  LEVEL = 87,
  Dialog = 3,
  Reward = "[{money,1.424},{prestige,142}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 234223240
})
table.insert(normalStep, {
  ID = "{3,425}",
  HEAD = "head25",
  LEVEL = 88,
  Dialog = 12,
  Reward = "[{money,1.425},{prestige,142}]",
  Reward2 = "[]",
  ratio_reward = "[{item,{3101,1},750,1000},{item,{1014,1},90,1000},{item,{1012,1},90,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 286067452
})
table.insert(normalStep, {
  ID = "{3,426}",
  HEAD = "head16",
  LEVEL = 88,
  Dialog = 3,
  Reward = "[{money,1.426},{prestige,142}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 218717484
})
table.insert(normalStep, {
  ID = "{3,427}",
  HEAD = "head25",
  LEVEL = 88,
  Dialog = 2,
  Reward = "[{money,1.427},{prestige,142}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 231893058
})
table.insert(normalStep, {
  ID = "{3,428}",
  HEAD = "head13",
  LEVEL = 88,
  Dialog = 2,
  Reward = "[{money,1.428},{prestige,142}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 239594142
})
table.insert(normalStep, {
  ID = "{3,429}",
  HEAD = "head24",
  LEVEL = 88,
  Dialog = 1,
  Reward = "[{money,1.429},{prestige,142}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 247055889
})
table.insert(normalStep, {
  ID = "{3,430}",
  HEAD = "head12",
  LEVEL = 88,
  Dialog = 12,
  Reward = "[{money,1.43},{prestige,143}]",
  Reward2 = "[{krypton,51703},{krypton,51703},{kenergy,530}]",
  ratio_reward = "[{item,{3101,1},850,1000},{item,{1014,1},110,1000},{item,{1012,2},110,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 2,
  Force = 304872528
})
table.insert(normalStep, {
  ID = "{3,431}",
  HEAD = "head24",
  LEVEL = 88,
  Dialog = 1,
  Reward = "[{money,1.431},{prestige,143}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 238397228
})
table.insert(normalStep, {
  ID = "{3,432}",
  HEAD = "head12",
  LEVEL = 89,
  Dialog = 1,
  Reward = "[{money,1.432},{prestige,143}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 239401951
})
table.insert(normalStep, {
  ID = "{3,433}",
  HEAD = "head16",
  LEVEL = 89,
  Dialog = 3,
  Reward = "[{money,1.433},{prestige,143}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 249954924
})
table.insert(normalStep, {
  ID = "{3,434}",
  HEAD = "head25",
  LEVEL = 89,
  Dialog = 2,
  Reward = "[{money,1.434},{prestige,143}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 260031002
})
table.insert(normalStep, {
  ID = "{3,435}",
  HEAD = "head13",
  LEVEL = 89,
  Dialog = 12,
  Reward = "[{money,1.435},{prestige,143}]",
  Reward2 = "[]",
  ratio_reward = "[{item,{3101,1},900,1000},{item,{1015,1},60,1000},{item,{1012,2},60,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 322493746
})
table.insert(normalStep, {
  ID = "{3,436}",
  HEAD = "head25",
  LEVEL = 89,
  Dialog = 2,
  Reward = "[{money,1.436},{prestige,143}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 255052013
})
table.insert(normalStep, {
  ID = "{3,437}",
  HEAD = "head13",
  LEVEL = 89,
  Dialog = 2,
  Reward = "[{money,1.437},{prestige,143}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 263315503
})
table.insert(normalStep, {
  ID = "{3,438}",
  HEAD = "head24",
  LEVEL = 90,
  Dialog = 1,
  Reward = "[{money,1.438},{prestige,143}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 274722485
})
table.insert(normalStep, {
  ID = "{3,439}",
  HEAD = "head12",
  LEVEL = 90,
  Dialog = 1,
  Reward = "[{money,1.439},{prestige,143}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 295346774
})
table.insert(normalStep, {
  ID = "{3,440}",
  HEAD = "head16",
  LEVEL = 90,
  Dialog = 12,
  Reward = "[{money,1.44},{prestige,144}]",
  Reward2 = "[{krypton,50104},{kenergy,540}]",
  ratio_reward = "[{item,{3101,1},1000,1000},{item,{1013,1},190,1000},{item,{1012,3},190,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 2,
  Force = 343662327
})
table.insert(normalStep, {
  ID = "{3,441}",
  HEAD = "head12",
  LEVEL = 90,
  Dialog = 1,
  Reward = "[{money,1.441},{prestige,144}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 297323161
})
table.insert(normalStep, {
  ID = "{3,442}",
  HEAD = "head16",
  LEVEL = 90,
  Dialog = 3,
  Reward = "[{money,1.442},{prestige,144}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 308830238
})
table.insert(normalStep, {
  ID = "{3,443}",
  HEAD = "head25",
  LEVEL = 90,
  Dialog = 2,
  Reward = "[{money,1.443},{prestige,144}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 328806361
})
table.insert(normalStep, {
  ID = "{3,444}",
  HEAD = "head13",
  LEVEL = 91,
  Dialog = 2,
  Reward = "[{money,1.444},{prestige,144}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 347090111
})
table.insert(normalStep, {
  ID = "{3,445}",
  HEAD = "head24",
  LEVEL = 91,
  Dialog = 12,
  Reward = "[{money,1.445},{prestige,144}]",
  Reward2 = "[]",
  ratio_reward = "[{item,{3101,1},750,1000},{item,{1014,1},100,1000},{item,{1012,1},100,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 2,
  Force = 379199894
})
table.insert(normalStep, {
  ID = "{3,446}",
  HEAD = "head13",
  LEVEL = 91,
  Dialog = 2,
  Reward = "[{money,1.446},{prestige,144}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 314486149
})
table.insert(normalStep, {
  ID = "{3,447}",
  HEAD = "head24",
  LEVEL = 91,
  Dialog = 1,
  Reward = "[{money,1.447},{prestige,144}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 320840786
})
table.insert(normalStep, {
  ID = "{3,448}",
  HEAD = "head12",
  LEVEL = 91,
  Dialog = 1,
  Reward = "[{money,1.448},{prestige,144}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 337019959
})
table.insert(normalStep, {
  ID = "{3,449}",
  HEAD = "head16",
  LEVEL = 91,
  Dialog = 3,
  Reward = "[{money,1.449},{prestige,144}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 371412429
})
table.insert(normalStep, {
  ID = "{3,450}",
  HEAD = "head25",
  LEVEL = 92,
  Dialog = 12,
  Reward = "[{money,1.45},{prestige,145}]",
  Reward2 = "[{krypton,50304},{kenergy,550}]",
  ratio_reward = "[{item,{3101,1},850,1000},{item,{1014,1},120,1000},{item,{1012,2},120,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 422209022
})
table.insert(normalStep, {
  ID = "{3,451}",
  HEAD = "head16",
  LEVEL = 92,
  Dialog = 3,
  Reward = "[{money,1.451},{prestige,145}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 368995037
})
table.insert(normalStep, {
  ID = "{3,452}",
  HEAD = "head25",
  LEVEL = 92,
  Dialog = 2,
  Reward = "[{money,1.452},{prestige,145}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 394746843
})
table.insert(normalStep, {
  ID = "{3,453}",
  HEAD = "head13",
  LEVEL = 92,
  Dialog = 2,
  Reward = "[{money,1.453},{prestige,145}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 426050000
})
table.insert(normalStep, {
  ID = "{3,454}",
  HEAD = "head24",
  LEVEL = 92,
  Dialog = 1,
  Reward = "[{money,1.454},{prestige,145}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 460735696
})
table.insert(normalStep, {
  ID = "{3,455}",
  HEAD = "head12",
  LEVEL = 92,
  Dialog = 12,
  Reward = "[{money,1.455},{prestige,145}]",
  Reward2 = "[]",
  ratio_reward = "[{item,{3101,1},900,1000},{item,{1015,1},70,1000},{item,{1012,3},70,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 2,
  Force = 483794100
})
table.insert(normalStep, {
  ID = "{3,456}",
  HEAD = "head24",
  LEVEL = 92,
  Dialog = 1,
  Reward = "[{money,1.456},{prestige,145}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 431272003
})
table.insert(normalStep, {
  ID = "{3,457}",
  HEAD = "head12",
  LEVEL = 93,
  Dialog = 1,
  Reward = "[{money,1.457},{prestige,145}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 460462819
})
table.insert(normalStep, {
  ID = "{3,458}",
  HEAD = "head16",
  LEVEL = 93,
  Dialog = 3,
  Reward = "[{money,1.458},{prestige,145}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 491382300
})
table.insert(normalStep, {
  ID = "{3,459}",
  HEAD = "head25",
  LEVEL = 93,
  Dialog = 2,
  Reward = "[{money,1.459},{prestige,145}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 520508938
})
table.insert(normalStep, {
  ID = "{3,460}",
  HEAD = "head13",
  LEVEL = 93,
  Dialog = 12,
  Reward = "[{money,1.46},{prestige,146}]",
  Reward2 = "[{krypton,50704},{kenergy,560}]",
  ratio_reward = "[{item,{3101,1},1000,1000},{item,{1013,1},200,1000},{item,{1012,4},200,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 574982222
})
table.insert(normalStep, {
  ID = "{3,461}",
  HEAD = "head25",
  LEVEL = 93,
  Dialog = 2,
  Reward = "[{money,1.461},{prestige,146}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 423466853
})
table.insert(normalStep, {
  ID = "{3,462}",
  HEAD = "head13",
  LEVEL = 93,
  Dialog = 2,
  Reward = "[{money,1.462},{prestige,146}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 484170428
})
table.insert(normalStep, {
  ID = "{3,463}",
  HEAD = "head24",
  LEVEL = 94,
  Dialog = 1,
  Reward = "[{money,1.463},{prestige,146}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 538549973
})
table.insert(normalStep, {
  ID = "{3,464}",
  HEAD = "head12",
  LEVEL = 94,
  Dialog = 1,
  Reward = "[{money,1.464},{prestige,146}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 559157285
})
table.insert(normalStep, {
  ID = "{3,465}",
  HEAD = "head16",
  LEVEL = 94,
  Dialog = 12,
  Reward = "[{money,1.465},{prestige,146}]",
  Reward2 = "[]",
  ratio_reward = "[{item,{3101,1},750,1000},{item,{1014,1},110,1000},{item,{1012,1},110,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 2,
  Force = 642106002
})
table.insert(normalStep, {
  ID = "{3,466}",
  HEAD = "head12",
  LEVEL = 94,
  Dialog = 1,
  Reward = "[{money,1.466},{prestige,146}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 482154265
})
table.insert(normalStep, {
  ID = "{3,467}",
  HEAD = "head16",
  LEVEL = 94,
  Dialog = 3,
  Reward = "[{money,1.467},{prestige,146}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 539121676
})
table.insert(normalStep, {
  ID = "{3,468}",
  HEAD = "head25",
  LEVEL = 94,
  Dialog = 2,
  Reward = "[{money,1.468},{prestige,146}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 590422019
})
table.insert(normalStep, {
  ID = "{3,469}",
  HEAD = "head13",
  LEVEL = 95,
  Dialog = 2,
  Reward = "[{money,1.469},{prestige,146}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 621524963
})
table.insert(normalStep, {
  ID = "{3,470}",
  HEAD = "head24",
  LEVEL = 95,
  Dialog = 12,
  Reward = "[{money,1.47},{prestige,147}]",
  Reward2 = "[{krypton,50504},{kenergy,570}]",
  ratio_reward = "[{item,{3101,1},850,1000},{item,{1014,1},130,1000},{item,{1012,2},130,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 733803654
})
table.insert(normalStep, {
  ID = "{3,471}",
  HEAD = "head13",
  LEVEL = 95,
  Dialog = 2,
  Reward = "[{money,1.471},{prestige,147}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 567131255
})
table.insert(normalStep, {
  ID = "{3,472}",
  HEAD = "head24",
  LEVEL = 95,
  Dialog = 1,
  Reward = "[{money,1.472},{prestige,147}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 633899308
})
table.insert(normalStep, {
  ID = "{3,473}",
  HEAD = "head12",
  LEVEL = 95,
  Dialog = 1,
  Reward = "[{money,1.473},{prestige,147}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 642404552
})
table.insert(normalStep, {
  ID = "{3,474}",
  HEAD = "head16",
  LEVEL = 95,
  Dialog = 3,
  Reward = "[{money,1.474},{prestige,147}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 647121366
})
table.insert(normalStep, {
  ID = "{3,475}",
  HEAD = "head25",
  LEVEL = 96,
  Dialog = 12,
  Reward = "[{money,1.475},{prestige,147}]",
  Reward2 = "[]",
  ratio_reward = "[{item,{3101,1},900,1000},{item,{1015,1},80,1000},{item,{1012,3},80,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 2,
  Force = 787273693
})
table.insert(normalStep, {
  ID = "{3,476}",
  HEAD = "head16",
  LEVEL = 96,
  Dialog = 3,
  Reward = "[{money,1.476},{prestige,147}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 604906097
})
table.insert(normalStep, {
  ID = "{3,477}",
  HEAD = "head25",
  LEVEL = 96,
  Dialog = 2,
  Reward = "[{money,1.477},{prestige,147}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 643342271
})
table.insert(normalStep, {
  ID = "{3,478}",
  HEAD = "head13",
  LEVEL = 96,
  Dialog = 2,
  Reward = "[{money,1.478},{prestige,147}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 659863922
})
table.insert(normalStep, {
  ID = "{3,479}",
  HEAD = "head24",
  LEVEL = 96,
  Dialog = 1,
  Reward = "[{money,1.479},{prestige,147}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 740834311
})
table.insert(normalStep, {
  ID = "{3,480}",
  HEAD = "head12",
  LEVEL = 96,
  Dialog = 12,
  Reward = "[{money,1.48},{prestige,148}]",
  Reward2 = "[{krypton,50105},{kenergy,580}]",
  ratio_reward = "[{item,{3101,1},1000,1000},{item,{1013,1},210,1000},{item,{1012,4},210,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 2,
  Force = 877309930
})
table.insert(normalStep, {
  ID = "{3,481}",
  HEAD = "head24",
  LEVEL = 96,
  Dialog = 1,
  Reward = "[{money,1.481},{prestige,148}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 686123615
})
table.insert(normalStep, {
  ID = "{3,482}",
  HEAD = "head12",
  LEVEL = 97,
  Dialog = 1,
  Reward = "[{money,1.482},{prestige,148}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 691509483
})
table.insert(normalStep, {
  ID = "{3,483}",
  HEAD = "head16",
  LEVEL = 97,
  Dialog = 3,
  Reward = "[{money,1.483},{prestige,148}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 723770408
})
table.insert(normalStep, {
  ID = "{3,484}",
  HEAD = "head25",
  LEVEL = 97,
  Dialog = 2,
  Reward = "[{money,1.484},{prestige,148}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 753361366
})
table.insert(normalStep, {
  ID = "{3,485}",
  HEAD = "head13",
  LEVEL = 97,
  Dialog = 12,
  Reward = "[{money,1.485},{prestige,148}]",
  Reward2 = "[{krypton,50305},{kenergy,585}]",
  ratio_reward = "[{item,{3101,1},750,1000},{item,{1014,1},120,1000},{item,{1012,2},120,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 898068518
})
table.insert(normalStep, {
  ID = "{3,486}",
  HEAD = "head25",
  LEVEL = 97,
  Dialog = 2,
  Reward = "[{money,1.486},{prestige,148}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 715815351
})
table.insert(normalStep, {
  ID = "{3,487}",
  HEAD = "head13",
  LEVEL = 97,
  Dialog = 2,
  Reward = "[{money,1.487},{prestige,148}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 768461522
})
table.insert(normalStep, {
  ID = "{3,488}",
  HEAD = "head24",
  LEVEL = 98,
  Dialog = 1,
  Reward = "[{money,1.488},{prestige,148}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 811423720
})
table.insert(normalStep, {
  ID = "{3,489}",
  HEAD = "head12",
  LEVEL = 98,
  Dialog = 1,
  Reward = "[{money,1.489},{prestige,148}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 879992160
})
table.insert(normalStep, {
  ID = "{3,490}",
  HEAD = "head16",
  LEVEL = 98,
  Dialog = 12,
  Reward = "[{money,1.49},{prestige,149}]",
  Reward2 = "[{krypton,50705},{kenergy,590}]",
  ratio_reward = "[{item,{3101,1},850,1000},{item,{1014,1},140,1000},{item,{1012,3},140,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 2,
  Force = 996330885
})
table.insert(normalStep, {
  ID = "{3,491}",
  HEAD = "head12",
  LEVEL = 98,
  Dialog = 1,
  Reward = "[{money,1.491},{prestige,149}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 759094364
})
table.insert(normalStep, {
  ID = "{3,492}",
  HEAD = "head16",
  LEVEL = 98,
  Dialog = 3,
  Reward = "[{money,1.492},{prestige,149}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 791412204
})
table.insert(normalStep, {
  ID = "{3,493}",
  HEAD = "head25",
  LEVEL = 98,
  Dialog = 2,
  Reward = "[{money,1.493},{prestige,149}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 844819650
})
table.insert(normalStep, {
  ID = "{3,494}",
  HEAD = "head13",
  LEVEL = 99,
  Dialog = 2,
  Reward = "[{money,1.494},{prestige,149}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 895041810
})
table.insert(normalStep, {
  ID = "{3,495}",
  HEAD = "head24",
  LEVEL = 99,
  Dialog = 12,
  Reward = "[{money,1.495},{prestige,149}]",
  Reward2 = "[{krypton,50505},{kenergy,595}]",
  ratio_reward = "[{item,{3101,1},900,1000},{item,{1015,1},90,1000},{item,{1012,4},90,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 1088283994
})
table.insert(normalStep, {
  ID = "{3,496}",
  HEAD = "head13",
  LEVEL = 99,
  Dialog = 2,
  Reward = "[{money,1.496},{prestige,149}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 834033808
})
table.insert(normalStep, {
  ID = "{3,497}",
  HEAD = "head24",
  LEVEL = 99,
  Dialog = 1,
  Reward = "[{money,1.497},{prestige,149}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 877477355
})
table.insert(normalStep, {
  ID = "{3,498}",
  HEAD = "head12",
  LEVEL = 99,
  Dialog = 1,
  Reward = "[{money,1.498},{prestige,149}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 914613281
})
table.insert(normalStep, {
  ID = "{3,499}",
  HEAD = "head16",
  LEVEL = 99,
  Dialog = 3,
  Reward = "[{money,1.499},{prestige,149}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 965162543
})
table.insert(normalStep, {
  ID = "{3,500}",
  HEAD = "head25",
  LEVEL = 100,
  Dialog = 12,
  Reward = "[{money,1.5},{prestige,150}]",
  Reward2 = "[{krypton,52105},{kenergy,600}]",
  ratio_reward = "[{item,{3101,1},1000,1000},{item,{1015,1},100,1000},{item,{1012,5},100,1000}]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 2,
  Force = 1144962145
})
