local GameUIStopNotice = LuaObjectManager:GetLuaObject("GameUIStopNotice")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
GameUIStopNotice.curData = nil
GameUIStopNotice.activeData = nil
function GameUIStopNotice:OnInitGame()
end
function GameUIStopNotice:Show(content)
  GameUIStopNotice.curData = content
  GameUIStopNotice:GenerateData()
  if GameUIStopNotice.activeData.leftTime > 0 then
    GameStateManager:GetCurrentGameState():AddObject(GameUIStopNotice)
  else
    GameUIStopNotice.curData = nil
  end
end
function GameUIStopNotice:OnAddToGameState()
  DebugOut("GameUIStopNotice:OnAddToGameState")
  self:LoadFlashObject()
  GameTimer:Add(self._OnTimerTick, 1000)
  GameUIStopNotice:SetBasicData()
  GameUIStopNotice._OnTimerTick()
  GameUIStopNotice:MoveIn()
end
function GameUIStopNotice:OnEraseFromGameState()
  GameUIStopNotice.activeData = nil
  self:UnloadFlashObject()
  collectgarbage("collect")
end
function GameUIStopNotice:OnFSCommand(cmd, arg)
  if cmd == "close" then
    GameStateManager:GetCurrentGameState():EraseObject(GameUIStopNotice)
  elseif cmd == "ShowDetail" then
    local index = tonumber(arg)
    local item = GameUIStopNotice.activeData.rewardList[index]
    if item then
      if item.item_type == "item" then
        item.cnt = 1
        ItemBox:showItemBox("Item", item, tonumber(item.number), 320, 480, 200, 200, "", nil, "", nil)
      elseif item.item_type == "fleet" then
        local fleetID = item.number
        ItemBox:ShowCommanderDetail2(fleetID, nil, nil)
      end
    end
  end
end
function GameUIStopNotice:GenerateData()
  if GameUIStopNotice.curData and GameUIStopNotice.curData ~= "" then
    loadstring(GameUIStopNotice.curData.content)()
    DebugOut("GameUIStopNotice:GenerateData", os.time())
    DebugTable(notice)
    local data = {}
    data.contentText = notice.contentText
    data.leftTime = GameUIStopNotice.curData.left_time or 0
    data.curBaseTime = os.time()
    data.rewardList = {}
    for k, v in pairs(notice.list or {}) do
      local item = v
      item.frame = GameHelper:GetAwardTypeIconFrameName(item.item_type, item.number, item.no)
      data.rewardList[#data.rewardList + 1] = item
    end
    GameUIStopNotice.activeData = data
  end
end
function GameUIStopNotice:SetBasicData()
  if GameUIStopNotice:GetFlashObject() then
    GameUIStopNotice:GetFlashObject():InvokeASCallback("_root", "SetBasicData", GameUIStopNotice.activeData)
  end
end
function GameUIStopNotice:MoveIn()
  if GameUIStopNotice:GetFlashObject() then
    GameUIStopNotice:GetFlashObject():InvokeASCallback("_root", "MoveIn")
  end
end
function GameUIStopNotice._OnTimerTick()
  if GameUIStopNotice.activeData then
    local left = GameUIStopNotice.activeData.leftTime - (os.time() - GameUIStopNotice.activeData.curBaseTime)
    if left < 0 then
      left = 0
    end
    if GameUIStopNotice:GetFlashObject() then
      GameUIStopNotice:GetFlashObject():InvokeASCallback("_root", "upTimeStr", GameUtils:formatTimeStringAsTwitterStyle3(left))
      if left == 0 then
        GameUIStopNotice:GetFlashObject():InvokeASCallback("_root", "MoveOut")
      end
    end
  end
  return 1000
end
function GameUIStopNotice:Update(dt)
  local flashObj = GameUIStopNotice:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "onUpdateFrame", dt)
    flashObj:Update(dt)
  end
end
if AutoUpdate.isAndroidDevice then
  function GameUIStopNotice.OnAndroidBack()
    if GameUIStopNotice:GetFlashObject() then
      GameUIStopNotice:GetFlashObject():InvokeASCallback("_root", "MoveOut")
    end
  end
end
