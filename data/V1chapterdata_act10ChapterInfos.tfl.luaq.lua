local ChapterInfos = GameData.chapterdata_act10.ChapterInfos
ChapterInfos[1] = {
  ChaperID = 1,
  ICON = "icon1",
  MapIndex = 1,
  BossPos = 5,
  EntroStory = {
    10101,
    10102,
    10103
  },
  AFTER_FINISH = {}
}
ChapterInfos[2] = {
  ChaperID = 2,
  ICON = "icon2",
  MapIndex = 2,
  BossPos = 10,
  EntroStory = {
    10201,
    10202,
    10203,
    10204
  },
  AFTER_FINISH = {}
}
ChapterInfos[3] = {
  ChaperID = 3,
  ICON = "icon30",
  MapIndex = 3,
  BossPos = 16,
  EntroStory = {
    10301,
    10302,
    10303
  },
  AFTER_FINISH = {}
}
ChapterInfos[4] = {
  ChaperID = 4,
  ICON = "icon400",
  MapIndex = 1,
  BossPos = 18,
  EntroStory = {
    10401,
    10402,
    10403,
    10404
  },
  AFTER_FINISH = {}
}
ChapterInfos[5] = {
  ChaperID = 5,
  ICON = "icon5",
  MapIndex = 1,
  BossPos = 18,
  EntroStory = {10501, 10502},
  AFTER_FINISH = {}
}
ChapterInfos[6] = {
  ChaperID = 6,
  ICON = "icon400",
  MapIndex = 1,
  BossPos = 18,
  EntroStory = {
    10601,
    10602,
    10603
  },
  AFTER_FINISH = {}
}
ChapterInfos[7] = {
  ChaperID = 7,
  ICON = "icon6",
  MapIndex = 1,
  BossPos = 18,
  EntroStory = {10701, 10702},
  AFTER_FINISH = {}
}
ChapterInfos[8] = {
  ChaperID = 8,
  ICON = "icon400",
  MapIndex = 1,
  BossPos = 18,
  EntroStory = {
    10801,
    10802,
    10803,
    10804,
    10805,
    10806
  },
  AFTER_FINISH = {}
}
ChapterInfos[9] = {
  ChaperID = 9,
  ICON = "icon5",
  MapIndex = 1,
  BossPos = 18,
  EntroStory = {},
  AFTER_FINISH = {}
}
ChapterInfos[10] = {
  ChaperID = 10,
  ICON = "icon5",
  MapIndex = 1,
  BossPos = 18,
  EntroStory = {},
  AFTER_FINISH = {}
}
