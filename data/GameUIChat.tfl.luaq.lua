require("EmojiConfig.tfl")
require("GameTextEdit.tfl")
local GameStateMainPlanet = GameStateManager.GameStateMainPlanet
local NetMessageMgr = NetMessageMgr
local NetAPIList = NetAPIList
local GameGlobalData = GameGlobalData
local GameStateWorldBoss = GameStateManager.GameStateWorldBoss
local GameStateColonization = GameStateManager.GameStateColonization
local GameStateStarCraft = GameStateManager.GameStateStarCraft
local GameStateWD = GameStateManager.GameStateWD
local GameUIChat = LuaObjectManager:GetLuaObject("GameUIChat")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameUIPlayerDetailInfo = LuaObjectManager:GetLuaObject("GameUIPlayerDetailInfo")
local GameUIStarCraftMap = LuaObjectManager:GetLuaObject("GameUIStarCraftMap")
local GameUIBarLeft = LuaObjectManager:GetLuaObject("GameUIBarLeft")
local GameObjectMainPlanet = LuaObjectManager:GetLuaObject("GameObjectMainPlanet")
local GameVipDetailInfoPanel = LuaObjectManager:GetLuaObject("GameVipDetailInfoPanel")
local GameUILargeMap = LuaObjectManager:GetLuaObject("GameUILargeMap")
GameUIChat.MessageStack = {}
GameUIChat.MessageStack.world = {}
GameUIChat.MessageStack.alliance = {}
GameUIChat.MessageStack.largemap_world = {}
GameUIChat.MessageStack.whisper = {}
GameUIChat.MessageStack.battle = {}
GameUIChat.MessageStack.largemap_country = {}
GameUIChat.MessageStack.max_size = 100
GameUIChat.lastMessage = ""
GameUIChat.lastSendTime = 0
GameUIChat.last_receiver = nil
GameUIChat.content_buffer = nil
GameUIChat.ThanksGivenChampionUserID = "-1i"
GameUIChat.worldNum = 0
GameUIChat.battleNum = 0
GameUIChat.allianceNum = 0
GameUIChat.whisperNum = 0
GameUIChat.largemapWorldNum = 0
GameUIChat.largemapCountryNum = 0
GameUIChat.activeChannel = ""
GameUIChat.isTranslate = false
GameUIChat.mTranslateResultList = {}
GameUIChat.mTranslateResultListBackup = {}
GameUIChat.mIsTranslateOpen = true
GameUIChat.lastSendIndex = nil
GameUIChat.lastSendChannel = nil
GameUIChat.lastSendMsg = ""
GameUIChat.curColor = {
  world = 0,
  battle = 0,
  alliance = 0,
  whisper = 0,
  largemap_country = 0,
  largemap_world = 0
}
GameUIChat.mCurTranslateMsg = {channelName = nil, msgIdx = -1}
local lastedTimestamp = 0
function GameUIChat:Open(name_channel, name_receiver, msg_content)
  DebugOut("name_channel, name_receiver, msg_content", name_channel, name_receiver, msg_content)
  if name_receiver then
    self.last_receiver = name_receiver
  end
  if msg_content then
    self.content_buffer = msg_content
  end
  self:SelectChannel(name_channel)
  local activeGameState = GameStateManager:GetCurrentGameState()
  if not activeGameState:IsObjectInState(self) then
    activeGameState:AddObject(self)
  end
end
function GameUIChat:ClearChatMessageOnLogIn()
end
function GameUIChat:OnInitGame()
  g_checkEmojiResDownload = GameUIChat.CheckEmojiPic
end
function GameUIChat:OnAddToGameState(state)
  if GameUtils:GetTutorialHelp() then
    local pos = self:getMenuPos("_root._main.s_1")
    if pos then
      local GameUIMaskLayer = LuaObjectManager:GetLuaObject("GameUIMaskLayer")
      GameUtils:ShowMaskLayer(tonumber(pos[1]), tonumber(pos[2]), GameUIMaskLayer.ButtonPos.right, GameLoader:GetGameText("LC_MENU_ASSISTANT_INFO_25"), GameUIMaskLayer.MaskStyle.small)
    end
  end
  self:LoadFlashObject()
  self:GetFlashObject():InvokeASCallback("_root", "setIsLargeState", GameStateManager:GetCurrentGameState() == GameStateManager.GameStateLargeMap)
  local alliance_info = GameGlobalData:GetData("alliance")
  if alliance_info.id == "" then
    DebugOut("OnAddToGameState:")
    DebugOut(self.activeChannel)
    if self.activeChannel == "alliance" then
      self.activeChannel = "world"
    end
    GameUIChat.allianceNum = 0
    GameUIChat.MessageStack.alliance = {}
    self:GetFlashObject():InvokeASCallback("_root", "setChannelEnable", "alliance", false)
  end
  GameObjectMainPlanet:CheckBattleChanelState()
  if GameUIChat.mChannelStatus then
    if not GameUIChat:IsBattleChannelOpened() then
      self:GetFlashObject():InvokeASCallback("_root", "setChannelEnable", "battle", false)
    end
  elseif not GameStateStarCraft.mStarCraftActive and not GameStateWD.inActive then
    self:GetFlashObject():InvokeASCallback("_root", "setChannelEnable", "battle", false)
  end
  if GameStateManager:GetCurrentGameState() == GameStateManager.GameStateLargeMap then
    local inCountry = GameUILargeMap.MyPlayerData.alliance_info.id ~= ""
    self:GetFlashObject():InvokeASCallback("_root", "setChannelEnable", "largemap_country", inCountry)
    if inCountry and self.activeChannel == "largemap_country" then
      self.activeChannel = "largemap_country"
    elseif not inCountry and self.activeChannel == "largemap_country" then
      self.activeChannel = "largemap_world"
    end
  end
  self:SelectChannel(self.activeChannel)
  self:MoveInChat()
  if GameStateStarCraft == GameStateManager:GetCurrentGameState() and GameUIStarCraftMap.firstTimeEnterChat == true then
    GameUIStarCraftMap.firstTimeEnterChat = false
  end
  if GameStateManager.GameStateWD == GameStateManager:GetCurrentGameState() and GameStateWD.firstTimeEnterChat == true then
    GameStateWD.firstTimeEnterChat = false
  end
  if GameStateStarCraft ~= GameStateManager:GetCurrentGameState() and GameStateManager.GameStateWD ~= GameStateManager:GetCurrentGameState() and GameUIBarLeft.firstTimeEnterChat == true then
    GameUIBarLeft.firstTimeEnterChat = false
  end
  RegisterKeyboardHideNotify(GameUIChat.onKeyboardHide)
  GameTextEdit.TouchInputCallback = GameUIChat.onKeyboardShow
end
function GameUIChat._OnTimerTick()
  if GameStateManager:GetCurrentGameState():IsObjectInState(GameUIChat) then
    if GameUIChat.lastSendIndex then
      local message_table = GameUIChat.MessageStack[GameUIChat.lastSendChannel]
      local message_info = message_table[GameUIChat.lastSendIndex]
      message_info.isfail = true
      GameUIChat.lastSendMsg = ""
      GameUIChat:UpdateMessageItem(GameUIChat.lastSendChannel, GameUIChat.lastSendIndex, nil)
      GameUIChat.lastSendIndex = nil
      GameUIChat.lastSendChannel = nil
    end
    return nil
  end
  return nil
end
function GameUIChat:OnEraseFromGameState(state)
  RemoveKeyboardHideNotify(GameUIChat.onKeyboardHide)
  GameTextEdit.TouchInputCallback = nil
  GameUIChat.isOnOpretion = nil
  self:UnloadFlashObject()
end
function GameUIChat:InitFlashObject()
  self:GetFlashObject():InvokeASCallback("_root", "onFlashObjectLoaded")
end
function GameUIChat:SetMessageContent(msg_content)
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "setMessageContent", msg_content)
end
function GameUIChat:OpenInputBox(msg_content)
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "openInputBox")
end
function GameUIChat:SelectChannel(nameChannel)
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  local changeText = ""
  if nameChannel == "world" then
    GameUIChat.worldNum = 0
    changeText = GameLoader:GetGameText("LC_MENU_CHANNEL_WORLD")
  elseif nameChannel == "whisper" then
    GameUIChat.whisperNum = 0
    changeText = GameLoader:GetGameText("LC_MENU_CHANNEL_WHISPER")
  elseif nameChannel == "battle" then
    GameUIChat.battleNum = 0
    changeText = GameLoader:GetGameText("LC_MENU_CHANNEL_BATTLE")
  elseif nameChannel == "alliance" then
    GameUIChat.allianceNum = 0
    changeText = GameLoader:GetGameText("LC_MENU_CHANNEL_ALLIANCE")
  elseif nameChannel == "largemap_world" then
    GameUIChat.largemapWorldNum = 0
    changeText = GameLoader:GetGameText("LC_MENU_MAP_CHAT_CHANNEL_WORLD")
  elseif nameChannel == "largemap_country" then
    GameUIChat.largemapCountryNum = 0
    changeText = GameLoader:GetGameText("LC_MENU_MAP_CHAT_CHANNEL_LEGION")
  end
  GameUIChat:SaveReadTimeStamp(nameChannel)
  self:GetFlashObject():InvokeASCallback("_root", "selectChannel", nameChannel, changeText)
  GameUIChat.activeChannel = nameChannel
  local msg_content = GameUIChat:GetMessageInputText()
  if nameChannel == "whisper" then
    if self.last_receiver then
      msg_content = "@" .. self.last_receiver .. " "
    end
  else
    msg_content = ""
  end
  if self.content_buffer then
    msg_content = msg_content .. self.content_buffer
  end
  self:SetMessageContent(msg_content)
  if msg_content then
    local htmlTxt = LuaUtils:ConvertEmojiUnicode2Html(msg_content)
    GameUIChat:SetMessageInputContentHtml(htmlTxt)
  end
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    local messageTable = self.MessageStack[nameChannel]
    local curtime = os.time()
    local newtable = {}
    DebugOut("curtime", curtime)
    for k, v in ipairs(messageTable) do
      if not (curtime - v.timestamp > 3600) or v.sender_role ~= nil then
        table.insert(newtable, v)
      end
    end
    DebugOut("before")
    DebugTable(messageTable)
    DebugTable(newtable)
    self.MessageStack[nameChannel] = newtable
    collectgarbage("collect")
    DebugOut("kasjkdasdjk:", nameChannel)
    DebugTable(self.MessageStack[nameChannel])
    flash_obj:InvokeASCallback("_root", "InitChatList", nameChannel, #self.MessageStack[nameChannel])
  end
end
function GameUIChat:SyncMessageItemWithChannelName(channelName, isMySend)
  if self:GetFlashObject() then
    local messageTable = self.MessageStack[channelName]
    assert(messageTable)
    self:GetFlashObject():InvokeASCallback("_root", "syncListboxItemCount", channelName, #messageTable, isMysend)
  end
end
function GameUIChat:AddMessageWithChannelName(channelName)
  if self:GetFlashObject() then
    local messageTable = self.MessageStack[channelName]
    assert(messageTable)
    self:GetFlashObject():InvokeASCallback("_root", "syncListboxItemCount", channelName, #messageTable)
  end
end
function GameUIChat:SetMessageItem(indexMessage, senderName, timeStr, msgContent, isSenderMe, arenaRank, tag, playerType, isKing, showTransBtn, mySend, isFail, colorIndex, vipChatInfo)
  local transBtnFr = "Idle"
  if not showTransBtn then
    transBtnFr = "highlight"
  end
  local flash_obj = GameUIChat:GetFlashObject()
  if flash_obj then
    flash_obj:InvokeASCallback("_root", "setTransButtonState", indexMessage, transBtnFr)
    flash_obj:InvokeASCallback("_root", "setMessageItem", indexMessage, senderName, timeStr, msgContent, isSenderMe, arenaRank, tag, playerType, isKing, GameUIChat.mIsTranslateOpen, transBtnFr, mySend, isFail, colorIndex, vipChatInfo)
  end
end
function GameUIChat:UpdateMessageItem(channel_name, indexMessage, translateContent)
  local message_table = GameUIChat.MessageStack[channel_name]
  local message_info = message_table[indexMessage]
  local flash_obj = GameUIChat:GetFlashObject()
  if message_info then
    if message_info.mysend then
      DebugOut("aaaaa-aaaaaa:333 ", message_info.content, self.lastSendMsg)
      if message_info.content == self.lastSendMsg then
        self.lastSendIndex = indexMessage
        self.lastSendChannel = channel_name
      end
    end
    local sender_me = false
    if GameGlobalData:GetData("userinfo").name == message_info.sender_name then
      sender_me = true
    end
    DebugOut("fuck_UpdateMessageItem")
    DebugTable(message_info)
    local arena_rank = -1
    if message_info.champion_rank and message_info.champion_rank <= 500 then
      arena_rank = message_info.champion_rank
    end
    local tag = 0
    local msgContent = message_info.content
    if message_info.emojiContent then
      msgContent = message_info.emojiContent
    else
      msgContent = LuaUtils:ConvertEmojiUnicode2Html(msgContent)
      message_info.emojiContent = msgContent
    end
    msgContent = LuaUtils:ConvertEmojiUnicode2Html(msgContent)
    local showTransBtn = true
    if message_info.translateResult and "" ~= message_info.translateResult then
      msgContent = message_info.translateResult
      showTransBtn = false
    end
    if translateContent then
      msgContent = translateContent
      showTransBtn = false
    end
    local playerType = tonumber(message_info.sender_role)
    if playerType == 1 then
      msgContent = " <font color='#EEEE00'> " .. msgContent .. "</font>"
    elseif playerType == 3 then
      msgContent = " <font color='#FFBBFF'> " .. msgContent .. "</font>"
    end
    local icon_frame = "head1"
    local isKing = false
    if message_info.tc_king and 1 == message_info.tc_king then
      isKing = true
    end
    DebugOut(msgContent)
    local rank = ""
    if message_info.wdc_img == nil or message_info.wdc_img == "" then
      local leaderlist = GameGlobalData:GetData("leaderlist")
      local curMatrixIndex = GameGlobalData.GlobalData.curMatrixIndex
      DebugTable(leaderlist)
      DebugOut(curMatrixIndex)
      if message_info.sex == 1 then
        icon_frame = "male"
      elseif message_info.sex == 0 then
        icon_frame = "female"
      elseif message_info.sex then
        icon_frame = GameDataAccessHelper:GetFleetAvatar(message_info.sex, 0)
      elseif leaderlist and curMatrixIndex and leaderlist.leader_ids[curMatrixIndex] then
        icon_frame = GameDataAccessHelper:GetFleetAvatar(leaderlist.leader_ids[curMatrixIndex], 0)
      else
        icon_frame = "female"
      end
    else
      icon_frame = message_info.wdc_img
      local extInfo = {}
      extInfo.mcIndex = indexMessage
      extInfo.frameName = message_info.wdc_img
      extInfo.channel = channel_name
      local function callback(extinfo)
        if GameUIChat:GetFlashObject() then
          GameUIChat:GetFlashObject():InvokeASCallback("_root", "setWDCIcon", extinfo.channel, extinfo.mcIndex, extinfo.frameName)
        end
      end
      icon_frame = GameUtils:GetPlayerRankingInWdc(icon_frame, extInfo, callback)
    end
    if message_info.wdc_rank ~= nil and message_info.wdc_ranking ~= nil and message_info.wdc_rank ~= 0 then
      rank = GameUtils:GetPlayerRank(message_info.wdc_rank, message_info.wdc_ranking)
      tag = 1
    else
      tag = 1
    end
    local nameText = GameUtils:GetUserDisplayName(message_info.sender_name)
    if playerType == nil then
      playerType = 4
    end
    local vipChatInfo = GameDataAccessHelper:GetVipChatInfo(message_info.viplevel)
    vipChatInfo.showEffect = message_info.show_effect
    GameUIChat:SetMessageItem(indexMessage, nameText, GameUtils:formatTimeStringAsTwitterStyleWithOutSecond(GameUIBarLeft:GetServerTimeUTC() - message_info.timestamp), msgContent, sender_me, arena_rank, tag, playerType, icon_frame, showTransBtn, message_info.mysend, message_info.isfail, message_info.colorIndex, vipChatInfo)
  end
end
function GameUIChat:GetActiveChannel()
  local flashObj = self:GetFlashObject()
  if flashObj then
    return ...
  end
  return nil
end
function GameUIChat:GetActiveMessageStack()
  return self.MessageStack[self:GetActiveChannel()]
end
function GameUIChat.CallbackOfTranslate(success, result)
  DebugOut("GameUIChat.CallbackOfTranslate ")
  local flashObj = GameUIChat:GetFlashObject()
  if success then
    DebugOut("Translate result : ")
    DebugTable(result)
    if GameUIChat.mCurTranslateMsg.channelName and GameUIChat.mCurTranslateMsg.channelName == GameUIChat:GetActiveChannel() then
      local msg_table = GameUIChat.MessageStack[GameUIChat.mCurTranslateMsg.channelName]
      local msg_info = msg_table[GameUIChat.mCurTranslateMsg.msgIdx]
      if msg_info then
        local resultContent = LuaUtils:ConvertEmojiUnicode2Html(result.translatedText)
        msg_info.translateResult = resultContent
        msg_info.cachetranslate = resultContent
        GameUIChat:UpdateMessageItem(GameUIChat.mCurTranslateMsg.channelName, GameUIChat.mCurTranslateMsg.msgIdx, resultContent)
        local backupMsg = {
          content = msg_info.content,
          timestamp = msg_info.timestamp,
          translateResult = msg_info.translateResult
        }
        table.insert(GameUIChat.mTranslateResultList, backupMsg)
        if flashObj then
          flashObj:InvokeASCallback("_root", "RefreshListItemHeight")
        end
      else
        DebugOut("Translate msg item is removed.")
      end
    end
  else
    DebugOut("Translate failed, errText : ")
    if flashObj then
      flashObj:InvokeASCallback("_root", "setTransButtonState", GameUIChat.mCurTranslateMsg.msgIdx, "Idle")
    end
  end
  GameUIChat.isTranslate = false
  GameUIChat.mCurTranslateMsg.channelName = nil
  GameUIChat.mCurTranslateMsg.msgIdx = -1
end
function GameUIChat:TranslateMessage(channelName, index_message)
  if nil == GameUIChat.mCurTranslateMsg.channelName and index_message and index_message > 0 then
    local msg_table = self:GetActiveMessageStack()
    local msg_info = msg_table[index_message]
    assert(msg_info)
    local flashObj = GameUIChat:GetFlashObject()
    if msg_info then
      if nil ~= msg_info.translateResult then
        msg_info.translateResult = nil
        GameUIChat:UpdateMessageItem(channelName, index_message)
        if flashObj then
          flashObj:InvokeASCallback("_root", "setTransButtonState", index_message, "Idle")
        end
      else
        if msg_info.cachetranslate ~= nil then
          msg_info.translateResult = msg_info.cachetranslate
          GameUIChat:UpdateMessageItem(channelName, index_message, msg_info.cachetranslate)
          return
        end
        if GameUIChat.isTranslate then
          return
        end
        if flashObj then
          flashObj:InvokeASCallback("_root", "setTransButtonState", index_message, "highlight")
        end
        GameUIChat.isTranslate = true
        GameUIChat.mCurTranslateMsg.channelName = channelName
        GameUIChat.mCurTranslateMsg.msgIdx = index_message
        DebugTable(GameUIChat.mCurTranslateMsg)
        DebugOut("Translate src: " .. msg_info.content)
        GameUtils:Translate(msg_info.content, GameUIChat.CallbackOfTranslate)
      end
    end
  end
end
function GameUIChat:SelectMessage(index_message)
  local flash_obj = self:GetFlashObject()
  if index_message and index_message > 0 then
    local msg_table = self:GetActiveMessageStack()
    local msg_info = msg_table[index_message]
    assert(msg_info)
    local user_info = GameGlobalData:GetData("userinfo")
    if msg_info.sender == nil then
      return
    elseif user_info.player_id == msg_info.sender then
    end
    self.selected_message = msg_info
    flash_obj:SetText("_root.operation.body.sender_name_text", GameUtils:GetUserDisplayName(msg_info.sender_name))
    if GameUtils:GetActiveServerInfo().tag == GameUtils:GetUserServerName(msg_info.sender_name) then
      self:MoveInOperation()
    end
  else
    self.selected_message = nil
  end
  flash_obj:InvokeASCallback("_root", "selectMessage", index_message)
end
function GameUIChat:MoveInChat()
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "moveinChat")
end
function GameUIChat:MoveOutChat()
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "moveoutChat")
end
function GameUIChat:MoveInOperation()
  self:GetFlashObject():InvokeASCallback("_root", "moveinOperation")
  GameUIChat.isOnOpretion = true
end
function GameUIChat:MoveOutOperation()
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    self:GetFlashObject():InvokeASCallback("_root", "moveoutOperation")
  end
  GameUIChat.isOnOpretion = nil
end
if AutoUpdate.isAndroidDevice then
  function GameUIChat.OnAndroidOperationBack()
    GameUIChat.isOnOpretion = nil
    GameUIChat:SelectMessage(-1)
  end
end
function GameUIChat:Visible(is_visible, mc_path)
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "setVisible", mc_path, is_visible)
end
function GameUIChat:OperationView(arg)
  local coordinate = LuaUtils:string_split(arg, "\001")
  GameUIPlayerDetailInfo:Show(self.selected_message.sender, tonumber(coordinate[1]), tonumber(coordinate[2]))
end
function GameUIChat:OperationWhisper()
  self:MoveOutOperation()
  self.last_receiver = self.selected_message.sender_name
  self:SelectChannel("whisper")
  self.isOnOpretion = nil
end
function GameUIChat:OperationBlock()
  local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
  GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
  local function AddtoBlacklistCallback()
    local content = {
      id = self.selected_message.sender,
      name = self.selected_message.sender_name
    }
    NetMessageMgr:SendMsg(NetAPIList.friend_ban_req.Code, content, self.chatCallback, true, nil)
  end
  GameUIMessageDialog:SetYesButton(AddtoBlacklistCallback, {})
  local info_title = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
  local info_content = GameLoader:GetGameText("LC_MENU_BLOCK_CONFIRM_ALERT")
  GameUIMessageDialog:Display(info_title, info_content)
end
function GameUIChat:OperationReport()
  local informant_content = {}
  informant_content.evidences = {}
  informant_content.wrongdoer = self.selected_message.sender_name
  local active_msg_stack = self:GetActiveMessageStack()
  local index_active_msg
  for index_msg = 1, #active_msg_stack do
    if active_msg_stack[index_msg] == self.selected_message then
      index_active_msg = index_msg
      break
    end
  end
  assert(index_active_msg)
  local index_msg_start = index_active_msg - 2
  local index_msg_end = index_active_msg + 2
  for index_msg = index_msg_start, index_msg_end do
    local msg = active_msg_stack[index_msg]
    if msg then
      local chat_info = {}
      chat_info.sender = msg.sender_name
      chat_info.content = msg.content
      if index_msg == index_active_msg then
        chat_info.inappropriate = 1
      else
        chat_info.inappropriate = 0
      end
      table.insert(informant_content.evidences, chat_info)
    end
  end
  local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
  GameStateManager:GetCurrentGameState():AddObject(ItemBox)
  ItemBox:ShowReport(informant_content)
end
function GameUIChat:Update(dt)
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    flash_obj:InvokeASCallback("_root", "onUpdateFrame", dt)
    local contentText = GameTextEdit:GetCurrentContent()
    flash_obj:InvokeASCallback("_root", "inputContent", contentText)
    local display1 = GameUIChat.worldNum
    local display2 = GameUIChat.allianceNum
    local display3 = GameUIChat.whisperNum
    local display4 = GameUIChat.battleNum
    if GameStateManager:GetCurrentGameState() == GameStateManager.GameStateLargeMap then
      display2 = GameUIChat.largemapCountryNum
      display4 = GameUIChat.largemapWorldNum
    end
    GameUIChat:CheckUpdateChatBtnStatus()
    if display1 > 99 then
      display1 = "99+"
    else
      display1 = "" .. display1
    end
    if display2 > 99 then
      display2 = "99+"
    else
      display2 = "" .. display2
    end
    if display3 > 99 then
      display3 = "99+"
    else
      display3 = "" .. display3
    end
    if display4 > 99 then
      display4 = "99+"
    else
      display4 = "" .. display4
    end
    if GameUIChat.mChannelStatus then
      if not GameUIChat:IsBattleChannelOpened() and GameStateManager:GetCurrentGameState() ~= GameStateManager.GameStateLargeMap then
        display4 = "0"
      end
    elseif not GameStateStarCraft.mStarCraftActive and not GameStateWD.inActive and GameStateManager:GetCurrentGameState() ~= GameStateManager.GameStateLargeMap then
      display4 = "0"
    end
    flash_obj:InvokeASCallback("_root", "setUnreadMsgCount", display1, display2, display3, display4)
    flash_obj:Update(dt)
    if self.channelNtyTime > 0 then
      self.channelNtyTime = self.channelNtyTime - 1
      if self.channelNtyTime == 0 then
        self:SelectChannel(self.activeChannel)
      end
    end
  end
end
function GameUIChat:CheckUpdateChatBtnStatus()
  local newChatTotalCount = GameUIChat.worldNum + GameUIChat.whisperNum
  if GameStateManager:GetCurrentGameState() == GameStateManager.GameStateLargeMap then
    newChatTotalCount = GameUIChat.worldNum + GameUIChat.whisperNum + GameUIChat.largemapWorldNum
    if GameUILargeMap.MyPlayerData.alliance_info.id ~= "" then
      newChatTotalCount = GameUIChat.worldNum + GameUIChat.whisperNum + GameUIChat.largemapWorldNum + GameUIChat.largemapCountryNum
    end
  else
    newChatTotalCount = GameUIChat.worldNum + GameUIChat.whisperNum + GameUIChat.allianceNum + GameUIChat.battleNum
    if GameUIChat.mChannelStatus then
      if not GameUIChat:IsBattleChannelOpened() then
        newChatTotalCount = GameUIChat.worldNum + GameUIChat.allianceNum + GameUIChat.whisperNum
      end
    elseif not GameStateStarCraft.mStarCraftActive and not GameStateWD.inActive then
      newChatTotalCount = GameUIChat.worldNum + GameUIChat.allianceNum + GameUIChat.whisperNum
    end
  end
  local oldCount = GameGlobalData:GetData("newChatTotalCount")
  if oldCount == nil then
    if newChatTotalCount > 0 then
      GameGlobalData:UpdateGameData("newChatTotalCount", newChatTotalCount)
      GameGlobalData:RefreshData("newChatTotalCount")
    end
  elseif oldCount ~= newChatTotalCount then
    GameGlobalData:UpdateGameData("newChatTotalCount", newChatTotalCount)
    GameGlobalData:RefreshData("newChatTotalCount")
  end
end
function GameUIChat.UpdateChatMessage(content)
  DebugOut("GameUIChat.UpdateChatMessage")
  DebugTable(content)
  local message_channel = ""
  content.time_relative = GameTimer:ConvertTimeToHReadbleTime(content.timestamp)
  if content.channel == "global_chat_channel" then
    if content.receiver ~= "" then
      message_channel = "whisper"
      if content.sender_role ~= nil and GameUIChat:CheckIsRead(message_channel, content.timestamp) == false and (not GameUIChat:GetFlashObject() or GameUIChat:GetActiveChannel() ~= message_channel) then
        GameUIChat.whisperNum = GameUIChat.whisperNum + 1
      end
    else
      message_channel = "world"
      if content.sender_role ~= nil and GameUIChat:CheckIsRead(message_channel, content.timestamp) == false and (not GameUIChat:GetFlashObject() or GameUIChat:GetActiveChannel() ~= message_channel) then
        GameUIChat.worldNum = GameUIChat.worldNum + 1
      end
    end
  elseif content.channel == "tc_chat_channel" then
    message_channel = "battle"
    if content.sender_role ~= nil and GameUIChat:CheckIsRead(message_channel, content.timestamp) == false and (not GameUIChat:GetFlashObject() or GameUIChat:GetActiveChannel() ~= message_channel) then
      GameUIChat.battleNum = GameUIChat.battleNum + 1
    end
  elseif content.channel == "largemap_world_channel" then
    message_channel = "largemap_world"
    if content.sender_role ~= nil and GameUIChat:CheckIsRead(message_channel, content.timestamp) == false and (not GameUIChat:GetFlashObject() or GameUIChat:GetActiveChannel() ~= message_channel) then
      GameUIChat.largemapWorldNum = GameUIChat.largemapWorldNum + 1
    end
  elseif content.channel == "largemap_country_channel" then
    message_channel = "largemap_country"
    if content.sender_role ~= nil and GameUIChat:CheckIsRead(message_channel, content.timestamp) == false and (not GameUIChat:GetFlashObject() or GameUIChat:GetActiveChannel() ~= message_channel) then
      GameUIChat.largemapCountryNum = GameUIChat.largemapCountryNum + 1
    end
  else
    message_channel = "alliance"
    if content.sender_role ~= nil and GameUIChat:CheckIsRead(message_channel, content.timestamp) == false and (not GameUIChat:GetFlashObject() or GameUIChat:GetActiveChannel() ~= message_channel) then
      GameUIChat.allianceNum = GameUIChat.allianceNum + 1
    end
  end
  local message_table = GameUIChat.MessageStack[message_channel]
  assert(message_table)
  if lastedTimestamp < content.timestamp then
    lastedTimestamp = content.timestamp
    GameUIChat:CheckUpdateChatBtnStatus()
  end
  DebugOut("chatNum", GameUIChat.whisperNum, GameUIChat.worldNum, GameUIChat.allianceNum, GameUIChat.battleNum)
  local translateMsg = GameUIChat:GetTranslateMsgFromOldMsg(content.content, content.timestamp)
  if translateMsg then
    content.translateResult = translateMsg.translateResult
    content.cachetranslate = content.translateResult
    content.emojiContent = translateMsg.emojiContent
    table.insert(GameUIChat.mTranslateResultList, translateMsg)
  end
  if GameUIChat:ReplayOldMsg(message_channel, content.origin_content, content) == false and GameUIChat:CheckMsgInList(message_channel, content.content, content.timestamp) == false then
    DebugOut("aaaaa-aaaaaa:111 ", message_channel)
    content.colorIndex = GameUIChat.curColor[message_channel] + 1
    GameUIChat.curColor[message_channel] = GameUIChat.curColor[message_channel] + 1
    table.insert(message_table, content)
    while #message_table > GameUIChat.MessageStack.max_size do
      table.remove(message_table, 1)
    end
    if GameUIChat.mCurTranslateMsg.channelName and GameUIChat.mCurTranslateMsg.channelName == message_channel then
      if 1 == GameUIChat.mCurTranslateMsg.msgIdx then
        GameUIChat.mCurTranslateMsg.channelName = nil
        GameUIChat.mCurTranslateMsg.msgIdx = -1
      else
        GameUIChat.mCurTranslateMsg.msgIdx = GameUIChat.mCurTranslateMsg.msgIdx - 1
      end
    end
    GameUIChat:SyncMessageItemWithChannelName(message_channel, content.mysend or false)
  else
    DebugOut("aaaaa-aaaaaa:222 ", GameUIChat.lastSendIndex)
    if GameUIChat.lastSendIndex then
      GameUIChat:UpdateMessageItem(GameUIChat.lastSendChannel, GameUIChat.lastSendIndex, nil)
    end
    GameUIChat.lastSendIndex = nil
    GameUIChat.lastSendChannel = nil
  end
end
function GameUIChat.chatCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.friend_ban_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.add_friend_ack.Code then
    local GameTip = LuaObjectManager:GetLuaObject("GameTip")
    local info_success = ""
    GameTip:Show(info_success)
    return true
  end
  return false
end
function GameUIChat.NetCallbackSendMessage(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.chat_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      local message_table = GameUIChat.MessageStack[GameUIChat.lastSendChannel]
      local message_info = message_table[GameUIChat.lastSendIndex]
      message_info.isfail = true
      GameUIChat.lastSendMsg = ""
      GameUIChat:UpdateMessageItem(GameUIChat.lastSendChannel, GameUIChat.lastSendIndex, nil)
      GameUIChat.lastSendIndex = nil
      GameUIChat.lastSendChannel = nil
    elseif GameUIChat.lastSendChannel == "whisper" then
    end
    return true
  end
  return false
end
function GameUIChat:SendMessage(sendMsg, index)
  if not sendMsg or sendMsg == "" then
    return
  end
  if self.lastSendIndex ~= nil then
    local GameTip = LuaObjectManager:GetLuaObject("GameTip")
    GameTip:Show(GameLoader:GetGameText("LC_MENU_CHAT_TIP_DISCONECT"))
    return
  end
  if index ~= "nil" then
    self.lastSendIndex = tonumber(index)
    self.lastSendChannel = self:GetActiveChannel()
  end
  DebugOut("GameUIChat:SendMessage " .. sendMsg)
  local levelInfo = GameGlobalData:GetData("levelinfo")
  if not AutoUpdate.isTestGW and levelInfo.level < 10 then
    GameUtils:ShowTips(GameLoader:GetGameText("LC_MENU_DO_NOT_CHAT_ALERT"))
    return
  end
  local GameTip = LuaObjectManager:GetLuaObject("GameTip")
  if DebugCommand then
    if LuaUtils:string_startswith(sendMsg, "gm:") then
      local cmd_gm = string.sub(sendMsg, 4)
      NetMessageMgr:SendMsg(NetAPIList.gm_cmd_req.Code, {cmd = cmd_gm}, nil, nil)
      local info_execute_gmcommand = "GMCommand(" .. cmd_gm .. ")"
      GameTip:Show(info_execute_gmcommand)
      DebugOut(info_execute_gmcommand)
      self:SetMessageContent("")
      return
    end
    if LuaUtils:string_startswith(sendMsg, "dbg:") and GameUtils:IsTestGateWay() then
      local debug_cmd = string.sub(sendMsg, 5)
      DebugOut("DebugCommand:excute(" .. debug_cmd .. ")")
      DebugCommand:excute(debug_cmd)
      self:SetMessageContent("")
      return
    end
  end
  if string.len(sendMsg) > 200 then
    sendMsg = string.sub(sendMsg, 1, 200)
  end
  local msg_receiver = ""
  local channelString = "global_chat_channel"
  local activeChannel = self:GetActiveChannel()
  if activeChannel == "world" then
  elseif activeChannel == "alliance" then
    local alliance_info = GameGlobalData:GetData("alliance")
    channelString = "alchat_" .. alliance_info.id
  elseif activeChannel == "whisper" then
    if string.find(sendMsg, "@") == 1 then
      local eindex_name = string.find(sendMsg, " ")
      if eindex_name then
        msg_receiver = string.sub(sendMsg, 2, eindex_name - 1)
        sendMsg = string.sub(sendMsg, eindex_name + 1)
        self._lastReceiver = msg_receiver
      else
        return
      end
    else
      local txt = GameLoader:GetGameText("LC_MENU_WHISPER_WRONG_FORMAT")
      GameTip:Show(txt)
      return
    end
  elseif activeChannel == "largemap_world" then
    channelString = "largemap_world_channel"
  elseif activeChannel == "largemap_country" then
    channelString = "largemap_country_channel"
  elseif activeChannel == "battle" then
    channelString = "tc_chat_channel"
  else
    assert(false)
  end
  if GameSettingData.EnableChatEffect == nil then
    GameSettingData.EnableChatEffect = true
  end
  local chat_req = {
    content = GameUtils:EscapeHtmlStr(sendMsg),
    receiver = msg_receiver,
    channel = channelString,
    show_effect = GameSettingData.EnableChatEffect and 1 or 0
  }
  if chat_req.content == "" then
    return
  end
  DebugOut("chat_req")
  DebugTable(chat_req)
  if GameUIChat.lastMessage == sendMsg and 10 >= os.time() - GameUIChat.lastSendTime then
    GameUtils:ShowTips(GameLoader:GetGameText("LC_MENU_CHAT_REPEAT_CHAT"))
    if self.lastSendIndex then
      local message_table = GameUIChat.MessageStack[GameUIChat.lastSendChannel]
      local message_info = message_table[GameUIChat.lastSendIndex]
      message_info.isfail = true
      GameUIChat.lastSendMsg = ""
      GameUIChat:UpdateMessageItem(GameUIChat.lastSendChannel, GameUIChat.lastSendIndex, nil)
      GameUIChat.lastSendIndex = nil
      GameUIChat.lastSendChannel = nil
    end
    return
  else
    GameUIChat.lastSendTime = os.time()
    GameUIChat.lastMessage = sendMsg
  end
  NetMessageMgr:SendMsg(NetAPIList.chat_req.Code, chat_req, self.NetCallbackSendMessage, false, nil)
  if activeChannel == "whisper" then
    self.lastSendMsg = "@" .. msg_receiver .. " " .. chat_req.content
  else
    self.lastSendMsg = chat_req.content
  end
  if index == "nil" then
    local chatContent = {}
    local levelinfo = GameGlobalData:GetData("levelinfo")
    local userinfo = GameGlobalData:GetData("userinfo")
    chatContent.sender_level = levelinfo.level
    chatContent.sender = userinfo.player_id
    chatContent.receiver = msg_receiver
    chatContent.channel = channelString
    chatContent.sender_name = userinfo.name
    chatContent.timestamp = os.time()
    chatContent.viplevel = GameVipDetailInfoPanel:GetVipLevel()
    chatContent.show_effect = GameSettingData.EnableChatEffect and 1 or 0
    chatContent.sex = 0
    if activeChannel == "whisper" then
      chatContent.content = "@" .. msg_receiver .. " " .. chat_req.content
    else
      chatContent.content = chat_req.content
    end
    self.lastSendMsg = chatContent.content
    chatContent.mysend = true
    GameUIChat.UpdateChatMessage(chatContent)
    self.lastSendChannel = self:GetActiveChannel()
  end
  self:SetMessageContent("")
end
function GameUIChat:OnFSCommand(cmd, arg)
  GameTextEdit:fscommand(self:GetFlashObject(), cmd, arg)
  DebugOut("\230\137\147\229\141\176 --- " .. cmd, arg)
  if cmd == "update_message_item" then
    local channel_name, index = unpack(LuaUtils:string_split(arg, ","))
    GameUIChat:UpdateMessageItem(channel_name, tonumber(index))
  elseif cmd == "pressedInBG" then
    if GameTextEdit:GetKeyboard() then
      self.m_donotMoveOut = true
    end
  elseif cmd == "releasedInBG" then
    if not self.m_donotMoveOut then
      GameUIChat:MoveOutChat()
      GameUIChat:MoveOutOperation()
    end
    self.m_donotMoveOut = false
  elseif cmd == "close" then
    if arg == "all" then
      GameUIChat:MoveOutChat()
      GameUIChat:MoveOutOperation()
    elseif arg == "operation" then
      GameUIChat:SelectMessage(-1)
    else
      assert(false)
    end
  elseif cmd == "animation_over_movein" then
    if self.MessageBuffer then
      self:OpenMessageEditbox(self.MessageBuffer)
      self.MessageBuffer = nil
    end
  elseif cmd == "animation_over_moveout" then
    if arg == "chat" then
      GameStateManager:GetCurrentGameState():EraseObject(self)
      local GameUIBarLeft = LuaObjectManager:GetLuaObject("GameUIBarLeft")
      GameUIBarLeft:ShowChatIcon()
      if GameStateManager:GetCurrentGameState() == GameStateWorldBoss then
        local GameObjectWorldBoss = LuaObjectManager:GetLuaObject("GameObjectWorldBoss")
        GameObjectWorldBoss:ShowChatIcon()
      elseif GameStateManager:GetCurrentGameState() == GameStateColonization then
        local GameUIColonial = LuaObjectManager:GetLuaObject("GameUIColonial")
        GameUIColonial:ShowChatIcon()
        local GameUISlaveSelect = LuaObjectManager:GetLuaObject("GameUISlaveSelect")
        GameUISlaveSelect:ShowChatIcon()
      elseif GameStateManager:GetCurrentGameState() == GameStateStarCraft then
        local GameUIStarCraftMap = LuaObjectManager:GetLuaObject("GameUIStarCraftMap")
        GameUIStarCraftMap:ShowChatIcon()
      elseif GameStateManager:GetCurrentGameState() == GameStateManager.GameStateLargeMap then
        local GameUILargeMap = LuaObjectManager:GetLuaObject("GameUILargeMap")
        GameUILargeMap:MoveInChatIcon()
      elseif GameStateManager:GetCurrentGameState():IsObjectInState(GameUIGlobalChatBar:GetLuaObj()) then
        GameUIGlobalChatBar:MoveIn()
      end
    elseif arg == "operation" then
      self:SelectMessage(-1)
    elseif arg == "player_view" then
      self:Visible(false, "player_view")
    end
  elseif cmd == "send_message" then
    DebugOut("sendMessage:", arg)
    local index, msg = unpack(LuaUtils:string_split(arg, "\001"))
    if msg == "" or msg == nil then
      msg = GameTextEdit:GetCurrentContent()
      GameTextEdit:SetCurrentContent("")
    end
    self:SendMessage(msg, index)
  elseif cmd == "select_channel" then
    self:SelectChannel(arg)
  elseif cmd == "select_message" then
    self:SelectMessage(tonumber(arg))
  elseif cmd == "translate_message" then
    local channel_name, index = unpack(LuaUtils:string_split(arg, ","))
    self:TranslateMessage(channel_name, tonumber(index))
  elseif cmd == "operation" then
    if arg == "whisper" then
      self:OperationWhisper()
    elseif arg == "block" then
      self:OperationBlock()
    elseif arg == "report" then
      self:OperationReport()
    else
      assert(false)
    end
  elseif cmd == "operation_view" then
    self:OperationView(arg)
  end
end
function GameUIChat.ChatFailedNotifyHandler(content)
  GameUIGlobalScreen:ShowAlert("error", content.code, nil)
  local message_table = GameUIChat.MessageStack[GameUIChat.lastSendChannel]
  if message_table then
    local message_info = message_table[GameUIChat.lastSendIndex]
    message_info.isfail = true
  end
  GameUIChat.lastSendMsg = ""
  GameUIChat:UpdateMessageItem(GameUIChat.lastSendChannel, GameUIChat.lastSendIndex, nil)
  GameUIChat.lastSendIndex = nil
  GameUIChat.lastSendChannel = nil
end
function GameUIChat.ThanksGivenChampionNotifyHandler(content)
  GameUIChat.ThanksGivenChampionUserID = content.user_id
  DebugOut("ThanksGivenChampionNotifyHandler")
  DebugTable(content)
end
if AutoUpdate.isAndroidDevice then
  function GameUIChat.OnAndroidBack()
    if GameUIChat.isOnOpretion then
      GameUIChat:MoveOutOperation()
    else
      GameUIChat:MoveOutChat()
      GameUIChat:MoveOutOperation()
    end
  end
end
function GameUIChat:GetTranslateMsgFromOldMsg(msgContent, timestamp)
  if #GameUIChat.mTranslateResultListBackup > 0 then
    for k = 1, #GameUIChat.mTranslateResultListBackup do
      local msg = GameUIChat.mTranslateResultListBackup[k]
      if msg.content == msgContent and msg.timestamp == timestamp then
        return msg
      end
    end
  end
  return nil
end
function GameUIChat:CheckMsgInList(channnel, msgContent, timestamp)
  local message_table = GameUIChat.MessageStack[channnel]
  for k, v in ipairs(message_table) do
    if v.content == msgContent and v.timestamp == timestamp then
      return true
    end
  end
  return false
end
function GameUIChat:ReplayOldMsg(channnel, msgContent, newMsg)
  local message_table = GameUIChat.MessageStack[channnel]
  for k, v in ipairs(message_table) do
    if v.content == msgContent and v.mysend then
      message_table[k] = newMsg
      self.lastSendMsg = newMsg.content
      newMsg.colorIndex = v.colorIndex
      return true
    end
  end
  return false
end
function GameUIChat:CheckIsRead(channel, timestamp)
  DebugOut("GameUIChat:CheckIsRead:", GameSettingData[channel], timestamp)
  if GameSettingData[channel] == nil or timestamp > tonumber(GameSettingData[channel]) then
    return false
  else
    return true
  end
end
function GameUIChat:SaveReadTimeStamp(channel)
  local message_table = GameUIChat.MessageStack[channel]
  DebugOut("ssddd:", channel)
  DebugOut("ssddd:", message_table)
  if message_table and #message_table > 0 then
    messageInfo = message_table[#message_table]
    GameSettingData[channel] = messageInfo.timestamp
    DebugOut("GameUIChat:SaveReadTimeStamp:", GameSettingData[channel])
    GameUtils:SaveSettingData()
  end
end
GameUIChat.mChannelStatus = nil
GameUIChat.channelNtyTime = 0
function GameUIChat.ChatChannelSwitchNtf(content)
  DebugOut("GameUIChat.ChatChannelSwitchNtf")
  DebugTable(content)
  GameUIChat.mChannelStatus = content
  if not GameStateManager:GetCurrentGameState():IsObjectInState(GameUIChat) then
    DebugOut("GameUIChat.ChatChannelSwitchNtf2222")
    GameUIChat.channelNtyTime = 20
  end
end
function GameUIChat:IsBattleChannelOpened()
  if GameUIChat.mChannelStatus then
    for k, v in pairs(GameUIChat.mChannelStatus.chat_channels) do
      if 2 == v.type then
        return v.switch
      end
    end
  end
  return false
end
function GameUIChat:SetMessageInputContentHtml(htmlStr)
  local flash_obj = GameUIChat:GetFlashObject()
  if flash_obj then
    flash_obj:InvokeASCallback("_root", "setMessageContentHtml", htmlStr)
  end
end
function GameUIChat:GetMessageInputText()
  local txt
  local flash_obj = GameUIChat:GetFlashObject()
  if flash_obj then
    txt = flash_obj:InvokeASCallback("_root", "getInputString")
  end
  return txt
end
function GameUIChat:getMenuPos(item)
  if self:GetFlashObject() then
    local pos = self:GetFlashObject():InvokeASCallback("_root", "getMenuPos", item)
    local pos_s = LuaUtils:string_split(pos, "\001")
    return pos_s
  end
  return nil
end
function GameUIChat.onKeyboardShow()
  DebugOut("ssssss key bo")
  if GameUtils:GetTutorialHelp() then
    GameUtils:HideTutorialHelp()
  end
end
function GameUIChat.onKeyboardHide()
  local txt = GameUIChat:GetMessageInputText()
  if txt then
    local htmlTxt = LuaUtils:ConvertEmojiUnicode2Html(txt)
    GameUIChat:SetMessageInputContentHtml(htmlTxt)
  end
end
function GameUIChat.CheckEmojiPic(resName)
  local localPath = "data2/" .. DynamicResDownloader:GetFullName(resName, DynamicResDownloader.resType.EMOJI_PIC)
  if DynamicResDownloader:IfResExsit(localPath) then
    DebugOut("DynamicResDownloader:IfResExsit(localPath)) " .. localPath)
    ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
    return true
  else
    local extInfo = {}
    extInfo.localPath = "data2/" .. resName
    DynamicResDownloader:AddDynamicRes(resName, DynamicResDownloader.resType.EMOJI_PIC, extInfo, nil)
    return false
  end
end
