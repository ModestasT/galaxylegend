local Step = GameData.wormhole.Step
table.insert(Step, {
  ID = "{2,1}",
  HEAD = "head31",
  LEVEL = 40,
  Dialog = 4,
  Reward = "[{money,2.6},{prestige,200}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{2,2}",
  HEAD = "head40",
  LEVEL = 41,
  Dialog = 3,
  Reward = "[{money,2.6},{prestige,200}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{2,3}",
  HEAD = "head22",
  LEVEL = 42,
  Dialog = 4,
  Reward = "[{money,2.6},{prestige,200}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{2,4}",
  HEAD = "head14",
  LEVEL = 43,
  Dialog = 3,
  Reward = "[{money,2.6},{prestige,200}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{2,5}",
  HEAD = "head13",
  LEVEL = 44,
  Dialog = 1,
  Reward = "[{money,3.8},{prestige,300}]",
  Reward2 = "[{item,{1204,1}}]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{2,6}",
  HEAD = "head35",
  LEVEL = 45,
  Dialog = 4,
  Reward = "[{money,2.94},{prestige,300}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{2,7}",
  HEAD = "head20",
  LEVEL = 46,
  Dialog = 4,
  Reward = "[{money,2.94},{prestige,300}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{2,8}",
  HEAD = "head21",
  LEVEL = 47,
  Dialog = 3,
  Reward = "[{money,2.94},{prestige,300}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{2,9}",
  HEAD = "head19",
  LEVEL = 48,
  Dialog = 4,
  Reward = "[{money,2.94},{prestige,300}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{2,10}",
  HEAD = "head13",
  LEVEL = 49,
  Dialog = 3,
  Reward = "[{money,4.9},{prestige,400}]",
  Reward2 = "[{item,{1204,1}},{item,{2506,1}}]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 2,
  Force = 100
})
table.insert(Step, {
  ID = "{2,11}",
  HEAD = "head35",
  LEVEL = 50,
  Dialog = 2,
  Reward = "[{money,3.68},{prestige,400}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{2,12}",
  HEAD = "head20",
  LEVEL = 51,
  Dialog = 3,
  Reward = "[{money,3.68},{prestige,400}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{2,13}",
  HEAD = "head21",
  LEVEL = 52,
  Dialog = 2,
  Reward = "[{money,3.68},{prestige,400}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{2,14}",
  HEAD = "head19",
  LEVEL = 53,
  Dialog = 4,
  Reward = "[{money,3.68},{prestige,400}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{2,15}",
  HEAD = "head13",
  LEVEL = 54,
  Dialog = 1,
  Reward = "[{money,7.36},{prestige,600}]",
  Reward2 = "[{item,{1204,1}},{item,{2507,1}}]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{2,16}",
  HEAD = "head31",
  LEVEL = 55,
  Dialog = 4,
  Reward = "[{money,4.9},{prestige,500}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{2,17}",
  HEAD = "head40",
  LEVEL = 56,
  Dialog = 2,
  Reward = "[{money,4.9},{prestige,500}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{2,18}",
  HEAD = "head22",
  LEVEL = 57,
  Dialog = 3,
  Reward = "[{money,4.9},{prestige,500}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{2,19}",
  HEAD = "head14",
  LEVEL = 58,
  Dialog = 1,
  Reward = "[{money,4.9},{prestige,500}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{2,20}",
  HEAD = "head13",
  LEVEL = 59,
  Dialog = 4,
  Reward = "[{money,8},{prestige,800}]",
  Reward2 = "[{item,{1206,1}},{item,{2507,1}}]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 2,
  Force = 100
})
table.insert(Step, {
  ID = "{2,21}",
  HEAD = "head19",
  LEVEL = 60,
  Dialog = 1,
  Reward = "[{money,8},{prestige,800}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{2,22}",
  HEAD = "head20",
  LEVEL = 61,
  Dialog = 2,
  Reward = "[{money,8},{prestige,800}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{2,23}",
  HEAD = "head21",
  LEVEL = 62,
  Dialog = 3,
  Reward = "[{money,8},{prestige,800}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{2,24}",
  HEAD = "head14",
  LEVEL = 63,
  Dialog = 4,
  Reward = "[{money,8},{prestige,800}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{2,25}",
  HEAD = "head13",
  LEVEL = 64,
  Dialog = 1,
  Reward = "[{money,9.8},{prestige,1000}]",
  Reward2 = "[{item,{1206,2}},{item,{2507,1}}]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 2,
  Force = 100
})
table.insert(Step, {
  ID = "{4,1}",
  HEAD = "head31",
  LEVEL = 40,
  Dialog = 4,
  Reward = "[{money,2.6},{prestige,200}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{4,2}",
  HEAD = "head40",
  LEVEL = 41,
  Dialog = 3,
  Reward = "[{money,2.6},{prestige,200}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{4,3}",
  HEAD = "head22",
  LEVEL = 42,
  Dialog = 4,
  Reward = "[{money,2.6},{prestige,200}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{4,4}",
  HEAD = "head14",
  LEVEL = 43,
  Dialog = 3,
  Reward = "[{money,2.6},{prestige,200}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{4,5}",
  HEAD = "head13",
  LEVEL = 44,
  Dialog = 1,
  Reward = "[{money,3.8},{prestige,300}]",
  Reward2 = "[{item,{1204,1}}]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{4,6}",
  HEAD = "head35",
  LEVEL = 45,
  Dialog = 4,
  Reward = "[{money,2.94},{prestige,300}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{4,7}",
  HEAD = "head20",
  LEVEL = 46,
  Dialog = 4,
  Reward = "[{money,2.94},{prestige,300}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{4,8}",
  HEAD = "head21",
  LEVEL = 47,
  Dialog = 3,
  Reward = "[{money,2.94},{prestige,300}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{4,9}",
  HEAD = "head19",
  LEVEL = 48,
  Dialog = 4,
  Reward = "[{money,2.94},{prestige,300}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{4,10}",
  HEAD = "head13",
  LEVEL = 49,
  Dialog = 3,
  Reward = "[{money,4.9},{prestige,400}]",
  Reward2 = "[{item,{1204,1}},{item,{2506,1}}]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 2,
  Force = 100
})
table.insert(Step, {
  ID = "{4,11}",
  HEAD = "head35",
  LEVEL = 50,
  Dialog = 2,
  Reward = "[{money,3.68},{prestige,400}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{4,12}",
  HEAD = "head20",
  LEVEL = 51,
  Dialog = 3,
  Reward = "[{money,3.68},{prestige,400}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{4,13}",
  HEAD = "head21",
  LEVEL = 52,
  Dialog = 2,
  Reward = "[{money,3.68},{prestige,400}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{4,14}",
  HEAD = "head19",
  LEVEL = 53,
  Dialog = 4,
  Reward = "[{money,3.68},{prestige,400}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{4,15}",
  HEAD = "head13",
  LEVEL = 54,
  Dialog = 1,
  Reward = "[{money,7.36},{prestige,600}]",
  Reward2 = "[{item,{1204,1}},{item,{2507,1}}]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{4,16}",
  HEAD = "head31",
  LEVEL = 55,
  Dialog = 4,
  Reward = "[{money,4.9},{prestige,500}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{4,17}",
  HEAD = "head40",
  LEVEL = 56,
  Dialog = 2,
  Reward = "[{money,4.9},{prestige,500}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{4,18}",
  HEAD = "head22",
  LEVEL = 57,
  Dialog = 3,
  Reward = "[{money,4.9},{prestige,500}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{4,19}",
  HEAD = "head14",
  LEVEL = 58,
  Dialog = 1,
  Reward = "[{money,4.9},{prestige,500}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{4,20}",
  HEAD = "head13",
  LEVEL = 59,
  Dialog = 4,
  Reward = "[{money,8},{prestige,800}]",
  Reward2 = "[{item,{1206,1}},{item,{2507,1}}]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 2,
  Force = 100
})
table.insert(Step, {
  ID = "{4,21}",
  HEAD = "head19",
  LEVEL = 60,
  Dialog = 1,
  Reward = "[{money,8},{prestige,800}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{4,22}",
  HEAD = "head20",
  LEVEL = 61,
  Dialog = 2,
  Reward = "[{money,8},{prestige,800}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{4,23}",
  HEAD = "head21",
  LEVEL = 62,
  Dialog = 3,
  Reward = "[{money,8},{prestige,800}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{4,24}",
  HEAD = "head14",
  LEVEL = 63,
  Dialog = 4,
  Reward = "[{money,8},{prestige,800}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{4,25}",
  HEAD = "head13",
  LEVEL = 64,
  Dialog = 1,
  Reward = "[{money,9.8},{prestige,1000}]",
  Reward2 = "[{item,{1206,2}},{item,{2507,1}}]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 2,
  Force = 100
})
table.insert(Step, {
  ID = "{10,1}",
  HEAD = "head31",
  LEVEL = 40,
  Dialog = 3,
  Reward = "[{money,2.6},{prestige,200},{technique,0.25}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{10,2}",
  HEAD = "head40",
  LEVEL = 41,
  Dialog = 2,
  Reward = "[{money,2.6},{prestige,200},{technique,0.25}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{10,3}",
  HEAD = "head22",
  LEVEL = 42,
  Dialog = 4,
  Reward = "[{money,2.6},{prestige,400},{technique,0.25}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{10,4}",
  HEAD = "head14",
  LEVEL = 43,
  Dialog = 1,
  Reward = "[{money,2.6},{prestige,300},{technique,0.25}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{10,5}",
  HEAD = "head116",
  LEVEL = 44,
  Dialog = 1,
  Reward = "[{money,2.6},{prestige,300},{technique,0.25}]",
  Reward2 = "[{item,{20376,10}}]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{10,6}",
  HEAD = "head116",
  LEVEL = 45,
  Dialog = 2,
  Reward = "[{money,2.94},{prestige,400},{technique,0.3}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{10,7}",
  HEAD = "head116",
  LEVEL = 46,
  Dialog = 4,
  Reward = "[{money,2.94},{prestige,300},{technique,0.3}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{10,8}",
  HEAD = "head116",
  LEVEL = 47,
  Dialog = 1,
  Reward = "[{money,2.94},{prestige,300},{technique,0.3}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{10,9}",
  HEAD = "head116",
  LEVEL = 48,
  Dialog = 3,
  Reward = "[{money,2.94},{prestige,500},{technique,0.3}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{10,10}",
  HEAD = "head116",
  LEVEL = 49,
  Dialog = 2,
  Reward = "[{money,2.94},{prestige,400},{technique,0.3}]",
  Reward2 = "[{item,{20339,2}}]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 2,
  Force = 100
})
table.insert(Step, {
  ID = "{10,11}",
  HEAD = "head116",
  LEVEL = 50,
  Dialog = 1,
  Reward = "[{money,2.94},{prestige,400},{technique,0.3}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{10,12}",
  HEAD = "head116",
  LEVEL = 51,
  Dialog = 3,
  Reward = "[{money,3.68},{prestige,600},{technique,0.35}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{10,13}",
  HEAD = "head116",
  LEVEL = 52,
  Dialog = 4,
  Reward = "[{money,2.94},{prestige,500},{technique,0.3}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{10,14}",
  HEAD = "head116",
  LEVEL = 53,
  Dialog = 2,
  Reward = "[{money,2.94},{prestige,500},{technique,0.3}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{10,15}",
  HEAD = "head116",
  LEVEL = 54,
  Dialog = 1,
  Reward = "[{money,7.36},{prestige,800},{technique,0.75}]",
  Reward2 = "[{item,{1203,2}},{item,{3101,5}}]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{20,1}",
  HEAD = "head31",
  LEVEL = 40,
  Dialog = 3,
  Reward = "[{money,2.6},{prestige,200},{technique,0.25}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{20,2}",
  HEAD = "head40",
  LEVEL = 41,
  Dialog = 2,
  Reward = "[{money,2.6},{prestige,200},{technique,0.25}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{20,3}",
  HEAD = "head22",
  LEVEL = 42,
  Dialog = 4,
  Reward = "[{money,2.6},{prestige,200},{technique,0.25}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{20,4}",
  HEAD = "head14",
  LEVEL = 43,
  Dialog = 1,
  Reward = "[{money,2.6},{prestige,200},{technique,0.25}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{20,5}",
  HEAD = "head58",
  LEVEL = 44,
  Dialog = 1,
  Reward = "[{money,3.8},{prestige,500},{technique,0.35}]",
  Reward2 = "[{item,{20376,10}}]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{20,6}",
  HEAD = "head58",
  LEVEL = 45,
  Dialog = 2,
  Reward = "[{money,2.94},{prestige,300},{technique,0.3}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{20,7}",
  HEAD = "head58",
  LEVEL = 46,
  Dialog = 4,
  Reward = "[{money,2.94},{prestige,300},{technique,0.3}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{20,8}",
  HEAD = "head58",
  LEVEL = 47,
  Dialog = 1,
  Reward = "[{money,2.94},{prestige,300},{technique,0.3}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{20,9}",
  HEAD = "head58",
  LEVEL = 48,
  Dialog = 3,
  Reward = "[{money,2.94},{prestige,300},{technique,0.3}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{20,10}",
  HEAD = "head58",
  LEVEL = 49,
  Dialog = 2,
  Reward = "[{money,4.9},{prestige,500},{technique,0.5}]",
  Reward2 = "[{item,{20339,2}}]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 2,
  Force = 100
})
table.insert(Step, {
  ID = "{20,11}",
  HEAD = "head58",
  LEVEL = 50,
  Dialog = 1,
  Reward = "[{money,3.68},{prestige,300},{technique,0.35}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{20,12}",
  HEAD = "head58",
  LEVEL = 51,
  Dialog = 3,
  Reward = "[{money,3.68},{prestige,300},{technique,0.35}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{20,13}",
  HEAD = "head58",
  LEVEL = 52,
  Dialog = 4,
  Reward = "[{money,3.68},{prestige,300},{technique,0.35}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{20,14}",
  HEAD = "head58",
  LEVEL = 53,
  Dialog = 2,
  Reward = "[{money,3.68},{prestige,300},{technique,0.35}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{20,15}",
  HEAD = "head58",
  LEVEL = 54,
  Dialog = 1,
  Reward = "[{money,7.36},{prestige,600},{technique,0.75}]",
  Reward2 = "[{item,{1204,2}},{item,{3101,5}}]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{30,1}",
  HEAD = "head31",
  LEVEL = 40,
  Dialog = 3,
  Reward = "[{money,2.6},{prestige,200},{technique,0.25}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{30,2}",
  HEAD = "head40",
  LEVEL = 41,
  Dialog = 2,
  Reward = "[{money,2.6},{prestige,200},{technique,0.25}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{30,3}",
  HEAD = "head22",
  LEVEL = 42,
  Dialog = 4,
  Reward = "[{money,2.6},{prestige,200},{technique,0.25}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{30,4}",
  HEAD = "head14",
  LEVEL = 43,
  Dialog = 1,
  Reward = "[{money,2.6},{prestige,200},{technique,0.25}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{30,5}",
  HEAD = "head101",
  LEVEL = 44,
  Dialog = 1,
  Reward = "[{money,3.8},{prestige,500},{technique,0.35}]",
  Reward2 = "[{item,{2502,1}}]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{30,6}",
  HEAD = "head101",
  LEVEL = 45,
  Dialog = 2,
  Reward = "[{money,2.94},{prestige,300},{technique,0.3}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{30,7}",
  HEAD = "head101",
  LEVEL = 46,
  Dialog = 4,
  Reward = "[{money,2.94},{prestige,300},{technique,0.3}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{30,8}",
  HEAD = "head101",
  LEVEL = 47,
  Dialog = 1,
  Reward = "[{money,2.94},{prestige,300},{technique,0.3}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{30,9}",
  HEAD = "head101",
  LEVEL = 48,
  Dialog = 3,
  Reward = "[{money,2.94},{prestige,300},{technique,0.3}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{30,10}",
  HEAD = "head101",
  LEVEL = 49,
  Dialog = 2,
  Reward = "[{money,4.9},{prestige,500},{technique,0.5}]",
  Reward2 = "[{item,{2503,1}}]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 2,
  Force = 100
})
table.insert(Step, {
  ID = "{30,11}",
  HEAD = "head101",
  LEVEL = 50,
  Dialog = 1,
  Reward = "[{money,3.68},{prestige,300},{technique,0.35}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{30,12}",
  HEAD = "head101",
  LEVEL = 51,
  Dialog = 3,
  Reward = "[{money,3.68},{prestige,300},{technique,0.35}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{30,13}",
  HEAD = "head101",
  LEVEL = 52,
  Dialog = 4,
  Reward = "[{money,3.68},{prestige,300},{technique,0.35}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{30,14}",
  HEAD = "head101",
  LEVEL = 53,
  Dialog = 2,
  Reward = "[{money,3.68},{prestige,300},{technique,0.35}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{30,15}",
  HEAD = "head101",
  LEVEL = 54,
  Dialog = 1,
  Reward = "[{money,7.36},{prestige,600},{technique,0.75}]",
  Reward2 = "[{item,{1202,2}},{item,{3101,5}}]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{40,1}",
  HEAD = "head31",
  LEVEL = 40,
  Dialog = 3,
  Reward = "[{money,2.6},{prestige,200},{technique,0.25}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{40,2}",
  HEAD = "head40",
  LEVEL = 41,
  Dialog = 2,
  Reward = "[{money,2.6},{prestige,200},{technique,0.25}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{40,3}",
  HEAD = "head130",
  LEVEL = 42,
  Dialog = 4,
  Reward = "[{money,3.8},{prestige,500},{technique,0.35}]",
  Reward2 = "[{item,{3101,10}},{item,{20402,1}}]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{40,4}",
  HEAD = "head130",
  LEVEL = 43,
  Dialog = 1,
  Reward = "[{money,2.6},{prestige,200},{technique,0.25}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{40,5}",
  HEAD = "head130",
  LEVEL = 44,
  Dialog = 1,
  Reward = "[{money,2.6},{prestige,200},{technique,0.25}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{40,6}",
  HEAD = "head130",
  LEVEL = 45,
  Dialog = 2,
  Reward = "[{money,3.8},{prestige,500},{technique,0.35}]",
  Reward2 = "[{item,{3101,10}},{item,{20339,2}}]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{40,7}",
  HEAD = "head130",
  LEVEL = 46,
  Dialog = 4,
  Reward = "[{money,2.6},{prestige,200},{technique,0.25}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{40,8}",
  HEAD = "head130",
  LEVEL = 47,
  Dialog = 1,
  Reward = "[{money,2.6},{prestige,200},{technique,0.25}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{40,9}",
  HEAD = "head130",
  LEVEL = 48,
  Dialog = 3,
  Reward = "[{money,3.8},{prestige,500},{technique,0.35}]",
  Reward2 = "[{item,{1206,1}},{item,{20409,1}}]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{40,10}",
  HEAD = "head130",
  LEVEL = 49,
  Dialog = 2,
  Reward = "[{money,2.6},{prestige,200},{technique,0.25}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 2,
  Force = 100
})
table.insert(Step, {
  ID = "{40,11}",
  HEAD = "head130",
  LEVEL = 50,
  Dialog = 1,
  Reward = "[{money,2.6},{prestige,200},{technique,0.25}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{40,12}",
  HEAD = "head130",
  LEVEL = 51,
  Dialog = 3,
  Reward = "[{money,3.8},{prestige,500},{technique,0.35}]",
  Reward2 = "[{item,{1206,1}},{item,{20409,2}}]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{40,13}",
  HEAD = "head130",
  LEVEL = 52,
  Dialog = 4,
  Reward = "[{money,3.68},{prestige,300},{technique,0.35}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{40,14}",
  HEAD = "head130",
  LEVEL = 53,
  Dialog = 2,
  Reward = "[{money,3.68},{prestige,300},{technique,0.35}]",
  Reward2 = "[]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
table.insert(Step, {
  ID = "{40,15}",
  HEAD = "head130",
  LEVEL = 54,
  Dialog = 1,
  Reward = "[{money,7.36},{prestige,600},{technique,0.75}]",
  Reward2 = "[{item,{1217,2}},{item,{20409,3}}]",
  ratio_reward = "[]",
  Condition = "[]",
  Monster_Name = "51.0",
  BackGroud = 1,
  Force = 100
})
