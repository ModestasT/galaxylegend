local NetMessageMgr = NetMessageMgr
local NetAPIList = NetAPIList
local GameTimer = GameTimer
local AlertDataList = AlertDataList
local GameGlobalData = GameGlobalData
local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
local GameUIArena = LuaObjectManager:GetLuaObject("GameUIArena")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameObjectFormationBackground = LuaObjectManager:GetLuaObject("GameObjectFormationBackground")
local GameStateArena = GameStateManager.GameStateArena
local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
local GameStateBattlePlay = GameStateManager.GameStateBattlePlay
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local GameUIGameUIAdvancedArenaLayer = require("data1/GameUIAdvancedArenaLayer.tfl")
GameUIArena.rewardContent = nil
GameUIArena.KingNumber = 200
function GameUIArena:RequestGalaxyArenaRewards()
  DebugOut("QJtest GameUIArena:RequestGalaxyArenaRewards( )")
  NetMessageMgr:SendMsg(NetAPIList.wdc_awards_req.Code, nil, GameUIArena.RequestGalaxyArenaRewardsCallback, true, nil)
end
function GameUIArena.RequestGalaxyArenaRewardsCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.wdc_awards_req.Code then
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.wdc_awards_ack.Code then
    DebugOut("GameUIArena.RequestGalaxyArenaRewardsCallback")
    DebugTable(content)
    GameUIArena.rewardContent = content
    local SortCallback = function(a, b)
      if a.id < b.id then
        return true
      end
    end
    table.sort(GameUIArena.rewardContent.class_awards, SortCallback)
    table.sort(GameUIArena.rewardContent.ranking_awards, SortCallback)
    local nowGradeIndex = 1
    local new_class_awards = {}
    local gradeDetail = {}
    for i = 1, #content.class_awards do
      if nowGradeIndex == content.class_awards[i].rank_level then
        table.insert(gradeDetail, content.class_awards[i])
      else
        nowGradeIndex = content.class_awards[i].rank_level
        table.insert(new_class_awards, gradeDetail)
        gradeDetail = {}
        table.insert(gradeDetail, content.class_awards[i])
      end
    end
    table.insert(new_class_awards, gradeDetail)
    GameUIArena.rewardContent.class_awards = new_class_awards
    DebugOut("RequestGalaxyArenaRewardsCallback new_class_awards=")
    DebugTable(new_class_awards)
    if GameUIArena:GetFlashObject() then
      local gradeTitleLeftText = GameLoader:GetGameText("LC_MENU_LEAGUE_GRADE_REWARDS_BUTTON")
      local gradeTitleRightText = GameLoader:GetGameText("LC_MENU_LEAGUE_RANK_REWARDS_BUTTON")
      GameUIArena:GetFlashObject():InvokeASCallback("_root", "setLocalText", "GRADE_LEFT_TEXT", gradeTitleLeftText)
      GameUIArena:GetFlashObject():InvokeASCallback("_root", "setLocalText", "GRADE_RIGHT_TEXT", gradeTitleRightText)
    end
    GameUIArena:showGradeRewardList()
    return true
  end
  return false
end
function GameUIArena:showGradeRewardList()
  if not GameUIArena.rewardContent then
    GameUIArena:RequestGalaxyArenaRewards()
    return
  end
  local flashObj = self:GetFlashObject()
  local rank_level = GameUIArena.rewardContent.rank_level
  flashObj:InvokeASCallback("_root", "setLocalText", "LEAGUE_SHORT_OF_CREDIT", GameLoader:GetGameText("LC_MENU_LEAGUE_SHORT_OF_CREDIT"))
  local player_main_fleet = GameGlobalData:GetFleetInfo(1)
  local leaderlist = GameGlobalData:GetData("leaderlist")
  local curMatrixIndex = GameGlobalData.GlobalData.curMatrixIndex
  local player_avatar = ""
  if leaderlist and curMatrixIndex then
    player_avatar = GameDataAccessHelper:GetFleetAvatar(leaderlist.leader_ids[curMatrixIndex], player_main_fleet.level)
  else
    player_avatar = GameUtils:GetPlayerAvatar(nil, player_main_fleet.level)
  end
  local left_time = GameLoader:GetGameText("LC_MENU_LEAGUE_SEASON_TIME_CHAR") .. GameUtils:formatTimeStringAsPartion(GameUIArena.rewardContent.left_time)
  local data = {}
  data.player_avatar = player_avatar
  data.score = GameUIArena.rewardContent.score
  data.rank_level = rank_level
  data.left_time = left_time
  data.season = GameUIArena.rewardContent.season
  data.TextSeason = string.gsub(GameLoader:GetGameText("LC_MENU_LEAGUE_SEASON_TITLE"), "<number>", data.season)
  data.class_awards = GameUIArena.rewardContent.class_awards
  flashObj:InvokeASCallback("_root", "showGradeRewardList", data)
  GameUIArena:showGradeRewardRankIcon()
end
function GameUIArena:showGradeRewardRankIcon()
  local param = {}
  local rankIcons = {}
  local benefitDetail = {}
  local extInfo = {}
  local selfRank_level = GameUIArena.rewardContent.rank_level
  for i = 1, #GameUIArena.rewardContent.class_awards do
    local rank_img = GameUIArena.rewardContent.class_awards[i][1].rank_img
    if i == selfRank_level then
      for _, v in ipairs(GameUIArena.rewardContent.class_awards[i]) do
        if v.id == GameUIArena.rewardContent.rank then
          rank_img = v.rank_img
          break
        end
      end
    end
    if i < selfRank_level then
      local length = #GameUIArena.rewardContent.class_awards[i]
      rank_img = GameUIArena.rewardContent.class_awards[i][length].rank_img
    end
    local frame = GameUtils:GetPlayerRankingInWdc(rank_img, {itemIndex = i, rank_img = rank_img}, GameUIArena.setGradeRewardRankIcon)
    DebugOut("showGradeRewardRankIcon ", rank_img, frame)
    table.insert(rankIcons, frame)
  end
  for i = 1, #GameUIArena.rewardContent.class_awards do
    local benefitGrade = {
      detail = {}
    }
    benefitGrade.id = GameUIArena.rewardContent.class_awards[i][1].rank_level
    local awards = GameUIArena.rewardContent.class_awards[i][1]
    if selfRank_level == benefitGrade.id then
      for _, v in ipairs(GameUIArena.rewardContent.class_awards[i]) do
        if v.id == GameUIArena.rewardContent.rank then
          awards = v
          break
        end
      end
    elseif selfRank_level < benefitGrade.id then
      awards = GameUIArena.rewardContent.class_awards[i][#GameUIArena.rewardContent.class_awards[i]]
    end
    DebugOut("awards.benefits = ")
    DebugTable(awards.benefits)
    for j = 1, #awards.benefits do
      local detail = {}
      extInfo.mc_1 = benefitGrade.id
      extInfo.mc_2 = j
      extInfo.param = awards.benefits[j]
      detail.icon = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(extInfo.param, extInfo, GameUIArena.setGradeItemBenefitIcon)
      detail.number = GameUtils.numberConversion(tonumber(GameHelper:GetAwardCount(extInfo.param.item_type, extInfo.param.number, extInfo.param.no))) .. "/H"
      table.insert(benefitGrade.detail, detail)
    end
    table.insert(benefitDetail, benefitGrade)
  end
  param.rankIcons = rankIcons
  param.benefitDetail = benefitDetail
  param.nextGradeScoreNeed = 0
  if selfRank_level < 8 then
    param.nextGradeScoreNeed = GameUIArena.rewardContent.class_awards[selfRank_level + 1][1].begin_rank - GameUIArena.rewardContent.score
  end
  DebugOut("GameUIArena:showGradeRewardRankIcon = ", selfRank_level, param.nextGradeScoreNeed)
  DebugTable(param)
  self:GetFlashObject():InvokeASCallback("_root", "showGradeRewardRankIcon", param)
  DebugOut("Show RefreshGradeReward")
  DebugTable(GameUIArena.needGetRewardList)
  if GameUIArena.needGetRewardList and 0 < #GameUIArena.needGetRewardList then
    self:GetFlashObject():InvokeASCallback("_root", "RefreshGradeReward", GameUIArena.needGetRewardList)
  end
end
function GameUIArena.setGradeItemBenefitIcon(extInfo)
  local icon = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(extInfo.param, nil, nil)
  local number = GameHelper:GetAwardCount(extInfo.param.item_type, extInfo.param.number, extInfo.param.no)
  if GameUIArena:GetFlashObject() then
    GameUIArena:GetFlashObject():InvokeASCallback("_root", "setGradeItemBenefitDetail", extInfo.mc_1, extInfo.mc_2, icon, number)
  end
  DebugOut("GameUIArena.setGradeItemBenefitIcon", extInfo.mc_1, extInfo.mc_2, icon, extInfo.param.number)
end
function GameUIArena.setGradeRewardRankIcon(param)
  local frame = GameUtils:GetPlayerRankingInWdc(param.rank_img, nil, nil)
  if GameUIArena:GetFlashObject() then
    GameUIArena:GetFlashObject():InvokeASCallback("_root", "setGradeRewardRankIcon", param.itemIndex, frame)
  end
end
function GameUIArena:showRankRewardList()
  if not GameUIArena.rewardContent or not GameUIArena.rewardContent.ranking_awards then
    GameUIArena:RequestGalaxyArenaRewards()
    return
  end
  self:GetFlashObject():InvokeASCallback("_root", "showRankRewardList", #GameUIArena.rewardContent.ranking_awards)
end
function GameUIArena:UpdateRankRewardItem(indexItem)
  local DataString = -1
  local ItemInfo
  local rankRewards = GameUIArena.rewardContent.ranking_awards
  if indexItem > 0 and indexItem <= #rankRewards then
    ItemInfo = rankRewards[indexItem]
  end
  if ItemInfo then
    local DataTable = {}
    table.insert(DataTable, ItemInfo.begin_rank)
    table.insert(DataTable, ItemInfo.end_rank)
    table.insert(DataTable, #ItemInfo.awards)
    for i = 1, #ItemInfo.awards do
      local extInfo = ItemInfo.awards[i]
      extInfo.indexItem = indexItem
      extInfo.mcIndex = i
      table.insert(DataTable, GameHelper:GetAwardTypeIconFrameNameSupportDynamic(ItemInfo.awards[i], extInfo, GameUIArena.UpdateRankRewardItemIcon))
      table.insert(DataTable, GameHelper:GetAwardCount(ItemInfo.awards[i].item_type, ItemInfo.awards[i].number, ItemInfo.awards[i].no))
    end
    DataString = table.concat(DataTable, "\001")
  end
  DebugOut("QJtest UpdateRankRewardItem ", indexItem, DataString)
  self:GetFlashObject():InvokeASCallback("_root", "setRankRewardItemData", indexItem, DataString)
end
function GameUIArena.UpdateRankRewardItemIcon(extInfo)
  DebugOut("UpdateRankRewardItemIcon")
  DebugTable(extInfo)
  extInfo.icon = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(extInfo, nil, nil)
  GameUIArena:GetFlashObject():InvokeASCallback("_root", "UpdateRankRewardItemIcon", extInfo)
end
function GameUIArena:showGradeDetail(index)
  local parame = {}
  local selfRank_level = GameUIArena.rewardContent.rank_level
  local detail = GameUIArena.rewardContent.class_awards[index][#GameUIArena.rewardContent.class_awards[index]]
  if selfRank_level == index then
    for _, v in ipairs(GameUIArena.rewardContent.class_awards[index]) do
      if v.id == GameUIArena.rewardContent.rank then
        detail = v
        break
      end
    end
  elseif index > selfRank_level then
    detail = GameUIArena.rewardContent.class_awards[index][1]
  end
  local extInfo = {}
  extInfo.mc_1 = "rankIcon"
  extInfo.param = detail.rank_img
  parame.rankIcon = GameUtils:GetPlayerRankingInWdc(detail.rank_img, extInfo, GameUIArena.UpdateGradeDetailIcon)
  parame.gradeName = GameLoader:GetGameText("LC_MENU_LEAGUE_MEDALNAME_" .. detail.rank_name)
  parame.score = GameLoader:GetGameText("LC_MENU_LEAGUE_LEVEL_REQUIRE_INTEGRAL") .. " " .. detail.begin_rank
  if index == 8 then
    parame.score = string.gsub(GameLoader:GetGameText("LC_MENU_GLC_TOP_RANK_TITLE"), "<number>", GameUIArena.KingNumber)
  end
  parame.playerTitle = GameLoader:GetGameText("LC_MENU_LEAGUE_PLAYER_TITLE") .. " " .. GameUtils:GetPlayerRank(detail.rank_name, 2)
  if index == 8 and selfRank_level == index and GameUIArena.rewardContent.ranking == 1 then
    parame.playerTitle = GameLoader:GetGameText("LC_MENU_LEAGUE_PLAYER_TITLE") .. " " .. GameUtils:GetPlayerRank(detail.rank_name, GameUIArena.rewardContent.ranking)
  end
  parame.text_season = GameLoader:GetGameText("LC_MENU_LEAGUE_RANK_SEASON_REWARDS")
  parame.rewardsSpeed = GameLoader:GetGameText("LC_MENU_LEAGUE_PRODUCTION_SPEED")
  parame.buffText_1 = GameLoader:GetGameText("LC_MENU_Equip_param_1")
  parame.buffText_2 = GameLoader:GetGameText("LC_MENU_Equip_param_2")
  parame.buffText_3 = GameLoader:GetGameText("LC_MENU_Equip_param_3")
  parame.buffText_4 = GameLoader:GetGameText("LC_MENU_Equip_param_4")
  parame.buffText_5 = GameLoader:GetGameText("LC_MENU_Equip_param_5")
  parame.buffText_6 = GameLoader:GetGameText("LC_MENU_Equip_param_6")
  parame.buffText_7 = GameLoader:GetGameText("LC_MENU_Equip_param_7")
  parame.buffText_8 = GameLoader:GetGameText("LC_MENU_Equip_param_8")
  parame.textInfo = GameLoader:GetGameText("LC_MENU_LEAGUE_MEDAL_REWARD_INFO")
  parame.benefit = {}
  for i = 1, #detail.benefits do
    local benefitDetail = {}
    extInfo.mc_1 = "reward_1"
    extInfo.mc_2 = i
    extInfo.param = detail.benefits[i]
    benefitDetail.icon = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(detail.benefits[i], extInfo, GameUIArena.UpdateGradeDetailIcon)
    benefitDetail.text = GameUtils.numberConversion(tonumber(GameHelper:GetAwardCount(extInfo.param.item_type, extInfo.param.number, extInfo.param.no))) .. "/H"
    table.insert(parame.benefit, benefitDetail)
  end
  parame.awards = {}
  for i = 1, #detail.awards do
    local awards = {}
    extInfo.mc_1 = "reward_2"
    extInfo.mc_2 = i
    extInfo.param = detail.awards[i]
    awards.icon = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(detail.awards[i], extInfo, GameUIArena.UpdateGradeDetailIcon)
    awards.text = GameUtils.numberConversion(tonumber(GameHelper:GetAwardCount(extInfo.param.item_type, extInfo.param.number, extInfo.param.no)))
    table.insert(parame.awards, awards)
  end
  parame.buffers = detail.buffers
  local tableRankGrades = {}
  DebugOut(" SmallRanIcons ")
  DebugTable(GameUIArena.rewardContent.class_awards[index])
  DebugOut(" SmallRanIcons 2222")
  DebugTable(parame.benefit)
  for i = 1, #GameUIArena.rewardContent.class_awards[index] do
    local detail = {}
    local rank_img = GameUIArena.rewardContent.class_awards[index][i].rank_img
    local rankId = GameUIArena.rewardContent.class_awards[index][i].rank_name
    detail.name = GameLoader:GetGameText("LC_MENU_LEAGUE_GRADENAME_" .. rankId)
    detail.icon = GameUtils:GetPlayerRankingInWdc(rank_img, {id = i, rank_img = rank_img}, GameUIArena.UpdateGradeDetailGradesIcon)
    table.insert(tableRankGrades, detail)
  end
  parame.gradesIcons = tableRankGrades
  parame.gradeNameBottom = GameLoader:GetGameText("LC_MENU_LEAGUE_SERIES_MEDALNAME_" .. index)
  DebugOut(" SmallRanIcons 2")
  DebugTable(tableRankGrades)
  self:GetFlashObject():InvokeASCallback("_root", "showGradeDetail", parame)
end
function GameUIArena.UpdateGradeDetailGradesIcon(extInfo)
  local icon = GameUtils:GetPlayerRankingInWdc(extInfo.rank_img, nil, nil)
  GameUIArena:GetFlashObject():InvokeASCallback("_root", "UpdateGradeDetailGradesIcon", extInfo.id, icon)
end
function GameUIArena.UpdateGradeDetailIcon(extInfo)
  local flashObj = GameUIArena:GetFlashObject()
  if extInfo.mc_1 == "rankIcon" then
    local iconFrame = GameUtils:GetPlayerRankingInWdc(extInfo.param, nil, nil)
    flashObj:InvokeASCallback("_root", "setGradeDetailIcon", extInfo.mc_1, nil, iconFrame)
  else
    local iconFrame = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(extInfo.param, nil, nil)
    flashObj:InvokeASCallback("_root", "setGradeDetailIcon", extInfo.mc_1, extInfo.mc_2, iconFrame)
  end
end
GameUIArena.needGetRewardList = {}
function GameUIArena.RefreshGradeReward(content)
  DebugOut("GameUIArena.RefreshGradeReward 1 =")
  DebugTable(content)
  local extInfo = {}
  for i = 1, #content.awards do
    local rewardDetail = {}
    rewardDetail.rank_level = content.awards[i].rank_level
    rewardDetail.id = content.awards[i].id
    rewardDetail.itemDetail = {}
    for j = 1, #content.awards[i].awards do
      local detail = {}
      extInfo.rank_level = content.awards[i].rank_level
      extInfo.id = content.awards[i].id
      extInfo.index = j
      extInfo.param = content.awards[i].awards[j]
      local item = content.awards[i].awards[j]
      detail.number = GameUtils.numberConversion(tonumber(GameHelper:GetAwardCount(item.item_type, item.number, item.no)))
      detail.icon = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(extInfo.param, extInfo, GameUIArena.setNeedGetRewardList)
      table.insert(rewardDetail.itemDetail, detail)
    end
    table.insert(GameUIArena.needGetRewardList, rewardDetail)
  end
  DebugOut("GameUIArena.RefreshGradeReward", GameUIArena.mCurrentArenaType)
  DebugTable(GameUIArena.needGetRewardList)
  if GameUIArena.mCurrentArenaType == GameUIArena.ARENA_TYPE.wdc and GameUIArena:GetFlashObject() and GameUIArena.needGetRewardList and #GameUIArena.needGetRewardList > 0 then
    GameUIArena:GetFlashObject():InvokeASCallback("_root", "RefreshGradeReward", GameUIArena.needGetRewardList)
  end
  if GameUIArena.needGetRewardList and #GameUIArena.needGetRewardList > 0 and GameUIArena:GetFlashObject() then
    GameUIArena:GetFlashObject():InvokeASCallback("_root", "showRedPoint")
  elseif GameUIArena:GetFlashObject() then
    GameUIArena:GetFlashObject():InvokeASCallback("_root", "hideRedPoint")
  end
end
function GameUIArena.setNeedGetRewardList(extInfo)
  local icon = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(extInfo.param, nil, nil)
  for k, v in ipairs(GameUIArena.needGetRewardList) do
    if extInfo.id == v.id then
      v.itemDetail[extInfo.index].icon = icon
      return
    end
  end
  if GameUIArena.mCurrentArenaType == GameUIArena.ARENA_TYPE.wdc and GameUIArena:GetFlashObject() and GameUIArena.needGetRewardList and #GameUIArena.needGetRewardList > 0 then
    GameUIArena:GetFlashObject():InvokeASCallback("_root", "RefreshGradeReward", GameUIArena.needGetRewardList)
  end
end
function GameUIArena:collectReward(index)
  NetMessageMgr:SendMsg(NetAPIList.wdc_awards_get_req.Code, {id = index}, GameUIArena.collectRewardCallback, true, nil)
end
function GameUIArena.collectRewardCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.wdc_awards_get_req.Code then
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.wdc_awards_get_ack.Code then
    if content.code == 0 then
      GameUIArena:GetFlashObject():InvokeASCallback("_root", "SetGradeItemBenefit", content.id)
      for i = #GameUIArena.needGetRewardList, 1, -1 do
        if GameUIArena.needGetRewardList[i].rank_level == content.id then
          table.remove(GameUIArena.needGetRewardList, i)
        end
      end
    end
    if #GameUIArena.needGetRewardList == 0 then
      GameUIArena:GetFlashObject():InvokeASCallback("_root", "hideRedPoint")
    end
    return true
  end
  return false
end
GameUIArena.RankList = {}
GameUIArena.RankList.top_list = {}
GameUIArena.RankList.class_list = {}
GameUIArena.nowRankListType = "top_list"
function GameUIArena:clearRankListData()
  GameUIArena.RankList = {}
  GameUIArena.RankList.top_list = {}
  GameUIArena.RankList.class_list = {}
end
function GameUIArena.getRankListData(content)
  if content.type == 1 then
    for k, v in ipairs(content.players) do
      local flag = false
      for _, m in ipairs(GameUIArena.RankList.top_list) do
        if v.user_id == m.user_id then
          flag = true
        end
      end
      if not flag then
        table.insert(GameUIArena.RankList.top_list, v)
      end
    end
  elseif content.type == 2 then
    for k, v in ipairs(content.players) do
      local flag = false
      for _, m in ipairs(GameUIArena.RankList.class_list) do
        if v.user_id == m.user_id then
          flag = true
        end
      end
      if not flag then
        table.insert(GameUIArena.RankList.class_list, v)
      end
    end
  end
end
function GameUIArena:RequestGalaxyArenaRankList()
  GameUIArena:clearRankListData()
  NetMessageMgr:SendMsg(NetAPIList.wdc_rank_board_req.Code, nil, GameUIArena.RequestGalaxyArenaRankListCallback, true, nil)
end
function GameUIArena.RequestGalaxyArenaRankListCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.wdc_rank_board_req.Code then
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.wdc_rank_board_ack.Code then
    DebugTable(content)
    GameUIArena.RankList.self = content.self
    GameUIArena.RankList.world_ranking = content.world_ranking
    GameUIArena.RankList.rank_level = content.rank_level
    GameUIArena.RankList.rank_img = content.rank_img
    GameUIArena.RankList.rank_name = content.rank_name
    GameUIArena.RankList.rank_ceil = content.rank_ceil
    table.sort(GameUIArena.RankList.top_list, function(v1, v2)
      return v1.ranking < v2.ranking
    end)
    table.sort(GameUIArena.RankList.class_list, function(v1, v2)
      return v1.ranking < v2.ranking
    end)
    GameUIArena:showRankList(GameUIArena.nowRankListType)
    return true
  end
  return false
end
function GameUIArena:showRankList(type)
  if not GameUIArena.RankList then
    GameUIArena:RequestGalaxyArenaRankList()
    return
  end
  local data = {}
  data.count = #GameUIArena.RankList[type]
  data.self = GameUIArena.RankList.self
  local rankImag = GameUtils:GetPlayerRankingInWdc(GameUIArena.RankList.self.rank_img, nil, nil)
  data.self.rankImag = rankImag
  data.rank_type = GameUIArena.nowRankListType
  data.rank_level = GameUIArena.RankList.rank_level
  data.world_ranking = GameUIArena.RankList.world_ranking
  data.RankingCeil = GameUIArena.RankList.rank_ceil
  local rankName = GameLoader:GetGameText("LC_MENU_LEAGUE_RANK_CHAR")
  if GameUIArena.nowRankListType == "class_list" then
    local nowRank = GameUIArena.RankList.rank_name
    rankName = GameLoader:GetGameText("LC_MENU_LEAGUE_GRADENAME_" .. nowRank) .. " " .. GameLoader:GetGameText("LC_MENU_LEAGUE_RANK_CHAR")
  end
  local TextInfo = string.gsub(GameLoader:GetGameText("LC_MENU_LEAGUE_GRADE_RANK_INFO"), "<number>", GameUIArena.KingNumber)
  DebugOut("GameUIArena:showRankList")
  DebugTable(data)
  self:GetFlashObject():InvokeASCallback("_root", "showRankList", data, rankName, TextInfo)
end
function GameUIArena:UpdateRankItem(indexItem)
  local DataString = -1
  local ItemInfo
  local rankList = GameUIArena.RankList[GameUIArena.nowRankListType]
  if indexItem > 0 and indexItem <= #rankList then
    ItemInfo = rankList[indexItem]
  end
  if ItemInfo then
    local DataTable = {}
    table.insert(DataTable, ItemInfo.ranking)
    table.insert(DataTable, ItemInfo.user_name)
    table.insert(DataTable, ItemInfo.force)
    table.insert(DataTable, ItemInfo.point)
    local rankingImage = GameUtils:GetPlayerRankingInWdc(ItemInfo.rank_img, nil, nil)
    table.insert(DataTable, rankingImage)
    DataString = table.concat(DataTable, "\001")
  end
  self:GetFlashObject():InvokeASCallback("_root", "setRankItemData", indexItem, DataString)
end
function GameUIArena:showRankPlayerInfo(indexItem)
  local playerInfo = GameUIArena.RankList.self
  if indexItem ~= 0 then
    playerInfo = GameUIArena.RankList[GameUIArena.nowRankListType][indexItem]
  end
  ItemBox:showRankPlayerInfo(playerInfo)
end
GameUIArena.HonorList = nil
function GameUIArena:RequestHonorWallList()
  NetMessageMgr:SendMsg(NetAPIList.wdc_honor_wall_req.Code, nil, GameUIArena.RequestHonorWallListCallback, true, nil)
end
function GameUIArena.RequestHonorWallListCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.wdc_honor_wall_req.Code then
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.wdc_honor_wall_ack.Code then
    DebugTable(content)
    GameUIArena.HonorList = content
    GameUIArena:showHonorWall()
    GameUIArena:showSelfHonorWall()
    DebugOut("RequestHonorWallListCallback")
    DebugTable(content)
    return true
  end
  return false
end
function GameUIArena:showHonorWall()
  local flashObj = self:GetFlashObject()
  if flashObj ~= nil then
    local disableHonorAll = #GameUIArena.HonorList.all == 0
    local text = GameLoader:GetGameText("LC_MENU_LEAGUE_SERVER_HONOR")
    flashObj:InvokeASCallback("_root", "showHonorWall", disableHonorAll, text)
  end
end
function GameUIArena:showHonorWallAll()
  if not GameUIArena.HonorList then
    GameUIArena:RequestHonorWallList()
  end
  if #GameUIArena.HonorList.all == 0 then
    return
  end
  local flashObj = self:GetFlashObject()
  local param = {}
  local seasonList = GameUIArena.HonorList.all
  flashObj:InvokeASCallback("_root", "setLocalText", "LEAGUE_CREDIT_CHAR", GameLoader:GetGameText("LC_MENU_LEAGUE_CREDIT_CHAR"))
  local SortCallback_season = function(a, b)
    if a.season < b.season then
      return true
    end
  end
  table.sort(seasonList, SortCallback_season)
  local SortCallback = function(a, b)
    if a.ranking < b.ranking then
      return true
    end
  end
  for i = 1, #seasonList do
    local seasonDetail = {}
    seasonDetail.season = seasonList[i].season
    seasonDetail.TextSeason = string.gsub(GameLoader:GetGameText("LC_MENU_LEAGUE_SEASON_TITLE"), "<number>", seasonList[i].season)
    seasonDetail.players = {}
    for j = 1, #seasonList[i].top_players do
      table.sort(seasonList[i].top_players, SortCallback)
      local playerInfo = {}
      if seasonList[i].top_players[j].icon == 0 or seasonList[i].top_players[j].icon == 1 then
        playerInfo.avatar = GameUtils:GetPlayerAvatarWithSex(seasonList[i].top_players[j].sex, seasonList[i].top_players[j].flevel)
      else
        playerInfo.avatar = GameDataAccessHelper:GetFleetAvatar(seasonList[i].top_players[j].icon, seasonList[i].top_players[j].flevel)
      end
      playerInfo.user_name = seasonList[i].top_players[j].user_name
      playerInfo.score = seasonList[i].top_players[j].point
      table.insert(seasonDetail.players, playerInfo)
    end
    table.insert(param, seasonDetail)
  end
  DebugOut("showHonorWallAll = ")
  DebugTable(param)
  flashObj:InvokeASCallback("_root", "showHonorWallAll", param)
end
function GameUIArena:showSelfHonorWall()
  if not GameUIArena.HonorList then
    GameUIArena:RequestHonorWallList()
  end
  local flashObj = self:GetFlashObject()
  local param = {}
  local selfHonorData = GameUIArena.HonorList.self
  local lastSeasonGroupIndex = 1
  local SortCallback = function(a, b)
    if a.season < b.season then
      return true
    end
  end
  table.sort(selfHonorData, SortCallback)
  local groupCount = math.ceil(#selfHonorData / 4)
  for i = 1, groupCount do
    local group = {}
    for j = 1, 4 do
      local seasonInfo = {}
      seasonInfo.hasData = false
      for k, v in pairs(selfHonorData) do
        if j + (i - 1) * 4 == tonumber(v.season) then
          seasonInfo = v
          local extInfo = {}
          extInfo.rank_img = seasonInfo.rank_img
          extInfo.season = seasonInfo.season
          seasonInfo.rankIcon = GameUtils:GetPlayerRankingInWdc(seasonInfo.rank_img, extInfo, GameUIArena.setSelfHonorRankIcon)
          seasonInfo.start_date = os.date("%Y-%m-%d", tonumber(seasonInfo.start_date))
          seasonInfo.end_date = os.date("%Y-%m-%d", tonumber(seasonInfo.end_date))
          seasonInfo.hasData = true
          seasonInfo.TextSeason = string.gsub(GameLoader:GetGameText("LC_MENU_LEAGUE_SEASON_TITLE"), "<number>", seasonInfo.season)
          break
        end
      end
      if #seasonInfo > 1 and seasonInfo.rank ~= 0 and lastSeasonGroupIndex ~= i then
        lastSeasonGroupIndex = i
      end
      if not seasonInfo.hasData then
        seasonInfo.season = (i - 1) * 4 + j
        seasonInfo.rankIcon = "rank1_0"
        seasonInfo.TextSeason = string.gsub(GameLoader:GetGameText("LC_MENU_LEAGUE_SEASON_TITLE"), "<number>", seasonInfo.season)
      end
      table.insert(group, seasonInfo)
    end
    table.insert(param, group)
  end
  DebugOut("GameUIArena:showSelfHonorWall")
  DebugTable(param)
  flashObj:InvokeASCallback("_root", "setLocalText", "LEAGUE_SEASON_TITLE", GameLoader:GetGameText("LC_MENU_LEAGUE_SEASON_TITLE"))
  flashObj:InvokeASCallback("_root", "showHonorWallSelf", param, lastSeasonGroupIndex)
end
function GameUIArena.setSelfHonorRankIcon(extInfo)
  extInfo.rankIcon = GameUtils:GetPlayerRankingInWdc(extInfo.rank_img, nil, nil)
  GameUIArena:GetFlashObject():InvokeASCallback("_root", "setSelfRankIcon", extInfo)
end
GameUIArena.wdc_historyReport = nil
GameUIArena.nowReportType = "atk_list"
function GameUIArena:RequestGalaxyArenaReport()
  NetMessageMgr:SendMsg(NetAPIList.wdc_report_req.Code, nil, GameUIArena.RequestGalaxyArenaReportCallback, true, nil)
end
function GameUIArena.RequestGalaxyArenaReportCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.wdc_report_req.Code then
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.wdc_report_ack.Code then
    GameUIArena.wdc_historyReport = content
    local SortCallback = function(a, b)
      if a.fight_time < b.fight_time then
        return true
      end
    end
    table.sort(GameUIArena.wdc_historyReport.atk_list, SortCallback)
    table.sort(GameUIArena.wdc_historyReport.defend_list, SortCallback)
    DebugOut(" GameUIArena.RequestGalaxyArenaReportCallback ")
    DebugTable(content)
    GameUIArena:AnimationMoveInGalaxyArenaReport()
    return true
  end
  return false
end
function GameUIArena:AnimationMoveInGalaxyArenaReport()
  if not GameUIArena.wdc_historyReport then
    GameUIArena:RequestGalaxyArenaReport()
  end
  local item_count = #GameUIArena.wdc_historyReport[GameUIArena.nowReportType]
  self:GetFlashObject():InvokeASCallback("_root", "GalaxyArenaMoveInReportBox", item_count, GameUIArena.nowReportType)
end
function GameUIArena:UpdateReportItem(index_item)
  local history_data = GameUIArena.wdc_historyReport[GameUIArena.nowReportType][index_item]
  local text_timeago = -1
  local text_result = -1
  local score_change = 0
  DebugOut(" GameUIArena:UpdateReportItem = ")
  DebugTable(history_data)
  if history_data then
    text_timeago = history_data.fight_time
    if history_data.is_win and GameUIArena.nowReportType == "atk_list" then
      text_result = GameLoader:GetGameText("LC_MENU_YOU_VS_OPPONENT_WIN")
      text_result = string.format(text_result, GameUtils:GetUserDisplayName(history_data.enemy.user_name))
      score_change = history_data.score_after - history_data.score_before
    elseif not history_data.is_win and GameUIArena.nowReportType == "atk_list" then
      text_result = GameLoader:GetGameText("LC_MENU_YOU_VS_OPPONENT_LOSE")
      text_result = string.format(text_result, GameUtils:GetUserDisplayName(history_data.enemy.user_name))
      score_change = history_data.score_after - history_data.score_before
    elseif history_data.is_win and GameUIArena.nowReportType == "defend_list" then
      text_result = GameLoader:GetGameText("LC_MENU_OPPONENT_VS_YOU_LOSE")
      text_result = string.format(text_result, GameUtils:GetUserDisplayName(history_data.enemy.user_name))
      score_change = history_data.score_after - history_data.score_before
    elseif not history_data.is_win and GameUIArena.nowReportType == "defend_list" then
      text_result = GameLoader:GetGameText("LC_MENU_OPPONENT_VS_YOU_WIN")
      text_result = string.format(text_result, GameUtils:GetUserDisplayName(history_data.enemy.user_name))
      score_change = history_data.score_after - history_data.score_before
    else
      assert(false)
    end
  else
    text_timeago = GameLoader:GetGameText("LC_MENU_NO_DATA_CHAR")
  end
  local isEndTime = GameUIAdvancedArenaLayer:IsEndTime()
  local buttonText = GameLoader:GetGameText("LC_MENU_LEAGUE_REVENGE")
  self:GetFlashObject():InvokeASCallback("_root", "updateReportItem", index_item, text_result, text_timeago, score_change, history_data.can_revenge, buttonText, isEndTime, GameUIArena.nowReportType)
end
function GameUIArena:checkStat()
  NetMessageMgr:SendMsg(NetAPIList.wdc_fight_status_req.Code, nil, GameUIArena.getProtectTimeStartBattleNetCallback, true, nil)
end
function GameUIArena.getProtectTimeStartBattleNetCallback(msgType, content)
  if msgType == NetAPIList.wdc_fight_status_ack.Code then
    if content.protect_time > 0 then
      local text_title = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
      local info = GameLoader:GetGameText("LC_MENU_LEAGUE_ATTACK_INFO")
      GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
      GameUIMessageDialog:SetYesButton(GameUIArena.revengeFromReport)
      GameUIMessageDialog:SetNoButton(nil)
      GameUIMessageDialog:Display(text_title, info)
    else
      GameUIArena.revengeFromReport()
    end
    return true
  end
  return false
end
GameUIArena.revengeIndex = nil
function GameUIArena.revengeFromReport()
  local indexItem = GameUIArena.revengeIndex
  local report_data = GameUIArena.wdc_historyReport[GameUIArena.nowReportType][indexItem]
  if report_data then
    local enemyInfo = {}
    enemyInfo.avatar = GameUtils:GetPlayerAvatar(report_data.enemy.sex, report_data.enemy.level)
    enemyInfo.name = report_data.enemy.user_name
    enemyInfo.mSex = report_data.enemy.sex
    GameUIArena.enemyInfo = enemyInfo
    local ArenaDefender = {
      defender = {}
    }
    ArenaDefender.defender.server_id = report_data.enemy.server_id
    ArenaDefender.defender.user_id = tonumber(report_data.enemy.user_id)
    ArenaDefender.defender.force = tonumber(report_data.enemy.force)
    ArenaDefender.defender.revenge_id = tonumber(report_data.report_id)
    GameUIAdvancedArenaLayer.currentEnemyInfo = ArenaDefender
    DebugOut("GameUIArena:revengeFromReport")
    DebugTable(GameUIAdvancedArenaLayer.currentEnemyInfo)
    GameStateManager.GameStateFormation.isNeedRequestMatrixInfo = false
    GameObjectFormationBackground.force = ArenaDefender.defender.force
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateFormation)
    GameUIArena.currentMenu = "GalaxyArenaReport"
  end
end
GameUIArena.currentMenu = nil
function GameUIArena:replayGalaxyArenaBattle(indexItem)
  local report_data = GameUIArena.wdc_historyReport[GameUIArena.nowReportType][indexItem]
  if GameStateManager:GetCurrentGameState().fightRoundData then
    GameStateManager:GetCurrentGameState().fightRoundData = nil
  end
  if report_data then
    NetMessageMgr:SendMsg(NetAPIList.wdc_report_detail_req.Code, {
      id = tonumber(report_data.report_id)
    }, GameUIArena.RequestReplayGalaxyArenaBattleCallback, true, nil)
  end
end
function GameUIArena.RequestReplayGalaxyArenaBattleCallback(msgtype, content)
  if msgtype == NetAPIList.wdc_report_detail_ack.Code then
    if #content.report.rounds == 0 and GameStateManager:GetCurrentGameState().fightRoundData then
      content.report.rounds = GameStateManager:GetCurrentGameState().fightRoundData
    end
    GameUIArena.currentMenu = "GalaxyArenaReport"
    GameStateManager.GameStateBattlePlay:InitBattle(GameStateBattlePlay.MODE_HISTORY, content.report, nil, nil)
    GameStateBattlePlay:RegisterOverCallback(function()
      GameStateManager:SetCurrentGameState(GameStateArena)
    end, nil)
    GameStateManager:SetCurrentGameState(GameStateBattlePlay)
    return true
  end
  return false
end
function GameUIArena:AndroidBack()
  if GameUIArena.windowType == GameUIArena.wdc_windowType.reward then
    GameUIArena:GetFlashObject():InvokeASCallback("_root", "hideRewardMenu")
  elseif GameUIArena.windowType == GameUIArena.wdc_windowType.leaderboard then
    GameUIArena:GetFlashObject():InvokeASCallback("_root", "closeRankList")
  elseif GameUIArena.windowType == GameUIArena.wdc_windowType.honor then
    GameUIArena:GetFlashObject():InvokeASCallback("_root", "hideHonorWallMenu")
  elseif GameUIArena.windowType == GameUIArena.wdc_windowType.report then
    GameUIArena:GetFlashObject():InvokeASCallback("_root", "GalaxyArenaMoveOutReportBox")
  elseif GameUIArena.windowType == GameUIArena.tlc_windowType.reward then
    GameUIArena:GetFlashObject():InvokeASCallback("_root", "hideRewardMenu_tlc")
  elseif GameUIArena.windowType == GameUIArena.tlc_windowType.leaderboard then
    GameUIArena:GetFlashObject():InvokeASCallback("_root", "closeRankList_tlc")
  elseif GameUIArena.windowType == GameUIArena.tlc_windowType.honor then
    GameUIArena:GetFlashObject():InvokeASCallback("_root", "hideHonorWallMenu_tlc")
  elseif GameUIArena.windowType == GameUIArena.tlc_windowType.report then
    GameUIArena:GetFlashObject():InvokeASCallback("_root", "GalaxyArenaMoveOutReportBox_tlc")
  end
end
