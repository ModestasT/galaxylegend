local GameUISupplyDialog = LuaObjectManager:GetLuaObject("GameUISupplyDialog")
local GameStateGlobalState = GameStateManager.GameStateGlobalState
local GameFleetInfoBag = LuaObjectManager:GetLuaObject("GameFleetInfoBag")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
function GameUISupplyDialog:OnFSCommand(cmd, arg)
  if cmd == "move_out_supply_over" then
    GameStateGlobalState:EraseObject(self)
    DebugOut("move_out_supply_over")
  elseif cmd == "close_supply_window" then
    self:CloseSupplyWindow()
  elseif cmd == "click_item_1" then
    DebugOut("click_item_1")
    GameUISupplyDialog:UseSupplyItem(1)
  elseif cmd == "click_item_2" then
    DebugOut("click_item_2")
    GameUISupplyDialog:UseSupplyItem(2)
  elseif cmd == "click_item_3" then
    DebugOut("click_item_3")
    GameUISupplyDialog:BuyBattleSupply()
  end
end
function GameUISupplyDialog:RegisterCloseOverCallback(func, args)
  self.OverCallback = {}
  self.OverCallback.func = func
  self.OverCallback.args = args
end
function GameUISupplyDialog:ExecuteCloseOverCallback()
  if self.OverCallback then
    local callback = self.OverCallback.func
    local args = self.OverCallback.args
    if args and #args > 0 then
      callback(unpack(args))
    else
      callback(args)
    end
    self.OverCallback = nil
  end
end
function GameUISupplyDialog:BuyBattleSupply()
  if self.SupplyCreditData then
    if tonumber(self.SupplyCreditData.left) > 0 and tonumber(self.SupplyCreditData.left) ~= -1 then
      local netPacket = {
        type = "battle_supply"
      }
      NetMessageMgr:SendMsg(NetAPIList.supply_exchange_req.Code, netPacket, GameUISupplyDialog.NetCallbackBuySupply, true, nil)
    else
      local function callback()
        GameUISupplyDialog:ShowVipMessageDialog()
      end
      self:RegisterCloseOverCallback(callback, nil)
      self:CloseSupplyWindow()
    end
  end
end
function GameUISupplyDialog:ShowVipMessageDialog()
  local text = AlertDataList:GetTextFromErrorCode(1201)
  local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
  local GameVip = LuaObjectManager:GetLuaObject("GameVip")
  GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.Green)
  local cancel = GameLoader:GetGameText("LC_MENU_BUTTON_CAPTION_CANCEL")
  local affairs = GameLoader:GetGameText("LC_MENU_GET_MORE_BUTTON")
  GameUIMessageDialog:SetRightTextButton(cancel)
  GameUIMessageDialog:SetLeftGreenButton(affairs, GameVip.GotoPayment)
  GameUIMessageDialog:Display("", text)
end
function GameUISupplyDialog.NetCallbackBuySupply(msgType, content)
  DebugOut("NetCallbackBuySupply:", msgType)
  DebugTable(content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.supply_exchange_req.Code then
    if content.code ~= 0 then
      local function callback()
        local GameVip = LuaObjectManager:GetLuaObject("GameVip")
        GameVip:CheckIsNeedShowVip(content.code)
      end
      GameUISupplyDialog:RegisterCloseOverCallback(callback, nil)
      GameUISupplyDialog:CloseSupplyWindow()
    end
    GameUISupplyDialog:RequestBattleSupplyInfo()
    return true
  end
  if msgType == NetAPIList.supply_info_ack.Code then
    GameUISupplyDialog:RequestBattleSupplyInfo()
    return true
  end
  return false
end
function GameUISupplyDialog:UseSupplyItem(index)
  if self.SupplyAllData and self.SupplyItemData[index] then
    for k, v in pairs(self.SupplyAllData) do
      if v.item_type == self.SupplyItemData[index].item_type and v.cnt > 0 then
        local param = {}
        param.pos = v.pos
        param.is_max = false
        param.params = {}
        param.count = 1
        NetMessageMgr:SendMsg(NetAPIList.use_item_req.Code, param, GameUISupplyDialog.useItemCallback, true)
      end
    end
  end
end
function GameUISupplyDialog.useItemCallback(msgType, content)
  DebugOut("useItemCallback:", msgType)
  DebugTable(content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.use_item_req.Code then
    if content.code ~= 0 then
      local textID = AlertDataList:GetTextFromErrorCode(content.code)
      GameTip:Show(textID, 3000)
    end
    GameUISupplyDialog:RequestBag()
    return true
  end
  return false
end
function GameUISupplyDialog:CloseSupplyWindow()
  local flashObj = self:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "CloseSupplyWindow")
  end
end
function GameUISupplyDialog:Show()
  GameUISupplyDialog:RequestBag()
end
function GameUISupplyDialog:OnEraseFromGameState()
  DebugOut("GameUISupplyDialog:OnEraseFromGameState")
  GameUISupplyDialog:UnloadFlashObject()
  GameUISupplyDialog:ExecuteCloseOverCallback()
end
function GameUISupplyDialog:OnAddToGameState()
  DebugOut("GameUISupplyDialog:OnAddToGameState")
  GameUISupplyDialog:LoadFlashObject()
  GameUISupplyDialog:GetFlashObject():InvokeASCallback("_root", "OpenSupplyWindow")
  GameUISupplyDialog:GetFlashObject():InvokeASCallback("_root", "SetSupplyWindowTitle", GameLoader:GetGameText("LC_MENU_SUPLY_INFO"))
  GameUISupplyDialog:SetSupplyCreditData(nil)
end
function GameUISupplyDialog:SetSupplyWindowData()
  local titleText = ""
  local itemFrame1, itemNum1, itemVisible1, btnText1, itemFrame2, itemNum2, itemVisible2, btnText2
  DebugOut("SetSupplyWindowData:")
  DebugTable(self.SupplyItemData)
  if self.SupplyItemData then
    if self.SupplyItemData[1] then
      itemFrame1 = "item_" .. tostring(self.SupplyItemData[1].item_type)
      itemNum1 = tonumber(self.SupplyItemData[1].cnt)
      btnText1 = self.SupplyItemData[1].supply
      if tonumber(itemNum1) == 0 then
        itemVisible1 = false
      else
        itemVisible1 = true
      end
    else
      itemFrame1 = "empty"
      itemNum1 = 0
      btnText1 = 0
      itemVisible1 = false
    end
    if self.SupplyItemData[2] then
      itemFrame2 = "item_" .. tostring(self.SupplyItemData[2].item_type)
      itemNum2 = tonumber(self.SupplyItemData[2].cnt)
      btnText2 = self.SupplyItemData[2].supply
      if tonumber(itemNum2) == 0 then
        itemVisible2 = false
      else
        itemVisible2 = true
      end
    else
      itemFrame2 = "empty"
      itemNum2 = 0
      btnText2 = 0
      itemVisible2 = false
    end
  else
    itemFrame1 = "empty"
    itemFrame2 = "empty"
    itemNum1 = 0
    itemNum2 = 0
    btnText1 = 0
    btnText2 = 0
    itemVisible1 = false
    itemVisible2 = false
  end
  DebugOut("SetSupplyItemData:", itemFrame1, itemNum1, itemVisible1, btnText1, itemFrame2, itemNum2, itemVisible2, btnText2)
  local flashObj = self:GetFlashObject()
  if flashObj then
    if itemNum1 == 0 then
      itemNum1 = ""
    else
      itemNum1 = string.format(GameLoader:GetGameText("LC_MENU_SUPLY_LEFT_NUMBERS"), itemNum1)
    end
    if itemNum2 == 0 then
      itemNum2 = ""
    else
      itemNum2 = string.format(GameLoader:GetGameText("LC_MENU_SUPLY_LEFT_NUMBERS"), itemNum2)
    end
    btnText1 = string.format(GameLoader:GetGameText("LC_MENU_SUPLY_ADD_BUTTON"), btnText1)
    btnText2 = string.format(GameLoader:GetGameText("LC_MENU_SUPLY_ADD_BUTTON"), btnText2)
    flashObj:InvokeASCallback("_root", "SetSupplyItem1Data", itemFrame1, itemNum1, itemVisible1, btnText1)
    flashObj:InvokeASCallback("_root", "SetSupplyItem2Data", itemFrame2, itemNum2, itemVisible2, btnText2)
  end
end
function GameUISupplyDialog:SetSupplyCreditData(data)
  self.SupplyCreditData = data
  local flashObj = self:GetFlashObject()
  if not flashObj then
    return
  end
  local btnText3 = string.format(GameLoader:GetGameText("LC_MENU_SUPLY_ADD_BUTTON"), 10)
  if data then
    local leftNum = tonumber(data.left)
    if leftNum == -1 then
      leftNum = 99
    end
    leftNum = string.format(GameLoader:GetGameText("LC_MENU_SUPLY_LEFT_TIMES"), leftNum)
    flashObj:InvokeASCallback("_root", "SetSupplyItem3Data", "x" .. data.count, leftNum, data.exchange_cost, btnText3)
  else
    local leftNum = string.format(GameLoader:GetGameText("LC_MENU_SUPLY_LEFT_TIMES"), 0)
    flashObj:InvokeASCallback("_root", "SetSupplyItem3Data", "x0", leftNum, "0", btnText3)
  end
end
function GameUISupplyDialog:RequestBattleSupplyInfo()
  local param = {}
  param.type = "battle_supply"
  NetMessageMgr:SendMsg(NetAPIList.supply_info_req.Code, param, self.RequestBattleSupplyInfoCallback, false, nil)
end
function GameUISupplyDialog.RequestBattleSupplyInfoCallback(msgType, content)
  if msgType == NetAPIList.supply_info_ack.Code or msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.supply_info_ack.Code then
    GameUISupplyDialog:SetSupplyCreditData(content)
    return true
  end
  return false
end
function GameUISupplyDialog:RequestBag()
  local userinfo = GameGlobalData:GetData("userinfo")
  if LuaUtils:table_empty(userinfo) then
    return
  end
  local bag_req_content = {
    owner = userinfo.player_id,
    bag_type = GameFleetInfoBag.BAG_OWNER_USER,
    pos = -1
  }
  local function netFailedCallback()
    if not GameUISupplyDialog:GetFlashObject() then
      GameStateGlobalState:AddObject(GameUISupplyDialog)
      GameStateGlobalState:ForceCompleteCammandList()
    end
    GameUISupplyDialog.SupplyItemData = nil
    self:SetSupplyWindowData()
  end
  NetMessageMgr:SendMsg(NetAPIList.bag_req.Code, bag_req_content, self.RequestBagCallback, false, netFailedCallback)
end
function GameUISupplyDialog.RequestBagCallback(msgType, content)
  if not GameUISupplyDialog:GetFlashObject() then
    GameStateGlobalState:AddObject(GameUISupplyDialog)
    GameStateGlobalState:ForceCompleteCammandList()
  end
  GameUISupplyDialog.SupplyItemData = nil
  if msgType == NetAPIList.bag_ack.Code then
    DebugStore("GameUISupplyDialog.RequestBagCallback(msgType, content)")
    DebugStoreTable(content)
    GameUISupplyDialog.SupplyAllData = nil
    GameUISupplyDialog.SupplyItemData = GameUISupplyDialog:BagDataFilter(content.grids)
    GameUISupplyDialog:SetSupplyWindowData()
    GameUISupplyDialog:RequestBattleSupplyInfo()
    return true
  end
  GameUISupplyDialog:SetSupplyWindowData()
  return false
end
function GameUISupplyDialog:BagDataFilter(data)
  local supplyData = {}
  for k, v in pairs(data) do
    if v.item_type == 2101 or v.item_type == 2102 or v.item_type == 2103 or v.item_type == 2104 then
      local itemData = {}
      itemData.item_type = v.item_type
      itemData.cnt = v.cnt
      itemData.pos = v.pos
      if v.item_type == 2102 then
        itemData.supply = 5
      elseif v.item_type == 2101 or v.item_type == 2103 then
        itemData.supply = 10
      else
        itemData.supply = 20
      end
      table.insert(supplyData, itemData)
    end
  end
  DebugOut("BagDataFilter:")
  DebugTable(supplyData)
  self.SupplyAllData = supplyData
  local resultData = {}
  local itemData, itemData2
  itemData = self:GetSupplyItem(supplyData, 2102)
  if itemData and #resultData < 3 then
    table.insert(resultData, itemData)
  end
  itemData = self:GetSupplyItem(supplyData, 2101)
  itemData2 = self:GetSupplyItem(supplyData, 2103)
  if itemData and itemData2 then
    itemData.cnt = itemData.cnt + itemData2.cnt
  elseif itemData2 then
    itemData = itemData2
  end
  if itemData and #resultData < 3 then
    table.insert(resultData, itemData)
  end
  itemData = self:GetSupplyItem(supplyData, 2104)
  if itemData and #resultData < 3 then
    table.insert(resultData, itemData)
  end
  return resultData
end
function GameUISupplyDialog:GetSupplyItem(data, itemID)
  if not data or not itemID then
    return nil
  end
  for k, v in pairs(data) do
    if v.item_type == itemID then
      return v
    end
  end
end
if AutoUpdate.isAndroidDevice then
  function GameUISupplyDialog.OnAndroidBack()
    GameUISupplyDialog:CloseSupplyWindow()
  end
end
