local Monsters_170 = GameData.ChapterMonsterAct10.Monsters_170
Monsters_170[1001001] = {
  ID = 1001001,
  durability = 3695762,
  Avatar = "head171",
  Ship = "ship178"
}
Monsters_170[1001002] = {
  ID = 1001002,
  durability = 2584871,
  Avatar = "head171",
  Ship = "ship178"
}
Monsters_170[1001003] = {
  ID = 1001003,
  durability = 1625670,
  Avatar = "head171",
  Ship = "ship178"
}
Monsters_170[1001004] = {
  ID = 1001004,
  durability = 2177095,
  Avatar = "head171",
  Ship = "ship178"
}
Monsters_170[1001005] = {
  ID = 1001005,
  durability = 2461477,
  Avatar = "head171",
  Ship = "ship178"
}
Monsters_170[1001006] = {
  ID = 1001006,
  durability = 3766158,
  Avatar = "head171",
  Ship = "ship178"
}
Monsters_170[1001007] = {
  ID = 1001007,
  durability = 2634106,
  Avatar = "head171",
  Ship = "ship178"
}
Monsters_170[1001008] = {
  ID = 1001008,
  durability = 1656636,
  Avatar = "head171",
  Ship = "ship178"
}
Monsters_170[1001009] = {
  ID = 1001009,
  durability = 2218563,
  Avatar = "head171",
  Ship = "ship178"
}
Monsters_170[1001010] = {
  ID = 1001010,
  durability = 2508362,
  Avatar = "head171",
  Ship = "ship178"
}
Monsters_170[1001011] = {
  ID = 1001011,
  durability = 3836553,
  Avatar = "head171",
  Ship = "ship178"
}
Monsters_170[1001012] = {
  ID = 1001012,
  durability = 2683342,
  Avatar = "head171",
  Ship = "ship178"
}
Monsters_170[1001013] = {
  ID = 1001013,
  durability = 1687601,
  Avatar = "head171",
  Ship = "ship178"
}
Monsters_170[1001014] = {
  ID = 1001014,
  durability = 2260032,
  Avatar = "head171",
  Ship = "ship178"
}
Monsters_170[1001015] = {
  ID = 1001015,
  durability = 2555247,
  Avatar = "head171",
  Ship = "ship178"
}
Monsters_170[1001016] = {
  ID = 1001016,
  durability = 3906949,
  Avatar = "head171",
  Ship = "ship178"
}
Monsters_170[1001017] = {
  ID = 1001017,
  durability = 2732578,
  Avatar = "head171",
  Ship = "ship178"
}
Monsters_170[1001018] = {
  ID = 1001018,
  durability = 1718566,
  Avatar = "head26",
  Ship = "ship5"
}
Monsters_170[1001019] = {
  ID = 1001019,
  durability = 2301500,
  Avatar = "head171",
  Ship = "ship178"
}
Monsters_170[1001020] = {
  ID = 1001020,
  durability = 2602133,
  Avatar = "head171",
  Ship = "ship178"
}
Monsters_170[1001021] = {
  ID = 1001021,
  durability = 3977344,
  Avatar = "head171",
  Ship = "ship178"
}
Monsters_170[1001022] = {
  ID = 1001022,
  durability = 2781813,
  Avatar = "head171",
  Ship = "ship178"
}
Monsters_170[1001023] = {
  ID = 1001023,
  durability = 1749531,
  Avatar = "head171",
  Ship = "ship178"
}
Monsters_170[1001024] = {
  ID = 1001024,
  durability = 2342969,
  Avatar = "head26",
  Ship = "ship5"
}
Monsters_170[1001025] = {
  ID = 1001025,
  durability = 2649018,
  Avatar = "head26",
  Ship = "ship5"
}
Monsters_170[1001026] = {
  ID = 1001026,
  durability = 4047740,
  Avatar = "head26",
  Ship = "ship5"
}
Monsters_170[1001027] = {
  ID = 1001027,
  durability = 2831049,
  Avatar = "head26",
  Ship = "ship5"
}
Monsters_170[1001028] = {
  ID = 1001028,
  durability = 1780496,
  Avatar = "head26",
  Ship = "ship5"
}
Monsters_170[1001029] = {
  ID = 1001029,
  durability = 2384437,
  Avatar = "head26",
  Ship = "ship5"
}
Monsters_170[1001030] = {
  ID = 1001030,
  durability = 2695903,
  Avatar = "head171",
  Ship = "ship178"
}
Monsters_170[1001031] = {
  ID = 1001031,
  durability = 4118135,
  Avatar = "head171",
  Ship = "ship178"
}
Monsters_170[1001032] = {
  ID = 1001032,
  durability = 2880284,
  Avatar = "head26",
  Ship = "ship5"
}
Monsters_170[1001033] = {
  ID = 1001033,
  durability = 1811461,
  Avatar = "head171",
  Ship = "ship178"
}
Monsters_170[1001034] = {
  ID = 1001034,
  durability = 2425906,
  Avatar = "head26",
  Ship = "ship5"
}
Monsters_170[1001035] = {
  ID = 1001035,
  durability = 2742788,
  Avatar = "head26",
  Ship = "ship5"
}
Monsters_170[1001036] = {
  ID = 1001036,
  durability = 4188531,
  Avatar = "head26",
  Ship = "ship5"
}
Monsters_170[1001037] = {
  ID = 1001037,
  durability = 2929520,
  Avatar = "head26",
  Ship = "ship5"
}
Monsters_170[1001038] = {
  ID = 1001038,
  durability = 1842427,
  Avatar = "head26",
  Ship = "ship5"
}
Monsters_170[1001039] = {
  ID = 1001039,
  durability = 2467374,
  Avatar = "head26",
  Ship = "ship5"
}
Monsters_170[1001040] = {
  ID = 1001040,
  durability = 2789674,
  Avatar = "head23",
  Ship = "ship70"
}
Monsters_170[1002001] = {
  ID = 1002001,
  durability = 4258926,
  Avatar = "head41",
  Ship = "ship25"
}
Monsters_170[1002002] = {
  ID = 1002002,
  durability = 2978756,
  Avatar = "head32",
  Ship = "ship14"
}
Monsters_170[1002003] = {
  ID = 1002003,
  durability = 1873392,
  Avatar = "head32",
  Ship = "ship14"
}
Monsters_170[1002004] = {
  ID = 1002004,
  durability = 2508843,
  Avatar = "head32",
  Ship = "ship14"
}
Monsters_170[1002005] = {
  ID = 1002005,
  durability = 2836559,
  Avatar = "head32",
  Ship = "ship14"
}
Monsters_170[1002006] = {
  ID = 1002006,
  durability = 4329322,
  Avatar = "head32",
  Ship = "ship14"
}
Monsters_170[1002007] = {
  ID = 1002007,
  durability = 3027991,
  Avatar = "head32",
  Ship = "ship14"
}
Monsters_170[1002008] = {
  ID = 1002008,
  durability = 1904357,
  Avatar = "head32",
  Ship = "ship14"
}
Monsters_170[1002009] = {
  ID = 1002009,
  durability = 2550311,
  Avatar = "head32",
  Ship = "ship14"
}
Monsters_170[1002010] = {
  ID = 1002010,
  durability = 2883444,
  Avatar = "head32",
  Ship = "ship14"
}
Monsters_170[1002011] = {
  ID = 1002011,
  durability = 4399717,
  Avatar = "head32",
  Ship = "ship14"
}
Monsters_170[1002012] = {
  ID = 1002012,
  durability = 3077227,
  Avatar = "head32",
  Ship = "ship14"
}
Monsters_170[1002013] = {
  ID = 1002013,
  durability = 1935322,
  Avatar = "head32",
  Ship = "ship14"
}
Monsters_170[1002014] = {
  ID = 1002014,
  durability = 2591780,
  Avatar = "head32",
  Ship = "ship14"
}
Monsters_170[1002015] = {
  ID = 1002015,
  durability = 2930330,
  Avatar = "head32",
  Ship = "ship14"
}
Monsters_170[1002016] = {
  ID = 1002016,
  durability = 4470112,
  Avatar = "head32",
  Ship = "ship14"
}
Monsters_170[1002017] = {
  ID = 1002017,
  durability = 3126463,
  Avatar = "head32",
  Ship = "ship14"
}
Monsters_170[1002018] = {
  ID = 1002018,
  durability = 1966287,
  Avatar = "head32",
  Ship = "ship14"
}
Monsters_170[1002019] = {
  ID = 1002019,
  durability = 2633248,
  Avatar = "head32",
  Ship = "ship14"
}
Monsters_170[1002020] = {
  ID = 1002020,
  durability = 2977215,
  Avatar = "head32",
  Ship = "ship14"
}
Monsters_170[1002021] = {
  ID = 1002021,
  durability = 4540508,
  Avatar = "head41",
  Ship = "ship25"
}
Monsters_170[1002022] = {
  ID = 1002022,
  durability = 3175698,
  Avatar = "head41",
  Ship = "ship25"
}
Monsters_170[1002023] = {
  ID = 1002023,
  durability = 1997252,
  Avatar = "head41",
  Ship = "ship25"
}
Monsters_170[1002024] = {
  ID = 1002024,
  durability = 2674716,
  Avatar = "head41",
  Ship = "ship25"
}
Monsters_170[1002025] = {
  ID = 1002025,
  durability = 3024100,
  Avatar = "head41",
  Ship = "ship25"
}
Monsters_170[1002026] = {
  ID = 1002026,
  durability = 4610903,
  Avatar = "head41",
  Ship = "ship25"
}
Monsters_170[1002027] = {
  ID = 1002027,
  durability = 3224934,
  Avatar = "head41",
  Ship = "ship25"
}
Monsters_170[1002028] = {
  ID = 1002028,
  durability = 2028217,
  Avatar = "head41",
  Ship = "ship25"
}
Monsters_170[1002029] = {
  ID = 1002029,
  durability = 2716185,
  Avatar = "head41",
  Ship = "ship25"
}
Monsters_170[1002030] = {
  ID = 1002030,
  durability = 3070985,
  Avatar = "head41",
  Ship = "ship25"
}
Monsters_170[1002031] = {
  ID = 1002031,
  durability = 4681299,
  Avatar = "head41",
  Ship = "ship25"
}
Monsters_170[1002032] = {
  ID = 1002032,
  durability = 3274170,
  Avatar = "head41",
  Ship = "ship25"
}
Monsters_170[1002033] = {
  ID = 1002033,
  durability = 2059183,
  Avatar = "head41",
  Ship = "ship25"
}
Monsters_170[1002034] = {
  ID = 1002034,
  durability = 2757653,
  Avatar = "head41",
  Ship = "ship25"
}
Monsters_170[1002035] = {
  ID = 1002035,
  durability = 3117871,
  Avatar = "head34",
  Ship = "ship64"
}
Monsters_170[1002036] = {
  ID = 1002036,
  durability = 4751694,
  Avatar = "head41",
  Ship = "ship25"
}
Monsters_170[1002037] = {
  ID = 1002037,
  durability = 3323405,
  Avatar = "head41",
  Ship = "ship25"
}
Monsters_170[1002038] = {
  ID = 1002038,
  durability = 2090148,
  Avatar = "head41",
  Ship = "ship25"
}
Monsters_170[1002039] = {
  ID = 1002039,
  durability = 2799122,
  Avatar = "head41",
  Ship = "ship25"
}
Monsters_170[1002040] = {
  ID = 1002040,
  durability = 3164756,
  Avatar = "head41",
  Ship = "ship25"
}
Monsters_170[1003001] = {
  ID = 1003001,
  durability = 4822090,
  Avatar = "head14",
  Ship = "ship47"
}
Monsters_170[1003002] = {
  ID = 1003002,
  durability = 3372641,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters_170[1003003] = {
  ID = 1003003,
  durability = 2121113,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters_170[1003004] = {
  ID = 1003004,
  durability = 2840590,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters_170[1003005] = {
  ID = 1003005,
  durability = 3211641,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters_170[1003006] = {
  ID = 1003006,
  durability = 4892485,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters_170[1003007] = {
  ID = 1003007,
  durability = 3421876,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters_170[1003008] = {
  ID = 1003008,
  durability = 2152078,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters_170[1003009] = {
  ID = 1003009,
  durability = 2882059,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters_170[1003010] = {
  ID = 1003010,
  durability = 3258526,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters_170[1003011] = {
  ID = 1003011,
  durability = 4962881,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters_170[1003012] = {
  ID = 1003012,
  durability = 3471112,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters_170[1003013] = {
  ID = 1003013,
  durability = 2183043,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters_170[1003014] = {
  ID = 1003014,
  durability = 2923527,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters_170[1003015] = {
  ID = 1003015,
  durability = 3305412,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters_170[1003016] = {
  ID = 1003016,
  durability = 5033276,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters_170[1003017] = {
  ID = 1003017,
  durability = 3520348,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters_170[1003018] = {
  ID = 1003018,
  durability = 2214008,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters_170[1003019] = {
  ID = 1003019,
  durability = 2964996,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters_170[1003020] = {
  ID = 1003020,
  durability = 3352297,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters_170[1003021] = {
  ID = 1003021,
  durability = 5103672,
  Avatar = "head14",
  Ship = "ship47"
}
Monsters_170[1003022] = {
  ID = 1003022,
  durability = 3569583,
  Avatar = "head14",
  Ship = "ship47"
}
Monsters_170[1003023] = {
  ID = 1003023,
  durability = 2244974,
  Avatar = "head14",
  Ship = "ship47"
}
Monsters_170[1003024] = {
  ID = 1003024,
  durability = 3006464,
  Avatar = "head14",
  Ship = "ship47"
}
Monsters_170[1003025] = {
  ID = 1003025,
  durability = 3399182,
  Avatar = "head14",
  Ship = "ship47"
}
Monsters_170[1003026] = {
  ID = 1003026,
  durability = 5174067,
  Avatar = "head14",
  Ship = "ship47"
}
Monsters_170[1003027] = {
  ID = 1003027,
  durability = 3618819,
  Avatar = "head14",
  Ship = "ship47"
}
Monsters_170[1003028] = {
  ID = 1003028,
  durability = 2275939,
  Avatar = "head14",
  Ship = "ship47"
}
Monsters_170[1003029] = {
  ID = 1003029,
  durability = 3047933,
  Avatar = "head14",
  Ship = "ship47"
}
Monsters_170[1003030] = {
  ID = 1003030,
  durability = 3446068,
  Avatar = "head14",
  Ship = "ship47"
}
Monsters_170[1003031] = {
  ID = 1003031,
  durability = 5244463,
  Avatar = "head14",
  Ship = "ship47"
}
Monsters_170[1003032] = {
  ID = 1003032,
  durability = 3668055,
  Avatar = "head14",
  Ship = "ship47"
}
Monsters_170[1003033] = {
  ID = 1003033,
  durability = 2306904,
  Avatar = "head14",
  Ship = "ship47"
}
Monsters_170[1003034] = {
  ID = 1003034,
  durability = 3089401,
  Avatar = "head14",
  Ship = "ship47"
}
Monsters_170[1003035] = {
  ID = 1003035,
  durability = 3492953,
  Avatar = "head24",
  Ship = "ship5"
}
Monsters_170[1003036] = {
  ID = 1003036,
  durability = 5314858,
  Avatar = "head14",
  Ship = "ship47"
}
Monsters_170[1003037] = {
  ID = 1003037,
  durability = 3717290,
  Avatar = "head14",
  Ship = "ship47"
}
Monsters_170[1003038] = {
  ID = 1003038,
  durability = 2337869,
  Avatar = "head14",
  Ship = "ship47"
}
Monsters_170[1003039] = {
  ID = 1003039,
  durability = 3130870,
  Avatar = "head14",
  Ship = "ship47"
}
Monsters_170[1003040] = {
  ID = 1003040,
  durability = 3539838,
  Avatar = "head14",
  Ship = "ship47"
}
Monsters_170[1004001] = {
  ID = 1004001,
  durability = 5385254,
  Avatar = "head41",
  Ship = "ship11"
}
Monsters_170[1004002] = {
  ID = 1004002,
  durability = 3766526,
  Avatar = "head41",
  Ship = "ship11"
}
Monsters_170[1004003] = {
  ID = 1004003,
  durability = 2368834,
  Avatar = "head41",
  Ship = "ship11"
}
Monsters_170[1004004] = {
  ID = 1004004,
  durability = 3172338,
  Avatar = "head41",
  Ship = "ship11"
}
Monsters_170[1004005] = {
  ID = 1004005,
  durability = 3586723,
  Avatar = "head41",
  Ship = "ship11"
}
Monsters_170[1004006] = {
  ID = 1004006,
  durability = 5455649,
  Avatar = "head41",
  Ship = "ship11"
}
Monsters_170[1004007] = {
  ID = 1004007,
  durability = 3815762,
  Avatar = "head41",
  Ship = "ship11"
}
Monsters_170[1004008] = {
  ID = 1004008,
  durability = 2399799,
  Avatar = "head41",
  Ship = "ship11"
}
Monsters_170[1004009] = {
  ID = 1004009,
  durability = 3213807,
  Avatar = "head41",
  Ship = "ship11"
}
Monsters_170[1004010] = {
  ID = 1004010,
  durability = 3633609,
  Avatar = "head41",
  Ship = "ship11"
}
Monsters_170[1004011] = {
  ID = 1004011,
  durability = 5526045,
  Avatar = "head41",
  Ship = "ship11"
}
Monsters_170[1004012] = {
  ID = 1004012,
  durability = 3864997,
  Avatar = "head41",
  Ship = "ship11"
}
Monsters_170[1004013] = {
  ID = 1004013,
  durability = 2430765,
  Avatar = "head41",
  Ship = "ship11"
}
Monsters_170[1004014] = {
  ID = 1004014,
  durability = 3255275,
  Avatar = "head41",
  Ship = "ship11"
}
Monsters_170[1004015] = {
  ID = 1004015,
  durability = 3680494,
  Avatar = "head40",
  Ship = "ship14"
}
Monsters_170[1004016] = {
  ID = 1004016,
  durability = 5596440,
  Avatar = "head41",
  Ship = "ship11"
}
Monsters_170[1004017] = {
  ID = 1004017,
  durability = 3914233,
  Avatar = "head41",
  Ship = "ship11"
}
Monsters_170[1004018] = {
  ID = 1004018,
  durability = 2461730,
  Avatar = "head41",
  Ship = "ship11"
}
Monsters_170[1004019] = {
  ID = 1004019,
  durability = 3296744,
  Avatar = "head41",
  Ship = "ship11"
}
Monsters_170[1004020] = {
  ID = 1004020,
  durability = 3727379,
  Avatar = "head41",
  Ship = "ship11"
}
Monsters_170[1004021] = {
  ID = 1004021,
  durability = 5666836,
  Avatar = "head41",
  Ship = "ship11"
}
Monsters_170[1004022] = {
  ID = 1004022,
  durability = 3963469,
  Avatar = "head41",
  Ship = "ship11"
}
Monsters_170[1004023] = {
  ID = 1004023,
  durability = 2492695,
  Avatar = "head41",
  Ship = "ship11"
}
Monsters_170[1004024] = {
  ID = 1004024,
  durability = 3338212,
  Avatar = "head41",
  Ship = "ship11"
}
Monsters_170[1004025] = {
  ID = 1004025,
  durability = 3774265,
  Avatar = "head41",
  Ship = "ship11"
}
Monsters_170[1004026] = {
  ID = 1004026,
  durability = 5737231,
  Avatar = "head41",
  Ship = "ship11"
}
Monsters_170[1004027] = {
  ID = 1004027,
  durability = 4012704,
  Avatar = "head41",
  Ship = "ship11"
}
Monsters_170[1004028] = {
  ID = 1004028,
  durability = 2523660,
  Avatar = "head40",
  Ship = "ship14"
}
Monsters_170[1004029] = {
  ID = 1004029,
  durability = 3379681,
  Avatar = "head40",
  Ship = "ship14"
}
Monsters_170[1004030] = {
  ID = 1004030,
  durability = 3821150,
  Avatar = "head40",
  Ship = "ship14"
}
Monsters_170[1004031] = {
  ID = 1004031,
  durability = 5807627,
  Avatar = "head40",
  Ship = "ship14"
}
Monsters_170[1004032] = {
  ID = 1004032,
  durability = 4061940,
  Avatar = "head40",
  Ship = "ship14"
}
Monsters_170[1004033] = {
  ID = 1004033,
  durability = 2554625,
  Avatar = "head40",
  Ship = "ship14"
}
Monsters_170[1004034] = {
  ID = 1004034,
  durability = 3421149,
  Avatar = "head40",
  Ship = "ship14"
}
Monsters_170[1004035] = {
  ID = 1004035,
  durability = 3868035,
  Avatar = "head40",
  Ship = "ship14"
}
Monsters_170[1004036] = {
  ID = 1004036,
  durability = 5878022,
  Avatar = "head40",
  Ship = "ship14"
}
Monsters_170[1004037] = {
  ID = 1004037,
  durability = 4111175,
  Avatar = "head40",
  Ship = "ship14"
}
Monsters_170[1004038] = {
  ID = 1004038,
  durability = 2585590,
  Avatar = "head40",
  Ship = "ship14"
}
Monsters_170[1004039] = {
  ID = 1004039,
  durability = 3462618,
  Avatar = "head71",
  Ship = "ship12"
}
Monsters_170[1004040] = {
  ID = 1004040,
  durability = 3914920,
  Avatar = "head71",
  Ship = "ship12"
}
Monsters_170[1005001] = {
  ID = 1005001,
  durability = 5948418,
  Avatar = "head46",
  Ship = "ship80"
}
Monsters_170[1005002] = {
  ID = 1005002,
  durability = 4160411,
  Avatar = "head46",
  Ship = "ship80"
}
Monsters_170[1005003] = {
  ID = 1005003,
  durability = 2616556,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters_170[1005004] = {
  ID = 1005004,
  durability = 3504086,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters_170[1005005] = {
  ID = 1005005,
  durability = 3961806,
  Avatar = "head46",
  Ship = "ship80"
}
Monsters_170[1005006] = {
  ID = 1005006,
  durability = 5983615,
  Avatar = "head46",
  Ship = "ship80"
}
Monsters_170[1005007] = {
  ID = 1005007,
  durability = 4185029,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters_170[1005008] = {
  ID = 1005008,
  durability = 2632038,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters_170[1005009] = {
  ID = 1005009,
  durability = 3524820,
  Avatar = "head46",
  Ship = "ship80"
}
Monsters_170[1005010] = {
  ID = 1005010,
  durability = 3985248,
  Avatar = "head46",
  Ship = "ship80"
}
Monsters_170[1005011] = {
  ID = 1005011,
  durability = 6018813,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters_170[1005012] = {
  ID = 1005012,
  durability = 4209647,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters_170[1005013] = {
  ID = 1005013,
  durability = 2647521,
  Avatar = "head46",
  Ship = "ship80"
}
Monsters_170[1005014] = {
  ID = 1005014,
  durability = 3545555,
  Avatar = "head46",
  Ship = "ship80"
}
Monsters_170[1005015] = {
  ID = 1005015,
  durability = 4008691,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters_170[1005016] = {
  ID = 1005016,
  durability = 6054011,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters_170[1005017] = {
  ID = 1005017,
  durability = 4234265,
  Avatar = "head46",
  Ship = "ship80"
}
Monsters_170[1005018] = {
  ID = 1005018,
  durability = 2663003,
  Avatar = "head46",
  Ship = "ship80"
}
Monsters_170[1005019] = {
  ID = 1005019,
  durability = 3566289,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters_170[1005020] = {
  ID = 1005020,
  durability = 4032134,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters_170[1005021] = {
  ID = 1005021,
  durability = 6089209,
  Avatar = "head46",
  Ship = "ship80"
}
Monsters_170[1005022] = {
  ID = 1005022,
  durability = 4258882,
  Avatar = "head47",
  Ship = "ship25"
}
Monsters_170[1005023] = {
  ID = 1005023,
  durability = 2678486,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters_170[1005024] = {
  ID = 1005024,
  durability = 3587023,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters_170[1005025] = {
  ID = 1005025,
  durability = 4055576,
  Avatar = "head46",
  Ship = "ship80"
}
Monsters_170[1005026] = {
  ID = 1005026,
  durability = 6124406,
  Avatar = "head46",
  Ship = "ship80"
}
Monsters_170[1005027] = {
  ID = 1005027,
  durability = 4283500,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters_170[1005028] = {
  ID = 1005028,
  durability = 2693968,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters_170[1005029] = {
  ID = 1005029,
  durability = 3607757,
  Avatar = "head46",
  Ship = "ship80"
}
Monsters_170[1005030] = {
  ID = 1005030,
  durability = 4079019,
  Avatar = "head46",
  Ship = "ship80"
}
Monsters_170[1005031] = {
  ID = 1005031,
  durability = 6159604,
  Avatar = "head47",
  Ship = "ship25"
}
Monsters_170[1005032] = {
  ID = 1005032,
  durability = 4308118,
  Avatar = "head47",
  Ship = "ship25"
}
Monsters_170[1005033] = {
  ID = 1005033,
  durability = 2709451,
  Avatar = "head47",
  Ship = "ship25"
}
Monsters_170[1005034] = {
  ID = 1005034,
  durability = 3628492,
  Avatar = "head47",
  Ship = "ship25"
}
Monsters_170[1005035] = {
  ID = 1005035,
  durability = 4102462,
  Avatar = "head47",
  Ship = "ship25"
}
Monsters_170[1005036] = {
  ID = 1005036,
  durability = 6194802,
  Avatar = "head47",
  Ship = "ship25"
}
Monsters_170[1005037] = {
  ID = 1005037,
  durability = 4332736,
  Avatar = "head47",
  Ship = "ship25"
}
Monsters_170[1005038] = {
  ID = 1005038,
  durability = 2724934,
  Avatar = "head47",
  Ship = "ship25"
}
Monsters_170[1005039] = {
  ID = 1005039,
  durability = 3649226,
  Avatar = "head36",
  Ship = "ship57"
}
Monsters_170[1005040] = {
  ID = 1005040,
  durability = 4125904,
  Avatar = "head47",
  Ship = "ship25"
}
Monsters_170[1006001] = {
  ID = 1006001,
  durability = 6229999,
  Avatar = "head36",
  Ship = "ship39"
}
Monsters_170[1006002] = {
  ID = 1006002,
  durability = 4357354,
  Avatar = "head36",
  Ship = "ship39"
}
Monsters_170[1006003] = {
  ID = 1006003,
  durability = 2740416,
  Avatar = "head36",
  Ship = "ship39"
}
Monsters_170[1006004] = {
  ID = 1006004,
  durability = 3669960,
  Avatar = "head36",
  Ship = "ship39"
}
Monsters_170[1006005] = {
  ID = 1006005,
  durability = 4149347,
  Avatar = "head44",
  Ship = "ship12"
}
Monsters_170[1006006] = {
  ID = 1006006,
  durability = 6265197,
  Avatar = "head36",
  Ship = "ship37"
}
Monsters_170[1006007] = {
  ID = 1006007,
  durability = 4381971,
  Avatar = "head36",
  Ship = "ship37"
}
Monsters_170[1006008] = {
  ID = 1006008,
  durability = 2755899,
  Avatar = "head36",
  Ship = "ship37"
}
Monsters_170[1006009] = {
  ID = 1006009,
  durability = 3690694,
  Avatar = "head36",
  Ship = "ship37"
}
Monsters_170[1006010] = {
  ID = 1006010,
  durability = 4172789,
  Avatar = "head36",
  Ship = "ship39"
}
Monsters_170[1006011] = {
  ID = 1006011,
  durability = 6300395,
  Avatar = "head36",
  Ship = "ship39"
}
Monsters_170[1006012] = {
  ID = 1006012,
  durability = 4406589,
  Avatar = "head36",
  Ship = "ship37"
}
Monsters_170[1006013] = {
  ID = 1006013,
  durability = 2771381,
  Avatar = "head36",
  Ship = "ship37"
}
Monsters_170[1006014] = {
  ID = 1006014,
  durability = 3711428,
  Avatar = "head36",
  Ship = "ship39"
}
Monsters_170[1006015] = {
  ID = 1006015,
  durability = 4196232,
  Avatar = "head36",
  Ship = "ship39"
}
Monsters_170[1006016] = {
  ID = 1006016,
  durability = 6335593,
  Avatar = "head36",
  Ship = "ship37"
}
Monsters_170[1006017] = {
  ID = 1006017,
  durability = 4431207,
  Avatar = "head36",
  Ship = "ship37"
}
Monsters_170[1006018] = {
  ID = 1006018,
  durability = 2786864,
  Avatar = "head36",
  Ship = "ship39"
}
Monsters_170[1006019] = {
  ID = 1006019,
  durability = 3732163,
  Avatar = "head36",
  Ship = "ship39"
}
Monsters_170[1006020] = {
  ID = 1006020,
  durability = 4219675,
  Avatar = "head36",
  Ship = "ship37"
}
Monsters_170[1006021] = {
  ID = 1006021,
  durability = 6370790,
  Avatar = "head36",
  Ship = "ship37"
}
Monsters_170[1006022] = {
  ID = 1006022,
  durability = 4455825,
  Avatar = "head36",
  Ship = "ship39"
}
Monsters_170[1006023] = {
  ID = 1006023,
  durability = 2802346,
  Avatar = "head36",
  Ship = "ship39"
}
Monsters_170[1006024] = {
  ID = 1006024,
  durability = 3752897,
  Avatar = "head36",
  Ship = "ship37"
}
Monsters_170[1006025] = {
  ID = 1006025,
  durability = 4243117,
  Avatar = "head36",
  Ship = "ship37"
}
Monsters_170[1006026] = {
  ID = 1006026,
  durability = 6405988,
  Avatar = "head36",
  Ship = "ship39"
}
Monsters_170[1006027] = {
  ID = 1006027,
  durability = 4480443,
  Avatar = "head36",
  Ship = "ship39"
}
Monsters_170[1006028] = {
  ID = 1006028,
  durability = 2817829,
  Avatar = "head44",
  Ship = "ship12"
}
Monsters_170[1006029] = {
  ID = 1006029,
  durability = 3773631,
  Avatar = "head44",
  Ship = "ship12"
}
Monsters_170[1006030] = {
  ID = 1006030,
  durability = 4266560,
  Avatar = "head44",
  Ship = "ship12"
}
Monsters_170[1006031] = {
  ID = 1006031,
  durability = 6441186,
  Avatar = "head44",
  Ship = "ship12"
}
Monsters_170[1006032] = {
  ID = 1006032,
  durability = 4505061,
  Avatar = "head44",
  Ship = "ship12"
}
Monsters_170[1006033] = {
  ID = 1006033,
  durability = 2833312,
  Avatar = "head44",
  Ship = "ship12"
}
Monsters_170[1006034] = {
  ID = 1006034,
  durability = 3794365,
  Avatar = "head44",
  Ship = "ship12"
}
Monsters_170[1006035] = {
  ID = 1006035,
  durability = 4290003,
  Avatar = "head44",
  Ship = "ship12"
}
Monsters_170[1006036] = {
  ID = 1006036,
  durability = 6476384,
  Avatar = "head44",
  Ship = "ship12"
}
Monsters_170[1006037] = {
  ID = 1006037,
  durability = 4529678,
  Avatar = "head44",
  Ship = "ship12"
}
Monsters_170[1006038] = {
  ID = 1006038,
  durability = 2848794,
  Avatar = "head44",
  Ship = "ship12"
}
Monsters_170[1006039] = {
  ID = 1006039,
  durability = 3815100,
  Avatar = "head44",
  Ship = "ship12"
}
Monsters_170[1006040] = {
  ID = 1006040,
  durability = 4313445,
  Avatar = "head38",
  Ship = "ship64"
}
Monsters_170[1007001] = {
  ID = 1007001,
  durability = 6511581,
  Avatar = "head26",
  Ship = "ship14"
}
Monsters_170[1007002] = {
  ID = 1007002,
  durability = 4554296,
  Avatar = "head26",
  Ship = "ship14"
}
Monsters_170[1007003] = {
  ID = 1007003,
  durability = 2864277,
  Avatar = "head26",
  Ship = "ship14"
}
Monsters_170[1007004] = {
  ID = 1007004,
  durability = 3835834,
  Avatar = "head26",
  Ship = "ship14"
}
Monsters_170[1007005] = {
  ID = 1007005,
  durability = 4336888,
  Avatar = "head26",
  Ship = "ship14"
}
Monsters_170[1007006] = {
  ID = 1007006,
  durability = 6546779,
  Avatar = "head26",
  Ship = "ship14"
}
Monsters_170[1007007] = {
  ID = 1007007,
  durability = 4578914,
  Avatar = "head26",
  Ship = "ship14"
}
Monsters_170[1007008] = {
  ID = 1007008,
  durability = 2879759,
  Avatar = "head26",
  Ship = "ship14"
}
Monsters_170[1007009] = {
  ID = 1007009,
  durability = 3856568,
  Avatar = "head26",
  Ship = "ship14"
}
Monsters_170[1007010] = {
  ID = 1007010,
  durability = 4360331,
  Avatar = "head26",
  Ship = "ship14"
}
Monsters_170[1007011] = {
  ID = 1007011,
  durability = 6581977,
  Avatar = "head26",
  Ship = "ship14"
}
Monsters_170[1007012] = {
  ID = 1007012,
  durability = 4603532,
  Avatar = "head26",
  Ship = "ship14"
}
Monsters_170[1007013] = {
  ID = 1007013,
  durability = 2895242,
  Avatar = "head26",
  Ship = "ship14"
}
Monsters_170[1007014] = {
  ID = 1007014,
  durability = 3877302,
  Avatar = "head26",
  Ship = "ship14"
}
Monsters_170[1007015] = {
  ID = 1007015,
  durability = 4383773,
  Avatar = "head26",
  Ship = "ship14"
}
Monsters_170[1007016] = {
  ID = 1007016,
  durability = 6617175,
  Avatar = "head26",
  Ship = "ship14"
}
Monsters_170[1007017] = {
  ID = 1007017,
  durability = 4628150,
  Avatar = "head26",
  Ship = "ship14"
}
Monsters_170[1007018] = {
  ID = 1007018,
  durability = 2910725,
  Avatar = "head26",
  Ship = "ship14"
}
Monsters_170[1007019] = {
  ID = 1007019,
  durability = 3898037,
  Avatar = "head26",
  Ship = "ship14"
}
Monsters_170[1007020] = {
  ID = 1007020,
  durability = 4407216,
  Avatar = "head26",
  Ship = "ship14"
}
Monsters_170[1007021] = {
  ID = 1007021,
  durability = 6652372,
  Avatar = "head26",
  Ship = "ship14"
}
Monsters_170[1007022] = {
  ID = 1007022,
  durability = 4652767,
  Avatar = "head32",
  Ship = "ship25"
}
Monsters_170[1007023] = {
  ID = 1007023,
  durability = 2926207,
  Avatar = "head26",
  Ship = "ship14"
}
Monsters_170[1007024] = {
  ID = 1007024,
  durability = 3918771,
  Avatar = "head26",
  Ship = "ship14"
}
Monsters_170[1007025] = {
  ID = 1007025,
  durability = 4430658,
  Avatar = "head26",
  Ship = "ship14"
}
Monsters_170[1007026] = {
  ID = 1007026,
  durability = 6687570,
  Avatar = "head26",
  Ship = "ship14"
}
Monsters_170[1007027] = {
  ID = 1007027,
  durability = 4677385,
  Avatar = "head26",
  Ship = "ship14"
}
Monsters_170[1007028] = {
  ID = 1007028,
  durability = 2941690,
  Avatar = "head26",
  Ship = "ship14"
}
Monsters_170[1007029] = {
  ID = 1007029,
  durability = 3939505,
  Avatar = "head26",
  Ship = "ship14"
}
Monsters_170[1007030] = {
  ID = 1007030,
  durability = 4454101,
  Avatar = "head26",
  Ship = "ship14"
}
Monsters_170[1007031] = {
  ID = 1007031,
  durability = 6722768,
  Avatar = "head34",
  Ship = "ship70"
}
Monsters_170[1007032] = {
  ID = 1007032,
  durability = 4702003,
  Avatar = "head32",
  Ship = "ship25"
}
Monsters_170[1007033] = {
  ID = 1007033,
  durability = 2957172,
  Avatar = "head32",
  Ship = "ship25"
}
Monsters_170[1007034] = {
  ID = 1007034,
  durability = 3960239,
  Avatar = "head32",
  Ship = "ship25"
}
Monsters_170[1007035] = {
  ID = 1007035,
  durability = 4477544,
  Avatar = "head32",
  Ship = "ship25"
}
Monsters_170[1007036] = {
  ID = 1007036,
  durability = 6757966,
  Avatar = "head32",
  Ship = "ship25"
}
Monsters_170[1007037] = {
  ID = 1007037,
  durability = 4726621,
  Avatar = "head32",
  Ship = "ship25"
}
Monsters_170[1007038] = {
  ID = 1007038,
  durability = 2972655,
  Avatar = "head32",
  Ship = "ship25"
}
Monsters_170[1007039] = {
  ID = 1007039,
  durability = 3980974,
  Avatar = "head32",
  Ship = "ship25"
}
Monsters_170[1007040] = {
  ID = 1007040,
  durability = 4500986,
  Avatar = "head32",
  Ship = "ship25"
}
Monsters_170[1008001] = {
  ID = 1008001,
  durability = 6793163,
  Avatar = "head71",
  Ship = "ship39"
}
Monsters_170[1008002] = {
  ID = 1008002,
  durability = 4751239,
  Avatar = "head71",
  Ship = "ship37"
}
Monsters_170[1008003] = {
  ID = 1008003,
  durability = 2988137,
  Avatar = "head71",
  Ship = "ship5"
}
Monsters_170[1008004] = {
  ID = 1008004,
  durability = 4001708,
  Avatar = "head161",
  Ship = "ship176"
}
Monsters_170[1008005] = {
  ID = 1008005,
  durability = 4524429,
  Avatar = "head71",
  Ship = "ship39"
}
Monsters_170[1008006] = {
  ID = 1008006,
  durability = 6828361,
  Avatar = "head71",
  Ship = "ship37"
}
Monsters_170[1008007] = {
  ID = 1008007,
  durability = 4775857,
  Avatar = "head71",
  Ship = "ship5"
}
Monsters_170[1008008] = {
  ID = 1008008,
  durability = 3003620,
  Avatar = "head71",
  Ship = "ship39"
}
Monsters_170[1008009] = {
  ID = 1008009,
  durability = 4022442,
  Avatar = "head71",
  Ship = "ship37"
}
Monsters_170[1008010] = {
  ID = 1008010,
  durability = 4547872,
  Avatar = "head71",
  Ship = "ship5"
}
Monsters_170[1008011] = {
  ID = 1008011,
  durability = 6863559,
  Avatar = "head71",
  Ship = "ship39"
}
Monsters_170[1008012] = {
  ID = 1008012,
  durability = 4800474,
  Avatar = "head71",
  Ship = "ship37"
}
Monsters_170[1008013] = {
  ID = 1008013,
  durability = 3019103,
  Avatar = "head71",
  Ship = "ship5"
}
Monsters_170[1008014] = {
  ID = 1008014,
  durability = 4043176,
  Avatar = "head71",
  Ship = "ship39"
}
Monsters_170[1008015] = {
  ID = 1008015,
  durability = 4571314,
  Avatar = "head71",
  Ship = "ship37"
}
Monsters_170[1008016] = {
  ID = 1008016,
  durability = 6898757,
  Avatar = "head71",
  Ship = "ship5"
}
Monsters_170[1008017] = {
  ID = 1008017,
  durability = 4825092,
  Avatar = "head71",
  Ship = "ship39"
}
Monsters_170[1008018] = {
  ID = 1008018,
  durability = 3034585,
  Avatar = "head71",
  Ship = "ship37"
}
Monsters_170[1008019] = {
  ID = 1008019,
  durability = 4063911,
  Avatar = "head71",
  Ship = "ship5"
}
Monsters_170[1008020] = {
  ID = 1008020,
  durability = 4594757,
  Avatar = "head71",
  Ship = "ship39"
}
Monsters_170[1008021] = {
  ID = 1008021,
  durability = 6933954,
  Avatar = "head71",
  Ship = "ship37"
}
Monsters_170[1008022] = {
  ID = 1008022,
  durability = 4849710,
  Avatar = "head71",
  Ship = "ship5"
}
Monsters_170[1008023] = {
  ID = 1008023,
  durability = 3050068,
  Avatar = "head71",
  Ship = "ship39"
}
Monsters_170[1008024] = {
  ID = 1008024,
  durability = 4084645,
  Avatar = "head71",
  Ship = "ship37"
}
Monsters_170[1008025] = {
  ID = 1008025,
  durability = 4618200,
  Avatar = "head71",
  Ship = "ship5"
}
Monsters_170[1008026] = {
  ID = 1008026,
  durability = 6969152,
  Avatar = "head71",
  Ship = "ship39"
}
Monsters_170[1008027] = {
  ID = 1008027,
  durability = 4874328,
  Avatar = "head71",
  Ship = "ship37"
}
Monsters_170[1008028] = {
  ID = 1008028,
  durability = 3065550,
  Avatar = "head71",
  Ship = "ship5"
}
Monsters_170[1008029] = {
  ID = 1008029,
  durability = 4105379,
  Avatar = "head71",
  Ship = "ship39"
}
Monsters_170[1008030] = {
  ID = 1008030,
  durability = 4641642,
  Avatar = "head71",
  Ship = "ship37"
}
Monsters_170[1008031] = {
  ID = 1008031,
  durability = 7004350,
  Avatar = "head71",
  Ship = "ship5"
}
Monsters_170[1008032] = {
  ID = 1008032,
  durability = 4898946,
  Avatar = "head168",
  Ship = "ship178"
}
Monsters_170[1008033] = {
  ID = 1008033,
  durability = 3081033,
  Avatar = "head71",
  Ship = "ship70"
}
Monsters_170[1008034] = {
  ID = 1008034,
  durability = 4126113,
  Avatar = "head71",
  Ship = "ship70"
}
Monsters_170[1008035] = {
  ID = 1008035,
  durability = 4665085,
  Avatar = "head71",
  Ship = "ship70"
}
Monsters_170[1008036] = {
  ID = 1008036,
  durability = 7039548,
  Avatar = "head71",
  Ship = "ship70"
}
Monsters_170[1008037] = {
  ID = 1008037,
  durability = 4923564,
  Avatar = "head71",
  Ship = "ship70"
}
Monsters_170[1008038] = {
  ID = 1008038,
  durability = 3096516,
  Avatar = "head71",
  Ship = "ship70"
}
Monsters_170[1008039] = {
  ID = 1008039,
  durability = 4146848,
  Avatar = "head71",
  Ship = "ship70"
}
Monsters_170[1008040] = {
  ID = 1008040,
  durability = 4688528,
  Avatar = "head71",
  Ship = "ship70"
}
