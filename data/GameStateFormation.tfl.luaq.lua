local GameStateBase = GameStateBase
local GameStateFormation = GameStateManager.GameStateFormation
local GameStateBattlePlay = GameStateManager.GameStateBattlePlay
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameObjectDragger = LuaObjectManager:GetLuaObject("GameObjectDragger")
local GameObjectDraggerFleet = GameObjectDragger:NewInstance("VesselsDragger.tfs", false)
local GameObjectTutorialCutscene = LuaObjectManager:GetLuaObject("GameObjectTutorialCutscene")
local GameObjectFormationBackground = LuaObjectManager:GetLuaObject("GameObjectFormationBackground")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameUIEnemyInfo = LuaObjectManager:GetLuaObject("GameUIEnemyInfo")
local QuestTutorialSkill = TutorialQuestManager.QuestTutorialSkill
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local GameStateDaily = GameStateManager.GameStateDaily
GameStateFormation.isNeedRequestMatrixInfo = true
GameStateFormation.enemyMatrixData = nil
GameStateFormation.enemyForce = 0
GameStateFormation.isArenaEnter = false
GameStateFormation.ArenaForce = 0
function GameObjectDraggerFleet:OnAddToGameState(state)
  local commander_data = GameGlobalData:GetFleetInfo(self.DraggedFleetID)
  if GameStateFormation.m_prevState == GameStateManager.GameStateLab and GameStateManager.GameStateLab:IsInCrusadeState() then
    commander_data = GameUICrusade:GetSelfFleet(self.DraggedFleetID)
  end
  DebugOut("OnAddToGameState:", self.DraggedFleetGridIndex, self.DraggedFleetID)
  if GameObjectFormationBackground:IsExtraHero(self.DraggedFleetGridIndex, self.DraggedFleetID) then
    commander_data = GameObjectFormationBackground:GenerateExtraFleetData(self.DraggedFleetID)
  end
  local vessels_image = GameDataAccessHelper:GetCommanderVesselsImage(self.DraggedFleetID, commander_data.level, false)
  local commander_avatar = GameDataAccessHelper:GetFleetAvatar(self.DraggedFleetID, commander_data.level)
  local vessels_type = GameDataAccessHelper:GetCommanderVesselsType(self.DraggedFleetID, commander_data.level)
  local commander_sex = GameDataAccessHelper:GetCommanderSex(self.DraggedFleetID)
  DebugOut("extra_id:", self.ExtraId)
  if self.ExtraId ~= -1 and self.DraggedFleetID == 1 then
    vessels_image = GameDataAccessHelper:GetCommanderVesselsImage(self.ExtraId, commander_data.level, false)
    commander_avatar = GameDataAccessHelper:GetFleetAvatar(self.ExtraId, commander_data.level)
    vessels_type = GameDataAccessHelper:GetCommanderVesselsType(self.ExtraId, commander_data.level)
    commander_sex = GameDataAccessHelper:GetCommanderSex(self.ExtraId)
  end
  if commander_sex == 1 then
    commander_sex = "man"
  elseif commander_sex == 0 then
    commander_sex = "woman"
  else
    commander_sex = "unknown"
  end
  local commanderID = commander_data.identity
  local commander_color = GameDataAccessHelper:GetCommanderColorFrame(commanderID, commander_data.level)
  local showSex = GameStateFormation.m_prevState == GameStateManager.GameStateInstance and GameStateManager.GameStateLab.mCurSubState == STATE_LAB_SUB_STATE.SUBSTATE_COSMOS
  self:GetFlashObject():InvokeASCallback("_root", "setVesselsData", vessels_image, commander_avatar, vessels_type, commanderID, commander_color, commander_sex, showSex)
  self:GetFlashObject():InvokeASCallback("_root", "animationBigger")
end
function GameObjectDraggerFleet:AnimationSmaller()
  self:GetFlashObject():InvokeASCallback("_root", "animationSmaller")
end
function GameObjectDraggerFleet:OnEraseFromGameState(state)
  DebugOut("erase fleetDragger from fleet")
  self.IsDraggedFleetInFreelist = false
  self.DraggedFleetGridIndex = -1
  self.DraggedFleetID = -1
  self.ExtraId = -1
  self.IsReleasedInFreeList = false
  self.ReleasedGridIndex = -1
  self.MoveBackDestGridIndex = -1
  self.IsMoveBackToFreeList = false
  self.MoveBackFleetID = -1
  local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
  if GameStateManager:GetCurrentGameState():IsObjectInState(ItemBox) then
    GameStateManager:GetCurrentGameState():EraseObject(ItemBox)
  end
end
function GameObjectDraggerFleet:SetAutoMoveTo(grid_type, grid_index, fleet_id)
  if grid_type == "rest" or grid_type == "battle" then
    GameObjectDraggerFleet.DragToGridType = grid_type
    GameObjectDraggerFleet.DragToGridIndex = grid_index
    local dest_pos = GameObjectFormationBackground:GetCommanderGridPos(grid_type, grid_index)
    assert(dest_pos)
    self:SetDestPosition(dest_pos._x, dest_pos._y)
    self:StartAutoMove()
  else
    assert(false)
  end
end
function GameObjectDraggerFleet:OnReachedDestPos()
  if GameObjectDraggerFleet.DragToGridType and GameObjectDraggerFleet.DragToGridIndex then
    GameObjectFormationBackground:OnLocateCommander(GameObjectDraggerFleet)
    GameStateFormation:EraseObject(self)
  else
    assert(false)
    GameObjectFormationBackground:OnDragedFleetMoveBack(self)
    GameStateFormation:EraseObject(self)
  end
end
function GameStateFormation:BeginDragFleet(fleetID, isInFreeList, initPosX, initPosY, mouseX, mouseY, extra_id)
  DebugOut("BeginDragFleet " .. tostring(isInFreeList) .. " posX:" .. initPosX .. " posY:" .. initPosY .. " mouseX:" .. mouseX .. "mouseY :" .. mouseY)
  GameStateManager:GetCurrentGameState():AddObject(GameObjectDraggerFleet)
  GameObjectDraggerFleet.DraggedFleetID = fleetID
  if fleetID == 1 and extra_id ~= nil then
    GameObjectDraggerFleet.ExtraId = extra_id
  end
  if isInFreeList then
    GameObjectDraggerFleet:BeginDrag(-70, -70, mouseX, mouseY)
  else
    GameObjectDraggerFleet:BeginDrag(-70, -70, mouseX, mouseY)
  end
  GameObjectDraggerFleet:SetDestPosition(initPosX, initPosY)
  GameObjectFormationBackground:OnBeginDragFleet(GameObjectDraggerFleet)
end
function GameStateFormation:InitGameState()
end
function GameStateFormation:SetBattleID(areaID, battleID)
  self.m_currentAreaID = areaID
  self.m_currentBattleID = battleID
end
function GameStateFormation:SetEnemyInfo(name, avatar, power)
  self.enemy_avatar = avatar
  self.enemy_name = name
  self.enemyForce = power
end
function GameStateFormation:SetFleetsBuffer(fleetbuff)
  self.fleetsBuffer = fleetbuff
end
function GameStateFormation:OnFocusGain(previousState)
  if GameStateFormation.mPreparePrestigeBack then
    GameStateFormation:RestoreDataForBackFromPrestige()
    GameStateFormation.mPreparePrestigeBack = false
  else
    self.m_prevState = previousState
  end
  if self.m_prevState == GameStateManager.GameStateInstance then
    GameStateBattlePlay.isLadderBattle = true
  else
    GameStateBattlePlay.isLadderBattle = false
  end
  GameObjectDraggerFleet:LoadFlashObject()
  GameObjectDraggerFleet:Init()
  DebugOut("m_currentAreaID", self.m_currentAreaID, "m_currentBattleID", self.m_currentBattleID)
  if self.m_currentAreaID == 1 and self.m_currentBattleID == 1007 then
    GameObjectFormationBackground.needAddHero = true
  else
    GameObjectFormationBackground.needAddHero = false
  end
  self.GameObjectDraggerFleet = GameObjectDraggerFleet
  self:AddObject(GameObjectFormationBackground)
  DebugTable(self.enemyMatrixData)
  if not self.isNeedRequestMatrixInfo and self.enemyMatrixData ~= nil then
    DebugOut("GameStateFormation:OnFocusGain")
    GameObjectFormationBackground.isNeedRequestMatrixInfo = false
    GameObjectFormationBackground:SetEnemiesMatrixData(self.enemyMatrixData)
    GameObjectFormationBackground:SetFleetsBuff(self.fleetsBuffer)
    GameObjectFormationBackground.force = GameStateFormation.enemyForce
    GameObjectFormationBackground.enemy_name = self.enemy_name
    GameObjectFormationBackground.enemy_avatar = self.enemy_avatar
    self.isNeedRequestMatrixInfo = true
  else
    DebugOut("ameStateFormation:OnFocusGain")
    DebugOut(self.isNeedRequestMatrixInfo)
  end
  if not GameObjectFormationBackground:HasTutorialStroy() then
  end
  if self.m_currentAreaID == 60 and self.m_currentBattleID == 1003 and immanentversion == 2 and not QuestTutorialSkill:IsActive() and not QuestTutorialSkill:IsFinished() then
    QuestTutorialSkill:SetActive(true)
  end
end
function GameStateFormation:OnFocusLost(newState)
  local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
  if GameStateFormation:IsObjectInState(ItemBox) then
    GameStateFormation:EraseObject(ItemBox)
  end
  self:EraseObject(GameObjectDraggerFleet)
  self:EraseObject(GameObjectFormationBackground)
  GameObjectDraggerFleet:UnloadFlashObject()
  self.m_currentBattleID = nil
  self.fleetsBuffer = nil
  GameStateFormation.isArenaEnter = false
end
function GameStateFormation:OnTouchPressed(x, y)
  if GameObjectDraggerFleet:IsAutoMove() then
    return
  end
  GameStateBase.OnTouchPressed(self, x, y)
end
function GameStateFormation:OnTouchMoved(x, y)
  if GameObjectDraggerFleet:IsAutoMove() then
    return
  end
  GameStateBase.OnTouchMoved(self, x, y)
  local dragedFleetID = GameObjectDraggerFleet.DraggedFleetID
  if dragedFleetID and dragedFleetID ~= -1 then
    GameObjectDraggerFleet:UpdateDrag()
  end
end
function GameStateFormation:OnTouchReleased(x, y)
  if GameObjectDraggerFleet:IsAutoMove() then
    return
  end
  GameStateBase.OnTouchReleased(self, x, y)
  local dragedFleetID = GameObjectDraggerFleet.DraggedFleetID
  if dragedFleetID and dragedFleetID ~= -1 then
    do
      local releaseInfo = GameObjectFormationBackground:GetFlashObject():InvokeASCallback("_root", "getInfoWhenDragRelease")
      local located_type, located_pos = GameObjectFormationBackground:GetCommanderLocatedInfo()
      located_pos = tonumber(located_pos)
      local from_grid_type = GameObjectDraggerFleet.DraggedFleetGridType
      local from_grid_index = GameObjectDraggerFleet.DraggedFleetGridIndex
      if located_type and located_pos then
        if located_type == "rest" then
          local located_commadner_id = GameObjectFormationBackground:GetCommanderIDFromGrid(located_type, located_pos)
          GameObjectDraggerFleet.IsReleasedInFreeList = true
          GameObjectDraggerFleet.ReleasedGridIndex = located_pos
          if dragedFleetID == 1 then
            GameTip:Show(GameLoader:GetGameText("LC_MENU_LOADING_HINT_9"), 3000)
            GameObjectDraggerFleet:SetAutoMoveTo(from_grid_type, from_grid_index, dragedFleetID)
          elseif GameObjectFormationBackground:IsExtraHero(from_grid_index, dragedFleetID) then
            GameTip:Show(GameLoader:GetGameText("LC_ALERT_can_not_adjust_queue"), 3000)
            GameObjectDraggerFleet:SetAutoMoveTo(from_grid_type, from_grid_index, dragedFleetID)
          elseif located_commadner_id > 0 then
            do
              local ret, major, adjutant = GameObjectFormationBackground:CheckAdjutant(GameObjectDraggerFleet.DraggedFleetGridType, located_type, dragedFleetID, GameObjectDraggerFleet.DraggedFleetGridIndex, located_pos)
              if 0 == ret then
                DebugOut("OK")
                GameObjectDraggerFleet:SetAutoMoveTo(located_type, located_pos)
              elseif -1 == ret then
                DebugOut("Out of max count adjutant can battle.")
                GameTip:Show(GameLoader:GetGameText("LC_MENU_BRIDGE_INFO_4"), 3000)
                GameObjectDraggerFleet:SetAutoMoveTo(from_grid_type, from_grid_index, dragedFleetID)
              elseif 1 == ret then
                DebugOut(tostring(adjutant) .. " is " .. tostring(major) .. "'s adjutant, but " .. tostring(major) .. " is on battle.")
                do
                  local tip = GameLoader:GetGameText("LC_MENU_BRIDGE_INFO_3")
                  local adjutantName = GameDataAccessHelper:GetFleetLevelDisplayName(adjutant)
                  local majorName = GameDataAccessHelper:GetFleetLevelDisplayName(major)
                  tip = string.gsub(tip, "<npc2_name>", adjutantName)
                  tip = string.gsub(tip, "<npc1_name>", majorName)
                  local function releaseAdjutantResultCallback(success)
                    DebugOut("releaseAdjutantResultCallback " .. tostring(success))
                    if success then
                      GameObjectFormationBackground:UpdateBattleCommanderByFleetId(major)
                      GameObjectDraggerFleet:SetAutoMoveTo(located_type, located_pos, dragedFleetID)
                    else
                      GameObjectDraggerFleet:SetAutoMoveTo(from_grid_type, from_grid_index, dragedFleetID)
                    end
                  end
                  local function okCallback()
                    GameStateFormation:RequestReleaseAdjutant(major, releaseAdjutantResultCallback)
                  end
                  local function cancelCallback()
                    GameObjectDraggerFleet:SetAutoMoveTo(from_grid_type, from_grid_index, dragedFleetID)
                  end
                  DebugOut("GameUIGlobalScreen2")
                  GameUIGlobalScreen:ShowMessageBox(2, "", tip, okCallback, cancelCallback)
                end
              else
                GameObjectDraggerFleet:SetAutoMoveTo(located_type, located_pos)
              end
            end
          else
            GameObjectDraggerFleet:SetAutoMoveTo(located_type, located_pos)
          end
        elseif located_type == "battle" then
          local located_commadner_id = GameObjectFormationBackground:GetCommanderIDFromGrid(located_type, located_pos)
          local validCommanderCount = GameGlobalData:GetData("matrix").count
          local commanderCount = GameObjectFormationBackground:CountBattleCommander()
          local commanderCountCheck = validCommanderCount > commanderCount or commanderCount == validCommanderCount and located_commadner_id > 0
          if located_commadner_id == 1 and GameObjectDraggerFleet.DraggedFleetGridType == "rest" then
            GameTip:Show(GameLoader:GetGameText("LC_MENU_LOADING_HINT_9"), 3000)
            GameObjectDraggerFleet:SetAutoMoveTo(from_grid_type, from_grid_index, dragedFleetID)
          elseif GameObjectFormationBackground:IsExtraHero(located_pos, located_commadner_id) or GameObjectFormationBackground:IsExtraHero(from_grid_index, dragedFleetID) then
            GameTip:Show(GameLoader:GetGameText("LC_ALERT_can_not_adjust_queue"), 3000)
            GameObjectDraggerFleet:SetAutoMoveTo(from_grid_type, from_grid_index, dragedFleetID)
          elseif from_grid_type == "rest" and not commanderCountCheck then
            GameObjectDraggerFleet:SetAutoMoveTo(from_grid_type, from_grid_index, dragedFleetID)
            local tip = GameLoader:GetGameText("LC_ALERT_fleet_count_over_max_new")
            local function okCallback()
              GameHelper:SetEnterPage(GameHelper.PagePrestige)
              GameStateManager:SetCurrentGameState(GameStateDaily)
            end
            local cancelCallback = function()
            end
            DebugOut("GameUIGlobalScreen3")
            GameUIGlobalScreen:ShowMessageBox(2, "", tip, okCallback, cancelCallback)
          else
            DebugOut("```3")
            do
              local ret, major, adjutant = GameObjectFormationBackground:CheckAdjutant(GameObjectDraggerFleet.DraggedFleetGridType, located_type, dragedFleetID, GameObjectDraggerFleet.DraggedFleetGridIndex, located_pos)
              if 0 == ret then
                DebugOut("OK")
                GameObjectDraggerFleet:SetAutoMoveTo(located_type, located_pos, dragedFleetID)
              elseif -1 == ret then
                DebugOut("Out of max count adjutant can battle.")
                GameTip:Show(GameLoader:GetGameText("LC_MENU_BRIDGE_INFO_4"), 3000)
                GameObjectDraggerFleet:SetAutoMoveTo(from_grid_type, from_grid_index, dragedFleetID)
              elseif ret > 0 then
                DebugOut(tostring(adjutant) .. " is " .. tostring(major) .. "'s adjutant, but " .. tostring(major) .. " is on battle.")
                do
                  local tip = GameLoader:GetGameText("LC_MENU_BRIDGE_INFO_3")
                  DebugOut("tip " .. tip)
                  local adjutantName = GameDataAccessHelper:GetFleetLevelDisplayName(adjutant)
                  local majorName = GameDataAccessHelper:GetFleetLevelDisplayName(major)
                  tip = string.gsub(tip, "<npc2_name>", adjutantName)
                  tip = string.gsub(tip, "<npc1_name>", majorName)
                  local function releaseAdjutantResultCallback(success)
                    DebugOut("releaseAdjutantResultCallback " .. tostring(success))
                    if success then
                      GameObjectFormationBackground:UpdateBattleCommanderByFleetId(major)
                      GameObjectDraggerFleet:SetAutoMoveTo(located_type, located_pos, dragedFleetID)
                    else
                      GameObjectDraggerFleet:SetAutoMoveTo(from_grid_type, from_grid_index, dragedFleetID)
                    end
                  end
                  local function okCallback()
                    GameStateFormation:RequestReleaseAdjutant(major, releaseAdjutantResultCallback)
                  end
                  local function cancelCallback()
                    GameObjectDraggerFleet:SetAutoMoveTo(from_grid_type, from_grid_index, dragedFleetID)
                  end
                  DebugOut("GameUIGlobalScreen1")
                  GameUIGlobalScreen:ShowMessageBox(2, "", tip, okCallback, cancelCallback)
                end
              else
                GameObjectDraggerFleet:SetAutoMoveTo(located_type, located_pos, dragedFleetID)
              end
            end
          end
        else
          assert(false)
        end
      else
        GameObjectDraggerFleet:SetAutoMoveTo(from_grid_type, from_grid_index, dragedFleetID)
      end
      GameObjectDraggerFleet:AnimationSmaller()
    end
  end
end
GameStateFormation.ReleaseAdjutantResultCallback = nil
function GameStateFormation:RequestReleaseAdjutant(major, callback)
  DebugOut("RequestReleaseAdjutant " .. tostring(major))
  local req = {major = major}
  GameStateFormation.ReleaseAdjutantResultCallback = callback
  NetMessageMgr:SendMsg(NetAPIList.release_adjutant_req.Code, req, self.RequestReleaseAdjutantCallback, true, nil)
end
function GameStateFormation.RequestReleaseAdjutantCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.release_adjutant_req.Code then
    if 0 == content.code then
      if GameStateFormation.ReleaseAdjutantResultCallback then
        GameStateFormation.ReleaseAdjutantResultCallback(true)
      end
    else
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      if GameStateFormation.ReleaseAdjutantResultCallback then
        GameStateFormation.ReleaseAdjutantResultCallback(false)
      end
    end
    return true
  end
  return false
end
GameStateFormation.mPreparePrestigeBack = false
GameStateFormation.mBackupData = nil
function GameStateFormation:PreparePrestigeBack()
  GameStateFormation.mPreparePrestigeBack = true
end
function GameStateFormation:BackupDataForGotoPrestige()
  GameStateFormation.mBackupData = {
    mStateData = {},
    mUIData = {}
  }
  GameStateFormation.mBackupData.mStateData.mPreState = GameStateFormation.m_prevState
  GameStateFormation.mBackupData.mStateData.enemyMatrixData = GameStateFormation.enemyMatrixData
  GameStateFormation.mBackupData.mStateData.enemyForce = GameStateFormation.enemyForce
  GameStateFormation.mBackupData.mStateData.isNeedRequestMatrixInfo = GameStateFormation.isNeedRequestMatrixInfo
  GameStateFormation.mBackupData.mStateData.m_currentAreaID = GameStateFormation.m_currentAreaID
  GameStateFormation.mBackupData.mStateData.m_currentBattleID = GameStateFormation.m_currentBattleID
  GameStateFormation.mBackupData.mUIData.force = GameObjectFormationBackground.force
  GameStateFormation.mBackupData.mUIData.enemy_matrix = GameObjectFormationBackground.enemy_matrix
  GameStateFormation.mBackupData.mUIData.m_enemysFormation = GameObjectFormationBackground.m_enemysFormation
  GameStateFormation.mBackupData.mUIData.jamedCount = GameObjectFormationBackground.jamedCount
end
function GameStateFormation:RestoreDataForBackFromPrestige()
  if GameStateFormation.mBackupData then
    GameStateFormation.m_prevState = GameStateFormation.mBackupData.mStateData.mPreState
    GameStateFormation.enemyMatrixData = GameStateFormation.mBackupData.mStateData.enemyMatrixData
    GameStateFormation.enemyForce = GameStateFormation.mBackupData.mStateData.enemyForce
    GameStateFormation.isNeedRequestMatrixInfo = GameStateFormation.mBackupData.mStateData.isNeedRequestMatrixInfo
    GameStateFormation.m_currentAreaID = GameStateFormation.mBackupData.mStateData.m_currentAreaID
    GameStateFormation.m_currentBattleID = GameStateFormation.mBackupData.mStateData.m_currentBattleID
    GameObjectFormationBackground.force = GameStateFormation.mBackupData.mUIData.force
    GameObjectFormationBackground.enemy_matrix = GameStateFormation.mBackupData.mUIData.enemy_matrix
    GameObjectFormationBackground.m_enemysFormation = GameStateFormation.mBackupData.mUIData.m_enemysFormation
    GameObjectFormationBackground.jamedCount = GameStateFormation.mBackupData.mUIData.jamedCount
    GameStateFormation.mBackupData = nil
  end
end
