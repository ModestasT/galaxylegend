local FacebookStoryAwardUI = LuaObjectManager:GetLuaObject("FacebookStoryAwardUI")
local GameStateGlobalState = GameStateManager.GameStateGlobalState
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
FacebookStoryAwardUI.mSharedCount = {}
FacebookStoryAwardUI.mAwardFactor = {}
function FacebookStoryAwardUI:OnAddToGameState()
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
end
function FacebookStoryAwardUI:OnEraseFromGameState()
  self:UnloadFlashObject()
end
function FacebookStoryAwardUI:GetShareStoryAward(shareType)
end
function FacebookStoryAwardUI.GetShareStoryCallback(msgType, content)
end
function FacebookStoryAwardUI:SetUpAwards(awardsInfo)
end
function FacebookStoryAwardUI:Show(awardsInfo)
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  if not GameStateGlobalState:IsObjectInState(self) then
    GameStateGlobalState:AddObject(self)
  end
  self:GetFlashObject():InvokeASCallback("_root", "ShowStoryAwards", awardsInfo)
end
function FacebookStoryAwardUI:GenerateRewards()
end
function FacebookStoryAwardUI:OnFSCommand(cmd, arg)
  if cmd == "moveout_over" then
    GameStateGlobalState:EraseObject(self)
  end
end
function FacebookStoryAwardUI:GetAwardByType(storyType)
  local awardInfo = {}
  local awardItem = FacebookGraphStorySharer.mAwards[storyType].awards[1]
  if awardItem then
    awardInfo.iconFrame = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(awardItem, nil, nil)
    awardInfo.critical = FacebookGraphStorySharer.mAwards[storyType].strike
    awardInfo.awardNum = GameHelper:GetAwardCount(awardItem.item_type, awardItem.number, awardItem.no)
    DebugOut("awardInfo = ")
    DebugTable(awardInfo)
  end
  return awardInfo
end
