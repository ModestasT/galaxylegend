local GameStateBase = GameStateBase
local GameStateTeamLeagueAtkFormation = GameStateManager.GameStateTeamLeagueAtkFormation
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameObjectDragger = LuaObjectManager:GetLuaObject("GameObjectDragger")
local GameObjectDraggerFleet = GameObjectDragger:NewInstance("VesselsDragger.tfs", false)
local GameObjectTeamLeagueAtkFormation = LuaObjectManager:GetLuaObject("GameObjectTeamLeagueAtkFormation")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameUIEnemyInfo = LuaObjectManager:GetLuaObject("GameUIEnemyInfo")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
function GameObjectDraggerFleet:OnAddToGameState(state)
  local commander_data = GameGlobalData:GetFleetInfo(self.DraggedFleetID)
  DebugOut("OnAddToGameState:", self.DraggedFleetGridIndex, self.DraggedFleetID)
  local vessels_image = GameDataAccessHelper:GetCommanderVesselsImage(self.DraggedFleetID, commander_data.level, false)
  local commander_avatar = GameDataAccessHelper:GetFleetAvatar(self.DraggedFleetID, commander_data.level)
  local vessels_type = GameDataAccessHelper:GetCommanderVesselsType(self.DraggedFleetID, commander_data.level)
  local commander_sex = GameDataAccessHelper:GetCommanderSex(self.DraggedFleetID)
  if self.ExtraId ~= -1 and self.ExtraId ~= nil then
    DebugOut("self.ExtraId = " .. self.ExtraId)
    vessels_image = GameDataAccessHelper:GetCommanderVesselsImage(self.ExtraId, commander_data.level, false)
    commander_avatar = GameDataAccessHelper:GetFleetAvatar(self.ExtraId, commander_data.level)
    vessels_type = GameDataAccessHelper:GetCommanderVesselsType(self.ExtraId, commander_data.level)
    commander_sex = GameDataAccessHelper:GetCommanderSex(self.ExtraId)
  end
  if commander_sex == 1 then
    commander_sex = "man"
  elseif commander_sex == 0 then
    commander_sex = "woman"
  else
    commander_sex = "unknown"
  end
  local commanderID = commander_data.identity
  local commander_color = GameDataAccessHelper:GetCommanderColorFrame(commanderID, commander_data.level)
  if self.ExtraId ~= -1 and self.ExtraId ~= nil then
    commander_color = GameDataAccessHelper:GetCommanderColorFrame(self.ExtraId, commander_data.level)
  end
  local showSex = true
  self:GetFlashObject():InvokeASCallback("_root", "setVesselsData", vessels_image, commander_avatar, vessels_type, commanderID, commander_color, commander_sex, showSex)
  self:GetFlashObject():InvokeASCallback("_root", "animationBigger")
end
function GameObjectDraggerFleet:AnimationSmaller()
  self:GetFlashObject():InvokeASCallback("_root", "animationSmaller")
end
function GameObjectDraggerFleet:OnEraseFromGameState(state)
  DebugOut("erase fleetDragger from fleet")
  self.IsDraggedFleetInFreelist = false
  self.DraggedFleetGridIndex = -1
  self.DraggedFleetID = -1
  self.IsReleasedInFreeList = false
  self.ReleasedGridIndex = -1
  self.MoveBackDestGridIndex = -1
  self.IsMoveBackToFreeList = false
  self.MoveBackFleetID = -1
  self.ExtraId = -1
end
function GameObjectDraggerFleet:SetAutoMoveTo(grid_type, grid_index, fleet_id)
  if grid_type == "rest" or grid_type == "battle" then
    GameObjectDraggerFleet.DragToGridType = grid_type
    GameObjectDraggerFleet.DragToGridIndex = grid_index
    local dest_pos = GameObjectTeamLeagueAtkFormation:GetCommanderGridPos(grid_type, grid_index)
    assert(dest_pos)
    self:SetDestPosition(dest_pos._x, dest_pos._y)
    self:StartAutoMove()
  else
    assert(false)
  end
end
function GameObjectDraggerFleet:OnReachedDestPos()
  if GameObjectDraggerFleet.DragToGridType and GameObjectDraggerFleet.DragToGridIndex then
    GameObjectTeamLeagueAtkFormation:OnLocateCommander(GameObjectDraggerFleet)
    GameStateTeamLeagueAtkFormation:EraseObject(self)
  else
    assert(false)
    GameObjectTeamLeagueAtkFormation:OnDragedFleetMoveBack(self)
    GameStateTeamLeagueAtkFormation:EraseObject(self)
  end
end
function GameStateTeamLeagueAtkFormation:BeginDragFleet(fleetID, isInFreeList, initPosX, initPosY, mouseX, mouseY, extra_id)
  DebugOut("BeginDragFleet " .. tostring(isInFreeList) .. " posX:" .. initPosX .. " posY:" .. initPosY .. " mouseX:" .. mouseX .. "mouseY :" .. mouseY)
  GameStateManager:GetCurrentGameState():AddObject(GameObjectDraggerFleet)
  GameObjectDraggerFleet.DraggedFleetID = fleetID
  if isInFreeList then
    GameObjectDraggerFleet:BeginDrag(-70, -70, mouseX, mouseY)
  else
    GameObjectDraggerFleet:BeginDrag(-70, -70, mouseX, mouseY)
  end
  if fleetID == 1 and extra_id ~= nil then
    GameObjectDraggerFleet.ExtraId = extra_id
  end
  GameObjectDraggerFleet:SetDestPosition(initPosX, initPosY)
  GameObjectTeamLeagueAtkFormation:OnBeginDragFleet(GameObjectDraggerFleet)
end
function GameStateTeamLeagueAtkFormation:InitGameState()
end
function GameStateTeamLeagueAtkFormation:OnFocusGain(previousState)
  self.m_prevState = previousState
  if GameStateTeamLeagueAtkFormation.initTeamIndex == nil then
    GameStateTeamLeagueAtkFormation.initTeamIndex = 1
  end
  GameObjectTeamLeagueAtkFormation.currentMatrixIndex = GameStateTeamLeagueAtkFormation.initTeamIndex
  GameObjectDraggerFleet:LoadFlashObject()
  GameObjectDraggerFleet:Init()
  self.GameObjectDraggerFleet = GameObjectDraggerFleet
  self:AddObject(GameObjectTeamLeagueAtkFormation)
end
function GameStateTeamLeagueAtkFormation:OnFocusLost(newState)
  self:EraseObject(GameObjectDraggerFleet)
  self:EraseObject(GameObjectTeamLeagueAtkFormation)
  GameObjectDraggerFleet:UnloadFlashObject()
  GameStateTeamLeagueAtkFormation.initTeamIndex = 1
end
function GameStateTeamLeagueAtkFormation:OnTouchPressed(x, y)
  if GameObjectDraggerFleet:IsAutoMove() then
    return
  end
  GameStateBase.OnTouchPressed(self, x, y)
end
function GameStateTeamLeagueAtkFormation:OnTouchMoved(x, y)
  if GameObjectDraggerFleet:IsAutoMove() then
    return
  end
  GameStateBase.OnTouchMoved(self, x, y)
  local dragedFleetID = GameObjectDraggerFleet.DraggedFleetID
  if dragedFleetID and dragedFleetID ~= -1 then
    GameObjectDraggerFleet:UpdateDrag()
  end
end
function GameStateTeamLeagueAtkFormation:OnTouchReleased(x, y)
  if GameObjectDraggerFleet:IsAutoMove() then
    return
  end
  GameStateBase.OnTouchReleased(self, x, y)
  local dragedFleetID = GameObjectDraggerFleet.DraggedFleetID
  if dragedFleetID and dragedFleetID ~= -1 then
    do
      local releaseInfo = GameObjectTeamLeagueAtkFormation:GetFlashObject():InvokeASCallback("_root", "getInfoWhenDragRelease")
      local located_type, located_pos = GameObjectTeamLeagueAtkFormation:GetCommanderLocatedInfo()
      located_pos = tonumber(located_pos)
      local from_grid_type = GameObjectDraggerFleet.DraggedFleetGridType
      local from_grid_index = GameObjectDraggerFleet.DraggedFleetGridIndex
      if located_type and located_pos then
        if located_type == "rest" then
          local located_commadner_id = GameObjectTeamLeagueAtkFormation:GetCommanderIDFromGrid(located_type, located_pos)
          GameObjectDraggerFleet.IsReleasedInFreeList = true
          GameObjectDraggerFleet.ReleasedGridIndex = located_pos
          if dragedFleetID == 1 then
            GameTip:Show(GameLoader:GetGameText("LC_MENU_LOADING_HINT_9"), 3000)
            GameObjectDraggerFleet:SetAutoMoveTo(from_grid_type, from_grid_index, dragedFleetID)
          elseif located_commadner_id > 0 then
            do
              local ret, major, adjutant = GameObjectTeamLeagueAtkFormation:CheckAdjutant(GameObjectDraggerFleet.DraggedFleetGridType, located_type, dragedFleetID, GameObjectDraggerFleet.DraggedFleetGridIndex, located_pos)
              if 0 == ret then
                DebugOut("OK")
                GameObjectDraggerFleet:SetAutoMoveTo(located_type, located_pos)
              elseif -1 == ret then
                DebugOut("Out of max count adjutant can battle.")
                GameTip:Show(GameLoader:GetGameText("LC_MENU_BRIDGE_INFO_4"), 3000)
                GameObjectDraggerFleet:SetAutoMoveTo(from_grid_type, from_grid_index, dragedFleetID)
              elseif 1 == ret then
                DebugOut(tostring(adjutant) .. " is " .. tostring(major) .. "'s adjutant, but " .. tostring(major) .. " is on battle.")
                do
                  local tip = GameLoader:GetGameText("LC_MENU_BRIDGE_INFO_3")
                  local adjutantName = GameDataAccessHelper:GetFleetLevelDisplayName(adjutant)
                  local majorName = GameDataAccessHelper:GetFleetLevelDisplayName(major)
                  tip = string.gsub(tip, "<npc2_name>", adjutantName)
                  tip = string.gsub(tip, "<npc1_name>", majorName)
                  local function releaseAdjutantResultCallback(success)
                    DebugOut("releaseAdjutantResultCallback " .. tostring(success))
                    if success then
                      GameObjectTeamLeagueAtkFormation:UpdateBattleCommanderByFleetId(major)
                      GameObjectDraggerFleet:SetAutoMoveTo(located_type, located_pos, dragedFleetID)
                    else
                      GameObjectDraggerFleet:SetAutoMoveTo(from_grid_type, from_grid_index, dragedFleetID)
                    end
                  end
                  local function okCallback()
                    GameStateTeamLeagueAtkFormation:RequestReleaseAdjutant(major, releaseAdjutantResultCallback)
                  end
                  local function cancelCallback()
                    GameObjectDraggerFleet:SetAutoMoveTo(from_grid_type, from_grid_index, dragedFleetID)
                  end
                  GameUIGlobalScreen:ShowMessageBox(2, "", tip, okCallback, cancelCallback)
                end
              else
                GameObjectDraggerFleet:SetAutoMoveTo(located_type, located_pos)
              end
            end
          else
            GameObjectDraggerFleet:SetAutoMoveTo(located_type, located_pos)
          end
        elseif located_type == "battle" then
          local located_commadner_id = GameObjectTeamLeagueAtkFormation:GetCommanderIDFromGrid(located_type, located_pos)
          local validCommanderCount = GameGlobalData:GetData("matrix").count
          local commanderCount = GameObjectTeamLeagueAtkFormation:CountBattleCommander()
          local commanderCountCheck = validCommanderCount > commanderCount or commanderCount == validCommanderCount and located_commadner_id > 0
          if located_commadner_id == 1 and GameObjectDraggerFleet.DraggedFleetGridType == "rest" then
            GameTip:Show(GameLoader:GetGameText("LC_MENU_LOADING_HINT_9"), 3000)
            GameObjectDraggerFleet:SetAutoMoveTo(from_grid_type, from_grid_index, dragedFleetID)
          elseif from_grid_type == "rest" and not commanderCountCheck then
            GameUIGlobalScreen:ShowAlert("error", 3008, nil)
            GameObjectDraggerFleet:SetAutoMoveTo(from_grid_type, from_grid_index, dragedFleetID)
          else
            DebugOut("```3")
            do
              local ret, major, adjutant = GameObjectTeamLeagueAtkFormation:CheckAdjutant(GameObjectDraggerFleet.DraggedFleetGridType, located_type, dragedFleetID, GameObjectDraggerFleet.DraggedFleetGridIndex, located_pos)
              if 0 == ret then
                DebugOut("OK")
                GameObjectDraggerFleet:SetAutoMoveTo(located_type, located_pos, dragedFleetID)
              elseif -1 == ret then
                DebugOut("Out of max count adjutant can battle.")
                GameTip:Show(GameLoader:GetGameText("LC_MENU_BRIDGE_INFO_4"), 3000)
                GameObjectDraggerFleet:SetAutoMoveTo(from_grid_type, from_grid_index, dragedFleetID)
              elseif ret > 0 then
                DebugOut(tostring(adjutant) .. " is " .. tostring(major) .. "'s adjutant, but " .. tostring(major) .. " is on battle.")
                do
                  local tip = GameLoader:GetGameText("LC_MENU_BRIDGE_INFO_3")
                  DebugOut("tip " .. tip)
                  local adjutantName = GameDataAccessHelper:GetFleetLevelDisplayName(adjutant)
                  local majorName = GameDataAccessHelper:GetFleetLevelDisplayName(major)
                  tip = string.gsub(tip, "<npc2_name>", adjutantName)
                  tip = string.gsub(tip, "<npc1_name>", majorName)
                  local function releaseAdjutantResultCallback(success)
                    DebugOut("releaseAdjutantResultCallback " .. tostring(success))
                    if success then
                      GameObjectTeamLeagueAtkFormation:UpdateBattleCommanderByFleetId(major)
                      GameObjectDraggerFleet:SetAutoMoveTo(located_type, located_pos, dragedFleetID)
                    else
                      GameObjectDraggerFleet:SetAutoMoveTo(from_grid_type, from_grid_index, dragedFleetID)
                    end
                  end
                  local function okCallback()
                    GameStateTeamLeagueAtkFormation:RequestReleaseAdjutant(major, releaseAdjutantResultCallback)
                  end
                  local function cancelCallback()
                    GameObjectDraggerFleet:SetAutoMoveTo(from_grid_type, from_grid_index, dragedFleetID)
                  end
                  DebugOut("GameUIGlobalScreen1")
                  GameUIGlobalScreen:ShowMessageBox(2, "", tip, okCallback, cancelCallback)
                end
              else
                GameObjectDraggerFleet:SetAutoMoveTo(located_type, located_pos, dragedFleetID)
              end
            end
          end
        else
          assert(false)
        end
      else
        GameObjectDraggerFleet:SetAutoMoveTo(from_grid_type, from_grid_index, dragedFleetID)
      end
      GameObjectDraggerFleet:AnimationSmaller()
    end
  end
end
GameStateTeamLeagueAtkFormation.ReleaseAdjutantResultCallback = nil
function GameStateTeamLeagueAtkFormation:RequestReleaseAdjutant(major, callback)
  DebugOut("RequestReleaseAdjutant " .. tostring(major))
  local req = {major = major}
  GameStateTeamLeagueAtkFormation.ReleaseAdjutantResultCallback = callback
  NetMessageMgr:SendMsg(NetAPIList.release_adjutant_req.Code, req, self.RequestReleaseAdjutantCallback, true, nil)
end
function GameStateTeamLeagueAtkFormation.RequestReleaseAdjutantCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.release_adjutant_req.Code then
    if 0 == content.code then
      if GameStateTeamLeagueAtkFormation.ReleaseAdjutantResultCallback then
        GameStateTeamLeagueAtkFormation.ReleaseAdjutantResultCallback(true)
      end
    else
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      if GameStateTeamLeagueAtkFormation.ReleaseAdjutantResultCallback then
        GameStateTeamLeagueAtkFormation.ReleaseAdjutantResultCallback(false)
      end
    end
    return true
  end
  return false
end
function GameStateTeamLeagueAtkFormation:EnterState(teamIndex)
  if teamIndex ~= nil then
    GameStateTeamLeagueAtkFormation.initTeamIndex = teamIndex
  end
  GameStateManager:SetCurrentGameState(GameStateManager.GameStateTeamLeagueAtkFormation)
end
