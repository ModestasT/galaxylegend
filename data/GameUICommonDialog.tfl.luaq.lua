local GameUICommonDialog = LuaObjectManager:GetLuaObject("GameUICommonDialog")
local GameNewMenuItem = LuaObjectManager:GetLuaObject("GameNewMenuItem")
local GameSetting = LuaObjectManager:GetLuaObject("GameSetting")
local GameStateMainPlanet = GameStateManager.GameStateMainPlanet
local GameStateGlobalState = GameStateManager.GameStateGlobalState
local GameObjectTutorialCutscene = LuaObjectManager:GetLuaObject("GameObjectTutorialCutscene")
GameUICommonDialog.m_dialogList = {}
GameUICommonDialog.m_dialogIndex = {}
GameUICommonDialog.m_playedStory = {}
GameUICommonDialog.showedLeftAvatar = false
GameUICommonDialog.showedRightAvatar = false
GameUICommonDialog.PendingStory = {}
function GameUICommonDialog:HasPendingStory()
  return self.m_dialogList and #self.m_dialogList > 0
end
function GameUICommonDialog:AddDialogue(dialoguesAdded)
  local DialoguesDB = GameData.AllDialogues.Dialogues
  for i, v in ipairs(dialoguesAdded) do
    table.insert(self.m_dialogList, DialoguesDB[v])
    table.insert(self.m_dialogIndex, v)
  end
end
function GameUICommonDialog:JudeIsLoadFlash()
  if not GameUICommonDialog:GetFlashObject() then
    self:LoadFlashObject()
  end
end
function GameUICommonDialog:IsSexSpecialText(textID)
  local textMap = {LC_STORY_TUTORIAL_49 = true, LC_STORY_NEW_CHAPTER1_1_20 = true}
  return textMap[textID]
end
function GameUICommonDialog:RefreshDialogueData()
  DebugOut("refresh dialogData")
  if #self.m_dialogList ~= 0 then
    local dialogue = self.m_dialogList[1]
    table.remove(self.m_dialogList, 1)
    local text = GameLoader:GetGameText(dialogue.TextID)
    if GameUICommonDialog:IsSexSpecialText(dialogue.TextID) and GameGlobalData:GetUserInfo() and GameGlobalData:GetUserInfo().sex == 0 and dialogue.NPCHead == -1 then
      text = GameLoader:GetGameText(dialogue.TextID .. "_0")
    end
    DebugOut("----> text =", text)
    if GameGlobalData:GetUserInfo() then
      text = string.gsub(text, "<player>", GameUtils:GetUserDisplayName(GameGlobalData:GetUserInfo().name))
      text = string.gsub(text, "<v_player>", GameUtils:GetUserDisplayName(GameUtils:GetAdjutantName()))
    end
    if text == "" then
      DebugOut("----> text error id= ", dialogue.TextID)
      text = "miss text id:" .. dialogue.TextID
    end
    self.dialogDetails = {
      headID = dialogue.NPCHead,
      headName = dialogue.HeadName,
      headPos = dialogue.NPCPos,
      headBodyText = text
    }
    self:SetDialogDetails(self.dialogDetails)
    self:BeginShowText()
  else
    DebugOut("endShow")
    self.mIsEnding = true
    self:GetFlashObject():InvokeASCallback("_root", "endShow")
  end
end
function GameUICommonDialog:SetDialogDetails(details)
  local headAvatarID = details.headID
  local isLeft = 0 == details.headPos
  local headAvatar = "head" .. headAvatarID
  local headName = details.headName
  if -1 == headAvatarID then
    if GameObjectTutorialCutscene:IsActive() then
      headAvatar = "male"
      headName = "???"
    else
      local player_main_fleet = GameGlobalData:GetFleetInfo(1)
      local leaderlist = GameGlobalData:GetData("leaderlist")
      local curMatrixIndex = GameGlobalData.GlobalData.curMatrixIndex
      local player_avatar = ""
      if leaderlist and curMatrixIndex then
        player_avatar = GameDataAccessHelper:GetFleetAvatar(leaderlist.leader_ids[curMatrixIndex], player_main_fleet.level)
      else
        player_avatar = GameUtils:GetPlayerAvatar(nil, player_main_fleet.level)
      end
      DebugOut("here error", headAvatar, player_avatar)
      headAvatar = player_avatar
      headName = GameUtils:GetUserDisplayName(GameGlobalData:GetUserInfo().name)
    end
  elseif 0 == headAvatarID then
    headAvatar = GameUtils:GetAdjutantAvatar()
    headName = GameUtils:GetAdjutantName()
  else
    headName = GameLoader:GetGameText("LC_NPC_" .. details.headName)
  end
  if GameDataAccessHelper:IsHeadResNeedDownload(headAvatar) and not GameDataAccessHelper:CheckFleetHasAvataImage(headAvatar) then
    headAvatar = "head9001"
  end
  DebugOut("SetDialogDetails " .. headName .. " " .. headAvatar)
  self:GetFlashObject():InvokeASCallback("_root", "SetAvatarDetails", isLeft, headAvatar, headName)
  self:GetFlashObject():InvokeASCallback("_root", "PlayAvatarShowHide", isLeft, true)
  if isLeft then
    self.showedLeftAvatar = true
  else
    self.showedRightAvatar = true
  end
end
function GameUICommonDialog:BeginShowText()
  self:ResetText()
  self:SetMaxLine()
  self:RefreshText()
end
function GameUICommonDialog:ResetText()
  self:GetFlashObject():resetStartLine("_root.main.BODY.dialog_box.SC_TEXT")
end
GameUICommonDialog.bodyMaxLine = 3
function GameUICommonDialog:SetMaxLine()
  self:GetFlashObject():setMaxLine("_root.main.BODY.dialog_box.SC_TEXT", self.bodyMaxLine)
end
function GameUICommonDialog:IsCurParagraphOver()
  return ...
end
function GameUICommonDialog:PageDown()
  self:GetFlashObject():addStartLine("_root.main.BODY.dialog_box.SC_TEXT")
end
function GameUICommonDialog:RefreshText()
  local text = self.dialogDetails.headBodyText
  local haveNext = #self.m_dialogList ~= 0
  self:SetBodyText(text, haveNext)
end
function GameUICommonDialog:OnAddToGameState(gameState)
  self.mIsEnding = false
  self:RefreshDialogueData()
  self:GetFlashObject():InvokeASCallback("_root", "beginShow")
  self:GetFlashObject():unlockInput()
end
function GameUICommonDialog:OnEraseFromGameState()
  self.mIsEnding = false
end
function GameUICommonDialog:DialogFlurry(index)
  local eventName
  DebugFlurry("GameUICommonDialog:DialogFlurry: ", index)
  if immanentversion == 1 then
    eventName = k_FlurryDialog[index]
  elseif immanentversion == 2 then
    eventName = k_FlurryDialog2[index]
  else
    assert(false)
  end
  if eventName then
    AddFlurryEvent(eventName, {}, nil)
  end
end
function GameUICommonDialog:OnFSCommand(cmd, args)
  DebugOut("GameUICommonDialog:OnFSCommand: " .. cmd)
  if cmd == "btnNextClicked" then
    if self.mIsEnding == true then
      GameUICommonDialog:OnFSCommand("moveoutOver", "")
      return
    end
    DebugOut("14544788")
    if not self:IsCurParagraphOver() then
      self:PageDown()
      DebugOut("GameUICommonDialog:OnFSCommand: PageDown")
    else
      local isLeft = 0 == self.dialogDetails.headPos
      if isLeft and self.showedLeftAvatar then
        self:GetFlashObject():InvokeASCallback("_root", "PlayAvatarShowHide", isLeft, false)
        self.showedLeftAvatar = false
      elseif not isLeft and self.showedRightAvatar then
        self:GetFlashObject():InvokeASCallback("_root", "PlayAvatarShowHide", isLeft, false)
        self.showedRightAvatar = false
      else
        GameUICommonDialog:OnFSCommand("hideAvatarOver", "")
      end
      local dialogIndex = self.m_dialogIndex[1]
      self:DialogFlurry(dialogIndex)
      DebugOut("GameUICommonDialog:OnFSCommand: ResetText")
    end
  elseif cmd == "hideAvatarOver" then
    local isLeft = 0 == self.dialogDetails.headPos
    self:GetFlashObject():InvokeASCallback("_root", "SetAvatarDetails", isLeft, "head1", "")
    GameUICommonDialog:RefreshDialogueData()
    table.remove(self.m_dialogIndex, 1)
  elseif cmd == "moveoutOver" then
    self.mIsEnding = false
    self:ResetText()
    GameStateManager.GameStateGlobalState:EraseObject(GameUICommonDialog)
    GameStateManager.GameStateGlobalState:ForceCompleteCammandList()
    if self.callback then
      DebugOut("GameUICommonDialog:PlayStory moveoutOver callback")
      self.callback()
    end
    GameUICommonDialog:DoPlayStory()
  end
end
function GameUICommonDialog:ClearDetails()
  self:GetFlashObject():InvokeASCallback("_root", "ClearDetails")
end
function GameUICommonDialog:SetBodyText(bodyText, haveNext)
  self:GetFlashObject():InvokeASCallback("_root", "setBodyText", bodyText, haveNext)
end
function GameUICommonDialog:IsNeedPlayStory(storyTable)
  if DebugConfig.isSuperMode then
    DebugOut("need not play!!!111111")
    return false
  end
  if not storyTable or 0 == #storyTable or self.m_playedStory[storyTable[1]] then
    DebugOut("need not play!!!")
    return false
  end
  DebugOut("need play!!!")
  return true
end
function GameUICommonDialog:ForcePlayStory(storyTable, callback)
  if not GameUICommonDialog:IsNeedPlayStory(storyTable) then
    if callback then
      callback()
    end
    return
  end
  local storyItem = {}
  storyItem.storyTable = storyTable
  storyItem.callback = callback
  table.insert(GameUICommonDialog.PendingStory, storyItem)
  self.m_playedStory[storyTable[1]] = true
  GameUICommonDialog:DoPlayStory()
end
function GameUICommonDialog:DoPlayStory()
  if GameStateGlobalState:CheckStackObject(self) or #GameUICommonDialog.PendingStory == 0 then
    return
  end
  local storyItem = table.remove(GameUICommonDialog.PendingStory)
  self.m_dialogList = {}
  self.m_dialogIndex = {}
  self.showedLeftAvatar = false
  self.showedRightAvatar = false
  self.callback = storyItem.callback
  self:AddDialogue(storyItem.storyTable)
  if #self.m_dialogList == 0 then
    return
  end
  self:ClearDetails()
  GameStateManager.GameStateGlobalState:AddObjectToStack(self)
end
function GameUICommonDialog:isActive()
  local isActive = GameStateGlobalState:CheckStackObject(self)
  DebugOut("GameUICommonDialog:isActive", isActive)
  return isActive
end
function GameUICommonDialog:PlayStory(storyTable, callback)
  DebugOut("GameUICommonDialog:PlayStory ")
  GameUICommonDialog:JudeIsLoadFlash()
  if storyTable and #storyTable > 0 then
    DebugOut("GameUICommonDialog:PlayStory ", storyTable[1], self.m_playedStory[storyTable[1]])
  else
    if callback then
      callback()
    end
    return
  end
  DebugOutPutTable(storyTable, "storyTable")
  self:ForcePlayStory(storyTable, callback)
end
if AutoUpdate.isAndroidDevice then
  function GameUICommonDialog.OnAndroidBack()
    GameUICommonDialog:OnFSCommand("btnNextClicked")
  end
end
