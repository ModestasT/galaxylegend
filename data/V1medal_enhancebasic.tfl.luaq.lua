local basic = GameData.medal_enhance.basic
table.insert(basic, {
  type = 100001,
  type_next = 101001,
  pos = 4,
  quality = 1,
  rank = 0,
  level = 1,
  exp = 5,
  attr_basic = "[{1,512}]",
  attr_add = "[{1,26271}]",
  attr_rank = "[]",
  price = 0,
  is_material = 1,
  glory = 0
})
table.insert(basic, {
  type = 100002,
  type_next = 101002,
  pos = 1,
  quality = 1,
  rank = 0,
  level = 1,
  exp = 5,
  attr_basic = "[{3,67},{7,31}]",
  attr_add = "[{3,3427},{7,1581}]",
  attr_rank = "[]",
  price = 0,
  is_material = 1,
  glory = 0
})
table.insert(basic, {
  type = 100003,
  type_next = 101003,
  pos = 2,
  quality = 1,
  rank = 0,
  level = 1,
  exp = 5,
  attr_basic = "[{4,14}]",
  attr_add = "[{4,744}]",
  attr_rank = "[]",
  price = 0,
  is_material = 1,
  glory = 0
})
table.insert(basic, {
  type = 100004,
  type_next = 101004,
  pos = 3,
  quality = 1,
  rank = 0,
  level = 1,
  exp = 5,
  attr_basic = "[{5,100}]",
  attr_add = "[{5,5112}]",
  attr_rank = "[]",
  price = 0,
  is_material = 1,
  glory = 0
})
table.insert(basic, {
  type = 100005,
  type_next = 101005,
  pos = 5,
  quality = 1,
  rank = 0,
  level = 1,
  exp = 5,
  attr_basic = "[{6,12}]",
  attr_add = "[{6,599}]",
  attr_rank = "[]",
  price = 0,
  is_material = 1,
  glory = 0
})
table.insert(basic, {
  type = 101001,
  type_next = 102001,
  pos = 4,
  quality = 1,
  rank = 1,
  level = 1,
  exp = 5,
  attr_basic = "[{1,512}]",
  attr_add = "[{1,26271}]",
  attr_rank = "[{1,24837}]",
  price = 0,
  is_material = 1,
  glory = 0
})
table.insert(basic, {
  type = 101002,
  type_next = 102002,
  pos = 1,
  quality = 1,
  rank = 1,
  level = 1,
  exp = 5,
  attr_basic = "[{3,67},{7,31}]",
  attr_add = "[{3,3427},{7,1581}]",
  attr_rank = "[{3,2620},{7,1209}]",
  price = 0,
  is_material = 1,
  glory = 0
})
table.insert(basic, {
  type = 101003,
  type_next = 102003,
  pos = 2,
  quality = 1,
  rank = 1,
  level = 1,
  exp = 5,
  attr_basic = "[{4,14}]",
  attr_add = "[{4,744}]",
  attr_rank = "[{4,2620}]",
  price = 0,
  is_material = 1,
  glory = 0
})
table.insert(basic, {
  type = 101004,
  type_next = 102004,
  pos = 3,
  quality = 1,
  rank = 1,
  level = 1,
  exp = 5,
  attr_basic = "[{5,100}]",
  attr_add = "[{5,5112}]",
  attr_rank = "[{5,4660}]",
  price = 0,
  is_material = 1,
  glory = 0
})
table.insert(basic, {
  type = 101005,
  type_next = 102005,
  pos = 5,
  quality = 1,
  rank = 1,
  level = 1,
  exp = 5,
  attr_basic = "[{6,12}]",
  attr_add = "[{6,599}]",
  attr_rank = "[{6,4660}]",
  price = 0,
  is_material = 1,
  glory = 0
})
table.insert(basic, {
  type = 102001,
  type_next = 103001,
  pos = 4,
  quality = 1,
  rank = 2,
  level = 1,
  exp = 5,
  attr_basic = "[{1,512}]",
  attr_add = "[{1,26271}]",
  attr_rank = "[{1,70671}]",
  price = 0,
  is_material = 1,
  glory = 0
})
table.insert(basic, {
  type = 102002,
  type_next = 103002,
  pos = 1,
  quality = 1,
  rank = 2,
  level = 1,
  exp = 5,
  attr_basic = "[{3,67},{7,31}]",
  attr_add = "[{3,3427},{7,1581}]",
  attr_rank = "[{3,7455},{7,3439}]",
  price = 0,
  is_material = 1,
  glory = 0
})
table.insert(basic, {
  type = 102003,
  type_next = 103003,
  pos = 2,
  quality = 1,
  rank = 2,
  level = 1,
  exp = 5,
  attr_basic = "[{4,14}]",
  attr_add = "[{4,744}]",
  attr_rank = "[{4,7455}]",
  price = 0,
  is_material = 1,
  glory = 0
})
table.insert(basic, {
  type = 102004,
  type_next = 103004,
  pos = 3,
  quality = 1,
  rank = 2,
  level = 1,
  exp = 5,
  attr_basic = "[{5,100}]",
  attr_add = "[{5,5112}]",
  attr_rank = "[{5,13259}]",
  price = 0,
  is_material = 1,
  glory = 0
})
table.insert(basic, {
  type = 102005,
  type_next = 103005,
  pos = 5,
  quality = 1,
  rank = 2,
  level = 1,
  exp = 5,
  attr_basic = "[{6,12}]",
  attr_add = "[{6,599}]",
  attr_rank = "[{6,13259}]",
  price = 0,
  is_material = 1,
  glory = 0
})
table.insert(basic, {
  type = 103001,
  type_next = 104001,
  pos = 4,
  quality = 1,
  rank = 3,
  level = 1,
  exp = 5,
  attr_basic = "[{1,512}]",
  attr_add = "[{1,26271}]",
  attr_rank = "[{1,136804}]",
  price = 0,
  is_material = 1,
  glory = 0
})
table.insert(basic, {
  type = 103002,
  type_next = 104002,
  pos = 1,
  quality = 1,
  rank = 3,
  level = 1,
  exp = 5,
  attr_basic = "[{3,67},{7,31}]",
  attr_add = "[{3,3427},{7,1581}]",
  attr_rank = "[{3,14431},{7,6657}]",
  price = 0,
  is_material = 1,
  glory = 0
})
table.insert(basic, {
  type = 103003,
  type_next = 104003,
  pos = 2,
  quality = 1,
  rank = 3,
  level = 1,
  exp = 5,
  attr_basic = "[{4,14}]",
  attr_add = "[{4,744}]",
  attr_rank = "[{4,14431}]",
  price = 0,
  is_material = 1,
  glory = 0
})
table.insert(basic, {
  type = 103004,
  type_next = 104004,
  pos = 3,
  quality = 1,
  rank = 3,
  level = 1,
  exp = 5,
  attr_basic = "[{5,100}]",
  attr_add = "[{5,5112}]",
  attr_rank = "[{5,25667}]",
  price = 0,
  is_material = 1,
  glory = 0
})
table.insert(basic, {
  type = 103005,
  type_next = 104005,
  pos = 5,
  quality = 1,
  rank = 3,
  level = 1,
  exp = 5,
  attr_basic = "[{6,12}]",
  attr_add = "[{6,599}]",
  attr_rank = "[{6,25667}]",
  price = 0,
  is_material = 1,
  glory = 0
})
table.insert(basic, {
  type = 104001,
  type_next = 105001,
  pos = 4,
  quality = 1,
  rank = 4,
  level = 1,
  exp = 5,
  attr_basic = "[{1,512}]",
  attr_add = "[{1,26271}]",
  attr_rank = "[{1,245583}]",
  price = 0,
  is_material = 1,
  glory = 0
})
table.insert(basic, {
  type = 104002,
  type_next = 105002,
  pos = 1,
  quality = 1,
  rank = 4,
  level = 1,
  exp = 5,
  attr_basic = "[{3,67},{7,31}]",
  attr_add = "[{3,3427},{7,1581}]",
  attr_rank = "[{3,25905},{7,11950}]",
  price = 0,
  is_material = 1,
  glory = 0
})
table.insert(basic, {
  type = 104003,
  type_next = 105003,
  pos = 2,
  quality = 1,
  rank = 4,
  level = 1,
  exp = 5,
  attr_basic = "[{4,14}]",
  attr_add = "[{4,744}]",
  attr_rank = "[{4,25905}]",
  price = 0,
  is_material = 1,
  glory = 0
})
table.insert(basic, {
  type = 104004,
  type_next = 105004,
  pos = 3,
  quality = 1,
  rank = 4,
  level = 1,
  exp = 5,
  attr_basic = "[{5,100}]",
  attr_add = "[{5,5112}]",
  attr_rank = "[{5,46076}]",
  price = 0,
  is_material = 1,
  glory = 0
})
table.insert(basic, {
  type = 104005,
  type_next = 105005,
  pos = 5,
  quality = 1,
  rank = 4,
  level = 1,
  exp = 5,
  attr_basic = "[{6,12}]",
  attr_add = "[{6,599}]",
  attr_rank = "[{6,46076}]",
  price = 0,
  is_material = 1,
  glory = 0
})
table.insert(basic, {
  type = 105001,
  type_next = 106001,
  pos = 4,
  quality = 1,
  rank = 5,
  level = 1,
  exp = 5,
  attr_basic = "[{1,512}]",
  attr_add = "[{1,26271}]",
  attr_rank = "[{1,366762}]",
  price = 0,
  is_material = 1,
  glory = 0
})
table.insert(basic, {
  type = 105002,
  type_next = 106002,
  pos = 1,
  quality = 1,
  rank = 5,
  level = 1,
  exp = 5,
  attr_basic = "[{3,67},{7,31}]",
  attr_add = "[{3,3427},{7,1581}]",
  attr_rank = "[{3,38688},{7,17847}]",
  price = 0,
  is_material = 1,
  glory = 0
})
table.insert(basic, {
  type = 105003,
  type_next = 106003,
  pos = 2,
  quality = 1,
  rank = 5,
  level = 1,
  exp = 5,
  attr_basic = "[{4,14}]",
  attr_add = "[{4,744}]",
  attr_rank = "[{4,38688}]",
  price = 0,
  is_material = 1,
  glory = 0
})
table.insert(basic, {
  type = 105004,
  type_next = 106004,
  pos = 3,
  quality = 1,
  rank = 5,
  level = 1,
  exp = 5,
  attr_basic = "[{5,100}]",
  attr_add = "[{5,5112}]",
  attr_rank = "[{5,68811}]",
  price = 0,
  is_material = 1,
  glory = 0
})
table.insert(basic, {
  type = 105005,
  type_next = 106005,
  pos = 5,
  quality = 1,
  rank = 5,
  level = 1,
  exp = 5,
  attr_basic = "[{6,12}]",
  attr_add = "[{6,599}]",
  attr_rank = "[{6,68811}]",
  price = 0,
  is_material = 1,
  glory = 0
})
table.insert(basic, {
  type = 106001,
  type_next = 0,
  pos = 4,
  quality = 1,
  rank = 6,
  level = 1,
  exp = 5,
  attr_basic = "[{1,512}]",
  attr_add = "[{1,26271}]",
  attr_rank = "[{1,549018}]",
  price = 0,
  is_material = 1,
  glory = 0
})
table.insert(basic, {
  type = 106002,
  type_next = 0,
  pos = 1,
  quality = 1,
  rank = 6,
  level = 1,
  exp = 5,
  attr_basic = "[{3,67},{7,31}]",
  attr_add = "[{3,3427},{7,1581}]",
  attr_rank = "[{3,57913},{7,26716}]",
  price = 0,
  is_material = 1,
  glory = 0
})
table.insert(basic, {
  type = 106003,
  type_next = 0,
  pos = 2,
  quality = 1,
  rank = 6,
  level = 1,
  exp = 5,
  attr_basic = "[{4,14}]",
  attr_add = "[{4,744}]",
  attr_rank = "[{4,57913}]",
  price = 0,
  is_material = 1,
  glory = 0
})
table.insert(basic, {
  type = 106004,
  type_next = 0,
  pos = 3,
  quality = 1,
  rank = 6,
  level = 1,
  exp = 5,
  attr_basic = "[{5,100}]",
  attr_add = "[{5,5112}]",
  attr_rank = "[{5,103005}]",
  price = 0,
  is_material = 1,
  glory = 0
})
table.insert(basic, {
  type = 106005,
  type_next = 0,
  pos = 5,
  quality = 1,
  rank = 6,
  level = 1,
  exp = 5,
  attr_basic = "[{6,12}]",
  attr_add = "[{6,599}]",
  attr_rank = "[{6,103005}]",
  price = 0,
  is_material = 1,
  glory = 0
})
table.insert(basic, {
  type = 200001,
  type_next = 201001,
  pos = 4,
  quality = 2,
  rank = 0,
  level = 1,
  exp = 30,
  attr_basic = "[{1,3082}]",
  attr_add = "[{1,59579},{21,0.38}]",
  attr_rank = "[]",
  price = 0,
  is_material = 1,
  glory = 1
})
table.insert(basic, {
  type = 200002,
  type_next = 201002,
  pos = 1,
  quality = 2,
  rank = 0,
  level = 1,
  exp = 30,
  attr_basic = "[{3,402},{7,185}]",
  attr_add = "[{3,7772},{37,2.48},{7,3585}]",
  attr_rank = "[]",
  price = 0,
  is_material = 1,
  glory = 1
})
table.insert(basic, {
  type = 200003,
  type_next = 201003,
  pos = 2,
  quality = 2,
  rank = 0,
  level = 1,
  exp = 30,
  attr_basic = "[{4,87}]",
  attr_add = "[{4,1687},{19,0.69}]",
  attr_rank = "[]",
  price = 0,
  is_material = 1,
  glory = 1
})
table.insert(basic, {
  type = 200004,
  type_next = 201004,
  pos = 3,
  quality = 2,
  rank = 0,
  level = 1,
  exp = 30,
  attr_basic = "[{5,600}]",
  attr_add = "[{5,11592},{22,0.38}]",
  attr_rank = "[]",
  price = 0,
  is_material = 1,
  glory = 1
})
table.insert(basic, {
  type = 200005,
  type_next = 201005,
  pos = 5,
  quality = 2,
  rank = 0,
  level = 1,
  exp = 30,
  attr_basic = "[{6,70}]",
  attr_add = "[{6,1358},{20,0.69}]",
  attr_rank = "[]",
  price = 0,
  is_material = 1,
  glory = 1
})
table.insert(basic, {
  type = 201001,
  type_next = 202001,
  pos = 4,
  quality = 2,
  rank = 1,
  level = 1,
  exp = 30,
  attr_basic = "[{1,3082}]",
  attr_add = "[{1,59579},{21,0.38}]",
  attr_rank = "[{1,47391},{21,1}]",
  price = 0,
  is_material = 1,
  glory = 1
})
table.insert(basic, {
  type = 201002,
  type_next = 202002,
  pos = 1,
  quality = 2,
  rank = 1,
  level = 1,
  exp = 30,
  attr_basic = "[{3,402},{7,185}]",
  attr_add = "[{3,7772},{37,2.48},{7,3585}]",
  attr_rank = "[{3,4541},{37,4},{7,2095}]",
  price = 0,
  is_material = 1,
  glory = 1
})
table.insert(basic, {
  type = 201003,
  type_next = 202003,
  pos = 2,
  quality = 2,
  rank = 1,
  level = 1,
  exp = 30,
  attr_basic = "[{4,87}]",
  attr_add = "[{4,1687},{19,0.69}]",
  attr_rank = "[{4,3093},{19,1}]",
  price = 0,
  is_material = 1,
  glory = 1
})
table.insert(basic, {
  type = 201004,
  type_next = 202004,
  pos = 3,
  quality = 2,
  rank = 1,
  level = 1,
  exp = 30,
  attr_basic = "[{5,600}]",
  attr_add = "[{5,11592},{22,0.38}]",
  attr_rank = "[{5,8891},{22,1}]",
  price = 0,
  is_material = 1,
  glory = 1
})
table.insert(basic, {
  type = 201005,
  type_next = 202005,
  pos = 5,
  quality = 2,
  rank = 1,
  level = 1,
  exp = 30,
  attr_basic = "[{6,70}]",
  attr_add = "[{6,1358},{20,0.69}]",
  attr_rank = "[{6,5502},{20,1}]",
  price = 0,
  is_material = 1,
  glory = 1
})
table.insert(basic, {
  type = 202001,
  type_next = 203001,
  pos = 4,
  quality = 2,
  rank = 2,
  level = 1,
  exp = 30,
  attr_basic = "[{1,3082}]",
  attr_add = "[{1,59579},{21,0.38}]",
  attr_rank = "[{1,120081},{21,3}]",
  price = 0,
  is_material = 1,
  glory = 1
})
table.insert(basic, {
  type = 202002,
  type_next = 203002,
  pos = 1,
  quality = 2,
  rank = 2,
  level = 1,
  exp = 30,
  attr_basic = "[{3,402},{7,185}]",
  attr_add = "[{3,7772},{37,2.48},{7,3585}]",
  attr_rank = "[{3,14516},{37,9},{7,6696}]",
  price = 0,
  is_material = 1,
  glory = 1
})
table.insert(basic, {
  type = 202003,
  type_next = 203003,
  pos = 2,
  quality = 2,
  rank = 2,
  level = 1,
  exp = 30,
  attr_basic = "[{4,87}]",
  attr_add = "[{4,1687},{19,0.69}]",
  attr_rank = "[{4,6949},{19,3}]",
  price = 0,
  is_material = 1,
  glory = 1
})
table.insert(basic, {
  type = 202004,
  type_next = 203004,
  pos = 3,
  quality = 2,
  rank = 2,
  level = 1,
  exp = 30,
  attr_basic = "[{5,600}]",
  attr_add = "[{5,11592},{22,0.38}]",
  attr_rank = "[{5,22529},{22,3}]",
  price = 0,
  is_material = 1,
  glory = 1
})
table.insert(basic, {
  type = 202005,
  type_next = 203005,
  pos = 5,
  quality = 2,
  rank = 2,
  level = 1,
  exp = 30,
  attr_basic = "[{6,70}]",
  attr_add = "[{6,1358},{20,0.69}]",
  attr_rank = "[{6,12360},{20,3}]",
  price = 0,
  is_material = 1,
  glory = 1
})
table.insert(basic, {
  type = 203001,
  type_next = 204001,
  pos = 4,
  quality = 2,
  rank = 3,
  level = 1,
  exp = 30,
  attr_basic = "[{1,3082}]",
  attr_add = "[{1,59579},{21,0.38}]",
  attr_rank = "[{1,209345},{21,7}]",
  price = 0,
  is_material = 1,
  glory = 1
})
table.insert(basic, {
  type = 203002,
  type_next = 204002,
  pos = 1,
  quality = 2,
  rank = 3,
  level = 1,
  exp = 30,
  attr_basic = "[{3,402},{7,185}]",
  attr_add = "[{3,7772},{37,2.48},{7,3585}]",
  attr_rank = "[{3,29622},{37,18},{7,13665}]",
  price = 0,
  is_material = 1,
  glory = 1
})
table.insert(basic, {
  type = 203003,
  type_next = 204003,
  pos = 2,
  quality = 2,
  rank = 3,
  level = 1,
  exp = 30,
  attr_basic = "[{4,87}]",
  attr_add = "[{4,1687},{19,0.69}]",
  attr_rank = "[{4,14487},{19,6}]",
  price = 0,
  is_material = 1,
  glory = 1
})
table.insert(basic, {
  type = 203004,
  type_next = 204004,
  pos = 3,
  quality = 2,
  rank = 3,
  level = 1,
  exp = 30,
  attr_basic = "[{5,600}]",
  attr_add = "[{5,11592},{22,0.38}]",
  attr_rank = "[{5,39277},{22,7}]",
  price = 0,
  is_material = 1,
  glory = 1
})
table.insert(basic, {
  type = 203005,
  type_next = 204005,
  pos = 5,
  quality = 2,
  rank = 3,
  level = 1,
  exp = 30,
  attr_basic = "[{6,70}]",
  attr_add = "[{6,1358},{20,0.69}]",
  attr_rank = "[{6,25768},{20,6}]",
  price = 0,
  is_material = 1,
  glory = 1
})
table.insert(basic, {
  type = 204001,
  type_next = 205001,
  pos = 4,
  quality = 2,
  rank = 4,
  level = 1,
  exp = 30,
  attr_basic = "[{1,3082}]",
  attr_add = "[{1,59579},{21,0.38}]",
  attr_rank = "[{1,343531},{21,13}]",
  price = 0,
  is_material = 1,
  glory = 1
})
table.insert(basic, {
  type = 204002,
  type_next = 205002,
  pos = 1,
  quality = 2,
  rank = 4,
  level = 1,
  exp = 30,
  attr_basic = "[{3,402},{7,185}]",
  attr_add = "[{3,7772},{37,2.48},{7,3585}]",
  attr_rank = "[{3,51773},{37,32},{7,23884}]",
  price = 0,
  is_material = 1,
  glory = 1
})
table.insert(basic, {
  type = 204003,
  type_next = 205003,
  pos = 2,
  quality = 2,
  rank = 4,
  level = 1,
  exp = 30,
  attr_basic = "[{4,87}]",
  attr_add = "[{4,1687},{19,0.69}]",
  attr_rank = "[{4,22953},{19,11}]",
  price = 0,
  is_material = 1,
  glory = 1
})
table.insert(basic, {
  type = 204004,
  type_next = 205004,
  pos = 3,
  quality = 2,
  rank = 4,
  level = 1,
  exp = 30,
  attr_basic = "[{5,600}]",
  attr_add = "[{5,11592},{22,0.38}]",
  attr_rank = "[{5,64452},{22,13}]",
  price = 0,
  is_material = 1,
  glory = 1
})
table.insert(basic, {
  type = 204005,
  type_next = 205005,
  pos = 5,
  quality = 2,
  rank = 4,
  level = 1,
  exp = 30,
  attr_basic = "[{6,70}]",
  attr_add = "[{6,1358},{20,0.69}]",
  attr_rank = "[{6,40824},{20,11}]",
  price = 0,
  is_material = 1,
  glory = 1
})
table.insert(basic, {
  type = 205001,
  type_next = 206001,
  pos = 4,
  quality = 2,
  rank = 5,
  level = 1,
  exp = 30,
  attr_basic = "[{1,3082}]",
  attr_add = "[{1,59579},{21,0.38}]",
  attr_rank = "[{1,540410},{21,20}]",
  price = 0,
  is_material = 1,
  glory = 1
})
table.insert(basic, {
  type = 205002,
  type_next = 206002,
  pos = 1,
  quality = 2,
  rank = 5,
  level = 1,
  exp = 30,
  attr_basic = "[{3,402},{7,185}]",
  attr_add = "[{3,7772},{37,2.48},{7,3585}]",
  attr_rank = "[{3,82228},{37,48},{7,37933}]",
  price = 0,
  is_material = 1,
  glory = 1
})
table.insert(basic, {
  type = 205003,
  type_next = 206003,
  pos = 2,
  quality = 2,
  rank = 5,
  level = 1,
  exp = 30,
  attr_basic = "[{4,87}]",
  attr_add = "[{4,1687},{19,0.69}]",
  attr_rank = "[{4,36125},{19,17}]",
  price = 0,
  is_material = 1,
  glory = 1
})
table.insert(basic, {
  type = 205004,
  type_next = 206004,
  pos = 3,
  quality = 2,
  rank = 5,
  level = 1,
  exp = 30,
  attr_basic = "[{5,600}]",
  attr_add = "[{5,11592},{22,0.38}]",
  attr_rank = "[{5,101390},{22,20}]",
  price = 0,
  is_material = 1,
  glory = 1
})
table.insert(basic, {
  type = 205005,
  type_next = 206005,
  pos = 5,
  quality = 2,
  rank = 5,
  level = 1,
  exp = 30,
  attr_basic = "[{6,70}]",
  attr_add = "[{6,1358},{20,0.69}]",
  attr_rank = "[{6,64253},{20,17}]",
  price = 0,
  is_material = 1,
  glory = 1
})
table.insert(basic, {
  type = 206001,
  type_next = 0,
  pos = 4,
  quality = 2,
  rank = 6,
  level = 1,
  exp = 30,
  attr_basic = "[{1,3082}]",
  attr_add = "[{1,59579},{21,0.38}]",
  attr_rank = "[{1,852007},{21,30}]",
  price = 0,
  is_material = 1,
  glory = 1
})
table.insert(basic, {
  type = 206002,
  type_next = 0,
  pos = 1,
  quality = 2,
  rank = 6,
  level = 1,
  exp = 30,
  attr_basic = "[{3,402},{7,185}]",
  attr_add = "[{3,7772},{37,2.48},{7,3585}]",
  attr_rank = "[{3,124485},{37,75},{7,57427}]",
  price = 0,
  is_material = 1,
  glory = 1
})
table.insert(basic, {
  type = 206003,
  type_next = 0,
  pos = 2,
  quality = 2,
  rank = 6,
  level = 1,
  exp = 30,
  attr_basic = "[{4,87}]",
  attr_add = "[{4,1687},{19,0.69}]",
  attr_rank = "[{4,61427},{19,25}]",
  price = 0,
  is_material = 1,
  glory = 1
})
table.insert(basic, {
  type = 206004,
  type_next = 0,
  pos = 3,
  quality = 2,
  rank = 6,
  level = 1,
  exp = 30,
  attr_basic = "[{5,600}]",
  attr_add = "[{5,11592},{22,0.38}]",
  attr_rank = "[{5,159851},{22,30}]",
  price = 0,
  is_material = 1,
  glory = 1
})
table.insert(basic, {
  type = 206005,
  type_next = 0,
  pos = 5,
  quality = 2,
  rank = 6,
  level = 1,
  exp = 30,
  attr_basic = "[{6,70}]",
  attr_add = "[{6,1358},{20,0.69}]",
  attr_rank = "[{6,109254},{20,25}]",
  price = 0,
  is_material = 1,
  glory = 1
})
table.insert(basic, {
  type = 300001,
  type_next = 301001,
  pos = 4,
  quality = 3,
  rank = 0,
  level = 1,
  exp = 0,
  attr_basic = "[{1,25624}]",
  attr_add = "[{1,98426},{21,0.66}]",
  attr_rank = "[]",
  price = 50,
  is_material = 0,
  glory = 8
})
table.insert(basic, {
  type = 300002,
  type_next = 301002,
  pos = 1,
  quality = 3,
  rank = 0,
  level = 1,
  exp = 0,
  attr_basic = "[{3,3343},{7,1542}]",
  attr_add = "[{3,12839},{37,4.14},{7,5923}]",
  attr_rank = "[]",
  price = 50,
  is_material = 0,
  glory = 8
})
table.insert(basic, {
  type = 300003,
  type_next = 301003,
  pos = 2,
  quality = 3,
  rank = 0,
  level = 1,
  exp = 0,
  attr_basic = "[{4,725}]",
  attr_add = "[{4,2786},{19,1.14}]",
  attr_rank = "[]",
  price = 50,
  is_material = 0,
  glory = 8
})
table.insert(basic, {
  type = 300004,
  type_next = 301004,
  pos = 3,
  quality = 3,
  rank = 0,
  level = 1,
  exp = 0,
  attr_basic = "[{5,4986}]",
  attr_add = "[{5,19151},{22,0.66}]",
  attr_rank = "[]",
  price = 50,
  is_material = 0,
  glory = 8
})
table.insert(basic, {
  type = 300005,
  type_next = 301005,
  pos = 5,
  quality = 3,
  rank = 0,
  level = 1,
  exp = 0,
  attr_basic = "[{6,584}]",
  attr_add = "[{6,2243},{20,1.14}]",
  attr_rank = "[]",
  price = 50,
  is_material = 0,
  glory = 8
})
table.insert(basic, {
  type = 301001,
  type_next = 302001,
  pos = 4,
  quality = 3,
  rank = 1,
  level = 1,
  exp = 0,
  attr_basic = "[{1,25624}]",
  attr_add = "[{1,98426},{21,0.66}]",
  attr_rank = "[{1,77602},{21,1}]",
  price = 50,
  is_material = 0,
  glory = 8
})
table.insert(basic, {
  type = 301002,
  type_next = 302002,
  pos = 1,
  quality = 3,
  rank = 1,
  level = 1,
  exp = 0,
  attr_basic = "[{3,3343},{7,1542}]",
  attr_add = "[{3,12839},{37,4.14},{7,5923}]",
  attr_rank = "[{3,5579},{37,6},{7,2574}]",
  price = 50,
  is_material = 0,
  glory = 8
})
table.insert(basic, {
  type = 301003,
  type_next = 302003,
  pos = 2,
  quality = 3,
  rank = 1,
  level = 1,
  exp = 0,
  attr_basic = "[{4,725}]",
  attr_add = "[{4,2786},{19,1.14}]",
  attr_rank = "[{4,6280},{19,1}]",
  price = 50,
  is_material = 0,
  glory = 8
})
table.insert(basic, {
  type = 301004,
  type_next = 302004,
  pos = 3,
  quality = 3,
  rank = 1,
  level = 1,
  exp = 0,
  attr_basic = "[{5,4986}]",
  attr_add = "[{5,19151},{22,0.66}]",
  attr_rank = "[{5,14559},{22,1}]",
  price = 50,
  is_material = 0,
  glory = 8
})
table.insert(basic, {
  type = 301005,
  type_next = 302005,
  pos = 5,
  quality = 3,
  rank = 1,
  level = 1,
  exp = 0,
  attr_basic = "[{6,584}]",
  attr_add = "[{6,2243},{20,1.14}]",
  attr_rank = "[{6,11170},{20,1}]",
  price = 50,
  is_material = 0,
  glory = 8
})
table.insert(basic, {
  type = 302001,
  type_next = 303001,
  pos = 4,
  quality = 3,
  rank = 2,
  level = 1,
  exp = 0,
  attr_basic = "[{1,25624}]",
  attr_add = "[{1,98426},{21,0.66}]",
  attr_rank = "[{1,189424},{21,4}]",
  price = 50,
  is_material = 0,
  glory = 8
})
table.insert(basic, {
  type = 302002,
  type_next = 303002,
  pos = 1,
  quality = 3,
  rank = 2,
  level = 1,
  exp = 0,
  attr_basic = "[{3,3343},{7,1542}]",
  attr_add = "[{3,12839},{37,4.14},{7,5923}]",
  attr_rank = "[{3,19224},{37,15},{7,8868}]",
  price = 50,
  is_material = 0,
  glory = 8
})
table.insert(basic, {
  type = 302003,
  type_next = 303003,
  pos = 2,
  quality = 3,
  rank = 2,
  level = 1,
  exp = 0,
  attr_basic = "[{4,725}]",
  attr_add = "[{4,2786},{19,1.14}]",
  attr_rank = "[{4,18104},{19,3}]",
  price = 50,
  is_material = 0,
  glory = 8
})
table.insert(basic, {
  type = 302004,
  type_next = 303004,
  pos = 3,
  quality = 3,
  rank = 2,
  level = 1,
  exp = 0,
  attr_basic = "[{5,4986}]",
  attr_add = "[{5,19151},{22,0.66}]",
  attr_rank = "[{5,35539},{22,4}]",
  price = 50,
  is_material = 0,
  glory = 8
})
table.insert(basic, {
  type = 302005,
  type_next = 303005,
  pos = 5,
  quality = 3,
  rank = 2,
  level = 1,
  exp = 0,
  attr_basic = "[{6,584}]",
  attr_add = "[{6,2243},{20,1.14}]",
  attr_rank = "[{6,32199},{20,3}]",
  price = 50,
  is_material = 0,
  glory = 8
})
table.insert(basic, {
  type = 303001,
  type_next = 304001,
  pos = 4,
  quality = 3,
  rank = 3,
  level = 1,
  exp = 0,
  attr_basic = "[{1,25624}]",
  attr_add = "[{1,98426},{21,0.66}]",
  attr_rank = "[{1,338767},{21,13}]",
  price = 50,
  is_material = 0,
  glory = 8
})
table.insert(basic, {
  type = 303002,
  type_next = 304002,
  pos = 1,
  quality = 3,
  rank = 3,
  level = 1,
  exp = 0,
  attr_basic = "[{3,3343},{7,1542}]",
  attr_add = "[{3,12839},{37,4.14},{7,5923}]",
  attr_rank = "[{3,53419},{37,30},{7,24643}]",
  price = 50,
  is_material = 0,
  glory = 8
})
table.insert(basic, {
  type = 303003,
  type_next = 304003,
  pos = 2,
  quality = 3,
  rank = 3,
  level = 1,
  exp = 0,
  attr_basic = "[{4,725}]",
  attr_add = "[{4,2786},{19,1.14}]",
  attr_rank = "[{4,33942},{19,9}]",
  price = 50,
  is_material = 0,
  glory = 8
})
table.insert(basic, {
  type = 303004,
  type_next = 304004,
  pos = 3,
  quality = 3,
  rank = 3,
  level = 1,
  exp = 0,
  attr_basic = "[{5,4986}]",
  attr_add = "[{5,19151},{22,0.66}]",
  attr_rank = "[{5,63558},{22,13}]",
  price = 50,
  is_material = 0,
  glory = 8
})
table.insert(basic, {
  type = 303005,
  type_next = 304005,
  pos = 5,
  quality = 3,
  rank = 3,
  level = 1,
  exp = 0,
  attr_basic = "[{6,584}]",
  attr_add = "[{6,2243},{20,1.14}]",
  attr_rank = "[{6,60369},{20,9}]",
  price = 50,
  is_material = 0,
  glory = 8
})
table.insert(basic, {
  type = 304001,
  type_next = 305001,
  pos = 4,
  quality = 3,
  rank = 4,
  level = 1,
  exp = 0,
  attr_basic = "[{1,25624}]",
  attr_add = "[{1,98426},{21,0.66}]",
  attr_rank = "[{1,541249},{21,24}]",
  price = 50,
  is_material = 0,
  glory = 8
})
table.insert(basic, {
  type = 304002,
  type_next = 305002,
  pos = 1,
  quality = 3,
  rank = 4,
  level = 1,
  exp = 0,
  attr_basic = "[{3,3343},{7,1542}]",
  attr_add = "[{3,12839},{37,4.14},{7,5923}]",
  attr_rank = "[{3,95527},{37,50},{7,44068}]",
  price = 50,
  is_material = 0,
  glory = 8
})
table.insert(basic, {
  type = 304003,
  type_next = 305003,
  pos = 2,
  quality = 3,
  rank = 4,
  level = 1,
  exp = 0,
  attr_basic = "[{4,725}]",
  attr_add = "[{4,2786},{19,1.14}]",
  attr_rank = "[{4,51573},{19,17}]",
  price = 50,
  is_material = 0,
  glory = 8
})
table.insert(basic, {
  type = 304004,
  type_next = 305004,
  pos = 3,
  quality = 3,
  rank = 4,
  level = 1,
  exp = 0,
  attr_basic = "[{5,4986}]",
  attr_add = "[{5,19151},{22,0.66}]",
  attr_rank = "[{5,101548},{22,24}]",
  price = 50,
  is_material = 0,
  glory = 8
})
table.insert(basic, {
  type = 304005,
  type_next = 305005,
  pos = 5,
  quality = 3,
  rank = 4,
  level = 1,
  exp = 0,
  attr_basic = "[{6,584}]",
  attr_add = "[{6,2243},{20,1.14}]",
  attr_rank = "[{6,91728},{20,17}]",
  price = 50,
  is_material = 0,
  glory = 8
})
table.insert(basic, {
  type = 305001,
  type_next = 306001,
  pos = 4,
  quality = 3,
  rank = 5,
  level = 1,
  exp = 0,
  attr_basic = "[{1,25624}]",
  attr_add = "[{1,98426},{21,0.66}]",
  attr_rank = "[{1,896067},{21,36}]",
  price = 50,
  is_material = 0,
  glory = 8
})
table.insert(basic, {
  type = 305002,
  type_next = 306002,
  pos = 1,
  quality = 3,
  rank = 5,
  level = 1,
  exp = 0,
  attr_basic = "[{3,3343},{7,1542}]",
  attr_add = "[{3,12839},{37,4.14},{7,5923}]",
  attr_rank = "[{3,155396},{37,72},{7,71686}]",
  price = 50,
  is_material = 0,
  glory = 8
})
table.insert(basic, {
  type = 305003,
  type_next = 306003,
  pos = 2,
  quality = 3,
  rank = 5,
  level = 1,
  exp = 0,
  attr_basic = "[{4,725}]",
  attr_add = "[{4,2786},{19,1.14}]",
  attr_rank = "[{4,71876},{19,28}]",
  price = 50,
  is_material = 0,
  glory = 8
})
table.insert(basic, {
  type = 305004,
  type_next = 306004,
  pos = 3,
  quality = 3,
  rank = 5,
  level = 1,
  exp = 0,
  attr_basic = "[{5,4986}]",
  attr_add = "[{5,19151},{22,0.66}]",
  attr_rank = "[{5,168118},{22,36}]",
  price = 50,
  is_material = 0,
  glory = 8
})
table.insert(basic, {
  type = 305005,
  type_next = 306005,
  pos = 5,
  quality = 3,
  rank = 5,
  level = 1,
  exp = 0,
  attr_basic = "[{6,584}]",
  attr_add = "[{6,2243},{20,1.14}]",
  attr_rank = "[{6,127840},{20,28}]",
  price = 50,
  is_material = 0,
  glory = 8
})
table.insert(basic, {
  type = 306001,
  type_next = 0,
  pos = 4,
  quality = 3,
  rank = 6,
  level = 1,
  exp = 0,
  attr_basic = "[{1,25624}]",
  attr_add = "[{1,98426},{21,0.66}]",
  attr_rank = "[{1,1438526},{21,50}]",
  price = 50,
  is_material = 0,
  glory = 8
})
table.insert(basic, {
  type = 306002,
  type_next = 0,
  pos = 1,
  quality = 3,
  rank = 6,
  level = 1,
  exp = 0,
  attr_basic = "[{3,3343},{7,1542}]",
  attr_add = "[{3,12839},{37,4.14},{7,5923}]",
  attr_rank = "[{3,236290},{37,100},{7,109004}]",
  price = 50,
  is_material = 0,
  glory = 8
})
table.insert(basic, {
  type = 306003,
  type_next = 0,
  pos = 2,
  quality = 3,
  rank = 6,
  level = 1,
  exp = 0,
  attr_basic = "[{4,725}]",
  attr_add = "[{4,2786},{19,1.14}]",
  attr_rank = "[{4,102416},{19,42}]",
  price = 50,
  is_material = 0,
  glory = 8
})
table.insert(basic, {
  type = 306004,
  type_next = 0,
  pos = 3,
  quality = 3,
  rank = 6,
  level = 1,
  exp = 0,
  attr_basic = "[{5,4986}]",
  attr_add = "[{5,19151},{22,0.66}]",
  attr_rank = "[{5,269892},{22,50}]",
  price = 50,
  is_material = 0,
  glory = 8
})
table.insert(basic, {
  type = 306005,
  type_next = 0,
  pos = 5,
  quality = 3,
  rank = 6,
  level = 1,
  exp = 0,
  attr_basic = "[{6,584}]",
  attr_add = "[{6,2243},{20,1.14}]",
  attr_rank = "[{6,182158},{20,42}]",
  price = 50,
  is_material = 0,
  glory = 8
})
table.insert(basic, {
  type = 400001,
  type_next = 401001,
  pos = 4,
  quality = 4,
  rank = 0,
  level = 1,
  exp = 0,
  attr_basic = "[{1,101378},{21,1}]",
  attr_add = "[{1,129360},{21,0.83}]",
  attr_rank = "[]",
  price = 300,
  is_material = 0,
  glory = 50
})
table.insert(basic, {
  type = 400002,
  type_next = 401002,
  pos = 1,
  quality = 4,
  rank = 0,
  level = 1,
  exp = 0,
  attr_basic = "[{3,13224},{37,1},{7,6101}]",
  attr_add = "[{3,16853},{37,5.48},{7,7774}]",
  attr_rank = "[]",
  price = 300,
  is_material = 0,
  glory = 50
})
table.insert(basic, {
  type = 400003,
  type_next = 401003,
  pos = 2,
  quality = 4,
  rank = 0,
  level = 1,
  exp = 0,
  attr_basic = "[{4,2870},{19,1}]",
  attr_add = "[{4,3657},{19,1.48}]",
  attr_rank = "[]",
  price = 300,
  is_material = 0,
  glory = 50
})
table.insert(basic, {
  type = 400004,
  type_next = 401004,
  pos = 3,
  quality = 4,
  rank = 0,
  level = 1,
  exp = 0,
  attr_basic = "[{5,19725},{22,1}]",
  attr_add = "[{5,25138},{22,0.83}]",
  attr_rank = "[]",
  price = 300,
  is_material = 0,
  glory = 50
})
table.insert(basic, {
  type = 400005,
  type_next = 401005,
  pos = 5,
  quality = 4,
  rank = 0,
  level = 1,
  exp = 0,
  attr_basic = "[{6,2310},{20,1}]",
  attr_add = "[{6,2944},{20,1.48}]",
  attr_rank = "[]",
  price = 300,
  is_material = 0,
  glory = 50
})
table.insert(basic, {
  type = 401001,
  type_next = 402001,
  pos = 4,
  quality = 4,
  rank = 1,
  level = 1,
  exp = 0,
  attr_basic = "[{1,101378},{21,1}]",
  attr_add = "[{1,129360},{21,0.83}]",
  attr_rank = "[{1,78571},{21,5}]",
  price = 300,
  is_material = 0,
  glory = 50
})
table.insert(basic, {
  type = 401002,
  type_next = 402002,
  pos = 1,
  quality = 4,
  rank = 1,
  level = 1,
  exp = 0,
  attr_basic = "[{3,13224},{37,1},{7,6101}]",
  attr_add = "[{3,16853},{37,5.48},{7,7774}]",
  attr_rank = "[{3,5998},{37,20},{7,2767}]",
  price = 300,
  is_material = 0,
  glory = 50
})
table.insert(basic, {
  type = 401003,
  type_next = 402003,
  pos = 2,
  quality = 4,
  rank = 1,
  level = 1,
  exp = 0,
  attr_basic = "[{4,2870},{19,1}]",
  attr_add = "[{4,3657},{19,1.48}]",
  attr_rank = "[{4,10250},{19,3}]",
  price = 300,
  is_material = 0,
  glory = 50
})
table.insert(basic, {
  type = 401004,
  type_next = 402004,
  pos = 3,
  quality = 4,
  rank = 1,
  level = 1,
  exp = 0,
  attr_basic = "[{5,19725},{22,1}]",
  attr_add = "[{5,25138},{22,0.83}]",
  attr_rank = "[{5,14741},{22,5}]",
  price = 300,
  is_material = 0,
  glory = 50
})
table.insert(basic, {
  type = 401005,
  type_next = 402005,
  pos = 5,
  quality = 4,
  rank = 1,
  level = 1,
  exp = 0,
  attr_basic = "[{6,2310},{20,1}]",
  attr_add = "[{6,2944},{20,1.48}]",
  attr_rank = "[{6,18231},{20,3}]",
  price = 300,
  is_material = 0,
  glory = 50
})
table.insert(basic, {
  type = 402001,
  type_next = 403001,
  pos = 4,
  quality = 4,
  rank = 2,
  level = 1,
  exp = 0,
  attr_basic = "[{1,101378},{21,1}]",
  attr_add = "[{1,129360},{21,0.83}]",
  attr_rank = "[{1,224273},{21,11}]",
  price = 300,
  is_material = 0,
  glory = 50
})
table.insert(basic, {
  type = 402002,
  type_next = 403002,
  pos = 1,
  quality = 4,
  rank = 2,
  level = 1,
  exp = 0,
  attr_basic = "[{3,13224},{37,1},{7,6101}]",
  attr_add = "[{3,16853},{37,5.48},{7,7774}]",
  attr_rank = "[{3,20769},{37,42},{7,9581}]",
  price = 300,
  is_material = 0,
  glory = 50
})
table.insert(basic, {
  type = 402003,
  type_next = 403003,
  pos = 2,
  quality = 4,
  rank = 2,
  level = 1,
  exp = 0,
  attr_basic = "[{4,2870},{19,1}]",
  attr_add = "[{4,3657},{19,1.48}]",
  attr_rank = "[{4,25676},{19,7}]",
  price = 300,
  is_material = 0,
  glory = 50
})
table.insert(basic, {
  type = 402004,
  type_next = 403004,
  pos = 3,
  quality = 4,
  rank = 2,
  level = 1,
  exp = 0,
  attr_basic = "[{5,19725},{22,1}]",
  attr_add = "[{5,25138},{22,0.83}]",
  attr_rank = "[{5,42078},{22,11}]",
  price = 300,
  is_material = 0,
  glory = 50
})
table.insert(basic, {
  type = 402005,
  type_next = 403005,
  pos = 5,
  quality = 4,
  rank = 2,
  level = 1,
  exp = 0,
  attr_basic = "[{6,2310},{20,1}]",
  attr_add = "[{6,2944},{20,1.48}]",
  attr_rank = "[{6,45667},{20,7}]",
  price = 300,
  is_material = 0,
  glory = 50
})
table.insert(basic, {
  type = 403001,
  type_next = 404001,
  pos = 4,
  quality = 4,
  rank = 3,
  level = 1,
  exp = 0,
  attr_basic = "[{1,101378},{21,1}]",
  attr_add = "[{1,129360},{21,0.83}]",
  attr_rank = "[{1,384225},{21,23}]",
  price = 300,
  is_material = 0,
  glory = 50
})
table.insert(basic, {
  type = 403002,
  type_next = 404002,
  pos = 1,
  quality = 4,
  rank = 3,
  level = 1,
  exp = 0,
  attr_basic = "[{3,13224},{37,1},{7,6101}]",
  attr_add = "[{3,16853},{37,5.48},{7,7774}]",
  attr_rank = "[{3,57932},{37,66},{7,26725}]",
  price = 300,
  is_material = 0,
  glory = 50
})
table.insert(basic, {
  type = 403003,
  type_next = 404003,
  pos = 2,
  quality = 4,
  rank = 3,
  level = 1,
  exp = 0,
  attr_basic = "[{4,2870},{19,1}]",
  attr_add = "[{4,3657},{19,1.48}]",
  attr_rank = "[{4,42661},{19,15}]",
  price = 300,
  is_material = 0,
  glory = 50
})
table.insert(basic, {
  type = 403004,
  type_next = 404004,
  pos = 3,
  quality = 4,
  rank = 3,
  level = 1,
  exp = 0,
  attr_basic = "[{5,19725},{22,1}]",
  attr_add = "[{5,25138},{22,0.83}]",
  attr_rank = "[{5,72087},{22,23}]",
  price = 300,
  is_material = 0,
  glory = 50
})
table.insert(basic, {
  type = 403005,
  type_next = 404005,
  pos = 5,
  quality = 4,
  rank = 3,
  level = 1,
  exp = 0,
  attr_basic = "[{6,2310},{20,1}]",
  attr_add = "[{6,2944},{20,1.48}]",
  attr_rank = "[{6,75877},{20,15}]",
  price = 300,
  is_material = 0,
  glory = 50
})
table.insert(basic, {
  type = 404001,
  type_next = 405001,
  pos = 4,
  quality = 4,
  rank = 4,
  level = 1,
  exp = 0,
  attr_basic = "[{1,101378},{21,1}]",
  attr_add = "[{1,129360},{21,0.83}]",
  attr_rank = "[{1,600936},{21,38}]",
  price = 300,
  is_material = 0,
  glory = 50
})
table.insert(basic, {
  type = 404002,
  type_next = 405002,
  pos = 1,
  quality = 4,
  rank = 4,
  level = 1,
  exp = 0,
  attr_basic = "[{3,13224},{37,1},{7,6101}]",
  attr_add = "[{3,16853},{37,5.48},{7,7774}]",
  attr_rank = "[{3,110454},{37,92},{7,50954}]",
  price = 300,
  is_material = 0,
  glory = 50
})
table.insert(basic, {
  type = 404003,
  type_next = 405003,
  pos = 2,
  quality = 4,
  rank = 4,
  level = 1,
  exp = 0,
  attr_basic = "[{4,2870},{19,1}]",
  attr_add = "[{4,3657},{19,1.48}]",
  attr_rank = "[{4,65661},{19,25}]",
  price = 300,
  is_material = 0,
  glory = 50
})
table.insert(basic, {
  type = 404004,
  type_next = 405004,
  pos = 3,
  quality = 4,
  rank = 4,
  level = 1,
  exp = 0,
  attr_basic = "[{5,19725},{22,1}]",
  attr_add = "[{5,25138},{22,0.83}]",
  attr_rank = "[{5,112746},{22,38}]",
  price = 300,
  is_material = 0,
  glory = 50
})
table.insert(basic, {
  type = 404005,
  type_next = 405005,
  pos = 5,
  quality = 4,
  rank = 4,
  level = 1,
  exp = 0,
  attr_basic = "[{6,2310},{20,1}]",
  attr_add = "[{6,2944},{20,1.48}]",
  attr_rank = "[{6,116786},{20,25}]",
  price = 300,
  is_material = 0,
  glory = 50
})
table.insert(basic, {
  type = 405001,
  type_next = 406001,
  pos = 4,
  quality = 4,
  rank = 5,
  level = 1,
  exp = 0,
  attr_basic = "[{1,101378},{21,1}]",
  attr_add = "[{1,129360},{21,0.83}]",
  attr_rank = "[{1,991433},{21,55}]",
  price = 300,
  is_material = 0,
  glory = 50
})
table.insert(basic, {
  type = 405002,
  type_next = 406002,
  pos = 1,
  quality = 4,
  rank = 5,
  level = 1,
  exp = 0,
  attr_basic = "[{3,13224},{37,1},{7,6101}]",
  attr_add = "[{3,16853},{37,5.48},{7,7774}]",
  attr_rank = "[{3,186839},{37,120},{7,86191}]",
  price = 300,
  is_material = 0,
  glory = 50
})
table.insert(basic, {
  type = 405003,
  type_next = 406003,
  pos = 2,
  quality = 4,
  rank = 5,
  level = 1,
  exp = 0,
  attr_basic = "[{4,2870},{19,1}]",
  attr_add = "[{4,3657},{19,1.48}]",
  attr_rank = "[{4,91691},{19,39}]",
  price = 300,
  is_material = 0,
  glory = 50
})
table.insert(basic, {
  type = 405004,
  type_next = 406004,
  pos = 3,
  quality = 4,
  rank = 5,
  level = 1,
  exp = 0,
  attr_basic = "[{5,19725},{22,1}]",
  attr_add = "[{5,25138},{22,0.83}]",
  attr_rank = "[{5,186010},{22,55}]",
  price = 300,
  is_material = 0,
  glory = 50
})
table.insert(basic, {
  type = 405005,
  type_next = 406005,
  pos = 5,
  quality = 4,
  rank = 5,
  level = 1,
  exp = 0,
  attr_basic = "[{6,2310},{20,1}]",
  attr_add = "[{6,2944},{20,1.48}]",
  attr_rank = "[{6,163082},{20,39}]",
  price = 300,
  is_material = 0,
  glory = 50
})
table.insert(basic, {
  type = 406001,
  type_next = 0,
  pos = 4,
  quality = 4,
  rank = 6,
  level = 1,
  exp = 0,
  attr_basic = "[{1,101378},{21,1}]",
  attr_add = "[{1,129360},{21,0.83}]",
  attr_rank = "[{1,1646330},{21,74}]",
  price = 550,
  is_material = 0,
  glory = 50
})
table.insert(basic, {
  type = 406002,
  type_next = 0,
  pos = 1,
  quality = 4,
  rank = 6,
  level = 1,
  exp = 0,
  attr_basic = "[{3,13224},{37,1},{7,6101}]",
  attr_add = "[{3,16853},{37,5.48},{7,7774}]",
  attr_rank = "[{3,296644},{37,150},{7,136846}]",
  price = 550,
  is_material = 0,
  glory = 50
})
table.insert(basic, {
  type = 406003,
  type_next = 0,
  pos = 2,
  quality = 4,
  rank = 6,
  level = 1,
  exp = 0,
  attr_basic = "[{4,2870},{19,1}]",
  attr_add = "[{4,3657},{19,1.48}]",
  attr_rank = "[{4,136052},{19,56}]",
  price = 550,
  is_material = 0,
  glory = 50
})
table.insert(basic, {
  type = 406004,
  type_next = 0,
  pos = 3,
  quality = 4,
  rank = 6,
  level = 1,
  exp = 0,
  attr_basic = "[{5,19725},{22,1}]",
  attr_add = "[{5,25138},{22,0.83}]",
  attr_rank = "[{5,308880},{22,74}]",
  price = 550,
  is_material = 0,
  glory = 50
})
table.insert(basic, {
  type = 406005,
  type_next = 0,
  pos = 5,
  quality = 4,
  rank = 6,
  level = 1,
  exp = 0,
  attr_basic = "[{6,2310},{20,1}]",
  attr_add = "[{6,2944},{20,1.48}]",
  attr_rank = "[{6,241984},{20,56}]",
  price = 550,
  is_material = 0,
  glory = 50
})
table.insert(basic, {
  type = 500001,
  type_next = 501001,
  pos = 4,
  quality = 5,
  rank = 0,
  level = 1,
  exp = 0,
  attr_basic = "[{1,177086},{21,2}]",
  attr_add = "[{1,159964},{21,1}]",
  attr_rank = "[]",
  price = 550,
  is_material = 0,
  glory = 90
})
table.insert(basic, {
  type = 500002,
  type_next = 501002,
  pos = 1,
  quality = 5,
  rank = 0,
  level = 1,
  exp = 0,
  attr_basic = "[{3,23100},{37,2},{7,10656}]",
  attr_add = "[{3,20867},{37,6.83},{7,9626}]",
  attr_rank = "[]",
  price = 550,
  is_material = 0,
  glory = 90
})
table.insert(basic, {
  type = 500003,
  type_next = 501003,
  pos = 2,
  quality = 5,
  rank = 0,
  level = 1,
  exp = 0,
  attr_basic = "[{4,5013},{19,2}]",
  attr_add = "[{4,4528},{19,1.83}]",
  attr_rank = "[]",
  price = 550,
  is_material = 0,
  glory = 90
})
table.insert(basic, {
  type = 500004,
  type_next = 501004,
  pos = 3,
  quality = 5,
  rank = 0,
  level = 1,
  exp = 0,
  attr_basic = "[{5,34456},{22,2}]",
  attr_add = "[{5,31124},{22,1}]",
  attr_rank = "[]",
  price = 550,
  is_material = 0,
  glory = 90
})
table.insert(basic, {
  type = 500005,
  type_next = 501005,
  pos = 5,
  quality = 5,
  rank = 0,
  level = 1,
  exp = 0,
  attr_basic = "[{6,4035},{20,2}]",
  attr_add = "[{6,3645},{20,1.83}]",
  attr_rank = "[]",
  price = 550,
  is_material = 0,
  glory = 90
})
table.insert(basic, {
  type = 501001,
  type_next = 502001,
  pos = 4,
  quality = 5,
  rank = 1,
  level = 1,
  exp = 0,
  attr_basic = "[{1,177086},{21,2}]",
  attr_add = "[{1,159964},{21,1}]",
  attr_rank = "[{1,112111},{21,9}]",
  price = 550,
  is_material = 0,
  glory = 90
})
table.insert(basic, {
  type = 501002,
  type_next = 502002,
  pos = 1,
  quality = 5,
  rank = 1,
  level = 1,
  exp = 0,
  attr_basic = "[{3,23100},{37,2},{7,10656}]",
  attr_add = "[{3,20867},{37,6.83},{7,9626}]",
  attr_rank = "[{3,16300},{37,28},{7,7519}]",
  price = 550,
  is_material = 0,
  glory = 90
})
table.insert(basic, {
  type = 501003,
  type_next = 502003,
  pos = 2,
  quality = 5,
  rank = 1,
  level = 1,
  exp = 0,
  attr_basic = "[{4,5013},{19,2}]",
  attr_add = "[{4,4528},{19,1.83}]",
  attr_rank = "[{4,11910},{19,6}]",
  price = 550,
  is_material = 0,
  glory = 90
})
table.insert(basic, {
  type = 501004,
  type_next = 502004,
  pos = 3,
  quality = 5,
  rank = 1,
  level = 1,
  exp = 0,
  attr_basic = "[{5,34456},{22,2}]",
  attr_add = "[{5,31124},{22,1}]",
  attr_rank = "[{5,21034},{22,9}]",
  price = 550,
  is_material = 0,
  glory = 90
})
table.insert(basic, {
  type = 501005,
  type_next = 502005,
  pos = 5,
  quality = 5,
  rank = 1,
  level = 1,
  exp = 0,
  attr_basic = "[{6,4035},{20,2}]",
  attr_add = "[{6,3645},{20,1.83}]",
  attr_rank = "[{6,21184},{20,6}]",
  price = 550,
  is_material = 0,
  glory = 90
})
table.insert(basic, {
  type = 502001,
  type_next = 503001,
  pos = 4,
  quality = 5,
  rank = 2,
  level = 1,
  exp = 0,
  attr_basic = "[{1,177086},{21,2}]",
  attr_add = "[{1,159964},{21,1}]",
  attr_rank = "[{1,299983},{21,21}]",
  price = 550,
  is_material = 0,
  glory = 90
})
table.insert(basic, {
  type = 502002,
  type_next = 503002,
  pos = 1,
  quality = 5,
  rank = 2,
  level = 1,
  exp = 0,
  attr_basic = "[{3,23100},{37,2},{7,10656}]",
  attr_add = "[{3,20867},{37,6.83},{7,9626}]",
  attr_rank = "[{3,49962},{37,58},{7,23048}]",
  price = 550,
  is_material = 0,
  glory = 90
})
table.insert(basic, {
  type = 502003,
  type_next = 503003,
  pos = 2,
  quality = 5,
  rank = 2,
  level = 1,
  exp = 0,
  attr_basic = "[{4,5013},{19,2}]",
  attr_add = "[{4,4528},{19,1.83}]",
  attr_rank = "[{4,31841},{19,14}]",
  price = 550,
  is_material = 0,
  glory = 90
})
table.insert(basic, {
  type = 502004,
  type_next = 503004,
  pos = 3,
  quality = 5,
  rank = 2,
  level = 1,
  exp = 0,
  attr_basic = "[{5,34456},{22,2}]",
  attr_add = "[{5,31124},{22,1}]",
  attr_rank = "[{5,56282},{22,21}]",
  price = 550,
  is_material = 0,
  glory = 90
})
table.insert(basic, {
  type = 502005,
  type_next = 503005,
  pos = 5,
  quality = 5,
  rank = 2,
  level = 1,
  exp = 0,
  attr_basic = "[{6,4035},{20,2}]",
  attr_add = "[{6,3645},{20,1.83}]",
  attr_rank = "[{6,56632},{20,14}]",
  price = 550,
  is_material = 0,
  glory = 90
})
table.insert(basic, {
  type = 503001,
  type_next = 504001,
  pos = 4,
  quality = 5,
  rank = 3,
  level = 1,
  exp = 0,
  attr_basic = "[{1,177086},{21,2}]",
  attr_add = "[{1,159964},{21,1}]",
  attr_rank = "[{1,576459},{21,35}]",
  price = 550,
  is_material = 0,
  glory = 90
})
table.insert(basic, {
  type = 503002,
  type_next = 504002,
  pos = 1,
  quality = 5,
  rank = 3,
  level = 1,
  exp = 0,
  attr_basic = "[{3,23100},{37,2},{7,10656}]",
  attr_add = "[{3,20867},{37,6.83},{7,9626}]",
  attr_rank = "[{3,98501},{37,90},{7,45440}]",
  price = 550,
  is_material = 0,
  glory = 90
})
table.insert(basic, {
  type = 503003,
  type_next = 504003,
  pos = 2,
  quality = 5,
  rank = 3,
  level = 1,
  exp = 0,
  attr_basic = "[{4,5013},{19,2}]",
  attr_add = "[{4,4528},{19,1.83}]",
  attr_rank = "[{4,57306},{19,24}]",
  price = 550,
  is_material = 0,
  glory = 90
})
table.insert(basic, {
  type = 503004,
  type_next = 504004,
  pos = 3,
  quality = 5,
  rank = 3,
  level = 1,
  exp = 0,
  attr_basic = "[{5,34456},{22,2}]",
  attr_add = "[{5,31124},{22,1}]",
  attr_rank = "[{5,108154},{22,35}]",
  price = 550,
  is_material = 0,
  glory = 90
})
table.insert(basic, {
  type = 503005,
  type_next = 504005,
  pos = 5,
  quality = 5,
  rank = 3,
  level = 1,
  exp = 0,
  attr_basic = "[{6,4035},{20,2}]",
  attr_add = "[{6,3645},{20,1.83}]",
  attr_rank = "[{6,101924},{20,24}]",
  price = 550,
  is_material = 0,
  glory = 90
})
table.insert(basic, {
  type = 504001,
  type_next = 505001,
  pos = 4,
  quality = 5,
  rank = 4,
  level = 1,
  exp = 0,
  attr_basic = "[{1,177086},{21,2}]",
  attr_add = "[{1,159964},{21,1}]",
  attr_rank = "[{1,881192},{21,52}]",
  price = 550,
  is_material = 0,
  glory = 90
})
table.insert(basic, {
  type = 504002,
  type_next = 505002,
  pos = 1,
  quality = 5,
  rank = 4,
  level = 1,
  exp = 0,
  attr_basic = "[{3,23100},{37,2},{7,10656}]",
  attr_add = "[{3,20867},{37,6.83},{7,9626}]",
  attr_rank = "[{3,159392},{37,124},{7,73530}]",
  price = 550,
  is_material = 0,
  glory = 90
})
table.insert(basic, {
  type = 504003,
  type_next = 505003,
  pos = 2,
  quality = 5,
  rank = 4,
  level = 1,
  exp = 0,
  attr_basic = "[{4,5013},{19,2}]",
  attr_add = "[{4,4528},{19,1.83}]",
  attr_rank = "[{4,85779},{19,36}]",
  price = 550,
  is_material = 0,
  glory = 90
})
table.insert(basic, {
  type = 504004,
  type_next = 505004,
  pos = 3,
  quality = 5,
  rank = 4,
  level = 1,
  exp = 0,
  attr_basic = "[{5,34456},{22,2}]",
  attr_add = "[{5,31124},{22,1}]",
  attr_rank = "[{5,165327},{22,52}]",
  price = 550,
  is_material = 0,
  glory = 90
})
table.insert(basic, {
  type = 504005,
  type_next = 505005,
  pos = 5,
  quality = 5,
  rank = 4,
  level = 1,
  exp = 0,
  attr_basic = "[{6,4035},{20,2}]",
  attr_add = "[{6,3645},{20,1.83}]",
  attr_rank = "[{6,152568},{20,36}]",
  price = 550,
  is_material = 0,
  glory = 90
})
table.insert(basic, {
  type = 505001,
  type_next = 506001,
  pos = 4,
  quality = 5,
  rank = 5,
  level = 1,
  exp = 0,
  attr_basic = "[{1,177086},{21,2}]",
  attr_add = "[{1,159964},{21,1}]",
  attr_rank = "[{1,1325346},{21,72}]",
  price = 550,
  is_material = 0,
  glory = 90
})
table.insert(basic, {
  type = 505002,
  type_next = 506002,
  pos = 1,
  quality = 5,
  rank = 5,
  level = 1,
  exp = 0,
  attr_basic = "[{3,23100},{37,2},{7,10656}]",
  attr_add = "[{3,20867},{37,6.83},{7,9626}]",
  attr_rank = "[{3,244361},{37,160},{7,112727}]",
  price = 550,
  is_material = 0,
  glory = 90
})
table.insert(basic, {
  type = 505003,
  type_next = 506003,
  pos = 2,
  quality = 5,
  rank = 5,
  level = 1,
  exp = 0,
  attr_basic = "[{4,5013},{19,2}]",
  attr_add = "[{4,4528},{19,1.83}]",
  attr_rank = "[{4,117497},{19,52}]",
  price = 550,
  is_material = 0,
  glory = 90
})
table.insert(basic, {
  type = 505004,
  type_next = 506004,
  pos = 3,
  quality = 5,
  rank = 5,
  level = 1,
  exp = 0,
  attr_basic = "[{5,34456},{22,2}]",
  attr_add = "[{5,31124},{22,1}]",
  attr_rank = "[{5,248658},{22,72}]",
  price = 550,
  is_material = 0,
  glory = 90
})
table.insert(basic, {
  type = 505005,
  type_next = 506005,
  pos = 5,
  quality = 5,
  rank = 5,
  level = 1,
  exp = 0,
  attr_basic = "[{6,4035},{20,2}]",
  attr_add = "[{6,3645},{20,1.83}]",
  attr_rank = "[{6,208981},{20,52}]",
  price = 550,
  is_material = 0,
  glory = 90
})
table.insert(basic, {
  type = 506001,
  type_next = 0,
  pos = 4,
  quality = 5,
  rank = 6,
  level = 1,
  exp = 0,
  attr_basic = "[{1,177086},{21,2}]",
  attr_add = "[{1,159964},{21,1}]",
  attr_rank = "[{1,1879395},{21,94}]",
  price = 550,
  is_material = 0,
  glory = 90
})
table.insert(basic, {
  type = 506002,
  type_next = 0,
  pos = 1,
  quality = 5,
  rank = 6,
  level = 1,
  exp = 0,
  attr_basic = "[{3,23100},{37,2},{7,10656}]",
  attr_add = "[{3,20867},{37,6.83},{7,9626}]",
  attr_rank = "[{3,344303},{37,200},{7,158832}]",
  price = 550,
  is_material = 0,
  glory = 90
})
table.insert(basic, {
  type = 506003,
  type_next = 0,
  pos = 2,
  quality = 5,
  rank = 6,
  level = 1,
  exp = 0,
  attr_basic = "[{4,5013},{19,2}]",
  attr_add = "[{4,4528},{19,1.83}]",
  attr_rank = "[{4,156994},{19,70}]",
  price = 550,
  is_material = 0,
  glory = 90
})
table.insert(basic, {
  type = 506004,
  type_next = 0,
  pos = 3,
  quality = 5,
  rank = 6,
  level = 1,
  exp = 0,
  attr_basic = "[{5,34456},{22,2}]",
  attr_add = "[{5,31124},{22,1}]",
  attr_rank = "[{5,352607},{22,94}]",
  price = 550,
  is_material = 0,
  glory = 90
})
table.insert(basic, {
  type = 506005,
  type_next = 0,
  pos = 5,
  quality = 5,
  rank = 6,
  level = 1,
  exp = 0,
  attr_basic = "[{6,4035},{20,2}]",
  attr_add = "[{6,3645},{20,1.83}]",
  attr_rank = "[{6,279232},{20,70}]",
  price = 550,
  is_material = 0,
  glory = 90
})
