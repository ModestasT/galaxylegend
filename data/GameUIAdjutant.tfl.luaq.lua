local GameUIAdjutant = LuaObjectManager:GetLuaObject("GameUIAdjutant")
local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameUIWDStuff = LuaObjectManager:GetLuaObject("GameUIWDStuff")
local TutorialAdjutantManager = LuaObjectManager:GetLuaObject("TutorialAdjutantManager")
local CommanderRanks = {
  [1] = "rank_E",
  [2] = "rank_D",
  [3] = "rank_C",
  [4] = "rank_B",
  [5] = "rank_A",
  [6] = "rank_S",
  [7] = "rank_SS"
}
local HeroStatus = {
  ON_BATTLE = 1,
  ON_REVERSE = 2,
  ON_DISMISS = 3,
  OTHER = 4
}
GameUIAdjutant.mFleetIdBeforeEnter = -1
GameUIAdjutant.mHeroList = nil
GameUIAdjutant.mCurAdjutantList = nil
GameUIAdjutant.mCurSelectedHeroIdx = 1
GameUIAdjutant.mCurSelectAdjutantIdx = 0
GameUIAdjutant.mCurRequestMajorIdx = 0
GameUIAdjutant.mCurRequestAdjutantIdxInHeroList = 0
GameUIAdjutant.mCurRequestAdjutantIdxInAdjutantList = 0
GameUIAdjutant.mIsReleaseHeroSelf = false
GameUIAdjutant.teamLeagueType = 0
GameUIAdjutant.mulMatrixs = nil
function GameUIAdjutant:GetLeaderFleet()
  local leaderFleet = 1
  if self.teamLeagueType == 0 then
    local leaderlist = GameGlobalData:GetData("leaderlist")
    leaderFleet = leaderlist.leader_ids[GameGlobalData:GetData("matrix").id]
  elseif self.teamLeagueType == 1 then
    local atk = FleetTeamLeagueMatrix:GetAttackInstance()
    if atk then
      leaderFleet = atk:GetLeaderFleet()
    end
  elseif self.teamLeagueType == 2 then
    local def = FleetTeamLeagueMatrix:GetDefenceInstance()
    if def then
      leaderFleet = def:GetLeaderFleet()
    end
  end
  DebugOut("GetLeaderFleet ==" .. leaderFleet)
  return leaderFleet
end
function GameUIAdjutant:OnAddToGameState(game_state)
  DebugOut("GameUIAdjutant:OnAddToGameState")
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  local function getMulMatrixCallback(msgType, content)
    if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.simple_mulmatrix_get_req.Code then
      if content.code ~= 0 then
        GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      end
      return true
    elseif msgType == NetAPIList.simple_mulmatrix_get_ack.Code then
      DebugTable(content)
      GameUIAdjutant.mulMatrixs = content.matrixs
      return true
    end
    return false
  end
  NetMessageMgr:SendMsg(NetAPIList.simple_mulmatrix_get_req.Code, nil, getMulMatrixCallback, false, nil)
  if not FleetTeamLeagueMatrix:GetAttackInstance():HasInit() then
    FleetTeamLeagueMatrix:GetMatrixsReq(0, nil)
  end
  GameUIAdjutant:CheckDownloadImage()
  GameUIAdjutant:Init(nil)
  local function callback()
    DebugOut("OnAddToGameState:callback")
    GameUIAdjutant:RequestDismissHeroList()
  end
  if not TutorialAdjutantManager:ReadyTutorialAdjutantBind_Adjutant(callback) then
    callback()
  end
end
function GameUIAdjutant.sortAdjutant(a, b)
  DebugOut("a=" .. tostring(a) .. " b=" .. tostring(b))
  local matrixCells = GameGlobalData:GetData("matrix").cells
  for k, v in pairs(matrixCells) do
    if v.fleet_identity == a then
      DebugOut("1 return false")
      return false
    end
  end
  for k, v in pairs(matrixCells) do
    if v.fleet_identity == b then
      DebugOut("2 return true")
      return true
    end
  end
  if GameUIAdjutant:GetMajorId(b) > 0 then
    DebugOut("3 return true")
    return true
  elseif GameUIAdjutant:GetMajorId(a) > 0 then
    DebugOut("4 return false")
    return false
  end
  DebugOut("5 return true")
  return true
end
function GameUIAdjutant:Init(heroList)
  DebugOut("GameUIAdjutant:Init ")
  GameUIAdjutant.mHeroList = heroList
  GameUIAdjutant.mCurSelectedHeroIdx = 1
  if GameUIAdjutant.mHeroList and #GameUIAdjutant.mHeroList > 0 then
    local battleHeros = {}
    if self.teamLeagueType == 0 then
      local matrixCells = GameGlobalData:GetData("matrix").cells
      if matrixCells and #matrixCells > 0 then
        for k = 1, #matrixCells do
          if -1 ~= matrixCells[k].fleet_identity and #GameUIAdjutant.mHeroList > 0 then
            for i = 1, #GameUIAdjutant.mHeroList do
              if matrixCells[k].fleet_identity == GameUIAdjutant.mHeroList[i].identity then
                table.insert(battleHeros, GameUIAdjutant.mHeroList[i])
                table.remove(GameUIAdjutant.mHeroList, i)
                break
              end
            end
          end
        end
      end
    end
    local team
    if self.teamLeagueType == 1 then
      team = FleetTeamLeagueMatrix:GetAttackInstance()
    elseif self.teamLeagueType == 2 then
      team = FleetTeamLeagueMatrix:GetDefenceInstance()
    end
    if team then
      local fleets = team:GetMatrixFleets()
      DebugOutPutTable(fleets, "fleets===")
      for k = 1, #fleets do
        for i = 1, #GameUIAdjutant.mHeroList do
          if fleets[k] == GameUIAdjutant.mHeroList[i].identity then
            table.insert(battleHeros, GameUIAdjutant.mHeroList[i])
            table.remove(GameUIAdjutant.mHeroList, i)
            break
          end
        end
      end
    end
    if #battleHeros > 0 then
      local mainFleet
      for k = 1, #battleHeros do
        if 1 == battleHeros[k].identity then
          mainFleet = battleHeros[k]
        else
          table.insert(GameUIAdjutant.mHeroList, 1, battleHeros[k])
        end
      end
      if mainFleet then
        table.insert(GameUIAdjutant.mHeroList, 1, mainFleet)
      end
    end
    DebugTable(heroList)
    DebugOut("battle Heros = " .. #battleHeros)
    DebugOut("all Heros = " .. #GameUIAdjutant.mHeroList)
    if 0 < GameUIAdjutant.mFleetIdBeforeEnter then
      for k = 1, #GameUIAdjutant.mHeroList do
        if GameUIAdjutant.mHeroList[k].identity == GameUIAdjutant.mFleetIdBeforeEnter then
          GameUIAdjutant.mCurSelectedHeroIdx = k
          break
        end
      end
    end
  end
  DebugTable(GameUIAdjutant.mHeroList)
  DebugOut("GameUIAdjutant.mCurSelectedHeroIdx " .. tostring(GameUIAdjutant.mCurSelectedHeroIdx) .. " GameUIAdjutant.mFleetIdBeforeEnter " .. tostring(GameUIAdjutant.mFleetIdBeforeEnter))
  GameUIAdjutant:SetHeroList()
end
function GameUIAdjutant:OnEraseFromGameState(game_state)
  self:UnloadFlashObject()
  GameUIAdjutant.mFleetIdBeforeEnter = -1
  if GameUIAdjutant.mExitCallback then
    GameUIAdjutant.mExitCallback()
    GameUIAdjutant.mExitCallback = nil
  end
  GameUIAdjutant.teamLeagueType = 0
  GameUIAdjutant.mulMatrixs = nil
end
function GameUIAdjutant:CheckDownloadImage()
  if (ext.crc32.crc32("data2/LAZY_LOAD_map_star_01.png") == "" or ext.crc32.crc32("data2/LAZY_LOAD_map_star_01.png") == nil) and not AutoUpdateInBackground:IsHeroFileUpdatedToServer("data2/LAZY_LOAD_map_star_01.png") then
    self:GetFlashObject():ReplaceTexture("LAZY_LOAD_map_star_01.png", "territorial_map_bg.png")
  end
end
GameUIAdjutant.mExitCallback = nil
function GameUIAdjutant:EnterAdjutantUI(exitCallback, teamLeagueType)
  local unlockLv = GameGlobalData:GetData("adjutant_unlock_level")
  local playerLv = GameGlobalData:GetData("levelinfo").level
  if unlockLv > playerLv then
    local tip = GameLoader:GetGameText("LC_MENU_BRIDGE_LEVEL")
    tip = string.format(tip, unlockLv)
    GameTip:Show(tip, 3000)
  else
    local curState = GameStateManager:GetCurrentGameState()
    if not curState:IsObjectInState(self) then
      curState:AddObject(self)
    end
    GameUIAdjutant.mExitCallback = exitCallback
  end
  if teamLeagueType ~= nil then
    GameUIAdjutant.teamLeagueType = teamLeagueType
  end
end
function GameUIAdjutant:Exit()
  local curState = GameStateManager:GetCurrentGameState()
  if curState:IsObjectInState(self) then
    curState:EraseObject(self)
  end
end
function GameUIAdjutant:OnFSCommand(cmd, arg)
  if cmd == "close" then
    GameUIAdjutant:Exit()
    GameUIAdjutant.IsShowAdjutantUI = false
    TutorialAdjutantManager:CheckRefreshNewEnhance()
  elseif cmd == "NeedUpdateHeroListItem" then
    GameUIAdjutant:UpdateHeroListItem(tonumber(arg))
  elseif cmd == "NeedUpdateAdjutantListItem" then
    GameUIAdjutant:UpdateAdjutantListItem(tonumber(arg))
  elseif cmd == "NeedUpdateSkillListItem" then
    GameUIAdjutant:UpdateSkillListItem(tonumber(arg))
  elseif cmd == "HeroItemClicked" then
    local lastSelectedIdx = GameUIAdjutant.mCurSelectedHeroIdx
    GameUIAdjutant.mCurSelectedHeroIdx = tonumber(arg)
    GameUIAdjutant:UpdateHeroListItem(lastSelectedIdx)
    GameUIAdjutant:UpdateHeroListItem(tonumber(arg))
    local adjutantList = GameUIAdjutant.mHeroList[tonumber(arg)].can_adjutant
    GameUIAdjutant:SetAdjutantList(adjutantList)
  elseif cmd == "AdjutantItemClicked" then
    local param_list = LuaUtils:string_split(arg, ",")
    GameUIAdjutant:SetAdjutantSelected(GameUIAdjutant.mCurSelectAdjutantIdx, false)
    GameUIAdjutant.mCurSelectAdjutantIdx = (tonumber(param_list[1]) - 1) * 2 + tonumber(param_list[2])
    GameUIAdjutant:SetAdjutantSelected(GameUIAdjutant.mCurSelectAdjutantIdx, true)
    GameUIAdjutant:SetAdjutantInfo(GameUIAdjutant.mCurSelectAdjutantIdx)
    TutorialAdjutantManager:ShowTutorialAdjutantBind_BindBtn(GameUIAdjutant.mCurSelectAdjutantIdx)
  elseif cmd == "commanderDetail" then
    GameUIAdjutant:ShowCommanderDetail()
  elseif cmd == "equip" then
    GameUIAdjutant:EquipAdjutant()
    TutorialAdjutantManager:CheckFinishedTutorialAdjutantBind()
  elseif cmd == "dismiss" or cmd == "dismissOther" then
    GameUIAdjutant:ReleaseAdjutant()
  else
    if cmd == "btn_help" then
      GameUIWDStuff:Show(GameUIWDStuff.menuType.menu_type_help)
      GameUIWDStuff:SetHelpText(GameLoader:GetGameText("LC_MENU_BRIDGE_HELP"))
    else
    end
  end
end
function GameUIAdjutant:SetHeroList()
  local flashObj = self:GetFlashObject()
  local heroCnt = 0
  if GameUIAdjutant.mHeroList and 0 < #GameUIAdjutant.mHeroList then
    heroCnt = #GameUIAdjutant.mHeroList
  end
  if flashObj then
    flashObj:InvokeASCallback("_root", "SetAdjutantInfoVisible", false)
    flashObj:InvokeASCallback("_root", "SetSkillListBoxVisible", false)
    flashObj:InvokeASCallback("_root", "SetNoAdjutantBoxVisible", false)
    flashObj:InvokeASCallback("_root", "SetDissmissAdjutantBoxVisible", false)
    flashObj:InvokeASCallback("_root", "SetHeroList", heroCnt)
  end
  if GameUIAdjutant.mHeroList and 0 < #GameUIAdjutant.mHeroList then
    GameUIAdjutant:OnFSCommand("HeroItemClicked", tostring(GameUIAdjutant.mCurSelectedHeroIdx))
    GameUIAdjutant:SetHeroToHead(GameUIAdjutant.mCurSelectedHeroIdx)
  end
end
function GameUIAdjutant:SetHeroToHead(selectedIdx)
  local flashObj = GameUIAdjutant:GetFlashObject()
  if not flashObj then
    return
  end
  DebugOut("GameUIAdjutant:SetHeroToHead = ", selectedIdx)
  flashObj:InvokeASCallback("_root", "SetHeroToHead", selectedIdx)
end
function GameUIAdjutant:SetAdjutantList(adjutantList)
  DebugOutPutTable(adjutantList, "SetAdjutantList")
  GameUIAdjutant.mCurAdjutantList = adjutantList
  GameUIAdjutant.mCurSelectAdjutantIdx = 0
  local flashObj = self:GetFlashObject()
  local adjutantCnt = 0
  if GameUIAdjutant.mCurAdjutantList and #GameUIAdjutant.mCurAdjutantList > 0 then
    GameUIAdjutant.mCurAdjutantList = LuaUtils:BubbleSort(GameUIAdjutant.mCurAdjutantList, GameUIAdjutant.sortAdjutant)
    if GameUIAdjutant.mHeroList[GameUIAdjutant.mCurSelectedHeroIdx] and 0 < GameUIAdjutant.mHeroList[GameUIAdjutant.mCurSelectedHeroIdx].cur_adjutant then
      local majorId = GameUIAdjutant.mHeroList[GameUIAdjutant.mCurSelectedHeroIdx].identity
      for k = 1, #GameUIAdjutant.mCurAdjutantList do
        local curMajor = GameUIAdjutant:GetMajorId(GameUIAdjutant.mCurAdjutantList[k])
        if majorId == GameUIAdjutant:GetMajorId(GameUIAdjutant.mCurAdjutantList[k]) then
          local curAdjutant = GameUIAdjutant.mCurAdjutantList[k]
          table.remove(GameUIAdjutant.mCurAdjutantList, k)
          table.insert(GameUIAdjutant.mCurAdjutantList, 1, curAdjutant)
          break
        end
      end
    end
    DebugOutPutTable(GameUIAdjutant.mCurAdjutantList, "after SetAdjutantList")
    adjutantCnt = #GameUIAdjutant.mCurAdjutantList
  end
  TutorialAdjutantManager:ShowTutorialAdjutantBind_ShowInFirst()
  if flashObj then
    flashObj:InvokeASCallback("_root", "SetAdjutantInfoVisible", false)
    flashObj:InvokeASCallback("_root", "SetSkillListBoxVisible", false)
    flashObj:InvokeASCallback("_root", "SetDissmissAdjutantBoxVisible", false)
    local noAdjutant = false
    if GameUIAdjutant.mHeroList[GameUIAdjutant.mCurSelectedHeroIdx].cur_adjutant == 0 then
      noAdjutant = true
    end
    flashObj:InvokeASCallback("_root", "SetNoAdjutantBoxInfo", GameLoader:GetGameText("LC_MENU_BRIDGE_INFO_2"))
    flashObj:InvokeASCallback("_root", "SetNoAdjutantBoxVisible", noAdjutant)
    local itemCount = math.ceil(adjutantCnt / 2)
    local isHeroIsAdjutant = false
    local hero = GameUIAdjutant.mHeroList[GameUIAdjutant.mCurSelectedHeroIdx]
    local majorId = GameUIAdjutant:GetMajorId(hero.identity)
    if majorId > 0 then
      itemCount = 0
    end
    if noAdjutant or 0 == itemCount then
      local hero = GameUIAdjutant.mHeroList[GameUIAdjutant.mCurSelectedHeroIdx]
      local force = hero.force
      local vessleType = GameDataAccessHelper:GetCommanderVesselsType(hero.identity, hero.level)
      local fleetName = GameDataAccessHelper:GetFleetLevelDisplayName(hero.identity, hero.level)
      local sexx = GameDataAccessHelper:GetCommanderSex(hero.identity)
      if hero.identity == 1 then
        vessleType = GameDataAccessHelper:GetCommanderVesselsType(GameUIAdjutant:GetLeaderFleet(), hero.level)
        fleetName = GameDataAccessHelper:GetFleetLevelDisplayName(GameUIAdjutant:GetLeaderFleet(), hero.level)
        sexx = GameDataAccessHelper:GetCommanderSex(GameUIAdjutant:GetLeaderFleet())
      end
      if sexx == 1 then
        sexx = "man"
      elseif sexx == 0 then
        sexx = "woman"
      else
        sexx = "unknown"
      end
      local heroid = hero.identity
      if heroid == 1 then
        heroid = GameUIAdjutant:GetLeaderFleet()
      end
      local adjutantDetailInfo = {
        color = GameDataAccessHelper:GetCommanderColorFrame(heroid, hero.level),
        avatarFr = GameDataAccessHelper:GetFleetAvatar(heroid, hero.level),
        vessleType = vessleType,
        commanderName = fleetName,
        vessleName = GameLoader:GetGameText("LC_FLEET_" .. vessleType),
        force = tostring(force),
        forceTitle = GameLoader:GetGameText("LC_MENU_FORCE_CHAR"),
        sex = sexx
      }
      flashObj:InvokeASCallback("_root", "SetAdjutantInfoVisible", true)
      flashObj:InvokeASCallback("_root", "SetAdjutantInfo", adjutantDetailInfo)
      if majorId > 0 then
        local titleStr = GameLoader:GetGameText("LC_MENU_BRIDGE_PLAYER_ADJUTANT")
        local desc = GameLoader:GetGameText("LC_MENU_BRIDGE_INFO_5")
        local majorName = GameDataAccessHelper:GetFleetLevelDisplayName(majorId)
        titleStr = string.gsub(titleStr, "<player_name>", majorName)
        local adjutantName = GameDataAccessHelper:GetFleetLevelDisplayName(hero.identity)
        desc = string.gsub(desc, "<npc2_name>", majorName)
        desc = string.gsub(desc, "<npc1_name>", adjutantName)
        flashObj:InvokeASCallback("_root", "SetDissmissAdjutantInfo", titleStr, desc)
        flashObj:InvokeASCallback("_root", "SetDissmissAdjutantBoxVisible", true)
        flashObj:InvokeASCallback("_root", "SetNoAdjutantBoxVisible", false)
      end
    end
    flashObj:InvokeASCallback("_root", "SetAdjutantList", itemCount)
    if adjutantCnt > 0 then
      for k = 1, #GameUIAdjutant.mCurAdjutantList do
        local adjutantId = GameUIAdjutant.mCurAdjutantList[k]
        local statusFr = GameUIAdjutant:GetAdjutantStatusFr(adjutantId)
        if "chose" == statusFr then
          GameUIAdjutant:SetAdjutantSelected(GameUIAdjutant.mCurSelectAdjutantIdx, false)
          GameUIAdjutant:SetAdjutantSelected(k, true)
          GameUIAdjutant.mCurSelectAdjutantIdx = k
          GameUIAdjutant:SetAdjutantInfo(k)
          break
        end
      end
    end
  end
end
function GameUIAdjutant:SetAdjutantSelected(adjutantIdx, isSelected)
  local itemId = math.ceil(adjutantIdx / 2)
  local isLeft = adjutantIdx % 2 == 1
  local flashObj = self:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "SetAdjutantSelected", itemId, isLeft, isSelected)
  end
end
function GameUIAdjutant:GetHeroStatus(fleetInfo)
  local matrixCells = GameGlobalData:GetData("matrix").cells
  DebugOutPutTable(matrixCells, " GameUIAdjutant:GetHeroStatus matrixCells ==")
  DebugOut("matrixCells =" .. #matrixCells)
  if self.teamLeagueType == 0 and matrixCells and #matrixCells > 0 then
    for k = 1, #matrixCells do
      if fleetInfo.identity == matrixCells[k].fleet_identity then
        return HeroStatus.ON_BATTLE
      end
    end
  end
  local team
  if self.teamLeagueType == 1 then
    team = FleetTeamLeagueMatrix:GetAttackInstance()
  elseif self.teamLeagueType == 2 then
    team = FleetTeamLeagueMatrix:GetDefenceInstance()
  end
  if team then
    local fleets = team:GetMatrixFleets()
    for k = 1, #fleets do
      if fleetInfo.identity == fleets[k] then
        return HeroStatus.ON_BATTLE
      end
    end
  end
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  DebugOutPutTable(fleets, " GameUIAdjutant:GetHeroStatus fleets ==")
  DebugOut("fleets =" .. #fleets)
  if fleets and #fleets > 0 then
    for k = 1, #fleets do
      if fleetInfo.identity == fleets[k].identity then
        return HeroStatus.ON_REVERSE
      end
    end
  end
  local allFleetId = GameGlobalData:GetData("fleetkryptons")
  DebugOut("allFleetId =" .. #allFleetId)
  DebugOutPutTable(allFleetId, " GameUIAdjutant:GetHeroStatus allFleetId ==")
  if allFleetId and #allFleetId > 0 then
    for k = 1, #allFleetId do
      if fleetInfo.identity == allFleetId[k].fleet_identity then
        return HeroStatus.ON_DISMISS
      end
    end
  end
  return HeroStatus.OTHER
end
function GameUIAdjutant:GetHeroStatusFr(status, isSelected)
  if status == HeroStatus.ON_BATTLE then
    if isSelected then
      return "chose"
    else
      return "on"
    end
  elseif status == HeroStatus.ON_REVERSE then
    if isSelected then
      return "off_chose"
    else
      return "off"
    end
  else
    return "dis"
  end
end
function GameUIAdjutant:GetHeroIdxByFleetId(fleetId)
  if GameUIAdjutant.mHeroList and #GameUIAdjutant.mHeroList > 0 then
    for k = 1, #GameUIAdjutant.mHeroList do
      if fleetId == GameUIAdjutant.mHeroList[k].identity then
        return k
      end
    end
  end
  return -1
end
function GameUIAdjutant:GetMajorId(adjutantId)
  if GameUIAdjutant.mHeroList and #GameUIAdjutant.mHeroList > 0 then
    for k = 1, #GameUIAdjutant.mHeroList do
      if GameUIAdjutant.mHeroList[k] and GameUIAdjutant.mHeroList[k].cur_adjutant == adjutantId then
        return GameUIAdjutant.mHeroList[k].identity
      end
    end
  end
  return -1
end
function GameUIAdjutant:GetAdjutantStatusFr(id)
  if self.mulMatrixs and #self.mulMatrixs > 0 then
    for _, v in pairs(self.mulMatrixs) do
      for k = 1, #v.cells do
        if id == v.cells[k].fleet_identity then
          return "dis", v.name
        end
      end
    end
  end
  local atk = FleetTeamLeagueMatrix:GetAttackInstance()
  if atk then
    local fleets = atk:GetMatrixFleets()
    for k = 1, #fleets do
      if fleets[k] == id then
        return "dis", GameLoader:GetGameText("LC_ALERT_TEAMLEAGUE_ATTACK_FLEETS_CHAR")
      end
    end
  end
  local def = FleetTeamLeagueMatrix:GetDefenceInstance()
  if def then
    local fleets = def:GetMatrixFleets()
    for k = 1, #fleets do
      if fleets[k] == id then
        return "dis", GameLoader:GetGameText("LC_ALERT_TEAMLEAGUE_DEFENSIVE_FLEETS_CHAR")
      end
    end
  end
  if GameUIAdjutant.mHeroList and 0 < #GameUIAdjutant.mHeroList then
    if GameUIAdjutant.mHeroList[GameUIAdjutant.mCurSelectedHeroIdx].cur_adjutant == id then
      return "chose"
    end
    for k = 1, #GameUIAdjutant.mHeroList do
      if GameUIAdjutant.mHeroList[k] and GameUIAdjutant.mHeroList[k].cur_adjutant == id then
        return "on"
      end
    end
  end
  return "off"
end
function GameUIAdjutant:GetHeroById(id)
  DebugOut("GetHeroById " .. tostring(id))
  local hero
  if GameUIAdjutant.mHeroList and #GameUIAdjutant.mHeroList > 0 then
    for k = 1, #GameUIAdjutant.mHeroList do
      if id == GameUIAdjutant.mHeroList[k].identity then
        return GameUIAdjutant.mHeroList[k]
      end
    end
  end
  return hero
end
function GameUIAdjutant:UpdateHeroListItem(itemId)
  local flashObj = self:GetFlashObject()
  if flashObj then
    local hero = GameUIAdjutant.mHeroList[itemId]
    if hero and hero.identity then
      local level = GameGlobalData:GetData("levelinfo").level
      local head = GameDataAccessHelper:GetFleetAvatar(hero.identity, hero.level)
      local commanderAbility = GameDataAccessHelper:GetCommanderAbility(hero.identity, hero.level)
      local rank = GameUtils:GetFleetRankFrame(commanderAbility.Rank, GameUIAdjutant.DownloadRankImageHeroCallback, itemId)
      local status = GameUIAdjutant:GetHeroStatus(hero)
      local isSelected = GameUIAdjutant.mCurSelectedHeroIdx == itemId
      local fr = GameUIAdjutant:GetHeroStatusFr(status, isSelected)
      if GameUIAdjutant:GetMajorId(hero.identity) > 0 then
        fr = "dis"
      end
      if hero.identity == 1 then
        head = GameDataAccessHelper:GetFleetAvatar(GameUIAdjutant:GetLeaderFleet(), hero.level)
        commanderAbility = GameDataAccessHelper:GetCommanderAbility(GameUIAdjutant:GetLeaderFleet(), hero.level)
        rank = GameUtils:GetFleetRankFrame(commanderAbility.Rank, GameUIAdjutant.DownloadRankImageHeroCallback, itemId)
      end
      local hasAdjutant = 0 < hero.cur_adjutant
      local adjutantHead = ""
      local adjutantRank = ""
      if hasAdjutant then
        local adjutant = GameUIAdjutant:GetHeroById(hero.cur_adjutant)
        adjutantHead = GameDataAccessHelper:GetFleetAvatar(adjutant.identity, adjutant.level)
        local adjutantAbility = GameDataAccessHelper:GetCommanderAbility(adjutant.identity, adjutant.level)
        adjutantRank = GameUtils:GetFleetRankFrame(adjutantAbility.Rank, GameUIAdjutant.DownloadRankImageAdjutantCallback, itemId)
      end
      flashObj:InvokeASCallback("_root", "UpdateHeroListItem", itemId, fr, hasAdjutant, head, rank, adjutantHead, adjutantRank)
    end
  end
end
function GameUIAdjutant.DownloadRankImageHeroCallback(extInfo)
  if GameUIAdjutant:GetFlashObject() then
    GameUIAdjutant:GetFlashObject():InvokeASCallback("_root", "updateRankHeroImage", extInfo.rank_id, extInfo.id)
  end
end
function GameUIAdjutant.DownloadRankImageAdjutantCallback(extInfo)
  if GameUIAdjutant:GetFlashObject() then
    GameUIAdjutant:GetFlashObject():InvokeASCallback("_root", "updateRankAdjutantImage", extInfo.rank_id, extInfo.id)
  end
end
function GameUIAdjutant:UpdateAdjutantListItem(itemId)
  local flashObj = self:GetFlashObject()
  local leftIdx = (itemId - 1) * 2 + 1
  local rightIdx = (itemId - 1) * 2 + 2
  if flashObj then
    local l = GameUIAdjutant.mCurAdjutantList[leftIdx]
    local leftAdjutant
    local lInfo = GameUIAdjutant:GetHeroById(l)
    if l and lInfo then
      local adjutantAbility = GameDataAccessHelper:GetCommanderAbility(l, lInfo.level)
      leftAdjutant = {
        fr = GameUIAdjutant:GetAdjutantStatusFr(l),
        avatarFr = GameDataAccessHelper:GetFleetAvatar(l, lInfo.level),
        rankFr = GameUtils:GetFleetRankFrame(adjutantAbility.Rank, GameUIAdjutant.DownloadRankImageleftAdjutantListCallback, itemId)
      }
    end
    local r = GameUIAdjutant.mCurAdjutantList[rightIdx]
    local rightAdjutant
    local rInfo = GameUIAdjutant:GetHeroById(r)
    if r and rInfo then
      local adjutantAbility = GameDataAccessHelper:GetCommanderAbility(r, rInfo.level)
      rightAdjutant = {
        fr = GameUIAdjutant:GetAdjutantStatusFr(r),
        avatarFr = GameDataAccessHelper:GetFleetAvatar(r, rInfo.level),
        rankFr = GameUtils:GetFleetRankFrame(adjutantAbility.Rank, GameUIAdjutant.DownloadRankImageRightAdjutantListCallback, itemId)
      }
    end
    DebugOutPutTable(leftAdjutant, "UpdateAdjutantListItem leftAdjutant")
    DebugOutPutTable(rightAdjutant, "UpdateAdjutantListItem rightAdjutant")
    flashObj:InvokeASCallback("_root", "UpdateAdjutantListItem", itemId, leftAdjutant, rightAdjutant)
    if leftAdjutant and leftIdx == GameUIAdjutant.mCurSelectAdjutantIdx then
      GameUIAdjutant:SetAdjutantSelected(GameUIAdjutant.mCurSelectAdjutantIdx, true)
    end
    if rightAdjutant and rightIdx == GameUIAdjutant.mCurSelectAdjutantIdx then
      GameUIAdjutant:SetAdjutantSelected(GameUIAdjutant.mCurSelectAdjutantIdx, true)
    end
    DebugOut("UpdateAdjutantListItem:TutorialAdjutantManager:", GameUIAdjutant.mCurSelectedHeroIdx)
    TutorialAdjutantManager:ShowTutorialAdjutantBind_Awatar(itemId)
  end
end
function GameUIAdjutant.DownloadRankImageleftAdjutantListCallback(extInfo)
  if GameUIAdjutant:GetFlashObject() then
    GameUIAdjutant:GetFlashObject():InvokeASCallback("_root", "updateLeftAdjutant", extInfo.rank_id, extInfo.id)
  end
end
function GameUIAdjutant.DownloadRankImageRightAdjutantListCallback(extInfo)
  if GameUIAdjutant:GetFlashObject() then
    GameUIAdjutant:GetFlashObject():InvokeASCallback("_root", "updateRightAdjutant", extInfo.rank_id, extInfo.id)
  end
end
function GameUIAdjutant:UpdateSkillListItem(itemId)
  local flashObj = self:GetFlashObject()
  if flashObj then
    local adjutant = GameUIAdjutant:GetHeroById(GameUIAdjutant.mCurAdjutantList[GameUIAdjutant.mCurSelectAdjutantIdx])
    if 1 == itemId then
      local skillName = GameLoader:GetGameText("LC_MENU_BRIDGE_RELATIONSHIP_TITLE")
      local skillItem = ""
      for k, v in pairs(adjutant.adjutant_vessels) do
        local vessleType = "TYPE_" .. v
        local vessleName = GameLoader:GetGameText("LC_FLEET_" .. vessleType)
        skillItem = skillItem .. vessleName .. " "
      end
      flashObj:InvokeASCallback("_root", "UpdateSkillListItem", itemId, false, skillName, "", 1, skillItem)
    else
      local idx = itemId - 1
      local skillName = GameDataAccessHelper:GetAdjutantSkillNameText(adjutant.adjutant_spell[idx].spell_id)
      local skillItem = GameDataAccessHelper:GetAdjutantSkillDesText(adjutant.adjutant_spell[idx].spell_id) .. "\001"
      DebugOut("GameUIAdjutant:UpdateSkillListItem")
      local unlockDesc = GameLoader:GetGameText("LC_MENU_BRIDGE_CONDITION")
      if unlockDesc then
        unlockDesc = string.format(unlockDesc, adjutant.adjutant_spell[idx].level)
      else
        unlockDesc = "unlock Lv "
      end
      DebugOut(unlockDesc)
      flashObj:InvokeASCallback("_root", "UpdateSkillListItem", itemId, adjutant.adjutant_spell[idx].state ~= 1, skillName, unlockDesc, 1, skillItem)
    end
  end
end
function GameUIAdjutant:SetAdjutantInfo(adjutantIdx)
  local flashObj = self:GetFlashObject()
  local adjutant = GameUIAdjutant:GetHeroById(GameUIAdjutant.mCurAdjutantList[adjutantIdx])
  if flashObj and adjutant then
    local force = adjutant.force
    local vessleType = GameDataAccessHelper:GetCommanderVesselsType(adjutant.identity)
    local fleetName = GameDataAccessHelper:GetFleetLevelDisplayName(adjutant.identity, adjutant.level)
    local sexx = GameDataAccessHelper:GetCommanderSex(adjutant.identity)
    if adjutant.identity == 1 then
      vessleType = GameDataAccessHelper:GetCommanderVesselsType(GameUIAdjutant:GetLeaderFleet())
      fleetName = GameDataAccessHelper:GetFleetLevelDisplayName(GameUIAdjutant:GetLeaderFleet(), adjutant.level)
      sexx = GameDataAccessHelper:GetCommanderSex(GameUIAdjutant:GetLeaderFleet())
    end
    if sexx == 1 then
      sexx = "man"
    elseif sexx == 0 then
      sexx = "woman"
    else
      sexx = "unknown"
    end
    local adjutantDetailInfo = {}
    if adjutant.identity ~= 1 then
      adjutantDetailInfo = {
        color = GameDataAccessHelper:GetCommanderColorFrame(adjutant.identity, adjutant.level),
        avatarFr = GameDataAccessHelper:GetFleetAvatar(adjutant.identity, adjutant.level),
        vessleType = vessleType,
        commanderName = fleetName,
        vessleName = GameLoader:GetGameText("LC_FLEET_" .. vessleType),
        force = tostring(force),
        forceTitle = GameLoader:GetGameText("LC_MENU_FORCE_CHAR"),
        sex = sexx
      }
    else
      adjutantDetailInfo = {
        color = GameDataAccessHelper:GetCommanderColorFrame(GameUIAdjutant:GetLeaderFleet(), adjutant.level),
        avatarFr = GameDataAccessHelper:GetFleetAvatar(GameUIAdjutant:GetLeaderFleet(), adjutant.level),
        vessleType = vessleType,
        commanderName = fleetName,
        vessleName = GameLoader:GetGameText("LC_FLEET_" .. vessleType),
        force = tostring(force),
        forceTitle = GameLoader:GetGameText("LC_MENU_FORCE_CHAR"),
        sex = sexx
      }
    end
    local statusFr, matrixName = GameUIAdjutant:GetAdjutantStatusFr(adjutant.identity)
    local equiped = true
    local titleStr = GameLoader:GetGameText("LC_MENU_BRIDGE_PLAYER_ADJUTANT")
    local desc = GameLoader:GetGameText("LC_MENU_BRIDGE_INFO_1")
    local dismissInfoVisible = false
    if "off" == statusFr then
      equiped = false
    elseif "chose" == statusFr then
      equiped = true
    elseif "on" == statusFr then
      equiped = true
      local majorId = GameUIAdjutant:GetMajorId(adjutant.identity)
      local majorName = GameDataAccessHelper:GetFleetLevelDisplayName(majorId)
      titleStr = string.gsub(titleStr, "<player_name>", majorName)
      local curHero = GameUIAdjutant.mHeroList[GameUIAdjutant.mCurSelectedHeroIdx]
      local curHeroName = GameDataAccessHelper:GetFleetLevelDisplayName(curHero.identity)
      local adjutantName = GameDataAccessHelper:GetFleetLevelDisplayName(adjutant.identity)
      desc = string.gsub(desc, "<npc1_name>", majorName)
      desc = string.gsub(desc, "<npc2_name>", adjutantName)
      desc = string.gsub(desc, "<npc3_name>", curHeroName)
      dismissInfoVisible = true
    elseif "dis" == statusFr then
      equiped = false
    end
    if dismissInfoVisible or "dis" == statusFr then
      flashObj:InvokeASCallback("_root", "SetSkillListBoxVisible", false)
      DebugOut("desc " .. desc)
      flashObj:InvokeASCallback("_root", "SetDissmissAdjutantInfo", titleStr, desc)
    else
      flashObj:InvokeASCallback("_root", "SetSkillListBoxVisible", true)
      flashObj:InvokeASCallback("_root", "SetSkillList", #adjutant.adjutant_spell + 1)
    end
    flashObj:InvokeASCallback("_root", "SetAdjutantInfoVisible", true)
    flashObj:InvokeASCallback("_root", "SetAdjutantInfo", adjutantDetailInfo)
    if "dis" == statusFr then
      local disTxt = string.format(GameLoader:GetGameText("LC_ALERT_TEAMLEAGUE_adjutant_on_matrix"), matrixName)
      flashObj:InvokeASCallback("_root", "SetNoAdjutantBoxInfo", disTxt)
      flashObj:InvokeASCallback("_root", "SetNoAdjutantBoxVisible", true)
    else
      flashObj:InvokeASCallback("_root", "SetNoAdjutantBoxVisible", false)
    end
    flashObj:InvokeASCallback("_root", "SetAdjutantEquipStatus", equiped)
    flashObj:InvokeASCallback("_root", "SetDissmissAdjutantBoxVisible", dismissInfoVisible)
  end
end
function GameUIAdjutant:ShowCommanderDetail()
  local fleetId = 1
  local fleetLv = 0
  if nil ~= GameUIAdjutant.mCurAdjutantList and 0 < #GameUIAdjutant.mCurAdjutantList and 0 < GameUIAdjutant.mCurSelectAdjutantIdx and GameUIAdjutant.mCurSelectAdjutantIdx <= #GameUIAdjutant.mCurAdjutantList then
    fleetId = GameUIAdjutant.mCurAdjutantList[GameUIAdjutant.mCurSelectAdjutantIdx]
  else
    fleetId = GameUIAdjutant.mHeroList[GameUIAdjutant.mCurSelectedHeroIdx].identity
  end
  local hero = GameUIAdjutant:GetHeroById(fleetId)
  if hero then
    fleetLv = hero.level
  end
  ItemBox:ShowCommanderDetail2(fleetId, nil, fleetLv)
end
function GameUIAdjutant:EquipAdjutant()
  local status = GameUIAdjutant:GetAdjutantStatusFr(GameUIAdjutant.mCurAdjutantList[GameUIAdjutant.mCurSelectAdjutantIdx])
  if "dis" == status then
    GameTip:Show("This adjutant is on battle.", 3000)
  else
    local major = GameUIAdjutant.mHeroList[GameUIAdjutant.mCurSelectedHeroIdx].identity
    local adjutant = GameUIAdjutant.mCurAdjutantList[GameUIAdjutant.mCurSelectAdjutantIdx]
    local adjutantIdx = GameUIAdjutant:GetHeroIdxByFleetId(adjutant)
    GameUIAdjutant.mCurRequestMajorIdx = GameUIAdjutant.mCurSelectedHeroIdx
    GameUIAdjutant.mCurRequestAdjutantIdxInHeroList = adjutantIdx
    local oldAdjutantId = GameUIAdjutant.mHeroList[GameUIAdjutant.mCurSelectedHeroIdx].cur_adjutant
    for k = 1, #GameUIAdjutant.mCurAdjutantList do
      if oldAdjutantId == GameUIAdjutant.mCurAdjutantList[k] then
        GameUIAdjutant.mCurRequestAdjutantIdxInAdjutantList = k
      end
    end
    GameUIAdjutant:RequestAddAdjutant(major, adjutant)
  end
end
function GameUIAdjutant:ReleaseAdjutant()
  local herosMajorId = GameUIAdjutant:GetMajorId(GameUIAdjutant.mHeroList[GameUIAdjutant.mCurSelectedHeroIdx].identity)
  local adjutant = GameUIAdjutant.mCurAdjutantList[GameUIAdjutant.mCurSelectAdjutantIdx]
  GameUIAdjutant.mIsReleaseHeroSelf = false
  if herosMajorId > 0 then
    adjutant = GameUIAdjutant.mHeroList[GameUIAdjutant.mCurSelectedHeroIdx].identity
    GameUIAdjutant.mIsReleaseHeroSelf = true
  end
  DebugOut(" GameUIAdjutant.mIsReleaseHeroSelf " .. tostring(GameUIAdjutant.mIsReleaseHeroSelf))
  local major = GameUIAdjutant:GetMajorId(adjutant)
  if major > 0 then
    GameUIAdjutant.mCurRequestMajorIdx = GameUIAdjutant:GetHeroIdxByFleetId(major)
    GameUIAdjutant.mCurRequestAdjutantIdxInHeroList = GameUIAdjutant:GetHeroIdxByFleetId(adjutant)
    GameUIAdjutant:RequestReleaseAdjutant(major)
  end
end
function GameUIAdjutant:RequestAddAdjutant(major, adjutant)
  DebugOut("RequestAddAdjutant " .. tostring(major) .. " " .. tostring(adjutant))
  if GameUIAdjutant.teamLeagueType == 0 then
    local req = {major = major, adjutant = adjutant}
    NetMessageMgr:SendMsg(NetAPIList.add_adjutant_req.Code, req, self.RequestAddAdjutantCallback, true, nil)
  elseif GameUIAdjutant.teamLeagueType == 1 then
    local req = {
      matrix_type = 1,
      major = major,
      adjutant = adjutant
    }
    NetMessageMgr:SendMsg(NetAPIList.add_tlc_adjutant_req.Code, req, self.RequestAddTlcAdjutantCallback, true, nil)
  elseif GameUIAdjutant.teamLeagueType == 2 then
    local req = {
      matrix_type = 2,
      major = major,
      adjutant = adjutant
    }
    DebugOut("send msg GameUIAdjutant.teamLeagueType")
    NetMessageMgr:SendMsg(NetAPIList.add_tlc_adjutant_req.Code, req, self.RequestAddTlcAdjutantCallback, true, nil)
  end
end
function GameUIAdjutant.RequestAddAdjutantCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.add_adjutant_req.Code then
    if 0 == content.code then
      GameUIAdjutant:RefreshCurMajorData()
      GameUIAdjutant:UpdateHeroListItem(GameUIAdjutant.mCurSelectedHeroIdx)
      GameUIAdjutant:UpdateHeroListItem(GameUIAdjutant.mCurRequestMajorIdx)
      GameUIAdjutant:UpdateHeroListItem(GameUIAdjutant.mCurRequestAdjutantIdxInHeroList)
      local oldItemId = math.ceil(GameUIAdjutant.mCurRequestAdjutantIdxInAdjutantList / 2)
      local itemId = math.ceil(GameUIAdjutant.mCurSelectAdjutantIdx / 2)
      GameUIAdjutant:UpdateAdjutantListItem(oldItemId)
      GameUIAdjutant:UpdateAdjutantListItem(itemId)
      GameUIAdjutant:SetAdjutantInfo(GameUIAdjutant.mCurSelectAdjutantIdx)
    else
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  return false
end
function GameUIAdjutant.RequestAddTlcAdjutantCallback(msgType, content)
  DebugOut("RequestAddTlcAdjutantCallback === " .. msgType)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.add_tlc_adjutant_req.Code then
    if 0 == content.code then
      GameUIAdjutant:RefreshCurMajorData()
      GameUIAdjutant:UpdateHeroListItem(GameUIAdjutant.mCurSelectedHeroIdx)
      GameUIAdjutant:UpdateHeroListItem(GameUIAdjutant.mCurRequestMajorIdx)
      GameUIAdjutant:UpdateHeroListItem(GameUIAdjutant.mCurRequestAdjutantIdxInHeroList)
      local oldItemId = math.ceil(GameUIAdjutant.mCurRequestAdjutantIdxInAdjutantList / 2)
      local itemId = math.ceil(GameUIAdjutant.mCurSelectAdjutantIdx / 2)
      GameUIAdjutant:UpdateAdjutantListItem(oldItemId)
      GameUIAdjutant:UpdateAdjutantListItem(itemId)
      GameUIAdjutant:SetAdjutantInfo(GameUIAdjutant.mCurSelectAdjutantIdx)
    else
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  return false
end
function GameUIAdjutant:RequestReleaseAdjutant(major)
  DebugOut("RequestReleaseAdjutant " .. tostring(major))
  local req = {major = major}
  NetMessageMgr:SendMsg(NetAPIList.release_adjutant_req.Code, req, self.RequestReleaseAdjutantCallback, true, nil)
end
function GameUIAdjutant.RequestReleaseAdjutantCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.release_adjutant_req.Code then
    if 0 == content.code then
      GameUIAdjutant:RefreshCurMajorData()
      GameUIAdjutant:UpdateHeroListItem(GameUIAdjutant.mCurSelectedHeroIdx)
      GameUIAdjutant:UpdateHeroListItem(GameUIAdjutant.mCurRequestMajorIdx)
      GameUIAdjutant:UpdateHeroListItem(GameUIAdjutant.mCurRequestAdjutantIdxInHeroList)
      local itemId = math.ceil(GameUIAdjutant.mCurSelectAdjutantIdx / 2)
      GameUIAdjutant:UpdateAdjutantListItem(itemId)
      DebugOut(" GameUIAdjutant.mIsReleaseHeroSelf " .. tostring(GameUIAdjutant.mIsReleaseHeroSelf))
      if GameUIAdjutant.mIsReleaseHeroSelf then
        local adjutantList = GameUIAdjutant.mCurAdjutantList
        GameUIAdjutant:SetAdjutantList(adjutantList)
      end
      GameUIAdjutant:SetAdjutantInfo(GameUIAdjutant.mCurSelectAdjutantIdx)
    else
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  return false
end
function GameUIAdjutant:RefreshCurMajorData()
  if GameUIAdjutant.mCurRequestMajorIdx > 0 then
    local fleetId = GameUIAdjutant.mHeroList[GameUIAdjutant.mCurRequestMajorIdx].identity
    GameUIAdjutant.mHeroList[GameUIAdjutant.mCurRequestMajorIdx] = GameGlobalData:GetFleetInfo(fleetId)
  end
end
function GameUIAdjutant:RequestDismissHeroList()
  DebugOut("RequestDismissHeroList ")
  NetMessageMgr:SendMsg(NetAPIList.fleet_dismiss_req.Code, nil, self.RequestDismissHeroListCallback, true, nil)
end
function GameUIAdjutant.RequestDismissHeroListCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.fleet_dismiss_req.Code then
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.fleet_dismiss_ack.Code then
    local list = GameGlobalData:GetData("fleetinfo").fleets
    local heroList = {}
    for k = 1, #list do
      table.insert(heroList, list[k])
    end
    DebugOutPutTable(heroList, "RequestDismissHeroListCallback =")
    if content.fleets and #content.fleets > 0 then
      for k = 1, #content.fleets do
        local isRepeat = false
        for index = 1, #list do
          if content.fleets[k].identity == list[index].identity then
            isRepeat = true
            break
          end
        end
        if not isRepeat then
          table.insert(heroList, content.fleets[k])
        end
      end
    end
    GameUIAdjutant:Init(heroList)
    return true
  end
  return false
end
if AutoUpdate.isAndroidDevice then
  function GameUIAdjutant.OnAndroidBack()
    GameUIAdjutant:OnFSCommand("close")
  end
end
