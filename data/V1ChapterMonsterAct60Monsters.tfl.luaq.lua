local Monsters = GameData.ChapterMonsterAct60.Monsters
Monsters[6001011] = {
  ID = 6001011,
  durability = 140,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001012] = {
  ID = 6001012,
  durability = 80,
  Avatar = "head13",
  Ship = "ship5"
}
Monsters[6001013] = {
  ID = 6001013,
  durability = 80,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[6001014] = {
  ID = 6001014,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001031] = {
  ID = 6001031,
  durability = 188,
  Avatar = "head13",
  Ship = "ship47"
}
Monsters[6001032] = {
  ID = 6001032,
  durability = 120,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001033] = {
  ID = 6001033,
  durability = 80,
  Avatar = "head24",
  Ship = "ship39"
}
Monsters[6001034] = {
  ID = 6001034,
  durability = 80,
  Avatar = "head24",
  Ship = "ship39"
}
Monsters[6001051] = {
  ID = 6001051,
  durability = 140,
  Avatar = "head24",
  Ship = "ship47"
}
Monsters[6001052] = {
  ID = 6001052,
  durability = 140,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001053] = {
  ID = 6001053,
  durability = 80,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[6001054] = {
  ID = 6001054,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001055] = {
  ID = 6001055,
  durability = 80,
  Avatar = "head25",
  Ship = "ship39"
}
Monsters[6001056] = {
  ID = 6001056,
  durability = 80,
  Avatar = "head25",
  Ship = "ship39"
}
Monsters[6001061] = {
  ID = 6001061,
  durability = 262,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001062] = {
  ID = 6001062,
  durability = 140,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001063] = {
  ID = 6001063,
  durability = 80,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[6001064] = {
  ID = 6001064,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001065] = {
  ID = 6001065,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001066] = {
  ID = 6001066,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001071] = {
  ID = 6001071,
  durability = 80,
  Avatar = "head24",
  Ship = "ship39"
}
Monsters[6001072] = {
  ID = 6001072,
  durability = 80,
  Avatar = "head24",
  Ship = "ship39"
}
Monsters[6001073] = {
  ID = 6001073,
  durability = 80,
  Avatar = "head24",
  Ship = "ship39"
}
Monsters[6001074] = {
  ID = 6001074,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001075] = {
  ID = 6001075,
  durability = 80,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[6001076] = {
  ID = 6001076,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001081] = {
  ID = 6001081,
  durability = 382,
  Avatar = "head25",
  Ship = "ship39"
}
Monsters[6001082] = {
  ID = 6001082,
  durability = 160,
  Avatar = "head25",
  Ship = "ship39"
}
Monsters[6001083] = {
  ID = 6001083,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001084] = {
  ID = 6001084,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001085] = {
  ID = 6001085,
  durability = 80,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[6001086] = {
  ID = 6001086,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001091] = {
  ID = 6001091,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001092] = {
  ID = 6001092,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001093] = {
  ID = 6001093,
  durability = 80,
  Avatar = "head24",
  Ship = "ship39"
}
Monsters[6001094] = {
  ID = 6001094,
  durability = 80,
  Avatar = "head24",
  Ship = "ship39"
}
Monsters[6001095] = {
  ID = 6001095,
  durability = 80,
  Avatar = "head24",
  Ship = "ship39"
}
Monsters[6001096] = {
  ID = 6001096,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001111] = {
  ID = 6001111,
  durability = 80,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[6001112] = {
  ID = 6001112,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001113] = {
  ID = 6001113,
  durability = 80,
  Avatar = "head25",
  Ship = "ship39"
}
Monsters[6001114] = {
  ID = 6001114,
  durability = 80,
  Avatar = "head25",
  Ship = "ship39"
}
Monsters[6001115] = {
  ID = 6001115,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001116] = {
  ID = 6001116,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001121] = {
  ID = 6001121,
  durability = 80,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[6001122] = {
  ID = 6001122,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001123] = {
  ID = 6001123,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001124] = {
  ID = 6001124,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001125] = {
  ID = 6001125,
  durability = 80,
  Avatar = "head24",
  Ship = "ship39"
}
Monsters[6001126] = {
  ID = 6001126,
  durability = 80,
  Avatar = "head24",
  Ship = "ship39"
}
Monsters[6001127] = {
  ID = 6001127,
  durability = 80,
  Avatar = "head24",
  Ship = "ship39"
}
Monsters[6001128] = {
  ID = 6001128,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001129] = {
  ID = 6001129,
  durability = 80,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[6001130] = {
  ID = 6001130,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001131] = {
  ID = 6001131,
  durability = 80,
  Avatar = "head25",
  Ship = "ship39"
}
Monsters[6001132] = {
  ID = 6001132,
  durability = 80,
  Avatar = "head25",
  Ship = "ship39"
}
Monsters[6001133] = {
  ID = 6001133,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001134] = {
  ID = 6001134,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001135] = {
  ID = 6001135,
  durability = 80,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[6001136] = {
  ID = 6001136,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001137] = {
  ID = 6001137,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001138] = {
  ID = 6001138,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001139] = {
  ID = 6001139,
  durability = 80,
  Avatar = "head24",
  Ship = "ship39"
}
Monsters[6001140] = {
  ID = 6001140,
  durability = 80,
  Avatar = "head24",
  Ship = "ship39"
}
Monsters[6001141] = {
  ID = 6001141,
  durability = 80,
  Avatar = "head24",
  Ship = "ship39"
}
Monsters[6001142] = {
  ID = 6001142,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001143] = {
  ID = 6001143,
  durability = 80,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[6001144] = {
  ID = 6001144,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001145] = {
  ID = 6001145,
  durability = 80,
  Avatar = "head25",
  Ship = "ship39"
}
Monsters[6001146] = {
  ID = 6001146,
  durability = 80,
  Avatar = "head25",
  Ship = "ship39"
}
Monsters[6001147] = {
  ID = 6001147,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001148] = {
  ID = 6001148,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001149] = {
  ID = 6001149,
  durability = 80,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[6001150] = {
  ID = 6001150,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001151] = {
  ID = 6001151,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001152] = {
  ID = 6001152,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001153] = {
  ID = 6001153,
  durability = 80,
  Avatar = "head24",
  Ship = "ship39"
}
Monsters[6001154] = {
  ID = 6001154,
  durability = 80,
  Avatar = "head24",
  Ship = "ship39"
}
Monsters[6001155] = {
  ID = 6001155,
  durability = 80,
  Avatar = "head24",
  Ship = "ship39"
}
Monsters[6001156] = {
  ID = 6001156,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001157] = {
  ID = 6001157,
  durability = 80,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[6001158] = {
  ID = 6001158,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001159] = {
  ID = 6001159,
  durability = 80,
  Avatar = "head25",
  Ship = "ship39"
}
Monsters[6001160] = {
  ID = 6001160,
  durability = 80,
  Avatar = "head25",
  Ship = "ship39"
}
Monsters[6001161] = {
  ID = 6001161,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001162] = {
  ID = 6001162,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001163] = {
  ID = 6001163,
  durability = 80,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[6001164] = {
  ID = 6001164,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001165] = {
  ID = 6001165,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001166] = {
  ID = 6001166,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001167] = {
  ID = 6001167,
  durability = 80,
  Avatar = "head24",
  Ship = "ship39"
}
Monsters[6001168] = {
  ID = 6001168,
  durability = 80,
  Avatar = "head24",
  Ship = "ship39"
}
Monsters[6001169] = {
  ID = 6001169,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001170] = {
  ID = 6001170,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001171] = {
  ID = 6001171,
  durability = 80,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[6001172] = {
  ID = 6001172,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001173] = {
  ID = 6001173,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001174] = {
  ID = 6001174,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001175] = {
  ID = 6001175,
  durability = 80,
  Avatar = "head24",
  Ship = "ship39"
}
Monsters[6001176] = {
  ID = 6001176,
  durability = 80,
  Avatar = "head24",
  Ship = "ship39"
}
Monsters[6001177] = {
  ID = 6001177,
  durability = 80,
  Avatar = "head24",
  Ship = "ship39"
}
Monsters[6001178] = {
  ID = 6001178,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001179] = {
  ID = 6001179,
  durability = 80,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[6001180] = {
  ID = 6001180,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001181] = {
  ID = 6001181,
  durability = 80,
  Avatar = "head25",
  Ship = "ship39"
}
Monsters[6001182] = {
  ID = 6001182,
  durability = 80,
  Avatar = "head25",
  Ship = "ship39"
}
Monsters[6001183] = {
  ID = 6001183,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001184] = {
  ID = 6001184,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001185] = {
  ID = 6001185,
  durability = 80,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[6001186] = {
  ID = 6001186,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001187] = {
  ID = 6001187,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001188] = {
  ID = 6001188,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001189] = {
  ID = 6001189,
  durability = 80,
  Avatar = "head24",
  Ship = "ship39"
}
Monsters[6001190] = {
  ID = 6001190,
  durability = 80,
  Avatar = "head24",
  Ship = "ship39"
}
Monsters[6001191] = {
  ID = 6001191,
  durability = 80,
  Avatar = "head24",
  Ship = "ship39"
}
Monsters[6001192] = {
  ID = 6001192,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001193] = {
  ID = 6001193,
  durability = 80,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[6001194] = {
  ID = 6001194,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001195] = {
  ID = 6001195,
  durability = 80,
  Avatar = "head25",
  Ship = "ship39"
}
Monsters[6001196] = {
  ID = 6001196,
  durability = 80,
  Avatar = "head25",
  Ship = "ship39"
}
Monsters[6001197] = {
  ID = 6001197,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001198] = {
  ID = 6001198,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001199] = {
  ID = 6001199,
  durability = 80,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[6001200] = {
  ID = 6001200,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001201] = {
  ID = 6001201,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001202] = {
  ID = 6001202,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001203] = {
  ID = 6001203,
  durability = 80,
  Avatar = "head24",
  Ship = "ship39"
}
Monsters[6001204] = {
  ID = 6001204,
  durability = 80,
  Avatar = "head24",
  Ship = "ship39"
}
Monsters[6001205] = {
  ID = 6001205,
  durability = 80,
  Avatar = "head24",
  Ship = "ship39"
}
Monsters[6001206] = {
  ID = 6001206,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001207] = {
  ID = 6001207,
  durability = 80,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[6001208] = {
  ID = 6001208,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001209] = {
  ID = 6001209,
  durability = 80,
  Avatar = "head25",
  Ship = "ship39"
}
Monsters[6001210] = {
  ID = 6001210,
  durability = 80,
  Avatar = "head25",
  Ship = "ship39"
}
Monsters[6001211] = {
  ID = 6001211,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001212] = {
  ID = 6001212,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001213] = {
  ID = 6001213,
  durability = 80,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[6001214] = {
  ID = 6001214,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001215] = {
  ID = 6001215,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001216] = {
  ID = 6001216,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001217] = {
  ID = 6001217,
  durability = 80,
  Avatar = "head24",
  Ship = "ship39"
}
Monsters[6001218] = {
  ID = 6001218,
  durability = 80,
  Avatar = "head24",
  Ship = "ship39"
}
Monsters[6001219] = {
  ID = 6001219,
  durability = 80,
  Avatar = "head24",
  Ship = "ship39"
}
Monsters[6001220] = {
  ID = 6001220,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001221] = {
  ID = 6001221,
  durability = 80,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[6001222] = {
  ID = 6001222,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001223] = {
  ID = 6001223,
  durability = 80,
  Avatar = "head25",
  Ship = "ship39"
}
Monsters[6001224] = {
  ID = 6001224,
  durability = 80,
  Avatar = "head25",
  Ship = "ship39"
}
Monsters[6001225] = {
  ID = 6001225,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001226] = {
  ID = 6001226,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001227] = {
  ID = 6001227,
  durability = 80,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[6001228] = {
  ID = 6001228,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001229] = {
  ID = 6001229,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001230] = {
  ID = 6001230,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001231] = {
  ID = 6001231,
  durability = 80,
  Avatar = "head24",
  Ship = "ship39"
}
Monsters[6001232] = {
  ID = 6001232,
  durability = 80,
  Avatar = "head24",
  Ship = "ship39"
}
Monsters[6001233] = {
  ID = 6001233,
  durability = 80,
  Avatar = "head24",
  Ship = "ship39"
}
Monsters[6001234] = {
  ID = 6001234,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001235] = {
  ID = 6001235,
  durability = 80,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[6001236] = {
  ID = 6001236,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001237] = {
  ID = 6001237,
  durability = 80,
  Avatar = "head25",
  Ship = "ship39"
}
Monsters[6001238] = {
  ID = 6001238,
  durability = 80,
  Avatar = "head25",
  Ship = "ship39"
}
Monsters[6001239] = {
  ID = 6001239,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001240] = {
  ID = 6001240,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001241] = {
  ID = 6001241,
  durability = 80,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[6001242] = {
  ID = 6001242,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001243] = {
  ID = 6001243,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001244] = {
  ID = 6001244,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001245] = {
  ID = 6001245,
  durability = 80,
  Avatar = "head24",
  Ship = "ship39"
}
Monsters[6001246] = {
  ID = 6001246,
  durability = 80,
  Avatar = "head24",
  Ship = "ship39"
}
Monsters[6001247] = {
  ID = 6001247,
  durability = 80,
  Avatar = "head24",
  Ship = "ship39"
}
Monsters[6001248] = {
  ID = 6001248,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001249] = {
  ID = 6001249,
  durability = 80,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[6001250] = {
  ID = 6001250,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001251] = {
  ID = 6001251,
  durability = 80,
  Avatar = "head25",
  Ship = "ship39"
}
Monsters[6001252] = {
  ID = 6001252,
  durability = 80,
  Avatar = "head25",
  Ship = "ship39"
}
Monsters[6001253] = {
  ID = 6001253,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001254] = {
  ID = 6001254,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001255] = {
  ID = 6001255,
  durability = 80,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[6001256] = {
  ID = 6001256,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001257] = {
  ID = 6001257,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001258] = {
  ID = 6001258,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001259] = {
  ID = 6001259,
  durability = 80,
  Avatar = "head24",
  Ship = "ship39"
}
Monsters[6001260] = {
  ID = 6001260,
  durability = 80,
  Avatar = "head24",
  Ship = "ship39"
}
Monsters[6001261] = {
  ID = 6001261,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001262] = {
  ID = 6001262,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001263] = {
  ID = 6001263,
  durability = 80,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[6001264] = {
  ID = 6001264,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001265] = {
  ID = 6001265,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001266] = {
  ID = 6001266,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001267] = {
  ID = 6001267,
  durability = 80,
  Avatar = "head24",
  Ship = "ship39"
}
Monsters[6001268] = {
  ID = 6001268,
  durability = 80,
  Avatar = "head24",
  Ship = "ship39"
}
Monsters[6001269] = {
  ID = 6001269,
  durability = 80,
  Avatar = "head24",
  Ship = "ship39"
}
Monsters[6001270] = {
  ID = 6001270,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001271] = {
  ID = 6001271,
  durability = 80,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[6001272] = {
  ID = 6001272,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001273] = {
  ID = 6001273,
  durability = 80,
  Avatar = "head25",
  Ship = "ship39"
}
Monsters[6001274] = {
  ID = 6001274,
  durability = 80,
  Avatar = "head25",
  Ship = "ship39"
}
Monsters[6001275] = {
  ID = 6001275,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001276] = {
  ID = 6001276,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001277] = {
  ID = 6001277,
  durability = 80,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[6001278] = {
  ID = 6001278,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001279] = {
  ID = 6001279,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001280] = {
  ID = 6001280,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001281] = {
  ID = 6001281,
  durability = 80,
  Avatar = "head24",
  Ship = "ship39"
}
Monsters[6001282] = {
  ID = 6001282,
  durability = 80,
  Avatar = "head24",
  Ship = "ship39"
}
Monsters[6001283] = {
  ID = 6001283,
  durability = 80,
  Avatar = "head24",
  Ship = "ship39"
}
Monsters[6001284] = {
  ID = 6001284,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001285] = {
  ID = 6001285,
  durability = 80,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[6001286] = {
  ID = 6001286,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001287] = {
  ID = 6001287,
  durability = 80,
  Avatar = "head25",
  Ship = "ship39"
}
Monsters[6001288] = {
  ID = 6001288,
  durability = 80,
  Avatar = "head25",
  Ship = "ship39"
}
Monsters[6001289] = {
  ID = 6001289,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001290] = {
  ID = 6001290,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001291] = {
  ID = 6001291,
  durability = 80,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[6001292] = {
  ID = 6001292,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001293] = {
  ID = 6001293,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001294] = {
  ID = 6001294,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001295] = {
  ID = 6001295,
  durability = 80,
  Avatar = "head24",
  Ship = "ship39"
}
Monsters[6001296] = {
  ID = 6001296,
  durability = 80,
  Avatar = "head24",
  Ship = "ship39"
}
Monsters[6001297] = {
  ID = 6001297,
  durability = 80,
  Avatar = "head24",
  Ship = "ship39"
}
Monsters[6001298] = {
  ID = 6001298,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001299] = {
  ID = 6001299,
  durability = 80,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[6001300] = {
  ID = 6001300,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001301] = {
  ID = 6001301,
  durability = 80,
  Avatar = "head25",
  Ship = "ship39"
}
Monsters[6001302] = {
  ID = 6001302,
  durability = 80,
  Avatar = "head25",
  Ship = "ship39"
}
Monsters[6001303] = {
  ID = 6001303,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001304] = {
  ID = 6001304,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001305] = {
  ID = 6001305,
  durability = 80,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[6001306] = {
  ID = 6001306,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001307] = {
  ID = 6001307,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001308] = {
  ID = 6001308,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001309] = {
  ID = 6001309,
  durability = 80,
  Avatar = "head24",
  Ship = "ship39"
}
Monsters[6001310] = {
  ID = 6001310,
  durability = 80,
  Avatar = "head24",
  Ship = "ship39"
}
Monsters[6001311] = {
  ID = 6001311,
  durability = 80,
  Avatar = "head24",
  Ship = "ship39"
}
Monsters[6001312] = {
  ID = 6001312,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001313] = {
  ID = 6001313,
  durability = 80,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[6001314] = {
  ID = 6001314,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001315] = {
  ID = 6001315,
  durability = 80,
  Avatar = "head25",
  Ship = "ship39"
}
Monsters[6001316] = {
  ID = 6001316,
  durability = 80,
  Avatar = "head25",
  Ship = "ship39"
}
Monsters[6001317] = {
  ID = 6001317,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001318] = {
  ID = 6001318,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001319] = {
  ID = 6001319,
  durability = 80,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[6001320] = {
  ID = 6001320,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001321] = {
  ID = 6001321,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001322] = {
  ID = 6001322,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001323] = {
  ID = 6001323,
  durability = 80,
  Avatar = "head24",
  Ship = "ship39"
}
Monsters[6001324] = {
  ID = 6001324,
  durability = 80,
  Avatar = "head24",
  Ship = "ship39"
}
Monsters[6001325] = {
  ID = 6001325,
  durability = 80,
  Avatar = "head24",
  Ship = "ship39"
}
Monsters[6001326] = {
  ID = 6001326,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001327] = {
  ID = 6001327,
  durability = 80,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[6001328] = {
  ID = 6001328,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001329] = {
  ID = 6001329,
  durability = 80,
  Avatar = "head25",
  Ship = "ship39"
}
Monsters[6001330] = {
  ID = 6001330,
  durability = 80,
  Avatar = "head25",
  Ship = "ship39"
}
Monsters[6001331] = {
  ID = 6001331,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001332] = {
  ID = 6001332,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001333] = {
  ID = 6001333,
  durability = 80,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[6001334] = {
  ID = 6001334,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001335] = {
  ID = 6001335,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001336] = {
  ID = 6001336,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001337] = {
  ID = 6001337,
  durability = 80,
  Avatar = "head24",
  Ship = "ship39"
}
Monsters[6001338] = {
  ID = 6001338,
  durability = 80,
  Avatar = "head24",
  Ship = "ship39"
}
Monsters[6001339] = {
  ID = 6001339,
  durability = 80,
  Avatar = "head24",
  Ship = "ship39"
}
Monsters[6001340] = {
  ID = 6001340,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001341] = {
  ID = 6001341,
  durability = 80,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[6001342] = {
  ID = 6001342,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001343] = {
  ID = 6001343,
  durability = 80,
  Avatar = "head25",
  Ship = "ship39"
}
Monsters[6001344] = {
  ID = 6001344,
  durability = 80,
  Avatar = "head25",
  Ship = "ship39"
}
Monsters[6001345] = {
  ID = 6001345,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001346] = {
  ID = 6001346,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001347] = {
  ID = 6001347,
  durability = 80,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[6001348] = {
  ID = 6001348,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001349] = {
  ID = 6001349,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001350] = {
  ID = 6001350,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001351] = {
  ID = 6001351,
  durability = 80,
  Avatar = "head24",
  Ship = "ship39"
}
Monsters[6001352] = {
  ID = 6001352,
  durability = 80,
  Avatar = "head24",
  Ship = "ship39"
}
Monsters[6001353] = {
  ID = 6001353,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001354] = {
  ID = 6001354,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001355] = {
  ID = 6001355,
  durability = 80,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[6001356] = {
  ID = 6001356,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001357] = {
  ID = 6001357,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001358] = {
  ID = 6001358,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001359] = {
  ID = 6001359,
  durability = 80,
  Avatar = "head24",
  Ship = "ship39"
}
Monsters[6001360] = {
  ID = 6001360,
  durability = 80,
  Avatar = "head24",
  Ship = "ship39"
}
Monsters[6001361] = {
  ID = 6001361,
  durability = 80,
  Avatar = "head24",
  Ship = "ship39"
}
Monsters[6001362] = {
  ID = 6001362,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001363] = {
  ID = 6001363,
  durability = 80,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[6001364] = {
  ID = 6001364,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001365] = {
  ID = 6001365,
  durability = 80,
  Avatar = "head25",
  Ship = "ship39"
}
Monsters[6001366] = {
  ID = 6001366,
  durability = 80,
  Avatar = "head25",
  Ship = "ship39"
}
Monsters[6001367] = {
  ID = 6001367,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001368] = {
  ID = 6001368,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001369] = {
  ID = 6001369,
  durability = 80,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[6001370] = {
  ID = 6001370,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001371] = {
  ID = 6001371,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001372] = {
  ID = 6001372,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001373] = {
  ID = 6001373,
  durability = 80,
  Avatar = "head24",
  Ship = "ship39"
}
Monsters[6001374] = {
  ID = 6001374,
  durability = 80,
  Avatar = "head24",
  Ship = "ship39"
}
Monsters[6001375] = {
  ID = 6001375,
  durability = 80,
  Avatar = "head24",
  Ship = "ship39"
}
Monsters[6001376] = {
  ID = 6001376,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001377] = {
  ID = 6001377,
  durability = 80,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[6001378] = {
  ID = 6001378,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001379] = {
  ID = 6001379,
  durability = 80,
  Avatar = "head25",
  Ship = "ship39"
}
Monsters[6001380] = {
  ID = 6001380,
  durability = 80,
  Avatar = "head25",
  Ship = "ship39"
}
Monsters[6001381] = {
  ID = 6001381,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001382] = {
  ID = 6001382,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001383] = {
  ID = 6001383,
  durability = 80,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[6001384] = {
  ID = 6001384,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001385] = {
  ID = 6001385,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001386] = {
  ID = 6001386,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001387] = {
  ID = 6001387,
  durability = 80,
  Avatar = "head24",
  Ship = "ship39"
}
Monsters[6001388] = {
  ID = 6001388,
  durability = 80,
  Avatar = "head24",
  Ship = "ship39"
}
Monsters[6001389] = {
  ID = 6001389,
  durability = 80,
  Avatar = "head24",
  Ship = "ship39"
}
Monsters[6001390] = {
  ID = 6001390,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001391] = {
  ID = 6001391,
  durability = 80,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[6001392] = {
  ID = 6001392,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001393] = {
  ID = 6001393,
  durability = 80,
  Avatar = "head25",
  Ship = "ship39"
}
Monsters[6001394] = {
  ID = 6001394,
  durability = 80,
  Avatar = "head25",
  Ship = "ship39"
}
Monsters[6001395] = {
  ID = 6001395,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6001396] = {
  ID = 6001396,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6000001] = {
  ID = 6000001,
  durability = 80,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[6000201] = {
  ID = 6000201,
  durability = 80,
  Avatar = "head13",
  Ship = "ship5"
}
Monsters[6000202] = {
  ID = 6000202,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6000203] = {
  ID = 6000203,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6000301] = {
  ID = 6000301,
  durability = 80,
  Avatar = "head24",
  Ship = "ship39"
}
Monsters[6000302] = {
  ID = 6000302,
  durability = 80,
  Avatar = "head24",
  Ship = "ship47"
}
Monsters[6000303] = {
  ID = 6000303,
  durability = 80,
  Avatar = "head24",
  Ship = "ship47"
}
Monsters[6000304] = {
  ID = 6000304,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6000401] = {
  ID = 6000401,
  durability = 80,
  Avatar = "head12",
  Ship = "ship24"
}
Monsters[6000402] = {
  ID = 6000402,
  durability = 80,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[6000403] = {
  ID = 6000403,
  durability = 80,
  Avatar = "head25",
  Ship = "ship39"
}
Monsters[6000404] = {
  ID = 6000404,
  durability = 80,
  Avatar = "head25",
  Ship = "ship39"
}
Monsters[6000405] = {
  ID = 6000405,
  durability = 80,
  Avatar = "head13",
  Ship = "ship47"
}
Monsters[6000406] = {
  ID = 6000406,
  durability = 80,
  Avatar = "head13",
  Ship = "ship47"
}
Monsters[6000407] = {
  ID = 6000407,
  durability = 80,
  Avatar = "head12",
  Ship = "ship42"
}
Monsters[6000408] = {
  ID = 6000408,
  durability = 80,
  Avatar = "head13",
  Ship = "ship43"
}
Monsters[6000409] = {
  ID = 6000409,
  durability = 80,
  Avatar = "head13",
  Ship = "ship42"
}
Monsters[6010001] = {
  ID = 6010001,
  durability = 2000,
  Avatar = "head13",
  Ship = "ship5"
}
Monsters[6010002] = {
  ID = 6010002,
  durability = 1500,
  Avatar = "head24",
  Ship = "ship42"
}
Monsters[6010003] = {
  ID = 6010003,
  durability = 600,
  Avatar = "head24",
  Ship = "ship39"
}
Monsters[6010004] = {
  ID = 6010004,
  durability = 700,
  Avatar = "head24",
  Ship = "ship47"
}
Monsters[6020001] = {
  ID = 6020001,
  durability = 700,
  Avatar = "head13",
  Ship = "ship14"
}
Monsters[6020002] = {
  ID = 6020002,
  durability = 700,
  Avatar = "head36",
  Ship = "ship39"
}
Monsters[6020003] = {
  ID = 6020003,
  durability = 700,
  Avatar = "head9001",
  Ship = "ship17"
}
