local GameStateRecruit = GameStateManager.GameStateRecruit
local GameUIRecruitTopbar = LuaObjectManager:GetLuaObject("GameUIRecruitTopbar")
local GameUIRecruitMain = LuaObjectManager:GetLuaObject("GameUIRecruitMain")
local GameUICommonDialog = LuaObjectManager:GetLuaObject("GameUICommonDialog")
local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local QuestTutorialRecruit = TutorialQuestManager.QuestTutorialRecruit
local QuestTutorialStarCharge = TutorialQuestManager.QuestTutorialStarCharge
local QuestTutorialBattleMap_second = TutorialQuestManager.QuestTutorialBattleMap_second
local GameStateMainPlanet = GameStateManager.GameStateMainPlanet
local QuestTutorialBattleMapAfterRecruit = TutorialQuestManager.QuestTutorialBattleMapAfterRecruit
local GameUICollect = LuaObjectManager:GetLuaObject("GameUICollect")
local GameObjectFirstCharge = LuaObjectManager:GetLuaObject("GameObjectFirstCharge")
local CommanderRecruitState = {}
CommanderRecruitState.exist = 1
CommanderRecruitState.ready = 2
CommanderRecruitState.recruit_able = 3
CommanderRecruitState.prestige_condition = 4
CommanderRecruitState.challenge_condition = 5
local CommanderRanks = {
  [1] = "rank_E",
  [2] = "rank_D",
  [3] = "rank_C",
  [4] = "rank_B",
  [5] = "rank_A",
  [6] = "rank_S",
  [7] = "rank_SS"
}
local m_targetTime = {}
local k_filterTypeAll = "all"
local k_filterTypeElite = "elite"
local k_filterTypeNormal = "normal"
function GameUIRecruitMain:OnInitGame()
end
function GameUIRecruitMain:InitFlashObject()
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "setLocalText", "commander_status_locked", GameLoader:GetGameText("LC_MENU_COMMANDER_RECRUIT_STATUS_LOCKED"))
  flash_obj:InvokeASCallback("_root", "setLocalText", "commander_status_inteam", GameLoader:GetGameText("LC_MENU_COMMANDER_RECRUIT_STATUS_INTEAM"))
  flash_obj:InvokeASCallback("_root", "setLocalText", "commander_status_ready", GameLoader:GetGameText("LC_MENU_COMMANDER_RECRUIT_STATUS_READY"))
  flash_obj:InvokeASCallback("_root", "setLocalText", "commander_status_recruitback", GameLoader:GetGameText("LC_MENU_RECALL_BUTTON"))
  flash_obj:InvokeASCallback("_root", "onFlashObjectLoaded")
end
function GameUIRecruitMain:OnAddToGameState()
  self:LoadFlashObject()
  GameTimer:Add(self._OnTimerTick, 1000)
end
function GameUIRecruitMain:OnEraseFromGameState()
  GameUIRecruitMain:GetFlashObject():InvokeASCallback("_root", "HideTutorialRecruit")
  self:UnloadFlashObject()
end
function GetCommanderRecruitCost(data_recruit)
  local cost = data_recruit.lepton_req
  if data_recruit.quark_req ~= 0 then
    cost = data_recruit.quark_req
  end
  return cost
end
function GameUIRecruitMain:ClearLocalData()
  self._CommanderTable = nil
  self._commanderListNormal = nil
  self.CommanderTableOwned = nil
end
function GameUIRecruitMain:OnFSCommand(cmd, arg)
  local commander_type, commander_index = unpack(LuaUtils:string_split(arg, ","))
  commander_index = tonumber(commander_index)
  if cmd == "update_commander_item" then
    self:UpdateCommanderItem(commander_type, commander_index)
  elseif cmd == "select_commander" then
    self:SelectCommander(commander_type, commander_index)
  elseif cmd == "recruit_commander" then
    AddFlurryEvent("TutorialStart_GetThirdFleet", {}, 2)
    self:RecruitCommander(commander_type, commander_index)
  elseif cmd == "fire_commander" then
    self:FireCommander(commander_type, commander_index)
  end
end
function GameUIRecruitMain:UpdateGameData()
  local activeTabitem = GameStateRecruit:GetActiveTab()
  if activeTabitem ~= "warpgate" then
    if not self._CommanderTable then
      NetMessageMgr:SendMsg(NetAPIList.recruit_list_req.Code, nil, self._NetMessageCallback, true, nil)
    else
      self:UpdateCommanderList()
      self:MoveIn()
    end
  end
end
function GameUIRecruitMain._NetMessageCallback(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.recruit_list_req.Code then
    if not GameUICollect:CheckTutorialCollect(content.code) and (content.code == 709 or content.code == 130) then
      local text = AlertDataList:GetTextFromErrorCode(130)
      local GameUIKrypton = LuaObjectManager:GetLuaObject("GameUIKrypton")
      GameUIKrypton.NeedMoreMoney(text)
    else
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  if msgtype == NetAPIList.recruit_list_ack.Code then
    GameUIRecruitMain:GenerateCommanderList(content)
    local activeTabitem = GameStateRecruit:GetActiveTab()
    return false
  elseif msgtype == NetAPIList.timelimit_hero_id_ack.Code then
    DebugOutPutTable(content.fleet_id, "fleet_sale_with_rebate_list")
    GameUIRecruitMain.fleet_sale_with_rebate_list = content.fleet_id
    local activeTabitem = GameStateRecruit:GetActiveTab()
    if activeTabitem ~= "warpgate" then
      GameUIRecruitMain:UpdateCommanderList()
      GameUIRecruitMain:MoveIn()
    end
    return true
  end
  return false
end
function GameUIRecruitMain.NetCallbackRecruit(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.recruit_fleet_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  if msgtype == NetAPIList.recruit_fleet_ack.Code then
    if content.code ~= 0 then
      local error_key = AlertDataList:GetTextIdFromCode(content.code)
      DebugOut("GameUIRecruitMain.NetCallbackRecruit", error_key)
      if error_key == "LC_ALERT_recruit_fleet_lepton_err" or error_key == "LC_ALERT_recruit_fleet_quark_err" then
        local energyType = "lepton"
        if error_key == "LC_ALERT_recruit_fleet_quark_err" then
          energyType = "quark"
        end
        local info = GameLoader:GetGameText(error_key)
        local fla = GameUIRecruitTopbar:GetFlashObject()
        fla:InvokeASCallback("_root", "SetPlayNeedMore", true, energyType, info)
      else
        GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      end
      return true
    end
    if QuestTutorialRecruit:IsActive() then
      QuestTutorialRecruit:SetFinish(true)
      GameUIRecruitMain:GetFlashObject():InvokeASCallback("_root", "HideTutorialRecruit")
      local function callback()
        GameUIRecruitTopbar:GetFlashObject():InvokeASCallback("_root", "ShowRecruitClose")
      end
      if immanentversion == 1 then
        GameUICommonDialog:PlayStory({
          100030,
          100031,
          100045
        }, callback)
      elseif immanentversion == 2 then
        callback()
        GameObjectFirstCharge.CheckShowFirstCharge()
        QuestTutorialBattleMap_second:SetActive(true)
        GameStateMainPlanet:SetStoryWhenFocusGain({
          51150,
          51151,
          51152
        })
      end
    end
    local recruit_info = GameUIRecruitMain:GetCommanderRecruitInfo(content.identity)
    local commander_type, commander_index = GameUIRecruitMain:GetCommanderItemWithIdentity(content.identity)
    recruit_info.state = content.state
    DebugOut("commander_type: ", commander_type)
    if commander_type == "commander_normal" or commander_type == "commander_advanced" then
      GameUIRecruitMain:OnCommanderGet(content.identity)
      GameUIRecruitMain:UpdateCommanderList(commander_type)
    else
      GameUIRecruitMain:UpdateCommanderItem(commander_type, commander_index)
    end
    local tip_text = GameLoader:GetGameText("LC_MENU_TIP_RECRUIT_SUCCESS")
    tip_text = string.format(tip_text, GameLoader:GetGameText(GameDataAccessHelper:GetCommanderName(recruit_info.identity)))
    GameTip:Show(tip_text)
    GameUIRecruitTopbar.UpdateResourceData()
    return true
  end
  return false
end
function GameUIRecruitMain.NetCallbackDismiss(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.dismiss_fleet_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  if msgtype == NetAPIList.dismiss_fleet_ack.Code then
    local data_commander = GameUIRecruitMain._lastFireCommander
    data_commander.state = CommanderRecruitState.ready
    local commander_type, commander_index = GameUIRecruitMain:GetCommanderItemWithIdentity(data_commander.identity)
    GameUIRecruitMain:UpdateCommanderItem(commander_type, commander_index)
    local fleet_info = GameGlobalData:GetData("fleetinfo")
    for index, data in ipairs(fleet_info.fleets) do
      if data.identity == GameUIRecruitMain._lastFireCommander.identity then
        table.remove(fleet_info.fleets, index)
        break
      end
    end
    GameUIRecruitTopbar.UpdateResourceData()
    return true
  end
  return false
end
function GameUIRecruitMain:GetActiveServiceNum()
  local fleet_info = GameGlobalData:GetData("fleetinfo")
  return #fleet_info.fleets - 1
end
function GameUIRecruitMain:MoveIn()
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "lua2fs_animationMoveIn")
end
function GameUIRecruitMain:GetFliterType()
  return (...), GameUIRecruitTopbar
end
function GameUIRecruitMain:OnCommanderGet(id)
  local commander_list = self._commanderListNormal
  for dataIndex, dataValue in ipairs(commander_list) do
    if dataValue.identity == id then
      table.remove(commander_list, dataIndex)
      GameUIRecruitMain:InsertCommanderOwned(dataValue)
      break
    end
  end
end
function GameUIRecruitMain:GetCommanderList(commander_type)
  local commander_list
  local isFlitCommand = false
  DebugOut("GameUIRecruitMain:GetCommanderList:", self.CommanderTableOwned)
  if commander_type == "commander_normal" or commander_type == "commander_advanced" then
    commander_list = self._commanderListNormal
    isFlitCommand = true
  elseif commander_type == "commander_owned" then
    commander_list = self.CommanderTableOwned
  else
    assert(false)
  end
  if isFlitCommand then
    if self:GetFliterType() == k_filterTypeNormal then
      DebugOut("GetCommanderList k_filterTypeNormal")
      filter_commander_list = {}
      for k, v in ipairs(commander_list) do
        local commanderColor = self.SelectCommanderColor(v.identity)
        if commanderColor == "commander_green" or commanderColor == "commander_normal" then
          table.insert(filter_commander_list, v)
        end
      end
      commander_list = filter_commander_list
    elseif self:GetFliterType() == k_filterTypeElite then
      DebugOut("GetCommanderList k_filterTypeElite")
      filter_commander_list = {}
      for k, v in ipairs(commander_list) do
        local commanderColor = self.SelectCommanderColor(v.identity)
        if commanderColor == "commander_advanced" or commanderColor == "commander_orange" then
          table.insert(filter_commander_list, v)
        end
      end
      commander_list = filter_commander_list
    end
  else
  end
  return commander_list
end
function GameUIRecruitMain:GetCommanderItem(commander_type, commander_index)
  return self:GetCommanderList(commander_type)[commander_index]
end
function GameUIRecruitMain:UpdateRecruitResource()
  local data_resource = GameGlobalData:GetData("resource")
  local flash_obj = self:GetFlashObject()
  local text_lepton = GameUtils.numberConversion(data_resource.lepton)
  local text_quark = GameUtils.numberConversion(data_resource.quark)
  flash_obj:InvokeASCallback("_root", "lua2fs_updateResourceData", text_lepton, text_quark, -1)
end
function GameUIRecruitMain:OnCommanderFilterTypeChanged()
  if self:GetFlashObject() then
    self:UpdateCommanderList()
  end
end
function GameUIRecruitMain:UpdateCommanderList(commander_type)
  DebugOut("GameUIRecruitMain:UpdateCommanderList")
  DebugOut("QuestTutorialRecruit:IsActive : " .. tostring(QuestTutorialRecruit:IsActive()))
  if QuestTutorialRecruit:IsActive() and QuestTutorialStarCharge:IsFinished() then
    GameUIRecruitMain:GetFlashObject():InvokeASCallback("_root", "ShowTutorialRecruit")
  else
    GameUIRecruitMain:GetFlashObject():InvokeASCallback("_root", "HideTutorialRecruit")
  end
  if not commander_type then
    local activeTabitem = GameStateRecruit:GetActiveTab()
    if activeTabitem == "recruitNormal" then
      commander_type = "commander_normal"
    elseif activeTabitem == "recruitAdvanced" then
      commander_type = "commander_advanced"
    elseif activeTabitem == "owned" then
      commander_type = "commander_owned"
    else
      assert(false)
    end
  end
  self._activeCommanderType = commander_type
  DebugOut("ashdhasdjaghsdj:", commander_type)
  local commander_list = self:GetCommanderList(commander_type)
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "syncCommanderListBox", commander_type, #commander_list)
end
function GameUIRecruitMain:GetActiveCommanderType()
  return self._activeCommanderType
end
function GameUIRecruitMain:GetCommanderRecruitInfo(commander_identity)
  for _, v in ipairs(self._CommanderTable) do
    if v.identity == commander_identity then
      return v
    end
  end
  return nil
end
function GameUIRecruitMain:FilterSellCommander(commanderIdTable)
  local commanderTable = {}
  for i, filterID in ipairs(commanderIdTable) do
    for j, commander in ipairs(self._commanderListNormal) do
      if commander.identity == filterID then
        table.insert(commanderTable, commander)
        break
      end
    end
  end
  self._commanderListNormal = commanderTable
  DebugOut("GameUIRecruitMain:FilterSellCommander")
end
function GameUIRecruitMain:GenerateCommanderList(content)
  local CommanderInMatrix = {}
  local CommanderNotInMatrix = {}
  local CommanderDismissed = {}
  if DebugConfig.isTempOffLineVersion then
    for _, v in ipairs(content.recruit_fleets) do
      v.sort_no = v.identity
    end
  end
  self._CommanderTable = content.recruit_fleets
  self._commanderListNormal = {}
  self.CommanderTableOwned = {}
  for _, v in ipairs(self._CommanderTable) do
    if v.state >= CommanderRecruitState.recruit_able then
      table.insert(self._commanderListNormal, v)
    else
      table.insert(self.CommanderTableOwned, v)
    end
  end
  self:SortCommanderSell()
  self:SortCommanderOwned()
  DebugOut("asdkjasdhakjsd:", self.CommanderTableOwned)
end
function GameUIRecruitMain:InsertCommanderSell(commander)
  table.insert(self._commanderListNormal, commander)
  self:SortCommanderSell()
end
function GameUIRecruitMain:SortCommanderSell()
  DebugOut("SortCommanderSell")
  local CommanderUnlock = {}
  local CommanderLocked = {}
  for i, v in ipairs(self._commanderListNormal) do
    if v.state == CommanderRecruitState.recruit_able then
      table.insert(CommanderUnlock, v)
    else
      table.insert(CommanderLocked, v)
    end
  end
  table.sort(CommanderUnlock, function(a, b)
    return a.sort_no > b.sort_no
  end)
  table.sort(CommanderLocked, function(a, b)
    return a.sort_no < b.sort_no
  end)
  self._commanderListNormal = {}
  LuaUtils:table_imerge(self._commanderListNormal, CommanderUnlock)
  LuaUtils:table_imerge(self._commanderListNormal, CommanderLocked)
  DebugOut("QuestTutorialStarCharge:IsActive : " .. tostring(QuestTutorialStarCharge:IsActive()))
  DebugOut("QuestTutorialRecruit:IsActive : " .. tostring(QuestTutorialRecruit:IsActive()))
  if QuestTutorialStarCharge:IsActive() or QuestTutorialRecruit:IsActive() then
    self:changeOrderInSellCommanderListPutTutorialNeedCommanderAtFirst()
  end
end
function GameUIRecruitMain:InsertCommanderOwned(commander)
  table.insert(self.CommanderTableOwned, commander)
  self:SortCommanderOwned()
end
function GameUIRecruitMain:SortCommanderOwned()
  local CommanderInMatrix = {}
  local CommanderNotInMatrix = {}
  local CommanderDismissed = {}
  for i, v in ipairs(self.CommanderTableOwned) do
    if v.state == CommanderRecruitState.exist then
      if GameUtils:IsInMatrix(v.identity) then
        table.insert(CommanderInMatrix, v)
      else
        table.insert(CommanderNotInMatrix, v)
      end
    else
      table.insert(CommanderDismissed, v)
    end
  end
  table.sort(CommanderInMatrix, function(a, b)
    return a.sort_no > b.sort_no
  end)
  table.sort(CommanderNotInMatrix, function(a, b)
    return a.sort_no > b.sort_no
  end)
  table.sort(CommanderDismissed, function(a, b)
    return a.sort_no > b.sort_no
  end)
  self.CommanderTableOwned = {}
  LuaUtils:table_imerge(self.CommanderTableOwned, CommanderInMatrix)
  LuaUtils:table_imerge(self.CommanderTableOwned, CommanderNotInMatrix)
  LuaUtils:table_imerge(self.CommanderTableOwned, CommanderDismissed)
end
function GameUIRecruitMain:GetCommanderItemWithIdentity(commander_identity)
  local commander_info = self:GetCommanderRecruitInfo(commander_identity)
  local commander_list
  local commander_type = ""
  if commander_info.state < 3 then
    commander_list = self.CommanderTableOwned
    commander_type = "commander_owned"
  elseif commander_info.identity >= 100 then
    commander_list = self._commanderListNormal
    commander_type = "commander_advanced"
  elseif commander_info.identity < 100 then
    commander_list = self._commanderListNormal
    commander_type = "commander_normal"
  else
    assert(false)
  end
  for index, v in ipairs(commander_list) do
    if v.identity == commander_identity then
      return commander_type, index
    end
  end
end
function GameUIRecruitMain:GetDepositInfo(data_commander)
  local deposit_rate = 0
  local lepton_req = 0
  local quark_req = 0
  if self.fleet_sale_with_rebate_list then
    for _, v in pairs(self.fleet_sale_with_rebate_list) do
      if v.fleet_id == data_commander.identity then
        deposit_rate = v.deposit_rate
        lepton_req = v.lepton_req
        quark_req = v.quark_req
      end
    end
  end
  return deposit_rate, lepton_req, quark_req
end
function GameUIRecruitMain.SelectCommanderColor(commander_id)
  local ability = GameDataAccessHelper:GetCommanderAbility(commander_id, 0)
  if ability and ability.COLOR then
    if ability.COLOR == 1 then
      return "commander_green"
    elseif ability.COLOR == 2 then
      return "commander_normal"
    elseif ability.COLOR == 3 then
      return "commander_advanced"
    elseif ability.COLOR == 3 then
      return "commander_orange"
    elseif ability.COLOR == 6 then
      return "commander_red"
    end
  end
  if commander_id >= 100 and commander_id <= 300 then
    return "commander_advanced"
  elseif commander_id >= 2 and commander_id <= 4 then
    return "commander_green"
  elseif commander_id >= 500 and commander_id < 600 then
    return "commander_orange"
  else
    return "commander_normal"
  end
end
function GameUIRecruitMain:UpdateCommanderItem(commander_type, commander_index)
  DebugOut("UpdateCommanderItem ", commander_type, commander_index)
  local data_commander = self:GetCommanderItem(commander_type, commander_index)
  assert(data_commander)
  local commander_status = ""
  if data_commander.state == CommanderRecruitState.exist then
    commander_status = "exist"
  elseif data_commander.state == CommanderRecruitState.ready then
    commander_status = "recruit_back"
  elseif data_commander.state == CommanderRecruitState.recruit_able then
    commander_status = "recruit_able"
  elseif data_commander.state == CommanderRecruitState.prestige_condition then
    commander_status = "locked"
  elseif data_commander.state == CommanderRecruitState.challenge_condition then
    commander_status = "locked"
  else
    assert(false)
  end
  local deposit_rate, lepton_req, quark_req = GameUIRecruitMain:GetDepositInfo(data_commander)
  DebugOut("deposit_rate", deposit_rate, "lepton_req", lepton_req, "quark_req", quark_req)
  local displayerInfo = data_commander.show_info
  local vessels_image_name = GameDataAccessHelper:GetCommanderShipByDownloadState(displayerInfo.ship)
  local vessels_type_frame = FleetDataAccessHelper:GetFleetVesselsFrameByVessel(displayerInfo.vessels)
  local colorFrame = FleetDataAccessHelper:GetFleetColorFrameByColor(displayerInfo.color)
  local spellFrame = FleetDataAccessHelper:GetFleetSpellFrameBySpell(displayerInfo.spell_id)
  local commander_name = FleetDataAccessHelper:GetFleetLevelDisplayName(displayerInfo.name, displayerInfo.fleet_id, displayerInfo.color, 0)
  local avatarFrame = FleetDataAccessHelper:GetFleetAvatarFrameByAvatar(displayerInfo.avatar, displayerInfo.fleet_id)
  local vessels_name = FleetDataAccessHelper:GetFleetVesselsTypeStringByVessel(displayerInfo.vessels)
  local data_table = {}
  data_table[#data_table + 1] = commander_name
  data_table[#data_table + 1] = GameUIRecruitMain.SelectCommanderColor(data_commander.identity)
  data_table[#data_table + 1] = avatarFrame
  data_table[#data_table + 1] = vessels_image_name
  data_table[#data_table + 1] = vessels_type_frame
  data_table[#data_table + 1] = vessels_name
  data_table[#data_table + 1] = spellFrame
  data_table[#data_table + 1] = commander_status
  data_table[#data_table + 1] = GameLoader:GetGameText("LC_MENU_LIMITED_SELL_CHAR")
  data_table[#data_table + 1] = GameLoader:GetGameText("LC_MENU_SELL_CHAR")
  data_table[#data_table + 1] = deposit_rate
  data_table[#data_table + 1] = commander_status == "recruit_able" and GetCommanderRecruitCost(data_commander) or 0
  data_table[#data_table + 1] = quark_req > 0 and quark_req or lepton_req
  data_table[#data_table + 1] = displayerInfo.damage_assess
  data_table[#data_table + 1] = displayerInfo.damage_rate
  data_table[#data_table + 1] = displayerInfo.defence_assess
  data_table[#data_table + 1] = displayerInfo.defence_rate
  data_table[#data_table + 1] = displayerInfo.assist_assess
  data_table[#data_table + 1] = displayerInfo.assist_rate
  data_table[#data_table + 1] = GameUtils:GetFleetRankFrame(displayerInfo.rank, GameUIRecruitMain.DownloadRankImageCallback, commander_index)
  local hasAdjutant = "false"
  if displayerInfo.can_be_adjutant and 1 == displayerInfo.can_be_adjutant then
    hasAdjutant = "true"
  end
  data_table[#data_table + 1] = hasAdjutant
  local sex = GameDataAccessHelper:GetCommanderSex(data_commander.identity)
  if sex == 1 then
    sex = "man"
  elseif sex == 0 then
    sex = "woman"
  else
    sex = "unknown"
  end
  data_table[#data_table + 1] = sex
  local data_string = table.concat(data_table, "\001")
  self:GetFlashObject():InvokeASCallback("_root", "setCommanderItem", commander_index, data_string)
  if commander_status == "locked" then
    local dataResource = GameGlobalData:GetData("resource")
    local rankLevelRequire = GameDataAccessHelper:GetMilitaryRankInfoWithPrestige(data_commander.prestige_req).Prestige_Level
    rankLevelRequire = GameDataAccessHelper:GetMilitaryRankName(rankLevelRequire)
    local lockedInfo = string.format(GameLoader:GetGameText("LC_MENU_CELESTIAL_PORTAL_COMMANDER_UNLOCK_DES_CHAR"), rankLevelRequire)
    if immanentversion == 2 then
      local progress = GameGlobalData:GetData("progress")
      local currentArea = progress.act
      local currentSection = progress.chapter
      local currentBattle = currentArea * 1000000 + currentSection * 1000
      local requireBattle = data_commander.visible_battle_req
      local requireArea = math.floor(requireBattle / 1000000)
      local requireSection = math.floor((requireBattle - requireArea * 1000000) / 1000)
      local isRequirePrestige = data_commander.prestige_req > dataResource.prestige
      DebugOut("currentBattle vs requireBattle:", currentBattle, requireArea * 1000000 + (requireSection + 1) * 1000)
      if currentBattle < requireArea * 1000000 + (requireSection + 1) * 1000 then
        DebugOut("requireArea requireSection:", requireArea, requireSection)
        if requireArea >= 1 and requireSection >= 1 then
          local areaName = GameLoader:GetGameText("LC_MENU_AREA_NAME_" .. requireArea)
          local sectionName = GameLoader:GetGameText("LC_MENU_SECTION_NAME_" .. requireArea .. "_" .. requireSection)
          local lockedBattleInfo = GameLoader:GetGameText("LC_MENU_CELESTIAL_PORTAL_COMMANDER_UNLOCK_BATTLE_DES_CHAR")
          lockedBattleInfo = string.gsub(lockedBattleInfo, "<area>", areaName)
          lockedBattleInfo = string.gsub(lockedBattleInfo, "<section>", sectionName)
          if isRequirePrestige then
            lockedInfo = lockedInfo .. "\n" .. lockedBattleInfo
          else
            lockedInfo = lockedBattleInfo
          end
        end
      end
    end
    self:GetFlashObject():InvokeASCallback("_root", "lua2fs_updateCommanderLockedText", commander_index, lockedInfo)
  end
  GameUIRecruitMain:UpdateItemCDTime(commander_type, commander_index)
end
function GameUIRecruitMain.DownloadRankImageCallback(extInfo)
  DebugOut("downloadImgCallback = ")
  DebugTable(extInfo)
  if GameUIRecruitMain:GetFlashObject() then
    GameUIRecruitMain:GetFlashObject():InvokeASCallback("_root", "updateRankImage", extInfo.rank_id, extInfo.id)
  end
end
function GameUIRecruitMain:RecruitCommander(commander_type, commander_index)
  local data_commander = self:GetCommanderItem(commander_type, commander_index)
  if data_commander.state == CommanderRecruitState.recruit_able or data_commander.state == CommanderRecruitState.ready then
    NetMessageMgr:SendMsg(NetAPIList.recruit_fleet_req.Code, {
      identity = data_commander.identity
    }, self.NetCallbackRecruit, true, nil)
  end
end
function GameUIRecruitMain:FireCommander(commander_type, commander_index)
  local data_commander = self:GetCommanderItem(commander_type, commander_index)
  if data_commander.state == CommanderRecruitState.exist then
    local packet = {
      id = data_commander.identity
    }
    NetMessageMgr:SendMsg(NetAPIList.dismiss_fleet_req.Code, packet, self.NetCallbackDismiss, true, nil)
    self._lastFireCommander = data_commander
  end
end
function GameUIRecruitMain.resourceListener()
  if GameStateRecruit == GameStateManager:GetCurrentGameState() then
    GameUIRecruitMain:UpdateRecruitResource()
  end
end
function GameUIRecruitMain:SetVisible(mc_path, is_visible)
  mc_path = mc_path or -1
  self:GetFlashObject():InvokeASCallback("_root", "lua2fs_setVisible", mc_path, is_visible)
end
function GameUIRecruitMain:Update(dt)
  local obj_flash = self:GetFlashObject()
  if obj_flash then
    obj_flash:InvokeASCallback("_root", "lua2fs_updateFrame", dt)
    obj_flash:Update(dt)
  end
end
function GameUIRecruitMain:OnParentStateGetFocus()
  self:UpdateRecruitResource()
end
function GameUIRecruitMain:SelectCommander(commander_type, commander_index)
  local commander_data = self:GetCommanderItem(commander_type, commander_index)
  if not commander_data then
    self:GetFlashObject():InvokeASCallback("_root", "lua2fs_selectCommander", -1)
    return
  end
  local desc_enable_table = {
    CommanderRecruitState.exist,
    CommanderRecruitState.ready,
    CommanderRecruitState.recruit_able
  }
  local enable_commander_desc = false
  for _, v_status in ipairs(desc_enable_table) do
    if commander_data.state == v_status then
      enable_commander_desc = true
      break
    end
  end
  DebugOut("enable_commander_desc - ", enable_commander_desc)
  if enable_commander_desc then
    self:GetFlashObject():InvokeASCallback("_root", "lua2fs_selectCommander", commander_index)
    self:ShowCommanderInfo(commander_data)
  end
end
function GameUIRecruitMain:ShowCommanderInfo(commander_data)
  local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
  ItemBox:ShowCommanderDetail2(commander_data.identity, nil, 0)
end
function GameUIRecruitMain:OnTouchReleased(x_pos, y_pos)
  self._lastReleasePosX = x_pos
  self._lastReleasePosY = y_pos
  return ...
end
function GameUIRecruitMain._OnTimerTick()
  if GameStateManager:GetCurrentGameState():IsObjectInState(GameUIRecruitMain) then
    if GameUIRecruitMain:NeedTimeTick() then
      local commander_list = GameUIRecruitMain:GetCommanderList(GameUIRecruitMain._activeCommanderType)
      if commander_list then
        for i = 1, #commander_list do
          GameUIRecruitMain:UpdateItemCDTime(GameUIRecruitMain._activeCommanderType, i)
        end
      end
    end
    return 1000
  end
  return nil
end
function GameUIRecruitMain:UpdateItemCDTime(commander_type, commander_index)
  local data_commander = self:GetCommanderItem(commander_type, commander_index)
  local rebateInfo
  for _, v in pairs(self.fleet_sale_with_rebate_list) do
    if v.fleet_id == data_commander.identity then
      rebateInfo = v
      break
    end
  end
  local deposit_rate = 0
  local lepton_req = 0
  local quark_req = 0
  local countTimeType = ""
  local timeString = 0
  local leftTime = 0
  if rebateInfo == nil or self._activeCommanderType == "commander_owned" then
    countTimeType = "none"
  elseif rebateInfo.left_time < os.time() then
    countTimeType = "none"
  else
    deposit_rate, lepton_req, quark_req = GameUIRecruitMain:GetDepositInfo(data_commander)
    if 0 < rebateInfo.deposit_rate and rebateInfo.deposit_rate < 100 then
      countTimeType = "fleet_cutoff"
    else
      countTimeType = "fleet_purchase_limit"
    end
    leftTime = rebateInfo.left_time - os.time()
    timeString = GameUtils:formatTimeString(leftTime)
  end
  local commander_status = ""
  if data_commander.state == CommanderRecruitState.exist then
    commander_status = "exist"
  elseif data_commander.state == CommanderRecruitState.ready then
    commander_status = "recruit_back"
  elseif data_commander.state == CommanderRecruitState.recruit_able then
    commander_status = "recruit_able"
  elseif data_commander.state == CommanderRecruitState.prestige_condition then
    commander_status = "locked"
  elseif data_commander.state == CommanderRecruitState.challenge_condition then
    commander_status = "locked"
  else
    assert(false)
  end
  local cost_value = commander_status == "recruit_able" and GetCommanderRecruitCost(data_commander) or 0
  local cost_value_after_discount = quark_req > 0 and quark_req or lepton_req
  GameUIRecruitMain:GetFlashObject():InvokeASCallback("_root", "setCommanderItemTimeText", commander_index, countTimeType, timeString, deposit_rate, cost_value, cost_value_after_discount, commander_status)
end
function GameUIRecruitMain:NeedTimeTick()
  return self._activeCommanderType == "commander_normal" or self._activeCommanderType == "commander_advanced"
end
function GameUIRecruitMain:changeOrderInSellCommanderListPutTutorialNeedCommanderAtFirst()
  DebugOut("changeOrderInSellCommanderListPutTutorialNeedCommanderAtFirst")
  for i, commander in ipairs(self._commanderListNormal) do
    if commander.identity == TutorialQuestManager.m_commanderLyonsID then
      if 1 == i then
        break
      end
      local commanderLyon = commander
      for j = i, 2, -1 do
        self._commanderListNormal[j] = self._commanderListNormal[j - 1]
      end
      self._commanderListNormal[1] = commanderLyon
      break
    end
  end
end
