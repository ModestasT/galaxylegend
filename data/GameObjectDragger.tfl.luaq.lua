local GameObjectDragger = LuaObjectManager:GetLuaObject("GameObjectDragger")
function GameObjectDragger:Init()
  self.m_isAutoMove = false
  self.m_frameCounter = 0
end
function GameObjectDragger:OnEraseFromGameState()
  self.m_isAutoMove = false
  self.m_frameCounter = 0
  DebugOut("self.m_isAutoMove = false in erase")
end
function GameObjectDragger:BeginDrag(initPosX, initPosY, mouseX, mouseY)
  self:GetFlashObject():InvokeASCallback("_root", "BeginDrag", initPosX, initPosY, mouseX, mouseY)
  self.m_frameCounter = 0
end
function GameObjectDragger:SetDestPosition(xPos, yPos)
  if xPos and yPos then
    self:GetFlashObject():InvokeASCallback("_root", "SetAutoMoveToPos", math.floor(xPos), math.floor(yPos))
  end
end
function GameObjectDragger:IsAutoMove()
  return self.m_isAutoMove
end
function GameObjectDragger:StartAutoMove()
  DebugOut("start auto move")
  self.m_isAutoMove = true
end
function GameObjectDragger:UpdateDrag()
  if self.m_frameCounter > 0 then
    self:GetFlashObject():InvokeASCallback("_root", "UpdateDrag")
  end
end
function GameObjectDragger:Update(dt)
  self.m_frameCounter = self.m_frameCounter + 1
  local flashobj = self:GetFlashObject()
  if flashobj then
    flashobj:Update(dt)
  else
    self.m_isAutoMove = false
    self.m_frameCounter = 0
    return
  end
  if self.m_isAutoMove and flashobj then
    flashobj:InvokeASCallback("_root", "UpdateAutoMove", dt)
    local isReachDest = self:GetFlashObject():InvokeASCallback("_root", "IsReachDestPos")
    if isReachDest == "true" then
      self.m_isAutoMove = false
      if self.OnReachedDestPos then
        self:OnReachedDestPos()
      end
    else
      self.m_isAutoMove = true
    end
  end
end
