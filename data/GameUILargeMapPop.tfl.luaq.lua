local GameUILargeMapPop = LuaObjectManager:GetLuaObject("GameUILargeMapPop")
local GameUILargeMapUI = LuaObjectManager:GetLuaObject("GameUILargeMapUI")
local GameUILargeMap = LuaObjectManager:GetLuaObject("GameUILargeMap")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameStateGlobalState = GameStateManager.GameStateGlobalState
GameUILargeMapPop.InvitePopData = nil
GameUILargeMapPop.chooseType = "Refuse"
GameUILargeMapPop.InviteMassID = -1
GameUILargeMapPop.pendInviteData = {}
function GameUILargeMapPop:OnInitGame()
end
function GameUILargeMapPop:OnAddToGameState()
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  GameUILargeMapPop:SetInvitePop()
end
function GameUILargeMapPop:OnEraseFromGameState()
  DebugOut("GameUILargeMapPop:OnEraseFromGameState")
  self:UnloadFlashObject()
  GameUILargeMapPop.InvitePopData = nil
end
function GameUILargeMapPop.LargeMapInviteNtf(content)
  GameUILargeMapPop:ShowInvitePop(content)
end
function GameUILargeMapPop:OnFSCommand(cmd, arg)
  if cmd == "close" then
    GameUILargeMapPop.InvitePopData = nil
    if #GameUILargeMapPop.pendInviteData > 0 then
      local data = table.remove(GameUILargeMapPop.pendInviteData, 1)
      GameUILargeMapPop:ShowInvitePop(data)
    else
      GameStateGlobalState:EraseObject(GameUILargeMapPop)
    end
  elseif cmd == "AcceptInvite" then
    GameUILargeMapPop.chooseType = "Accept"
    GameUILargeMapPop:OnInvite(GameUILargeMapPop.InvitePopData.troop_id, true, arg == "2")
  elseif cmd == "RefuseInvite" then
    GameUILargeMapPop.chooseType = "Refuse"
    GameUILargeMapPop:OnInvite(GameUILargeMapPop.InvitePopData.troop_id, false, arg == "2")
  end
end
function GameUILargeMapPop:ShowInvitePop(data)
  if GameUILargeMapPop.InvitePopData then
    GameUILargeMapPop.pendInviteData[#GameUILargeMapPop.pendInviteData + 1] = data
    return
  end
  GameUILargeMapPop.InvitePopData = data
  if not self:GetFlashObject() then
    GameStateGlobalState:AddObject(GameUILargeMapPop)
  else
    GameUILargeMapPop:SetInvitePop()
  end
end
function GameUILargeMapPop:SetInvitePop()
  if GameUILargeMapPop.InvitePopData then
    local dstName = ""
    dstName = GameLoader:GetGameText(GameUILargeMapPop.InvitePopData.block_name_key)
    if GameUILargeMapPop.InvitePopData.alliance_info.id ~= "" and GameUILargeMapPop.InvitePopData.alliance_info.id ~= "0" then
      dstName = dstName .. "(" .. GameUILargeMapPop.InvitePopData.alliance_info.name .. ")"
    end
    local data = {}
    data.desc = string.gsub(GameLoader:GetGameText("LC_MENU_MAP_FIGHT_INVITE_JOIN_TEAM"), "<number>", dstName)
    data.troopId = GameUILargeMapPop.InvitePopData.troop_id
    data.leaderName = GameUILargeMapPop.InvitePopData.leader_name
    data.leaderAvatar = FleetDataAccessHelper:GetFleetAvatarFrameByAvatar(GameUILargeMapPop.InvitePopData.leader_avatar, 0, 0)
    data.leaderLevel = "Lv." .. GameUILargeMapPop.InvitePopData.leader_level
    data.leaderForce = GameUtils.numberConversion(GameUILargeMapPop.InvitePopData.leader_force)
    if GameUILargeMapPop:GetFlashObject() then
      GameUILargeMapPop:GetFlashObject():InvokeASCallback("_root", "SetInvitePop", data)
    end
  end
end
function GameUILargeMapPop:OnInvite(troopId, isAccept, pauseNotice)
  GameUILargeMapPop.InviteMassID = GameUILargeMapPop.InvitePopData.id
  local param = {}
  param.troops_id = troopId
  param.is_accept = isAccept
  param.pause_notice = pauseNotice
  NetMessageMgr:SendMsg(NetAPIList.large_map_mass_invite_do_req.Code, param, GameUILargeMapPop.LargeMapMassInviteDoCallBack, true, nil)
end
function GameUILargeMapPop.LargeMapMassInviteDoCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.large_map_mass_invite_do_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    else
      if GameUILargeMapPop.chooseType == "Accept" then
        GameUILargeMapPop.pendInviteData = {}
      end
      if GameUILargeMap:GetFlashObject() and GameUILargeMapPop.chooseType == "Accept" then
        GameUILargeMapUI:Show(GameUILargeMap.MyPlayerData, nil, GameUILargeMapUI.TYPE_MASS_MEMBERS)
        if GameUILargeMapPop.InviteMassID ~= -1 then
          GameUILargeMap:GetFlashObject():InvokeASCallback("_root", "SetCenterPosition", GameUILargeMapPop.InviteMassID)
          GameUILargeMapPop.InviteMassID = -1
        end
      end
      GameUILargeMapPop.chooseType = "Refuse"
    end
    return true
  end
  return false
end
if AutoUpdate.isAndroidDevice then
  function GameUILargeMapPop.OnAndroidBack()
    GameUILargeMapPop:OnFSCommand("close")
  end
end
