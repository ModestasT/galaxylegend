local GameUIArena = LuaObjectManager:GetLuaObject("GameUIArena")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameStateBattlePlay = GameStateManager.GameStateBattlePlay
local GameStateArena = GameStateManager.GameStateArena
local GameUIBattleResult = LuaObjectManager:GetLuaObject("GameUIBattleResult")
local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
local GameCommonGoodsList = LuaObjectManager:GetLuaObject("GameCommonGoodsList")
local GameObjectFormationBackground = LuaObjectManager:GetLuaObject("GameObjectFormationBackground")
local GameUIWDStuff = LuaObjectManager:GetLuaObject("GameUIWDStuff")
require("FleetMatrix.tfl")
GameUIAdvancedArenaLayer = {}
GameUIAdvancedArenaLayer.advanceArenaInfo = nil
GameUIAdvancedArenaLayer.protectTime = nil
GameUIAdvancedArenaLayer.recoveryTime = nil
GameUIAdvancedArenaLayer.accountingTime = nil
GameUIAdvancedArenaLayer.currentEnemyInfo = nil
GameUIAdvancedArenaLayer.searchOpponentinfo = nil
GameUIAdvancedArenaLayer.isShowHelp = false
GameUIAdvancedArenaLayer.isShowStore = false
GameUIAdvancedArenaLayer.isShowMatrix = false
GameUIAdvancedArenaLayer.StateType = {first = 1, other = 2}
GameUIAdvancedArenaLayer.searchOpponentinfoState = GameUIAdvancedArenaLayer.StateType.first
function GameUIAdvancedArenaLayer:OnEnter()
  GameGlobalData:RegisterDataChangeCallback("wdcPlayerInfo", GameUIAdvancedArenaLayer.UpdateWDCInfo)
  NetMessageMgr:SendMsg(NetAPIList.wdc_enter_req.Code, nil, GameUIAdvancedArenaLayer.NetCallbackEnterAdvancedArenaLayer, true)
  GameGlobalData:RegisterDataChangeCallback("resource", GameUIAdvancedArenaLayer.updateSilverCount)
  GameGlobalData:RegisterDataChangeCallback("wdc_supply", GameUIAdvancedArenaLayer.updateWdcSupplyCallback)
end
function GameUIAdvancedArenaLayer:Update(dt)
  if self.protectTime then
    self:UpdateProtectTime()
  end
  if self.recoveryTime then
    self:UpdateRecoveryTime()
  end
  if self.accountingTime then
    self:UpdateAcountingTime()
  end
end
function GameUIAdvancedArenaLayer:CleanSearchInfo()
  GameUIAdvancedArenaLayer.searchOpponentinfo = nil
  GameUIAdvancedArenaLayer.searchOpponentinfoState = GameUIAdvancedArenaLayer.StateType.first
end
function GameUIAdvancedArenaLayer:OnFSCommand(cmd, arg)
  if "close_advanced" == cmd then
    if GameUIAdvancedArenaLayer.searchOpponentinfo then
      local text_title = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
      local info = GameLoader:GetGameText("LC_MENU_LEAGUE_QUIT_INFO")
      GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
      GameUIMessageDialog:SetYesButton(GameUIAdvancedArenaLayer.CloseButtonYesCallback)
      GameUIMessageDialog:SetNoButton(nil)
      GameUIMessageDialog:Display(text_title, info)
    else
      GameUIAdvancedArenaLayer:MoveOutAnimation()
    end
  end
  if "moveOutOver" == cmd then
    GameStateArena:Quit()
  end
  if "SearchOpponent" == cmd then
    GameUIAdvancedArenaLayer:SearchOpponentAction()
  end
  if "advanceArena_store" == cmd then
    local exchange = {}
    exchange.items = {}
    table.insert(exchange.items, "silver")
    GameCommonGoodsList:SetID(exchange)
    GameCommonGoodsList:SetStoreType(GameCommonGoodsList.TYPE.wdc)
    GameStateManager:GetCurrentGameState():AddObject(GameCommonGoodsList)
    self.isShowStore = true
  end
  if "change_matrix" == cmd then
    FleetMatrix:GetMatrixsReq(GameUIAdvancedArenaLayer.GetMatrixCallback, FleetMatrix.MATRIX_TYPE_WORLD_CHAMPION)
    self.isShowMatrix = true
  end
  if "UpdateMarix" == cmd then
    local index = tonumber(arg)
    FleetMatrix:SwitchMatrixClient(index, self.UpdateWorldChampionFleetAvatar)
  end
  if "choose_worldchampion_matrix" == cmd then
    FleetMatrix:SetTypeMatrix(FleetMatrix.MATRIX_TYPE_WORLD_CHAMPION)
  end
  if "buy_credit" == cmd then
    GameUIAdvancedArenaLayer:RequestBuyEnegyPrice()
  end
  if "challenge_player" == cmd then
    NetMessageMgr:SendMsg(NetAPIList.wdc_fight_status_req.Code, nil, GameUIAdvancedArenaLayer.getProtectTimeMoveInFormationNetCallback, true, nil)
    self.currentPlayer = arg
  end
  if "reward" == cmd then
  end
  if "leaderboard" == cmd then
  end
  if "honor" == cmd then
  end
  if "report" == cmd then
  end
  if "advanced_help" == cmd then
    GameUIWDStuff:Show(GameUIWDStuff.menuType.menu_type_help)
    GameUIWDStuff:SetHelpText(GameLoader:GetGameText("LC_MENU_LEAGUE_HELP"))
  end
  if "hideMatrix" == cmd then
    self.isShowMatrix = false
  end
end
function GameUIAdvancedArenaLayer.UpdateWDCInfo()
  if GameUIArena:GetFlashObject() then
    local wdcPlayerInfo = GameGlobalData:GetData("wdcPlayerInfo")
    local content = {}
    content.info = wdcPlayerInfo
    GameUIAdvancedArenaLayer:Init(content)
  end
end
function GameUIAdvancedArenaLayer.NetCallbackEnterAdvancedArenaLayer(msgType, content)
  if msgType == NetAPIList.wdc_enter_ack.Code then
    GameUIAdvancedArenaLayer:SetSearchOpponentStates()
    GameUIAdvancedArenaLayer:Init(content)
    GameUIAdvancedArenaLayer:MoveIn()
    if GameUIAdvancedArenaLayer.searchOpponentinfo then
      DebugOut("shufsd")
      GameUIAdvancedArenaLayer:ShowPlayerDetail()
      GameUIArena:GetFlashObject():InvokeASCallback("_root", "stopSearchAnimation")
    end
    return true
  end
  return false
end
function GameUIAdvancedArenaLayer.updateSilverCount()
  local count = GameGlobalData:GetData("resource")
  local silverCount = GameUtils.numberConversion(count.silver)
  if GameUIArena:GetFlashObject() then
    GameUIArena:GetFlashObject():InvokeASCallback("_root", "setCurrencyCount", silverCount)
  end
  GameUIAdvancedArenaLayer.updateWdcSupplyCallback()
end
function GameUIAdvancedArenaLayer:IsEndTime()
  if self.advanceArenaInfo.player_info.end_time > 0 then
    return true
  end
  return false
end
function GameUIAdvancedArenaLayer:Init(content)
  DebugOut("InitData = ")
  DebugTable(content)
  self.advanceArenaInfo = content.info
  if self.advanceArenaInfo.player_info.protect_time <= 0 then
    GameUIAdvancedArenaLayer.protectTime = nil
    self:SetProtectTime(self.advanceArenaInfo.player_info.protect_time)
  else
    GameUIAdvancedArenaLayer.protectTime = os.time() + self.advanceArenaInfo.player_info.protect_time
  end
  GameUIAdvancedArenaLayer.recoveryTime = os.time() + self.advanceArenaInfo.player_info.final_time
  GameUIAdvancedArenaLayer.accountingTime = os.time() + self.advanceArenaInfo.player_info.end_time
  self:SetDanAndintegrateInfo(self.advanceArenaInfo.player_info.rank_img, self.advanceArenaInfo.player_info.point, self.advanceArenaInfo.player_info.next_rank_point, self.advanceArenaInfo.player_info.rank_point)
  local count = GameGlobalData:GetData("resource")
  DebugOut("count = ")
  DebugTable(count)
  local wdcSourceCount = GameUtils.numberConversion(count.silver)
  local currentForce = GameUtils.numberConversion(self.advanceArenaInfo.player_info.force)
  local hoursSilver = GameUtils.numberConversion(self.advanceArenaInfo.player_info.res_per_hour)
  self:SetAdvanceOutputSourceInfo(wdcSourceCount, currentForce)
  local current_supply = GameUtils.numberConversion(count.wdc_supply)
  self:SetAdvanceEnergy(self.advanceArenaInfo.search_cost, self.advanceArenaInfo.max_supply, current_supply, self.advanceArenaInfo.can_buy, self.advanceArenaInfo.player_info.end_time)
  self:SetCurrencyItem()
  GameUIArena.KingNumber = self.advanceArenaInfo.king_num
  if GameUIAdvancedArenaLayer.searchOpponentinfoState == GameUIAdvancedArenaLayer.StateType.first then
    DebugOut("123232")
    local buttonText = GameLoader:GetGameText("LC_MENU_LEAGUE_SEARCH_RIVAL_BUTTON")
    GameUIAdvancedArenaLayer:SetSearchOpponentButtonText(buttonText)
  else
    DebugOut("12222")
    local buttonText = GameLoader:GetGameText("LC_MENU_GLC_CHANGE_OPPONENT")
    GameUIAdvancedArenaLayer:SetSearchOpponentButtonText(buttonText)
  end
  if self.advanceArenaInfo.pop_report then
    GameUIArena.nowReportType = "defend_list"
    GameUIArena:RequestGalaxyArenaReport()
  end
end
function GameUIAdvancedArenaLayer:SetCurrencyItem()
  local itemFrame = GameHelper:GetAwardTypeIconFrameName("silver", nil, nil)
  if GameUIArena:GetFlashObject() then
    GameUIArena:GetFlashObject():InvokeASCallback("_root", "setCurrencyFram", itemFrame)
  end
end
function GameUIAdvancedArenaLayer.DownloadCurrencyItemCallback(extInfo)
  local itemFrame = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(extInfo, nil, nil)
  if GameUIArena:GetFlashObject() then
    GameUIArena:GetFlashObject():InvokeASCallback("_root", "", itemFrame)
  end
end
function GameUIAdvancedArenaLayer:SetDanAndintegrateInfo(dan, currentScore, nextScore, preScore)
  local extInfo = {}
  extInfo.ranking = dan
  local rankingFrame = GameUtils:GetPlayerRankingInWdc(dan, extInfo, GameUIAdvancedArenaLayer.DownloadRaningImageCallback)
  GameUIArena:GetFlashObject():InvokeASCallback("_root", "setDanAndintegrateInfo_as", rankingFrame, currentScore, nextScore, preScore)
end
function GameUIAdvancedArenaLayer.DownloadRaningImageCallback(content)
  if GameUIArena:GetFlashObject() then
    GameUIArena:GetFlashObject():InvokeASCallback("_root", "setRankingImage", content.ranking)
  end
end
function GameUIAdvancedArenaLayer:SetAdvanceOutputSourceInfo(token, per_hour)
  local tokenCountText = token
  local per_hourCountText = per_hour
  GameUIArena:GetFlashObject():InvokeASCallback("_root", "setAdvanceOutputSourceInfo_as", tokenCountText, per_hourCountText)
  local player_main_fleet = GameGlobalData:GetFleetInfo(1)
  local leaderlist = GameGlobalData:GetData("leaderlist")
  local curMatrixIndex = GameGlobalData.GlobalData.curMatrixIndex
  local player_avatar = GameDataAccessHelper:GetFleetAvatar(leaderlist.leader_ids[curMatrixIndex], player_main_fleet.level)
  GameUIArena:GetFlashObject():InvokeASCallback("_root", "setPlayerAvatar", player_avatar)
end
function GameUIAdvancedArenaLayer.DownloadItemCallback()
  local itemFrame = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(GameUIAdvancedArenaLayer.advanceArenaInfo.token)
  GameUIArena:GetFlashObject():InvokeASCallback("_root", "setOutputSourceFrame_as", itemFrame)
end
function GameUIAdvancedArenaLayer:SetAdvanceEnergy(matchCost, costMax, costCurrent, canBuy, finalTime)
  local name = GameLoader:GetGameText("")
  local isCanSearch = true
  if tonumber(matchCost) > tonumber(costCurrent) or finalTime > 0 then
    isCanSearch = false
  end
  local energyText = GameUIArena:GetEnergyColor(costCurrent) .. "/" .. costMax
  DebugOut("energyText = " .. energyText)
  GameUIArena:GetFlashObject():InvokeASCallback("_root", "setAdvanceEnergy_as", matchCost, name, canBuy, isCanSearch, energyText)
end
function GameUIAdvancedArenaLayer:MoveIn()
  DebugOut("GameUIAdvancedArenaLayerMoveIn")
  if GameUIArena:GetFlashObject() then
    GameUIArena:GetFlashObject():InvokeASCallback("_root", "MoveInAdvancedLayer")
  end
end
function GameUIAdvancedArenaLayer:UpdateProtectTime()
  if GameUIArena:GetFlashObject() then
    local left_time = -1
    if self.protectTime then
      left_time = self.protectTime - os.time()
      if left_time < 0 then
        left_time = -1
        self.protectTime = nil
      end
      self:SetProtectTime(left_time)
    end
  end
end
function GameUIAdvancedArenaLayer:SetProtectTime(time)
  local timeStatus = 0
  if time <= 0 then
    timeStatus = 0
  else
    timeStatus = 1
  end
  local timeText = GameLoader:GetGameText("LC_MENU_LEAGUE_SAVE_TIME") .. GameUtils:formatTimeString(time)
  if GameUIArena:GetFlashObject() then
    GameUIArena:GetFlashObject():InvokeASCallback("_root", "setProtectTime", timeStatus, timeText)
  end
end
function GameUIAdvancedArenaLayer:UpdateRecoveryTime()
  if GameUIArena:GetFlashObject() then
    local left_time = -1
    if self.recoveryTime then
      left_time = self.recoveryTime - os.time()
      if left_time < 0 then
        left_time = -1
        self.recoveryTime = nil
      end
      self:SetRecoveryTime(left_time)
    end
  end
end
function GameUIAdvancedArenaLayer:SetRecoveryTime(time)
  local timeStatus = 0
  if time <= 0 then
    timeStatus = 0
  else
    timeStatus = 1
  end
  local text = GameLoader:GetGameText("LC_MENU_LEAGUE_SEASON_END_INFO")
  GameUIArena:GetFlashObject():InvokeASCallback("_root", "setRecoveryTime", timeStatus, GameUtils:formatTimeString(time), text)
end
function GameUIAdvancedArenaLayer:UpdateAcountingTime()
  if GameUIArena:GetFlashObject() then
    local left_time = -1
    if self.accountingTime then
      left_time = self.accountingTime - os.time()
      if left_time < 0 then
        left_time = -1
        self.accountingTime = nil
      end
      self:SetAccountingTime(left_time)
    end
  end
end
function GameUIAdvancedArenaLayer:SetAccountingTime(time)
  local timeStatus = 0
  if time <= 0 then
    timeStatus = 0
  else
    timeStatus = 1
  end
  local text = GameLoader:GetGameText("LC_MENU_LEAGUE_SEASON_END_INFO") .. GameUtils:formatTimeString(time)
  GameUIArena:GetFlashObject():InvokeASCallback("_root", "setAccountingTime", timeStatus, text)
end
function GameUIAdvancedArenaLayer:SearchOpponentAction()
  NetMessageMgr:SendMsg(NetAPIList.wdc_fight_status_req.Code, nil, GameUIAdvancedArenaLayer.getProtectTimeNetCallback, true, nil)
end
function GameUIAdvancedArenaLayer.getProtectTimeNetCallback(msgType, content)
  if msgType == NetAPIList.wdc_fight_status_ack.Code then
    if content.protect_time > 0 then
      local text_title = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
      local info = GameLoader:GetGameText("LC_MENU_LEAGUE_ATTACK_INFO")
      GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
      GameUIMessageDialog:SetYesButton(GameUIAdvancedArenaLayer.SearchOpponentButtonCallback)
      GameUIMessageDialog:SetNoButton(nil)
      GameUIMessageDialog:Display(text_title, info)
    else
      GameUIAdvancedArenaLayer:SearchOpponent()
    end
    return true
  end
  return true
end
function GameUIAdvancedArenaLayer.getProtectTimeMoveInFormationNetCallback(msgType, content)
  if msgType == NetAPIList.wdc_fight_status_ack.Code then
    if content.protect_time > 0 then
      local text_title = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
      local info = GameLoader:GetGameText("LC_MENU_LEAGUE_ATTACK_INFO")
      GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
      GameUIMessageDialog:SetYesButton(GameUIAdvancedArenaLayer.onEnterFormation)
      GameUIMessageDialog:SetNoButton(nil)
      GameUIMessageDialog:Display(text_title, info)
    else
      GameUIAdvancedArenaLayer:RequireMatrix()
    end
    return true
  end
  return false
end
function GameUIAdvancedArenaLayer.onEnterFormation()
  GameUIAdvancedArenaLayer:RequireMatrix()
end
function GameUIAdvancedArenaLayer.SearchOpponentButtonCallback()
  GameUIAdvancedArenaLayer:SearchOpponent()
end
function GameUIAdvancedArenaLayer:SetSearchOpponentStates()
  if GameUIAdvancedArenaLayer.searchOpponentinfo then
    GameUIAdvancedArenaLayer.searchOpponentinfoState = GameUIAdvancedArenaLayer.StateType.other
  else
    GameUIAdvancedArenaLayer.searchOpponentinfoState = GameUIAdvancedArenaLayer.StateType.first
  end
end
function GameUIAdvancedArenaLayer:SearchOpponent()
  self:SetSearchOpponentStates()
  self:SearchOpponentInfo()
end
function GameUIAdvancedArenaLayer:SearchOpponentInfo()
  NetMessageMgr:SendMsg(NetAPIList.wdc_match_req.Code, nil, GameUIAdvancedArenaLayer.SearchOpponentNetCallback, true)
end
function GameUIAdvancedArenaLayer.SearchOpponentNetCallback(msgType, content)
  if msgType == NetAPIList.wdc_match_ack.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      return true
    end
    GameUIAdvancedArenaLayer:SetSearchOpponent(content)
    return true
  end
  return false
end
function GameUIAdvancedArenaLayer:SetSearchOpponent(content)
  DebugOut("match_resout = ")
  DebugTable(content)
  GameUIAdvancedArenaLayer.searchOpponentinfo = content.challenge_list
  GameUIAdvancedArenaLayer:ShowPlayerDetail()
  if GameUIArena:GetFlashObject() then
    if GameUIAdvancedArenaLayer.searchOpponentinfoState == GameUIAdvancedArenaLayer.StateType.first then
      GameUIArena:GetFlashObject():InvokeASCallback("_root", "showSearchAnimation")
    else
      GameUIArena:GetFlashObject():InvokeASCallback("_root", "showSearchAnimation2")
    end
  end
  GameUIAdvancedArenaLayer:SetSearchOpponentStates()
  local buttonText = GameLoader:GetGameText("LC_MENU_GLC_CHANGE_OPPONENT")
  GameUIAdvancedArenaLayer:SetSearchOpponentButtonText(buttonText)
end
function GameUIAdvancedArenaLayer:SetSearchOpponentButtonText(txt)
  if GameUIArena:GetFlashObject() then
    GameUIArena:GetFlashObject():InvokeASCallback("_root", "setSearchButtonText", txt)
  end
end
function GameUIAdvancedArenaLayer:ShowPlayerDetail()
  DebugTable(GameUIAdvancedArenaLayer.searchOpponentinfo)
  if GameUIArena:GetFlashObject() then
    GameUIArena:GetFlashObject():InvokeASCallback("_root", "HideArenaOpponentLayer")
  end
  if #GameUIAdvancedArenaLayer.searchOpponentinfo == 1 then
    GameUIAdvancedArenaLayer:SetPlayerInfo(self.searchOpponentinfo[1], "_root.DianFeng.Pop_RivalAll.Pop_Rival")
  else
    GameUIAdvancedArenaLayer:SetPlayerInfo(self.searchOpponentinfo[1], "_root.DianFeng.Pop_RivalAll.Pop_Rival1")
    GameUIAdvancedArenaLayer:SetPlayerInfo(self.searchOpponentinfo[2], "_root.DianFeng.Pop_RivalAll.Pop_Rival2")
  end
end
function GameUIAdvancedArenaLayer:SetPlayerInfo(playerInfo, asObject)
  local _playerInfo = {}
  DebugOut("fortp")
  DebugTable(playerInfo)
  _playerInfo.username = playerInfo.defender.user_name
  _playerInfo.level = GameLoader:GetGameText("LC_MENU_Level") .. playerInfo.defender.level
  _playerInfo.force = playerInfo.defender.force
  if playerInfo.defender.icon == 0 or playerInfo.defender.icon == 1 then
    _playerInfo.avatar = GameUtils:GetPlayerAvatar(playerInfo.defender.sex, 0)
  else
    _playerInfo.avatar = GameDataAccessHelper:GetFleetAvatar(playerInfo.defender.icon, 0)
  end
  local extInfo = {}
  extInfo.rank_img = playerInfo.defender.rank_img
  extInfo.asObject = asObject
  _playerInfo.ranking = GameUtils:GetPlayerRankingInWdc(playerInfo.defender.rank_img, extInfo, GameUIAdvancedArenaLayer.DownloadOpponetRanking)
  _playerInfo.fleetcommands = ""
  _playerInfo.fleetAvatars = ""
  for k, v in ipairs(playerInfo.defender.fleets) do
    if v.identity ~= 1 then
      _playerInfo.fleetcommands = _playerInfo.fleetcommands .. GameDataAccessHelper:GetCommanderColorFrame(v.identity, v.level) .. "\001"
      _playerInfo.fleetAvatars = _playerInfo.fleetAvatars .. GameDataAccessHelper:GetFleetAvatar(v.identity, v.level) .. "\001"
    end
  end
  if 0 <= playerInfo.win_score then
    _playerInfo.winAddForce = "+" .. playerInfo.win_score
  else
    _playerInfo.winAddForce = playerInfo.win_score
  end
  if 0 <= playerInfo.lose_score then
    _playerInfo.faileForce = "+" .. playerInfo.lose_score
  else
    _playerInfo.faileForce = playerInfo.lose_score
  end
  _playerInfo.serverid = playerInfo.defender.server_id
  _playerInfo.userid = playerInfo.defender.user_id
  if GameUIArena:GetFlashObject() then
    GameUIArena:GetFlashObject():InvokeASCallback("_root", "setPlayerInfo_as", _playerInfo, asObject)
  end
end
function GameUIAdvancedArenaLayer.DownloadOpponetRanking(extInfo)
  if GameUIArena:GetFlashObject() then
    local rank = GameUtils:GetPlayerRankingInWdc(extInfo.rank_img, nil, nil)
    GameUIArena:GetFlashObject():InvokeASCallback("_root", "setOponnentPlayerRank", extInfo.asObject, rank)
  end
end
function GameUIAdvancedArenaLayer:SetEnemyInfo(serverid, userid)
  local enemyInfo = {}
  DebugOut("serverid = " .. serverid .. " userid = " .. userid)
  DebugTable(self.searchOpponentinfo)
  local currentEnemy = GameUIAdvancedArenaLayer:GetOpponentPlayer(serverid, userid)
  DebugOut("currentEnemy = ")
  DebugTable(currentEnemy)
  if currentEnemy.defender.icon == 0 or currentEnemy.defender.icon == 1 then
    enemyInfo.avatar = GameUtils:GetPlayerAvatar(currentEnemy.defender.sex, 0)
  else
    enemyInfo.avatar = GameDataAccessHelper:GetFleetAvatar(currentEnemy.defender.icon, 0)
  end
  enemyInfo.name = currentEnemy.defender.user_name
  enemyInfo.mSex = currentEnemy.defender.sex
  GameUIArena.enemyInfo = enemyInfo
end
function GameUIAdvancedArenaLayer:RequireMatrix()
  local data = LuaUtils:string_split(self.currentPlayer, "\001")
  local param = {
    server_id = data[1],
    user_id = data[2]
  }
  DebugOut("param = ")
  DebugTable(param)
  local currentForce = GameUtils.numberConversion(self.advanceArenaInfo.player_info.force)
  GameStateManager.GameStateFormation.enemyMatrixData = nil
  GameStateManager.GameStateFormation.isArenaEnter = true
  GameStateManager.GameStateFormation.ArenaForce = currentForce
  self:SetEnemyInfo(tonumber(data[1]), tonumber(data[2]))
  GameUIAdvancedArenaLayer.currentEnemyInfo = GameUIAdvancedArenaLayer:GetOpponentPlayer(tonumber(data[1]), tonumber(data[2]))
  GameObjectFormationBackground.force = GameUIAdvancedArenaLayer.currentEnemyInfo.defender.force
  GameStateManager.GameStateFormation.isNeedRequestMatrixInfo = false
  GameStateManager:SetCurrentGameState(GameStateManager.GameStateFormation)
end
function GameUIAdvancedArenaLayer:GetOpponentPlayer(serverid, userid)
  for k, v in ipairs(GameUIAdvancedArenaLayer.searchOpponentinfo) do
    if serverid == tonumber(v.defender.server_id) and userid == tonumber(v.defender.user_id) then
      return v
    end
  end
end
function GameUIAdvancedArenaLayer:StartBattle()
  NetMessageMgr:SendMsg(NetAPIList.wdc_fight_status_req.Code, nil, GameUIAdvancedArenaLayer.getProtectTimeStartBattleNetCallback, true, nil)
end
function GameUIAdvancedArenaLayer.getProtectTimeStartBattleNetCallback(msgType, content)
  if msgType == NetAPIList.wdc_fight_status_ack.Code then
    if content.protect_time > 0 then
      local text_title = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
      local info = GameLoader:GetGameText("LC_MENU_LEAGUE_ATTACK_INFO")
      GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
      GameUIMessageDialog:SetYesButton(GameUIAdvancedArenaLayer.StartBattleProtect)
      GameUIMessageDialog:SetNoButton(nil)
      GameUIMessageDialog:Display(text_title, info)
    else
      GameUIAdvancedArenaLayer:GotoBattle()
    end
    return true
  end
  return false
end
function GameUIAdvancedArenaLayer.StartBattleProtect()
  GameUIAdvancedArenaLayer:GotoBattle()
end
function GameUIAdvancedArenaLayer:GotoBattle()
  local param = {
    server_id = self.currentEnemyInfo.defender.server_id,
    user_id = self.currentEnemyInfo.defender.user_id,
    revenge_id = self.currentEnemyInfo.defender.revenge_id or 0
  }
  DebugOut("param")
  DebugTable(param)
  if GameStateManager:GetCurrentGameState().fightRoundData then
    GameStateManager:GetCurrentGameState().fightRoundData = nil
  end
  NetMessageMgr:SendMsg(NetAPIList.wdc_fight_req.Code, param, self.NetcallbackBattle, true)
end
function GameUIAdvancedArenaLayer.NetcallbackBattle(msgType, content)
  if msgType == NetAPIList.wdc_fight_ack.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      return true
    end
    GameUIAdvancedArenaLayer.battleResult = content
    if content.report then
      local reportData = content.report
      if #reportData.rounds == 0 and GameStateManager:GetCurrentGameState().fightRoundData then
        reportData.rounds = GameStateManager:GetCurrentGameState().fightRoundData
      end
      if reportData then
        GameStateBattlePlay.curBattleType = "advanceArenaLayer"
        GameStateBattlePlay:InitBattle(GameStateBattlePlay.MODE_PLAY, reportData)
        GameStateManager:SetCurrentGameState(GameStateBattlePlay)
        GameStateBattlePlay:RegisterOverCallback(GameUIAdvancedArenaLayer.PlayerBattleCallback, nil)
      end
    end
    return true
  end
  return false
end
function GameUIAdvancedArenaLayer.PlayerBattleCallback()
  GameUIBattleResult:LoadFlashObject()
  GameUIBattleResult:SetFightReport(GameUIAdvancedArenaLayer.battleResult.report, nil, nil)
  local itemIconS = ""
  local itemNameS = ""
  local itemNumberS = ""
  for k, v in ipairs(GameUIAdvancedArenaLayer.battleResult.awards) do
    local itemName = GameHelper:GetAwardTypeText(v.item_type, v.number)
    local icon = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(v, nil, nil)
    local number = GameHelper:GetAwardCount(v.item_type, v.number, v.no)
    itemIconS = itemIconS .. icon .. "\001"
    itemNameS = itemNameS .. itemName .. "\001"
    itemNumberS = itemNumberS .. number .. "\001"
  end
  local dataTable = {}
  dataTable.rank = GameUIAdvancedArenaLayer.battleResult.result.score_before
  dataTable.rank_after = GameUIAdvancedArenaLayer.battleResult.result.score_after
  dataTable.change_rank = GameUIAdvancedArenaLayer.battleResult.result.score_after - GameUIAdvancedArenaLayer.battleResult.result.score_before
  dataTable.isWin = GameUIAdvancedArenaLayer.battleResult.result.is_win
  dataTable.itemIconS = itemIconS
  dataTable.itemNameS = itemNameS
  dataTable.itemNumberS = itemNumberS
  dataTable.damage = GameUIAdvancedArenaLayer.battleResult.damage
  dataTable.suffer_damage = GameUIAdvancedArenaLayer.battleResult.suffer_damage
  GameUIBattleResult:ShowAdvancedWin(dataTable)
  GameStateManager:SetCurrentGameState(GameStateArena)
  GameUIAdvancedArenaLayer:CleanSearchInfo()
  GameStateArena:AddObject(GameUIBattleResult)
end
function GameUIAdvancedArenaLayer.GetMatrixCallback()
  if GameUIArena:GetFlashObject() then
    local index = FleetMatrix:getCurrentMartixIndex(FleetMatrix.MATRIX_TYPE_WORLD_CHAMPION)
    GameUIArena:GetFlashObject():InvokeASCallback("_root", "initWorldChampionMatrix", FleetMatrix.matrixs_in_as, index)
    GameUIAdvancedArenaLayer:UpdateWorldChampionFleetAvatar()
    local playerText = GameLoader:GetGameText("LC_MENU_ARENA_PLAYER_MEMBER_CHAR")
    GameUIArena:GetFlashObject():InvokeASCallback("_root", "showWorldChampionMatrix", playerText)
  end
end
function GameUIAdvancedArenaLayer:UpdateWorldChampionFleetAvatar()
  local curMatrixAvatarS = GameHelper:GetCurrentMatrixFleetAvatarAndRank(FleetMatrix.system_index)
  if GameUIArena:GetFlashObject() then
    GameUIArena:GetFlashObject():InvokeASCallback("_root", "showWorldChampionFleetsAvatarChangeFormation", curMatrixAvatarS)
  end
end
function GameUIAdvancedArenaLayer.CloseButtonYesCallback()
  GameUIAdvancedArenaLayer:CleanSearchInfo()
  GameUIAdvancedArenaLayer:MoveOutAnimation()
  NetMessageMgr:SendMsg(NetAPIList.wdc_enemy_release_req.Code, nil, GameUIAdvancedArenaLayer.releaseEnemyCallback, false, nil)
end
function GameUIAdvancedArenaLayer.releaseEnemyCallback(msgType, content)
  if content.api == NetAPIList.wdc_enemy_release_req.Code and msgType == NetAPIList.common_ack.Code then
    return true
  end
  return false
end
function GameUIAdvancedArenaLayer:MoveOutAnimation()
  if GameUIArena:GetFlashObject() then
    GameUIArena:GetFlashObject():InvokeASCallback("_root", "moveOutAdvancedLayer")
  end
end
function GameUIAdvancedArenaLayer:RequestBuyEnegyPrice()
  local param = {type = "wdc_supply"}
  NetMessageMgr:SendMsg(NetAPIList.supply_info_req.Code, param, GameUIAdvancedArenaLayer.RequestButEnegyCallback, true, nil)
end
function GameUIAdvancedArenaLayer.RequestButEnegyCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.supply_info_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      return true
    end
    return true
  end
  if msgType == NetAPIList.supply_info_ack.Code then
    local price = content.exchange_cost
    local count = content.count
    local textContentInfo = string.gsub(GameLoader:GetGameText("LC_MENU_LEAGUE_RECHARGE_RECOVERY_INFO"), "<number1>", price)
    local info = string.gsub(textContentInfo, "<number2>", count)
    local buyCallback = function()
      local param = {type = "wdc_supply"}
      NetMessageMgr:SendMsg(NetAPIList.supply_exchange_req.Code, param, GameUIAdvancedArenaLayer.BuyEnegy, false, nil)
    end
    GameUtils:CreditCostConfirm(info, buyCallback)
    return true
  end
  return false
end
function GameUIAdvancedArenaLayer.BuyEnegy(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.supply_exchange_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  return false
end
function GameUIAdvancedArenaLayer.updateWdcSupplyCallback()
  if GameUIAdvancedArenaLayer.advanceArenaInfo == nil then
    return
  end
  if GameUIArena:GetFlashObject() then
    local resourceCount = GameGlobalData:GetData("resource")
    local energyText = GameUIArena:GetEnergyColor(resourceCount.wdc_supply) .. "/" .. GameUIAdvancedArenaLayer.advanceArenaInfo.max_supply
    DebugOut("energyText == " .. energyText)
    GameUIArena:GetFlashObject():InvokeASCallback("_root", "updateWdcSupply", energyText)
    if GameUIAdvancedArenaLayer.advanceArenaInfo then
      local isCanSearch = true
      local matchCost = GameUIAdvancedArenaLayer.advanceArenaInfo.search_cost
      local costCurrent = resourceCount.wdc_supply
      local finalTime = GameUIAdvancedArenaLayer.advanceArenaInfo.player_info.end_time
      if tonumber(matchCost) > tonumber(costCurrent) or finalTime > 0 then
        isCanSearch = false
      end
      GameUIArena:GetFlashObject():InvokeASCallback("_root", "setSearchButtonState", isCanSearch)
    end
  end
end
function GameUIAdvancedArenaLayer:AndroidBack()
  if self.isShowStore then
    GameCommonGoodsList:AndroidBack()
  elseif self.isShowMatrix then
    if GameUIArena:GetFlashObject() then
      GameUIArena:GetFlashObject():InvokeASCallback("_root", "hideWorldChampionMatrix")
    end
  else
    GameUIAdvancedArenaLayer:OnFSCommand("close_advanced")
  end
end
return GameUIAdvancedArenaLayer
