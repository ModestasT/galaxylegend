local GameUIFairArenaMain = LuaObjectManager:GetLuaObject("GameUIFairArenaMain")
local GameStateFairArena = GameStateManager.GameStateFairArena
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
GameUIFairArenaMain.baseData = nil
function GameUIFairArenaMain:OnInitGame()
end
function GameUIFairArenaMain:Close()
  if GameUIFairArenaMain:GetFlashObject() then
    GameUIFairArenaMain:GetFlashObject():InvokeASCallback("_root", "MoveOut")
  end
end
function GameUIFairArenaMain:OnAddToGameState()
  DebugOut("GameUIFairArenaMain:OnAddToGameState")
  self:LoadFlashObject()
  self:GetFlashObject():InvokeASCallback("_root", "initBaseUI")
end
function GameUIFairArenaMain:InitBaseUI()
  local data = {}
  self:GetFlashObject():InvokeASCallback("_root", "initBaseUI", data)
end
function GameUIFairArenaMain:OnEraseFromGameState()
  self:UnloadFlashObject()
  GameUIFairArenaMain.baseData = nil
  collectgarbage("collect")
end
function GameUIFairArenaMain:OnFSCommand(cmd, arg)
  if cmd == "setBasicData" then
  end
end
