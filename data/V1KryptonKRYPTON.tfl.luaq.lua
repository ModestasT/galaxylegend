local KRYPTON = GameData.Krypton.KRYPTON
KRYPTON[50101] = {
  ITEM_TYPE = 50101,
  buff_value = 300,
  buff_type = 1
}
KRYPTON[50102] = {
  ITEM_TYPE = 50102,
  buff_value = 600,
  buff_type = 1
}
KRYPTON[50103] = {
  ITEM_TYPE = 50103,
  buff_value = 1200,
  buff_type = 1
}
KRYPTON[50104] = {
  ITEM_TYPE = 50104,
  buff_value = 2400,
  buff_type = 1
}
KRYPTON[50105] = {
  ITEM_TYPE = 50105,
  buff_value = 4800,
  buff_type = 1
}
KRYPTON[50201] = {
  ITEM_TYPE = 50201,
  buff_value = 150,
  buff_type = 2
}
KRYPTON[50202] = {
  ITEM_TYPE = 50202,
  buff_value = 300,
  buff_type = 2
}
KRYPTON[50203] = {
  ITEM_TYPE = 50203,
  buff_value = 600,
  buff_type = 2
}
KRYPTON[50204] = {
  ITEM_TYPE = 50204,
  buff_value = 1200,
  buff_type = 2
}
KRYPTON[50205] = {
  ITEM_TYPE = 50205,
  buff_value = 2400,
  buff_type = 2
}
KRYPTON[50301] = {
  ITEM_TYPE = 50301,
  buff_value = 150,
  buff_type = 3
}
KRYPTON[50302] = {
  ITEM_TYPE = 50302,
  buff_value = 300,
  buff_type = 3
}
KRYPTON[50303] = {
  ITEM_TYPE = 50303,
  buff_value = 600,
  buff_type = 3
}
KRYPTON[50304] = {
  ITEM_TYPE = 50304,
  buff_value = 1200,
  buff_type = 3
}
KRYPTON[50305] = {
  ITEM_TYPE = 50305,
  buff_value = 2400,
  buff_type = 3
}
KRYPTON[50401] = {
  ITEM_TYPE = 50401,
  buff_value = 150,
  buff_type = 4
}
KRYPTON[50402] = {
  ITEM_TYPE = 50402,
  buff_value = 300,
  buff_type = 4
}
KRYPTON[50403] = {
  ITEM_TYPE = 50403,
  buff_value = 600,
  buff_type = 4
}
KRYPTON[50404] = {
  ITEM_TYPE = 50404,
  buff_value = 1200,
  buff_type = 4
}
KRYPTON[50405] = {
  ITEM_TYPE = 50405,
  buff_value = 2400,
  buff_type = 4
}
KRYPTON[50501] = {
  ITEM_TYPE = 50501,
  buff_value = 150,
  buff_type = 5
}
KRYPTON[50502] = {
  ITEM_TYPE = 50502,
  buff_value = 300,
  buff_type = 5
}
KRYPTON[50503] = {
  ITEM_TYPE = 50503,
  buff_value = 600,
  buff_type = 5
}
KRYPTON[50504] = {
  ITEM_TYPE = 50504,
  buff_value = 1200,
  buff_type = 5
}
KRYPTON[50505] = {
  ITEM_TYPE = 50505,
  buff_value = 2400,
  buff_type = 5
}
KRYPTON[50601] = {
  ITEM_TYPE = 50601,
  buff_value = 150,
  buff_type = 6
}
KRYPTON[50602] = {
  ITEM_TYPE = 50602,
  buff_value = 300,
  buff_type = 6
}
KRYPTON[50603] = {
  ITEM_TYPE = 50603,
  buff_value = 600,
  buff_type = 6
}
KRYPTON[50604] = {
  ITEM_TYPE = 50604,
  buff_value = 1200,
  buff_type = 6
}
KRYPTON[50605] = {
  ITEM_TYPE = 50605,
  buff_value = 2400,
  buff_type = 6
}
KRYPTON[50701] = {
  ITEM_TYPE = 50701,
  buff_value = 150,
  buff_type = 7
}
KRYPTON[50702] = {
  ITEM_TYPE = 50702,
  buff_value = 300,
  buff_type = 7
}
KRYPTON[50703] = {
  ITEM_TYPE = 50703,
  buff_value = 600,
  buff_type = 7
}
KRYPTON[50704] = {
  ITEM_TYPE = 50704,
  buff_value = 1200,
  buff_type = 7
}
KRYPTON[50705] = {
  ITEM_TYPE = 50705,
  buff_value = 2400,
  buff_type = 7
}
KRYPTON[50801] = {
  ITEM_TYPE = 50801,
  buff_value = 150,
  buff_type = 8
}
KRYPTON[50802] = {
  ITEM_TYPE = 50802,
  buff_value = 300,
  buff_type = 8
}
KRYPTON[50803] = {
  ITEM_TYPE = 50803,
  buff_value = 600,
  buff_type = 8
}
KRYPTON[50804] = {
  ITEM_TYPE = 50804,
  buff_value = 1200,
  buff_type = 8
}
KRYPTON[50805] = {
  ITEM_TYPE = 50805,
  buff_value = 2400,
  buff_type = 8
}
KRYPTON[50901] = {
  ITEM_TYPE = 50901,
  buff_value = 8,
  buff_type = 9
}
KRYPTON[50902] = {
  ITEM_TYPE = 50902,
  buff_value = 15,
  buff_type = 9
}
KRYPTON[50903] = {
  ITEM_TYPE = 50903,
  buff_value = 30,
  buff_type = 9
}
KRYPTON[50904] = {
  ITEM_TYPE = 50904,
  buff_value = 45,
  buff_type = 9
}
KRYPTON[50905] = {
  ITEM_TYPE = 50905,
  buff_value = 60,
  buff_type = 9
}
KRYPTON[51001] = {
  ITEM_TYPE = 51001,
  buff_value = 53,
  buff_type = 10
}
KRYPTON[51002] = {
  ITEM_TYPE = 51002,
  buff_value = 105,
  buff_type = 10
}
KRYPTON[51003] = {
  ITEM_TYPE = 51003,
  buff_value = 210,
  buff_type = 10
}
KRYPTON[51004] = {
  ITEM_TYPE = 51004,
  buff_value = 315,
  buff_type = 10
}
KRYPTON[51005] = {
  ITEM_TYPE = 51005,
  buff_value = 420,
  buff_type = 10
}
KRYPTON[51101] = {
  ITEM_TYPE = 51101,
  buff_value = 8,
  buff_type = 11
}
KRYPTON[51102] = {
  ITEM_TYPE = 51102,
  buff_value = 15,
  buff_type = 11
}
KRYPTON[51103] = {
  ITEM_TYPE = 51103,
  buff_value = 30,
  buff_type = 11
}
KRYPTON[51104] = {
  ITEM_TYPE = 51104,
  buff_value = 45,
  buff_type = 11
}
KRYPTON[51105] = {
  ITEM_TYPE = 51105,
  buff_value = 60,
  buff_type = 11
}
KRYPTON[51201] = {
  ITEM_TYPE = 51201,
  buff_value = 8,
  buff_type = 12
}
KRYPTON[51202] = {
  ITEM_TYPE = 51202,
  buff_value = 15,
  buff_type = 12
}
KRYPTON[51203] = {
  ITEM_TYPE = 51203,
  buff_value = 30,
  buff_type = 12
}
KRYPTON[51204] = {
  ITEM_TYPE = 51204,
  buff_value = 45,
  buff_type = 12
}
KRYPTON[51205] = {
  ITEM_TYPE = 51205,
  buff_value = 60,
  buff_type = 12
}
KRYPTON[51301] = {
  ITEM_TYPE = 51301,
  buff_value = 8,
  buff_type = 13
}
KRYPTON[51302] = {
  ITEM_TYPE = 51302,
  buff_value = 15,
  buff_type = 13
}
KRYPTON[51303] = {
  ITEM_TYPE = 51303,
  buff_value = 30,
  buff_type = 13
}
KRYPTON[51304] = {
  ITEM_TYPE = 51304,
  buff_value = 45,
  buff_type = 13
}
KRYPTON[51305] = {
  ITEM_TYPE = 51305,
  buff_value = 60,
  buff_type = 13
}
KRYPTON[51401] = {
  ITEM_TYPE = 51401,
  buff_value = 8,
  buff_type = 14
}
KRYPTON[51402] = {
  ITEM_TYPE = 51402,
  buff_value = 15,
  buff_type = 14
}
KRYPTON[51403] = {
  ITEM_TYPE = 51403,
  buff_value = 30,
  buff_type = 14
}
KRYPTON[51404] = {
  ITEM_TYPE = 51404,
  buff_value = 45,
  buff_type = 14
}
KRYPTON[51405] = {
  ITEM_TYPE = 51405,
  buff_value = 60,
  buff_type = 14
}
KRYPTON[51501] = {
  ITEM_TYPE = 51501,
  buff_value = 15,
  buff_type = 15
}
KRYPTON[51502] = {
  ITEM_TYPE = 51502,
  buff_value = 30,
  buff_type = 15
}
KRYPTON[51503] = {
  ITEM_TYPE = 51503,
  buff_value = 60,
  buff_type = 15
}
KRYPTON[51504] = {
  ITEM_TYPE = 51504,
  buff_value = 120,
  buff_type = 15
}
KRYPTON[51505] = {
  ITEM_TYPE = 51505,
  buff_value = 240,
  buff_type = 15
}
KRYPTON[51601] = {
  ITEM_TYPE = 51601,
  buff_value = 3,
  buff_type = 16
}
KRYPTON[51602] = {
  ITEM_TYPE = 51602,
  buff_value = 6,
  buff_type = 16
}
KRYPTON[51603] = {
  ITEM_TYPE = 51603,
  buff_value = 10,
  buff_type = 16
}
KRYPTON[51604] = {
  ITEM_TYPE = 51604,
  buff_value = 18,
  buff_type = 16
}
KRYPTON[51605] = {
  ITEM_TYPE = 51605,
  buff_value = 30,
  buff_type = 16
}
KRYPTON[51701] = {
  ITEM_TYPE = 51701,
  buff_value = 8,
  buff_type = 17
}
KRYPTON[51702] = {
  ITEM_TYPE = 51702,
  buff_value = 15,
  buff_type = 17
}
KRYPTON[51703] = {
  ITEM_TYPE = 51703,
  buff_value = 30,
  buff_type = 17
}
KRYPTON[51704] = {
  ITEM_TYPE = 51704,
  buff_value = 45,
  buff_type = 17
}
KRYPTON[51705] = {
  ITEM_TYPE = 51705,
  buff_value = 60,
  buff_type = 17
}
KRYPTON[52001] = {
  ITEM_TYPE = 52001,
  buff_value = 30,
  buff_type = 20
}
KRYPTON[52002] = {
  ITEM_TYPE = 52002,
  buff_value = 60,
  buff_type = 20
}
KRYPTON[52003] = {
  ITEM_TYPE = 52003,
  buff_value = 120,
  buff_type = 20
}
KRYPTON[52004] = {
  ITEM_TYPE = 52004,
  buff_value = 240,
  buff_type = 20
}
KRYPTON[52005] = {
  ITEM_TYPE = 52005,
  buff_value = 480,
  buff_type = 20
}
