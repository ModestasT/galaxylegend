local GameUIActivityNew = LuaObjectManager:GetLuaObject("GameUIActivityNew")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
local GameUIWDStuff = LuaObjectManager:GetLuaObject("GameUIWDStuff")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameUIKrypton = LuaObjectManager:GetLuaObject("GameUIKrypton")
local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
local QuestTutorialComboGacha = TutorialQuestManager.QuestTutorialComboGacha
local QuestTutorialComboGachaGetHero = TutorialQuestManager.QuestTutorialComboGachaGetHero
GameUIcomboGacha = {}
GameUIcomboGacha.mFlashObj = nil
GameUIcomboGacha.isOpenComboGacha = false
GameUIcomboGacha.GachaChestInfoStr = nil
GameUIcomboGacha.comboGachaId = -1
GameUIcomboGacha.resourceId = -1
local currentMainItem, gateWayInfo, comboGachaInfo, callbackFunc
GameUIcomboGacha.showTutorial = false
local Fakecontent = {
  state = {
    [1] = {
      name = "aaa.s65",
      item = {
        number = 117,
        level = 0,
        no = 1,
        item_type = "fleet"
      },
      times = 0
    }
  }
}
function GameUIcomboGacha:Init(flashObj)
  self.mFlashObj = flashObj
end
function GameUIcomboGacha:clearData()
  gateWayInfo = nil
  comboGachaInfo = nil
  GameUIcomboGacha.isOpenComboGacha = false
  GameUIcomboGacha.GachaChestInfoStr = nil
  currentMainItem = nil
  callbackFunc = nil
end
if AutoUpdate.isAndroidDevice then
  function GameUIcomboGacha.OnAndroidBack()
    GameUIcomboGacha:OnFSCommand("comboClose")
  end
end
function GameUIcomboGacha:checkNetGateWayInfo()
  DebugOut("checkNetGateWayInfo", GameUIcomboGacha.resourceId)
  local requestType = GameUIActivityNew.getGatewayRequestString(GameUIActivityNew.category.TYPE_DNA, GameUIcomboGacha.resourceId)
  local requestParam = {
    lang = GameSettingData.Save_Lang
  }
  DebugOut("requestType checkNetGateWayInfo", requestType)
  GameUtils:HTTP_SendRequest(requestType, requestParam, GameUIcomboGacha.checkNetGateWayInfoCallBack, true, GameUtils.httpRequestFailedCallback, "GET", "notencrypt")
end
function GameUIcomboGacha.checkNetGateWayInfoCallBack(responseContent)
  DebugOut("GameUIcomboGacha checkNetGateWayInfoCallBack")
  gateWayInfo = responseContent
  DebugOut(GameUIcomboGacha.isOpenComboGacha)
  if not GameUIcomboGacha.isOpenComboGacha then
    GameUIcomboGacha:tryOpenComboGacha()
  else
    GameUIcomboGacha:RefreshGateWayUI()
  end
  return true
end
function GameUIcomboGacha:RequestComboGachaInfo(callback)
  if callback ~= nil then
    callbackFunc = callback
  else
    callbackFunc = nil
  end
  local requestParam = {
    activity_id = GameUIcomboGacha.comboGachaId
  }
  NetMessageMgr:SendMsg(NetAPIList.activity_dna_req.Code, requestParam, GameUIcomboGacha.RequestComboGachaInfoCallback, true, nil)
  DebugOut("RequestComboGachaInfo", GameUIcomboGacha.comboGachaId)
end
function GameUIcomboGacha.RequestComboGachaInfoCallback(msgType, content)
  DebugOut("RequestComboGachaInfoCallback", msgType)
  if msgType == NetAPIList.activity_dna_ack.Code then
    DebugTable(content)
    comboGachaInfo = content
    GameUIcomboGacha.resourceId = content.kid_id
    DebugOut(GameUIcomboGacha.isOpenComboGacha)
    GameUIActivityNew.setHasOpen(GameUIActivityNew.category.TYPE_DNA, GameUIcomboGacha.comboGachaId, true)
    if not GameUIcomboGacha.isOpenComboGacha then
      GameUIcomboGacha:tryOpenComboGacha()
    else
      GameUIcomboGacha:RefreshUI()
    end
    if callbackFunc ~= nil then
      callbackFunc()
    end
    return true
  elseif msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.activity_dna_req.Code then
    GameUIcomboGacha.resourceId = content.kid_id
    GameUIActivityNew.setHasOpen(GameUIActivityNew.category.TYPE_DNA, GameUIcomboGacha.comboGachaId, false)
    if content.code == 101006 then
      DebugOut("GameUIcomboGacha 101006 \228\184\141\229\156\168\230\180\187\229\138\168\230\151\182\233\151\180\229\134\133")
    else
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    if callbackFunc ~= nil then
      callbackFunc()
    end
    return true
  end
  return false
end
function GameUIcomboGacha:tryOpenComboGacha()
  DebugOut("tryOpenComboGacha")
  if gateWayInfo and comboGachaInfo then
    local time = comboGachaInfo.dna_charge_status[0]
    DebugOut("time", time)
    self.mFlashObj:InvokeASCallback("_root", "lua2fs_setLatestCumulativeTime", time)
    self:RefreshUI()
    self:RefreshGateWayUI()
    self:OpenComboGacha()
    GameUIcomboGacha.RefreshResource()
  else
    DebugOut("gateWayInfo", gateWayInfo)
    DebugOut("comboGachaInfo", comboGachaInfo)
  end
end
function GameUIcomboGacha:OpenComboGacha()
  DebugOut("OpenComboGacha", self.mFlashObj)
  GameGlobalData:RegisterDataChangeCallback("resource", GameUIcomboGacha.RefreshResource)
  if self.mFlashObj then
    DebugOut("pre lua2fs_openComboGacha")
    self.mFlashObj:InvokeASCallback("_root", "lua2fs_openComboGacha")
  end
  GameUIcomboGacha.isOpenComboGacha = true
end
function GameUIcomboGacha:closeComboGacha()
  if self.mFlashObj then
    self.mFlashObj:InvokeASCallback("_root", "lua2fs_closeComboGacha")
  end
  GameUIcomboGacha:clearData()
end
function GameUIcomboGacha:RefreshGateWayUI()
  DebugOut("RefreshGateWayUI")
  self.mFlashObj:InvokeASCallback("_root", "lua2fs_setProductContext", gateWayInfo.desc1, gateWayInfo.desc2, gateWayInfo.desc3, gateWayInfo.heroName)
  GameUIcomboGacha:updateBannerInfo(gateWayInfo.eventIconPicUrl1)
  if gateWayInfo.eventIconPicUrl2 == "" or gateWayInfo.eventIconPicUrl2 == nil then
    DebugOut("hide CutOffBanner")
    self.mFlashObj:InvokeASCallback("_root", "lua2fs_setCutOffBanner", false)
  else
    self.mFlashObj:InvokeASCallback("_root", "lua2fs_setCutOffBanner", true)
    GameUIcomboGacha:updateCutoffInfo(gateWayInfo.eventIconPicUrl2)
  end
end
function GameUIcomboGacha:RefreshUI()
  DebugOut("RefreshUI")
  if comboGachaInfo.charge_next_condition == true then
    local callback = function()
      GameUIcomboGacha:checkNextItem()
    end
    local info = GameLoader:GetGameText("LC_MENU_COMBO_GACHA_SWITCHING")
    local text_title = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
    DebugOut("GameUIcomboGacha:GameUIMessageDialog")
    GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.OK)
    GameUIMessageDialog:SetOkButton(callback)
    GameUIMessageDialog:Display(text_title, info)
  end
  local awardItemArray = comboGachaInfo.activity_award
  local purchaseTimeArray = comboGachaInfo.dna_charge_status
  if #awardItemArray > 1 then
    self:startAwardShowAnimation(awardItemArray, purchaseTimeArray)
  else
    currentMainItem = comboGachaInfo.award_id
    local item_name = GameHelper:GetAwardNameText(comboGachaInfo.item_id.item_type, comboGachaInfo.item_id.number)
    local AwardString = ""
    local firstItem = ""
    local tip_text = ""
    if #awardItemArray > 0 then
      firstItem = awardItemArray[#awardItemArray]
      local param = {}
      table.insert(param, firstItem)
      ItemBox:ShowRewardBox(param)
    end
    local purchaseTime = 0
    if #purchaseTimeArray > 0 then
      purchaseTime = purchaseTimeArray[#purchaseTimeArray]
    end
    local cutoffPrice = comboGachaInfo.discount
    DebugOut("tip_text", tip_text)
    if comboGachaInfo.kid_id ~= self.comboGachaId then
      comboGachaInfo.dna_need = 0
    end
    if GameUIActivityNew.activityContentIndex == GameUIActivityNew.combogachaItemid and QuestTutorialComboGacha:IsActive() then
      GameUIcomboGacha.showTutorial = true
    else
      GameUIcomboGacha.showTutorial = false
    end
    local lang = "en"
    if GameSettingData and GameSettingData.Save_Lang then
      lang = GameSettingData.Save_Lang
      if string.find(lang, "ru") == 1 then
        lang = "ru"
      else
        lang = "en"
      end
    end
    self.mFlashObj:InvokeASCallback("_root", "lua2fs_setPurchaseInfo", item_name, comboGachaInfo.dna_had, comboGachaInfo.dna_need, 1, comboGachaInfo.price, comboGachaInfo.discount / 100 * comboGachaInfo.price, purchaseTime, comboGachaInfo.dna_charge_max, comboGachaInfo.charge_award_tag, nil, GameLoader:GetGameText("LC_MENU_NEWS_DOWNLOAD_CHAR"), GameUIcomboGacha.showTutorial, lang)
  end
end
function GameUIcomboGacha:RefreshUIAfterAnimation()
  local AwardString = ""
  local firstItem = ""
  local awardItemArray = comboGachaInfo.activity_award
  local purchaseTimeArray = comboGachaInfo.dna_charge_status
  local tip_text = ""
  currentMainItem = comboGachaInfo.award_id
  local item_name = GameHelper:GetAwardNameText(comboGachaInfo.item_id.item_type, comboGachaInfo.item_id.number)
  local purchaseTime = 0
  if #purchaseTimeArray > 0 then
    purchaseTime = purchaseTimeArray[#awardItemArray]
  end
  local cutoffPrice = comboGachaInfo.discount
  if comboGachaInfo.kid_id ~= self.comboGachaId then
    comboGachaInfo.dna_need = 0
  end
  if GameUIActivityNew.activityContentIndex == GameUIActivityNew.combogachaItemid and QuestTutorialComboGacha:IsActive() then
    GameUIcomboGacha.showTutorial = true
  else
    GameUIcomboGacha.showTutorial = false
  end
  local lang = "en"
  if GameSettingData and GameSettingData.Save_Lang then
    lang = GameSettingData.Save_Lang
    if string.find(lang, "ru") == 1 then
      lang = "ru"
    else
      lang = "en"
    end
  end
  self.mFlashObj:InvokeASCallback("_root", "lua2fs_setPurchaseInfo", item_name, comboGachaInfo.dna_had, comboGachaInfo.dna_need, 1, comboGachaInfo.price, comboGachaInfo.discount / 100 * comboGachaInfo.price, purchaseTime, comboGachaInfo.dna_charge_max, comboGachaInfo.charge_award_tag, nil, GameLoader:GetGameText("LC_MENU_NEWS_DOWNLOAD_CHAR"), GameUIcomboGacha.showTutorial, lang)
end
function GameUIcomboGacha:startAwardShowAnimation(awardItemArray, purchaseTimeArray)
  DebugOut("startAwardShowAnimation")
  if #awardItemArray ~= #purchaseTimeArray then
    DebugOut("error: #awardItemArray ~= #purchaseTimeArray,skip animation")
    self:OnFSCommand("showAnimationOver", nil)
    return
  else
    DebugTable(awardItemArray)
    DebugTable(purchaseTimeArray)
    awardItemString = ""
    local param = {}
    for k1, v1 in ipairs(awardItemArray) do
      local canInsert = true
      for k2, v2 in ipairs(param) do
        if v1.item_type == v2.item_type then
          if GameHelper:IsResource(v1.item_type) then
            v2.number = v2.number + v1.number
            canInsert = false
            break
          end
          if v1.number == v2.number then
            v2.no = v2.no + v1.no
            canInsert = false
          end
          break
        end
      end
      if canInsert then
        table.insert(param, v1)
      end
    end
    DebugOut("showreward in Combogacha ")
    DebugTable(param)
    ItemBox:ShowRewardBox(param)
  end
end
function GameUIcomboGacha:getBuyTenAwardText(ItemArrary)
  local AwardStringTable = {}
  for key, value in pairs(ItemArrary) do
    local AwardString = GameHelper:GetAwardNameText(value.item_type, value.number)
    local count = GameHelper:GetAwardCount(value.item_type, value.number, value.no)
    local award_text = AwardString .. " x" .. count
    local weight = 0
    if GameHelper:IsResource(value.item_type) then
      weight = 1
    else
      weight = 2
    end
    local hasflag = false
    for i, j in pairs(AwardStringTable) do
      if award_text == j.text then
        j.number = j.number + 1
        hasflag = true
      end
    end
    if hasflag == false then
      local awardtext = {
        text = award_text,
        number = 1,
        mweight = weight
      }
      table.insert(AwardStringTable, awardtext)
    end
  end
  table.sort(AwardStringTable, function(v1, v2)
    return v1.mweight > v2.mweight
  end)
  local text = ""
  for k, v in pairs(AwardStringTable) do
    local itemText1 = "<font color='#FF9900'>" .. tostring("[" .. v.text .. "]") .. "</font>"
    local itemText2 = "<font color='#BFFFFF'>" .. tostring("x" .. v.number) .. "</font>"
    text = text .. itemText1 .. itemText2 .. " "
  end
  return text
end
function GameUIcomboGacha:checkNextItem()
  DebugOut("checkNextItem")
  local requestParam = {
    activity_id = GameUIcomboGacha.comboGachaId
  }
  NetMessageMgr:SendMsg(NetAPIList.akt_dna_next_stg_req.Code, requestParam, GameUIcomboGacha.checkNextItemCallBack, true, nil)
end
function GameUIcomboGacha.checkNextItemCallBack(msgType, content)
  DebugOut("checkNextItemCallBack", msgType)
  if msgType == NetAPIList.activity_dna_ack.Code then
    DebugTable(content)
    comboGachaInfo = content
    GameUIcomboGacha.resourceId = content.kid_id
    DebugOut("GameUIcomboGacha.comboGachaId", GameUIcomboGacha.comboGachaId)
    GameUIcomboGacha:checkNetGateWayInfo()
    GameUIcomboGacha:RefreshUI()
    return true
  end
  return false
end
function GameUIcomboGacha:RequestPurchase(time)
  DebugOut("RequestPurchase", time, GameUIcomboGacha.comboGachaId)
  local requestParam = {
    activity_id = GameUIcomboGacha.comboGachaId,
    charge_time = time
  }
  NetMessageMgr:SendMsg(NetAPIList.activity_dna_charge_req.Code, requestParam, GameUIcomboGacha.RequestPurchaseCallBack, true, nil)
end
function GameUIcomboGacha.RequestPurchaseCallBack(msgType, content)
  DebugOut("RequestPurchaseCallBack", msgType)
  if msgType == NetAPIList.activity_dna_ack.Code then
    DebugTable(content)
    comboGachaInfo = content
    GameUIcomboGacha.resourceId = content.kid_id
    GameUIcomboGacha:RefreshUI()
    if GameUIActivityNew.activityContentIndex == GameUIActivityNew.combogachaItemid and QuestTutorialComboGacha:IsActive() and comboGachaInfo.dna_charge_status[1] then
      AddFlurryEvent("Gacha_Buy_" .. tostring(comboGachaInfo.dna_charge_status[1]), {}, 1)
    end
    return true
  elseif msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.activity_dna_charge_req.Code then
    local GameVip = LuaObjectManager:GetLuaObject("GameVip")
    GameVip:CheckIsNeedShowVip(content.code)
    return true
  end
  return false
end
function GameUIcomboGacha:RequestClaimAward()
  DebugOut("claimAwardRequest")
  local requestParam = {
    activity_id = GameUIcomboGacha.comboGachaId
  }
  NetMessageMgr:SendMsg(NetAPIList.akt_dna_get_award_req.Code, requestParam, GameUIcomboGacha.RequestClaimAwardCallBack, true, nil)
  if GameUIActivityNew.activityContentIndex == GameUIActivityNew.combogachaItemid and QuestTutorialComboGacha:IsActive() then
    AddFlurryEvent("Gacha_Receive", {}, 1)
  end
end
function GameUIcomboGacha.RequestClaimAwardCallBack(msgType, content)
  DebugOut("RequestClaimAwardCallBack", msgType)
  if msgType == NetAPIList.activity_dna_ack.Code then
    DebugTable(content)
    comboGachaInfo = content
    GameUIcomboGacha.resourceId = content.kid_id
    GameUIcomboGacha:RefreshUI()
    return true
  elseif msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.activity_dna_req.Code then
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  end
  return false
end
function GameUIcomboGacha:OnFSCommand(cmd, arg)
  DebugOut("GameUIcomboGacha:OnFSCommand", cmd, arg)
  if cmd == "comboBuyOne" then
    self:RequestPurchase(1)
  elseif cmd == "comboBuyTen" then
    self:RequestPurchase(10)
  elseif cmd == "comboGetAward" then
    TutorialQuestManager:GetComboGachaSendHero(3)
    self:RequestClaimAward()
  elseif cmd == "comboClose" then
    self:closeComboGacha()
  elseif cmd == "showAnimationOver" then
    self:RefreshUIAfterAnimation()
  elseif cmd == "mainScreenClicked" then
    self:showItemDetil(currentMainItem, currentMainItem.item_type)
  end
end
function GameUIcomboGacha.ComboGachaCallback(msgType, content)
  DebugOut("GameUIcomboGacha-ComboGachaCallback.content:", content)
  if msgType == NetAPIList.combo_guide_ack.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  return false
end
function GameUIcomboGacha:showCostConfirmPanel(costPrice, strID, messageboxHandler)
  local info_content = GameLoader:GetGameText(strID)
  info_content = string.format(info_content, costPrice)
  GameUtils:CreditCostConfirm(info_content, messageboxHandler)
end
function GameUIcomboGacha.RefreshResource()
  if GameUIcomboGacha.isOpenComboGacha then
    local resource = GameGlobalData:GetData("resource")
    GameUIcomboGacha.mFlashObj:InvokeASCallback("_root", "lua2fs_RefreshResource", GameUtils.numberConversion(resource.money), GameUtils.numberConversion(resource.credit), GameUtils.numberConversion(resource.kenergy))
  end
end
function GameUIcomboGacha:showItemDetil(item_, item_type)
  DebugOut("GameUIcomboGacha:showItemDetil", item_type)
  if GameHelper:IsResource(item_type) then
    return
  end
  local item = item_
  if item_type == "krypton" then
    if item.level == 0 then
      item.level = 1
    end
    local detail = GameUIKrypton:TryQueryKryptonDetail(item.number, item.level, function(msgType, content)
      local ret = GameUIKrypton.QueryKryptonDetailCallBack(msgType, content)
      if ret then
        local tmp = GameGlobalData:GetKryptonDetail(item.number, item.level)
        if tmp == nil then
          return false
        end
        ItemBox:SetKryptonBox(tmp, item.number)
        local operationTable = {}
        operationTable.btnUnloadVisible = false
        operationTable.btnUpgradeVisible = false
        operationTable.btnUseVisible = false
        operationTable.btnDecomposeVisible = false
        ItemBox:ShowKryptonBox(320, 240, operationTable)
      end
      return ret
    end)
    if detail == nil then
    else
      ItemBox:SetKryptonBox(detail, item.number)
      local operationTable = {}
      operationTable.btnUnloadVisible = false
      operationTable.btnUpgradeVisible = false
      operationTable.btnUseVisible = false
      operationTable.btnDecomposeVisible = false
      ItemBox:ShowKryptonBox(320, 240, operationTable)
    end
  end
  if item_type == "item" then
    GameUIcomboGacha:checkIfNeedDynamicDownload(item_)
    item.cnt = 1
    ItemBox:showItemBox("Item", item, tonumber(item.number), 320, 480, 200, 200, "", nil, "", nil)
  end
  if item_type == "fleet" then
    ItemBox:ShowCommanderDetail2(tonumber(item.number))
  end
end
function GameUIcomboGacha:checkIfNeedDynamicDownload(gameItem)
  DebugOut("checkIfNeedDynamicDownload")
  local fullFileName = DynamicResDownloader:GetFullName(gameItem.number .. ".png", DynamicResDownloader.resType.PIC)
  local localPath = "data2/" .. fullFileName
  if DynamicResDownloader:IsDynamicStuffByGameItem(gameItem) then
    DebugOut("IsDynamicStuffByGameItem true")
    if not DynamicResDownloader:IfResExsit(localPath) then
      frame = "temp"
      local itemID = gameItem.number
      local resName = itemID .. ".png"
      local extInfo = {}
      extInfo.gameItem = gameItem
      extInfo.localPath = localPath
      DebugOut("prepare to download", extInfo.localPath)
      DynamicResDownloader:AddDynamicRes(resName, DynamicResDownloader.resType.PIC, extInfo, GameUIcomboGacha.dynamicDownloaderCallback)
    else
      DebugOut("get local resource")
    end
  end
end
function GameUIcomboGacha.dynamicDownloaderCallback(extInfo)
end
function GameUIcomboGacha:SetGachaChestOpenInfo(titleStr)
  if self.mFlashObj then
    self.mFlashObj:InvokeASCallback("_root", "setRollTextInfo", titleStr)
  end
end
function GameUIcomboGacha:Update(dt)
  local flash_obj = self.mFlashObj
  if flash_obj then
    flash_obj:InvokeASCallback("_root", "updateRollTextPos", dt)
  else
    DebugOut("error flash_obj", flash_obj)
  end
end
function GameUIcomboGacha:CreateGachaChestInfo(ChestInfos)
  self.GachaChestInfoStr = ""
  infoNum = math.min(#ChestInfos.state, 5)
  for i = 1, infoNum do
    local tName = GameUtils:GetUserDisplayName(ChestInfos.state[i].name)
    local time = ChestInfos.state[i].times
    local tChestName = GameHelper:GetAwardTypeText(ChestInfos.state[i].item.item_type, ChestInfos.state[i].item.number)
    local tChestNum = GameHelper:GetAwardCount(ChestInfos.state[i].item.item_type, ChestInfos.state[i].item.number, ChestInfos.state[i].item.no)
    local str = GameLoader:GetGameText("LC_MENU_COMBO_GACHA_BROADCAST_TEXT")
    str = string.gsub(str, "<item_name>", tChestName)
    str = string.gsub(str, "<point>", "" .. tChestNum)
    str = string.format(str, tName)
    self.GachaChestInfoStr = self.GachaChestInfoStr .. str .. "    "
  end
  DebugOut("GameUIcomboGacha:CreateGachaChestInfo", self.GachaChestInfoStr)
end
function GameUIcomboGacha.RecentChestNtfHandler(content)
  DebugOut("GameUIcomboGacha:RecentChestNtfHandler")
  DebugTable(content)
  GameUIcomboGacha:CreateGachaChestInfo(content)
  GameUIcomboGacha:SetGachaChestOpenInfo(GameUIcomboGacha.GachaChestInfoStr)
end
function GameUIcomboGacha:updateBannerInfo(pngName)
  DebugOut("GameUIcomboGacha:updateBannerInfo", pngName)
  if not pngName then
    DebugOut("error pngName")
    return
  end
  local localPath = "data2/" .. pngName
  local replaceName = "combo_gacha_1.png"
  if pngName then
    if DynamicResDownloader:IfResExsit(localPath) or ext.IsFileExist and ext.IsFileExist(localPath) then
      DebugOut("get local resource")
      if self.mFlashObj then
        ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
        self.mFlashObj:ReplaceTexture(replaceName, pngName)
      end
    else
      DebugOut("GameUIcomboGacha:prepare to download banner")
      local extInfo = {}
      extInfo.pngName = pngName
      extInfo.localPath = localPath
      extInfo.replaceName = replaceName
      DynamicResDownloader:AddDynamicRes(pngName, DynamicResDownloader.resType.FESTIVAL_BANNER, extInfo, GameUIcomboGacha.bannerDownloaderCallback)
    end
  end
end
function GameUIcomboGacha:updateCutoffInfo(pngName)
  DebugOut("GameUIcomboGacha:updateCutoffInfo", pngName)
  local localPath = "data2/" .. pngName
  local replaceName = "store_sale_bg2.png"
  if DynamicResDownloader:IfResExsit(localPath) then
    DebugOut("get local resource")
    if self.mFlashObj then
      ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
      self.mFlashObj:ReplaceTexture(replaceName, pngName)
    end
  else
    DebugOut("GameUIcomboGacha:prepare to download cutoff pic")
    local extInfo = {}
    extInfo.pngName = pngName
    extInfo.localPath = localPath
    extInfo.replaceName = replaceName
    DynamicResDownloader:AddDynamicRes(pngName, DynamicResDownloader.resType.FESTIVAL_BANNER, extInfo, GameUIcomboGacha.cutoffPicDownloaderCallback)
  end
end
function GameUIcomboGacha.bannerDownloaderCallback(extInfo)
  DebugOut("GameUIcomboGacha.bannerDownloaderCallback")
  if DynamicResDownloader:IfResExsit(extInfo.localPath) and GameUIcomboGacha.mFlashObj then
    GameUIcomboGacha.mFlashObj:ReplaceTexture(extInfo.replaceName, extInfo.pngName)
    if extInfo.callback then
      extInfo.callback()
    end
  else
    DebugOut("download failed")
  end
end
function GameUIcomboGacha.cutoffPicDownloaderCallback(extInfo)
  DebugOut("GameUIcomboGacha.bannerDownloaderCallback")
  if DynamicResDownloader:IfResExsit(extInfo.localPath) and GameUIcomboGacha.mFlashObj then
    GameUIcomboGacha.mFlashObj:ReplaceTexture(extInfo.replaceName, extInfo.pngName)
    if extInfo.callback then
      extInfo.callback()
    end
  end
end
return GameUIcomboGacha
