local GameStateGlobalState = GameStateManager.GameStateGlobalState
local GameStateBattlePlay = GameStateManager.GameStateBattlePlay
local GameObjectLevelUp = LuaObjectManager:GetLuaObject("GameObjectLevelUp")
local GameRush = LuaObjectManager:GetLuaObject("GameRush")
local GameUIEvent = LuaObjectManager:GetLuaObject("GameUIEvent")
local GameUICommonDialog = LuaObjectManager:GetLuaObject("GameUICommonDialog")
local QuestTutorialBuildAffairs = TutorialQuestManager.QuestTutorialBuildAffairs
local QuestTutorialUseAffairs = TutorialQuestManager.QuestTutorialUseAffairs
local QuestTutorialBuildKrypton = TutorialQuestManager.QuestTutorialBuildKrypton
local QuestTutorialUseKrypton = TutorialQuestManager.QuestTutorialUseKrypton
local QuestTutorialBuildTechLab = TutorialQuestManager.QuestTutorialBuildTechLab
local QuestTutorialUseTechLab = TutorialQuestManager.QuestTutorialUseTechLab
local GameVipDetailInfoPanel = LuaObjectManager:GetLuaObject("GameVipDetailInfoPanel")
local TutorialAdjutantManager = LuaObjectManager:GetLuaObject("TutorialAdjutantManager")
local GameObjectBattleMap = LuaObjectManager:GetLuaObject("GameObjectBattleMap")
local GameObjectTutorialCutscene = LuaObjectManager:GetLuaObject("GameObjectTutorialCutscene")
local GameUIBarRight = LuaObjectManager:GetLuaObject("GameUIBarRight")
GameObjectLevelUp.lockAnimationFlag = false
GameObjectLevelUp.unlockAnimationFlagFirst = false
GameObjectLevelUp.unlockAnimationFlagSecond = false
GameObjectLevelUp.showType = 1
GameObjectLevelUp.unlockInfo = {}
GameObjectLevelUp.willDisplay = false
GameObjectLevelUp.battleSpeedUnlock = false
function GameObjectLevelUp:OnInitGame()
  GameGlobalData:RegisterDataChangeCallback("levelinfo", self.levelupNotifyCallback)
end
function GameObjectLevelUp:OnAddToGameState()
  if self:GetFlashObject() == nil then
    GameObjectLevelUp:LoadFlashObject()
  end
  self:MoveIn()
end
function GameObjectLevelUp:OnEraseFromGameState()
  GameObjectLevelUp.willDisplay = false
  if immanentversion170 == 4 or immanentversion170 == 5 then
    local levelInfo = GameGlobalData:GetData("levelinfo")
    if levelInfo.level >= 25 then
      local techlab = GameGlobalData:GetBuildingInfo("tech_lab")
      if techlab.level > 0 then
        QuestTutorialBuildTechLab:SetFinish(true)
        if not QuestTutorialUseTechLab:IsFinished() then
          QuestTutorialUseTechLab:SetActive(true)
        end
      else
        QuestTutorialBuildTechLab:SetActive(true)
        GameStateManager:SetCurrentGameState(GameStateManager.GameStateMainPlanet)
      end
    end
    if levelInfo.level >= 35 and not QuestTutorialBuildKrypton:IsFinished() then
      local krypton = GameGlobalData:GetBuildingInfo("krypton_center")
      if krypton.level > 0 then
        QuestTutorialBuildKrypton:SetFinish(true)
        if not QuestTutorialUseKrypton:IsFinished() then
          QuestTutorialUseKrypton:SetActive(true)
        end
      else
        QuestTutorialBuildKrypton:SetActive(true)
        GameStateManager:SetCurrentGameState(GameStateManager.GameStateMainPlanet)
      end
    end
  end
  if immanentversion == 2 then
    local levelInfo = GameGlobalData:GetData("levelinfo")
    if levelInfo.level >= 23 and not QuestTutorialBuildAffairs:IsFinished() then
      local affairs_hall = GameGlobalData:GetBuildingInfo("affairs_hall")
      if affairs_hall.level > 0 then
        QuestTutorialBuildAffairs:SetFinish(true)
        if not QuestTutorialUseAffairs:IsFinished() then
          QuestTutorialUseAffairs:SetActive(true)
        end
      else
        QuestTutorialBuildAffairs:SetActive(true)
        GameStateManager:SetCurrentGameState(GameStateManager.GameStateMainPlanet)
      end
    end
    if levelInfo.level >= 24 and not QuestTutorialBuildKrypton:IsFinished() then
      local krypton = GameGlobalData:GetBuildingInfo("krypton_center")
      if krypton.level > 0 then
        QuestTutorialBuildKrypton:SetFinish(true)
        if not QuestTutorialUseKrypton:IsFinished() then
          QuestTutorialUseKrypton:SetActive(true)
        end
      else
        QuestTutorialBuildKrypton:SetActive(true)
        GameStateManager:SetCurrentGameState(GameStateManager.GameStateMainPlanet)
      end
    end
    if levelInfo.level >= 30 then
      local techlab = GameGlobalData:GetBuildingInfo("tech_lab")
      if techlab.level > 0 then
        QuestTutorialBuildTechLab:SetFinish(true)
        if not QuestTutorialUseTechLab:IsFinished() then
          QuestTutorialUseTechLab:SetActive(true)
        end
      else
        QuestTutorialBuildTechLab:SetActive(true)
        local callback = function()
          GameStateManager:SetCurrentGameState(GameStateManager.GameStateMainPlanet)
        end
        GameUICommonDialog:PlayStory({51413}, callback)
      end
    end
  end
  TutorialQuestManager:CheckTlcTutorial()
  if TutorialQuestManager.QuestTutorialTlcMainUiEntry:IsActive() then
    GameUIBarRight:ShowTutorialTlc()
  end
  TutorialAdjutantManager:CheckActiveTutorialAdjutantStoryPlay()
  self:UnloadFlashObject()
end
function GameObjectLevelUp:OnFSCommand(cmd, arg)
  DebugOut("GameObjectLevelUp:OnFSCommand:")
  DebugOut("cmd = " .. cmd)
  if cmd == "animation_over_moveout" then
    if GameObjectTutorialCutscene:IsActive() and GameObjectTutorialCutscene:IsNeedShowNewFakeBattle() then
      GameObjectBattleMap:OnFSCommand("ShowVictoryTipsOver")
    else
      GameObjectLevelUp.willDisplay = false
      GameStateGlobalState:EraseObject(self)
      GameVipDetailInfoPanel:CheckShowTempVip()
    end
  elseif cmd == "main_movein_over" then
    if GameObjectTutorialCutscene:IsActive() and GameObjectTutorialCutscene:IsNeedShowNewFakeBattle() then
      self:GetFlashObject():InvokeASCallback("_root", "mainMoveOut")
    elseif GameObjectLevelUp._lastLevelInfo then
      GameObjectLevelUp:SetLevelUnlockInfo(GameObjectLevelUp._lastLevelInfo.level)
    end
  elseif cmd == "unlock1_movein_over" then
    GameObjectLevelUp.lockAnimationFlag = true
    if GameObjectLevelUp.showType == 2 then
      self:GetFlashObject():InvokeASCallback("_root", "unlock3Movein")
    end
  elseif cmd == "unlock2_movein_over" then
    GameObjectLevelUp.unlockAnimationFlagFirst = true
    if GameObjectLevelUp.showType == 4 then
      self:GetFlashObject():InvokeASCallback("_root", "unlock3Movein")
    end
  elseif cmd == "unlock3_movein_over" then
    GameObjectLevelUp.unlockAnimationFlagSecond = true
  elseif cmd == "main_moveout" then
    GameObjectLevelUp:MainMoveOut(GameObjectLevelUp.showType)
  elseif cmd == "unlock1_moveto_mid" then
    if GameObjectLevelUp.showType == 6 then
      self:GetFlashObject():InvokeASCallback("_root", "unlock4Movein")
    end
  elseif cmd == "unlock4_movein_over" then
    GameObjectLevelUp.unlockAnimationFlagFirstMid = true
  end
end
function GameObjectLevelUp.levelupNotifyCallback()
  GameObjectLevelUp._lastLevelInfo = LuaUtils:table_rcopy(GameGlobalData:GetData("levelinfo"))
  DebugOutPutTable(GameObjectLevelUp._lastLevelInfo, "GameObjectLevelUp._lastLevelInfo")
  GameObjectLevelUp:CheckNeedDisplay()
end
function GameObjectLevelUp:MoveIn()
  local flash_obj = self:GetFlashObject()
  GameObjectLevelUp.lockAnimationFlag = false
  GameObjectLevelUp.unlockAnimationFlagFirst = false
  GameObjectLevelUp.unlockAnimationFlagSecond = false
  GameObjectLevelUp.unlockAnimationFlagFirstMid = false
  local lang = "en"
  if GameSettingData and GameSettingData.Save_Lang then
    lang = GameSettingData.Save_Lang
    if string.find(lang, "ru") == 1 then
      lang = "ru"
    end
  end
  self:GetFlashObject():InvokeASCallback("_root", "lua2fs_animationMoveIn", lang)
end
function GameObjectLevelUp:showNewFakeBattleVictory()
  self:LoadFlashObject()
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "showNewFakeBattleVictory")
end
function GameObjectLevelUp:UpdateLevelInfo(level)
  DebugOut("GameObjectLevelUp:UpdateLevelInfo:")
  DebugOut("level:" .. level)
  level = level or GameGlobalData:GetData("levelinfo").level
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "lua2fs_updateLevelupInfo", level)
end
function GameObjectLevelUp.lockLevelupInfo(content)
  if content then
    DebugOutPutTable(content, "lockLevelupInfo.content:")
    GameObjectLevelUp.unlockInfo = content.ntf
    for key, value in pairs(GameObjectLevelUp.unlockInfo) do
      if value.level == GameGlobalData:GetData("levelinfo").level and value.name == "combat_speed" then
        DebugOut("unlock combat_speed:")
        GameObjectLevelUp.battleSpeedUnlock = true
      end
    end
  end
end
function GameObjectLevelUp:SetLevelUnlockInfo(level)
  GameObjectLevelUp.showType = 1
  local name, nextname = "", ""
  local nameText, nextNameText = "", ""
  local descText, nextDescText = "", ""
  local title, nextTitle = "", ""
  if #GameObjectLevelUp.unlockInfo == 0 then
    GameObjectLevelUp.showType = 1
  end
  if #GameObjectLevelUp.unlockInfo == 1 then
    local item = GameObjectLevelUp.unlockInfo[1]
    if item.level == level then
      if item.is_opened == 1 then
        GameObjectLevelUp.showType = 3
        title = GameLoader:GetGameText("LC_MENU_NEW_FUNCTION")
        descText = GameLoader:GetGameText("LC_MENU_open_DESC")
      else
        title = GameLoader:GetGameText("LC_MENU_NEW_FUNCTION")
        GameObjectLevelUp.showType = 5
        descText = GameLoader:GetGameText("LC_MENU_" .. item.name .. "_DESC")
      end
    else
      title = GameLoader:GetGameText("LC_MENU_COMING_SOON")
      GameObjectLevelUp.showType = 5
      if item.is_opened == 1 then
        local leveltext = "<font color='#FFBF00'>" .. tostring(item.level) .. "</font>"
        descText = string.format(GameLoader:GetGameText("LC_MENU_LEVEL_FUNCTION_OPEN_INFO"), leveltext)
      else
        local leveltext = "<font color='#FFBF00'>" .. tostring(item.level) .. "</font>"
        descText = "1." .. string.format(GameLoader:GetGameText("LC_MENU_LEVEL_FUNCTION_OPEN_INFO"), leveltext) .. "\n" .. "2." .. GameLoader:GetGameText("LC_MENU_" .. item.name .. "_DESC")
      end
    end
    if item.name == "empire_headquarters" then
      name = "Command_post"
    else
      name = item.name
    end
    nameText = GameLoader:GetGameText("LC_MENU_" .. string.upper(name) .. "_BUTTON")
  end
  if #GameObjectLevelUp.unlockInfo == 2 then
    local item_one, item_two
    if GameObjectLevelUp.unlockInfo[1].level < GameObjectLevelUp.unlockInfo[2].level then
      item_one = GameObjectLevelUp.unlockInfo[1]
      item_two = GameObjectLevelUp.unlockInfo[2]
    else
      item_one = GameObjectLevelUp.unlockInfo[2]
      item_two = GameObjectLevelUp.unlockInfo[1]
    end
    if GameObjectLevelUp.unlockInfo[1].level == GameObjectLevelUp.unlockInfo[2].level then
      if GameObjectLevelUp.unlockInfo[2].is_opened == 1 then
        item_one = GameObjectLevelUp.unlockInfo[2]
        item_two = GameObjectLevelUp.unlockInfo[1]
      else
        item_one = GameObjectLevelUp.unlockInfo[1]
        item_two = GameObjectLevelUp.unlockInfo[2]
      end
    end
    if item_one.level == level then
      if item_one.is_opened == 1 then
        GameObjectLevelUp.showType = 2
        title = GameLoader:GetGameText("LC_MENU_NEW_FUNCTION")
        descText = GameLoader:GetGameText("LC_MENU_open_DESC")
      else
        GameObjectLevelUp.showType = 4
        title = GameLoader:GetGameText("LC_MENU_NEW_FUNCTION")
        descText = GameLoader:GetGameText("LC_MENU_" .. tostring(item_one.name) .. "_DESC")
      end
    else
      GameObjectLevelUp.showType = 4
      title = GameLoader:GetGameText("LC_MENU_COMING_SOON")
      if item_one.is_opened == 1 then
        local leveltext = "<font color='#FFBF00'>" .. tostring(item_one.level) .. "</font>"
        descText = string.format(GameLoader:GetGameText("LC_MENU_LEVEL_FUNCTION_OPEN_INFO"), leveltext)
      else
        local leveltext = "<font color='#FFBF00'>" .. tostring(item_one.level) .. "</font>"
        descText = "1." .. string.format(GameLoader:GetGameText("LC_MENU_LEVEL_FUNCTION_OPEN_INFO"), leveltext) .. "\n" .. "2." .. GameLoader:GetGameText("LC_MENU_" .. tostring(item_one.name) .. "_DESC")
      end
    end
    if item_two.level == level then
      if item_two.is_opened == 1 then
        GameObjectLevelUp.showType = 6
        nextTitle = GameLoader:GetGameText("LC_MENU_NEW_FUNCTION")
        nextDescText = GameLoader:GetGameText("LC_MENU_open_DESC")
      else
        nextTitle = GameLoader:GetGameText("LC_MENU_NEW_FUNCTION")
        nextDescText = GameLoader:GetGameText("LC_MENU_" .. tostring(item_two.name) .. "_DESC")
      end
    else
      nextTitle = GameLoader:GetGameText("LC_MENU_COMING_SOON")
      if item_two.is_opened == 1 then
        local leveltext = "<font color='#FFBF00'>" .. tostring(item_two.level) .. "</font>"
        nextDescText = string.format(GameLoader:GetGameText("LC_MENU_LEVEL_FUNCTION_OPEN_INFO"), leveltext)
      else
        local leveltext = "<font color='#FFBF00'>" .. tostring(item_two.level) .. "</font>"
        nextDescText = "1." .. string.format(GameLoader:GetGameText("LC_MENU_LEVEL_FUNCTION_OPEN_INFO"), leveltext) .. "\n" .. "2." .. GameLoader:GetGameText("LC_MENU_" .. tostring(item_two.name) .. "_DESC")
      end
    end
    if item_one.name == "empire_headquarters" then
      name = "Command_post"
    else
      name = item_one.name
    end
    if item_two.name == "empire_headquarters" then
      nextname = "Command_post"
    else
      nextname = item_two.name
    end
    nameText = GameLoader:GetGameText("LC_MENU_" .. string.upper(name) .. "_BUTTON")
    nextNameText = GameLoader:GetGameText("LC_MENU_" .. string.upper(nextname) .. "_BUTTON")
  end
  DebugOut("showinfodata:" .. "showType:" .. GameObjectLevelUp.showType .. " name:" .. name .. " nextname:" .. nextname .. " nameText:" .. nameText .. " nextNameText:" .. nextNameText .. " descText:" .. descText .. " nextDescText:" .. nextDescText)
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    DebugOut("tp:name:" .. name .. "nextname:" .. nextname)
    local lang = "en"
    if GameSettingData and GameSettingData.Save_Lang then
      lang = GameSettingData.Save_Lang
      if string.find(lang, "ru") == 1 then
        lang = "ru"
      end
    end
    flash_obj:InvokeASCallback("_root", "lua2fs_showUnlockInfo", GameObjectLevelUp.showType, name, nextname, nameText, nextNameText, descText, nextDescText, title, nextTitle, lang)
  end
  GameObjectLevelUp:ShowAnimation(GameObjectLevelUp.showType)
end
function GameObjectLevelUp:ShowAnimation(showType)
  DebugOut("GameObjectLevelUp:ShowAnimation:" .. showType)
  local flash_obj = self:GetFlashObject()
  if showType == 1 then
    flash_obj:InvokeASCallback("_root", "mainMoveOut")
  end
  if showType == 2 or showType == 3 or showType == 6 then
    flash_obj:InvokeASCallback("_root", "unlock1Movein")
  end
  if showType == 4 or showType == 5 then
    flash_obj:InvokeASCallback("_root", "unlock2Movein")
  end
end
function GameObjectLevelUp:MainMoveOut(showType)
  DebugOut("MainMoveOut:" .. "showType:" .. showType)
  if GameObjectLevelUp.battleSpeedUnlock then
    GameUtils:SetBattleSpeed(2)
    GameUtils:SaveSettingData()
    GameObjectLevelUp.battleSpeedUnlock = false
  end
  local flash_obj = self:GetFlashObject()
  if (showType == 2 or showType == 4) and GameObjectLevelUp.unlockAnimationFlagSecond and flash_obj then
    flash_obj:InvokeASCallback("_root", "mainMoveOut")
  end
  if showType == 3 and GameObjectLevelUp.lockAnimationFlag and flash_obj then
    flash_obj:InvokeASCallback("_root", "mainMoveOut")
  end
  if showType == 5 and GameObjectLevelUp.unlockAnimationFlagFirst and flash_obj then
    flash_obj:InvokeASCallback("_root", "mainMoveOut")
  end
  if showType == 6 and GameObjectLevelUp.unlockAnimationFlagFirstMid and flash_obj then
    flash_obj:InvokeASCallback("_root", "mainMoveOut")
  end
end
function GameObjectLevelUp:CheckNeedDisplay()
  if self._lastLevelInfo and self._lastLevelInfo.up_level > 0 then
    if GameObjectLevelUp:GetFlashObject() == nil then
      self:LoadFlashObject()
    end
    if GameStateGlobalState:IsObjectInState(self) then
      GameObjectLevelUp:MoveIn()
    else
      GameStateGlobalState:AddObject(self)
    end
    self:UpdateLevelInfo(self._lastLevelInfo.level)
    if 0 >= self._lastLevelInfo.up_level - 1 then
      self._lastLevelInfo.up_level = 0
    end
    GameObjectLevelUp.willDisplay = true
    DebugOut("add to GameStateGlobalState")
    GameGlobalData:RefreshData("levelupNotify")
    if self._lastLevelInfo.level == 13 then
      GameUtils:LogFirebaseEvent("level_up", "level|13")
      GameUtils:LogFirebaseEvent("fte_end", "value|true")
    end
  end
end
function GameObjectLevelUp:DisplayAllowed()
  local ActiveState = GameStateManager:GetCurrentGameState()
  if ActiveState == GameStateBattlePlay then
    return false
  elseif ActiveState == GameStateArena then
    return false
  elseif ActiveState == GameStateBattleMap and GameStateBattleMap:IsObjectInState(GameUIEvent) then
    return false
  end
  return true
end
