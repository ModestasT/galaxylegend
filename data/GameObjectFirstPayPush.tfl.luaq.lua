local GameObjectFirstPayPush = LuaObjectManager:GetLuaObject("GameObjectFirstPayPush")
local GameUIFirstCharge = LuaObjectManager:GetLuaObject("GameUIFirstCharge")
function GameObjectFirstPayPush:OnFSCommand(cmd, arg)
  if cmd == "close" then
    function self.m_closeCallback()
      GameStateManager:GetCurrentGameState():EraseObject(self)
    end
    self:GetFlashObject():InvokeASCallback("_root", "SetContentMove", false)
  elseif cmd == "enter" then
    function self.m_closeCallback()
      GameStateManager:GetCurrentGameState():EraseObject(self)
      self:EnterPay()
    end
    self:GetFlashObject():InvokeASCallback("_root", "SetContentMove", false)
  elseif cmd == "close_menu" and self.m_closeCallback then
    self.m_closeCallback()
  end
end
function GameObjectFirstPayPush:EnterPay()
  GameStateManager:GetCurrentGameState():AddObject(GameUIFirstCharge)
end
function GameObjectFirstPayPush:Show()
  GameStateManager:GetCurrentGameState():AddObject(self)
end
function GameObjectFirstPayPush.FirstPayPushNTF(content)
  GameObjectFirstPayPush.content = content
  local pngName = content.bg_pic .. ".png"
  local picFinishCallBack = function()
    GameNotice:AddNotice("NewFisrtPayPush")
    if GameStateManager.GameStateMainPlanet == GameStateManager:GetCurrentGameState() then
      GameNotice:CheckShowNotice()
    end
    local callBack = function()
      GameNotice:CheckShowNotice()
    end
    GameNotice.RegistFetchInfoFunction(callBack)
  end
  if not DynamicResDownloader:IfResExsit("data2/" .. pngName) then
    DynamicResDownloader:AddDynamicRes(pngName, DynamicResDownloader.resType.WELCOME_PIC, nil, picFinishCallBack)
  else
    picFinishCallBack()
  end
end
function GameObjectFirstPayPush:OnAddToGameState()
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  if GameObjectFirstPayPush.content ~= nil then
    local data = {}
    data.type = "PNG"
    data.titleText = GameLoader:GetGameText("LC_MENU_" .. GameObjectFirstPayPush.content.title)
    data.descText = GameLoader:GetGameText("LC_MENU_" .. GameObjectFirstPayPush.content.desc)
    data.isShowEnter = true
    data.showTime = false
    data.png = GameObjectFirstPayPush.content.bg_pic .. ".png"
    self:GetFlashObject():InvokeASCallback("_root", "SetContent", data)
    local replaceName = "welcome_banner.png"
    GameObjectFirstPayPush:GetFlashObject():ReplaceTexture(replaceName, data.png)
    self:GetFlashObject():InvokeASCallback("_root", "SetContentMove", true)
    if self:GetFlashObject() then
      self:GetFlashObject():InvokeASCallback("_root", "OnTimeChange", data)
    end
  end
end
function GameObjectFirstPayPush:OnInitGame()
end
function GameObjectFirstPayPush:OnEraseFromGameState()
  self:UnloadFlashObject()
end
