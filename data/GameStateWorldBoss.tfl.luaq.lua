local GameStateWorldBoss = GameStateManager.GameStateWorldBoss
local GameObjectBattleMapBG = LuaObjectManager:GetLuaObject("GameObjectBattleMapBG")
local GameObjectWorldBoss = LuaObjectManager:GetLuaObject("GameObjectWorldBoss")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
function GameStateWorldBoss:OnFocusGain(laststate)
  function GameStateWorldBoss.onQuit()
    GameStateManager:SetCurrentGameState(laststate)
  end
  local content = {can_interrupt = false}
  NetMessageMgr:SendMsg(NetAPIList.ready_world_boss_req.Code, content, self.NetCallbackReady, true, nil)
  GameObjectWorldBoss:LoadFlashObject()
end
function GameStateWorldBoss:OnFocusLost()
  GameStateWorldBoss.BossInfo = nil
  GameStateWorldBoss.PowerupInfo = nil
  self:EraseObject(GameObjectWorldBoss)
  NetMessageMgr:SendMsg(NetAPIList.exit_world_boss_req.Code, nil, self.NetCallbackQuit, false)
end
function GameStateWorldBoss:Quit()
  local GameStateMainPlanet = GameStateManager.GameStateMainPlanet
  GameStateManager:SetCurrentGameState(GameStateMainPlanet)
end
function GameStateWorldBoss:GetBossInfo()
  return GameStateWorldBoss.BossInfo
end
function GameStateWorldBoss:GetPowerupInfo()
  return GameStateWorldBoss.PowerupInfo
end
function GameStateWorldBoss.NetCallbackReady(msgType, content)
  DebugOut(msgType, "call back ready")
  DebugTable(content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.ready_world_boss_req.Code then
    if content.code == 100128 then
      local function callback()
        local content = {can_interrupt = true}
        NetMessageMgr:SendMsg(NetAPIList.ready_world_boss_req.Code, content, GameStateWorldBoss.NetCallbackReady, true, nil)
      end
      local function callback2()
        GameStateWorldBoss:Quit()
      end
      GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.Caption)
      local cancel = GameLoader:GetGameText("LC_MENU_BUTTON_CAPTION_CANCEL")
      local affairs = GameLoader:GetGameText("LC_MENU_BUTTON_CAPTION_CONFIRM")
      DebugOut("cancle", cancel, affairs)
      GameUIMessageDialog:SetRightTextButton(cancel, callback2)
      GameUIMessageDialog:SetLeftTextButton(affairs, callback)
      GameUIMessageDialog:Display("", GameLoader:GetGameText("LC_ALERT_boss_robot_attackment_is_on_work"))
    elseif content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      GameStateWorldBoss:Quit()
    end
    return true
  end
  if msgType == NetAPIList.ready_world_boss_ack.Code then
    GameObjectWorldBoss:SetTargetWaitingTime(content.countdown)
    local powerupinfo = {}
    powerupinfo.rate = content.force_rate
    powerupinfo.cost = content.force_rate_cost
    powerupinfo.current_level = content.current_level
    GameStateWorldBoss.PowerupInfo = powerupinfo
    GameObjectWorldBoss:UpdatePowerupInfo()
    return true
  end
  return false
end
function GameStateWorldBoss.NetCallbackQuit(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.exit_world_boss_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  return false
end
function GameStateWorldBoss.WorldBossInfoNotifyHandler(content)
  GameStateWorldBoss.TemBossInfo = content
  if GameStateManager:GetCurrentGameState() == GameStateWorldBoss then
    local needdisplay = false
    if GameStateWorldBoss.BossInfo == nil then
      needdisplay = true
    else
      local damage = content.hp - GameStateWorldBoss.BossInfo.hp
      content.damage = damage
    end
    GameStateWorldBoss.BossInfo = content
    if GameStateWorldBoss.BossInfo.toplist and #GameStateWorldBoss.BossInfo.toplist > 1 then
      table.sort(GameStateWorldBoss.BossInfo.toplist, function(a, b)
        return a.top < b.top
      end)
    end
    if needdisplay then
      GameObjectBattleMapBG:LoadBGMap(1, 1)
      GameStateWorldBoss:AddObject(GameObjectBattleMapBG)
      GameStateWorldBoss:AddObject(GameObjectWorldBoss)
      local bossname = GameLoader:GetGameText("LC_BATTLE_AREA53_1001_NAME")
      GameObjectWorldBoss:SetBossName(bossname)
    end
    GameObjectWorldBoss:UpdateWorldBossInfo()
    GameStateWorldBoss:CheckQuit(GameStateWorldBoss.BossInfo)
  end
end
function GameStateWorldBoss:CheckQuit(BossInfo)
  if not BossInfo then
    return
  end
  if BossInfo.hp == 0 then
    local info = GameLoader:GetGameText("LC_ALERT_world_boss_dead")
    GameTip:Show(info)
    GameStateWorldBoss:Quit()
  elseif BossInfo.end_time == 0 then
    local info = GameLoader:GetGameText("LC_ALERT_not_in_world_boss_time")
    GameTip:Show(info)
    GameStateWorldBoss:Quit()
  end
end
function GameStateWorldBoss.WorldBossPowerupInfoNotifyHandler(content)
  if GameStateManager:GetCurrentGameState() == GameStateWorldBoss then
    GameStateWorldBoss.PowerupInfo = content
    GameObjectWorldBoss:UpdatePowerupInfo()
  end
end
