local Classify = GameData.achievement.Classify
Classify.activity = {
  Classify = "activity",
  Name = "Activity",
  IconID = 2
}
Classify.plot = {
  Classify = "plot",
  Name = "Plot",
  IconID = 3
}
Classify.alliance = {
  Classify = "alliance",
  Name = "Alliance",
  IconID = 4
}
Classify.festival = {
  Classify = "festival",
  Name = "Festival",
  IconID = 5
}
Classify.fleets_streng = {
  Classify = "fleets_streng",
  Name = "Fleet",
  IconID = 6
}
