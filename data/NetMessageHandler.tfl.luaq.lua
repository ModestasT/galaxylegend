local GameStateMainPlanet = GameStateManager.GameStateMainPlanet
local GameStateWorldBoss = GameStateManager.GameStateWorldBoss
local GameStateColonization = GameStateManager.GameStateColonization
local GameStateStarCraft = GameStateManager.GameStateStarCraft
local GameStateWD = GameStateManager.GameStateWD
local GameUIColonial = LuaObjectManager:GetLuaObject("GameUIColonial")
local GameUISlaveSelect = LuaObjectManager:GetLuaObject("GameUISlaveSelect")
local GameUIBarLeft = LuaObjectManager:GetLuaObject("GameUIBarLeft")
local GameUIChat = LuaObjectManager:GetLuaObject("GameUIChat")
local GameObjectWorldBoss = LuaObjectManager:GetLuaObject("GameObjectWorldBoss")
local GameUIStarCraftMap = LuaObjectManager:GetLuaObject("GameUIStarCraftMap")
local GameObjectMainPlanet = LuaObjectManager:GetLuaObject("GameObjectMainPlanet")
NetMessageHandler = {}
function NetMessageHandler.ChatMessageNotifyHandler(content)
  content.content = GameUtils:CensorWordsFilter(content.content)
  GameUIBarLeft:MsgIncoming(content)
  GameUIChat.UpdateChatMessage(content)
end
function NetMessageHandler.SpecialEfficacyNotifyHandler(content)
  if content.id == 100 then
    GameObjectMainPlanet:SpecialEfficacyNotifyHandler(content)
  end
end
