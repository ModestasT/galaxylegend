local Monster = GameData.medal_boss.Monster
Monster[56000001] = {
  ID = 56000001,
  vessels = 1,
  durability = 20,
  Avatar = "head60",
  Ship = "ship63"
}
Monster[56000002] = {
  ID = 56000002,
  vessels = 2,
  durability = 14,
  Avatar = "head61",
  Ship = "ship64"
}
Monster[56000003] = {
  ID = 56000003,
  vessels = 3,
  durability = 18,
  Avatar = "head62",
  Ship = "ship65"
}
Monster[56000004] = {
  ID = 56000004,
  vessels = 4,
  durability = 19,
  Avatar = "head63",
  Ship = "ship66"
}
Monster[56000005] = {
  ID = 56000005,
  vessels = 6,
  durability = 11,
  Avatar = "head64",
  Ship = "ship67"
}
Monster[56000006] = {
  ID = 56000006,
  vessels = 1,
  durability = 19,
  Avatar = "head65",
  Ship = "ship68"
}
Monster[56000007] = {
  ID = 56000007,
  vessels = 2,
  durability = 12,
  Avatar = "head66",
  Ship = "ship69"
}
Monster[56000008] = {
  ID = 56000008,
  vessels = 3,
  durability = 18,
  Avatar = "head67",
  Ship = "ship70"
}
Monster[56000009] = {
  ID = 56000009,
  vessels = 4,
  durability = 14,
  Avatar = "head68",
  Ship = "ship71"
}
Monster[56000010] = {
  ID = 56000010,
  vessels = 6,
  durability = 19,
  Avatar = "head69",
  Ship = "ship72"
}
Monster[56000011] = {
  ID = 56000011,
  vessels = 1,
  durability = 18,
  Avatar = "head70",
  Ship = "ship73"
}
Monster[56000012] = {
  ID = 56000012,
  vessels = 2,
  durability = 12,
  Avatar = "head71",
  Ship = "ship74"
}
Monster[56000013] = {
  ID = 56000013,
  vessels = 3,
  durability = 18,
  Avatar = "head72",
  Ship = "ship75"
}
Monster[56000014] = {
  ID = 56000014,
  vessels = 4,
  durability = 11,
  Avatar = "head73",
  Ship = "ship76"
}
Monster[56000015] = {
  ID = 56000015,
  vessels = 6,
  durability = 14,
  Avatar = "head74",
  Ship = "ship77"
}
Monster[56000016] = {
  ID = 56000016,
  vessels = 1,
  durability = 104,
  Avatar = "head75",
  Ship = "ship78"
}
Monster[56000017] = {
  ID = 56000017,
  vessels = 2,
  durability = 101,
  Avatar = "head97",
  Ship = "ship100"
}
Monster[56000018] = {
  ID = 56000018,
  vessels = 3,
  durability = 108,
  Avatar = "head98",
  Ship = "ship101"
}
Monster[56000019] = {
  ID = 56000019,
  vessels = 4,
  durability = 109,
  Avatar = "head99",
  Ship = "ship102"
}
Monster[56000020] = {
  ID = 56000020,
  vessels = 6,
  durability = 104,
  Avatar = "head100",
  Ship = "ship103"
}
Monster[56000021] = {
  ID = 56000021,
  vessels = 1,
  durability = 104,
  Avatar = "head101",
  Ship = "ship104"
}
Monster[56000022] = {
  ID = 56000022,
  vessels = 2,
  durability = 102,
  Avatar = "head102",
  Ship = "ship105"
}
Monster[56000023] = {
  ID = 56000023,
  vessels = 3,
  durability = 107,
  Avatar = "head103",
  Ship = "ship106"
}
Monster[56000024] = {
  ID = 56000024,
  vessels = 4,
  durability = 104,
  Avatar = "head104",
  Ship = "ship107"
}
Monster[56000025] = {
  ID = 56000025,
  vessels = 6,
  durability = 101,
  Avatar = "head105",
  Ship = "ship108"
}
Monster[56000026] = {
  ID = 56000026,
  vessels = 1,
  durability = 101,
  Avatar = "head76",
  Ship = "ship79"
}
Monster[56000027] = {
  ID = 56000027,
  vessels = 2,
  durability = 109,
  Avatar = "head77",
  Ship = "ship80"
}
Monster[56000028] = {
  ID = 56000028,
  vessels = 3,
  durability = 106,
  Avatar = "head78",
  Ship = "ship81"
}
Monster[56000029] = {
  ID = 56000029,
  vessels = 4,
  durability = 107,
  Avatar = "head79",
  Ship = "ship82"
}
Monster[56000030] = {
  ID = 56000030,
  vessels = 6,
  durability = 101,
  Avatar = "head80",
  Ship = "ship83"
}
Monster[56000031] = {
  ID = 56000031,
  vessels = 1,
  durability = 160,
  Avatar = "head81",
  Ship = "ship84"
}
Monster[56000032] = {
  ID = 56000032,
  vessels = 2,
  durability = 150,
  Avatar = "head82",
  Ship = "ship85"
}
Monster[56000033] = {
  ID = 56000033,
  vessels = 3,
  durability = 154,
  Avatar = "head83",
  Ship = "ship86"
}
Monster[56000034] = {
  ID = 56000034,
  vessels = 4,
  durability = 159,
  Avatar = "head84",
  Ship = "ship87"
}
Monster[56000035] = {
  ID = 56000035,
  vessels = 6,
  durability = 158,
  Avatar = "head85",
  Ship = "ship88"
}
Monster[56000036] = {
  ID = 56000036,
  vessels = 1,
  durability = 156,
  Avatar = "head86",
  Ship = "ship89"
}
Monster[56000037] = {
  ID = 56000037,
  vessels = 2,
  durability = 155,
  Avatar = "head87",
  Ship = "ship90"
}
Monster[56000038] = {
  ID = 56000038,
  vessels = 3,
  durability = 158,
  Avatar = "head88",
  Ship = "ship91"
}
Monster[56000039] = {
  ID = 56000039,
  vessels = 4,
  durability = 153,
  Avatar = "head89",
  Ship = "ship92"
}
Monster[56000040] = {
  ID = 56000040,
  vessels = 6,
  durability = 156,
  Avatar = "head90",
  Ship = "ship93"
}
Monster[56000041] = {
  ID = 56000041,
  vessels = 1,
  durability = 155,
  Avatar = "head91",
  Ship = "ship94"
}
Monster[56000042] = {
  ID = 56000042,
  vessels = 2,
  durability = 159,
  Avatar = "head97",
  Ship = "ship100"
}
Monster[56000043] = {
  ID = 56000043,
  vessels = 3,
  durability = 151,
  Avatar = "head98",
  Ship = "ship101"
}
Monster[56000044] = {
  ID = 56000044,
  vessels = 4,
  durability = 157,
  Avatar = "head99",
  Ship = "ship102"
}
Monster[56000045] = {
  ID = 56000045,
  vessels = 6,
  durability = 151,
  Avatar = "head100",
  Ship = "ship103"
}
Monster[56000046] = {
  ID = 56000046,
  vessels = 1,
  durability = 204,
  Avatar = "head101",
  Ship = "ship104"
}
Monster[56000047] = {
  ID = 56000047,
  vessels = 2,
  durability = 206,
  Avatar = "head102",
  Ship = "ship105"
}
Monster[56000048] = {
  ID = 56000048,
  vessels = 3,
  durability = 204,
  Avatar = "head103",
  Ship = "ship106"
}
Monster[56000049] = {
  ID = 56000049,
  vessels = 4,
  durability = 209,
  Avatar = "head104",
  Ship = "ship107"
}
Monster[56000050] = {
  ID = 56000050,
  vessels = 6,
  durability = 207,
  Avatar = "head105",
  Ship = "ship108"
}
Monster[56000051] = {
  ID = 56000051,
  vessels = 1,
  durability = 209,
  Avatar = "head106",
  Ship = "ship109"
}
Monster[56000052] = {
  ID = 56000052,
  vessels = 2,
  durability = 206,
  Avatar = "head107",
  Ship = "ship110"
}
Monster[56000053] = {
  ID = 56000053,
  vessels = 3,
  durability = 204,
  Avatar = "head108",
  Ship = "ship111"
}
Monster[56000054] = {
  ID = 56000054,
  vessels = 4,
  durability = 205,
  Avatar = "head109",
  Ship = "ship112"
}
Monster[56000055] = {
  ID = 56000055,
  vessels = 6,
  durability = 205,
  Avatar = "head110",
  Ship = "ship113"
}
Monster[56000056] = {
  ID = 56000056,
  vessels = 1,
  durability = 206,
  Avatar = "head111",
  Ship = "ship114"
}
Monster[56000057] = {
  ID = 56000057,
  vessels = 2,
  durability = 209,
  Avatar = "head112",
  Ship = "ship115"
}
Monster[56000058] = {
  ID = 56000058,
  vessels = 3,
  durability = 208,
  Avatar = "head113",
  Ship = "ship116"
}
Monster[56000059] = {
  ID = 56000059,
  vessels = 4,
  durability = 202,
  Avatar = "head114",
  Ship = "ship117"
}
Monster[56000060] = {
  ID = 56000060,
  vessels = 6,
  durability = 207,
  Avatar = "head115",
  Ship = "ship118"
}
Monster[56000061] = {
  ID = 56000061,
  vessels = 1,
  durability = 302,
  Avatar = "head116",
  Ship = "ship119"
}
Monster[56000062] = {
  ID = 56000062,
  vessels = 2,
  durability = 308,
  Avatar = "head117",
  Ship = "ship120"
}
Monster[56000063] = {
  ID = 56000063,
  vessels = 3,
  durability = 306,
  Avatar = "head118",
  Ship = "ship121"
}
Monster[56000064] = {
  ID = 56000064,
  vessels = 4,
  durability = 308,
  Avatar = "head119",
  Ship = "ship122"
}
Monster[56000065] = {
  ID = 56000065,
  vessels = 6,
  durability = 308,
  Avatar = "head120",
  Ship = "ship123"
}
Monster[56000066] = {
  ID = 56000066,
  vessels = 1,
  durability = 309,
  Avatar = "head121",
  Ship = "ship124"
}
Monster[56000067] = {
  ID = 56000067,
  vessels = 2,
  durability = 302,
  Avatar = "head122",
  Ship = "ship125"
}
Monster[56000068] = {
  ID = 56000068,
  vessels = 3,
  durability = 307,
  Avatar = "head123",
  Ship = "ship126"
}
Monster[56000069] = {
  ID = 56000069,
  vessels = 4,
  durability = 302,
  Avatar = "head124",
  Ship = "ship127"
}
Monster[56000070] = {
  ID = 56000070,
  vessels = 6,
  durability = 305,
  Avatar = "head125",
  Ship = "ship128"
}
Monster[56000071] = {
  ID = 56000071,
  vessels = 1,
  durability = 305,
  Avatar = "head126",
  Ship = "ship129"
}
Monster[56000072] = {
  ID = 56000072,
  vessels = 2,
  durability = 309,
  Avatar = "head127",
  Ship = "ship130"
}
Monster[56000073] = {
  ID = 56000073,
  vessels = 3,
  durability = 310,
  Avatar = "head128",
  Ship = "ship131"
}
Monster[56000074] = {
  ID = 56000074,
  vessels = 4,
  durability = 308,
  Avatar = "head129",
  Ship = "ship132"
}
Monster[56000075] = {
  ID = 56000075,
  vessels = 6,
  durability = 304,
  Avatar = "head130",
  Ship = "ship133"
}
Monster[5600076] = {
  ID = 5600076,
  vessels = 4,
  durability = 5,
  Avatar = "head102",
  Ship = "ship105"
}
Monster[5600077] = {
  ID = 5600077,
  vessels = 5,
  durability = 5,
  Avatar = "head92",
  Ship = "ship95"
}
Monster[5600078] = {
  ID = 5600078,
  vessels = 6,
  durability = 5,
  Avatar = "head77",
  Ship = "ship80"
}
Monster[5600079] = {
  ID = 5600079,
  vessels = 1,
  durability = 5,
  Avatar = "head100",
  Ship = "ship103"
}
Monster[5600080] = {
  ID = 5600080,
  vessels = 2,
  durability = 5,
  Avatar = "head108",
  Ship = "ship111"
}
Monster[5600081] = {
  ID = 5600081,
  vessels = 3,
  durability = 5,
  Avatar = "head97",
  Ship = "ship100"
}
Monster[5600082] = {
  ID = 5600082,
  vessels = 4,
  durability = 5,
  Avatar = "head74",
  Ship = "ship77"
}
Monster[5600083] = {
  ID = 5600083,
  vessels = 5,
  durability = 5,
  Avatar = "head99",
  Ship = "ship102"
}
Monster[5600084] = {
  ID = 5600084,
  vessels = 6,
  durability = 5,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5600085] = {
  ID = 5600085,
  vessels = 1,
  durability = 5,
  Avatar = "head58",
  Ship = "ship61"
}
Monster[5600086] = {
  ID = 5600086,
  vessels = 2,
  durability = 5,
  Avatar = "head136",
  Ship = "ship139"
}
Monster[5600087] = {
  ID = 5600087,
  vessels = 3,
  durability = 5,
  Avatar = "head84",
  Ship = "ship87"
}
Monster[5600088] = {
  ID = 5600088,
  vessels = 4,
  durability = 5,
  Avatar = "head75",
  Ship = "ship78"
}
Monster[5600089] = {
  ID = 5600089,
  vessels = 5,
  durability = 5,
  Avatar = "head139",
  Ship = "ship142"
}
Monster[5600090] = {
  ID = 5600090,
  vessels = 6,
  durability = 5,
  Avatar = "head13",
  Ship = "ship37"
}
