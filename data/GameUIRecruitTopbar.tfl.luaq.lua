local GameStateRecruit = GameStateManager.GameStateRecruit
local GameUIRecruitTopbar = LuaObjectManager:GetLuaObject("GameUIRecruitTopbar")
local GameUIRecruitMain = LuaObjectManager:GetLuaObject("GameUIRecruitMain")
local GameUIRecruitWarpgate = LuaObjectManager:GetLuaObject("GameUIRecruitWarpgate")
local QuestTutorialStarCharge = TutorialQuestManager.QuestTutorialStarCharge
local QuestTutorialRecruit = TutorialQuestManager.QuestTutorialRecruit
local GameUICommonDialog = LuaObjectManager:GetLuaObject("GameUICommonDialog")
local GameUIRecruitMainNew = LuaObjectManager:GetLuaObject("GameUIRecruitMainNew")
local GameUIHeroUnion = LuaObjectManager:GetLuaObject("GameUIHeroUnion")
local k_filterTypeAll = "all"
local k_filterTypeElite = "elite"
local k_filterTypeNormal = "normal"
local m_currentFilterType = k_filterTypeAll
function GameUIRecruitTopbar:OnFSCommand(cmd, arg)
  if cmd == "select_tab" then
    GameStateRecruit:SelectTab(arg)
  elseif cmd == "on_clicked_close" then
    GameStateRecruit:Quit()
  elseif cmd == "GetMoreEnergeMoveInOver" then
  elseif cmd == "GetMoreEnergeMoveOutOver" then
    self:GetFlashObject():InvokeASCallback("_root", "SetShowNeedMore", false)
  elseif cmd == "getMoreEnergy" then
    GameStateRecruit:SelectTab("warpgate")
  elseif cmd == "helpConfirm" then
    self:HideWarpGateHelp()
  elseif cmd == "close_help" then
    self:GetFlashObject():InvokeASCallback("_root", "SetShowWarpGateHelp", false)
  elseif cmd == "FilterMenuOpen" then
    local fla = self:GetFlashObject()
    fla:InvokeASCallback("_root", "OpenFilter")
  elseif cmd == "FilterMenuClose" then
    local fla = self:GetFlashObject()
    fla:InvokeASCallback("_root", "CloseFilter")
  elseif cmd == "FilterMenuSelect" then
    self:SetFilterType(arg, true, true)
  end
end
function GameUIRecruitTopbar:SetFilterType(filterType, isPlayAnim, isUpdateRecruitList)
  local titleText
  if filterType == k_filterTypeNormal then
    titleText = GameLoader:GetGameText("LC_MENU_STAR_PORT_FILTER_NORMAL_LABEL")
  elseif filterType == k_filterTypeElite then
    titleText = GameLoader:GetGameText("LC_MENU_STAR_PORT_FILTER_ELITE_LABEL")
  else
    titleText = GameLoader:GetGameText("LC_MENU_STAR_PORT_FILTER_ALL_LABEL")
  end
  m_currentFilterType = filterType
  local fla = self:GetFlashObject()
  fla:InvokeASCallback("_root", "ChangeFilterType", filterType, titleText, isPlayAnim)
  if isUpdateRecruitList then
    GameUIRecruitMain:OnCommanderFilterTypeChanged()
  end
end
function GameUIRecruitTopbar:GetFliterType()
  return m_currentFilterType
end
function GameUIRecruitTopbar:SetShowFilterMenu(isShow)
  if immanentversion == 1 then
    self:GetFlashObject():InvokeASCallback("_root", "SetShowFilterMenu", isShow)
  end
end
function GameUIRecruitTopbar:GetActiveTabItem()
  return ...
end
function GameUIRecruitTopbar:SelectTabItem(name_tab)
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "lua2fs_selectTabItem", name_tab)
  if name_tab == -1 then
    self:GetFlashObject():InvokeASCallback("_root", "HideRecruitClose")
  end
end
function GameUIRecruitTopbar:PlayWarpGateHelp()
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "SetPlayWarpGateHelp", true)
  self.isShowHelpWindow = true
end
function GameUIRecruitTopbar:HideWarpGateHelp()
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "SetPlayWarpGateHelp", false)
  self.isShowHelpWindow = false
end
function GameUIRecruitTopbar.UpdateResourceData()
  if GameStateManager:GetCurrentGameState() == GameStateRecruit and GameStateRecruit:IsObjectInState(GameUIRecruitTopbar) then
    local data_resource = GameGlobalData:GetData("resource")
    local flash_obj = GameUIRecruitTopbar:GetFlashObject()
    flash_obj:InvokeASCallback("_root", "setLeptonAndQuarkInfo", GameUtils.numberConversion(data_resource.lepton), GameUtils.numberConversion(data_resource.quark))
    local numText = string.format("%d / %d", GameUIRecruitMain:GetActiveServiceNum(), GameGlobalData.max_recruit)
    DebugOut("GameUIRecruitTopbar.UpdateResourceData", numText)
    flash_obj:InvokeASCallback("_root", "setTeamNumber", numText)
  end
end
function GameUIRecruitTopbar:OnInitGame()
  GameGlobalData:RegisterDataChangeCallback("resource", self.UpdateResourceData)
end
function GameUIRecruitTopbar:OnParentStateGetFocus()
  GameUIRecruitTopbar.UpdateResourceData()
end
function GameUIRecruitTopbar:OnAddToGameState()
  self:LoadFlashObject()
  self:SetFilterType(k_filterTypeAll, false, false)
  DebugOut("GameUIRecruitTopbar:OnAddToGameState")
  DebugOut("activeTab", self:GetActiveTabItem())
  self:GetFlashObject():InvokeASCallback("_root", "HideWarpGateTutorial")
  if not QuestTutorialStarCharge:IsActive() or self:GetActiveTabItem() ~= "warpgate" then
  end
  self:GetFlashObject():InvokeASCallback("_root", "HideRecruitNormalTutorial")
  if QuestTutorialRecruit:IsActive() then
    local activeTabItem = self:GetActiveTabItem()
    if activeTabItem and activeTabItem ~= "" and activeTabItem ~= "recruitNormal" then
      local function callback()
        self:GetFlashObject():InvokeASCallback("_root", "ShowRecruitNormalTutorial")
      end
      GameUICommonDialog:PlayStory({100029}, callback)
    end
  end
end
function GameUIRecruitTopbar:OnEraseFromGameState()
  self:UnloadFlashObject()
end
function GameUIRecruitTopbar:SetVisible(isShow)
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "setRootVisible", isShow)
  end
end
if AutoUpdate.isAndroidDevice then
  function GameUIRecruitTopbar.OnAndroidBack()
    local tab_type = GameUIRecruitTopbar:GetActiveTabItem()
    if tab_type == "recruitNormal" then
      GameUIRecruitMainNew.OnAndroidBack()
    elseif tab_type == "warpgate" then
      GameUIHeroUnion.OnAndroidBack()
    elseif tab_type == "owned" then
      GameStateRecruit:Quit()
    else
      GameStateRecruit:Quit()
    end
  end
end
